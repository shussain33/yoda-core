/**
 * yogeshb12:52:38 pm  Copyright Softcell Technolgy
 **/
package com.softcell.rest.interceptors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.AuthenticationConfiguration;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.cache.TokenCache;
import com.softcell.gonogo.cache.services.RedisService;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.AuthSkipConfiguration;
import com.softcell.gonogo.model.core.CheckOAuthTokenRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.AuthTokenPayLoad;
import com.softcell.gonogo.model.security.AuthTokenValidateResponse;
import com.softcell.gonogo.model.security.Authorities;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author yogeshb
 *
 */
@Component
public class TokenAuthInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthInterceptor.class);

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private LookupService lookupService;

    @Value("${gonogo.uam.authskipFLag}")
    private boolean authSkipFlag;

    @Autowired
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long startTime = System.currentTimeMillis();

        logger.info("Request RequestURI: {}" , request.getRequestURI());
        logger.info("Request handler: {}", handler);
        logger.info("Request URL::{}  : Start Time= {} ",request.getRequestURL() , startTime);

        request.setAttribute("startTime", startTime);

        String bearerToken = request.getHeader(Constant.AUTHORIZATION);
        String institutionID = request.getHeader(Constant.INST_ID);
        String sourceId = request.getHeader(Constant.SOURCE_ID);
        String authToken = null;

        if(Objects.nonNull(bearerToken) && bearerToken.startsWith("Bearer ")) {
            authToken = bearerToken.substring(7, bearerToken.length());
        }
        TokenCache.INHERITABLE_THREAD_LOCAL_CACHE.set(bearerToken);
        if(StringUtils.isBlank(institutionID) || StringUtils.isBlank(sourceId)){
            institutionID = GNGWorkflowConstant.DEFAULT_INSTITUTION_ID.toFaceValue();
            sourceId = GNGWorkflowConstant.DEFAULT_INSTITUTION_ID.toFaceValue();
        }

        String role = request.getHeader(Constant.ROLENAME);
        String userId = request.getHeader(Constant.USER_ID);
        String apiName = GngUtils.getApiName(request);
        if(Objects.nonNull(bearerToken) && bearerToken.startsWith("Self-Serve ")) {
            authToken = bearerToken;
        }

        logger.warn("Inside TokenAuthInterceptor for institutionId {} accessing api {}",institutionID,apiName);

        ActionConfiguration skipConfiguration = lookupService.getActionConfig(institutionID, ActionName.API_SKIP_AUTHENTICATION);
        if(skipConfiguration != null && skipConfiguration.getSkipApiList() != null && CollectionUtils.isNotEmpty(skipConfiguration.getSkipApiList()) && containsApi(skipConfiguration.getSkipApiList(), apiName)){
            logger.warn("API SKIP enabled for apiName {} and institutionID {} ",apiName,institutionID);
            return true;
        }

        ActionConfiguration actionConfiguration = lookupService.getActionConfigurations(institutionID, sourceId, ActionName.API_ROLE_AUTHORISATION_ENABLE);
        if((StringUtils.isBlank(authToken) || StringUtils.isBlank(role)) && actionConfiguration!= null && actionConfiguration.isEnable() && !StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(),GNGWorkflowConstant.OFF.toFaceValue())){
            if(StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(),GNGWorkflowConstant.ON.toFaceValue())) {
                logger.warn("Authentication Failed for api {} and institution {}",apiName,institutionID);
                return setNegativeResponse(request,response);
            }else{
                logger.warn("Authentication Attempted for api {} and institution {}",apiName,institutionID);
            }
        }

        if(authSkipFlag && StringUtils.isNotBlank(institutionID) &&StringUtils.isNotBlank(authToken) && actionConfiguration !=null && !StringUtils.equalsIgnoreCase(sourceId, Constant.DEFAULT_INSTID)) {

            if (lookupService.checkActionAccess(institutionID,sourceId,ActionName.API_ROLE_AUTHORISATION_ENABLE) && StringUtils.isNotBlank(role)) {

                if (lookupService.checkApiRoleAccess(institutionID, sourceId,apiName, role,request.getMethod())) {
                    logger.info("preHandle: inside if:1");
                    return validateTokenWithAuthorities(authToken, request, response, institutionID,sourceId,role,userId,apiName);
                } else if (StringUtils.equalsIgnoreCase(actionConfiguration.getAuthorisationMode(),GNGWorkflowConstant.ON.toFaceValue())){
                    logger.info("preHandle: inside if:2");
                    return setNegativeResponse(request,response);
                } else if(!StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(),GNGWorkflowConstant.OFF.toFaceValue())){
                    logger.info("preHandle: inside if:3");
                    return validateToken(authToken, request, response, institutionID,false,actionConfiguration.isCheckUserId(),userId);
                }

            } else {
                logger.info("preHandle: inside else:1");
                return validateToken(authToken, request, response, institutionID,false,actionConfiguration.isCheckUserId(),userId);
            }

        }else if (lookupService.checkActionsAccess(institutionID, ActionName.MANDATORY_AUTHTOKEN) && StringUtils.isBlank(authToken)){
            logger.info("preHandle: inside else:2");
            AuthTokenPayLoad authTokenPayLoad = new AuthTokenPayLoad();
            authTokenPayLoad.setMessage(ErrorCode.NON_AUTHORITATIVE_INFORMATION);
            verifyToken(request, response, authTokenPayLoad);
            return false;
        }
        return super.preHandle(request, response, handler);
    }

    public boolean validateToken(String authToken,HttpServletRequest request, HttpServletResponse response,String institutionID,boolean userDetails,boolean checkUserId,String userId){

        try {
            userId = userId.concat("#").concat(institutionID);
            boolean redisToken = redisService.checkTokenIntoRedis(authToken);
            if (!redisToken) {
                AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_TOKEN_KEY.toFaceValue());
                AuthTokenPayLoad authTokenPayLoad = checkToken(authToken, config, institutionID,userDetails);

                logger.info("Token status is ={}", authTokenPayLoad.getStatus());
                if (!(StringUtils.equalsIgnoreCase("200", authTokenPayLoad.getStatus()))) {
                    verifyToken(request, response, authTokenPayLoad);
                    return false;
                }else if(checkUserId && !StringUtils.equals(userId,authTokenPayLoad.getUserName())){
                    return  setNegativeResponse(request,response);
                } else {
                    redisService.setToken(authToken, authTokenPayLoad.getExp(), authTokenPayLoad);
                }
            }
        }catch (Exception e){
            logger.error("Error occured in validateToken of TokenAuthInterceptor {}",e);
            return false;
        }
        return true;
    }

    public boolean validateTokenWithAuthorities(String authToken,HttpServletRequest request, HttpServletResponse response,String institutionID,String sourceId,String role, String userId,String apiName){

        try {
            ActionConfiguration actionConfiguration = lookupService.getActionConfigurations(institutionID, sourceId, ActionName.API_ROLE_AUTHORISATION_ENABLE);
            if(actionConfiguration != null && actionConfiguration.isEnable() && !StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(), GNGWorkflowConstant.OFF.toFaceValue())) {

                String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionID,FieldSeparator.UNDER_SCORE_STR,Constant.USER_DETAILS
                        ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
                Object obj = redisService.getSetCacheValues(redisKey);
                LoginServiceResponse loginServiceResponse = (LoginServiceResponse) obj;

                if (loginServiceResponse == null && !StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(), GNGWorkflowConstant.REPORT_ONLY.toFaceValue()) ) {
                    logger.info("validateTokenWithAuthorities: inside if: 1");
                    return setNegativeResponse(request, response);
                } else if(loginServiceResponse!= null){

                    if(actionConfiguration.isCheckUserId() && !StringUtils.equals(userId,loginServiceResponse.getLoginId())){
                        logger.info("validateTokenWithAuthorities: inside elseif: 1");
                        return setNegativeResponse(request, response);
                    }
                    if (!loginServiceResponse.getAuthorizationToken().equals(authToken) && StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(), GNGWorkflowConstant.ON.toFaceValue())) {
                        logger.info("validateTokenWithAuthorities: inside elseif: 2");
                        return setNegativeResponse(request, response);
                    }
                    if(loginServiceResponse.getRoles()!= null && !loginServiceResponse.getRoles().contains(role) && StringUtils.equalsIgnoreCase(actionConfiguration.getAuthorisationMode(), GNGWorkflowConstant.ON.toFaceValue())){
                        logger.info("validateTokenWithAuthorities: inside elseif: 3");
                        return setNegativeResponse(request, response);
                    }
                }
                logger.warn("Interceptor Logger :: User {} with role {} accessed api {} method {} in Authentication Mode {} and Authorisation Mode {}",userId,role,apiName,request.getMethod(),actionConfiguration.getAuthenticationMode(),actionConfiguration.getAuthorisationMode());
            }
        }catch (Exception e){
            logger.error("Error occured in validateTokenwithAuthorities of TokenAuthInterceptor {}",e);
            return false;
        }
        return true;
    }

    private AuthSkipConfiguration checkAuthConfig(String institutionID) {
        AuthSkipConfiguration authSkipConfiguration = redisService.getAuthConfig(institutionID);
        if (authSkipConfiguration==null){
            authSkipConfiguration=configurationRepository.getAuthConfigByInstId(institutionID);
            if (authSkipConfiguration!=null){
                redisService.setAuthConfig(authSkipConfiguration);
            }
        }
        return authSkipConfiguration;
    }


    public AuthTokenPayLoad checkToken(String value, AuthenticationConfiguration config, String institutionId,boolean userDetails) {
        logger.warn("check token is valid or not");
        String tokenUrl = config.getUrl();
        logger.warn("check token url: {}",tokenUrl);

        CheckOAuthTokenRequest checkOAuthTokenRequest = new CheckOAuthTokenRequest();
        checkOAuthTokenRequest.setToken(value);
        checkOAuthTokenRequest.setUserDetails(userDetails);

        checkOAuthTokenRequest.setApplication(GNGWorkflowConstant.GNG.toFaceValue());
        checkOAuthTokenRequest.setInstitutionName(Institute.getInstitute(institutionId).name());
        String response=null;
        AuthTokenPayLoad authTokenPayLoad=new AuthTokenPayLoad();


        AuthTokenValidateResponse authTokenValidateResponse=new AuthTokenValidateResponse();
        try {
            response = httpTransportationService.postRequest(tokenUrl, JsonUtil.ObjectToString(checkOAuthTokenRequest), MediaType.APPLICATION_JSON_VALUE);
            authTokenValidateResponse= JsonUtil.StringToObject(response,  AuthTokenValidateResponse.class);

            logger.info("Response ={}",response);
            if(authTokenValidateResponse!=null && authTokenValidateResponse.getBody()!=null
                    && authTokenValidateResponse.getBody().getAuthTokenPayLoad()!=null){
                authTokenPayLoad=authTokenValidateResponse.getBody().getAuthTokenPayLoad();
            }

        } catch(ClientProtocolException ex) {
            logger.error("Client Protocol Error when connecting to AuthServer while getting a token:{} ",ex);

        } catch(IOException ex) {
            logger.error("IO Exception when connecting to AuthServer while getting a token:{} ", ex);
        } catch (Exception e) {
            logger.error("Error occured while building Request and postRequest {} ", e);

        }
        return authTokenPayLoad;
    }

    public boolean setNegativeResponse(HttpServletRequest request, HttpServletResponse response){
        try {
            AuthTokenPayLoad authTokenPayLoad = new AuthTokenPayLoad();
            authTokenPayLoad.setMessage(ErrorCode.NON_AUTHORITATIVE_INFORMATION);
            verifyToken(request, response, authTokenPayLoad);
        }catch (Exception e){
            logger.error("Exception occured in setNegativeResponse {}",e);

        }
        return false;
    }

    public void verifyToken(HttpServletRequest request, HttpServletResponse response, AuthTokenPayLoad authTokenPayLoad) throws IOException {
        BaseResponse baseResponse;
        try {
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.UNAUTHORIZED_FAILED, String.valueOf(authTokenPayLoad.getMessage()));
            ObjectMapper mapper=new ObjectMapper();
            response.addHeader("Access-Control-Allow-Origin", "*");

            if (request.getHeader("Access-Control-Request-Method") != null
                    && "OPTIONS".equals(request.getMethod())) {

                response.addHeader("Access-Control-Allow-Methods",
                        "GET, POST, PUT, DELETE");
                response.addHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token-key,username,password");
                response.addHeader("Access-Control-Max-Age", "1");
            }

            response.setStatus(200);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(mapper.writeValueAsString(baseResponse));


        }catch (Exception e){
            logger.error("Exception occured while setting the response:{}",e);
        }
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        logger.info("Request URL:: {} Sent to Handler :: Current Time={}",request.getRequestURL(),System.currentTimeMillis());
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        long startTime = (Long) request.getAttribute("startTime");

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        logger.info("Request URL:: {} Time Taken= {} ms" , request.getRequestURL(), System.currentTimeMillis() - startTime);

        if (null != ex) {
            logger.error("Some error occured while afterCompletion , with probable cause {}",ex);
        }
    }

    public Authorities[] mapAuthorities(List<String> authoritiesList){

        Authorities[] authorities = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            authorities = mapper.readValue(authoritiesList.toString(), Authorities[].class);
        }catch (Exception e){
            logger.error("Error occured in mapAuthorities due to {}",e);
        }
        return authorities;
    }

    public boolean isTokenRoleValid(Object obj,String role,String authorisationMode,boolean isCheckUserId,String userId,String institutionID){

        try{
            AuthTokenPayLoad authTokenPayLoad = (AuthTokenPayLoad) obj;
            if(authTokenPayLoad != null){

                if(isCheckUserId && !StringUtils.equals(userId,authTokenPayLoad.getUserName())){
                    return false;
                }

                for(Authorities authorities : authTokenPayLoad.getAuthorityList()){
                    if(StringUtils.equalsIgnoreCase(authorities.getRoleName(),role) && authorities.isActive() && StringUtils.equalsIgnoreCase(String.valueOf(authTokenPayLoad.getInstitutionId()),institutionID)){
                        return true;
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error occured in isTokenRoleValid due to {}",e);
            return false;
        }

        logger.warn("Authorisation mode {} failed for role {}",authorisationMode,role);
        return StringUtils.equalsIgnoreCase(authorisationMode,GNGWorkflowConstant.REPORT_ONLY.toFaceValue());
    }

    private boolean containsApi(Set<String> skipSet, String apiName){
        for(String skipApi : skipSet){
            if (apiName.contains(skipApi)) {
                return true;
            }
        }
        return false;
    }

}

