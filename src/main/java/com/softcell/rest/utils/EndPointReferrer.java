package com.softcell.rest.utils;

/**
 * this class represent all rest end points configured within a application
 * this will work as centralized repository for end points
 */
public final class EndPointReferrer {


    public static final String ADD_MULTI_PRODUCT_CONFIG = "add-multiproduct-config";
    public static final String DELETE_MULTI_PRODUCT_CONFIG = "delete-multiproduct-config";
    public static final String UPDT_MULTI_PROD_CONFIG = "update-multiproduct-config";
    public static final String FIND_ALL_MULTI_PROD_CONFIG = "find-all-multiproduct-config";
    public static final String SET_VER = "set-version";
    public static final String RETIRED_VER = "retired-version";
    public static final String SHOW_VER = "show-version";
    public static final String IS_VALID_VER = "is-valid-version";
    public static final String ADD_INSTI_PROD_CONFIG = "add-institution-product-config";
    public static final String REMOVE_INSTI_PROD_CONFIG = "remove-institution-product-config";
    public static final String UPDT_INSTI_PROD_CONFIG = "update-institution-product-config";
    public static final String FIND_ALL_INSTI_PROD_CONFIG = "find-all-institution-product-config";
    public static final String ADD_TMPLT_CONFIG = "add-template-config";
    public static final String UPDT_TMPLT_PATH = "update-template-path";
    public static final String UPDT_TMPLT_LOGO = "update-template-logo";
    public static final String ENABLE_TMPLT = "enable-template";
    public static final String DISABLE_TMPLT = "disable-template";
    public static final String FIND_ALL_TMPLT_CONFIG = "find-all-template-config";
    public static final String ADD_ACTION_CONFIG = "add-action-config";
    public static final String UPDT_ACTION_CONFIG = "update-action-config";
    public static final String DEL_ACTION_CONFIG = "delete-action-config";
    public static final String FIND_ALL_ACTION_CONFIG ="find-all-action-config";
    public static final String ADD_SMS_CONFIG = "add-sms-config";
    public static final String UPDT_SMS_CONFIG = "update-sms-config";
    public static final String DEL_SMS_CONFIG = "delete-sms-config";
    public static final String ADD_EMAIL_CONFIG = "add-email-config";
    public static final String UPDT_EMAIL_CONFIG = "update-email-config";
    public static final String DISABLE_EMAIL_CONFIG = "disable-email-config";
    public static final String ENABLE_EMAIL_CONFIG = "enable-email-config";
    public static final String FIND_ALL_EMAIL_CONFIG = "find-all-email-config";
    public static final String ADD_ATTCH_IN_EMAIL_CONFIG = "add-attachment-email-config";
    public static final String DEL_ATTCH_IN_EMAIL_CONFIG = "delete-attachments-email-config";
    public static final String ADD_SMS_TEMPLATE_CONFIG = "add-sms-template-config";
    public static final String GET_SMS_TEMPLATE_CONFIG = "get-sms-template-config";
    public static final String DELETE_SMS_TEMPLATE_CONFIG = "delete-sms-template-config";
    /**
     * masters upload aka {@see FileUploadController } related endpoint constants
     */
    public static final String MASTERS_UPLOAD_BASE_ENDPOINT = "master/upload";
    public static final String SCHEME_MASTER = "scheme-master";
    public static final String ASSET_MODEL_MASTER = "asset-model-master";
    public static final String PINCODE_MASTER = "pincode-master";
    public static final String EMPLOYER_MASTER = "employer-master";
    public static final String DEALER_EMAIL_MASTER = "dealer-email-master";
    public static final String GNG_CC_DEALEREMAIL_MASTER = "gng-cc-dealerEmail-master";
    public static final String SCHEME_MODEL_DEALER_CITY_MAPPING_MASTER = "scheme-model-dealer-City-mapping-master";
    public static final String SCHEME_DATE_MAPPING_MASTER = "scheme-date-mapping-master";
    public static final String CAR_SURROGATE_MASTER = "car-surrogate-master";
    public static final String PL_PINCODE_EMAIL_MASTER = "pl-pincode-email-master";
    public static final String BANK_DETAILS_MASTER = "bank-details-master";
    public static final String HIERARCHY_MASTER = "hierarchy-master";
    public static final String CDL_HIERARCHY_MASTER = "cdl-hierarchy-master";
    public static final String BRANCH_MASTER = "branch-master";
    public static final String CITY_STATE_MASTER = "city-state-master";
    public static final String VERSION = "version";
    public static final String MODEL_VARIANT_MASTER = "model-variant-master";
    public static final String ASSET_CATEGORY_MASTER = "asset-category-master";
    public static final String STATE_MASTER = "state-master";
    public static final String CITY_MASTER = "city-master";
    public static final String UPLOAD_ROI_SCHEME_MASTER = "upload-roi-scheme-master";
    public static final String CREDIT_PROMOTION_MASTER = "credit-promotion-master";
    public static final String DSA_BRANCH_MASTER = "dsa-branch-master";
    public static final String DSA_PRODUCT_MASTER = "dsa-product-master";
    public static final String DEALER_BRANCH_MASTER = "dealer-branch-master";
    public static final String NEGATIVE_AREA_GEO_LIMIT_MASTER = "negative-area-funding-master";
    public static final String UPDATE_LOS_UTR_DETAILS_MASTER="update-losid-utr-details";
    public static final String UPDATE_NET_DISBURSEMENT_AMOUNT="update-net-disbursement-amount";
    public static final String RELIANCE_DEALER_BRANCH_MASTER="reliance-dealer-branch-master";
    public static final String REFERENCE_MASTER = "reference-master";
    public static final String LOYALTY_CARD_MASTER = "loyalty-card-master";
    public static final String COMMON_GENERAL_MASTER = "common-general-parameter-master";
    public static final String EW_INS_GNG_ASSET_CAT_MASTER = "ew-ins-gng-asset-category-master";
    public static final String EW_PREMIUM_MASTER = "ew-premium-master";
    public static final String INSURANCE_PREMIUM_MASTER ="insurance-premium-master";
    public static final String ELEC_SERV_PROV_MASTER ="elec-serv-prov-master";
    public static final String SOURCING_DETAIL_MASTER = "sourcing-detail-master";
    public static final String SUPPLIER_LOCATION_MASTER = "supplier-location-master";

    public static final String LOGS = "/logs";
    public static final String TKIL_DEALER_BRANCH_MASTER="tkil-dealer-branch-master";
    public static final String TAX_CODE_MASTER = "tax-code-master";
    public static final String ACCOUNTS_HEAD_MASTER = "accounts-head-master";
    public static final String STATE_BRANCH_MASTER = "state-branch-master";
    public static final String TCLOS_GNG_MAPPING_MASTER = "tclos-gng-mapping-master";
    public static final String MANUFACTURER_MASTER = "manufacturer-master";


    /**
     * home controller i.e user management specific apis
     */

    public static final String HOME_CTRL = "";
    public static final String LOGIN = "login-web";
    public static final String LOGOUT = "logout";
    public static final String CHANGE_PASS = "change-password-gng";
    public static final String RESET_PASS = "reset-password-gng";
    public static final String LOGIN_V2 = "login-web-v2";
    public static final String CHANGE_PASS_V2 = "change-password-gng-v2";
    public static final String RESET_PASS_V2 = "reset-password-gng-v2";
    public static final String LOGIN_V3 = "login-web-v3";
    public static final String RESET_PASS_V3 = "reset-password-gng-v3";
    public static final String CHANGE_USER_PASS = "change-user-password-gng";
    public static final String FORGOT_PASS = "forgot-password";

    // QueueMgmt related endpoints
    //      HeartbeatController
    public static final String AM_ALIVE = "i-am-alive";
    //      QueueMgmtAdmin
    public static final String QUEUE_MGMT = "queue-mgmt";
    public static final String CRO_AUDIT = "cro-audit";
    public static final String CRO_STATISTICS = "cro-statistics";
    public static final String ASSIGN_CASES = "assign-cases";
    public static final String SCHEDULER_INFO = "scheduler-info";
    public static final String RUN_SCHEDULER = "run-scheduler";
    public static final String CHANGE_AVAILABILITY = "change-availability";
    public static final String UNASSIGNED_APPLICATIONS = "unassigned-applications";
    public static final String CREATE_QUEUE_GROUP_CONFIGURATION = "create-queuegroups";
    public static final String GET_QUEUE_GROUP_CONFIGURATION = "get-queuegroups";
    public static final String CHANGE_APP_LOCK_STATUS = "change-app-lock-status";

    /**
     * workFlow related EndPointReferrer
     */

    public static final String WORKFLOW_BASE_ENPOINT = "worker";
    public static final String REPROCESS_BY_ID = "reprocess-by-id";
    public static final String REPROCESS_UPDATED = "reprocess-updated";
    public static final String BRE_AUDIT_DATA = "bre-audit-data";
    public static final String REAPPRAISE = "reappraise";
    public static final String REAPPRAISAL_STATUS = "reappraisal-status";
    public static final String WF_COMM_CONNECTION = "workflow/communication";
    public static final String WF_COMM_CONNECTION_DELETE = "workflow/communication/delete";



    /**
     * Serial Number Validation Controller related EndPoint Referer
     */

    public static final String API_MANUFACTURER_SERIAL_NUMBER_VALIDATION = "api/manufacturer/serial-number-validation";
    public static final String API_MANUFACTURER_IS_SERIAL_NO_APPLICABLE = "api/manufacturer/is-serial-no-applicable";
    public static final String GET_UPDATED_SERIAL_NO_DETAILS = "get-updated-sr-number-details";
    public static final String GET_SERIAL_NO_DETAILS = "get-serial-number-details";
    public static final String SERIAL_OR_IMEI_NUMBER_ROLLBACK = "serial-or-imei-number-rollback";
    public static final String GET_SERIAL_OR_IMEI_NUMBER_FOR_ROLLBACK = "get-serial-or-imei-number-for-rollback";
    public static final String SERIAL_NUMBER_DISBURSED = "serial-number-disbursed";
    public static final String KENT_SERIAL_NUMBER = "GetSerialInfo";
    public static final String KENT_SERIAL_ROLLBACK = "Rollback";
    public static final String KENT_SERIAL_DISBURSED = "SerialNoSold";


    /**
     * Reporting Controller related EndPointReferel
     */

    public static final String REPORTING_BASE_ENDPOINT = "report";
    public static final String CUSTOM_REPORT = "custom-report";
    public static final String ASSET_MANUFACTURE_GRAPH = "asset-manufacture-graph";
    public static final String DOWNLOAD_CREDIT = "download-credit";
    public static final String DOWNLOAD_TVR_REPORT = "download-tvr-report";
    public static final String SAVE_CUSTOM_REPORT = "save-custom-report";
    public static final String PREVIEW_CUSTOM_REPORT = "preview-custom-report";
    public static final String DOWNLOAD_ZIP_REPORT = "download-zip-report";
    public static final String REPORTING_DIMENSION = "reporting-Dimension";
    public static final String SEARCH_REPORT_NAME = "search-report-name";
    public static final String FETCH_REPORT_CONFIG = "fetch-report-config";
    public static final String SAVE_DEFAULT_REPORT = "save-default-report";
    public static final String CHANNEL_WISE_REPORT = "channel-wise-report";
    public static final String DEVICE_INFO_REPORT = "device-info-report";
    public static final String SALES_INCENT_REPORT = "sales-incent-report";
    public static final String SALES_INCENT_REPORT_ZIP = "sales-incent-report-zip";
    public static final String SALES_REPORT_ZIP = "sales-report-zip";
    public static final String CREDIT_REPORT_ZIP = "credit-report-zip";
    public static final String FIRST_LAST_LOGIN = "first-last-login";
    public static final String FIRST_LAST_LOGIN_ZIP = "first-last-login-zip";
    public static final String OTP_ZIP = "otp-zip";
    public static final String OTP_ZIP_REPORT = "otp-zip-report";
    public static final String SERIAL_NUMBER_INFO_LOG_ALL = "serial-number-info-log-all";
    public static final String SERIAL_NUMBER_INFO_LOG_BY_FILTERS = "serial-number-info-log-by-filters";
    public static final String SERIAL_NUMBER_INFO_ZIP_REPORT = "serial-number-log-zip-by-vendor";
    public static final String ASSET_HIERARCHY = "asset-hierarchy";
    public static final String LOS_UTR_UPDATE_ZIP_REPORT="los-utr-update-report";
    public static final String SERIAL_NUMBER_ROLLBACK_ZIP_REPORT = "serial-number-rollback-report";
    public static final String DOWNLOAD_CREDIT_REPORT = "download-credit-report";

    /**
     * MultiProduct Controller related EndPointReferel
     */

    public static final String MULTIPRODUCT_BASE_ENDPOINT = "multiproduct";
    public static final String ASSET_COUNT = "asset-count";
    public static final String PRODUCT_ADD_REQUEST = "product-add-request";
    public static final String IS_DEALER_SUBVENTION_WAIVED_OFF = "is-dealer-subvention-waived-off";
    /**
     * MetaDataStore Controller related EndPoint Referel
     */

    public static final String GET_CAR_SURROGATE_MASTER = "get-car-surrogate-master";
    public static final String FILTERED_SCHEME_MASTER_DEPRECATED = "filtered-scheme-master-deprecated";
    public static final String FILTERED_SCHEME_MASTER = "filtered-scheme-master";
    public static final String BANK_DETAILS = "bank-details";
    public static final String ASSET_MODEL_MAKE_WEB = "asset-model-make-web";
    public static final String PINCODE_DETAILS_WEB = "pincode-details-web";
    public static final String EMPLOYER_MASTER_DETAILS_WEB = "employer-master-details-web";
    public static final String GET_MODEL_VARIANTS = "get-model-variants";
    public static final String GET_REFERENCE_RELATION_DETAILS = "get-reference-relation-details";
    public static final String CITY_MASTER_DETAILS_WEB = "city-master-details-web";
    /**
     * Master Controller Related EndPointReferrer
     */
    public static final String MASTER_BASE_ENDPOINT = "master/search";
    public static final String ASSET_MODEL_MASTER_GET_MANUFACTURERS = "asset-model-master/get-manufacturers";
    public static final String ASSET_MODEL_MASTER_GET_MANUFACTURERS_DETAILS = "asset-model-master/get-manufacturers-details";
    public static final String ASSET_MODEL_MASTER_GET_MODEL_NUMBER_WITH_OTHER_DETAILS = "asset-model-master/get-model-number-with-other-details";
    public static final String ASSET_MODEL_MASTER_GET_CATEGORIES = "asset-model-master/get-categories";
    public static final String ASSET_MODEL_MASTER_GET_MAKE = "asset-model-master/get-make";
    public static final String ASSET_MODEL_MASTER_GET_MODEL_NUMBER = "asset-model-master/get-model-number";
    public static final String ASSET_MODEL_MASTER_BY_MODELNUMBER = "asset-model-master-by-modelnumber";
    public static final String FULL_TEXT_ASSET_MODEL_MASTER = "full-text/asset-model-master";
    public static final String DEALER_EMAIL_MASTER_GET_DEALER_RANKING = "dealer-email-master/get-dealer-ranking";
    /**
     * DataEntryController related EndPointReferrer
     */
    public static final String SAVE_APPLICATION_STEPID = "save-application/{stepId}";
    public static final String SUBMIT_STEPID = "submit/{stepId}";
    public static final String STEP_THIRD_PARTY_DATA = "third-party-save";
    public static final String STEP_REGISTRATION = "registration";
    public static final String STEP_DEMOGRAPHIC_DETAILS = "demographic-details";
    public static final String STEP_PROFESSION_INCOME_DETAILS = "profession-income-details";
    public static final String STEP_COLLATERAL_DETAILS = "collateral-details";
    public static final String STEP_IMD_DETAILS = "imd-details";
    public static final String STEP_INCOME_DETAILS = "income-details";

    /*
    * dataEntryController related EndPointreferral xtra for in case of pl
    **/
   public static final String STEP_BANKING_DETAILS = "banking-details";
    /*
    * CAM-Credit Approval Memo
    * */
    public static final String CAM_DETAILS = "cam-details/{camType}";
    public static final String CAM_RTR_DETAILS = "rtr-details";
    public static final String CAM_PL_BS_DETAILS = "pl-bs-details";
    public static final String CAM_BANKING_DETAILS = "banking-details";
    public static final String CAM_RATIOS_DETAILS = "ratios-details";
    public static final String CAM_SUMMARY = "cam-summary";
    public static final String GET_CAM_DETAILS = "get-cam-details";
    public static final String ALL_CAM_DETAILS = "cam-details";
    public static final String UPDATE_CAM_DETAILS = "update-cam-details";

    // Verification
    public static final String SAVE_VERIFICATION_DETAILS = "save-verification/{verificationType}";
    public static final String GET_VERIFICATION_DETAILS = "get-verification";
    public static final String OFFICE_VERIFICATION = "office";
    public static final String RESIDENCE_VERIFICATION = "residence";
    public static final String BANK_VERIFICATION = "bank-details";
    public static final String ITR_VERIFICATION = "itr";
    public static final String REFERENCE_DETAILS = "reference-details";
    public static final String LEGAL_VERIFICATION = "legal";
    public static final String VALUATION_VERIFICATION = "valuation";
    public static final String TRIGGER_VERIFICATION = "trigger-verification";
    public static final String SEND_BACK_CASES = "SendBackCases";
    public static final String DASHBOARD = "Dashboard";

    public static final String SAVE_PROPERTY_VISIT_DETAILS = "save-property-visit";
    public static final String GET_PROPERTY_VISIT_DETAILS = "get-property-visit";
    public static final String SAVE_LEGAL_VERIFICATION_DETAILS = "save-legal-verification";
    public static final String GET_LEGAL_VERIFICATION_DETAILS = "get-legal-verification";
    public static final String SAVE_CUSTOMER_CREDIT_DOCS = "save-customer-credit-docs";
    public static final String GET_CUSTOMER_CREDIT_DOCS = "get-customer-credit-docs";
    public static final String SAVE_ELIGIBILITY_DETAILS = "save-eligibility-details";
    public static final String GET_ELIGIBILITY_DETAILS = "get-eligibility-details";

    public static final String SAVE_PERSONAL_DISCUSSION_DETAILS = "save-personal-discussion";
    public static final String SAVE_REPAYMENT_DETAILS = "save-repayment";
    public static final String GET_REPAYMENT_DETAILS = "get-repayment";
    public static final String SAVE_VALUATION_DETAILS = "save-valuation";
    public static final String DELETE_VALUATION_DETAILS = "delete-valuation";
    public static final String GET_VALUATION_DETAILS = "get-valuation";

    public static final String SAVE_LIST_OF_DOCUMENTS = "save-list-of-docs";
    public static final String GET_LIST_OF_DOCUMENTS = "get-list-of-docs";
    public static final String SAVE_LOAN_CHARGES_DETAILS = "save-loan-charges";
    public static final String GET_LOAN_CHARGES_DETAILS = "get-loan-charges";
    public static final String SAVE_DM_DETAILS = "save-disbursementmemo";
    public static final String GET_DM_DETAILS = "get-disbursementmemo";
    public static final String ADDRESS_TYPE_OFFICE = "OFFICE";
    public static final String INDIVIDUAL = "Individual";
    public static final String ADDRESS_TYPE_RESIDENCE = "RESIDENCE";
    public static final String SAVE_SANCTION_CONDITIONS = "save-sanction-conditions";
    public static final String GET_SANCTION_CONDITIONS  = "get-sanction-conditions";
    public static final String GET_DOCUMENT_BY_FILE_NAME  = "get-document-by-fileName";
    public static final String  UPDATE_COMPLETED_INFO_GNG = "update-completed-info-gng";
    public static final String  DELETE_COLLATERAL = "delete-collateral";
    public static final String  DELETE_CO_APPLICANT = "delete-co-applicant";

    public static final String SAVE_PERFIOS_DATA = "save-pefios-data";
    public static final String GET_PERFIOS_DATA = "get-pefios-data";
    public static final String UPDATE_PERFIOS_DATA = "update-pefios-data";

    public static final String GET_MIFIN_LOG = "get-mifin-log";

    public static final String REASSIGN_APPLICATION = "reassign-application";
    public static final String RESET_ALLOCATION_INFO = "reset-allocation-info";
    public static final String MIGRATE_COAPPLICANT_CIBILSCORE = "migrate-coapplicant-cibilscore";
    public static final String MIGRATE_APPLICANT_PLBS_DATA = "migrate-applicant-plbs-data";

    /**
     * GradualApplication Controller related EndPointReferrer
     */
    public static final String SUBMIT_APPLICATION_STEPID = "submit-application/{stepId}";
    public static final String APPLICATION_REQUEST_VALIDATION = "application-request-validation";
    /**
     * File Parser Controller Related End Point Referer.
     */
    public static final String MASTER_PARSER_BASE_ENDPOINT = "master/parser";
    public static final String PARSE_SCHEME_MASTER = "scheme-master";
    public static final String PARSE_ASSET_MODEL_MASTER = "asset-model-master";
    public static final String PARSE_PINCODE_MASTER = "pincode-master";
    public static final String PARSE_EMPLOYER_MASTER = "employer-master";
    public static final String PARSE_DEALER_EMAIL_MASTER = "dealer-email-master";
    public static final String PARSE_SCHEME_MODEL_DEALER_CITY_MAPPING_MASTER = "scheme-model-dealer-City-mapping-master";
    public static final String PARSE_SCHEME_DATE_MAPPING_MASTER = "scheme-date-mapping-master";
    public static final String PARSE_HITACHI_SCHEME_MASTER = "hitachi-scheme-master";
    public static final String PARSE_CAR_SURROGATE_MASTER = "car-surrogate-master";
    /**
     * ES Search Controller Related End Point Referer
     */
    public static final String ES_SEARCH_BASE_ENDPOINT = "api/es/";
    public static final String SEARCH = "search";
    public static final String UPDATEINDEX_STARTDATE_ENDDATE = "updateIndex/{startDate}/{endDate}/{instituionId}";
    /**
     * Document Store Controller Related End Point Referer
     */

    public static final String UPLOAD_IMAGE = "upload-image";
    public static final String UPLOAD_IMAGE_WITH_METADATA = "upload-image-with-metadata";
    public static final String UPDATE_IMAGE_STATUS = "update-image-status";
    public static final String GET_IMAGE = "get-image";
    public static final String GET_IMAGES = "get-images";
    public static final String GET_IMAGE_DETAILS = "get-image-details" ;
    public static final String IMAGES_METADATA = "images-metadata";
    public static final String GET_IMAGE_BY_ID = "get-image-by-id";
    public static final String GET_IMAGE_BY_ID_BASE64 = "get-image-by-id-base64";
    public static final String GET_PDF_BY_ID = "get-pdf-by-id";
    public static final String SEND_MAIL_PDF = "send-mail-pdf";
    public static final String SEND_MAIL_PDF_V1 = "send-mail-pdf-v1";
    public static final String GET_PDF_REF = "get-pdf-ref";
    public static final String CIBIL_PDF_REPORT = "cibil-pdf-report";
    public static final String CIBIL_PDF_REPORT_COAPP = "cibil-pdf-report-coapp";
    public static final String UPLOAD_IMAGE_LINKAGE = "upload-image-linkage";
    public static final String DELETE_DOCUMENT = "delete-document";
    /**
     * DMZ Controller related End Point Referer.
     */

    public static final String CONNECT_DEALER = "connectDealer";
    public static final String CONNECT_RELIANCE = "connectReliance";
    public static final String CONNECT_LOS_GNG = "connectLosGng";
    public static final String CONNECT_LOS_MB = "connectLosMB";
    public static final String LOS_RESPONSE = "los-response";
    public static final String CHECK_SERVICE_STATUS = "check-service-status";
    public static final String RELIANCE_RESPONSE = "reliance-response";
    public static final String TKIL_LOG = "tkil-log";
    /**
     * Digitization Controller related End Point Referer.
     */

    public static final String DIGITIZATION_BASE_ENDPOINT = "digitization";
    public static final String DOWNLOAD_APPLICATION_FORM = "download-application-form";
    public static final String DOWNLOAD_AGREEMENT_FORM = "download-agreement-form";
    public static final String DOWNLOAD_DELIVERY_ORDER = "download-delivery-order";
    public static final String DOWNLOAD_ACH_MANDATE_FORM = "download-ach-mandate-form";
    public static final String DOWNLOAD_APPLICATION_REPORT = "download-application-report";  //FiveStar
    public static final String DOWNLOAD_SANCTION_LETTER_LAP = "download-sanction-letter"; // SBFC LAP

    /**
     * added for Tvs digitization purpose
     */
    public static final String DOWNLOAD_DIGITIZATION_FORM = "download-digitization-form";
    /**
     * CRO Controller End Point Referer
     */

    public static final String APPLICATION_DATA = "application-data";
    public static final String APPLICATION_DATA_COAPPLICANT = "application-data-coapplicant";
    public static final String APPLICATION_IMAGES = "application-images";
    public static final String DASHBOARD_APPLICATION_DATA = "dashboard-application-data";
    public static final String APPLICATION_DATA_CRO2 = "application-data-cro2";
    public static final String APPLICATION_DATA_PARTIAL = "application-data-partial";
    public static final String CRO_APPROVAL = "cro-approval";
    public static final String RESET_STATUS = "reset-status";
    public static final String CRO_ONHOLD = "cro-onhold";
    public static final String CRO_QUEUE = "cro-queue";
    public static final String CRO_QUEUE_CRITERIA = "cro-queue-criteria";
    public static final String CRO2_QUEUE = "cro2-queue";
    public static final String CRO3_QUEUE = "cro3-queue";
    public static final String CRO4_QUEUE = "cro4-queue";
    public static final String FETCH_POSTIPA = "fetch-postIpa";
    public static final String UPDATE_LOS_DETAILS = "update-los-details";
    public static final String UPDATE_INVOICE_DETAILS = "update-invoice-details";
    public static final String DASHBOARD_APP_DATA = "dashboard-app-data";
    public static final String REAPPRAISAL_APPLICATIONS = "reappraisal-applications";
    public static final String APPLICATION_STATUS_LOG="get-application-status-log";
    public static final String DEDUPE_APPLICATION_DETAILS="get-dedupe-application-details";
    public static final String UPDATE_TVR_STATUS = "update-tvr-status";
    public static final String GET_TVR_STATUS = "get-tvr-status";
    public static final String APPLICATION_DATA_BY_MOB = "application-data-by-mob";
    /**
     * Credit Card Controller End Point Referer
     */
    public static final String CREDIT_CARD_BASE_ENDPOINT = "surrogate/check";
    public static final String CREDITCARD = "creditcard";
    /**
     * Client Authentication Controller End Point Referel
     */

    public static final String GET_OTP = "get-otp";
    public static final String GET_OTP_BY_AADHAR = "get-otp-by-aadhar";
    public static final String VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR = "verify-otp-with-ekyc-details-by-aadhar";
    public static final String EKYC_BIOMETRIC = "ekyc-biometric";
    public static final String EKYC_BIOMETRIC_V2_2 = "ekyc-biometric-v2-2";
    public static final String EKYC_IRIS = "ekyc-iris";
    public static final String SEND_SMS = "send-sms";
    public static final String EKYC_IRIS_V2_1 = "ekyc-iris-v2-1";
    public static final String EKYC_IRIS_V2_2 = "ekyc-iris-v2-2";
    public static final String GET_OTP_BY_AADHAR_V2_1 = "get-otp-by-aadhar-v2-1";
    public static final String GET_OTP_BY_AADHAR_V2_2 = "get-otp-by-aadhar-v2-2";
    public static final String VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1 = "verify-otp-with-ekyc-details-by-aadhar-v2-1";
    public static final String VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1_1 = "verify-otp-with-ekyc-details-by-aadhar-v2-1-1";

    public static final String VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_2 = "verify-otp-with-ekyc-details-by-aadhar-v2-2";
    /**
     * Application Tracker Controller related End Point.
     */
    public static final String APPLICATION_TRACKER_BASE_ENDPOINT = "history";
    public static final String APPLICATION = "application";
    public static final String DOWNLOAD_CASEHISTORY = "download-casehistory";
    /**
     * Application Health Controller related End Point.
     */

    public static final String APPLICATION_HEALTH_BASE_ENDPOINT = "health";
    public static final String DB_STATS = "db-stats";
    public static final String DB_COLLECTIONS = "db-collections";
    public static final String DB_SERVERSTATUS = "db-serverStatus";
    public static final String DB_HOSTINFO = "db-hostInfo";
    public static final String EXTERNAL_SERVICES_STATUS = "external-services-status";
    public static final String SET_APPLICATION_PASSIVE_MODE = "set-application-passive-mode";
    public static final String SET_APPLICATION_ACTIVE_MODE = "set-application-active-mode";
    /**
     * Application Controller  Related End Point Referer.
     */

    public static final String SUBMIT_APPLICATION = "submit-application";
    public static final String STATUS = "status";
    //old service .this service is replaced by "save-post-ipa-pdf" service
    public static final String POST_IPA_PDF = "post-ipa-pdf";
    //newly added service
    public static final String SAVE_POST_IPA_REQUEST = "save-post-ipa-request";
    public static final String GET_POST_IPA = "get-post-ipa";
    public static final String DASHBOARD_DETAIL = "dashboard-detail";
    public static final String DASHBOARD_DETAIL_V2 = "dashboard-detail-v2";
    //old service
    public static final String POST_IPA_STAGE_UPDATE = "post-ipa-stage-update";
    //newly added service
    public static final String POST_IPA_STAGE_UPDATE_DO_STORE = "post-ipa-stage-update-do-store";

    public static final String GET_PARTIAL_SAVE_REQUEST = "get-partial-save-request";
    public static final String GET_FILE_HANDOVER_DETAILS = "get-file-handover-details";
    public static final String SET_FILE_HANDOVER_DETAILS = "set-file-handover-details";
    public static final String GET_NEGATIVE_AREA_FUNDING_DETAILS = "get-negative-area-funding-details";
    public static final String CHECK_LOYALTY_CARD_STATUS="check-loyalty-card-status";
    public static final String UPDATE_LOYALTY_CARD_DETAILS = "update-loyalty-card-details";
    public static final String GET_LOYALTY_CARD_DETAILS = "get-loyalty-card-details";
    public static final String APPLICATION_STATUS ="get-application-status";
    public static final String GET_EXTENDED_WARRANTY_PREMIUM = "get-extended-warranty-premium";
    public static final String UPDATE_EXTENDED_WARRANTY_DETAILS = "update-extended-warranty-details";
    public static final String UPDATE_EXTENDED_WARRANTY_DETAILS_NSN = "update-extended-warranty-details-nsn";

    public static final String GET_EXTENDED_WARRANTY_DETAILS = "get-extended-warranty-details";
    public static final String GET_INSURANCE_PREMIUM_AGAINST_CATEGORY = "get-insurance-premium-against-category";
    public static final String UPDATE_INSURANCE_PREMIUM_DETAILS = "update-insurance-premium-details";
    public static final String GET_INSURANCE_DETAILS = "get-insurance-details";
    public static final String UPDATE_CREDIT_VIDYA_DETAILS = "update-credit-vidya-details";
    public static final String GET_CREDIT_VIDYA_DETAILS = "get-credit-vidya-details";
    public static final String UPDATE_INSURANCE_PROVIDER_PREMIUM_DETAILS = "update-insurance-provider-premium-details";
    public static final String GET_INSURANCE_PREMIUM_AGAINST_PROVIDER = "get-insurance-premium-against-provider";

    public static final String GET_ACTIVITYLOG_DATA = "get-activity-log-data";



    /**
     * Analytics Controller  Related End Point Referer.
     */

    public static final String STACK_GRAPH = "stack-graph";
    public static final String LOGIN_BY_STATUS_GRAPH = "login-by-status-graph";
    public static final String LOGIN_BY_STATUS_TABLE = "login-by-status-table";
    public static final String LOGIN_BY_STATUS_TABLE_DTC = "login-by-status-table-dtc";
    public static final String LOGIN_BY_STAGE = "login-by-stage";
    public static final String ANALYTICS_ASSET_MANUFACTURE_GRAPH = "asset-manufacture-graph";
    public static final String LOGIN_BY_DEALERSHIP = "login-by-dealership";
    public static final String SCORE_TREE_GRAPH = "score-tree-graph";
    public static final String DSA_REPORTING_DATA = "dsa-reporting-data";
    public static final String UNIQUE_CITIES = "unique-cities";
    public static final String UNIQUE_DEALERS = "unique-dealers";
    public static final String UNIQUE_DSAS = "unique-dsas";
    public static final String UNIQUE_STAGES = "unique-stages";

    /**
     * Admin Controller Related End Point referer.
     */

    public static final String PANLOG = "panlog";
    public static final String AADHARLOG = "aadharlog";
    public static final String MBLOG = "mblog";
    public static final String MBLOG_WITH_COAPPLICANT = "mblog-with-coapplicant";
    public static final String SCORINGLOG = "scoringlog";
    public static final String MAINLOG = "mainlog";
    public static final String ADMIN_SERIAL_NUMBER_INFO_LOG_BY_FILTERS = "serial-number-info-log-by-filters";
    public static final String ADMIN_SERIAL_NUMBER_INFO_LOG_ALL = "serial-number-info-log-all";
    public static final String SERIAL_NUMBER_REQUEST_LOG_BY_FILTERS = "serial-number-request-log-by-filters";
    public static final String SERIAL_NUMBER_REQUEST_LOG_ALL = "serial-number-request-log-all";
    public static final String GET_MAIL_LOG_BY_FILTER = "get-mail-log-by-filter";
    public static final String GET_MAIL_LOG_ALL = "get-mail-log-all";
    public static final String SINGLE_BUCKET_LOG = "single-bucket-log";
    public static final String GET_LOG_REPORT = "get-log-report";
    public static final String GET_JSON_TO_CSV_REPORT = "get-json-to-csv-report";
    public static final String CACHE_REFRESH = "cache-refresh";
    public static final String UPDATE_STATUS_AND_STAGE = "update-status-and-stage";
    public static final String MODIFY_APP_STATUS_AND_STAGE="modify-application-status-and-stage";
    public static final String GET_APP_STATUS_AND_STAGE = "get-app-status-and-stage";
    public static final String GET_SALES_FORCE_LOG = "get-sales-force-log";
    public static final String GET_CREDIT_VIDYA_RESPONSE = "get-credit-vidya-response";
    public static final String GET_SAATHI_RESPONSE = "get-saathi-response";
    public static final String GET_TKYC_LOG = "get-tkyc-log";
    public static final String GET_TCLOS_LOG = "get-tclos-log";
    public static final String GET_SMS_SERVICE_LOG = "get-sms-service-log";
    public static final String BULK_DECISION = "bulk-decision";


    /**
     * Master data View controller related End Point Referrer.
     */
    public static final String MASTER_VIEW_BASE_ENDPOINT = "master/view";
    public static final String GET_ASSET_MODEL_MASTER = "asset-model-master";
    public static final String GET_ALL_SCHEMEIDS = "get-all-schemeIds";
    public static final String GET_SCHEME_DATE_MAPPING_DETAILS = "get-scheme-date-mapping-details";
    public static final String UPDATE_SCHEME_DATE_MAPPING_DETAILS = "update-scheme-details";
    public static final String VALIDATE_ASSET_COST = "validate-asset-cost";
    public static final String GET_PINCODE_MASTER = "pincode-master";
    public static final String GET_EMPLOYER_MASTER = "employer-master";
    public static final String GET_DEALER_EMAIL_MASTER = "dealer-email-master";
    public static final String GET_SCHEME_DEALER_CITY_MAPPING = "scheme-dealer-city-mapping";
    public static final String GET_MANFC_AGAINST_CCID = "get-manfc-against-ccid";
    public static final String GET_ALL_STATES = "get-all-states";
    public static final String GET_ALL_CITY_AGAINST_STATE = "get-all-city-against-state";
    public static final String UPDATE_DEALER_SCHEME_MAPPING_DETAILS = "update-dealer-scheme-mapping-details";
    public static final String FETCH_DEALER_SCHEME_MAPPING_DETAILS = "fetch-dealer-scheme-mapping-details";
    public static final String GET_ASSETCTG_AGAINST_MANFCR = "get-assetCtg-against-manfcr";
    public static final String GET_MODELNO_AGAINST_CTG_MNFC_MAKE = "get-modelno-against-ctg-mnfc-make";
    public static final String GET_ALL_DEALER_AGAINST_CITY = "get-all-dealer-against-city";
    public static final String GET_ALL_MAKE_AGAINST_MNFC_CTG = "get-all-make-against-mnfc-ctg";
    public static final String GET_SCHEMES_AGAINST_CONDITION = "get-schemes-against-condition";
    public static final String GET_ALL_MANUFACTURER = "get-all-manufacturer";
    public static final String GET_ALL_APPLICABLE_SCHEME = "get-all-applicable-scheme";
    public static final String APPROVE_DECLINE_SCHEME_STATUS = "approve-decline-scheme-status";
    public static final String DOWNLOAD_SCHEME_REPORT = "download-scheme-report";
    public static final String DELETE_CGP_MASTER_RECORD = "delete-cgp-master-record";
    public static final String VIEW_CGP_MASTER= "common-general-master";
    public static final String UPDATE_CGP_MASTER_RECORD = "update-cgp-master-record";
    public static final String INSERT_CGP_MASTER_RECORD = "insert-cgp-master-record";
    public static final String GET_SOURCING_DETAILS = "get-sourcing-details" ;
    public static final String GET_ELEC_SERV_PROVIDERS = "elec-serv-prov-master" ;
    public static final String GET_DEVIATION_MASTER = "get-deviation-master" ;
    public static final String GET_ORGANISATIONAL_HIERARCHY_MASTER = "get-organisational-hierarchy-master" ;

    public static final String GET_ALL_BRANCHES = "get-all-branches";
    public static final String GET_ALL_USERS = "get-all-users";
    public static final String GET_ALL_USERS_DATA = "get-all-users-data" ;
    /**
     * Configuration specific settings for spring application
     */
    public static final String CONFIGURATION_ENDPONT = "api/config/";

    public static final String MIGRATION_ENDPONT = "api/migration/";
    /**
     * Application specific settings for spring application
     */
    public static final String APP_CONFIGURATION = "api/app-config/";
    /**
     * Logging management specific api
     */
    public static final String LOGGING_MANAGEMENT = "management";
    public static final String AADHAAR_LOG = "aadhaar-log";
    /**
     * dictionary related endpoint
     */
    public static final String METADATA = "/api/metadata";
    public static final String METADATA_DICTIONARY = "dict";
    public static final String METADATA_DICTIONARY_PUT = "dict/put";
    public static final String METADATA_DICTIONARY_PATCH = "dict/patch";
    public static final String METADATA_DICTIONARY_DELETE = "dict/delete";
    public static final String UPDATE_REFERENCE_DETAILS = "update-applicant-reference-details";
    public static final String ENUMS = "/enums";
    public static final String UPDATE_BANKING_DETAILS = "update-banking-details";
    public static final String ADD_VALIDATION_VENDOR = "add-validation-vendors";
    public static final String BANKING_MASTER = "banking-master";
    public static final String LOS_BANKING_MASTER = "los-bank-master";
    public static final String GET_ALL_BANK_NAME = "get-all-bank-names";
    public static final String GET_STATE_BY_BANK_NAME = "get-state-by-bank-name";
    public static final String GET_DISTRICT = "get-district";
    public static final String GET_BRANCHES = "get-branches";
    public static final String GET_BANK_DETAILS = "get-bank-details";
    public static final String GET_BANK_DETAILS_BY_IFSC_CODE = "get-bank-details-by-ifsc-code";
    public static final String GET_BANK_DETAILS_BY_MICR_CODE = "get-bank-details-by-micr-code";
    public static final String GET_VALIDATION_VENDOR_CONFIG = "get-validation-vendor-config";
    public static final String DELETE_VALIDATION_VENDOR = "delete-validation-vendor-config";
    public static final String GET_BANK_NAME_FROM_LOS_MASTER = "get-bank-name-from-los-master";
    public static final String GET_ACCOUNT_TYPE="get-account-type";
    public static final String CACHE_MONITOR = "cache-monitor";
    public static final String CACHE_DROP = "cache-drop";
    public static final String CACHE_DROP_BY_KEY = "cache-drop-by-key";

    public static final String EXPOSE_API = "api/ops";
    public static final String UPDATE_OPERATIONS_DATA = "update-operations-data";
    public static final String  UPDATE_REVISED_EMI_AMOUNT = "update-revised-emi-loan-amt";
    public static final String UPDATE_DEALER_NAME_IN_BUCKET = "update-dealer-name-in-bucket";
    public static final String LOS_MB_DATA_PUSH_LOG = "los-mb-data-push-log";
    public static final String SEND_DIGITIZE_MAIL = "send-digitize-mail";
    public static final String ADD_SECONDARY_ATTCH_IN_EMAIL_CONFIG = "add-secondary-attachment-in-email-config";
    public static final String DEL_SECONDARY_ATTCH_IN_EMAIL_CONFIG = "delete-secondary-attachment-in-email-config";
    public static final String UPDATE_GST_DETAILS = "update-gst-details";
    public static final String DAILY_DISBURSAL_REPORT = "get-daily-disbursal";

    /**
     * DMS related endpoints
     */
    public static final String DMS = "dms";
    public static final String PUSH_DOCUMENTS = "push-documents";
    public static final String GET_PUSH_DOCUMENTS_INFO = "get-push-documents-info";
    public static final String GET_ERROR_LOG = "get-error-log";

    // SMS mining endpoints
    public static final String APP_EXISTS = "appExists";
    public static final String SUBMIT_CUSTOMER_FINANCIAL_PROFILE = "submitCustomerFinancialProfile";
    public static final String BUREAU_PDF_REPORT = "bureau-pdf-report";

    // Karza API Connector End points
    public static final String TOTAL_KYC = "total-kyc";
    public static final String TOTAL_KYC_LPG = "lpg";
    public static final String TOTAL_KYC_LPG_V2 = "lpg-v2";
    public static final String TOTAL_KYC_DRIV_LIC = "dl";
    public static final String TOTAL_KYC_DRIV_LIC_V2 = "dl-v2";
    public static final String TOTAL_KYC_ELEC = "elec";
    public static final String TOTAL_KYC_ELEC_V2 = "elec-v2";
    public static final String TOTAL_KYC_VOTER = "voter";
    public static final String TOTAL_KYC_VOTER_V2 = "voter-v2";
    public static final String TOTAL_KYC_GST_AUTH = "gst-auth";
    public static final String TOTAL_KYC_PAN_AUTH = "pan-auth";
    public static final String TOTAL_KYC_GST_IDENTITY = "gst-identity";
    public static final String TOTAL_KYC_VERIFICATION = "kyc-verification";
    public static final String TVR_DATA_PUSH_LOG = "tvr-data-push-log/{refId}";
    public static final String TVR_MANUAL_DATA_PUSH = "tvr-manual-data-push";
    public static final String TKIL_MANUAL_DATA_PUSH = "tkil-manual-data-push";
    public static final String GET_DOCUMENTS_FOR_DMS = "get-documents-for-dms";
    public static final String ADD_LOYALTY_CARD="add-loyalty-card-config";
    public static final String FIND_LOYALTY_CARD_CONFIG = "find-loyalty-card-config";
    public static final String DELETE_LOYALTY_CARD_CONFIG ="delete-loyalty-card-config";
    public static final String UPDATE_LOYALTY_CARD_CONFIG ="update-loyalty-card-config";
    public static final String UPDATE_SMS_TEMPLATE_CONFIG = "update-sms-config-template";
    public static final String KARZA_KYC = "karza-kyc";

    public static final String DISBURSE_LOAN ="disburse-loan";

    /**
     * dms configuration related endpoints
     */
    public static final String ADD_DMS_CONFIG="add-dms-config";
    public static final String UPDATE_DMS_CONFIG="update-dms-config";
    public static final String DELETE_DMS_CONFIG="delete-dms-config";
    public static final String GET_DMS_CONFIG="get-dms-config";
    public static final String FOR_TESTING="for-testing";
    public static final String E_SIGN_BY_E_MUDRA = "esign-by-emudra";
    public static final String E_SIGN_E_MANDATE_BY_E_MUDRA = "esign-emandate-by-emudra";
    public static final String E_MANDATE_BY_E_MUDRA = "e-mandate-by-emudra";
    public static final String SIGNED_XML_REF_ID = "signed-xml-ref-id";
    public static final String E_SIGN_REF_ID = "e-sign-ref-id";
    public static final String POSIDEX_DEDUPE_APPLICATION_DETAILS = "posidex-dedupe-application-details";

    // Quick Data Entry
    public static final String QUICK_DATA_ENTRY_STEPID = "qde/{stepId}";
    public static final String QUICK_DATA_ENTRY_STATUS = "qde-status";

    public static final String GET_DOCUMENT = "get-document";
    public static final String UPLOAD_DOCUMENT = "upload-document";
    public static final String GET_APPLICATION_DOCUMENT = "get-application-document";
    public static final String GET_LOANAPP_DOCUMENT = "get-loanapp-document";

    /**
     * endpoint related EFL File Upload Opearations.
     */
    public static final String SEND_BACK = "send-back";
    public static final String FILE_UPLOAD_HISTORY = "file-upload-history";
    public static final String ADD_CUSTOMER_DETAILS = "add-customer-details";
    public static final String DASHBOARD_DETAIL_EFL = "dashboard-detail-efl";
    public static final String APPLICATION_IMAGES_EFL = "application-images-efl";
    public static final String REMOVE_DOCUMENT = "remove-document";
    public static final String GET_NOTIFICATION = "get-notification";
    public static final String NOTIFICATION = "notification";
    public static final String  SAVE_FCM_TOKEN = "save-fcm-token";

    public static final String INITIALIZE_POST_IPA = "initialise-post-ipa";

    /**
     * endpoint related to the Delivery Order Operation.
     */
    public static final String CANCEL_DELIVERY_ORDER = "cancel-delivery-order";
    public static final String RESTORE_DELIVERY_ORDER = "restore-delivery-order";
    public static final String GET_DELIVERY_ORDER_STATUS = "get-delivery-order-status";
    public static final String GET_DO_OPERATION_LOG= "get-do-operation-log";
    public static final String UPDATE_DEDUPE_PARAMETER = "update-dedupe-parameter";
    public static final String CANCEL_DO_NOT_RAISED_APPLICATION = "cancel-do-not-raised-application";
    public static final String GET_DO_NOT_RAISED_COUNT_FOR_CANCEL = "get-do-not-raised-count-for-cancel";
    public static final String RAW_RESPONSE = "raw-response";
    public static final String IMAGES_METADATA_E_SIGNED_DOCUMENT = "image-metadata-of-signed-document";
    public static final String PROMOTIONAL_SCHEME_MASTER = "promotional-scheme-master";
    public static final String BANK_MASTER = "bank-master";

    public static final String DOWNLOAD_EMANDATE_XML = "download-emandate-xml";
    public static final String GEN_SAP_FILE = "gen-sap-file";

    /**
     * Master configuration for uploading masters
     */
    public static final String ADD_MASTER_MAPPING_CONFIGURATION = "add-master-mapping-config";
    public static final String GENERIC_MASTER = "generic-master";
    public static final String GET_INSURANCE_PREMIUM = "get-insurance-premium";


    public static final String UPDATE_REVISED_AMOUNT_ON_ADDON = "update-revised-amount-on-addon";

    public static final String GET_LOS_CHARGE_CONFIG = "get-los-charge-config";
    public static final String CREATE_LOS_CHARGE_CONFIG = "create-los-charge-config";

    public static final String CHECK_PINCODE_GEO_LIMIT="check-pincode-geo-limit";

    //case cancellation job configuration

    public static final String ADD_CASE_CANCEL_CONFIG = "add-case-cancel-config";
    public static final String DELETE_CASE_CANCEL_CONFIG ="delete-case-cancel-config";
    public static final String GET_CASE_CANCEL_CONFIG ="get-case-cancel-config";

    // loyalty card related endpoint

    public static final String GET_LOYALTY_CARD_PROVIDERS ="get-loyalty-card-providers";

    // imps related endpoint

    public static final String PAYNIMO_BANK_MASTER ="paynimo-bank-master";


    /**
     * Service Code Registration related EndPoint
     */
    public static final String SERVICE_CODE_BASE_ENPOINT = "api/manufacturer/service-code-registration";
    public static final String GET_HELP_DESK_NUMBER = "get-help-desk-number";
    public static final String GET_UPDATED_SERVICE_REQUEST_NO_DETAILS = "get-updated-service-request-number-details";
    public static final String GET_SERVICE_REQUEST_NO_DETAILS = "get-service-request-number-details";


    public static final String DIGITIZATION_DEALER_EMAIL_MASTER = "digitization-dealer-email-master";
    public static final String DEVIATION_MASTER = "deviation-master";
    public static final String ORGANISATION_HIERARCHY_MASTER = "organisation-hierarchy-master";
    public static final String UPLOAD_POTENTIAL_CUSTOMER = "upload-potential-customer";
    /**
     * IMPS Controller related EndPoint Referer
     */
    public static final String IMPS = "imps/";
    public static final String VALIDATE_ACCOUNT_NUMBER = "validate-account-number";
    public static final String IMPS_CONFIG = "imps-config";
    public static final String DELETE_IMPS_CONFIG = "delete-imps-config";
    public static final String ACCOUNT_NUMBER_LOG_ZIP_REPORT = "account-number-log-zip-report";
    public static final String GET_ACCOUNT_VALIDATION_ATTEMPT = "get-account-validation-attempt";
    public static final String GET_VALIDATED_BANKS = "get-validated-banks";
    public static final String IS_ACCOUNT_NUMBER_VALIDATION_APPLICABLE = "is-account-no-validation-applicable";
    public static final String UPDATE_BANKING_DETAILS_IMPS = "update-banking-details-imps";


    /**
     * SerialNumberApplicable log EndPoint Referer
     */
    public static final String SERIAL_NUMBER_APPLICABLE_LOG_ZIP_REPORT = "serial-number-applicable-log-zip-report";
    public static final String SAVE_SERIAL_NUMBER_APPLICABLE_RESPONSE = "save-serial-number-applicable-response";

    public static final String VERIFY_KYC = "kyc/{kycName}";
    public static final String PAN = "pan";
    public static final String AADHAAR = "aadhaar";

    public static final String VERIFY_EMAIL = "verify-email";

    public static final String CALL_MB = "call-mb";
    public static final String CALL_SOBRE = "call-sobre";
    public static final String CALL_SOBRE_FOR_DMS = "call-sobre-for-dms";

    public static final String SAVE_DEVIATION = "save-deviation";
    public static final String APPROVE_ALL_DEVIATION = "approve-all-deviation";
    public static final String GET_DEVIATION = "get-deviation";

    public static final String SAVE_DEDUPE_MATCH = "save-dedupe-match";
    public static final String GET_DEDUPE_INFO = "get-dedupe-info";
    public static final String VERIFY_APPROVAL= "verify-approval";
    public static final String VERIFY_STAGE_SUBMIT= "verify-stage-submit";

    public static final String VERIFY_MIFIN_PUSH = "verify-mifin-push";
    /**
     * Application configurationController related end points - Start
     */
    public static final String SYNCH_USERS= "synch-users";
    public static final String SYNCH_USER= "synch-user";
    public static final String GET_USERS_HIERARCHY= "get-users-hierarchy";
    public static final String GET_INSTITUTION_CONFIG= "get-institution-config";
    public static final String GET_INTIMATION_CONFIG = "get-intimation-config";
    public static final String SAVE_MODULE_CONFIG= "save-module-config";
    public static final String SAVE_STAGES_CONFIG= "save-stage-config";
    public static final String SAVE_ROLE_CONFIG= "save-role-config";
    public static final String MODULE_CONFIG= "module-config";
    public static final String DELETE_MODULE_CONFIG= "delete-module-config";
    public static final String SAVE_VALUEMASTER_CONFIG= "save-valuemaster-config";
    public static final String VALUEMASTER_CONFIG= "valuemaster-config";
    public static final String DELETE_VALUEMASTER_CONFIG= "delete-valuemaster-config";
    public static final String INTIMATION_CONFIG= "intimation-config";
    public static final String SAVE_INTIMATION_CONFIG= "save-intimation-config";
    public static final String DELETE_INTIMATION_CONFIG= "delete-intimation-config";
    public static final String SAVE_INTIMATION_GROUP_CONFIG= "save-intimation-group-config";
    public static final String DELETE_INTIMATION_GROUP_CONFIG= "delete-intimation-group-config";
    public static final String GENERATE_INTIMATION_GROUPS= "generate-intimation-groups";
    public static final String CREATE_WORKFLOW = "create-workflow";

    //Esign Enach
    public static final String QUICK_CHECK = "quick-check";
    public static final String CREATE_EMANDATE = "create-emandate";
    public static final String CALLBACK_EMANDATE = "callback-emandate";
    public static final String GET_EMANDATE_STATUS = "get-emandate-status";
    public static final String PROCESS_EMANDATE = "process-emandate";
    public static final String FETCH_EMANDATE_DETAILS = "fetch-emandate-details";
    public static final String RESEND_EMANDATE_DETAILS = "resend-emandate-details";

    // For SBFC-LMS
    public static final String SBFC_LMS = "sbfc-lms";
    public static final String RETRIEVE_CLIENT_BY_IDENTIFIER = "retrieve-client-by-identifier";
    public static final String CLIENT_CREATION = "client-creation";
    public static final String DATA_PUSH = "data-push";
    public static final String CREATE_LOAN_API = "create-loan-api";
    public static final String ADD_MASTER_DATA = "add-master-data";
    public static final String GET_MASTER_DATA = "get-master-data";
    public static final String DELETE_MASTER_DATA = "delete-master-data";
    public static final String SBFC_LMS_LOG_BY_REFID = "get-log-by-refid";
    public static final String SBFC_LMS_LOG_BY_ZIP_REPORT = "sbfc-lms-log-by-zip";
    public static final String APPROVE = "approve";
    public static final String DISBURSE = "disburse";

    /**
     * template service related Endpoints
     */
    public static final String GET_TEMPLATES = "get-templates/{instituteId}";
    public static final String GET_ALL_TEMPLATE = "get-all-template/{instituteId}";
    public static final String CREATE_TEMPLATE = "create-template";
    public static final String SAVE_TEMPLATE = "submit";
    public static final String GENRATE_TEMPLATE = "generate-template";
    public static final String UPDATE_TEMPLATE_INFO = "update-template-info";
    public static final String DELETE_TEMPLATE = "delete-template";
    public static final String INTIMATE_USERS = "intimate-users";
    /**
     * Third party related end points - Start
     *
     *
     */
    public static final String SAVE_APPLICATION_DATA_STEPID = "save-application-data/{stepId}";

    public static final String PAISA_BAZAR = "paisa-bazar";

    public static final String INITIATE_FINFORT = "initiate-finfort";
    public static final String FINFORT_STATUS = "finfort-status";
    public static final String FINFORT_FILELIST = "finfort-filelist";

    /**
     * Api related to Oo_Origination
     */
    public static final String GET_ROI_SCHEME_MASTER_DATA = "get-roi-scheme-master-data";
    public static final String SAVE_ORIGINATION = "save-origination";
    public static final String GET_ORIGINATION = "get-origination";
    public static final String MIGRATE_APP_BRANCHES = "migrate-app-branches";

    // end point related to master creation
    public static final String SAVE_DROP_DOWN_MASTER = "save-drop-down-master";
    public static final String GET_DROP_DOWN_MASTER = "get-drop-down-master";
    public static final String ONE = "One";
    public static final String MULTIPLE = "Multiple";

    /**
     * -End
     */

    /**
     * Migration Endpoints
     */
        public static final String REGISTRATION_INFO = "registration-info";
        public static final String SOURCING_CHANNEL = "sourcing-channel";
        public static final String BRANCH_MANAGER_NAME = "branch-manager-name";
    /**
     * Migration endpoints end
     */

    //3rd  topup api
    public static final String MANUAL_CALL_CREDITVIDYA = "manual-credit-vidya/{refId}/{instId}";

    // Mifin Controller endpoint
    public static final String MIFIN = "mifin";
    //1st api Applicantbasic info
    public static final String INITIATE_MIFIN_DEDUPE ="initiate-mifin-dedupe";
    public static final String FETCH_APPLICANT_DETAILS="dedupe-details";
    public static final String GET_MIFIN_LOANDETAIL="get-mifin-loandetail";
    //2nd api MIFIN dedupe detail
    public static final String MIFIN_DEDUPE ="mifin-dedupe";
    public static final String MIFIN_DEDUPE2 ="mifin-dedupe2";
    public  static  final String FETCH_MIFIN_DEDUPE_DETAIL="fetch-mifin-dedupe-detail";
    //3rd api
    public static final String  MIFIN_LOAN_DETAIL="mifin-loandetail";
    public static final String  FETCH_MIFIN_LOAN_DETAIL="fetch-mifin-loan-detail";

    public static final String GET_MIFIN_LMS_DATA = "get-mifin-lms-data";

    public static final String AGGREGATOR = "aggregator";
    public static final String MIFIN_EMI_CALCULATOR = "mifin-emi-calculator";
    public static final String DOWNLOAD_EXCEL = "download-excel";
    public static final String DSAID_DECISION = "dsaId-decision" ;
    public static final String COUNT = "count";
    public static final String ALLOCATEINFO_CHANGE = "allocate-Info-change";
    public static final String CAM_DETAIL_UPDATE = "cam-detail-update";

    public static final String CALCULATE_PREMIUM_AMOUNT= "calculate-premium-amount";
    public static final String SAVE_INSURANCE_DATA = "save-insurance-data";
    public static final String GET_INSURANCE_DATA= "get-insurance-data";
    public static final String INSURANCE_DATA_PUSH= "insurance-data-push";




    //For production support issue
    public static final String UPDATE_API = "update-api/{stepId}";
    public static final String  BRANCH_UPDATE = "branch_update";
    public static final String UPDATE_DISBURSAL_DATA = "update-disbursal-data";
    public static final String   ENCRYPT_PASSWORD = "encrpted_password";
    public static final String   ALLOCATIONDATA = "allocation_data";
    public static final String  IFSC_CODE  = "ifsc-code";
    public static final String  PINCODE_UPADATE_CODE  = "pinCode-update-code";
    public static final String DELETE_PINCODE = "delete-pinCode";
    public static final String DELETE_IFSC= "delete-ifsc";
    public static final String COMPANY_MASTER_UPDATE = "company-master-update";
    public static final String COMPANY_MASTER_DELETE = "company-master-delete";
    public static final String BANK_MASTER_UPDATE = "bank-master-update";
    public static final String BANK_MASTER_DELETE = "bank-master-delete";
    public static final String GET_BANK_MASTER = "get-bank-master";

    /**
     * private constructor to check this class should not be instantiated in from outside in any case
     * as it is declared final class
     */
        public static final String CAM_GST_DETAILS = "gst-details";

    public static final String FINBIT_DETAILS = "finbit-details";
    public static final String PUSH_CORPORATE_API_DETAILS = "push-corporate-ap-details";

    public static final String GET_FINBIT_DATA = "get-finbit-data";
    public static final String SAVE_FINBIT_DATA = "save-finbit-data";

    public  static final String FINBIT_CALLBACK = "finbit-callback";

    public  static  final String FINBIT_MONTHLY_SUMMARY = "finbit-monthly-summary";

    public static final String SEND_MAIL = "send-mail";

    public static final String KARZA_API_VERIFICATION = "karza-api";

    public static final String POPULATE_REPAYMENT = "populate-repayment";

    public static final String BUREAU_HIT_COUNT = "bureau-hit-count";

    ///DIGIPL
    public static final String SAVE_LOGIN_DATA = "save-login-data";
    public static final String STEP_PERSONAL_DETAILS = "personal-details";
    public static final String STEP_EMPLOYMENT_DETAILS = "employment-details";

    public static final String SAVE_DIGIPL_ELIGIBILITY_DETAILS = "save-digipl-eligibility-details";
    public static final String GET_DIGIPL_ELIGIBILITY_DETAILS = "get-digipl-eligibility-details";
    public static final String SAVE_OCR_DATA = "save-ocr-data";
    public static final String SUBMIT_OCR_DATA = "submit-ocr-data";
    public static final String RESET_OCR_DATA = "reset-ocr-data";
    public static final String GET_OCR_DATA = "get-ocr-data";
    public static final String RESET_APPLICATION_STATUS = "reset-application-status";
    public static final String PERSIST_REPAYMENT_DETAILS = "persist-repayment";
    public static final String SAVE_MISMATCH_DETAILS = "save-mismatchList-details";
    public static final String GET_MISMATCH_DETAILS = "get-mismatchList-details";
    public static final String UPDATE_KYC_DETAILS = "update-kyc-details";
    public static final String GET_KYC_DETAILS = "get-kyc-details";
    public static final String GET_APPLICATION_BUCKET = "get-application-bucket";
    public static final String GET_CRM_DEDUPE = "get-crm-dedupe";

    public static final String MCP_SAVE_APPLICATION_DATA = "mcp-save-application/{stepId}";
    public static final String MCP_SAVE = "mcp-save";
    public static final String SAVE_LOAN_CHARGES_AND_REPAYMENT_DETAILS = "save-loan-charges-and-repayment";
    public static final String GET_SCORING_LOG = "get-scoring-log";
    public static final String GET_SCORINGCALL_LOG = "get-scoringcall-log";
    public static final String GET_SCORING_DETAILS = "get-scoring-details";
    public static final String GENERATE_FILE_PERFIOS = "generate-file";
    public static final String EVALUATE_FROM_SOBRE = "evaluate-sobre";
    public static final String SAVE_FACEMATCH_DATA = "save-facematch-data";


    public static final String PINCODE_MASTER_DETAILS = "pincode-master-details";
    public static final String EMPLOYER_MASTER_DETAILS = "employer-master-details";
    public static final String CITY_MASTER_DETAILS = "city-master-details";
    public static final String PAN_SEARCH = "pan-search";
    public static final String MIFIN_GONOGO_LOGS = "mifin-Ambit-Gonogo-Logs";
    public static final String UPDATE_PROCESS_DEDUPE="update-process-dedupe";
    public static final String SAVE_FINANCIAL_AND_FETCH_DEDUPE = "save-financial-fetch-dedupe";

    /**
     * Collection config related End points
     */
    public static final String GET_COLLECTION_DATA = "get-collection-data";
    public static final String UPDATE_COLLECTION_DATA = "update-collection-data";
    public static final String SAVE_COLLECTION_CONFIG = "save-collection-config";
    public static final String GET_COLLECTION_CONFIG = "get-collection-config";
    public static final String DOWNLOAD_JSON_DATA = "download-json-data";

    /**
     * End-Point for FOS (can add remark for SBFC)
     */
    public static final String FOS_REMARK = "fos-add-remark";

    /**
     * download pl,bl,lap different report for sbfc
     */
    public static final String GONOGO_REPORTS  = "gonogo-reports";
    public static final String DOWNLOAD_REPORTS  = "download-reports";
    public static final String GET_ALL_REPORTS_NAME  = "get-all-reports-name";

    public static final String VERIFY_OTP = "verify-otp";

    /**
     * master restriction Configuration Api
     */

    public static final String ADD_MASTER_SCHEDULAR_CONFIG = "add-master-scheduler-config";
    public static final String GET_MASTER_SCHEDULAR_CONFIG = "get-master-scheduler-config";

    public static final String API_ROLE_AUTHORISATION_MASTER = "api-role-authorisation-master";


    private EndPointReferrer() {
        // TODO Auto_generated constructor stub
    }

}
