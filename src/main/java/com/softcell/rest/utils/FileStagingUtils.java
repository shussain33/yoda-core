package com.softcell.rest.utils;

import com.softcell.constants.FieldSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by prateek on 2/2/17.
 */
@Component
public class FileStagingUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileStagingUtils.class);

    private static String fileUploadDirectory ;

    @Value("${gonogo.tmp.dir}")
    public void setFileUploadDirectory(String fileUploadDirectory ){
        this.fileUploadDirectory = fileUploadDirectory;
    }

    public static File saveFileToStagingDirectory(MultipartFile file) {

        String newFilenameBase = UUID.randomUUID().toString();

        String originalFileExtension = file.getOriginalFilename().substring(
                file.getOriginalFilename().lastIndexOf(FieldSeparator.DOT)
        );

        String newFilename = newFilenameBase + originalFileExtension;

        String storageDirectory = fileUploadDirectory;

        File newFile = new File(storageDirectory + FieldSeparator.FORWARD_SLASH + newFilename);

        try {

            if(!newFile.exists())
            {
                File parentFile = newFile.getParentFile();
                if(!parentFile.isDirectory()) {
                    parentFile.mkdirs();
                }
            }
            logger.debug("Uploading {} ", file);

            file.transferTo(newFile);

        } catch (IOException e) {
            logger.error("Exception Occurred while transferring file {} to staging area {}",
                    storageDirectory + FieldSeparator.FORWARD_SLASH + newFilename, newFile.getParentFile());
            logger.error("{}",e.getStackTrace());
        }

        return newFile;

    }

    public static String getFileUploadDirectory() {
        return fileUploadDirectory;
    }
}
