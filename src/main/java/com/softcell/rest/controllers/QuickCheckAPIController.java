package com.softcell.rest.controllers;

import com.softcell.gonogo.model.quickcheck.EMandateCallBackRequest;
import com.softcell.gonogo.model.quickcheck.QuickCheckAPIRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.QuickCheckAPIManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * Created by ssg0302 on 27/8/19.
 */
@RestController
@RequestMapping(value = EndPointReferrer.QUICK_CHECK,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class QuickCheckAPIController {

    private final static Logger logger = LoggerFactory.getLogger(QuickCheckAPIController.class);

    @Autowired
    QuickCheckAPIManager quickCheckAPIManager;

    @PostMapping(EndPointReferrer.CREATE_EMANDATE)
    public ResponseEntity<BaseResponse> createEMandate(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.CREATE_EMANDATE);

        return new ResponseEntity<>(quickCheckAPIManager.createEMandate(quickCheckAPIRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PROCESS_EMANDATE)
    public ResponseEntity<BaseResponse> processEMandate(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.PROCESS_EMANDATE);

        return new ResponseEntity<>(quickCheckAPIManager.processEMandate(quickCheckAPIRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CALLBACK_EMANDATE)
    public ResponseEntity<BaseResponse> callBackEMandate(
            @RequestBody @NotNull EMandateCallBackRequest eMandateCallBackRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.CALLBACK_EMANDATE);

        return new ResponseEntity<>(quickCheckAPIManager.callBackEMandate(eMandateCallBackRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_EMANDATE_STATUS)
    public ResponseEntity<BaseResponse> getEMandateStatus(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.GET_EMANDATE_STATUS);

        return new ResponseEntity<>(quickCheckAPIManager.getEMandateStatus(quickCheckAPIRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FETCH_EMANDATE_DETAILS)
    public ResponseEntity<BaseResponse> fetchEmandateDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.FETCH_EMANDATE_DETAILS);

        return new ResponseEntity<>(quickCheckAPIManager.fetchEMandateDetails(quickCheckAPIRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESEND_EMANDATE_DETAILS)
    public ResponseEntity<BaseResponse> resendEmandateDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.RESEND_EMANDATE_DETAILS);

        return new ResponseEntity<>(quickCheckAPIManager.resendLink(quickCheckAPIRequest), HttpStatus.OK);
    }

}
