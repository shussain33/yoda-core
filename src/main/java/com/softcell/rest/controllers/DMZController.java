package com.softcell.rest.controllers;


import com.softcell.constants.ActionName;
import com.softcell.dao.mongodb.repository.DMZRepository;
import com.softcell.gonogo.model.los.LosLogging;
import com.softcell.gonogo.model.reliance.RelianceDigitalLogging;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.dmz.DmzServiceCheckRequest;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DMZConnector;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * This class is specific to reliance related task.
 * It will help
 *
 * @author bhuvneshk
 */

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class DMZController {

    private static final Logger logger = LoggerFactory.getLogger(DMZController.class);

    @Autowired
    private DMZConnector dmzconnector;

    @Autowired
    private DMZRepository dmzRepository;

    @Autowired
    private LookupService lookupService;

    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     * @Depricated This service is depricated as other dealer is pushing DO too. like tkil , reliance etc
     * Please refer connectDealer method which can .
     */
    @PostMapping(value = EndPointReferrer.CONNECT_RELIANCE)
    public ResponseEntity<BaseResponse> connectReliance(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.CONNECT_RELIANCE);

        return new ResponseEntity<>(
                dmzconnector.connectRelianceDigitalService(dmzRequest),
                HttpStatus.OK);
    }


    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.CONNECT_LOS_GNG)
    public ResponseEntity<BaseResponse> connectLosGNG(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CONNECT_LOS_GNG);

        return new ResponseEntity<>(
                dmzconnector.postGNGDataToLos(dmzRequest),
                HttpStatus.OK);
    }

    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.CONNECT_LOS_MB)
    public ResponseEntity<BaseResponse> connectLosMB(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CONNECT_LOS_MB);

        return new ResponseEntity<>(
                dmzconnector.postMBDataToLos(dmzRequest),
                HttpStatus.OK);
    }

    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.LOS_RESPONSE)
    public ResponseEntity<List<LosLogging>> getLosResponseData(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.LOS_RESPONSE);

        return new ResponseEntity<>(
                dmzRepository.getLosLogData(dmzRequest.getGonogoRefId()),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.CHECK_SERVICE_STATUS)
    public ResponseEntity<BaseResponse> checkServiceStatus(
            @Validated(value = {Header.FetchGrp.class, DmzServiceCheckRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzServiceCheckRequest dmzServiceCheckReq) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CHECK_SERVICE_STATUS);

        return new ResponseEntity<>(
                dmzconnector.getServiceStatus(dmzServiceCheckReq),
                HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.RELIANCE_RESPONSE)
    public ResponseEntity<List<RelianceDigitalLogging>> getRelianceResponseData(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RELIANCE_RESPONSE);

        return new ResponseEntity<>(
                dmzRepository.getRelianceLogData(dmzRequest.getGonogoRefId()),
                HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.TVR_MANUAL_DATA_PUSH)
    public ResponseEntity<BaseResponse> pushTvr(
            @Validated(value = {Header.InstWithProductGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RELIANCE_RESPONSE);

        boolean pushStatus = false;

        if (lookupService.checkActionsAccess(dmzRequest.getHeader().getInstitutionId(),
                dmzRequest.getHeader().getProduct().toProductId(),
                ActionName.TVR_DATA_PUSH)) {

            pushStatus = dmzconnector.pushTvr(dmzRequest.getGonogoRefId(), dmzRequest.getHeader().getInstitutionId(), null);

            return new ResponseEntity<>(
                    GngUtils.getBaseResponse(HttpStatus.OK, pushStatus),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    GngUtils.getBaseResponse(HttpStatus.FORBIDDEN, pushStatus),
                    HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.CONNECT_DEALER)
    public ResponseEntity<BaseResponse> connectDealer(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class, DmzRequest.ConnectDealer.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.CONNECT_DEALER);

        return new ResponseEntity<>(
                dmzconnector.connectDealer(dmzRequest),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.TKIL_LOG)
    public ResponseEntity<BaseResponse> getTkilLogData(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.TKIL_LOG);

        return new ResponseEntity<>(
                dmzconnector.getTkilLogs(dmzRequest.getGonogoRefId()),
                HttpStatus.OK);

    }
}
