package com.softcell.rest.controllers;

import com.softcell.gonogo.model.lms.*;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.impl.LMSIntegrationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kumar on 20/7/18.
 */
@RestController
@RequestMapping(value = EndPointReferrer.SBFC_LMS,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class SBFCLMSController {

    private final static Logger logger = LoggerFactory.getLogger(SBFCLMSController.class);

    @Autowired
    LMSIntegrationManager lmsIntegrationManager;

    @PostMapping(value = EndPointReferrer.RETRIEVE_CLIENT_BY_IDENTIFIER)
    public ResponseEntity<RetrieveClientResponse> retrieveClientByIdentifier(
            @RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS retrieve-client-by-identifier");

        return new ResponseEntity(lmsIntegrationManager.retrieveClientByIdentifier(sbfclmsIntegrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.CLIENT_CREATION)
    public ResponseEntity<ClientCreationResponse> clientCreation(
            @RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS client-creation");

        return new ResponseEntity(lmsIntegrationManager.clientCreation(sbfclmsIntegrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.CREATE_LOAN_API)
    public ResponseEntity<CreateLoanResponse> createLoanApi(@RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS create-loan-api");

        return new ResponseEntity(lmsIntegrationManager.createLoanApi(sbfclmsIntegrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DATA_PUSH)
    public ResponseEntity<DisburseResponse> dataPush(
            @RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS API");

        return new ResponseEntity(lmsIntegrationManager.dataPush(sbfclmsIntegrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_MASTER_DATA)
    public ResponseEntity addMasterData(
            @RequestBody String requestJson) {
        logger.info("Service started for Insert SBFC-LMS Master Data");

        return new ResponseEntity(lmsIntegrationManager.addMasterData(requestJson), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_MASTER_DATA)
    public ResponseEntity getLendTraMasterData() {

        logger.debug("Service started for Get SBFC-LMS Master Data");

        return new ResponseEntity(lmsIntegrationManager.getMasterData(), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_MASTER_DATA + "/{institutionId}")
    public ResponseEntity getMasterDataByInstitutionId(@PathVariable(value = "institutionId") String institutionId) throws Exception{
        logger.debug("Service started for get master data by institutionId");

        return new ResponseEntity(lmsIntegrationManager.getMasterDataByInstitutionId(institutionId), HttpStatus.OK);
    }


    @PostMapping(value = EndPointReferrer.DELETE_MASTER_DATA)
    public void deleteMasterData() {

        logger.debug("Service started for Deleting SBFC-LMS Master Data");

        lmsIntegrationManager.deleteMasterData();
    }

    @PostMapping(value = EndPointReferrer.APPROVE)
    public ResponseEntity<ApproveResponse> approve(@RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS approve");

        return new ResponseEntity(lmsIntegrationManager.approve(sbfclmsIntegrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DISBURSE)
    public ResponseEntity<DisburseResponse> disburse(
            @RequestBody SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {
        logger.info("Service started for SBFC-LMS API");

        return new ResponseEntity(lmsIntegrationManager.disburse(sbfclmsIntegrationRequest), HttpStatus.OK);
    }
}