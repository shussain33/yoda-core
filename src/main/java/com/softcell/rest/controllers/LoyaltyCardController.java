package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.loyaltycard.LoyaltyCardStatusRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.LoyaltyCardManager;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 12/12/17.
 */
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
@RestController
public class LoyaltyCardController {


    private static final Logger logger = LoggerFactory.getLogger(LoyaltyCardController.class);

    @Autowired
    private LoyaltyCardManager loyaltyCardManager;

    @PostMapping(EndPointReferrer.CHECK_LOYALTY_CARD_STATUS)
    public ResponseEntity<BaseResponse> checkLoyaltyCardStatus(
            @Validated(value = {LoyaltyCardStatusRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LoyaltyCardStatusRequest loyaltyCardStatusRequest) throws Exception {

        logger.debug("{} controller started with parameter as [{} , {} ]",
                EndPointReferrer.CHECK_LOYALTY_CARD_STATUS,
                loyaltyCardStatusRequest.getHeader().getInstitutionId(),
                loyaltyCardStatusRequest.getLoyaltyCardNo());

        return new ResponseEntity<>(loyaltyCardManager.checkLoyaltyCardStatus(loyaltyCardStatusRequest), HttpStatus.CREATED);

    }

    @PostMapping(EndPointReferrer.UPDATE_LOYALTY_CARD_DETAILS)
    public ResponseEntity<BaseResponse> updateLoyaltyCardDetails(
            @Validated(value = {LoyaltyCardDetails.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LoyaltyCardDetails loyaltyCardDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]", EndPointReferrer.UPDATE_LOYALTY_CARD_DETAILS,
                loyaltyCardDetails.getRefId(),
                loyaltyCardDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(loyaltyCardManager.updateLoyaltyCardDetails(loyaltyCardDetails), HttpStatus.OK);

    }

    @GetMapping(EndPointReferrer.GET_LOYALTY_CARD_DETAILS + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getLoyaltyCardDetails(
            @PathVariable("refId") @NotBlank @Valid String refId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_LOYALTY_CARD_DETAILS,
                refId,
                institutionId);

        return new ResponseEntity<>(loyaltyCardManager.fetchLoyaltyCardDetails(refId, institutionId), HttpStatus.OK);

    }


    @GetMapping(EndPointReferrer.GET_LOYALTY_CARD_PROVIDERS + "/{dealerId}/{institutionId}")
    public ResponseEntity<BaseResponse> getLoyaltyCardProviders(
            @PathVariable("dealerId") @NotBlank @Valid String dealerId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_LOYALTY_CARD_PROVIDERS,
                dealerId, institutionId);

        return new ResponseEntity<>(loyaltyCardManager.getApplicableLoyaltyCardProviders(dealerId, institutionId), HttpStatus.OK);

    }


}
