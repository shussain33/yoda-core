package com.softcell.rest.controllers;

import com.softcell.gonogo.model.mifin.MiFinResponse;
import com.softcell.gonogo.model.core.TopUpDedupeRequest;
import com.softcell.gonogo.model.request.AdminLogRequest;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataEntryManager;
import com.softcell.service.ExternalApiManager;
import com.softcell.service.MifinManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping(
        value = EndPointReferrer.MIFIN,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class MifinController
{

    private static final Logger logger = LoggerFactory.getLogger(MifinController.class);

    @Autowired
    private MifinManager mifinManager;

    @Autowired
    private ExternalApiManager externalApiManager;
    private DataEntryManager dataEntryManager;

//test
    @PostMapping("test-lms-push")
    public MiFinResponse pushDataToMifin(
            @RequestBody @NotNull AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MIFIN);
        return externalApiManager.pushDataToMiFin(adminLogRequest.getRefID());
    }

    //MIFIN 1st basic call
    @PostMapping(EndPointReferrer.INITIATE_MIFIN_DEDUPE)
    public ResponseEntity<BaseResponse> getApplicantInfo(

            @RequestBody @NotNull TopUpDedupeRequest
                    topUpDedupeRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.INITIATE_MIFIN_DEDUPE);

        return new ResponseEntity<>(
                mifinManager.getTopupDedupeResponse(topUpDedupeRequest,httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FETCH_APPLICANT_DETAILS)
    public ResponseEntity<BaseResponse> getMifinApplicantData(
            @RequestBody @NotNull AdminLogRequest adminLogRequest,  HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FETCH_APPLICANT_DETAILS);
        return new ResponseEntity<BaseResponse>(
                mifinManager. getMifinApplicantData(adminLogRequest.getRefID(), httpRequest),HttpStatus.OK);
    }

      //2nd APIMIFIN dedupe detail
    @PostMapping(EndPointReferrer.MIFIN_DEDUPE)
    public ResponseEntity<BaseResponse> sendDatatoMifinDedupe(
            @RequestBody @NotNull TopUpDedupeRequest topUpDedupeRequest ,HttpServletRequest httpRequest) throws Exception {
        logger.debug("MIFIN_DEDUPE {}", topUpDedupeRequest.getRefId());
        return new ResponseEntity<BaseResponse>(
                mifinManager.sendDatatoMifinDedupe(topUpDedupeRequest, httpRequest),
                HttpStatus.OK);
    }

    //2nd APIMIFIN dedupe detail
    @PostMapping(EndPointReferrer.MIFIN_DEDUPE2)
    public ResponseEntity<BaseResponse> getDedupe(
            @RequestBody @NotNull ApplicationRequest applicationRequest , HttpServletRequest httpRequest) throws Exception {
        logger.debug("Rcvd at {}", EndPointReferrer.MIFIN_DEDUPE2);
        return new ResponseEntity<BaseResponse>(
                mifinManager.getDedupeData(applicationRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FETCH_MIFIN_DEDUPE_DETAIL)
    public  ResponseEntity<BaseResponse> getMifinDedupeDetail(
            @RequestBody @NotNull TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) {
        logger.debug("{} controller started", EndPointReferrer.FETCH_MIFIN_DEDUPE_DETAIL);
        return new ResponseEntity<>(mifinManager.getMifinDedupeDetail(topUpDedupeRequest, httpRequest),
                                        HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIFIN_LOAN_DETAIL)
    public ResponseEntity<BaseResponse> sendMiFINLoanDetail(
            @Validated({Header.FetchGrp.class}) String custId, @RequestBody @NotNull TopUpDedupeRequest topUpDedupeRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MIFIN_LOAN_DETAIL);

        return new ResponseEntity<>(
                mifinManager.sendMiFINLoanDetail(topUpDedupeRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_MIFIN_LOANDETAIL)
    public ResponseEntity<BaseResponse> getMiFINLoanDetail(
            @Validated({Header.FetchGrp.class})  @RequestBody @NotNull TopUpDedupeRequest loanDetailRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_MIFIN_LOANDETAIL);

        return new ResponseEntity<>(
                mifinManager.getMiFINLoanDetail(loanDetailRequest, httpRequest),
                HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.GET_MIFIN_LMS_DATA)
    public ResponseEntity<BaseResponse> getMifinLmsDetail(
            @Validated({Header.FetchGrp.class})  @RequestBody @NotNull TopUpDedupeRequest loanDetailRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_MIFIN_LMS_DATA);

        return new ResponseEntity<>(
                mifinManager.getMifinLmsDetail(loanDetailRequest, httpRequest),
                HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.MIFIN_EMI_CALCULATOR)
    public ResponseEntity<BaseResponse> sendEmiCalRequest( @RequestBody @NotNull AdminLogRequest adminLogRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MIFIN_EMI_CALCULATOR);

        return new ResponseEntity<BaseResponse>(
                mifinManager.sendEmiCalRequest(adminLogRequest, httpRequest),
                HttpStatus.OK);
    }

}