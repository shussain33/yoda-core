package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.ChangePasswordRequest;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.LogoutRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.HomeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author yogeshb
 * @category Handles requests for the application home page.
 */

@RestController
@RequestMapping(
        path = EndPointReferrer.HOME_CTRL,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private HomeManager homeManager;

    /**
     * @param loginRequest
     * @param httpRequest
     * @return
     */
    @PostMapping(EndPointReferrer.LOGIN)
    public ResponseEntity<BaseResponse> getLogin(
            @Validated(value = {LoginRequest.FetchGrp.class,Header.AppSourceGrp.class})
            @RequestBody @NotNull @Valid LoginRequest loginRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("login controller started");

        return new ResponseEntity(homeManager.login(loginRequest, httpRequest), HttpStatus.OK);

    }

    /**
     * @param logoutRequest
     * @param httpRequest
     * @return
     */
    @PostMapping(EndPointReferrer.LOGOUT)
    public ResponseEntity<BaseResponse> logout(
            @Validated(value = {LogoutRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LogoutRequest logoutRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("logout controller started ");

        return new ResponseEntity(homeManager.logout(logoutRequest, httpRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.CHANGE_PASS)
    public ResponseEntity<BaseResponse> changePassword(
            @Validated(value = {ChangePasswordRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("changePassword controller started");

        return new ResponseEntity(homeManager.changePassword(chngPassRqst, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESET_PASS)
    public ResponseEntity<BaseResponse> resetPassword(
            @Validated(value = {ChangePasswordRequest.ResetGrp.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("resetPassword controller started");

        return new ResponseEntity(homeManager.resetPassword(chngPassRqst,httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.LOGIN_V2)
    public ResponseEntity<BaseResponse> getLogin_V2(
            @Validated(value = {LoginRequest.FetchGrp.class,Header.AppSourceGrp.class})
            @RequestBody @NotNull @Valid LoginRequest loginRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.LOGIN_V2);

        return new ResponseEntity(homeManager.loginV2(loginRequest, httpRequest), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.CHANGE_PASS_V2)
    public ResponseEntity<BaseResponse> changePasswordV2(
            @Validated(value = {ChangePasswordRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.LOGIN_V2);

        return new ResponseEntity(homeManager.changePasswordV2(chngPassRqst, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESET_PASS_V2)
    public ResponseEntity<BaseResponse> resetPasswordV2(
            @Validated(value = {ChangePasswordRequest.ResetGrp.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.RESET_PASS_V2);

        return new ResponseEntity(homeManager.resetPasswordV2(chngPassRqst,httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.LOGIN_V3)
    public ResponseEntity<BaseResponse> getLogin_V3(
            @Validated(value = {LoginRequest.FetchGrp.class,Header.AppSourceGrp.class})
            @RequestBody @NotNull @Valid LoginRequest loginRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.LOGIN_V3);

        return new ResponseEntity(homeManager.loginV3(loginRequest, httpRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.RESET_PASS_V3)
    public ResponseEntity<BaseResponse> resetPasswordV3(
            @Validated(value = {ChangePasswordRequest.ResetGrpV3.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.RESET_PASS_V3);

        return new ResponseEntity(homeManager.resetPasswordV3(chngPassRqst,httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CHANGE_USER_PASS)
    public ResponseEntity<BaseResponse> changeUserPassword(
            @Validated(value = {ChangePasswordRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("changeUserPassword controller started");

        return new ResponseEntity(homeManager.changeUserPassword(chngPassRqst, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FORGOT_PASS)
    public ResponseEntity<BaseResponse> forgotPassword(
            @RequestBody @NotNull ChangePasswordRequest chngPassRqst,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.FORGOT_PASS);

        return new ResponseEntity(homeManager.forgotPassword(chngPassRqst,httpRequest), HttpStatus.OK);
    }
}
