package com.softcell.rest.controllers;


import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AuditDataManager;
import com.softcell.service.CroManager;
import com.softcell.workflow.AuditDataRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * This Controller is use for handling CRO Screen requests.
 *
 * @author yogeshb
 */

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class CroController {

    private static final Logger logger = LoggerFactory.getLogger(CroController.class);


    @Autowired
    private CroManager croManager;

    @Autowired
    private AuditDataManager auditDataManager;

    /**
     * To get all Application Data for cro screen
     *
     * @param checkApplicationStatus
     * @return
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.APPLICATION_DATA)
    public ResponseEntity<BaseResponse> populatecroScreen2(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus
    ) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_DATA);
        return new ResponseEntity<>(
                croManager.getApplicationData(checkApplicationStatus),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer. APPLICATION_DATA_BY_MOB)
    public ResponseEntity<BaseResponse> populatecroScreen3(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetGngByMobRequest getGngByMobRequest
    ) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_DATA_BY_MOB);
        return new ResponseEntity<>(
                croManager.getApplicationDataByMob(getGngByMobRequest),
                HttpStatus.OK);
    }

    /**
     * To get all Application Data for cro screen
     *
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION_DATA_COAPPLICANT)
    public ResponseEntity<BaseResponse> getCoApplicantApplicationData(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_DATA_COAPPLICANT);

        return new ResponseEntity<>(croManager.getCoApplicationData(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION_IMAGES)
    public ResponseEntity<BaseResponse> getApplicationImageByRefID(

            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_IMAGES);

        return new ResponseEntity<>(croManager.getApplicationImages(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     * @param checkApplicationStatus
     * @return
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.DASHBOARD_APPLICATION_DATA)
    public ResponseEntity<BaseResponse> dashBoard(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DASHBOARD_APPLICATION_DATA);

        return new ResponseEntity<>(croManager.getDashboardData(checkApplicationStatus), HttpStatus.OK);
    }

    /**
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION_DATA_CRO2)
    public ResponseEntity<BaseResponse> populateCro2(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_DATA_CRO2);


        return new ResponseEntity<>(croManager.getApplicationDataForCroTwoScreen(checkApplicationStatus), HttpStatus.OK);
    }

    /**
     * It is used to fetch data from application document which is partially
     * saved
     *
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION_DATA_PARTIAL)
    public ResponseEntity<BaseResponse> getPartialSavedApplicationData(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION_DATA_PARTIAL);

        return new ResponseEntity<>(croManager
                .getPartialSavedApplicationData(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     * Use for Approve or declined or On hold Application
     *
     * @param croApprovalRequest
     * @param httpHeaders
     * @param httpRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO_APPROVAL)
    public ResponseEntity<BaseResponse> croApproval(
            @Validated({CroApprovalRequest.UpdateOrDelete.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroApprovalRequest croApprovalRequest,
            @RequestHeader("User-Agent") HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO_APPROVAL);

        return new ResponseEntity<>(croManager.setCroDecesion(
                croApprovalRequest, httpHeaders, httpRequest), HttpStatus.OK);

    }

    /**
     * @param croApprovalRequest
     * @return
     */
    @PostMapping(EndPointReferrer.RESET_STATUS)
    public ResponseEntity<BaseResponse> resetQueue(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroApprovalRequest croApprovalRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RESET_STATUS);

        return new ResponseEntity<>(croManager.resetStatus(croApprovalRequest), HttpStatus.OK);

    }

    /**
     * @param croApprovalRequest
     * @param httpHeaders
     * @param httpRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO_ONHOLD)
    public ResponseEntity<BaseResponse> croOnHold(
            @Validated({CroApprovalRequest.UpdateOrDelete.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroApprovalRequest croApprovalRequest,
            @RequestHeader("User-Agent") HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO_ONHOLD);

        return new ResponseEntity<>(croManager.setCroDecesionOnHold(
                croApprovalRequest, httpHeaders, httpRequest), HttpStatus.OK);

    }

    /**
     * for Cro1 and CRO9
     *
     * @param croQueueRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO_QUEUE)
    public ResponseEntity<BaseResponse> croQueue(
            @Validated(value = {CroQueueRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO_QUEUE);

        return new ResponseEntity<>(croManager.getCroQueue(croQueueRequest), HttpStatus.OK);

    }

    /**
     * for Cro1 and CRO9 with hierarchy criteria
     *
     * @param croQueueRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO_QUEUE_CRITERIA)
    public ResponseEntity<BaseResponse> croQueueCriteria(
            @Validated(value = {CroQueueRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO_QUEUE_CRITERIA);

        return new ResponseEntity<>(croManager.getCroQueueCriteria(croQueueRequest), HttpStatus.OK);
    }


    /**
     * for Cro2
     *
     * @param croQueueRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO2_QUEUE)
    public ResponseEntity<BaseResponse> cro2BothCases(

            @Validated(value = {CroQueueRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO2_QUEUE);

        return new ResponseEntity<>(croManager.getCroTwoQueue(croQueueRequest), HttpStatus.OK);
    }


    /**
     * for Cro3 queue based on criteria It is used by PL and CCBT It provide
     * records based for PL and CCBT loan type It is sales queue. Data is
     * fetched from response and request table
     *
     * @param croQueueRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO3_QUEUE)
    public ResponseEntity<BaseResponse> cro3Queue(
            @Validated(value = {CroQueueRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO3_QUEUE);

        return new ResponseEntity<>(croManager.getCro3Queue(croQueueRequest), HttpStatus.OK);
    }


    /**
     * for Cro3 queue based on criteria It is used by PL and CCBT It provide
     * records based for PL and CCBT loan type It is leads queue. It provides
     * request records whose are partial filled.
     *
     * @param croQueueRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CRO4_QUEUE)
    public ResponseEntity<BaseResponse> cro4Queue(
            @Validated(value = {CroQueueRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CRO4_QUEUE);

        return new ResponseEntity<>(croManager.getCro4Queue(croQueueRequest), HttpStatus.OK);
    }

    /**
     * To get postIPA details
     *
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.FETCH_POSTIPA)
    public ResponseEntity<BaseResponse> postIpo(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FETCH_POSTIPA);

        return new ResponseEntity<>(croManager.getpostIpo(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     * @param lOSDetailsRequest
     * @return
     */
    @PostMapping(EndPointReferrer.UPDATE_LOS_DETAILS)
    public ResponseEntity<BaseResponse> updateLosDetails(
            @Validated({LOSDetailsRequest.UpdateOrDeleteGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LOSDetailsRequest lOSDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_LOS_DETAILS);

        return new ResponseEntity<>(croManager.updateLosDetails(lOSDetailsRequest), HttpStatus.OK);

    }

    /**
     * This service is use to update the invoice details.
     *
     * @param invoiceDetailsRequest
     * @return
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.UPDATE_INVOICE_DETAILS)
    public ResponseEntity<BaseResponse> updateInvoiceDetails(
            @Validated(value = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid InvoiceDetailsRequest invoiceDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_INVOICE_DETAILS);

        return new ResponseEntity<>(croManager.updateInvoiceDetails(invoiceDetailsRequest),
                HttpStatus.OK);
    }

    /**
     * @param checkApplicationStatus
     * @return list of all the application requests including reappraisal requests.
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.DASHBOARD_APP_DATA)
    public ResponseEntity<BaseResponse> dashBoardApplicationData(
            @Validated({CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DASHBOARD_APP_DATA);


        return new ResponseEntity<>(croManager.getMergeApplicationRequest(checkApplicationStatus), HttpStatus.OK);
    }

    /**
     * @param auditDataRequest request for getting application data of reappraisal
     *                         applications.
     * @return list of GoNoGoCustomerApplication
     */
    @PostMapping(EndPointReferrer.REAPPRAISAL_APPLICATIONS)
    public ResponseEntity<BaseResponse> getReappraisalApplications(
            @Validated({AuditDataRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid AuditDataRequest auditDataRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.REAPPRAISAL_APPLICATIONS);

        return new ResponseEntity<>(auditDataManager
                .getReappraisalApplications(auditDataRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.APPLICATION_STATUS_LOG)
    public ResponseEntity<BaseResponse> getApplicationStatusLog(
            @Validated({ApplicationStatusLogRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception {

        logger.debug("{} controller started  with the referenceId {}", EndPointReferrer.APPLICATION_STATUS_LOG, applicationStatusLogRequest.getRefId());

        return ResponseEntity.status(HttpStatus.OK).body(croManager.getApplicationStatusLog(applicationStatusLogRequest));
    }

    @PostMapping(EndPointReferrer.DEDUPE_APPLICATION_DETAILS)
    public ResponseEntity<BaseResponse> getDedupeApplicationDetails(
            @Validated({ApplicationStatusLogRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception {

        logger.debug("{} controller started  with the referenceId {}", EndPointReferrer.DEDUPE_APPLICATION_DETAILS, applicationStatusLogRequest.getRefId());

        return ResponseEntity.status(HttpStatus.OK).body(croManager.getDedupeApplicationDetails(applicationStatusLogRequest));
    }

    @PostMapping(EndPointReferrer.POSIDEX_DEDUPE_APPLICATION_DETAILS)
    public ResponseEntity<BaseResponse> getPosidexDedupeApplicationDetails(
            @Validated({ApplicationStatusLogRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception {

        logger.debug("{} controller started  with the referenceId {}", EndPointReferrer.POSIDEX_DEDUPE_APPLICATION_DETAILS, applicationStatusLogRequest.getRefId());

        return ResponseEntity.status(HttpStatus.OK).body(croManager.getPosidexDedupeApplicationDetails(applicationStatusLogRequest));
    }


    @PostMapping(EndPointReferrer.UPDATE_TVR_STATUS)
    public ResponseEntity<BaseResponse> setTVRStatus(
            @Validated({UpdateTvrStatusRequest.UpdateOrDelete.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid UpdateTvrStatusRequest updateTvrStatusRequest,
            @RequestHeader("User-Agent") HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_TVR_STATUS);
        return new ResponseEntity<>(croManager.setTVRStatus(updateTvrStatusRequest, httpHeaders, httpRequest), HttpStatus.OK);
    }

    /**
     * To get TVR status
     *
     * @param getTvrStatusRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_TVR_STATUS)
    public ResponseEntity<BaseResponse> getTVRStatus(
            @Validated({GetTvrStatusRequest.GetTvrStatusRequestGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetTvrStatusRequest getTvrStatusRequest,
            @RequestHeader("User-Agent") HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_TVR_STATUS);
        return new ResponseEntity<>(croManager.getTVRStatus(getTvrStatusRequest, httpHeaders, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DOCUMENTS_FOR_DMS)
    public ResponseEntity<BaseResponse> getDocumentsForDms(
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_DOCUMENTS_FOR_DMS);

        return new ResponseEntity<>(croManager.getDocumentsForDmsPush(checkApplicationStatus), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_DEDUPE_PARAMETER)
    public ResponseEntity<BaseResponse> changeApplicationLockStatus(
            @Validated({AdditionalDedupeFieldsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.UPDATE_DEDUPE_PARAMETER);
        return new ResponseEntity<>(croManager.updateDedupeParameter(additionalDedupeFieldsRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.REASSIGN_APPLICATION)
    public ResponseEntity<BaseResponse> reassignApplication(
            @RequestBody @NotNull @Valid ReassignApplicationRequest reassignApplicationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.REASSIGN_APPLICATION);

        return new ResponseEntity<>(
                croManager.reassignApplication(reassignApplicationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESET_ALLOCATION_INFO)
    public ResponseEntity<BaseResponse> resetAllocationInfo(
            @RequestBody @NotNull @Valid ChangeAllocationInfoRequest changeAllocationInfoRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RESET_ALLOCATION_INFO);

        return new ResponseEntity<>(
                croManager.resetAllocationInfo(changeAllocationInfoRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FOS_REMARK)
    public ResponseEntity<BaseResponse> fosAddRemark(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull CroApprovalRequest croApprovalRequest,
            @RequestHeader("User-Agent") HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FOS_REMARK);

        return new ResponseEntity<>(croManager.fosAddRemark(
                croApprovalRequest, httpHeaders, httpRequest), HttpStatus.OK);

    }
}
