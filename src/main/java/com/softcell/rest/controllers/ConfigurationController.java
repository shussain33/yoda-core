package com.softcell.rest.controllers;


import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.sms.GngSmsServiceConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.Constant;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.los.LosChargeConfig;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.VersionRequest;
import com.softcell.gonogo.model.security.Versions;
import com.softcell.gonogo.service.SmsConfigService;
import com.softcell.gonogo.service.metadata.AppMetadataService;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ConfigurationManager;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.CONFIGURATION_ENDPONT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE

)
public class ConfigurationController {


    private Logger logger = LoggerFactory.getLogger(ConfigurationController.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private AppMetadataService appMetadataService;

    @Autowired
    private SmsConfigService smsConfigService;


    /**
     * @param versionRequest
     * @return
     */
    @PostMapping(value = EndPointReferrer.SET_VER)
    public ResponseEntity<GenericResponse> setVersion(
            @Validated(value = {VersionRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull VersionRequest versionRequest) {
        return new ResponseEntity<>(
                configurationManager.setVersion(versionRequest),
                HttpStatus.OK);
    }

    /**
     * @param versionRequest
     * @return
     */
    @RequestMapping(value = EndPointReferrer.RETIRED_VER, method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> removeVersion(
            @Validated(value = {VersionRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull VersionRequest versionRequest) {
        return new ResponseEntity<>(
                configurationManager.removeVersion(versionRequest),
                HttpStatus.OK);
    }

    /**
     * @return
     */
    @GetMapping(value = EndPointReferrer.SHOW_VER)
    public ResponseEntity<List<Versions>> showVersion() {
        return new ResponseEntity<>(
                configurationManager.getVersionConguration(), HttpStatus.OK);
    }

    /**
     * @param versionRequest
     * @return
     */
    @PostMapping(value = EndPointReferrer.IS_VALID_VER)
    public ResponseEntity<GenericResponse> isValidVersion(
            @Validated(value = {VersionRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull VersionRequest versionRequest) {

        GenericResponse genericResponse = new GenericResponse();
        if (configurationManager.isValidVersion(versionRequest.getHeader()
                .getInstitutionId(), versionRequest.getVersion())) {
            genericResponse.setStatus(Constant.SUCCESS);
            return new ResponseEntity<>(genericResponse, HttpStatus.OK);
        } else {
            genericResponse.setStatus(Constant.FAILED);
            return new ResponseEntity<>(genericResponse, HttpStatus.OK);
        }

    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_INSTI_PROD_CONFIG)
    public ResponseEntity<BaseResponse> setInstitutionProductConfiguration(
            @Validated(value = {InstitutionProductConfiguration.InsertGrp.class})
            @RequestBody @Valid @NotNull InstitutionProductConfiguration institutionProductConfiguration) {

        logger.info(" handler for adding product configuration started ");

        return new ResponseEntity<>(
                configurationManager
                        .addInstitutionProductConfig(institutionProductConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.REMOVE_INSTI_PROD_CONFIG)
    public ResponseEntity<BaseResponse> removeInstitutionProductConfiguration(
            @Validated({InstitutionProductConfiguration.DeleteGrp.class})
            @RequestBody @Valid @NotNull InstitutionProductConfiguration institutionProductConfiguration) {

        logger.info("inside remove-institution-product-app");
        return new ResponseEntity<>(
                configurationManager
                        .removeInstitutionProductConfig(institutionProductConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_INSTI_PROD_CONFIG)
    public ResponseEntity<BaseResponse> updateInstitutionProductConfiguration(
            @Validated({InstitutionProductConfiguration.UpdateGrp.class})
            @Valid @RequestBody InstitutionProductConfiguration institutionProductConfiguration) {

        logger.info("inside update-institution-product-app");

        return new ResponseEntity<>(
                configurationManager
                        .updateInstitutionProductConfig(institutionProductConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.FIND_ALL_INSTI_PROD_CONFIG)
    public ResponseEntity<List<InstitutionProductConfiguration>> getInstitutionProductConfiguration(
            @Validated({Header.FetchGrp.class})
            @RequestBody @Valid @NotNull InstitutionProductConfiguration institutionProductConfiguration) {
        logger.info("inside find-all-institution-product-app");
        return new ResponseEntity<>(
                configurationManager
                        .getInstitutionProductConfig(institutionProductConfiguration),
                HttpStatus.OK);
    }


    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_TMPLT_CONFIG)
    public ResponseEntity<BaseResponse> addTemplateConfiguration(
            @Validated(value = {TemplateConfiguration.InsertGrp.class})
            @RequestBody @Valid @NotNull TemplateConfiguration templateConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addTemplateConfiguration(templateConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_TMPLT_PATH)
    public ResponseEntity<BaseResponse> updateTemplatePath(
            @Validated(value = {TemplateConfiguration.UpdateTemplatePathGrp.class})
            @RequestBody @Valid @NotNull TemplateConfiguration templateConfiguration) {
        return new ResponseEntity<>(
                configurationManager.updateTemplatePath(templateConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_TMPLT_LOGO)
    public ResponseEntity<BaseResponse> updateTemplateLogo(
            @Validated(value = {TemplateConfiguration.UpdateTemplateLogoGrp.class})
            @RequestBody @Valid @NotNull TemplateConfiguration templateConfiguration) {
        return new ResponseEntity<>(
                configurationManager.updateTemplateLogo(templateConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ENABLE_TMPLT)
    public ResponseEntity<BaseResponse> enableTemplate(
            @Validated(value = {TemplateConfiguration.FetchGrp.class})
            @RequestBody @Valid @NotNull TemplateConfiguration templateConfiguration) {

        return new ResponseEntity<>(
                configurationManager.enableTemplate(templateConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DISABLE_TMPLT)
    public ResponseEntity<BaseResponse> disableTemplate(
            @Validated(value = {TemplateConfiguration.FetchGrp.class})
            @RequestBody @Valid @NotNull TemplateConfiguration templateConfiguration) {

        return new ResponseEntity<>(
                configurationManager.disableTemplate(templateConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param templateConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.FIND_ALL_TMPLT_CONFIG)
    public ResponseEntity<BaseResponse> getTemplateConfiguration(
            @Validated(value = {TemplateConfiguration.InstitutionProductGrp.class})
            @RequestBody TemplateConfiguration templateConfiguration) {

        return new ResponseEntity<>(
                configurationManager.getTemplateConfiguration(templateConfiguration),
                HttpStatus.OK);

    }


    /**
     * @param actionConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_ACTION_CONFIG)
    public ResponseEntity<BaseResponse> addActionConfiguration(
            @RequestBody ActionConfiguration actionConfiguration) {

        return new ResponseEntity<>(
                appMetadataService.insertActionConfiguration(actionConfiguration),
                HttpStatus.OK);
    }



    /**
     * @param actionConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_ACTION_CONFIG)
    public ResponseEntity<BaseResponse> updateActionConfiguration(
            @RequestBody ActionConfiguration actionConfiguration) {
        return new ResponseEntity<>(
                appMetadataService.updateActionConfiguration(actionConfiguration),
                HttpStatus.OK);
    }


    /**
     * @param actionConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DEL_ACTION_CONFIG)
    public ResponseEntity<BaseResponse> deleteActionConfiguration(
            @RequestBody ActionConfiguration actionConfiguration) {

        return new ResponseEntity<>(
                appMetadataService.deleteActionConfiguration(actionConfiguration),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.FIND_ALL_ACTION_CONFIG+"/{institutionId}/{productId}/{active}")
    public ResponseEntity<BaseResponse> findAllActionConfiguration(
            @PathVariable("institutionId") @Valid @NotEmpty String institutionId,
            @PathVariable("productId") @Valid @NotEmpty String productId ,
            @PathVariable("active") @Valid @NotEmpty boolean active) {

        return new ResponseEntity<>(
                appMetadataService.findAllActionConfiguration(institutionId, productId, active),
                HttpStatus.OK);
    }


    /**
     * @param smsConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_SMS_CONFIG)
    public ResponseEntity<List<BaseResponse>> addSmsConfiguration(
            @RequestBody List<GngSmsServiceConfiguration> smsConfiguration) {

        return new ResponseEntity<>(
                smsConfigService.insertSmsConfiguration(smsConfiguration),
                HttpStatus.OK);

    }

    /**
     * Email app services.
     */
    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> addEmailConfiguration(
            @Validated(value = {EmailConfiguration.InsertGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> updateEmailConfiguration(
            @Validated(value = {EmailConfiguration.UpdateGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {

        return new ResponseEntity<>(
                configurationManager.updateEmailConfiguration(emailConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_ATTCH_IN_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> addAttachmentsEmailConfiguration(
            @Validated(value = {EmailConfiguration.UpdateAttachmentGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addAttachmentsInEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }

    /**
     *
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ADD_SECONDARY_ATTCH_IN_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> addSecondaryAttachmentsEmailConfiguration(
            @Validated(value = {EmailConfiguration.UpdateSecondaryAttachmentGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addSecondaryAttachmentsInEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }


    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DEL_ATTCH_IN_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> deleteAttachmentsEmailConfiguration(
            @Validated(value = {EmailConfiguration.UpdateAttachmentGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.deleteAttachmentsInEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DEL_SECONDARY_ATTCH_IN_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> deleteSecondaryAttachmentsEmailConfiguration(
            @Validated(value = {EmailConfiguration.UpdateSecondaryAttachmentGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.deleteSecondaryAttachmentsInEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }



    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DISABLE_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> disableEmailConfiguration(
            @Validated(value = {EmailConfiguration.FetchGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {

        return new ResponseEntity<>(
                configurationManager.disableEmailConfiguration(emailConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.ENABLE_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> enableEmailConfiguration(
            @Validated(value = {EmailConfiguration.FetchGrp.class})
            @RequestBody @Valid @NotNull EmailConfiguration emailConfiguration) {

        return new ResponseEntity<>(
                configurationManager.enableEmailConfiguration(emailConfiguration),
                HttpStatus.OK);

    }

    /**
     * @param emailConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.FIND_ALL_EMAIL_CONFIG)
    public ResponseEntity<BaseResponse> getAllEmailConfiguration(
            @Validated(value = {EmailConfiguration.InstitutionGrp.class})
            @RequestBody EmailConfiguration emailConfiguration) {
        return new ResponseEntity<>(
                configurationManager.getAllEmailConfiguration(emailConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param smsConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.UPDT_SMS_CONFIG)
    public ResponseEntity<List<BaseResponse>> updateSmsConfiguration(
            @RequestBody List<GngSmsServiceConfiguration> smsConfiguration) {

        return new ResponseEntity<>(
                smsConfigService.updateSmsConfiguration(smsConfiguration),
                HttpStatus.OK);
    }


    /**
     * @param smsConfiguration
     * @return
     */
    @PostMapping(value = EndPointReferrer.DEL_SMS_CONFIG)
    public ResponseEntity<List<BaseResponse>> deleteSmsConfiguration(
            @RequestBody List<GngSmsServiceConfiguration> smsConfiguration) {
        return new ResponseEntity<>(
                smsConfigService.deleteSmsConfiguration(smsConfiguration),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_VALIDATION_VENDOR)
    public ResponseEntity<BaseResponse> addValidationVendors(
            @RequestBody ValidationVendorsConfiguration validationVendorsConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addValidationVendors(validationVendorsConfiguration),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_VALIDATION_VENDOR)
    public ResponseEntity<BaseResponse> deleteValidationVendors(
            @RequestBody ValidationVendorsConfiguration validationVendorsConfiguration) {
        return new ResponseEntity<>(
                configurationManager.deleteValidationVendors(validationVendorsConfiguration),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_VALIDATION_VENDOR_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> getValidationVendors(
            @Valid @NotEmpty @PathVariable String institutionId) {
        return new ResponseEntity<>(
                configurationManager.getValidationVendors(institutionId),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.CACHE_MONITOR)
    public ResponseEntity<BaseResponse> getCacheData() {
        return new ResponseEntity<>(
                configurationManager.getCacheData(),
                HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.CACHE_DROP)
    public ResponseEntity<BaseResponse> dropCache() {
        return new ResponseEntity<>(
                configurationManager.dropCache(),
                HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.CACHE_DROP_BY_KEY+ "/{key}")
    public ResponseEntity<BaseResponse> dropCache(
            @Valid @NotEmpty @PathVariable String key) {
        return new ResponseEntity<>(
                configurationManager.dropCache(key),
                HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.ADD_LOYALTY_CARD)
    public ResponseEntity<BaseResponse> addLoyaltyCardConfig(
            @Validated(value = {LoyaltyCardConfiguration.AddGrp.class})
            @RequestBody @Valid @NotNull LoyaltyCardConfiguration loyaltyCardConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addLoyaltyCardConfig(loyaltyCardConfiguration),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.FIND_LOYALTY_CARD_CONFIG + "/{institutionId}/{productId}")
    public ResponseEntity<BaseResponse> findLoyaltyCardConfig(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String productId) {
        return new ResponseEntity<>(
                configurationManager.findLoyaltyCardConfig(institutionId, productId),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_LOYALTY_CARD_CONFIG + "/{institutionId}/{productId}/{loyaltyCardType}")
    public ResponseEntity<BaseResponse> deleteLoyaltyCardConfig(
            @PathVariable("institutionId") @Valid @NotEmpty String institutionId,
            @PathVariable("productId") @Valid @NotEmpty String productId,
            @PathVariable("loyaltyCardType") @Valid @NotEmpty LoyaltyCardType loyaltyCardType) {
        return new ResponseEntity<>(
                configurationManager.deleteLoyaltyCardConfig(institutionId, productId,loyaltyCardType),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.UPDATE_LOYALTY_CARD_CONFIG)
    public ResponseEntity<BaseResponse> updateLoyaltyCardConfig(
            @Validated(value = {LoyaltyCardConfiguration.UpdateGrp.class})
            @RequestBody @Valid @NotNull LoyaltyCardConfiguration loyaltyCardConfiguration) {
        return new ResponseEntity<>(
                configurationManager.updateLoyaltyCardConfig(loyaltyCardConfiguration),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_SMS_TEMPLATE_CONFIG)
    public ResponseEntity<BaseResponse> addSmsTemplateConfig(
            @Validated(value = {SmsTemplateConfiguration.InsertGrp.class})
            @RequestBody @Valid @NotNull SmsTemplateConfiguration smsTemplateConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addSmsTemplateConfig(smsTemplateConfiguration),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_SMS_TEMPLATE_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> getSmsTemplateConfig(
            @Validated(value = {SmsTemplateConfiguration.FetchGrp.class})
            @Valid @NotEmpty @PathVariable String institutionId) {
        return new ResponseEntity<>(
                configurationManager.getSmsTemplateConfig(institutionId),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.UPDATE_SMS_TEMPLATE_CONFIG)
    public ResponseEntity<BaseResponse> updateSmsTemplateConfig(
            @Validated(value = {SmsTemplateConfiguration.UpdateGrp.class})
            @RequestBody @Valid @NotNull SmsTemplateConfiguration smsTemplateConfiguration) {
        return new ResponseEntity<>(
                configurationManager.updateSmsTemplateConfig(smsTemplateConfiguration),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_SMS_TEMPLATE_CONFIG + "/{institutionId}/{productId}/{smsType}")
    public ResponseEntity<BaseResponse> deleteSmsTemplateConfig(
            @PathVariable("institutionId") @Valid @NotEmpty String institutionId, @PathVariable("productId") @Valid @NotEmpty String productId, @PathVariable("smsType") @Valid @NotEmpty String smsType) {
        return new ResponseEntity<>(
                configurationManager.deleteSmsTemplateConfig(institutionId, productId, smsType),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_DMS_CONFIG)
    public ResponseEntity<BaseResponse> addDmsFolderConfiguration(
            @Validated(value = {DmsFolderConfiguration.AddGrp.class})
            @RequestBody @Valid @NotNull List<DmsFolderConfiguration> dmsFolderConfigurationList) {
        return new ResponseEntity<>(
                configurationManager.addDmsFolderConfiguration(dmsFolderConfigurationList),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_DMS_CONFIG +"/{institutionId}")
    public ResponseEntity<BaseResponse> getDmsFolderConfiguration(
            @Valid @NotEmpty @PathVariable String institutionId) {
        return new ResponseEntity<>(
                configurationManager.getDmsFolderConfiguration(institutionId),
                HttpStatus.OK);
    }


    @PostMapping(value = EndPointReferrer.UPDATE_DMS_CONFIG)
    public ResponseEntity<BaseResponse> updateDmsFolderConfiguration(
            @Validated(value = {DmsFolderConfiguration.UpdateGrp.class})
            @RequestBody @Valid @NotNull DmsFolderConfiguration dmsFolderConfiguration) {
        return new ResponseEntity<>(
                configurationManager.updateDmsFolderConfiguration(dmsFolderConfiguration),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_DMS_CONFIG + "/{institutionId}/{indexName}")
    public ResponseEntity<BaseResponse> deleteDmsFolderConfiguration(
            @PathVariable("institutionId") @Valid @NotEmpty String institutionId, @PathVariable("indexName") @Valid @NotEmpty String indexName) {
        return new ResponseEntity<>(
                configurationManager.deleteDmsFolderConfiguration(institutionId, indexName),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_MASTER_MAPPING_CONFIGURATION)
    public ResponseEntity<BaseResponse> addMasterMappingConfiguration(
            @Validated(value = {MasterMappingConfiguration.AddGrp.class})
            @RequestBody @Valid @NotNull List<MasterMappingConfiguration> masterMappingConfiguration) {
        return new ResponseEntity<>(
                configurationManager.addMasterMappingConfiguration(masterMappingConfiguration),
                HttpStatus.OK);
    }


    @GetMapping(value = EndPointReferrer.GET_LOS_CHARGE_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> getLosChargeConfig(@PathVariable("institutionId") String institutionId) {
        return new ResponseEntity<>(
                configurationManager.getLosChargeConfiguration(institutionId),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.CREATE_LOS_CHARGE_CONFIG)
    public ResponseEntity<BaseResponse> createLosChargeConfig(@RequestBody LosChargeConfig losChargeConfig) {
        return new ResponseEntity<>(
                configurationManager.createLosChargeConfig(losChargeConfig), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.ADD_CASE_CANCEL_CONFIG)
    public ResponseEntity<BaseResponse> addCaseCancellationConfig(
            @Validated(value = {CaseCancellationJobConfig.AddGrp.class})
            @RequestBody @Valid @NotNull CaseCancellationJobConfig caseCancellationJobConfig) {

        logger.info("{} controller started....", EndPointReferrer.ADD_CASE_CANCEL_CONFIG);

        return new ResponseEntity<>(
                configurationManager.addCaseCancellationConfig(caseCancellationJobConfig),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_CASE_CANCEL_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> getCaseCancellationConfig(
            @PathVariable("institutionId")  @Valid @NotEmpty String institutionId) {

        logger.info("{} controller started....", EndPointReferrer.GET_CASE_CANCEL_CONFIG);

        return new ResponseEntity<>(
                configurationManager.getCaseCancellationConfig(institutionId),
                HttpStatus.OK);
    }


    @PostMapping(value = EndPointReferrer.DELETE_CASE_CANCEL_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> deleteCaseCancellationConfig(
            @PathVariable("institutionId")  @Valid @NotEmpty String institutionId) {

        logger.info("{} controller started....",EndPointReferrer.DELETE_CASE_CANCEL_CONFIG);

        return new ResponseEntity<>(
                configurationManager.deleteCaseCancellationConfig(institutionId),
                HttpStatus.OK);
    }

    /**
     * @param institutionId
     * @return
     * @throws Exception
     */
    @GetMapping(EndPointReferrer.IMPS_CONFIG + "/{institutionId}")
    public ResponseEntity<BaseResponse> getIMPSDomain(
            @PathVariable(value = "institutionId") @Valid @NotNull String institutionId
    ) throws Exception {

        return new ResponseEntity<>(configurationManager.getIMPSDomain(institutionId), HttpStatus.OK);
    }

    /**
     * @param impsConfigDomain
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.IMPS_CONFIG)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<BaseResponse> saveIMPSDomain(@RequestBody @Valid @NotNull IMPSConfigDomain impsConfigDomain) throws Exception {
        return new ResponseEntity<>(configurationManager.addIMPSConfiguration(impsConfigDomain),HttpStatus.OK);
    }

    /**
     * @param impsConfigDomain
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.DELETE_IMPS_CONFIG)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<BaseResponse> removeIMPSDomain(@RequestBody @Valid @NotNull IMPSConfigDomain impsConfigDomain) throws Exception {
        return new ResponseEntity<>(configurationManager.deleteIMPSDomain(impsConfigDomain),HttpStatus.OK);
    }
    @PostMapping(value = EndPointReferrer.ADD_MASTER_SCHEDULAR_CONFIG)
    public ResponseEntity<BaseResponse> addMasterSchedularConfig(
            @RequestBody @NotNull @Validated(value = {MasterSchedulerConfiguration.InsertGrp.class}) MasterSchedulerConfiguration masterSchedulerConfiguration,
            @RequestParam @NotBlank String userName, @RequestParam @NotBlank String reason, @RequestParam String masterType) {

        return new ResponseEntity<>(configurationManager.insertMasterSchedulerConfig(masterSchedulerConfiguration,userName,reason,masterType),HttpStatus.OK);
    }



    @GetMapping(value = EndPointReferrer.GET_MASTER_SCHEDULAR_CONFIG+"/{institutionId}")
    public ResponseEntity<BaseResponse> getMasterSchedularConfig(
            @PathVariable(value = "institutionId") @Valid @NotNull String institutionId ) {

        return new ResponseEntity<>(configurationManager.fetchMasterSchedulerConfig(institutionId),HttpStatus.OK);
    }
}
