package com.softcell.rest.controllers;


import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.UpdatedSRNumberDetailsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.GetRollbackDetailsRequest;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorResultRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.SerialNumberManufacturerManager;
import com.softcell.service.SerialNumberValidationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author mahesh
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class SerialNumberValidationController {

    private static final Logger logger = LoggerFactory.getLogger(SerialNumberValidationController.class);

    @Autowired
    private SerialNumberValidationManager serialNumberValidationManager;

    @Autowired
    private SerialNumberManufacturerManager serialNumberManufacturerManager;

    /**
     * @param serialSaleConfirmationRequest
     * @return
     */
    @PostMapping(EndPointReferrer.API_MANUFACTURER_SERIAL_NUMBER_VALIDATION)
    public ResponseEntity<BaseResponse> serialNumberValidation(
            @Validated(value = {SerialSaleConfirmationRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        serialSaleConfirmationRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberManufacturerManager.getVendorResponse(serialSaleConfirmationRequest), HttpStatus.OK);

    }


    /**
     * @param serialNumberApplicableCheckRequest
     * @return
     */
    @PostMapping(EndPointReferrer.API_MANUFACTURER_IS_SERIAL_NO_APPLICABLE)
    public ResponseEntity<BaseResponse> isSerialNumberConfirmationApplicable(
            @Validated(value = {SerialNumberApplicableCheckRequest.FetchGrp.class, Header.InstDealerProductGrp.class})
            @RequestBody @NotNull @Valid SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest) {

        serialNumberApplicableCheckRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberValidationManager.checkSerialNumberFlag(serialNumberApplicableCheckRequest), HttpStatus.OK);
    }

    /**
     * @param updatedSRNumberDetailsRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_UPDATED_SERIAL_NO_DETAILS)
    public ResponseEntity<BaseResponse> updatedSRNumberDetailsResult(
            @Validated(value = {UpdatedSRNumberDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody UpdatedSRNumberDetailsRequest updatedSRNumberDetailsRequest) {

        updatedSRNumberDetailsRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberValidationManager
                .getUpdatedSRNumberDetails(updatedSRNumberDetailsRequest.getReferenceID()),
                HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.GET_SERIAL_NO_DETAILS)
    public ResponseEntity<BaseResponse> getSerialNumberDetails(
            @Validated(value = {UpdatedSRNumberDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody UpdatedSRNumberDetailsRequest updatedSRNumberDetailsRequest) {

        updatedSRNumberDetailsRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberValidationManager
                .getSerialNumberDetails(updatedSRNumberDetailsRequest.getReferenceID()),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SERIAL_OR_IMEI_NUMBER_FOR_ROLLBACK)
    public ResponseEntity<BaseResponse> getRollbackDetails(
            @Validated(value = {GetRollbackDetailsRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid GetRollbackDetailsRequest getRollbackDetailsRequest) throws Exception {

        getRollbackDetailsRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberValidationManager
                .getRollbackServiceRequestDetails(getRollbackDetailsRequest),
                HttpStatus.OK);

    }

    /**
     * @param rollbackRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SERIAL_OR_IMEI_NUMBER_ROLLBACK)
    public ResponseEntity<BaseResponse> doRollback(
            @Validated(value = {RollbackRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid RollbackRequest rollbackRequest) throws Exception {

        rollbackRequest.getHeader().setDateTime(new Date());

        return new ResponseEntity<>(serialNumberValidationManager
                .doRollback(rollbackRequest),
                HttpStatus.OK);

    }

    /**
     * @param disbursementRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SERIAL_NUMBER_DISBURSED)
    public ResponseEntity<BaseResponse> doSerialDisbursement(
            @RequestBody @Valid DisbursementRequest disbursementRequest) throws Exception {

        disbursementRequest.getHeader().setDateTime(new Date());
        return new ResponseEntity<>(serialNumberValidationManager
                .doDisbursement(disbursementRequest),
                HttpStatus.OK);

    }

    /**
     * @param serialNumberApplicableVendorResultRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SAVE_SERIAL_NUMBER_APPLICABLE_RESPONSE)
    public ResponseEntity<BaseResponse> saveSerialNumberApplicableResponse(
            @Validated(value = {SerialNumberApplicableVendorResultRequest.FetchGrp.class, Header.InstDealerProductGrp.class})
            @RequestBody @NotNull @Valid SerialNumberApplicableVendorResultRequest serialNumberApplicableVendorResultRequest) throws Exception{

        return new ResponseEntity<>(serialNumberValidationManager.saveSerialNumberApplicableResponse(serialNumberApplicableVendorResultRequest), HttpStatus.OK);
    }
}