package com.softcell.rest.controllers;


import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.gonogo.exceptions.ReportConfigNotInDb;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLogRequest;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.reports.request.ReportFilteredRequest;
import com.softcell.gonogo.model.request.AdminLogRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.SerialNumberInfoLogRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.emudra.ESignRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.emudra.SFTPLogOutput;
import com.softcell.gonogo.model.request.imps.AccountNumberInfoLogRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorLogRequest;
import com.softcell.reporting.domains.CreditReportRequest;
import com.softcell.reporting.domains.FirstLastLoginReportRequest;
import com.softcell.reporting.domains.OtpReportRequest;
import com.softcell.reporting.domains.SalesReportRequest;
import com.softcell.reporting.report.HDBFSCreditReportGenerator;
import com.softcell.reporting.report.HDBFSReport;
import com.softcell.reporting.report.TVRReport;
import com.softcell.reporting.request.LosUtrUpdateReportRequest;
import com.softcell.reporting.request.ReportRequest;
import com.softcell.reporting.request.ReportSearchRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ReportingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.StopWatch;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;


/**
 * @author kishorp A reporting consume all services which will be used for
 *         reporting purpose.
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.REPORTING_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE

)
public class ReportingController {

    private static final Logger logger = LoggerFactory.getLogger(ReportingController.class);

    @Autowired
    private ReportingManager reportingManager;

    @Autowired
    private TVRReport tvrReport;

    @Autowired
    private HDBFSReport hdbfsReport;

    @Autowired
    private HDBFSCreditReportGenerator hdbfsCreditReportGenerator;


    /**
     * @param configuration
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.CUSTOM_REPORT)
    @Deprecated
    public ResponseEntity<BaseResponse> customReport(
            @Validated(value = {ReportingConfigurations.CustomReportGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingConfigurations configuration) {

        logger.info(" custom report handler  start processing your request ");

        return new ResponseEntity<>(reportingManager.getCustomReports(configuration), HttpStatus.OK);

    }


    /**
     * @param configuration Configuration is used for report type and selection.
     *                      <em>institutionID,reportType,sProductType,reportCycle<em> is mandatory.
     *                      <em>endDate,startDate,localCopy,interval and format are an optional.<em>
     * @param configuration
     * @return Zip file of report in requested format.
     * @see org.springframework.http.HttpHeaders
     * @see javax.servlet.http.HttpServletRequest
     */

    /**
     * @param reportingModuleConfiguration Configuration is used for report type and selection.
     *                                     <em>institutionID,reportType,sProductType,reportCycle<em> is mandatory.
     *                                     <em>endDate,startDate,localCopy,interval and format are an optional.<em>
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws IOException
     */
    @PostMapping(value = EndPointReferrer.DOWNLOAD_CREDIT)
    public Callable<byte[]> generateReport(
            @Validated(value = {ReportingModuleConfiguration.FetchGrp.class ,
                    ReportingModuleConfiguration.ProductTypeGrpForCreditReport.class})
            @RequestBody @NotNull @Valid ReportingModuleConfiguration reportingModuleConfiguration) throws IOException {

        logger.info(" download credit handler  start processing your request ");

        return () -> hdbfsReport.getCreditReport(reportingModuleConfiguration);

    }


    /**
     * @param configuration
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws IOException
     */
    @PostMapping(value = EndPointReferrer.DOWNLOAD_TVR_REPORT)
    public Callable<byte[]> generateTVRReport(
            @Validated(value = {ReportingModuleConfiguration.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingModuleConfiguration configuration) throws IOException {

        logger.info(" download tvr report  handler  start processing your request ");


        return () -> tvrReport.getTVRReport(configuration);


    }


    /**
     * @param configuration
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.SAVE_CUSTOM_REPORT)
    @Deprecated
    public ResponseEntity<BaseResponse> saveCustomReport(
            @Validated(value = {ReportingConfigurations.CustomReportGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingConfigurations configuration) {

        logger.info(" save custom report handler start processing your request ");

        return new ResponseEntity<>(reportingManager.saveCustomReports(configuration), HttpStatus.OK);

    }

    /**
     * @param configuration
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.PREVIEW_CUSTOM_REPORT)
    @Deprecated
    public ResponseEntity<BaseResponse> previewCustomReport(
            @Validated(value = {ReportingConfigurations.CustomReportGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingConfigurations configuration) {

        logger.info(" preview custom  report handler start processing your request ");

        return new ResponseEntity<>(reportingManager.previewCustomReports(configuration), HttpStatus.OK);

    }

    /**
     * @param configuration
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.DOWNLOAD_ZIP_REPORT)
    public ResponseEntity<BaseResponse> csvZipReport(
            @Validated(value = {ReportingConfigurations.CustomReportGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingConfigurations configuration) throws Exception {

        logger.info(" download zip report handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getCsvReport(configuration), HttpStatus.OK);

    }

    /**
     * @param reportRequest
     * @return ResponseEntity<BaseResponse>
     * @throws ReportConfigNotInDb
     */
    @PostMapping(EndPointReferrer.REPORTING_DIMENSION)
    public ResponseEntity<BaseResponse> getGoNoGoSystemFields(@Validated(value = {Header.FetchGrp.class})
                                                              @RequestBody @Valid @NotNull ReportRequest reportRequest) throws ReportConfigNotInDb {

        logger.info(" reporting dimension handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getReportDimensions(reportRequest), HttpStatus.OK);

    }

    /**
     * @param reportSearchRequest
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.SEARCH_REPORT_NAME)
    public ResponseEntity<BaseResponse> searchByReportName(
            @Validated(value = {ReportSearchRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull ReportSearchRequest reportSearchRequest) {

        logger.info(" search report name handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getReportNames(reportSearchRequest),
                HttpStatus.OK);
    }


    /**
     * @param reportRequest
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.FETCH_REPORT_CONFIG)
    public ResponseEntity<BaseResponse> deliverReportConfig(
            @Validated(value = {Header.FetchGrp.class, ReportRequest.FetchGrp.class})
            @RequestBody @Valid @NotNull ReportRequest reportRequest) {

        logger.info(" fetch report configuration handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getReportConfiguration(reportRequest),
                HttpStatus.OK);

    }

    /**
     * @param configuration
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(value = EndPointReferrer.SAVE_DEFAULT_REPORT)
    public ResponseEntity<BaseResponse> saveDefaultReport(
            @Validated(value = {ReportingConfigurations.CustomReportGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportingConfigurations configuration) {

        logger.info(" save default report  handler start processing your request ");

        return new ResponseEntity<>(reportingManager.saveDefaultReports(configuration),
                HttpStatus.OK);


    }

    /**
     * @param reportFilteredRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.CHANNEL_WISE_REPORT)
    public Callable<byte[]> getChannelWiseReport(
            @RequestBody @NotNull @Valid ReportFilteredRequest reportFilteredRequest) throws Exception {

        logger.debug(" channel wise report handler start processing your request ");

        return () -> reportingManager.getChannelWiseReport(
                reportFilteredRequest.getDateStartFrom(), reportFilteredRequest.getEndDate(),
                reportFilteredRequest.getProduct(), reportFilteredRequest.getHeader().getInstitutionId());

    }

    /**
     * @param reportFilteredRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.DEVICE_INFO_REPORT)
    public Callable<byte[]> getDeviceReport(
            @Validated(value = {ReportFilteredRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ReportFilteredRequest reportFilteredRequest) throws Exception {

        logger.debug(" device info report handler start processing your request ");

        return () -> reportingManager.getDeviceReport(reportFilteredRequest.getDateStartFrom(),
                reportFilteredRequest.getEndDate(), reportFilteredRequest.getProduct(), reportFilteredRequest.getHeader()
                        .getInstitutionId());

    }

    /**
     * @param salesReportRequest
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.SALES_INCENT_REPORT)
    public ResponseEntity<BaseResponse> getSalesIncentiveReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid SalesReportRequest salesReportRequest) throws Exception {

        return new ResponseEntity<>(reportingManager.generateSalesIncentiveReport(salesReportRequest),
                HttpStatus.OK);

    }

    /**
     * @param salesReportRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.SALES_INCENT_REPORT_ZIP)
    public Callable<byte[]> getZippedSalesReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid SalesReportRequest salesReportRequest) throws Exception {

        logger.debug(" sales incentive report handler start processing your request ");

        return () -> reportingManager.generateZippedSalesIncentiveReport(salesReportRequest);

    }

    /**
     * @param salesReportRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.SALES_REPORT_ZIP)
    public Callable<byte[]> getSalesReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid SalesReportRequest salesReportRequest) throws Exception {

        logger.debug(" sales report zip handler start processing your request ");

        return () -> reportingManager.generateZippedSalesReport(salesReportRequest);

    }

    /**
     * @param creditReportRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.CREDIT_REPORT_ZIP)
    public Callable<byte[]> getCreditReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid CreditReportRequest creditReportRequest) throws Exception {

        logger.debug(" credit report zip  handler start processing your request ");

        return () -> reportingManager.generateZippedCreditReport(creditReportRequest);

    }

    /**
     * @param firstLastLoginReportRequest
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.FIRST_LAST_LOGIN)
    public ResponseEntity<BaseResponse> getFirstLastLoginReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid FirstLastLoginReportRequest firstLastLoginReportRequest) throws Exception {

        logger.debug(" first last login report handler start processing your request ");

        return new ResponseEntity<>(reportingManager.generateFirstLastLoginReport(firstLastLoginReportRequest),
                HttpStatus.OK);

    }

    /**
     * @param firstLastLoginReportRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.FIRST_LAST_LOGIN_ZIP)
    public Callable<HttpEntity<StreamingResponseBody>> getZippedFirstLastLoginReport(
            @Validated(SalesReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid FirstLastLoginReportRequest firstLastLoginReportRequest) throws Exception {

        logger.debug(" first-last-login zip report handler start processing your request ");

        return () -> {

            byte[] bytes = reportingManager.generateZippedFirstLastLoginReport(firstLastLoginReportRequest);

            return getBytetoStream(bytes, "FirstLastLoginReport");

        };

    }

    /**
     * @param otpReportRequest
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.OTP_ZIP)
    public ResponseEntity<BaseResponse> getOtpReport(
            @Validated(OtpReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid OtpReportRequest otpReportRequest) throws Exception {

        logger.debug(" otp report handler start processing your request ");

        return new ResponseEntity<>(reportingManager.generateOtpReport(otpReportRequest), HttpStatus.OK);

    }

    /**
     * @param otpReportRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.OTP_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getZippedOtpReport(
            @Validated(OtpReportRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid OtpReportRequest otpReportRequest) throws Exception {

        logger.debug(" otp zip report handler start processing your request ");

        return () -> {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();

            byte[] bytes = reportingManager.generateZippedOtpReport(otpReportRequest);

            stopWatch.stop();

            logger.info(" time taken to process otp report is [{}] sec", stopWatch.getTotalTimeSeconds());

            return getBytetoStream(bytes, "OtpReport");

        };

    }

    /**
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.ASSET_HIERARCHY)
    @Deprecated
    public ResponseEntity<BaseResponse> getAssetAnalyticalReport() throws Exception {

        logger.debug(" asset hierarchy handler start processing your request ");

        return new ResponseEntity<>(reportingManager.createTreeViewForAssetModelReport(), HttpStatus.OK);
    }

    /**
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.SERIAL_NUMBER_INFO_LOG_ALL)
    public ResponseEntity<BaseResponse> getSerialNumberLogs() throws Exception {

        logger.debug(" serial number info  handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getSerialLog(), HttpStatus.OK);
    }

    /**
     * @param adminLogRequest
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.SERIAL_NUMBER_INFO_LOG_BY_FILTERS)
    public ResponseEntity<BaseResponse> getSerialNumberLogReportByRefID(
            @Validated(AdminLogRequest.FetchGrp.class)
            @RequestBody @NotNull @Valid AdminLogRequest adminLogRequest) throws Exception {

        logger.debug(" serial number info log by filters handler start processing your request ");

        return new ResponseEntity<>(
                reportingManager.getSerialByIDLog(adminLogRequest.getRefID(), adminLogRequest.getVendor()),
                HttpStatus.OK);
    }

    /**
     * @param serialNumberInfoLogRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     */
    @PostMapping(value = EndPointReferrer.SERIAL_NUMBER_INFO_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getSerialNumberLogReportByVendor(
            @Validated(value = {Header.FetchGrp.class, SerialNumberInfoLogRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SerialNumberInfoLogRequest serialNumberInfoLogRequest) {

        logger.debug(" serial number info  zip report handler start processing your request ");

        return () -> {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();


            byte[] serialNumberLogZipReportByVendor = reportingManager.getSerialNumberLogZipReportByVendor(serialNumberInfoLogRequest.getHeader().getInstitutionId(),serialNumberInfoLogRequest.getVendor(),
                    serialNumberInfoLogRequest.getDateStartFrom(), serialNumberInfoLogRequest.getEndDate());

            stopWatch.stop();

            logger.info(" time taken to process serial number info  report is [{}] sec", stopWatch.getTotalTimeSeconds());

            return getBytetoStream(serialNumberLogZipReportByVendor, "SerialNumberLogReport");

        };

    }

    @PostMapping(value = EndPointReferrer.SERIAL_NUMBER_ROLLBACK_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getSerialNumberRollbackLogReportByVendor(
            @Validated(value = {Header.FetchGrp.class, SerialNumberInfoLogRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SerialNumberInfoLogRequest serialNumberInfoLogRequest) {

        logger.debug(" serial number rollback  zip report handler start processing your request ");

        return () -> {

            byte[] serialNumberRollbackReportByVendor = reportingManager.getSerialNumberRollBackReportByVendor(serialNumberInfoLogRequest);

            return getBytetoStream(serialNumberRollbackReportByVendor, "SerialNumberRollbackReport");
        };

    }

    /**
     * @param adminLogRequest
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.SERIAL_NUMBER_REQUEST_LOG_BY_FILTERS)
    public ResponseEntity<BaseResponse> getSerialNumberRequestLogReport(
            @RequestBody AdminLogRequest adminLogRequest) {

        logger.info("serial number request log by filters handler started processing your request ");

        return new ResponseEntity<>(reportingManager.getSerialNumberRequestLog(
                adminLogRequest.getRefID(), adminLogRequest.getVendor()), HttpStatus.OK);
    }

    /**
     * @return ResponseEntity<BaseResponse>
     */
    @GetMapping(EndPointReferrer.SERIAL_NUMBER_REQUEST_LOG_ALL)
    public ResponseEntity<BaseResponse> getSerialNumberRequestLogReport() {

        logger.info(" serial number request log all handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getSerialNumberRequestLog(), HttpStatus.OK);

    }

    /**
     * @param adminLogRequest
     * @return ResponseEntity<BaseResponse>
     */
    @PostMapping(EndPointReferrer.GET_MAIL_LOG_BY_FILTER)
    public ResponseEntity<BaseResponse> getMailReportLogByID(
            @RequestBody AdminLogRequest adminLogRequest) {

        logger.info("get mail log by filter handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getMailLogReport(adminLogRequest.getRefID()), HttpStatus.OK);
    }

    /**
     * @return ResponseEntity<BaseResponse>
     */
    @GetMapping(EndPointReferrer.GET_MAIL_LOG_ALL)
    public ResponseEntity<BaseResponse> getMailLogReport() {

        logger.info(" get mail log all handler start processing your request ");

        return new ResponseEntity<>(reportingManager.getMailLogReport(), HttpStatus.OK);

    }

    /**
     * @param losUtrUpdateReportRequest
     * @return ResponseEntity<BaseResponse>
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.LOS_UTR_UPDATE_ZIP_REPORT)
    public ResponseEntity<BaseResponse> getLosUtrUpdateReport(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LosUtrUpdateReportRequest losUtrUpdateReportRequest) throws Exception {

        logger.debug(" los utr update zip report   handler start processing your request ");

        return new ResponseEntity(reportingManager.getLosUtrUpdateReport(losUtrUpdateReportRequest),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.AADHAAR_LOG)
    public ResponseEntity<BaseResponse> getAadhaarLog(
            @Validated(value = {AadhaarLogRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid AadhaarLogRequest aadhaarLogRequest) {

        logger.debug("initiate aadhaar log service {}", EndPointReferrer.AADHAAR_LOG);

        return new ResponseEntity(reportingManager.getAadhaarLogReport(aadhaarLogRequest),
                HttpStatus.OK);

    }

    /**
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.LOS_MB_DATA_PUSH_LOG)
    public ResponseEntity<BaseResponse> getLosMB(
            @Validated(value = {Header.FetchGrp.class, DmzRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DmzRequest dmzRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.LOS_MB_DATA_PUSH_LOG);

        return new ResponseEntity<>(
                reportingManager.getMbPushLogs(dmzRequest),
                HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.TVR_DATA_PUSH_LOG)
    public ResponseEntity<BaseResponse> getTvrPush(
            @PathVariable @NotNull @Valid String refId) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.TVR_DATA_PUSH_LOG);

        return new ResponseEntity<>(
                reportingManager.getTvrLogs(refId),
                HttpStatus.OK);
    }

    /**
     * utility method to convert bytes array to StreamingResponseBody {@code StreamingResponseBody}
     * with header attached to every HttpEntity
     *
     * @param bytes
     * @param reportName
     * @return HttpEntity<StreamingResponseBody>
     */
    private HttpEntity<StreamingResponseBody> getBytetoStream(byte[] bytes, String reportName) {

        HttpHeaders headers = new HttpHeaders();

        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        headers.setContentLength(bytes.length);

        headers.setContentType(MediaType.parseMediaType("application/zip"));

        headers.setContentDispositionFormData("inline", reportName);

        return ResponseEntity.status(HttpStatus.OK).body(outputStream -> outputStream.write(bytes));

    }


    /**
     * @param
     *                                     <em>refId<em> is mandatory.
     * @return Callable<HttpEntity<StreamingResponseBody>>
     * @throws IOException
     */
    @PostMapping(value = EndPointReferrer.DOWNLOAD_EMANDATE_XML)
    public Callable<byte[]> generateXMLSignedEMudra(
            @RequestBody @NotNull @Valid ESignRequest eSignRequest) throws IOException {

        logger.info(" download xml esigned by emudra");

        return () -> hdbfsReport.getSignedXMLEMudra(eSignRequest.getRefId(),eSignRequest.getHeader().getInstitutionId());

    }

    @PostMapping(EndPointReferrer.SIGNED_XML_REF_ID)
    public ResponseEntity<List<SFTPLogOutput>> getSignedXMLBYrefId(
            @RequestBody @Valid @NotNull ESignRequest eSignRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.SIGNED_XML_REF_ID);

        return new ResponseEntity<>(hdbfsReport.getSignedXMLBYrefId(eSignRequest.getRefId()), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.E_SIGN_REF_ID)
    public ResponseEntity<List<ESignedLog>> getESignBYrefId(
            @RequestBody @Valid @NotNull ESignRequest eSignRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.E_SIGN_REF_ID);

        return new ResponseEntity<>(hdbfsReport.getESignBYrefId(eSignRequest.getRefId()), HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.DOWNLOAD_CREDIT_REPORT)
    public Callable<byte[]> generateCreditReport(
            @Validated(value = {ReportingModuleConfiguration.FetchGrp.class ,
                    ReportingModuleConfiguration.ProductTypeGrpForCreditReport.class})
            @RequestBody @NotNull @Valid ReportingModuleConfiguration reportingModuleConfiguration) throws IOException {

        logger.info(" download credit handler  start processing your request ");

        return () -> hdbfsCreditReportGenerator.generateCreditReport(reportingModuleConfiguration);

    }

    /**
     * @param accountNumberInfoLogRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     */
    @PostMapping(value = EndPointReferrer.ACCOUNT_NUMBER_LOG_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getAccountNumberLogReport(
            @Validated(value = {Header.FetchGrp.class, AccountNumberInfoLogRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid AccountNumberInfoLogRequest accountNumberInfoLogRequest) {

        logger.debug(" account number info  zip report handler start processing your request ");

        return () -> {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();

            byte[] accountNumberLogZipReport = reportingManager.getAccountNumberLogZipReport(accountNumberInfoLogRequest.getHeader().getInstitutionId(),
                    accountNumberInfoLogRequest.getHeader().getProduct().name(),accountNumberInfoLogRequest.getDateStartFrom(),accountNumberInfoLogRequest.getEndDate());

            stopWatch.stop();

            logger.info(" time taken to process account number info report is [{}] sec", stopWatch.getTotalTimeSeconds());

            return getBytetoStream(accountNumberLogZipReport, "AccountNumberLogReport");

        };

    }

    /**
     * @param serialNumberApplicableVendorLogRequest
     * @return Callable<HttpEntity<StreamingResponseBody>>
     */
    @PostMapping(value = EndPointReferrer.SERIAL_NUMBER_APPLICABLE_LOG_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getSerialNumberApplicableVendorLogReport(
            @Validated(value = {Header.FetchGrp.class, SerialNumberApplicableVendorLogRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SerialNumberApplicableVendorLogRequest serialNumberApplicableVendorLogRequest) {

        logger.debug("serial number applicable vendor log report handler start processing your request ");

        return () -> {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();

            byte[] serialNumberApplicableVendorLogZipReport = reportingManager.getSerialNumberApplicableVendorLogZipReport(serialNumberApplicableVendorLogRequest.getHeader().getInstitutionId(),
                   serialNumberApplicableVendorLogRequest.getDateStartFrom(),serialNumberApplicableVendorLogRequest.getEndDate());

            stopWatch.stop();

            logger.info("time taken to process serial number applicable vendor log report is [{}] sec", stopWatch.getTotalTimeSeconds());

            return getBytetoStream(serialNumberApplicableVendorLogZipReport, "SerialNumberApplicableVendorLogReport");

        };

    }

    @GetMapping(EndPointReferrer.SBFC_LMS_LOG_BY_REFID + "/{refId}")
    public ResponseEntity<BaseResponse> getSBFCLMSLogByRefId(
            @PathVariable(value = "refId") @Valid @NotNull String refId) throws Exception {

        return new ResponseEntity(reportingManager.getSBFCLMSLogByRefId(refId), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SBFC_LMS_LOG_BY_ZIP_REPORT)
    public Callable<HttpEntity<StreamingResponseBody>> getSBFCLMSZipLogReport(
            @RequestBody @NotNull @Valid SBFCLMSIntegrationLogRequest sbfclmsIntegrationLogRequest) {

        logger.debug(" SBFC-LMS log zip report handler start processing your request ");

        return () -> {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();


            byte[] logZipReport = reportingManager.getSBFCLMSLogZipReport(sbfclmsIntegrationLogRequest.getInstId(),
                    sbfclmsIntegrationLogRequest.getStartDate(), sbfclmsIntegrationLogRequest.getEndDate());

            stopWatch.stop();

            logger.info(" time taken to process SBFC-LMS report is [{}] sec", stopWatch.getTotalTimeSeconds());

            return getBytetoStream(logZipReport, "SBFCLMSLogReport");

        };

    }
}
