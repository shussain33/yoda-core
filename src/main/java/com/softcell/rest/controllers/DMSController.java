package com.softcell.rest.controllers;

import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DMSFeedManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 24/6/17.
 */
@RestController
@RequestMapping(value = EndPointReferrer.DMS,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DMSController {

    private static final Logger logger = LoggerFactory.getLogger(DMSController.class);

    @Autowired
    private DMSFeedManager dmsFeedManager;

    @PostMapping(EndPointReferrer.PUSH_DOCUMENTS)
    public ResponseEntity<BaseResponse> pushDocuments(
            @Validated(value = {Header.FetchGrp.class, PushDMSDocumentsRequest.FetchGrp.class})
            @RequestBody  @NotNull @Valid PushDMSDocumentsRequest pushDMSDocumentsRequest) throws Exception {

        logger.info("{} controller started with parameter referenceId {}", EndPointReferrer.PUSH_DOCUMENTS, pushDMSDocumentsRequest.getRefId());

        return ResponseEntity.status(HttpStatus.OK).body(dmsFeedManager.pushDocumentsToDms(pushDMSDocumentsRequest));

    }

    @GetMapping(EndPointReferrer.GET_PUSH_DOCUMENTS_INFO +"/{sRefId}/{sInstitutionId}")
    public ResponseEntity<BaseResponse> getDMSPushDocumentsInfo(@PathVariable("sRefId") @NotNull @Valid String refId ,
                                                  @PathVariable("sInstitutionId") @NotNull @Valid String institutionId) throws Exception {

        logger.info("{} controller started with parameter referenceId {} and institutionId {}", EndPointReferrer.GET_PUSH_DOCUMENTS_INFO, refId ,institutionId);

        return ResponseEntity.status(HttpStatus.OK).body(dmsFeedManager.getPushDocumentsInfo(refId, institutionId));

    }

    @GetMapping(EndPointReferrer.GET_ERROR_LOG +"/{sRefId}/{sInstitutionId}")
    public ResponseEntity<BaseResponse> getDMSErrorLog(@PathVariable("sRefId") @NotNull @Valid String refId ,
                                                                @PathVariable("sInstitutionId") @NotNull @Valid String institutionId) throws Exception {

        logger.info("{} controller started with parameter referenceId {} and institutionId {}", EndPointReferrer.GET_ERROR_LOG, refId ,institutionId);

        return ResponseEntity.status(HttpStatus.OK).body(dmsFeedManager.getDMSErrorLog(refId, institutionId));

    }



}
