package com.softcell.rest.controllers;

import com.google.gson.Gson;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.AppDisbursalRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.LOSGoNoGoApplicationService;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class LOSGoNoGoApplicationController {
	private final Logger logger = LoggerFactory.getLogger(LOSGoNoGoApplicationController.class);

	@Autowired
	LOSGoNoGoApplicationService gonogoCustomerApplicationService;

	Gson gson = new Gson();


    /**
     *
     * @param caseId
     * @return tvscdlosresponse list. using old connector configuration
     */
	@RequestMapping(value = "/tvscdlos", method = RequestMethod.GET)
	public @ResponseBody
	List<TVSCdLosGroupResponse> pullAndTranformDataByCaseID(@RequestParam String caseId)
			throws Exception {

		if (StringUtils.isNotBlank(caseId)) {
			return gonogoCustomerApplicationService.pullAndPopulateDataByOnlyCaseId(caseId);
		} else {
			return null;
		}

	}


    /**
     *
     * @param date
     * @param stage
     * @return get record from provided date and its next date
     * @throws Exception
     */
	@RequestMapping(value = "/goNoGoCustomerApplicationRecordByDateAndStage", method = RequestMethod.GET)
	@ResponseBody
	public List<GoNoGoCustomerApplication> goNoGoCustomerApplicationRecord(@RequestParam String date, @RequestParam String stage) throws Exception {
		List<GoNoGoCustomerApplication> SearchResults = new ArrayList<GoNoGoCustomerApplication>();
		logger.debug("in goNoGoCustomerApplicationRecordByDateAndStage controller");
		try {
			// String question="";
			SearchResults = gonogoCustomerApplicationService.getsearchRecord(date, stage);
			return SearchResults;
		} catch (Exception e) {
			logger.error("Error occured in goNoGoCustomerApplicationRecordByDateAndStage controller", e);
			return SearchResults;
		}
	}


    /**
     *
     * @param caseId
     * @return for checking update insert status
     */
	@RequestMapping(value = "/tvsGroupServiceStatusByCaseID", method = RequestMethod.GET)
	@ResponseBody
	public TvsGroupServiceStatus groupServiceStatusByCaseId(@RequestParam String caseId) {
		TvsGroupServiceStatus tvsGroupServiceStatus = new TvsGroupServiceStatus();
		logger.debug("in TvsGroupServiceStatusByCaseID controller::: " + caseId);
		try {
            tvsGroupServiceStatus = gonogoCustomerApplicationService.getTvsGroupServiceStatus(caseId);

		} catch (Exception e) {
			logger.debug("Error occured in TvsGroupService_StatusByCaseID", e);

		}

		return tvsGroupServiceStatus;
	}

    /**
     *
     * @param caseId
     * @return get auditinfo list according caseid
     */
	@RequestMapping(value = "/getAuditLogById", method = RequestMethod.GET)
	@ResponseBody
	public List<AuditingInfo> getAuditingInfoById(@RequestParam String caseId) {
		List<AuditingInfo> AuditingInfo = new ArrayList<AuditingInfo>();
		logger.debug("in controller::: " + caseId);
		try {
			AuditingInfo = gonogoCustomerApplicationService.getAuditLogById(caseId);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return AuditingInfo;
	}

    /**
     *
     * @return all audit info
     */
	@RequestMapping(value = "/getCompleteAuditLog", method = RequestMethod.GET)
	@ResponseBody
	public List<AuditingInfo> getCompleteAuditingInfo() {
		List<AuditingInfo> auditingLogList = new ArrayList<AuditingInfo>();

		try {
			auditingLogList = gonogoCustomerApplicationService.getCompleteAuditLog();

		} catch (Exception e) {
			e.printStackTrace();

		}
		return auditingLogList;
	}


    /**
     *
     * @param losTvsRequest
     * @return response list.
     * integrated connector call
     * @throws Exception
     */
	@RequestMapping(value = "/tvscdlos", method = RequestMethod.POST)
	public @ResponseBody
    BaseResponse pullAndTranformDataByCaseID(@RequestBody LosTvsRequest losTvsRequest)
			throws Exception {
	    BaseResponse baseResponse = null;
		String caseId = losTvsRequest.getRefID();
		List<TVSCdLosGroupResponse> tvsCdLosGroupResponseList = new ArrayList<TVSCdLosGroupResponse>();

		try {
			logger.debug("in tvscdlos api");
			//before initiate
			if(StringUtils.isNotBlank(caseId)){
                baseResponse = gonogoCustomerApplicationService.pullAndTranformDataByCaseID(losTvsRequest);
			}
		} catch (Exception ex) {
			logger.error("Error in Los Tvs Request call : {} ", ex.getMessage());
			Collection<Error> errors = new ArrayList<>();
			errors.add(Error.builder()
					.message(ex.getMessage())
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.HIGH.name())
					.build());

			baseResponse =  GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,errors);
		}
		return baseResponse;
	}

}