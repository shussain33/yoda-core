package com.softcell.rest.controllers;

import com.softcell.gonogo.model.reports.request.GoNoGoReportRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.GoNoGoReportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        value = EndPointReferrer.GONOGO_REPORTS,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class GoNoGoReportController {

    private static final Logger logger = LoggerFactory.getLogger(GoNoGoReportController.class);

    @Autowired
    GoNoGoReportManager goNoGoReportManager;

    @PostMapping(EndPointReferrer.DOWNLOAD_REPORTS)
    public ResponseEntity downloadReports(@RequestBody GoNoGoReportRequest goNoGoReportRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.DOWNLOAD_REPORTS);
        return new ResponseEntity<>(
                goNoGoReportManager.downloadReportsDump(goNoGoReportRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_ALL_REPORTS_NAME)
    public ResponseEntity getAllReportsName(@RequestBody GoNoGoReportRequest goNoGoReportRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_ALL_REPORTS_NAME);
        return new ResponseEntity<>(
                goNoGoReportManager.getAllReportsName(goNoGoReportRequest),
                HttpStatus.OK);
    }
}
