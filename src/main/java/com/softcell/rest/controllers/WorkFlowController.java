package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.nextgen.manager.WfDAGRunner;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AuditDataManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.ReInitiateApplicationRequest;
import com.softcell.workflow.WorkFlowRestartConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author kishor
 * @author vinodk
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.WORKFLOW_BASE_ENPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class WorkFlowController {

    private static final Logger logger = LoggerFactory.getLogger(WorkFlowController.class);

    @Autowired
    private AuditDataManager auditDataManager;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;


    @Autowired
    private WfDAGRunner wfDAGRunner;

    /**
     * @param workFlowRestartConfiguration The work-flow restart app is defined by
     *                                     CRO and it will be execute completely or partially.
     *                                     Module defined by user and all those are not executed
     *                                     successfully in last run, due to fatal error or system
     *                                     was unstable.
     * @return If request re-processing start successfully. return successful
     * object of <em>Acknowledgement<em>.
     * else return failure message with error code.
     */
    @Deprecated
    @PostMapping(EndPointReferrer.REPROCESS_BY_ID)
    public ResponseEntity<BaseResponse> reprocessApplicationByRefId(
            @Validated(value = {Header.FetchGrp.class, WorkFlowRestartConfiguration.FetchGrp.class})
            @RequestBody @NotNull @Valid WorkFlowRestartConfiguration workFlowRestartConfiguration) throws Exception {

        return new ResponseEntity<>(
                auditDataManager
                        .getApplicationData(workFlowRestartConfiguration),
                HttpStatus.OK);
    }

    /**
     * @param reInitiateApplicationRequest the application updated data and new app to start
     *                                     work flow. the work app will be partial execution of
     *                                     process or full re-submit of application.
     * @return If request re-processing start successfully. return successful
     * object of <em>Acknowledgement<em>.
     * else return failure message with error code.
     */
    @PostMapping(EndPointReferrer.REPROCESS_UPDATED)
    public ResponseEntity<BaseResponse> reprocessUpdatedApplication(
            @Validated(value = {ReInitiateApplicationRequest.FetchGrp.class, WorkFlowRestartConfiguration.ReInitGrp.class})
            @RequestBody @NotNull @Valid ReInitiateApplicationRequest reInitiateApplicationRequest) throws Exception {
        return new ResponseEntity<>(
                auditDataManager.
                        reInitiateApplication(reInitiateApplicationRequest),
                HttpStatus.OK);
    }

    /**
     * @param auditDataRequest the application updated data and new app to
     *                         start work flow. the work app will be partial
     *                         execution of process or full re-submit of application.
     * @return If request re-processing start successfully. return successful
     * object of <em>Acknowledgement<em>.
     * else return failure message with error code.
     */
    @PostMapping(EndPointReferrer.BRE_AUDIT_DATA)
    public ResponseEntity<BaseResponse> reprocessUpdatedApplication(
            @Validated(value = {Header.FetchGrp.class, AuditDataRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid AuditDataRequest auditDataRequest) throws  Exception{

        return new ResponseEntity<>(auditDataManager.getBreAuditData(auditDataRequest), HttpStatus.OK);
    }

    /**
     * @param applicationRequest
     * @return
     */
    @PostMapping(EndPointReferrer.REAPPRAISE)
    public ResponseEntity<BaseResponse> reappraiseApplication(
            @Validated(value = {Header.FetchGrp.class, ApplicationRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationRequest applicationRequest) throws Exception {

        return new ResponseEntity<>(
                auditDataManager.doReAppraiseApplication(applicationRequest),
                HttpStatus.OK);

    }

    /**
     * @param checkApplicationStatus @CheckApplicationStatus request
     * @return list of application response including reappraised application response.
     */
    @PostMapping(EndPointReferrer.REAPPRAISAL_STATUS)
    public ResponseEntity<BaseResponse> getReappraisedStatus(
            @Validated(value = {Header.FetchGrp.class, CheckApplicationStatus.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        return new ResponseEntity<>(
                auditDataManager.getReAppraiseStatus(checkApplicationStatus),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.QUICK_DATA_ENTRY_STATUS)
    public ResponseEntity<BaseResponse> getQuickDataEntryStatus  (
            @Validated(value = {Header.FetchGrp.class, AuditDataRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid AuditDataRequest auditDataRequest) throws Exception {

        return new ResponseEntity<>(
                auditDataManager.getQuickDataEntryStatus(auditDataRequest),
                HttpStatus.OK);
    }

    @GetMapping(EndPointReferrer.WF_COMM_CONNECTION + "/{institutionId}")
    public ResponseEntity<BaseResponse> getCommunicationDomain(
            @PathVariable(value = "institutionId") @Valid @NotNull String institutionId
    ) throws Exception {

        return new ResponseEntity<>(workFlowCommunicationManager.getWfJobComm(institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.WF_COMM_CONNECTION)
    @ResponseStatus(HttpStatus.CREATED)
    public void saveCommunicationDomain(@RequestBody @Valid @NotNull WFJobCommDomain wfJobCommDomain) throws Exception {
        workFlowCommunicationManager.saveWfJobComm(wfJobCommDomain);
    }

    @PostMapping(EndPointReferrer.WF_COMM_CONNECTION_DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCommunicationDomain(@RequestBody @Valid @NotNull WFJobCommDomain wfJobCommDomain) throws Exception {
        workFlowCommunicationManager.deleteWfJobComm(wfJobCommDomain);
    }


    @PostMapping("/start-workflow")
    public ResponseEntity<BaseResponse> startWorkFlow(
            @NotNull
            @Validated(value = {Header.InsertGrp.class})
            @RequestBody ApplicationRequest applicationRequest) {

        GoNoGoCustomerApplication goNoGoCustomerApplication = new GoNoGoCustomerApplication();

        goNoGoCustomerApplication.setApplicationRequest(applicationRequest);

        goNoGoCustomerApplication.setGngRefId(applicationRequest.getRefID());

        return new ResponseEntity<>(wfDAGRunner.buildDag(goNoGoCustomerApplication), HttpStatus.OK);

    }


}
