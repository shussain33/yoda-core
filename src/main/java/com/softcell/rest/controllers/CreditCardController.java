package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.CreditCardManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.CREDIT_CARD_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class CreditCardController {

    private static final Logger logger = LoggerFactory.getLogger(CreditCardController.class);

    @Autowired
    private CreditCardManager creditCardManager;

    /**
     * @param creditCardSurrogateRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CREDITCARD)
    public ResponseEntity<BaseResponse> creditCardSurrogateCheck(
            @Validated(value = {CreditCardSurrogateRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CreditCardSurrogateRequest creditCardSurrogateRequest) {

                return new ResponseEntity<>(creditCardManager
                        .checkCreditCard(creditCardSurrogateRequest),
                        HttpStatus.OK);
    }
}
