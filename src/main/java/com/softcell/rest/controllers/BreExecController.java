package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.BreExecRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.BreExecManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 6/2/18.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class BreExecController {
    private static final Logger logger = LoggerFactory.getLogger(BreExecController.class);
    @Autowired
    private BreExecManager breExecManager;

    @PostMapping(EndPointReferrer.VERIFY_KYC)
    public ResponseEntity<BaseResponse> verifyKyc(@RequestBody @NotNull ApplicationRequest applicationRequest,HttpServletRequest httpServletRequest,
                                                  @PathVariable String kycName) {
        logger.debug("{} controller started", EndPointReferrer.VERIFY_KYC);
        return ResponseEntity.status(HttpStatus.OK).body(breExecManager.verifyKyc(applicationRequest, httpServletRequest, kycName));
    }

    @PostMapping(EndPointReferrer.CALL_MB) //beauro
    public ResponseEntity<BaseResponse> callBureau(@RequestBody @NotNull @Validated BreExecRequest breExecRequest, HttpServletRequest httpServletRequest) {
        logger.debug("{} controller started", EndPointReferrer.CALL_MB);
        return ResponseEntity.status(HttpStatus.OK).body(breExecManager.callMultibureau(breExecRequest,httpServletRequest));
    }

    @PostMapping(EndPointReferrer.CALL_SOBRE)
    public ResponseEntity<BaseResponse> executeSobre(@RequestBody @NotNull BreExecRequest breExecRequest,HttpServletRequest httpServletRequest ) {
        logger.debug("{} controller started", EndPointReferrer.CALL_SOBRE);
        return ResponseEntity.status(HttpStatus.OK).body(breExecManager.executeSobre(breExecRequest, httpServletRequest));
    }

    @PostMapping(EndPointReferrer.EVALUATE_FROM_SOBRE)
    public ResponseEntity<BaseResponse> executeSobre(@RequestBody @NotNull LoginDataRequest loginDataRequest, HttpServletRequest httpServletRequest ) {
        ///This method do not persist any data as a GoNoGoCustomer or Rawresponse. It is just for calculations.
        logger.debug("{} controller started", EndPointReferrer.EVALUATE_FROM_SOBRE);
        return ResponseEntity.status(HttpStatus.OK).body(breExecManager.executeSobre(loginDataRequest, httpServletRequest));
    }
}
