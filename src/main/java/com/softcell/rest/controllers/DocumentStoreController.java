package com.softcell.rest.controllers;


import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.DocImageFIleGetRequest;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.KycImages;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DocumentStoreManager;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;


/**
 * @author yogeshb
 */

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class DocumentStoreController {

    private static final Logger logger = LoggerFactory.getLogger(DocumentStoreController.class);

    @Autowired
    private DocumentStoreManager documentStoreManager;

    /**
     * Upload document images
     *
     * @param fileUploadRequest
     * @return FileUploadResponse
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UPLOAD_IMAGE)
    public ResponseEntity<BaseResponse> handleFileUpload(
            @Validated(value = {FileUploadRequest.InsertGrp.class, FileHeader.FetchGrp.class})
            @RequestBody @NotNull @Valid FileUploadRequest fileUploadRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.saveImageToDB(fileUploadRequest), HttpStatus.OK);
    }

    /**
     * Revise service for image upload using multipart Entity
     *
     * @param fileUploadRequest
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UPLOAD_IMAGE_WITH_METADATA)
    public ResponseEntity<BaseResponse> onUpload(
            @Validated(value = {FileUploadRequest.InsertGrp.class, FileHeader.InsertGrp.class})
            @RequestPart("meta-data") @NotNull @Valid FileUploadRequest fileUploadRequest,
            @RequestPart("file-data") @NotNull @Valid MultipartFile file) throws Exception {

        return new ResponseEntity<>(documentStoreManager.saveImageWithMetadataToDB(file, fileUploadRequest),
                HttpStatus.OK);
    }

    /**
     * @param fileUploadRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UPDATE_IMAGE_STATUS)
    public ResponseEntity<BaseResponse> updateImageStatus(
            @Validated(value = {FileUploadRequest.ApproveOrReject.class, FileHeader.FetchGrp.class})
            @RequestBody @NotNull @Valid FileUploadRequest fileUploadRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.updateImageStatus(fileUploadRequest), HttpStatus.OK);
    }

    /**
     * @param docImageFIleGetRequest
     * @return
     * @throws Exception
     * @deprecated
     */
    @Deprecated
    @RequestMapping(value = EndPointReferrer.GET_IMAGE, produces = "image/jpg")
    public ResponseEntity<byte[]> fetchFileUpload(
            @RequestBody DocImageFIleGetRequest docImageFIleGetRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocumentImage(docImageFIleGetRequest),
                HttpStatus.OK);
    }

    /**
     * @param docImageFIleGetRequest
     * @return
     * @throws Exception
     * @deprecated
     */
    @Deprecated
    @PostMapping(EndPointReferrer.GET_IMAGES)
    public ResponseEntity<KycImages> fetchFileUploads(
            @RequestBody DocImageFIleGetRequest docImageFIleGetRequest) throws Exception {
        return new ResponseEntity<>(
                documentStoreManager.getDocumentImages(docImageFIleGetRequest),
                HttpStatus.OK);
    }

    /**
     * @param checkApplicationStatus
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.GET_IMAGE_DETAILS)
    public ResponseEntity<BaseResponse> getImageDetails(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocumentImageDetails(checkApplicationStatus), HttpStatus.OK);
    }

    /**
     * It uses dmi client to get images metadata against specific co-Applicant.
     *
     * @param checkApplicationStatus
     * @return
     * @throws Exception
     */

    //FIXME:applicant id is should be marked as not null as documentStoreManager.validateRequest made it mandatory @yogeshb to ask
    @PostMapping(EndPointReferrer.IMAGES_METADATA)
    public ResponseEntity<BaseResponse> getImageDetailsByApplicantID(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

            return new ResponseEntity<>(documentStoreManager.getDocumentImageDetailsByApplicant(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     *
     * @param checkApplicationStatus
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.IMAGES_METADATA_E_SIGNED_DOCUMENT)
    public ResponseEntity<BaseResponse> getImageDetailsById(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocumentImageDetailsByRefId(checkApplicationStatus), HttpStatus.OK);

    }


    /**
     * @param getImageRequest
     * @return
     * @throws Exception
     * @deprecated
     */
    @Deprecated
    @PostMapping(value = EndPointReferrer.GET_IMAGE_BY_ID, produces = "image/jpg")
    public ResponseEntity<BaseResponse> getImageByRefId(@RequestBody GetFileRequest getImageRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocumentImageByRefId(getImageRequest), HttpStatus.OK);
    }

    /**
     * @param getImageRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.GET_IMAGE_BY_ID_BASE64)
    public ResponseEntity<BaseResponse> getImageByRefIdBase64(
            @Validated(value = {GetFileRequest.ImgFetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getImageRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocumentBase64ImageByRefId(getImageRequest),
                HttpStatus.OK);
    }

    /**
     * @param getImageRequest
     * @return
     * @throws Exception
     * @deprecated
     */
    @Deprecated
    @PostMapping(value = EndPointReferrer.GET_PDF_BY_ID, produces = "image/jpg")
    public ResponseEntity<BaseResponse> getPByRefId(@RequestBody GetFileRequest getImageRequest)
            throws Exception {

        return new ResponseEntity<>(documentStoreManager.getPdfDocumentByRefId(getImageRequest),
                HttpStatus.OK);
    }

    /**
     * @param httpHeaders
     * @param getImageRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.SEND_MAIL_PDF)
    public ResponseEntity<BaseResponse> sendMail(
            @RequestHeader HttpHeaders httpHeaders,
            @Validated(value = {GetFileRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getImageRequest) throws Exception {

        logger.debug("send-mail-pdf service hit");

        //FIXME:Can we take httpheaders to service layer @Pratik

        boolean authFlag = true;

        if (!authFlag) {

            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return new ResponseEntity<>(GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION,errors), HttpStatus.OK);
        }

        return new ResponseEntity<>(
                documentStoreManager.sendMailToCustomer(getImageRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SEND_DIGITIZE_MAIL)
    public ResponseEntity<BaseResponse> sendDigitizeMail(
            @RequestHeader HttpHeaders httpHeaders,
            @Validated(value = {GetFileRequest.FetchRefIdGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getImageRequest) throws Exception {

        logger.debug("{} service hit",EndPointReferrer.SEND_DIGITIZE_MAIL);

        return new ResponseEntity<>(
                documentStoreManager.sendDigitizeMailToCustomer(getImageRequest),
                HttpStatus.OK);
    }

    /**
     * @param getFileRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.GET_PDF_REF)
    public ResponseEntity<BaseResponse> getDoPdf(
            @Validated(value = {GetFileRequest.FetchRefIdGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getFileRequest)
            throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDeliveryOrderPdf(getFileRequest.getRefID(),getFileRequest.getHeader().getInstitutionId()), HttpStatus.OK);
    }

    /**
     * @param getFileRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.CIBIL_PDF_REPORT)
    public ResponseEntity<BaseResponse> getCibilPdf(
            @Validated(value = {GetFileRequest.FetchRefIdGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getFileRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getCibilPdfReport(getFileRequest), HttpStatus.OK);
    }

    /**
     *
     * @param getFileRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.BUREAU_PDF_REPORT +"/{bureau}")
    public ResponseEntity<BaseResponse> getBureauPdf(
            @Validated(value = {GetFileRequest.FetchRefIdGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getFileRequest,
            @PathVariable @NotNull String bureau) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getBureauPdfReport(getFileRequest,bureau), HttpStatus.OK);
    }

    /**
     * @param cibilPdfRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.CIBIL_PDF_REPORT_COAPP)
    public ResponseEntity<BaseResponse> getCibilPdfWithCoApplicant(
            @Validated(value = {GetFileRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest cibilPdfRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getCibilPdfReportForCoapplicant(cibilPdfRequest), HttpStatus.OK);

    }

    /**
     * Upload document images with the application image linkage details
     *
     * @param fileUploadRequest
     * @return FileUploadResponce
     * @throws Exception
     */
    @Deprecated
    @PostMapping(EndPointReferrer.UPLOAD_IMAGE_LINKAGE)
    public ResponseEntity<BaseResponse> uploadImage(
            @RequestBody FileUploadRequest fileUploadRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.saveImage(fileUploadRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DOCUMENT)
    public ResponseEntity<BaseResponse> getDocument(
            @Validated(value = {Header.FetchGrp.class, GetFileRequest.FetchDocumentGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getFileRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getDocument(getFileRequest), HttpStatus.OK);
    }

    /**
     * Added for TVS. To upload any document specific to a loan application
     * @param fileUploadRequest
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UPLOAD_DOCUMENT)
    public ResponseEntity<BaseResponse> uploadDocument(
            @Validated(value = {FileHeader.InsertGrp.class, FileUploadRequest.DocumentInsertGrp.class})
            @RequestPart("meta-data") @NotNull @Valid FileUploadRequest fileUploadRequest,
            @RequestPart("file-data") @NotNull @Valid MultipartFile file
    ) throws Exception {
        return new ResponseEntity<>(documentStoreManager.saveDocumentToDB(fileUploadRequest, file), HttpStatus.OK);
    }

    /**
     * Added for TVS. This is used to fetch the SAP CSV file. SAP CSV file download functionality
     * is for debugging purpose.
     * @param getFileRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.GET_APPLICATION_DOCUMENT)
    public ResponseEntity<BaseResponse> getApplicationDocument(
            @Validated(value = {Header.FetchGrp.class, GetFileRequest.FetchDocumentGrp.class})
            @RequestBody @NotNull @Valid GetFileRequest getFileRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getApplicationDocument(getFileRequest), HttpStatus.OK);
    }

    /**
     * Added for TVS. USed to get the Aadhaar PDF document.
     * @param fileUploadRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.GET_LOANAPP_DOCUMENT)
    public ResponseEntity<BaseResponse> getLoanAppDocument(
            @Validated(value = {Header.FetchGrp.class,  FileUploadRequest.DocumentFetchGrp.class})
            @RequestBody @NotNull @Valid FileUploadRequest fileUploadRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.getLoanAppDocument(fileUploadRequest), HttpStatus.OK);
    }

    /**
     * Added for SBFC to soft delete documents.
     */
    @PostMapping(EndPointReferrer.DELETE_DOCUMENT)
    public ResponseEntity<BaseResponse> softDeleteDocument(
            @Validated(value = {Header.FetchGrp.class,  FileUploadRequest.DocumentFetchGrp.class})
            @RequestBody @NotNull @Valid FileUploadRequest fileUploadRequest) throws Exception {

        return new ResponseEntity<>(documentStoreManager.deleteDocument(fileUploadRequest), HttpStatus.OK);
    }
}
