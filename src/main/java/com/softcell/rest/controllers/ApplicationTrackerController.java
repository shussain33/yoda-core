package com.softcell.rest.controllers;

import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ApplicationTrackerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping(
        value = EndPointReferrer.APPLICATION_TRACKER_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ApplicationTrackerController {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTrackerController.class);

    @Autowired
    private ApplicationTrackerManager trackManager;

    /**
     * @param checkApplicationStatus
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION)
    public ResponseEntity<BaseResponse> getApplicationTrackingLog(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.APPLICATION);

        return new ResponseEntity<>(trackManager.getApplicationCaseHistory(checkApplicationStatus), HttpStatus.OK);

    }


    /**
     * @param configuration
     * @return
     */
    @PostMapping(EndPointReferrer.DOWNLOAD_CASEHISTORY)
    public ResponseEntity<BaseResponse> generateReport(
            @Validated(value = {ReportingModuleConfiguration.FetchGrp.class})
            @RequestBody @Valid @NotNull ReportingModuleConfiguration configuration) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DOWNLOAD_CASEHISTORY);

        return new ResponseEntity<>(trackManager.getApplicationCSV(configuration), HttpStatus.OK);

    }
}
