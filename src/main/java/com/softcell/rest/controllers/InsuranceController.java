package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.InsuranceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg0268 on 9/11/19.
 */

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class InsuranceController {

    @Autowired
    InsuranceManager insuranceManager;

    private static final Logger logger = LoggerFactory.getLogger(InsuranceController.class);

    @PostMapping(EndPointReferrer.SAVE_INSURANCE_DATA)
    public ResponseEntity<BaseResponse> saveInsuranceData(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull InsuranceRequest insuranceRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started with stepId = {}, tempSave {}", EndPointReferrer.SAVE_INSURANCE_DATA);
        return new ResponseEntity<>(
                insuranceManager.saveInsuranceData(insuranceRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_INSURANCE_DATA)
    public ResponseEntity<BaseResponse> getInsuranceData(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull InsuranceRequest insuranceRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started with stepId = {}, tempSave {}", EndPointReferrer.GET_INSURANCE_DATA);
        return new ResponseEntity<>(
                insuranceManager.getInsuranceData(insuranceRequest, httpRequest),
                HttpStatus.OK);
    }

}
