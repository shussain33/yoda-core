package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.UpdateDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.UpdateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg0268 on 2/9/19.
 */

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class UpdateDataController {

    private static final Logger logger = LoggerFactory.getLogger(DataEntryController.class);
    @Autowired
    private UpdateManager updateManager;

    @PostMapping(EndPointReferrer.UPDATE_API)
    public ResponseEntity<BaseResponse> updateCompletedInfoGonogo (
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull UpdateDataRequest updateDataRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started with stepId = {}, tempSave {}", EndPointReferrer.UPDATE_API,
                stepId);

        return new ResponseEntity<>(
                updateManager.updateData(updateDataRequest, stepId,httpRequest),
                HttpStatus.OK);
    }

}
