package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.MigrationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.FileStagingUtils;
import com.softcell.service.MigrationManager;
import org.apache.commons.io.FileUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;

/**
 * Created by amit on 11/3/19.
 */

@RestController
@RequestMapping(
        value = EndPointReferrer.MIGRATION_ENDPONT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class MigrationController {

    private Logger logger = LoggerFactory.getLogger(MigrationController.class);

    @Autowired
    MigrationManager migrationManager;

    @PostMapping(value = EndPointReferrer.REGISTRATION_INFO)
    public ResponseEntity<BaseResponse> migrateRegistrationInfo(@Validated(value = {Header.FetchGrp.class})
                              @RequestBody @Valid @NotNull MigrationRequest migrationRequest) {

        return new ResponseEntity<>(migrationManager.updateRegistrationInfo(migrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SOURCING_CHANNEL)
    public ResponseEntity<BaseResponse> migrateSourcingChannel(@Validated(value = {Header.FetchGrp.class})
                                                                @RequestBody @Valid @NotNull MigrationRequest migrationRequest) {

        return new ResponseEntity<>(migrationManager.updateSourcingChannel(migrationRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.BRANCH_MANAGER_NAME)
    public ResponseEntity<BaseResponse> migrateBranchManagerName(@Validated(value = {Header.FetchGrp.class})
                                                               @RequestBody @Valid @NotNull MigrationRequest migrationRequest, HttpServletRequest httpRequest) {

        return new ResponseEntity<>(migrationManager.updateBranchMangerName(migrationRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.DSAID_DECISION)
    public ResponseEntity<BaseResponse> bulkDecision(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.DSAID_DECISION);

        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
        BaseResponse baseResponse = migrationManager.bulkDsaIdCases(uploadedFile, institutionId);
        FileUtils.deleteQuietly(uploadedFile);

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
    @PostMapping(EndPointReferrer.COUNT)
    public ResponseEntity<BaseResponse> countDsaIdToupdate(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.COUNT);
        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
        BaseResponse baseResponse = migrationManager.bulkDsaIdCasesCount(uploadedFile, institutionId);
        FileUtils.deleteQuietly(uploadedFile);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.ALLOCATEINFO_CHANGE)
    public ResponseEntity<BaseResponse> allocationInfoToupdate(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.ALLOCATEINFO_CHANGE);
        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
        BaseResponse baseResponse = migrationManager.allocateInfoChange(uploadedFile, institutionId);
        FileUtils.deleteQuietly(uploadedFile);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.CAM_DETAIL_UPDATE)
    public ResponseEntity<BaseResponse> camDetailUpdate(
            @Validated({Header.FetchGrp.class})
            @RequestBody @Valid @NotNull MigrationRequest migrationRequest
            ) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.CAM_DETAIL_UPDATE);
        return new ResponseEntity<>(migrationManager.changeInBankingDetail(migrationRequest.getHeader().getInstitutionId(), migrationRequest.isHit()), HttpStatus.OK);
    }
}
