package com.softcell.rest.controllers;

import com.softcell.gonogo.model.email.EmailRequest;
import com.softcell.gonogo.model.email.GenericMailRequest;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.EmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 24/2/18.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class EmailController {

    private static final Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    EmailSender emailSender;

    @PostMapping(EndPointReferrer.VERIFY_EMAIL)
    public ResponseEntity<BaseResponse> sendVerificationMail(
            @Validated(value = {Header.InsertGroup.class})
            @RequestBody @NotNull @Valid EmailRequest emailRequest
    ) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.VERIFY_EMAIL);
        return new ResponseEntity<>(emailSender.sendVerificationMail(emailRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SEND_MAIL)
    public ResponseEntity<?> sendMail(
            @Validated(value = {Header.InsertGroup.class})
            @RequestBody @NotNull @Valid GenericMailRequest emailRequest
    ) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.SEND_MAIL);
        return new ResponseEntity<>(emailSender.sendMail(emailRequest),
                HttpStatus.OK);
    }

}
