package com.softcell.rest.controllers;

import com.softcell.gonogo.model.analytics.LoginStatusTable;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AnalyticsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author prateek
 */


@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
@RestController
public class AnalyticsController {

    private static final Logger logger = LoggerFactory.getLogger(AnalyticsController.class);

    @Autowired
    private AnalyticsManager analyticsHandler;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @Deprecated
    @PostMapping(EndPointReferrer.STACK_GRAPH)
    public ResponseEntity<BaseResponse> getOldStack(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" stack-graph handler start processing your request ");

        return new ResponseEntity<>(analyticsHandler.getLoginStatusGraphDataOld(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */

    @PostMapping(EndPointReferrer.LOGIN_BY_STATUS_GRAPH)
    public ResponseEntity<BaseResponse> getStack(
            @Validated(value = {StackRequest.UpgradedStackGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" login-by-status-graph handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getLoginStatusGraphData(stackRequest), HttpStatus.OK);
    }

    /**
     * new controller for previous {@see getStackTable} with modifications to
     * send total count for document present in system
     *
     * @param stackRequest
     * @return {@link LoginStatusTable}
     * @throws Exception {generic exception}
     */
    @PostMapping(EndPointReferrer.LOGIN_BY_STATUS_TABLE)
    public ResponseEntity<BaseResponse> getLoginStatustable(
            @Validated(value = {StackRequest.StatusTblLgnGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" login-by-status-table handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getLoginStatusTableData(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.LOGIN_BY_STATUS_TABLE_DTC)
    public ResponseEntity<BaseResponse> getLoginStatustablePL(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" login-by-status-table-dtc handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getLoginStatusTableDataForDtc(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.LOGIN_BY_STAGE)
    public ResponseEntity<BaseResponse> getLoginByStagesReport(
            @Validated(value = {StackRequest.StackGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {


        logger.debug(" login-by-stage handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.generateLoginByStagesReport(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.ANALYTICS_ASSET_MANUFACTURE_GRAPH)
    public ResponseEntity<BaseResponse> getAssetManufactureReport(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" analytics-asset-manufacture-graph  handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.generateAssetManufacturerReport(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @Deprecated
    @PostMapping(EndPointReferrer.LOGIN_BY_DEALERSHIP)
    public ResponseEntity<BaseResponse> getDealerWiseReport(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" login-by-dealership  handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getLoginByTopDealer(stackRequest), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.SCORE_TREE_GRAPH)
    public ResponseEntity<BaseResponse> getStackScore(
            @Validated(value = {StackRequest.ScoreTreeGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest)
            throws Exception {

        logger.debug(" score-tree-graph handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getStackScoreData(stackRequest), HttpStatus.OK);

    }

    /**
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.DSA_REPORTING_DATA)
    public ResponseEntity<BaseResponse> getCSVLog() throws Exception {

        logger.debug(" dsa-reporting-data handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getDsaReport(), HttpStatus.OK);
    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UNIQUE_CITIES)
    public ResponseEntity<BaseResponse> getUniqueCitiesBasedOnHierarchy(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" unique-cities handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getRoleBasedDistinctBranches(stackRequest), HttpStatus.OK);

    }

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UNIQUE_DEALERS)
    public ResponseEntity<BaseResponse> getRoleBasedOnDistinctDealers(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" unique-dealers handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getRoleBasedOnDistinctDealers(stackRequest), HttpStatus.OK);

    }


    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.UNIQUE_DSAS)
    public ResponseEntity<BaseResponse> getRoleBasedOnDistinctDSA(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" unique-dsas handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getRoleBasedOnDistinctDSA(stackRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UNIQUE_STAGES)
    public ResponseEntity<BaseResponse> getRoleBasedOnDistinctStages(
            @Validated(value = {StackRequest.StatusTblGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid StackRequest stackRequest) throws Exception {

        logger.debug(" unique-stages handler start processing your request  ");

        return new ResponseEntity<>(analyticsHandler.getRoleBasedDistinctStages(stackRequest), HttpStatus.OK);

    }

}
