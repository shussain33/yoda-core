package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.ThirdPartyRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataConversionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg222 on 13/5/19.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
/**
 * This Controller is used to Convert all the application related conversion.
 */
public class DataConversionController {

    private static final Logger logger = LoggerFactory.getLogger(DataConversionController.class);
    @Autowired
    private DataConversionManager dataConversionManager;


    /**
     *
     * @param thirdPartyRequest pass header and aRefId for download excel functionality
     * @return excel or csv of fields which are provided in database of all client
     * @throws Exception
     */

    @PostMapping(EndPointReferrer.DOWNLOAD_EXCEL)
    public ResponseEntity<BaseResponse> downloadExcel(
            @RequestBody @NotNull @Valid ThirdPartyRequest thirdPartyRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.DOWNLOAD_EXCEL);
        return new ResponseEntity<>(
                dataConversionManager.downloadExcel(thirdPartyRequest),HttpStatus.OK);
    }

}
