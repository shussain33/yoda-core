package com.softcell.rest.controllers;

import com.softcell.gonogo.model.imps.AccountValidationAttemptRequest;
import com.softcell.gonogo.model.imps.ValidatedBankRequest;
import com.softcell.gonogo.model.request.BankingDetailsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.IMPSManager;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author sampat
 */
@RestController
@RequestMapping(
        path = EndPointReferrer.IMPS,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class IMPSController {

    private static final Logger logger = LoggerFactory.getLogger(IMPSController.class);

    @Autowired
    IMPSManager impsManager;

    /**
     * @param impsRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.VALIDATE_ACCOUNT_NUMBER)
    public ResponseEntity<BaseResponse> validateAccountNumber(
            @Validated(value = {Header.FetchGrp.class, IMPSRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid IMPSRequest impsRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.VALIDATE_ACCOUNT_NUMBER);

        return new ResponseEntity<>(
                impsManager.validateAccountNumber(impsRequest),
                HttpStatus.OK);
    }

    /**
     * @param validationAttemptRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.GET_ACCOUNT_VALIDATION_ATTEMPT)
    public ResponseEntity<BaseResponse> getAccountValidationAttempt(
            @Validated(value = {Header.FetchGrp.class, AccountValidationAttemptRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid AccountValidationAttemptRequest validationAttemptRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.GET_ACCOUNT_VALIDATION_ATTEMPT);

        return new ResponseEntity<>(
                impsManager.getAccountValidationAttempt(validationAttemptRequest.getHeader().getInstitutionId(), validationAttemptRequest.getRefID()),
                HttpStatus.OK);
    }

    /**
     * @param validatedBankRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.GET_VALIDATED_BANKS)
    public ResponseEntity<BaseResponse> getAccountValidationAttempt(
            @Validated(value = {Header.FetchGrp.class, ValidatedBankRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid ValidatedBankRequest validatedBankRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.GET_VALIDATED_BANKS);

        return new ResponseEntity<>(
                impsManager.getValidatedBanks(validatedBankRequest.getHeader().getInstitutionId(), validatedBankRequest.getRefID()),
                HttpStatus.OK);
    }

    /**
     * @param institutionId
     * @return
     * @throws Exception
     */
    @GetMapping(value = EndPointReferrer.IS_ACCOUNT_NUMBER_VALIDATION_APPLICABLE+ "/{institutionId}")
    public ResponseEntity<BaseResponse> isAccountNumberValidationIsApplicable(
            @Valid @NotEmpty @PathVariable String institutionId) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.IS_ACCOUNT_NUMBER_VALIDATION_APPLICABLE);

        return new ResponseEntity<>(
                impsManager.checkAccountNumberValidationFlag(institutionId),
                HttpStatus.OK);
    }

    /**
     * @param bankingDetailsRequest
     * @return
     * @throws Exception
     */
    @PostMapping(value = EndPointReferrer.UPDATE_BANKING_DETAILS_IMPS)
    public ResponseEntity<BaseResponse> updateBankingDetailsForImps(
            @Validated(value = {Header.FetchGrp.class, ValidatedBankRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid BankingDetailsRequest bankingDetailsRequest) throws Exception {


        logger.debug("{} controller started", EndPointReferrer.UPDATE_BANKING_DETAILS_IMPS);

        return new ResponseEntity<>(
                impsManager.updateBankingDetailsImps(bankingDetailsRequest.getBankingDetails().get(0),bankingDetailsRequest.getHeader().getInstitutionId(), bankingDetailsRequest.getRefId()),
                HttpStatus.OK);
    }
}
