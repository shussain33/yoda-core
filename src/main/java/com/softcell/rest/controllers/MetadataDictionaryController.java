package com.softcell.rest.controllers;

import com.softcell.constants.Product;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.domains.MetadataDictRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.MetaDataDictionaryService;
import io.swagger.annotations.Api;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Created by prateek on 11/3/17.
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.METADATA,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
@Api(value = "metadata dictionary", description = "Metadata dictionary api ")
public class MetadataDictionaryController {

    private static final Logger logger = LoggerFactory.getLogger(MetadataDictionaryController.class);

    @Autowired
    private MetaDataDictionaryService metaDataDictionaryService;


    @GetMapping(EndPointReferrer.METADATA_DICTIONARY)
    public ResponseEntity<BaseResponse> getTypedMetaData(
            @RequestParam("institutionId") @Valid @Pattern(regexp = "[0-9]+") String institutionId,
            @RequestParam("product") @Valid @NotNull String product,
            @RequestParam("type") @Valid @NotNull String type
    ) {

        return ResponseEntity.status(HttpStatus.OK).body(metaDataDictionaryService.findTypeValues(institutionId, product, type));
    }

    @PostMapping(EndPointReferrer.METADATA_DICTIONARY_PUT)
    public ResponseEntity<BaseResponse> replaceTypedMetaData(@RequestBody @Valid @NotNull MetadataDictRequest metadataDictRequest) {

        return ResponseEntity.status(HttpStatus.OK).body(metaDataDictionaryService.replaceTypeValues(metadataDictRequest));
    }

    @PostMapping(EndPointReferrer.METADATA_DICTIONARY)
    public ResponseEntity<BaseResponse> createTypedMetaData(@RequestBody @Valid @NotNull MetadataDictRequest metadataDictRequest) {

        return ResponseEntity.status(HttpStatus.CREATED).body(metaDataDictionaryService.createTypeValues(metadataDictRequest));
    }

    @PostMapping(EndPointReferrer.METADATA_DICTIONARY_DELETE)
    public ResponseEntity<BaseResponse> deleteTypedMetaData(@RequestBody @Valid @NotNull MetadataDictRequest metadataDictRequest) {

        return ResponseEntity.status(HttpStatus.OK).body(metaDataDictionaryService.deleteType(metadataDictRequest));
    }

    @GetMapping(value = EndPointReferrer.METADATA_DICTIONARY + "/{institutionId}")
    public ResponseEntity<BaseResponse> getMetadataDictByInstitution(
            @Valid @NotEmpty @PathVariable String institutionId) {
        BaseResponse byInstitutionId = metaDataDictionaryService.findByInstitutionId(institutionId);
        return ResponseEntity.status(HttpStatus.OK).body(byInstitutionId);
    }

    @GetMapping(EndPointReferrer.METADATA_DICTIONARY + "/{institutionId}/{product}")
    public ResponseEntity<BaseResponse> getMetadataDictByInstitutionNProduct(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotNull @PathVariable Product product) {

        BaseResponse baseResponse = metaDataDictionaryService.findByInstitutionIdNProduct(institutionId, product.name());

        return ResponseEntity.status(HttpStatus.OK).body(baseResponse);
    }

    @PostMapping(EndPointReferrer.METADATA_DICTIONARY_PATCH)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<BaseResponse> renameTypeValue(@RequestBody @Valid @NotNull MetadataDictRequest metadataDictRequest) {

        return ResponseEntity.status(HttpStatus.OK).body(metaDataDictionaryService.renameTypeValues(metadataDictRequest));

    }

    @GetMapping(EndPointReferrer.METADATA_DICTIONARY + EndPointReferrer.ENUMS + "/{type}")
    public ResponseEntity<BaseResponse> getMetadataDictByType(
            @Valid @NotEmpty @PathVariable String type) {
        return ResponseEntity.status(HttpStatus.OK).body(metaDataDictionaryService.findByType(type));
    }


}
