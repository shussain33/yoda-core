package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.DedupeRemarkRequest;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.UpdateCamRequest;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.request.DeviationRequest;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.mifin.MiFinResponse;
import com.softcell.gonogo.model.ops.CustomerCreditRequest;
import com.softcell.gonogo.model.ssl.perfios.PerfiosRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataEntryManager;
import com.softcell.service.ExternalApiManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by archana on 25/1/18.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class DataEntryController {

    private static final Logger logger = LoggerFactory.getLogger(DataEntryController.class);

    @Autowired
    private DataEntryManager dataEntryManager;

    @Autowired
    private ExternalApiManager externalApiManager;


    /**
     * @param applicationRequest application to be saved.
     * @param stepId             step number
     * @param httpRequest
     * @return saved application request with generated unique id's
     */
    @PostMapping(EndPointReferrer.SAVE_APPLICATION_STEPID)
    public ResponseEntity<BaseResponse> saveApplication(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationRequest applicationRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_APPLICATION_STEPID);

        return new ResponseEntity<>(
                dataEntryManager.save(applicationRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SUBMIT_STEPID)
    public ResponseEntity<BaseResponse> submitApplication(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationRequest applicationRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug(" {} controller started with step {} ", EndPointReferrer.SUBMIT_APPLICATION, stepId);

        return new ResponseEntity<>(dataEntryManager.submit(applicationRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CAM_DETAILS)
    public ResponseEntity<BaseResponse> saveCamDetails(
            @Validated(value = {CamDetailsRequest.InsertGrp.class})
            @RequestBody @NotNull @Valid CamDetailsRequest camDetailsRequest,
            @PathVariable String camType,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CAM_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveCamDetails(camDetailsRequest, camType, false, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_CAM_DETAILS)
    public ResponseEntity<BaseResponse> getCamDetails(
            @Validated(value = {CamDetailsRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid CamDetailsRequest camDetailsRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_CAM_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getCamDetailsByCamType(camDetailsRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_CAM_DETAILS)
    public ResponseEntity<BaseResponse> updateCamDetails(
            @RequestBody UpdateCamRequest updateCamRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_CAM_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.updateCamDetails(updateCamRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_VERIFICATION_DETAILS)
    public ResponseEntity<BaseResponse> saveVerificationDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull VerificationRequest verificationRequest,
            @PathVariable String verificationType,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_VERIFICATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveVerificationDetails(verificationRequest, verificationType, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_VERIFICATION_DETAILS)
    public ResponseEntity<BaseResponse> getVerificationDetails(
            @RequestBody @NotNull @Valid VerificationRequest verificationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_VERIFICATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getVerificationDetails(verificationRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_ACTIVITYLOG_DATA)
    public ResponseEntity<BaseResponse> getActivityLogsDetail(@RequestBody @NotNull UserActivityRequest userActivityRequest) throws Exception{
        logger.info("Inside controller {}",EndPointReferrer.GET_ACTIVITYLOG_DATA);
        return new ResponseEntity<>(dataEntryManager.getActivityLogsDetails(userActivityRequest),HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_PROPERTY_VISIT_DETAILS)
    public ResponseEntity<BaseResponse> savePropertyVisitDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull PropertyVisitRequest propertyVisitRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_PROPERTY_VISIT_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.savePropertyVisitDetails(propertyVisitRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_PROPERTY_VISIT_DETAILS)
    public ResponseEntity<BaseResponse> getPropertyVisitDetails(
            @RequestBody @NotNull @Valid PropertyVisitRequest propertyVisitRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_PROPERTY_VISIT_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getPropertyVisitDetails(propertyVisitRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_PERSONAL_DISCUSSION_DETAILS)
    public ResponseEntity<BaseResponse> savePersonalDiscussion(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationRequest applicationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_PERSONAL_DISCUSSION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.savePersonalDiscussion(applicationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_REPAYMENT_DETAILS)
    public ResponseEntity<BaseResponse> saveRepaymentDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull RepaymentRequest repaymentRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_REPAYMENT_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveRepaymentDetails(repaymentRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_REPAYMENT_DETAILS)
    public ResponseEntity<BaseResponse> getRepaymentDetails(
            @RequestBody @NotNull @Valid RepaymentRequest repaymentRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_REPAYMENT_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getRepaymentDetails(repaymentRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_LEGAL_VERIFICATION_DETAILS)
    public ResponseEntity<BaseResponse> saveLegalVerificationDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull LegalVerificationRequest legalVerificationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_LEGAL_VERIFICATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveLegalVerificationDetails(legalVerificationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_LEGAL_VERIFICATION_DETAILS)
    public ResponseEntity<BaseResponse> getLegalVerificationDetails(
            @RequestBody @NotNull @Valid LegalVerificationRequest legalVerificationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_LEGAL_VERIFICATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getLegalVerificationDetails(legalVerificationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_CUSTOMER_CREDIT_DOCS)
    public ResponseEntity<BaseResponse> saveCustomerCreditDocs(
            @Validated(value = {CustomerCreditRequest.InsertGrp.class})
            @RequestBody @NotNull CustomerCreditRequest customerCreditRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_CUSTOMER_CREDIT_DOCS);

        return new ResponseEntity<>(
                dataEntryManager.saveCustomerCreditDocs(customerCreditRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_CUSTOMER_CREDIT_DOCS)
    public ResponseEntity<BaseResponse> getCustomerCreditDocs(
            @Validated(value = {CustomerCreditRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CustomerCreditRequest customerCreditRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_CUSTOMER_CREDIT_DOCS);

        return new ResponseEntity<>(
                dataEntryManager.getCustomerCreditDocs(customerCreditRequest, httpRequest),
                HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.SAVE_LIST_OF_DOCUMENTS)
    public ResponseEntity<BaseResponse> saveListOfDocs(
            @Validated(value = {Header.FetchGrp.class, ListOfDocsRequest.InsertGrp.class})
            @RequestBody @NotNull ListOfDocsRequest listOfDocsRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_LIST_OF_DOCUMENTS);

        return new ResponseEntity<>(
                dataEntryManager.saveListOfDocs(listOfDocsRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_LIST_OF_DOCUMENTS)
    public ResponseEntity<BaseResponse> getListOfDocs(
            @Validated(value = {Header.FetchGrp.class, ListOfDocsRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid ListOfDocsRequest listOfDocsRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_LIST_OF_DOCUMENTS);

        return new ResponseEntity<>(
                dataEntryManager.getListOfDocs(listOfDocsRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_ELIGIBILITY_DETAILS)
    public ResponseEntity<BaseResponse> saveEligibility(
            @Validated(value = {EligibilityRequest.InsertGroup.class})
            @RequestBody @NotNull EligibilityRequest eligibilityRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_ELIGIBILITY_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveEligibilityDetails(eligibilityRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_ELIGIBILITY_DETAILS)
    public ResponseEntity<BaseResponse> getEligibility(

            @Validated(value = {EligibilityRequest.FetchGroup.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid EligibilityRequest eligibilityRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_ELIGIBILITY_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getEligibilityDetails(eligibilityRequest, httpRequest),
                HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS)
    public ResponseEntity<BaseResponse> saveLoanChargesDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull LoanChargesRequest loanChargesRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveLoanChargesDetails(loanChargesRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_LOAN_CHARGES_DETAILS)
    public ResponseEntity<BaseResponse> getLoanChargesDetails(
            @RequestBody @NotNull @Valid LoanChargesRequest loanChargesRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_LOAN_CHARGES_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getLoanChargesDetails(loanChargesRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.TRIGGER_VERIFICATION)
    public ResponseEntity<BaseResponse> triggerVerification(
            @RequestBody @NotNull @Valid VerificationRequest verificationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.TRIGGER_VERIFICATION);

        return new ResponseEntity<>(
                dataEntryManager.triggerVerification(verificationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_VALUATION_DETAILS)
    public ResponseEntity<BaseResponse> saveValuationDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ValuationRequest valuationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_VALUATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveValuationDetails(valuationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.DELETE_VALUATION_DETAILS)
    public ResponseEntity<BaseResponse> deleteValuationDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ValuationRequest valuationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DELETE_VALUATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.deleteValuationDetails(valuationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_VALUATION_DETAILS)
    public ResponseEntity<BaseResponse> getValuationDetails(
            @RequestBody @NotNull @Valid ValuationRequest valuationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_VALUATION_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getValuationDetails(valuationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CHANGE_APP_LOCK_STATUS)
    public ResponseEntity<BaseResponse> changeAppLockStatus(
            @Validated({ApplicationLockRequest.InsertGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationLockRequest applicationLockRequest, HttpServletRequest httpRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.CHANGE_APP_LOCK_STATUS);
        return new ResponseEntity<>(dataEntryManager.changeApplicationLockStatus(applicationLockRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_DEVIATION)
    public ResponseEntity<BaseResponse> saveDeviation(
            @RequestBody @NotNull @Valid DeviationRequest deviationRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.SAVE_DEVIATION);
        return new ResponseEntity<>(
                dataEntryManager.saveDeviation(deviationRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.APPROVE_ALL_DEVIATION)
    public ResponseEntity<BaseResponse> approveAllDeviation(
            @RequestBody @NotNull @Valid DeviationRequest deviationRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.APPROVE_ALL_DEVIATION);
        return new ResponseEntity<>(
                dataEntryManager.approveAllDeviation(deviationRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DEVIATION)
    public ResponseEntity<BaseResponse> getDeviation(
            @RequestBody @NotNull @Valid DeviationRequest deviationRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_DEVIATION);
        return new ResponseEntity<>(
                dataEntryManager.getDeviation(deviationRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIFIN)
    public MiFinResponse pushDataToMifin(
            @RequestBody @NotNull AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MIFIN);
        return externalApiManager.pushDataToMiFin(adminLogRequest.getRefID());
    }

    @PostMapping(EndPointReferrer.SAVE_DM_DETAILS)
    public ResponseEntity<BaseResponse> saveDMDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull DMRequest dmRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_DM_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveDMDetails(dmRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DM_DETAILS)
    public ResponseEntity<BaseResponse> getDMDetails(
            @RequestBody @NotNull @Valid DMRequest dmRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_DM_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getDMDetails(dmRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_SANCTION_CONDITIONS)
    public ResponseEntity<BaseResponse> saveSanctionConditions(
            @Validated(value = {Header.FetchGrp.class, SanctionConditionRequest.InsertGrp.class})
            @RequestBody @NotNull SanctionConditionRequest sanctionConditionRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_SANCTION_CONDITIONS);

        return new ResponseEntity<>(
                dataEntryManager.saveSanctionConditions(sanctionConditionRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_DEDUPE_MATCH)
    public ResponseEntity<BaseResponse> saveDedupeRemarks(
            @RequestBody @NotNull @Valid DedupeRemarkRequest deviationRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.SAVE_DEDUPE_MATCH);
        return new ResponseEntity<>(
                dataEntryManager.saveDedupeRemarks(deviationRequest),HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DEDUPE_INFO)
    public ResponseEntity<BaseResponse> getDedupeInfo(
            @RequestBody @NotNull @Valid DedupeRemarkRequest deviationRequest) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_DEDUPE_INFO);
        return new ResponseEntity<>(
                dataEntryManager.getDedupeRemarks(deviationRequest),HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SANCTION_CONDITIONS)
    public ResponseEntity<BaseResponse> getSanctionConditions(
            @Validated(value = {Header.FetchGrp.class, SanctionConditionRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SanctionConditionRequest sanctionConditionRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_SANCTION_CONDITIONS);

        return new ResponseEntity<>(
                dataEntryManager.getSanctionConditions(sanctionConditionRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DOCUMENT_BY_FILE_NAME)
    public ResponseEntity<BaseResponse> getDocumentByFileName(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull DocumentRequest documentRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_DOCUMENT_BY_FILE_NAME);

        return new ResponseEntity<>(
                dataEntryManager.getDocumentByFileName(documentRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_COMPLETED_INFO_GNG)
    public ResponseEntity<BaseResponse> updateCompletedInfoGonogo (
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull UpdateGNGRequest updateGNGRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_COMPLETED_INFO_GNG);

        return new ResponseEntity<>(
                dataEntryManager.updateCompletedInfoGonogo(updateGNGRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.VERIFY_APPROVAL)
    public ResponseEntity<BaseResponse> verifyBeforeAprv(
            @RequestBody @NotNull @Valid ApprovalVerificationRequest amountVerificationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_APPROVAL);

        return new ResponseEntity<>(
                dataEntryManager.verifyApproval(amountVerificationRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.DELETE_COLLATERAL)
    public ResponseEntity<BaseResponse> deleteCollateral(
            @RequestBody @NotNull @Valid ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DELETE_COLLATERAL);

        return new ResponseEntity<>(
                dataEntryManager.deleteCollateral(applicationRequest, httpRequest),
                HttpStatus.OK);
    }

    /*@PostMapping(EndPointReferrer.DELETE_CO_APPLICANT)
    public ResponseEntity<BaseResponse> deleteCoApplicant(
            @RequestBody @NotNull @Valid ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DELETE_CO_APPLICANT);

        return new ResponseEntity<>(
                dataEntryManager.deleteCoApplicant(applicationRequest, httpRequest),
                HttpStatus.OK);
    }*/

    @PostMapping(EndPointReferrer.DELETE_CO_APPLICANT)
    public ResponseEntity<BaseResponse> deleteCoApplicantData(
            @RequestBody @NotNull @Valid DeleteCoApplicantRequest request, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DELETE_CO_APPLICANT);

        return new ResponseEntity<>(
                dataEntryManager.deleteCoApplicantData(request, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_PERFIOS_DATA)
    public ResponseEntity<BaseResponse> savePerfiosData(
            @RequestBody @NotNull @Valid PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_PERFIOS_DATA);

        return new ResponseEntity<>(
                dataEntryManager.savePerfiosData(perfiosRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_PERFIOS_DATA)
    public ResponseEntity<BaseResponse> getPerfiosData(
            @RequestBody @NotNull @Valid PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_PERFIOS_DATA);

        return new ResponseEntity<>(
                dataEntryManager.getPerfiosData(perfiosRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_MIFIN_LOG)
    public ResponseEntity<BaseResponse> getMifinLog(
            @RequestBody @NotNull @Valid PerfiosRequest mifinRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_MIFIN_LOG);

        return new ResponseEntity<>(
                dataEntryManager.getMifinLogData(mifinRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIGRATE_COAPPLICANT_CIBILSCORE)
    public ResponseEntity<BaseResponse> migrateCoApplicantCibilScore(@RequestBody @NotNull @Valid CoApplicantCibilScoreRequest coApplicantCibilScoreRequest){
        logger.debug("{} started",EndPointReferrer.MIGRATE_COAPPLICANT_CIBILSCORE);


        return new ResponseEntity<>(
                dataEntryManager.getCoApplicantCibilScore(
                        coApplicantCibilScoreRequest.getHeader().getInstitutionId(),coApplicantCibilScoreRequest.getRefIds()),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIGRATE_APPLICANT_PLBS_DATA)
    public ResponseEntity<BaseResponse> migrateCoApplicantPLBSData(@RequestBody @NotNull @Valid CoApplicantCibilScoreRequest coApplicantCibilScoreRequest){
        logger.debug("{} started",EndPointReferrer.MIGRATE_APPLICANT_PLBS_DATA);

        return new ResponseEntity<>(
                dataEntryManager.migrateCoApplicantPLBSData(
                        coApplicantCibilScoreRequest.getHeader().getInstitutionId(),coApplicantCibilScoreRequest.getRefIds()),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_ORIGINATION)
    public ResponseEntity<BaseResponse> saveOriginationDetails(
            @RequestBody @NotNull @Valid OriginationRequest originationRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_ORIGINATION);

        return new ResponseEntity<>(
                dataEntryManager.saveOriginationDetails(originationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_ORIGINATION)
    public ResponseEntity<BaseResponse> fetchOriginationDetails(
            @RequestBody @NotNull @Valid OriginationRequest originationRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_ORIGINATION);

        return new ResponseEntity<>(
                dataEntryManager.fetchOriginationDetails(originationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIGRATE_APP_BRANCHES)
    public ResponseEntity<BaseResponse> migrateApplicationBranches(
            @RequestBody @NotNull @Valid DataMigrationRequest dataMigrationRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MIGRATE_APP_BRANCHES);

        return new ResponseEntity<>(
                dataEntryManager.migrateApplicationBranches(dataMigrationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.VERIFY_STAGE_SUBMIT)
    public ResponseEntity<BaseResponse> verifyStageSubmit(
            @RequestBody @NotNull @Valid StageSubmitVerificationRequest submitVerificationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_STAGE_SUBMIT);

        return new ResponseEntity<>(
                dataEntryManager.verifyStageSubmition(submitVerificationRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CALCULATE_PREMIUM_AMOUNT)
    public ResponseEntity<BaseResponse> calculatePremiumAmount(
            @RequestBody @NotNull @Valid InsuranceRequest insuranceRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CALCULATE_PREMIUM_AMOUNT);

        return new ResponseEntity<>(
                dataEntryManager.calculatePremiumAmount(insuranceRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.INSURANCE_DATA_PUSH)
    public ResponseEntity<BaseResponse> insuranceDataPush(
            @RequestBody @NotNull AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.INSURANCE_DATA_PUSH);

        return new ResponseEntity<>(
                dataEntryManager.insuranceDataPush(adminLogRequest.getRefID(), adminLogRequest.getHeader()),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.POPULATE_REPAYMENT)
    public ResponseEntity<BaseResponse> populateRepaymentDetails(
            @RequestBody @NotNull CamDetailsRequest camDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.POPULATE_REPAYMENT);

        return new ResponseEntity<>(
                dataEntryManager.populateRepaymentDetails(camDetailsRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_LOGIN_DATA)
    public ResponseEntity<BaseResponse> saveLoginData(@Validated({Header.FetchGrp.class})
                                                      @RequestBody @NotNull LoginDataRequest loginDataRequest, HttpServletRequest httpServletRequest) throws Exception{
        logger.debug("{} controller started ", EndPointReferrer.SAVE_LOGIN_DATA);
        return new ResponseEntity<>(
                dataEntryManager.saveLoginData(loginDataRequest,httpServletRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_OCR_DATA)
    public ResponseEntity<BaseResponse> saveOCRData(
            @RequestBody @NotNull @Valid OCRRequest ocrRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.SAVE_OCR_DATA);
        return new ResponseEntity(dataEntryManager.saveOCRData(ocrRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SUBMIT_OCR_DATA)
    public ResponseEntity<BaseResponse> submitOCRData(
            @RequestBody @NotNull @Valid OCRRequest ocrRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.SUBMIT_OCR_DATA);
        return new ResponseEntity(dataEntryManager.submitOCRData(ocrRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESET_OCR_DATA)
    public ResponseEntity<BaseResponse> resetOCRData(
            @RequestBody @NotNull @Valid OCRRequest ocrRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.RESET_OCR_DATA);
        return new ResponseEntity(dataEntryManager.resetOCRData(ocrRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_OCR_DATA)
    public ResponseEntity<BaseResponse> getOCRData(
            @RequestBody @NotNull @Valid OCRRequest ocrRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.GET_OCR_DATA);
        return new ResponseEntity(dataEntryManager.getOCRData(ocrRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RESET_APPLICATION_STATUS)
    public ResponseEntity<BaseResponse> resetApplicationStatus(
            @RequestBody @NotNull @Valid LoginDataRequest loginDataRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.RESET_APPLICATION_STATUS);
        return new ResponseEntity(dataEntryManager.resetApplicationStatus(loginDataRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PERSIST_REPAYMENT_DETAILS)
    public ResponseEntity<BaseResponse> persistRepaymentDetails(
            @Validated(value = {BankingDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid BankingDetailsRequest bankingDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.PERSIST_REPAYMENT_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.persistRepayment(bankingDetailsRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_DIGIPL_ELIGIBILITY_DETAILS)
    public ResponseEntity<BaseResponse> saveDigiPLEligibility(
            @Validated(value = {EligibilityRequest.InsertGroup.class})
            @RequestBody @NotNull EligibilityRequest eligibilityRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_DIGIPL_ELIGIBILITY_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveDigiPLEligibilityDetails(eligibilityRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DIGIPL_ELIGIBILITY_DETAILS)
    public ResponseEntity<BaseResponse> getDigiPLEligibility(

            @Validated(value = {EligibilityRequest.FetchGroup.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid EligibilityRequest eligibilityRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_DIGIPL_ELIGIBILITY_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getDigiPLEligibilityDetails(eligibilityRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_MISMATCH_DETAILS)
    public ResponseEntity<BaseResponse> saveMismatchDetails(
            @RequestBody @NotNull @Valid MismatchRequest mismatchRequest, HttpServletRequest httpRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.SAVE_MISMATCH_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.saveMismatchDetails(mismatchRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_MISMATCH_DETAILS)
    public ResponseEntity<BaseResponse> fetchMismatchDetails(
            @RequestBody @NotNull @Valid MismatchRequest mismatchRequest, HttpServletRequest httpRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.GET_MISMATCH_DETAILS);

        return new ResponseEntity<>(
                dataEntryManager.getMismatchDetails(mismatchRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_KYC_DETAILS)
    public ResponseEntity<BaseResponse> updateKycDetails(
            @RequestBody @NotNull @Valid KycUpdateRequest kycUpdateRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.UPDATE_KYC_DETAILS);
        return new ResponseEntity(dataEntryManager.updateKycDetails(kycUpdateRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.GET_KYC_DETAILS)
    public ResponseEntity<BaseResponse> getKycDetails(
            @RequestBody @NotNull @Valid KycUpdateRequest kycUpdateRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.GET_KYC_DETAILS);
        return new ResponseEntity(dataEntryManager.getKycDetails(kycUpdateRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_APPLICATION_BUCKET)
    public ResponseEntity<BaseResponse> getApplicationBucket(@Validated({Header.FetchGrp.class})
                                                             @RequestBody @NotNull LoginDataRequest loginDataRequest) throws Exception{
        logger.debug("{} controller started ", EndPointReferrer.GET_APPLICATION_BUCKET);
        return new ResponseEntity<>(
                dataEntryManager.getApplicationBucket(loginDataRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_CRM_DEDUPE)
    public ResponseEntity<BaseResponse> getCRMDedupe(@Validated({Header.FetchGrp.class})
                                                     @RequestBody @NotNull LoginDataRequest loginDataRequest) throws Exception{
        logger.debug("{} controller started ", EndPointReferrer.GET_CRM_DEDUPE);
        return new ResponseEntity<>(
                dataEntryManager.fetchCRMDedupe(loginDataRequest),
                HttpStatus.OK);
    }

}