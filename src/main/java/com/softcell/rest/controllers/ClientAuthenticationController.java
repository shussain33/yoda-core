/**
 * yogeshb12:20:15 pm  Copyright Softcell Technolgy
 **/
package com.softcell.rest.controllers;


import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.core.kyc.request.aadhar.ClientAadhaarRequest;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.emudra.ESignRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.security.GetOtpRequest;
import com.softcell.gonogo.model.security.GetOtpResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;


/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ClientAuthenticationController {

    private static final Logger logger = LoggerFactory.getLogger(ClientAuthenticationController.class);

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    /**
     * @param httpHeaders
     * @param getOtpRequest
     * @return OTP number to user
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.GET_OTP)
    public ResponseEntity<BaseResponse> getOtp(
            @Validated(value = {GetOtpRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull GetOtpRequest getOtpRequest,
            @RequestHeader HttpHeaders httpHeaders) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_OTP);

        boolean authFlag = true; //GngUtils.isAuthenticated(httpHeaders);

        GetOtpResponse getOtpResponse= new GetOtpResponse();

        if (!authFlag) {
            getOtpResponse.setStatus(Status.FAILED);

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return new ResponseEntity<>(GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors), HttpStatus.OK);
        }

        return new ResponseEntity<>(clientAuthenticationManager.sendOtp(getOtpRequest), HttpStatus.OK);

    }

    /**
     * This service sends otp through aadhar get-way
     *
     * @param aadharOtpRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_OTP_BY_AADHAR)
    public ResponseEntity<BaseResponse> getOtpByAadhar(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpGrp.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpRequest) throws Exception {

            logger.debug("{} controller started",EndPointReferrer.GET_OTP_BY_AADHAR);

            return new ResponseEntity<>(clientAuthenticationManager.getOtpByAadhar(aadharOtpRequest), HttpStatus.OK);

    }

    /**
     * @param aadharOtpWithOtherDetailsRequest
     * @return This Service verify otp with aadhaar through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR)
    public ResponseEntity<BaseResponse> verifyOtpByAadhar(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpVerificationGrp.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR);
        return new ResponseEntity<>( clientAuthenticationManager
                .verifyOtpByAadharAndGetEkycDeatils(aadharOtpWithOtherDetailsRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_OTP_BY_AADHAR_V2_1)
    public ResponseEntity<BaseResponse> getOtpByAadharV2_1(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpGrp.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.GET_OTP_BY_AADHAR);

        return new ResponseEntity<>(clientAuthenticationManager.getOtpByAadharServiceVersionDecider(aadharOtpRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_OTP_BY_AADHAR_V2_2)
    public ResponseEntity<BaseResponse> getOtpByAadharV2_1_NEW(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpGrp.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.GET_OTP_BY_AADHAR_V2_2);

        return new ResponseEntity<>(clientAuthenticationManager.getOtpByAadharV2_2(aadharOtpRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1)
    public ResponseEntity<BaseResponse> verifyOtpByAadharV2_1(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1);
        return new ResponseEntity<>( clientAuthenticationManager
                .verifyOtpByAadharServiceVersionDecider(aadharOtpWithOtherDetailsRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1_1)
    public ResponseEntity<BaseResponse> verifyOtpByAadharV2_1_1(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_1_1);
        return new ResponseEntity<>( clientAuthenticationManager
                .verifyOtpByAadharServiceVersionDeciderV2_1_1(aadharOtpWithOtherDetailsRequest), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_2)
    public ResponseEntity<BaseResponse> verifyOtpByAadharV2_1NewImpl(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.VERIFY_OTP_WITH_EKYC_DETAILS_BY_AADHAR_V2_2);
        return new ResponseEntity<>( clientAuthenticationManager
                .verifyOtpByAadharAndGetEkycDeatilsV2_2_NEW(aadharOtpWithOtherDetailsRequest), HttpStatus.OK);

    }

    /**
     * @param aadharEkycwithBioRequest
     * @return This Service verify iris through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.EKYC_BIOMETRIC)
    public ResponseEntity<BaseResponse> getEkycByBIO(
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.EKYC_BIOMETRIC);

        return new ResponseEntity<>(
                clientAuthenticationManager.verifyBioByAadharServiceVersionDecider(aadharEkycwithBioRequest),
                HttpStatus.OK);
    }

    /**
     * @param aadharEkycwithBioRequest
     * @return This Service verify iris through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.EKYC_BIOMETRIC_V2_2)
    public ResponseEntity<BaseResponse> getEkycByBIOV22(
            @RequestBody @Valid @NotNull ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.EKYC_BIOMETRIC_V2_2);

        return new ResponseEntity<>(
                clientAuthenticationManager.verifyBioByAadharAndGetEkycDetailsV2_2(aadharEkycwithBioRequest),
                HttpStatus.OK);
    }

    /**
     * @param clientAadhaarRequest
     * @return This Service verify iris through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.EKYC_IRIS_V2_1)
    public ResponseEntity<BaseResponse> getEkycByIRISV2_1(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.IrisVerificationGrp.class,
                    ClientAadhaarRequest.IrisVerificationGrp2_1.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest clientAadhaarRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.EKYC_IRIS);

        return new ResponseEntity<>( clientAuthenticationManager
                .verifyIrisByAadharServiceVersionDecider(clientAadhaarRequest), HttpStatus.OK);
    }


    /**
     * @param clientAadhaarRequest
     * @return This Service verify iris through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.EKYC_IRIS_V2_2)
    public ResponseEntity<BaseResponse> getEkycByIRISV2_2(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.IrisVerificationGrp.class,
                    ClientAadhaarRequest.IrisVerificationGrp2_1.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest clientAadhaarRequest) throws Exception{

        logger.debug("{} controller started", EndPointReferrer.EKYC_IRIS_V2_2);

        return new ResponseEntity<>( clientAuthenticationManager
                .verifyIrisByAadharAndGetEkycDeatilsV2_2(clientAadhaarRequest), HttpStatus.OK);
    }

    /**
     * @param clientAadhaarRequest
     * @return This Service verify iris through aadhar get-way.
     */
    @PostMapping(EndPointReferrer.EKYC_IRIS)
    public ResponseEntity<BaseResponse> getEkycByIRIS(
            @Validated(value = {Header.FetchGrp.class, ClientAadhaarRequest.IrisVerificationGrp.class})
            @RequestBody @Valid @NotNull ClientAadhaarRequest clientAadhaarRequest) {

        logger.debug("{} controller started", EndPointReferrer.EKYC_IRIS);

        return new ResponseEntity<>( clientAuthenticationManager
                .verifyIrisByAadharAndGetEkycDeatils(clientAadhaarRequest), HttpStatus.OK);
    }

    /**
     * @param smsRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SEND_SMS)
    public ResponseEntity<BaseResponse> sendSms(
            @Validated(value = {SmsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull SmsRequest smsRequest) {

        logger.debug("{} controller started",EndPointReferrer.SEND_SMS);

        return new ResponseEntity<>(clientAuthenticationManager.sendSms(smsRequest), HttpStatus.OK);

    }

    /**
     *
     * @param eSignRequest
     * @return
     */
    @PostMapping(EndPointReferrer.E_SIGN_BY_E_MUDRA)
    public ResponseEntity<BaseResponse> esignEMudra(
            @Validated(value = {ESignRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull ESignRequest eSignRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.E_SIGN_BY_E_MUDRA);

        return new ResponseEntity<>(clientAuthenticationManager.doESign(eSignRequest), HttpStatus.OK);

    }

    /**
     *
     * @param eSignRequest
     * @return
     */
    @PostMapping(EndPointReferrer.E_SIGN_E_MANDATE_BY_E_MUDRA)
    public ResponseEntity<BaseResponse> esignemandateEMudra(
            @Validated(value = {ESignRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull ESignRequest eSignRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.E_SIGN_E_MANDATE_BY_E_MUDRA);

        return new ResponseEntity<>(clientAuthenticationManager.doESignAndEMandate(eSignRequest), HttpStatus.OK);

    }


    /**
     *
     * @param eSignRequest
     * @return
     */
    @PostMapping(EndPointReferrer.E_MANDATE_BY_E_MUDRA)
    public ResponseEntity<BaseResponse> eMandateXmlEMudra(
            @Validated(value = {ESignRequest.EMandateGrp.class, Header.FetchGrp.class})
            @RequestBody @Valid @NotNull ESignRequest eSignRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.E_MANDATE_BY_E_MUDRA);

        return new ResponseEntity<>(clientAuthenticationManager.doEMandate(eSignRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.VERIFY_OTP)
    public ResponseEntity<BaseResponse> sendGenericEmailByType(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @Valid @NotNull SmsRequest smsRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.VERIFY_OTP);
        return new ResponseEntity<>(clientAuthenticationManager.verifyOtp(smsRequest), HttpStatus.OK);
    }
}

