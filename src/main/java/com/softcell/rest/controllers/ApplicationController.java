package com.softcell.rest.controllers;


import com.softcell.constants.master.MasterDetails;
import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyPremiumRequest;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.GetInsurancePremiumRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.FileStagingUtils;
import com.softcell.service.*;
import org.apache.commons.io.FileUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.concurrent.Callable;

import static com.softcell.constants.Constant.*;

/**
 * @author kishor
 */
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
@RestController
public class ApplicationController {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
    @Autowired
    DisbursementManager disbursementManager;
    @Autowired
    private ApplicationManager applicationManager;
    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;
    @Autowired
    private QuickDataEntryManager quickDataEntryManager;
    @Autowired
    private FileUploadManager fileUploadManager;

    /**
     * @param applicationRequest
     * @return Application submission Status
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.SUBMIT_APPLICATION)
    public ResponseEntity<BaseResponse> setApp(
            @Validated(value = {ApplicationRequest.FetchGrp.class, Header.FetchGrp.class, Header.InsertWithoutDealerGrp.class})
            @RequestBody @NotNull @Valid ApplicationRequest applicationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SUBMIT_APPLICATION);

        return new ResponseEntity<>(
                applicationManager.submitApplication(applicationRequest),
                HttpStatus.OK);

    }

    /**
     * @param refId
     * @return
     */
    @GetMapping(EndPointReferrer.APP_EXISTS + "/{refId}")
    public ResponseEntity<BaseResponse> appExists(@PathVariable("refId") @NotBlank @Valid final String refId)
            throws Exception {

        logger.debug("App exists request received for application - {} on {}", refId, EndPointReferrer.APP_EXISTS);

        return new ResponseEntity<>(applicationManager.appExists(refId), HttpStatus.OK);
    }

    /**
     * @param checkApplicationStatus
     * @return
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.STATUS)
    public ResponseEntity<BaseResponse> getStatus(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.STATUS);

        return new ResponseEntity<>(applicationManager.getStatus(checkApplicationStatus), HttpStatus.OK);

    }

    /**
     * Get Post IPA Details ,generate pdf,store pdf in gridfs .
     *
     * @param postIpaRequest
     * @return
     * @author yogeshb
     */
    @Deprecated
    @PostMapping(EndPointReferrer.POST_IPA_PDF)
    public ResponseEntity<BaseResponse> setPostIpaPdf(
            @Validated(value = {PostIpaRequest.InsertGrp.class, FileHeader.DsaGrp.class})
            @RequestBody @NotNull @Valid PostIpaRequest postIpaRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.POST_IPA_PDF);

        return new ResponseEntity<>(applicationManager.setpostIpaPdf(postIpaRequest, httpRequest), HttpStatus.OK);

    }

    /**
     * Newly added service which is used to save postIpaRequest in the database.
     *
     * @param postIpaRequest
     * @param httpRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.SAVE_POST_IPA_REQUEST)
    public ResponseEntity<BaseResponse> savePostIpaPdf(
            @Validated(value = {PostIpaRequest.InsertGrp.class, FileHeader.DsaGrp.class})
            @RequestBody @NotNull @Valid PostIpaRequest postIpaRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_POST_IPA_REQUEST);

        return new ResponseEntity<>(applicationManager.savePostIpaRequest(postIpaRequest, httpRequest), HttpStatus.OK);

    }

    /**
     * @param postIpaRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_POST_IPA)
    public ResponseEntity<BaseResponse> getPostIpaPdf(
            @Validated(value = {PostIpaRequest.FetchGrp.class, FileHeader.FetchGrp.class})
            @RequestBody @NotNull @Valid PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_POST_IPA);

        return new ResponseEntity<>(applicationManager.getpostIpaPdf(postIpaRequest), HttpStatus.OK);
    }

    /**
     * @param dashboardRequest
     * @return
     */
    @CrossOrigin
    @PostMapping(EndPointReferrer.DASHBOARD_DETAIL)
    public Callable<ResponseEntity<BaseResponse>> getDashBoardData(
            @Validated(value = {DashboardRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DashboardRequest dashboardRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DASHBOARD_DETAIL);

        return () -> ResponseEntity.status(HttpStatus.OK).body(applicationManager.getDashBoardData(dashboardRequest));

    }

    /**
     * old service
     *
     * @param postIpaRequest
     * @return
     * @throws Exception
     */
    @Deprecated
    @PostMapping(EndPointReferrer.POST_IPA_STAGE_UPDATE)
    public ResponseEntity<BaseResponse> setPostIpaStage(
            @Validated(value = {PostIpaRequest.FetchGrp.class, FileHeader.FetchGrp.class})
            @RequestBody @NotNull @Valid PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.POST_IPA_STAGE_UPDATE);

        return new ResponseEntity<>(applicationManager.setPostIpaStage(postIpaRequest), HttpStatus.OK);

    }

    /**
     * (Newly added service) This service is used to update post ipa stage and generate Delivery Order , store in the GridFs
     *
     * @param postIpaRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.POST_IPA_STAGE_UPDATE_DO_STORE)
    public ResponseEntity<BaseResponse> setPostIpaStageAndStoreDO(
            @Validated(value = {PostIpaRequest.FetchGrp.class, FileHeader.FetchGrp.class})
            @RequestBody @NotNull @Valid PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.POST_IPA_STAGE_UPDATE_DO_STORE);

        return new ResponseEntity<>(applicationManager.setPostIpaStageAndStoreDO(postIpaRequest), HttpStatus.OK);

    }

    /**
     * @param partialSaveRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_PARTIAL_SAVE_REQUEST)
    public ResponseEntity<BaseResponse> getPartialSaveRequest(
            @RequestBody @NotNull @Valid PartialSaveRequest partialSaveRequest) {

        logger.debug("{} controller started", EndPointReferrer.GET_PARTIAL_SAVE_REQUEST);

        return new ResponseEntity<>(applicationManager.getPartialSaveRequest(partialSaveRequest),
                HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_FILE_HANDOVER_DETAILS)
    public ResponseEntity<BaseResponse> getFileHandOverDetails(
            @Validated(value = {FileHandoverDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid FileHandoverDetailsRequest request) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_FILE_HANDOVER_DETAILS);

        return new ResponseEntity<>(applicationManager.getFileHandoverDetails(request), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.SET_FILE_HANDOVER_DETAILS)
    public ResponseEntity<BaseResponse> setFileHandOverDetails(
            @Validated(value = {FileHandoverDetailsRequest.InsertGrp.class, Header.InsertGrp.class})
            @RequestBody @NotNull @Valid FileHandoverDetails fileHandoverDetails) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SET_FILE_HANDOVER_DETAILS);

        return new ResponseEntity<>(applicationManager.setFileHandOverDetails(fileHandoverDetails), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_NEGATIVE_AREA_FUNDING_DETAILS)
    public ResponseEntity<BaseResponse> getNegativeAreaFundingDetails(
            @Validated(value = {NegativeAreaFundingRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid NegativeAreaFundingRequest negativeAreaFundingRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_NEGATIVE_AREA_FUNDING_DETAILS);

        return new ResponseEntity<>(applicationManager.
                getNegativeAreaFundingDetails(negativeAreaFundingRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.APPLICATION_STATUS)
    public ResponseEntity<BaseResponse> getApplicationStatus(
            @Validated(value = {CheckApplicationStatus.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]", EndPointReferrer.APPLICATION_STATUS, checkApplicationStatus.getHeader().getInstitutionId(), checkApplicationStatus.getGonogoRefId());

        return new ResponseEntity<>(applicationManager.getApplicationStatus(checkApplicationStatus), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.UPDATE_REFERENCE_DETAILS)
    public ResponseEntity<BaseResponse> updateReferenceDetails(
            @Validated(value = {UpdateReferencesRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid UpdateReferencesRequest updateReferencesRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_REFERENCE_DETAILS,
                updateReferencesRequest.getHeader().getInstitutionId(),
                updateReferencesRequest.getRefId());

        return new ResponseEntity<>(applicationManager.updateApplicantReferences(updateReferencesRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_BANKING_DETAILS)
    public ResponseEntity<BaseResponse> updateReferenceDetails(
            @Validated(value = {BankingDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid BankingDetailsRequest bankingDetailsRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_BANKING_DETAILS,
                bankingDetailsRequest.getHeader().getInstitutionId(),
                bankingDetailsRequest.getRefId());

        return new ResponseEntity<>(applicationManager.updateBankingDetails(bankingDetailsRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_EXTENDED_WARRANTY_PREMIUM)
    public ResponseEntity<BaseResponse> getExtendedWarrantyPremiumData(
            @Validated(value = {ExtendedWarrantyPremiumRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid ExtendedWarrantyPremiumRequest extendedWarrantyPremiumRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_EXTENDED_WARRANTY_PREMIUM,
                extendedWarrantyPremiumRequest.getHeader().getInstitutionId(),
                extendedWarrantyPremiumRequest.getGngAssetCategory());

        return new ResponseEntity<>(applicationManager.getExtendedWarrantyPremium(extendedWarrantyPremiumRequest), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.UPDATE_EXTENDED_WARRANTY_DETAILS)
    public ResponseEntity<BaseResponse> updateExtendedWarrantyDetails(
            @Validated(value = {ExtendedWarrantyDetails.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ExtendedWarrantyDetails extendedWarrantyDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_EXTENDED_WARRANTY_DETAILS,
                extendedWarrantyDetails.getRefId(),
                extendedWarrantyDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.updateExtendedWarrantyDetails(extendedWarrantyDetails), HttpStatus.OK);

    }

    //This method is only for TVS.
    @PostMapping(EndPointReferrer.UPDATE_EXTENDED_WARRANTY_DETAILS_NSN)
    public ResponseEntity<BaseResponse> updateExtendedWarrantyDetails1(
            @Validated(value = {Header.FetchGrp.class, ExtendedWarrantyDetails.UpdateGrp.class})
            @RequestBody @NotNull @Valid ExtendedWarrantyDetails extendedWarrantyDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_EXTENDED_WARRANTY_DETAILS_NSN,
                extendedWarrantyDetails.getRefId(),
                extendedWarrantyDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.updateExtendedWarrantyDetails(extendedWarrantyDetails), HttpStatus.OK);

    }



    @GetMapping(EndPointReferrer.GET_EXTENDED_WARRANTY_DETAILS + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getExtendedWarrantyDetails(
            @PathVariable("refId") @NotBlank @Valid String refId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_EXTENDED_WARRANTY_DETAILS,
                refId,
                institutionId);

        return new ResponseEntity<>(applicationManager.fetchExtendedWarrantyDetails(refId, institutionId), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.GET_INSURANCE_PREMIUM_AGAINST_CATEGORY)
    public ResponseEntity<BaseResponse> getInsurancePremiumAgainstAssetCategory(
            @Validated(value = {GetInsurancePremiumRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetInsurancePremiumRequest getInsurancePremiumRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_INSURANCE_PREMIUM_AGAINST_CATEGORY,
                getInsurancePremiumRequest.getHeader().getInstitutionId(),
                getInsurancePremiumRequest.getGngAssetCategory());

        return new ResponseEntity<>(applicationManager.getInsurancePremiumAgainstAssetCategory(getInsurancePremiumRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_INSURANCE_PREMIUM_DETAILS)
    public ResponseEntity<BaseResponse> updateInsurancePremiumDetails(
            @Validated(value = {InsurancePremiumDetails.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid InsurancePremiumDetails insurancePremiumDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_INSURANCE_PREMIUM_DETAILS,
                insurancePremiumDetails.getRefId(),
                insurancePremiumDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.updateInsurancePremiumDetails(insurancePremiumDetails), HttpStatus.OK);

    }

    //This method is only for TVS
    @PostMapping(EndPointReferrer.UPDATE_INSURANCE_PROVIDER_PREMIUM_DETAILS)
    public ResponseEntity<BaseResponse> updateInsurancePremiumProviderDetails(
            @Validated(value = {Header.FetchGrp.class, InsurancePremiumDetails.UpdateGrp.class})
            @RequestBody @NotNull @Valid InsurancePremiumDetails insurancePremiumDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_INSURANCE_PROVIDER_PREMIUM_DETAILS,
                insurancePremiumDetails.getRefId(),
                insurancePremiumDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.updateInsurancePremiumDetails(insurancePremiumDetails), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_INSURANCE_PREMIUM_AGAINST_PROVIDER)
    public ResponseEntity<BaseResponse> getInsurancePremiumAgainstProvider(
            @Validated(value = {GetInsurancePremiumRequest.FetchInsurancePremium.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetInsurancePremiumRequest getInsurancePremiumRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_INSURANCE_PREMIUM_AGAINST_PROVIDER,
                getInsurancePremiumRequest.getHeader().getInstitutionId(),
                getInsurancePremiumRequest.getInsuranceType());

        return new ResponseEntity<>(applicationManager.getInsurancePremiumAgainstProvider(getInsurancePremiumRequest), HttpStatus.OK);

    }


    @PostMapping(EndPointReferrer.UPDATE_CREDIT_VIDYA_DETAILS)
    public ResponseEntity<BaseResponse> updateCreditVidyaDetails(
            @Validated(value = {CreditVidyaDetails.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid CreditVidyaDetails creditVidyaDetails) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_CREDIT_VIDYA_DETAILS,
                creditVidyaDetails.getRefId(),
                creditVidyaDetails.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.updateCreditVidyaDetails(creditVidyaDetails), HttpStatus.OK);

    }


    @GetMapping(EndPointReferrer.GET_INSURANCE_DETAILS + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getInsuranceDetailsByRefId(
            @PathVariable("refId") @NotBlank @Valid String refId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_INSURANCE_DETAILS,
                refId,
                institutionId);

        return new ResponseEntity<>(applicationManager.fetchInsuranceDetailsByRefId(refId, institutionId), HttpStatus.OK);

    }

    @GetMapping(EndPointReferrer.GET_CREDIT_VIDYA_DETAILS + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getCreditVidyaDetailsByRefId(
            @PathVariable("refId") @NotBlank @Valid String refId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_CREDIT_VIDYA_DETAILS,
                refId,
                institutionId);

        return new ResponseEntity<>(applicationManager.fetchCreditVidyaDetailsByRefId(refId, institutionId), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_REVISED_EMI_AMOUNT)
    public ResponseEntity<BaseResponse> updateRevisedEmiAmount(
            @Validated(value = {UpdateRevisedEmiRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid UpdateRevisedEmiRequest updateRevisedEmiRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_REVISED_EMI_AMOUNT,
                updateRevisedEmiRequest.getHeader().getInstitutionId(),
                updateRevisedEmiRequest.getRefId());

        return new ResponseEntity<>(applicationManager.updateRevisedEmiAmount(updateRevisedEmiRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_GST_DETAILS)
    public ResponseEntity<BaseResponse> updateGstDetails(
            @Validated(value = {GstDetailsRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid GstDetailsRequest gstDetailsRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_GST_DETAILS,
                gstDetailsRequest.getHeader().getInstitutionId(),
                gstDetailsRequest.getRefId());

        return new ResponseEntity<>(applicationManager.updateGstDetails(gstDetailsRequest), HttpStatus.OK);

    }

    /**
     * @param customerFinancialProfileRequest
     * @return Customer financial profile submit status.
     */
    @PostMapping(EndPointReferrer.SUBMIT_CUSTOMER_FINANCIAL_PROFILE)
    public ResponseEntity<BaseResponse> submitCustomerFinancialProfile(
            @RequestBody @NotNull @Valid final CustomerFinancialProfileRequest customerFinancialProfileRequest)
            throws Exception {

        logger.debug("Submit customer financial profile request received for application - {} on {}",
                customerFinancialProfileRequest.getRefId(), EndPointReferrer.SUBMIT_CUSTOMER_FINANCIAL_PROFILE);

        return new ResponseEntity<>(applicationManager.submitCustomerFinancialProfile(customerFinancialProfileRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.DISBURSE_LOAN)
    public ResponseEntity<BaseResponse> disburseLoan(
            @Validated(value = {AppDisbursalRequest.FetchGrp.class, Header.FetchGrp.class
                    , Header.InsertWithoutDealerGrp.class})
            @RequestBody @NotNull @Valid AppDisbursalRequest applicationRequest) throws Exception {
        logger.debug("{} controller ", EndPointReferrer.DISBURSE_LOAN);
        ResponseEntity<BaseResponse> baseResponse = new ResponseEntity<>(
                disbursementManager.disburseLoan(applicationRequest),
                HttpStatus.OK);
        return baseResponse;
    }

    @PostMapping(EndPointReferrer.QUICK_DATA_ENTRY_STEPID)
    public ResponseEntity<BaseResponse> quickDecisionMaking(
            @RequestBody ApplicationRequest applicationRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.QUICK_DATA_ENTRY_STEPID);

        return new ResponseEntity<>(
                quickDataEntryManager.submitQuickData(applicationRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.INITIALIZE_POST_IPA)
    public ResponseEntity<BaseResponse> initializePostIPA(
            @Validated(value = {SchemeMetadataRequest.FetchGrp.class, Header.FetchWithDealerCriteriaGrp.class
                    , Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid SchemeMetadataRequest schemeMetadataRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.INITIALIZE_POST_IPA);
        return new ResponseEntity<>(applicationManager.initializePostIpa(schemeMetadataRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GEN_SAP_FILE)
    public ResponseEntity<BaseResponse> generateSapFile(
            @Validated(value = {GenSapFileRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GenSapFileRequest genSapFileRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GEN_SAP_FILE);
        return new ResponseEntity<>(disbursementManager.genSapFile(genSapFileRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_INSURANCE_PREMIUM)
    public ResponseEntity<BaseResponse> getInsurancePremium(
            @Validated(value = {GetInsurancePremiumRequest.GetInsurancePremium.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetInsurancePremiumRequest getInsurancePremiumRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{}]",
                EndPointReferrer.GET_INSURANCE_PREMIUM,
                getInsurancePremiumRequest.getHeader().getInstitutionId());

        return new ResponseEntity<>(applicationManager.getInsurancePremium(getInsurancePremiumRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_REVISED_AMOUNT_ON_ADDON)
    public ResponseEntity<BaseResponse> updateAddonServiceRevisedAmount(
            @Validated(value = {UpdateRevisedEmiRequest.FetchGrp.class, Header.FetchGrp.class, UpdateRevisedEmiRequest.AddOnGrp.class})
            @RequestBody @NotNull @Valid UpdateRevisedEmiRequest updateRevisedEmiRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.UPDATE_REVISED_AMOUNT_ON_ADDON,
                updateRevisedEmiRequest.getHeader().getInstitutionId(),
                updateRevisedEmiRequest.getRefId());

        return new ResponseEntity<>(applicationManager.updateAddonServiceRevisedAmount(updateRevisedEmiRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.CHECK_PINCODE_GEO_LIMIT)
    public ResponseEntity<BaseResponse> checkPinCodeGeographicalLimit(
    @Validated(value = {GeoLimitRequest.FetchGrp.class})
    @RequestBody @NotNull @Valid GeoLimitRequest geoLimitRequest) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.CHECK_PINCODE_GEO_LIMIT,
                geoLimitRequest.getHeader().getInstitutionId(),
                geoLimitRequest.getHeader().getDealerId());

        return new ResponseEntity<>(applicationManager.checkPinCodeGeographicalLimit(geoLimitRequest), HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.API_ROLE_AUTHORISATION_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadApiRoleAuthorisationMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId,
            @RequestParam @NotBlank String sourceId,
            @RequestParam(required = false, defaultValue = USER_EMAIL) String emailId,
            @RequestParam(required = false, defaultValue = USER_NAME) String userName,
            @RequestParam(required = false, defaultValue = USER_ROLE) String userRole,
            @RequestParam(required = false) String deviceInfo) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.API_ROLE_AUTHORISATION_MASTER);

        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
        String masterName = MasterDetails.API_ROLE_AUTHORISATION_MASTER.name();
        MasterActivityDetails masterActivityDetails = fileUploadManager.masterActivityDetailsInitializer(institutionId,masterName, emailId, userName, userRole, deviceInfo);
        ResponseEntity<BaseResponse> response = fileUploadManager.isInvalidUpload(institutionId, uploadedFile,masterActivityDetails);
        if (null!=response) {
            return response;
        }

        BaseResponse baseResponse = fileUploadManager.uploadApiRoleAuthorisationMaster(uploadedFile, institutionId, masterActivityDetails,sourceId);

        FileUtils.deleteQuietly(uploadedFile);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

}
