package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.FileParserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.MASTER_PARSER_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class FileParserController {

    private Logger logger = LoggerFactory.getLogger(FileParserController.class);

    @Autowired
    private FileParserManager fileParserManager;

    @PostMapping(EndPointReferrer.PARSE_SCHEME_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserSchememaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/scheme-master");
        return new ResponseEntity<>(fileParserManager.parseSchemesMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_ASSET_MODEL_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserAssetModelMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/asset-model-master");
        return new ResponseEntity<>(fileParserManager.parseAssetModelMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_PINCODE_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserPinCodeMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/pincode-master");
        return new ResponseEntity<>(fileParserManager.parsePincodeMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_EMPLOYER_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserEmployerMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/employer-master");
        return new ResponseEntity<>(fileParserManager.parseEmployerMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_DEALER_EMAIL_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserDealerEmailMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/dealer-email-master");
        return new ResponseEntity<>(fileParserManager.parseDealerEmailMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_SCHEME_MODEL_DEALER_CITY_MAPPING_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserSchemeModelDealerCityMappingMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/scheme-model-dealer-City-mapping-master");
        return new ResponseEntity<>(fileParserManager.parseSchemeModelDealerCityMappingMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_SCHEME_DATE_MAPPING_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserSchemeDateMappingMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/scheme-date-mapping-master");
        return new ResponseEntity<>(fileParserManager.parseSchemeDateMappingMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_HITACHI_SCHEME_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserHitachiSchemeMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/hitachi-scheme-master");
        return new ResponseEntity<>(fileParserManager.parseHitachiSchemeMaster(file, institutionId), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PARSE_CAR_SURROGATE_MASTER)
    public ResponseEntity<FileUploadStatus> handleFileParserCarSurrogateMaster(
            @RequestParam("file") MultipartFile file, @RequestParam("institutionId") String institutionId) {
        logger.info("inside master/parser/car-surrogate-master");
        return new ResponseEntity<>(fileParserManager.parseCarSurrogateMaster(file, institutionId), HttpStatus.OK);
    }
}
