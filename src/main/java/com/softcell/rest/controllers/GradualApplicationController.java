package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.GradualApplicationManager;
import com.softcell.service.validator.ApplicationValidationEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author vinodk
 *         <p/>
 *         This class saves the application in steps
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE

)
public class GradualApplicationController {

    private static final Logger logger = LoggerFactory.getLogger(GradualApplicationController.class);

    @Autowired
    private GradualApplicationManager gradualApplicationManager;

    @Autowired
    private ApplicationValidationEngine applicationValidationEngine;

    /**
     * @param applicationRequest application to be saved.
     * @param stepId             step number
     * @param httpRequest
     * @return saved application request with generated unique id's
     */
    @PostMapping(EndPointReferrer.SUBMIT_APPLICATION_STEPID)
    public ResponseEntity<BaseResponse> saveApplication(
            @RequestBody ApplicationRequest applicationRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started",EndPointReferrer.SUBMIT_APPLICATION_STEPID);

        return new ResponseEntity<>(
                gradualApplicationManager.initApplicationLifeCycle(applicationRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    /**
     * validate application request
     *
     * @param applicationRequest
     * @return
     */
    @PostMapping(EndPointReferrer.APPLICATION_REQUEST_VALIDATION)
    public ResponseEntity<BaseResponse> validateApplicationRequest(
            @RequestBody ApplicationRequest applicationRequest) {

        logger.debug("{} controller started",EndPointReferrer.APPLICATION_REQUEST_VALIDATION);

        return new ResponseEntity<>(applicationValidationEngine.validate(applicationRequest), HttpStatus.OK);

    }
}
