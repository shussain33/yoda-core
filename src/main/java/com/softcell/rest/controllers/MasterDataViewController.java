package com.softcell.rest.controllers;

import com.softcell.gonogo.model.masters.SchemeDetailsDateMapping;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.DeviationMasterRequest;
import com.softcell.gonogo.model.request.master.GetAllElecServProvRequest;
import com.softcell.gonogo.model.request.master.HierarchyMasterRequest;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostDetails;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.DeleteCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.InsertCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.SourcingDetailsRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.UpdateCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.GetSchemeZipReportRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.*;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.MasterDataViewManager;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;



/**
 * @author mahesh
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.MASTER_VIEW_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class MasterDataViewController {

    private static final Logger logger = LoggerFactory.getLogger(MasterDataViewController.class);

    @Autowired
    private MasterDataViewManager masterDataViewManager;


    @Deprecated //Mongo does not contain this collection (this is in UM)
    @GetMapping(EndPointReferrer.DEALER_BRANCH_MASTER + "/{sInstitutionId}/{sProduct}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getDealerBranchMasterDetails(
            @PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
            @PathVariable("sProduct") @NotNull @Valid String product,
            @PathVariable("iSkip") Integer skip,
            @PathVariable("iLimit") Integer limit) {


        return ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .viewDealerBranchMasterDetails(institutionId,product, limit, skip));
    }


    @CrossOrigin
    @GetMapping(EndPointReferrer.GET_ASSET_MODEL_MASTER+ "/{sInstitutionId}/{sProduct}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getAssetModelMasterDetails(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                   @PathVariable("sProduct") @Valid @NotNull String product,
                                                                   @PathVariable("iSkip") @Valid Integer skip,
                                                                   @PathVariable("iLimit") @Valid  Integer limit) {

        return new ResponseEntity<>(masterDataViewManager
                .viewAssetModelMasterDetails(institutionId,product, limit, skip), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_ALL_SCHEMEIDS)
    public ResponseEntity<BaseResponse> getAllSchemeIds(
            @Validated(value = {Header.InstWithProductGrp.class, SchemeMasterRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SchemeMasterRequest schemeMasterRequest) {

        logger.info("Inside Get All SchemeId's");

        return  ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager.getAllSchemeIds(schemeMasterRequest));
    }


    @PostMapping(EndPointReferrer.UPDATE_SCHEME_DATE_MAPPING_DETAILS)
    public ResponseEntity<BaseResponse> updateSchemeDetails(
            @Validated(value = {Header.InstWithProductGrp.class, SchemeDetailsDateMapping.FetchGrp.class})
            @RequestBody @NotNull @Valid UpdateSchemeDateMappingDetailsRequest updateSchemeDetailsRequest) throws Exception {

        logger.info("Inside Update Scheme date mapping details");

        return ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.updateSchemeDateMappingDetails(updateSchemeDetailsRequest));

    }

    @PostMapping(EndPointReferrer.UPDATE_DEALER_SCHEME_MAPPING_DETAILS)
    public ResponseEntity<BaseResponse> updateDealerSchemeMappingDetails(
            @Validated(value = {Header.InstWithProductGrp.class, SchemeDealerMappingDetails.FetchGrp.class})
            @RequestBody @NotNull @Valid SchemeDealerMappingDetails schemeDealerMappingDetails) throws Exception {

        logger.info("Inside Update Dealer Scheme Mapping Details");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .updateSchemeDealerMappingDetails(schemeDealerMappingDetails));
    }

    @CrossOrigin
    @PostMapping(EndPointReferrer.FETCH_DEALER_SCHEME_MAPPING_DETAILS)
    public ResponseEntity<BaseResponse> getDealerSchemeMappingDetails(
            @Validated(value = {Header.InstWithProductGrp.class, GetSchemeDealerMappingRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetSchemeDealerMappingRequest getSchemeDealerMappingRequest) throws Exception {

        logger.info("Inside Fetch Dealer Scheme Mapping details");

        return  ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getSchemeDealerMappingDetails(getSchemeDealerMappingRequest));
    }

    @PostMapping(EndPointReferrer.GET_SCHEME_DATE_MAPPING_DETAILS)
    public ResponseEntity<BaseResponse> getSchemeDateMappingDeatils(
            @Validated(value = {Header.InstWithProductGrp.class, GetSchemeDateMappingDetailsRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetSchemeDateMappingDetailsRequest schemeDateMappingDetailsRequest) throws Exception {

        logger.info("Inside scheme date mapping details");

        return  ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getSchemeDateMappingDeatils(schemeDateMappingDetailsRequest));
    }


    @PostMapping(EndPointReferrer.VALIDATE_ASSET_COST)
    public ResponseEntity<BaseResponse> validateAssetCost(
            @Validated(value = {Header.InstWithProductGrp.class, AssetCostDetails.FetchGrp.class})
            @RequestBody @NotNull @Valid AssetCostValidationRequest assetCostValidationRequest) {

        logger.info("Inside Validate Asset Cost Service");

        return  ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .validateAssetCost(assetCostValidationRequest));
    }

    @CrossOrigin
    @GetMapping(EndPointReferrer.GET_PINCODE_MASTER+ "/{sInstitutionId}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getpinCodeMasterDetails(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                @PathVariable("iSkip") Integer skip,
                                                                @PathVariable("iLimit") Integer limit) {
        logger.info("Inside Get Pincode Master");

        return new ResponseEntity<>(masterDataViewManager
                .viewPinCodeMasterDetails(institutionId, limit, skip), HttpStatus.OK);
    }

    /**
     * @param institutionId
     * @param skip
     * @param limit
     * @return
     */
    @CrossOrigin
    @GetMapping(EndPointReferrer.GET_EMPLOYER_MASTER+ "/{sInstitutionId}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getEmployerMasterDetails(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                 @PathVariable("iSkip") Integer skip,
                                                                 @PathVariable("iLimit") Integer limit) {

        logger.info("Inside Get Employer Master");


        return new ResponseEntity<>(masterDataViewManager
                .viewEmployerMasterDetails(institutionId, limit, skip), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(EndPointReferrer.GET_DEALER_EMAIL_MASTER+ "/{sInstitutionId}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getDealerEmailMasterDetails(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                    @PathVariable("iSkip") Integer skip,
                                                                    @PathVariable("iLimit") Integer limit) {

        logger.info("Inside Get Dealer Email Master");

        return new ResponseEntity<>(masterDataViewManager
                .viewDealerEmailMasterDetails(institutionId, limit, skip),
                HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(EndPointReferrer.GET_SCHEME_DEALER_CITY_MAPPING+ "/{sInstitutionId}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getSchemeDealerCityMappingDetails(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                          @PathVariable("iSkip") Integer skip,
                                                                          @PathVariable("iLimit") Integer limit) {

        logger.info("Inside Get Scheme Deaaler City Mapping Master");

        return new ResponseEntity<>(masterDataViewManager.viewSchemeModelDealerCityMappingMaster(institutionId, limit, skip)
                , HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_MANFC_AGAINST_CCID)
    public ResponseEntity<BaseResponse> getManufacturerAgainstCcid(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetSchemeInformationRequest getSchemeInformationRequest) throws Exception {

        logger.info("Inside Get All Manufacturer Against CCID");

        return ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getManufacturerAgainstCcid(getSchemeInformationRequest));
    }

    @PostMapping(EndPointReferrer.GET_ALL_STATES)
    public ResponseEntity<BaseResponse> getAllStates(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetAllStateRequest allStateRequest) throws Exception {

        logger.info("Inside Get All States");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.getAllState(allStateRequest
                .getHeader().getInstitutionId()));
    }

    @PostMapping(EndPointReferrer.GET_ALL_CITY_AGAINST_STATE)
    public ResponseEntity<BaseResponse> getAllCitiesAgainstState(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllCityAgainstStateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllCityAgainstStateRequest getAllCityAgainstStateRequest) {

        logger.info("Inside Get All City Against State");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getAllCityAgainstState(getAllCityAgainstStateRequest));
    }



    @PostMapping(EndPointReferrer.GET_ASSETCTG_AGAINST_MANFCR)
    public ResponseEntity<BaseResponse> getAssetCtgAgainstManfc(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllAssetCtgrAgainstMnfcRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllAssetCtgrAgainstMnfcRequest getAllAssetCtgrAgainstMnfcRequest) {

        logger.info("Inside Get Asset Category Against Manufacturer");

        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getAllAssetCategoryAgainstManfctr(getAllAssetCtgrAgainstMnfcRequest));

    }

    @PostMapping(EndPointReferrer.GET_MODELNO_AGAINST_CTG_MNFC_MAKE)
    public ResponseEntity<BaseResponse> getModelNoAgainstAsstManf(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllModelAgainstAsstCtgRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllModelAgainstAsstCtgRequest getAllModelAgainstAsstCtgRequest) {

        logger.info("Inside Get ModelNo Category Against Manufacturer_Category_Make");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getAllModelAgainstAsstCtgMnfc(getAllModelAgainstAsstCtgRequest));
    }

    @PostMapping(EndPointReferrer.GET_ALL_DEALER_AGAINST_CITY)
    public ResponseEntity<BaseResponse> getDealerAgainstCity(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllDealerAgainstCityRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllDealerAgainstCityRequest getAllDealerAgainstCityRequest) {

        logger.info("Inside Get All Dealer against City");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getAllDealerAgainstCity(getAllDealerAgainstCityRequest));

    }

    @PostMapping(EndPointReferrer.GET_ALL_MAKE_AGAINST_MNFC_CTG)
    public ResponseEntity<BaseResponse> getMakeAgainstMnfcCtg(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllAssetMakeAgainstMnfCtgRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllAssetMakeAgainstMnfCtgRequest getAllAssetMakeAgainstMnfCtgRequest) throws Exception {

        logger.info("Inside Get All Make Against Manufacturer_Category");

        return ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getAssetMakeAgainstMnfcAndCtg(getAllAssetMakeAgainstMnfCtgRequest));

    }

    @PostMapping(EndPointReferrer.GET_SCHEMES_AGAINST_CONDITION)
    public ResponseEntity<BaseResponse> getConditionalScheme(
            @Validated(value = {Header.InstWithProductGrp.class, GetConditionalSchemeRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetConditionalSchemeRequest getConditionalSchemeRequest) {

        logger.info("Inside Get Scheme Against Condition");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .getConditionalScheme(getConditionalSchemeRequest));
    }

    @PostMapping(EndPointReferrer.GET_ALL_MANUFACTURER)
    public ResponseEntity<BaseResponse> getAllManufacturer(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetAllManufacturerRequest getAllManufacturerRequest) {
        logger.info("Inside Get All Manufacturer");

        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getAllManufacturer(getAllManufacturerRequest));
    }

    @PostMapping(EndPointReferrer.GET_ALL_APPLICABLE_SCHEME)
    public ResponseEntity<BaseResponse> getAllApplicableScheme(
            @Validated(value = {Header.InstWithProductGrp.class, GetAllApplicableSchemeRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid GetAllApplicableSchemeRequest getAllApplicableSchemeRequest) {

        logger.info("Inside Get All Applicable Scheme");

        BaseResponse baseResponse;
        List<ApplicableSchemeDetails> applicableSchemeDetailsList = masterDataViewManager
                .getAllApplicableScheme(getAllApplicableSchemeRequest);

        if (null != applicableSchemeDetailsList && !applicableSchemeDetailsList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicableSchemeDetailsList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return  ResponseEntity.status(HttpStatus.OK).body(baseResponse);
    }

    @PostMapping(EndPointReferrer.APPROVE_DECLINE_SCHEME_STATUS)
    public ResponseEntity<BaseResponse> approveDeclineSchemeStatus(
            @Validated(value = {Header.InstWithProductGrp.class, SchemeApproveDeclineRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid SchemeApproveDeclineRequest schemeApproveDeclineRequest) throws Exception {

        logger.info("Inside Approve/Decline Scheme Status");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager
                .approveDeclineSchemeStatus(schemeApproveDeclineRequest));
    }

    @PostMapping(value = EndPointReferrer.DOWNLOAD_SCHEME_REPORT)
    public ResponseEntity<byte []> csvZipReport(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetSchemeZipReportRequest getSchemeZipReportRequest)
            throws Exception {
        logger.info("Inside DownLoad Zip Report.");

        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.getCsvZipReport(getSchemeZipReportRequest));
    }

    @PostMapping(value = EndPointReferrer.DELETE_CGP_MASTER_RECORD)
    public ResponseEntity<BaseResponse> deleteCommonGeneralParameterMaster(
            @Validated(value = {Header.FetchGrp.class, DeleteCommonGeneralMasterRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid DeleteCommonGeneralMasterRequest deleteCommonGeneralMasterRequest)
            throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.DELETE_CGP_MASTER_RECORD,
                deleteCommonGeneralMasterRequest.getHeader().getInstitutionId(),
                deleteCommonGeneralMasterRequest.getUniqueId());


        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.deleteCommonGeneralParameterMaster(deleteCommonGeneralMasterRequest));
    }

    @PostMapping(value = EndPointReferrer.UPDATE_CGP_MASTER_RECORD)
    public ResponseEntity<BaseResponse> updateCommonGeneralParameterMaster(
            @Validated(value = {Header.FetchGrp.class ,UpdateCommonGeneralMasterRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid UpdateCommonGeneralMasterRequest  updateCommonGeneralMasterRequest)
            throws Exception {

        logger.debug(" {} controller started with parameters as [{} ,{}]",
                EndPointReferrer.UPDATE_CGP_MASTER_RECORD,
                updateCommonGeneralMasterRequest.getHeader().getInstitutionId(),
                updateCommonGeneralMasterRequest.getUniqueId());


        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.updateCommonGeneralParameterMaster(updateCommonGeneralMasterRequest));
    }

    @PostMapping(value = EndPointReferrer.INSERT_CGP_MASTER_RECORD)
    public ResponseEntity<BaseResponse> insertCGPMasterRecord(
            @Validated(value = {Header.FetchGrp.class ,InsertCommonGeneralMasterRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid InsertCommonGeneralMasterRequest insertCommonGeneralMasterRequest)
            throws Exception {

        logger.debug(" {} controller started with parameters as [{} ,{}]",
                EndPointReferrer.INSERT_CGP_MASTER_RECORD,
                insertCommonGeneralMasterRequest.getHeader().getInstitutionId());


        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.insertCommonGeneralParameterMaster(insertCommonGeneralMasterRequest));
    }

    @GetMapping(EndPointReferrer.VIEW_CGP_MASTER+ "/{sInstitutionId}/{iSkip}/{iLimit}")
    public ResponseEntity<BaseResponse> getCommonGeneralParameterMaster(@PathVariable("sInstitutionId") @NotNull @Valid String institutionId,
                                                                   @PathVariable("iSkip") @Valid Integer skip,
                                                                   @PathVariable("iLimit") @Valid  Integer limit) {

        logger.debug(" {} controller started with parameters as [{}]",
                EndPointReferrer.VIEW_CGP_MASTER, institutionId);

        return new ResponseEntity<>(masterDataViewManager
                .getCommonGeneralMasterRecord(institutionId, limit, skip), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.GET_SOURCING_DETAILS)
    public ResponseEntity<BaseResponse> getSourcingDetails(
            @Validated(value = {Header.InstWithDsaGrp.class})
            @RequestBody @NotNull @Valid SourcingDetailsRequest sourcingDetailsRequest)
            throws Exception {

        logger.debug(" {} controller started with parameters as [{} ,{}]",
                EndPointReferrer.GET_SOURCING_DETAILS,
                sourcingDetailsRequest.getHeader().getInstitutionId(),
                sourcingDetailsRequest.getHeader().getDsaId() );


        return  ResponseEntity.status(HttpStatus.OK).body(masterDataViewManager.getSourcingDetails(sourcingDetailsRequest));
    }

    /**
     * Get All Electrcity Service Providers from master
     * @param getAllElecServProvRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_ELEC_SERV_PROVIDERS)
    public ResponseEntity<BaseResponse> getAllElecServProviders(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid GetAllElecServProvRequest getAllElecServProvRequest) {
        logger.info("Inside Get All Electricity Service Providers");
        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getAllElecServProviders(getAllElecServProvRequest));
    }

    /**
     * Get All AgencyMaster from DealerBranchMaster
     * @param agencyMasterRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_DEVIATION_MASTER)
    public ResponseEntity<BaseResponse> getAgencyMaster(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DeviationMasterRequest agencyMasterRequest) {
        logger.info("Get agency master :");
        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getAllDeviationMasters(agencyMasterRequest));
    }

    @PostMapping(EndPointReferrer.GET_ORGANISATIONAL_HIERARCHY_MASTER)
    public ResponseEntity<BaseResponse> getOrganizationalHierarchyMaster(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid HierarchyMasterRequest hierarchyMasterRequest) {
        logger.info("Get hierarchy master :");
        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager
                .getAllHierarchyMasters(hierarchyMasterRequest));
    }

    @PostMapping(EndPointReferrer.GET_ROI_SCHEME_MASTER_DATA)
    public ResponseEntity<BaseResponse> getRoiSchemeMasterData(
            @Validated(value = {Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid HierarchyMasterRequest hierarchyMasterRequest) {
        logger.info("Inside get ROIScheme master");
        return ResponseEntity.status( HttpStatus.OK).body(masterDataViewManager.getRoiSchemeMasterData(hierarchyMasterRequest));
    }
}
