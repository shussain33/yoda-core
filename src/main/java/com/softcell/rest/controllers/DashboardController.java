package com.softcell.rest.controllers;

import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DashboardManger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.concurrent.Callable;

/**
 * Created by AmitBotre on 30/12/19.
 */
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
@RestController
public class DashboardController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    DashboardManger dashboardManger;

    @CrossOrigin
    @PostMapping(EndPointReferrer.DASHBOARD_DETAIL_V2)
    public Callable<ResponseEntity<BaseResponse>> getDashBoardData(
            @Validated(value = {DashboardRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DashboardRequest dashboardRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.DASHBOARD_DETAIL_V2);

        return () -> ResponseEntity.status(HttpStatus.OK).body(dashboardManger.getDashBoardData(dashboardRequest));
    }
}
