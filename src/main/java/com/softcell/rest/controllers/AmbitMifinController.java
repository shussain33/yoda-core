package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.AmbitMifinRequest.AmbitMifinLog;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.UpdateDedupeRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AmbitMifinManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(
        value = EndPointReferrer.MIFIN,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class AmbitMifinController {

    @Autowired
    AmbitMifinManager ambitMifinManager;

    @PostMapping(EndPointReferrer.PAN_SEARCH)
    public ResponseEntity<BaseResponse> panCardSearch(
            @RequestBody ApplicationRequest applicationRequest) throws Exception {

        log.debug("{} controller started ", EndPointReferrer.PAN_SEARCH);

        return new ResponseEntity<>(ambitMifinManager.panSearchAndSearchExistingApplicant(applicationRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MIFIN_GONOGO_LOGS)
    public ResponseEntity<BaseResponse> getMifinAmbitLogs(
            @RequestBody AmbitMifinLog ambitMifinLog) throws Exception {

        log.debug("{} controller started ", EndPointReferrer.MIFIN_GONOGO_LOGS);

        return new ResponseEntity<>(ambitMifinManager.getMifinAmbitLogs(ambitMifinLog), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_PROCESS_DEDUPE)
    public ResponseEntity<BaseResponse> updateprocessDedupe(
            @RequestBody UpdateDedupeRequest updateDedupeRequest) throws Exception {

        log.debug("{} controller started ", EndPointReferrer.UPDATE_PROCESS_DEDUPE);

        return new ResponseEntity<>(ambitMifinManager.updateProcessDedupe( updateDedupeRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_FINANCIAL_AND_FETCH_DEDUPE)
    public ResponseEntity<BaseResponse> saveFinancialAndFetchDedupe(
            @RequestBody ApplicationRequest applicationRequest) throws Exception {

        log.debug("{} controller started ", EndPointReferrer.SAVE_FINANCIAL_AND_FETCH_DEDUPE);

        return new ResponseEntity<>(ambitMifinManager.saveFinancialAndFetchDedupe(applicationRequest), HttpStatus.OK);
    }
}
