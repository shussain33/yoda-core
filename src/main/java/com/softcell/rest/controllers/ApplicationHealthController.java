package com.softcell.rest.controllers;


import com.softcell.config.ApplicationEnvConfiguration;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ApplicationHealthManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.APPLICATION_HEALTH_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE

)
public class ApplicationHealthController {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationHealthController.class);

    @Autowired
    private ApplicationHealthManager applicationHealthManager;

    /**
     * @return <code></>ResponseEntity {@see Object}</code>
     */

    @GetMapping(EndPointReferrer.DB_STATS)
    public ResponseEntity<Object> getDBLog() throws Exception {
        try {
            if (ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE) {
                return new ResponseEntity<>(
                        applicationHealthManager.getDBStat(),
                        HttpStatus.NOT_FOUND);
            }
            if (null != applicationHealthManager.getDBStat()) {
                return new ResponseEntity<>(
                        applicationHealthManager.getDBStat(), HttpStatus.OK);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(applicationHealthManager.getDBStat(),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(applicationHealthManager.getDBStat(),
                HttpStatus.NOT_FOUND);
    }

    /**
     * @return
     */
    @GetMapping(EndPointReferrer.DB_COLLECTIONS)
    public ResponseEntity<Object> getDBCollections() throws Exception {

        return new ResponseEntity<>(applicationHealthManager.getCollectionNames(), HttpStatus.OK);

    }

    /**
     * @return
     */
    @GetMapping(EndPointReferrer.DB_SERVERSTATUS)

    public ResponseEntity<Object> getDBServerStatus() throws Exception {

        return new ResponseEntity<>(applicationHealthManager.getServerStatus(), HttpStatus.OK);

    }

    /**
     * @return
     */
    @GetMapping(EndPointReferrer.DB_HOSTINFO)
    public ResponseEntity<Object> getDBhostInfo() throws Exception {

        return new ResponseEntity<>(applicationHealthManager.gethostInfo(), HttpStatus.OK);

    }

    /**
     * @param header
     * @return
     */
    @PostMapping(EndPointReferrer.EXTERNAL_SERVICES_STATUS)
    public ResponseEntity<Object> getServiceStatus(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid Header header) {
        Object object = applicationHealthManager.getExternalServicesStatus(header);
        if (null != object) {
            return new ResponseEntity<>(
                    object,
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(object, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * @param loginRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SET_APPLICATION_PASSIVE_MODE)
    public ResponseEntity<Boolean> getSetServerInPassiveMode(
            @RequestBody LoginRequest loginRequest) {
        if (loginRequest.getUserName().equalsIgnoreCase("admintext")
                && loginRequest.getPassword().equalsIgnoreCase("admintext")) {
            ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE = true;
            return new ResponseEntity<>(
                    ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE,
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE,
                    HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * @param loginRequest
     * @return
     */
    @PostMapping(EndPointReferrer.SET_APPLICATION_ACTIVE_MODE)
    public ResponseEntity<Boolean> getSetServerInActiveMode(
            @RequestBody LoginRequest loginRequest) {
        if (loginRequest.getUserName().equalsIgnoreCase("admintext")
                && loginRequest.getPassword().equalsIgnoreCase("admintext")) {
            ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE = false;
            return new ResponseEntity<>(
                    ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE,
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    ApplicationEnvConfiguration.IS_APPLICATION_IN_PASSIVE_MODE,
                    HttpStatus.BAD_REQUEST);
        }

    }

}
