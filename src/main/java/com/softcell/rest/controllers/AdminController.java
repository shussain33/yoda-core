package com.softcell.rest.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.gonogo.model.core.CancelDoNotRaisedApplicationRequest;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.ModifyAppStageAndStatusRequest;
import com.softcell.gonogo.model.sms.SmsServReqResLogRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.FileStagingUtils;
import com.softcell.service.AdminManager;
import com.softcell.service.FileUploadManager;
import org.apache.commons.io.FileUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;


/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminManager adminManager;

    @Autowired
    private FileUploadManager fileUploadManager;
    /**
     * To get Pan Request Json.
     *
     * @param adminLogRequest
     * @return PanRequest
     * @author yogeshb
     */
    /*@PostMapping(EndPointReferrer.PANLOG)
    public ResponseEntity<BaseResponse> getPanLog(@RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.PANLOG);

        return new ResponseEntity<>(adminManager.getPanLog(adminLogRequest.getRefID()), HttpStatus.OK);
    }
*/
    /**
     * @param adminLogRequest
     * @return
     */
    /*@PostMapping(EndPointReferrer.AADHARLOG)
    public ResponseEntity<BaseResponse> getAadharLog(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.AADHARLOG);

        return new ResponseEntity<>(adminManager.getAadharLog(adminLogRequest.getRefID()), HttpStatus.OK);
    }*/

    /*@GetMapping(EndPointReferrer.AADHARLOG + "/{aadharNo}/{institutionId}")
    public ResponseEntity<BaseResponse> getAadharLogByAadharNoAndInstitutionId(
            @PathVariable("aadharNo") @NotBlank @Valid String aadharNo,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.AADHARLOG,
                aadharNo,
                institutionId);

        return new ResponseEntity<>(adminManager.getAadharLogByAadharNoAndInstId(aadharNo, institutionId), HttpStatus.OK);
    }*/

    /**
     * To get Mb Request Json.
     *
     * @param adminLogRequest
     * @return RequestJsonDomain
     * @author yogeshb
     */
    @PostMapping(EndPointReferrer.MBLOG)
    public ResponseEntity<BaseResponse> getMbLog(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.MBLOG);

        return new ResponseEntity<>(adminManager.getMbLog(adminLogRequest.getRefID()), HttpStatus.OK);
    }


    /**
     * To get Mb Request Json with coapplicant.
     *
     * @param adminLogRequest
     * @return RequestJsonDomain
     * @author bhuvnesh
     */
    @PostMapping(EndPointReferrer.MBLOG_WITH_COAPPLICANT)
    public ResponseEntity<BaseResponse> getMbLogWithCoapplicant(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.MBLOG_WITH_COAPPLICANT);

        return new ResponseEntity<>(adminManager.getMbLogWithCoApplicant(adminLogRequest.getRefID()), HttpStatus.OK);
    }


    /**
     * To get Scoring Request Json.
     *
     * @param adminLogRequest
     * @return ScoringApplicationRequest
     * @author yogeshb
     */
    @PostMapping(EndPointReferrer.SCORINGLOG)
    public ResponseEntity<BaseResponse> getScoringLog(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.SCORINGLOG);

        return new ResponseEntity<>(adminManager.getScoringLog(adminLogRequest.getRefID()), HttpStatus.OK);
    }

    /**
     * To get Single ApplicationRequest Json
     *
     * @param adminLogRequest
     * @return ApplicationRequest
     * @author yogeshb
     */
    @PostMapping(EndPointReferrer.MAINLOG)
    public ResponseEntity<BaseResponse> getMainLog(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.MAINLOG);

        return new ResponseEntity<>(adminManager.getMainLog(adminLogRequest.getRefID()), HttpStatus.OK);
    }

    /**
     * To get Single goNoGoCustomerApplication Json
     *
     * @param adminLogRequest
     * @return GoNoGoCroApplicationResponse
     * @author yogeshb
     */
    @PostMapping(EndPointReferrer.SINGLE_BUCKET_LOG)
    public ResponseEntity<BaseResponse> getSingleApplicationResponseLog(
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.SINGLE_BUCKET_LOG);

        return new ResponseEntity<>(adminManager.getSingleBucketData(adminLogRequest
                .getRefID()), HttpStatus.OK);
    }

    /**
     * get All WorkFlowLog Json
     *
     * @return List<WorkFlowLog>
     * @author yogeshb
     *//*
    @Deprecated
    @PostMapping(EndPointReferrer.GET_LOG_REPORT)
    public ResponseEntity<BaseResponse> getLog() throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.GET_LOG_REPORT);

        return new ResponseEntity<>(adminManager.getLogData(), HttpStatus.OK);
    }*/

    /**
     * To convert json to csv
     *
     * @param json
     * @return String
     */
    /*@Deprecated
    @PostMapping(EndPointReferrer.GET_JSON_TO_CSV_REPORT)
    public ResponseEntity<BaseResponse> getCSVLog(@RequestBody List<OtpLog> json) throws JsonProcessingException {

        logger.debug("{} controller started ",EndPointReferrer.GET_JSON_TO_CSV_REPORT);

        return new ResponseEntity<>(adminManager.getCsvFromJson(json), HttpStatus.OK);
    }*/


    /**
     * @param loginRequest
     * @return
     */
    @PostMapping(EndPointReferrer.CACHE_REFRESH)
    public ResponseEntity<BaseResponse> refresh(@RequestBody LoginRequest loginRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.CACHE_REFRESH);

        return new ResponseEntity<>(adminManager.doCacheRefresh(loginRequest), HttpStatus.OK);
    }

    /**
     * @param loginRequest
     * @return
     */
    @Deprecated
    @PostMapping(EndPointReferrer.UPDATE_STATUS_AND_STAGE)
    public ResponseEntity<BaseResponse> updateStatusAndStage(
            @RequestBody LoginRequest loginRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.UPDATE_STATUS_AND_STAGE);

        return new ResponseEntity<>(adminManager.updateStageAndStatus(loginRequest), HttpStatus.OK);
    }

    @GetMapping(EndPointReferrer.GET_APP_STATUS_AND_STAGE + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getApplStatusAndStage(
            @PathVariable("refId") @NotBlank @Valid String refId,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{} , {}]",
                EndPointReferrer.GET_APP_STATUS_AND_STAGE,
                refId,
                institutionId);

        return new ResponseEntity<>(adminManager.getApplicationStatusAndStage(refId, institutionId), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.MODIFY_APP_STATUS_AND_STAGE)
    public ResponseEntity<BaseResponse> modifyApplicationStatusAndStage(
            @Validated(value = {Header.FetchGrp.class, ModifyAppStageAndStatusRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid ModifyAppStageAndStatusRequest stageAndStatusRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.UPDATE_STATUS_AND_STAGE);

        return new ResponseEntity<>(adminManager.modifyAppStageAndStatus(stageAndStatusRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.UPDATE_DEALER_NAME_IN_BUCKET)
    public ResponseEntity<BaseResponse> updateDealerName(
            @RequestBody LoginRequest loginRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.UPDATE_DEALER_NAME_IN_BUCKET);

        return new ResponseEntity<>(adminManager.updateDealerName(loginRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SALES_FORCE_LOG)
    public Callable<ResponseEntity<BaseResponse>> getSalesForceLog(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid SalesForceLog salesForceLog) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_SALES_FORCE_LOG);
        return () -> ResponseEntity.status(HttpStatus.OK).body(adminManager.getSalesForceLog(salesForceLog));
    }

    @PostMapping(EndPointReferrer.RAW_RESPONSE)
    public ResponseEntity<BaseResponse> getRawResponse(
            @Validated(value = {AdminLogRequest.RawResponseGrp.class})
            @RequestBody AdminLogRequest adminLogRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.RAW_RESPONSE);
        return new ResponseEntity<>(adminManager.getRawResponse(adminLogRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.CANCEL_DO_NOT_RAISED_APPLICATION)
    public ResponseEntity<BaseResponse> cancelDoNotRaisedApplication(
            @Validated(value = {Header.FetchGrp.class, CancelDoNotRaisedApplicationRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.CANCEL_DO_NOT_RAISED_APPLICATION);

        return new ResponseEntity<>(adminManager.cancelDoNotRaisedApplication(cancelDoNotRaisedApplicationRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_DO_NOT_RAISED_COUNT_FOR_CANCEL)
    public ResponseEntity<BaseResponse> getDoNotRaisedApplicationCountForCancel(
            @RequestBody @NotNull @Valid CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.GET_DO_NOT_RAISED_COUNT_FOR_CANCEL);

        return ResponseEntity.status(HttpStatus.OK).body(adminManager
                .getDoNotRaisedApplicationCountForCancel(cancelDoNotRaisedApplicationRequest));

    }

    @PostMapping(EndPointReferrer.GET_CREDIT_VIDYA_RESPONSE)
    public ResponseEntity<BaseResponse> getCreditVidyaResponse(
            @RequestBody @NotNull @Valid CreditVidyaLogRequest creditVidyaLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.GET_CREDIT_VIDYA_RESPONSE);

        return new ResponseEntity(adminManager.getCreditVidyaResponse(creditVidyaLogRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SAATHI_RESPONSE)
    public ResponseEntity<BaseResponse> getSaathiResponse(
            @RequestBody @NotNull @Valid SaathiLogRequest saathiLogRequest) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.GET_SAATHI_RESPONSE);

        return new ResponseEntity(adminManager.getSaathiResponse(saathiLogRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_TKYC_LOG)
    public ResponseEntity<BaseResponse> getTkycLog(
            @RequestBody @NotNull @Valid TKycLogRequest tKycLogRequest) throws Exception {
        logger.debug("{} controller started ",EndPointReferrer.GET_TKYC_LOG);
        return new ResponseEntity(adminManager.getTkycLog(tKycLogRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_TCLOS_LOG)
    public ResponseEntity<BaseResponse> getTcLosLog(
            @RequestBody @NotNull @Valid TcLosLogRequest tcLosLogRequest) throws Exception {
        logger.debug("{} controller started ",EndPointReferrer.GET_TCLOS_LOG);
        return new ResponseEntity(adminManager.getTcLosLog(tcLosLogRequest), HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.GET_SMS_SERVICE_LOG)
    public ResponseEntity<BaseResponse> getSmsServiceReqResLog(
            @RequestBody @NotNull @Valid SmsServReqResLogRequest smsServReqResLogRequest) throws Exception {
        logger.debug("{} controller started ",EndPointReferrer.GET_SMS_SERVICE_LOG);
        return new ResponseEntity(adminManager.getSmsServiceReqResLog(smsServReqResLogRequest), HttpStatus.OK);
    }

    /*
    * A File is providing decision, remarks and the refIds to be changed to
    * Here is file format
    * -------------
    * decision => Approved / Declined / Queue / OnHold/ Cancelled / Disbursed / LMS-Initiated
    * remark => This wll be populated in CRO Justification
    * refId1
    * refId2
    * ...
    * refIdn
    * -------------
    * */
    @PostMapping(EndPointReferrer.BULK_DECISION)
    public ResponseEntity<BaseResponse> bulkDecision(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ",EndPointReferrer.BULK_DECISION);

        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
        BaseResponse baseResponse = adminManager.bulkDeclineCases(uploadedFile, institutionId);
        FileUtils.deleteQuietly(uploadedFile);

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
}
