package com.softcell.rest.controllers;

import com.softcell.gonogo.model.coorgination.icici.CoorginationAPIRequest;
import com.softcell.gonogo.model.core.request.ThirdPartyRequest;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.finbit.FinbitCallbackRequest;
import com.softcell.gonogo.model.finbit.FinbitRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.LoanChargesRepaymentRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.ssl.perfios.GenerateFileRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ThirdPartyIntegrationManager;
import com.softcell.ssl2.finfort.model.FileUploadRequest;
import com.softcell.ssl2.finfort.model.FinfortStatusRequest;
import com.softcell.ssl2.perfios.PerfiosDataRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg408 on 24/9/18.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ThirdPartyIntegrationController {

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyIntegrationController.class);

    @Autowired
    private ThirdPartyIntegrationManager thirdPartyIntegrationManager;

    @PostMapping(EndPointReferrer.SAVE_APPLICATION_DATA_STEPID)
    public ResponseEntity<BaseResponse> saveApplicationDataFromPaisaBazar(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationRequest applicationRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.AGGREGATOR);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.saveData(applicationRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.INITIATE_FINFORT)
    public ResponseEntity<BaseResponse> initiateFinfort(
            @Validated({Header.FetchGrp.class}) @RequestBody @NotNull ApplicationRequest applicationRequest,
                                                       HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.INITIATE_FINFORT);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.initiateFinfort(applicationRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FINFORT_STATUS)
    public ResponseEntity<BaseResponse> processFinfortStatus(@RequestBody FinfortStatusRequest statusRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FINFORT_STATUS);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.processFinfortStatus(statusRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FINFORT_FILELIST)
    public ResponseEntity<BaseResponse> processFileList(@RequestBody FileUploadRequest fileUploadRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FINFORT_FILELIST);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.processFileList(fileUploadRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_PERFIOS_DATA)
    public ResponseEntity<BaseResponse> updatePerfiosData(
            @Validated({Header.FetchGrp.class}) @RequestBody @NotNull PerfiosDataRequest perfiosDataRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_PERFIOS_DATA);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.updatePerfiosData(perfiosDataRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FINBIT_DETAILS)
    public ResponseEntity<BaseResponse> getFinbitDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull FinbitRequest finbitRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.FINBIT_DETAILS);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getFinbitDetails(finbitRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_FINBIT_DATA)
    public ResponseEntity<BaseResponse> saveFinbitData(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull FinbitRequest finbitRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.SAVE_FINBIT_DATA);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.saveFinbitData(finbitRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_FINBIT_DATA)
    public ResponseEntity<BaseResponse> getFinbitData(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull FinbitRequest finbitRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.GET_FINBIT_DATA);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getFinbitData(finbitRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FINBIT_CALLBACK)
    public ResponseEntity<BaseResponse> finbitCallback(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull FinbitCallbackRequest finbitCallbackRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.FINBIT_CALLBACK);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.finbitCallback(finbitCallbackRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.FINBIT_MONTHLY_SUMMARY)
    public ResponseEntity<BaseResponse> getFinbitMonthlySummary(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull FinbitRequest finbitRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.FINBIT_MONTHLY_SUMMARY);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getFinbitMonthlySummary(finbitRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.PUSH_CORPORATE_API_DETAILS)
    public ResponseEntity<BaseResponse> getCoorginationDetails(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull CoorginationAPIRequest coorginationAPIRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.PUSH_CORPORATE_API_DETAILS);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getCoorginationDetails(coorginationAPIRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_LOAN_CHARGES_AND_REPAYMENT_DETAILS)
    public ResponseEntity<BaseResponse> saveLoanChargesAndRepaymentDetails(
            @Validated({Header.FetchGrp.class, LoanChargesRepaymentRequest.FetchGrp.class,LoanChargesRepaymentRequest.InsertGrp.class})
            @RequestBody @NotNull LoanChargesRepaymentRequest loanChargesRepaymentRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.SAVE_LOAN_CHARGES_AND_REPAYMENT_DETAILS);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.saveLoanChargesAndRepaymentDetails(loanChargesRepaymentRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.MCP_SAVE_APPLICATION_DATA)
    public ResponseEntity<BaseResponse> saveMCPApplicationData(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull LoginDataRequest loginDataRequest,
            @PathVariable String stepId,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.MCP_SAVE_APPLICATION_DATA);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.saveMcpData(loginDataRequest, stepId, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SCORING_DETAILS)
    public ResponseEntity<BaseResponse> getScoringDetails(
            @Validated({Header.FetchGrp.class}) @RequestBody @NotNull @Valid ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_SCORING_DETAILS);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getScoringCallDetails(thirdPartyRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_SCORINGCALL_LOG)
    public ResponseEntity<BaseResponse> getScoringCallLog(
            @Validated({Header.FetchGrp.class}) @RequestBody @NotNull @Valid ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_SCORINGCALL_LOG);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.getScoringCallLog(thirdPartyRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GENERATE_FILE_PERFIOS)
    public ResponseEntity<BaseResponse> generatePerfiosFile(
            @RequestBody @NotNull @Valid GenerateFileRequest generateFileRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GENERATE_FILE_PERFIOS);

        return new ResponseEntity<>(
                thirdPartyIntegrationManager.generatePerfiosFile(generateFileRequest, httpRequest),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_FACEMATCH_DATA)
    public ResponseEntity<BaseResponse> saveFaceMatchData(
            @Validated({FaceMatchRequest.InsertGroup.class, Header.InsertGrp.class})
            @RequestBody @Valid @NotNull FaceMatchRequest faceMatchRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.SAVE_FACEMATCH_DATA);
        return new ResponseEntity<>(
                thirdPartyIntegrationManager.saveFaceMatchData(faceMatchRequest),
                HttpStatus.OK);
    }


}
