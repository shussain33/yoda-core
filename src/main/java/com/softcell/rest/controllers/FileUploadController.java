package com.softcell.rest.controllers;

import com.softcell.constants.Status;
import com.softcell.constants.master.MasterDetails;
import com.softcell.gonogo.model.MasterMappingDetails;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.FileStagingUtils;
import com.softcell.service.FileUploadManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.io.FileUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This controller is specially used for upload file.
 *
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.MASTERS_UPLOAD_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class FileUploadController {

    private static final Logger logger = LoggerFactory
            .getLogger(FileUploadController.class);



    private FileUploadManager fileUploadManager;

    @Autowired
    public FileUploadController(FileUploadManager fileUploadManager) {
        this.fileUploadManager = fileUploadManager;
    }

    /**
     * This service use to upload scheme master csv ,parse it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file
     * @return file upload status
     */
    @PostMapping(EndPointReferrer.SCHEME_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadSchememaster(
            @RequestParam(value = "file", required = true) final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {
        BaseResponse baseResponse;
        logger.debug("inside upload/scheme-master called");
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.SCHEME_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager
                    .uploadSchemesMaster(
                            uploadedFile,
                            institutionId);
            // cleaning file handler resources after its work done
            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload AssetModelMaster csv ,parse it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file
     * @return FileUploadStatus object
     */
    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadAssetModelMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/asset-model-master");
        BaseResponse baseResponse;

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ASSET_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadAssetModelMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }


        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload PinCode csv ,parse it and store it in mongodb
     * with rename previous collection with collection name followed by current
     * date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.PINCODE_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadPinCodeMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam("institutionId") @NotBlank String institutionId)
            throws Exception {

        logger.info("inside upload/pincode-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.PINCODE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadPincodeMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload EmployerMaster csv ,parse it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.EMPLOYER_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadEmployerMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/employer-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.EMPLOYER_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadEmployerMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload DealerEmailMaster csv ,parse it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.DEALER_EMAIL_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadDealerEmailMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/dealer-email-master");


        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DEALER_EMAIL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadDealerEmailMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload gng cc/bcc dealer email master csv ,parse it
     * and store it in mongodb with rename previous collection with collection
     * name followed by current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.GNG_CC_DEALEREMAIL_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadGNGDealerEmailMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/gngdealer-email-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.GNG_CC_DEALEREMAIL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadGNGDealerEmailMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload SchemeModelDealerCityMapping csv ,parse it and
     * store it in mongodb with rename previous collection with collection name
     * followed by current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.SCHEME_MODEL_DEALER_CITY_MAPPING_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadSchemeModelDealerCityMappingMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/scheme-model-dealer-City-mapping-master");


        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.SCHEME_MODEL_DEALER_CITY_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadSchemeModelDealerCityMappingMaster(uploadedFile,
                            institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This service use to upload SchemeDateMapping csv ,parse it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.SCHEME_DATE_MAPPING_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadSchemeDateMappingMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/scheme-date-mapping-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.SCHEME_DATE_MAPPING.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadSchemeDateMappingMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * This service use to upload CarSurrogateMaster csv ,parse it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.CAR_SURROGATE_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadCarSurrogateMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/car-surrogate-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.CAR_SURROGATE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadCarSurrogateMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * This master is use to upload pincodeEmailMaster for product pl salary.
     * parse it and store it in mongodb with rename previous collection with
     * collection name
     *
     * @param file
     * @return
     * @category PL_SALARY Masters
     */
    @PostMapping(EndPointReferrer.PL_PINCODE_EMAIL_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadPlPincodeEmailMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/pl-pincode-email-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.PL_PINCODE_EMAIL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadPlPincodeEmailMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * This service use to upload BankDetailsMaster csv ,parse it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file          master file to be be saved or uploaded.
     * @param institutionId institutionId for which bankDetails master to be uploaded
     * @return FileUploadStatus
     */
    @PostMapping(EndPointReferrer.BANK_DETAILS_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadBankDetailsMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam("institutionId") @NotBlank String institutionId)
            throws Exception {

        logger.info("inside upload/bank-details-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.BANK_DETAILS_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadBankDetailsMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * This service use to upload HierarchyMaster csv ,parse it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file          master file to be be saved or uploaded.
     * @param institutionId institutionId for which hierarchy master to be uploaded
     * @return FileUploadStatus
     */
    @PostMapping(EndPointReferrer.HIERARCHY_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadHierarchyMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam("institutionId") @NotBlank String institutionId)
            throws Exception {

        logger.info("inside upload/hierarchy-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.HIERARCHY_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadHierarchyMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * This service use to upload CDL HierarchyMaster csv ,parse it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file          master file to be be saved or uploaded.
     * @param institutionId institutionId for which hierarchy master to be uploaded
     * @return FileUploadStatus
     */
    @PostMapping(EndPointReferrer.CDL_HIERARCHY_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadCDLHierarchyMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam("institutionId") @NotBlank String institutionId)
            throws Exception {

        logger.info("inside upload/hierarchy-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.CDL_HIERARCHY_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadCDLHierarchyMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }


        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.CITY_STATE_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadCityStateMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/asset-model-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.CITY_STATE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadCityStateMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @return
     */
    @PostMapping(EndPointReferrer.VERSION)
    public ResponseEntity<BaseResponse> handleFileUploadVersion(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file)
            throws Exception {

        logger.info("inside upload/version");

        File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

        BaseResponse baseResponse = fileUploadManager
                .uploadVersionMaster(uploadedFile);

        FileUtils.deleteQuietly(uploadedFile);

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.MODEL_VARIANT_MASTER)
    public ResponseEntity<BaseResponse> handleModelVariantMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/model-variant-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.MODEL_VARIANT_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadModelVariantMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.ASSET_CATEGORY_MASTER)
    public ResponseEntity<BaseResponse> handleAssetCategoryMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/asset-category-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ASSET_CAT_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadAssetCategoryMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.STATE_MASTER)
    public ResponseEntity<BaseResponse> handleStateMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/state-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.STATE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadStateMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.CITY_MASTER)
    public ResponseEntity<BaseResponse> handleCityMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/city-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.CITY_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager.uploadCityMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.CREDIT_PROMOTION_MASTER)
    public ResponseEntity<BaseResponse> handleCreditPromotionMaster(
            @RequestParam(value = "file", required = true) @NotNull MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/credit-promotion-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.CREDIT_PROMOTION_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadCreditPromotionMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.DSA_BRANCH_MASTER)
    public ResponseEntity<BaseResponse> handleDsaBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/dsa-branch-master");
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DSA_BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {

            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadDsaBranchMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.DSA_PRODUCT_MASTER)
    public ResponseEntity<BaseResponse> handleDsaProductMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/dsa-product-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DSA_PRODUCT_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadDsaProductMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @PostMapping(EndPointReferrer.DEALER_BRANCH_MASTER)
    public ResponseEntity<BaseResponse> handleDealerBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/dealer-branch-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DEALER_BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            List<BaseResponse> baseList = new ArrayList<BaseResponse>();

            baseResponse = fileUploadManager.uploadDealerBranchMaster(uploadedFile, institutionId);

            baseList.add(baseResponse);

            baseResponse = fileUploadManager.uploadCDLHierarchyMaster(uploadedFile, institutionId);

            baseList.add(baseResponse);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, baseList);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.NEGATIVE_AREA_GEO_LIMIT_MASTER)
    public ResponseEntity<BaseResponse> handleNegativeAreaFundingMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.info("inside upload/negative-area-geo-limit-funding-master");

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.NEGATIVE_AREA_GEO_LIMIT_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadNegativeAreaGeoLimitMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @CrossOrigin
    @PostMapping(EndPointReferrer.UPDATE_LOS_UTR_DETAILS_MASTER)
    public ResponseEntity<BaseResponse> handleLosUtrDetailsMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_LOS_UTR_DETAILS_MASTER);

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.UPDATE_LOS_UTR_DETAILS_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadLosMasterDetails(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);

        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.UPDATE_NET_DISBURSEMENT_AMOUNT)
    public ResponseEntity<BaseResponse> handleNetDisbursementMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.UPDATE_NET_DISBURSEMENT_AMOUNT);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.NET_DISBURSAL_DETAILS_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .updateNetDisbursementAmount(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.RELIANCE_DEALER_BRANCH_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleRelianceDealerBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws IOException {

        logger.debug("{} controller started ", EndPointReferrer.RELIANCE_DEALER_BRANCH_MASTER);

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.RELIANCE_DEALER_BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadRelianceDealerBranchMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.REFERENCE_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleReferenceMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId,
            @RequestParam @NotBlank String product) throws IOException {

        logger.debug("{} controller started ", EndPointReferrer.REFERENCE_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.REFERENCE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadReferenceDetailsMaster(uploadedFile, institutionId, product);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }
    @PostMapping(value = EndPointReferrer.LOYALTY_CARD_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleLoyaltyCardMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws IOException {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.LOYALTY_CARD_MASTER,
                institutionId);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.LOYALTY_CARD_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadLoyaltyCardMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.BANKING_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleBankingMaster(
            @RequestParam(value = "file", required = true)
            @NotNull final MultipartFile file, @RequestParam @NotBlank String institutionId) throws IOException {
        logger.debug("{} controller started ", EndPointReferrer.BANKING_MASTER);

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.BANKING_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadBankingMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.LOS_BANKING_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleLosBankingMaster(
            @RequestParam(value = "file")
            @NotNull final MultipartFile file, @RequestParam @NotBlank String institutionId) throws IOException {
        logger.debug("{} controller started ", EndPointReferrer.LOS_BANKING_MASTER);

        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.LOS_BANKING_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadLosBankingMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.COMMON_GENERAL_MASTER)
    public ResponseEntity<BaseResponse> handleCommonGeneralParameterMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.COMMON_GENERAL_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.COMMON_GENERAL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadCommonGeneralParameterMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }


    @PostMapping(value = EndPointReferrer.EW_INS_GNG_ASSET_CAT_MASTER)
    public ResponseEntity<BaseResponse> handleEwInsGngAssetCategoryMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.EW_INS_GNG_ASSET_CAT_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.EW_INS_GNG_ASSET_CAT_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadEwInsGngAssetCategoryMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.EW_PREMIUM_MASTER)
    public ResponseEntity<BaseResponse> handleEwPremiumDataMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.EW_PREMIUM_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.EW_PREMIUM_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadEwPremiumDataMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.INSURANCE_PREMIUM_MASTER)
    public ResponseEntity<BaseResponse> handleInsurancePremiumMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.INSURANCE_PREMIUM_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.INSURANCE_PREMIUM_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadInsurancePremiumMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    /**
     * Method to upload Electricity Service Provider Master.
     * @param file
     * @param institutionId
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.ELEC_SERV_PROV_MASTER)
    public ResponseEntity<BaseResponse> handleFileUploadElecServProvMaster(
            @RequestParam(value = "file", required = true) final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.ELEC_SERV_PROV_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ELEC_SERV_PROV_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadElecServProvMaster(
                            uploadedFile,
                            institutionId);

            // cleaning file handler resources after its work done
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.BRANCH_MASTER)
    public ResponseEntity<BaseResponse> handleBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ", EndPointReferrer.BRANCH_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadBranchMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }


    @PostMapping(value = EndPointReferrer.SOURCING_DETAIL_MASTER)
    public ResponseEntity<BaseResponse> handleSourcingDetailMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.SOURCING_DETAIL_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.SOURCING_DETAIL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadSourcingDetailMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.TKIL_DEALER_BRANCH_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> handleTkilDealerBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws IOException {

        logger.debug("{} controller started ", EndPointReferrer.TKIL_DEALER_BRANCH_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.TKIL_DEALER_BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadTkilDealerBranchMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }


    @PostMapping(value = EndPointReferrer.PROMOTIONAL_SCHEME_MASTER)
    public ResponseEntity<BaseResponse> handlePromotionalSchemeMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.PROMOTIONAL_SCHEME_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.PROMOTIONAL_SCHEME_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadPromotionalSchemeMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @PostMapping(value = EndPointReferrer.BANK_MASTER)
    public ResponseEntity<BaseResponse> handleBankMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started with parameter [{}] ",
                EndPointReferrer.BANK_MASTER,
                institutionId );
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.BANK_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);

            baseResponse = fileUploadManager
                    .uploadBankMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.TAX_CODE_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadTaxCodeMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {
        logger.debug("{} controller started ", EndPointReferrer.TAX_CODE_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.TAX_CODE_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadTaxCodeMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.ACCOUNTS_HEAD_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadAccountsHeadMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.ACCOUNTS_HEAD_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ACCOUNTS_HEAD_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadAccountsHeadMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.STATE_BRANCH_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadStateBranchMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.STATE_BRANCH_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.STATE_BRANCH_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadStateBranchMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.GENERIC_MASTER)
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadGenericMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId,
            @RequestParam @NotBlank MasterDetails masterName) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.GENERIC_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, masterName.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadGenericMaster(uploadedFile, institutionId, masterName);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @RequestMapping(value = EndPointReferrer.TCLOS_GNG_MAPPING_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadTCLosGngMappingMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.TCLOS_GNG_MAPPING_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.TCLOS_GNG_MAPPING_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadTCLosGngMappingMaster(uploadedFile, institutionId);

            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.MANUFACTURER_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadManufacturerMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.MANUFACTURER_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.MANUFACTURER_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadManufacturers(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.SUPPLIER_LOCATION_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadSupplierLocationMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.SUPPLIER_LOCATION_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.SUPPLIER_LOCATION_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadSupplierLocationMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.PAYNIMO_BANK_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadPaynimoBankMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.PAYNIMO_BANK_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.PAYNIMO_BANK_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadPaynimoBankMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.DIGITIZATION_DEALER_EMAIL_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadDigitizationDealerEmailMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.DIGITIZATION_DEALER_EMAIL_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DIGITIZATION_DEALER_EMAIL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadDigitizationEmailMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.DEVIATION_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadDeviationMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId,
            @RequestParam @NotBlank String product) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.DEVIATION_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.DEVIATION_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadDeviationMaster(uploadedFile, institutionId, product);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.ORGANISATION_HIERARCHY_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadOrganizationalHierarchyMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.ORGANISATION_HIERARCHY_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ORGANIZATION_HIERARCHY_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadOrganizationalHierarchyMaster(uploadedFile, institutionId);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.UPLOAD_POTENTIAL_CUSTOMER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadPotentialCustomer(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId, String product) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.UPLOAD_POTENTIAL_CUSTOMER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.POTENTIAL_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadPotentialCustomer(uploadedFile, institutionId, product);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @RequestMapping(value = EndPointReferrer.UPLOAD_ROI_SCHEME_MASTER )
    @ResponseBody
    public ResponseEntity<BaseResponse> uploadRoiSchemeMaster(
            @RequestParam(value = "file", required = true) @NotNull final MultipartFile file,
            @RequestParam @NotBlank String institutionId, String product) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.UPLOAD_ROI_SCHEME_MASTER);
        BaseResponse baseResponse;
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        boolean status = fileUploadManager.checkMasterUploadRestriction(institutionId, MasterMappingDetails.ROI_SCHEME_MASTER.getMasterName(), fileUploadStatus);
        if (status) {
            fileUploadStatus.setStatus(Status.ERROR.name());
            baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
        }else {
            File uploadedFile = FileStagingUtils.saveFileToStagingDirectory(file);
            baseResponse = fileUploadManager.uploadRoiSchemeMaster(uploadedFile, institutionId, product);
            FileUtils.deleteQuietly(uploadedFile);
        }
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
}
