package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.MultiProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.MULTIPRODUCT_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class MultiProductController {

    @Autowired
    private MultiProductManager multiProductManager;

    /**
     * @param multiProductRequest
     * @return
     */
    @PostMapping(EndPointReferrer.ASSET_COUNT)
    public ResponseEntity<BaseResponse> getNumberOfProduct(
            @Validated(value = {MultiProductRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid MultiProductRequest multiProductRequest) throws Exception {

        return new ResponseEntity<>(multiProductManager.getProductCount(multiProductRequest), HttpStatus.OK);
    }

    /**
     * @param multiProductRequest
     * @return
     */
    @PostMapping(EndPointReferrer.PRODUCT_ADD_REQUEST)
    public ResponseEntity<BaseResponse> addProduct(
            @Validated(value = {MultiProductRequest.FetchGrp.class, MultiProductRequest.ProductSelectionGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid MultiProductRequest multiProductRequest) throws Exception {

        return new ResponseEntity<>(
                multiProductManager.addNewProduct(multiProductRequest),
                HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.IS_DEALER_SUBVENTION_WAIVED_OFF)
    public ResponseEntity<BaseResponse> getServiceStatus(
            @Validated(value = {Header.FetchWithDealerCriteriaGrp.class})
            @RequestBody Header header) {
        return new ResponseEntity<>(
                multiProductManager.isDealerSubventionWaivedOff(header),
                HttpStatus.OK);

    }
}