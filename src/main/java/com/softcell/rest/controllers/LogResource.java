package com.softcell.rest.controllers;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.softcell.rest.domains.LoggerRequest;
import com.softcell.rest.utils.EndPointReferrer;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This controller help in finding log label of various scan package/classes
 * and let u modify the behaviour of logger at runtime
 * this will help in reproducing error if occurred any in production environment
 * <p>
 * Created by prateek on 8/2/17.
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.LOGGING_MANAGEMENT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class LogResource {


    @GetMapping(EndPointReferrer.LOGS)
    public ResponseEntity<List<LoggerRequest>> getList() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        return new ResponseEntity<>(context.getLoggerList().
                stream().
                map(LoggerRequest::new).
                collect(Collectors.toList()), org.springframework.http.HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.LOGS)
    @ResponseStatus(org.springframework.http.HttpStatus.NO_CONTENT)
    public void changeLabel(@RequestBody LoggerRequest loggerRequest) {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.getLogger(loggerRequest.getName()).setLevel(Level.valueOf(loggerRequest.getLevel()));
    }
}
