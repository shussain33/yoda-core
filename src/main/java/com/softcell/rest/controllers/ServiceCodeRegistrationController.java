package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.servicecoderegistration.ServiceCodeRegistrationRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.servicecoderegistration.ServiceCodeRegistrationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *  created by prasenjit wadmare on 21/12/2017
 */

@RestController
@RequestMapping(
        value = EndPointReferrer.SERVICE_CODE_BASE_ENPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ServiceCodeRegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(ServiceCodeRegistrationController.class);

    @Autowired
    private ServiceCodeRegistrationManager serviceCodeRegistrationManager;


    /**
     * @param serviceCodeRegistrationRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_HELP_DESK_NUMBER)
    public ResponseEntity<BaseResponse> getVoltasServiceHelpDeskNumber(
            @Validated(value = {ServiceCodeRegistrationRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid ServiceCodeRegistrationRequest serviceCodeRegistrationRequest) throws Exception {

        return new ResponseEntity<>(serviceCodeRegistrationManager.getVoltasHelpDeskNumber(serviceCodeRegistrationRequest), HttpStatus.OK);

    }


}
