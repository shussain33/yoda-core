package com.softcell.rest.controllers;

import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.DropdownMasterRequest;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.DealerRankingRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.MasterManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.MASTER_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE

)
public class MasterController {

    private static final Logger logger = LoggerFactory.getLogger(MasterController.class);

    @Autowired
    private MasterManager masterManager;

    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_MANUFACTURERS)
    public ResponseEntity<BaseResponse> getManufacturers(
            @RequestBody @Validated(value = {Header.FetchGrp.class,Header.InstWithProductGrp.class})
            @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_MANUFACTURERS);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getManufacturerList(metadataRequest));

    }

    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_MANUFACTURERS_DETAILS)
    public ResponseEntity<BaseResponse> getManufacturerDetailsList(
            @RequestBody @Validated(value = {Header.FetchGrp.class,Header.InstWithProductGrp.class})
            @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_MANUFACTURERS);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getManufacturerDetailsList(metadataRequest));

    }

    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_MODEL_NUMBER_WITH_OTHER_DETAILS)
    public ResponseEntity<BaseResponse> getModels(
            @Validated(value = {ApplicationMetadataRequest.FetchWithMnfModelGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_MODEL_NUMBER_WITH_OTHER_DETAILS);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getListOfModelsWithOtherDetails(metadataRequest));

    }


    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_CATEGORIES)
    public ResponseEntity<BaseResponse> getCategories(
            @Validated(value = {ApplicationMetadataRequest.FetchWithManufacturerGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_CATEGORIES);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getCategoryList(metadataRequest));
    }

    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_MAKE)
    public ResponseEntity<BaseResponse> getMakes(
            @Validated(value = {ApplicationMetadataRequest.FetchWithMnfCatGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_MAKE);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getMakeList(metadataRequest));
    }

    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_GET_MODEL_NUMBER)
    public ResponseEntity<BaseResponse> getModelNos(
            @Validated(value = {ApplicationMetadataRequest.FetchAllForModelGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_GET_MODEL_NUMBER);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getModelNumberList(metadataRequest));
    }

    /**
     * Straight search for Asset model master.
     *
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.ASSET_MODEL_MASTER_BY_MODELNUMBER)
    public ResponseEntity<BaseResponse> getAssetModelMaster(
            @Validated(value = {ApplicationMetadataRequest.ModelNumberGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @Valid @NotNull ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.ASSET_MODEL_MASTER_BY_MODELNUMBER);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getAssetModelMasterList(metadataRequest));
    }

    @PostMapping(EndPointReferrer.FULL_TEXT_ASSET_MODEL_MASTER)
    @Deprecated
    public ResponseEntity<BaseResponse> getAssetModelMasterByTextSearch(
            @RequestBody ApplicationMetadataRequest metadataRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.FULL_TEXT_ASSET_MODEL_MASTER);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getAssetModelMasterListByTextSearch(metadataRequest
                .getQuery()));
    }

    @PostMapping(EndPointReferrer.DEALER_EMAIL_MASTER_GET_DEALER_RANKING)
    public ResponseEntity<BaseResponse> getDealerRanking(
            @Validated(value = {Header.FetchWithDealerCriteriaGrp.class})
            @RequestBody @Valid @NotNull DealerRankingRequest dealerRankingRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.DEALER_EMAIL_MASTER_GET_DEALER_RANKING);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getDealerRanking(dealerRankingRequest));

    }

    
    @PostMapping(EndPointReferrer.SAVE_DROP_DOWN_MASTER)
    public ResponseEntity<BaseResponse> saveDropDownMaster(
            @Validated(value = {Header.FetchWithDealerCriteriaGrp.class})
            @RequestBody @Valid @NotNull DropdownMaster dropdownMaster) {

        logger.debug("{} service started from master controller", EndPointReferrer.SAVE_DROP_DOWN_MASTER);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.saveDropDownMaster(dropdownMaster));

    }

    @PostMapping(EndPointReferrer.GET_DROP_DOWN_MASTER)
    public ResponseEntity<BaseResponse> getDropDownMaster(
            @Validated(value = {Header.FetchWithDealerCriteriaGrp.class})
            @RequestBody @Valid @NotNull DropdownMasterRequest dropdownMasterRequest) {

        logger.debug("{} service started from master controller", EndPointReferrer.GET_DROP_DOWN_MASTER);

        return ResponseEntity.status(HttpStatus.OK).body(masterManager.getDropDownMaster(dropdownMasterRequest));

    }


}
