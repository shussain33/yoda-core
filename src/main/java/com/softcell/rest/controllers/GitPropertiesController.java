package com.softcell.rest.controllers;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.service.impl.GitPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by prateek on 13/3/17.
 */
@RestController
@RequestMapping(
        value = "/api/git" ,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class GitPropertiesController {

    @Autowired
    private GitPropertiesService gitPropertiesService;

    @GetMapping("/status")
    public ResponseEntity<BaseResponse> getGitStatus(){

        return  new ResponseEntity<>(gitPropertiesService.getGitProprty(), HttpStatus.OK);
    }


}
