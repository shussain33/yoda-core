package com.softcell.rest.controllers;

import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAPIRequest;
import com.softcell.gonogo.model.kyc.KyDetailsRequest;
import com.softcell.gonogo.model.kyc.KycVerificationRequest;
import com.softcell.gonogo.model.kyc.request.*;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstAuthDetailRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstIdentyRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.KarzaServiceCaller;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.TotalKycManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Controller for getting Total KYC API details.
 * Created by anupamad on 13/7/17.
 */
@RestController
@RequestMapping(value = EndPointReferrer.TOTAL_KYC,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE)

public class TotalKycController {
    @Autowired
    private TotalKycManager totalKycManager;

    @Autowired
    private KarzaServiceCaller karzaServiceCaller;

    private static final Logger logger = LoggerFactory.getLogger(TotalKycController.class);

    /**
     * Get the Lpg Details
     * @param totalKycLpgDetailRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.TOTAL_KYC_LPG)
    public ResponseEntity<BaseResponse> getLpgDetail(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycLpgDetailRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycLpgDetailRequest totalKycLpgDetailRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_LPG);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getLpgDetail(totalKycLpgDetailRequest));
    }

    /**
     * Get the Driving License details.
     * @param totalKycDlDetailRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.TOTAL_KYC_DRIV_LIC)
    public ResponseEntity<BaseResponse> getDlDetail(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycDlDetailRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycDlDetailRequest totalKycDlDetailRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_DRIV_LIC);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getDlDetail(totalKycDlDetailRequest));
    }

    /**
     * Get Electricity Details.
     * @param totalKycElecDtlRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.TOTAL_KYC_ELEC)
    public ResponseEntity<BaseResponse> getElecDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycElecDtlRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycElecDtlRequest totalKycElecDtlRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_ELEC);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getElecDetail(totalKycElecDtlRequest));
    }

    /**
     * Get Voter Id Details.
     * @param totalKycVoterDetailRequest
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.TOTAL_KYC_VOTER)
    public ResponseEntity<BaseResponse> geVoterDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycVoterDetailRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycVoterDetailRequest totalKycVoterDetailRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_VOTER);

        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getVoterDetail(totalKycVoterDetailRequest));

    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_LPG_V2 )
    public ResponseEntity<BaseResponse> getLpgDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycLpgDetailRequestV2.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycLpgDetailRequestV2 totalKycLpgDetailRequestV2) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_LPG_V2);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getLpgDetailV2(totalKycLpgDetailRequestV2));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_DRIV_LIC_V2 )
    public ResponseEntity<BaseResponse> getDlDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycDlDetailRequestV2.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycDlDetailRequestV2 totalKycDlDetailRequestV2) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_DRIV_LIC_V2);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getDlDetailV2(totalKycDlDetailRequestV2));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_VOTER_V2 )
    public ResponseEntity<BaseResponse> geVoterDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycVoterDetailRequestV2.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycVoterDetailRequestV2 totalKycVoterDetailRequestV2) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_VOTER_V2);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getVoterDetailV2(totalKycVoterDetailRequestV2));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_ELEC_V2 )
    public ResponseEntity<BaseResponse> getElecDetailV2(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycElecDetailRequestV2.FetchGrp.class})
            @RequestBody @NotNull @Valid TotalKycElecDetailRequestV2 totalKycElecDetailRequestV2) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_ELEC_V2);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getElecDetailV2(totalKycElecDetailRequestV2));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_GST_AUTH )
    public ResponseEntity<BaseResponse> geGstAuthDetail(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycGstAuthDetailRequest.InsertGrp.class})
            @RequestBody @NotNull @Valid TotalKycGstAuthDetailRequest totalKycGstAuthDetailRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_GST_AUTH);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getGstAuthDetail(totalKycGstAuthDetailRequest));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_GST_IDENTITY )
    public ResponseEntity<BaseResponse> getGstIdentification(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycGstIdentyRequest.InsertGrp.class})
            @RequestBody @NotNull @Valid TotalKycGstIdentyRequest totalKycGstIdentyRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_GST_IDENTITY);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getGstIdentityDetails(totalKycGstIdentyRequest));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_PAN_AUTH )
    public ResponseEntity<BaseResponse> getPanAuthDetail(
            @Validated(value = {Header.InstWithProductGrp.class, TotalKycPanAuthDetailRequest.InsertGrp.class})
            @RequestBody @NotNull @Valid TotalKycPanAuthDetailRequest totalKycPanAuthDetailRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_PAN_AUTH);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getPanAuthDetail(totalKycPanAuthDetailRequest));
    }

    @PostMapping(EndPointReferrer.TOTAL_KYC_VERIFICATION )
    public ResponseEntity<BaseResponse> verifyKYC(
            @RequestBody @NotNull @Valid KycVerificationRequest kycVerificationRequest) throws Exception {

        logger.info("Inside : " + EndPointReferrer.TOTAL_KYC_VERIFICATION);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.verifyKYC(kycVerificationRequest));
    }

    /*
    * @param applicationRequest
    * */
    @PostMapping(EndPointReferrer.KARZA_KYC )
    public ResponseEntity<BaseResponse> getKycDetail(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid KyDetailsRequest kyDetailsRequest
            ) throws Exception {
        ApplicationRequest applicationRequest = kyDetailsRequest.getApplicationRequest();
        String kycType = kyDetailsRequest.getKycType();
        logger.info("Calling karza for {}",kycType);
        logger.info("Inside : " + EndPointReferrer.KARZA_KYC);
        return ResponseEntity.status(HttpStatus.OK).body(totalKycManager.getKycDetailByKarza(applicationRequest, kycType));
    }

    @PostMapping(EndPointReferrer.KARZA_API_VERIFICATION)
    public ResponseEntity<BaseResponse> getKarzaAPIVerification(
            @Validated({Header.FetchGrp.class})
            @RequestBody @NotNull KarzaAPIRequest karzaAPIRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.KARZA_API_VERIFICATION);

        return new ResponseEntity<>(
                karzaServiceCaller.getKarzaAPIVerification(karzaAPIRequest, httpRequest),
                HttpStatus.OK);
    }

}
