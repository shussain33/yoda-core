package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.LOSDetailsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.reporting.request.DailyDisbursalRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ExternalExposeServiceManager;
import com.softcell.utils.GngDateUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.Callable;

/**
 * Created by yogeshb on 24/5/17.
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.EXPOSE_API,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ExternalExposeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalExposeController.class);

    @Autowired
    private ExternalExposeServiceManager externalExposeServiceManager;

    @PostMapping(EndPointReferrer.UPDATE_OPERATIONS_DATA)
    public ResponseEntity<BaseResponse> updateOperationsData(
            @RequestHeader HttpHeaders httpHeaders,
            @Validated({LOSDetailsRequest.LosGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid LOSDetailsRequest lOSDetailsRequest) throws Exception {

        LOGGER.debug("{} controller started", EndPointReferrer.APPLICATION_DATA);

        return new ResponseEntity<>(
                externalExposeServiceManager.updateOpsData(lOSDetailsRequest),
                HttpStatus.OK);
    }


    @PostMapping(EndPointReferrer.DAILY_DISBURSAL_REPORT)
    public Callable<byte[]> getDailyDisbursal(
            @RequestBody @NotNull @Valid DailyDisbursalRequest dailyDisbursalRequest) throws ParseException {

        LOGGER.debug("{} controller started", EndPointReferrer.APPLICATION_DATA);

        DateTime startDate = new DateTime(new SimpleDateFormat(GngDateUtil.ddMMyyyy).parse(dailyDisbursalRequest.getCurrentDate())).withHourOfDay(0)
                .withMinuteOfHour(0).withSecondOfMinute(0)
                .withZoneRetainFields(DateTimeZone.UTC);

        dailyDisbursalRequest.setStartDate(startDate.toDate());
        dailyDisbursalRequest.setEndDate( startDate.plusDays(1).toDate());


        return () -> externalExposeServiceManager.getDailyDisbursal(dailyDisbursalRequest);

    }

}
