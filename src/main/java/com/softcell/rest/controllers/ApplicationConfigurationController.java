package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AppConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 28/5/18.
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.APP_CONFIGURATION,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ApplicationConfigurationController {

    private Logger logger = LoggerFactory.getLogger(ApplicationConfigurationController.class);

    @Inject
    AppConfigurationManager appConfigurationManager;

    @PostMapping(value = EndPointReferrer.SYNCH_USERS)
    public ResponseEntity<BaseResponse> synchUsers(@Validated(value = {Header.FetchGrp.class})
            @RequestBody @Valid @NotNull AdminConfigRequest synchUsersRequest, HttpServletRequest httpRequest) {

        return new ResponseEntity<>(appConfigurationManager.synchUsers(synchUsersRequest, httpRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SYNCH_USER)
    public ResponseEntity<BaseResponse> synchUser(@Validated(value = {Header.FetchGrp.class})
                                                   @RequestBody @Valid @NotNull SynchUserRequest synchUserRequest) {

        return new ResponseEntity<>(appConfigurationManager.synchUser(synchUserRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.GET_USERS_HIERARCHY)
    public ResponseEntity<BaseResponse> getUsersHierarchy(@Validated(value = {Header.FetchGrp.class})
                                                   @RequestBody @Valid @NotNull AdminConfigRequest adminConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.getUsersHierarchy(adminConfigRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.GET_INSTITUTION_CONFIG)
    public ResponseEntity<BaseResponse> getInstitutionConfiguration(@Validated(value = {Header.FetchGrp.class})
                           @RequestBody @Valid @NotNull AdminConfigRequest adminConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.getInstitutionConfiguration(adminConfigRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.GET_INTIMATION_CONFIG)
    public ResponseEntity<BaseResponse> getIntimationConfiguration(@Validated(value = {Header.FetchGrp.class})
                            @RequestBody @Valid @NotNull AdminConfigRequest adminConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.getIntimationConfiguration(adminConfigRequest), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SAVE_MODULE_CONFIG)
    public ResponseEntity<BaseResponse> saveProductModuleConfig(@Validated(value = {Header.FetchGrp.class})
                          @RequestBody @Valid @NotNull InstitutionModuleRequest institutionModuleRequest) {

        return new ResponseEntity<>(appConfigurationManager.saveProductModuleConfig(
                institutionModuleRequest, EndPointReferrer.SAVE_MODULE_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_MODULE_CONFIG)
    public ResponseEntity<BaseResponse> deleteProductModuleConfig(@Validated(value = {Header.FetchGrp.class})
                                                                  @RequestBody @Valid InstitutionModuleRequest institutionModuleRequest) {

        return new ResponseEntity<>(appConfigurationManager.deleteProductModuleConfig(
                institutionModuleRequest, EndPointReferrer.DELETE_MODULE_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SAVE_VALUEMASTER_CONFIG)
    public ResponseEntity<BaseResponse> saveProductValueMaterConfig(@Validated(value = {Header.FetchGrp.class})
                                                                @RequestBody @Valid @NotNull InstitutionModuleRequest institutionModuleRequest) {

        return new ResponseEntity<>(appConfigurationManager.saveProductModuleConfig(
                institutionModuleRequest, EndPointReferrer.SAVE_VALUEMASTER_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_VALUEMASTER_CONFIG)
    public ResponseEntity<BaseResponse> deleteProductValueMaterConfig(@Validated(value = {Header.FetchGrp.class})
                                                                  @RequestBody @Valid InstitutionModuleRequest institutionModuleRequest) {

        return new ResponseEntity<>(appConfigurationManager.deleteProductModuleConfig(
                institutionModuleRequest, EndPointReferrer.DELETE_VALUEMASTER_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SAVE_INTIMATION_CONFIG)
    public ResponseEntity<BaseResponse> saveIntimationConfig(@Validated(value = {Header.FetchGrp.class})
                       @RequestBody @Valid @NotNull IntimationConfigRequest intimationConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.saveIntimationConfig(intimationConfigRequest,
                EndPointReferrer.SAVE_INTIMATION_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_INTIMATION_CONFIG)
    public ResponseEntity<BaseResponse> deleteIntimationConfig(@Validated(value = {Header.FetchGrp.class})
                       @RequestBody @Valid IntimationConfigRequest intimationConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.deleteIntimationConfig(intimationConfigRequest,
                EndPointReferrer.DELETE_INTIMATION_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.SAVE_INTIMATION_GROUP_CONFIG)
    public ResponseEntity<BaseResponse> saveIntimationGroupConfig(@Validated(value = {Header.FetchGrp.class})
                                                             @RequestBody @Valid @NotNull IntimationConfigRequest intimationConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.saveIntimationConfig(intimationConfigRequest,
                EndPointReferrer.SAVE_INTIMATION_GROUP_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.DELETE_INTIMATION_GROUP_CONFIG)
    public ResponseEntity<BaseResponse> deleteIntimationGroupConfig(@Validated(value = {Header.FetchGrp.class})
                                                                      @RequestBody @Valid IntimationConfigRequest intimationConfigRequest) {

        return new ResponseEntity<>(appConfigurationManager.deleteIntimationConfig(intimationConfigRequest,
                EndPointReferrer.DELETE_INTIMATION_GROUP_CONFIG), HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.GENERATE_INTIMATION_GROUPS)
    public ResponseEntity<BaseResponse> generateIntimationGroup(@Validated(value = {Header.FetchGrp.class})
                     @RequestBody @Valid GenerateGroupRequest generateGroupRequest) {

        return new ResponseEntity<>(appConfigurationManager.generateBranchGroups(generateGroupRequest, false), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CREATE_WORKFLOW)
    public ResponseEntity<BaseResponse> creRawWorkflow(
            @RequestBody @NotNull @Valid WorkflowRequest workflowRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.CREATE_WORKFLOW);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.createWorkflow(
                workflowRequest),HttpStatus.OK);
    }

    //template releted

    @GetMapping(EndPointReferrer.GET_TEMPLATES)
    public ResponseEntity<BaseResponse> getTemplateDetails(
            @NotNull @PathVariable String instituteId) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_TEMPLATES);
        return new ResponseEntity<>(
                appConfigurationManager.getTemplateDetails(instituteId),
                HttpStatus.OK);
    }

    @GetMapping(EndPointReferrer.GET_ALL_TEMPLATE)
    public ResponseEntity<BaseResponse> getAllTemplateDetails(
            @NotNull @PathVariable String instituteId) throws Exception {
        logger.debug("{} controller started", EndPointReferrer.GET_ALL_TEMPLATE);
        return new ResponseEntity<>(
                appConfigurationManager.getAllTemplateDetails(instituteId),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CREATE_TEMPLATE)
    public ResponseEntity<BaseResponse> createTemplate(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TemplateRequest templateRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.CREATE_TEMPLATE);
        return new ResponseEntity<>(appConfigurationManager.createTemplate(templateRequest,httpRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GENRATE_TEMPLATE)
    public ResponseEntity<BaseResponse> genrateTemplate(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TemplateRequest templateRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.GENRATE_TEMPLATE);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.genrateTemplate(templateRequest,httpRequest),HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_TEMPLATE)
    public ResponseEntity<BaseResponse> save(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TemplateRequest templateRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.SAVE_TEMPLATE);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.save(templateRequest,httpRequest),HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_TEMPLATE_INFO)
    public ResponseEntity<BaseResponse> updateTemplateInfo(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TemplateRequest templateRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.UPDATE_TEMPLATE_INFO);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.updateTemplateInfo(templateRequest,httpRequest),HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.DELETE_TEMPLATE)
    public ResponseEntity<BaseResponse> deleteTemplate(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid TemplateRequest templateRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.DELETE_TEMPLATE);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.deleteTemplate(templateRequest,httpRequest),HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.INTIMATE_USERS)
    public ResponseEntity<BaseResponse> intimateUsers(
            @Validated(value = {TemplateRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid IntimationRequest intimationRequest,
            HttpServletRequest httpRequest)throws Exception{
        logger.debug("{} controller started", EndPointReferrer.DELETE_TEMPLATE);
        return new ResponseEntity<BaseResponse>(appConfigurationManager.intimateUser(intimationRequest),HttpStatus.OK);

    }
}
