package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.queue.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.QueueAdminManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author archana
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.QUEUE_MGMT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class QueueAdminController {

    private static final Logger logger = LoggerFactory.getLogger(QueueAdminController.class);

    @Autowired
    private QueueAdminManager adminManager;

    @PostMapping(EndPointReferrer.STATUS)
    public ResponseEntity<BaseResponse> getStatus(@Validated(value = { Header.AppSourceGrp.class})
                                                  @RequestBody @Valid @NotNull QueueStatusRequest queueStatusRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.STATUS);

        return new ResponseEntity(adminManager.getStatus(queueStatusRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CRO_AUDIT)
    public ResponseEntity<BaseResponse> getCroAudit(@Validated(value = { Header.AppSourceGrp.class})
                                                  @RequestBody @Valid @NotNull CroAuditRequest croAuditRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.CRO_AUDIT);

        return new ResponseEntity(adminManager.getCroAuditData(croAuditRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CRO_STATISTICS)
    public ResponseEntity<BaseResponse> getCroStatistics(@Validated(value = { Header.AppSourceGrp.class})
                                                    @RequestBody @Valid @NotNull CroAuditRequest croAuditRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.CRO_STATISTICS);

        return new ResponseEntity(adminManager.getCroStatistics(croAuditRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.ASSIGN_CASES)
    public ResponseEntity<BaseResponse> assignCases(@Validated(value = { Header.AppSourceGrp.class})
                                                         @RequestBody @Valid @NotNull CaseAllocationRequest caseAllocationRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.ASSIGN_CASES);

        return new ResponseEntity(adminManager.assignCases(caseAllocationRequest), HttpStatus.OK);
    }



    @PostMapping(EndPointReferrer.SCHEDULER_INFO)
    public ResponseEntity<BaseResponse> getSchedulerInfo(@Validated(value = { Header.AppSourceGrp.class})
                                                 @RequestBody @Valid @NotNull SchedulerInfoRequest schedulerInfoRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.SCHEDULER_INFO);

        return new ResponseEntity(adminManager.getSchedulerInfo(schedulerInfoRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.RUN_SCHEDULER)
    public ResponseEntity<BaseResponse> runScheduler(@Validated(value = { Header.AppSourceGrp.class})
                                                 @RequestBody @Valid @NotNull RunSchedulerRequest schedulerInfoRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.RUN_SCHEDULER);

        return new ResponseEntity(adminManager.runScheduler(schedulerInfoRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.CHANGE_AVAILABILITY)
    public ResponseEntity<BaseResponse> changeAvailability(@Validated(value = { Header.AppSourceGrp.class})
                                                     @RequestBody @Valid @NotNull ChangeAvailabilityRequest changeAvailabilityRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.CHANGE_AVAILABILITY);

        return new ResponseEntity(adminManager.changeAvailability(changeAvailabilityRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UNASSIGNED_APPLICATIONS)
    public ResponseEntity<BaseResponse> getUnassignedApplications(@Validated(value = { Header.AppSourceGrp.class})
                                                     @RequestBody @Valid @NotNull RunSchedulerRequest schedulerInfoRequest) throws Exception {

        logger.debug("{} controller started ", EndPointReferrer.UNASSIGNED_APPLICATIONS);

        return new ResponseEntity(adminManager.getAllUnassignedApplicationIds(), HttpStatus.OK);
    }
}
