package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.digitization.DigitizationApplicationReportRequest;
import com.softcell.gonogo.model.request.digitization.DigitizeApplicationFormRequest;
import com.softcell.gonogo.model.request.digitization.DownloadDigitizationFormRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DigitizationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author yogeshb
 */
@RestController
@RequestMapping(
        value = EndPointReferrer.DIGITIZATION_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE


)
public class DigitizationController {

    private static final Logger logger = LoggerFactory.getLogger(DigitizationController.class);

    @Autowired
    private DigitizationManager digitizationManager;

    /**
     * @param request
     * @return
     */
    @PostMapping(EndPointReferrer.DOWNLOAD_APPLICATION_FORM)
    public ResponseEntity<BaseResponse> getApplicationPDF(
            @Validated(value = {DigitizeApplicationFormRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DigitizeApplicationFormRequest request) throws Exception {

        return new ResponseEntity<>(digitizationManager.getApplicationForm(request.getHeader().getInstitutionId(),
                request.getHeader().getProduct().toProductId(), request.getRefID()), HttpStatus.OK);
    }

    /**
     * @param request
     * @return
     */
    @PostMapping(EndPointReferrer.DOWNLOAD_AGREEMENT_FORM)
    public ResponseEntity<BaseResponse> getAgreementFormPDF(
            @Validated(value = {DigitizeApplicationFormRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DigitizeApplicationFormRequest request) throws Exception {

        return new ResponseEntity<>(digitizationManager.getAgreementForm(request.getHeader().getInstitutionId(),
                request.getHeader().getProduct().toProductId(), request.getRefID()), HttpStatus.OK);

    }

    /**
     *
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping(EndPointReferrer.DOWNLOAD_DELIVERY_ORDER)
    public ResponseEntity<BaseResponse> getDeliveryOrder(
            @Validated(value = {DigitizeApplicationFormRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DigitizeApplicationFormRequest request) throws Exception {

        return new ResponseEntity<>(digitizationManager.getDeliveryOrder(request.getHeader().getInstitutionId(),
                request.getHeader().getProduct().toProductId(), request.getRefID(), request.getFileOperationType()), HttpStatus.OK);

    }

    /**
     * this service is used to download and save Tvs application and agreement form in the database
     * @param downloadDigitizationFormRequest
     * @return
     * @throws Exception
     */

    @PostMapping(EndPointReferrer.DOWNLOAD_DIGITIZATION_FORM)
    public ResponseEntity<BaseResponse> downloadDigitizationForm(
            @Validated(value = {DownloadDigitizationFormRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DownloadDigitizationFormRequest downloadDigitizationFormRequest) throws Exception {

        return new ResponseEntity<>(digitizationManager.downloadDigitizationForm(downloadDigitizationFormRequest), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.DOWNLOAD_ACH_MANDATE_FORM)
    public ResponseEntity<BaseResponse> downloadAchMandateForm(
            @Validated(value = {DownloadDigitizationFormRequest.FetchGrp.class, Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DownloadDigitizationFormRequest downloadAchFormRequest) throws Exception {

        return new ResponseEntity<>(digitizationManager.downloadAchMandateForm(downloadAchFormRequest.getHeader().getInstitutionId(),
                downloadAchFormRequest.getHeader().getProduct().toProductId(), downloadAchFormRequest.getRefID()), HttpStatus.OK);

    }

    /**
     * For SBFC Sanction letter for LAP
     */
    @PostMapping(EndPointReferrer.DOWNLOAD_SANCTION_LETTER_LAP)
    public ResponseEntity<BaseResponse> getSanctionLetter(
            @Validated(value = {DigitizationApplicationReportRequest.FetchGrp.class,Header.InstWithProductGrp.class})
            @RequestBody @NotNull @Valid DigitizationApplicationReportRequest request) throws Exception{

        return new ResponseEntity<BaseResponse>(digitizationManager.downloadSanctionLetter(request), HttpStatus.OK);
    }


}
