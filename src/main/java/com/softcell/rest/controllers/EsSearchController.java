package com.softcell.rest.controllers;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.queuemanager.search.request.ESQueueRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.EsSearchManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;


@RestController
@RequestMapping(
        value = EndPointReferrer.ES_SEARCH_BASE_ENDPOINT,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept="+MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class EsSearchController {

    private static final Logger logger = LoggerFactory.getLogger(EsSearchController.class);

    @Autowired
    private EsSearchManager esSearchManager;

    @PostMapping(EndPointReferrer.SEARCH)
    public ResponseEntity<BaseResponse> search(@RequestBody
                                                             ESQueueRequest esQueueRequest) {
        return new ResponseEntity<>(esSearchManager.croQueue(esQueueRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATEINDEX_STARTDATE_ENDDATE)
    public Callable<ResponseEntity<BaseResponse>> search(@PathVariable String startDate, @PathVariable String endDate
            , @PathVariable String instituionId) {

        return () -> ResponseEntity.status(HttpStatus.ACCEPTED).body(esSearchManager.updateDatabaseIndex(startDate, endDate, instituionId));
    }
}
