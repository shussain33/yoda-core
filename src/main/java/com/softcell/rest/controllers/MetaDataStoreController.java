package com.softcell.rest.controllers;

/**
 * kishorp11:08:18 PM  Copyright Softcell Technolgy
 **/


import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.SchemeMetadataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.MasterDataViewManager;
import com.softcell.service.MetaDataStoreManager;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * @author kishorp
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class MetaDataStoreController {

    private static final Logger logger = LoggerFactory.getLogger(MetaDataStoreController.class);

    @Autowired
    private MetaDataStoreManager metaDataStoreManager;

    @Autowired
    private MasterDataViewManager masterDataViewManager;

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.GET_CAR_SURROGATE_MASTER)
    public ResponseEntity<BaseResponse> getCarSurrogateMaster(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {

        logger.info("Inside get-car-surrogate-master");

        return ResponseEntity.status(HttpStatus.OK).body(
                metaDataStoreManager.getCarSurrogateMaster(metadataRequest.getHeader().getInstitutionId(), metadataRequest.getQuery()));
    }

    /**
     * @param schemeMetadataRequest
     * @return
     */
    @Deprecated
    @PostMapping(EndPointReferrer.FILTERED_SCHEME_MASTER_DEPRECATED)
    public ResponseEntity<List<SchemeMasterData>> getSchemeMasterDeprecated(
            @Validated(value = {SchemeMetadataRequest.FetchGrp.class, Header.FetchWithDealerCriteriaGrp.class})
            @RequestBody @NotNull @Valid SchemeMetadataRequest schemeMetadataRequest) {

        logger.info("Inside filtered-scheme-master");

        return ResponseEntity.status(HttpStatus.OK).body(metaDataStoreManager.getFilteredSchemes(schemeMetadataRequest));

    }

    /**
     * @param schemeMetadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.FILTERED_SCHEME_MASTER)
    public ResponseEntity<BaseResponse> getSchemeMaster(
            @Validated(value = {SchemeMetadataRequest.FetchGrp.class, Header.FetchWithDealerCriteriaGrp.class, Header.InstWithProductGrp.class})

            @RequestBody @NotNull @Valid SchemeMetadataRequest schemeMetadataRequest) throws Exception {

        logger.info("Inside filtered-scheme-master");

        return new ResponseEntity<>(masterDataViewManager
                .getFilteredSchemes(schemeMetadataRequest), HttpStatus.OK);
    }

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.BANK_DETAILS)
    public ResponseEntity<BaseResponse> bankMasterDetails(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getBankDetails(metadataRequest.getHeader().getInstitutionId()),
                HttpStatus.OK);
    }

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.ASSET_MODEL_MAKE_WEB)
    public ResponseEntity<BaseResponse> assetModelWeb(
            @Validated(value = {ApplicationMetadataRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getAssetMaster(metadataRequest.getHeader().getInstitutionId(), metadataRequest.getQuery()),
                HttpStatus.OK);
    }

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.PINCODE_DETAILS_WEB)
    public ResponseEntity<BaseResponse> pinCodeDetails(
            @Validated(value = {ApplicationMetadataRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getPinCodeDetails(metadataRequest.getHeader().getInstitutionId(), metadataRequest.getQuery()),
                HttpStatus.OK);
    }

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(EndPointReferrer.EMPLOYER_MASTER_DETAILS_WEB)
    public ResponseEntity<BaseResponse> employeDetails(
            @Validated(value = {Header.FetchGrp.class, ApplicationMetadataRequest.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getEmployerMasterDetails(metadataRequest.getHeader().getInstitutionId(), metadataRequest.getQuery()),
                HttpStatus.OK);
    }

    /**
     * @param modelVariantMasterRequest
     * @return
     */
    @PostMapping(value = EndPointReferrer.GET_MODEL_VARIANTS)
    public ResponseEntity<BaseResponse> getModelVariantManufacturer(
            @RequestBody @Validated @NotNull ModelVariantMasterRequest modelVariantMasterRequest) {

        return new ResponseEntity<>(metaDataStoreManager
                .getModelVariantManufacturer(modelVariantMasterRequest), HttpStatus.OK);

    }

    /**
     * @param modelVariantMasterRequest
     * @return
     */
    @PostMapping(value = EndPointReferrer.GET_REFERENCE_RELATION_DETAILS)
    public ResponseEntity<BaseResponse> getReferenceMasterDetails(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull ModelVariantMasterRequest modelVariantMasterRequest) {

        return new ResponseEntity<>(metaDataStoreManager
                .getReferenceDetails(modelVariantMasterRequest), HttpStatus.OK);
    }

    /**
     * @param metadataRequest
     * @return
     */
    @PostMapping(value = EndPointReferrer.CITY_MASTER_DETAILS_WEB)
    public ResponseEntity<BaseResponse> getCity(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationMetadataRequest metadataRequest){
        return new ResponseEntity<>(metaDataStoreManager.getCity(metadataRequest.getHeader().getInstitutionId(),
                metadataRequest.getQuery()),HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @GetMapping(value = EndPointReferrer.GET_ALL_BANK_NAME + "/{institutionId}")
    public ResponseEntity<BaseResponse> getBankNames(
            @Valid @NotEmpty @PathVariable String institutionId) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBankNames(institutionId), HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @GetMapping(value = EndPointReferrer.GET_STATE_BY_BANK_NAME + "/{institutionId}/{bank}")
    public ResponseEntity<BaseResponse> getStates(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String bank) {

        return new ResponseEntity<>(metaDataStoreManager
                .getStates(institutionId,bank), HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @GetMapping(value = EndPointReferrer.GET_DISTRICT + "/{institutionId}/{bank}/{state}")
    public ResponseEntity<BaseResponse> getDistrict(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String bank,
            @Valid @NotEmpty @PathVariable String state) {

        return new ResponseEntity<>(metaDataStoreManager
                .getDistrict(institutionId,bank,state), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_BRANCHES + "/{institutionId}/{bank}/{state}/{district}")
    public ResponseEntity<BaseResponse> getBranches(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String bank,
            @Valid @NotEmpty @PathVariable String state,
            @Valid @NotEmpty @PathVariable String district) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBranches(institutionId,bank,state,district), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_BANK_DETAILS + "/{institutionId}")
    public ResponseEntity<BaseResponse> getBankDetails(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @RequestParam("bank") String bank,
            @Valid @NotEmpty @RequestParam("state") String state,
            @Valid @NotEmpty @RequestParam("district") String district,
            @Valid @NotEmpty @RequestParam("branch") String branch) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBankMasterDetails(institutionId,bank,state,district,branch), HttpStatus.OK);

    }

    @GetMapping(value = EndPointReferrer.GET_BANK_DETAILS_BY_IFSC_CODE + "/{institutionId}/{ifsc}")
    public ResponseEntity<BaseResponse> getBankDetails(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String ifsc) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBankMasterDetailsByIfscCode(institutionId,ifsc), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_BANK_DETAILS_BY_MICR_CODE + "/{institutionId}/{micr}")
    public ResponseEntity<BaseResponse> getBankDetailsByMicr(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String micr) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBankMasterDetailsByMicrCode(institutionId,micr), HttpStatus.OK);
    }

    @GetMapping(value = EndPointReferrer.GET_BANK_NAME_FROM_LOS_MASTER + "/{institutionId}/{bankName}")
    public ResponseEntity<BaseResponse> getBankNames(
            @Valid @NotEmpty @PathVariable String institutionId,
            @Valid @NotEmpty @PathVariable String bankName) {

        return new ResponseEntity<>(metaDataStoreManager
                .getBankNamesFromBankMaster(institutionId,bankName), HttpStatus.OK);

    }

    @GetMapping(value = EndPointReferrer.GET_ACCOUNT_TYPE + "/{institutionId}")
    public ResponseEntity<BaseResponse> getAccountTypes(
            @NotEmpty @PathVariable String institutionId
            ) {

        return new ResponseEntity<>(metaDataStoreManager
                .getAccountType(institutionId), HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.PINCODE_MASTER_DETAILS)
    public ResponseEntity<BaseResponse> getPinCodeMasterDetails(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getPincodeMaster(metadataRequest.getHeader().getInstitutionId()),
                HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.EMPLOYER_MASTER_DETAILS)
    public ResponseEntity<BaseResponse> getEmployerMasterDetails(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull @Valid ApplicationMetadataRequest metadataRequest) {
        return new ResponseEntity<>(
                metaDataStoreManager.getEmployerMaster(metadataRequest.getHeader().getInstitutionId()),
                HttpStatus.OK);
    }

    @PostMapping(value = EndPointReferrer.CITY_MASTER_DETAILS)
    public ResponseEntity<BaseResponse> getCitiesMasterDetails(
            @Validated(value = {Header.FetchGrp.class})
            @RequestBody @NotNull ApplicationMetadataRequest metadataRequest){
        return new ResponseEntity<>(metaDataStoreManager.getCityMaster(metadataRequest.getHeader().getInstitutionId()),HttpStatus.OK);
    }


}
