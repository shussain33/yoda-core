package com.softcell.rest.controllers;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dooperation.DeliveryOrderOperationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DoOperationManager;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 30/8/17.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
/**
 * This Controller is used to manage all Delivery Order Related Operations .Such as Cancel Delivery Order ,
 * Restore canceled delivery Order etc.
 */
public class DoOperationController {


    private static final Logger logger = LoggerFactory.getLogger(DoOperationController.class);

    @Autowired
    private DoOperationManager doOperationManager;


    @PostMapping(EndPointReferrer.CANCEL_DELIVERY_ORDER)
    public ResponseEntity<BaseResponse> cancelDeliveryOrder(
            @Validated(value = {DeliveryOrderOperationRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.CANCEL_DELIVERY_ORDER);

        return new ResponseEntity<>(
                doOperationManager.cancelDeliveryOrder(deliveryOrderOperationRequest),
                HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.RESTORE_DELIVERY_ORDER)
    public ResponseEntity<BaseResponse> restoreDeliveryOrder(
            @Validated(value = {DeliveryOrderOperationRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RESTORE_DELIVERY_ORDER);

        return new ResponseEntity<>(
                doOperationManager.restoreDeliveryOrder(deliveryOrderOperationRequest),
                HttpStatus.OK);

    }

    @PostMapping(EndPointReferrer.GET_DELIVERY_ORDER_STATUS)
    public ResponseEntity<BaseResponse> getDeliveryOrderStatus(
            @Validated(value = {DeliveryOrderOperationRequest.FetchGrp.class, Header.FetchGrp.class})
            @RequestBody @NotNull @Valid DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.RESTORE_DELIVERY_ORDER);

        return new ResponseEntity<>(
                doOperationManager.getDeliveryOrderStatus(deliveryOrderOperationRequest),
                HttpStatus.OK);

    }

    @GetMapping(EndPointReferrer.GET_DO_OPERATION_LOG + "/{refId}/{institutionId}")
    public ResponseEntity<BaseResponse> getDoOperationLog(@PathVariable("refId") @NotBlank @Valid String refId,
                                                          @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug("{} controller started", EndPointReferrer.GET_DO_OPERATION_LOG);

        return new ResponseEntity<>(
                doOperationManager.getDoOperationLog(refId, institutionId),
                HttpStatus.OK);

    }


}
