package com.softcell.rest.controllers;

import com.softcell.dao.mongodb.repository.CommonFunctionalityService;
import com.softcell.gonogo.model.request.CollectionConfigRequest;
import com.softcell.gonogo.model.request.CollectionDataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class CommonFunctionalityController {

    private static final Logger logger = LoggerFactory.getLogger(CommonFunctionalityController.class);

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private CommonFunctionalityService commonFunctionalityService;

    @PostMapping(EndPointReferrer.GET_COLLECTION_DATA)
    public ResponseEntity<BaseResponse> getDataForSelectedCollection(@Validated({Header.FetchGrp.class})
                                                                     @RequestBody @NotNull CollectionDataRequest collectionDataRequest) {
        logger.debug("{} controller started", EndPointReferrer.GET_COLLECTION_DATA);
        return new ResponseEntity<>(
                commonFunctionalityService.getData(collectionDataRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.SAVE_COLLECTION_CONFIG)
    public ResponseEntity<BaseResponse> saveCollectionConfig(@Validated({Header.FetchGrp.class})
                                                             @RequestBody @NotNull CollectionConfigRequest request) {
        logger.debug("{} controller started", EndPointReferrer.SAVE_COLLECTION_CONFIG);
        return new ResponseEntity<>(
                commonFunctionalityService.saveCollectionConfiguration(request), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.GET_COLLECTION_CONFIG)
    public ResponseEntity<BaseResponse> getCollectionConfig() {
        logger.debug("{} controller started", EndPointReferrer.GET_COLLECTION_CONFIG);
        return new ResponseEntity<>(
                commonFunctionalityService.getCollectionConfiguration(), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.UPDATE_COLLECTION_DATA)
    public ResponseEntity<BaseResponse> updateCollectionData(@Validated({Header.FetchGrp.class})
                                                             @RequestBody @NotNull CollectionDataRequest collectionDataRequest) {
        logger.debug("{} controller started", EndPointReferrer.UPDATE_COLLECTION_DATA);
        return new ResponseEntity<>(
                commonFunctionalityService.saveData(collectionDataRequest), HttpStatus.OK);
    }

    @PostMapping(EndPointReferrer.DOWNLOAD_JSON_DATA)
    public ResponseEntity<BaseResponse> getDownloadedJsonData(@Validated({Header.FetchGrp.class})
                                                              @RequestBody @NotNull CollectionDataRequest collectionDataRequest){
        return new ResponseEntity<>(
                commonFunctionalityService.getDownloadedJsonData(collectionDataRequest), HttpStatus.OK);
    }
}
