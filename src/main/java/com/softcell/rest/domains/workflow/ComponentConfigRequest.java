package com.softcell.rest.domains.workflow;

import com.softcell.constants.ComponentName;
import com.softcell.constants.ModuleKeyName;

/**
 * @author bhuvneshk
 */
public class ComponentConfigRequest {

    private String institutionId;
    private ComponentName componentName;
    private boolean componentActive;
    private ModuleKeyName moduleKeyName;
    private boolean moduleKeyActive;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public ComponentName getComponentName() {
        return componentName;
    }

    public void setComponentName(ComponentName componentName) {
        this.componentName = componentName;
    }

    public boolean isComponentActive() {
        return componentActive;
    }

    public void setComponentActive(boolean componentActive) {
        this.componentActive = componentActive;
    }

    public ModuleKeyName getModuleKeyName() {
        return moduleKeyName;
    }

    public void setModuleKeyName(ModuleKeyName moduleKeyName) {
        this.moduleKeyName = moduleKeyName;
    }

    public boolean isModuleKeyActive() {
        return moduleKeyActive;
    }

    public void setModuleKeyActive(boolean moduleKeyActive) {
        this.moduleKeyActive = moduleKeyActive;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ComponentConfigRequest [institutionId=");
        builder.append(institutionId);
        builder.append(", componentName=");
        builder.append(componentName);
        builder.append(", componentActive=");
        builder.append(componentActive);
        builder.append(", moduleKeyName=");
        builder.append(moduleKeyName);
        builder.append(", moduleKeyActive=");
        builder.append(moduleKeyActive);
        builder.append("]");
        return builder.toString();
    }


}
