package com.softcell.rest.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.MetaDataDictionaryType;
import com.softcell.constants.Product;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Created by prateek on 11/3/17.
 */
public class MetadataDictRequest {

    @JsonProperty("sInstitutionId")
    @NotBlank
    private String institutionId;

    @JsonProperty("sProduct")
    @NotNull
    private Product product;

    @JsonProperty("sType")
    @NotNull
    private MetaDataDictionaryType type;

    @JsonProperty("mRenameValue")
    private Map<String, String> renameValueMap;

    @JsonProperty("cValues")
    private Collection<String> values;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public MetaDataDictionaryType getType() {
        return type;
    }

    public void setType(MetaDataDictionaryType type) {
        this.type = type;
    }

    public Collection<String> getValues() {
        return values;
    }

    public void setValues(Collection<String> values) {
        this.values = values;
    }

    public Map<String, String> getRenameValueMap() {
        return renameValueMap;
    }

    public void setRenameValueMap(Map<String, String> renameValueMap) {
        this.renameValueMap = renameValueMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetadataDictRequest)) return false;
        MetadataDictRequest that = (MetadataDictRequest) o;
        return Objects.equals(getInstitutionId(), that.getInstitutionId()) &&
                getProduct() == that.getProduct() &&
                getType() == that.getType() &&
                Objects.equals(getRenameValueMap(), that.getRenameValueMap()) &&
                Objects.equals(getValues(), that.getValues());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInstitutionId(), getProduct(), getType(), getRenameValueMap(), getValues());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MetadataDictRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", product=").append(product);
        sb.append(", type=").append(type);
        sb.append(", renameValueMap=").append(renameValueMap);
        sb.append(", values=").append(values);
        sb.append('}');
        return sb.toString();
    }
}
