package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "CredentialsDocument")
public class InstitutionServiceCrediantials extends AuditEntity {

    @JsonProperty("INSTITUTION_ID")
    private String institutionId;

    @JsonProperty("USER_ID")
    private String userId;

    @JsonProperty("PASSWORD")
    private String password;

    @JsonProperty("Credentials")
    private List<Credentials> credentialList;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Credentials> getCredentialList() {
        return credentialList;
    }

    public void setCredentialList(List<Credentials> credentialList) {
        this.credentialList = credentialList;
    }

    @Override
    public String toString() {
        return "InstitutionServiceCrediantials [institutionId=" + institutionId
                + ", userId=" + userId + ", password=" + password
                + ", credentialList=" + credentialList + "]";
    }
}
