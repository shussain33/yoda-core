package com.softcell.config;

import java.util.Objects;

/**
 * @author yogeshb
 */
public class AadharConfiguration {

    private String institutionId;

    private String aggregatorId;

    private String password;

    private String memberId;

    private String aadharOtpServiceUrl;

    private String aadharEkyServiceUrl;

    private String baseUrl;

    private String licenseKey;

    private String pdfReportFlag;

    private String auaCode;

    private String subAuaCode;

    private int connectionTimeout;

    private String authApiVersion;
    /**
     * following fields for authentication.
     * if these are true the this will send to Aadhar service.
     */
    private boolean genderValidation;

    private boolean dobValidation;

    private boolean ageValidation;

    private boolean nameValidation;

    private boolean emailValidation;

    private boolean phoneValidation;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the aggregatorId
     */
    public String getAggregatorId() {
        return aggregatorId;
    }

    /**
     * @param aggregatorId the aggregatorId to set
     */
    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * @param memberId the memberId to set
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     * @return the aadharOtpServiceUrl
     */
    public String getAadharOtpServiceUrl() {
        return aadharOtpServiceUrl;
    }

    /**
     * @param aadharOtpServiceUrl the aadharServiceUrl to set
     */
    public void setAadharOtpServiceUrl(String aadharOtpServiceUrl) {
        this.aadharOtpServiceUrl = aadharOtpServiceUrl;
    }

    /**
     * @return the aadharEkyServiceUrl
     */
    public String getAadharEkyServiceUrl() {
        return aadharEkyServiceUrl;
    }

    /**
     * @param aadharEkyServiceUrl the aadharEkyServiceUrl to set
     */
    public void setAadharEkyServiceUrl(String aadharEkyServiceUrl) {
        this.aadharEkyServiceUrl = aadharEkyServiceUrl;
    }

    /**
     * @return the licenseKey
     */
    public String getLicenseKey() {
        return licenseKey;
    }

    /**
     * @param licenseKey the licenseKey to set
     */
    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    /**
     * @return the auaCode
     */
    public String getAuaCode() {
        return auaCode;
    }

    /**
     * @param auaCode the auaCode to set
     */
    public void setAuaCode(String auaCode) {
        this.auaCode = auaCode;
    }

    /**
     * @return the subAuaCode
     */
    public String getSubAuaCode() {
        return subAuaCode;
    }

    /**
     * @param subAuaCode the subAuaCode to set
     */
    public void setSubAuaCode(String subAuaCode) {
        this.subAuaCode = subAuaCode;
    }

    /**
     * @return the connectionTimeout
     */
    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * @param connectionTimeout the connectionTimeout to set
     */
    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * @return the genderValidation
     */
    public boolean isGenderValidation() {
        return genderValidation;
    }

    /**
     * @param genderValidation the genderValidation to set
     */
    public void setGenderValidation(boolean genderValidation) {
        this.genderValidation = genderValidation;
    }

    /**
     * @return the dobValidation
     */
    public boolean isDobValidation() {
        return dobValidation;
    }

    /**
     * @param dobValidation the dobValidation to set
     */
    public void setDobValidation(boolean dobValidation) {
        this.dobValidation = dobValidation;
    }

    /**
     * @return the ageValidation
     */
    public boolean isAgeValidation() {
        return ageValidation;
    }

    /**
     * @param ageValidation the ageValidation to set
     */
    public void setAgeValidation(boolean ageValidation) {
        this.ageValidation = ageValidation;
    }

    /**
     * @return the nameValidation
     */
    public boolean isNameValidation() {
        return nameValidation;
    }

    /**
     * @param nameValidation the nameValidation to set
     */
    public void setNameValidation(boolean nameValidation) {
        this.nameValidation = nameValidation;
    }

    /**
     * @return the emailValidation
     */
    public boolean isEmailValidation() {
        return emailValidation;
    }

    /**
     * @param emailValidation the emailValidation to set
     */
    public void setEmailValidation(boolean emailValidation) {
        this.emailValidation = emailValidation;
    }

    /**
     * @return the phoneValidation
     */
    public boolean isPhoneValidation() {
        return phoneValidation;
    }

    /**
     * @param phoneValidation the phoneValidation to set
     */
    public void setPhoneValidation(boolean phoneValidation) {
        this.phoneValidation = phoneValidation;
    }

    public String getPdfReportFlag() {
        return pdfReportFlag;
    }

    public void setPdfReportFlag(String pdfReportFlag) {
        this.pdfReportFlag = pdfReportFlag;
    }

    public String getAuthApiVersion() {
        return authApiVersion;
    }

    public void setAuthApiVersion(String authApiVersion) {
        this.authApiVersion = authApiVersion;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {
        private AadharConfiguration aadharConfiguration = new AadharConfiguration();

        public AadharConfiguration build (){
            return this.aadharConfiguration;
        }

        public Builder institutionId(String institutionId){
            this.aadharConfiguration.setInstitutionId(institutionId);
            return this;
        }

        public Builder aggregatorId(String aggregatorId){
            this.aadharConfiguration.setAggregatorId(aggregatorId);
            return this;
        }

        public Builder password(String password){
            this.aadharConfiguration.setPassword(password);
            return this;
        }

        public Builder memberId(String memberId){
            this.aadharConfiguration.setMemberId(memberId);
            return this;
        }

        public Builder aadharOtpServiceUrl(String aadharOtpServiceUrl) {
            this.aadharConfiguration.setAadharOtpServiceUrl(aadharOtpServiceUrl);
            return this;
        }

        public Builder aadharEkyServiceUrl(String aadharEkyServiceUrl){
            this.aadharConfiguration.setAadharEkyServiceUrl(aadharEkyServiceUrl);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.aadharConfiguration.setBaseUrl(baseUrl);
            return this;
        }

        public Builder licenseKey(String licenseKey){
            this.aadharConfiguration.setLicenseKey(licenseKey);
            return this;
        }

        public Builder pdfReportFlag(String pdfReportFlag){
            this.aadharConfiguration.setPdfReportFlag(pdfReportFlag);
            return this;
        }

        public Builder auaCode(String auaCode){
            this.aadharConfiguration.setAuaCode(auaCode);
            return this;
        }

        public Builder subAuaCode(String subAuaCode){
            this.aadharConfiguration.setSubAuaCode(subAuaCode);
            return this;
        }

        public Builder connectionTimeout(int connectionTimeout) {
            this.aadharConfiguration.setConnectionTimeout(connectionTimeout);
            return this;
        }

        public Builder genderValidation(boolean genderValidation){
            this.aadharConfiguration.setGenderValidation(genderValidation);
            return this;
        }

        public Builder dobValidation(boolean dobValidation ){
            this.aadharConfiguration.setDobValidation(dobValidation);
            return this;
        }

        public Builder ageValidation(boolean ageValidation){
            this.aadharConfiguration.setAgeValidation(ageValidation);
            return this;
        }

        public Builder nameValidation(boolean nameValidation){
            this.aadharConfiguration.setNameValidation(nameValidation);
            return this;
        }

        public Builder emailValidation(boolean emailValidation){
            this.aadharConfiguration.setEmailValidation(emailValidation);
            return this;
        }

        public Builder phoneValidation(boolean phoneValidation){
            this.aadharConfiguration.setPhoneValidation(phoneValidation);
            return this;
        }

        public Builder authApiVersion(String authApiVersion) {
            this.aadharConfiguration.authApiVersion = authApiVersion;
            return this;
        }


    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AadharConfiguration{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", aggregatorId='").append(aggregatorId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", memberId='").append(memberId).append('\'');
        sb.append(", aadharOtpServiceUrl='").append(aadharOtpServiceUrl).append('\'');
        sb.append(", aadharEkyServiceUrl='").append(aadharEkyServiceUrl).append('\'');
        sb.append(", baseUrl='").append(baseUrl).append('\'');
        sb.append(", licenseKey='").append(licenseKey).append('\'');
        sb.append(", pdfReportFlag='").append(pdfReportFlag).append('\'');
        sb.append(", auaCode='").append(auaCode).append('\'');
        sb.append(", subAuaCode='").append(subAuaCode).append('\'');
        sb.append(", connectionTimeout=").append(connectionTimeout);
        sb.append(", authApiVersion='").append(authApiVersion).append('\'');
        sb.append(", genderValidation=").append(genderValidation);
        sb.append(", dobValidation=").append(dobValidation);
        sb.append(", ageValidation=").append(ageValidation);
        sb.append(", nameValidation=").append(nameValidation);
        sb.append(", emailValidation=").append(emailValidation);
        sb.append(", phoneValidation=").append(phoneValidation);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AadharConfiguration that = (AadharConfiguration) o;
        return Objects.equals(connectionTimeout, that.connectionTimeout) &&
                Objects.equals(genderValidation, that.genderValidation) &&
                Objects.equals(dobValidation, that.dobValidation) &&
                Objects.equals(ageValidation, that.ageValidation) &&
                Objects.equals(nameValidation, that.nameValidation) &&
                Objects.equals(emailValidation, that.emailValidation) &&
                Objects.equals(phoneValidation, that.phoneValidation) &&
                Objects.equals(institutionId, that.institutionId) &&
                Objects.equals(aggregatorId, that.aggregatorId) &&
                Objects.equals(password, that.password) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(aadharOtpServiceUrl, that.aadharOtpServiceUrl) &&
                Objects.equals(aadharEkyServiceUrl, that.aadharEkyServiceUrl) &&
                Objects.equals(baseUrl, that.baseUrl) &&
                Objects.equals(licenseKey, that.licenseKey) &&
                Objects.equals(pdfReportFlag, that.pdfReportFlag) &&
                Objects.equals(auaCode, that.auaCode) &&
                Objects.equals(subAuaCode, that.subAuaCode) &&
                Objects.equals(authApiVersion, that.authApiVersion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(institutionId, aggregatorId, password, memberId, aadharOtpServiceUrl, aadharEkyServiceUrl, baseUrl, licenseKey, pdfReportFlag, auaCode, subAuaCode, connectionTimeout, authApiVersion, genderValidation, dobValidation, ageValidation, nameValidation, emailValidation, phoneValidation);
    }
}