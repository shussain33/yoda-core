package com.softcell.config;

/**
 * This class represents model for holding the amazon S3 specific details.
 *
 * @author vinodk
 */
public class AmazonS3Configuration {

    private String accessKey;

    private String secretKey;

    private String bucketName;

    /**
     * @return the accessKey
     */
    public String getAccessKey() {
        return accessKey;
    }

    /**
     * @param accessKey the accessKey to set
     */
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * @return the secretKey
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * @param secretKey the secretKey to set
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * @return the bucketName
     */
    public String getBucketName() {
        return bucketName;
    }

    /**
     * @param bucketName the bucketName to set
     */
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }


    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private  AmazonS3Configuration amazonS3Configuration = new AmazonS3Configuration();

        public AmazonS3Configuration build(){
            return this.amazonS3Configuration;
        }

        public Builder accessKey(String accessKey){
            this.amazonS3Configuration.setAccessKey(accessKey);
            return this;
        }

        public Builder secretKey(String secretKey){
            this.amazonS3Configuration.setSecretKey(secretKey);
            return this;
        }

        public Builder bucketName(String bucketName){
            this.amazonS3Configuration.setBucketName(bucketName);
            return this;
        }

    }

    /**
     *
      * @return
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AmazonS3Configuration [accessKey=");
        builder.append(accessKey);
        builder.append(", secretKey=");
        builder.append(secretKey);
        builder.append(", bucketName=");
        builder.append(bucketName);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((accessKey == null) ? 0 : accessKey.hashCode());
        result = prime * result
                + ((bucketName == null) ? 0 : bucketName.hashCode());
        result = prime * result
                + ((secretKey == null) ? 0 : secretKey.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AmazonS3Configuration other = (AmazonS3Configuration) obj;
        if (accessKey == null) {
            if (other.accessKey != null)
                return false;
        } else if (!accessKey.equals(other.accessKey))
            return false;
        if (bucketName == null) {
            if (other.bucketName != null)
                return false;
        } else if (!bucketName.equals(other.bucketName))
            return false;
        if (secretKey == null) {
            if (other.secretKey != null)
                return false;
        } else if (!secretKey.equals(other.secretKey))
            return false;
        return true;
    }
}
