package com.softcell.config;

/**
 * Contains los dmz connector information
 *
 * @author bhuvneshk
 */
public class LosConfiguration {

    private String gngPostUrl;

    private String mbPostUrl;

    private String baseUrl;

    /**
     * @return the gngPostUrl
     */
    public String getGngPostUrl() {
        return gngPostUrl;
    }

    /**
     * @param gngPostUrl the gngPostUrl to set
     */
    public void setGngPostUrl(String gngPostUrl) {
        this.gngPostUrl = gngPostUrl;
    }

    /**
     * @return the mbPostUrl
     */
    public String getMbPostUrl() {
        return mbPostUrl;
    }

    /**
     * @param mbPostUrl the mbPostUrl to set
     */
    public void setMbPostUrl(String mbPostUrl) {
        this.mbPostUrl = mbPostUrl;
    }

    /**
     * @return the baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl the baseUrl to set
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private LosConfiguration losConfiguration = new LosConfiguration();

        public LosConfiguration build(){
            return this.losConfiguration;
        }

        public Builder gngPostUrl(String gngPostUrl){
            this.losConfiguration.setGngPostUrl(gngPostUrl);
            return this;
        }

        public Builder mbPostUrl(String mbPostUrl){
            this.losConfiguration.setMbPostUrl(mbPostUrl);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.losConfiguration.setBaseUrl(baseUrl);
            return this;
        }


    }


    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LosConfiguration [gngPostUrl=");
        builder.append(gngPostUrl);
        builder.append(", mbPostUrl=");
        builder.append(mbPostUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append("]");
        return builder.toString();
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + ((gngPostUrl == null) ? 0 : gngPostUrl.hashCode());
        result = prime * result + ((mbPostUrl == null) ? 0 : mbPostUrl.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LosConfiguration other = (LosConfiguration) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (gngPostUrl == null) {
            if (other.gngPostUrl != null)
                return false;
        } else if (!gngPostUrl.equals(other.gngPostUrl))
            return false;
        if (mbPostUrl == null) {
            if (other.mbPostUrl != null)
                return false;
        } else if (!mbPostUrl.equals(other.mbPostUrl))
            return false;
        return true;
    }


}
