package com.softcell.config;

import com.softcell.gonogo.cache.Cache;

public class ScoringConfiguration {
    /**
     * @param institutionId
     * @return pan communication domain
     */
    public static ScoringCommunication getCreditial(String institutionId) {
        ScoringCommunication comunicationDomain = Cache.URL_CONFIGURATION.getScoringCommunicationConfig().get(institutionId);
        return comunicationDomain;
    }

}
