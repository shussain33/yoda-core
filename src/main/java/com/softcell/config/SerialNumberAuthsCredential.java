package com.softcell.config;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "serialNumberAuthsCredential")
public class SerialNumberAuthsCredential extends AuditEntity {

    private String institutionId;
    private String userName;
    private String institutionName;
    private String password;
    private Date updateDate;
    private String vendor;
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SerialNumberAuthsCredential [institutionId=");
        builder.append(institutionId);
        builder.append(", userName=");
        builder.append(userName);
        builder.append(", institutionName=");
        builder.append(institutionName);
        builder.append(", password=");
        builder.append(password);
        builder.append(", updateDate=");
        builder.append(updateDate);
        builder.append(", vendor=");
        builder.append(vendor);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }


}
