/**
 *
 */
package com.softcell.config.templates;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import com.softcell.config.watermark.WaterMarkDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "templateConfiguration")
public class TemplateConfiguration {

    @JsonProperty("sInstitutionId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InstitutionProductGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String institutionId;

    @JsonProperty("sInstitutionName")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class
            }
    )
    private String institutionName;

    @JsonProperty("sProductId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InstitutionProductGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String productId;

    @JsonProperty("sTemplateId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class
            }
    )
    private String templateId;

    @JsonProperty("eTemplateName")
    @NotNull(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private TemplateName templateName;

    @JsonProperty("sLogoId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String logoId;

    @JsonProperty("sTemplatePath")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class
            }
    )
    private String templatePath;

    @JsonProperty("aWaterMarkDetails")
    private List<WaterMarkDetails> waterMarkDetails;

    @JsonProperty("sEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate;

    @JsonProperty("sLastUpdateDate")
    private Date lastUpdateDate;

    @JsonProperty("sUser")
    private User user;

    @JsonProperty("sTemplateDisplayName")
    private String templateDisplayName;

    @JsonProperty("sApiType")
    public String apiType = "Velocity";

    @JsonProperty("sProName")
    public String productName;

    @JsonProperty("sTempType")
    public String type = "Document";

    public interface InsertGrp {
    }

    public interface UpdateTemplatePathGrp {
    }

    public interface UpdateTemplateLogoGrp {
    }

    public interface FetchGrp {
    }

    public interface InstitutionProductGrp {
    }
}
