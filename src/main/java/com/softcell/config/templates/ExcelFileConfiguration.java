package com.softcell.config.templates;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import com.softcell.config.watermark.WaterMarkDetails;
import com.softcell.gonogo.model.core.ExcelCalcField;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ssg208 on 18/12/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "ExcelSheets")
public class ExcelFileConfiguration {

    @JsonProperty("sInstitutionId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InstitutionProductGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String institutionId;

    @JsonProperty("sInstitutionName")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class
            }
    )
    private String institutionName;

    @JsonProperty("sProductId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InstitutionProductGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String productId;

    @JsonProperty("sExcelId")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class
            }
    )
    private String excelId;

    @JsonProperty("sExcelName")
    @NotNull(
            groups = {
                    TemplateConfiguration.FetchGrp.class,
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplatePathGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String excelName;

    @JsonProperty("sFileData")
    @NotEmpty(
            groups = {
                    TemplateConfiguration.InsertGrp.class,
                    TemplateConfiguration.UpdateTemplateLogoGrp.class
            }
    )
    private String fileData;

    @JsonProperty("aInput")
    private List<ExcelCalcField> input;

    @JsonProperty("aOutput")
    private List<ExcelCalcField> output;

    @JsonProperty("sEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate;

    @JsonProperty("sLastUpdateDate")
    private Date lastUpdateDate;

    @JsonProperty("sUser")
    private User user;

    public interface InsertGrp {
    }

    public interface UpdateTemplatePathGrp {
    }

    public interface UpdateTemplateLogoGrp {
    }

    public interface FetchGrp {
    }

    public interface InstitutionProductGrp {
    }
}
