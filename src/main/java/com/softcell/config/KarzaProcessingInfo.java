package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by yogesh on 6/10/18.
 */

@Document(collection = "karzaProcessingInfo")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KarzaProcessingInfo {

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("productName")
    private String productName;

    @JsonProperty("applicantId")
    private String applicantId;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("request")
    private String request;

    @JsonProperty("response")
    private String response;

    @JsonProperty("author")
    private String author;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("status")
    private String status;

    @JsonProperty("errorMessage")
    private String errorMessage;

    @JsonProperty("errors")
    private String errors;

    @JsonProperty("kycName")
    private String kycName;

    @JsonProperty("kycNumber")
    private String kycNumber;

    @JsonProperty("insertDate")
    private String insertDate;

    @JsonProperty("lastUpdateDate")
    private String lastUpdateDate;

    @JsonProperty("oResponse")
    private Object responseObj;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;
}