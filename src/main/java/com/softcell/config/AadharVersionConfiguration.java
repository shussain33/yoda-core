package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.aadhar.KycRequestType;
import com.softcell.constants.aadhar.KycRequestVersion;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 30/11/17.
 */
@Document(collection = "aadharVersionConfiguration")
@Data
public class AadharVersionConfiguration {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sKycReqType")
    private KycRequestType kycReqType;

    @JsonProperty("sKycRequestVer")
    private KycRequestVersion kycReqVersion;

    @JsonProperty("sEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("sLastUpdateDate")
    private Date lastUpdateDate;

}
