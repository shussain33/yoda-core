package com.softcell.config;

import java.util.Map;

public class SerialNumberURLConfiguration {

    private Map<String, SerialNumberAuthsCredential> companySerialNumberAuthMap;

    public Map<String, SerialNumberAuthsCredential> getCompanySerialNumberAuthMap() {
        return companySerialNumberAuthMap;
    }

    public void setCompanySerialNumberAuthMap(
            Map<String, SerialNumberAuthsCredential> companySerialNumberAuthMap) {
        this.companySerialNumberAuthMap = companySerialNumberAuthMap;
    }

}
