package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

/**
 * @author yogeshb
 */
@Document(collection = "MultiProductConfiguration")
public class MultiProductConfiguration extends AuditEntity {

    @JsonProperty("sConfigId")
    @Id
    private String configId;

    @NotEmpty
    @JsonProperty("sInstID")
    private String institutionID;

    @CreatedDate
    @JsonProperty("dtDateTime")
    private Date dateTime;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sDsa")
    private String dsa;

    @JsonProperty("sDealer")
    private String dealer;

    @JsonProperty("iNumberOfProductsAllowed")
    private int numberOfproductsAllowed;

    @JsonProperty("bActive")
    private boolean active;

    public static Builder getBuilder(String configId) {
        return new Builder(configId);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDsa() {
        return dsa;
    }

    public void setDsa(String dsa) {
        this.dsa = dsa;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public int getNumberOfproductsAllowed() {
        return numberOfproductsAllowed;
    }

    public void setNumberOfproductsAllowed(int numberOfproductsAllowed) {
        this.numberOfproductsAllowed = numberOfproductsAllowed;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MultiProductConfiguration)) return false;
        MultiProductConfiguration that = (MultiProductConfiguration) o;
        return Objects.equals(numberOfproductsAllowed, that.numberOfproductsAllowed) &&
                Objects.equals(active, that.active) &&
                Objects.equals(configId, that.configId) &&
                Objects.equals(institutionID, that.institutionID) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(product, that.product) &&
                Objects.equals(dsa, that.dsa) &&
                Objects.equals(dealer, that.dealer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configId, institutionID, dateTime, product, dsa, dealer, numberOfproductsAllowed, active);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("configId", configId)
                .append("institutionID", institutionID)
                .append("dateTime", dateTime)
                .append("product", product)
                .append("dsa", dsa)
                .append("dealer", dealer)
                .append("numberOfproductsAllowed", numberOfproductsAllowed)
                .append("active", active)
                .toString();
    }

    public static class Builder {

        private MultiProductConfiguration multiProductConfiguration = new MultiProductConfiguration();

        public Builder(String configId) {
            this.multiProductConfiguration.configId = configId;
        }

        public MultiProductConfiguration build() {
            return this.multiProductConfiguration;
        }

        public Builder institutionID(String institutionID) {
            this.multiProductConfiguration.institutionID = institutionID;
            return this;
        }

        public Builder dateTime(Date dateTime) {
            this.multiProductConfiguration.dateTime = dateTime;
            return this;
        }

        public Builder product(String product) {
            this.multiProductConfiguration.product = product;
            return this;
        }

        public Builder dsa(String dsa) {
            this.multiProductConfiguration.dsa = dsa;
            return this;
        }

        public Builder dealer(String dealer) {
            this.multiProductConfiguration.dealer = dealer;
            return this;
        }

        public Builder numberOfproductsAllowed(int numberOfproductsAllowed) {
            this.multiProductConfiguration.numberOfproductsAllowed = numberOfproductsAllowed;
            return this;
        }

        public Builder active(boolean active) {
            this.multiProductConfiguration.active = active;
            return this;
        }
    }
}
