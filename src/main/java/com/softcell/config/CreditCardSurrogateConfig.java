package com.softcell.config;

public class CreditCardSurrogateConfig {

    private String url;

    private String baseUrl;

    private String action;

    private String target;

    private boolean dedupe;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public boolean isDedupe() {
        return dedupe;
    }

    public void setDedupe(boolean dedupe) {
        this.dedupe = dedupe;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private CreditCardSurrogateConfig creditCardSurrogateConfig = new CreditCardSurrogateConfig();

        public CreditCardSurrogateConfig build(){
            return this.creditCardSurrogateConfig;
        }

        public Builder url(String url){
            this.creditCardSurrogateConfig.setUrl(url);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.creditCardSurrogateConfig.setBaseUrl(baseUrl);
            return this;
        }

        public Builder action(String action) {
            this.creditCardSurrogateConfig.setAction(action);
            return this;
        }

        public Builder target(String target){
            this.creditCardSurrogateConfig.setTarget(target);
            return this;
        }

        public Builder dedupe(Boolean dedupe){
            this.creditCardSurrogateConfig.setDedupe(dedupe);
            return this;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreditCardSurrogateConfig [url=");
        builder.append(url);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", action=");
        builder.append(action);
        builder.append(", target=");
        builder.append(target);
        builder.append(", dedupe=");
        builder.append(dedupe);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + (dedupe ? 1231 : 1237);
        result = prime * result + ((target == null) ? 0 : target.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CreditCardSurrogateConfig))
            return false;
        CreditCardSurrogateConfig other = (CreditCardSurrogateConfig) obj;
        if (action == null) {
            if (other.action != null)
                return false;
        } else if (!action.equals(other.action))
            return false;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (dedupe != other.dedupe)
            return false;
        if (target == null) {
            if (other.target != null)
                return false;
        } else if (!target.equals(other.target))
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }
}
