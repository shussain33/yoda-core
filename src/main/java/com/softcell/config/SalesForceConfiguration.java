package com.softcell.config;

import java.util.Map;

/**
 * @author prateek
 */
public class SalesForceConfiguration {

    private String userName;
    private String userPassword;
    private String keyStorePass;
    private String cin;
    private String leadSource;
    private Map<String,String> recordTypes;
    private Map<String,String> businessTypes;
    private ProxyConfiguration proxyConfiguration;

    public static class Builder{

        private SalesForceConfiguration salesForceConfiguration  = new SalesForceConfiguration();

        public SalesForceConfiguration build(){ return  this.salesForceConfiguration; }

        public Builder userName(String userName){
            this.salesForceConfiguration.userName = userName;
            return this;
        }

        public Builder userPassword(String userPassword){
            this.salesForceConfiguration.userPassword = userPassword;
            return this;
        }

        public Builder keyStorePass(String keyStorePass){
            this.salesForceConfiguration.keyStorePass = keyStorePass;
            return this;
        }

        public Builder cin(String cin){
            this.salesForceConfiguration.cin = cin;
            return this;
        }

        public Builder leadSource(String leadSource){
            this.salesForceConfiguration.leadSource = leadSource;
            return  this;
        }

        public Builder recordTypes(Map<String,String> recordTypes){
            this.salesForceConfiguration.recordTypes = recordTypes;
            return  this;
        }

        public Builder businessTypes(Map<String,String> businessTypes){
            this.salesForceConfiguration.businessTypes = businessTypes;
            return  this;
        }
    }

    public static Builder builder(){
        return new Builder();
    }




    /**
     * @return the keyStorePass
     */
    public String getKeyStorePass() {
        return keyStorePass;
    }

    /**
     * @param keyStorePass the keyStorePass to set
     */
    public void setKeyStorePass(String keyStorePass) {
        this.keyStorePass = keyStorePass;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword
     *            the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public Map<String, String> getRecordTypes() {
        return recordTypes;
    }

    public void setRecordTypes(Map<String, String> recordTypes) {
        this.recordTypes = recordTypes;
    }

    public Map<String, String> getBusinessTypes() {
        return businessTypes;
    }

    public void setBusinessTypes(Map<String, String> businessTypes) {
        this.businessTypes = businessTypes;
    }

    public ProxyConfiguration getProxyConfiguration() {
        return proxyConfiguration;
    }

    public void setProxyConfiguration(ProxyConfiguration proxyConfiguration) {
        this.proxyConfiguration = proxyConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SalesForceConfiguration)) return false;

        SalesForceConfiguration that = (SalesForceConfiguration) o;

        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (userPassword != null ? !userPassword.equals(that.userPassword) : that.userPassword != null) return false;
        if (keyStorePass != null ? !keyStorePass.equals(that.keyStorePass) : that.keyStorePass != null) return false;
        if (cin != null ? !cin.equals(that.cin) : that.cin != null) return false;
        if (leadSource != null ? !leadSource.equals(that.leadSource) : that.leadSource != null) return false;
        if (recordTypes != null ? !recordTypes.equals(that.recordTypes) : that.recordTypes != null) return false;
        return !(businessTypes != null ? !businessTypes.equals(that.businessTypes) : that.businessTypes != null);

    }

    @Override
    public int hashCode() {
        int result = userName != null ? userName.hashCode() : 0;
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (keyStorePass != null ? keyStorePass.hashCode() : 0);
        result = 31 * result + (cin != null ? cin.hashCode() : 0);
        result = 31 * result + (leadSource != null ? leadSource.hashCode() : 0);
        result = 31 * result + (recordTypes != null ? recordTypes.hashCode() : 0);
        result = 31 * result + (businessTypes != null ? businessTypes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SalesForceConfiguration{");
        sb.append("userName='").append(userName).append('\'');
        sb.append(", userPassword='").append(userPassword).append('\'');
        sb.append(", keyStorePass='").append(keyStorePass).append('\'');
        sb.append(", cin='").append(cin).append('\'');
        sb.append(", leadSource='").append(leadSource).append('\'');
        sb.append(", recordTypes=").append(recordTypes);
        sb.append(", businessTypes=").append(businessTypes);
        sb.append('}');
        return sb.toString();
    }
}
