package com.softcell.config;

import java.util.Arrays;

/**
 * @author kishor
 *         Use to  connect Elasticsearch app
 *         for search functionality and indexing.
 */
public class ElasticSearchConfig {

    private String[] nodeIpAddresses;

    private String ipAddress;

    private int httpPort;

    private int tcpPort;

    private String clusterName;

    private String userName;

    private String password;

    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String[] getNodeIpAddresses() {
        return nodeIpAddresses;
    }

    public void setNodeIpAddresses(String[] nodeIpAddresses) {
        this.nodeIpAddresses = nodeIpAddresses;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(int httpPort) {
        this.httpPort = httpPort;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ElasticSearchConfig [nodeIpAddresses=");
        builder.append(Arrays.toString(nodeIpAddresses));
        builder.append(", ipAddress=");
        builder.append(ipAddress);
        builder.append(", httpPort=");
        builder.append(httpPort);
        builder.append(", tcpPort=");
        builder.append(tcpPort);
        builder.append(", clusterName=");
        builder.append(clusterName);
        builder.append(", userName=");
        builder.append(userName);
        builder.append(", password=");
        builder.append(password);
        builder.append("]");
        return builder.toString();
    }


}
