package com.softcell.config;

/**
 * Connection app for NTC
 *
 * @author bhuvneshk
 */
public class NTCConfiguration {

    private String ntcUrl;

    private String baseUrl;

    private String institutionId;

    private String aggregatorId;

    private String password;

    private String memberId;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @return the ntcUrl
     */
    public String getNtcUrl() {
        return ntcUrl;
    }

    /**
     * @param ntcUrl the ntcUrl to set
     */
    public void setNtcUrl(String ntcUrl) {
        this.ntcUrl = ntcUrl;
    }


    /**
     * @return the aggregatorId
     */
    public String getAggregatorId() {
        return aggregatorId;
    }

    /**
     * @param aggregatorId the aggregatorId to set
     */
    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * @param memberId the memberId to set
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }


    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private NTCConfiguration ntcConfiguration  =  new NTCConfiguration();

        public NTCConfiguration build(){
            return this.ntcConfiguration;
        }

        public Builder ntcUrl(String ntcUrl){
            this.ntcConfiguration.setNtcUrl(ntcUrl);
            return this;
        }


        public Builder baseUrl(String baseUrl){
            this.ntcConfiguration.setBaseUrl(baseUrl);
            return this;
        }


        public Builder institutionId(String institutionId){
            this.ntcConfiguration.setInstitutionId(institutionId);
            return this;
        }


        public Builder aggregatorId(String aggregatorId){
            this.ntcConfiguration.setAggregatorId(aggregatorId);
            return this;
        }

        public Builder password(String password){
            this.ntcConfiguration.setPassword(password);
            return this;
        }


        public Builder memberId(String memberId){
            this.ntcConfiguration.setMemberId(memberId);
            return this;
        }


    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NTCConfiguration [ntcUrl=");
        builder.append(ntcUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", aggregatorId=");
        builder.append(aggregatorId);
        builder.append(", password=");
        builder.append(password);
        builder.append(", memberId=");
        builder.append(memberId);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((aggregatorId == null) ? 0 : aggregatorId.hashCode());
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((memberId == null) ? 0 : memberId.hashCode());
        result = prime * result + ((ntcUrl == null) ? 0 : ntcUrl.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof NTCConfiguration))
            return false;
        NTCConfiguration other = (NTCConfiguration) obj;
        if (aggregatorId == null) {
            if (other.aggregatorId != null)
                return false;
        } else if (!aggregatorId.equals(other.aggregatorId))
            return false;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (memberId == null) {
            if (other.memberId != null)
                return false;
        } else if (!memberId.equals(other.memberId))
            return false;
        if (ntcUrl == null) {
            if (other.ntcUrl != null)
                return false;
        } else if (!ntcUrl.equals(other.ntcUrl))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        return true;
    }


}
