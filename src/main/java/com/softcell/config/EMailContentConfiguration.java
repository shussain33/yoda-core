package com.softcell.config;

import java.util.Date;

public class EMailContentConfiguration {
    private String disclaimer;
    private String subject;
    private String institutionId;
    private String product;
    private Date insertDate;
    private boolean active;


    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "EMailContentConfiguration [disclaimer=" + disclaimer + ", subject="
                + subject + ", institutionId=" + institutionId + ", product="
                + product + ", insertDate=" + insertDate + ", active=" + active
                + "]";
    }


}
