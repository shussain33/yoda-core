package com.softcell.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author kishorp
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "serviceCommunication")
public class ServiceCommunication {

    private String serviceId;

    private String institutionId;

    private String aggregatorId;

    private String memberId;

    private String password;

    private String serviceUrl;

    private String type;

    private  String baseUrl;

    private String sourceSystem;

    private String numberOfReTry;


}
