package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class Credentials {
    @Id
    @JsonProperty("SERVICE_ID")
    private String serviceID;

    @JsonProperty("SERVICE_NAME")
    private String serviceName;

    @JsonProperty("INSTITUTION_ID")
    private String institutionId;

    @JsonProperty("AGGREGATOR_ID")
    private String aggregatorId;

    @JsonProperty("MEMBER_ID")
    private String memberId;

    @JsonProperty("PASSWORD")
    private String password;

    @JsonProperty("MULTIBUREAU_URL")
    private String serviceUrl;

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getAggregatorId() {
        return aggregatorId;
    }

    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    @Override
    public String toString() {
        return "Credentials [serviceID=" + serviceID + ", serviceName="
                + serviceName + ", institutionId=" + institutionId
                + ", aggregatorId=" + aggregatorId + ", memberId=" + memberId
                + ", password=" + password + ", serviceUrl=" + serviceUrl + "]";
    }


}
