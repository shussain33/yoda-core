package com.softcell.config;

import com.softcell.dao.mongodb.repository.UrlConfigurationRepository;
import com.softcell.dao.mongodb.repository.UrlConfigurationRepositoryImpl;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.multibureau.pickup.MbCommunicationDomain;

public class MultiBureauConfig {

    /*-----
    used collection serviceCommunication for communicationParameter
    -----*/

    public static MbCommunicationDomain getMbCommunicationDomain(String intistutionId) {
        UrlConfigurationRepository urlConfigurationRepository = new UrlConfigurationRepositoryImpl();
        MbCommunicationDomain mbCommunicationDomain = new MbCommunicationDomain();
        ServiceCommunication serviceCommunication = urlConfigurationRepository.getServiceCommunication(intistutionId,ConfigType.MBCONSUMER );
        if(serviceCommunication != null){
            ConfigType.convertDataToMbCommunication(serviceCommunication,mbCommunicationDomain);
        }else{
            mbCommunicationDomain = Cache.URL_CONFIGURATION.getMbCommunicationDomainConfig().get(intistutionId);
        }
        return mbCommunicationDomain;
    }

    public static MbCommunicationDomain getMbCorpCommunicationDomain(String intistutionId) {
        UrlConfigurationRepository urlConfigurationRepository = new UrlConfigurationRepositoryImpl();
        MbCommunicationDomain mbCommunicationDomain = new MbCommunicationDomain();

        ServiceCommunication serviceCommunication = urlConfigurationRepository.getServiceCommunication(intistutionId,ConfigType.MBCORP );

        if(serviceCommunication != null){
            ConfigType.convertDataToMbCommunication(serviceCommunication,mbCommunicationDomain);
        }else{
             mbCommunicationDomain = Cache.URL_CONFIGURATION.getMbCorpCommunicationDomainConfig().get(intistutionId);
          }
        return mbCommunicationDomain;
    }
}
