package com.softcell.config.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * this class represent a gonogo sms service app domain based on institutioIds
 *
 * @author kishorp
 */
@Document(collection = "gngSmsServiceConfiguration")
public class GngSmsServiceConfiguration extends AuditEntity {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sProductName")
    private Product productName;

    @JsonProperty("sVendorName")
    private String vendorName;

    @JsonProperty("sContent")
    private String content;

    @JsonProperty("bEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("sLastUpdateDate")
    private Date lastUpdateDate;

    @JsonProperty("oUser")
    private User user;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result
                + ((createDate == null) ? 0 : createDate.hashCode());
        result = prime * result + (enable ? 1231 : 1237);
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((institutionName == null) ? 0 : institutionName.hashCode());
        result = prime * result
                + ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
        result = prime * result
                + ((productId == null) ? 0 : productId.hashCode());
        result = prime * result
                + ((productName == null) ? 0 : productName.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        result = prime * result
                + ((vendorName == null) ? 0 : vendorName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GngSmsServiceConfiguration other = (GngSmsServiceConfiguration) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (createDate == null) {
            if (other.createDate != null)
                return false;
        } else if (!createDate.equals(other.createDate))
            return false;
        if (enable != other.enable)
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (institutionName == null) {
            if (other.institutionName != null)
                return false;
        } else if (!institutionName.equals(other.institutionName))
            return false;
        if (lastUpdateDate == null) {
            if (other.lastUpdateDate != null)
                return false;
        } else if (!lastUpdateDate.equals(other.lastUpdateDate))
            return false;
        if (productId == null) {
            if (other.productId != null)
                return false;
        } else if (!productId.equals(other.productId))
            return false;
        if (productName != other.productName)
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        if (vendorName == null) {
            if (other.vendorName != null)
                return false;
        } else if (!vendorName.equals(other.vendorName))
            return false;
        return true;
    }


}
