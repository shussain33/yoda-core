package com.softcell.config.casehistory;

import com.google.common.base.Objects;

/**
 * @author kishorp
 */
public class StepProperties {

    private String stageId;

    private String displayName;

    private String systemStageName;

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getSystemStageName() {
        return systemStageName;
    }

    public void setSystemStageName(String systemStageName) {
        this.systemStageName = systemStageName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StepProperties{");
        sb.append("stageId='").append(stageId).append('\'');
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append(", systemStageName='").append(systemStageName).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StepProperties)) return false;
        StepProperties that = (StepProperties) o;
        return Objects.equal(getStageId(), that.getStageId()) &&
                Objects.equal(getDisplayName(), that.getDisplayName()) &&
                Objects.equal(getSystemStageName(), that.getSystemStageName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getStageId(), getDisplayName(), getSystemStageName());
    }
}
