package com.softcell.config.casehistory;

import com.google.common.base.Objects;

import java.util.Map;

public class StageProperties {

    private String stageId;

    private String displayName;

    private String systemStageName;

    private Map<String, StepProperties> stepProperties;

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSystemStageName() {
        return systemStageName;
    }

    public void setSystemStageName(String systemStageName) {
        this.systemStageName = systemStageName;
    }

    public Map<String, StepProperties> getStepProperties() {
        return stepProperties;
    }

    public void setStepProperties(Map<String, StepProperties> stepProperties) {
        this.stepProperties = stepProperties;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StageProperties{");
        sb.append("stageId='").append(stageId).append('\'');
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append(", systemStageName='").append(systemStageName).append('\'');
        sb.append(", stepProperties=").append(stepProperties);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StageProperties)) return false;
        StageProperties that = (StageProperties) o;
        return Objects.equal(getStageId(), that.getStageId()) &&
                Objects.equal(getDisplayName(), that.getDisplayName()) &&
                Objects.equal(getSystemStageName(), that.getSystemStageName()) &&
                Objects.equal(getStepProperties(), that.getStepProperties());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getStageId(), getDisplayName(), getSystemStageName(), getStepProperties());
    }
}
