package com.softcell.config.casehistory;

import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

/**
 * @author kishorp
 */
@Document(collection = "applicationStageConfiguration")
public class StageConfiguration extends AuditEntity {

    private String institutionId;

    private String productName;

    private Map<String, Map<String, StageProperties>> stageProperties;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Map<String, Map<String, StageProperties>> getStageProperties() {
        return stageProperties;
    }

    public void setStageProperties(
            Map<String, Map<String, StageProperties>> stageProperties) {
        this.stageProperties = stageProperties;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StageConfiguration{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", productName='").append(productName).append('\'');
        sb.append(", stageProperties=").append(stageProperties);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StageConfiguration)) return false;
        StageConfiguration that = (StageConfiguration) o;
        return Objects.equal(getInstitutionId(), that.getInstitutionId()) &&
                Objects.equal(getProductName(), that.getProductName()) &&
                Objects.equal(getStageProperties(), that.getStageProperties());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getInstitutionId(), getProductName(), getStageProperties());
    }
}
