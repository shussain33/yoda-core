package com.softcell.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author shyamk
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmandateCredentials {

    private String initiatingPartyId;
    private String cdtrAccId;
    private String cdtrAgntMemBankName;
    private String cdtrAgntMemId;
    private String cdtrName;
    private String instructingAgentMmbId;
    private String instructingBankName;
    private String mdthFreqTp;
    private String mndtSeqTp;
    private String mndtLclInstrmPrtry;
    private String mndtSvcLvlPrtry;
    private List<String> allowedBanks;

}
