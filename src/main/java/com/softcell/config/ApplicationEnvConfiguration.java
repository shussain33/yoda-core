/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 7, 2016 5:06:05 PM
 */
package com.softcell.config;

/**
 * @author kishorp
 *
 */
public class ApplicationEnvConfiguration {

    public static String SCORING_SERVICE_URL = "http://gng.softcell.in/AppScoringV2Git/api/ScoringV3/";//"http://172.26.1.149:8080/AppScoring/api/ScoringV3/";
     public static String PAN_SERVICE_URL = "";
    public static String AADHAR_SERVICE_URL = "";
    public static String MB_SERVICE_URL = "";
    public static String QUEUE_MANAGER_SERVICE_URL = "";
    public static boolean IS_APPLICATION_IN_PASSIVE_MODE;

}
