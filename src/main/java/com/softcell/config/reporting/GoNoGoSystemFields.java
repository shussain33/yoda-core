package com.softcell.config.reporting;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.reporting.domains.ColumnConfiguration;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

/**
 * @author prateek exposed api for clients
 *         <p/>
 *         { fieldsDetails : [ { sCloumnKey:"", sColumnDisplayName: "",
 *         sColumnIndex:0, bViewable:false, bDownloadable:false } ],
 *         reportingConfigurations : [ {oHeader : { sAppID:"", sInstID:"",
 *         sSourceID:"", sAppSource:"", sReqType:"", dtSubmit:"", sDsaId:"",
 *         sCroId:"", sDealerId:"", }, sReportId : '', aProductType: [],
 *         aFlatConfig : { mHeaderMap : [ 0:{ columnKey:"",
 *         columnDisplayName:"", columnIndex:"", isViewable:true,
 *         isDownloadable:true }, 1:{ columnKey:"", columnDisplayName:"",
 *         columnIndex:"", isViewable:true, isDownloadable:true } ],
 *         reportName:"", reportType :"", reportFormat : "", header : "",
 *         seperater : "" }, sBranchId : "", sUserId : "", oPaggination : {
 *         iPageId :0, iLimit: 0, iSkip : 0 },
 *         <p/>
 *         <p/>
 *         }], version : "", institutionId:"" }
 */
@Document(collection = "goNoGoSystemFields")
public class GoNoGoSystemFields extends AuditEntity {

    @JsonProperty("oColumns")
    private Set<ColumnConfiguration> fieldsDetails;
    @JsonProperty("oReports")
    private List<ReportingConfigurations> reportingConfigurations;
    @JsonIgnore
    private String institutionId;

    public List<ReportingConfigurations> getReportingConfigurations() {
        return reportingConfigurations;
    }

    public void setReportingConfigurations(
            List<ReportingConfigurations> reportingConfigurations) {
        this.reportingConfigurations = reportingConfigurations;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Set<ColumnConfiguration> getFieldsDetails() {
        return fieldsDetails;
    }

    public void setFieldsDetails(Set<ColumnConfiguration> fieldsDetails) {
        this.fieldsDetails = fieldsDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GoNoGoSystemFields{");
        sb.append("fieldsDetails=").append(fieldsDetails);
        sb.append(", reportingConfigurations=").append(reportingConfigurations);
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GoNoGoSystemFields that = (GoNoGoSystemFields) o;

        if (fieldsDetails != null ? !fieldsDetails.equals(that.fieldsDetails) : that.fieldsDetails != null)
            return false;
        if (reportingConfigurations != null ? !reportingConfigurations.equals(that.reportingConfigurations) : that.reportingConfigurations != null)
            return false;
        return !(institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (fieldsDetails != null ? fieldsDetails.hashCode() : 0);
        result = 31 * result + (reportingConfigurations != null ? reportingConfigurations.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        return result;
    }
}
