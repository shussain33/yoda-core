package com.softcell.config.reporting;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Pagination;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author kishor
 *         <p/>
 *         exposed json to client
 *         <p/>
 *         { oHeader : { sAppID:"", sInstID:"", sSourceID:"", sAppSource:"",
 *         sReqType:"", dtSubmit:"", sDsaId:"", sCroId:"", sDealerId:"", },
 *         sReportId : '', aProductType: [], aFlatConfig : { mHeaderMap : [ 0:{
 *         columnKey:"", columnDisplayName:"", columnIndex:"", isViewable:true,
 *         isDownloadable:true }, 1:{ columnKey:"", columnDisplayName:"",
 *         columnIndex:"", isViewable:true, isDownloadable:true } ],
 *         reportName:"", reportType :"", reportFormat : "", header : "",
 *         seperater : "" }, sBranchId : "", sUserId : "", oPaggination : {
 *         iPageId :0, iLimit: 0, iSkip : 0 },
 *         <p/>
 *         <p/>
 *         }
 */
@Document(collection = "reportingConfigurations")
public class ReportingConfigurations extends AuditEntity {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {Header.FetchGrp.class}
    )
    @Valid
    private Header header;

    @JsonProperty("sReportId")
    @NotEmpty(groups = {
            ReportingConfigurations.CustomReportGrp.class
    }
    )
    private String reportId;

    @JsonProperty("sReportName")
    private String reportName;

    @JsonProperty("sReportCategory")
    private String reportCategory;

    @JsonProperty("aProductType")
    private Set<String> products;

    @JsonProperty("aFlatConfig")
    private FlatReportConfiguration flatReportConfiguration;

    @JsonProperty("oUnSelectedColumns")
    private Set<ColumnConfiguration> unSelectedColumn;

    @JsonProperty("sBranchId")
    private String branchId;

    @JsonProperty("sUserId")
    @NotEmpty(groups = {
            ReportingConfigurations.CustomReportGrp.class
    }
    )
    private String userId;

    @JsonProperty("oPaggination")
    private Pagination pagination;
    @JsonIgnore
    private boolean inActive;


    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    /**
     * @return the reportId
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * @param reportId the reportId to set
     */
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * @return the flatReportConfiguration
     */
    public FlatReportConfiguration getFlatReportConfiguration() {
        return flatReportConfiguration;
    }

    /**
     * @param flatReportConfiguration the flatReportConfiguration to set
     */
    public void setFlatReportConfiguration(
            FlatReportConfiguration flatReportConfiguration) {
        this.flatReportConfiguration = flatReportConfiguration;
    }

    /**
     * @return the products
     */
    public Set<String> getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(Set<String> products) {
        this.products = products;
    }

    /**
     * @return the branchId
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * @param branchId the branchId to set
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * @param pagination the pagination to set
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportCategory() {
        return reportCategory;
    }

    public void setReportCategory(String reportCategory) {
        this.reportCategory = reportCategory;
    }


    public Set<ColumnConfiguration> getUnSelectedColumn() {
        return unSelectedColumn;
    }

    public void setUnSelectedColumn(Set<ColumnConfiguration> unSelectedColumn) {
        this.unSelectedColumn = unSelectedColumn;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((branchId == null) ? 0 : branchId.hashCode());
        result = prime
                * result
                + ((flatReportConfiguration == null) ? 0
                : flatReportConfiguration.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((pagination == null) ? 0 : pagination.hashCode());
        result = prime * result
                + ((products == null) ? 0 : products.hashCode());
        result = prime * result
                + ((reportCategory == null) ? 0 : reportCategory.hashCode());
        result = prime * result
                + ((reportId == null) ? 0 : reportId.hashCode());
        result = prime * result
                + ((reportName == null) ? 0 : reportName.hashCode());
        result = prime
                * result
                + ((unSelectedColumn == null) ? 0 : unSelectedColumn.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReportingConfigurations other = (ReportingConfigurations) obj;
        if (branchId == null) {
            if (other.branchId != null)
                return false;
        } else if (!branchId.equals(other.branchId))
            return false;
        if (flatReportConfiguration == null) {
            if (other.flatReportConfiguration != null)
                return false;
        } else if (!flatReportConfiguration
                .equals(other.flatReportConfiguration))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (pagination == null) {
            if (other.pagination != null)
                return false;
        } else if (!pagination.equals(other.pagination))
            return false;
        if (products == null) {
            if (other.products != null)
                return false;
        } else if (!products.equals(other.products))
            return false;
        if (reportCategory == null) {
            if (other.reportCategory != null)
                return false;
        } else if (!reportCategory.equals(other.reportCategory))
            return false;
        if (reportId == null) {
            if (other.reportId != null)
                return false;
        } else if (!reportId.equals(other.reportId))
            return false;
        if (reportName == null) {
            if (other.reportName != null)
                return false;
        } else if (!reportName.equals(other.reportName))
            return false;
        if (unSelectedColumn == null) {
            if (other.unSelectedColumn != null)
                return false;
        } else if (!unSelectedColumn.equals(other.unSelectedColumn))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

    public interface CustomReportGrp {
    }

}
