package com.softcell.config.reporting;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

public class FlatConfig {
    public static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();

    static {
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(0);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(0, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dsaId");
        columnConfiguration.setColumnIndex(1);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(1, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dealerId");
        columnConfiguration.setColumnIndex(2);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(2, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deDate");
        columnConfiguration.setColumnIndex(3);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(3, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSubmitDate");
        columnConfiguration.setColumnIndex(4);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(4, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croId");
        columnConfiguration.setColumnIndex(5);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(5, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentStage");
        columnConfiguration.setColumnIndex(6);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(6, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("firstName");
        columnConfiguration.setColumnIndex(7);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(7, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("lastName");
        columnConfiguration.setColumnIndex(8);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(8, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("middleName");
        columnConfiguration.setColumnIndex(9);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(9, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("maritalStatus");
        columnConfiguration.setColumnIndex(10);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(10, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("gender");
        columnConfiguration.setColumnIndex(11);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(11, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(12);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(12, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("age");
        columnConfiguration.setColumnIndex(13);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(13, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eduction");
        columnConfiguration.setColumnIndex(14);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(14, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(15);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(15, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appStatus");
        columnConfiguration.setColumnIndex(16);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(16, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfDependent");
        columnConfiguration.setColumnIndex(17);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(17, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfEarningMemb");
        columnConfiguration.setColumnIndex(18);
        columnConfiguration.setColumnKey("C_A_noOfEarningMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(18, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfFamilyMemb");
        columnConfiguration.setColumnIndex(19);
        columnConfiguration.setColumnKey("C_A_noOfFamilyMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(19, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber0");
        columnConfiguration.setColumnIndex(20);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(20, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber1");
        columnConfiguration.setColumnIndex(21);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(21, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType0");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(22, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails0");
        columnConfiguration.setColumnIndex(23);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(23, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals0");
        columnConfiguration.setColumnIndex(24);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(24, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(25);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(25, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(26);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(26, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails0");
        columnConfiguration.setColumnIndex(27);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(27, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi0");
        columnConfiguration.setColumnIndex(28);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(28, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount0");
        columnConfiguration.setColumnIndex(29);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(29, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct0");
        columnConfiguration.setColumnIndex(30);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(30, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType0");
        columnConfiguration.setColumnIndex(31);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(31, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine10");
        columnConfiguration.setColumnIndex(32);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(32, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine20");
        columnConfiguration.setColumnIndex(33);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(33, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(34);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(34, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark0");
        columnConfiguration.setColumnIndex(35);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(35, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode0");
        columnConfiguration.setColumnIndex(36);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(36, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city0");
        columnConfiguration.setColumnIndex(37);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(37, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state0");
        columnConfiguration.setColumnIndex(38);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(38, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine11");
        columnConfiguration.setColumnIndex(39);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(39, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine21");
        columnConfiguration.setColumnIndex(40);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(40, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(41);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(41, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark1");
        columnConfiguration.setColumnIndex(42);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(42, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode1");
        columnConfiguration.setColumnIndex(43);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(43, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city1");
        columnConfiguration.setColumnIndex(44);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(44, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state1");
        columnConfiguration.setColumnIndex(45);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(45, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine12");
        columnConfiguration.setColumnIndex(46);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(46, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine22");
        columnConfiguration.setColumnIndex(47);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(47, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(48);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(48, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark2");
        columnConfiguration.setColumnIndex(49);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(49, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode2");
        columnConfiguration.setColumnIndex(50);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(50, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city2");
        columnConfiguration.setColumnIndex(51);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(51, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state2");
        columnConfiguration.setColumnIndex(52);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(52, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country0");
        columnConfiguration.setColumnIndex(53);
        columnConfiguration.setColumnKey("C_CustomerAddress_country0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(53, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country1");
        columnConfiguration.setColumnIndex(54);
        columnConfiguration.setColumnKey("C_CustomerAddress_country1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(54, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country2");
        columnConfiguration.setColumnIndex(55);
        columnConfiguration.setColumnKey("C_CustomerAddress_country2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(55, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType0");
        columnConfiguration.setColumnIndex(56);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(56, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt0");
        columnConfiguration.setColumnIndex(57);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(57, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode0");
        columnConfiguration.setColumnIndex(58);
        columnConfiguration.setColumnKey("C_Phone_countryCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(58, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode0");
        columnConfiguration.setColumnIndex(59);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(59, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber0");
        columnConfiguration.setColumnIndex(60);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(60, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType1");
        columnConfiguration.setColumnIndex(61);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(61, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt1");
        columnConfiguration.setColumnIndex(62);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(62, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode1");
        columnConfiguration.setColumnIndex(63);
        columnConfiguration.setColumnKey("C_Phone_countryCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(63, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode1");
        columnConfiguration.setColumnIndex(64);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(64, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber1");
        columnConfiguration.setColumnIndex(65);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(65, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType2");
        columnConfiguration.setColumnIndex(66);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(66, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt2");
        columnConfiguration.setColumnIndex(67);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(67, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode2");
        columnConfiguration.setColumnIndex(68);
        columnConfiguration.setColumnKey("C_Phone_countryCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(68, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode2");
        columnConfiguration.setColumnIndex(69);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(69, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber2");
        columnConfiguration.setColumnIndex(70);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(70, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType3");
        columnConfiguration.setColumnIndex(71);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(71, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt3");
        columnConfiguration.setColumnIndex(72);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(72, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode3");
        columnConfiguration.setColumnIndex(73);
        columnConfiguration.setColumnKey("C_Phone_countryCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(73, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode3");
        columnConfiguration.setColumnIndex(74);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(74, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber3");
        columnConfiguration.setColumnIndex(75);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(75, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress0");
        columnConfiguration.setColumnIndex(76);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(76, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType0");
        columnConfiguration.setColumnIndex(77);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(77, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress1");
        columnConfiguration.setColumnIndex(78);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(78, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType1");
        columnConfiguration.setColumnIndex(79);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(79, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress2");
        columnConfiguration.setColumnIndex(80);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(80, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType2");
        columnConfiguration.setColumnIndex(81);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(81, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress3");
        columnConfiguration.setColumnIndex(82);
        columnConfiguration.setColumnKey("C_Email_emailAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(82, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType3");
        columnConfiguration.setColumnIndex(83);
        columnConfiguration.setColumnKey("C_Email_emailType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(83, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution0");
        columnConfiguration.setColumnIndex(84);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(84, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName0");
        columnConfiguration.setColumnIndex(85);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(85, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType0");
        columnConfiguration.setColumnIndex(86);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(86, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName0");
        columnConfiguration.setColumnIndex(87);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(87, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber0");
        columnConfiguration.setColumnIndex(88);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(88, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName1");
        columnConfiguration.setColumnIndex(89);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(89, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber1");
        columnConfiguration.setColumnIndex(90);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(90, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName2");
        columnConfiguration.setColumnIndex(91);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(91, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber2");
        columnConfiguration.setColumnIndex(92);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(92, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName3");
        columnConfiguration.setColumnIndex(93);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(93, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber3");
        columnConfiguration.setColumnIndex(94);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(94, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName4");
        columnConfiguration.setColumnIndex(95);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(95, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber4");
        columnConfiguration.setColumnIndex(96);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(96, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName5");
        columnConfiguration.setColumnIndex(97);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(97, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber5");
        columnConfiguration.setColumnIndex(98);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(98, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName6");
        columnConfiguration.setColumnIndex(99);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(99, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber6");
        columnConfiguration.setColumnIndex(100);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(100, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg0");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(101, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake0");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(102, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName0");
        columnConfiguration.setColumnIndex(103);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(103, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo0");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(104, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice0");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(105, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(106, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt");
        columnConfiguration.setColumnIndex(107);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(107, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr");
        columnConfiguration.setColumnIndex(108);
        columnConfiguration.setColumnKey("C_L_A_loanApr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(108, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose");
        columnConfiguration.setColumnIndex(109);
        columnConfiguration.setColumnKey("C_L_A_loanPurpose999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(109, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanTenor");
        columnConfiguration.setColumnIndex(110);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(110, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(111, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(111, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("marginAmt");
        columnConfiguration.setColumnIndex(112);
        columnConfiguration.setColumnKey("C_L_A_marginAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(112, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("numberOfAdvEmi");
        columnConfiguration.setColumnIndex(113);
        columnConfiguration.setColumnKey("C_L_A_numberOfAdvanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(113, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType0");
        columnConfiguration.setColumnIndex(114);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(114, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(115);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(115, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(116);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(116, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(117);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(117, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(118);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(118, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(119);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(119, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(120);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(120, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(121);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(121, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(122);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(122, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(123);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(123, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(124);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(124, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croApprAmt0");
        columnConfiguration.setColumnIndex(125);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(125, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("apprDate0");
        columnConfiguration.setColumnIndex(126);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(126, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("downPayment0");
        columnConfiguration.setColumnIndex(127);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(127, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(128, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi0");
        columnConfiguration.setColumnIndex(129);
        columnConfiguration.setColumnKey("CRO_CroDecision_emi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(129, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("interestRate0");
        columnConfiguration.setColumnIndex(130);
        columnConfiguration.setColumnKey("CRO_CroDecision_interestRate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(130, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("tenor0");
        columnConfiguration.setColumnIndex(131);
        columnConfiguration.setColumnKey("CRO_CroDecision_tenor0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(131, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability4");
        columnConfiguration.setColumnIndex(132);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(132, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability5");
        columnConfiguration.setColumnIndex(133);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(133, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("panName");
        columnConfiguration.setColumnIndex(134);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(134, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("panStatus0");
        columnConfiguration.setColumnIndex(135);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(135, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appScore");
        columnConfiguration.setColumnIndex(136);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(136, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressScore");
        columnConfiguration.setColumnIndex(137);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(137, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("officeAddressScore");
        columnConfiguration.setColumnIndex(138);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(138, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue4");
        columnConfiguration.setColumnIndex(139);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(139, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue5");
        columnConfiguration.setColumnIndex(140);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(140, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore0");
        columnConfiguration.setColumnIndex(141);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(141, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("negativePincode");
        columnConfiguration.setColumnIndex(142);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(142, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("mobileVer");
        columnConfiguration.setColumnIndex(143);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(143, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(144);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(144, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(145);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(145, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(146);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(146, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(147);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(147, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(148);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(148, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(149);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(149, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(150);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(150, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(151);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(151, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(152);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(152, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(153);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(153, columnConfiguration);

    }
}
