package com.softcell.config.reporting;

import com.softcell.constants.FieldSeparator;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * class to generate default system
 *
 * @author Kishore Parkhe
 */
public class ColumConfigurationMap {

    public static Map<String, ColumnConfiguration> fieldKeyMap = new TreeMap<>();
    public static Set<ColumnConfiguration> sysFieldList = new HashSet<>();
    public static Map<Integer, ColumnConfiguration> defaultConfigMap = new TreeMap<>();
    public static Map<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap<>();

    static {

        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(0);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(0, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dsaId");
        columnConfiguration.setColumnIndex(1);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(1, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dealerId");
        columnConfiguration.setColumnIndex(2);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(2, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deDate");
        columnConfiguration.setColumnIndex(3);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(3, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSubmitDate");
        columnConfiguration.setColumnIndex(4);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(4, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croId");
        columnConfiguration.setColumnIndex(5);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(5, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentStage");
        columnConfiguration.setColumnIndex(6);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(6, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("firstName");
        columnConfiguration.setColumnIndex(7);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(7, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("lastName");
        columnConfiguration.setColumnIndex(8);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(8, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("middleName");
        columnConfiguration.setColumnIndex(9);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(9, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("maritalStatus");
        columnConfiguration.setColumnIndex(10);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(10, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("gender");
        columnConfiguration.setColumnIndex(11);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(11, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(12);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(12, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("age");
        columnConfiguration.setColumnIndex(13);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(13, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eduction");
        columnConfiguration.setColumnIndex(14);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(14, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(15);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(15, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appStatus");
        columnConfiguration.setColumnIndex(16);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(16, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfDependent");
        columnConfiguration.setColumnIndex(17);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(17, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfEarningMemb");
        columnConfiguration.setColumnIndex(18);
        columnConfiguration.setColumnKey("C_A_noOfEarningMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(18, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfFamilyMemb");
        columnConfiguration.setColumnIndex(19);
        columnConfiguration.setColumnKey("C_A_noOfFamilyMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(19, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber0");
        columnConfiguration.setColumnIndex(20);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(20, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber1");
        columnConfiguration.setColumnIndex(21);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(21, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType0");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(22, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails0");
        columnConfiguration.setColumnIndex(23);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(23, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals0");
        columnConfiguration.setColumnIndex(24);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(24, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(25);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(25, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(26);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(26, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails0");
        columnConfiguration.setColumnIndex(27);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(27, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi0");
        columnConfiguration.setColumnIndex(28);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(28, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount0");
        columnConfiguration.setColumnIndex(29);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(29, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct0");
        columnConfiguration.setColumnIndex(30);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(30, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType0");
        columnConfiguration.setColumnIndex(31);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(31, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine10");
        columnConfiguration.setColumnIndex(32);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(32, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine20");
        columnConfiguration.setColumnIndex(33);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(33, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(34);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(34, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark0");
        columnConfiguration.setColumnIndex(35);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(35, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode0");
        columnConfiguration.setColumnIndex(36);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(36, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city0");
        columnConfiguration.setColumnIndex(37);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(37, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state0");
        columnConfiguration.setColumnIndex(38);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(38, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine11");
        columnConfiguration.setColumnIndex(39);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(39, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine21");
        columnConfiguration.setColumnIndex(40);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(40, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(41);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(41, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark1");
        columnConfiguration.setColumnIndex(42);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(42, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode1");
        columnConfiguration.setColumnIndex(43);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(43, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city1");
        columnConfiguration.setColumnIndex(44);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(44, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state1");
        columnConfiguration.setColumnIndex(45);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(45, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine12");
        columnConfiguration.setColumnIndex(46);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(46, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine22");
        columnConfiguration.setColumnIndex(47);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(47, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(48);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(48, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark2");
        columnConfiguration.setColumnIndex(49);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(49, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode2");
        columnConfiguration.setColumnIndex(50);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(50, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city2");
        columnConfiguration.setColumnIndex(51);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(51, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state2");
        columnConfiguration.setColumnIndex(52);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(52, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country0");
        columnConfiguration.setColumnIndex(53);
        columnConfiguration.setColumnKey("C_CustomerAddress_country0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(53, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country1");
        columnConfiguration.setColumnIndex(54);
        columnConfiguration.setColumnKey("C_CustomerAddress_country1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(54, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country2");
        columnConfiguration.setColumnIndex(55);
        columnConfiguration.setColumnKey("C_CustomerAddress_country2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(55, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType0");
        columnConfiguration.setColumnIndex(56);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(56, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt0");
        columnConfiguration.setColumnIndex(57);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(57, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode0");
        columnConfiguration.setColumnIndex(58);
        columnConfiguration.setColumnKey("C_Phone_countryCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(58, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode0");
        columnConfiguration.setColumnIndex(59);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(59, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber0");
        columnConfiguration.setColumnIndex(60);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(60, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType1");
        columnConfiguration.setColumnIndex(61);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(61, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt1");
        columnConfiguration.setColumnIndex(62);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(62, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode1");
        columnConfiguration.setColumnIndex(63);
        columnConfiguration.setColumnKey("C_Phone_countryCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(63, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode1");
        columnConfiguration.setColumnIndex(64);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(64, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber1");
        columnConfiguration.setColumnIndex(65);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(65, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType2");
        columnConfiguration.setColumnIndex(66);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(66, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt2");
        columnConfiguration.setColumnIndex(67);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(67, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode2");
        columnConfiguration.setColumnIndex(68);
        columnConfiguration.setColumnKey("C_Phone_countryCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(68, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode2");
        columnConfiguration.setColumnIndex(69);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(69, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber2");
        columnConfiguration.setColumnIndex(70);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(70, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType3");
        columnConfiguration.setColumnIndex(71);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(71, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt3");
        columnConfiguration.setColumnIndex(72);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(72, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode3");
        columnConfiguration.setColumnIndex(73);
        columnConfiguration.setColumnKey("C_Phone_countryCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(73, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode3");
        columnConfiguration.setColumnIndex(74);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(74, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber3");
        columnConfiguration.setColumnIndex(75);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(75, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress0");
        columnConfiguration.setColumnIndex(76);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(76, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType0");
        columnConfiguration.setColumnIndex(77);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(77, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress1");
        columnConfiguration.setColumnIndex(78);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(78, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType1");
        columnConfiguration.setColumnIndex(79);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(79, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress2");
        columnConfiguration.setColumnIndex(80);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(80, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType2");
        columnConfiguration.setColumnIndex(81);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(81, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress3");
        columnConfiguration.setColumnIndex(82);
        columnConfiguration.setColumnKey("C_Email_emailAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(82, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType3");
        columnConfiguration.setColumnIndex(83);
        columnConfiguration.setColumnKey("C_Email_emailType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(83, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution0");
        columnConfiguration.setColumnIndex(84);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(84, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName0");
        columnConfiguration.setColumnIndex(85);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(85, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType0");
        columnConfiguration.setColumnIndex(86);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(86, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName0");
        columnConfiguration.setColumnIndex(87);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(87, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber0");
        columnConfiguration.setColumnIndex(88);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(88, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName1");
        columnConfiguration.setColumnIndex(89);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(89, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber1");
        columnConfiguration.setColumnIndex(90);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(90, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName2");
        columnConfiguration.setColumnIndex(91);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(91, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber2");
        columnConfiguration.setColumnIndex(92);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(92, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName3");
        columnConfiguration.setColumnIndex(93);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(93, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber3");
        columnConfiguration.setColumnIndex(94);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(94, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName4");
        columnConfiguration.setColumnIndex(95);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(95, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber4");
        columnConfiguration.setColumnIndex(96);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(96, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName5");
        columnConfiguration.setColumnIndex(97);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(97, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber5");
        columnConfiguration.setColumnIndex(98);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(98, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName6");
        columnConfiguration.setColumnIndex(99);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(99, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber6");
        columnConfiguration.setColumnIndex(100);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(100, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg0");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(101, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake0");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(102, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName0");
        columnConfiguration.setColumnIndex(103);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(103, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo0");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(104, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice0");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(105, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(106, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt");
        columnConfiguration.setColumnIndex(107);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(107, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr");
        columnConfiguration.setColumnIndex(108);
        columnConfiguration.setColumnKey("C_L_A_loanApr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(108, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose");
        columnConfiguration.setColumnIndex(109);
        columnConfiguration.setColumnKey("C_L_A_loanPurpose999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(109, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanTenor");
        columnConfiguration.setColumnIndex(110);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(110, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(111, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("marginAmt");
        columnConfiguration.setColumnIndex(112);
        columnConfiguration.setColumnKey("C_L_A_marginAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(112, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("numberOfAdvEmi");
        columnConfiguration.setColumnIndex(113);
        columnConfiguration.setColumnKey("C_L_A_numberOfAdvanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(113, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType0");
        columnConfiguration.setColumnIndex(114);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(114, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(115);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(115, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(116);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(116, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(117);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(117, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(118);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(118, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(119);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(119, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(120);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(120, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(121);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(121, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(122);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(122, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(123);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(123, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(124);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(124, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croApprAmt0");
        columnConfiguration.setColumnIndex(125);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(125, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("apprDate0");
        columnConfiguration.setColumnIndex(126);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(126, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("downPayment0");
        columnConfiguration.setColumnIndex(127);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(127, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(128, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi0");
        columnConfiguration.setColumnIndex(129);
        columnConfiguration.setColumnKey("CRO_CroDecision_emi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(129, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("interestRate0");
        columnConfiguration.setColumnIndex(130);
        columnConfiguration.setColumnKey("CRO_CroDecision_interestRate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(130, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("tenor0");
        columnConfiguration.setColumnIndex(131);
        columnConfiguration.setColumnKey("CRO_CroDecision_tenor0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(131, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability4");
        columnConfiguration.setColumnIndex(132);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(132, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability5");
        columnConfiguration.setColumnIndex(133);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(133, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("panName");
        columnConfiguration.setColumnIndex(134);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(134, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("panStatus0");
        columnConfiguration.setColumnIndex(135);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(135, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appScore");
        columnConfiguration.setColumnIndex(136);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(136, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressScore");
        columnConfiguration.setColumnIndex(137);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(137, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("officeAddressScore");
        columnConfiguration.setColumnIndex(138);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(138, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue4");
        columnConfiguration.setColumnIndex(139);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(139, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue5");
        columnConfiguration.setColumnIndex(140);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(140, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore0");
        columnConfiguration.setColumnIndex(141);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(141, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("negativePincode");
        columnConfiguration.setColumnIndex(142);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(142, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("mobileVer");
        columnConfiguration.setColumnIndex(143);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(143, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(144);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(144, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(145);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(145, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(146);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(146, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(147);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(147, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(148);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(148, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(149);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(149, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(150);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(150, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(151);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(151, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(152);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(152, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(153);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(153, columnConfiguration);

    }

    static {
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability0");
        columnConfiguration.setColumnIndex(0);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(0, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability1");
        columnConfiguration.setColumnIndex(1);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(1, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability2");
        columnConfiguration.setColumnIndex(2);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(2, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability3");
        columnConfiguration.setColumnIndex(3);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(3, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability4");
        columnConfiguration.setColumnIndex(4);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(4, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability5");
        columnConfiguration.setColumnIndex(5);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(5, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName0");
        columnConfiguration.setColumnIndex(6);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(6, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName1");
        columnConfiguration.setColumnIndex(7);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(7, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName2");
        columnConfiguration.setColumnIndex(8);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(8, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName3");
        columnConfiguration.setColumnIndex(9);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(9, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName4");
        columnConfiguration.setColumnIndex(10);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(10, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName5");
        columnConfiguration.setColumnIndex(11);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(11, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue0");
        columnConfiguration.setColumnIndex(12);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(12, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue1");
        columnConfiguration.setColumnIndex(13);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(13, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue2");
        columnConfiguration.setColumnIndex(14);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(14, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue3");
        columnConfiguration.setColumnIndex(15);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(15, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue4");
        columnConfiguration.setColumnIndex(16);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(16, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue5");
        columnConfiguration.setColumnIndex(17);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(17, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage0");
        columnConfiguration.setColumnIndex(18);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(18, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage1");
        columnConfiguration.setColumnIndex(19);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(19, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage2");
        columnConfiguration.setColumnIndex(20);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(20, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage3");
        columnConfiguration.setColumnIndex(21);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(21, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage4");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(22, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage5");
        columnConfiguration.setColumnIndex(23);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(23, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore0");
        columnConfiguration.setColumnIndex(24);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(24, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore1");
        columnConfiguration.setColumnIndex(25);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(25, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder0");
        columnConfiguration.setColumnIndex(26);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(26, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder1");
        columnConfiguration.setColumnIndex(27);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(27, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder2");
        columnConfiguration.setColumnIndex(28);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(28, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder3");
        columnConfiguration.setColumnIndex(29);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(29, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder4");
        columnConfiguration.setColumnIndex(30);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(30, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder5");
        columnConfiguration.setColumnIndex(31);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(31, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appStatus");
        columnConfiguration.setColumnIndex(32);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(32, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSubmitDate");
        columnConfiguration.setColumnIndex(33);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(33, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(34);
        columnConfiguration.setColumnKey("A_gngRefId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(34, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("negativePincode");
        columnConfiguration.setColumnIndex(35);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(35, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfRetry");
        columnConfiguration.setColumnIndex(36);
        columnConfiguration.setColumnKey("A_noOfReTry999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(36, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(37);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(37, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croApprAmt0");
        columnConfiguration.setColumnIndex(38);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(38, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("apprDate0");
        columnConfiguration.setColumnIndex(39);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(39, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("downPayment0");
        columnConfiguration.setColumnIndex(40);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(40, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(41);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(41, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi0");
        columnConfiguration.setColumnIndex(42);
        columnConfiguration.setColumnKey("CRO_CroDecision_emi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(42, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("interestRate0");
        columnConfiguration.setColumnIndex(43);
        columnConfiguration.setColumnKey("CRO_CroDecision_interestRate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(43, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("tenor0");
        columnConfiguration.setColumnIndex(44);
        columnConfiguration.setColumnKey("CRO_CroDecision_tenor0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(44, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("aadharVerFlag");
        columnConfiguration.setColumnIndex(45);
        columnConfiguration.setColumnKey("C_A_addharVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(45, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("age");
        columnConfiguration.setColumnIndex(46);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(46, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appId");
        columnConfiguration.setColumnIndex(47);
        columnConfiguration.setColumnKey("C_A_applicantId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(47, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("cardNumber");
        columnConfiguration.setColumnIndex(48);
        columnConfiguration.setColumnKey("C_A_creditCardNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(48, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(49);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(49, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eduction");
        columnConfiguration.setColumnIndex(50);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(50, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("gender");
        columnConfiguration.setColumnIndex(51);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(51, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressFlag");
        columnConfiguration.setColumnIndex(52);
        columnConfiguration.setColumnKey("C_A_isResidenceAddSameAsAbove999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(52, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("maritalStatus");
        columnConfiguration.setColumnIndex(53);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(53, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("mobileVer");
        columnConfiguration.setColumnIndex(54);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(54, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfDependent");
        columnConfiguration.setColumnIndex(55);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(55, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfEarningMemb");
        columnConfiguration.setColumnIndex(56);
        columnConfiguration.setColumnKey("C_A_noOfEarningMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(56, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfFamilyMemb");
        columnConfiguration.setColumnIndex(57);
        columnConfiguration.setColumnKey("C_A_noOfFamilyMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(57, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber0");
        columnConfiguration.setColumnIndex(58);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(58, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber1");
        columnConfiguration.setColumnIndex(59);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(59, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType0");
        columnConfiguration.setColumnIndex(60);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(60, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType1");
        columnConfiguration.setColumnIndex(61);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(61, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails0");
        columnConfiguration.setColumnIndex(62);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(62, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails1");
        columnConfiguration.setColumnIndex(63);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(63, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals0");
        columnConfiguration.setColumnIndex(64);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(64, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals1");
        columnConfiguration.setColumnIndex(65);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(65, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(66);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(66, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(67);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(67, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(68);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(68, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(69);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(69, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails0");
        columnConfiguration.setColumnIndex(70);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(70, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails1");
        columnConfiguration.setColumnIndex(71);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(71, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi0");
        columnConfiguration.setColumnIndex(72);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(72, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi1");
        columnConfiguration.setColumnIndex(73);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(73, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount0");
        columnConfiguration.setColumnIndex(74);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(74, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount1");
        columnConfiguration.setColumnIndex(75);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(75, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct0");
        columnConfiguration.setColumnIndex(76);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(76, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct1");
        columnConfiguration.setColumnIndex(77);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(77, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType0");
        columnConfiguration.setColumnIndex(78);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(78, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType1");
        columnConfiguration.setColumnIndex(79);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(79, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType2");
        columnConfiguration.setColumnIndex(80);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(80, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType3");
        columnConfiguration.setColumnIndex(81);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(81, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress0");
        columnConfiguration.setColumnIndex(82);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(82, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress1");
        columnConfiguration.setColumnIndex(83);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(83, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress2");
        columnConfiguration.setColumnIndex(84);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(84, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress3");
        columnConfiguration.setColumnIndex(85);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(85, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity0");
        columnConfiguration.setColumnIndex(86);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(86, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity1");
        columnConfiguration.setColumnIndex(87);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(87, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity2");
        columnConfiguration.setColumnIndex(88);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(88, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity3");
        columnConfiguration.setColumnIndex(89);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(89, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount0");
        columnConfiguration.setColumnIndex(90);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(90, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount1");
        columnConfiguration.setColumnIndex(91);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(91, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount2");
        columnConfiguration.setColumnIndex(92);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(92, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount3");
        columnConfiguration.setColumnIndex(93);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(93, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType0");
        columnConfiguration.setColumnIndex(94);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(94, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType1");
        columnConfiguration.setColumnIndex(95);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(95, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType2");
        columnConfiguration.setColumnIndex(96);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(96, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType3");
        columnConfiguration.setColumnIndex(97);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(97, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres0");
        columnConfiguration.setColumnIndex(98);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(98, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres1");
        columnConfiguration.setColumnIndex(99);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(99, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres2");
        columnConfiguration.setColumnIndex(100);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(100, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres3");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(101, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity0");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(102, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity1");
        columnConfiguration.setColumnIndex(103);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(103, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity2");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(104, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity3");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(105, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress0");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(106, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress1");
        columnConfiguration.setColumnIndex(107);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(107, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress2");
        columnConfiguration.setColumnIndex(108);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(108, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress3");
        columnConfiguration.setColumnIndex(109);
        columnConfiguration.setColumnKey("C_Email_emailAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(109, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType0");
        columnConfiguration.setColumnIndex(110);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(110, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType1");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(111, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType2");
        columnConfiguration.setColumnIndex(112);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(112, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType3");
        columnConfiguration.setColumnIndex(113);
        columnConfiguration.setColumnKey("C_Email_emailType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(113, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution0");
        columnConfiguration.setColumnIndex(114);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(114, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution1");
        columnConfiguration.setColumnIndex(115);
        columnConfiguration.setColumnKey("C_Employment_constitution1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(115, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName0");
        columnConfiguration.setColumnIndex(116);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(116, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName1");
        columnConfiguration.setColumnIndex(117);
        columnConfiguration.setColumnKey("C_Employment_employmentName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(117, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType0");
        columnConfiguration.setColumnIndex(118);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(118, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType1");
        columnConfiguration.setColumnIndex(119);
        columnConfiguration.setColumnKey("C_Employment_employmentType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(119, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary0");
        columnConfiguration.setColumnIndex(120);
        columnConfiguration.setColumnKey("C_Employment_grossSalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(120, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary1");
        columnConfiguration.setColumnIndex(121);
        columnConfiguration.setColumnKey("C_Employment_grossSalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(121, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType0");
        columnConfiguration.setColumnIndex(122);
        columnConfiguration.setColumnKey("C_Employment_industryType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(122, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType1");
        columnConfiguration.setColumnIndex(123);
        columnConfiguration.setColumnKey("C_Employment_industryType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(123, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt0");
        columnConfiguration.setColumnIndex(124);
        columnConfiguration.setColumnKey("C_Employment_itrAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(124, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt1");
        columnConfiguration.setColumnIndex(125);
        columnConfiguration.setColumnKey("C_Employment_itrAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(125, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment0");
        columnConfiguration.setColumnIndex(126);
        columnConfiguration.setColumnKey("C_Employment_modePayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(126, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment1");
        columnConfiguration.setColumnIndex(127);
        columnConfiguration.setColumnKey("C_Employment_modePayment1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(127, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(128, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary1");
        columnConfiguration.setColumnIndex(129);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(129, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer0");
        columnConfiguration.setColumnIndex(130);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(130, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer1");
        columnConfiguration.setColumnIndex(131);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(131, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince0");
        columnConfiguration.setColumnIndex(132);
        columnConfiguration.setColumnKey("C_Employment_workExps0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(132, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince1");
        columnConfiguration.setColumnIndex(133);
        columnConfiguration.setColumnKey("C_Employment_workExps1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(133, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName0");
        columnConfiguration.setColumnIndex(134);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(134, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName1");
        columnConfiguration.setColumnIndex(135);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(135, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName2");
        columnConfiguration.setColumnIndex(136);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(136, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName3");
        columnConfiguration.setColumnIndex(137);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(137, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName4");
        columnConfiguration.setColumnIndex(138);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(138, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName5");
        columnConfiguration.setColumnIndex(139);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(139, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName6");
        columnConfiguration.setColumnIndex(140);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(140, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber0");
        columnConfiguration.setColumnIndex(141);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(141, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber1");
        columnConfiguration.setColumnIndex(142);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(142, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber2");
        columnConfiguration.setColumnIndex(143);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(143, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber3");
        columnConfiguration.setColumnIndex(144);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(144, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber4");
        columnConfiguration.setColumnIndex(145);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(145, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber5");
        columnConfiguration.setColumnIndex(146);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(146, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber6");
        columnConfiguration.setColumnIndex(147);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(147, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg0");
        columnConfiguration.setColumnIndex(148);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(148, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg1");
        columnConfiguration.setColumnIndex(149);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(149, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake0");
        columnConfiguration.setColumnIndex(150);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(150, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake1");
        columnConfiguration.setColumnIndex(151);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(151, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName0");
        columnConfiguration.setColumnIndex(152);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(152, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName1");
        columnConfiguration.setColumnIndex(153);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(153, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo0");
        columnConfiguration.setColumnIndex(154);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(154, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo1");
        columnConfiguration.setColumnIndex(155);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(155, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice0");
        columnConfiguration.setColumnIndex(156);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(156, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice1");
        columnConfiguration.setColumnIndex(157);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(157, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(158);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(158, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt");
        columnConfiguration.setColumnIndex(159);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(159, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr");
        columnConfiguration.setColumnIndex(160);
        columnConfiguration.setColumnKey("C_L_A_loanApr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(160, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose");
        columnConfiguration.setColumnIndex(161);
        columnConfiguration.setColumnKey("C_L_A_loanPurpose999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(161, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanTenor");
        columnConfiguration.setColumnIndex(162);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(162, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(163);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(163, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("marginAmt");
        columnConfiguration.setColumnIndex(164);
        columnConfiguration.setColumnKey("C_L_A_marginAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(164, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("numberOfAdvEmi");
        columnConfiguration.setColumnIndex(165);
        columnConfiguration.setColumnKey("C_L_A_numberOfAdvanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(165, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(166);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(166, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(167);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(167, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi0");
        columnConfiguration.setColumnIndex(168);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(168, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi1");
        columnConfiguration.setColumnIndex(169);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(169, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt0");
        columnConfiguration.setColumnIndex(170);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(170, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt1");
        columnConfiguration.setColumnIndex(171);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(171, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr0");
        columnConfiguration.setColumnIndex(172);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(172, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr1");
        columnConfiguration.setColumnIndex(173);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(173, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose0");
        columnConfiguration.setColumnIndex(174);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(174, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose1");
        columnConfiguration.setColumnIndex(175);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(175, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType0");
        columnConfiguration.setColumnIndex(176);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(176, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType1");
        columnConfiguration.setColumnIndex(177);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(177, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB0");
        columnConfiguration.setColumnIndex(178);
        columnConfiguration.setColumnKey("C_LoanDetails_mob0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(178, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB1");
        columnConfiguration.setColumnIndex(179);
        columnConfiguration.setColumnKey("C_LoanDetails_mob1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(179, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("firstName");
        columnConfiguration.setColumnIndex(180);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(180, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("lastName");
        columnConfiguration.setColumnIndex(181);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(181, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("middleName");
        columnConfiguration.setColumnIndex(182);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(182, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode0");
        columnConfiguration.setColumnIndex(183);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(183, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode1");
        columnConfiguration.setColumnIndex(184);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(184, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode2");
        columnConfiguration.setColumnIndex(185);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(185, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode3");
        columnConfiguration.setColumnIndex(186);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(186, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode4");
        columnConfiguration.setColumnIndex(187);
        columnConfiguration.setColumnKey("C_Phone_areaCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(187, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode5");
        columnConfiguration.setColumnIndex(188);
        columnConfiguration.setColumnKey("C_Phone_areaCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(188, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode6");
        columnConfiguration.setColumnIndex(189);
        columnConfiguration.setColumnKey("C_Phone_areaCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(189, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode0");
        columnConfiguration.setColumnIndex(190);
        columnConfiguration.setColumnKey("C_Phone_countryCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(190, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode1");
        columnConfiguration.setColumnIndex(191);
        columnConfiguration.setColumnKey("C_Phone_countryCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(191, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode2");
        columnConfiguration.setColumnIndex(192);
        columnConfiguration.setColumnKey("C_Phone_countryCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(192, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode3");
        columnConfiguration.setColumnIndex(193);
        columnConfiguration.setColumnKey("C_Phone_countryCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(193, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode4");
        columnConfiguration.setColumnIndex(194);
        columnConfiguration.setColumnKey("C_Phone_countryCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(194, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode5");
        columnConfiguration.setColumnIndex(195);
        columnConfiguration.setColumnKey("C_Phone_countryCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(195, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode6");
        columnConfiguration.setColumnIndex(196);
        columnConfiguration.setColumnKey("C_Phone_countryCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(196, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt0");
        columnConfiguration.setColumnIndex(197);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(197, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt1");
        columnConfiguration.setColumnIndex(198);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(198, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt2");
        columnConfiguration.setColumnIndex(199);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(199, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt3");
        columnConfiguration.setColumnIndex(200);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(200, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt4");
        columnConfiguration.setColumnIndex(201);
        columnConfiguration.setColumnKey("C_Phone_extension4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(201, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt5");
        columnConfiguration.setColumnIndex(202);
        columnConfiguration.setColumnKey("C_Phone_extension5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(202, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt6");
        columnConfiguration.setColumnIndex(203);
        columnConfiguration.setColumnKey("C_Phone_extension6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(203, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber0");
        columnConfiguration.setColumnIndex(204);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(204, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber1");
        columnConfiguration.setColumnIndex(205);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(205, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber2");
        columnConfiguration.setColumnIndex(206);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(206, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber3");
        columnConfiguration.setColumnIndex(207);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(207, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber4");
        columnConfiguration.setColumnIndex(208);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(208, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber5");
        columnConfiguration.setColumnIndex(209);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(209, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber6");
        columnConfiguration.setColumnIndex(210);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(210, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType0");
        columnConfiguration.setColumnIndex(211);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(211, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType1");
        columnConfiguration.setColumnIndex(212);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(212, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType2");
        columnConfiguration.setColumnIndex(213);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(213, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType3");
        columnConfiguration.setColumnIndex(214);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(214, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType4");
        columnConfiguration.setColumnIndex(215);
        columnConfiguration.setColumnKey("C_Phone_phoneType4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(215, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType5");
        columnConfiguration.setColumnIndex(216);
        columnConfiguration.setColumnKey("C_Phone_phoneType5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(216, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType6");
        columnConfiguration.setColumnIndex(217);
        columnConfiguration.setColumnKey("C_Phone_phoneType6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(217, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("otherSource");
        columnConfiguration.setColumnIndex(218);
        columnConfiguration.setColumnKey("C_otherSourceIncomeAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(218, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentStage");
        columnConfiguration.setColumnIndex(219);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(219, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(220);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(220, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("responseFormat");
        columnConfiguration.setColumnIndex(221);
        columnConfiguration.setColumnKey("appRequest_responseFormat999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(221, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(222);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(222, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(223);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(223, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(224);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(224, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(225);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(225, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(226);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(226, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(227);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(227, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(228);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(228, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(229);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(229, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(230);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(230, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(231);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(231, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSecndoryId");
        columnConfiguration.setColumnIndex(232);
        columnConfiguration.setColumnKey("header_applicationId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(232, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("applicationSource");
        columnConfiguration.setColumnIndex(233);
        columnConfiguration.setColumnKey("header_applicationSource999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(233, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croId");
        columnConfiguration.setColumnIndex(234);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(234, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deDate");
        columnConfiguration.setColumnIndex(235);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(235, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dealerId");
        columnConfiguration.setColumnIndex(236);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(236, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dsaId");
        columnConfiguration.setColumnIndex(237);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(237, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("institutionId");
        columnConfiguration.setColumnIndex(238);
        columnConfiguration.setColumnKey("header_institutionId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(238, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("requestType");
        columnConfiguration.setColumnIndex(239);
        columnConfiguration.setColumnKey("header_requestType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(239, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("sourceId");
        columnConfiguration.setColumnIndex(240);
        columnConfiguration.setColumnKey("header_sourceId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(240, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("suspiciousActivity");
        columnConfiguration.setColumnIndex(241);
        columnConfiguration.setColumnKey("request_suspiciousActivity999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(241, columnConfiguration);
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine10");
        columnConfiguration.setColumnIndex(242);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(242, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine11");
        columnConfiguration.setColumnIndex(243);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(243, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine12");
        columnConfiguration.setColumnIndex(244);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(244, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine20");
        columnConfiguration.setColumnIndex(245);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(245, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine21");
        columnConfiguration.setColumnIndex(246);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(246, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine22");
        columnConfiguration.setColumnIndex(247);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(247, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city0");
        columnConfiguration.setColumnIndex(248);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(248, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city1");
        columnConfiguration.setColumnIndex(249);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(249, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city2");
        columnConfiguration.setColumnIndex(250);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(250, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country0");
        columnConfiguration.setColumnIndex(251);
        columnConfiguration.setColumnKey("C_CustomerAddress_country0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(251, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country1");
        columnConfiguration.setColumnIndex(252);
        columnConfiguration.setColumnKey("C_CustomerAddress_country1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(252, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country2");
        columnConfiguration.setColumnIndex(253);
        columnConfiguration.setColumnKey("C_CustomerAddress_country2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(253, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress0");
        columnConfiguration.setColumnIndex(254);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(254, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress1");
        columnConfiguration.setColumnIndex(255);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(255, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress2");
        columnConfiguration.setColumnIndex(256);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(256, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark0");
        columnConfiguration.setColumnIndex(257);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(257, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark1");
        columnConfiguration.setColumnIndex(258);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(258, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark2");
        columnConfiguration.setColumnIndex(259);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(259, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(260);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(260, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(261);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(261, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(262);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(262, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode0");
        columnConfiguration.setColumnIndex(263);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(263, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode1");
        columnConfiguration.setColumnIndex(264);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(264, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode2");
        columnConfiguration.setColumnIndex(265);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(265, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state0");
        columnConfiguration.setColumnIndex(266);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(266, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state1");
        columnConfiguration.setColumnIndex(267);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(267, columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state2");
        columnConfiguration.setColumnIndex(268);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMap.put(268, columnConfiguration);

    }

    static {

        ColumnConfiguration columnConfiguration;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability0");
        columnConfiguration.setColumnIndex(0);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability1");
        columnConfiguration.setColumnIndex(1);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability2");
        columnConfiguration.setColumnIndex(2);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability3");
        columnConfiguration.setColumnIndex(3);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability4");
        columnConfiguration.setColumnIndex(4);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability4",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability5");
        columnConfiguration.setColumnIndex(5);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_addStability5",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName0");
        columnConfiguration.setColumnIndex(6);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName1");
        columnConfiguration.setColumnIndex(7);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName2");
        columnConfiguration.setColumnIndex(8);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName3");
        columnConfiguration.setColumnIndex(9);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName4");
        columnConfiguration.setColumnIndex(10);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName4",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName5");
        columnConfiguration.setColumnIndex(11);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldName5",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue0");
        columnConfiguration.setColumnIndex(12);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue1");
        columnConfiguration.setColumnIndex(13);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue2");
        columnConfiguration.setColumnIndex(14);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue3");
        columnConfiguration.setColumnIndex(15);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue4");
        columnConfiguration.setColumnIndex(16);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue4",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue5");
        columnConfiguration.setColumnIndex(17);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_fieldValue5",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage0");
        columnConfiguration.setColumnIndex(18);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage1");
        columnConfiguration.setColumnIndex(19);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage2");
        columnConfiguration.setColumnIndex(20);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage3");
        columnConfiguration.setColumnIndex(21);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage4");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message4",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage5");
        columnConfiguration.setColumnIndex(23);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_message5",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore0");
        columnConfiguration.setColumnIndex(24);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_nameScore0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore1");
        columnConfiguration.setColumnIndex(25);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_nameScore1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder0");
        columnConfiguration.setColumnIndex(26);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder1");
        columnConfiguration.setColumnIndex(27);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder2");
        columnConfiguration.setColumnIndex(28);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder3");
        columnConfiguration.setColumnIndex(29);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder4");
        columnConfiguration.setColumnIndex(30);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order4",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder5");
        columnConfiguration.setColumnIndex(31);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_ApplicationScoringResult_order5",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appStatus");
        columnConfiguration.setColumnIndex(32);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_applicationStatus999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSubmitDate");
        columnConfiguration.setColumnIndex(33);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_dateTime999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(34);
        columnConfiguration.setColumnKey("A_gngRefId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_gngRefId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("negativePincode");
        columnConfiguration.setColumnIndex(35);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_negativePincode999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfRetry");
        columnConfiguration.setColumnIndex(36);
        columnConfiguration.setColumnKey("A_noOfReTry999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_noOfReTry999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(37);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("A_statusFlag999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croApprAmt0");
        columnConfiguration.setColumnIndex(38);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_amtApproved0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("apprDate0");
        columnConfiguration.setColumnIndex(39);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_decisionUpdateDate0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("downPayment0");
        columnConfiguration.setColumnIndex(40);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_downPayment0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(41);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_eligibleAmt0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi0");
        columnConfiguration.setColumnIndex(42);
        columnConfiguration.setColumnKey("CRO_CroDecision_emi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_emi0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("interestRate0");
        columnConfiguration.setColumnIndex(43);
        columnConfiguration.setColumnKey("CRO_CroDecision_interestRate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_interestRate0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("tenor0");
        columnConfiguration.setColumnIndex(44);
        columnConfiguration.setColumnKey("CRO_CroDecision_tenor0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("CRO_CroDecision_tenor0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("aadharVerFlag");
        columnConfiguration.setColumnIndex(45);
        columnConfiguration.setColumnKey("C_A_addharVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_addharVerified999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("age");
        columnConfiguration.setColumnIndex(46);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_age999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appId");
        columnConfiguration.setColumnIndex(47);
        columnConfiguration.setColumnKey("C_A_applicantId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_applicantId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("cardNumber");
        columnConfiguration.setColumnIndex(48);
        columnConfiguration.setColumnKey("C_A_creditCardNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_creditCardNumber999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(49);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_dateOfBirth999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eduction");
        columnConfiguration.setColumnIndex(50);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_education999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("gender");
        columnConfiguration.setColumnIndex(51);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_gender999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressFlag");
        columnConfiguration.setColumnIndex(52);
        columnConfiguration.setColumnKey("C_A_isResidenceAddSameAsAbove999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_A_isResidenceAddSameAsAbove999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("maritalStatus");
        columnConfiguration.setColumnIndex(53);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_maritalStatus999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("mobileVer");
        columnConfiguration.setColumnIndex(54);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_mobileVerified999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfDependent");
        columnConfiguration.setColumnIndex(55);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_noOfDependents999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfEarningMemb");
        columnConfiguration.setColumnIndex(56);
        columnConfiguration.setColumnKey("C_A_noOfEarningMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_noOfEarningMembers999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfFamilyMemb");
        columnConfiguration.setColumnIndex(57);
        columnConfiguration.setColumnKey("C_A_noOfFamilyMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_A_noOfFamilyMembers999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber0");
        columnConfiguration.setColumnIndex(58);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_accountNumber0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber1");
        columnConfiguration.setColumnIndex(59);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_accountNumber1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType0");
        columnConfiguration.setColumnIndex(60);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_accountType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType1");
        columnConfiguration.setColumnIndex(61);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_accountType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails0");
        columnConfiguration.setColumnIndex(62);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_anyEmi0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails1");
        columnConfiguration.setColumnIndex(63);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_anyEmi1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals0");
        columnConfiguration.setColumnIndex(64);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_BankingDetails_avgBankBalance0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals1");
        columnConfiguration.setColumnIndex(65);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_BankingDetails_avgBankBalance1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(66);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_bankName0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(67);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_bankName1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(68);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_branchName0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(69);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_branchName1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails0");
        columnConfiguration.setColumnIndex(70);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_currentlyRunningLoan0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails1");
        columnConfiguration.setColumnIndex(71);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_currentlyRunningLoan1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi0");
        columnConfiguration.setColumnIndex(72);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_BankingDetails_deductedEmiAmt0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi1");
        columnConfiguration.setColumnIndex(73);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_BankingDetails_deductedEmiAmt1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount0");
        columnConfiguration.setColumnIndex(74);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_mentionAmount0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount1");
        columnConfiguration.setColumnIndex(75);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_mentionAmount1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct0");
        columnConfiguration.setColumnIndex(76);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_salaryAccount0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct1");
        columnConfiguration.setColumnIndex(77);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_BankingDetails_salaryAccount1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType0");
        columnConfiguration.setColumnIndex(78);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType1");
        columnConfiguration.setColumnIndex(79);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType2");
        columnConfiguration.setColumnIndex(80);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressType2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType3");
        columnConfiguration.setColumnIndex(81);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressType3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress0");
        columnConfiguration.setColumnIndex(82);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtAddress0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress1");
        columnConfiguration.setColumnIndex(83);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtAddress1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress2");
        columnConfiguration.setColumnIndex(84);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtAddress2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress3");
        columnConfiguration.setColumnIndex(85);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtAddress3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity0");
        columnConfiguration.setColumnIndex(86);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtCity0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity1");
        columnConfiguration.setColumnIndex(87);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtCity1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity2");
        columnConfiguration.setColumnIndex(88);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtCity2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity3");
        columnConfiguration.setColumnIndex(89);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_monthAtCity3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount0");
        columnConfiguration.setColumnIndex(90);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_rentAmount0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount1");
        columnConfiguration.setColumnIndex(91);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_rentAmount1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount2");
        columnConfiguration.setColumnIndex(92);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_rentAmount2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount3");
        columnConfiguration.setColumnIndex(93);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_rentAmount3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType0");
        columnConfiguration.setColumnIndex(94);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_residenceAddressType0",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType1");
        columnConfiguration.setColumnIndex(95);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_residenceAddressType1",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType2");
        columnConfiguration.setColumnIndex(96);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_residenceAddressType2",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType3");
        columnConfiguration.setColumnIndex(97);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_residenceAddressType3",
                columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres0");
        columnConfiguration.setColumnIndex(98);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_CustomerAddress_timeAtAddress0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres1");
        columnConfiguration.setColumnIndex(99);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_CustomerAddress_timeAtAddress1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres2");
        columnConfiguration.setColumnIndex(100);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_CustomerAddress_timeAtAddress2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres3");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap
                .put("C_CustomerAddress_timeAtAddress3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity0");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_yearAtCity0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity1");
        columnConfiguration.setColumnIndex(103);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_yearAtCity1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity2");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_yearAtCity2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity3");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_yearAtCity3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress0");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailAddress0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress1");
        columnConfiguration.setColumnIndex(107);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailAddress1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress2");
        columnConfiguration.setColumnIndex(108);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailAddress2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress3");
        columnConfiguration.setColumnIndex(109);
        columnConfiguration.setColumnKey("C_Email_emailAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailAddress3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType0");
        columnConfiguration.setColumnIndex(110);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType1");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType2");
        columnConfiguration.setColumnIndex(112);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailType2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType3");
        columnConfiguration.setColumnIndex(113);
        columnConfiguration.setColumnKey("C_Email_emailType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Email_emailType3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution0");
        columnConfiguration.setColumnIndex(114);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_constitution0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution1");
        columnConfiguration.setColumnIndex(115);
        columnConfiguration.setColumnKey("C_Employment_constitution1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_constitution1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName0");
        columnConfiguration.setColumnIndex(116);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_employmentName0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName1");
        columnConfiguration.setColumnIndex(117);
        columnConfiguration.setColumnKey("C_Employment_employmentName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_employmentName1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType0");
        columnConfiguration.setColumnIndex(118);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_employmentType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType1");
        columnConfiguration.setColumnIndex(119);
        columnConfiguration.setColumnKey("C_Employment_employmentType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_employmentType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary0");
        columnConfiguration.setColumnIndex(120);
        columnConfiguration.setColumnKey("C_Employment_grossSalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_grossSalary0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary1");
        columnConfiguration.setColumnIndex(121);
        columnConfiguration.setColumnKey("C_Employment_grossSalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_grossSalary1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType0");
        columnConfiguration.setColumnIndex(122);
        columnConfiguration.setColumnKey("C_Employment_industryType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_industryType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType1");
        columnConfiguration.setColumnIndex(123);
        columnConfiguration.setColumnKey("C_Employment_industryType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_industryType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt0");
        columnConfiguration.setColumnIndex(124);
        columnConfiguration.setColumnKey("C_Employment_itrAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_itrAmount0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt1");
        columnConfiguration.setColumnIndex(125);
        columnConfiguration.setColumnKey("C_Employment_itrAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_itrAmount1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment0");
        columnConfiguration.setColumnIndex(126);
        columnConfiguration.setColumnKey("C_Employment_modePayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_modePayment0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment1");
        columnConfiguration.setColumnIndex(127);
        columnConfiguration.setColumnKey("C_Employment_modePayment1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_modePayment1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_monthlySalary0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary1");
        columnConfiguration.setColumnIndex(129);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_monthlySalary1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer0");
        columnConfiguration.setColumnIndex(130);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_timeWithEmployer0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer1");
        columnConfiguration.setColumnIndex(131);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_timeWithEmployer1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince0");
        columnConfiguration.setColumnIndex(132);
        columnConfiguration.setColumnKey("C_Employment_workExps0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_workExps0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince1");
        columnConfiguration.setColumnIndex(133);
        columnConfiguration.setColumnKey("C_Employment_workExps1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Employment_workExps1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName0");
        columnConfiguration.setColumnIndex(134);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName1");
        columnConfiguration.setColumnIndex(135);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName2");
        columnConfiguration.setColumnIndex(136);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName3");
        columnConfiguration.setColumnIndex(137);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName4");
        columnConfiguration.setColumnIndex(138);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName5");
        columnConfiguration.setColumnIndex(139);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName6");
        columnConfiguration.setColumnIndex(140);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycName6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber0");
        columnConfiguration.setColumnIndex(141);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber1");
        columnConfiguration.setColumnIndex(142);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber2");
        columnConfiguration.setColumnIndex(143);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber3");
        columnConfiguration.setColumnIndex(144);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber4");
        columnConfiguration.setColumnIndex(145);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber5");
        columnConfiguration.setColumnIndex(146);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber6");
        columnConfiguration.setColumnIndex(147);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_KYC_Kyc_kycNumber6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg0");
        columnConfiguration.setColumnIndex(148);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_assetCtg0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg1");
        columnConfiguration.setColumnIndex(149);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_assetCtg1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake0");
        columnConfiguration.setColumnIndex(150);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_assetMake0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake1");
        columnConfiguration.setColumnIndex(151);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_assetMake1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName0");
        columnConfiguration.setColumnIndex(152);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_dlrName0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName1");
        columnConfiguration.setColumnIndex(153);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_dlrName1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo0");
        columnConfiguration.setColumnIndex(154);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_modelNo0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo1");
        columnConfiguration.setColumnIndex(155);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_modelNo1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice0");
        columnConfiguration.setColumnIndex(156);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_price0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice1");
        columnConfiguration.setColumnIndex(157);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_AssetDetails_price1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(158);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_emi999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt");
        columnConfiguration.setColumnIndex(159);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_loanAmount999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr");
        columnConfiguration.setColumnIndex(160);
        columnConfiguration.setColumnKey("C_L_A_loanApr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_loanApr999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose");
        columnConfiguration.setColumnIndex(161);
        columnConfiguration.setColumnKey("C_L_A_loanPurpose999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_loanPurpose999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanTenor");
        columnConfiguration.setColumnIndex(162);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_loanTenor999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(163);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_loanType999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("marginAmt");
        columnConfiguration.setColumnIndex(164);
        columnConfiguration.setColumnKey("C_L_A_marginAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_marginAmount999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("numberOfAdvEmi");
        columnConfiguration.setColumnIndex(165);
        columnConfiguration.setColumnKey("C_L_A_numberOfAdvanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_L_A_numberOfAdvanceEmi999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(166);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_balanceTenure0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(167);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_balanceTenure0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi0");
        columnConfiguration.setColumnIndex(168);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_emiAmt0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi1");
        columnConfiguration.setColumnIndex(169);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_emiAmt1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt0");
        columnConfiguration.setColumnIndex(170);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanAmt0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt1");
        columnConfiguration.setColumnIndex(171);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanAmt1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr0");
        columnConfiguration.setColumnIndex(172);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanApr0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr1");
        columnConfiguration.setColumnIndex(173);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanApr1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose0");
        columnConfiguration.setColumnIndex(174);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanPurpose0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose1");
        columnConfiguration.setColumnIndex(175);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanPurpose1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType0");
        columnConfiguration.setColumnIndex(176);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType1");
        columnConfiguration.setColumnIndex(177);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_loanType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB0");
        columnConfiguration.setColumnIndex(178);
        columnConfiguration.setColumnKey("C_LoanDetails_mob0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_mob0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB1");
        columnConfiguration.setColumnIndex(179);
        columnConfiguration.setColumnKey("C_LoanDetails_mob1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_LoanDetails_mob1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("firstName");
        columnConfiguration.setColumnIndex(180);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_N_firstName999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("lastName");
        columnConfiguration.setColumnIndex(181);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_N_lastName999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("middleName");
        columnConfiguration.setColumnIndex(182);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_N_middleName999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode0");
        columnConfiguration.setColumnIndex(183);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode1");
        columnConfiguration.setColumnIndex(184);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode2");
        columnConfiguration.setColumnIndex(185);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode3");
        columnConfiguration.setColumnIndex(186);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode4");
        columnConfiguration.setColumnIndex(187);
        columnConfiguration.setColumnKey("C_Phone_areaCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode5");
        columnConfiguration.setColumnIndex(188);
        columnConfiguration.setColumnKey("C_Phone_areaCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode6");
        columnConfiguration.setColumnIndex(189);
        columnConfiguration.setColumnKey("C_Phone_areaCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_areaCode6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode0");
        columnConfiguration.setColumnIndex(190);
        columnConfiguration.setColumnKey("C_Phone_countryCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode1");
        columnConfiguration.setColumnIndex(191);
        columnConfiguration.setColumnKey("C_Phone_countryCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode2");
        columnConfiguration.setColumnIndex(192);
        columnConfiguration.setColumnKey("C_Phone_countryCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode3");
        columnConfiguration.setColumnIndex(193);
        columnConfiguration.setColumnKey("C_Phone_countryCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode4");
        columnConfiguration.setColumnIndex(194);
        columnConfiguration.setColumnKey("C_Phone_countryCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode5");
        columnConfiguration.setColumnIndex(195);
        columnConfiguration.setColumnKey("C_Phone_countryCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode6");
        columnConfiguration.setColumnIndex(196);
        columnConfiguration.setColumnKey("C_Phone_countryCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_countryCode6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt0");
        columnConfiguration.setColumnIndex(197);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt1");
        columnConfiguration.setColumnIndex(198);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt2");
        columnConfiguration.setColumnIndex(199);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt3");
        columnConfiguration.setColumnIndex(200);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt4");
        columnConfiguration.setColumnIndex(201);
        columnConfiguration.setColumnKey("C_Phone_extension4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt5");
        columnConfiguration.setColumnIndex(202);
        columnConfiguration.setColumnKey("C_Phone_extension5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt6");
        columnConfiguration.setColumnIndex(203);
        columnConfiguration.setColumnKey("C_Phone_extension6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_extension6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber0");
        columnConfiguration.setColumnIndex(204);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber1");
        columnConfiguration.setColumnIndex(205);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber2");
        columnConfiguration.setColumnIndex(206);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber3");
        columnConfiguration.setColumnIndex(207);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber4");
        columnConfiguration.setColumnIndex(208);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber5");
        columnConfiguration.setColumnIndex(209);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber6");
        columnConfiguration.setColumnIndex(210);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneNumber6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType0");
        columnConfiguration.setColumnIndex(211);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType1");
        columnConfiguration.setColumnIndex(212);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType2");
        columnConfiguration.setColumnIndex(213);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType3");
        columnConfiguration.setColumnIndex(214);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType4");
        columnConfiguration.setColumnIndex(215);
        columnConfiguration.setColumnKey("C_Phone_phoneType4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType5");
        columnConfiguration.setColumnIndex(216);
        columnConfiguration.setColumnKey("C_Phone_phoneType5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType6");
        columnConfiguration.setColumnIndex(217);
        columnConfiguration.setColumnKey("C_Phone_phoneType6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_Phone_phoneType6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("otherSource");
        columnConfiguration.setColumnIndex(218);
        columnConfiguration.setColumnKey("C_otherSourceIncomeAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_otherSourceIncomeAmount999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentStage");
        columnConfiguration.setColumnIndex(219);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("appRequest_currentStageId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(220);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("appRequest_refID999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("responseFormat");
        columnConfiguration.setColumnIndex(221);
        columnConfiguration.setColumnKey("appRequest_responseFormat999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("appRequest_responseFormat999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(222);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(223);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(224);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(225);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId3", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(226);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId4", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(227);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId5", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(228);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId6", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(229);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId7", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(230);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId8", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(231);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("dedup_DedupeDetails_refId9", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSecndoryId");
        columnConfiguration.setColumnIndex(232);
        columnConfiguration.setColumnKey("header_applicationId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_applicationId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("applicationSource");
        columnConfiguration.setColumnIndex(233);
        columnConfiguration.setColumnKey("header_applicationSource999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_applicationSource999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croId");
        columnConfiguration.setColumnIndex(234);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_croId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deDate");
        columnConfiguration.setColumnIndex(235);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_dateTime999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dealerId");
        columnConfiguration.setColumnIndex(236);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_dealerId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dsaId");
        columnConfiguration.setColumnIndex(237);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_dsaId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("institutionId");
        columnConfiguration.setColumnIndex(238);
        columnConfiguration.setColumnKey("header_institutionId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_institutionId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("requestType");
        columnConfiguration.setColumnIndex(239);
        columnConfiguration.setColumnKey("header_requestType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_requestType999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("sourceId");
        columnConfiguration.setColumnIndex(240);
        columnConfiguration.setColumnKey("header_sourceId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("header_sourceId999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("suspiciousActivity");
        columnConfiguration.setColumnIndex(241);
        columnConfiguration.setColumnKey("request_suspiciousActivity999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("request_suspiciousActivity999", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine10");
        columnConfiguration.setColumnIndex(242);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine10", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine11");
        columnConfiguration.setColumnIndex(243);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine11", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine12");
        columnConfiguration.setColumnIndex(244);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine12", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine20");
        columnConfiguration.setColumnIndex(245);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine20", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine21");
        columnConfiguration.setColumnIndex(246);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine21", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine22");
        columnConfiguration.setColumnIndex(247);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_addressLine22", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city0");
        columnConfiguration.setColumnIndex(248);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_city0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city1");
        columnConfiguration.setColumnIndex(249);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_city1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city2");
        columnConfiguration.setColumnIndex(250);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_city2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country0");
        columnConfiguration.setColumnIndex(251);
        columnConfiguration.setColumnKey("C_CustomerAddress_country0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_country0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country1");
        columnConfiguration.setColumnIndex(252);
        columnConfiguration.setColumnKey("C_CustomerAddress_country1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_country1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country2");
        columnConfiguration.setColumnIndex(253);
        columnConfiguration.setColumnKey("C_CustomerAddress_country2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_country2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress0");
        columnConfiguration.setColumnIndex(254);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_distanceFrom0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress1");
        columnConfiguration.setColumnIndex(255);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_distanceFrom1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress2");
        columnConfiguration.setColumnIndex(256);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_distanceFrom2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark0");
        columnConfiguration.setColumnIndex(257);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_landMark0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark1");
        columnConfiguration.setColumnIndex(258);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_landMark1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark2");
        columnConfiguration.setColumnIndex(259);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_landMark2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(260);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_line30", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(261);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_line31", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(262);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_line32", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode0");
        columnConfiguration.setColumnIndex(263);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_pin0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode1");
        columnConfiguration.setColumnIndex(264);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_pin1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode2");
        columnConfiguration.setColumnIndex(265);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_pin2", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state0");
        columnConfiguration.setColumnIndex(266);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_state0", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state1");
        columnConfiguration.setColumnIndex(267);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_state1", columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state2");
        columnConfiguration.setColumnIndex(268);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        fieldKeyMap.put("C_CustomerAddress_state2", columnConfiguration);

    }

    static {

        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability0");
        columnConfiguration.setColumnIndex(0);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_addStability0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability1");
        columnConfiguration.setColumnIndex(1);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability2");
        columnConfiguration.setColumnIndex(2);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability3");
        columnConfiguration.setColumnIndex(3);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability4");
        columnConfiguration.setColumnIndex(4);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressStability5");
        columnConfiguration.setColumnIndex(5);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName0");
        columnConfiguration.setColumnIndex(6);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName1");
        columnConfiguration.setColumnIndex(7);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName2");
        columnConfiguration.setColumnIndex(8);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName3");
        columnConfiguration.setColumnIndex(9);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName4");
        columnConfiguration.setColumnIndex(10);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldName5");
        columnConfiguration.setColumnIndex(11);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue0");
        columnConfiguration.setColumnIndex(12);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue1");
        columnConfiguration.setColumnIndex(13);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue2");
        columnConfiguration.setColumnIndex(14);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue3");
        columnConfiguration.setColumnIndex(15);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue4");
        columnConfiguration.setColumnIndex(16);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldValue5");
        columnConfiguration.setColumnIndex(17);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage0");
        columnConfiguration.setColumnIndex(18);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage1");
        columnConfiguration.setColumnIndex(19);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage2");
        columnConfiguration.setColumnIndex(20);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage3");
        columnConfiguration.setColumnIndex(21);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage4");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("scoringMessage5");
        columnConfiguration.setColumnIndex(23);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore0");
        columnConfiguration.setColumnIndex(24);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("namScore1");
        columnConfiguration.setColumnIndex(25);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder0");
        columnConfiguration.setColumnIndex(26);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder1");
        columnConfiguration.setColumnIndex(27);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder2");
        columnConfiguration.setColumnIndex(28);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder3");
        columnConfiguration.setColumnIndex(29);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder4");
        columnConfiguration.setColumnIndex(30);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("fieldOrder5");
        columnConfiguration.setColumnIndex(31);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_order5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appStatus");
        columnConfiguration.setColumnIndex(32);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSubmitDate");
        columnConfiguration.setColumnIndex(33);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("refId");
        columnConfiguration.setColumnIndex(34);
        columnConfiguration.setColumnKey("A_gngRefId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("negativePincode");
        columnConfiguration.setColumnIndex(35);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfRetry");
        columnConfiguration.setColumnIndex(36);
        columnConfiguration.setColumnKey("A_noOfReTry999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(37);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croApprAmt0");
        columnConfiguration.setColumnIndex(38);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("apprDate0");
        columnConfiguration.setColumnIndex(39);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("downPayment0");
        columnConfiguration.setColumnIndex(40);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(41);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi0");
        columnConfiguration.setColumnIndex(42);
        columnConfiguration.setColumnKey("CRO_CroDecision_emi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("interestRate0");
        columnConfiguration.setColumnIndex(43);
        columnConfiguration.setColumnKey("CRO_CroDecision_interestRate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("tenor0");
        columnConfiguration.setColumnIndex(44);
        columnConfiguration.setColumnKey("CRO_CroDecision_tenor0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("aadharVerFlag");
        columnConfiguration.setColumnIndex(45);
        columnConfiguration.setColumnKey("C_A_addharVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("age");
        columnConfiguration.setColumnIndex(46);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appId");
        columnConfiguration.setColumnIndex(47);
        columnConfiguration.setColumnKey("C_A_applicantId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("cardNumber");
        columnConfiguration.setColumnIndex(48);
        columnConfiguration.setColumnKey("C_A_creditCardNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(49);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eduction");
        columnConfiguration.setColumnIndex(50);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("gender");
        columnConfiguration.setColumnIndex(51);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressFlag");
        columnConfiguration.setColumnIndex(52);
        columnConfiguration.setColumnKey("C_A_isResidenceAddSameAsAbove999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("maritalStatus");
        columnConfiguration.setColumnIndex(53);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("mobileVer");
        columnConfiguration.setColumnIndex(54);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfDependent");
        columnConfiguration.setColumnIndex(55);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfEarningMemb");
        columnConfiguration.setColumnIndex(56);
        columnConfiguration.setColumnKey("C_A_noOfEarningMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("noOfFamilyMemb");
        columnConfiguration.setColumnIndex(57);
        columnConfiguration.setColumnKey("C_A_noOfFamilyMembers999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber0");
        columnConfiguration.setColumnIndex(58);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankActNumber1");
        columnConfiguration.setColumnIndex(59);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType0");
        columnConfiguration.setColumnIndex(60);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankAccountType1");
        columnConfiguration.setColumnIndex(61);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails0");
        columnConfiguration.setColumnIndex(62);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emiDetails1");
        columnConfiguration.setColumnIndex(63);
        columnConfiguration.setColumnKey("C_BankingDetails_anyEmi1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals0");
        columnConfiguration.setColumnIndex(64);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("avgBankBals1");
        columnConfiguration.setColumnIndex(65);
        columnConfiguration.setColumnKey("C_BankingDetails_avgBankBalance1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(66);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(67);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName0");
        columnConfiguration.setColumnIndex(68);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankName1");
        columnConfiguration.setColumnIndex(69);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails0");
        columnConfiguration.setColumnIndex(70);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentLoanDetails1");
        columnConfiguration.setColumnIndex(71);
        columnConfiguration
                .setColumnKey("C_BankingDetails_currentlyRunningLoan1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi0");
        columnConfiguration.setColumnIndex(72);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deductionEmi1");
        columnConfiguration.setColumnIndex(73);
        columnConfiguration.setColumnKey("C_BankingDetails_deductedEmiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount0");
        columnConfiguration.setColumnIndex(74);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("bankMensionAmount1");
        columnConfiguration.setColumnIndex(75);
        columnConfiguration.setColumnKey("C_BankingDetails_mentionAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct0");
        columnConfiguration.setColumnIndex(76);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("salaryAcct1");
        columnConfiguration.setColumnIndex(77);
        columnConfiguration.setColumnKey("C_BankingDetails_salaryAccount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType0");
        columnConfiguration.setColumnIndex(78);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType1");
        columnConfiguration.setColumnIndex(79);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType2");
        columnConfiguration.setColumnIndex(80);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressType3");
        columnConfiguration.setColumnIndex(81);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress0");
        columnConfiguration.setColumnIndex(82);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress1");
        columnConfiguration.setColumnIndex(83);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress2");
        columnConfiguration.setColumnIndex(84);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtAddress3");
        columnConfiguration.setColumnIndex(85);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity0");
        columnConfiguration.setColumnIndex(86);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity1");
        columnConfiguration.setColumnIndex(87);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity2");
        columnConfiguration.setColumnIndex(88);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthAtCity3");
        columnConfiguration.setColumnIndex(89);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount0");
        columnConfiguration.setColumnIndex(90);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount1");
        columnConfiguration.setColumnIndex(91);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount2");
        columnConfiguration.setColumnIndex(92);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("rentAmmount3");
        columnConfiguration.setColumnIndex(93);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType0");
        columnConfiguration.setColumnIndex(94);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType1");
        columnConfiguration.setColumnIndex(95);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType2");
        columnConfiguration.setColumnIndex(96);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("resAddressType3");
        columnConfiguration.setColumnIndex(97);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres0");
        columnConfiguration.setColumnIndex(98);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres1");
        columnConfiguration.setColumnIndex(99);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres2");
        columnConfiguration.setColumnIndex(100);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeAtAddres3");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_CustomerAddress_timeAtAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity0");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity1");
        columnConfiguration.setColumnIndex(103);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity2");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("yrsAtCity3");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_CustomerAddress_yearAtCity3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress0");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress1");
        columnConfiguration.setColumnIndex(107);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress2");
        columnConfiguration.setColumnIndex(108);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailAddress3");
        columnConfiguration.setColumnIndex(109);
        columnConfiguration.setColumnKey("C_Email_emailAddress3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType0");
        columnConfiguration.setColumnIndex(110);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType1");
        columnConfiguration.setColumnIndex(111);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType2");
        columnConfiguration.setColumnIndex(112);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emailType3");
        columnConfiguration.setColumnIndex(113);
        columnConfiguration.setColumnKey("C_Email_emailType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution0");
        columnConfiguration.setColumnIndex(114);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employContitution1");
        columnConfiguration.setColumnIndex(115);
        columnConfiguration.setColumnKey("C_Employment_constitution1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName0");
        columnConfiguration.setColumnIndex(116);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerName1");
        columnConfiguration.setColumnIndex(117);
        columnConfiguration.setColumnKey("C_Employment_employmentName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType0");
        columnConfiguration.setColumnIndex(118);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("employerType1");
        columnConfiguration.setColumnIndex(119);
        columnConfiguration.setColumnKey("C_Employment_employmentType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary0");
        columnConfiguration.setColumnIndex(120);
        columnConfiguration.setColumnKey("C_Employment_grossSalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("grossSalary1");
        columnConfiguration.setColumnIndex(121);
        columnConfiguration.setColumnKey("C_Employment_grossSalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType0");
        columnConfiguration.setColumnIndex(122);
        columnConfiguration.setColumnKey("C_Employment_industryType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("industryType1");
        columnConfiguration.setColumnIndex(123);
        columnConfiguration.setColumnKey("C_Employment_industryType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt0");
        columnConfiguration.setColumnIndex(124);
        columnConfiguration.setColumnKey("C_Employment_itrAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("itrAmt1");
        columnConfiguration.setColumnIndex(125);
        columnConfiguration.setColumnKey("C_Employment_itrAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment0");
        columnConfiguration.setColumnIndex(126);
        columnConfiguration.setColumnKey("C_Employment_modePayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("modeOfPayment1");
        columnConfiguration.setColumnIndex(127);
        columnConfiguration.setColumnKey("C_Employment_modePayment1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("monthlySalary1");
        columnConfiguration.setColumnIndex(129);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer0");
        columnConfiguration.setColumnIndex(130);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("timeWithEmployer1");
        columnConfiguration.setColumnIndex(131);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince0");
        columnConfiguration.setColumnIndex(132);
        columnConfiguration.setColumnKey("C_Employment_workExps0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("workExperince1");
        columnConfiguration.setColumnIndex(133);
        columnConfiguration.setColumnKey("C_Employment_workExps1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName0");
        columnConfiguration.setColumnIndex(134);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName1");
        columnConfiguration.setColumnIndex(135);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName2");
        columnConfiguration.setColumnIndex(136);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName3");
        columnConfiguration.setColumnIndex(137);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName4");
        columnConfiguration.setColumnIndex(138);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName5");
        columnConfiguration.setColumnIndex(139);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycName6");
        columnConfiguration.setColumnIndex(140);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber0");
        columnConfiguration.setColumnIndex(141);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber1");
        columnConfiguration.setColumnIndex(142);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber2");
        columnConfiguration.setColumnIndex(143);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber3");
        columnConfiguration.setColumnIndex(144);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber4");
        columnConfiguration.setColumnIndex(145);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber5");
        columnConfiguration.setColumnIndex(146);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("kycNumber6");
        columnConfiguration.setColumnIndex(147);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg0");
        columnConfiguration.setColumnIndex(148);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetCtg1");
        columnConfiguration.setColumnIndex(149);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake0");
        columnConfiguration.setColumnIndex(150);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetMake1");
        columnConfiguration.setColumnIndex(151);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName0");
        columnConfiguration.setColumnIndex(152);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetDealerName1");
        columnConfiguration.setColumnIndex(153);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo0");
        columnConfiguration.setColumnIndex(154);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetModelNo1");
        columnConfiguration.setColumnIndex(155);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice0");
        columnConfiguration.setColumnIndex(156);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("assetPrice1");
        columnConfiguration.setColumnIndex(157);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(158);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt");
        columnConfiguration.setColumnIndex(159);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr");
        columnConfiguration.setColumnIndex(160);
        columnConfiguration.setColumnKey("C_L_A_loanApr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose");
        columnConfiguration.setColumnIndex(161);
        columnConfiguration.setColumnKey("C_L_A_loanPurpose999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanTenor");
        columnConfiguration.setColumnIndex(162);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType");
        columnConfiguration.setColumnIndex(163);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("marginAmt");
        columnConfiguration.setColumnIndex(164);
        columnConfiguration.setColumnKey("C_L_A_marginAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("numberOfAdvEmi");
        columnConfiguration.setColumnIndex(165);
        columnConfiguration.setColumnKey("C_L_A_numberOfAdvanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(166);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanBalsTenor0");
        columnConfiguration.setColumnIndex(167);
        columnConfiguration.setColumnKey("C_LoanDetails_balanceTenure0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi0");
        columnConfiguration.setColumnIndex(168);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanEmi1");
        columnConfiguration.setColumnIndex(169);
        columnConfiguration.setColumnKey("C_LoanDetails_emiAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt0");
        columnConfiguration.setColumnIndex(170);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanAmt1");
        columnConfiguration.setColumnIndex(171);
        columnConfiguration.setColumnKey("C_LoanDetails_loanAmt1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr0");
        columnConfiguration.setColumnIndex(172);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanApr1");
        columnConfiguration.setColumnIndex(173);
        columnConfiguration.setColumnKey("C_LoanDetails_loanApr1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose0");
        columnConfiguration.setColumnIndex(174);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanPurpose1");
        columnConfiguration.setColumnIndex(175);
        columnConfiguration.setColumnKey("C_LoanDetails_loanPurpose1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType0");
        columnConfiguration.setColumnIndex(176);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanType1");
        columnConfiguration.setColumnIndex(177);
        columnConfiguration.setColumnKey("C_LoanDetails_loanType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB0");
        columnConfiguration.setColumnIndex(178);
        columnConfiguration.setColumnKey("C_LoanDetails_mob0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("loanMOB1");
        columnConfiguration.setColumnIndex(179);
        columnConfiguration.setColumnKey("C_LoanDetails_mob1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("firstName");
        columnConfiguration.setColumnIndex(180);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("lastName");
        columnConfiguration.setColumnIndex(181);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("middleName");
        columnConfiguration.setColumnIndex(182);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode0");
        columnConfiguration.setColumnIndex(183);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode1");
        columnConfiguration.setColumnIndex(184);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode2");
        columnConfiguration.setColumnIndex(185);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode3");
        columnConfiguration.setColumnIndex(186);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode4");
        columnConfiguration.setColumnIndex(187);
        columnConfiguration.setColumnKey("C_Phone_areaCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode5");
        columnConfiguration.setColumnIndex(188);
        columnConfiguration.setColumnKey("C_Phone_areaCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phAreaCode6");
        columnConfiguration.setColumnIndex(189);
        columnConfiguration.setColumnKey("C_Phone_areaCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode0");
        columnConfiguration.setColumnIndex(190);
        columnConfiguration.setColumnKey("C_Phone_countryCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode1");
        columnConfiguration.setColumnIndex(191);
        columnConfiguration.setColumnKey("C_Phone_countryCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode2");
        columnConfiguration.setColumnIndex(192);
        columnConfiguration.setColumnKey("C_Phone_countryCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode3");
        columnConfiguration.setColumnIndex(193);
        columnConfiguration.setColumnKey("C_Phone_countryCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode4");
        columnConfiguration.setColumnIndex(194);
        columnConfiguration.setColumnKey("C_Phone_countryCode4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode5");
        columnConfiguration.setColumnIndex(195);
        columnConfiguration.setColumnKey("C_Phone_countryCode5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phCountryCode6");
        columnConfiguration.setColumnIndex(196);
        columnConfiguration.setColumnKey("C_Phone_countryCode6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt0");
        columnConfiguration.setColumnIndex(197);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt1");
        columnConfiguration.setColumnIndex(198);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt2");
        columnConfiguration.setColumnIndex(199);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt3");
        columnConfiguration.setColumnIndex(200);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt4");
        columnConfiguration.setColumnIndex(201);
        columnConfiguration.setColumnKey("C_Phone_extension4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt5");
        columnConfiguration.setColumnIndex(202);
        columnConfiguration.setColumnKey("C_Phone_extension5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phExt6");
        columnConfiguration.setColumnIndex(203);
        columnConfiguration.setColumnKey("C_Phone_extension6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber0");
        columnConfiguration.setColumnIndex(204);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber1");
        columnConfiguration.setColumnIndex(205);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber2");
        columnConfiguration.setColumnIndex(206);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber3");
        columnConfiguration.setColumnIndex(207);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber4");
        columnConfiguration.setColumnIndex(208);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber5");
        columnConfiguration.setColumnIndex(209);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phNumber6");
        columnConfiguration.setColumnIndex(210);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType0");
        columnConfiguration.setColumnIndex(211);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType1");
        columnConfiguration.setColumnIndex(212);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType2");
        columnConfiguration.setColumnIndex(213);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType3");
        columnConfiguration.setColumnIndex(214);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType4");
        columnConfiguration.setColumnIndex(215);
        columnConfiguration.setColumnKey("C_Phone_phoneType4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType5");
        columnConfiguration.setColumnIndex(216);
        columnConfiguration.setColumnKey("C_Phone_phoneType5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("phType6");
        columnConfiguration.setColumnIndex(217);
        columnConfiguration.setColumnKey("C_Phone_phoneType6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("otherSource");
        columnConfiguration.setColumnIndex(218);
        columnConfiguration.setColumnKey("C_otherSourceIncomeAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("currentStage");
        columnConfiguration.setColumnIndex(219);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appId");
        columnConfiguration.setColumnIndex(220);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("responseFormat");
        columnConfiguration.setColumnIndex(221);
        columnConfiguration.setColumnKey("appRequest_responseFormat999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId0");
        columnConfiguration.setColumnIndex(222);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId1");
        columnConfiguration.setColumnIndex(223);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId2");
        columnConfiguration.setColumnIndex(224);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId3");
        columnConfiguration.setColumnIndex(225);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId4");
        columnConfiguration.setColumnIndex(226);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId5");
        columnConfiguration.setColumnIndex(227);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId6");
        columnConfiguration.setColumnIndex(228);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId6");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId7");
        columnConfiguration.setColumnIndex(229);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId7");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId8");
        columnConfiguration.setColumnIndex(230);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId8");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dedupeRefId9");
        columnConfiguration.setColumnIndex(231);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId9");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("appSecndoryId");
        columnConfiguration.setColumnIndex(232);
        columnConfiguration.setColumnKey("header_applicationId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("applicationSource");
        columnConfiguration.setColumnIndex(233);
        columnConfiguration.setColumnKey("header_applicationSource999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("croId");
        columnConfiguration.setColumnIndex(234);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("deDate");
        columnConfiguration.setColumnIndex(235);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dealerId");
        columnConfiguration.setColumnIndex(236);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("dsaId");
        columnConfiguration.setColumnIndex(237);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("institutionId");
        columnConfiguration.setColumnIndex(238);
        columnConfiguration.setColumnKey("header_institutionId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("requestType");
        columnConfiguration.setColumnIndex(239);
        columnConfiguration.setColumnKey("header_requestType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("sourceId");
        columnConfiguration.setColumnIndex(240);
        columnConfiguration.setColumnKey("header_sourceId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("suspiciousActivity");
        columnConfiguration.setColumnIndex(241);
        columnConfiguration.setColumnKey("request_suspiciousActivity999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine10");
        columnConfiguration.setColumnIndex(242);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine11");
        columnConfiguration.setColumnIndex(243);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine12");
        columnConfiguration.setColumnIndex(244);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine20");
        columnConfiguration.setColumnIndex(245);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine21");
        columnConfiguration.setColumnIndex(246);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine22");
        columnConfiguration.setColumnIndex(247);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city0");
        columnConfiguration.setColumnIndex(248);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city1");
        columnConfiguration.setColumnIndex(249);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("city2");
        columnConfiguration.setColumnIndex(250);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country0");
        columnConfiguration.setColumnIndex(251);
        columnConfiguration.setColumnKey("C_CustomerAddress_country0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country1");
        columnConfiguration.setColumnIndex(252);
        columnConfiguration.setColumnKey("C_CustomerAddress_country1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("country2");
        columnConfiguration.setColumnIndex(253);
        columnConfiguration.setColumnKey("C_CustomerAddress_country2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress0");
        columnConfiguration.setColumnIndex(254);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress1");
        columnConfiguration.setColumnIndex(255);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("distFromAddress2");
        columnConfiguration.setColumnIndex(256);
        columnConfiguration.setColumnKey("C_CustomerAddress_distanceFrom2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark0");
        columnConfiguration.setColumnIndex(257);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark1");
        columnConfiguration.setColumnIndex(258);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("landMark2");
        columnConfiguration.setColumnIndex(259);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(260);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(261);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("addressLine31");
        columnConfiguration.setColumnIndex(262);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode0");
        columnConfiguration.setColumnIndex(263);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode1");
        columnConfiguration.setColumnIndex(264);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("pincode2");
        columnConfiguration.setColumnIndex(265);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state0");
        columnConfiguration.setColumnIndex(266);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state1");
        columnConfiguration.setColumnIndex(267);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("state2");
        columnConfiguration.setColumnIndex(268);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        sysFieldList.add(columnConfiguration);

    }

    public static FlatReportConfiguration getConFigurationV2CSV() {
        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();
        flatReprtConfiguration
                .setHeaderMap(FlatConfig.defaultConfigMapVersion2);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("GNGVersion2");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");
        return flatReprtConfiguration;
    }
}
