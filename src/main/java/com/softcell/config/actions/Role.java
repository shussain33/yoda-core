package com.softcell.config.actions;

/**
 * @author prateek
 */
public class Role {

    private int id;

    private String role;

    private String roleDescription;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Role [id=");
        builder.append(id);
        builder.append(", role=");
        builder.append(role);
        builder.append(", roleDescription=");
        builder.append(roleDescription);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result
                + ((roleDescription == null) ? 0 : roleDescription.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role other = (Role) obj;
        if (id != other.id)
            return false;
        if (role == null) {
            if (other.role != null)
                return false;
        } else if (!role.equals(other.role))
            return false;
        if (roleDescription == null) {
            if (other.roleDescription != null)
                return false;
        } else if (!roleDescription.equals(other.roleDescription))
            return false;
        return true;
    }


}
