package com.softcell.config;

/**
 * @author kishor
 */
public class ReAppraisalConfig {
    /**
     * <em>institutionId</em>
     * This will  used to define for particular institution and
     * unique, Which  will  map to UAT to production.
     */
    private String institutionId;
    /**
     * The <em>productType<em> is filter for product level.
     * and used differentiate in particular institution.
     */
    private String productType;
    /**
     * <em>allowedReTry<em> is number of try to reappraisal or re-initiate.
     */
    private int allowedReTry;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getAllowedReTry() {
        return allowedReTry;
    }

    public void setAllowedReTry(int allowedReTry) {
        this.allowedReTry = allowedReTry;
    }

}
