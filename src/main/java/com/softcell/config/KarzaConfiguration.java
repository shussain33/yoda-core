package com.softcell.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogesh on 6/10/18.
 */

@Document(collection = "karzaConfiguration")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KarzaConfiguration {

    private String institutionId;

    private String institutionName;

    private String productName;

    private String password;

    private String loginId;

    private String consent;

    private String karzaUrl;

    private boolean active;

    private int karzaRetryCount;

    private Date insertDate;

    private Date lastUpdateDate;
}