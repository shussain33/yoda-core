package com.softcell.config;

/**
 * This is to keep app for reliance DMZ
 *
 * @author bhuvneshk
 */
public class RelianceConfiguration {


    private String url;

    private String baseUrl;

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private RelianceConfiguration relianceConfiguration = new RelianceConfiguration();

        public RelianceConfiguration build(){
            return this.relianceConfiguration;
        }

        public Builder url(String url){
            this.relianceConfiguration.setUrl(url);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.relianceConfiguration.setBaseUrl(baseUrl);
            return this;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RelianceConfiguration [url=");
        builder.append(url);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof RelianceConfiguration))
            return false;
        RelianceConfiguration other = (RelianceConfiguration) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }

}
