package com.softcell.config;

import java.util.Date;

/**
 * @author kishorp
 */
public class ActionConfiguration {

    private String institutionId;

    private boolean demoAction;

    private Date insertDate;

    private String institutionName;

    private String product;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isDemoAction() {
        return demoAction;
    }

    public void setDemoAction(boolean isDemoAction) {
        this.demoAction = isDemoAction;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

}
