package com.softcell.config;

import com.softcell.gonogo.model.multibureau.pickup.MbCommunicationDomain;
import com.softcell.queuemanager.config.EsConnectionConfig;

import java.util.List;
import java.util.Map;

/**
 * @author bhuvneshk
 */

public class URLConfiguration {

    private Map<String, MbCommunicationDomain> mbCommunicationDomainConfig;
    private Map<String, MbCommunicationDomain> mbCorpCommunicationDomainConfig;
    private Map<String, PanCommunicationDomain> panCommunicationDomainConfig;
    private Map<String, ScoringCommunication> scoringCommunicationConfig;
    private Map<String, OtpConfiguration> otpConfiguration;
    private Map<String, EmailServiceConfiguration> emailServiceConfiguration;
    private Map<String, EmailServiceConfiguration> emailServiceConfigurationForPlAndCcbt;
    private Map<String, String> dealerEmailMapping;
    private Map<String, SalesForceConfiguration> salesForceConfiguration;
    private Map<String, AadharConfiguration> aadharConfiguration;
    private Map<String, AuthenticationConfiguration> authenticationConfiguration;
    private Map<String, AmazonS3Configuration> amazonS3Configuration;
    private Map<String, EsConnectionConfig> esConnectionConfig;
    private Map<String, NTCConfiguration> ntcConfiguration;
    private Map<String, CreditCardSurrogateConfig> creditCardSurrogateConfig;
    private Map<String, RelianceConfiguration> relianceConfiguration;
    private Map<String, LosConfiguration> losConfiguration;
    private Map<String, SmsServiceConfiguration> smsServiceConfiguration;
    private Map<String,EmandateCredentials> emandateCredentialsConfig;

    public Map<String, EmandateCredentials> getEmandateCredentialsConfig() {
        return emandateCredentialsConfig;
    }

    public void setEmandateCredentialsConfig(Map<String, EmandateCredentials> emandateCredentialsConfig) {
        this.emandateCredentialsConfig = emandateCredentialsConfig;
    }

    public Map<String, CreditCardSurrogateConfig> getCreditCardSurrogateConfig() {
        return creditCardSurrogateConfig;
    }

    public void setCreditCardSurrogateConfig(
            Map<String, CreditCardSurrogateConfig> creditCardSurrogateConfig) {
        this.creditCardSurrogateConfig = creditCardSurrogateConfig;
    }

    public Map<String, EmailServiceConfiguration> getEmailServiceConfigurationForPlAndCcbt() {
        return emailServiceConfigurationForPlAndCcbt;
    }

    public void setEmailServiceConfigurationForPlAndCcbt(
            Map<String, EmailServiceConfiguration> emailServiceConfigurationForPlAndCcbt) {
        this.emailServiceConfigurationForPlAndCcbt = emailServiceConfigurationForPlAndCcbt;
    }

    public Map<String, EsConnectionConfig> getEsConnectionConfig() {
        return esConnectionConfig;
    }

    public void setEsConnectionConfig(
            Map<String, EsConnectionConfig> esConnectionConfig) {
        this.esConnectionConfig = esConnectionConfig;
    }

    public Map<String, String> getDealerEmailMapping() {
        return dealerEmailMapping;
    }

    public void setDealerEmailMapping(Map<String, String> dealerEmailMapping) {
        this.dealerEmailMapping = dealerEmailMapping;
    }

    public Map<String, MbCommunicationDomain> getMbCommunicationDomainConfig() {
        return mbCommunicationDomainConfig;
    }

    public void setMbCommunicationDomainConfig(
            Map<String, MbCommunicationDomain> mbCommunicationDomainConfig) {
        this.mbCommunicationDomainConfig = mbCommunicationDomainConfig;
    }

    public Map<String, PanCommunicationDomain> getPanCommunicationDomainConfig() {
        return panCommunicationDomainConfig;
    }

    public void setPanCommunicationDomainConfig(
            Map<String, PanCommunicationDomain> panCommunicationDomainConfig) {
        this.panCommunicationDomainConfig = panCommunicationDomainConfig;
    }

    public Map<String, ScoringCommunication> getScoringCommunicationConfig() {
        return scoringCommunicationConfig;
    }

    public void setScoringCommunicationConfig(
            Map<String, ScoringCommunication> scoringCommunicationConfig) {
        this.scoringCommunicationConfig = scoringCommunicationConfig;
    }

    public Map<String, OtpConfiguration> getOtpConfiguration() {
        return otpConfiguration;
    }

    public void setOtpConfiguration(
            Map<String, OtpConfiguration> otpConfiguration) {
        this.otpConfiguration = otpConfiguration;
    }

    public Map<String, EmailServiceConfiguration> getEmailServiceConfiguration() {
        return emailServiceConfiguration;
    }

    public void setEmailServiceConfiguration(
            Map<String, EmailServiceConfiguration> emailServiceConfiguration) {
        this.emailServiceConfiguration = emailServiceConfiguration;
    }

    /**
     * @return the salesForceConfiguration
     */
    public Map<String, SalesForceConfiguration> getSalesForceConfiguration() {
        return salesForceConfiguration;
    }

    /**
     * @param salesForceConfiguration the salesForceConfiguration to set
     */
    public void setSalesForceConfiguration(
            Map<String, SalesForceConfiguration> salesForceConfiguration) {
        this.salesForceConfiguration = salesForceConfiguration;
    }

    /**
     * @return the aadharConfiguration
     */
    public Map<String, AadharConfiguration> getAadharConfiguration() {
        return aadharConfiguration;
    }

    /**
     * @param aadharConfiguration the aadharConfiguration to set
     */
    public void setAadharConfiguration(
            Map<String, AadharConfiguration> aadharConfiguration) {
        this.aadharConfiguration = aadharConfiguration;
    }


    /**
     * @return the authenticationConfiguration
     */
    public Map<String, AuthenticationConfiguration> getAuthenticationConfiguration() {
        return authenticationConfiguration;
    }

    /**
     * @param authenticationConfiguration the authenticationConfiguration to set
     */
    public void setAuthenticationConfiguration(
            Map<String, AuthenticationConfiguration> authenticationConfiguration) {
        this.authenticationConfiguration = authenticationConfiguration;
    }

    /**
     * @return the amazonS3Configuration
     */
    public Map<String, AmazonS3Configuration> getAmazonS3Configuration() {
        return amazonS3Configuration;
    }

    /**
     * @param amazonS3Configuration the amazonS3Configuration to set
     */
    public void setAmazonS3Configuration(
            Map<String, AmazonS3Configuration> amazonS3Configuration) {
        this.amazonS3Configuration = amazonS3Configuration;
    }


    /**
     * @return the ntcConfiguration
     */
    public Map<String, NTCConfiguration> getNtcConfiguration() {
        return ntcConfiguration;
    }


    /**
     * @param ntcConfiguration the ntcConfiguration to set
     */
    public void setNtcConfiguration(Map<String, NTCConfiguration> ntcConfiguration) {
        this.ntcConfiguration = ntcConfiguration;
    }


    /**
     * @return the relianceConfiguration
     */
    public Map<String, RelianceConfiguration> getRelianceConfiguration() {
        return relianceConfiguration;
    }

    /**
     * @param relianceConfiguration the relianceConfiguration to set
     */
    public void setRelianceConfiguration(Map<String, RelianceConfiguration> relianceConfiguration) {
        this.relianceConfiguration = relianceConfiguration;
    }


    /**
     * @return the losConfiguration
     */
    public Map<String, LosConfiguration> getLosConfiguration() {
        return losConfiguration;
    }

    /**
     * @param losConfiguration the losConfiguration to set
     */
    public void setLosConfiguration(Map<String, LosConfiguration> losConfiguration) {
        this.losConfiguration = losConfiguration;
    }

    /**
     * @return the smsServiceConfiguration
     */
    public Map<String, SmsServiceConfiguration> getSmsServiceConfiguration() {
        return smsServiceConfiguration;
    }

    /**
     * @param smsServiceConfiguration the smsServiceConfiguration to set
     */
    public void setSmsServiceConfiguration(
            Map<String, SmsServiceConfiguration> smsServiceConfiguration) {
        this.smsServiceConfiguration = smsServiceConfiguration;
    }

    public Map<String, MbCommunicationDomain> getMbCorpCommunicationDomainConfig() {
        return mbCorpCommunicationDomainConfig;
    }

    public void setMbCorpCommunicationDomainConfig(Map<String, MbCommunicationDomain> mbCorpCommunicationDomainConfig) {
        this.mbCorpCommunicationDomainConfig = mbCorpCommunicationDomainConfig;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("URLConfiguration [mbCommunicationDomainConfig=");
        builder.append(mbCommunicationDomainConfig);
        builder.append(", panCommunicationDomainConfig=");
        builder.append(panCommunicationDomainConfig);
        builder.append(", scoringCommunicationConfig=");
        builder.append(scoringCommunicationConfig);
        builder.append(", otpConfiguration=");
        builder.append(otpConfiguration);
        builder.append(", emailServiceConfiguration=");
        builder.append(emailServiceConfiguration);
        builder.append(", emailServiceConfigurationForPlAndCcbt=");
        builder.append(emailServiceConfigurationForPlAndCcbt);
        builder.append(", dealerEmailMapping=");
        builder.append(dealerEmailMapping);
        builder.append(", salesForceConfiguration=");
        builder.append(salesForceConfiguration);
        builder.append(", aadharConfiguration=");
        builder.append(aadharConfiguration);
        builder.append(", authenticationConfiguration=");
        builder.append(authenticationConfiguration);
        builder.append(", amazonS3Configuration=");
        builder.append(amazonS3Configuration);
        builder.append(", esConnectionConfig=");
        builder.append(esConnectionConfig);
        builder.append(", ntcConfiguration=");
        builder.append(ntcConfiguration);
        builder.append(", creditCardSurrogateConfig=");
        builder.append(creditCardSurrogateConfig);
        builder.append(", relianceConfiguration=");
        builder.append(relianceConfiguration);
        builder.append(", losConfiguration=");
        builder.append(losConfiguration);
        builder.append(", smsServiceConfiguration=");
        builder.append(smsServiceConfiguration);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((aadharConfiguration == null) ? 0 : aadharConfiguration
                .hashCode());
        result = prime
                * result
                + ((amazonS3Configuration == null) ? 0 : amazonS3Configuration
                .hashCode());
        result = prime
                * result
                + ((authenticationConfiguration == null) ? 0
                : authenticationConfiguration.hashCode());
        result = prime
                * result
                + ((creditCardSurrogateConfig == null) ? 0
                : creditCardSurrogateConfig.hashCode());
        result = prime
                * result
                + ((dealerEmailMapping == null) ? 0 : dealerEmailMapping
                .hashCode());
        result = prime
                * result
                + ((emailServiceConfiguration == null) ? 0
                : emailServiceConfiguration.hashCode());
        result = prime
                * result
                + ((emailServiceConfigurationForPlAndCcbt == null) ? 0
                : emailServiceConfigurationForPlAndCcbt.hashCode());
        result = prime
                * result
                + ((esConnectionConfig == null) ? 0 : esConnectionConfig
                .hashCode());
        result = prime
                * result
                + ((losConfiguration == null) ? 0 : losConfiguration.hashCode());
        result = prime
                * result
                + ((mbCommunicationDomainConfig == null) ? 0
                : mbCommunicationDomainConfig.hashCode());
        result = prime
                * result
                + ((ntcConfiguration == null) ? 0 : ntcConfiguration.hashCode());
        result = prime
                * result
                + ((otpConfiguration == null) ? 0 : otpConfiguration.hashCode());
        result = prime
                * result
                + ((panCommunicationDomainConfig == null) ? 0
                : panCommunicationDomainConfig.hashCode());
        result = prime
                * result
                + ((relianceConfiguration == null) ? 0 : relianceConfiguration
                .hashCode());
        result = prime
                * result
                + ((salesForceConfiguration == null) ? 0
                : salesForceConfiguration.hashCode());
        result = prime
                * result
                + ((scoringCommunicationConfig == null) ? 0
                : scoringCommunicationConfig.hashCode());
        result = prime
                * result
                + ((smsServiceConfiguration == null) ? 0
                : smsServiceConfiguration.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        URLConfiguration other = (URLConfiguration) obj;
        if (aadharConfiguration == null) {
            if (other.aadharConfiguration != null)
                return false;
        } else if (!aadharConfiguration.equals(other.aadharConfiguration))
            return false;
        if (amazonS3Configuration == null) {
            if (other.amazonS3Configuration != null)
                return false;
        } else if (!amazonS3Configuration.equals(other.amazonS3Configuration))
            return false;
        if (authenticationConfiguration == null) {
            if (other.authenticationConfiguration != null)
                return false;
        } else if (!authenticationConfiguration
                .equals(other.authenticationConfiguration))
            return false;
        if (creditCardSurrogateConfig == null) {
            if (other.creditCardSurrogateConfig != null)
                return false;
        } else if (!creditCardSurrogateConfig
                .equals(other.creditCardSurrogateConfig))
            return false;
        if (dealerEmailMapping == null) {
            if (other.dealerEmailMapping != null)
                return false;
        } else if (!dealerEmailMapping.equals(other.dealerEmailMapping))
            return false;
        if (emailServiceConfiguration == null) {
            if (other.emailServiceConfiguration != null)
                return false;
        } else if (!emailServiceConfiguration
                .equals(other.emailServiceConfiguration))
            return false;
        if (emailServiceConfigurationForPlAndCcbt == null) {
            if (other.emailServiceConfigurationForPlAndCcbt != null)
                return false;
        } else if (!emailServiceConfigurationForPlAndCcbt
                .equals(other.emailServiceConfigurationForPlAndCcbt))
            return false;
        if (esConnectionConfig == null) {
            if (other.esConnectionConfig != null)
                return false;
        } else if (!esConnectionConfig.equals(other.esConnectionConfig))
            return false;
        if (losConfiguration == null) {
            if (other.losConfiguration != null)
                return false;
        } else if (!losConfiguration.equals(other.losConfiguration))
            return false;
        if (mbCommunicationDomainConfig == null) {
            if (other.mbCommunicationDomainConfig != null)
                return false;
        } else if (!mbCommunicationDomainConfig
                .equals(other.mbCommunicationDomainConfig))
            return false;
        if (ntcConfiguration == null) {
            if (other.ntcConfiguration != null)
                return false;
        } else if (!ntcConfiguration.equals(other.ntcConfiguration))
            return false;
        if (otpConfiguration == null) {
            if (other.otpConfiguration != null)
                return false;
        } else if (!otpConfiguration.equals(other.otpConfiguration))
            return false;
        if (panCommunicationDomainConfig == null) {
            if (other.panCommunicationDomainConfig != null)
                return false;
        } else if (!panCommunicationDomainConfig
                .equals(other.panCommunicationDomainConfig))
            return false;
        if (relianceConfiguration == null) {
            if (other.relianceConfiguration != null)
                return false;
        } else if (!relianceConfiguration.equals(other.relianceConfiguration))
            return false;
        if (salesForceConfiguration == null) {
            if (other.salesForceConfiguration != null)
                return false;
        } else if (!salesForceConfiguration
                .equals(other.salesForceConfiguration))
            return false;
        if (scoringCommunicationConfig == null) {
            if (other.scoringCommunicationConfig != null)
                return false;
        } else if (!scoringCommunicationConfig
                .equals(other.scoringCommunicationConfig))
            return false;
        if (smsServiceConfiguration == null) {
            if (other.smsServiceConfiguration != null)
                return false;
        } else if (!smsServiceConfiguration
                .equals(other.smsServiceConfiguration))
            return false;
        return true;
    }
}
