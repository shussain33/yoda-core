package com.softcell.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by priyanka on 14/12/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProxyConfiguration {
    private String proxyhost;
    private String port;
}
