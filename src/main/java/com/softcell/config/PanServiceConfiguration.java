/**
 * kishorp10:07:57 AM  Copyright Softcell Technolgy
 **/
package com.softcell.config;


/**
 * @author kishorp
 *
 */
public class PanServiceConfiguration {
    /**
     *
     * @param institutionId
     * @return pan communication domain
     */
    public static PanCommunicationDomain getCreditial(String institutionId) {
        PanCommunicationDomain communicationDomain = com.softcell.gonogo.cache.Cache.URL_CONFIGURATION.getPanCommunicationDomainConfig().get(institutionId);
        return communicationDomain;
    }

}
