package com.softcell.config.watermark;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by mahesh on 21/11/17.
 */
@Data
public class WaterMarkDetails {

    @JsonProperty("bWatrmrkEnble")
    private boolean waterMarkEnable;

    @JsonProperty("sWatrmrkName")
    private String waterMarkName;

    @JsonProperty("sWtrmrkPath")
    private String waterMarkPath;


}
