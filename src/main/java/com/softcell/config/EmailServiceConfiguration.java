/**
 * kishorp11:17:55 PM  Copyright Softcell Technolgy
 **/
package com.softcell.config;

/**
 * @author kishorp
 *
 */
public class EmailServiceConfiguration {

    private String emailUrl;

    private String baseUrl;

    private int connectionTimeout;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getEmailUrl() {
        return emailUrl;
    }

    public void setEmailUrl(String emailUrl) {
        this.emailUrl = emailUrl;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public static  Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private EmailServiceConfiguration emailServiceConfiguration = new EmailServiceConfiguration();

        public EmailServiceConfiguration build(){
            return this.emailServiceConfiguration;
        }

        public Builder emailUrl(String emailUrl){
            this.emailServiceConfiguration.setEmailUrl(emailUrl);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.emailServiceConfiguration.setBaseUrl(baseUrl);
            return this;
        }

        public Builder connectionTimeout(int connectionTimeout){
            this.emailServiceConfiguration.setConnectionTimeout(connectionTimeout);
            return this;
        }



    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EmailServiceConfiguration [emailUrl=");
        builder.append(emailUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", connectionTimeout=");
        builder.append(connectionTimeout);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + connectionTimeout;
        result = prime * result
                + ((emailUrl == null) ? 0 : emailUrl.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof EmailServiceConfiguration))
            return false;
        EmailServiceConfiguration other = (EmailServiceConfiguration) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (connectionTimeout != other.connectionTimeout)
            return false;
        if (emailUrl == null) {
            if (other.emailUrl != null)
                return false;
        } else if (!emailUrl.equals(other.emailUrl))
            return false;
        return true;
    }


}
