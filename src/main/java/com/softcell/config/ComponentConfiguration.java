package com.softcell.config;

import com.softcell.constants.ComponentConfigurationType;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.workflow.component.ComponentSetting;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


/**
 * @author vinodk
 */
@Document(collection = "ComponentConfiguration")
public class ComponentConfiguration extends AuditEntity{

    private String institutionId;

    private int reAppraiseCount;

    private int reInitiateCnt;

    private int reProcessCnt;

    private ComponentSetting componentSettings;

    private ComponentConfigurationType componentConfigType;

    private Date insertDate = new Date();

    private Date lastUpdateDate = new Date();

    private boolean active;

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    /**
     * @return the componentSettings
     */
    public ComponentSetting getComponentSettings() {
        return componentSettings;
    }

    /**
     * @param componentSettings the componentSettings to set
     */
    public void setComponentSettings(ComponentSetting componentSettings) {
        this.componentSettings = componentSettings;
    }

    /**
     * @return the componentConfigType
     */
    public ComponentConfigurationType getComponentConfigType() {
        return componentConfigType;
    }

    /**
     * @param componentConfigType the componentConfigType to set
     */
    public void setComponentConfigType(
            ComponentConfigurationType componentConfigType) {
        this.componentConfigType = componentConfigType;
    }

    /**
     * @return the reAppraiseCount
     */
    public int getReAppraiseCount() {
        return reAppraiseCount;
    }

    /**
     * @param reAppraiseCount the reAppraiseCount to set
     */
    public void setReAppraiseCount(int reAppraiseCount) {
        this.reAppraiseCount = reAppraiseCount;
    }

    /**
     * @return the reInitiateCnt
     */
    public int getReInitiateCnt() {
        return reInitiateCnt;
    }

    /**
     * @param reInitiateCnt the reInitiateCnt to set
     */
    public void setReInitiateCnt(int reInitiateCnt) {
        this.reInitiateCnt = reInitiateCnt;
    }

    /**
     * @return the reProcessCnt
     */
    public int getReProcessCnt() {
        return reProcessCnt;
    }

    /**
     * @param reProcessCnt the reProcessCnt to set
     */
    public void setReProcessCnt(int reProcessCnt) {
        this.reProcessCnt = reProcessCnt;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ComponentConfiguration [institutionId=");
        builder.append(institutionId);
        builder.append(", reAppraiseCount=");
        builder.append(reAppraiseCount);
        builder.append(", reInitiateCnt=");
        builder.append(reInitiateCnt);
        builder.append(", reProcessCnt=");
        builder.append(reProcessCnt);
        builder.append(", componentSettings=");
        builder.append(componentSettings);
        builder.append(", componentConfigType=");
        builder.append(componentConfigType);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime
                * result
                + ((componentConfigType == null) ? 0 : componentConfigType
                .hashCode());
        result = prime
                * result
                + ((componentSettings == null) ? 0 : componentSettings
                .hashCode());
        result = prime * result
                + ((insertDate == null) ? 0 : insertDate.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result + reAppraiseCount;
        result = prime * result + reInitiateCnt;
        result = prime * result + reProcessCnt;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComponentConfiguration other = (ComponentConfiguration) obj;
        if (active != other.active)
            return false;
        if (componentConfigType != other.componentConfigType)
            return false;
        if (componentSettings == null) {
            if (other.componentSettings != null)
                return false;
        } else if (!componentSettings.equals(other.componentSettings))
            return false;
        if (insertDate == null) {
            if (other.insertDate != null)
                return false;
        } else if (!insertDate.equals(other.insertDate))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (reAppraiseCount != other.reAppraiseCount)
            return false;
        if (reInitiateCnt != other.reInitiateCnt)
            return false;
        if (reProcessCnt != other.reProcessCnt)
            return false;
        return true;
    }
}
