/**
 * kishorp11:13:38 PM  Copyright Softcell Technolgy
 **/
package com.softcell.config;

/**
 * @author kishorp
 *
 */
public class OtpConfiguration {

    private String otpServiceURL;

    private String baseUrl;

    private int connectionTimeout = 1000;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getOtpServiceURL() {
        return otpServiceURL;
    }

    public void setOtpServiceURL(String otpServiceURL) {
        this.otpServiceURL = otpServiceURL;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }


    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {

        private OtpConfiguration otpConfiguration = new OtpConfiguration();

        public OtpConfiguration build(){
            return this.otpConfiguration;
        }

        public Builder otpServiceURL(String url){
            this.otpConfiguration.setOtpServiceURL(url);
            return this;
        }

        public Builder baseUrl(String url){
            this.otpConfiguration.setBaseUrl(url);
            return this;
        }

        public Builder connectionTimeout(int timeout){
            this.otpConfiguration.setConnectionTimeout(timeout);
            return this;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OtpConfiguration [otpServiceURL=");
        builder.append(otpServiceURL);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", connectionTimeout=");
        builder.append(connectionTimeout);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + connectionTimeout;
        result = prime * result
                + ((otpServiceURL == null) ? 0 : otpServiceURL.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof OtpConfiguration))
            return false;
        OtpConfiguration other = (OtpConfiguration) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (connectionTimeout != other.connectionTimeout)
            return false;
        if (otpServiceURL == null) {
            if (other.otpServiceURL != null)
                return false;
        } else if (!otpServiceURL.equals(other.otpServiceURL))
            return false;
        return true;
    }


}
