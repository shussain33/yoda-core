package com.softcell.config;

/***
 * It is used for configuring authentication service
 *
 * @author bhuvneshk
 */
public class AuthenticationConfiguration {

    private String url;

    private String changePassUrl;

    private String baseUrl;

    private String resetPassUrl;

    private String fetchUsersUrl;

    private String changeUserPassUrl;

    private String resetPassUam;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getResetPassUam() {
        return resetPassUam;
    }

    public void setResetPassUam(String resetPassUam) {
        this.resetPassUam = resetPassUam;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the changePassUrl
     */
    public String getChangePassUrl() {
        return changePassUrl;
    }

    /**
     * @param changePassUrl the changePassUrl to set
     */
    public void setChangePassUrl(String changePassUrl) {
        this.changePassUrl = changePassUrl;
    }

    /**
     * @return the resetPassUrl
     */
    public String getResetPassUrl() {
        return resetPassUrl;
    }

    /**
     * @param resetPassUrl the resetPassUrl to set
     */
    public void setResetPassUrl(String resetPassUrl) {
        this.resetPassUrl = resetPassUrl;
    }

    public String getFetchUsersUrl() {        return fetchUsersUrl;    }

    public void setFetchUsersUrl(String fetchUsersUrl) {    this.fetchUsersUrl = fetchUsersUrl;    }

    public static Builder builder(){
        return new Builder();
   }

    public String getChangeUserPassUrl() {
        return changeUserPassUrl;
    }

    public void setChangeUserPassUrl(String changeUserPassUrl) {
        this.changeUserPassUrl = changeUserPassUrl;
    }

   public static class Builder{
        private AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();

        public AuthenticationConfiguration build(){
            return this.authenticationConfiguration;
        }

        public Builder url(String url){
            this.authenticationConfiguration.setUrl(url);
            return this;
        }

        public Builder changePassUrl(String changePassUrl){
            this.authenticationConfiguration.setChangePassUrl(changePassUrl);
            return this;
        }

        public Builder baseUrl(String  baseUrl){
            this.authenticationConfiguration.setBaseUrl(baseUrl);
            return this;
        }

        public Builder resetPassUrl (String resetPassUrl){
            this.authenticationConfiguration.setResetPassUrl(resetPassUrl);
            return this;

        }

       public Builder resetPassUam (String resetPassUam){
           this.authenticationConfiguration.setResetPassUam(resetPassUam);
           return this;
       }

   }

}
