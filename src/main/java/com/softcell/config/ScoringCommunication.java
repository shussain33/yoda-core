/**
 * kishorp1:09:36 PM  Copyright Softcell Technolgy
 **/
package com.softcell.config;

/**
 * @author kishorp
 *
 */
public class ScoringCommunication {

    private String institutionId;

    private String userId;

    private String password;

    private String scoringServiceUrl;

    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getScoringServiceUrl() {
        return scoringServiceUrl;
    }

    public void setScoringServiceUrl(String scoringServiceUrl) {
        this.scoringServiceUrl = scoringServiceUrl;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {

        private ScoringCommunication scoringCommunication =  new ScoringCommunication();

        public ScoringCommunication build(){
            return this.scoringCommunication;
        }

        public Builder institutionId(String institutionId){
            this.scoringCommunication.setInstitutionId(institutionId);
            return this;
        }

        public Builder userId (String userId){
            this.scoringCommunication.setUserId(userId);
            return this;
        }

        public Builder password(String password){
            this.scoringCommunication.setPassword(password);
            return this;
        }

        public Builder scoringServiceUrl(String scoringServiceUrl){
            this.scoringCommunication.setScoringServiceUrl(scoringServiceUrl);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.scoringCommunication.setBaseUrl(baseUrl);
            return this;
        }
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ScoringCommunication [institutionId=");
        builder.append(institutionId);
        builder.append(", userId=");
        builder.append(userId);
        builder.append(", password=");
        builder.append(password);
        builder.append(", scoringServiceUrl=");
        builder.append(scoringServiceUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime
                * result
                + ((scoringServiceUrl == null) ? 0 : scoringServiceUrl
                .hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ScoringCommunication))
            return false;
        ScoringCommunication other = (ScoringCommunication) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (scoringServiceUrl == null) {
            if (other.scoringServiceUrl != null)
                return false;
        } else if (!scoringServiceUrl.equals(other.scoringServiceUrl))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }
}
