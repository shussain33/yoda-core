package com.softcell.config;

/**
 * @author vinodk
 */
public class SmsServiceConfiguration {

    private String url;

    private String baseUrl;

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl the baseUrl to set
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }


    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private SmsServiceConfiguration smsServiceConfiguration = new SmsServiceConfiguration();

        public SmsServiceConfiguration build(){
            return this.smsServiceConfiguration;
        }

        public Builder url(String url){
            this.smsServiceConfiguration.setUrl(url);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.smsServiceConfiguration.setBaseUrl(baseUrl);
            return this;
        }
    }


    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsServiceConfiguration [url=");
        builder.append(url);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmsServiceConfiguration other = (SmsServiceConfiguration) obj;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }
}
