package com.softcell.config;

import com.softcell.gonogo.model.multibureau.pickup.MbCommunicationDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ssg237 on 6/7/20.
 */
public  class ConfigType {

    private static final Logger logger = LoggerFactory.getLogger(ConfigType.class);
    public static final String MBCORP = "MBCORP";
    public static final String MBCONSUMER = "MBCONSUMER";
    public static final String SCORING = "SCORING";
    public static final String SMSSERVICE = "SMSSERVICE";

    public static void convertDataToMbCommunication(ServiceCommunication serviceCommunication, MbCommunicationDomain mbCommunicationDomain) {

        logger.info("{} in converDataToMbCommunication",serviceCommunication.getInstitutionId());
        mbCommunicationDomain.setInstitutionId(serviceCommunication.getInstitutionId());
        mbCommunicationDomain.setAggregatorId(serviceCommunication.getAggregatorId());
        mbCommunicationDomain.setMemberId(serviceCommunication.getMemberId());
        mbCommunicationDomain.setPassword(serviceCommunication.getPassword());
        mbCommunicationDomain.setBaseUrl(serviceCommunication.getBaseUrl());
        mbCommunicationDomain.setMbUrl(serviceCommunication.getServiceUrl());
        mbCommunicationDomain.setSourceSystem(serviceCommunication.getSourceSystem());
        mbCommunicationDomain.setMbId(serviceCommunication.getServiceId());
        mbCommunicationDomain.setNumberOfReTry(Integer.parseInt(serviceCommunication.getNumberOfReTry()));
    }


    public static void convertDataToScoringCommunication(ServiceCommunication serviceCommunication, ScoringCommunication comunicationDomain) {

        logger.info("{} in convertDataToScoringCommunication",serviceCommunication.getInstitutionId());

        comunicationDomain.setInstitutionId(serviceCommunication.getInstitutionId());
        comunicationDomain.setBaseUrl(serviceCommunication.getBaseUrl());
        comunicationDomain.setPassword(serviceCommunication.getPassword());
        comunicationDomain.setScoringServiceUrl(serviceCommunication.getServiceUrl());
        comunicationDomain.setUserId(serviceCommunication.getServiceId());
    }

    public static void convertDataToSmsServiceConfiguration(ServiceCommunication serviceCommunication, SmsServiceConfiguration smsServiceConfiguration) {
        smsServiceConfiguration.setBaseUrl(serviceCommunication.getBaseUrl());
        smsServiceConfiguration.setUrl(serviceCommunication.getServiceUrl());
    }
}

