/**
 * kishorp10:08:33 AM  Copyright Softcell Technolgy
 **/
package com.softcell.config;

/**
 * @author kishorp
 */
@Deprecated
public class PanCommunicationDomain {

    private String panUrl;

    private String baseUrl;

    private String institutionId;

    private String aggregatorId;

    private String memberId;

    private String password;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPanUrl() {
        return panUrl;
    }

    public void setPanUrl(String panUrl) {
        this.panUrl = panUrl;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getAggregatorId() {
        return aggregatorId;
    }

    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Builder builder (){
        return new Builder();
    }

    public static class Builder {
        private PanCommunicationDomain panCommunicationDomain = new PanCommunicationDomain();

        public PanCommunicationDomain build(){
            return this.panCommunicationDomain;
        }

        public Builder panUrl(String url){
            this.panCommunicationDomain.setPanUrl(url);
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.panCommunicationDomain.setBaseUrl(baseUrl);
            return this;
        }

        public Builder institutionId(String institutionId){
            this.panCommunicationDomain.setInstitutionId(institutionId);
            return this;
        }

        public Builder aggregatorId(String aggregatorId){
            this.panCommunicationDomain.setAggregatorId(aggregatorId);
            return this;
        }

        public Builder memberId(String memberId){
            this.panCommunicationDomain.setMemberId(memberId);
            return this;
        }

        public Builder password(String password){
            this.panCommunicationDomain.setPassword(password);
            return this;
        }


    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PanCommunicationDomain [panUrl=");
        builder.append(panUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", aggregatorId=");
        builder.append(aggregatorId);
        builder.append(", memberId=");
        builder.append(memberId);
        builder.append(", password=");
        builder.append(password);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((aggregatorId == null) ? 0 : aggregatorId.hashCode());
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((memberId == null) ? 0 : memberId.hashCode());
        result = prime * result + ((panUrl == null) ? 0 : panUrl.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PanCommunicationDomain))
            return false;
        PanCommunicationDomain other = (PanCommunicationDomain) obj;
        if (aggregatorId == null) {
            if (other.aggregatorId != null)
                return false;
        } else if (!aggregatorId.equals(other.aggregatorId))
            return false;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (memberId == null) {
            if (other.memberId != null)
                return false;
        } else if (!memberId.equals(other.memberId))
            return false;
        if (panUrl == null) {
            if (other.panUrl != null)
                return false;
        } else if (!panUrl.equals(other.panUrl))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        return true;
    }

}
