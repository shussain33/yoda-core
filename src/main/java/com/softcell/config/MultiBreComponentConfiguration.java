package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.workflow.component.Component;
import java.util.*;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * @author apadhye
 */
@Document(collection = "MultiBreComponentConfiguration")
public class MultiBreComponentConfiguration extends AuditEntity {

    private String institutionId;

    private String institutionName;

    private String productId;

    private String productName;

    private int reAppraiseCount;

    private int reInitiateCnt;

    private int reProcessCnt;

    private boolean active;

    @JsonIgnore // Ignore For multibre
    private Map<Integer, Component> componentMap = new TreeMap<Integer, Component>();

    // For MultiBRE : Key is completed stage and the value is component map.
    //private Map<String, Map<Integer, Component>> multiBreComponentMap = new HashMap();
    //private Map<BreSetting, Map<Integer, Component>> multiBreComponentMap = new HashMap();
    // Execution point
    private Map<String, List<BreSetting>> multiBreComponentMap = new HashMap();

    private ComponentConfigurationType componentConfigType;

    private Date insertDate = new Date();

    private Date lastUpdateDate = new Date();

    ///////////
    //private ComponentSetting componentSettings;

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    /**
     * @return the componentSettings
     */
    /*public ComponentSetting getComponentSettings() {
        return componentSettings;
    }*/

    /**
     * @param componentSettings the componentSettings to set
     */
    /*public void setComponentSettings(ComponentSetting componentSettings) {
        this.componentSettings = componentSettings;
    }*/

    /**
     * @return the componentConfigType
     */
    public ComponentConfigurationType getComponentConfigType() {
        return componentConfigType;
    }

    /**
     * @param componentConfigType the componentConfigType to set
     */
    public void setComponentConfigType(
            ComponentConfigurationType componentConfigType) {
        this.componentConfigType = componentConfigType;
    }

    /**
     * @return the reAppraiseCount
     */
    public int getReAppraiseCount() {
        return reAppraiseCount;
    }

    /**
     * @param reAppraiseCount the reAppraiseCount to set
     */
    public void setReAppraiseCount(int reAppraiseCount) {
        this.reAppraiseCount = reAppraiseCount;
    }

    /**
     * @return the reInitiateCnt
     */
    public int getReInitiateCnt() {
        return reInitiateCnt;
    }

    /**
     * @param reInitiateCnt the reInitiateCnt to set
     */
    public void setReInitiateCnt(int reInitiateCnt) {
        this.reInitiateCnt = reInitiateCnt;
    }

    /**
     * @return the reProcessCnt
     */
    public int getReProcessCnt() {
        return reProcessCnt;
    }

    /**
     * @param reProcessCnt the reProcessCnt to set
     */
    public void setReProcessCnt(int reProcessCnt) {
        this.reProcessCnt = reProcessCnt;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Map<Integer, Component> getComponentMap() {
        return componentMap;
    }

    public void setComponentMap(Map<Integer, Component> componentMap) {
        this.componentMap = componentMap;
    }

    public Map<String, List<BreSetting>> getMultiBreComponentMap() {
        return multiBreComponentMap;
    }

    public void setMultiBreComponentMap(Map<String, List<BreSetting>> multiBreComponentMap) {
        this.multiBreComponentMap = multiBreComponentMap;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ComponentConfiguration [institutionId=");
        builder.append(institutionId);
        builder.append(", reAppraiseCount=");
        builder.append(reAppraiseCount);
        builder.append(", reInitiateCnt=");
        builder.append(reInitiateCnt);
        builder.append(", reProcessCnt=");
        builder.append(reProcessCnt);
        /*builder.append(", componentSettings=");
        builder.append(componentSettings);*/
        builder.append(", componentConfigType=");
        builder.append(componentConfigType);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime
                * result
                + ((componentConfigType == null) ? 0 : componentConfigType
                .hashCode());
        /*result = prime
                * result
                + ((componentSettings == null) ? 0 : componentSettings
                .hashCode());
        */
        result = prime * result
                + ((insertDate == null) ? 0 : insertDate.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result + reAppraiseCount;
        result = prime * result + reInitiateCnt;
        result = prime * result + reProcessCnt;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComponentConfiguration other = (ComponentConfiguration) obj;
        if (active != other.isActive())
            return false;
        if (componentConfigType != other.getComponentConfigType())
            return false;
        /*if (componentSettings == null) {
            if (other.componentSettings != null)
                return false;
        } else if (!componentSettings.equals(other.componentSettings))
            return false;
        */
        if (insertDate == null) {
            if (other.getInsertDate() != null)
                return false;
        } else if (!insertDate.equals(other.getInsertDate()))
            return false;
        if (institutionId == null) {
            if (other.getInstitutionId() != null)
                return false;
        } else if (!institutionId.equals(other.getInstitutionId()))
            return false;
        if (reAppraiseCount != other.getReAppraiseCount())
            return false;
        if (reInitiateCnt != other.getReInitiateCnt())
            return false;
        if (reProcessCnt != other.getReProcessCnt())
            return false;
        return true;
    }

    public class BreSetting {
        private Map<Integer, Component> componentMap;

        private boolean mainThreadWait;

        public Map<Integer, Component> getComponentMap() {
            return componentMap;
        }

        public void setComponentMap(Map<Integer, Component> componentMap) {
            this.componentMap = componentMap;
        }

        public boolean isMainThreadWait() {
            return mainThreadWait;
        }

        public void setMainThreadWait(boolean mainThreadWait) {
            this.mainThreadWait = mainThreadWait;
        }
    }
}
