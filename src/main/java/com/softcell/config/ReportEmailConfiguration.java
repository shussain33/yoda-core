/**
 * yogeshb7:07:00 pm  Copyright Softcell Technolgy
 **/
package com.softcell.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 *
 */
@Document(collection = "ReportEmailMaster")
public class ReportEmailConfiguration extends AuditEntity {
    @JsonProperty("sReportType")
    private String reportType;

    @JsonProperty("sProductType")
    private String productType;

    @JsonProperty("sEmailId")
    private String email;

    @JsonProperty("sRecipientType")
    private String recipientType;

    @JsonProperty("sInstID")
    private String institutionID;

    @JsonProperty("bAct")
    private boolean active;

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ReportEmailConfiguration [reportType=" + reportType
                + ", productType=" + productType + ", email=" + email
                + ", recipientType=" + recipientType + ", institutionID="
                + institutionID + ", active=" + active + "]";
    }
}
