package com.softcell.config;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by amit on 20/2/18.
 */
@Document(collection = "HierarchyGoNoGoMapping")
public class CriteriaMapping {

    String hierarchyLevel;

    String fieldMapping;

    public String getHierarchyLevel() {
        return hierarchyLevel;
    }

    public void setHierarchyLevel(String hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }

    public String getFieldMapping() {
        return fieldMapping;
    }

    public void setFieldMapping(String fieldMapping) {
        this.fieldMapping = fieldMapping;
    }
}
