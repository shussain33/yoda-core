package com.softcell.config.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author kishorp
 */
@Document(collection = "validationVendorsConfig")
public class ValidationVendorsConfiguration extends AuditEntity {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sProductName")
    private Product productName;

    @JsonProperty("sVendorName")
    private Vendor vendor;

    @JsonProperty("bEnable")
    private boolean enable;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ValidationVendorsConfiguration{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", institutionName='").append(institutionName).append('\'');
        sb.append(", productId='").append(productId).append('\'');
        sb.append(", productName=").append(productName);
        sb.append(", vendor=").append(vendor);
        sb.append(", enable=").append(enable);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ValidationVendorsConfiguration that = (ValidationVendorsConfiguration) o;

        if (enable != that.enable) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (institutionName != null ? !institutionName.equals(that.institutionName) : that.institutionName != null)
            return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productName != that.productName) return false;
        return vendor == that.vendor;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (institutionName != null ? institutionName.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (enable ? 1 : 0);
        return result;
    }
}
