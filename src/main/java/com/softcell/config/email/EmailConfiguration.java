/**
 *
 */
package com.softcell.config.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.LogoType;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;


/**
 * @author prateek
 */
@Document(collection = "emailConfiguration")
public class EmailConfiguration extends AuditEntity {

    @JsonProperty("sInstitutionId")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class,
                    EmailConfiguration.UpdateAttachmentGrp.class,
                    EmailConfiguration.UpdateSecondaryAttachmentGrp.class,
                    EmailConfiguration.FetchGrp.class,
                    EmailConfiguration.InstitutionGrp.class
            }
    )
    private String institutionId;

    @JsonProperty("sInstitutionName")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class
            }
    )
    private String institutionName;

    @JsonProperty("sProductId")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class,
                    EmailConfiguration.UpdateAttachmentGrp.class,
                    EmailConfiguration.UpdateSecondaryAttachmentGrp.class,
                    EmailConfiguration.FetchGrp.class
            }
    )
    private String productId;

    @JsonProperty("sProductName")
    @NotNull(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class,
                    EmailConfiguration.UpdateAttachmentGrp.class,
                    EmailConfiguration.UpdateSecondaryAttachmentGrp.class,
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.FetchGrp.class
            }
    )
    private Product productName;

    @JsonProperty("sLogoId")
    private String logoId;

    @JsonProperty("sTemplateName")
    private String templateName;

    @JsonProperty("sLogoType")
    @NotNull(
            groups = {
                    EmailConfiguration.InsertGrp.class
            }
    )
    private LogoType logoType;

    @JsonProperty("sLogoDescription")
    private String logoDescription;

    @JsonProperty("sEmailSubject")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class
            }
    )
    private String emailSubject;

    @JsonProperty("sEmailBody")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class
            }
    )
    private String emailBody;

    @JsonProperty("sEmailSignature")
    private String emailSignature;

    @JsonProperty("sCopyRight")
    @NotEmpty(
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateGrp.class
            }
    )
    private String copyRight;

    @JsonProperty("aAttachment")
    @NotNull(groups = {
            EmailConfiguration.InsertGrp.class,
            EmailConfiguration.UpdateAttachmentGrp.class
    })
    @Size(min = 1,
            groups = {
                    EmailConfiguration.InsertGrp.class,
                    EmailConfiguration.UpdateAttachmentGrp.class
            }
    )
    private Set<TemplateName> attachments;

    @JsonProperty("aSecondaryAttachment")
    @NotNull(groups = {
            EmailConfiguration.UpdateSecondaryAttachmentGrp.class
    })
    @Size(min = 1,
            groups = {
                    EmailConfiguration.UpdateSecondaryAttachmentGrp.class
            }
    )
    private Set<TemplateName> secondaryAttachments;

    @JsonProperty("bEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate;

    @JsonProperty("dtLastUpdateDate")
    private Date lastUpdateDate;


    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }

    public String getLogoId() {
        return logoId;
    }

    public void setLogoId(String logoId) {
        this.logoId = logoId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public LogoType getLogoType() {
        return logoType;
    }

    public void setLogoType(LogoType logoType) {
        this.logoType = logoType;
    }

    public String getLogoDescription() {
        return logoDescription;
    }

    public void setLogoDescription(String logoDescription) {
        this.logoDescription = logoDescription;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailSignature() {
        return emailSignature;
    }

    public void setEmailSignature(String emailSignature) {
        this.emailSignature = emailSignature;
    }

    public String getCopyRight() {
        return copyRight;
    }

    public void setCopyRight(String copyRight) {
        this.copyRight = copyRight;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Set<TemplateName> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<TemplateName> attachments) {
        this.attachments = attachments;
    }

    public Set<TemplateName> getSecondaryAttachments() {
        return secondaryAttachments;
    }

    public void setSecondaryAttachments(Set<TemplateName> secondaryAttachments) {
        this.secondaryAttachments = secondaryAttachments;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EmailConfiguration{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", institutionName='").append(institutionName).append('\'');
        sb.append(", productId='").append(productId).append('\'');
        sb.append(", productName=").append(productName);
        sb.append(", logoId='").append(logoId).append('\'');
        sb.append(", templateName='").append(templateName).append('\'');
        sb.append(", logoType=").append(logoType);
        sb.append(", logoDescription='").append(logoDescription).append('\'');
        sb.append(", emailSubject='").append(emailSubject).append('\'');
        sb.append(", emailBody='").append(emailBody).append('\'');
        sb.append(", emailSignature='").append(emailSignature).append('\'');
        sb.append(", copyRight='").append(copyRight).append('\'');
        sb.append(", attachments=").append(attachments);
        sb.append(", secondaryAttachments=").append(secondaryAttachments);
        sb.append(", enable=").append(enable);
        sb.append(", createDate=").append(createDate);
        sb.append(", lastUpdateDate=").append(lastUpdateDate);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EmailConfiguration that = (EmailConfiguration) o;

        if (enable != that.enable) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (institutionName != null ? !institutionName.equals(that.institutionName) : that.institutionName != null)
            return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productName != that.productName) return false;
        if (logoId != null ? !logoId.equals(that.logoId) : that.logoId != null) return false;
        if (templateName != null ? !templateName.equals(that.templateName) : that.templateName != null) return false;
        if (logoType != that.logoType) return false;
        if (logoDescription != null ? !logoDescription.equals(that.logoDescription) : that.logoDescription != null)
            return false;
        if (emailSubject != null ? !emailSubject.equals(that.emailSubject) : that.emailSubject != null) return false;
        if (emailBody != null ? !emailBody.equals(that.emailBody) : that.emailBody != null) return false;
        if (emailSignature != null ? !emailSignature.equals(that.emailSignature) : that.emailSignature != null)
            return false;
        if (copyRight != null ? !copyRight.equals(that.copyRight) : that.copyRight != null) return false;
        if (attachments != null ? !attachments.equals(that.attachments) : that.attachments != null) return false;
        if (secondaryAttachments != null ? !secondaryAttachments.equals(that.secondaryAttachments) : that.secondaryAttachments != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null) return false;
        return lastUpdateDate != null ? lastUpdateDate.equals(that.lastUpdateDate) : that.lastUpdateDate == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (institutionName != null ? institutionName.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (logoId != null ? logoId.hashCode() : 0);
        result = 31 * result + (templateName != null ? templateName.hashCode() : 0);
        result = 31 * result + (logoType != null ? logoType.hashCode() : 0);
        result = 31 * result + (logoDescription != null ? logoDescription.hashCode() : 0);
        result = 31 * result + (emailSubject != null ? emailSubject.hashCode() : 0);
        result = 31 * result + (emailBody != null ? emailBody.hashCode() : 0);
        result = 31 * result + (emailSignature != null ? emailSignature.hashCode() : 0);
        result = 31 * result + (copyRight != null ? copyRight.hashCode() : 0);
        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        result = 31 * result + (secondaryAttachments != null ? secondaryAttachments.hashCode() : 0);
        result = 31 * result + (enable ? 1 : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        return result;
    }

    public interface InsertGrp {
    }

    public interface UpdateGrp {
    }

    public interface UpdateAttachmentGrp {
    }

    public interface UpdateSecondaryAttachmentGrp {
    }

    public interface FetchGrp {
    }

    public interface InstitutionGrp {
    }
}
