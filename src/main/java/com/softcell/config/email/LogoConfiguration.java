package com.softcell.config.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import com.softcell.constants.LogoType;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 */
@Document(collection = "logoConfiguration")
public class LogoConfiguration extends AuditEntity {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sinstitutionName")
    private String institutionName;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sProductName")
    private Product productName;

    @JsonProperty("sLogoName")
    private String logoName;

    @JsonProperty("sLogoId")
    private String LogoId;

    @JsonProperty("sLogoDescription")
    private String logoDescription;

    @JsonProperty("bEnable")
    private boolean enable;

    @JsonProperty("dtcreateDate")
    private Date createDate;

    @JsonProperty("dtLastUpdateDate")
    private Date lastUpdateDate;

    @JsonProperty("sLogoType")
    private LogoType logoType;

    @JsonProperty("oUser")
    private User user;


    public LogoType getLogoType() {
        return logoType;
    }

    public void setLogoType(LogoType logoType) {
        this.logoType = logoType;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public String getLogoId() {
        return LogoId;
    }

    public void setLogoId(String logoId) {
        LogoId = logoId;
    }

    public String getLogoDescription() {
        return logoDescription;
    }

    public void setLogoDescription(String logoDescription) {
        this.logoDescription = logoDescription;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
