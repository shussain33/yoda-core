package com.softcell.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.mongodb.CommandResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by prateek on 8/3/17.
 */
@Component
public class DBHealthChecker extends HealthCheck {

    private static final Logger logger = LoggerFactory.getLogger(DBHealthChecker.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    protected Result check() {

        try{

            CommandResult stats = mongoTemplate.getDb().getStats();

            if(stats.ok()){
                return HealthCheck.Result.healthy("database is up and running");
            }

        }catch (Exception e){

            //e.printStackTrace();

            logger.error("error occurred while checking mongodb status with probable cause [{}] ", e.getMessage());

        }

        return HealthCheck.Result.unhealthy("database  heartbeat lost ");
    }
}
