package com.softcell.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Aadhaar heartbeat check service
 *
 * Created by prateek on 8/3/17.
 */
@Component
public class AadhaarHealthChecker extends HealthCheck {

    private static final Logger logger = LoggerFactory.getLogger(AadhaarHealthChecker.class);


    private WorkFlowCommunicationManager workFlowCommunicationManager;

    private HttpTransportationService httpTransportationService;

    @Autowired
    public AadhaarHealthChecker(WorkFlowCommunicationManager workFlowCommunicationManager,
                                HttpTransportationService httpTransportationService) {
        this.workFlowCommunicationManager = workFlowCommunicationManager;
        this.httpTransportationService = httpTransportationService;
    }

    @Override
    protected Result check() throws Exception {
        try{

            WFJobCommDomain wFJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType("4019", WfJobTypeConst.AADHAR_EXECUTOR.getValue());

            if(httpTransportationService.checkServiceHeartBeat(wFJobCommDomain.getBaseUrl())){
                return Result.healthy("Aadhaar service is up and running ");
            }


        }catch (Exception e){
            logger.error("error occurred while checking heartbeat of aadhaar health cheacker with probable cause [{}] ", e.getMessage());
        }

        return  Result.unhealthy(" Aadhaar service heartbeat lost ");
    }
}
