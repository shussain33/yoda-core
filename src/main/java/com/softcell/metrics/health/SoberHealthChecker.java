package com.softcell.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * Health check service for Sobre
 *
 * Created by prateek on 8/3/17.
 */
@Component
public class SoberHealthChecker extends HealthCheck {

    private static final Logger logger = LoggerFactory.getLogger(SoberHealthChecker.class);

    @Autowired private WorkFlowCommunicationManager workFlowCommunicationManager;


    @Autowired
    HttpTransportationService httpTransportationService;

    @Override
    protected Result check()  {

        try{

            WFJobCommDomain wfCommDomainJobByType = workFlowCommunicationManager
                    .getWfCommDomainJobByType("4019", WfJobTypeConst.SCORING_JOB.getValue());

            if(httpTransportationService.checkServiceHeartBeat(wfCommDomainJobByType.getBaseUrl())){

                return  HealthCheck.Result.healthy(" sobre service is up and running ");

            }


        }catch (Exception e){
            logger.error("Error while fetching communication domain for sobre from mongodb with probable cause [{}]", e.getMessage());

        }

        return HealthCheck.Result.unhealthy(" sobre service  heartbeat lost  ");
    }
}
