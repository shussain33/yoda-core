package com.softcell.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * heartbeat cheacker for pan service
 *
 * Created by prateek on 8/3/17.
 */
@Component
public class PanHealthChecker extends HealthCheck {

    private static final Logger logger = LoggerFactory.getLogger(PanHealthChecker.class);

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private HttpTransportationService httpTransportationService;



    @Override
    protected Result check() throws Exception {

        try{


            WFJobCommDomain wfCommDomainJobByType = workFlowCommunicationManager
                    .getWfCommDomainJobByType("4019", WfJobTypeConst.PAN_VERIFICATION_JOB.getValue());

            if(httpTransportationService.checkServiceHeartBeat(wfCommDomainJobByType.getBaseUrl())){
                Result.healthy(" Pan service is up and running ");
            }


        }catch (Exception e ){
            logger.error(" error occurred while checking pan service heartbeat with probable cause [{}] ",e.getMessage() );
        }

        return Result.unhealthy(" Pan service heartbeat lost ");


    }
}
