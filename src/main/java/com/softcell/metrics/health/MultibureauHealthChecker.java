package com.softcell.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by prateek on 8/3/17.
 */
@Component
public class MultibureauHealthChecker extends HealthCheck {

    private static final Logger logger = LoggerFactory.getLogger(MultibureauHealthChecker.class);


    private final WorkFlowCommunicationManager workFlowCommunicationManager;


    private final  HttpTransportationService httpTransportationService;

    public MultibureauHealthChecker(WorkFlowCommunicationManager workFlowCommunicationManager,
                                    HttpTransportationService httpTransportationService){
        this.httpTransportationService = httpTransportationService;
        this.workFlowCommunicationManager = workFlowCommunicationManager;
    }


    @Override
    protected Result check() {

        try{

            WFJobCommDomain wFJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType("4019", WfJobTypeConst.MULTIBUREAU_JOB.getValue());

            if(httpTransportationService.checkServiceHeartBeat(wFJobCommDomain.getBaseUrl())){
                return  Result.healthy("multibureau service is up and running ");
            }

        }catch (Exception e){
            logger.error("error occurred while checking multibureau heartbeat with probable cause [{}] ", e.getMessage());
        }

        return Result.unhealthy(" multibureau service heartbeat lost ");
    }
}
