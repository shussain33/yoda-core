package com.softcell.dao.mongodb.config;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author vinodk
 *         <p/>
 *         This class will cache the app loaded from the database.
 */
public class ConfigCache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 1L;

    private int cacheSize;

    public ConfigCache(int cacheSize) {
        super(cacheSize, 0.75f, true);
        this.cacheSize = cacheSize;
    }

    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() >= cacheSize;
    }
}
