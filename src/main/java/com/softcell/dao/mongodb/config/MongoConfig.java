package com.softcell.dao.mongodb.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

/**
 * singleton app class handling mongotemplate and
 * gridfstemplate
 *
 * @author prateek
 */
@Deprecated
public class MongoConfig {

    private static final ApplicationContext applicationContext;

    private static final String MONGO_CONFIG_LOC = "mongo-data.xml";
    private static final String MONGO_BEAN = "mongoTemplate";
    private static final String MONGO_GRIDFS_BEAN = "gridTemplate";

    /**
     * static method to initialize application context while object of class
     * is created
     */
    static {
        applicationContext = new ClassPathXmlApplicationContext(MONGO_CONFIG_LOC);
    }

    private MongoConfig() {

    }

    /**
     * static utility method to get mongotemplate from
     * application context
     * USAGES:: MongoConfig.getMongoTemplate();
     *
     * @return
     */
    @Deprecated
    public static MongoTemplate getMongoTemplate() {
        MongoTemplate mongoTemplate = null;

        if (null != applicationContext) {
            mongoTemplate = (MongoTemplate) applicationContext.getBean(MONGO_BEAN);
        }

        return mongoTemplate;
    }

    /**
     * static utility method to get gridfs template from applicationContext
     * USAGES:: MongoConfig.getGridFSTemplate();
     *
     * @return
     */
    public static GridFsTemplate getGridFSTemplate() {
        GridFsTemplate gridFsTemplate = null;

        if (null != applicationContext) {
            gridFsTemplate = (GridFsTemplate) applicationContext.getBean(MONGO_GRIDFS_BEAN);
        }

        return gridFsTemplate;
    }

    public enum COLLECTIONS {
        GONOGO_CUSTOMER_APPLICATION("goNoGoCustomerApplication"), POST_IPA_DOC("postIpaDoc"),
        LOGGER_DOC("loggerDoc"), ACTIVITY_LOGS("activityLogs"), APPLICATION_DOCUMENT("ApplicationDocument"),
        ASSET_MODEL_MASTER("assetModelMaster");

        private String collection;

        private COLLECTIONS(final String collection) {
            this.collection = collection;
        }

        public String toString() {
            return collection;
        }
    }


}
