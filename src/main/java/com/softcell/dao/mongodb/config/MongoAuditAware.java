package com.softcell.dao.mongodb.config;

import org.springframework.data.domain.AuditorAware;

/**
 * Created by prateek on 26/2/17.
 */
public class MongoAuditAware implements AuditorAware<String > {

    @Override
    public String getCurrentAuditor() {
        return "prateek";
    }
}
