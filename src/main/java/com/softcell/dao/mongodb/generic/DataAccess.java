package com.softcell.dao.mongodb.generic;

import java.lang.annotation.*;

/**
 * Created by prateek on 2/3/17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Documented
public @interface DataAccess {

    Class<?> entity();

}
