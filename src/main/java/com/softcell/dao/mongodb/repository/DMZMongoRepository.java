package com.softcell.dao.mongodb.repository;


import com.softcell.constants.ActionName;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.LosLogging;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.masters.RelianceDealerBranchMaster;
import com.softcell.gonogo.model.masters.TkilDealerBranchMaster;
import com.softcell.gonogo.model.reliance.RelianceDigitalLogging;
import com.softcell.gonogo.model.response.dmz.DmzServiceCheckRequest;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.model.tkil.TKILLogging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DMZMongoRepository implements DMZRepository {

    private static final Logger logger = LoggerFactory.getLogger(DMZMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public DMZMongoRepository(){
        if(null == mongoTemplate){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
    }

    @Override
    public void saveRelianceDigitalObj(RelianceDigitalLogging relianceDigitalLogging) throws Exception {

        logger.debug("DMZ repo is invoked to insert reliance digital log");

        try {

            mongoTemplate.insert(relianceDigitalLogging);

        } catch (Exception e) {

            logger.error("Error while inserting reliance log in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while inserting reliance log in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public void saveLosLogObj(LosLogging logLogging) throws Exception {

        logger.debug("DMZ repo is invoked to insert LOS log");

        try {

            mongoTemplate.insert(logLogging);

        } catch (Exception e) {

            logger.error("Error while inserting LOS log in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while inserting LOS log in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public List<LosLogging> getLosLogData(String refId) throws Exception {

        logger.debug("DMZ repo is invoked to fetch LOS log fro refId {}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId));
            query.with(new Sort(Sort.Direction.DESC, "insertDate"));

            return mongoTemplate.find(query, LosLogging.class);

        } catch (Exception e) {

            logger.error("Error while fetching LOS log in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching LOS log in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public Object getServiceStatus(DmzServiceCheckRequest dmzServiceCheckReq) throws Exception {

        logger.debug("DMZ repo is invoked to check success log for dmz service {}", dmzServiceCheckReq.getActionName());

        Object dmzLogObject = null;

        if (dmzServiceCheckReq.getActionName() == ActionName.LOS_DATA_ENTRY_INTERFACE) {

            dmzLogObject = checkLosInterfaceStatus(dmzServiceCheckReq.getGonogoRefId());

        } else if (dmzServiceCheckReq.getActionName() == ActionName.RELIANCE_INTERFACE) {

            dmzLogObject = checkReliancInterfaceStatus(dmzServiceCheckReq.getGonogoRefId());

        }else if(dmzServiceCheckReq.getActionName() == ActionName.TKIL_INTERFACE){

            dmzLogObject = checkTkilInterfaceStatus(dmzServiceCheckReq.getGonogoRefId());

        }

        return dmzLogObject;
    }

    private RelianceDigitalLogging checkReliancInterfaceStatus(String refId) throws Exception {

        logger.debug("DMZ repo is invoked to fetch reliance success  log for refId {%s}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("request.refID").is(refId).and("response.errorCode").is(Status.SUCCESS));

            return mongoTemplate.findOne(query, RelianceDigitalLogging.class);

        } catch (Exception e) {

            logger.error("Error while fetching reliance log in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching reliance log in DMZ repo possible cause {%s}", e.getMessage()));

        }

    }

    private LosLogging checkLosInterfaceStatus(String refId) throws Exception {

        logger.debug("DMZ repo is invoked to fetch los success  log for refId {}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId).and("losCustomerResponse.errorDesc").is(Status.SUCCESS));

            return mongoTemplate.findOne(query, LosLogging.class);

        } catch (Exception e) {

            logger.error("Error while inserting LOS log in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while inserting LOS log in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public boolean checkInvoiceObject(DmzServiceCheckRequest dmzServiceCheckReq) throws Exception {

        logger.debug("DMZ repo is invoked to check invoice object for refId {}", dmzServiceCheckReq.getGonogoRefId());

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(dmzServiceCheckReq.getGonogoRefId()).and("invoiceDetails").ne(null));

            return mongoTemplate.exists(query, GoNoGoCustomerApplication.class);

        } catch (Exception e) {

            logger.error("Error while checking invoice object in DMZ repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while checking invoice object in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public boolean checkLosStatus(String refId) throws Exception {

        logger.debug("Error while checking losLog success data push for refId {}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId).and("losStage").is(GNGWorkflowConstant.LOS_BDE.toFaceValue()));

            return mongoTemplate.exists(query, LosLogging.class);

        } catch (Exception e) {

            logger.error("Error while checking losLog success data push with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while checking losLog success data push with possible cause {%s}", e.getMessage()));

        }
    }


    @Override
    public boolean checkRelianceStatus(String refId) throws Exception {

        logger.debug("DMZ repo  invoked to check reliance success data push for refId {}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("request.refID").is(refId).and("response.errorCode").is(Status.SUCCESS));

            return mongoTemplate.exists(query, RelianceDigitalLogging.class);

        } catch (Exception e) {

            logger.error("Error while checking reliance success data push with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while checking reliance success data push with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public boolean checkTkilStatus(String refId) throws Exception {

        logger.debug("DMZ repo  invoked to check tkil success data push for refId {}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("request.refID").is(refId).and("response.errorCode").is(Status.SUCCESS));

            return mongoTemplate.exists(query, TKILLogging.class);

        } catch (Exception e) {

            logger.error("Error while checking tkil success data push with possible cause {}", e.getMessage());

            throw new Exception(String.format("Error while checking tkil success data push with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public List<RelianceDigitalLogging> getRelianceLogData(String refId) throws Exception {

        logger.debug("DMZ repo invoked to fetch reliance log for refId {}", refId);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("request.refID").is(refId));

            return mongoTemplate.find(query, RelianceDigitalLogging.class);

        } catch (Exception e) {

            logger.error("Error while fetching reliance log with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching reliance log with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public boolean checkRelianceDealerAccess(String dealerName) throws Exception {

        logger.debug("DMZ repo invoked to check dealer name {} has access to push data to reliance ", dealerName);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("dealerName").is(dealerName).and("active").is(true));

            return mongoTemplate.exists(query, RelianceDealerBranchMaster.class);

        } catch (Exception e) {

            logger.error("Error while checking dealer access for reliance with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while checking dealer access for reliance with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public void losMbPushLogs(MbPushResponse mbPushResponse) {

        try {

            mongoTemplate.insert(mbPushResponse);

        } catch (Exception e) {

            logger.error("Error while inserting mb datapush log in DMZ repo possible cause {}", e.getMessage());

            throw new SystemException(String.format("Error while inserting mb datapush log in DMZ repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public void saveTvrLog(TVRLogs tvrLogs) {
        try {
            mongoTemplate.insert(tvrLogs);

        } catch (Exception e) {

            logger.error("Error while inserting mb datapush log in DMZ repo possible cause {}", e.getMessage());

        }
    }

    @Override
    public void saveTkilLog(TKILLogging tkilLogging) throws Exception{
        try {
            mongoTemplate.insert(tkilLogging);

        } catch (DataAccessException de) {

            logger.error("Error while inserting mb datapush log in DMZ repo possible cause {}", de.getMessage());

            throw new SystemException(String.format("Error while inserting mb datapush log in DMZ repo possible cause {%s}", de.getMessage()));

        }
    }

    @Override
    public boolean checkTkilDealerAccess(String institutionId, String dealerName) throws Exception {

        logger.debug("DMZ repo invoked to check dealer name {} has access to push data to tkil ", dealerName);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("dealerName").is(dealerName).and("active").is(true));

            return mongoTemplate.exists(query, TkilDealerBranchMaster.class);

        } catch (Exception e) {

            logger.error("Error while checking dealer access for tkil with possible cause {}", e.getMessage());

            throw new SystemException(String.format("Error while checking dealer access for tkil with possible cause {%s}", e.getMessage()));

        }
    }

    private TKILLogging checkTkilInterfaceStatus(String refId) throws Exception {

        logger.debug("DMZ repo is invoked to fetch reliance success  log for refId {%s}", refId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("request.refID").is(refId).and("response.errorCode").is(Status.SUCCESS));

            return mongoTemplate.findOne(query, TKILLogging.class);

        } catch (Exception e) {

            logger.error("Error while fetching tkil log in DMZ repo possible cause {}", e.getMessage());

            throw new SystemException(String.format("Error while fetching tkil log in DMZ repo possible cause {%s}", e.getMessage()));

        }

    }


    @Override
    public List<TKILLogging> getTkilLogData(String refId) throws Exception {

        logger.debug("DMZ repo invoked to fetch tkil log for refId {}", refId);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("request.refID").is(refId));

            return mongoTemplate.find(query, TKILLogging.class);

        } catch (Exception e) {

            logger.error("Error while fetching tkil log with possible cause {}", e.getMessage());

            throw new SystemException(String.format("Error while fetching tkil log with possible cause {%s}", e.getMessage()));

        }
    }
}
