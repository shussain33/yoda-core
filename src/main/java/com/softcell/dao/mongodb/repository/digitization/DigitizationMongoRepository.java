package com.softcell.dao.mongodb.repository.digitization;

import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.digitization.DownloadDigitizationFormRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.Document;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * @author yogeshb
 */
@Repository
public class DigitizationMongoRepository implements DigitizationRepository {

    private static final Logger logger = LoggerFactory.getLogger(DigitizationMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Override
    public String getBase64Image(FileUploadRequest fileUploadRequest) throws Exception {

        logger.debug("Digitization repo invoked to fetch Base64 image for institution : {}, refId : {}, fileType : {} , fileName : {}",
                fileUploadRequest.getFileHeader().getInstitutionId(), fileUploadRequest.getGonogoReferanceId()
                , fileUploadRequest.getUploadFileDetails().getFileType(),
                fileUploadRequest.getUploadFileDetails().getFileName());

        try {

            GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria
                    .where("metadata.gonogoReferanceId")
                    .is(fileUploadRequest.getGonogoReferanceId())
                    .and("metadata.fileHeader.institutionId")
                    .is(fileUploadRequest.getFileHeader().getInstitutionId())
                    .and("metadata.uploadFileDetails.fileType")
                    .in("jpg","png")
                    .and("metadata.uploadFileDetails.FileName")
                    .is(fileUploadRequest.getUploadFileDetails().getFileName())));

            return getBase64ConvertedString(gridFSDBFile);

        } catch (Exception e) {

            logger.error("Error while fetching base64 image with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching base64 image with possible cause {%s}", e.getMessage()));

        }

    }

    @Override
    public String getBase64Image(String logoId) throws Exception {

        logger.debug("Digitization repo invoked to fetch base64 image for logoId {}", logoId);

        try {

            GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria
                    .where("_id").is(logoId)));

            return getBase64ConvertedString(gridFSDBFile);

        } catch (Exception e) {

            logger.error("Error while fetching base64 image with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching base64 image with possible cause {%s}", e.getMessage()));

        }
    }


    /**
     * @param gridFSDBFile
     * @return
     */
    private String getBase64ConvertedString(GridFSDBFile gridFSDBFile) throws Exception {

        logger.debug("Base64 Conversion to string started");

        if (gridFSDBFile != null) {

            ByteArrayOutputStream bao = new ByteArrayOutputStream();

            InputStream inputStream = gridFSDBFile.getInputStream();

            try {

                BufferedImage img = ImageIO.read(inputStream);

                ImageIO.write(img, gridFSDBFile.getContentType(), bao);

                byte[] encodeBase64 = Base64.encodeBase64(bao.toByteArray());

                return new String(encodeBase64);

            } catch (Exception e) {

                logger.error("Error converting base64 to String with possible cause {}", e.getMessage());

                logger.error("{}",e.getStackTrace());

                throw new Exception(String.format("Error converting base64 to String with possible cause {%s}", e.getMessage()));

            }
        }

        return null;

    }

    @Override
    public GoNoGoCroApplicationResponse getApplication(String institutionId,
                                                       String refID) throws Exception {

        logger.debug("Digitization repo invoked to fetch gng customer response for institution {} and refId {}",institutionId,refID);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refID).and("applicationRequest.header.institutionId")
                    .is(institutionId).and("applicationStatus").is("Approved"));

            return mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);

        } catch (Exception e) {

            logger.error("Error while fetching gng customer response with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching gng customer response with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public PostIPA getPostIpa(String institutionId, String refID) throws Exception {

        logger.debug("Digitization repo invoked to fetch post ipa detail");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refID).and("header.institutionId").is(institutionId));

            PostIpaRequest postIpaRequest = mongoTemplate.findOne(query, PostIpaRequest.class);

            if (postIpaRequest != null) {
                return postIpaRequest.getPostIPA();
            } else {
                return null;
            }

        }  catch (Exception e) {

            logger.error("Error while fetching post ipa with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching post ipa with possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public PostIpaRequest getPostIpaRequest(String institutionId, String refID) throws Exception {

        logger.debug("Digitization repo invoked to fetch postIpaRequest detail");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refID).and("header.institutionId").is(institutionId));

            return mongoTemplate.findOne(query, PostIpaRequest.class);

        } catch (DataAccessException e) {

            logger.error("Error while fetching postIpaRequest with possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new SystemException(String.format("Error while fetching postIpaRequest with possible cause {%s}", e.getMessage()));

        }


    }

    @Override
    public SerialNumberInfo getImeiNumberDetails(String refID) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refID").is(refID)
                    .and("status").is(Status.VALID.name()));

            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            return mongoTemplate.findOne(query, SerialNumberInfo.class);
        } catch (DataAccessException e) {

            logger.error("Error while fetching ImeINumberDetails with possible cause {}", e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("Error while fetching ImeINumberDetails with possible cause {%s}", e.getMessage()));

        }

    }

    @Override
    public Document getDigitizationForm(DownloadDigitizationFormRequest downloadDigitizationFormRequest) {

        Document digitizationDocument = null;

        try {
            Query digitizationFormQuery = QueryBuilder.buildApplicationAgreementFormExistanceQuery(downloadDigitizationFormRequest.getRefID(),
                    downloadDigitizationFormRequest.getHeader().getInstitutionId(), downloadDigitizationFormRequest.getFileName().toFaceValue());


            GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(digitizationFormQuery);

            if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {

                digitizationDocument = new Document();

                byte[] encodeBase64 = Base64.encodeBase64(IOUtils.toByteArray(gridFSDBFile.getInputStream()));

                digitizationDocument.setByteCode(new String(encodeBase64));
                digitizationDocument.setDocID(String.valueOf(gridFSDBFile.getId()));
                digitizationDocument.setStatus(Status.SUCCESS.name());
                digitizationDocument.setDocName(downloadDigitizationFormRequest.getFileName().toFaceValue());

            }
        } catch (Exception exp) {

            logger.error("Exception occurred in while fetching image  for refId {} for probable cause [{}] ", downloadDigitizationFormRequest.getRefID(), exp);

            throw new SystemException(String.format("Exception occurred in while fetching image  for refId {%s} for probable cause [{%s}] ", downloadDigitizationFormRequest.getRefID(), exp));

        }
        return digitizationDocument;
    }
}
