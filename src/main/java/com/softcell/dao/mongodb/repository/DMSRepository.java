package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.dms.DMSLogResponse;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.dms.collection.DMSCreatedFolderInformation;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;

import java.util.List;
import java.util.Set;

/**
 * Created by mahesh on 24/6/17.
 */
public interface DMSRepository {


    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    LOSDetails getLOSInformation(String refId ,String institutionId);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    boolean checkFolderAvailability(String refId, String institutionId);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    Set<String> alreadyPushedDocumentNames(String refId, String institutionId);

    /**
     *
     * @param refId
     * @param institutionId
     * @param folderType
     * @return
     */
    long getFolderIdAgainstFolderName(String refId, String institutionId, String folderType);

    /**
     *
     * @param dmsCreatedFolderInformation
     */
    void saveCreatedFolderInformation(DMSCreatedFolderInformation dmsCreatedFolderInformation);

    /**
     *
     * @param dmsPushDocumentInformation
     */
    void savePushDocumentInformation(DMSPushDocumentInformation dmsPushDocumentInformation);

    /**
     *
     * @param institutionId
     * @return
     */
    boolean checkWFJobCommDomainConfiguration(String institutionId);

    /**
     *
     * @param institutionId
     * @param refId
     * @return
     */
    byte[] getCibilReportBase64Code(String institutionId, String refId);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    DMSLogResponse getPushDocumentsInfo(String refId, String institutionId);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    boolean checkApplicationAgreementFormExistance(String refId, String institutionId);

    /**
     *
     * @param institutionId
     * @param refId
     * @return
     */
    DMSPushDocumentInformation getSuccessFailureCountOfPushDocumentsWithCause(String institutionId, String refId);

    /**
     *
     * @param dmsErrorLog
     */
    void saveDmsErrorLog(PushDMSDocumentsRequest dmsErrorLog);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    List<PushDMSDocumentsRequest> getDMSErrorLog(String refId, String institutionId);
}
