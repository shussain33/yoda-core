package com.softcell.dao.mongodb.repository.security;

import com.softcell.gonogo.model.security.Versions;

import java.util.List;

/**
 * @author yogeshb
 */
public interface SecurityRepository {
    /**
     * @param versionRequest
     * @return
     */
    public boolean addAllowedVersion(String instition, String version);

    /**
     * @param versionRequest
     * @return
     */
    public boolean removeOldVersion(String instition, String version);

    /**
     * @param versionRequest
     * @return
     */
    public boolean updateVersion(String instition, String version);

    /**
     * @param versionRequest
     * @return
     */
    public List<Versions> showAllVersions();

    /**
     * @param version
     * @return
     */
    public boolean isValidVersion(String instition, String version);
}
