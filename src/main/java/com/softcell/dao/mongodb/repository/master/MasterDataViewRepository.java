package com.softcell.dao.mongodb.repository.master;

import com.mongodb.WriteResult;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.SourcingDetailsRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.UpdateCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.GetSchemeDateMappingDetailsRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.SchemeApproveDeclineRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.UpdateSchemeDateMappingDetailsRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.gonogo.model.response.Table;
import com.softcell.gonogo.model.response.master.commongeneralmaster.SourcingDetails;
import com.softcell.gonogo.model.response.master.schemedatemapping.AssetCostValidationResponse;
import com.softcell.gonogo.model.response.master.schemedatemapping.ManufacturerDescWithCcid;
import com.softcell.gonogo.model.response.master.schemedatemapping.SchemeInformationResponse;

import java.util.List;
import java.util.Set;

/**
 * @author mahesh
 */
public interface MasterDataViewRepository {
    /**
     * @param instututionId
     * @return
     */
    public Table getAssetModelDetails(String instututionId,int limit,int skip,String productAliasName);

    /**
     * @return
     */
    public List<SchemeInformationResponse> viewAllSchemeIds(String institutionId ,String productAliasName);

    /**
     * @param schemeDateMappingDetailsRequest
     * @return
     */
    public SchemeDetailsDateMapping viewSchemeDateMappingDeatils(
            GetSchemeDateMappingDetailsRequest schemeDateMappingDetailsRequest , String productAliasName) throws Exception;

    /**
     * @param assetCostValidationRequest
     * @return
     */
    public AssetCostValidationResponse validateAssetCost(
            AssetCostValidationRequest assetCostValidationRequest ,String productAliasName);

    /**
     * @param institutionId
     * @return
     */
    public Table getPinCodeMasterDetails(String institutionId,int limit,int skip);

    /**
     * @param institutionId
     * @return
     */
    public Table getEmployerMasterDetails(String institutionId,int limit,int skip);

    /**
     * @param institutionId
     * @return
     */
    public Table getDealerBranchMasterDetails(
            String institutionId,String product,int limit,int skip);

    /**
     * @param institutionId
     * @return
     */
    public Table getDealerEmailMasterDetails(
            String institutionId,int limit,int skip);

    /**
     * @param institutionId
     * @return
     */
    public Table getSchemeModelDealerCityMappingDetails(
            String institutionId,int limit,int skip);


    /**
     * @param getSchemeInformationRequest
     * @return
     */
    public Set<String> getManufacturerAgainstCcid(
            GetSchemeInformationRequest getSchemeInformationRequest ,String productAliasName) throws Exception;

    /**
     * @param institutionId
     * @return
     */
    public Set<String> getAllState(String institutionId) throws Exception;

    /**
     * @param getAllCityAgainstStateRequest
     * @return
     */
    public  Set<InclCityWithStateName> getAllCityAgainstState(
            GetAllCityAgainstStateRequest getAllCityAgainstStateRequest);

    /**
     * @param updateSchemeDealerMappingDetails
     * @return
     */
    boolean updateSchemeDealerMappingDetails(
            SchemeDealerMappingDetails updateSchemeDealerMappingDetails , String productAliasName) throws Exception;


    /**
     * @param getSchemeDealerMappingRequest
     * @return
     */
    public SchemeDealerMappingDetails getSchemeDealerMappingDataAgainstSchemeId(
            GetSchemeDealerMappingRequest getSchemeDealerMappingRequest ,String productAliasName) throws Exception;

    /**
     * @param assetCtgrAgainstMnfcRequest
     * @return
     */
    public Set<IncludedAssetCtgWithManfc> viewAllAssetCtgrAgainstManfctr(
            GetAllAssetCtgrAgainstMnfcRequest assetCtgrAgainstMnfcRequest ,String productAliasName);

    /**
     * @param getAllModelAgainstAsstCtgRequest
     * @return
     */
    public   Set<IncludedModelIdWithInformation> viewAllModelAgainstAsstCtgMnfc(
            GetAllModelAgainstAsstCtgRequest getAllModelAgainstAsstCtgRequest ,String productAliasName);

    /**
     * @param getAllDealerAgainstCityRequest
     * @return
     */
    public Set<InclDealerIdWithStateCity> viewAllDealerAgainstCity(
            GetAllDealerAgainstCityRequest getAllDealerAgainstCityRequest);

    /**
     * @param institutionId
     * @param dealerID
     * @return
     */
    public DealerEmailMaster getDealerDetails(String institutionId,
                                              String dealerID) throws Exception;

    /**
     * @param modelNo
     * @param catgDesc
     * @param maufacturerDesc
     * @param assetMake
     * @return
     */
    public AssetModelMaster getAssetModelMaster(String modelNo,
                                                String catgDesc, String maufacturerDesc, String assetMake ,String productAliasName ,String institutionId) throws Exception;

    /**
     * @param manufacturerId
     * @return
     */
    public Set<String> getNonVanillaSchemeIds(String manufacturerId ,String productAliasName , String institutionId) throws Exception;

    /**
     * @param ccid
     * @return
     */
    public List<SchemeMasterData> getVanillaSchemeMasterDetails(String ccid ,String productAliasName , String institutionId) throws Exception;

    /**
     * @param schemeSet
     * @return
     */
    public List<SchemeMasterData> getSchemeMasterData(Set<String> schemeSet ,String productAliasName ,String institutionId) throws Exception;

    /**
     * @param finalSchemeIdSet
     * @return
     */
    public List<SchemeDetailsDateMapping> getSchemeDetailsDateMapping(
            Set<String> finalSchemeIdSet ,String productAliasName ,String institutionId) throws Exception;

    /**
     * @return
     */
    public List<SchemeMasterData> getGeneralSchemeMasterDetails(String productAliasName ,String institutionId);

    /**
     * @param getAllAssetMakeAgainstMnfCtgRequest
     * @return
     */
    public  Set<IncludedMakeWithCtgMnfcr> getAssetMakeAgainstMnfcCtg(
            GetAllAssetMakeAgainstMnfCtgRequest getAllAssetMakeAgainstMnfCtgRequest ,String productAliasName) throws Exception;

    /**
     * This method are used to get all scheme against manufacturer against
     * manufacturer from scheme master.
     *
     * @param manufacturerId
     * @return
     */
    public List<SchemeInformationResponse> getSchemeAgainstManufacturer(
            String manufacturerId,String institutionId, String productAliasName);

    /**
     * This method are used to get all Vanilla scheme from scheme master.
     *
     * @return
     */
    public List<SchemeInformationResponse> getVanillaScheme(String institutionId, String productAliasName);

    /**
     * This method are used to get all General scheme from scheme master.
     *
     * @return
     */
    public List<SchemeInformationResponse> getGeneralScheme(String institutionId, String productAliasName);

    /**
     * @param getAllManufacturerRequest
     * @return
     */
    public List<ManufacturerDescWithCcid> getAllManufacturer(
            GetAllManufacturerRequest getAllManufacturerRequest ,String productAliasName );

    /**
     * @param schemeStatus
     * @return
     */
    public List<SchemeDetailsDateMapping> getAllSchemeDateMappingScheme(
            String schemeStatus ,String productAliasName ,String institutionId);

    /**
     * @param schemeID
     * @param typeOfScheme
     * @return
     */
    public SchemeDealerMappingDetails getApplicableAssetDetails(
            String schemeID, String typeOfScheme ,String productAliasName, String institutionId);

    /**
     * @param schemeApproveDeclineRequest
     * @return
     */
    public boolean changeSchemeStatus(
            SchemeApproveDeclineRequest schemeApproveDeclineRequest ,String productAliasName) throws Exception;

    /**
     * @param schemeApproveDeclineRequest
     * @return
     */
    public boolean checkCheckerDetails(
            SchemeApproveDeclineRequest schemeApproveDeclineRequest ,String productAliasName);

    /**
     * @param partialSchemeList
     * @param schemeStatus
     * @return
     */
    public List<SchemeDealerMappingDetails> getAllPartialDealerMappingSchemeData(
            List<String> partialSchemeList, String schemeStatus ,String productAliasName ,String institutionId);

    /**
     * @param schemeStatus
     * @return
     */
    public List<String> getAllPartialDealerMappingSchemeId(String schemeStatus ,String productAliasName ,String institutionId);

    /**
     * @param filterSchemeId
     * @param modelId
     * @return
     */
    public boolean checkModelSchemeMapping(String filterSchemeId, String modelId ,String productAliasName ,String institutionId);

    /**
     * @param filterSchemeId
     * @param dealerID
     * @return
     */
    public boolean checkDealerSchemeMapping(String filterSchemeId,
                                            String dealerID ,String productAliasName ,String institutionId);

    /**
     * @param schemeApproveDeclineRequest
     * @return
     */
    public boolean checkPartialSchemeDetails(
            SchemeApproveDeclineRequest schemeApproveDeclineRequest ,String productAliasName) throws Exception;

    /**
     *
     * @param updateSchemeDateMappingDetailsRequest
     * @param productAliasName
     * @return
     * @throws Exception
     */
    boolean updateSchemeDateMappingDetails(
            UpdateSchemeDateMappingDetailsRequest updateSchemeDateMappingDetailsRequest ,String productAliasName) throws Exception;

    /**
     *
     * @param uniqueIds
     * @param institutionId
     * @return
     */
    WriteResult deleteCommonGeneralMaster(List<String> uniqueIds, String institutionId);

    /**
     *
     * @param updateCommonGeneralMasterRequest
     * @return
     */
    WriteResult updateCommonGeneralParameterMaster(UpdateCommonGeneralMasterRequest updateCommonGeneralMasterRequest);

    /**
     *
     * @param commonGeneralParameterMaster
     * @return
     */
    void insertCommonGeneralMaster(CommonGeneralParameterMaster commonGeneralParameterMaster);

    /**
     *
     * @param institutionId
     * @param limit
     * @param skip
     * @return
     */
    Table getCommonGeneralMasterRecord(String institutionId, Integer limit, Integer skip);

    /**
     *
     * @param sourcingDetailsRequest
     * @return
     */
    SourcingDetails getSourcingDetails(SourcingDetailsRequest sourcingDetailsRequest);

    Table getAllElecServProviders(String institutionId);

    /**
     *
     * @param scheme
     * @param institutionId
     * @param productAliasName
     * @return
     */
    SchemeMasterData getSchemeDetails(String scheme, String institutionId, String productAliasName);

    /**
     *
     * @param institutionId
     * @return
     */
    String getAssetCatIdFromAssetCatMaster(String institutionId ,String assetCategory);


    List<DeviationMaster> fetchAllDeviationMasters(String institutionID, Product product);

    DeviationMaster fetchDeviationMastersRecord(String institutionID, Product product, String deviation, String category);

    List<OrganizationalHierarchyMaster> fetchAllHierarchyMasters(String institutionID);

    /*List<OrganizationalHierarchyMaster> fetchOrganizationalHierarchyMasters(String institutionId, Integer branchCode,
                                                                                     String userId, String productName);*/

    List<OrganizationalHierarchyMasterV2> fetchOrganizationalHierarchyMasters(String institutionId, Integer branchCode,
                                                                                     String userId, String productName);

    List<RoiSchemeMaster> getRoiSchemeMasterDetails(String institutionId, String product);

    RoiSchemeMaster getSchemeCompanySpecificData(String companyName, String scheme);

}
