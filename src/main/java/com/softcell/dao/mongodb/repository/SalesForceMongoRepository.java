package com.softcell.dao.mongodb.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sforce.soap.enterprise.*;
import com.sforce.soap.enterprise.Error;
import com.sforce.soap.enterprise.sobject.*;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import com.softcell.config.AmazonS3Configuration;
import com.softcell.config.SalesForceConfiguration;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.KycJsonKeys;
import com.softcell.constants.Product;
import com.softcell.constants.UploadFileTypes;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadInputStreamRequest;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.salesforcedetails.Errors;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.core.salesforcedetails.Success;
import com.softcell.gonogo.model.core.scoring.response.*;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.CroApprovalRequest;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.salesforce.SalesForceInfo;
import com.softcell.service.AmazonS3CloudStorageService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class SalesForceMongoRepository implements SalesForceRepository {
    public enum ApplicantType {
        BORROWER("Borrower"), COBORROWER("Co-Borrower") , REFERENCE("Reference");

        ApplicantType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        private String value;
    }
    public enum ObjectType {
        CONTACT_COBORROWER("Contact-Co-Borrower"), CONTACT_BORROWER("Contact-Borrower"),BORROWER_INCOME("Borrower-Income"), MB("Mb"), LEAD("Lead"),
        BANK_DETAILS("Bank-Details"), EMPLOYMENT("Employment"), LIABILITY("Liability"), LOAN_APP_SCORE("Loan-App-Score"),
        PRODUCT("Product"), LEAD_CRO("Lead-Cro-Decision"), INCOME_DETAIL("Income-Details");
        ObjectType(String values) {
            this.values = values;
        }
        public String getValues() {
            return values;
        }
        private String values;
    }

    private ModuleRequestRepository moduleRequestRepository;

    static final String SUCCESS = "SUCCESS";
    static final String ERROR = "ERROR";
    static final String SPACE = " ";

    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

    //initialized in init method.
    public static List<MultiBureauCoApplicantResponse> coApplicantMBResponses;

    static EnterpriseConnection connection;

    //Applicant Id and contact Id against it inserted id's and initialized in init method
    Map<String, String> applicantIdcontactIdMap;

    SalesForceInfo salesForceInfo = null;
    com.softcell.gonogo.model.core.salesforcedetails.Errors errors=null;
    Success success = null;
    SalesForceLog salesForceLog=null;
    private Logger logger = LoggerFactory.getLogger(SalesForceMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * @return the mongoTemplate
     */
    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    /**
     * @param mongoTemplate
     *            the mongoTemplate to set
     */
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Inject
    public SalesForceMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void connectSalesForce(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {
        logger.info("Inside connectSalesForce ");

        ApplicationRequest applicationRequest = goNoGoCustomerApplication
                .getApplicationRequest();

        SalesForceConfiguration salesForceConfiguration = Cache.URL_CONFIGURATION
				.getSalesForceConfiguration().get(
                        applicationRequest.getHeader().getInstitutionId());

        ConnectorConfig config=getConnectorConfig(applicationRequest.getHeader().getInstitutionId());
        init(goNoGoCustomerApplication);
        try {

            connection = Connector.newConnection(config);
            logger.info("SALES FORCE START");
            logger.info("Auth EndPoint: " + config.getAuthEndpoint());
            logger.info("Service EndPoint: "
                    + config.getServiceEndpoint());
            logger.info("Username: " + config.getUsername());
            logger.info("SessionId: " + config.getSessionId());


            String leadId = this.createLead(goNoGoCustomerApplication,salesForceConfiguration);

            salesForceInfo = populateSalesForceInfo(leadId, applicationRequest,
                    null);
            saveInDb(salesForceInfo);
            logger.info("SALES FORCE END");

        } catch (Exception e) {
            logger.error("Error saving details in SalesForce cause {}",e.getMessage());
            salesForceInfo = populateSalesForceInfo(null, applicationRequest,
                    e.toString());
            saveInDb(salesForceInfo);

            salesForceLog = salesForceResponseDetails(null, e.getMessage(), null, goNoGoCustomerApplication.getApplicationRequest().getRefID()
                    , goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            saveSfResponse(salesForceLog);
        }

        logger.info("Successfully created lead connectSalesForce ");
    }

    /**
     * This method initializes all the objects which are used later in all methods.
     * @param goNoGoCustomerApplication
     */
    private void init(GoNoGoCustomerApplication goNoGoCustomerApplication){
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        applicantIdcontactIdMap = new HashMap<String, String>();
        coApplicantMBResponses = moduleRequestRepository
                .getCoApplicantResponseByAppRefId(goNoGoCustomerApplication
                        .getGngRefId());
    }


    /**
     * It populates Application Id, Status and Lead Id
     *
     * @param leadId
     *            Lead id for the lead object get created on sales force
     * @param applicationRequest
     *            applicationRequest Object to get ref Id
     * @return
     */
    private SalesForceInfo populateSalesForceInfo(String leadId,
                                                  ApplicationRequest applicationRequest, String exception) {
        salesForceInfo = new SalesForceInfo();
        salesForceInfo.setApplicationRefId(applicationRequest.getRefID());
        salesForceInfo.setContactIdApplicantIdMap(applicantIdcontactIdMap);
        if (exception != null && !"".endsWith(exception)) {
            salesForceInfo.setStatus(exception);
        } else if (leadId != null) {
            salesForceInfo.setLeadId(leadId);
            salesForceInfo.setStatus(SUCCESS);
        } else {
            salesForceInfo.setStatus(ERROR);
        }
        return salesForceInfo;
    }

    /**
     * It save the applicationRequestId , Lead Id and status in SalesforceInfo
     * collection
     *
     * @param salesForceInfo
     *            {@link SalesForceInfo is}
     */
    private void saveInDb(SalesForceInfo salesForceInfo) {
        logger.info("Saving SalesForce information ");
        mongoTemplate.save(salesForceInfo);
        logger.info("Saved SalesForce information ");
    }

    /**
     *
     * @param salesForceLog
     */
    private void saveSfResponse(SalesForceLog salesForceLog) {
        logger.info("Saving SalesForce Log Details ");
        Query query = new Query();
        try{
            if(salesForceLog!=null){
        query.addCriteria(Criteria.where("_id").is(salesForceLog.getRefId()).
                and("header.institutionId").is(salesForceLog.getHeader().getInstitutionId()));
        Update update = new Update();

        if(salesForceLog.getLeadId()!=null){

            update.set("leadId", salesForceLog.getLeadId());
            }
        if (salesForceLog.getSuccess() != null) {
            update.push("success").each(salesForceLog.getSuccess().toArray());
        }
        if (salesForceLog.getError() != null) {
            update.push("error").each(salesForceLog.getError().toArray());
        }
            mongoTemplate.upsert(query, update, SalesForceLog.class);
        }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("Error saving salesForce Log Details for refId cause {}",salesForceLog.getRefId(),e.getMessage());
        }

    }



    /**
     * It create lead object in Sales Force.
     *
     *            Application request contains data related to applicant using
     *            which Lead , Contact and BankDetails is Populated
     *
     * @return If Object is created successfully LeadId is return Else Exception
     *         or Error is return
     * @throws Exception
     */
    public String createLead(GoNoGoCustomerApplication goNoGoCustomerApplication,SalesForceConfiguration salesForceConfig)
            throws Exception {

        ApplicationRequest applicationRequest = goNoGoCustomerApplication
                .getApplicationRequest();
        logger.info("Inside createLead ");

        String leadId = null;


        if (applicationRequest != null) {
            Request request = applicationRequest.getRequest();
            Lead lead = new Lead();

            setScoringDetails(goNoGoCustomerApplication,lead);

            leadId = saveLeadDetails(goNoGoCustomerApplication,lead,salesForceConfig);

            if (leadId != null) {
                if (request.getApplicant() != null) {
                    saveApplicantInfo(goNoGoCustomerApplication, leadId);

                    //Save the reference of the Applicant.
                    saveApplicantReferenceInfo(request.getApplicant()
                                    .getApplicantReferences(), leadId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                            goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

                    //Save scoring details if loan type is not personal loan.
                    if (!request.getApplication().getLoanType()
                            .equalsIgnoreCase(Product.PL.name())) {
                        if (request.getApplication() != null) {
                            saveProductDetails(request, leadId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                        }
                    }
                }
                if (request.getCoApplicant() != null) {

                    //Save the co-applicant details in Sales Force.
                    saveCoApplicantInfo(goNoGoCustomerApplication, leadId);
                }
            }
        }

        logger.info("Lead created is " + leadId + " in createLead ");
        return leadId;
    }

    /**
     * This method saves income details of the contact based on it's employment type.
     *
     * @param incomeDetails Income Details from GNG application
     * @param employment employment details from GNG application.
     * @param contactId contactId of SalesForce against which income details will be saved.
     */
    private void saveIncomeDetails(IncomeDetails incomeDetails,
                                   Employment employment, String contactId, Application application,String refid, ObjectType objectType, String instId) {

        Income_Detail__c incomeDetailsSF = null;

        if (employment.getEmploymentType().equalsIgnoreCase("Salaried")) {
            incomeDetailsSF = getSalariedDetails(incomeDetails
                    .getSalariedDetails());
        } else {
            incomeDetailsSF = getSenpIncomeDetails(incomeDetails
                    .getSenpProfileIncome(),incomeDetails.getSalariedDetails().getObligations());
        }
        incomeDetailsSF.setIncome_Document_Available__c(incomeDetails
                .getIncomeDocumentAvailable());
        //check if application is not null.
        if (application != null)
            incomeDetailsSF.setComfortable_EMI_by_Customer__c(application
                    .getEmi());
        incomeDetailsSF.setContact__c(contactId);

        try {

            save(incomeDetailsSF,refid, String.valueOf(objectType), instId);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error saving income details of contact {} cause {}",contactId, e.getMessage());
            salesForceLog = salesForceResponseDetails(null, e.getMessage(),String.valueOf(objectType), refid, instId);
            saveSfResponse(salesForceLog);
        }
    }

    /**
     *
     * @param salariedDetails Salaried income details from GNG application.
     * @return SalesForce object with salaried details.
     */
    private Income_Detail__c getSalariedDetails(SalariedDetails salariedDetails){

        Income_Detail__c incomeDetailsSF = new Income_Detail__c();

        incomeDetailsSF.setBasic__c(salariedDetails.getBasic());
        incomeDetailsSF.setCCA__c(salariedDetails.getCCA());
        incomeDetailsSF.setDA__c(salariedDetails.getDA());
        incomeDetailsSF.setPF__c(salariedDetails.getPF());
        incomeDetailsSF.setProfessional_Tax__c(salariedDetails
                .getProfessionalTax());
        incomeDetailsSF.setHRA__c(salariedDetails.getHRA());
        incomeDetailsSF.setLIC__c(salariedDetails.getLIC());
        incomeDetailsSF.setESI__c(salariedDetails.getESI());
        incomeDetailsSF.setIncentive__c(salariedDetails.getIncentive());
        incomeDetailsSF.setOther_Deduction__c(salariedDetails
                .getOtherDeductions());
        incomeDetailsSF.setOther_Income__c(salariedDetails.getOther());
        incomeDetailsSF.setIncome_from_Other_source__c(salariedDetails
                .getOtherSourceIncome());
        incomeDetailsSF.setLess_Obligation__c(salariedDetails
                .getObligations());

        return incomeDetailsSF;
    }

    /**
     *
     * @param senpIncomeDetails Self Employed Income details from GNG application
     * @return Sales Force object with self employed income details.
     */
    private Income_Detail__c getSenpIncomeDetails(List<SENPProfileIncome> senpIncomeDetails,double obligation){

        Income_Detail__c incomeDetailsSF = new Income_Detail__c();
        incomeDetailsSF.setLess_Obligation__c(obligation);
        double netProfit = 0f;
        double depriciation = 0f;
        int yearCount = 0;
        double otherIncome = 0f;
        for (int i = 0; i < senpIncomeDetails.size(); i++) {
            SENPProfileIncome senpProfileIncome = senpIncomeDetails.get(i);
            switch (i) {
                case 0:
                    incomeDetailsSF
                            .setIncome_Year_1__c(senpProfileIncome.getYear());
                    incomeDetailsSF.setTurnover_Year_1__c(senpProfileIncome
                            .getTurnOver());
                    incomeDetailsSF.setNet_Profit_Year_1__c(senpProfileIncome
                            .getNetProfit());
                    incomeDetailsSF
                            .setDate_of_ITR_Filling_Year_1__c(convertStringToCalDate(senpProfileIncome
                                    .getItrDate()));
                    incomeDetailsSF.setDepreciation_Year_1__c(senpProfileIncome
                            .getDepreciation());
                    incomeDetailsSF.setOther_Income_Year_1__c(senpProfileIncome
                            .getOtherIncome());

                    netProfit = senpProfileIncome.getNetProfit();
                    depriciation = senpProfileIncome.getDepreciation();
                    otherIncome = senpProfileIncome.getOtherIncome();

                    yearCount += 1;
                    break;
                case 1:
                    incomeDetailsSF
                            .setIncome_Year_2__c(senpProfileIncome.getYear());
                    incomeDetailsSF.setTurnover_Year_2__c(senpProfileIncome
                            .getTurnOver());
                    incomeDetailsSF.setNet_Profit_Year_2__c(senpProfileIncome
                            .getNetProfit());
                    incomeDetailsSF
                            .setDate_of_ITR_Filling_Year_2__c(convertStringToCalDate(senpProfileIncome
                                    .getItrDate()));
                    incomeDetailsSF.setDepreciation_Year_2__c(senpProfileIncome
                            .getDepreciation());
                    incomeDetailsSF.setOther_Income_Year_2__c(senpProfileIncome
                            .getOtherIncome());

                    netProfit += senpProfileIncome.getNetProfit();
                    depriciation += senpProfileIncome.getDepreciation();
                    otherIncome += senpProfileIncome.getOtherIncome();
                    yearCount += 1;

                    break;
                case 2:
                    incomeDetailsSF
                            .setIncome_Year_3__c(senpProfileIncome.getYear());
                    incomeDetailsSF.setTurnover_Year_3__c(senpProfileIncome
                            .getTurnOver());
                    incomeDetailsSF.setNet_Profit_Year_3__c(senpProfileIncome
                            .getNetProfit());
                    incomeDetailsSF
                            .setDate_of_ITR_Filling_Year_3__c(convertStringToCalDate(senpProfileIncome
                                    .getItrDate()));
                    incomeDetailsSF.setDepreciation_Year_3__c(senpProfileIncome
                            .getDepreciation());
                    incomeDetailsSF.setOther_Income_Year_3__c(senpProfileIncome
                            .getOtherIncome());
                    break;
            }
        }
        double grossAnnual = ((netProfit + depriciation + otherIncome) / yearCount);
        //double netMonthly =
        return incomeDetailsSF;
    }

    /**
     * It is use to convert String date into calendar date
     *
     * @param dateString
     *            String date which is converted into calendar date;
     * @return
     */
    private Calendar convertStringToCalDate(String dateString) {
        Calendar dateCal = null;
        if (StringUtils.isNotBlank(dateString)) {
            try {
                dateCal = Calendar.getInstance();
                Date date = sdf.parse(dateString);
                dateCal.setTime(date);
                dateCal.add(Calendar.DATE, 1);
            } catch (ParseException e) {
                dateCal = null;
                e.printStackTrace();
            }
        }
        return dateCal;
    }

    /**
     * @return Liability Object with applicant existing loan details filled.
     */
    private void saveLiabilityDetails(List<LoanDetails> loanDetailsList,String contactId,String refid, String instId) {

        if (loanDetailsList != null && loanDetailsList.size() > 0) {
            int size = loanDetailsList.size();

            for (int i = 0; i < size; i++) {
                LoanDetails loanDetails = loanDetailsList.get(i);
                Liability__c liability = new Liability__c();
                liability.setLoan_Ownership__c(loanDetails.getLoanOwnership());
                liability.setLoan_Type__c(loanDetails.getLoanType());
                liability.setLoan_Account_Number__c(loanDetails
                        .getLoanAcountNo());
                liability.setLoan_Availed_From__c(loanDetails
                        .getCreditGranter());
                liability.setLoan_Amount__c(loanDetails.getLoanAmt());
                liability.setEMI_Amount__c(loanDetails.getEmiAmt());
                liability.setMOB__c((double) loanDetails.getMob());
                liability.setBalance_Tenore__c((double) loanDetails
                        .getBalanceTenure());
                liability.setRepayment_from_which_Bank_A_c__c(loanDetails
                        .getRepaymentBankName());
                liability.setObligate__c(loanDetails.getObligate());
                liability.setContact__c(contactId);

                try {
                  save(liability, refid, String.valueOf(ObjectType.LIABILITY), instId);

                } catch (Exception e) {
                    //local test to remove sysout
                    e.printStackTrace();
                    logger.error("Error saving liability details of contact {} cause {}",contactId, e.getMessage());
                    salesForceLog = salesForceResponseDetails(null, e.getMessage(),String.valueOf(ObjectType.LIABILITY), refid, instId);
                    saveSfResponse(salesForceLog);

                }
            }
        }
    }

    /**
     * Lead object is populated based on the request application.
     *
     * @return Lead Object which contains applicant information
     */
    private String saveLeadDetails(GoNoGoCustomerApplication goNoGoCustomerApplication,Lead lead,SalesForceConfiguration salesForceConfig) {

        Request request = goNoGoCustomerApplication.getApplicationRequest().getRequest();
        String leadId = null;
        Applicant applicant = request.getApplicant();
        Application application = request.getApplication();

        lead.setDOB__c(convertStringToCalDate(applicant.getDateOfBirth()));

        if (application.getProperty() != null) {
            lead.setLoan_Purpose__c(application.getProperty().getPropertyName()
                    + "/" + application.getProperty().getPurpose());
            lead.setRemarks__c(application.getProperty().getRemark());
        }

        if (applicant != null) {

            lead.setFirstName(applicant.getApplicantName().getFirstName());
            lead.setMiddleName(applicant.getApplicantName().getMiddleName());
            lead.setLastName(applicant.getApplicantName().getLastName());

            if (applicant.getFatherName() != null) {
                lead.setFather_Name__c(applicant.getFatherName().getFirstName()
                        + " " + applicant.getFatherName().getMiddleName() + " "
                        + applicant.getFatherName().getLastName());

            }

            lead.setGender__c(applicant.getGender());
            // Set occupation(Salaried or employed)
            if (applicant.getEmployment() != null
                    && applicant.getEmployment().size() > 0) {
                lead.setSector__c(applicant.getEmployment().get(0)
                        .getEmploymentType());
            }

            // set lead address details
            setAddressDetails(lead, applicant.getAddress());

            // set lead kyc details
            setKycDetails(lead, applicant.getKyc());

            // set lead phone details
            setPhoneDetails(lead, applicant.getPhone());

            // set lead email details.
            setEmailDetails(lead, applicant.getEmail());

            if (applicant.getIncomeDetails() != null) {
                boolean isSalaried = false;
                if (applicant.getEmployment().get(0).getEmploymentType()
                        .equals("Salaried")) {
                    isSalaried = true;
                }

                lead.setGross_Salary_Yearly__c(getGrossAnnualIncome(
                        applicant.getIncomeDetails(), isSalaried));

                lead.setNet_Salary_Monthly__c(getNetMonthlyIncome(
                        applicant.getIncomeDetails(), isSalaried));
            }
        }

        if (application != null) {
            // income details
            lead.setAmount_in_Rs__c(application.getLoanAmount());
            lead.setProposed_EMI__c(application.getEmi());

        }
        lead.setApplication_Id__c(goNoGoCustomerApplication.getGngRefId());
        lead.setApplication_Form_No__c("GG" + goNoGoCustomerApplication.getApplicationRequest().getApplicationFormNumber());
        lead.setProperty_Selected__c(application.getProperty().getStatus());
        lead.setCollateral_Type__c("None");
        lead.setStatus("Open");
        lead.setLoan_Officer__c(goNoGoCustomerApplication.getApplicationRequest().getHeader().getDsaId());
        AppMetaData appMetaData = goNoGoCustomerApplication.getApplicationRequest().getAppMetaData();

        if (null != appMetaData.getBranchV2() && StringUtils.isNotBlank(appMetaData.getBranchV2().getBranchName())) {

            lead.setBranch__c(appMetaData.getBranchV2().getBranchName());

        } else if (null != appMetaData && null != appMetaData.getBranch()) {

            lead.setBranch__c(appMetaData.getBranch().getBranchName());

        }

        Application loanApplication = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication();

        if (salesForceConfig.getBusinessTypes() != null){
            String businessType = salesForceConfig.getBusinessTypes()
                    .get(Product.getProductNameFromEnumName(loanApplication.getLoanType()));
            lead.setBusiness_Type__c(businessType);
            if (StringUtils.equals(loanApplication.getLoanType(), Product.PL.name())) {
                lead.setLoan_Tenor_in_Month__c((double) loanApplication.getLoanTenor());
            }
        }

        if(salesForceConfig.getRecordTypes() != null){
            String recordType = salesForceConfig.getRecordTypes()
                    .get(Product.getProductNameFromEnumName(loanApplication.getLoanType()));
            lead.setRecordType_List__c(recordType);
        }
        lead.setLeadSource(salesForceConfig.getLeadSource());
        lead.setLeadSource_S__c(salesForceConfig.getLeadSource());

        lead.setSecurity_del__c("dummy");
        lead.setIndustry("NoneInd");
        lead.setCIN__c(salesForceConfig.getCin());
        lead.setUser_Id__c("");

        try {
            leadId = save(lead, goNoGoCustomerApplication.getApplicationRequest().getRefID(), String.valueOf(ObjectType.LEAD),
            goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error saving lead for refID {} cause {}",goNoGoCustomerApplication.getGngRefId(),e.getMessage());
            salesForceLog = salesForceResponseDetails(null, e.getMessage(), String.valueOf(ObjectType.LEAD),
                    goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            saveSfResponse(salesForceLog);
        }
        return leadId;
    }

    /**
     * Populate pan detail from applicant KYC details
     *
     * @param kycList
     *            KYC details of applicant
     */
    private void setKycDetails(SObject sObject, List<Kyc> kycList) {

        if (sObject instanceof Lead) {
            Lead lead = (Lead) sObject;

            String idNoProofType = "";

            for (Kyc kyc : kycList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.ID_NO_PROOF.name(),kyc.getKycName())) {
                    idNoProofType = kyc.getKycNumber();
                }
            }

            for (Kyc kyc : kycList) {
                if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.PAN.name(), kyc.getKycName())) {
                    if (StringUtils.isNotBlank(kyc.getKycNumber()))
                        lead.setPAN__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.VOTER_ID.name(), kyc
                        .getKycName())) {
                    lead.setVoter_Id__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.AADHAAR.name(), kyc
                        .getKycName())) {
                    lead.setAadhaar_Number__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.DRIVING_LICENSE.name(), kyc.getKycName())) {
                    lead.setDriving_License__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.PASSPORT.name(), kyc
                        .getKycName())) {
                    lead.setPassport_Id__c(kyc.getKycNumber());
                } else if (KycJsonKeys.ID_NO.equalsIgnoreCase(kyc.getKycName())) {
                    //Set ID number according to Id type.
                    if (KycJsonKeys.IdNoValues.AADHAR_CARD
                            .equalsIgnoreCase(idNoProofType)) {
                        lead.setAadhaar_Number__c(kyc.getKycNumber());
                    } else if (KycJsonKeys.IdNoValues.VALID_PASSPORT.equals(idNoProofType)) {
                        lead.setPassport_Id__c(kyc.getKycNumber());
                    } else if (KycJsonKeys.IdNoValues.VOTERS_CARD.equals(idNoProofType)) {
                        lead.setVoter_Id__c(kyc.getKycNumber());
                    } else if (KycJsonKeys.IdNoValues.PAN_CARD.equals(idNoProofType)) {
                        lead.setPAN__c(kyc.getKycNumber());
                    }else if (KycJsonKeys.IdNoValues.DRIVING_LICENSE.equals(idNoProofType)){

                        lead.setDriving_License__c(kyc.getKycNumber());
                    }
                }
            }
        }

        if (sObject instanceof Contact) {
            String idNoProofType = "";
            Contact contact = (Contact) sObject;

            for (Kyc kyc : kycList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.ID_NO_PROOF.name(), kyc.getKycName())) {
                    idNoProofType = kyc.getKycNumber();
                }
            }

            for (Kyc kyc : kycList) {
                if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.PAN.name(), kyc.getKycName())) {

                    contact.setPAN_ID__c(kyc.getKycNumber());

                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.VOTER_ID.name(), kyc
                        .getKycName())) {
                    contact.setVoter_Id__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.AADHAAR.name(), kyc
                        .getKycName())) {
                    contact.setAadhaar_Number__c(kyc.getKycNumber());
                }
                if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.DRIVING_LICENSE.name(), kyc.getKycName())) {

                    contact.setDriving_License__c(kyc.getKycNumber());
                } else if (StringUtils.endsWithIgnoreCase(GNGWorkflowConstant.PASSPORT.name(), kyc
                        .getKycName())) {

                    contact.setPassport_Id__c(kyc.getKycNumber());
                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DOB_PROOF_TYPE.name(), kyc
                        .getKycName())) {

                    contact.setDOB_Proof_Type__c(kyc.getKycNumber());

                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERMANENT_ADDRESS_PROOF_TYPE.name(), kyc.getKycName())) {

                    contact.setPermanent_Address_Proof_Type__c(kyc
                            .getKycNumber());

                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PRESENT_ADDRESS_PROOF_TYPE.name(), kyc.getKycName())) {

                    contact.setPresent_Address_Proof_Type__c(kyc.getKycNumber());

                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.SIGNATURE_PROOF_TYPE.name(), kyc.getKycName())) {

                    contact.setSignature_Proof_Type__c(kyc.getKycNumber());

                } else if (KycJsonKeys.ID_NO.equalsIgnoreCase(kyc.getKycName())) {

                    if (KycJsonKeys.IdNoValues.AADHAR_CARD
                            .equalsIgnoreCase(idNoProofType)) {

                        contact.setAadhaar_Number__c(kyc.getKycNumber());

                    } else if (KycJsonKeys.IdNoValues.VALID_PASSPORT.equals(idNoProofType)) {

                        contact.setPassport_Id__c(kyc.getKycNumber());

                    } else if (KycJsonKeys.IdNoValues.VOTERS_CARD.equals(idNoProofType)) {

                        contact.setVoter_Id__c(kyc.getKycNumber());

                    } else if (KycJsonKeys.IdNoValues.BANK_PASSBOOK_PHOTO
                            .equals(idNoProofType)) {

                        contact.setBank_Account_Id__c(kyc.getKycNumber());

                    } else if (KycJsonKeys.IdNoValues.PAN_CARD.equals(idNoProofType)) {
                        contact.setPAN_ID__c(kyc.getKycNumber());
                    }else if (KycJsonKeys.IdNoValues.DRIVING_LICENSE.equals(idNoProofType)){

                        contact.setDriving_License__c(kyc.getKycNumber());
                    }
                }
            }
        }
    }

    /**
     * Add scoring details under lead object.
     */
    @SuppressWarnings("unchecked")
    private void setScoringDetails(
            GoNoGoCustomerApplication goNoGoCustomerApplication,Lead lead) {
        // Get the componenet response from goNoGoCustomerApplication.
        ComponentResponse applicantComponentsResponse = goNoGoCustomerApplication
                .getApplicantComponentResponse();
        ScoringResponse scoringResponse = null;
        lead.setApplication_Status__c(goNoGoCustomerApplication
                .getApplicationStatus());
		/*
		 * Check if applicantComponenetsResponse and scoringService response is
		 * not null
		 */
        if(applicantComponentsResponse != null &&
                (scoringResponse = applicantComponentsResponse
                .getScoringServiceResponse()) != null) {

            DecisionResponse decisionResponse = scoringResponse.getDecisionResponse();
            //Set decision came from scoring.
            if (decisionResponse != null) {
                lead.setCredit_Decision__c(decisionResponse.getDecision());
                if (decisionResponse.getDetails() != null
                        && decisionResponse.getDetails().size() > 0) {
                    lead.setCredit_Decision_Remark__c(decisionResponse
                            .getDetails().get(0).getRemark());
                }
            }else{
                setExtraLeadDetails(goNoGoCustomerApplication,lead);
            }

            if (scoringResponse.getEligibilityResponse() != null) {
                try {
                    // if scoreData is not null then set the application score.
                    if (scoringResponse.getScoreData() != null
                            && !StringUtils.isBlank(scoringResponse.getScoreData()
                            .getScoreValue())) {
                        double appScore = 0.0f;
                        if (!StringUtils.isBlank(scoringResponse.getScoreData()
                                .getFinalScore())) {
                            appScore = Double.valueOf(scoringResponse
                                    .getScoreData().getFinalScore());
                        } else {
                            appScore = Double.valueOf(scoringResponse
                                    .getScoreData().getScoreValue());
                        }
                        lead.setApplication_Score__c(String.valueOf(appScore));
                    }


                    //Set eligibility details.
                    Eligibility eligibilityResponse = scoringResponse
                            .getEligibilityResponse();
                    ExtendedEligibility extendedEligibility = null;
                    // Check if eligibility response and its Additional properties
                    // are not null.
                    if (eligibilityResponse != null
                            && eligibilityResponse.getAdditionalProperties() != null
                            && (extendedEligibility = eligibilityResponse.getExtendedEligibilty()) != null) {

                        lead.setAvailable_Income_FOIR__c(extendedEligibility.getAvailableIncomeFOIR());

                        lead.setAvailable_Income_INSR__c(extendedEligibility.getAvailableIncomeINSR());

                        lead.setIncome_Towards_Loan_Repay__c(extendedEligibility.getIncomeAvailableTowardsLoanRepay());

                        lead.setEMI_Per_Lakh__c(extendedEligibility.getEmiPerLakh());

                        lead.setEMI__c(extendedEligibility.getActualEmi());

                        lead.setFOIR__c(extendedEligibility.getActualFOIRPercentage());

                        lead.setINSR__c(extendedEligibility.getActualINSRPercentage());

                        lead.setLCR__c(extendedEligibility.getPercentLCRCalculated());

                        lead.setLTV__c(extendedEligibility.getPercentLTVCalculated());

                        lead.setLoan_Rate__c(extendedEligibility.getRateOfInterest());

                        lead.setLoan_Tenor_in_Month__c(extendedEligibility.getMaxTenure());

                        //Commented out ROI_C as it does not exist in DMI Production.
                        //lead.setROI__c(extendedEligibility.getRateOfInterest());

                        lead.setEligible_Loan_Amount_INR__c(scoringResponse
                                .getEligibilityResponse().getEligibilityAmount());
                    }

                } catch (Exception e) {
                    logger.error(e.toString(), e.getCause());
                }
            }
        }else{
            setExtraLeadDetails(goNoGoCustomerApplication, lead);
        }
    }

    private void setExtraLeadDetails(GoNoGoCustomerApplication gngCustApp,Lead lead){

        if(null != gngCustApp.getApplicationRequest()
          && null != gngCustApp.getApplicationRequest().getRequest()
          && null != gngCustApp.getApplicationRequest().getRequest().getApplication()){

            lead.setLoan_Tenor_in_Month__c((double) gngCustApp.getApplicationRequest().getRequest().getApplication().getLoanTenor());
            lead.setCredit_Decision__c(GNGWorkflowConstant.QUEUED.toFaceValue());
        }
    }

    @SuppressWarnings("unchecked")
    private void saveScoreTreeDetails(Object scoreTree, String contactId , String refid, String instId )
            throws Exception {
        Map<String, Object> masterMap = (Map<String, Object>) ((Map<String, Object>) scoreTree)
                .get("masterMap");
		/*
		 * NOTE:- Following code is written on the assumption that there will be
		 * three Attributes for each category in the score-table for DMI
		 */

        // Score table entry map i.e Attribute as key(SALARIED or
        // SELF-EMPLOYED)
        // and remaining as value.
        Set<Map.Entry<String, Object>> scoreTableCatEntries = masterMap
                .entrySet();

        // attributeMap i.e key may be CAPACITY,COLLATERAL,CHARACTER and
        // remaining data as value.
        Map<String, Object> attributeMap = null;

        for (Map.Entry<String, Object> entry : scoreTableCatEntries) {
            attributeMap = (Map) entry.getValue();
            break;
        }
        // List to hold the RuleName and Rule Score.

        if (attributeMap != null) {
            Set<Map.Entry<String, Object>> attrMapEntries = attributeMap
                    .entrySet();
            // iterating on values of keys(CAPACITY,COLLATERAL,CHARACTER)
            for (Map.Entry<String, Object> entry : attrMapEntries) {

                // This map will contain actulal field names and the score
                // values.
                Map<String, Object> ruleCalculationsMap = (Map<String, Object>) entry
                        .getValue();
                // iterate furthermore for getting the fieldNames and values
                if (ruleCalculationsMap != null) {
                    for (Map.Entry<String, Object> ruleEntry : ruleCalculationsMap
                            .entrySet()) {
                        // TODO: Need to ask DMI to create the <key,value>
                        // pair for this thing.
                        // eg:- FieldName will be key and fieldScore and
                        // fieldValue will be values.
                        Map<String, Object> mapValue = (Map<String, Object>) ruleEntry
                                .getValue();
                        if (mapValue == null)
                            continue;
                        Loan_App_Score__c loanAppScore_c = new Loan_App_Score__c();
                        String fieldName = mapValue.get("FieldName").toString();
                        String fieldScore = mapValue.get("cScore").toString();

                        loanAppScore_c.setApplication_Name__c(fieldName);
                        loanAppScore_c.setApplication_Score__c(fieldScore);
                        loanAppScore_c.setContact__c(contactId);
                        try {
                            save(loanAppScore_c, refid, String.valueOf(ObjectType.LOAN_APP_SCORE) + " " + fieldName, instId);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                            logger.error("Error saving loan application score details {} cause {}",contactId, e.getMessage());
                            salesForceLog = salesForceResponseDetails(null, e.getMessage(),
                                    String.valueOf(ObjectType.LOAN_APP_SCORE), refid, instId);
                            saveSfResponse(salesForceLog);
                        }
                    }
                }
            }
        }
    }


    /**
     *
     * Save method is called to create sObject i.e(Lead, Contact, BankDetails)
     * it returns Unique id if record created successfully else throw an
     * exception
     *
     * @param sobject
     *            sObject is parent class for Lead , Contact and BankDetails
     *            Type parameter mention which type of sObject is to save
     *            Possible values are LEAD, CONTACT, BANK
     * @return It return primary key for the object created on Sales Force
     * @throws Exception
     */
    public String save(SObject sobject,String refid, String objType, String instId) throws Exception {
        logger.info("Inside create lead save to sales force  ");

        SObject[] records = { sobject };
        String errorsMessage = "Error while saving " + sobject.getClass().getName() + " : ";

        SaveResult[] saveResults;
        // try {
        saveResults = connection.create(records);

        // check the returned results for any errors
        for (int i = 0; i < saveResults.length; i++) {
            if (saveResults[i].isSuccess()) {
                String id = saveResults[i].getId();
                logger.info("Successfully created" + sobject.getClass().getName() + " record - Id:"
                        + id);
                salesForceLog = salesForceResponseDetails(id, null, objType, refid, instId);
                saveSfResponse(salesForceLog);
                return id;
            } else {
                com.sforce.soap.enterprise.Error[] errors = saveResults[i]
                        .getErrors();
                for (int j = 0; j < errors.length; j++) {
                    errorsMessage += errors[j].getMessage() + SPACE;
                    String[] errorsFields = errors[j].getFields();
                    for (int k = 0; k < errorsFields.length; k++) {
                        errorsMessage += errorsFields[k] + SPACE;
                    }

                }

                logger.info("Error created" + sobject.getClass().getName() + " record -"
                        + errorsMessage);
                salesForceLog = salesForceResponseDetails(null, errorsMessage, objType, refid, instId);
                saveSfResponse(salesForceLog);
                throw new Exception(errorsMessage);
            }
        }

        return null;
    }

    /**
     * It concatenate First , Middle Name and Last Name
     *
     * @param name
     *            Name object consist of first name , Middle Name and Last Name
     * @return
     */
    private static String getName(Name name) {
        String nameStr = "";
        if (name != null) {
            String nameAppend = name.getFirstName();
            if (nameAppend != null && !("".equals(nameAppend))) {
                nameStr += nameAppend + SPACE;
            }
            nameAppend = name.getMiddleName();
            if (nameAppend != null && !("".equals(nameAppend))) {
                nameStr += nameAppend + SPACE;
            }
            nameAppend = name.getLastName();
            if (nameAppend != null && !("".equals(nameAppend))) {
                nameStr += nameAppend;
            }
        }
        return nameStr;
    }

    @Override
    public SalesForceInfo getByApplicationRefId(String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("applicationRefId").is(refId));
        return mongoTemplate.findOne(query, SalesForceInfo.class);
    }

    @Override
    public boolean updateAmazonS3UrlInSalesForce(String amazonS3Urls,
                                                 String leadId, String institutionId, boolean isProfilePic,
                                                 UploadFileDetails uploadDetails, SalesForceInfo salesForceInfo,
                                                 String applicantId) {

        boolean result = true;
        ConnectorConfig config = getConnectorConfig(institutionId);
        try {
            boolean contactUpdated = false;
            connection = Connector.newConnection(config);
            QueryResult res = connection
                    .query("Select id from  Lead where id = " + "'" + leadId
                            + "'");
            Lead lead = (null != res && res.getSize() > 0) ? (Lead) res
                    .getRecords()[0] : null;

            if (lead != null) {

                switch (uploadDetails.getFileType()) {
                    case UploadFileTypes.OTHER1:
                        lead.setAdditional_Image1_Proof__c(amazonS3Urls);
                        lead.setAdditional_Image1_Name__c(uploadDetails
                                .getFileName());
                        break;
                    case UploadFileTypes.OTHER2:
                        lead.setAdditional_Image2_Proof__c(amazonS3Urls);
                        lead.setAdditional_Image2_Name__c(uploadDetails
                                .getFileName());
                        break;
                    case UploadFileTypes.OTHER3:
                        lead.setAdditional_Image3_Proof__c(amazonS3Urls);
                        lead.setAdditional_Image3_Name__c(uploadDetails
                                .getFileName());
                        break;
                    case UploadFileTypes.OTHER4:
                        lead.setAdditional_Image4_Proof__c(amazonS3Urls);
                        lead.setAdditional_Image4_Name__c(uploadDetails
                                .getFileName());
                        break;
                    case UploadFileTypes.OTHER5:
                        lead.setAdditional_Image5_Proof__c(amazonS3Urls);
                        lead.setAdditional_Image5_Name__c(uploadDetails
                                .getFileName());
                        break;

                    default:
                        updateContactImageURLInSalesForce(amazonS3Urls,
                                salesForceInfo, applicantId, uploadDetails);
                        contactUpdated = true;
                        break;
                }
            }
            // If image was under contact then do not update the lead.
            if (contactUpdated == false) {
                Lead[] leadObject = { lead };
                SaveResult[] results = connection.update(leadObject);

                if (null != results && results.length > 0) {
                    result = results[0].getSuccess();
                    if (!result && results[0].getErrors() != null
                            && results[0].getErrors().length > 0) {
                        Error[] errs = results[0].getErrors();
                        logger.error(errs != null && errs.length > 0 ? errs[0]
                                .getMessage()
                                : "Errors occurred while updating urls for lead ID: "
                                + leadId
                                + " and profile photo = "
                                + isProfilePic);
                    } else {
                        logger.info("Url updated successfully for lead Id:-"
                                + leadId);
                    }

                } else {
                    logger.error("Lead Id " + leadId
                            + " returned null because Lead with id " + leadId
                            + " does not exist in sales force");
                    result = false;
                }
            }
        } catch (ConnectionException e) {
            logger.error(e.getMessage(), e.getCause());
            result = false;
        }
        return result;
    }


    private void updateContactImageURLInSalesForce(String amazonUrl,
                                                   SalesForceInfo salesForceInfo, String applicantId,UploadFileDetails uploadDetails) {
        boolean result;
        try {
            String contactId = salesForceInfo.getContactIdApplicantIdMap().get(
                    applicantId);
            QueryResult res = connection
                    .query("Select id from  Contact where id = " + "'"
                            + contactId + "'");
            Contact contact = (null != res && res.getSize() > 0) ? (Contact) res
                    .getRecords()[0] : null;
            if (contact != null) {

                switch (uploadDetails.getFileType()) {

                    case UploadFileTypes.APPLICANT_PHOTO:
                        contact.setApplicant_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.PERMANENT_ADDRESS:
                        contact.setPermanent_Address_Proof_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.PRESENT_ADDRESS:
                        contact.setPresent_Address_Proof_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.DATE_OF_BIRTH:
                        contact.setDOB_Proof_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.ID_NO:
                        contact.setID_Proof_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.SIGNATURE_PROOF:
                        contact.setSignature_Proof_Image__c(amazonUrl);
                        break;

                    case UploadFileTypes.PAN:
                        contact.setPAN_Proof_Image__c(amazonUrl);
                        break;
                }

                Contact[] contactObject = { contact };
                SaveResult[] results = connection.update(contactObject);

                if (null != results && results.length > 0) {
                    result = results[0].getSuccess();
                    if (!result && results[0].getErrors() != null
                            && results[0].getErrors().length > 0) {
                        Error[] errs = results[0].getErrors();
                        logger.error(errs != null && errs.length > 0 ? errs[0]
                                .getMessage()
                                : "Errors occurred while updating urls for Contact: "
                                + contactId);
                    } else {
                        logger.info("Url updated successfully for Contact Id:-"
                                + contactId);
                    }
                }
            }

        } catch (Exception e) {
            salesForceInfo.setStatus(e+"");
            logger.error("Lead Id " + salesForceInfo.getLeadId());
            saveInDb(salesForceInfo);
        }
    }

    /**
     *
     * @param institutionId
     *            institution id for which connector config needs to be
     *            initialized
     * @return connector config for sales force
     */
    private ConnectorConfig getConnectorConfig(String institutionId) {

        ConnectorConfig config = new ConnectorConfig();
        SalesForceConfiguration salesForceCredential = Cache.URL_CONFIGURATION
                .getSalesForceConfiguration().get(institutionId);
        //Return null config, if credentials not found.
        if (salesForceCredential == null)
            return null;

        config.setUsername(salesForceCredential.getUserName());
        config.setPassword(salesForceCredential.getUserPassword());

        if(salesForceCredential.getProxyConfiguration()!=null){
            String host=salesForceCredential.getProxyConfiguration().getProxyhost();
            String port=salesForceCredential.getProxyConfiguration().getPort();
            if (StringUtils.isNotBlank(host)&&StringUtils.isNotBlank(port)){
                config.setProxy(host, Integer.parseInt(port));
            }
        }

        return config;
    }

    /**
     * This methods saves the references of the applicant to Sales Force.
     *
     * @param applicantReferenceList
     *            applicant references to be saved in database
     * @param leadId
     *            lead id against which applicant reference needs to be saved.
     */
    private void saveApplicantReferenceInfo(List<ApplicantReference> applicantReferenceList, String leadId,String refid, String instId) {


        if (applicantReferenceList != null && applicantReferenceList.size() > 0) {
            for (int i = 0; i < applicantReferenceList.size(); i++) {

                ApplicantReference applicantReference = applicantReferenceList
                        .get(i);
                Contact reference = new Contact();
                reference.setBorrower_Type__c(ApplicantType.REFERENCE.name());

                if (applicantReference.getName() != null) {
                    reference.setFirstName(applicantReference.getName()
                            .getFirstName());
                    reference.setMiddleName(applicantReference.getName()
                            .getMiddleName());
                    reference.setLastName(applicantReference.getName()
                            .getLastName());
                }

                if (applicantReference.getAddress() != null) {

                    reference.setMailing_Address__c(applicantReference
                            .getAddress().getAddressLine1()
                            + ","
                            + applicantReference.getAddress().getAddressLine2()
                            + ","
                            + applicantReference.getAddress().getLandMark());

                   reference.setMailingCity(applicantReference.getAddress()
                            .getCity());
                   /* reference.setMailingCountry(applicantReference.getAddress()
                            .getCountry());*/
                    reference.setMailingState(applicantReference.getAddress()
                            .getState());
                    reference.setMailingPostalCode(applicantReference
                            .getAddress().getPin() + "");

                    reference.setMailingStreet(applicantReference
                            .getAddress().getAddressLine1()
                            + ","
                            + applicantReference.getAddress().getAddressLine2()
                            + ","
                            +applicantReference.getAddress()
                            .getLandMark());
                }

                if (applicantReference.getPhone() != null) {
                    reference.setPhone(applicantReference.getPhone()
                            .getPhoneNumber());
                }
                reference.setOccupation__c(applicantReference.getOccupation());
                reference.setRelationship_with_Borrower__c(applicantReference.getRelationType());
                reference.setLead__c(leadId);

                try {
                    save(reference, refid, String.valueOf(ApplicantType.REFERENCE), instId);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Error saving reference details for lead {} cause {}",leadId, e.getMessage());
                    salesForceLog = salesForceResponseDetails(null, e.getMessage(),
                            String.valueOf(ApplicantType.REFERENCE), refid, instId);
                    saveSfResponse(salesForceLog);
                }
            }
        }

    }
    /**
     *
     * @param goNoGoCustomerApplication
     *            GNG application request.
     * @return list of the SF contact object.
     * @throws Exception
     */
    private void saveCoApplicantInfo(GoNoGoCustomerApplication goNoGoCustomerApplication,
                                     String leadId) throws Exception {

        Request request = goNoGoCustomerApplication.getApplicationRequest()
                .getRequest();

        List<CoApplicant> coApplicantList = request.getCoApplicant();
        if (coApplicantList != null && coApplicantList.size() > 0) {
            for (int i = 0; i < coApplicantList.size(); i++) {
                CoApplicant coApplicant = coApplicantList.get(i);
                Contact coBorrower = getContactDetails(coApplicant,
                        ApplicantType.COBORROWER);

                String contactId = null;
                try {

                    if (coApplicant.isEarning()) {

                        boolean isSalaried = false;
                        if (coApplicant.getEmployment().get(0)
                                .getEmploymentType().equals("Salaried")) {
                            isSalaried = true;
                        }
                        coBorrower
                                .setAnnual_Income_Gross__c(getGrossAnnualIncome(
                                        coApplicant.getIncomeDetails(),
                                        isSalaried));
                        coBorrower
                                .setAverage_Monthly_Net_Income_Assessed__c(getNetMonthlyIncome(
                                        coApplicant.getIncomeDetails(),
                                        isSalaried));
                    }
                    // Save co-applicant details in salesforce.
                    coBorrower.setLead__c(leadId);

                    contactId = save(coBorrower, goNoGoCustomerApplication.getApplicationRequest().getRefID(), String.valueOf(ObjectType.CONTACT_COBORROWER),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                    if (coApplicantMBResponses != null) {
                        // Iterate over executeForCoApplicant MB response list and upload
                        // bureau details of current executeForCoApplicant.
                        for (MultiBureauCoApplicantResponse mbCoAppResponse : coApplicantMBResponses) {
                            if (coApplicant.getApplicantId().equals(
                                    mbCoAppResponse.getCoApplicantRefId())) {
                                ResponseMultiJsonDomain mbResponse = mbCoAppResponse
                                        .getMultiBureauJsonRespose();
                                if (mbResponse != null) {

                                    uploadBureauPdfs(mbResponse,
                                            goNoGoCustomerApplication
                                                    .getApplicationRequest()
                                                    .getHeader()
                                                    .getInstitutionId(),
                                            contactId, leadId, goNoGoCustomerApplication.getApplicationRequest().getRefID());
                                    break;
                                }
                            }
                        }
                    }
                    //Set contact ID against applicant Id.
                    applicantIdcontactIdMap.put(coApplicant.getApplicantId(), contactId);
                } catch (Exception e) {
                    logger.error("Error saving executeForCoApplicant Info for lead id {} cause {}",leadId, e.getMessage());
                    e.printStackTrace();
                    salesForceLog = salesForceResponseDetails(null, e.getMessage(),  String.valueOf(ObjectType.CONTACT_COBORROWER),
                            goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                            goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                    saveSfResponse(salesForceLog);
                }

				/*
				 * If banking details of co-applicant are not null the save
				 * these details against the co-Applicant.
				 */
                if (coApplicant.getBankingDetails() != null
                        && coApplicant.getBankingDetails().size() > 0 && coApplicant.isEarning() == true) {
                    if (StringUtils.isNotBlank(contactId)) {

                        saveBankingDetails(coApplicant, contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                    } else {
                        logger.error("Skipping banking details of CoApplicant because contact not saved in Sales Force for"
                                + coApplicant.getApplicantName());
                    }
                }

				/*
				 * If income details and employment details are not null then
				 * post those details of the Co-Applicant to sales force
				 */
                if (coApplicant.getIncomeDetails() != null
                        && coApplicant.getEmployment() != null
                        && coApplicant.getEmployment().size() > 0 && coApplicant.isEarning() == true) {

                    if (StringUtils.isNotBlank(contactId)) {

                        saveEmploymentDetails(coApplicant, contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

                        saveIncomeDetails(coApplicant.getIncomeDetails(),
                                coApplicant.getEmployment().get(0), contactId,
                                request.getApplication(), goNoGoCustomerApplication.getApplicationRequest().getRefID(), ObjectType.CONTACT_COBORROWER,
                                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                    } else {
                        logger.error("Skipping income details and employment details of CoApplicant "
                                + "because contact not saved in Sales Force for"
                                + coApplicant.getApplicantName());
                    }
                }

				/*
				 * If loan details are not null then post those details to sales
				 * force.
				 */
                if (coApplicant.getLoanDetails() != null
                        && coApplicant.getLoanDetails().size() > 0 && coApplicant.isEarning() == true) {

                    saveLiabilityDetails(coApplicant.getLoanDetails(),
                            contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                            goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                }



                if (goNoGoCustomerApplication.getApplicantComponentResponse() != null
                        && goNoGoCustomerApplication
                        .getApplicantComponentResponse()
                        .getScoringServiceResponse() != null && coApplicant.isEarning() == true) {

                    List<ScoreAndDecisionDomain> coApplicantScores = goNoGoCustomerApplication
                            .getApplicantComponentResponse()
                            .getScoringServiceResponse()
                            .getScoreAndDecisionDomains();

                    if (coApplicant.isEarning() && coApplicantScores != null
                            && !coApplicantScores.isEmpty()) {

                        Object coAppScoreTree = null;
                        for (ScoreAndDecisionDomain coApplicantScore : coApplicantScores) {
                            if (!StringUtils.isEmpty(coApplicant
                                    .getApplicantId())
                                    && coApplicant.getApplicantId().equals(
                                    coApplicantScore.getApplicantId())) {
                                coAppScoreTree = coApplicantScore
                                        .getScoreTree();
                                break;
                            }
                        }

                        if (coAppScoreTree != null) {
                            saveScoreTreeDetails(coAppScoreTree, contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                        }
                    }
                }
            }
        }
    }


    private void saveApplicantInfo(GoNoGoCustomerApplication goNoGoCustomerApplication, String leadId) throws Exception {
        Request request = goNoGoCustomerApplication.getApplicationRequest()
                .getRequest();

        Applicant applicant = request.getApplicant();
        boolean isSalaried = false;

        if(applicant == null)
            return;

        if (applicant.getEmployment().get(0).getEmploymentType()
                .equals("Salaried")) {
            isSalaried = true;
        }

        Contact borrower = getContactDetails(applicant,
                ApplicantType.BORROWER);

        String contactId = null;
        try {
            // Save Applicant details in salesforce.
            borrower.setLead__c(leadId);
            borrower.setAnnual_Income_Gross__c(getGrossAnnualIncome(
                    applicant.getIncomeDetails(), isSalaried));
            borrower.setAverage_Monthly_Net_Income_Assessed__c(getNetMonthlyIncome(
                    applicant.getIncomeDetails(), isSalaried));
            contactId = save(borrower, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                    String.valueOf(ObjectType.CONTACT_BORROWER),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            ComponentResponse componentResponse = goNoGoCustomerApplication
                    .getApplicantComponentResponse();
            if (componentResponse != null) {
                ResponseMultiJsonDomain mbResponse = componentResponse
                        .getMultiBureauJsonRespose();
                if (mbResponse != null) {

                    uploadBureauPdfs(mbResponse, goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getInstitutionId(), contactId, leadId, goNoGoCustomerApplication.getApplicationRequest().getRefID());
                }
            }

            applicantIdcontactIdMap.put(applicant.getApplicantId(),contactId);
        } catch (Exception e) {
            logger.error("Error saving applicant info for {} cause {}",applicant.getApplicantName(), e.getMessage());
            e.printStackTrace();
            salesForceLog = salesForceResponseDetails(null,  e.getMessage(), String.valueOf(ObjectType.CONTACT_BORROWER),
                    goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            saveSfResponse(salesForceLog);
        }

		/*
		 * If banking details of Applicant are not null the save these details
		 * against the Applicant.
		 */
        if (applicant.getBankingDetails() != null
                && applicant.getBankingDetails().size() > 0) {
            if (StringUtils.isNotBlank(contactId)) {

                saveBankingDetails(applicant, contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            } else {
                logger.error("Skipping banking details of CoApplicant because contact not saved in Sales Force for"
                        + applicant.getApplicantName());
            }
        }

		/*
		 * If income details and employment details are not null then post those
		 * details of the Applicant to sales force
		 */
        if (applicant.getIncomeDetails() != null
                && applicant.getEmployment() != null
                && applicant.getEmployment().size() > 0) {

            if (StringUtils.isNotBlank(contactId)) {
                saveEmploymentDetails(applicant, contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

                saveIncomeDetails(applicant.getIncomeDetails(), applicant
                        .getEmployment().get(0), contactId, request.getApplication(),
                        goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                        ObjectType.BORROWER_INCOME, goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            } else {
                logger.error("Skipping income details and employment details of CoApplicant "
                        + "because contact not saved in Sales Force for"
                        + applicant.getApplicantName());
            }
        }

		/*
		 * If loan details are not null then post those details to sales force.
		 */
        if (applicant.getLoanDetails() != null
                && applicant.getLoanDetails().size() > 0) {
            saveLiabilityDetails(applicant.getLoanDetails(), contactId, goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        }


        if (goNoGoCustomerApplication.getApplicantComponentResponse() != null
                && goNoGoCustomerApplication.getApplicantComponentResponse()
                .getScoringServiceResponse() != null
                && goNoGoCustomerApplication.getApplicantComponentResponse()
                .getScoringServiceResponse().getScoreTree() != null) {

            saveScoreTreeDetails(goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getScoringServiceResponse().getScoreTree(), contactId,
                    goNoGoCustomerApplication.getApplicationRequest().getRefID(), goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        }
    }

    /**
     *
     * @param applicant GNG applicant details
     * @param applicantType type of the applicant.
     * @return Sales Force Contact object with GNG applicant details.
     */
    private Contact getContactDetails(Applicant applicant,ApplicantType applicantType){

        Contact borrower = new Contact();

        boolean isEarning = true;
        if(applicant instanceof CoApplicant){
            CoApplicant coApplicant = (CoApplicant) applicant;
            borrower.setRelationship_with_Borrower__c(coApplicant.getRelationWithApplicant());
            borrower.setIs_CoBorrower_Earning__c(coApplicant.isEarning());
            isEarning = coApplicant.isEarning();
        }

        // Set applicant type
        borrower.setBorrower_Type__c(applicantType.getValue());
        borrower.setMarital_Status__c(applicant.getMaritalStatus());
        borrower.setGender__c(applicant.getGender());
        borrower.setNo_of_Dependents__c(applicant.getNoOfDependents() + "");
        borrower.setReligion__c(applicant.getReligion());
        borrower.setEducation_Qualification__c(applicant.getEducation());

        if (isEarning) {
            if (applicant.getEmployment() != null && applicant.getEmployment().size() > 0) {
                boolean isSalaried = applicant
                        .getEmployment()
                        .get(0)
                        .getEmploymentType()
                        .equals("Salaried");
                borrower.setMonthly_Income__c(getNetMonthlyIncome(applicant.getIncomeDetails(), isSalaried));
                borrower.setAnnual_Income_Gross__c(getGrossAnnualIncome(applicant.getIncomeDetails(), isSalaried));
            }
        }

        // Set the address details of the customer from GNG object to SF
        // object.
        setAddressDetails(borrower, applicant.getAddress());
        // Set the phone details of the customer from GNG object to SF
        // object.
        setPhoneDetails(borrower, applicant.getPhone());
        // Set the email details of the customer from GNG object to SF
        // object.
        setEmailDetails(borrower, applicant.getEmail());

        //Set kyc details in contact.
        setKycDetails(borrower, applicant.getKyc());

        if (applicant.getApplicantName() != null) {
            borrower
                    .setFirstName(applicant.getApplicantName().getFirstName());
            borrower.setMiddleName(applicant.getApplicantName()
                    .getMiddleName());
            borrower.setLastName(applicant.getApplicantName().getLastName());
        }
		/*
		 * Set applicant spouse name
		 */
        if (applicant.getSpouseName() != null) {
            borrower.setSpouse_Name__c(getName(applicant.getSpouseName()));
        }

        if (applicant.getFatherName() != null) {
            borrower.setFather_Name__c(getName(applicant.getFatherName()));
        }

        // Set work experience and employment type
        if (applicant.getEmployment() != null
                && applicant.getEmployment().size() > 0 && isEarning == true) {
            borrower.setTotal_Experience__c(applicant.getEmployment().get(0)
                    .getWorkExps());
            borrower.setSector__c(applicant.getEmployment().get(0)
                    .getEmploymentType());

            if (applicant.getEmployment().get(0).getEmploymentType()
                    .equalsIgnoreCase("Salaried")) {
                borrower.setName_of_Firm_Company_Establishment_In__c(applicant
                        .getEmployment().get(0).getEmploymentName());
            } else {
                borrower.setName_of_Firm_Company_Establishment_In__c(applicant
                        .getEmployment().get(0).getBusinessName());
            }
            borrower.setCurrent_Employment_Date__c(convertStringToCalDate(applicant
                    .getEmployment().get(0).getDateOfJoining()));
        }

        if (StringUtils.isNotBlank(applicant.getDateOfBirth()))
            borrower.setBirthdate(convertStringToCalDate(applicant
                    .getDateOfBirth()));

        if (applicant.getKyc() != null) {
            for (Kyc kyc : applicant.getKyc()) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PAN.name(), kyc.getKycName())) {
                    borrower.setPAN_ID__c(kyc.getKycNumber());
                }
            }
        }



        return borrower;
    }

    /**
     * This method sets the bank details properties from GNG object to Sales
     * force object and calls save method which saves the details to sales force
     *
     * @param applicant
     *            customer whose bank details will be saved
     * @param contactId
     *            contactId against which the bank details will be saved
     */
    private void saveBankingDetails(Applicant applicant, String contactId, String Refid, String instId) {

            List<BankingDetails> bankingDetails = applicant.getBankingDetails();

            for (int i = 0; i < bankingDetails.size(); i++) {
                Bank_Detail__c bankDetailsSFObj = new Bank_Detail__c();
                BankingDetails bankDetail = bankingDetails.get(i);

                bankDetailsSFObj.setAny_Regular_Loan_EMI_Debited__c(bankDetail
                        .getAnyEmi());
                bankDetailsSFObj.setAverage_Balance__c(bankDetail
                        .getAvgBankBalance());
                bankDetailsSFObj.setBank_Name__c(bankDetail.getBankName());

                if (bankDetail.getAccountHolderName() != null) {
                    bankDetailsSFObj
                            .setBank_Account_Name__c(bankDetail
                                    .getAccountHolderName().getFirstName()
                                    + " "
                                    + bankDetail.getAccountHolderName()
                                    .getMiddleName()
                                    + " "
                                    + bankDetail.getAccountHolderName()
                                    .getLastName());
                }

                bankDetailsSFObj.setBank_Account_Number__c(bankDetail
                        .getAccountNumber());
                bankDetailsSFObj.setBank_Account_Type__c(bankDetail
                        .getAccountType());
                bankDetailsSFObj.setNo_Inward_Check_Bounce__c(bankDetail
                        .getInwardChequeReturn());
                bankDetailsSFObj.setNo_Outward_Check_Bounce__c(bankDetail
                        .getOutwardChequeReturn());
                bankDetailsSFObj.setEMI_Debited_Amount__c(bankDetail
                        .getDeductedEmiAmt());
                bankDetailsSFObj
                        .setSalary_Credit__c(bankDetail.isSalaryAccount() ? "Yes"
                                : "No");
                bankDetailsSFObj.setIs_Salaried_Account__c(bankDetail
                        .isSalaryAccount());

                if (applicant.getIncomeDetails() != null
                        && applicant.getIncomeDetails().getSalariedDetails() != null) {
                    bankDetailsSFObj
                            .setAverage_Income_in_the_Last_3_month__c(applicant
                                    .getIncomeDetails().getSalariedDetails()
                                    .getLastTwoMonthSalary());
                }

                bankDetailsSFObj.setContact__c(contactId);

                try {
                    save(bankDetailsSFObj, Refid, String.valueOf(ObjectType.BANK_DETAILS), instId);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Error saving bank details for applicant {} cause {}", applicant.getApplicantName(), e.getMessage());
                    salesForceLog = salesForceResponseDetails(null, e.getMessage(),String.valueOf(ObjectType.BANK_DETAILS), Refid, instId);
                    saveSfResponse(salesForceLog);
                }
            }

    }

    /**
     * This method sets the employment details properties from GNG object to Sales
     * force object and calls save method which saves the details to sales force
     *
     * @param applicant customer whose employment details will be saved
     * @param contactId  contactId against which the employment details will be saved
     */
    private void saveEmploymentDetails(Applicant applicant,
                                       String contactId, String refId, String instId) {


        List<Employment> empList = applicant.getEmployment();

        for (int i = 0; i < empList.size(); i++) {
            Employment__c employmentSFObj = new Employment__c();
            Employment employment = empList.get(i);

            employmentSFObj.setMode_of_Payment__c(employment.getModePayment());
            employmentSFObj.setConstitution__c(employment.getConstitution());
            employmentSFObj.setDepartment__c(employment.getDepartment());
            employmentSFObj.setDesignation__c(employment.getDesignation());
            employmentSFObj.setConstitution__c(employment.getConstitution());
            employmentSFObj.setOccupation_Type__c(employment.getEmploymentType());
            employmentSFObj.setEmployer_Name__c(employment.getEmploymentName());

			/*
			 * Temporary block of code below salariedDetails.Remark field is
			 * used for taking Negative Profile Yes or No. Once actual field is
			 * created then this block will be removed.
			 *
			 * Temp code block START.
			 */
            SalariedDetails salariedDetails = applicant.getIncomeDetails() != null ? applicant
                    .getIncomeDetails().getSalariedDetails() : null;

            if (salariedDetails != null && salariedDetails.getRemarks() != null) {
                // Temporarily we are taking profile negative value in Salaried
                // details remark.
                if (salariedDetails.getRemarks().equals("Yes")) {
                    employmentSFObj.setIs_Negative_Profile__c(true);
                } else {
                    employmentSFObj.setIs_Negative_Profile__c(false);
                }
            }
			/*
			 * Temp code block END.
			 */

            // If commencement date is not null then convert it to calendar date
            // and set
            if (StringUtils.isNotBlank(employment.getCommencementDate()))
                employmentSFObj
                        .setDate_of_Commencement__c(convertStringToCalDate(employment
                                .getCommencementDate()));
            // Set employment address
            setAddressDetails(employmentSFObj, applicant.getAddress());

            // Set employment email.
            setEmailDetails(employmentSFObj, applicant.getEmail());

            setPhoneDetails(employmentSFObj, applicant.getPhone());

            // Set contactId against employment details.
            employmentSFObj.setContact__c(contactId);

            try {
                save(employmentSFObj, refId, String.valueOf(ObjectType.EMPLOYMENT), instId);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error saving employment details for applicant {} cause {}",applicant.getApplicantName(), e.getMessage());
                salesForceLog = salesForceResponseDetails(null, e.getMessage(), String.valueOf(ObjectType.EMPLOYMENT), refId, instId);
                saveSfResponse(salesForceLog);
            }
        }
    }

    /**
     * This method sets the email in contact object.
     *
     * @param sObject
     *            SF contact object in which email will be set..
     * @param emailList
     *            list of email's from which the email id's will be set.
     */
    private void setEmailDetails(SObject sObject, List<Email> emailList) {
        if (emailList == null)
            return;
        if (sObject instanceof Contact) {
            Contact contact = (Contact) sObject;

            for (Email email : emailList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_EMAIL.name(), email
                        .getEmailType())) {
                    contact.setEmail(email.getEmailAddress());
                }
            }
        }

        if (sObject instanceof Employment__c) {
            Employment__c employment = (Employment__c) sObject;

            for (Email email : emailList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.OFFICE_EMAIL.name(), email
                        .getEmailType())) {
                    employment.setWork_Email__c(email.getEmailAddress());
                }
            }
        }

        if (sObject instanceof Lead) {
            Lead lead = (Lead) sObject;

            for (Email email : emailList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_EMAIL.name(), email
                        .getEmailType())) {
                    lead.setEmail(email.getEmailAddress());
                }
            }
        }
    }

    /**
     *
     * @param sObject
     *            contact in which details will be set.
     * @param phoneList
     *            list of phones from where the phone details will be set.
     */
    private void setPhoneDetails(SObject sObject, List<Phone> phoneList) {
        if (sObject instanceof Contact) {
            Contact contact = (Contact) sObject;
            for (Phone phone : phoneList) {
				/*
				 * Setting only mobile number because landline(other phone) is
				 * mapped to lead directly.
				 */
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_MOBILE.name(), phone
                        .getPhoneType())) {
                    contact.setMobilePhone(phone.getPhoneNumber());
                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.OFFICE_PHONE.name(),phone
                        .getPhoneType())) {
                    contact.setOffice_Phone__c(phone.getPhoneNumber());
                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_PHONE.name(),phone.getPhoneType())) {
                    contact.setPhone(phone.getPhoneNumber());
                }
            }
        } else if (sObject instanceof Employment__c) {
            Employment__c employment = (Employment__c) sObject;
            // TODO: Add phone numbers once phone number fields are added for SF
            // employment object.
        } else if (sObject instanceof Lead){
            Lead lead = (Lead)sObject;
            for (Phone phone : phoneList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_PHONE.name(),phone.getPhoneType())) {
                    lead.setPhone(phone.getPhoneNumber());
                }
            }
        }
    }

    /**
     * This method sets the address details of the customer from GNG object to
     * sales force object.
     *
     * @param sObject
     *            contact in which details will be set.
     * @param addressList
     *            source address list from where the addresses will be set in
     *            contact object.
     */
    private void setAddressDetails(SObject sObject,
                                   List<CustomerAddress> addressList) {
        if (addressList == null)
            return;

        if (sObject instanceof Contact) {
            Contact contact = (Contact) sObject;
            for (CustomerAddress address : addressList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERMANENT.name(),address
                        .getAddressType())) {
					/*
					 * Set permanent address details of the customer.
					 */
                    contact.setOtherCity(address.getCity());
                    contact.setOtherCountry(address.getCountry());
                    contact.setOtherStreet(address.getAddressLine1() + ", "
                            + address.getAddressLine2() + ","
                            + address.getLandMark());
                    contact.setOtherState(address.getState());
                    contact.setOtherPostalCode(address.getPin() + "");

                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.RESIDENCE.name(),address.getAddressType())) {
					/*
					 * Set residence address in mailing address of the customer.
					 */
                    contact.setMailing_Address__c(address.getAddressLine1()
                            + "," + address.getAddressLine2() + ","
                            + address.getLandMark());
                    contact.setMailingCity(address.getCity());
                    contact.setMailingCountry(address.getCountry());
                    contact.setMailingStreet(address.getAddressLine1() + ","
                            + address.getAddressLine2() + ","
                            + address.getLandMark());
                    contact.setMailingState(address.getState());
                    contact.setMailingPostalCode(address.getPin() + "");
                    contact.setHome_Owner__c(address.getAccommodation());
                    contact.setIf_Rented_Rent_P_M__c(address.getRentAmount());
                    contact.setIf_Rented_Landlord_Name__c(address
                            .getLandLoard());

                    if (address.getMonthAtAddress() != 0) {
                        Date referenceDate = new Date();
                        Calendar c = Calendar.getInstance();
                        c.setTime(referenceDate);
                        c.add(Calendar.MONTH, -address.getMonthAtAddress());

                        contact.setMoveIn_Year__c(c.get(Calendar.YEAR) + "");
                    } else {
                        contact.setMoveIn_Year__c(null);
                    }
                }
            }
        }

        if (sObject instanceof Employment__c) {

            Employment__c employment_c = (Employment__c) sObject;
            for (CustomerAddress address : addressList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.OFFICE.name(), address
                        .getAddressType())) {

                    employment_c.setAddress__c(address.getAddressLine1() + ","
                            + address.getAddressLine2());
                    employment_c.setLandmark__c(address.getLandMark());
                    employment_c.setPincode__c((double) address.getPin());
                    employment_c.setCity__c(address.getCity());
                    employment_c.setState__c(address.getState());
                }
            }
        }

        if (sObject instanceof Lead) {
            Lead lead = (Lead) sObject;

            for (CustomerAddress address : addressList) {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.RESIDENCE.name(), address
                        .getAddressType())) {
                    lead.setStreet(address.getAddressLine1() + ","
                            + address.getAddressLine2() + ","
                            + address.getLandMark());
                    lead.setPostalCode(address.getPin() + "");
                    lead.setCity(address.getCity());
                    lead.setState(address.getState());
                    lead.setCountry(address.getCountry());
                }
            }
        }
    }


    private void saveProductDetails(Request request,String leadId, String refId, String instId) {
        Product__c product = new Product__c();

        if (request.getApplication() != null) {
            Property property = request.getApplication().getProperty();
            product.setProduct__c(property.getPropertyName());
            product.setPurpose__c(property.getPurpose());

            if (property.getPropertyAddress() != null) {
                product.setProperty_Address__c(property.getPropertyAddress()
                        .getAddressLine1()
                        + ","
                        + property.getPropertyAddress().getAddressLine2()
                        + ","
                        + property.getPropertyAddress().getLandMark());
                product.setCity__c(property.getPropertyAddress().getCity());
                product.setState__c(property.getPropertyAddress().getState());
                product.setPincode__c(Double.valueOf(property
                        .getPropertyAddress().getPin()));
                product.setProperty_Area_Value__c(property.getPropertyUnit());

                if (StringUtils.isNotBlank(property.getPropertyArea()))
                    product.setProperty_Area__c(Double.valueOf(property
                            .getPropertyArea()));

                product.setOwnership__c(property.getPropertyType());

                if (StringUtils.isNotBlank(property.getPropertyValue()))
                    product.setAgreement_Value__c(Double.valueOf(property
                            .getPropertyValue()));

                if (StringUtils.isNotBlank(property.getMarketValue()))
                    product.setMarket_Value__c(Double.valueOf(property
                            .getMarketValue()));

                product.setOCR_Amount__c(property.getOCRAmt());
                product.setRemarks__c(property.getRemark());

                product.setLead__c(leadId);

                try {
                    save(product, refId, String.valueOf(ObjectType.PRODUCT), instId);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Error saving product details for lead {} cause {}",leadId,e.getMessage());
                    salesForceLog = salesForceResponseDetails(null, e.getMessage(), String.valueOf(ObjectType.PRODUCT), refId, instId);
                    saveSfResponse(salesForceLog);
                }
            }
        }
    }


    @Override
    public boolean updateCroDecision(CroApprovalRequest croApprovalRequest) {

        boolean result = false;
        ConnectorConfig config = getConnectorConfig(croApprovalRequest
                .getHeader().getInstitutionId());
        SalesForceInfo sfInfo = getByApplicationRefId(croApprovalRequest
                .getReferenceId());
        if (config != null) {
            try {
                connection = Connector.newConnection(config);
                QueryResult res = connection
                        .query("Select id from  Lead where id = " + "'"
                                + sfInfo.getLeadId() + "'");
                Lead lead = (null != res && res.getSize() > 0) ? (Lead) res
                        .getRecords()[0] : null;
                if (lead != null) {
                    lead.setEMI__c(croApprovalRequest.getEmi());
                    lead.setCredit_Decision__c(croApprovalRequest
                            .getApplicationStatus());

                    if (croApprovalRequest.getCroJustification() != null) {
                        List<CroJustification> justifications = croApprovalRequest
                                .getCroJustification();
                        if (justifications.get(0) != null) {
                            CroJustification croJustification = justifications.get(0);
                            lead.setCredit_Decision_Remark__c(croJustification.getRemark());
                            lead.setCredit_Officer__c(croJustification.getCroID());
                        }
                    }
                    if (StringUtils.equalsIgnoreCase(
                            croApprovalRequest.getApplicationStatus(),
                            "Approved")) {
                        lead.setApproved_Amount__c(croApprovalRequest
                                .getApprovedAmount());
                         lead.setLoan_Tenor_in_Month__c(((double) (croApprovalRequest
                                .getTenor())));
                    }

                    Lead[] leadObject = { lead };
                    SaveResult[] results = connection.update(leadObject);

                    if (null != results && results.length > 0) {
                        result = results[0].getSuccess();
                        if (!result && results[0].getErrors() != null
                                && results[0].getErrors().length > 0) {
                            Error[] errs = results[0].getErrors();
                            logger.error(errs != null && errs.length > 0 ? errs[0]
                                    .getMessage()
                                    : "Errors occurred while updating urls for lead ID: "
                                    + sfInfo.getLeadId());
                            salesForceLog = salesForceResponseDetails(null,  errs[0]
                                    .getMessage(), String.valueOf(ObjectType.LEAD_CRO), croApprovalRequest
                                    .getReferenceId(), croApprovalRequest.getHeader().getInstitutionId());
                            saveSfResponse(salesForceLog);
                        } else {
                            logger.info("Url updated successfully for lead Id:-"
                                    + sfInfo.getLeadId());
                            salesForceLog = salesForceResponseDetails(sfInfo.getLeadId(), null, String.valueOf(ObjectType.LEAD_CRO), croApprovalRequest
                                    .getReferenceId(), croApprovalRequest.getHeader().getInstitutionId());
                            saveSfResponse(salesForceLog);
                        }
                    }
                    return result;
                }

            } catch (Exception e) {
                logger.error("Exception:- " + e);
              salesForceLog = salesForceResponseDetails(null, e.toString(), String.valueOf(ObjectType.LEAD_CRO), croApprovalRequest
                        .getReferenceId(), croApprovalRequest.getHeader().getInstitutionId());
                saveSfResponse(salesForceLog);
                return false;
            }
        }
        return result;
    }

    private void uploadBureauPdfs(ResponseMultiJsonDomain mbResponse,String institutionId,
                                  String contactId, String leadId, String refId) throws Exception {

        AmazonS3CloudStorageService s3CloudService = null;

        Map<String, AmazonS3Configuration> s3ConfigMap = Cache.URL_CONFIGURATION
                .getAmazonS3Configuration();

        AmazonS3Configuration s3Config = s3ConfigMap != null ? s3ConfigMap
                .get(institutionId) : null;

        if (s3Config != null) {
            s3CloudService = new AmazonS3CloudStorageService(
                    s3Config.getAccessKey(), s3Config.getSecretKey());
            // Upload finished list.
            List<Finished> finished = mbResponse.getFinishedList();
            if (CollectionUtils.isNotEmpty(finished)) {
                for (Finished finish : finished) {
                    uploadAndSaveBureauData(finish, leadId, contactId,
                    s3Config.getBucketName(), s3CloudService, refId, institutionId);
                }
            }

            // Upload reject list
            finished = mbResponse.getReject();
            if (CollectionUtils.isNotEmpty(finished)) {
                for (Finished finish : finished) {
                    uploadAndSaveBureauData(finish, leadId, contactId,
                    s3Config.getBucketName(), s3CloudService, refId, institutionId);
                }
            }

            // Upload inprocess list.
            finished = mbResponse.getInprocessList();
            if (CollectionUtils.isNotEmpty(finished)) {
                for (Finished finish : finished) {
                   uploadAndSaveBureauData(finish, leadId, contactId,
                            s3Config.getBucketName(), s3CloudService, refId, institutionId);
                }
            }
        }
    }

    private void uploadAndSaveBureauData(Finished finish, String leadId,
                                         String contactId, String bucketName,
                                         AmazonS3CloudStorageService s3CloudService, String refId, String instId) throws Exception {

        String bureauName = finish.getBureau();
        Multibureau_Data__c mbData = new Multibureau_Data__c();
        // get pdf report
        byte[] base64 = finish.getPdfReport();

        if (base64 != null) {
            InputStream inputStream = new ByteArrayInputStream(base64);

            AmazonS3UploadInputStreamRequest uploadRequest = new AmazonS3UploadInputStreamRequest();
            String targetFileName = leadId + "/" + contactId + "/" + bureauName
                    + ".pdf";
            uploadRequest.mapObjectFromInputStream(inputStream, bucketName,
                    targetFileName, "application/pdf");

            AmazonS3UploadFileResponse s3Response = s3CloudService
                    .uploadFile(uploadRequest);
            mbData.setBureau_URL__c(s3Response.getS3Url());
        }

        mbData.setBureau__c(bureauName);

        mbData.setContact__c(contactId);
        mbData.setResponse__c(getBureauResponse(finish.getResponseJsonObject()));
        mbData.setLead__c(leadId);
        try {
            save(mbData, refId, String.valueOf(ObjectType.MB) + " " + bureauName, instId);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error saving mb details for lead {} cause {}", leadId, e.getMessage());
            salesForceLog = salesForceResponseDetails(null, e.getMessage(), String.valueOf(ObjectType.MB), refId, instId);
            saveSfResponse(salesForceLog);
        }
    }

    private String getBureauResponse(Object bureauResponseObject) {
        String bureauResponseString = "";
        if (null != bureauResponseObject) {
            if (bureauResponseObject instanceof LinkedHashMap) {
                try {
                    bureauResponseString = new ObjectMapper().writeValueAsString(bureauResponseObject);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    logger.error("Error converting bureau response object to string while sending to Salesforce cause");
                }
            } else {
                //it is json string with escape character.
                bureauResponseString = bureauResponseObject.toString().replace("\\", "");
            }
        }
        return bureauResponseString;
    }

    private double getGrossAnnualIncome(IncomeDetails incomeDetails,
                                        boolean isSalaried) {
        double grossIncome = 0f;

        if (isSalaried == true) {
            grossIncome = incomeDetails.getSalariedDetails().getGrossIncome() * 12;
        } else {
            double netProfit = 0;
            double depriciation = 0;
            double otherIncome = 0;
            int yearCount = 0;
            for (int i = 0; i < incomeDetails.getSenpProfileIncome().size(); i++) {
                SENPProfileIncome senpProfileIncome = incomeDetails
                        .getSenpProfileIncome().get(i);
                if (!StringUtils.isBlank(senpProfileIncome.getItrDate())) {
                    netProfit += senpProfileIncome.getNetProfit();
                    depriciation += senpProfileIncome.getDepreciation();
                    otherIncome += senpProfileIncome.getOtherIncome();
                    // Consider only first two years income if provided are more
                    // than 2;
                    yearCount += 1;
                }

                if (i == 1)
                    break;
            }
            double totalConsideredIncome = (netProfit + depriciation + otherIncome);
            grossIncome = yearCount > 0 ? (totalConsideredIncome / yearCount) : totalConsideredIncome;
        }
        return grossIncome;
    }

    private double getNetMonthlyIncome(IncomeDetails incomeDetails,
                                       boolean isSalaried) {
        double netMonthlyIncome = 0;

        if (isSalaried == true) {
            netMonthlyIncome = incomeDetails.getSalariedDetails()
                    .getNetIncome();
        } else {
            double netProfit = 0;
            double depriciation = 0;
            double otherIncome = 0;
            int yearCount = 0;
            for (int i = 0; i < incomeDetails.getSenpProfileIncome().size(); i++) {
                SENPProfileIncome senpProfileIncome = incomeDetails
                        .getSenpProfileIncome().get(i);
                if (!StringUtils.isBlank(senpProfileIncome.getItrDate())) {
                    netProfit += senpProfileIncome.getNetProfit();
                    depriciation += senpProfileIncome.getDepreciation();
                    otherIncome += senpProfileIncome.getOtherIncome();
                    // Consider only first two years income if provided are more
                    // than 2;
                    yearCount += 1;
                }

                if (i == 1)
                    break;
            }
            double totalIncome = (netProfit + depriciation + otherIncome);
            netMonthlyIncome = yearCount > 0 ? (totalIncome / yearCount) : totalIncome;
        }
        return netMonthlyIncome;
    }
    private SalesForceLog salesForceResponseDetails (String objId, String exception, String objTyp, String
            RefId, String instId) {
        salesForceLog = new SalesForceLog();
        com.softcell.gonogo.model.request.core.Header header = new Header();
        if (salesForceLog != null) {
            salesForceLog.setRefId(RefId);
            if (instId != null) {
                header.setInstitutionId(instId);
                salesForceLog.setHeader(header);
            }
            if (objTyp == String.valueOf(ObjectType.LEAD)) {
                salesForceLog.setLeadId(objId);
            }

            if (exception == null) {
                List<Success> successList = new ArrayList<>();
                success = new Success();
                success.setSuccessMessage("Data saved successfully");
                success.setObjType(objTyp);
                success.setObjId(objId);
                successList.add(success);
                salesForceLog.setSuccess(successList);
            }else{
                List<Errors> errorsList = new ArrayList<>();
                errors = new Errors();
                errors.setErrorMessage(exception);
                errors.setObjType(objTyp);
                errors.setObjId(objId);
                errorsList.add(errors);
                salesForceLog.setError(errorsList);
        }
            return salesForceLog;
         }
        else{
            return null;
        }
    }
}
