package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.List;

/**
 * @author yogeshb
 */

@Repository
public class AnalyticsMongoRepository implements AnalyticsRepository {

    private static final Logger logger = LoggerFactory.getLogger(AnalyticsMongoRepository.class);

    private MongoTemplate mongoTemplate;

    /**
     * @param mongoTemplate
     */
    @Inject
    public AnalyticsMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public long count(Query query) throws Exception {

        logger.debug("Analytic Repo started for gonogo reponse count");

        try {

            return mongoTemplate.count(query, GoNoGoCustomerApplication.class);

        } catch (Exception e) {

            logger.error("Error while count for gonogoResponse {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AggregationResults<BasicDBObject> fetchLoginStatusGraphData(

            Aggregation aggregation, String collection) throws Exception {

        logger.debug("Analytic Repo started to fetch fetchLoginStatusGraphData ");

        try {

            return mongoTemplate.aggregate(aggregation, collection,
                    BasicDBObject.class);

        } catch (DataAccessException e) {

            logger.error("Error while fetching fetchLoginStatusGraphData with probable cause  [{}]", e.getMessage());

            throw new SystemException(e.getMessage());
        }
    }

    public AggregationResults<BasicDBObject> fetchLoginStatusTableData(
            Aggregation aggregation, String collection) throws Exception {

        logger.debug("Analytic Repo started to fetch fetchLoginStatusTableData");

        try {

            return mongoTemplate.aggregate(aggregation, collection, BasicDBObject.class);

        } catch (DataAccessException e) {

            logger.error("Error while fetching fetchLoginStatusTableData {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AggregationResults<BasicDBObject> getStageWiseLoginReport(
            Aggregation aggregation) throws Exception {

        logger.debug("Analytic Repo started to fetch getStageWiseLoginReport");

        try {

            return mongoTemplate.aggregate(aggregation,
                    MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(),
                    BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getStageWiseLoginReport {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AggregationResults<BasicDBObject> getLoginDealersWise(
            Aggregation aggregation) throws Exception {

        logger.debug("Analytic Repo started to fetch getLoginDealersWise");

        try {

            Assert.notNull(aggregation, "Aggregation for login by dealership report must not be blank or null !!!");

            return mongoTemplate.aggregate(aggregation,
                    MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(),
                    BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getLoginDealersWise {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AggregationResults<BasicDBObject> getAssetManufactureReport(
            Aggregation aggregation) throws Exception {

        logger.debug("Analytic Repo started to fetch getAssetManufactureReport");

        try {

            Assert.notNull(aggregation, "Aggregation for asset Manufacture report must not be blank or null !!!");

            return mongoTemplate.aggregate(aggregation, MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(), BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getAssetManufactureReport {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


    @Override
    public AggregationResults<BasicDBObject> getUniqueAssetManufacturers(
            Aggregation aggregation) throws Exception {

        logger.debug("Analytic Repo started to fetch getUniqueAssetManufacturers");

        try {

            Assert.notNull(aggregation, "Aggregation for unique asset manufacturer must not be blank or null !!!");

            return mongoTemplate.aggregate(aggregation, MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(), BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getUniqueAssetManufacturers {}", e.getMessage());

            throw new Exception(e.getMessage());

        }



    }

    @Override
    public List<GoNoGoCroApplicationResponse> getDsaReport() throws Exception {

        logger.debug("Analytic Repo started to fetch getDsaReport");

        try {

            return mongoTemplate.findAll(GoNoGoCroApplicationResponse.class);

        } catch (Exception e) {

            logger.error("Error while fetching getDsaReport {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public GoNoGoCustomerApplication fetchStackScoreData(Query query, StackRequest stackRequest) throws Exception {

        logger.debug("Analytic Repo started to fetch fetchStackScoreData");

        try {

            return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);

        } catch (Exception e) {

            logger.error("Error while fetching fetchStackScoreData {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List getRoleBasedDistinctFields(Query query, String collection, String field) throws Exception {

        logger.debug("Analytic Repo started to fetch getRoleBasedDistinctBranches");

        try {

            return mongoTemplate.getCollection(collection).distinct(field, query.getQueryObject());


        } catch (DataAccessException e) {

            logger.error("Error while fetching getRoleBasedDistinctBranches with probable cause  [{}]", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public AggregationResults<BasicDBObject> getDailyDisbursalReport(Aggregation aggregation) throws Exception {

        logger.debug("Analytic Repo started to fetch getDailyDisbursalReport");

        try {

            Assert.notNull(aggregation, "Aggregation for Daily Disbursal Report must not be blank or null !!!");

            return mongoTemplate.aggregate(aggregation, MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(), BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getDailyDisbursalReport {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AggregationResults<BasicDBObject> getPostIPARequest(Aggregation aggregation) throws Exception {

        logger.debug("Analytic repo started to fetch getPostIPARequest");

        try {

            Assert.notNull(aggregation, "Aggregation to get PostIPARequest must not be blank or null !!!");

            return mongoTemplate.aggregate(aggregation, MongoConfig.COLLECTIONS.POST_IPA_DOC.toString(), BasicDBObject.class);

        } catch (Exception e) {

            logger.error("Error while fetching getPostIPARequest {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }



}
