package com.softcell.dao.mongodb.repository.loyaltycard;

import com.mongodb.WriteResult;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.LoyaltyCardMaster;
import com.softcell.gonogo.model.request.loyaltycard.LoyaltyCardStatusRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;

/**
 * Created by mahesh on 12/12/17.
 */
@Repository
public class LoyaltyCardMongoRepository implements LoyaltyCardRepository {

    private static final Logger logger = LoggerFactory.getLogger(LoyaltyCardMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private GridFsTemplate gridFsTemplate;


    @Override
    public LoyaltyCardMaster checkLoyaltyCardStatus(LoyaltyCardStatusRequest loyaltyCardStatusRequest) {

        logger.debug("checkLoyaltyCardStatus repo started");

        try {
            Query query = new Query();

            query.addCriteria(Criteria
                    .where("loyaltyCardNo")
                    .is(loyaltyCardStatusRequest.getLoyaltyCardNo())
                    .and("institutionId").is(loyaltyCardStatusRequest.getHeader().getInstitutionId())
                    .and("active").is(true));

            return mongoTemplate.findOne(query, LoyaltyCardMaster.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred  while checking LoyaltyCardStatus [{}]", e.getMessage());

            throw new SystemException(String.format("error occurred  while checking LoyaltyCardStatus with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public boolean checkLoyaltyCardInDedupe(LoyaltyCardStatusRequest loyaltyCardStatusRequest) {
        logger.debug("checkLoyaltyCardInDedupe repo started");

        try {
            Query query = new Query();

            query.addCriteria(Criteria
                    .where("loyaltyCardNo")
                    .is(loyaltyCardStatusRequest.getLoyaltyCardNo())
                    .and("refId").ne(loyaltyCardStatusRequest.getRefId())
                    .and("header.institutionId").is(loyaltyCardStatusRequest.getHeader().getInstitutionId())
                    .and("activeFlag").is(true));

            return mongoTemplate.exists(query, LoyaltyCardDetails.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred  while checking LoyaltyCardInDedupe [{}]", e.getMessage());

            throw new SystemException(String.format("error occurred  while checking LoyaltyCardInDedupe with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public void updateLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails) {

        logger.debug("update Loyalty Card Details repo started");

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(loyaltyCardDetails.getRefId())
                    .and("institutionId").is(loyaltyCardDetails.getHeader().getInstitutionId())
                    .and("activeFlag").is(true));

            Update update = new Update();
            update.set("header", loyaltyCardDetails.getHeader());
            update.set("loyaltyCardNo", loyaltyCardDetails.getLoyaltyCardNo());
            update.set("loyaltyCardPrice", loyaltyCardDetails.getLoyaltyCardPrice());
            update.set("loyaltyCardType", loyaltyCardDetails.getLoyaltyCardType());
            update.set("revisedTotalEmi", loyaltyCardDetails.getRevisedTotalEmi());

            mongoTemplate.upsert(query, update, LoyaltyCardDetails.class);

        } catch (DataAccessException e) {

            e.printStackTrace();
            logger.error("Error occurred while updating loyalty card details " +
                    "  with probable cause [{}] "
                    , e.getMessage());

            throw new SystemException(String.format("Error occurred while updating loyalty card details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public boolean softDeleteLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(loyaltyCardDetails.getRefId()).and("activeFlag").is(true));

            Update update = new Update();

            update.set("activeFlag", false);
            update.set("header.dateTime", new Date());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, LoyaltyCardDetails.class);

            logger.info("writeResult :-" + writeResult);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException e) {

            e.printStackTrace();
            logger.error("Error occurred while soft deleting loyalty card details with probable cause [{}] "
                    , e.getMessage());

            throw new SystemException(String.format("Error occurred while soft deleting loyalty card details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public LoyaltyCardDetails fetchLoyaltyCardDetails(String refId, String institutionId) {
        logger.debug("fetchLoyaltyCardDetails repo started");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("header.institutionId").is(institutionId).and("activeFlag").is(true));

            return mongoTemplate.findOne(query, LoyaltyCardDetails.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while fetching fetchLoyaltyCardDetails with probable cause [{}] "
                    , e.getMessage());

            throw new SystemException(String.format(" error occurred  while fetchLoyaltyCardDetails based on institutionId and referenceId with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public Set<String> getApplicableLoyaltyCardProviders(String dealerId, String institutionId) throws Exception {
        logger.debug("inside get applicable loyalty card providers against dealer");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("dealerId").is(dealerId).and("active").is(true));

            DealerBranchMaster dealerBranchMaster = mongoTemplate.findOne(query, DealerBranchMaster.class);
            if (null != dealerBranchMaster) {
                return dealerBranchMaster.getLoyaltyCards();
            } else {
                return null;
            }

        } catch (Exception e) {

            logger.error("Error occurred while getting loyalty card providers against dealerwith probable cause [{}] ", e.getMessage());
            throw new Exception(e.getMessage());
        }

    }
}
