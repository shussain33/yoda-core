package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.core.request.MbPushSequence;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LosMongoRepository implements LosRepository {

    private static final Logger logger = LoggerFactory.getLogger(LosMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String getSupplierIdFromDesc(String supplierDesc, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("supplierDesc").is(supplierDesc)
                .and("active").is(true).and("institutionID").is(institutionId));
        DealerEmailMaster dealerDetail = mongoTemplate.findOne(query,
                DealerEmailMaster.class);
        return dealerDetail != null ? dealerDetail.getDealerID() : FieldSeparator.BLANK;

    }

    @Override
    public AssetModelMaster getAssetDetails(String make, String modelNo,
                                            String manufacture, String category, String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("make").is(make).and("modelNo")
                .is(modelNo).and("manufacturerDesc").is(manufacture)
                .and("catgDesc").is(category).and("institutionId")
                .is(institutionId).and("active").is(true));

        return mongoTemplate.findOne(query, AssetModelMaster.class);
    }

    @Override
    public String getMakeIdbyMakeDesc(String make, String manufacture, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("make").is(make).and("modelNo").is(FieldSeparator.BLANK)
                .and("manufacturerDesc").is(manufacture).and("active").is(true)
                .and("institutionId").is(institutionId));
        AssetModelMaster assetModel = mongoTemplate.findOne(query,
                AssetModelMaster.class);
        return assetModel != null ? assetModel.getModelId() : FieldSeparator.BLANK;
    }

    @Override
    public String getEmployerIdByName(String employerName, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("employerName").is(employerName)
                .and("institutionId").is(institutionId).and("active").is(true));
        EmployerMaster employerMaster = mongoTemplate.findOne(query,
                EmployerMaster.class);

        return employerMaster != null ? employerMaster.getEmployerId() : FieldSeparator.BLANK;
    }

    @Override
    public String getAssetCatgIdbyCatgDesc(String assetCatgDesc, String institutionId) {
        String catId = "";
        Query query = new Query();
        query.addCriteria(Criteria.where("assetCatgDesc").is(assetCatgDesc).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "assetCategory");
        if (bdObj != null) {
            catId = (String) bdObj.get("assetCatgId");
        }
        return catId;
    }

    @Override
    public String getStateIdByState(String state, String institutionId) {
        String stateId = "";
        Query query = new Query();
        query.addCriteria(Criteria.where("stateDesc").is(state).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "stateMaster");
        if (bdObj != null) {
            stateId = (String) bdObj.get("stateId");
        }
        return stateId;
    }

    @Override
    public String getCityIdByCity(String city, String institutionId) {
        String cityId = "";
        Query query = new Query();
        query.addCriteria(Criteria.where("cityDesc").is(city).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "cityMaster");
        if (bdObj != null) {
            cityId = (String) bdObj.get("cityId");
        }
        return cityId;
    }

    @Override
    public String getCreditPromotionIdByScheme(String scheme, String institutionId) {
        String creditPromotionId = "";
        Query query = new Query();
        query.addCriteria(Criteria.where("schemeId").is(scheme).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "creditPromotionMaster");
        if (bdObj != null) {
            creditPromotionId = (String) bdObj.get("promotionId");
        }
        return creditPromotionId;
    }

    @Override
    public String getBranchByDealerId(String dealerId, String productId, String institutionId) {
        String branchId = "";
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerId").is(dealerId)
                .and("productId").is(productId).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "dealerBranchMaster");
        if (bdObj != null) {
            branchId = (String) bdObj.get("branchId");
        }
        return branchId;
    }

    @Override
    public List<String> getDSAByBranchId(String branchId, String institutionId) {
        List<String> dsaIds = new ArrayList<String>();
        Query query = new Query();
        query.addCriteria(Criteria.where("branchId").is(branchId).and("active").is(true)
                .and("institutionId").is(institutionId));
        List<BasicDBObject> bdObj = mongoTemplate.find(query,
                BasicDBObject.class, "dsaBranchMaster");
        if (bdObj != null && !bdObj.isEmpty()) {
            for (BasicDBObject basicDBObject : bdObj) {
                dsaIds.add((String) basicDBObject.get("brokerId"));
            }
        }
        return dsaIds;
    }

    @Override
    public String getDSAByProdcutId(String _dsaId, String productId, String institutionId) {
        String dsaId = null;
        Query query = new Query();
        logger.debug("{} : {}", _dsaId, productId);
        query.addCriteria(Criteria.where("brokerId").is(_dsaId)
                .and("productId").is(productId).and("active").is(true)
                .and("institutionId").is(institutionId));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "dsaProductMaster");
        if (bdObj != null) {
            return _dsaId;
        }
        return dsaId;
    }

    @Override
    public String getEducationByEducationDesc(String eduDesc) {
        Query query = new Query();
        String eduValue = null;
        query.addCriteria(Criteria.where("KEY1").is("QUALIFIC")
                .and("DESCRIPTION").is(eduDesc));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "commonGeneralMaster");
        if (bdObj != null && !bdObj.isEmpty()) {
            eduValue = (String) bdObj.get("VALUE");
        }
        return eduValue;
    }

    @Override
    public String getResidenceByResidenceDesc(String resDesc) {
        Query query = new Query();
        String eduValue = null;
        query.addCriteria(Criteria.where("key1").is("ACCOTYPE")
                .and("description").is(resDesc));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "commonGeneralParameterMaster");
        if (bdObj != null && !bdObj.isEmpty()) {
            eduValue = (String) bdObj.get("value");
        }
        return eduValue;
    }

    @Override
    public String getValueFromCGPMOnKey1AndDescription(String key1, String description) {

        Query query = new Query();
        String value = null;

        query.addCriteria(Criteria.where("key1").is(key1)
                .and("description").is(description));

        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class,
                "commonGeneralParameterMaster");
        if (bdObj != null && !bdObj.isEmpty()) {
            value = (String) bdObj.get("value");
        }
        return value;
    }

    @Override
    public boolean isItHyderabadDealer(String dealerId, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerId").is(dealerId).and("branchId").is("1278")
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        BasicDBObject bdObj = mongoTemplate.findOne(query, BasicDBObject.class, "dealerBranchMaster");
        if (bdObj != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isValidDealerToPushLos(String dealerId, String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("dealerId").is(dealerId).and("dealerFlag").is(true)
                .and("institutionId").is(institutionId)
                .and("active").is(true));

        return mongoTemplate.exists(query, DealerBranchMaster.class);
    }

    @Override
    public String getProductSerialNo(String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("refID").is(refId).and("status").is(Status.VALID));
        SerialNumberInfo serialNumberInfo = mongoTemplate.findOne(query, SerialNumberInfo.class);
        return serialNumberInfo != null ? serialNumberInfo.getSerialNumber() : FieldSeparator.BLANK;
    }

    @Override
    public long getMbPushSequenceNumber(String instituteId) {

            MbPushSequence mbPushSequence = new MbPushSequence();
            mbPushSequence.setSequence(1);
            mbPushSequence.setInstitutionId(instituteId);
            mongoTemplate.insert(mbPushSequence);

        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(instituteId));
        mbPushSequence = mongoTemplate.findOne(query, MbPushSequence.class);

        long sequenceId = mbPushSequence.getSequence();
        long incrementalSequenceNumber = sequenceId + 1;

        Update update = new Update();
        update.set("sequence",incrementalSequenceNumber);
        mongoTemplate.updateFirst(query,update,MbPushSequence.class);
        return sequenceId;
    }

    @Override
    public LoyaltyCardDetails getLoyaltyCardsDetails(String refID, String institutionId) {
        LoyaltyCardDetails loyaltyCardDetails = null;
        if(StringUtils.isNotBlank(refID) && StringUtils.isNotBlank(institutionId) ){
            Query query =  new Query();
            query.addCriteria(Criteria.where("refId").is(refID).and("header.institutionId").is(institutionId).and("activeFlag").is(true));
            loyaltyCardDetails =  mongoTemplate.findOne(query,LoyaltyCardDetails.class);
        }
        return loyaltyCardDetails;

    }

    @Override
    public String getBankIdFromBankName(String bankName) {
        Query query =  new Query();
        query.addCriteria(Criteria.where("bankDescription").is(bankName).and("active").is(true));
        BankMaster bankMaster =  mongoTemplate.findOne(query,BankMaster.class);
        return bankMaster !=null ? bankMaster.getBankCode():"";
    }

    @Override
    public String getStateIdByBranchName(String branchName){

        Query query =  new Query();
        query.addCriteria(Criteria.where("branchName").is(branchName).and("active").is(true));
        BranchMaster branchMaster =  mongoTemplate.findOne(query,BranchMaster.class);
        return branchMaster !=null ? branchMaster.getStateId():"";
    }

    @Override
    public String getPromotionSchemeBySurrogateAndProduct(String surrogateName, String productCode) {
        Query query =  new Query();
        query.addCriteria(Criteria.where("promotionDesc").is(surrogateName).and("productCode").is(productCode).and("active").is(true));
        PromotionalSchemeMaster promotionalSchemeMaster =  mongoTemplate.findOne(query,PromotionalSchemeMaster.class);
        return promotionalSchemeMaster !=null ? promotionalSchemeMaster.getPromotionCode():"";
    }

    @Override
    public LosMbData getLosMbData(String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("refId").is(refId));
        LosMbData losMbData = mongoTemplate.findOne(query, LosMbData.class);
        return losMbData;
    }

    @Override
    public void saveLosMbData(LosMbData losMbData) {
        /*if (!mongoTemplate.collectionExists(LosMbData.class)) {
            mongoTemplate.createCollection(LosMbData.class);
        }*/
        mongoTemplate.save(losMbData);
    }
}

