package com.softcell.dao.mongodb.repository;

import com.softcell.dao.mongodb.repository.elsearch.IndexingRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class ApplicationStageMongoRepository implements ApplicationStageRepository {


    private static final Logger logger = LoggerFactory.getLogger(ApplicationStageMongoRepository.class);


    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private IndexingRepository indexingRepository;

    @Override
    public boolean updateApplicationStage(SerialSaleConfirmationRequest confirmationRequest,String stage) throws Exception {

        logger.debug("AppStage Repo started to updateApplicationStage");

        try {

            Update update = new Update();

            update.set("applicationRequest.currentStageId", stage);

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(
                    confirmationRequest.getReferenceID()));

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            indexingRepository.indexNotification(getGoNoGoCustomerApplicationByRefId(confirmationRequest
                    .getReferenceID()));

            return true;

        } catch (Exception e) {

            logger.error("Error while updating applications stage : [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    @Override
    public void updateApplicationStage(String refID, String stage) throws Exception {
        logger.debug("AppStage Repo started to updateApplicationStage");

        try {

            Update update = new Update();

            update.set("applicationRequest.currentStageId", stage);

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            indexingRepository.indexNotification(getGoNoGoCustomerApplicationByRefId(refID));

        } catch (Exception e) {

            logger.error("Error while updating applications stage : [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    private GoNoGoCustomerApplication getGoNoGoCustomerApplicationByRefId(
            String referenceId) throws Exception {

        logger.debug("AppStage Repo started to fetch getGoNoGoCustomerApplicationByRefId");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId));

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            return goNoGoCustomerApplication;

        } catch (Exception e) {

            logger.error("Error while fetchin gonogo response : [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }
}


