/**
 *
 */
package com.softcell.dao.mongodb.repository.systemConfiguration;

import com.softcell.config.KarzaConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.Product;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.configuration.AuthSkipConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.los.LosChargeConfig;
import com.softcell.gonogo.model.MasterSchedulerActivityLogs;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author yogeshb
 */
public interface ConfigurationRepository{

    /**
     * @param institutionProductConfiguration
     * @return
     */
    boolean addInstitutionProductConfiguration(InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    boolean removeInstitutionProductConfiguration(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    InstitutionProductConfiguration updateInstitutionProductConfiguration(
            InstitutionProductConfiguration institutionProductConfiguration) throws SystemException;

    /**
     * @param institutionProductConfiguration
     * @return
     */
    List<InstitutionProductConfiguration> findAllInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institution
     * @param productID
     * @param productName
     * @return
     */
    boolean isProductExist(String institution, String productID,
                           String productName);

    /**
     * @param templateConfiguration
     * @return
     */
    boolean saveTemplateConfiguration(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    List<TemplateConfiguration> getTemplateConfiguration(
            TemplateConfiguration templateConfiguration);

    /**
     * @param institutionId
     * @param productId
     * @param templateName
     * @return
     */
    TemplateConfiguration getTemplateConfiguration(String institutionId,
                                                   String productId, String templateName);

    /**
     * @param templateConfiguration
     * @return
     */
    boolean updateTemplatePath(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    boolean updateTemplateLogo(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    boolean enableTemplate(TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    boolean disableTemplate(TemplateConfiguration templateConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean saveEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean updateEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean addAttachmentInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean removeAttachmentFromEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean disableEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    boolean enableEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    List<EmailConfiguration> getEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @return
     */
    List<InstitutionProductConfiguration> getInstitutionProductConfiguration();


    /**
     *
     * @param validationVendorsConfiguration
     */
    ValidationVendorsConfiguration saveAllowedVendors(ValidationVendorsConfiguration validationVendorsConfiguration);

    /**
     *
     * @return
     */
    List<ValidationVendorsConfiguration> getApplicableVendorConfiguration(String institutionId);

    /**
     *
     * @param validationVendorsConfiguration
     */
    boolean deleteValidationVendorConfiguration(ValidationVendorsConfiguration validationVendorsConfiguration);

    /**
     *
     * @param emailConfiguration
     * @return
     */
    boolean addSecondaryAttachmentInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     *
     * @param emailConfiguration
     * @return
     */
    boolean removeSecondaryAttachmentFromEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     *
     * @param loyaltyCardConfiguration
     * @return
     */
    boolean saveLoyaltyCardConfiguration(LoyaltyCardConfiguration loyaltyCardConfiguration);

    /**
     *
     * @param institutionId
     * @return
     */
    List<LoyaltyCardConfiguration> getLoyaltyCardConfiguration(String institutionId ,String productId);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    boolean removeLoyaltyCardConfiguration(String institutionId, String productId ,LoyaltyCardType loyaltyCardType);

    /**
     *
     * @param loyaltyCardConfiguration
     * @return
     */
    boolean updateLoyaltyCardConfiguration(LoyaltyCardConfiguration loyaltyCardConfiguration);

    /**
     * update the Sms Template Configuration
     * @param smsTemplateConfiguration
     * @return
     */
    boolean updateSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration);

    /**
     * insert SmsTemplateConfiguration
     * @param smsTemplateConfiguration
     * @return
     */
    boolean saveSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration);

    /**
     * get SmsTemplateConfiguration
     * @param institutionId
     * @return
     */
    List<SmsTemplateConfiguration> getSmsTemplateConfiguration(String institutionId);

    /**
     * delete SmsTemplateConfiguration
     * @param institutionId
     * @param productId
     * @param smsType
     * @return
     */
    boolean removeSmsTemplateConfiguration(String institutionId, String productId, String smsType);
    /**
>>>>>>> 6b842fdd1536cc915dce8cdf1328cebb41aa8ccd
     *
     * @param dmsFolderConfigurationList
     * @return
     */
    boolean saveDmsFolderConfiguration(List<DmsFolderConfiguration> dmsFolderConfigurationList);

    /**
     *
     * @param institutionId
     * @return
     */
    List<DmsFolderConfiguration> getDmsFolderConfiguration(String institutionId);

    /**
     *
     * @param dmsFolderConfiguration
     * @return
     */
    boolean updateDmsFolderConfiguration(DmsFolderConfiguration dmsFolderConfiguration);

    /**
     *
     * @param institutionId
     * @param indexName
     * @return
     */
    boolean removeDmsFolderConfiguration(String institutionId, String indexName);

    boolean saveMasterMappingConfiguration(List<MasterMappingConfiguration> masterMappingConfigurationList);

    MasterMappingConfiguration getMasterMappingConfiguration(String institutionId, String masterName);

    /**
     *
     * @param institutionId
     * @return
     */
    List<LosChargeConfig> getLosChargeConfiguration(String institutionId);


    /**
     *
     * @param losChargeConfig
     * @return
     */
    LosChargeConfig createLosChargeConfiguration(LosChargeConfig losChargeConfig);

    /**
     *
     * @param institutionId
     * @param chargeType
     * @return
     */
    LosChargeConfig getLosChargeConfiguration(String institutionId,LosChargeConfig.LosChargeType chargeType,String productId);

    /**
     *
     * @param caseCancellationJobConfig
     * @return
     */
    boolean saveCaseCancelConfig(CaseCancellationJobConfig caseCancellationJobConfig);

    /**
     *
     * @param institutionId
     * @return
     */
    CaseCancellationJobConfig getCaseCancellationConfiguration(String institutionId);

    /**
     *
     * @param institutionId
     * @return
     */
    boolean deleteCaseCancellationConfiguration(String institutionId);


    /**
     * @param impsConfigDomain
     * @throws Exception
     */
    boolean addIMPSConfiguration(IMPSConfigDomain impsConfigDomain) throws SystemException;

    /**
     * @param institutionId
     * @return
     * @throws Exception
     */
    IMPSConfigDomain findIMPSConfigByInstitutionId(@NotNull final String institutionId) throws SystemException;

    /**
     * @param impsConfigDomain
     * @throws Exception
     */
    boolean deleteIMPSConfig(IMPSConfigDomain impsConfigDomain) throws SystemException;

    /**
     * Returns all the active institution ids
     * @return
     */
    List<String> fetchActiveInstitutionIds();

    KarzaConfiguration findKarzaConfiguration(String institutionId, String productName);

    List<MasterSchedulerConfiguration> fetchMasterSchedulerConfig(String institutionId);

    boolean insertMasterSchedularConfig(String instId, String masterName, String fromTime, String toTime);

    void insterMasterActivityLogs(MasterSchedulerActivityLogs activityLogs);

    AuthSkipConfiguration getAuthConfigByInstId(String institutionID);

    MasterMappingConfiguration getMasterMappingConfiguration(String institutionId, String masterName, Product product);
}
