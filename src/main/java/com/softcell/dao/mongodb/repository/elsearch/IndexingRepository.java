package com.softcell.dao.mongodb.repository.elsearch;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;

/**
 * @author kishorp
 */
public interface IndexingRepository {
    /**
     * @param customerApplication
     * @return
     */
    boolean indexNotification(GoNoGoCustomerApplication customerApplication);
}
