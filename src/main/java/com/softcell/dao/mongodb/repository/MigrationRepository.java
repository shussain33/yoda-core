package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.request.MigrationRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by amit on 11/3/19.
 */
public interface MigrationRepository {
    void save(Object object);

    Object fetch(Class<?> classz, String id);

    List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds);

    List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds, List<String> includes);

    List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds,
                                                     Map<String, Object> conditions, List<String> includes);

    boolean updateApplication(String refId, Map<String, Object> updateData);

    Object fetch(Class className, Map<String, Object> conditions, List<String> includes);

    GoNoGoCustomerApplication fetchGonogoCustomerByDsaId(String institutionId, String primaryId);

    int updateDsaId(MigrationRequest migrationRequest, List<String> secondaryIdList, String primaryId, Name dsaName, Email dsaEmailId);

    int updateAllocationInfo(MigrationRequest migrationRequest, String secId, String primaryId);

    long countDsaId (MigrationRequest migrationRequest,String secondaryIdList);

   List <CamDetails>fetchCamDetails(String instituteId);

   boolean updateCamDetail(String refId, CamDetails camDetails);
}
