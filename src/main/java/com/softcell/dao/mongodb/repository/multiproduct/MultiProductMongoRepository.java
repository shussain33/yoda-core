package com.softcell.dao.mongodb.repository.multiproduct;

import com.softcell.config.MultiProductConfiguration;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author yogeshb
 */
@Repository
public class MultiProductMongoRepository implements MultiProductRepository {

    public static final Logger logger = LoggerFactory.getLogger(MultiProductMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public MultiProductConfiguration getMultiProductConfiguration(
            MultiProductRequest multiProductRequest) {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("institutionID")
                .is(multiProductRequest.getHeader().getInstitutionId())
                // .and("dsa").is(multiProductRequest.getHeader().getDsaId())
                // .and("dealer")
                // .is(multiProductRequest.getHeader().getDealerId())
                .and("active").is(true).and("product")
                .is(multiProductRequest.getProduct()));
        return mongoTemplate.findOne(query, MultiProductConfiguration.class);
    }

    @Override
    public boolean add(MultiProductConfiguration multiProductConfiguration) {
        try {
            /*if (!mongoTemplate
                    .collectionExists(MultiProductConfiguration.class)) {
                mongoTemplate.createCollection(MultiProductConfiguration.class);
            }*/
            mongoTemplate.insert(multiProductConfiguration);
            return true;
        } catch (Exception ex) {
            logger.error("MongoException:" + ex);
            return false;
        }
    }

    @Override
    public boolean delete(MultiProductConfiguration multiProductConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(
                    multiProductConfiguration.getConfigId()));
            Update update = new Update();
            update.set("active", false);
            mongoTemplate.updateFirst(query, update,
                    MultiProductConfiguration.class);
            return true;
        } catch (Exception ex) {
            logger.error("MongoException:" + ex);
            return false;
        }

    }

    @Override
    public boolean update(MultiProductConfiguration multiProductConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(
                    multiProductConfiguration.getConfigId()));
            Update update = new Update();
            update.set("numberOfproductsAllowed",
                    multiProductConfiguration.getNumberOfproductsAllowed());
            mongoTemplate.updateFirst(query, update,
                    MultiProductConfiguration.class);
            return true;
        } catch (Exception ex) {
            logger.error("MongoException:" + ex);
            return false;
        }
    }

    @Override
    public List<MultiProductConfiguration> findAll() {
        return mongoTemplate.findAll(MultiProductConfiguration.class);
    }

    @Override
    public GoNoGoCroApplicationResponse getParentApplication(String refID) throws Exception {

        logger.debug("getParentApplication repo started");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            logger.debug("getParentApplication repo query formed {}", query);

            return mongoTemplate.findOne(query,
                    GoNoGoCroApplicationResponse.class);

        } catch (Exception e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public boolean checkRefernceIDExistance(String refID) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));
            return mongoTemplate.exists(query,
                    GoNoGoCroApplicationResponse.class);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public GoNoGoCroApplicationResponse cloneApplication(
            GoNoGoCroApplicationResponse parentApplication) {
        try {
            mongoTemplate.insert(parentApplication);
            return parentApplication;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred at cloning application with another reference ID with probable cause [{}]",e.getMessage());
            throw new SystemException(String.format("Error occurred at cloning application with another reference ID with probable cause [{%s}]",e.getMessage()));
        }

    }

    @Override
    public long getProductCount(String rootID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("rootID").is(rootID));
        return mongoTemplate.count(query, GoNoGoCroApplicationResponse.class);
    }

    @Override
    public PostIpaRequest getPostIPA(String refID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refID));
        return mongoTemplate.findOne(query, PostIpaRequest.class);
    }

    @Override
    public Set<String> getAllRefenceIDAgainstRootApplication(String rootID) {

        logger.debug("getAllRefenceIDAgainstRootApplication repo started ");

        Query query = new Query();
        query.addCriteria(Criteria.where("rootID").is(rootID));
        query.fields().include("_id");

        logger.debug("getAllRefenceIDAgainstRootApplication repo query formed {} ", query);

        List<GoNoGoCroApplicationResponse> goNoGoCroApplicationResponseList = mongoTemplate
                .find(query, GoNoGoCroApplicationResponse.class);

        Set<String> refIds = new HashSet<String>();

        if (!goNoGoCroApplicationResponseList.isEmpty()) {

            refIds.add(rootID);

            for (GoNoGoCroApplicationResponse goNoGoCroApplicationResponse : goNoGoCroApplicationResponseList) {

                refIds.add(goNoGoCroApplicationResponse.getGngRefId());

            }

        }

        logger.debug("getAllRefenceIDAgainstRootApplication repo completed successfully with refId count {}", refIds.size());

        return refIds;
    }

    @Override
    public boolean getPostIPAExistance(String refID) {
        return mongoTemplate.exists(new Query(Criteria.where("_id").is(refID)),
                PostIpaRequest.class);
    }

    @Override
    public boolean checkApplicationStage(String refID) {
        Set<String> stages = new HashSet<String>();
        stages.add(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        stages.add(GNGWorkflowConstant.DO.toFaceValue());
        stages.add(GNGWorkflowConstant.SRNV.toFaceValue());
        stages.add(GNGWorkflowConstant.INV_GNR.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_QDE.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_APRV.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_DCLN.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_DISB.toFaceValue());

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refID)
                .and("applicationStatus")
                .is(GNGWorkflowConstant.APPROVED.toFaceValue())
                .and("applicationRequest.currentStageId").in(stages));
        return mongoTemplate.exists(query, GoNoGoCroApplicationResponse.class);
    }

    @Override
    public boolean checkDealerSubventionWaivedOff(Header header) {
        try {
            String dealerID = header.getDealerId();
            Query query = new Query();
            query.addCriteria(Criteria.where("dealerID").is(dealerID)
                    .and("institutionID").is(header.getInstitutionId())
                    .and("itNum").is(Status.N.name()).and("active").is(true));
            return mongoTemplate.exists(query, DealerEmailMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while checking existance of Delear Subvention flag for " +
                    "Dealer {} with probable cause [{}]", header.getDealerId(), ex.getMessage());
            throw new SystemException(String.format("Error occurred while checking existance of " +
                    "Dealer Subvention flag for dealer {%s} with probable cause [{%s}]",
                    header.getDealerId(), ex.getMessage()));
        }

    }

    @Override
    public double getSumOfNetFundingAmount(Set<String> refIDs) {

        /*List<AggregationOperation> pipelineOperation = new ArrayList<>();
        pipelineOperation.add(match(Criteria.where("_id").in(refIDs)));
        pipelineOperation.add(group("_id").sum("postIPA.approvedAmount").as("total"));


        Aggregation aggregation = Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());


        AggregationResults<BasicDBObject> postIpaDoc = mongoTemplate.aggregate(aggregation, "postIpaDoc", BasicDBObject.class);

        List<BasicDBObject> mappedResults = postIpaDoc.getMappedResults();

        double total = mappedResults.get(0).getDouble("total");

        return total;*/
        return 0D;
    }


}
