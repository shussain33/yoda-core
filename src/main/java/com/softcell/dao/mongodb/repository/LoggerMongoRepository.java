/**
 * yogeshb6:44:40 pm  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.emudra.SFTPLog;
import com.softcell.workflow.component.finished.AuditData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;


/**
 * @author yogeshb
 */
@Repository
public class LoggerMongoRepository implements LoggerRepository {

    private static final Logger logger = LoggerFactory.getLogger(LoggerMongoRepository.class);


    private MongoTemplate mongoTemplate;

    public LoggerMongoRepository() {
    }

    @Autowired
    public LoggerMongoRepository(MongoTemplate mongoTemplate) {

        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public boolean setLogDetails(OtpLog workFlowLog) {
        try {
            mongoTemplate.insert(workFlowLog);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting OtpLogs with probable cause [{}]",e.getMessage());
            return false;
        }
    }


    @Override
    public boolean setAadharLogDeatails(AadharLog aadharLog) {
        try {
            mongoTemplate.insert(aadharLog);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting AadhaarLog with probable cause [{}]", e.getMessage());
            return false;
        }
    }

    @Override
    public boolean saveAuditData(AuditData auditData, String auditCollectionName) {
        try {
            MongoTemplate mongoTemplate = MongoConfig.getMongoTemplate();

            /*if (!mongoTemplate.collectionExists(auditCollectionName)) {
                mongoTemplate.createCollection(auditCollectionName);
            }*/
            mongoTemplate.save(auditData, auditCollectionName);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while save AuditData with probable cause [{}]", e.getMessage());
            return false;
        }
    }

    @Override
    public void saveESignedLog(ESignedLog eSignedLog) {
        try {
            mongoTemplate.insert(eSignedLog);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ESignedLog with probable cause [{}]", e.getMessage());
        }
    }

    @Override
    public void saveSFTFLog(SFTPLog sftpLog) {
        try {
            mongoTemplate.insert(sftpLog);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting SFTPLog with probable cause [{}]", e.getMessage());
        }
    }
}
