package com.softcell.dao.mongodb.repository.search;

import com.softcell.constants.Institute;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author yogeshb
 */
public interface MasterRepository {


    /**
     * @param query
     * @return
     */
    List<AssetModelMaster> getAssetModelMasterByFullTextSearch(
            String query);

    /**
     * @param metadataRequest
     * @return
     */
    Set<String> getManufacturer(
            ApplicationMetadataRequest metadataRequest ,String productAliasName);

    List<ManufacturerMaster> getManufacturerDetailsList(ApplicationMetadataRequest metadataRequest);
    /**
     * @param metadataRequest
     * @return
     */
    Set<String> getCategories(ApplicationMetadataRequest metadataRequest, String productAliasName);

    /**
     * @param metadataRequest
     * @return
     */
    Set<String> getMakes(ApplicationMetadataRequest metadataRequest, String productAliasName);

    /**
     * @param metadataRequest
     * @return
     */
    Set<String> getModelNumbers(
            ApplicationMetadataRequest metadataRequest ,String productAliasName);

    /**
     * @return String Set
     * @throws Exception
     */
    Set<String> getAllUniqueCategories(ApplicationMetadataRequest metadataRequest, String productAliasName) throws Exception;

    /**
     *
     * @param modelNo
     * @param institutionId
     * @return
     */
    List<AssetModelMaster> getAssetModelMasterDetails(String modelNo,
                                                      String institutionId ,String productAliasName);

    /**
     * @param dealerId
     * @return
     */
    DealerEmailMaster getDealerRanking(String dealerId, String institutionId);

    /**
     * @param metadataRequest
     * @param productAliasName
     * @return
     */
    List<AssetModelMaster> getModelsWithOtherDetails(ApplicationMetadataRequest metadataRequest, String productAliasName);


    StateMaster getState(String stateDesc, String institutionId);

    AccountsHeadMaster getAccountHeadMaster(String tranAccDesc, String institutionId);

    StateBranchMaster getStateBranchMaster(String branchcode, String institutionId);

    TaxCodeMaster getTaxCodeMaster(String stateCode, String institutionId);

    List<TCLosGngMappingMaster> getTCLosGngMappingMaster(String institutionId);

    ManufacturerMaster getManufacturer(String mfrCode, String institutionId);

    boolean saveDropDownMaster(DropdownMaster dropdownMaster);

    List<DropdownMaster> fetchDropDownMaster(DropdownMasterRequest dropdownMasterRequest);

    Map<String, Map<String, String>> getThirdPartyFieldValues(Institute institute, String apiName);

    Map<String, Map<String, Map<String, String>>> getThirdPartyAPI(Institute institute, String apiName);

    List<DropdownMaster> fetchDropDownMaster(String instId, String dropDownType,String QueryType, String product);

    Map<String, Map<String, String>> fetchLogsMaster(String institutionId, String type, String masterValue);

}
