package com.softcell.dao.mongodb.repository.systemConfiguration;

import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.cache.services.RedisServiceImpl;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.admin.InstitutionConfig;
import com.softcell.gonogo.model.configuration.admin.IntimationConfiguration;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.verification.BankingVerification;
import com.softcell.gonogo.model.core.verification.ItrVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.quickcheck.EMandateConstants;
import com.softcell.gonogo.model.quickcheck.EMandateDetails;
import com.softcell.gonogo.model.quickcheck.ProcessedRequests;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.request.TemplateRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.utils.GngNumUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.softcell.constants.TemplateConstants.*;
import static com.softcell.constants.TemplateConstants.MANDATE_ID;
import static com.softcell.constants.TemplateConstants.MANDATE_URL;

/**
 * Created by amit on 30/5/18.
 */
@Repository
public class AppConfigurationMongoRepository implements AppConfigurationRepository {
    private static final Logger logger = LoggerFactory.getLogger(AppConfigurationMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AppConfigurationHelper appConfigurationHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Override
    public void save(Object object) {
        mongoTemplate.save(object);
    }

    @Override
    public void saveAll(List<LoginServiceResponse> list, Class classname) {
        /*if(!mongoTemplate.collectionExists(LoginServiceResponse.class))
            mongoTemplate.createCollection(LoginServiceResponse.class);*/
        mongoTemplate.insertAll(list);
    }

    @Override
    public void saveAll( Class classname, List<?> list) {
        /*if(!mongoTemplate.collectionExists(classname))
            mongoTemplate.createCollection(classname);*/
        mongoTemplate.insertAll(list);
    }

    @Override
    public void removeAllUsers(Class className, String institutionId) {
        /*if(mongoTemplate.collectionExists(className))*/
            mongoTemplate.findAllAndRemove(new Query(Criteria.where("institutionId").is(Integer.valueOf(institutionId))), className);
    }

    @Override
    public void update(LoginServiceResponse user) {
        /*if(!mongoTemplate.collectionExists(LoginServiceResponse.class))
            mongoTemplate.createCollection(LoginServiceResponse.class);*/
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        mongoTemplate.remove(query, LoginServiceResponse.class);
        mongoTemplate.insert(user);
    }

    @Override
    public Object fetch(Class<?> classz, String id) {
        Query query;
        if(id != null) {
            query = new Query(Criteria.where("_id").is(id));
        } else{
            query = new Query();
        }
        return mongoTemplate.findOne(query, classz);
    }

    @Override
    public boolean saveHierarchyMaster(String institutionId, List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        try {

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId").is(institutionId));
                mongoTemplate.remove(query, OrganizationalHierarchyMasterV2.class);


            mongoTemplate.insert(hierarchyMasterV2List, OrganizationalHierarchyMasterV2.class);
            return true;
        } catch (Exception ex) {
            logger.info("Unable to update new hierarchy data!");
        }
        return false;
    }

    @Override
    public List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId, String productName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("product").is(productName));
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<LoginServiceResponse> fetchUserDetails(String institutionId, String productName){
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(Integer.valueOf(institutionId));
        if(StringUtils.isNotEmpty(productName))
            criteria.and("product").in(productName);
        query.addCriteria(criteria);
        return mongoTemplate.find(query, LoginServiceResponse.class);
    }

    @Override
    public List<LoginServiceResponse> fetchUserDetails(String institutionId, String searchType, String[] searchCriteria){
        Query query = new Query();
        query.addCriteria( Criteria.where("institutionId").is(Integer.valueOf(institutionId)));
        if (StringUtils.equalsIgnoreCase(searchType, IntimationConstants.BRANCH)) { //BranchWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("branchName").regex(searchCriteria[0], "i")));
        } else if (searchType.contains("(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")")) { //ProductWise
            query.addCriteria(Criteria.where("product").elemMatch(Criteria.where("productName").is(searchCriteria[0])));
        } else if (searchType.startsWith(IntimationConstants.BRANCH + "_")) { //BranchRoleWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("branchName").regex(searchCriteria[0], "i")));
            query.addCriteria(Criteria.where("roles").in(searchCriteria[1]));
        } else if (StringUtils.equalsIgnoreCase(searchType, IntimationConstants.ZONE)) { //ZoneWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("zone").regex(searchCriteria[0], "i")));
        } else if (searchType.startsWith(IntimationConstants.ZONE + "_")) { //ZoneRoleWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("zone").regex(searchCriteria[0], "i")));
            query.addCriteria(Criteria.where("roles").in(searchCriteria[1]));
        }  else if (searchType.startsWith(IntimationConstants.LOCATION + "_")) { //LocationRoleWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("location").regex(searchCriteria[0], "i")));
            query.addCriteria(Criteria.where("roles").in(searchCriteria[1]));
        }  else if (searchType.startsWith(IntimationConstants.REGION + "_")) { //RegionRoleWise
            query.addCriteria(Criteria.where("branches").elemMatch(Criteria.where("region").regex(searchCriteria[0], "i")));
            query.addCriteria(Criteria.where("roles").in(searchCriteria[1]));
        } else { //RoleWise
            query.addCriteria(Criteria.where("roles").in(searchCriteria[0]));
        }
        query.fields().include("loginId")
                .include("userName")
                .include("institutionId")
                .include("employeeId")
                .include("email")
                .include("mobile");
        return mongoTemplate.find(query, LoginServiceResponse.class);
    }

    @Override
    public OrganizationalHierarchyMasterV2 fetchHierarchyMasterForProductBranch(String institutionId,
                                                                                String productName, int branchCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).
                and("product").is(productName).
                and("branch.branchId").is(branchCode));
        if(mongoTemplate == null){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        return mongoTemplate.findOne(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId,
                              List<String> branchNameList) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).
                and("branch.branchName").in(branchNameList));
        query.fields().include("branch");
        if(mongoTemplate == null){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId, String productName, String role) {
        Criteria criteria = Criteria.where("institutionId").is(institutionId).and("product").is(productName);
        if (role != null)
            criteria.and("roleWiseUsersList.role").is(role);
        Query query = new Query();
        query.addCriteria(criteria);
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<IntimationConfiguration> fetchIntimitionConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return mongoTemplate.find(query, IntimationConfiguration.class);
    }

    @Override
    public IntimationConfiguration fetchIntimitionConfiguration(String institutionId, String product) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("product").is(product));
        return mongoTemplate.findOne(query, IntimationConfiguration.class);
    }

    @Override
    public void removeIntimationConfiguration(Class<IntimationConfiguration> intimationConfigurationClass, String institutionId, String productName) {

            Query query = new Query(Criteria.where("institutionId").is(institutionId).and("product").is(productName));
            mongoTemplate.remove(query, IntimationConfiguration.class);

    }

    @Override
    public void updateIntimation(IntimationConfiguration intimationConfiguration){
        logger.info("inside updateIntimation ");
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("institutionId").is(intimationConfiguration.getInstitutionId())
                    .and("product").is(intimationConfiguration.getProduct());
            query.addCriteria(criteria);

            Update update = new Update();
            update.set("institutionId", intimationConfiguration.getInstitutionId());
            update.set("product", intimationConfiguration.getProduct());
            update.set("intimationConfig", intimationConfiguration.getIntimationConfig());

            mongoTemplate.upsert(query, update, IntimationConfiguration.class);
        }catch(Exception e){
            logger.error("Exception caught while updateIntimation " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public InstitutionConfig fetchInstitutionConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return mongoTemplate.findOne(query, InstitutionConfig.class);
    }

    @Override
    public InstitutionConfig fetchInstitutionConfiguration(String institutionId, String configType) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        if (StringUtils.isNotEmpty(configType)) {
            query.fields().include("institutionId");
            query.fields().include("instituteName");
            query.fields().include("products");
            query.fields().include("productConfigs.productName");
            query.fields().include("productConfigs.maxCreditLimit");

            if (configType.equals(EndPointReferrer.MODULE_CONFIG)) {
                query.fields().include("productConfigs.moduleConfigList");
            } else if (configType.equals(EndPointReferrer.VALUEMASTER_CONFIG)) {
                query.fields().include("productConfigs.screenValueMasterConfigs");
            } else if (configType.equals(EndPointReferrer.INTIMATION_CONFIG)) {
                query.fields().include("productConfigs.intimationConfig");
            }

        }
        return mongoTemplate.findOne(query, InstitutionConfig.class);
    }

    @Override
    public List<TemplateDetails> findTemplateDetailsByInstituteId(String instituteId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(instituteId);
        query.addCriteria(criteria);
        List<TemplateDetails> tempDetails = mongoTemplate.find(query,TemplateDetails.class);
        return tempDetails;
    }

    @Override
    public TemplateDetails findTemplateDetailsByInstituteIdAndTemplateName(String institutionId, String productName,
                                                                           String templateName) {
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(institutionId).and("productName").is(productName)
                .and("_id").is(templateName);
        query.addCriteria(criteria);
        if(mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
        return mongoTemplate.findOne(query,TemplateDetails.class);
    }

    @Override
    public TemplateDetails findTemplateDetailsByInstituteIdAndTemplateKey(String institutionId, String productName,
                                                                          String templateKey) {
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(institutionId).and("productName").is(productName)
                .and("templateKey").is(templateKey);
        query.addCriteria(criteria);
        if (mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
        return mongoTemplate.findOne(query, TemplateDetails.class);
    }


    @Override
    public boolean deleteTemplate(TemplateDetails templateDetails) {
        mongoTemplate.remove(templateDetails);
        return true;
    }

    @Override
    public Map<String, String> getTemplateConfigrationData(String refId, List<String> fields, String instituteId, String verificationType) throws Exception {
        logger.info("Inside getTemplateConfigrationData()");
        int counter = 0;
        int addressCounter = 0;
        int addResCounter = 0;
        int addOffCounter = 0;
        int emailCounter = 0;
        int phoneCounter = 0;
        Map<String, String> configMap = new HashMap<>();
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        LoanCharges loanCharges = null;
        CamDetails camDetails = null;
        DisbursementMemo disbursementMemo =null;
        EligibilityDetails eligibility = null;
        SanctionConditions sanctionConditions = null;
        List<PropertyVerification> verificationList = null;
        VerificationDetails verificationDetails = null;
        Repayment repayment = null;
        List<RepaymentDetails> repaymentDetailsList = null;
        Valuation valuation = null;
        EMandateDetails eMandateDetails = null;
        ProcessedRequests processedRequests = null;
        CamDetails BankingData = null;

        try {

            if(applicationRepository == null)
                applicationRepository = new ApplicationMongoRepository();
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            if(goNoGoCustomerApplication == null){
                goNoGoCustomerApplication = new GoNoGoCustomerApplication();
            }

            valuation = applicationRepository.fetchValuationDetails(refId, instituteId);
            verificationDetails =applicationRepository.fetchVerificationDetailsByRefId(refId,instituteId);

            loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, instituteId);
            if (loanCharges != null && loanCharges.getLoanDetails() == null) {
                LoanAmtDetails loanDetails = new LoanAmtDetails();
                loanCharges.setLoanDetails(loanDetails);
            }
            if (loanCharges != null && loanCharges.getInterestDetails() == null){
                loanCharges.setInterestDetails(new InterestDetails());
            }
            if(loanCharges == null){
                loanCharges = new LoanCharges();
                loanCharges.setLoanDetails(new LoanAmtDetails());
                loanCharges.setTenureDetails(new TenureDetails());
                loanCharges.setInterestDetails(new InterestDetails());
            }


            camDetails = applicationRepository.fetchCamSummaryByRefId(refId, instituteId);
            if (camDetails != null && camDetails.getSummary() == null){
                camDetails.setSummary(new CamSummary());
            }
            if(camDetails == null){
                camDetails = new CamDetails();
                camDetails.setSummary(new CamSummary());
            }

        if(eligibility == null) {
            eligibility = applicationRepository.fetchEligibilityDetails(refId,instituteId);
            if(eligibility == null){
                eligibility = new EligibilityDetails();

            }
        }

        sanctionConditions = applicationRepository.fetchSanctionConditions(refId, instituteId);
            if(sanctionConditions == null){
                sanctionConditions = new SanctionConditions();
        }

            disbursementMemo = applicationRepository.fetchDMDetails(refId, instituteId);
            if (disbursementMemo != null && CollectionUtils.isEmpty(disbursementMemo.getDmInputList())){
                List<DMInput> dmInputs = new ArrayList<>();
                disbursementMemo.setDmInputList(dmInputs);
            }
            if(disbursementMemo == null){
                disbursementMemo = new DisbursementMemo();
                List<DMInput> dmInputs = new ArrayList<>();
                disbursementMemo.setDmInputList(dmInputs);
            }

            repayment = applicationRepository.fetchRepaymentDetails(refId, instituteId);
            if (repayment != null && CollectionUtils.isEmpty(repayment.getRepaymentDetailsList())){
                repaymentDetailsList = new ArrayList<>();
                repayment.setRepaymentDetailsList(repaymentDetailsList);
            }
            if (repayment == null){
                repayment = new Repayment();
                repaymentDetailsList = new ArrayList<>();
                repayment.setRepaymentDetailsList(repaymentDetailsList);
            }

            eMandateDetails = applicationRepository.fetchEMandateDetails(refId);
            if(eMandateDetails != null){
                processedRequests = eMandateDetails.getProcessedRequests().stream()
                        .filter(req -> StringUtils.equalsIgnoreCase(EMandateConstants.PROCESS_MANDATE, req.getRequestType()))
                        .findAny().orElse(null);
            }

            BankingData = applicationRepository.fetchCamDetails(refId, instituteId, EndPointReferrer.CAM_BANKING_DETAILS);

        } catch (Exception e){
            logger.debug("Error while fetching db details for related collections");
            logger.error("{}",e.getStackTrace());
        }

        //taking values
        Application application = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication();
        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
        List<CoApplicant> coApplicantList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
        List<Collateral> collateralList = application.getCollateral();
        Request request = goNoGoCustomerApplication.getApplicationRequest().getRequest();
        int cnt = 1;
        if (appConfigurationHelper == null)
            appConfigurationHelper = new AppConfigurationHelper();

        for (String fieldList : fields) {
        try {
                switch (fieldList) {
                    case LOAN_PURPOSE:
                        configMap.put(TemplateConstants.LOAN_PURPOSE, appConfigurationHelper.setValueForNull(application.getLoanPurpose()));
                        break;

                    case APPLICATION_DATE:
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        String applicationDate = formatter.format(goNoGoCustomerApplication.getDateTime());
                        configMap.put(TemplateConstants.APPLICATION_DATE, appConfigurationHelper.setValueForNull(applicationDate));
                        break;

                    case LOAN_AMT:
                        int loanAmt = (int)application.getLoanAmount();
                        configMap.put(LOAN_AMT, appConfigurationHelper.setValueForNull(Integer.toString(loanAmt)));
                        break;

                    case APPLICANT_NAME:
                        String applicantName = GngUtils.convertNameToFullName(applicant.getApplicantName());
                        configMap.put(APPLICANT_NAME, appConfigurationHelper.setValueForNull(applicantName));
                        break;


                    case COAPPLICANT_1 :
                        if(coApplicantList.size() >= 1) {
                           configMap.put(COAPPLICANT_1,
                                        appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(0).getApplicantName())));
                        }

                        break;
                    case COAPPLICANT_2 :
                        if(coApplicantList.size() >= 2) {
                            configMap.put(COAPPLICANT_2,
                                        appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(1).getApplicantName())));

                        }

                        break;
                    case COAPPLICANT_3 :

                        if(coApplicantList.size() >= 3) {
                            configMap.put(COAPPLICANT_3,
                                        appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(2).getApplicantName())));

                            }

                        break;


                    case COAPPLICANT_NAME_1:
                        if(coApplicantList.size() >= 1) {
                            configMap.put(COAPPLICANT_NAME_1,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(0).getApplicantName()))));
                        }

                        break;
                    case COAPPLICANT_NAME_2:
                        if(coApplicantList.size() >= 2) {
                            configMap.put(COAPPLICANT_NAME_2,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(1).getApplicantName()))));

                        }

                        break;
                    case COAPPLICANT_NAME_3:

                        if(coApplicantList.size() >= 3) {
                            configMap.put(COAPPLICANT_NAME_3,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(2).getApplicantName()))));

                        }

                        break;

                    case COAPPLICANT_4:
                        if(coApplicantList.size() >= 4) {
                            configMap.put(COAPPLICANT_4,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(3).getApplicantName()))));

                        }

                        break;

                    case COAPPLICANT_5:
                        if(coApplicantList.size() >= 5) {
                            configMap.put(COAPPLICANT_5,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(4).getApplicantName()))));

                        }
                        break;

                    case COAPPLICANT_6:
                        if(coApplicantList.size() >= 6) {
                            configMap.put(COAPPLICANT_6,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(5).getApplicantName()))));

                        }
                        break;

                    case COAPPLICANT_7:
                        if(coApplicantList.size() >= 7) {
                            configMap.put(COAPPLICANT_7,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(6).getApplicantName()))));

                        }
                        break;

                    case COAPPLICANT_8:
                        if(coApplicantList.size() >= 8) {
                            configMap.put(COAPPLICANT_8,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(7).getApplicantName()))));

                        }
                        break;

                    case COAPPLICANT_9:
                        if(coApplicantList.size() >= 9) {
                            configMap.put(COAPPLICANT_9,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(8).getApplicantName()))));

                        }
                        break;

                    case COAPPLICANT_10:
                        if(coApplicantList.size() >= 10) {
                            configMap.put(COAPPLICANT_10,
                                    "Co-Applicant Name: ".concat(appConfigurationHelper.setValueForNull(GngUtils.convertNameToFullName(coApplicantList.get(9).getApplicantName()))));

                        }
                        break;

                    case REF_ID:
                        configMap.put(REF_ID, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getGngRefId()));
                        break;

                    case MOBILE_NO:
                        applicant.getPhone().forEach(obj ->{
                            configMap.put(MOBILE_NO, appConfigurationHelper.setValueForNull(appConfigurationHelper.setValueForNull(obj.getPhoneNumber())));
                        });

                        break;

                    case ADDRESS:
                        applicant.getAddress().forEach(obj -> {
                            configMap.put(ADDRESS, appConfigurationHelper.getCompleteAddress(obj.getAddressLine1(), obj.getAddressLine2(), obj.getLandMark()));
                        });
                        break;

                    case EMAIL:
                        applicant.getEmail().forEach(obj -> {
                            configMap.put(EMAIL, appConfigurationHelper.setValueForNull(obj.getEmailAddress()));
                        });
                        break;

                    case LOAN_TYPE:
                        if (applicant.getProfessionIncomeDetails() != null){
                            configMap.put(LOAN_TYPE, appConfigurationHelper.setValueForNull(applicant.getProfessionIncomeDetails().getLoanScheme()));
                        }

                        break;

                    case LOAN_SCHEME:
                        configMap.put(LOAN_SCHEME, appConfigurationHelper.setValueForNull(applicant.getProfessionIncomeDetails().getLoanScheme()));
                        break;

                    case BRANCH_ADD:
                        configMap.put(BRANCH_ADD, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData()
                                .getBranchV2().getLocation()));
                        break;

                    case BRANCH_NAME:
                        configMap.put(BRANCH_NAME, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData()
                                .getBranchV2().getBranchName()));
                        break;

                    case LOCATION:
                        configMap.put(LOCATION, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData()
                                .getBranchV2().getBranchName()));
                        break;

                    case BRANCH_MANAGER_NAME:
                        configMap.put(LOAN_SCHEME, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData()
                                .getBranchV2().getBranchManagerName()));
                        break;

                    case PROPERTY_OWNER:
                        application.getCollateral().forEach(obj -> {
                            obj.getOwnerNames().forEach(name -> {
                                configMap.put(PROPERTY_OWNER, appConfigurationHelper.setValueForNull(name.getFirstName()));
                            });
                        });

                        break;

                    case INSURENCE_PREMIUM:
                        configMap.put(INSURENCE_PREMIUM, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getInsuranceAmt())));

                        break;

                    case EMI_DATE:
                        if(loanCharges.getEmiStartDate() == null){
                            loanCharges.setEmiStartDate(new Date());
                        }
                        configMap.put(EMI_DATE, appConfigurationHelper.setValueForNull(loanCharges.getEmiStartDate().toString()));

                        break;

                    case TOTAL_LOAN_AMT:
                        int totalLoanAmt = (int)loanCharges.getNetDisbAmt();
                        configMap.put(TOTAL_LOAN_AMT, appConfigurationHelper.setValueForNull(Integer.toString(totalLoanAmt)));

                        break;

                    case PROCESSING_FEE:
                        int processingFee = (int)loanCharges.getProFees();
                        configMap.put(PROCESSING_FEE, appConfigurationHelper.setValueForNull(Integer.toString(processingFee)));

                        break;

                    case SANCTION_LOAN_AMT:
                        int sanctionLoanAmt;
                        if(!Double.valueOf(loanCharges.getLoanDetails().getFinalLoanApprovedAmt()).equals(Double.valueOf(0))){
                            sanctionLoanAmt = (int) loanCharges.getLoanDetails().getFinalLoanApprovedAmt();                         //Removal of Decimal Point
                        } else {
                            sanctionLoanAmt = (int) camDetails.getSummary().getFinalLoanApproved();
                        }
                        configMap.put(SANCTION_LOAN_AMT, appConfigurationHelper.setValueForNull(Integer.toString(sanctionLoanAmt)));
                        break;

                    case INSURENCE_AMT:
                        int insuranceAmount;
                        if(!Double.valueOf(loanCharges.getInsuranceAmt()).equals(Double.valueOf(0))) {
                            insuranceAmount = (int) loanCharges.getInsuranceAmt();
                        } else {
                            insuranceAmount = (int) camDetails.getSummary().getInsuranceOfferedAmt();
                        }
                        configMap.put(INSURENCE_AMT, appConfigurationHelper.setValueForNull(Integer.toString(insuranceAmount)));

                        break;

                    case CURRENT_DISB_DATE:
                        configMap.put(CURRENT_DISB_DATE, appConfigurationHelper.setValueForNull(loanCharges.getDisbursedDate().toString()));

                        break;

                    case CURRENT_DISB_AMT:
                        configMap.put(CURRENT_DISB_AMT, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getNetDisbAmt())));

                        break;

                    case REPAYMENT_TERM:
                        configMap.put(REPAYMENT_TERM, appConfigurationHelper.setValueForNull(camDetails.getSummary().getTenure()));

                        break;

                    case ROI_TYPE:
                        configMap.put(ROI_TYPE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getRoiType()));

                        break;

                    case MODE_OF_PAYMENT:
                        configMap.put(MODE_OF_PAYMENT, appConfigurationHelper.setValueForNull(applicant.getProfessionIncomeDetails().getModeOfPayment()));

                        break;

                    case APPLICABLE_ROI:
                        configMap.put(APPLICABLE_ROI, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getRoi())));

                        break;

                    case EMI:
                        int emi = (int)camDetails.getSummary().getEmi();
                        configMap.put(EMI, appConfigurationHelper.setValueForNull(Integer.toString(emi)));

                        break;

                    case PROCESSING_FEES:
                        configMap.put(PROCESSING_FEES, appConfigurationHelper.setValueForNull(camDetails.getSummary().getPfPercentage()));

                        break;

                    case TERMS_OF_LOANS:

                            if (disbursementMemo.getLoanCharges() != null && disbursementMemo.getLoanCharges().getTenureDetails() != null){
                                configMap.put(TERMS_OF_LOANS, appConfigurationHelper.setValueForNull(Integer.toString(disbursementMemo.getLoanCharges().getTenureDetails().getDisbTenureMonths())));
                            }

                        break;

                    case TYPE_OF_INTREST:
                        configMap.put(TYPE_OF_INTREST, appConfigurationHelper.setValueForNull(camDetails.getSummary().getRoiType()));

                        break;

                    case TENURE:
                        configMap.put(TENURE, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getTenureDetails().getTenureMonths())));

                        break;

                    case REQUESTED_LOANAMT:
                        configMap.put(REQUESTED_LOANAMT, appConfigurationHelper.setValueForNull(Double.toString(application.getLoanAmount())));

                        break;

                    case ROI:

                        //For PL ROI from Cam Details
                        if (goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct() == Product.PL){
                            int roi = (int)camDetails.getSummary().getRoi();
                            configMap.put(ROI, appConfigurationHelper.setValueForNull(Integer.toString(roi)));
                        }else {
                            int roi = (int) loanCharges.getInterestDetails().getInterestRate();
                            configMap.put(ROI, appConfigurationHelper.setValueForNull(Integer.toString(roi)));
                        }
                        break;

                    case SANCTION_LETTER_VALIDITY:
                        configMap.put("SANCTION_LETTER_VALIDITY", "90 days from date of this letter");
                        break;

                    case TOTAL_LOAN_DISB:
                        disbursementMemo.dmInputList.forEach(obj -> {
                            configMap.put(TOTAL_LOAN_DISB, appConfigurationHelper.setValueForNull(obj.getDmAmt()));
                        });

                        break;

                    case BANK_NAME:
                        disbursementMemo.dmInputList.forEach(obj -> {
                            configMap.put(BANK_NAME, appConfigurationHelper.setValueForNull(obj.dmBankName));
                        });
                        break;

                    case BANK_ACC_NO_ENDING_WITH:
                        disbursementMemo.dmInputList.forEach(obj -> {
                            if(StringUtils.isNotEmpty(obj.dmAccNo))obj.dmAccNo = obj.dmAccNo.replace(obj.dmAccNo.substring(0, obj.dmAccNo.length()-4), "xxxxx");
                            configMap.put(BANK_ACC_NO_ENDING_WITH, appConfigurationHelper.setValueForNull(obj.dmAccNo));
                        });
                        break;

                    case BANK_ACC_NO:
                        disbursementMemo.dmInputList.forEach(obj -> {
                            configMap.put(BANK_ACC_NO, appConfigurationHelper.setValueForNull(obj.dmAccNo));
                        });
                        break;

                    case PD_DONE_BY:
                        configMap.put(PD_DONE_BY, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                .getPersonalDiscussion().getPdDoneBy()));

                        break;

                    case PD_DATE:
                        configMap.put(PD_DATE, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                .getPersonalDiscussion().getDiscussiondate().toString()));

                        break;

                    case VINTAGE :
                        configMap.put(VINTAGE,appConfigurationHelper.setValueForNull(camDetails.getSummary().getBusinessVintage()));
                        break;

                    case CAM_DATE :
                        configMap.put(CAM_DATE,appConfigurationHelper.setValueForNull(camDetails.getSummary().getApprovalDate().toString()));
                        break;

                    case AGE :
                        configMap.put(AGE,appConfigurationHelper.setValueForNull(Integer.toString(applicant.getAge())));
                        break;

                    case RELATION_WITH_EQUITAS :
                        if(CollectionUtils.isNotEmpty(coApplicantList)) {
                            for (CoApplicant coapp : coApplicantList) {
                                configMap.put(RELATION_WITH_EQUITAS,appConfigurationHelper.setValueForNull(coapp.getRelationWithApplicant()));
                            }
                        }
                        break;

                    case OBLIGATION :
                        configMap.put(OBLIGATION,appConfigurationHelper.setValueForNull(Double.toString(eligibility.getFixedOblgation())));
                        break;

                    case USAGE :
                        for (Collateral list : collateralList) {
                            configMap.put(USAGE,appConfigurationHelper.setValueForNull(list.getUsage()));
                        }
                        break;

                    case OCCUPANCY_STATUS :
                        for (Collateral list : collateralList) {
                            configMap.put(OCCUPANCY_STATUS,appConfigurationHelper.setValueForNull(list.getOccupation()));
                        }
                        break;

                    case PROPERTY_TYPE :
                        for (Collateral list : collateralList) {
                            configMap.put(PROPERTY_TYPE,appConfigurationHelper.setValueForNull(list.getType()));
                        }
                        break;

                    case LAN:
                        configMap.put(LAN, appConfigurationHelper.setValueForNull(camDetails.getSummary().getLan()));

                        break;

                    case LOGINDATE:
                        configMap.put(LOGINDATE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getLoginDate().toString()));

                        break;

                    case APPROVALDATE:
                        if (camDetails.getSummary().getApprovalDate() != null) {
                        configMap.put(APPROVALDATE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getApprovalDate().toString()));
                        }
                        break;

                    case RMNAME:
                        configMap.put(RMNAME, appConfigurationHelper.setValueForNull(camDetails.getSummary().getRmName()));

                        break;

                    case SOURCINGCHANNEL:
                        configMap.put(SOURCINGCHANNEL, appConfigurationHelper.setValueForNull(application.getChannel()));
                        break;

                    case APPLICANTNAME:
                        configMap.put(APPLICANTNAME, appConfigurationHelper.setValueForNull(""));

                        break;

                    case LOANAMOUNT:
                        String loanAmount = GngNumUtil.doubleConversion(application.getLoanAmount());
                        configMap.put(LOANAMOUNT, appConfigurationHelper.setValueForNull(loanAmount));
                        break;

                    case MOBILENUMBER:
                        String mobileNo = MOBILENUMBER;
                        if(CollectionUtils.isNotEmpty(applicant.getPhone()))
                            mobileNo = applicant.getPhone().get(0).getPhoneNumber();
                        configMap.put(MOBILENUMBER, appConfigurationHelper.setValueForNull(mobileNo));
                        break;

                    case DATEOFBIRTH:
                        configMap.put(DATEOFBIRTH, appConfigurationHelper.setValueForNull(applicant.getDateOfBirth()));
                        break;

                    case TELEPHONENUMBER:
                        configMap.put(TELEPHONENUMBER, appConfigurationHelper.setValueForNull(camDetails.getSummary().getTelephoneNumber()));

                        break;

                    case ROIOFFERED:
                        configMap.put(ROIOFFERED, appConfigurationHelper.setValueForNull(camDetails.getSummary().getRoiOffered()));

                        break;

                    case TENORREQUESTED:
                        configMap.put(TENORREQUESTED, appConfigurationHelper.setValueForNull(application.getTenorRequested()));
                        break;

                    case PROCESSINGFEES:
                        int proFee = (int)camDetails.getSummary().getProcessingFees();
                        configMap.put(PROCESSINGFEES, appConfigurationHelper.setValueForNull(Integer.toString(proFee)));
                        break;

                    case PFPERCENTAGE:
                        configMap.put(PFPERCENTAGE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getPfPercentage()));

                        break;

                    case COMPANYNAME:
                        configMap.put(COMPANYNAME, appConfigurationHelper.setValueForNull(camDetails.getSummary().getCompanyName()));

                        break;

                    case PSL:
                        configMap.put(PSL, appConfigurationHelper.setValueForNull(camDetails.getSummary().getPslSubClassification()));

                        break;

                    case SCHEME:
                        configMap.put(SCHEME, appConfigurationHelper.setValueForNull(camDetails.getSummary().getScheme()));

                        break;

                    case INDUSTRY:
                        configMap.put(INDUSTRY, appConfigurationHelper.setValueForNull(camDetails.getSummary().getIndustry()));

                        break;

                    case WORKINGSINCE:
                        configMap.put(WORKINGSINCE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getWorkingSince()));

                        break;

                    case TOTALWORKEXPERIENCE:
                        configMap.put(TOTALWORKEXPERIENCE, appConfigurationHelper.setValueForNull(Integer.toString(camDetails.getSummary().getTotalWorkExperience())));

                        break;

                    case PSLSUBCLASSIFICATION:
                        configMap.put(PSLSUBCLASSIFICATION, appConfigurationHelper.setValueForNull(camDetails.getSummary().getPslSubClassification()));

                        break;

                    case BTBANKORNBFCNAME:
                        configMap.put(BTBANKORNBFCNAME, appConfigurationHelper.setValueForNull(camDetails.getSummary().getBtBankOrNBFCName()));

                        break;

                    case ORIGINALLOANAMOUNT:
                        configMap.put(ORIGINALLOANAMOUNT, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getOriginalLoanAmount())));

                        break;

                    case TOTALTENURE:
                        configMap.put(TOTALTENURE, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getTotalTenure())));

                        break;

                    case LOANTYPE:
                        configMap.put(LOANTYPE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getLoanType()));

                        break;

                    case EXISTINGLAN:
                        configMap.put(EXISTINGLAN, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getExistingLan())));

                        break;

                    case MOB:
                        configMap.put(MOB, appConfigurationHelper.setValueForNull(Integer.toString(camDetails.getSummary().getMob())));

                        break;

                    case FINALLOANAPPROVED:
                        configMap.put(FINALLOANAPPROVED, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getFinalLoanApproved())));

                        break;

                    case BTOREXISTINGLOANAMT:
                        configMap.put(BTOREXISTINGLOANAMT, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getBtOrExistingLoanAmt())));

                        break;

                    case TOPUPORADDITIONALAMT:
                        configMap.put(TOPUPORADDITIONALAMT, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getTopUpOrAdditionalAmt())));

                        break;

                    case INSURANCEOFFEREDAMT:
                        int insuranceOffAmt = (int)camDetails.getSummary().getInsuranceOfferedAmt();
                        configMap.put(INSURANCEOFFEREDAMT, appConfigurationHelper.setValueForNull(Integer.toString(insuranceOffAmt)));

                        break;

                    case MAXLTV:
                        configMap.put(MAXLTV, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getMaxLTV())));

                        break;

                    case LTVFORLOANAPPROVED:
                        configMap.put(LTVFORLOANAPPROVED, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getLtvForLoanApproved())));

                        break;

                    case TOPUPEMI:
                        configMap.put(TOPUPEMI, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getTopupEMI())));

                        break;

                    case INSURANCEOFFEREDASLOAN:
                        configMap.put(INSURANCEOFFEREDASLOAN, appConfigurationHelper.setValueForNull(Double.toString(camDetails.getSummary().getInsuranceOfferedAmt())));

                        break;

                    case PDSTATUS:
                        configMap.put(PDSTATUS, appConfigurationHelper.setValueForNull(""));

                        break;

                    case INSTALLMENTTYPE:
                        configMap.put(INSTALLMENTTYPE, appConfigurationHelper.setValueForNull(camDetails.getSummary().getInstallmentType()));

                        break;

                    case PROPERTYVALUECONSIDR:
                        configMap.put(PROPERTYVALUECONSIDR, appConfigurationHelper.setValueForNull(camDetails.getSummary().getPropertyvalueConsidr()));

                        break;

                    case FINALLOANAPPROVEDBY:
                        configMap.put(FINALLOANAPPROVEDBY, appConfigurationHelper.setValueForNull(camDetails.getSummary().getFinalLoanApprovedBy()));

                        break;

                    case PAN_NO:
                        applicant.getKyc().forEach(obj->{
                            if (StringUtils.equals(obj.getKycName(),GNGWorkflowConstant.PAN.toFaceValue()))
                                configMap.put(PAN_NO, appConfigurationHelper.setValueForNull(obj.getKycNumber()));
                        });

                        break;

                    case CITY:
                        configMap.put(CITY,appConfigurationHelper.setValueForNull(applicant.getAddress().get(0).getCity()));

                        break;

                    case GENDER:
                        configMap.put(GENDER,appConfigurationHelper.setValueForNull(applicant.getGender()));

                        break;

                    case PIN_NO:
                        configMap.put(PIN_NO,appConfigurationHelper.setValueForNull(Long.toString(applicant.getAddress().get(0).getPin())));

                        break;

                    case DURATION_OF_STAY:
                        configMap.put(DURATION_OF_STAY,appConfigurationHelper.setValueForNull(Integer.toString(applicant.getAddress().get(0).getTimeAtAddress())));

                        break;

                    case RESIDENCE_TYPE:
                        applicant.getAddress().forEach(obj->{if (StringUtils.equals(obj.getAddressType(),GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                            configMap.put(RESIDENCE_TYPE, appConfigurationHelper.setValueForNull(obj.getResidenceAddressType()));
                        });
                        break;

                    case MONTHLYINCOME:
                        configMap.put(MONTHLYINCOME,appConfigurationHelper.setValueForNull(Double.toString(applicant.getEmployment().get(0).getMonthlySalary())));

                        break;

                    case TIME_IN_CURRENTCMPNY:
                        configMap.put(TIME_IN_CURRENTCMPNY,appConfigurationHelper.setValueForNull(Integer.toString(applicant.getEmployment().get(0).getTimeWithEmployer())));

                        break;

                    case COMPANYADDRESS:
                        applicant.getAddress().forEach(obj->{
                            if (StringUtils.equalsIgnoreCase(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                configMap.put(COMPANYADDRESS, appConfigurationHelper.getCompleteAddress(obj.getAddressLine1(), obj.getAddressLine1(), obj.getLandMark()));
                            }
                        });

                        break;

                    case OFFICEADDRESS:
                        applicant.getAddress().forEach(obj -> {
                            if (StringUtils.equalsIgnoreCase(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                configMap.put(OFFICEADDRESS, appConfigurationHelper.getCompleteAddress(obj.getAddressLine1(), obj.getAddressLine1(), obj.getLandMark()).concat(Long.toString(obj.getPin())));
                            }
                        });

                        break;

                    case EMAIL_OFFICE:
                        configMap.put(EMAIL_OFFICE,appConfigurationHelper.setValueForNull(applicant.getProfessionIncomeDetails().getOfficeEmail()));
                        break;

                    case LANDLINE_OFFICE:
                        if(CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(LANDLINE_OFFICE, appConfigurationHelper.setValueForNull(obj.getLandLine()));
                                }else {
                                    configMap.put(LANDLINE_OFFICE, "");
                                }
                            });
                        }else{
                            configMap.put(LANDLINE_OFFICE, "");
                        }
                        break;

                    case MIFIN_PROSPECTCODE:
                        configMap.put(MIFIN_PROSPECTCODE,appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getMiFinProspectCode()));

                        break;

                    case LOAN_AMOUNT:
                        String loanAMount = GngNumUtil.doubleConversion(application.getLoanAmount());
                        configMap.put(LOAN_AMOUNT,appConfigurationHelper.setValueForNull(loanAMount));
                        break;

                    case TENOR:
                        configMap.put(TENOR,appConfigurationHelper.setValueForNull(application.getTenorRequested()));


                        break;

                    case LOANPURPOSE:
                        configMap.put(LOANPURPOSE,appConfigurationHelper.setValueForNull(application.getLoanPurpose()));

                        break;

                    case DOB:
                        configMap.put(DOB,appConfigurationHelper.setValueForNull(applicant.getDateOfBirth()));
                        break;

                    case TOTALEXPERIENCE:
                        configMap.put(TOTALEXPERIENCE,appConfigurationHelper.setValueForNull(Integer.toString(applicant.getEmployment().get(0).getTimeWithEmployer())));

                        break;

                    case COMPANY_NAME:
                        configMap.put(COMPANY_NAME,appConfigurationHelper.setValueForNull(applicant.getEmployment().get(0).getEmploymentName()));
                        break;

                    case SALARYBANKNAME:
                        configMap.put(SALARYBANKNAME,appConfigurationHelper.setValueForNull(applicant.getBankingDetails().get(0).getBankName()));
                        break;

                    case SALARYBANKACCNO:
                        configMap.put(SALARYBANKACCNO,appConfigurationHelper.setValueForNull(applicant.getBankingDetails().get(0).getAccountNumber()));
                        break;

                    case DATE_OF_APPLICATION:
                        configMap.put(DATE_OF_APPLICATION,appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getHeader().getDateTime().toString()));
                        break;

                    case MARGIN_ROI :
                        if(eligibility != null){
                                    configMap.put(MARGIN_ROI, "");
                        }else{
                            configMap.put(MARGIN_ROI, "");
                        }
                        break;

                    case TOTAL_ROI :
                        if(eligibility != null){
                            configMap.put(TOTAL_ROI, "");
                        }else{
                            configMap.put(TOTAL_ROI, "");
                        }
                        break;

                    case SANCTION_AMT :
                        if(eligibility != null){
                            configMap.put(SANCTION_AMT, "");
                        }else{
                            configMap.put(SANCTION_AMT, "");
                        }
                        break;

                    case TENOR_ELIGIBILITY :
                        if(eligibility != null){
                            configMap.put(TENOR_ELIGIBILITY, "");
                        }else{
                            configMap.put(TENOR_ELIGIBILITY, "");
                        }
                        break;

                    case TODAY_DATE :
                        DateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
                        Date today = new Date();
                        String todayWithZeroTime = formatter1.format(today);
                        configMap.put(TODAY_DATE, todayWithZeroTime);
                        break;

                    case CONDITION :
                        String condition = "";
                        if(CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())){
                            int condIndex = 1;
                            for (SanctionConditionDetails temp : sanctionConditions.getScDetailsList()) {
                                condition = condition + condIndex + ". "+ temp.getCondition() + "<br/>";
                                condIndex++;
                            }
                            configMap.put(CONDITION, appConfigurationHelper.setValueForNull(condition));
                        }else {
                            configMap.put(CONDITION, "");
                        }
                        break;

                    case ADVANCE_EMI :
                        if(eligibility != null){
                            configMap.put(ADVANCE_EMI, "");
                        }else{
                            configMap.put(ADVANCE_EMI, "");
                        }
                        break;

                    case DESCRIPTIONS :
                        String description = "";
                        if(CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())){
                            for (SanctionConditionDetails temp : sanctionConditions.getScDetailsList()) {
                                description = description + temp.getDescription() + "<br/>";
                            }
                            configMap.put(DESCRIPTIONS, appConfigurationHelper.setValueForNull(description));
                        }else {
                            configMap.put(DESCRIPTIONS, "");
                        }
                        break;

                    case RAISED :
                        String raised = "";
                        if(CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())){
                            for (SanctionConditionDetails temp : sanctionConditions.getScDetailsList()) {
                                raised = raised + temp.getRaisedBy() + "<br/>";
                            }
                            configMap.put(RAISED, appConfigurationHelper.setValueForNull(raised));
                        }else {
                            configMap.put(RAISED, "");
                        }
                        break;

                    case GUARANTOR:
                        String guarantor = "";
                        if(CollectionUtils.isNotEmpty(coApplicantList)){
                            for (CoApplicant coApp : coApplicantList) {
                                String name = GngUtils.convertNameToFullName(coApp.getApplicantName());
                                guarantor = guarantor + name + "- ";
                                guarantor = guarantor + coApp.getEntityType() + ", ";
                            }
                            configMap.put(GUARANTOR, appConfigurationHelper.setValueForNull(guarantor));
                        }
                        break;

                    case CAM_SUMMARY_REMARKS:
                        String camRemarks = "";
                        if(camDetails.getSummary() != null){
                            if(CollectionUtils.isNotEmpty(camDetails.getSummary().getRemarkList())) {
                                for (Remark remark : camDetails.getSummary().getRemarkList()) {
                                    camRemarks = camRemarks + remark.getComment() + ",";
                                }
                                configMap.put(CAM_SUMMARY_REMARKS, appConfigurationHelper.setValueForNull(camRemarks));
                            }

                        }else {
                            configMap.put(CAM_SUMMARY_REMARKS, "");

                        }
                        break;

                    case PRINCIPAL_OUTSTANDING:
                            int principalOutstanding = (int)loanCharges.getLoanDetails().getExistingLoanAmt();
                            configMap.put(PRINCIPAL_OUTSTANDING, appConfigurationHelper.setValueForNull(Integer.toString(principalOutstanding)));
                            camDetails.getSummary().getPos();
                        break;

                    case FOS_NAME:
                        if (goNoGoCustomerApplication.getApplicationRequest() != null && goNoGoCustomerApplication.getApplicationRequest().getHeader() != null) {
                            configMap.put(FOS_NAME, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationRequest().getHeader().getDsaId()));
                        }
                        break;
                    case DECISION:
                        configMap.put(DECISION, appConfigurationHelper.setValueForNull(goNoGoCustomerApplication.getApplicationStatus()));
                        break;
                    case DECISION_BY:
                        if(CollectionUtils.isNotEmpty(goNoGoCustomerApplication.getCroJustification())){
                            Date lastDate = null;
                            String decisionBy = "";
                            for (CroJustification justification : goNoGoCustomerApplication.getCroJustification()){
                                if(lastDate == null || (justification.getCroJustificationUpdateDate() != null &&
                                        lastDate.before(justification.getCroJustificationUpdateDate()))){
                                    decisionBy = justification.getCroID();
                                }
                            }
                            configMap.put(DECISION_BY, appConfigurationHelper.setValueForNull(decisionBy));
                        }
                        break;
                    case CPA_NAME:
                        if (goNoGoCustomerApplication.getCompleted() != null) {
                            Map<String, CompletionInfo> completed = goNoGoCustomerApplication.getCompleted();
                            for (String submitBy:completed.keySet()){
                                if (StringUtils.equalsIgnoreCase(submitBy,"submit-by-CPA")){
                                    configMap.put(CPA_NAME, appConfigurationHelper.setValueForNull(completed.get(submitBy).getUserName()));
                                }
                            }
                        }
                        break;

                    case VERIFICATION_AGENCY:
                        if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.VALUATION_VERIFICATION)){
                            if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())){
                                int agencyCounter = 1;
                                String valuationAgencyName = "";
                                for (ValuationDetails valuationDetails: valuation.getValuationDetailsList()) {
                                    String agencyName = (valuationDetails.getAgencyName() != null) ? valuationDetails.getAgencyName() : valuationDetails.getAgencyCode();
                                    valuationAgencyName = valuationAgencyName + agencyCounter + "." + agencyName + "<br/>   ";
                                    agencyCounter++;
                                }
                                configMap.put(VERIFICATION_AGENCY, appConfigurationHelper.setValueForNull(valuationAgencyName));
                            }
                        }
                        else if(verificationDetails != null) {
                            int agencyCounter = 1;
                            String verificationAgencyName = "";
                            if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.OFFICE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getOfficeVerification())) {
                                    for(PropertyVerification verification : verificationDetails.getOfficeVerification()) {
                                        String agencyName = (verification.getAgencyName() != null) ? verification.getAgencyName() :
                                                verification.getAgencyCode();
                                        verificationAgencyName = verificationAgencyName + agencyCounter + "." + agencyName + "<br/>  ";
                                        agencyCounter++;
                                    }
                                    configMap.put(VERIFICATION_AGENCY, appConfigurationHelper.setValueForNull(verificationAgencyName));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.RESIDENCE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getResidenceVerification())) {
                                    for(PropertyVerification verification : verificationDetails.getResidenceVerification()) {
                                        String agencyName = (verification.getAgencyName() != null) ? verification.getAgencyName() :
                                                verification.getAgencyCode();
                                        verificationAgencyName = verificationAgencyName + agencyCounter + "." + agencyName + "<br/>  ";
                                        agencyCounter++;
                                    }
                                    configMap.put(VERIFICATION_AGENCY, appConfigurationHelper.setValueForNull(verificationAgencyName));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.BANK_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getBankingVerification())) {
                                    for(BankingVerification verification : verificationDetails.getBankingVerification()) {
                                        String agencyName = (verification.getAgencyName() != null) ? verification.getAgencyName() :
                                                verification.getAgencyCode();
                                        verificationAgencyName = verificationAgencyName + agencyCounter + "." + agencyName + "<br/>  ";
                                        agencyCounter++;
                                    }
                                    configMap.put(VERIFICATION_AGENCY, appConfigurationHelper.setValueForNull(verificationAgencyName));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.ITR_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getItrVerification())) {
                                    for(ItrVerification verification : verificationDetails.getItrVerification()) {
                                        String agencyName = (verification.getAgencyName() != null) ? verification.getAgencyName() :
                                                verification.getAgencyCode();
                                        verificationAgencyName = verificationAgencyName + agencyCounter + "." + agencyName + "<br/>  ";
                                        agencyCounter++;
                                    }
                                    configMap.put(VERIFICATION_AGENCY, appConfigurationHelper.setValueForNull(verificationAgencyName));
                                }
                            }
                        }
                        break;

                    case VERIFICATION_TRIGGER_BY:
                        String triggerBy = "";
                        Date lastDate = null;
                        if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.VALUATION_VERIFICATION)){
                            if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
                                for (ValuationDetails obj : valuation.getValuationDetailsList()) {
                                    if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                                        if ((lastDate == null) ||
                                                (obj.getSubmissionTime() != null && lastDate.before(obj.getSubmissionTime()))) {
                                            triggerBy = obj.getTriggerByName();
                                            lastDate = obj.getSubmissionTime();
                                        }
                                    }
                                }
                                configMap.put(VERIFICATION_TRIGGER_BY, appConfigurationHelper.setValueForNull(triggerBy));
                            }
                        }
                        else if(verificationDetails != null) {
                            if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.OFFICE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getOfficeVerification())) {
                                    for(PropertyVerification obj : verificationDetails.getOfficeVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                                            if ((lastDate == null) ||
                                                    (obj.getSubmissionTime() != null && lastDate.before(obj.getSubmissionTime()))) {
                                                triggerBy = obj.getTriggerByName();
                                                lastDate = obj.getSubmissionTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFICATION_TRIGGER_BY, appConfigurationHelper.setValueForNull(triggerBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.RESIDENCE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getResidenceVerification())) {
                                    for(PropertyVerification obj : verificationDetails.getResidenceVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                                            if ((lastDate == null) ||
                                                    (obj.getSubmissionTime() != null && lastDate.before(obj.getSubmissionTime()))) {
                                                triggerBy = obj.getTriggerByName();
                                                lastDate = obj.getSubmissionTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFICATION_TRIGGER_BY, appConfigurationHelper.setValueForNull(triggerBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.BANK_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getBankingVerification())) {
                                    for(BankingVerification obj : verificationDetails.getBankingVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                                            if ((lastDate == null) ||
                                                    (obj.getSubmissionTime() != null && lastDate.before(obj.getSubmissionTime()))) {
                                                triggerBy = obj.getTriggerByName();
                                                lastDate = obj.getSubmissionTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFICATION_TRIGGER_BY, appConfigurationHelper.setValueForNull(triggerBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.ITR_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getItrVerification())) {
                                    for(ItrVerification obj : verificationDetails.getItrVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                                            if ((lastDate == null) ||
                                                    (obj.getSubmissionTime() != null && lastDate.before(obj.getSubmissionTime()))) {
                                                triggerBy = obj.getTriggerByName();
                                                lastDate = obj.getSubmissionTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFICATION_TRIGGER_BY, appConfigurationHelper.setValueForNull(triggerBy));
                                }
                            }
                        }
                        break;

                    case VERIFIED_BY:
                        String verifiedBy = "";
                        Date lastDateVerification = null;
                        if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.VALUATION_VERIFICATION)){
                            if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
                                for (ValuationDetails obj : valuation.getValuationDetailsList()) {
                                    if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                                        if ((lastDateVerification == null) ||
                                                (obj.getVerificationTime() != null && lastDateVerification.before(obj.getVerificationTime()))) {
                                            verifiedBy = obj.getVerifiedByName();
                                            lastDateVerification = obj.getVerificationTime();
                                        }
                                    }
                                }
                                configMap.put(VERIFIED_BY, appConfigurationHelper.setValueForNull(verifiedBy));
                            }
                        }
                        else if(verificationDetails != null) {
                            if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.OFFICE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getOfficeVerification())) {
                                    for(PropertyVerification obj : verificationDetails.getOfficeVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                                            if ((lastDateVerification == null) ||
                                                    (obj.getVerificationTime() != null && lastDateVerification.before(obj.getVerificationTime()))) {
                                                verifiedBy = obj.getVerifiedByName();
                                                lastDateVerification = obj.getVerificationTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFIED_BY, appConfigurationHelper.setValueForNull(verifiedBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.RESIDENCE_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getResidenceVerification())) {
                                    for(PropertyVerification obj : verificationDetails.getResidenceVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                                            if ((lastDateVerification == null) ||
                                                    (obj.getVerificationTime() != null && lastDateVerification.before(obj.getVerificationTime()))) {
                                                verifiedBy = obj.getVerifiedByName();
                                                lastDateVerification = obj.getVerificationTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFIED_BY, appConfigurationHelper.setValueForNull(verifiedBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.BANK_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getBankingVerification())) {
                                    for(BankingVerification obj : verificationDetails.getBankingVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                                            if ((lastDateVerification == null) ||
                                                    (obj.getVerificationTime() != null && lastDateVerification.before(obj.getVerificationTime()))) {
                                                verifiedBy = obj.getVerifiedByName();
                                                lastDateVerification = obj.getVerificationTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFIED_BY, appConfigurationHelper.setValueForNull(verifiedBy));
                                }
                            } else if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.ITR_VERIFICATION)) {
                                if (CollectionUtils.isNotEmpty(verificationDetails.getItrVerification())) {
                                    for(ItrVerification obj : verificationDetails.getItrVerification()) {
                                        if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                                            if ((lastDateVerification == null) ||
                                                    (obj.getSubmissionTime() != null && lastDateVerification.before(obj.getSubmissionTime()))) {
                                                verifiedBy = obj.getVerifiedByName();
                                                lastDateVerification = obj.getSubmissionTime();
                                            }
                                        }
                                    }
                                    configMap.put(VERIFIED_BY, appConfigurationHelper.setValueForNull(verifiedBy));
                                }
                            }
                        }
                        break;

                    case TOTAL_APRV_AMT:
                        int totalAprvAmt = 0;
                        if(Double.valueOf(loanCharges.getNetDisbAmt()).equals(Double.valueOf(0))) {
                            totalAprvAmt = (int) loanCharges.getNetDisbAmt();
                        }
                        configMap.put(TOTAL_APRV_AMT, appConfigurationHelper.setValueForNull(Integer.toString(totalAprvAmt)));
                        break;

                    case VERIFICATION_BY:
                        if (goNoGoCustomerApplication.getCompleted() != null) {
                            Map<String, CompletionInfo> completed = goNoGoCustomerApplication.getCompleted();
                            if (StringUtils.equalsIgnoreCase(verificationType, EndPointReferrer.VALUATION_VERIFICATION)){
                                verificationType = GNGWorkflowConstant.SAVEVALUATION.toFaceValue();
                            }
                            if (completed.containsKey(verificationType)){
                                configMap.put(VERIFICATION_BY, appConfigurationHelper.setValueForNull(completed.get(verificationType).getUserName()));
                            }
                        }
                        break;

                    case RESIDENCE_ADDRESS:
                        applicant.getAddress().forEach(address -> {
                           if (StringUtils.equalsIgnoreCase(address.getAddressType(),GNGWorkflowConstant.RESIDENCE.toFaceValue())){
                               configMap.put(RESIDENCE_ADDRESS, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                           }
                        });
                        break;

                    case PERMANENTADDRESS:
                        applicant.getAddress().forEach(address -> {
                            if (StringUtils.equalsIgnoreCase(address.getAddressType(),GNGWorkflowConstant.PERMANENT.toFaceValue())){
                                configMap.put(PERMANENTADDRESS,appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                            }
                        });
                        break;

                    case COAPP1_PERM_ADD:
                    case COAPP2_PERM_ADD:
                    case COAPP3_PERM_ADD:

                        for (CoApplicant coApplicant: coApplicantList) {
                            if (addressCounter == 0){
                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(),GNGWorkflowConstant.PERMANENT.toFaceValue())){
                                        configMap.put(COAPP1_PERM_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }

                                }
                            }
                            if (addressCounter == 1){
                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(),GNGWorkflowConstant.PERMANENT.toFaceValue())){
                                        configMap.put(COAPP2_PERM_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }

                            }
                            if (addressCounter == 3){

                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(),GNGWorkflowConstant.PERMANENT.toFaceValue())){
                                        configMap.put(COAPP3_PERM_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            addressCounter++;
                        }
                        break;

                    case COAPP1_RES_ADD:
                    case COAPP2_RES_ADD:
                    case COAPP3_RES_ADD:

                        for (CoApplicant coApplicant: coApplicantList) {
                            if (addResCounter == 0){
                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())){
                                        configMap.put(COAPP1_RES_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            if (addResCounter == 1){
                                for (CustomerAddress address:coApplicant.getAddress()){

                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())){
                                        configMap.put(COAPP2_RES_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }

                            }
                            if (addResCounter == 3){

                                for (CustomerAddress address:coApplicant.getAddress()){

                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())){
                                        configMap.put(COAPP3_RES_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            addResCounter++;
                        }
                        break;

                    case COAPP1_OFFICE_ADD:
                    case COAPP2_OFFICE_ADD:
                    case COAPP3_OFFICE_ADD:
                        for (CoApplicant coApplicant: coApplicantList) {
                            if (addOffCounter == 0){
                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())){
                                        configMap.put(COAPP1_OFFICE_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            if (addOffCounter == 1){
                                for (CustomerAddress address:coApplicant.getAddress()){

                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())){
                                        configMap.put(COAPP2_OFFICE_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            if (addOffCounter == 3){

                                for (CustomerAddress address:coApplicant.getAddress()){
                                    if (StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())){
                                        configMap.put(COAPP3_OFFICE_ADD, appConfigurationHelper.setValueForNull(appConfigurationHelper.getCompleteAddress(address)));
                                    }
                                }
                            }
                            addOffCounter++;
                        }
                        break;

                    case TERM_OF_LOAN:
                        configMap.put(TERM_OF_LOAN, appConfigurationHelper.setValueForNull(Integer.toString(loanCharges.getTenureDetails().getDisbTenureMonths())));
                        break;

                    case INTEREST_TYPE:
                        configMap.put(INTEREST_TYPE, appConfigurationHelper.setValueForNull(loanCharges.getTypeOfROI()));
                        break;

                    case INITIAL_MARGIN_DEPOSIT:
                        int initial_margin_deposit = (int)loanCharges.getImdCollectedAmt();
                        configMap.put(INITIAL_MARGIN_DEPOSIT, appConfigurationHelper.setValueForNull(Integer.toString(initial_margin_deposit)));
                        break;

                    case MODE_OF_REPAYMENT:
                        if (CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())){
                            repayment.getRepaymentDetailsList().forEach(repaymentDetails ->
                            configMap.put(MODE_OF_REPAYMENT, appConfigurationHelper.setValueForNull(repaymentDetails.getRepaymentType())));
                        }
                        break;

                    case COAPP1_EMAIL:
                    case COAPP2_EMAIL:
                    case COAPP3_EMAIL:

                        for (CoApplicant coApplicant: coApplicantList){
                            if (emailCounter == 0 ){
                                for (Email email: coApplicant.getEmail())
                                    configMap.put(COAPP1_EMAIL, appConfigurationHelper.setValueForNull(email.getEmailAddress()));
                                emailCounter++;

                            }
                            if (emailCounter == 1 ) {
                                for (Email email: coApplicant.getEmail())
                                    configMap.put(COAPP2_EMAIL, appConfigurationHelper.setValueForNull(email.getEmailAddress()));
                                emailCounter++;
                            }
                            if (emailCounter == 2 ) {
                                for (Email email: coApplicant.getEmail()){
                                    configMap.put(COAPP3_EMAIL, appConfigurationHelper.setValueForNull(email.getEmailAddress()));
                                    emailCounter++;
                                }
                            }
                        }
                        break;

                    case COAPP1_PHONE:
                    case COAPP2_PHONE:
                    case COAPP3_PHONE:
                        for (CoApplicant coApplicant: coApplicantList){
                            if (phoneCounter == 0){
                                for (Phone phone: coApplicant.getPhone()){
                                    configMap.put(COAPP1_PHONE, appConfigurationHelper.setValueForNull(phone.getPhoneNumber()));
                                }
                            }
                            if (phoneCounter == 1){
                                for (Phone phone: coApplicant.getPhone()){
                                    configMap.put(COAPP2_PHONE, appConfigurationHelper.setValueForNull(phone.getPhoneNumber()));
                                }
                            }
                            if (phoneCounter == 2){
                                for (Phone phone: coApplicant.getPhone()){
                                    configMap.put(COAPP3_PHONE, appConfigurationHelper.setValueForNull(phone.getPhoneNumber()));
                                }
                            }
                        }
                        break;

                    case ACCHOLDER_NAME:
                        if (CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())){
                            repayment.getRepaymentDetailsList().forEach(repaymentDetails ->
                                    configMap.put(ACCHOLDER_NAME,
                                            appConfigurationHelper.setValueForNull( GngUtils.convertNameToFullName(repaymentDetails.getAccountHolderName()))));
                        }
                        break;

                    case TYPE_OF_ACC:

                            repayment.getRepaymentDetailsList().forEach(repaymentDetails ->
                                    configMap.put(TYPE_OF_ACC, appConfigurationHelper.setValueForNull(repaymentDetails.getAccountType())));

                        break;

                    case IFSC:
                        if (CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())){
                            repayment.getRepaymentDetailsList().forEach(repaymentDetails ->
                                    configMap.put(IFSC, appConfigurationHelper.setValueForNull(repaymentDetails.getIfscCode())));
                        }
                        break;

                    case CHQ_IN_FAV:


                            disbursementMemo.getDmInputList().forEach(dmInput -> {
                                configMap.put(CHQ_IN_FAV, appConfigurationHelper.setValueForNull(dmInput.getDmFavourting()));
                            });



                        break;

                    case SANCTION_LOANAMT_WORDS:
                        configMap.put(SANCTION_LOANAMT_WORDS,
                                appConfigurationHelper.setValueForNull(GngNumUtil.convert(((long) (loanCharges.getLoanDetails().getFinalLoanApprovedAmt())))));
                        break;

                    case COLLATERAL_ADD:
                        String singleAdd ;
                        String address = new String("") ;
                        String collateralCount;
                        String fullStop = ".   ";
                        int i = 1;
                        for (Collateral collateral: collateralList){
                            collateralCount = "Collateral"+i+": ";
                            singleAdd = collateralCount + appConfigurationHelper.getCompleteAddress(collateral.getAddress())+fullStop;
                            address = address.concat(singleAdd);
                            i++;
                        }
                        configMap.put(COLLATERAL_ADD, appConfigurationHelper.setValueForNull(address));
                        break;

                    case CREDIT_SUBMIT:
                        if (goNoGoCustomerApplication.getCompleted() != null) {
                            Map<String, CompletionInfo> completed = goNoGoCustomerApplication.getCompleted();
                            for (String submitBy:completed.keySet()){
                                if (StringUtils.equalsIgnoreCase(submitBy,"submit-by-CREDIT")){
                                    configMap.put(CREDIT_SUBMIT, appConfigurationHelper.setValueForNull(completed.get(submitBy).getUserName()));
                                }
                            }
                        }
                        break;

                    case MODEOFPAYMENT:
                        if (CollectionUtils.isNotEmpty(disbursementMemo.getDmInputList())){
                            disbursementMemo.getDmInputList().forEach(dmInput ->
                            configMap.put(MODEOFPAYMENT, appConfigurationHelper.setValueForNull(dmInput.getDmPaymentMode()) ));
                        }
                        break;

                    case DISB_BANK_NAME:
                        if (CollectionUtils.isNotEmpty(disbursementMemo.getDmInputList())){
                            disbursementMemo.getDmInputList().forEach(dmInput ->
                                    configMap.put(DISB_BANK_NAME, appConfigurationHelper.setValueForNull(dmInput.getDmBankName())));
                        }
                        break;

                    case PLACE_OF_EXECUTION:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())){
                            for (CustomerAddress address1 : applicant.getAddress()){
                                if (StringUtils.equalsIgnoreCase(address1.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())){
                                    configMap.put(PLACE_OF_EXECUTION, appConfigurationHelper.setValueForNull(address1.getCity()));
                                }
                            }
                        }
                        break;

                    case COAPPLICANT:
                        String coApplicant = "Co-Applicant : ";
                        if(CollectionUtils.isNotEmpty(coApplicantList)){
                            for (CoApplicant coApp : coApplicantList) {
                                coApplicant = coApplicant + GngUtils.convertNameToFullName(coApp.getApplicantName()) + "<br/>";
                            }
                            configMap.put(COAPPLICANT, appConfigurationHelper.setValueForNull(coApplicant));
                        }
                        break;


                    case SANCTION_CON_WITH_NUMBER :
                        String conditionWithno = null;
                        int count = 20;
                        if(CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())){
                            for (SanctionConditionDetails temp : sanctionConditions.getScDetailsList()) {
                                conditionWithno = conditionWithno +  count + ". " + temp.getCondition() + "<br/>";
                                count ++;
                            }
                            configMap.put(SANCTION_CON_WITH_NUMBER, appConfigurationHelper.setValueForNull(conditionWithno));
                        }else {
                            configMap.put(SANCTION_CON_WITH_NUMBER, "");
                        }
                        break;

                    case CAM_TENOR:
                        if(camDetails.getSummary() != null){
                            configMap.put(CAM_TENOR,appConfigurationHelper.setValueForNull(camDetails.getSummary().getTenure()));
                        }else {
                            configMap.put(CAM_TENOR,"");
                        }

                    case CAM_NOTEPAD_COMMENT :
                        if(camDetails.getSummary() != null){
                            String camComment = "";
                            int commentCounter = 1;
                            if(camDetails.getSummary().getRemarkList() != null) {
                                for (Remark remark : camDetails.getSummary().getRemarkList()) {
                                    String comment = remark.getComment();
                                    camComment = camComment + commentCounter + "." + comment + "<br/>  ";
                                    commentCounter++;
                        }
                                configMap.put(CAM_NOTEPAD_COMMENT, appConfigurationHelper.setValueForNull(camComment));
                            }
                        }
                        break;
                    case MANDATE_ID:
                        if(null != processedRequests && null != processedRequests.getEmandateResponse())
                            configMap.put(MANDATE_ID, appConfigurationHelper.setValueForNull(processedRequests.getEmandateResponse().getPayload().getMandateId()));
                        break;
                    case MANDATE_URL:
                        if(null != processedRequests && null != processedRequests.getEmandateResponse())
                            configMap.put(MANDATE_URL, appConfigurationHelper.setValueForNull(processedRequests.getEmandateResponse().getPayload().getEmandateURL()));
                        break;

                    case REPAY_BANK_NAME1:
                        if (repayment.getRepaymentDetailsList().size() >= 1){
                                    configMap.put(REPAY_BANK_NAME1,
                                            appConfigurationHelper.setValueForNull( repayment.getRepaymentDetailsList().get(0).getBankName()));
                        }
                        break;

                    case REPAY_BANK_NAME2:
                        if (repayment.getRepaymentDetailsList().size() >= 2){
                            configMap.put(REPAY_BANK_NAME2,
                                    appConfigurationHelper.setValueForNull( repayment.getRepaymentDetailsList().get(1).getBankName()));
                        }
                        break;


                    case PRE_EMI:
                        configMap.put(PRE_EMI, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getPreEMIDeductAmt())));

                        break;

                    case GST_AMOUNT:
                        configMap.put(GST_AMOUNT, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getGstAmt())));

                        break;

                    case DOCUMENT_CHARGES:
                        if(loanCharges.getLoanDetails() != null) {
                            configMap.put(DOCUMENT_CHARGES, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getLoanDetails().getDocumentCharges())));
                        }
                        break;

                    case PDC_AMOUNT:
                        if(loanCharges.getLoanDetails() != null) {
                            configMap.put(PDC_AMOUNT, appConfigurationHelper.setValueForNull(Double.toString(loanCharges.getLoanDetails().getPdcAmount())));
                        }
                        break;

                    case DOCUMENT_FEES:
                        if(camDetails.getSummary() != null) {
                            if (camDetails.getSummary().getFinalLoanApproved() > 100000 && camDetails.getSummary().getFinalLoanApproved() <= 1500000) {
                                configMap.put(DOCUMENT_FEES, "2301");
                            } else if(camDetails.getSummary().getFinalLoanApproved() > 1500000){
                                configMap.put(DOCUMENT_FEES, "3481");
                            }
                        }
                        break;

                    case PROCESSING_FEES_AMOUNT:
                        if(camDetails.getSummary() != null && camDetails.getSummary().getFinalLoanApproved()> 0) {
                                Double pfAmt = (camDetails.getSummary().getFinalLoanApproved() * Double.parseDouble(camDetails.getSummary().getPfPercentage())) / 100;
                                Double gstAmt = (pfAmt * 18) / 100;
                                configMap.put(PROCESSING_FEES_AMOUNT, Double.toString(Math.round(pfAmt + gstAmt)));
                        }
                        break;

                    case ADDRESSLINE1_OFFICE:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(ADDRESSLINE1_OFFICE, appConfigurationHelper.setValueForNull(obj.getAddressLine1()));
                                } else {
                                    configMap.put(ADDRESSLINE1_OFFICE, "");
                                }
                            });
                        } else {
                            configMap.put(ADDRESSLINE1_OFFICE, "");
                        }
                        break;

                    case ADDRESSLINE2_OFFICE:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(ADDRESSLINE2_OFFICE, appConfigurationHelper.setValueForNull(obj.getAddressLine2()));
                                } else {
                                    configMap.put(ADDRESSLINE2_OFFICE, "");
                                }
                            });
                        } else {
                            configMap.put(ADDRESSLINE2_OFFICE, "");
                        }
                        break;

                    case LANDMARK:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(LANDMARK, appConfigurationHelper.setValueForNull(obj.getLandMark()));
                                }});
                        }
                        break;

                    case CITY_OFFICE:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(CITY_OFFICE, appConfigurationHelper.setValueForNull(obj.getCity()));
                                } else {
                                    configMap.put(CITY_OFFICE, "");
                                }
                            });
                        } else {
                            configMap.put(CITY_OFFICE, "");
                        }
                        break;

                    case PIN_OFFICE:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(PIN_OFFICE, appConfigurationHelper.setValueForNull((Long.toString(obj.getPin()))));
                                } else {
                                    configMap.put(PIN_OFFICE, "");
                                }
                            });
                        } else {
                            configMap.put(PIN_OFFICE, "");
                        }
                        break;

                    case STATE_OFFICE:
                        if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                            applicant.getAddress().forEach(obj -> {
                                if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                    configMap.put(STATE_OFFICE, appConfigurationHelper.setValueForNull(obj.getState()));
                                } else {
                                    configMap.put(STATE_OFFICE, "");
                                }
                            });
                        } else {
                            configMap.put(STATE_OFFICE, "");
                        }

                        break;

                    case DISBURSAL_BANK_NAME:
                        if (BankingData != null && CollectionUtils.isNotEmpty(BankingData.getBankingStatements())) {
                            BankingData.getBankingStatements().forEach(obj -> {
                                if (obj.isRepayBank()) {
                                    configMap.put(DISBURSAL_BANK_NAME, appConfigurationHelper.setValueForNull(obj.getBankName()));
                                }
                            });
                        }
                        break;

                    case ACCOUNT_NUMBER:
                        if (BankingData != null && CollectionUtils.isNotEmpty(BankingData.getBankingStatements())) {
                            BankingData.getBankingStatements().forEach(obj -> {
                                if (obj.isRepayBank()) {
                                    configMap.put(ACCOUNT_NUMBER, appConfigurationHelper.setValueForNull(obj.getAccountNumber()));
                                }
                            });
                        }
                        break;

                    case OTP:
                        configMap.put(OTP, "18119");

                        break;
                    default:
                        logger.info("Field Not found {}", fieldList );
                        break;
                }
        } catch (Exception e) {
                logger.error("Exception caught while getting getTemplateConfigrationData for :  " + fieldList + e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("Exception caught while getting getTemplateConfigrationData due to null value {%s}", e.getMessage()));
        }
    }
        return configMap;
    }


    @Override
    public Boolean saveTemplateDetail(TemplateRequest templateRequest) {
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("productName").is(templateRequest.getTemplateDetails().getProductName())
                    .and("institutionId").is(templateRequest.getHeader().getInstitutionId())
                    .and("templateName").is(templateRequest.getTemplateDetails().getTemplateName());
            Update update = new Update();
            update.set("type", templateRequest.getTemplateDetails().getType());
            update.set("mailSubject", templateRequest.getTemplateDetails().getMailSubject());
            update.set("templateContent", templateRequest.getTemplateDetails().getTemplateContent());
            update.set("templateDisplayName", templateRequest.getTemplateDetails().getTemplateName());
            update.set("otpTemplate", templateRequest.getTemplateDetails().isOtpTemplate());
            query.addCriteria(criteria);
            WriteResult writeResult = mongoTemplate.upsert(query, update, TemplateDetails.class);

            logger.debug("update templateDetails compete for instituteid {} with query {}",
                    templateRequest.getHeader().getInstitutionId(), query.toString());
            return writeResult.isUpdateOfExisting();
        } catch (Exception e) {
            logger.error("Error occurred while update templateDetails " +
                    " with probable cause [{}] ", e.getMessage());

        }
        return true;
    }

    @Override
    public List<TemplateConfiguration> findVelocityTemplateByInstitution(String instituteId) {
        List<TemplateConfiguration> templateConfigurations = null;
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(instituteId));
        templateConfigurations = mongoTemplate.find(query, TemplateConfiguration.class);
        return templateConfigurations;
    }

    public LoginServiceResponse fetchLoggedUserDetail(String institutionId, String productName, String branchId,String role, String loginId){
        LoginServiceResponse loginServiceResponse = null;
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(Integer.valueOf(institutionId)).
                    and("loginId").is(loginId).
                    and("product").elemMatch(
                    Criteria.where("productName").is(productName)).
                    and("roles").in(role));
            query.fields().include("loginId")
                    .include("roles")
                    .include("userName")
                    .include("institutionId")
                    .include("employeeId");
            logger.debug("Query for fetching User detail - {}", query.toString());
            loginServiceResponse = mongoTemplate.findOne(query, LoginServiceResponse.class);
        } catch (Exception e) {
            logger.error("Error occured wile fetchLogged User detail {}", e.getStackTrace());
        }
        return loginServiceResponse;
    }

    @Override
    public TemplateDetails findTemplateDetailsByInstituteIdAndProduct(String institutionId, String productName) {
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(institutionId).and("productName").is(productName)
                .and("otpTemplate").is(true);
        query.addCriteria(criteria);
        if (mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
        return mongoTemplate.findOne(query, TemplateDetails.class);
    }

    @Override
    public boolean checkCacheEnable() {
        logger.info("Inside checkCacheEnable Repo");
        boolean cacheStatus=false;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(Constant.IS_CACHE_ENABLED));
            logger.info("Query Criteria {}",query);
            BasicDBObject cacheConfiguration = mongoTemplate.findOne(query, BasicDBObject.class, Constant.CACHE_CONFIGURATION_COLLECTION);
            if(cacheConfiguration!=null && cacheConfiguration.containsField("status") && cacheConfiguration.get("status").equals(true)){
                cacheStatus=true;
                if(cacheConfiguration.containsField(Constant.CRITICAL_THRESHOLD) && cacheConfiguration.containsField(Constant.WARNING_THRESHOLD)){
                    RedisServiceImpl.criticalThreshold=cacheConfiguration.getInt(Constant.CRITICAL_THRESHOLD);
                    RedisServiceImpl.warningThreshold=cacheConfiguration.getInt(Constant.WARNING_THRESHOLD);
                }
            }
        } catch (Exception e) {
            logger.error("Exception occurred while executing checkCacheEnable with probable cause [{}]", e);
        }
        return cacheStatus;
    }
}
