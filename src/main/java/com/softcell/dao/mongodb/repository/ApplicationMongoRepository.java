package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.constants.*;
import com.softcell.constants.dbconstants.GoNoGoCustomerApplicationKeyConstants;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.elsearch.IndexingElasticSearchRepository;
import com.softcell.dao.mongodb.repository.elsearch.IndexingRepository;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.Buckets;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.OtpVerificationData;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.coorgination.icici.SchemeDetails;
import com.softcell.gonogo.model.coorgination.icici.SourcingDetails;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.core.scoring.response.Eligibility;
import com.softcell.gonogo.model.core.user.TvrDetails;
import com.softcell.gonogo.model.core.valuation.*;
import com.softcell.gonogo.model.core.verification.*;
import com.softcell.gonogo.model.finbit.FinbitData;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.finbit.FinbitRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.quickcheck.EMandateDetails;
import com.softcell.gonogo.model.quickcheck.EMandateDetailsCallLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.*;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.ApplicationResponse;
import com.softcell.gonogo.model.response.CroQueue;
import com.softcell.gonogo.model.response.PosidexResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.workflow.cache.ApplicationQueue;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.thirdparty.utils.ThirdPartyIntegrationHelper;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.SequenceGenerator;
import com.softcell.workflow.component.finished.AuditData;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author yogeshb
 * @version 1.0
 */

@Repository
public class ApplicationMongoRepository implements ApplicationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationMongoRepository.class);

    private IndexingRepository indexingRepository;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private ModuleHelper moduleHelper;


    public ApplicationMongoRepository() {
        //FIXME It will be removed when new workflow will be introduced as It is not getting autowired in current workflow call
        if (mongoTemplate == null) {
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
    }


    /**
     * To store whole ApplicationRequest object and dumb to mongodb.
     */
    @Override
    public String saveApplication(ApplicationRequest applicationRequest)  {

        logger.debug("saveApplication repo started ");

        try {

            mongoTemplate.insert(applicationRequest);

            logger.debug("saveApplication repo completed for refId {}", applicationRequest.getRefID());

            GoNoGoCustomerApplication gngCustApp = getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());

            if (null != gngCustApp) {
                updateApplicationInElasticsearch(gngCustApp);
            }

            return applicationRequest.getRefID();

        } catch (Exception e) {

            logger.error("Error occurred while saving ApplicationRequest " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    applicationRequest.getHeader().getInstitutionId(), applicationRequest.getRefID(), e.getMessage());
            return null;

            //throw new Exception(e.getMessage());

        }
    }

    @Override
    public ApplicationRequest getCheckStatus(String applicationId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(applicationId));

        List<String> includeField = Cache.WEB_RESPONSE_INCLUDE_FIELD_MAP
                .get(CacheConstant.STATUS_INCLUDE_FIELD_TAB.toString());

        if (includeField != null) {
            for (String field : includeField) {
                query.fields().include(field);
            }
        }
        return null;
    }

    /**
     * FOr CRO Screen
     */
    @Override
    public List<ApplicationRequest> fetchByCroIDAndInstitution(String croID,
                                                               String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("header.croId").is(croID)
                .and("header.institutionId").is(institutionId));
        return mongoTemplate.find(query, ApplicationRequest.class);

    }

    /**
     * FOr CRO Screen
     */
    @Override
    public Stream<CroQueue> getCroQueue(CroQueueRequest croQueueRequestObj)  {

        logger.debug("getCroQueue repo started");

        String croID = croQueueRequestObj.getCroId();

        String institutionId = croQueueRequestObj.getInstitutionId();

        int skip = croQueueRequestObj.getSkip();

        int limit = croQueueRequestObj.getLimit();

        RequestCriteria criteria = croQueueRequestObj.getCriteria();

        List<CroQueue> croQueueRequest = new ArrayList<CroQueue>();

        if (skip == 0 && limit == 0) {
            skip = 0;
            limit = 100;
        }

        Stream<CroQueue> croQueueStream = null;

        try {
            Query query = new Query();
            Criteria basicCriteria =
                    Criteria.where("applicationRequest.header.croId").is(croID)
                            .and("applicationRequest.header.institutionId").is(institutionId)
                            .and("applicationStatus").ne("NEW")
                            .and("applicationRequest.qdeDecision").ne(Boolean.TRUE);
            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */
            query.addCriteria(basicCriteria).skip(skip).limit(limit);

            query.fields().include("applicationRequest")
                    .include("applicationStatus")
                    .include("dateTime");

            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            logger.debug("getCroQueue repo query formed {}", query.toString());

            MongoRepositoryUtil.addHierarchyCriteria(criteria, query, mongoTemplate);
            Stream<GoNoGoCustomerApplication> goNoGoCustomerApplication = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, GoNoGoCustomerApplication.class, "goNoGoCustomerApplication"));
            croQueueStream = goNoGoCustomerApplication.map(application -> {
                CroQueue croQueue = buildCroQueueObjectFromAppResponse(application.getApplicationRequest());
                croQueue.setDateTime(application.getDateTime());
                croQueue.setStatus(application.getApplicationStatus());
                return croQueue;
            });
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching getCroQueue  for institutionId [{}]  with probable cause [{}] ",
                    institutionId, e.getMessage());
            //throw new Exception(e.getMessage());
        }
        return croQueueStream;
    }

    /**
     * For CRO Screen with cirteria
     */
    @Override
    public List<CroQueue> getCroQueueCriteria(CroQueueRequest croQueueRequestObj)  {

        logger.debug("getCroQueueCriteria repo started");

        List<CroQueue> croQueueRequest = new ArrayList<CroQueue>();

        String croID = croQueueRequestObj.getCroId();

        String institutionId = croQueueRequestObj.getInstitutionId();

        int skip = croQueueRequestObj.getSkip();

        int limit = croQueueRequestObj.getLimit();

        RequestCriteria criteria = croQueueRequestObj.getCriteria();

        if (skip == 0 && limit == 0) {
            skip = 0;
            limit = 100;
        }

        try {

            Query query = new Query();

            query.addCriteria(
                    Criteria.where("applicationRequest.header.croId").is(croID)
                            .and("applicationRequest.header.institutionId")
                            .is(institutionId).and("applicationStatus")
                            .ne("NEW")).skip(skip).limit(limit);

            query.fields().include("applicationRequest")
                    .include("applicationStatus")
                    .include("dateTime");

            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            MongoRepositoryUtil.addHierarchyCriteria(criteria, query,
                    mongoTemplate);

            logger.debug("getCroQueueCriteria repo query created {}", query.toString());

            List<GoNoGoCustomerApplication> resultList = mongoTemplate.find(
                    query, GoNoGoCustomerApplication.class);

            for (GoNoGoCustomerApplication application : resultList) {

                CroQueue croQueue = buildCroQueueObjectFromAppResponse(application.getApplicationRequest());
                croQueue.setDateTime(application.getDateTime());
                croQueue.setStatus(application.getApplicationStatus());
                croQueueRequest.add(croQueue);

            }

        } catch (Exception e) {

            logger.error("Error occurred while fetching getCroQueueCriteria " +
                            " for institutionId {}  with probable cause [{}] ",
                    institutionId, e.getMessage());

            //throw new Exception(e.getMessage());
        }

        logger.debug("getCroQueueCriteria repo completed");

        return croQueueRequest;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * ApplicationRepository#getCro2Queue(java
     * .lang.String, java.lang.String)
     */
    @Override
    public List<CroQueue> getCro2Queue(CroQueueRequest croQueueRequestObj)  {

        logger.debug("getCro2Queue repo started");

        String institutionId = croQueueRequestObj.getInstitutionId();

        int skip = croQueueRequestObj.getSkip();

        int limit = croQueueRequestObj.getLimit();

        RequestCriteria criteria = croQueueRequestObj.getCriteria();
        Criteria basicCriteria;

        List<CroQueue> croQueueRequest = new ArrayList<CroQueue>();

        if (skip == 0 && limit == 0) {
            skip = 0;
            limit = 100;
        }

        try {
            List<String> allowedStatuses = new ArrayList<String>() {{
                add(GNGWorkflowConstant.APPROVED.toFaceValue());
                add(GNGWorkflowConstant.DECLINED.toFaceValue());
                add(GNGWorkflowConstant.CANCELLED.toFaceValue());
            }};
            Query query = new Query();
            basicCriteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationStatus").in(allowedStatuses)
                    .and("applicationRequest.qdeDecision").ne(Boolean.TRUE);

            if (StringUtils.isNotBlank(croQueueRequestObj.getCurrentStageId())) {
                basicCriteria.and("applicationRequest.currentStageId").is(croQueueRequestObj.getCurrentStageId());
            }

            /*Criteria.where("applicationStatus").regex(
                    "APPROVED", "i"),*/
            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */
            query.addCriteria(basicCriteria).skip(skip).limit(limit);
            query.fields().include("applicationRequest")
                    .include("applicationStatus")
                    .include("dateTime")
                    .include("tvrDetails");

            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            MongoRepositoryUtil.addHierarchyCriteria(criteria, query,
                    mongoTemplate);


            logger.debug("getCro2Queue repo query formed {}", query.toString());

            List<GoNoGoCustomerApplication> resultList = mongoTemplate.find(
                    query, GoNoGoCustomerApplication.class);

            for (GoNoGoCustomerApplication application : resultList) {

                CroQueue croQueue = buildCroQueueObjectFromAppResponse(application.getApplicationRequest());
                croQueue.setDateTime(application.getDateTime());
                croQueue.setStatus(application.getApplicationStatus());
                if (application.getTvrDetails() != null) {
                    croQueue.setTvrDecision(true);
                }
                croQueueRequest.add(croQueue);
            }
        } catch (Exception e) {

            logger.error("Error occurred while fetching getCro2Queue " +
                            " for institutionId {}  with probable cause [{}] ",
                    institutionId, e.getMessage());
            //throw new Exception(e.getMessage());
        }

        logger.debug("getCro2Queue repo completed ");
        return croQueueRequest;
    }

    @Override
    public List<CroQueue> getCro3Queue(CroQueueRequest croQueueRequestObj)  {

        logger.debug("getCro3Queue repo started");

        List<CroQueue> croQueueList = new ArrayList<CroQueue>();

        String institutionId = croQueueRequestObj.getInstitutionId();

        int skip = croQueueRequestObj.getSkip();

        int limit = croQueueRequestObj.getLimit();

        RequestCriteria criteria = croQueueRequestObj.getCriteria();

        String approved = Status.APPROVED.name();

        String declined = Status.DECLINED.name();

        if (skip == 0 && limit == 0) {
            skip = 0;
            limit = 100;
        }

        try {
            List<String> allowedStatuses = new ArrayList<String>() {{
                add(approved);
                add(declined);
            }};
            Query query = new Query();

            Criteria basicCriteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationStatus").in(allowedStatuses)
                    .and("applicationRequest.qdeDecision").ne(Boolean.TRUE);
            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */

            query.addCriteria(basicCriteria).skip(skip).limit(limit);

            query.fields().include("applicationRequest")
                    .include("applicationStatus")
                    .include("dateTime");

            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            MongoRepositoryUtil.addHierarchyCriteria(criteria, query,
                    mongoTemplate);

            logger.debug("getCro3Queue repo query created for app response {}", query.toString());

            List<GoNoGoCustomerApplication> resultList = mongoTemplate.find(
                    query, GoNoGoCustomerApplication.class);
            if (!CollectionUtils.isEmpty(resultList)) {
                for (GoNoGoCustomerApplication application : resultList) {

                    CroQueue croQueue = buildCroQueueObjectFromAppResponse(application.getApplicationRequest());
                    croQueue.setDateTime(application.getDateTime());
                    croQueue.setStatus(application.getApplicationStatus());
                    croQueue.setRequestType("FP");
                    croQueueList.add(croQueue);
                }
            }

            logger.debug("getCro3Queue repo query response count for gngResponse {}", croQueueList.size());

            // fetching data from Application request

            query = new Query();
            basicCriteria = Criteria.where("header.institutionId").is(institutionId)
                    .and("currentStageId").is("DE")
                    .and("qdeDecision").ne(Boolean.TRUE);
            query.addCriteria(basicCriteria).skip(skip).limit(limit);

            query.with(new Sort(Sort.Direction.DESC, "header.dateTime"));

            MongoRepositoryUtil.addHierarchyCriteriaApplicationRequest(
                    criteria, query, mongoTemplate);

            logger.debug("getCro3Queue repo query created for app request {}", query.toString());

            List<ApplicationRequest> requestList = mongoTemplate.find(query,
                    ApplicationRequest.class);

            if (!CollectionUtils.isEmpty(requestList)) {
                for (ApplicationRequest application : requestList) {

                    CroQueue croQueue = buildCroQueueObjectFromAppResponse(application);
                    croQueue.setDateTime(application.getHeader().getDateTime());
                    croQueue.setStatus("");
                    croQueue.setRequestType("PS");

                    croQueueList.add(croQueue);
                }
            }

            if (!CollectionUtils.isEmpty(croQueueList)) {
                Collections.sort(croQueueList, (CroQueue c1, CroQueue c2) -> c2.getDateTime().compareTo(c1.getDateTime()));
            }

            logger.debug("getCro3Queue repo query response count for gngrequest {}", croQueueList.size());

        } catch (Exception e) {

            logger.error("Error occurred while fetching getCro3Queue " +
                            " for institutionId {}  with probable cause [{}] ",
                    institutionId, e.getMessage());

            //throw new Exception(e.getMessage());

        }
        return croQueueList;
    }

    @Override
    public List<CroQueue> getCro4Queue(CroQueueRequest croQueueRequestObj)  {

        logger.debug("getCro4Queue repo started");

        List<CroQueue> croQueueRequest = new ArrayList<CroQueue>();

        String institutionId = croQueueRequestObj.getInstitutionId();
        int skip = croQueueRequestObj.getSkip();
        int limit = croQueueRequestObj.getLimit();
        RequestCriteria criteria = croQueueRequestObj.getCriteria();

        if (skip == 0 && limit == 0) {
            skip = 0;
            limit = 100;
        }
        try {
            Query query = new Query();
            query.addCriteria(
                    Criteria.where("header.institutionId").is(institutionId)
                            .and("currentStageId").is("DE")
                            .and("qdeDecision").ne(Boolean.TRUE)
            ).skip(skip).limit(limit);
            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            // added for residence with pincode 0
            query.addCriteria(Criteria.where("request.applicant.address")
                    .elemMatch(
                            Criteria.where("addressType").is("RESIDENCE")
                                    .and("pin").is(0)));

            MongoRepositoryUtil.addHierarchyCriteriaApplicationRequest(
                    criteria, query, mongoTemplate);

            logger.debug("getCro4Queue repo query created {}", query.toString());

            List<ApplicationRequest> requestList = mongoTemplate.find(query,
                    ApplicationRequest.class);

            for (ApplicationRequest application : requestList) {

                CroQueue croQueue = buildCroQueueObjectFromAppResponse(application);

                croQueue.setDateTime(application.getHeader().getDateTime());
                croQueue.setStatus("");
                croQueue.setRequestType("PS");
            }
        } catch (Exception e) {

            logger.error("Error occurred while fetching getCro4Queue " +
                            " for institutionId {}  with probable cause [{}] ",
                    institutionId, e.getMessage());

            //throw new Exception(e.getMessage());
        }

        logger.debug("getCro4Queue repo completed with queue count {}", croQueueRequest.size());

        return croQueueRequest;
    }

    @Override
    public boolean saveGoNoGoCustomerApplication(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {
        try {
            /*if (!mongoTemplate
                    .collectionExists(GoNoGoCustomerApplication.class)) {
                mongoTemplate.createCollection(GoNoGoCustomerApplication.class);
            }*/

            mongoTemplate.insert(goNoGoCustomerApplication);
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while save GoNoGoCustomerApplication "
                    , e.getMessage());

            return false;
        }
    }

    @Override
    public boolean updateCroDecision(CroApprovalRequest croApprovalRequest) {

        String referenceId = croApprovalRequest.getReferenceId();
        String status = croApprovalRequest.getApplicationStatus();
        List<String> dedupeRefIds = croApprovalRequest.getDedupeRefrenceIds();

        boolean updateFlagForApprovedAmt = croApprovalRequest.isApprovedAmountExist();
        double approvedAmount = croApprovalRequest.getApprovedAmount();
        int tenor = croApprovalRequest.getTenor();
        double emi = croApprovalRequest.getEmi();
        String croID = croApprovalRequest.getHeader().getCroId();
        GoNoGoCustomerApplication goNoGoCustomerApplication = getGoNoGoCustomerApplicationByRefId(referenceId);
        List<CroJustification> croJustifications = croApprovalRequest
                .getCroJustification();
        String refId = croApprovalRequest.getReferenceId();
        String institutionId = croApprovalRequest.getHeader().getInstitutionId();
        String userId = croApprovalRequest.getHeader().getLoggedInUserId();
        String loggedInUserRole = croApprovalRequest.getHeader().getLoggedInUserRole();

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId));

            Update update = new Update();
            String stage = null;

            if (status != null) {

                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.APPROVED.toFaceValue(), status)) {
                    if(goNoGoCustomerApplication.getAllocationInfo() != null) {
                    stage = GNGWorkflowConstant.APRV.toFaceValue();
                    update.set("allocationInfo.locked", false);
                    }else{
                        AllocationInfo allocationInfo = new AllocationInfo();
                        stage = GNGWorkflowConstant.APRV.toFaceValue();
                        allocationInfo.setOperator(userId);
                        allocationInfo.setRole(loggedInUserRole);
                        updateApplicationForAllocationInfo(refId,allocationInfo,institutionId);
                        update.set("allocationInfo.locked", false);
                    }
                } else if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.SUBJ_APRV.toFaceValue(), status)) {
                    stage = GNGWorkflowConstant.SUBJ_APRV.toFaceValue();
                    update.set("allocationInfo.locked", false);
                }else if(StringUtils.equalsIgnoreCase(GNGWorkflowConstant.EXT_APPROVED.toFaceValue(), status)){
                    stage = GNGWorkflowConstant.EXT_APRV.toFaceValue();
                }
                // New Changes which update following new Fields in db.
                CroDecision croDecision = null;
                if (updateFlagForApprovedAmt) {
                    croDecision = new CroDecision();
                    if (approvedAmount != 0) {
                        croDecision.setAmtApproved(approvedAmount);
                    }
                    if (emi != 0) {
                        croDecision.setEmi(emi);
                    }
                    if (tenor != 0) {
                        croDecision.setTenor(tenor);
                    }
                    croDecision.setLtv(croApprovalRequest.getLtv());
                }
                // ********************************************************
                // Newly added change
                Object[] croJustificationList = croJustifications.toArray();
                if (croJustificationList != null) {
                    for (CroJustification croJustification : croJustifications) {
                        croJustification.setCroID(croID);
                        croJustification.setDecisionCase(croApprovalRequest
                                .getApplicationStatus());
                        croJustification.setSubjectTo(Cache.getUserName(croApprovalRequest.getHeader().getInstitutionId(),
                                croApprovalRequest.getHeader().getLoggedInUserId()));
                    }
                    update.push("croJustification").each(croJustificationList);
                }
                // ***********************
                update.set("applicationStatus", status);
                update.set("statusFlag", true);
                update.set("applicationRequest.currentStageId", stage);
                goNoGoCustomerApplication = getGoNoGoCustomerApplicationByRefId(referenceId);
                // TODO : Check with Vinod and Sumit : This is HDBFS related calculation
                /*
                    if (goNoGoCustomerApplication != null
                            && goNoGoCustomerApplication.getCroDecisions() == null) {
                        List<CroDecision> croDecisionList = getCroDecision(goNoGoCustomerApplication);
                        update.set("croDecisions", croDecisionList);
                    }
                } else {
                    if (croDecision != null) {
                        List<CroDecision> croDecisionList = new ArrayList<CroDecision>();
                        croDecisionList.add(croDecision);
                        update.set("croDecisions", croDecisionList);
                    }
                }*/
                update.set("intrimStatus.appsStatus", Status.COMPLETE.toString());
            } else {
                return false;
            }

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);
            updateDecisionInfo(goNoGoCustomerApplication, query, croJustifications);
            updateDedupeStatus(dedupeRefIds);
            try {
                goNoGoCustomerApplication.setStatusFlag(true);
                goNoGoCustomerApplication.getApplicationRequest()
                        .setCurrentStageId(stage);
                goNoGoCustomerApplication.setApplicationStatus(status);
                indexingRepository = new IndexingElasticSearchRepository();
                indexingRepository.indexNotification(goNoGoCustomerApplication);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            return true;
        } catch (Exception e) {
//            e.printStackTrace();
            logger.error("Error occurred while update cro decision " +
                    " for refID {} with probable cause [{}] ", referenceId, e.getMessage());

            return false;
        }
    }

    @Override
    public ApplicationResponse checkStatus(String referenceId,
                                           String institutionId)  {

        logger.debug("checkStatus repo started for ref Id {}", referenceId);

        ApplicationResponse applicationResponse = new ApplicationResponse();

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            List<String> includeField = Cache.WEB_RESPONSE_INCLUDE_FIELD_MAP
                    .get(CacheConstant.STATUS_INCLUDE_FIELD_TAB.toString());

            if (includeField != null) {
                for (String field : includeField) {
                    query.fields().include(field);
                }
            }

            logger.debug("checkStatus repo query formed {} ", query.toString());

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            if (goNoGoCustomerApplication != null) {

                applicationResponse.setInterimStatus(goNoGoCustomerApplication
                        .getIntrimStatus());
                applicationResponse
                        .setApplicationStatus(goNoGoCustomerApplication
                                .getApplicationStatus());
                applicationResponse
                        .setModuleOutcomes(goNoGoCustomerApplication
                                .getApplScoreVector());
                applicationResponse
                        .setCroJustification(goNoGoCustomerApplication
                                .getCroJustification());

                //Field added for ReAppraisal Comparison
                applicationResponse.setSurrogate(goNoGoCustomerApplication
                        .getApplicationRequest().getRequest().getApplicant()
                        .getSurrogate());
                applicationResponse.setSubmitDate(goNoGoCustomerApplication
                        .getApplicationRequest().getHeader().getDateTime());
                //Field added for ReAppraisal Comparison
                applicationResponse.setAppliedAmount(goNoGoCustomerApplication
                        .getApplicationRequest().getRequest().getApplication()
                        .getLoanAmount());

                if (goNoGoCustomerApplication.getApplicationRequest() != null && goNoGoCustomerApplication.getApplicationRequest()
                        .getReAppraiseDetails() != null) {
                    applicationResponse
                            .setReAppraiseReason(goNoGoCustomerApplication
                                    .getApplicationRequest()
                                    .getReAppraiseDetails()
                                    .getReappraiseReason());

                    applicationResponse
                            .setReAppraiseRemark(goNoGoCustomerApplication
                                    .getApplicationRequest()
                                    .getReAppraiseDetails()
                                    .getReappraiseRemark());
                }

                if (goNoGoCustomerApplication.getCroDecisions() != null) {

                    applicationResponse
                            .setCroDecisions(goNoGoCustomerApplication
                                    .getCroDecisions());

                }
                applicationResponse.setQdeDecision(goNoGoCustomerApplication.getApplicationRequest().isQdeDecision());

                ApplicationQueue.APPLICATION_STATUS.put(referenceId,
                        goNoGoCustomerApplication.getIntrimStatus());

            }
        } catch (Exception e) {

            logger.error(e.getMessage());

            //throw new Exception(e.getMessage());

        }

        logger.debug("checkStatus repo completed with application status {}", applicationResponse.getApplicationStatus());

        return applicationResponse;

    }

    @Override
    public GoNoGoCroApplicationResponse getApplicationByRefId(String refID,
                                                              String requestSource, String institutionId)  {

        logger.debug("getApplicationByRefId repository started for refid : {}", refID);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            List<String> includeFieldList = Cache.WEB_RESPONSE_INCLUDE_FIELD_MAP
                    .get(requestSource);

            if (includeFieldList != null) {

                for (String field : includeFieldList) {
                    query.fields().include(field);
                }

            }

            logger.debug("getApplicationByRefId repo query formed : {}", query);

            GoNoGoCroApplicationResponse croApplicationResponse = mongoTemplate
                    .findOne(query, GoNoGoCroApplicationResponse.class);

            logger.debug("getApplicationByRefId repository called completed successfully");

            if (croApplicationResponse != null
                    && !CollectionUtils.isEmpty(croApplicationResponse.getCroDecisions())
                    && !CollectionUtils.isEmpty(croApplicationResponse.getCroJustification())) {

                Collections.sort(croApplicationResponse.getCroDecisions());
                Collections.sort(croApplicationResponse.getCroJustification());
            }
            return croApplicationResponse;

        } catch (Exception e) {

            logger.error("Error occurred while fetching Application " +
                    " for institutionId {} and refID {} with probable cause [{}] ", institutionId, refID, e.getMessage());
                return null;
            //throw new Exception(e.getMessage());
        }
    }


    @Override
    public GoNoGoCroApplicationResponse getApplicationByRefIdForAuditLog(String refID,
                                                                         String requestSource, String institutionId)  {

        logger.debug("getApplicationByRefId repository started for refid : {}", refID);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            logger.debug("getApplicationByRefId repo query formed : {}", query);

            GoNoGoCroApplicationResponse croApplicationResponse = mongoTemplate
                    .findOne(query, GoNoGoCroApplicationResponse.class);

            logger.debug("getApplicationByRefId repository called completed successfully");

            if (croApplicationResponse != null
                    && !CollectionUtils.isEmpty(croApplicationResponse.getCroDecisions())
                    && !CollectionUtils.isEmpty(croApplicationResponse.getCroJustification())) {

                Collections.sort(croApplicationResponse.getCroDecisions());
                Collections.sort(croApplicationResponse.getCroJustification());
            }
            return croApplicationResponse;

        } catch (Exception e) {

            logger.error("Error occurred while fetching Application " +
                    " for institutionId {} and refID {} with probable cause [{}] ", institutionId, refID, e.getMessage());
                return null;
            //throw new Exception(e.getMessage());
        }
    }


    public CoApplicant getCoApplicantBycoApplicantId(String refId, String coApplicantId, String institutionId)  {

        try {

            logger.debug("getCoApplicantBycoApplicantId repo started for refId {}", refId);

            Query query = new Query();
            query.addCriteria(Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                    .and("_id").is(refId)
                    .and("applicationRequest.request.executeForCoApplicant.applicantId")
                    .is(coApplicantId));

            query.fields().include("applicationRequest");

            logger.debug("getCoApplicantBycoApplicantId repo query formed {}", query);

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            CoApplicant coApplicant = null;

            if (goNoGoCustomerApplication != null && goNoGoCustomerApplication.getApplicationRequest() != null
                    && goNoGoCustomerApplication.getApplicationRequest().getRequest() != null) {

                List<CoApplicant> coApplicantList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();

                if (coApplicantList != null && coApplicantList.size() > 0) {
                    coApplicant = coApplicantList.get(0);
                }
            }

            logger.debug("getCoApplicantBycoApplicantId completed");

            return coApplicant;

        } catch (Exception e) {

            logger.error("Error occurred while fetching ApplicationRequest for coapplicant " +
                    " for institutionId {} and refID {} with probable cause [{}] ", institutionId, refId, e.getMessage());
                return null;
            //throw new Exception(e.getMessage());

        }
    }

    @Deprecated
    @Override
    public ApplicationRequest getApplicationRequestByRefId(String refID,
                                                           String institutionId)  {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
//                    .and("header.institutionId").is(institutionId)
            );
            return mongoTemplate.findOne(
                    query, ApplicationRequest.class);

        } catch (Exception e) {

            logger.error("Error occurred while fetching ApplicationRequest " +
                    " for institutionId {} and refID {} with probable cause [{}] ", institutionId, refID, e.getMessage());
                return  null;

            //throw new Exception(e.getMessage());

        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * ApplicationRepository#getCroId(java.lang
     * .String)
     */
    @Override
    public String getCroId(String dsaId, String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("dsaName").is(dsaId).and("active")
                .is(true));
        CroDsaMaster croDsaMaster = mongoTemplate.findOne(query,
                CroDsaMaster.class);
        if (croDsaMaster != null)
            return croDsaMaster.getCroId();
        else {
            return "default";
        }
    }


    @Override
    public boolean saveCroDsaMaster(CroDsaMaster croDsaMaster) {
        if (croDsaMaster != null) {
            mongoTemplate.insert(croDsaMaster);
            return true;
        }
        return false;
    }


    @Override
    public boolean savePostIpaDetails(PostIpaRequest postIpaRequest)  {

        logger.debug("savePostIpaDetails repo started");

        try {

            BasicDBObject dbObject = new BasicDBObject();

            mongoTemplate.getConverter().write(postIpaRequest, dbObject);

            mongoTemplate.execute(PostIpaRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(postIpaRequest.getRefID())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            logger.debug("savePostIpaDetails repo competed");

            return true;

        } catch (Exception e) {

            logger.error("Error occurred while saving PostIpaRequest " +
                    "  with probable cause [{}] ", e.getMessage());
                return false;
            //throw new Exception(e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * ApplicationRepository#fetchPostIpaDetails
     * (java.lang.String)
     */
    @Override
    public PostIpaRequest fetchPostIpaDetails(String refId, String institutionId)  {

        logger.debug("fetchPostIpaDetails repo started");

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("header.institutionId").is(institutionId));

            PostIpaRequest postIpaRequest = mongoTemplate.findOne(query, PostIpaRequest.class);

            logger.debug("fetchPostIpaDetails repo completed");

            return postIpaRequest;

        } catch (Exception e) {

            logger.error("Error occurred while fetchPostIpaDetails " +
                    " for institutionId {} and refID {} with probable cause [{}] ", institutionId, refId, e.getMessage());

            //throw new Exception(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean upsertApplication(ApplicationRequest applicationRequest)  {

        logger.debug("upsertApplication repo started");

        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(applicationRequest, dbObject);
            mongoTemplate.execute(ApplicationRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(applicationRequest.getRefID())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            logger.debug("upsert Application repo updated successfully");

            return true;

        } catch (Exception e) {

            logger.error("Error occurred while save ApplicationRequest " +
                    " with probable cause [{}] ", e.getMessage());
                return false;
            //throw new Exception(e.getMessage());
        }
    }

    @Override
    public ApplicationRequest getApplication(
            RetrieveApplicationRequest retrieveApplicationRequest) {
        ApplicationRequest applicationRequest = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria
                    .where("request.applicant.phone.phoneNumber")
                    .is(retrieveApplicationRequest.getPhone().getPhoneNumber())
                    .and("request.applicant.dateOfBirth")
                    .is(retrieveApplicationRequest.getDateOfBirth()));
            applicationRequest = mongoTemplate.findOne(query,
                    ApplicationRequest.class);
            return applicationRequest;
        } catch (Exception e) {
            logger.error("Error occurred while fetching getApplication " +
                    " with probable cause [{}] ", e.getMessage());
        }
        return applicationRequest;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * ApplicationRepository#updateOnHoldCroDecision
     * (com.softcell.gonogo.model.request.CroApprovalRequest)
     */
    @Override
    public boolean updateOnHoldCroDecision(CroApprovalRequest croApprovalRequest) {
        String croID = croApprovalRequest.getHeader().getLoggedInUserId();
        String stages = null;
        Query query = new Query();
        try {
            query.addCriteria(Criteria.where("_id")
                    .is(croApprovalRequest.getReferenceId())
                    .and("applicationRequest.header.institutionId")
                    .is(croApprovalRequest.getHeader().getInstitutionId()));
            List<CroJustification> croJustifications;
                    Update update = new Update();
            if (croApprovalRequest.getApplicationStatus() != null) {
                if (StringUtils.equals(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.ON_HOLD.toFaceValue())) {
                    stages = GNGWorkflowConstant.CR_H.toFaceValue();
                } else if (StringUtils.equals(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.DECLINED.toFaceValue())) {
                    stages = GNGWorkflowConstant.DCLN.toFaceValue();
                }else if(StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.EXTDECLINED.toFaceValue())){
                    stages = GNGWorkflowConstant.EXT_DCLN.toFaceValue();
                }
                if (stages != null) {
                    update.set("applicationRequest.currentStageId", stages);
                    update.set("applicationStatus",
                            croApprovalRequest.getApplicationStatus());
                    croJustifications = croApprovalRequest
                            .getCroJustification();
                    Object[] croJustificationList = croJustifications.toArray();
                    if (croJustificationList != null) {
                        for (CroJustification croJustification : croJustifications) {
                            croJustification.setCroID(croID);
                            croJustification.setDecisionCase(croApprovalRequest
                                    .getApplicationStatus());
                            croJustification.setSubjectTo(Cache.getUserName(croApprovalRequest.getHeader().getInstitutionId(),
                                    croApprovalRequest.getHeader().getLoggedInUserId()));
                        }
                        update.push("croJustification").each(croJustificationList);
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            updateDecisionInfo(goNoGoCustomerApplication, query, croJustifications);
            updateApplicationInElasticsearch(goNoGoCustomerApplication);


            /**
             * Newly Added logic JIRA id GON-30
             */
            String refID = croApprovalRequest.getReferenceId();
            List<String> dedupeRefList = croApprovalRequest
                    .getDedupeRefrenceIds();
            if (dedupeRefList != null && StringUtils.isNotBlank(refID)) {
                boolean match = dedupeRefList.stream().anyMatch(
                        croApprovalRequest.getReferenceId()::equals);
                if (match) {
                    /**
                     * remove refID from Dedupe List
                     */
                    dedupeRefList.remove(refID);
                    updateDedupeStatus(dedupeRefList);
                } else {
                    updateDedupeStatus(dedupeRefList);
                }
            }
            return true;
        } catch (Exception e) {
            logger.error("Error occurred while updating " +
                    "updateOnHoldCroDecision  with probable cause [{}] ", e.getMessage());
            return false;
        }

    }

    private void updateDecisionInfo(GoNoGoCustomerApplication goNoGoCustomerApplication, Query query, List<CroJustification> croJustifications) {
        try {
            DecisionData decisionData = goNoGoCustomerApplication.getDecisionData();
            if(!CollectionUtils.isEmpty(croJustifications)){
                String firstDecision = null;
                Date firstDecisionDate = null;
                String lastDecision = null;
                Date lastDecisionDate = null;
                CroJustification firstDecisionObj = (decisionData != null)? decisionData.getFirstDecision() : null;
                CroJustification lastDecisionObj = (decisionData != null)? decisionData.getFirstDecision() : null;
                for (CroJustification decision : croJustifications){
                    if(StringUtils.equalsIgnoreCase(decision.getDecisionCase(), "Approved") ||
                            StringUtils.equalsIgnoreCase(decision.getDecisionCase(),"Declined")){
                        if(firstDecision == null || firstDecisionDate.compareTo(decision.getCroJustificationUpdateDate()) > 0){
                            firstDecision =  decision.getDecisionCase();
                            firstDecisionDate = decision.getCroJustificationUpdateDate();
                            firstDecisionObj = decision;
                        }
                        if(lastDecision == null || lastDecisionDate.compareTo(decision.getCroJustificationUpdateDate()) < 0){
                            lastDecision =  decision.getDecisionCase();
                            lastDecisionDate = decision.getCroJustificationUpdateDate();
                            lastDecisionObj = decision;
                        }
                    }
                }
                Update update = new Update();
                update.set("decisionData.firstDecision", firstDecisionObj);
                update.set("decisionData.lastDecision", lastDecisionObj);
                mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            }
        } catch(Exception e){
            logger.error("Error occoured while updating decision info");
        }
    }

    /**
     * @param dedupeReferenceIds
     */
    private void updateDedupeStatus(List<String> dedupeReferenceIds) {
        if (dedupeReferenceIds != null && dedupeReferenceIds.size() > 0) {
            logger.info("updating status for dedupe to Declined");
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where("_id").in(dedupeReferenceIds)
                        .and("applicationStatus").ne("Approved"));
                Update update = new Update();
                update.set("applicationStatus", GNGWorkflowConstant.DECLINED.toFaceValue());
                update.set("applicationRequest.currentStageId", GNGWorkflowConstant.DCLN.toFaceValue());
                mongoTemplate.updateMulti(query, update,
                        GoNoGoCustomerApplication.class);

                logger.info("updated status for dedupe to Declined");
            } catch (Exception exp) {
                exp.printStackTrace();
                logger.error("Error occurred while updating " +
                        "dedupe Reference  Ids  status  with probable cause [{}] ", exp.getMessage());
            }

        }
    }


    @Override
    public String getSequence(String dealerId, String institutionId) {

        logger.info("inside getLatestSequence");
        /*if (!mongoTemplate.collectionExists(DealerSequenceRequest.class)) {
            mongoTemplate.createCollection(DealerSequenceRequest.class);
        }*/

        String sequence = "000001";
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerId").is(dealerId));

        DealerSequenceRequest sequenceObj = mongoTemplate.findOne(query,
                DealerSequenceRequest.class);

        if (sequenceObj == null) {

            sequenceObj = new DealerSequenceRequest();
            sequenceObj.setDealerId(dealerId);
            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);
            mongoTemplate.insert(sequenceObj);

            return sequence;

        } else {
            sequence = sequenceObj.getSequence();
            long sequenceNumber = Long.parseLong(sequence) + 1;
            sequence = SequenceGenerator.generateSequence(sequenceNumber);
            query = new Query();
            query.addCriteria(Criteria.where("dealerId").is(dealerId));

            Update update = new Update();
            update.set("sequence", sequence);
            mongoTemplate.updateFirst(query, update,
                    DealerSequenceRequest.class);

        }

        logger.info("outside getLatestSequence sequence is :" + sequenceObj);
        return sequence;
    }

    @Override
    public String getSequenceForProspectNumber(String institutionId, String sequenceType) {

        logger.info("Inside getSequenceForProspectNumber");

        String sequence = "00001";
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("sequenceType").is(sequenceType));

        DealerSequenceRequest sequenceObj = mongoTemplate.findOne(query,
                DealerSequenceRequest.class);
        if (sequenceObj == null) {

            sequenceObj = new DealerSequenceRequest();

            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);

            sequenceObj.setSequenceType(sequenceType);

            mongoTemplate.insert(sequenceObj);
            return sequence;
        } else {
            sequence = sequenceObj.getSequence();
            int sequenceInt = Integer.parseInt(sequence) + 1;
            logger.info("calling genrateSequenceForProspectNumber ");
            sequence = SequenceGenerator.genrateSequenceForProspectNumber(sequenceInt);

            Update update = new Update();
            update.set("sequence", sequence);

            mongoTemplate.updateFirst(query, update,
                    DealerSequenceRequest.class);
        }
        return sequence;
    }

    @Override
    public String getSequenceForNewProducts(String productID,
                                            String institutionId) {
        logger.info("getSequenceForNewProducts repo started for product:{} , institution:{}"
                , productID, institutionId);

        /*if (!mongoTemplate.collectionExists(DealerSequenceRequest.class)) {
            mongoTemplate.createCollection(DealerSequenceRequest.class);
        }*/

        String sequence = "0000000001";

        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("productID").is(productID));

        DealerSequenceRequest sequenceObj = mongoTemplate.findOne(query,
                DealerSequenceRequest.class);

        if (sequenceObj == null) {

            sequenceObj = new DealerSequenceRequest();
            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);
            sequenceObj.setProductID(productID);

            mongoTemplate.insert(sequenceObj);

            return sequence;

        } else {

            sequence = sequenceObj.getSequence();

            long sequenceNumber = Long.parseLong(sequence) + 1;

            sequence = SequenceGenerator.genrateSequenceForNewProduct(sequenceNumber);

            query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("productID").is(productID));

            Update update = new Update();
            update.set("sequence", sequence);

            mongoTemplate.updateFirst(query, update, DealerSequenceRequest.class);

        }

        logger.info("getSequenceForNewProducts repo completed with sequence {}", sequenceObj);

        return sequence;
    }

    @Override
    public boolean resetStatus(String applicationID, String institutionID,
                               String croID, String referenceId, String status) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionID));
            Update update = new Update();


            String currentStage = null;

            if (StringUtils.isNotBlank(status)) {

                update.set("applicationStatus", status);

                currentStage = GngUtils.getCurrentStageBasedOnApplicationStatus(status);

                if (StringUtils.isNotBlank(currentStage)) {

                    update.set("applicationRequest.currentStageId", currentStage);
                }

            } else {
                return false;
            }

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            goNoGoCustomerApplication.setApplicationStatus(status);

            updateApplicationInElasticsearch(goNoGoCustomerApplication);
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while updating " +
                    "applicationStatus  for institutionId {} and refId {} with probable cause [{}] ", institutionID, referenceId, e.getMessage());

            return false;
        }
    }

    @Override
    public void updateApplicationInElasticsearch(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        try {
            indexingRepository = new IndexingElasticSearchRepository();
            indexingRepository.indexNotification(goNoGoCustomerApplication);
        } catch (Exception e) {
            logger.error("Error occurred while updating application in elasticsearch " +
                            "for institutionId {} and refId {} with probable cause [{}] ",
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                    goNoGoCustomerApplication.getGngRefId(), e.getMessage());
        }
    }


    @Override
    public String setPostIpaStage(String refId, String stage,
                                  String institutionId)  {

        logger.debug("setPostIpaStage repo called for refId {}", refId);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("header.institutionId").is(institutionId));

            logger.debug("setPostIpaStage repo query formed {}", query.toString());

            Update update = new Update();

            if (refId != null) {

                update.set("currentStageId", stage);

            } else {

                return Status.ERROR.name();

            }

            mongoTemplate.updateFirst(query, update, ApplicationRequest.class);

            logger.debug("setPostIpaStage repo update query success");

            query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            logger.debug("setPostIpaStage repo query formed {}", query.toString());

            update = new Update();
            update.set("applicationRequest.currentStageId", stage);

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            logger.debug("setPostIpaStage repo update call success and flow completed with stage {}", stage);
            //update stage in elastic search
            updateApplicationInElasticsearch(getGoNoGoCustomerApplicationByRefId(refId));


            return Status.SUCCESS.name();

        } catch (Exception e) {

            logger.error("Error occurred while updating " +
                    "postIpa object and currentStageId for institutionId {} and refId {} with probable cause [{}] ", institutionId, refId, e.getMessage());

            return null;
            //throw new Exception(e.getMessage());
        }
    }

    @Override
    public ApplicationRequest getPartialSaveRequest(
            PartialSaveRequest partialSaveRequest) {

        logger.debug("getPartialSaveRequest repo started");

        Query query = new Query();

        query.addCriteria(Criteria.where("request.applicant.phone.phoneNumber")
                .is(partialSaveRequest.getMobileNo()).and("request.applicant.dateOfBirth").is(partialSaveRequest.getDob())
                .and("header.institutionId")
                .is(partialSaveRequest.getHeader().getInstitutionId())
                .orOperator(Criteria.where("header.dealerId").is(partialSaveRequest.getDealerId())));

        logger.debug("getPartialSaveRequest repo formed query {}", query.toString());

        return mongoTemplate.findOne(query,
                ApplicationRequest.class);

    }

    @Override
    public HashMap<String, List<String>> getKycMap(String refId,
                                                   String institutionId) {
        Query query = new Query();
        query.fields().include("request.applicant.kyc");
        query.addCriteria(Criteria.where("_id").is(refId));

        ApplicationRequest response = mongoTemplate.findOne(query,
                ApplicationRequest.class);
        List<Kyc> kycList = response.getRequest().getApplicant().getKyc();
        HashMap<String, List<String>> kycMap = new HashMap<String, List<String>>();
        List<String> value = null;
        for (Kyc kyc : kycList) {
            String kycName = kyc.getKycName();
            String kycValue = kyc.getKycNumber();

            if (kycMap.containsKey(kycName)) {
                value = kycMap.get(kycName);
                value.add(kycValue);
                kycMap.put(kycName, value);

            } else {
                value = new ArrayList<String>();
                value.add(kycValue);
                kycMap.put(kycName, value);
            }

        }

        return kycMap;

    }

    @Override
    public GoNoGoCustomerApplication getGoNoGoCustomerApplicationByRefId(
            String referenceId) {
        logger.debug("getGoNoGoCustomerApplicationByRefId repo started for refId {}", referenceId);
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(referenceId));

        GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                .findOne(query, GoNoGoCustomerApplication.class);
        logger.debug("fetched gngApp for refId {}", referenceId);
        return goNoGoCustomerApplication;
    }

    private List<CroDecision> getCroDecision(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {
        Eligibility eligibilityResponse = goNoGoCustomerApplication
                .getApplicantComponentResponse().getScoringServiceResponse()
                .getEligibilityResponse();
        List<CroDecision> croDecisions = new ArrayList<CroDecision>();
        CroDecision croDecision = new CroDecision();
        croDecision.setEligibleAmt(eligibilityResponse.getEligibilityAmount());
        croDecision.setTenor(Integer.parseInt(CustomFormat.DECIMAL_FORMATE_0.format(eligibilityResponse
                .getMaxTenor())));
        croDecision.setDownPayment(eligibilityResponse.getDp());
        croDecisions.add(croDecision);

        // Calculation to find approved amount.
        if (goNoGoCustomerApplication.getApplicationRequest().getRequest() != null) {

            if (eligibilityResponse.getEligibilityAmount() > goNoGoCustomerApplication
                    .getApplicationRequest().getRequest().getApplication()
                    .getLoanAmount()) {
                croDecision.setAmtApproved(goNoGoCustomerApplication
                        .getApplicationRequest().getRequest().getApplication()
                        .getLoanAmount());
            } else {
                croDecision.setAmtApproved(eligibilityResponse
                        .getEligibilityAmount());
            }
        }

        return croDecisions;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * ApplicationRepository#getPostIPA(java
     * .lang.String)
     */
    @Override
    public PostIPA getPostIPA(String refID, String institutionId)  {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("header.institutionId").is(institutionId));

            PostIpaRequest postIpaRequest = mongoTemplate.findOne(query,
                    PostIpaRequest.class);

            if (postIpaRequest != null) {
                return postIpaRequest.getPostIPA();
            } else {
                return null;
            }

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching " +
                    "postIpa object for institutionId {} and refId {} with probable cause [{}] ", institutionId, refID, e.getMessage());
            //throw new Exception(e.getMessage());
            return null;
        }

    }

    /**
     * Update LOSDetails in GoNoGoCustomerApplication
     */
    @Override
    public boolean updateLosDetails(String refID, LOSDetails losDetails,
                                    String institutionId)  {

        logger.debug("updateLosDetails repo started");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            Update update = new Update();

            update.set("losDetails", losDetails);

            update.set("applicationRequest.currentStageId", losDetails.getStatus());

            mongoTemplate
                    .updateFirst(query, update, GoNoGoCroApplicationResponse.class);

            logger.debug("updateLosDetails repo saved los status successfully ");

            GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);
            updateApplicationInElasticsearch(goNoGoCroApplicationResponse);

            return true;
        } catch (Exception e) {

            //e.printStackTrace();

            logger.error("Error occurred while updating los details " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    institutionId, refID, e.getMessage());

            //throw new Exception(e.getMessage());
            return false;
        }

    }

    @Override
    public boolean softDeleteDOReport(String refID, String institutionId, String fileName)  {

        logger.debug("softDeleteDOReport repo started");

        try {
            Query query = new Query();
            Pattern pattern = Pattern.compile("^" + Pattern.quote("DO_"));
            query.addCriteria(Criteria
                    .where("metadata.gonogoReferanceId").is(refID)
                    .and("metadata.uploadFileDetails.deleted").is(false)
                    .and("metadata.uploadFileDetails.fileType")
                    .is("application/pdf")
                    .and("metadata.uploadFileDetails.FileName").regex(pattern));

            logger.debug("softDeleteDOReport repo query formed {}", query.toString());

            GridFSDBFile doFile = gridFsTemplate.findOne(query);

            if (null != doFile) {

                DBObject dbObject = doFile.getMetaData();
                DBObject uploadFileDetails = (DBObject) dbObject.get("uploadFileDetails");
                uploadFileDetails.put("deleted", true);
                dbObject.put("uploadFileDetails", uploadFileDetails);
                doFile.setMetaData(dbObject);
                doFile.save();

                logger.debug("softDeleteDOReport repo started completed");
            }


            return true;

        } catch (Exception e) {

            logger.error("Error occurred while updating softDeleteDOReport " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    institutionId, refID, e.getMessage());
            return false;
            //throw new Exception(e.getMessage());

        }

    }


    @Override
    public boolean saveActivityLog(ActivityLogs activityLogs)  {


        logger.debug("saveActivityLog repo started");

        try {
            /*if (!mongoTemplate.collectionExists(ActivityLogs.class)) {

                mongoTemplate.createCollection(ActivityLogs.class);

            }*/

            mongoTemplate.insert(activityLogs);

            try {

                Thread.sleep(200);

            } catch (InterruptedException e) {

                logger.error("Error occurred while inserting activity log " +
                        " with probable cause [{}] ", e.getMessage());

                //throw new Exception(e.getMessage());

            }

            logger.debug("saveActivityLog repo completed");

            return true;

        } catch (Exception e) {

            logger.error("Error occurred while inserting activity log " +
                    " with probable cause [{}] ", e.getMessage());
                return false;
            //throw new Exception(e.getMessage());
        }
    }

    /**
     * @param refID Customer reference id.
     * @return true if application is already submitted.
     */
    @Override
    public boolean isSubmited(String refID, String action, String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("refId").is(refID).and("action")
                .is(action).and("institutionId").is(institutionId));
        return mongoTemplate.exists(query, ActivityLogs.class);
    }

    @Override
    public List<AuditData> getBreAuditData(AuditDataRequest auditDataRequest) {
        String collectionName = MongoRepositoryUtil
                .getAuditCollectionName(GoNoGoCroApplicationResponse.class);
        Query query = new Query();
        query.addCriteria(
                Criteria.where("auditData._id")
                        .is(auditDataRequest.getRefId())
                        .and("auditData.applicationRequest.header.institutionId")
                        .is(auditDataRequest.getHeader().getInstitutionId()))
                .limit(auditDataRequest.getNumberAuditRecords())
                .with(new Sort(Direction.ASC,
                        "auditData.applicationRequest.header.dateTime"));

        return mongoTemplate.find(query, AuditData.class, collectionName);
    }

    @Override
    public boolean updateInvoiceDetails(String refID,
                                        InvoiceDetails invoiceDetails, String institutionId)  {

        logger.debug("updateInvoiceDetails repo started");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            Update update = new Update();

            update.set("invoiceDetails", invoiceDetails);

            update.set("applicationRequest.currentStageId", GNGWorkflowConstant.INV_GNR.toFaceValue());

            mongoTemplate.updateFirst(query, update, GoNoGoCroApplicationResponse.class);

            logger.debug("updateInvoiceDetails repo completed");
            updateApplicationInElasticsearch(getGoNoGoCustomerApplicationByRefId(refID));


            return true;

        } catch (Exception e) {

            logger.error("Error occurred while updating InvoiceDetails " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    institutionId, refID, e.getMessage());
                return false;
            //throw new Exception(e.getMessage());

        }

    }

    @Override
    public DealerEmailMaster getDealerEmailMaster(Header header)  {

        logger.debug("getDealerEmailMaster repo started to getch dealer detail for dealer {}", header.getDealerId());

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("dealerID").is(header.getDealerId())
                    .and("institutionID").is(header.getInstitutionId())
                    .and("active").is(true));

            return mongoTemplate.findOne(query, DealerEmailMaster.class);

        } catch (Exception e) {

            logger.error("Error occurred while fetching DealerEmailMaster " +
                            " for institutionId {}  with probable cause [{}] ",
                    header.getInstitutionId(), e.getMessage());
                return null;
            //throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<ApplicationTracking> getAppTrackingLog(
            CheckApplicationStatus checkApplicationStatus) {
        String gngRefId = checkApplicationStatus.getGonogoRefId();
        Query query = new Query();
        query.addCriteria(Criteria.where("gngRefId").is(gngRefId));

        query.with(new Sort(Sort.Direction.ASC, "startDate"));
        List<ApplicationTracking> trackingLog = mongoTemplate.find(query,
                ApplicationTracking.class);

        return trackingLog;
    }

    @Override
    public GoNoGoCroApplicationResponse getGoNoGoCustomerApplicationByRefId(
            String refID, String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refID)
                .and("applicationRequest.header.institutionId")
                .is(institutionId));

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = null;

        try {

            goNoGoCroApplicationResponse = mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();

            logger.error("error occured while fetching gonogo customer application by ref id with probable cause [{}] ", e.getMessage());

            //throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }


        if (null != goNoGoCroApplicationResponse && !CollectionUtils.isEmpty(goNoGoCroApplicationResponse.getCroJustification()) &&
                !CollectionUtils.isEmpty(goNoGoCroApplicationResponse.getCroJustification())) {

            Collections.sort(goNoGoCroApplicationResponse.getCroJustification(),
                    (croJustification1, croJustification2) -> croJustification1.getCroJustificationUpdateDate().compareTo(croJustification2.getCroJustificationUpdateDate()));

            if (null != goNoGoCroApplicationResponse.getCroDecisions() && !goNoGoCroApplicationResponse.getCroDecisions().isEmpty()) {
                Collections.sort(goNoGoCroApplicationResponse.getCroDecisions(),
                        (croDecision, t1) -> croDecision.getDecisionUpdateDate().compareTo(t1.getDecisionUpdateDate()));
            }

        }

        return goNoGoCroApplicationResponse;
    }

    public String getParentRefId(String refId) {
        Query query = new Query();
        String parentId = null;
        query.addCriteria(Criteria.where("gngRefId").is(refId));
        GoNoGoCustomerApplication gonogoResponse = mongoTemplate
                .findOne(query, GoNoGoCustomerApplication.class);
        if (null != gonogoResponse) {
            parentId = gonogoResponse.getParentID();
        }
        return parentId;
    }

    @Override
    public boolean isProductExist(String institution, String productID,
                                  String productName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionID").is(institution)
                .and("productID").is(productID).and("productName")
                .is(productName).and("status").is(true));
        return mongoTemplate.exists(query,
                InstitutionProductConfiguration.class);
    }

    @Override
    public boolean upsertFileHandoverDetails(FileHandoverDetails fileHandoverDetails)  {

        logger.debug("upsertFileHandoverDetails repo started");

        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(fileHandoverDetails, dbObject);
            mongoTemplate.execute(FileHandoverDetails.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(fileHandoverDetails.getRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            logger.debug("upsertFileHandoverDetails repo completed");

            return true;

        } catch (DataAccessException e) {

            logger.error("Error occurred while saving DealerEmailMaster " +
                            "  with probable cause [{}] "
                    , e.getMessage());
                return  false;
            //throw new SystemException(String.format("Error occurred while saving filehandover details with probable cause [%s] ", e.getMessage()));
        }
    }

    @Override
    public FileHandoverDetails getFileHandoverDetails(
            FileHandoverDetailsRequest fileHandoverDetailsRequest)  {

        logger.debug("getFileHandoverDetails repo started");

        try {

            Query query = new Query();
            query.addCriteria(Criteria
                    .where("refId")
                    .is(fileHandoverDetailsRequest.getRefId())
                    .and("header.institutionId")
                    .is(fileHandoverDetailsRequest.getHeader()
                            .getInstitutionId()));

            logger.debug("getFileHandoverDetails repo query formed {}", query.toString());

            return mongoTemplate.findOne(query, FileHandoverDetails.class);

        } catch (DataAccessException e) {

            logger.error("Error occurred while fetching FileHandoverDetails " +
                            "  with probable cause [{}] "
                    , e.getMessage());
                return null;
            //throw new SystemException(e.getMessage());
        }

    }

    @Override
    public boolean updateGstDetails(GstDetailsRequest gstDetailsRequest) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(gstDetailsRequest.getRefId())
                    .and("applicationRequest.header.institutionId")
                    .is(gstDetailsRequest.getHeader().getInstitutionId()));

            Update update = new Update();

            if (null != gstDetailsRequest.getGstDetails()) {
                update.set("applicationRequest.request.gstDetails", gstDetailsRequest.getGstDetails());
            }

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCroApplicationResponse.class);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {

            //e.printStackTrace();

            logger.error("error occurred  while updating GstDetails  parameter", e.getMessage());

            //throw new SystemException(String.format("error occurred  while updating GstDetails  parameter with probable cause [%s]", e.getMessage()));

            return false;
        }
    }

    @Override
    public void submitCustomerFinancialProfile(final CustomerFinancialProfileRequest customerFinancialProfileRequest) {
        try {
            final BasicDBObject dbObject = new BasicDBObject();

            mongoTemplate.getConverter().write(customerFinancialProfileRequest, dbObject);
            mongoTemplate.execute(CustomerFinancialProfileRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(customerFinancialProfileRequest.getRefId())).
                        getQueryObject(), dbObject, true, false
                );
                return null;
            });
            logger.debug("Customer financial profile saved successfully for refId {}",
                    customerFinancialProfileRequest.getRefId());

        } catch (Exception ex) {
            logger.error("Customer financial profile save failed for refId {} with probable cause [{}] ",
                    customerFinancialProfileRequest.getRefId(), ex.getMessage(), ex);

            /*throw new SystemException(String.format("Customer financial profile save failed with probable cause [%s]",
                    ex.getMessage()));
       */ }
    }

    @Override
    public void setMultiProductUpdatedParameter(PostIpaRequest postIpaRequest) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(postIpaRequest.getRefID()));

            double unUtilizedAmount = postIpaRequest.getPostIPA().getApprovedAmount() - postIpaRequest.getPostIPA().getNetFundingAmount();
            Update update = new Update();
            update.set("croDecisions.0.unUtilizedAmt", unUtilizedAmount);

            mongoTemplate.updateFirst(query, update, GoNoGoCroApplicationResponse.class);

        } catch (DataAccessException e) {

            //e.printStackTrace();

            logger.error("error occurred  while updating multiproduct  parameter", e.getMessage());

            //throw new SystemException(String.format("error occurred  while updating multiproduct  parameter with probable cause [%s]", e.getMessage()));


        }
    }

    @Override
    public boolean isGoNoGoCustomerAppPresent(String refId) {
        Query query = new Query();

        query.addCriteria(Criteria.where("_id").is(refId));

        return mongoTemplate.exists(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean isApplicationRequestExist(String refId, String institutionId) {
        Query query = new Query();

        query.addCriteria(Criteria.where("_id").is(refId).and("header.institutionId").is(institutionId));

        return mongoTemplate.exists(query, ApplicationRequest.class);
    }

    @Override
    public WriteResult updateApplicantReferences(UpdateReferencesRequest updateReferencesRequest) {
        WriteResult writeResult = null;
        try {

            Query query = new Query();
            query.addCriteria(Criteria
                    .where("_id")
                    .is(updateReferencesRequest.getRefId())
                    .and("applicationRequest.header.institutionId")
                    .is(updateReferencesRequest.getHeader().getInstitutionId()));

            Update update = new Update();

            update.set("applicationRequest.request.applicant.applicantReferences", updateReferencesRequest.getApplicantReferences());

            writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.info("Write Result:->", writeResult);

        } catch (DataAccessException e) {

            logger.error("error occurred  while updateApplicantReferences  list with probable cause {[%s]}", e.getMessage());

            //throw new SystemException(String.format("error occurred  while updating updateApplicantReferences  list with probable cause [%s]", e.getMessage()));

        }
        return writeResult;
    }

    @Override
    public WriteResult updateBankingDetails(BankingDetailsRequest bankingDetailsRequest) {
        WriteResult writeResult = null;
        try {

            Query query = new Query();
            query.addCriteria(Criteria
                    .where("_id")
                    .is(bankingDetailsRequest.getRefId())
                    .and("applicationRequest.header.institutionId")
                    .is(bankingDetailsRequest.getHeader().getInstitutionId()));

            Update update = new Update();

            update.set("applicationRequest.request.applicant.bankingDetails", bankingDetailsRequest.getBankingDetails());

            writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.info("Write Result:->", writeResult);

        } catch (DataAccessException e) {

            logger.error("error occurred  while BankingDetails  list with probable cause {[%s]}", e.getMessage());

            //throw new SystemException(String.format("error occurred  while updating BankingDetails  list with probable cause [%s]", e.getMessage()));

        }
        return writeResult;
    }


    @Override
    public List<NegativeAreaGeoLimitMaster> getNegativeAreaFundingDetails(NegativeAreaFundingRequest negativeAreaFundingRequest)  {

        logger.debug("getNegativeAreaFundingDetails repo started");

        try {

            Query query = new Query();

            query.addCriteria(Criteria
                    .where("pinCode")
                    .is(negativeAreaFundingRequest.getPinCode())
                    .and("institutionId")
                    .is(negativeAreaFundingRequest.getHeader()
                            .getInstitutionId()).and("isNegative").is(Status.YES.name()).and("active").is(true));


            logger.debug("getNegativeAreaFundingDetails executed Successfully");

            return mongoTemplate.find(query,
                    NegativeAreaGeoLimitMaster.class);

        } catch (DataAccessException e) {

            logger.error("Error occurred while fetching NegativeAreaGeoLimitMaster " +
                            "for institutionId {} and pincode {} " +
                            "  with probable cause [{}] "
                    , negativeAreaFundingRequest.getHeader()
                            .getInstitutionId(), negativeAreaFundingRequest.getPinCode(), e.getMessage());
                        return null;
            //throw new SystemException(String.format(" error occurred  while getting negative area funding details based on institutionId and referenceId with probable cause [%s]", e.getMessage()));
        }
    }

    private CroQueue buildCroQueueObjectFromAppResponse(ApplicationRequest applicationRequest) {

        CroQueue croQueue = new CroQueue();

        if (null != applicationRequest
                && applicationRequest.getHeader() != null
                && applicationRequest.getRequest() != null
                && applicationRequest.getRequest().getApplicant() != null) {

            croQueue.setRefId(applicationRequest.getRefID());

            croQueue.setName(applicationRequest.getRequest()
                    .getApplicant().getApplicantName());

            List<Phone> phoneList = applicationRequest.getRequest()
                    .getApplicant().getPhone();

            Phone mobilePhone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE, phoneList).orElse(null);

            if (mobilePhone != null) {

                croQueue.setMobileNumber(mobilePhone.getPhoneNumber());

            }


            croQueue.setProcessedBy(applicationRequest.getHeader()
                    .getCroId());

            croQueue.setDsaName((applicationRequest.getHeader()
                    .getDsaId()));

            croQueue.setApplicationId(applicationRequest.getHeader()
                    .getApplicationId());

            if (applicationRequest.getRequest().getApplication() != null) {

                croQueue.setLoanAmmount(applicationRequest.getRequest()
                        .getApplication().getLoanAmount());

            }

        }

        croQueue.setStage(applicationRequest.getCurrentStageId());

        return croQueue;
    }


    @Override

    public boolean updateOpsData(LOSDetailsRequest lOSDetailsRequest) {
        logger.debug("updateLosDetails repo started");
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(lOSDetailsRequest.getRefID())
                    .and("applicationStatus").is(GNGWorkflowConstant.APPROVED.toFaceValue())
                    .and("losDetails.losID").is(lOSDetailsRequest.getlOSDetails().getLosID())
                    .and("losDetails.status").ne(GNGWorkflowConstant.LOS_DCLN.toFaceValue())
                    .and("applicationRequest.header.institutionId")
                    .is(lOSDetailsRequest.getHeader().getInstitutionId()));

            Update update = new Update();

            if (StringUtils.isNotBlank(lOSDetailsRequest.getlOSDetails().getUtrNumber())) {
                update.set("losDetails.utrNumber", lOSDetailsRequest.getlOSDetails().getUtrNumber());
            }
            if (StringUtils.isNotBlank(lOSDetailsRequest.getlOSDetails().getDealerReferenceNumber())) {
                update.set("losDetails.dealerReferenceNumber", lOSDetailsRequest.getlOSDetails().getDealerReferenceNumber());
            }

            update.set("losDetails.dealerTieUpFlag", lOSDetailsRequest.getlOSDetails().getDealerTieUpFlag());

            update.set("applicationRequest.currentStageId", GNGWorkflowConstant.LOS_DISB.toFaceValue());

            update.set("losDetails.status", GNGWorkflowConstant.LOS_DISB.toFaceValue());

            WriteResult writeResult = mongoTemplate
                    .updateFirst(query, update, GoNoGoCroApplicationResponse.class);

            logger.debug("updateLosDetails repo saved los status successfully ");

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {

            logger.error("Error occurred while updating los details " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    lOSDetailsRequest.getHeader().getInstitutionId(), lOSDetailsRequest.getRefID(),
                    e.getMessage());
            return false;
           /* throw new SystemException(String.format("Error occurred while updating los details " +
                            "for institutionId {%s} and refId {%s} with probable cause [{%s}]",
                    lOSDetailsRequest.getHeader().getInstitutionId(), lOSDetailsRequest.getRefID(),
                    e.getMessage()));*/
        }
    }

    @Override
    public EWPremiumMaster getExtendedWarrantyPremium(String institutionId, String productAliasName, String eWAssetCategory, double gngAssetPrice) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("ewAssetCategory").is(eWAssetCategory)
                    .and("minAssetValue").lte(gngAssetPrice)
                    .and("maxAssetValue").gte(gngAssetPrice)
                    .and("institutionId").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("active").is(true));

            return mongoTemplate.findOne(query, EWPremiumMaster.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching ExtendedWarrantyPremium with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching ExtendedWarrantyPremium  with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public boolean checkGngAssetAndEWAssetMapping(String institutionId, String productAliasName, String eWAssetCategory) {

        Query query = new Query();

        query.addCriteria(Criteria.where("ewAssetCategory").is(eWAssetCategory)
                .and("institutionId").is(institutionId)
                .and("productFlag").is(productAliasName)
                .and("active").is(true));

        return mongoTemplate.exists(query, EWPremiumMaster.class);
    }

    @Override
    public void updateExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(extendedWarrantyDetails.getRefId())
                    .and("institutionId").is(extendedWarrantyDetails.getHeader().getInstitutionId())
                    .and("activeFlag").is(true));

            Update update = new Update();
            update.set("header", extendedWarrantyDetails.getHeader());
            update.set("gngAssetCategory", extendedWarrantyDetails.getGngAssetCategory());
            update.set("eWAssetCategory", extendedWarrantyDetails.geteWAssetCategory());
            update.set("oemWarranty", extendedWarrantyDetails.getOemWarranty());
            update.set("ewWarrantyTenure", extendedWarrantyDetails.getEwWarrantyTenure());
            update.set("ewWarrantyPremium", extendedWarrantyDetails.getEwWarrantyPremium());
            update.set("assetSerialNumber", extendedWarrantyDetails.getAssetSerialNumber());
            update.set("revisedTotalEmi", extendedWarrantyDetails.getRevisedTotalEmi());
            update.set("paymentMethod", extendedWarrantyDetails.getPaymentMethod());
            update.set("ewDPAmt", extendedWarrantyDetails.getEwDPAmt());

            mongoTemplate.upsert(query, update, ExtendedWarrantyDetails.class);

        } catch (DataAccessException e) {

            //e.printStackTrace();
            logger.error("Error occurred while updating Extended Warranty Details with probable cause [{}] "
                    , e.getMessage());

            //throw new SystemException(String.format("Error occurred while updating Extended Warranty Details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public ExtendedWarrantyDetails fetchExtendedWarrantyDetails(String refId, String institutionId) {

        logger.debug("fetchExtendedWarrantyDetails repo started");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("header.institutionId").is(institutionId).and("activeFlag").is(true));

            return mongoTemplate.findOne(query, ExtendedWarrantyDetails.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching ExtendedWarrantyDetails with probable cause [{}] ", e.getMessage());
                return null;
            //throw new SystemException(String.format(" error occurred  while fetching ExtendedWarrantyDetails based on institutionId and referenceId with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean checkGngAssetAndInsuranceAssetMapping(String institutionId, String productAliasName, String insuranceAssetCat) {

        Query query = new Query();

        query.addCriteria(Criteria.where("insuranceAssetCat").is(insuranceAssetCat)
                .and("institutionId").is(institutionId)
                .and("productFlag").is(productAliasName)
                .and("active").is(true));

        return mongoTemplate.exists(query, InsurancePremiumMaster.class);
    }

    @Override
    public InsurancePremiumMaster getInsurancePremiumData(String institutionId, String productAliasName, String insuranceAssetCat, double gngAssetPrice) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("insuranceAssetCat").is(insuranceAssetCat)
                    .and("minAssetValue").lte(gngAssetPrice)
                    .and("maxAssetValue").gte(gngAssetPrice)
                    .and("institutionId").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("active").is(true));

            return mongoTemplate.findOne(query, InsurancePremiumMaster.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching InsurancePremiumData with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching InsurancePremiumData  with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public void updateInsuranceDetails(InsurancePremiumDetails insurancePremiumDetails) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(insurancePremiumDetails.getRefId())
                    .and("institutionId").is(insurancePremiumDetails.getHeader().getInstitutionId())
                    .and("activeFlag").is(true));

            Update update = new Update();
            update.set("header", insurancePremiumDetails.getHeader());
            update.set("gngAssetCategory", insurancePremiumDetails.getGngAssetCategory());
            update.set("insAssetCategory", insurancePremiumDetails.getInsAssetCategory());
            update.set("assetSerialNumber", insurancePremiumDetails.getAssetSerialNumber());
            update.set("insurancePremium", insurancePremiumDetails.getInsurancePremium());
            update.set("insuranceEmi", insurancePremiumDetails.getInsuranceEmi());
            update.set("paymentMethod", insurancePremiumDetails.getPaymentMethod());
            update.set("insuranceDPAmt", insurancePremiumDetails.getInsuranceDPAmt());
            update.set("insuranceType", insurancePremiumDetails.getInsuranceType());

            mongoTemplate.upsert(query, update, InsurancePremiumDetails.class);

        } catch (DataAccessException e) {

            //e.printStackTrace();
            logger.error("Error occurred while updating Insurance Details with probable cause [{}] "
                    , e.getMessage());

            //throw new SystemException(String.format("Error occurred while updating Insurance Details with probable cause [%s] ", e.getMessage()));
        }

    }


    @Override
    public void updateCreditVidyaDetails(CreditVidyaDetails creditVidyaDetails) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(creditVidyaDetails.getRefId())
                    .and("institutionId").is(creditVidyaDetails.getHeader().getInstitutionId())
                    .and("activeFlag").is(true));

            Update update = new Update();
            update.set("header", creditVidyaDetails.getHeader());
            update.set("cVAmount", creditVidyaDetails.getcVAmount());
            update.set("paymentMethod", creditVidyaDetails.getPaymentMethod());

            mongoTemplate.upsert(query, update, CreditVidyaDetails.class);

        } catch (DataAccessException e) {

            //e.printStackTrace();
            logger.error("Error occurred while updating Creditvidya Details with probable cause [{}] "
                    , e.getMessage());

            //throw new SystemException(String.format("Error occurred while updating Creditvidya Details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public InsurancePremiumDetails fetchInsuranceDetails(String refId, String institutionId) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("header.institutionId").is(institutionId).and("activeFlag").is(true));

            return mongoTemplate.findOne(query, InsurancePremiumDetails.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching InsurancePremiumDetails with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching InsurancePremiumDetails based on institutionId and referenceId with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public CreditVidyaDetails fetchCreditVidyaDetails(String refId, String institutionId) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("header.institutionId").is(institutionId).and("activeFlag").is(true));

            return mongoTemplate.findOne(query, CreditVidyaDetails.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching CreditVidyaDetails with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching CreditVidyaDetails based on institutionId and referenceId with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public EwInsGngAssetCategoryMaster getAssetCategoryAndOemWarranty(String institutionId, String productAliasName, String gngAssetCategory) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("gngAssetCat").is(gngAssetCategory)
                    .and("institutionId").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("active").is(true));

            return mongoTemplate.findOne(query, EwInsGngAssetCategoryMaster.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching getAssetCategoryAndOemWarranty with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching getAssetCategoryAndOemWarranty  with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public SerialNumberInfo getSerialNumberInfo(String refId) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(refId).and("status").is(Status.VALID.name()));

            return mongoTemplate.findOne(query, SerialNumberInfo.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while getting SerialNumberInfo with probable cause [{}]", e.getMessage());
            return  null;
            //throw new SystemException(String.format(" Error occurred while getting SerialNumberInfo with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean updateRevisedEmiAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(updateRevisedEmiRequest.getRefId())
                    .and("header.institutionId").is(updateRevisedEmiRequest.getHeader().getInstitutionId()));

            Update update = new Update();
            update.set("postIPA.revisedEmi", updateRevisedEmiRequest.getRevisedEmi());
            update.set("postIPA.revisedLoanAmount", updateRevisedEmiRequest.getRevisedLoanAmt());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, PostIpaRequest.class);

            logger.info("Write Result:->", writeResult);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while updating revisedEmi amount in postIpa collection with probable cause [{}]", e.getMessage());
            return false;
            //throw new SystemException(String.format("error occured while updating revisedEmi amount in postIpa collection with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean softDeleteExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(extendedWarrantyDetails.getRefId()).and("activeFlag").is(true));

            Update update = new Update();

            update.set("activeFlag", false);
            update.set("header.dateTime", new Date());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, ExtendedWarrantyDetails.class);

            logger.info("writeResult :-" + writeResult);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException e) {

            //e.printStackTrace();
            logger.error("Error occurred while soft deleting extended warranty details with probable cause [{}] "
                    , e.getMessage());
                return false;
            //throw new SystemException(String.format("Error occurred while soft deleting extended warranty details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public boolean softDeleteInsuranceDetails(String refId) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(refId).and("activeFlag").is(true));

            Update update = new Update();

            update.set("activeFlag", false);
            update.set("header.dateTime", new Date());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, InsurancePremiumDetails.class);

            logger.info("writeResult :-" + writeResult);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException e) {

            //e.printStackTrace();
            logger.error("Error occurred while soft deleting Insurance details with probable cause [{}] "
                    , e.getMessage());
                return false;
            //throw new SystemException(String.format("Error occurred while soft deleting Insurance details with probable cause [%s] ", e.getMessage()));
        }

    }

    @Override
    public boolean postIpaExists(String refID, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refID)
                .and("header.institutionId").is(institutionId));
        return mongoTemplate.exists(query, PostIpaRequest.class);
    }

    @Override
    public GoNoGoCroApplicationResponse getGoNoGoCustomerApplicationOnlyByRefId(String refId) {
        logger.debug("fetching gonogo application by refId only for {}",refId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));

            return mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();

            logger.error("error occured while fetching gonogo customer application by ref id with probable cause [{}] ", e.getMessage());
                return null;
            //throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean updateTvrStatus(UpdateTvrStatusRequest updateTvrStatusRequest) {
        String referenceId = updateTvrStatusRequest.getReferenceId();
        TvrDetails details = updateTvrStatusRequest.getTvrDetails();
        if (details != null) {
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where("_id").is(referenceId));
                Update update = new Update();
                update.set("tvrDetails", details);
                WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
                return writeResult.isUpdateOfExisting();
            } catch (Exception e) {
                //e.printStackTrace();
                logger.error("Error occurred while update Tvr for refID {} with probable cause [{}] "
                        , referenceId, e.getMessage());
                return false;
            }
        } else {
            logger.debug("No TVR Details for refID {} to update", referenceId);
            return false;
        }
    }

    @Override
    public boolean isSerialOrImeiValidationSkipApplicableToDealer(String institutionId, String dealerId, String product) {

        boolean canDealerSkip = false;

        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("dealerId").is(dealerId).and("productId").is(product).and("active").is(true));
        query.fields().include("skipSerialOrImeiValidation");

        DealerBranchMaster dealerBranchMaster = mongoTemplate.findOne(query, DealerBranchMaster.class);

        if (null != dealerBranchMaster) {
            canDealerSkip = dealerBranchMaster.isSkipSerialOrImeiValidation();
        }

        return canDealerSkip;
    }

    @Override
    public GstDetails fetchGstDetails(String refId, String instId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));
            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
            GstDetails gstDetails = goNoGoCustomerApplication.getApplicationRequest().getRequest().getGstDetails();
            return gstDetails;
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while update Tvr for refID {} with probable cause [{}] "
                    , refId, e.getMessage());
        }
        return null;
    }

    @Override
    public List<BankingDetails> getBankAccountHolderName(String instId, String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId).
                and("applicationRequest.header.institutionId").is(instId));
        GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
        List<BankingDetails> bankingDetails = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBankingDetails();
        return bankingDetails;
    }

    @Override
    public BranchMaster getDealerBranchState(String branchName, String instId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("branchName").is(branchName).and("active")
                .is(true).and("institutionId").is(instId));
        return mongoTemplate.findOne(query, BranchMaster.class);
    }

    @Override
    public String getSequenceForTvs(String institutionId, Product product, String branchCode) {
        logger.info("getSequenceForNewProducts repo started for product:{} , institution:{}"
                , product.toProductId(), institutionId);

        /*if (!mongoTemplate.collectionExists(DealerSequenceRequest.class)) {
            mongoTemplate.createCollection(DealerSequenceRequest.class);
        }*/

        String sequence = "0000001";

        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("branchCode").is(branchCode));

        DealerSequenceRequest sequenceObj = mongoTemplate.findOne(query,
                DealerSequenceRequest.class);

        if (sequenceObj == null) {

            sequenceObj = new DealerSequenceRequest();
            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);
            sequenceObj.setProductID(product.toProductId());
            sequenceObj.setBranchCode(branchCode);

            mongoTemplate.insert(sequenceObj);

            return sequence;

        } else {

            sequence = sequenceObj.getSequence();

            long sequenceInt = Long.parseLong(sequence) + 1;

            sequence = String.format("%07d", sequenceInt);

            query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("branchCode").is(branchCode));

            Update update = new Update();
            update.set("sequence", sequence);

            mongoTemplate.updateFirst(query, update, DealerSequenceRequest.class);

        }

        logger.info("getSequenceForNewProducts repo completed with sequence {}", sequenceObj);

        return sequence;
    }


    @Override
    public PosidexResponse getPosidexResponse(ApplicationStatusLogRequest applicationStatusLogRequest) {
        PosidexResponse posidexResponse = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationStatusLogRequest.getRefId()).and("applicationRequest.header.institutionId").is(applicationStatusLogRequest.getHeader().getInstitutionId()));
            query.fields().include("applicantComponentResponse.posidexResponse");
            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
            if (null != goNoGoCustomerApplication && null != goNoGoCustomerApplication.getApplicantComponentResponse() && null != goNoGoCustomerApplication.getApplicantComponentResponse().getPosidexResponse()) {
                posidexResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getPosidexResponse();
            }
        } catch (Exception e) {
            logger.error("Error occurred while getting posidex data for refID {} with probable cause [{}] "
                    , applicationStatusLogRequest.getRefId(), e.getMessage());
            //throw new SystemException(String.format("Error occurred while getting posidex data for refID {%s} with probable cause [{%s}]", applicationStatusLogRequest.getRefId(), e.getMessage()));
        }
        return posidexResponse;
    }

    @Override
    public GoNoGoCustomerApplication getGonogoDocument(String refID) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);

        } catch (DataAccessException e) {

            //e.printStackTrace();

            logger.error("error occurred while fetching gonogo customer application by ref id with probable cause [{}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean updateDedupeFields(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(additionalDedupeFieldsRequest.getRefId()).and("applicationRequest.header.institutionId").is(additionalDedupeFieldsRequest.getHeader().getInstitutionId()));
        Update update = new Update();
        update.set("applicationRequest.request.application.dedupeEmiPaid", additionalDedupeFieldsRequest.getDedupeEmiPaid());
        update.set("applicationRequest.request.application.dedupeTenor", additionalDedupeFieldsRequest.getDedupeTenor());
        update.set("applicationRequest.request.application.dedupeRemark", additionalDedupeFieldsRequest.getDedupeRemark());
        update.set("intrimStatus.dedupeCompleteDt", new Date());
        WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

        if (null != writeResult) {
            return writeResult.isUpdateOfExisting();
        } else {
            return false;
        }

    }

    @Override
    public boolean updateDisbursementData(String refId, String agreementNum, String stage, String dealerId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));
            Update update = new Update();
            update.set("agreementNum", agreementNum);
            update.set("applicationRequest.currentStageId", stage);
            update.set("disbursementDetails.disbDate", new Date());
            update.set("disbursementDetails.dealerId", dealerId);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            updateApplicationInElasticsearch(getGoNoGoCustomerApplicationByRefId(refId));
            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while updating Agreement Number for refID {} with probable cause [{}] "
                    , refId, e.getMessage());
            return false;
            /*throw new SystemException(String.format("Error occurred while updating Agreement Number for refID [%s] "
                    + "with probable cause [%s] ", refId, e.getMessage()));*/
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
    }

    @Override
    public void updateDedupeApplicationStatus(String institutionId, String dedupedRefId, String appStatus, String appStage, GoNoGoCustomerApplication goNoGoCustomerApplication, String croJustificationRemark) {

        logger.debug("updateDedupeApplicationStatus repo started");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(dedupedRefId).
                    and("applicationRequest.header.institutionId").is(institutionId));

            Update update = new Update();

            update.set("applicationRequest.currentStageId", appStage);
            update.set("applicationStatus", appStatus);

            // update in the elastic search

            List<CroJustification> croJustification = goNoGoCustomerApplication.getCroJustification();

            CroJustification croJust = new CroJustification();
            croJust.setRemark(croJustificationRemark);
            croJust.setDecisionCase(appStatus);

            if (CollectionUtils.isEmpty(croJustification)) {
                croJustification = new ArrayList<>();
            }

            croJustification.add(croJust);
            update.set("croJustification", croJustification);

            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            goNoGoCustomerApplication.setApplicationStatus(appStatus);
            goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(appStage);
            goNoGoCustomerApplication.setStatusFlag(true);

            updateInElasticSearch(goNoGoCustomerApplication);

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("error occured while updating dedupeApplicationStatus with probable cause [{}] ", e.getMessage());

            //throw new SystemException(String.format("error occured while updating dedupeApplicationStatus with probable cause [{%s}]", e.getMessage()));
        }

    }

    private void updateInElasticSearch(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        try {
            indexingRepository = new IndexingElasticSearchRepository();
            indexingRepository.indexNotification(goNoGoCustomerApplication);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    @Override
    public ApplicationRequest getApplicationRequest(String refId, String instId) {
        logger.debug("getApplicantData");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).
                    and("applicationRequest.header.institutionId").is(instId));
            GoNoGoCustomerApplication goCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
            return goCustomerApplication.getApplicationRequest();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("error while finding data against ref id" + e.getMessage());
        }
        return null;
    }

    @Override
    public boolean updateDedupeFieldsInApplicationRequest(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(additionalDedupeFieldsRequest.getRefId()).and("header.institutionId").is(additionalDedupeFieldsRequest.getHeader().getInstitutionId()));
        Update update = new Update();
        update.set("request.application.dedupeEmiPaid", additionalDedupeFieldsRequest.getDedupeEmiPaid());
        update.set("request.application.dedupeTenor", additionalDedupeFieldsRequest.getDedupeTenor());
        update.set("request.application.dedupeRemark", additionalDedupeFieldsRequest.getDedupeRemark());
        WriteResult writeResult = mongoTemplate.updateFirst(query, update, ApplicationRequest.class);

        if (null != writeResult) {
            return writeResult.isUpdateOfExisting();
        } else {
            return false;
        }

    }

    @Override
    public long getIncrIdBySeqType(String institutionId, String branchCode, Product product, SequenceType seqType, int n) {
        logger.info("getIncrIdBySeqType repo started for product:{} , institution:{}", product, institutionId);

        /*if (!mongoTemplate.collectionExists(DealerSequenceRequest.class)) {
            mongoTemplate.createCollection(DealerSequenceRequest.class);
        }*/

        String sequence = "1";
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("sequenceType")
                .is(seqType.name()).and("branchCode").is(branchCode).and("productID").is(product.toProductId()));

        DealerSequenceRequest sequenceObj = mongoTemplate.findOne(query,
                DealerSequenceRequest.class);

        if (sequenceObj == null) {
            sequenceObj = new DealerSequenceRequest();
            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);
            sequenceObj.setSequenceType(seqType.name());
            sequenceObj.setBranchCode(branchCode);
            sequenceObj.setProductID(product.toProductId());

            mongoTemplate.insert(sequenceObj);
            return Long.parseLong(sequence);
        } else {
            sequence = sequenceObj.getSequence();
            long sequenceInt = Long.parseLong(sequence) + n;
            query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("sequenceType")
                    .is(seqType.name()).and("branchCode").is(branchCode).and("productID").is(product.toProductId()));
            Update update = new Update();
            update.set("sequence", "" + sequenceInt);
            mongoTemplate.updateFirst(query, update, DealerSequenceRequest.class);
            return sequenceInt;
        }
    }

    @Override
    public String getUpdatedCountForEMandate(String institutionId) {
        logger.info("getUpdatedCountForEMandate repo started for institution:{}", institutionId);

        /*if (!mongoTemplate.collectionExists(EmandateCounter.class)) {
            mongoTemplate.createCollection(EmandateCounter.class);
        }*/

        String sequence = "000001";
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));

        EmandateCounter sequenceObj = mongoTemplate.findOne(query,
                EmandateCounter.class);

        if (sequenceObj == null) {
            sequenceObj = new EmandateCounter();
            sequenceObj.setSequence(sequence);
            sequenceObj.setInstitutionId(institutionId);
            mongoTemplate.insert(sequenceObj);
            return sequence;
        } else {
            sequence = sequenceObj.getSequence();
            long sequenceInt = Long.parseLong(sequence) + 1;
            sequence = SequenceGenerator.generateSequence(sequenceInt);
            query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId));
            Update update = new Update();
            update.set("sequence", sequence);
            mongoTemplate.updateFirst(query, update, EmandateCounter.class);
        }
        logger.info("getUpdatedCountForEMandate repo completed with sequence {}", sequenceObj);
        return sequence;
    }

    @Override
    public InsurancePremiumMaster getInsurancePremium(String institutionId, String productAliasName) {

        logger.debug("getInsurance premimum repo invoked ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("active").is(true));

            return mongoTemplate.findOne(query, InsurancePremiumMaster.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching InsurancePremiumData with probable cause [{}] ", e.getMessage());
                return null;
            //throw new SystemException(String.format(" error occurred  while fetching InsurancePremiumData  with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public InsurancePremiumMaster getInsurancePremiumAgainstProvider(String institutionId, String insuranceType) {

        logger.debug("getInsurance premimum repo invoked ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("insuranceType").is(insuranceType)
                    .and("active").is(true));

            return mongoTemplate.findOne(query, InsurancePremiumMaster.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching InsurancePremiumData with probable cause [{}] ", e.getMessage());
                return null;
            //throw new SystemException(String.format(" error occurred  while fetching InsurancePremiumData  with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean updateAddonServiceRevisedAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(updateRevisedEmiRequest.getRefId())
                    .and("header.institutionId").is(updateRevisedEmiRequest.getHeader().getInstitutionId()));

            Update update = new Update();
            update.set("postIPA.revisedEmi", updateRevisedEmiRequest.getRevisedEmi());
            update.set("postIPA.revisedLoanAmount", updateRevisedEmiRequest.getRevisedLoanAmt());
            update.set("postIPA.emiStartDate", updateRevisedEmiRequest.getEmiStartDate());
            update.set("postIPA.emiEndDate", updateRevisedEmiRequest.getEmiEndDate());
            update.set("postIPA.downPaymentReceiptNo", updateRevisedEmiRequest.getDownPaymentReceiptNo());
            update.set("postIPA.downPaymentDate", updateRevisedEmiRequest.getDownPaymentDate());
            update.set("postIPA.downPaymentAmt", updateRevisedEmiRequest.getDownPaymentAmt());
            update.set("postIPA.umfc", updateRevisedEmiRequest.getUmfc());
            update.set("postIPA.totalRecvAmt", updateRevisedEmiRequest.getTotalRecvAmt());
            update.set("postIPA.totalRecDwnAmt", updateRevisedEmiRequest.getTotalRecDwnAmt());
            update.set("postIPA.netDisbursalAmount", updateRevisedEmiRequest.getNetDisbursalAmount());
            update.set("postIPA.loanAccAgrAmt", updateRevisedEmiRequest.getLoanAccAgrAmt());
            update.set("postIPA.advanceEmi", updateRevisedEmiRequest.getAdvanceEmi());
            update.set("postIPA.loanDisbursalAmt", updateRevisedEmiRequest.getLoanDisbursalAmt());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, PostIpaRequest.class);

            logger.info("Write Result:->", writeResult);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while updating revisedEmi amount in postIpa collection with probable cause [{}]", e.getMessage());
            return false;
            //throw new SystemException(String.format("error occured while updating revisedEmi amount in postIpa collection with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean isApplicationStatusCancelled(String refID, String institutionId) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID).
                    and("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationRequest.currentStageId").is(GNGWorkflowConstant.CNCLD.toFaceValue())
                    .and("applicationStatus").is(GNGWorkflowConstant.CANCELLED.toFaceValue()));

            return mongoTemplate.exists(query, GoNoGoCustomerApplication.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while checking Cancelled application with probable cause [{}]", e.getMessage());
            //throw new SystemException(String.format("error occured while checking Cancelled application with probable cause [{%s}]", e.getMessage()));
            return false;
        }

    }

    @Override
    public String getApplicationStage(String institutionId, String refID) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID).
                    and("applicationRequest.header.institutionId").is(institutionId));
            query.fields().include("applicationRequest.currentStageId");

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
            String applicationStage = null;
            if (null != goNoGoCustomerApplication && null != goNoGoCustomerApplication.getApplicationRequest() &&
                    null != goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId()) {
                applicationStage = goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId();

            }
            return applicationStage;
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while getting application stage with probable cause [{}]", e.getMessage());
            //throw new SystemException(String.format("error occured while getting application stage with probable cause [{%s}]", e.getMessage()));
            return  null;
        }
    }

    @Override
    public boolean updateGoNoGoCustomerApplication(ApplicationRequest applicationRequest)  {
        logger.debug("update GonogoCustomerApplication repo started");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("applicationRequest", applicationRequest);
            update.set(GoNoGoCustomerApplicationKeyConstants.LASTUPDATEDDATE, new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.debug("update gonogoCustomerApplication compete for refid {} with query {}",
                    applicationRequest.getRefID(), query.toString());
            return writeResult.isUpdateOfExisting();
        } catch (Exception e) {

            logger.error("Error occurred while update gonogoCustomerApplication " +
                    " with probable cause [{}] ", e.getMessage());
                return false;
            //throw new Exception(e.getMessage());
        }
    }

    @Override
    public boolean checkGeoLimitApplicableForBranch(GeoLimitRequest geoLimitRequest, String branchId) {
        logger.debug("inside geo limit applicable for branch repo");
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true).
                    and("institutionId").is(geoLimitRequest.getHeader().getInstitutionId())
                    .and("branchCode").is(branchId)
                    .and("isGeoAreaActive").is(Status.YES.name()));

            logger.debug("geo limit applicable for branch Query :{}", query.toString());

            List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasters = mongoTemplate.find(query, NegativeAreaGeoLimitMaster.class);

            return CollectionUtils.isEmpty(negativeAreaGeoLimitMasters) || checkPinCodeInMasterList(negativeAreaGeoLimitMasters, geoLimitRequest.getPinCode());

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while checking geoLimit applicable for branch with probable cause [{}]", e.getMessage());
            return false;
            //throw new SystemException(String.format("error occured while checking geoLimit applicable for branch with probable cause [{%s}]", e.getMessage()));

        }
    }

    private boolean checkPinCodeInMasterList(List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasters, String pinCode) {
        return negativeAreaGeoLimitMasters.stream().anyMatch(negativeGeoMaster -> negativeGeoMaster.getPinCode().equals(pinCode));
    }

    @Override
    public String getBranchIdAgainstDealerId(String institutionId, String dealerId) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true).
                    and("institutionId").is(institutionId)
                    .and("dealerId").is(dealerId));

            DealerBranchMaster dealerBranchMaster = mongoTemplate.findOne(query, DealerBranchMaster.class);


            if (null != dealerBranchMaster && StringUtils.isNotBlank(dealerBranchMaster.getBranchId())) {
                return dealerBranchMaster.getBranchId();
            } else {
                return null;
            }

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while getting branchId  with probable cause [{}]", e.getMessage());
            return null;
            //throw new SystemException(String.format("error occured while getting branchId  with probable cause  [{%s}]", e.getMessage()));

        }
    }

    @Override
    public boolean checkGeoLimitIsApplicableForBranch(String institutionId, String branchId, long pincode) {
        logger.debug("inside geo limit applicable for branch repo");
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true).
                    and("institutionId").is(institutionId)
                    .and("branchCode").is(branchId)
                    .and("isGeoAreaActive").is(Status.YES.name()));

            logger.debug("geo limit applicable for branch Query :{}", query.toString());

            List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasters = mongoTemplate.find(query, NegativeAreaGeoLimitMaster.class);

            return !CollectionUtils.isEmpty(negativeAreaGeoLimitMasters) && checkPinCodeInMasterList(negativeAreaGeoLimitMasters, String.valueOf(pincode));

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("error occured while checking geoLimit applicable for branch with probable cause [{}]", e.getMessage());
            //throw new SystemException(String.format("error occured while checking geoLimit applicable for branch with probable cause [{%s}]", e.getMessage()));
            return false;
        }
    }

    @Override
    public boolean saveCamDetails(CamDetailsRequest camDetailsRequest, String camType) {
        return saveCamDetails(camDetailsRequest.getRefId(), camDetailsRequest.getHeader().getInstitutionId(),
                                camDetailsRequest.getCamDetails(), camType);
    }

    @Override
    public boolean saveCamDetails(String refId, String institutionId, CamDetails camDetails, String camType) {
        logger.info("Inside saveCamDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));
            Update update = new Update();
            //update.set("refId", camDetailsRequest.getRefId());
            update.set("remarks", camDetails.getRemarks());
            //update.set("institutionId", camDetailsRequest.getHeader().getInstitutionId());
            //saving only specific cam type's details.
            switch (camType) {
                case EndPointReferrer.CAM_RTR_DETAILS:
                    update.set("rtrDetailList", camDetails.getRtrDetailList());
                    break;
                case EndPointReferrer.CAM_BANKING_DETAILS:
                    update.set("bankingStatements", camDetails.getBankingStatements());
                    update.set("sbfc",camDetails.isSbfc());
                    update.set("nonBusinessBnkStmtSummary",camDetails.getNonBusinessBnkStmtSummary());
                    update.set("netBusinessBnkStmtSummary",camDetails.getNetBusinessBnkStmtSummary());
                    update.set("totalAbb",camDetails.getTotalAbb());
                    update.set("avgCreditSummation",camDetails.getAvgCreditSummation());
                    update.set("totalNoOfTransactionSummary",camDetails.getTotalNoOfTransactionSummary());
                    update.set("i_W_ReturnsSummary",camDetails.getI_W_ReturnsSummary());
                    update.set("totalCreditSummation",camDetails.getTotalCreditSummation());
                    break;
                case EndPointReferrer.CAM_PL_BS_DETAILS:
                    update.set("plAndBSAnalysis", camDetails.getPlAndBSAnalysis());
                    break;
                case EndPointReferrer.CAM_SUMMARY:
                    update.set("summary", camDetails.getSummary());
                    break;
                case EndPointReferrer.CAM_GST_DETAILS:
                    update.set("gstDetails", camDetails.getGstDetails());
                    break;
                case EndPointReferrer.ALL_CAM_DETAILS:
                    update.set("summary", camDetails.getSummary());
                    break;
                default:
                    logger.warn("{} is incorrect", camType);
                    break;
            }
            mongoTemplate.upsert(query, update, CamDetails.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving camDetails " + e.getMessage());
            return false;
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving camDetails {%s}", e.getMessage()));
        }
    }

    @Override
    public CamDetails fetchCamDetails(CamDetailsRequest camDetailsRequest) {
        return fetchCamDetails(camDetailsRequest.getRefId(), camDetailsRequest.getHeader().getInstitutionId(),
                camDetailsRequest.getCamType());
    }

    @Override
    public CamDetails fetchCamDetails(String refId, String institutionId, String camType) {
        logger.info("Inside fetchCamDetails()");
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            if (StringUtils.isNotEmpty(camType)) {
                switch (camType) {
                    case EndPointReferrer.CAM_RTR_DETAILS:
                        query.fields().include("rtrDetailList");
                        break;
                    case EndPointReferrer.CAM_PL_BS_DETAILS:
                        query.fields().include("plAndBSAnalysis");
                        break;
                    case EndPointReferrer.CAM_BANKING_DETAILS:
                        query.fields().include("bankingStatements");
                        query.fields().include("sbfc");
                        query.fields().include("nonBusinessBnkStmtSummary");
                        query.fields().include("netBusinessBnkStmtSummary");
                        query.fields().include("totalAbb");
                        query.fields().include("avgCreditSummation");
                        query.fields().include("totalNoOfTransactionSummary");
                        query.fields().include("i_W_ReturnsSummary");
                        query.fields().include("totalCreditSummation");
                        break;
                    case EndPointReferrer.CAM_SUMMARY:
                        query.fields().include("summary");
                        break;
                    case EndPointReferrer.CAM_GST_DETAILS:
                        query.fields().include("gstDetails");
                        break;
                    case EndPointReferrer.ALL_CAM_DETAILS:
                        query.fields().include("plAndBSAnalysis");
                        query.fields().include("rtrDetailList");
                        query.fields().include("bankingStatements");
                        query.fields().include("sbfc");
                        query.fields().include("nonBusinessBnkStmtSummary");
                        query.fields().include("netBusinessBnkStmtSummary");
                        query.fields().include("totalAbb");
                        query.fields().include("avgCreditSummation");
                        query.fields().include("totalNoOfTransactionSummary");
                        query.fields().include("i_W_ReturnsSummary");
                        query.fields().include("disbBankName");
                        query.fields().include("summary");
                        query.fields().include("modDetailsList");
                        query.fields().include("gstDetails");
                        query.fields().include("totalCreditSummation");
                        break;
                }
                query.fields().include("refId");
                query.fields().include("remarks");
            }
            return mongoTemplate.findOne(query, CamDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching CamDetails with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching CamDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveVerificationDetails(VerificationRequest verificationRequest, String verificationType,
                                           Map<String, Set<String>> verificationStatusDealersMap) {
        return saveVerificationDetails(verificationRequest.getRefId(), verificationRequest.getHeader(),
                                        verificationType, verificationRequest.getVerificationDetails(),
                                        verificationStatusDealersMap);
    }

    @Override
    public boolean saveItrVerificationDetails(String refId, String institutionId, List<ItrVerification> itrVerificationList) {
        logger.info("Inside saveItrVerificationDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));
            Update update = new Update();
            update.set("itrVerification", itrVerificationList);
            mongoTemplate.upsert(query, update, VerificationDetails.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving VerificationDetails " + e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("Exception caught while saving VerificationDetails {%s}", e.getMessage()));
        }
    }


    @Override
    public boolean saveVerificationDetails(String refId, Header header, String verificationType,
                                           VerificationDetails verificationDetails,
                                           Map<String, Set<String>> verificationStatusDealersMap, ApplicationRequest applicationRequest) {
        logger.info("Inside saveVerificationDetails() for refId: {}",refId);
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(refId).and("institutionId").is(header.getInstitutionId());
            query.addCriteria(criteria);
            VerificationDetails dbVDObject = mongoTemplate.findOne(query, VerificationDetails.class);
            query = new Query();

            Update update = new Update();
            update.set("remarks", verificationDetails.getRemarks());

            String loggedInRole = header.getLoggedInUserRole();
            boolean isCredit = false;
            if(Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)) {
                if(header.getCredit() != null) {
                    isCredit = header.getCredit();
                }
            }
            boolean isVerifier = Cache.VERIFICATION_ROLE_URL_MAP.containsKey(loggedInRole);
            String statusVerified = ThirdPartyVerification.Status.VERIFIED.name();
            String statusSubmitted = ThirdPartyVerification.Status.SUBMITTED.name();
            String agencyCode = header.getDealerId();
            if(applicationRequest != null && verificationDetails!= null){
                validRvOvList(verificationDetails, dbVDObject, applicationRequest);
            }

            switch (verificationType)
            {
                case EndPointReferrer.OFFICE_VERIFICATION :

                    List<PropertyVerification> dbObjectList = (dbVDObject == null)? new ArrayList<>() : dbVDObject.getOfficeVerification();
                    List<PropertyVerification> reqObjectList = verificationDetails.getOfficeVerification();

                    List<PropertyVerification> officeVerificationList = getValidPropertyVerificationListToUpdate(dbVDObject, dbObjectList,
                            reqObjectList, isVerifier, statusVerified, statusSubmitted, agencyCode, loggedInRole, verificationStatusDealersMap, isCredit);
                    update.set("officeVerification", officeVerificationList);
                    break;

                case EndPointReferrer.RESIDENCE_VERIFICATION :

                    List<PropertyVerification> dbObjectListRV = (dbVDObject == null)? new ArrayList<>() : dbVDObject.getResidenceVerification();
                    List<PropertyVerification> reqObjectListRV = verificationDetails.getResidenceVerification();

                    List<PropertyVerification> residenceVerificationList = getValidPropertyVerificationListToUpdate(dbVDObject, dbObjectListRV,
                            reqObjectListRV, isVerifier, statusVerified, statusSubmitted, agencyCode, loggedInRole, verificationStatusDealersMap, isCredit);
                    update.set("residenceVerification", residenceVerificationList);

                    break;
                case EndPointReferrer.REFERENCE_DETAILS :
                    update.set("referenceDetails", verificationDetails.getReferenceDetails());
                    break;
                case EndPointReferrer.BANK_VERIFICATION :

                    List<BankingVerification> bankingVerificationList = getValidBankingVerificationListToUpdate(verificationDetails,
                            dbVDObject, isVerifier, statusVerified, statusSubmitted, agencyCode, loggedInRole, verificationStatusDealersMap, isCredit);
                    update.set("bankingVerification", bankingVerificationList);
                    break;

                case EndPointReferrer.ITR_VERIFICATION :

                    List<ItrVerification> itrVerificationList = getValidItrVerificationListToUpdate(verificationDetails,
                            dbVDObject, isVerifier, statusVerified, statusSubmitted, agencyCode, loggedInRole, verificationStatusDealersMap, isCredit);
                    update.set("itrVerification", itrVerificationList);
                    break;

                default:
                    logger.warn("{} is incorrect",verificationType);
                    break;
            }
            query.addCriteria(criteria);
            logger.debug("update query for saveVerificationDetails() is {}",update);
            mongoTemplate.upsert(query, update, VerificationDetails.class);
            logger.debug("{} verification details updated successfully for refId: {}",verificationType,refId);
        }catch(Exception e){
            logger.error("Exception caught while saving VerificationDetails " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving VerificationDetails {%s}",e.getMessage()));
        }

        return true;
    }

    @Override
    public boolean saveVerificationDetails(String refId, Header header, String verificationType,
                                           VerificationDetails verificationDetails,
                                           Map<String, Set<String>> verificationStatusDealersMap) {
        return saveVerificationDetails(refId,  header,  verificationType,
             verificationDetails,
            verificationStatusDealersMap, null);
    }

    private void validRvOvList(VerificationDetails varificationDetails, VerificationDetails verificationdetailsdb,ApplicationRequest applicationRequest) {
        logger.debug("Inside validRvOVList function for refId: {}",applicationRequest.getRefID());
        GoNoGoCustomerApplication goNoGoCustomerApplicationdb = getGoNoGoCustomerApplicationOnlyByRefId(applicationRequest.getRefID());

        /*Scenerion*/
        //if There is change in like Individual to NON-individual and vise versa we need to update rvOv

        Applicant applicant = applicationRequest.getRequest().getApplicant();
        List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant();
        List<CoApplicant> coApplicantsdb = goNoGoCustomerApplicationdb.getApplicationRequest().getRequest().getCoApplicant();
        Applicant applicantdb = goNoGoCustomerApplicationdb.getApplicationRequest().getRequest().getApplicant();

        validDataForRvOv(applicant, applicantdb, verificationdetailsdb);
        /* The size of coapplicantys from request and that from db is always same since when a co-app is deleted , immediately it is saved. */
        if( org.apache.commons.collections.CollectionUtils.isNotEmpty(coApplicants) && org.apache.commons.collections.CollectionUtils.isNotEmpty(coApplicantsdb)) {
            CoApplicant coappDB = null;
            for( CoApplicant coApplicant : coApplicants ) {
                coappDB = coApplicantsdb.stream().filter(codb -> StringUtils.equalsIgnoreCase(codb.getApplicantId(), coApplicant.getApplicantId()))
                        .findFirst().orElse(null);
                if( coappDB != null) {
                    validDataForRvOv(coApplicant, coappDB, verificationdetailsdb);
                    logger.debug("Inside validRvOVList:- validDataForRvOv call finished");
                }
            }
        }
        logger.debug("Finished validRvOvList execution for refId: {}",applicationRequest.getRefID());
    }

    private void validDataForRvOv(Applicant applicant, Applicant applicantdb, VerificationDetails verificationdetailsdb) {
        logger.debug("Inside validDataForRvOv function for refId: {}",verificationdetailsdb != null ? verificationdetailsdb.getRefId() : " ");
        String applicantId = applicant.getApplicantId();

        ApplicantType dbAppType = GngUtils.isIndividual(applicantdb.getApplicantType()) ? ApplicantType.INDIVIDUAL : ApplicantType.CORPORATE.NON_INDIVIDUAL;
        ApplicantType newAppType = GngUtils.isIndividual(applicant.getApplicantType()) ? ApplicantType.INDIVIDUAL : ApplicantType.CORPORATE.NON_INDIVIDUAL;
        if( dbAppType != newAppType ) {
            Iterator itr = null;
            if(newAppType == ApplicantType.NON_INDIVIDUAL &&  verificationdetailsdb != null){
                if(org.apache.commons.collections.CollectionUtils.isNotEmpty(verificationdetailsdb.getResidenceVerification())){
                     itr = verificationdetailsdb.getResidenceVerification().iterator();
                     PropertyVerification rv = null;

                     while(itr.hasNext()) {
                         rv = (PropertyVerification)itr.next();
                         if( StringUtils.equalsIgnoreCase(rv.getApplicantId(), applicantId) ){
                             itr.remove();
                             logger.debug("Inside validDataForRvOv function:- object is removed");
                         }
                     }
                 }
            }/*
            else {
                //Todo need busines clearification
                 itr = verificationdetailsdb.getOfficeVerification().iterator();
            }
*/

        }
        logger.debug("finished validDataForRvOv execution for refId: {}",verificationdetailsdb != null ? verificationdetailsdb.getRefId() : " ");
    }

    private List<PropertyVerification> getValidPropertyVerificationListToUpdate(VerificationDetails dbVDObject, List<PropertyVerification> dbObjectList,
                     List<PropertyVerification> reqObjectList, boolean isVerifier, String statusVerified, String statusSubmitted,
                     String agencyCode, String loggedInRole, Map<String, Set<String>> verificationStatusDealersMap,boolean isCredit) {
        List<PropertyVerification> propertyVerificationList;
        logger.debug("Inside getValidPropertyVerificationListToUpdate: 1");

        if(dbVDObject != null && dbObjectList != null) {
            logger.debug("Inside getValidPropertyVerificationListToUpdate: 2");
            propertyVerificationList = new ArrayList<>();
            GoNoGoCustomerApplication goNoGoCustomerApplication = getGoNoGoCustomerApplicationByRefId(dbVDObject.getRefId());
            /*Filter all objects which are not going to update
             (1.all verified status object 2.if user is verifier then all those records which are not assigned to his agency)*/
            dbObjectList.forEach(obj -> {
                if(StringUtils.equals(statusVerified, obj.getStatus()) || (isVerifier && !StringUtils.equals(obj.getAgencyCode(), agencyCode))
                        || (!isVerifier && StringUtils.equals(statusSubmitted, obj.getStatus()))){
                    propertyVerificationList.add(obj);
                }
            });

            /* CPA can re initiate the application for verification again if it is already verified*/
            if(!isVerifier) {
                logger.debug("Inside getValidPropertyVerificationListToUpdate: 3");
                List<PropertyVerification> reInitiatedList = reqObjectList.stream().filter(reqObj -> reqObj.isReinitiate()).collect(Collectors.toList());
                removeObjectsFromFinalListPV(propertyVerificationList, reInitiatedList);
            }
            // Remove below code to remove access of CPA & CREDIT roles to verify applications
            if(Roles.isCpaRole(loggedInRole) || Roles.isCreditRole(loggedInRole)
                    || StringUtils.equalsIgnoreCase(loggedInRole, Roles.Role.CREDIT.name()) ||
                    StringUtils.equalsIgnoreCase(loggedInRole, Roles.Role.BOPS.name()) || isCredit){
                logger.debug("Inside getValidPropertyVerificationListToUpdate: 4");
                List<PropertyVerification> verifiedObjList = reqObjectList.stream().filter(reqObj ->
                        StringUtils.equalsIgnoreCase(statusVerified, reqObj.getStatus())).collect(Collectors.toList());
                removeObjectsFromFinalListPV(propertyVerificationList, verifiedObjList);
            }

            /*get all objects from request which are not in above list*/
            reqObjectList.forEach(obj -> {
                boolean validAdd = true;
                for (PropertyVerification propertyVerification : propertyVerificationList) {
                    if(StringUtils.equals(obj.getApplicantId(), propertyVerification.getApplicantId())){
                        validAdd = false;
                        break;
                    }
                }
                if (obj.isReinitiate()) {
                    logger.debug("Inside getValidPropertyVerificationListToUpdate: 5");
                    obj = copyAddressFromRequest(obj, goNoGoCustomerApplication.getApplicationRequest().getRequest());
                }
                logger.debug("Verification Valid Add {}", validAdd);
                if(validAdd){
                    if(isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())
                            && obj.getVerificationTime() == null){
                        obj.setVerificationTime(new Date());
                        logger.debug("RV/OV Verification is verified for RefId {} by Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    } else if(!isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())){
                        logger.debug("RV/OV Verification is triggered for RefId {} to Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    }
                    propertyVerificationList.add(obj);
                }

            });

        }else {
            logger.debug("Inside getValidPropertyVerificationListToUpdate: 6");
            TreeSet<PropertyVerification> uniqueObjList = reqObjectList.stream().collect(Collectors.toCollection(
                    () -> new TreeSet<PropertyVerification>((obj1, obj2) -> {
                        if(obj1.getApplicantId().compareTo(obj2.getApplicantId()) == 0 ) {
                            return 0;
                        } else {
                            return 1;
                        }
                    })));
            propertyVerificationList = new ArrayList<>(uniqueObjList);
        }
        return propertyVerificationList;
    }



    private void removeObjectsFromFinalListPV(List<PropertyVerification> propertyVerificationList, List<PropertyVerification> reInitiatedList) {
        if(!CollectionUtils.isEmpty(reInitiatedList)) {
            reInitiatedList.forEach(obj -> {
                for (int i = 0; i < propertyVerificationList.size(); i++) {
                    if (StringUtils.equals(obj.getApplicantId(), propertyVerificationList.get(i).getApplicantId())) {
                        propertyVerificationList.remove(i);
                        break;
                    }
                }
            });
        }
    }

    private List<BankingVerification> getValidBankingVerificationListToUpdate(VerificationDetails verificationDetails, VerificationDetails dbVDObject,
                            boolean isVerifier, String statusVerified, String statusSubmitted, String agencyCode,
                            String loggedInRole, Map<String, Set<String>> verificationStatusDealersMap, boolean isCredit) {

        List<BankingVerification> bankingVerificationList;

        if(dbVDObject != null && dbVDObject.getBankingVerification() != null) {
            bankingVerificationList = new ArrayList<>();
            List<BankingVerification> dbObjectList = dbVDObject.getBankingVerification();
            List<BankingVerification> reqObjectList = verificationDetails.getBankingVerification();
            /*Filter all objects which are not going to update
             (1.all verified status object 2.if user is verifier then all those records which are not assigned to his agency)*/
            dbObjectList.forEach(obj -> {
                if(StringUtils.equals(statusVerified, obj.getStatus()) || (isVerifier && !StringUtils.equals(obj.getAgencyCode(), agencyCode))
                        || (!isVerifier && StringUtils.equals(statusSubmitted, obj.getStatus()))){
                    bankingVerificationList.add(obj);
                }
            });

            /* CPA can re initiate the application for verification again if it is already verified*/
            if(!isVerifier) {
                List<BankingVerification> reInitiatedList = reqObjectList.stream().filter(reqObj -> reqObj.isReinitiate()).collect(Collectors.toList());
                removeObjectsFromFinalListBank(bankingVerificationList, reInitiatedList);
            }

            // Remove below code to remove access of CPA & CREDIT roles to verify applications
            if(Roles.isCpaRole(loggedInRole) || Roles.isCreditRole(loggedInRole)
                    || StringUtils.equalsIgnoreCase(loggedInRole, Roles.Role.CREDIT.name()) || isCredit){
                List<BankingVerification> verifiedObjList = reqObjectList.stream().filter(reqObj ->
                        StringUtils.equalsIgnoreCase(statusVerified, reqObj.getStatus())).collect(Collectors.toList());
                removeObjectsFromFinalListBank(bankingVerificationList, verifiedObjList);
            }

            /*get all objects from request which are not in above list*/
            reqObjectList.forEach(obj -> {
                boolean validAdd = true;
                for (BankingVerification bankingVerification : bankingVerificationList) {
                    if(StringUtils.equals(obj.getApplicantId(), bankingVerification.getApplicantId())
                            && StringUtils.equals(obj.getAccountNumber(), bankingVerification.getAccountNumber())){
                        validAdd = false;
                        break;
                    }
                }
                logger.debug("Verification Valid Add {}", validAdd);
                if(validAdd){
                    if(isVerifier  && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())
                            && obj.getVerificationTime() == null){
                        obj.setVerificationTime(new Date());
                        logger.debug("Bank Verification is verified for RefId {} by Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    } else if(!isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())){
                        logger.debug("Bank Verification is triggered for RefId {} to Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    }
                    bankingVerificationList.add(obj);
                }
            });

        }else {
            List<BankingVerification> reqObjectList = verificationDetails.getBankingVerification();
            TreeSet<BankingVerification> uniqueObjList = reqObjectList.stream().collect(Collectors.toCollection(
                    () -> new TreeSet<BankingVerification>((obj1, obj2) -> {
                        if(obj1.getAccountNumber().compareTo(obj2.getAccountNumber()) == 0 &&
                                obj1.getApplicantId().compareTo(obj2.getApplicantId()) == 0 ) {
                            return 0;
                        } else {
                            return 1;
                        }
                    })));
            bankingVerificationList = new ArrayList<>(uniqueObjList);
        }
        return bankingVerificationList;
    }

    private void removeObjectsFromFinalListBank(List<BankingVerification> bankingVerificationList, List<BankingVerification> reInitiatedList) {
        if(!CollectionUtils.isEmpty(reInitiatedList)) {
            reInitiatedList.forEach(obj -> {
                for (int i = 0; i < bankingVerificationList.size(); i++) {
                    if (StringUtils.equals(obj.getApplicantId(), bankingVerificationList.get(i).getApplicantId())
                            && StringUtils.equals(obj.getAccountNumber(), bankingVerificationList.get(i).getAccountNumber())) {
                        bankingVerificationList.remove(i);
                        break;
                    }
                }
            });
        }
    }

    private List<ItrVerification> getValidItrVerificationListToUpdate(VerificationDetails verificationDetails, VerificationDetails dbVDObject,
                                  boolean isVerifier, String statusVerified, String statusSubmitted, String agencyCode,
                                  String loggedInRole, Map<String, Set<String>> verificationStatusDealersMap, boolean isCredit) {
        List<ItrVerification> itrVerificationList;
        if(dbVDObject != null && dbVDObject.getItrVerification() != null) {
            itrVerificationList = new ArrayList<>();
            List<ItrVerification> dbObjectList = dbVDObject.getItrVerification();
            List<ItrVerification> reqObjectList = verificationDetails.getItrVerification();
            /*Filter all objects which are not going to update
             (1.all verified status object 2.if user is verifier then all those records which are not assigned to his agency)*/
            dbObjectList.forEach(obj -> {
                if(StringUtils.equals(statusVerified, obj.getStatus()) || (isVerifier && !StringUtils.equals(obj.getAgencyCode(), agencyCode))
                        || (!isVerifier && StringUtils.equals(statusSubmitted, obj.getStatus()))){
                    itrVerificationList.add(obj);
                }
            });
            /* CPA can re initiate the application for verification again if it is already verified*/
            if(!isVerifier) {
                List<ItrVerification> reInitiatedList = reqObjectList.stream().filter(reqObj -> reqObj.isReinitiate()).collect(Collectors.toList());
                removeObjectsFromFinalListITR(itrVerificationList, reInitiatedList);
            }
            // Remove below code to remove access of CPA & CREDIT roles to verify applications
            if(Roles.isCpaRole(loggedInRole) || Roles.isCreditRole(loggedInRole)
                    || StringUtils.equalsIgnoreCase(loggedInRole, Roles.Role.CREDIT.name()) || isCredit){
                List<ItrVerification> verifiedObjList = reqObjectList.stream().filter(reqObj ->
                        StringUtils.equalsIgnoreCase(statusVerified, reqObj.getStatus())).collect(Collectors.toList());
                removeObjectsFromFinalListITR(itrVerificationList, verifiedObjList);
            }

            /*get all objects from request which are not in above list*/
            reqObjectList.forEach(obj -> {
                boolean validAdd = true;
                for (ItrVerification itrVerification : itrVerificationList) {
                    if(StringUtils.equals(obj.getApplicantId(), itrVerification.getApplicantId())
                            && StringUtils.equals(obj.getAcknowledgementNumber(), itrVerification.getAcknowledgementNumber())){
                        validAdd = false;
                        break;
                    }
                }
                logger.debug("Verification Valid Add {}", validAdd);
                if(validAdd){
                    if(isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())
                            && obj.getVerificationTime() == null){
                        obj.setVerificationTime(new Date());
                        logger.debug("ITR Verification is verified for RefId {} by Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    } else if(!isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())){
                        logger.debug("ITR Verification is triggered for RefId {} to Agency {}-{}", obj.getRefId(), obj.getAgencyCode(), obj.getAgencyName());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode() +"##"+ obj.getAgencyName());
                    }
                    itrVerificationList.add(obj);
                }
            });

        }else {
            List<ItrVerification> reqObjectList = verificationDetails.getItrVerification();
            TreeSet<ItrVerification> uniqueObjList = reqObjectList.stream().collect(Collectors.toCollection(
                    () -> new TreeSet<ItrVerification>((obj1, obj2) -> {
                        if(obj1.getAcknowledgementNumber().compareTo(obj2.getAcknowledgementNumber()) == 0 &&
                                obj1.getApplicantId().compareTo(obj2.getApplicantId()) == 0 ) {
                            return 0;
                        } else {
                            return 1;
                        }
                    })));
            itrVerificationList = new ArrayList<>(uniqueObjList);
        }
        return itrVerificationList;
    }

    private void removeObjectsFromFinalListITR(List<ItrVerification> itrVerificationList, List<ItrVerification> reInitiatedList) {
        if(!CollectionUtils.isEmpty(reInitiatedList)) {
            reInitiatedList.forEach(obj -> {
                for (int i = 0; i < itrVerificationList.size(); i++) {
                    if (StringUtils.equals(obj.getApplicantId(), itrVerificationList.get(i).getApplicantId())
                            && StringUtils.equals(obj.getAcknowledgementNumber(), itrVerificationList.get(i).getAcknowledgementNumber())) {
                        itrVerificationList.remove(i);
                        break;
                    }
                }
            });
        }
    }

    @Override
    public VerificationDetails fetchVerificationDetailsByRefId(VerificationRequest verificationRequest) {
        logger.info("Inside fetchVerificationDetailsByRefId() for refId: {}",verificationRequest.getRefId());
        try {
            Query query = new Query();
            Criteria criteria = getQueryCriteriaForVerificationDetails(verificationRequest);
            query.addCriteria(criteria);

            if (StringUtils.isNotEmpty(verificationRequest.getVerificationType())) {
                switch (verificationRequest.getVerificationType()) {
                    case EndPointReferrer.OFFICE_VERIFICATION:
                        query.fields().include("officeVerification");
                        break;
                    case EndPointReferrer.RESIDENCE_VERIFICATION:
                        query.fields().include("residenceVerification");
                        break;
                    case EndPointReferrer.REFERENCE_DETAILS:
                        query.fields().include("referenceDetails");
                        break;
                    case EndPointReferrer.BANK_VERIFICATION:
                        query.fields().include("bankingVerification");
                        break;
                    case EndPointReferrer.ITR_VERIFICATION:
                        query.fields().include("itrVerification");
                        break;
                }
                query.fields().include("refId");
                query.fields().include("remarks");
            }
            logger.debug("finished fetchVerificationDetailsByRefId() for refId: {}",verificationRequest.getRefId());
            return mongoTemplate.findOne(query, VerificationDetails.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching VerificationDetails with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching VerificationDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    private Criteria getQueryCriteriaForVerificationDetails(VerificationRequest verificationRequest) {

        Criteria criteria =  Criteria.where("_id").is(verificationRequest.getRefId())
                .and("institutionId").is(verificationRequest.getHeader().getInstitutionId());

        String role = verificationRequest.getHeader().getLoggedInUserRole();

        List<String> statusList = new ArrayList<>();
        statusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
        statusList.add(ThirdPartyVerification.Status.VERIFIED.name());
        MongoRepositoryUtil.createCriteriaForVerifierRole(verificationRequest.getHeader(), criteria, role, statusList);
        return criteria;
    }


    @Override
    public boolean savePropertyVisitDetails(PropertyVisitRequest propertyVisitRequest) {
        logger.info("Inside savePropertyVisitDetails for refId {}",propertyVisitRequest.getRefId());
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(propertyVisitRequest.getRefId())
                    .and("institutionId").is(propertyVisitRequest.getHeader().getInstitutionId()));
            Update update = new Update();

            /*update.set("applicantId", propertyVisitRequest.getPropertyVisit().getApplicantId());
            update.set("applicantName", propertyVisitRequest.getPropertyVisit().getApplicantName());*/
            update.set("propertyVisitDetailsList", propertyVisitRequest.getPropertyVisit().getPropertyVisitDetailsList());

            //for verifier
            update.set("agencyCode",propertyVisitRequest.getPropertyVisit().getAgencyCode());
            update.set("status",propertyVisitRequest.getPropertyVisit().getStatus());

            mongoTemplate.upsert(query, update, PropertyVisit.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving PropertyVisitDetails " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving PropertyVisitDetails {%s}", e.getMessage()));
        }

        return true;
    }

    @Override
    public PropertyVisit fetchPropertyVisitDetailsByRefId(PropertyVisitRequest propertyVisitRequest) {
        logger.info("Inside fetchPropertyVisitDetailsByRefId for refId {}", propertyVisitRequest.getRefId());
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(propertyVisitRequest.getRefId())
                    .and("institutionId").is(propertyVisitRequest.getHeader().getInstitutionId()));

            return mongoTemplate.findOne(query, PropertyVisit.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching PropertyVisit with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching PropertyVisit with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean savePersonalDiscussionDetails(ApplicationRequest applicationRequest) {
        logger.info("Inside savePersonalDiscussionDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("applicationRequest.request.personalDiscussion", applicationRequest.getRequest().getPersonalDiscussion());
            mongoTemplate.upsert(query, update, GoNoGoCustomerApplication.class);
        }catch(Exception e){
            logger.error("Exception caught while saving PersonalDiscussionDetails " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving PersonalDiscussionDetails {%s}",e.getMessage()));
        }

        return true;
    }

    @Override
    public Repayment fetchRepaymentDetails(RepaymentRequest repaymentRequest) {
        logger.info("Inside fetchRepaymentDetails");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(repaymentRequest.getRefId())
                    .and("institutionId").is(repaymentRequest.getHeader().getInstitutionId()));

            return mongoTemplate.findOne(query, Repayment.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching repaymentDetails with probable cause [{}] ", e.getMessage());

            return null;
            //throw new SystemException(String.format(" error occurred  while fetching repaymentDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveRepaymentDetails(Repayment repayment) {

        logger.debug("repayment Details repo started");
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(repayment, dbObject);
            mongoTemplate.execute(Repayment.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(repayment.getRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            logger.debug("repayment Details repo completed");
            return true;
        } catch (DataAccessException e) {
            logger.error("Error occurred while saving repayment details" +
                            "  with probable cause [{}] "
                    , e.getMessage());
            return false;
            //throw new SystemException(String.format("Error occurred while saving repayment details with probable cause [%s] ", e.getMessage()));
        }
    }

    @Override
    public  Repayment fetchRepaymentDetails(String refId , String institutionId){
        try{
            return mongoTemplate.findOne(new Query().addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId)),Repayment.class);
        }catch (DataAccessException e) {
            logger.error("Error occurred while saving repayment details" +
                            "  with probable cause [{}] "
                    , e.getMessage());
            return null;
            //throw new SystemException(String.format("Error occurred while saving repayment details with probable cause [%s] ", e.getMessage()));
        }

    }


   /* @Override
    public boolean saveLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest) {
        logger.info("Inside savePropertyVisitDetails()");
        try {
            Criteria criteria = Criteria.where("_id").is(legalVerificationRequest.getRefId())
                    .and("institutionId").is(legalVerificationRequest.getHeader().getInstitutionId());

            // if logged in user is verifier then do not allow to update pending or verified records
            if(Cache.VERIFICATION_ROLE_URL_MAP.containsKey(legalVerificationRequest.getHeader().getLoggedInUserRole())){
                // verifier will update records on in submitted status
                List<String> statusList = new ArrayList<>();
                statusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
                criteria.and("status").in(statusList);
            }*//*else {
                // if CPA then don't allow to update submitted applications
                criteria.and("status").ne(ThirdPartyVerification.Status.SUBMITTED.name());
            }*//*
            // Remove above commented else block to remove access of CPA & Credit roles of verification
            Query query = new Query();
            query.addCriteria(criteria);

            // Check If user is verifier then update its verification submission time
            if (Cache.VERIFICATION_ROLE_URL_MAP.containsKey(legalVerificationRequest.getHeader().getLoggedInUserRole())) {
                if (StringUtils.equalsIgnoreCase(legalVerificationRequest.getLegalVerification().getAgencyCode(),
                        legalVerificationRequest.getHeader().getDealerId())) {
                    if(StringUtils.equals(legalVerificationRequest.getLegalVerification().getStatus(),ThirdPartyVerification.Status.VERIFIED.name()))
                        legalVerificationRequest.getLegalVerification().setVerificationTime(new Date());
                }
            }

            Update update = new Update();
            //update.set("institutionId", legalVerificationRequest.getHeader().getInstitutionId());
            update.set("applicantId", legalVerificationRequest.getLegalVerification().getApplicantId());
            update.set("applicantName", legalVerificationRequest.getLegalVerification().getApplicantName());
            update.set("natureOfProp", legalVerificationRequest.getLegalVerification().getNatureOfProp());
            update.set("agencyCode", legalVerificationRequest.getLegalVerification().getAgencyCode());
            update.set("status", legalVerificationRequest.getLegalVerification().getStatus());
            update.set("legalVerificationInput", legalVerificationRequest.getLegalVerification().getLegalVerificationInput());
            update.set("legalVerificationOutput", legalVerificationRequest.getLegalVerification().getLegalVerificationOutput());
            mongoTemplate.upsert(query, update, LegalVerification.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving save LegalVerification Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save LegalVerification Details {%s}", e.getMessage()));
        }

        return true;
    }*/

    @Override
    public void updateLegalVerificationMultiDetails(LegalVerificationRequest legalVerificationRequest) {
        logger.info("Inside saveLegalVerificationMultiDetails()");
         Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(legalVerificationRequest.getRefId())
                    .and("institutionId").is(legalVerificationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("legalVerificationDetailsList",
                    legalVerificationRequest.getLegalVerification().getLegalVerificationDetailsList());
            mongoTemplate.upsert(query, update, LegalVerification.class);
    }

    @Override
    public boolean saveLegalVerificationMultiDetails(LegalVerificationRequest legalVerificationRequest,
                                                     Map<String, Set<String>> verificationStatusDealersMap) {
        logger.info("Inside saveLegalVerificationMultiDetails()");
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(legalVerificationRequest.getRefId())
                    .and("institutionId").is(legalVerificationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            LegalVerification dbVDObject = mongoTemplate.findOne(query, LegalVerification.class);
            query = new Query();
            Update update = new Update();
            String loggedInRole = legalVerificationRequest.getHeader().getLoggedInUserRole();
            boolean isVerifier = (Cache.VERIFICATION_ROLE_URL_MAP.containsKey(loggedInRole));
            String statusVerified = ThirdPartyVerification.Status.VERIFIED.name();
            String statusSubmitted = ThirdPartyVerification.Status.SUBMITTED.name();

                List<LegalVerificationDetails> legalVerificationDetailsList = getValidLegalVerificationDetailsToUpdate(legalVerificationRequest.getLegalVerification(), dbVDObject, isVerifier,
                            statusVerified, statusSubmitted, legalVerificationRequest.getHeader().getDealerId(), loggedInRole, verificationStatusDealersMap);

                if (legalVerificationRequest.getLegalVerification() != null)
                update.set("legalVerificationDetailsList", legalVerificationRequest.getLegalVerification().getLegalVerificationDetailsList());

            query.addCriteria(criteria);
            mongoTemplate.upsert(query, update, LegalVerification.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving legalVerification Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving legalVerification Details {%s}", e.getMessage()));
        }

        return true;
    }

    //for legalverification for multiple collaterals
    private List<LegalVerificationDetails> getValidLegalVerificationDetailsToUpdate(LegalVerification verificationDetails, LegalVerification dbVDObject,
                boolean isVerifier, String statusVerified, String statusSubmitted, String agencyCode, String loggedInRole, Map<String, Set<String>> verificationStatusDealersMap) {
        List<LegalVerificationDetails> legalVerificationDetailsList;
        if(dbVDObject != null && dbVDObject.getLegalVerificationDetailsList() != null) {
            legalVerificationDetailsList = new ArrayList<>();
            List<LegalVerificationDetails> dbObjectList = dbVDObject.getLegalVerificationDetailsList();
            List<LegalVerificationDetails> reqObjectList = verificationDetails.getLegalVerificationDetailsList();
            GoNoGoCustomerApplication goNoGoCustomerApplication = getGoNoGoCustomerApplicationByRefId(dbVDObject.getRefId());
            /*Filter all objects which are not going to update
             (1.all verified status object 2.if user is verifier then all those records which are not assigned to his agency)*/
            dbObjectList.forEach(obj -> {
                if(StringUtils.equals(statusVerified, obj.getStatus()) || (isVerifier && !StringUtils.equals(obj.getAgencyCode(), agencyCode))
                        || (!isVerifier && StringUtils.equals(statusSubmitted, obj.getStatus()))){
                    legalVerificationDetailsList.add(obj);
                }
            });

            /*get all objects from request which are not in above list*/
            reqObjectList.forEach(obj -> {
                boolean validAdd = true;
                for (LegalVerificationDetails legalVerificationDetails : legalVerificationDetailsList) {
                    if(StringUtils.equals(obj.getCollateralId(), legalVerificationDetails.getCollateralId()) &&
                            StringUtils.equals(obj.getAgencyCode(), legalVerificationDetails.getAgencyCode())){
                        validAdd = false;
                        break;
                    }
                }
                if(obj.isReinitiate()){
                    obj = copyCollateralData(obj,goNoGoCustomerApplication.getApplicationRequest().getRequest());
                }
                if(validAdd){
                    if(isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())
                            && obj.getVerificationTime() == null){
                        obj.setVerificationTime(new Date());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode());
                    } else if(!isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())){
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode());
                    }
                    legalVerificationDetailsList.add(obj);
                }
            });

        }else {
            if(verificationDetails != null) {
            legalVerificationDetailsList = verificationDetails.getLegalVerificationDetailsList();
            }else{
                legalVerificationDetailsList = null;
            }
        }
        return legalVerificationDetailsList;
    }

    @Override
    public LegalVerification fetchLegalVerificatioDetailsByRefId(LegalVerificationRequest legalVerificationRequest) {
        try {

            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(legalVerificationRequest.getRefId())
                    .and("institutionId").is(legalVerificationRequest.getHeader().getInstitutionId());

            List<String> statusList = new ArrayList<>();
            statusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
            statusList.add(ThirdPartyVerification.Status.VERIFIED.name());
            MongoRepositoryUtil.createCriteriaForVerifierRole(legalVerificationRequest.getHeader(), criteria,
                    legalVerificationRequest.getHeader().getLoggedInUserRole(), statusList);

            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, LegalVerification.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching LegalVerificatio with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching LegalVerificatio with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public LegalVerification fetchLegalVerificatioDetails(String refId, String instId) {
        try {

            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId);

            query.addCriteria(criteria);
            return  mongoTemplate.findOne(query, LegalVerification.class);
            /*LegalVerification data =  mongoTemplate.findOne(query, LegalVerification.class);
            migrateToNewLegalVerification(refId, instId, data);
            return data;*/
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching LegalVerification with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching LegalVerificatio with probable cause [{%s}]", e.getMessage()));
        }
    }

    /*private void migrateToNewLegalVerification(String refId, String instId, LegalVerification legalVerification) {
        if(legalVerification !=null && legalVerification.getLegalVerificationDetailsList()==null) {
            LegalVerificationDetails legalVerificatioDetailsByRefId = fetchOldLegalVerificatioDetailsByRefId(
                    refId,  instId);
            if (legalVerificatioDetailsByRefId != null) {
                legalVerificatioDetailsByRefId.setCollateralId("0");
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                legalVerificationDetailsList.add(legalVerificatioDetailsByRefId);
                legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            }
        }
    }*/

    @Override
    public LegalVerificationDetails fetchOldLegalVerificatioDetailsByRefId(LegalVerificationRequest legalVerificationRequest) {

        try {

            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(legalVerificationRequest.getRefId())
                    .and("institutionId").is(legalVerificationRequest.getHeader().getInstitutionId());

            List<String> statusList = new ArrayList<>();
            statusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
            statusList.add(ThirdPartyVerification.Status.VERIFIED.name());
            MongoRepositoryUtil.createCriteriaForVerifierRole(legalVerificationRequest.getHeader(), criteria,
                    legalVerificationRequest.getHeader().getLoggedInUserRole(), statusList);

            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, LegalVerificationDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching LegalVerificatio with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching LegalVerificatio with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean updateExternalServiceFlagForLMS(ApplicationRequest applicationRequest, String name) {
        logger.info("Inside updateExternalServiceFlagForLMS()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            if(StringUtils.equals(name, GNGWorkflowConstant.LMS_API_CALL.name())) {
                update.set("applicationRequest.externalServiceCalls.pushToLms", true);
                update.set("applicationRequest.externalServiceCalls.lmsMsg",applicationRequest.getExternalServiceCalls().getLmsMsg());
                mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            }
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateExternalServiceFlagForLMS with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateExternalServiceFlagForLMS with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    /**
     * Make AllocationStatus of all applications to null, having userId as AllocationStatus.operator
     *
     * @param userId
     * @param institutionId
     */
    @Override
    public void releaseAllocatedApplications(String userId, String institutionId) {
        Query query = new Query();
        // Remove all application which is allocated by current user except Manually locked cases
        query.addCriteria(Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                .and("allocationInfo.operator").is(userId)
                .and("allocationInfo.locked").is(false));

        Update update = new Update();
        update.set("allocationInfo", null);
        mongoTemplate.updateMulti(query, update, GoNoGoCustomerApplication.class);
    }

    @Override
    public void changeApplicationLockStatus(String userId, String institutionId, String refId, boolean isLocked){

        Query query = new Query();
        // Remove all application which is allocated by current user except Manually locked cases
        query.addCriteria(Criteria.where("_id").is(refId)
                .and("applicationRequest.header.institutionId").is(institutionId)
                .and("allocationInfo.operator").is(userId));

        Update update = new Update();
        update.set("allocationInfo.locked", isLocked);
        mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean updateGoNoGoCustomerApplication(String refId, AllocationInfo allocationInfo, String institutionId)  {
        logger.debug("update GonogoCustomerApplication repo started");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId)
                    .and("locked").ne(true));
            Update update = new Update();
            update.set("allocationInfo", allocationInfo);
            update.set(GoNoGoCustomerApplicationKeyConstants.LASTUPDATEDDATE, new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.debug("update gonogoCustomerApplication compete for refid {} with query {}",
                    refId, query.toString());
            return writeResult.isUpdateOfExisting();
        } catch (Exception e) {

            logger.error("Error occurred while update gonogoCustomerApplication " +
                    " with probable cause [{}] ", e.getMessage());
            return false;
            //throw new Exception(e.getMessage());
        }
    }

    @Override
    public boolean saveEligibilityDetails(EligibilityRequest eligibilityRequest) {
              EligibilityDetails eligibilityDetails=eligibilityRequest.getEligibilityDetails();
        eligibilityDetails.setRefId(eligibilityRequest.getRefId());
        eligibilityDetails.setInstitutionId(eligibilityRequest.getHeader().getInstitutionId());
        logger.debug("eligibility Details repo started");
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(eligibilityDetails, dbObject);
            mongoTemplate.execute(EligibilityDetails.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(eligibilityDetails.getRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });
            logger.debug("eligibility Details repo started");
            return true;
        } catch (DataAccessException e) {
            logger.error("Error occurred while saving eligibility details" + "  with probable cause [{}] ", e.getMessage());
            return false;
            //throw new SystemException(String.format("Error occurred while saving eligibility details with probable cause [%s] ", e.getMessage()));
        }
    }

    @Override
    public boolean updateApplicationForAllocationInfo(String refId, AllocationInfo allocationInfo, String institutionId)  {
        logger.debug("update GonogoCustomerApplication repo started");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId));
            Update update = new Update();
            update.set("allocationInfo", allocationInfo);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.debug("update gonogoCustomerApplication compete for refid {} with query {}",
                    refId, query.toString());
            return writeResult.isUpdateOfExisting();
        } catch (Exception e) {

            logger.error("Error occurred while update gonogoCustomerApplication " +
                    " with probable cause [{}] ", e.getMessage());
            return false;
            //throw new Exception(e.getMessage());
        }
    }

    @Override
    public boolean saveListOfDocs(ListOfDocsRequest listOfDocsRequest) {
        logger.info("Inside saveListOfDocs()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(listOfDocsRequest.getRefId())
                    .and("institutionId").is(listOfDocsRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("institutionId", listOfDocsRequest.getHeader().getInstitutionId());
            update.set("applicantId", listOfDocsRequest.getListOfDocs().getApplicantId());
            update.set("applicantName", listOfDocsRequest.getListOfDocs().getApplicantName());
            update.set("docDetails", listOfDocsRequest.getListOfDocs().getDocDetails());
            update.set("disbursedDate", listOfDocsRequest.getListOfDocs().getDisbursedDate());
            mongoTemplate.upsert(query, update, ListOfDocs.class);

        } catch (Exception e) {
            logger.error("Exception caught while saving ListOfDocs " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save ListOfDocs {%s}", e.getMessage()));
        }

        return true;
    }

    @Override
    public ListOfDocs fetchListOfDocs(ListOfDocsRequest listOfDocsRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(listOfDocsRequest.getRefId())
                    .and("institutionId").is(listOfDocsRequest.getHeader().getInstitutionId()));

            return mongoTemplate.findOne(query, ListOfDocs.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching ListOfDocs with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching ListOfDocs with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveCustomerCreditDocs(CustomerCreditRequest customerCreditRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(customerCreditRequest.getRefId())
                    .and("institutionId").is(customerCreditRequest.getHeader().getInstitutionId()));

            Update update = new Update();
            update.set("institutionId", customerCreditRequest.getHeader().getInstitutionId());
            update.set("applicantId", customerCreditRequest.getCustomerCreditDocs().getApplicantId());
            update.set("applicantName", customerCreditRequest.getCustomerCreditDocs().getApplicantName());
            update.set("customerCreditDetails", customerCreditRequest.getCustomerCreditDocs().getCustomerCreditDetails());
            mongoTemplate.upsert(query, update, CustomerCreditDocs.class);

        } catch (DataAccessException e) {
            logger.error("Exception caught while saving save customerCreditDocs " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save customerCreditDocs {%s}", e.getMessage()));
        }
        return true;
    }

    @Override
    public CustomerCreditDocs fetchCustomerCreditDocs(CustomerCreditRequest customerCreditRequest) {
        return fetchCustomerCreditDocs(customerCreditRequest.getRefId(), customerCreditRequest.getHeader().getInstitutionId());
    }

    @Override
    public CustomerCreditDocs fetchCustomerCreditDocs(String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));
            return mongoTemplate.findOne(query, CustomerCreditDocs.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching customerCreditDocs with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching customerCreditDocs with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveLoanChargesDetails(LoanChargesRequest loanChargesRequest) {
        return saveLoanChargesDetails(loanChargesRequest.getRefId(), loanChargesRequest.getHeader().getInstitutionId(),
                loanChargesRequest.getLoanCharges());
    }
    @Override
    public boolean saveLoanChargesDetails (String refId, String institutionId,  LoanCharges loanCharges){
        logger.info("Inside saveLoanChargesDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));
            Update update = new Update();
            /*update.set("refId", loanChargesRequest.getRefId());
            update.set("institutionId", loanChargesRequest.getHeader().getInstitutionId());
            */
            update.set("loanDetails", loanCharges.getLoanDetails());
            update.set("tenureDetails", loanCharges.getTenureDetails());
            update.set("interestDetails", loanCharges.getInterestDetails());
            update.set("typeOfROI", loanCharges.getTypeOfROI());
            update.set("emiStartDate", loanCharges.getEmiStartDate());
            update.set("emiEndDate", loanCharges.getEmiEndDate());
            update.set("imdCollectedAmt", loanCharges.getImdCollectedAmt());
            update.set("proFees", loanCharges.getProFees());
            update.set("proFeesAmt", loanCharges.getProFeesAmt());
            update.set("gst", loanCharges.getGst());
            update.set("gstAmt", loanCharges.getGstAmt());
            update.set("totalFees", loanCharges.getTotalFees());
            update.set("proDeductAmt", loanCharges.getProDeductAmt());
            update.set("cersaiFees", loanCharges.getCersaiFees());
            update.set("preEMIDeductAmt", loanCharges.getPreEMIDeductAmt());
            update.set("insuranceCoName", loanCharges.getInsuranceCoName());
            update.set("insuranceAmt", loanCharges.getInsuranceAmt());
            update.set("netDisbAmt", loanCharges.getNetDisbAmt());
            update.set("finalEMIAmt", loanCharges.getFinalEMIAmt());
            update.set("advanceEMI", loanCharges.getAdvanceEMI());
            update.set("disbursedDate", loanCharges.getDisbursedDate());
            update.set("remark", loanCharges.getRemark());
            update.set("insurenceCatList", loanCharges.getInsurenceCatList());
            update.set("creditVidyaCharge", loanCharges.getCreditVidyaCharge());
            update.set("bopsReceivedDt", loanCharges.getBopsReceivedDt());
            update.set("fileReceivedDt", loanCharges.getFileReceivedDt());
            update.set("hOpsFileRecieveDt", loanCharges.getHOpsFileRecieveDt());
            update.set("isCashOut", loanCharges.isCashOut());
            update.set("dueDate", loanCharges.getDueDate());
            update.set("cersaiFeeWithGST",loanCharges.getCersaiFeeWithGST());
            update.set("advEMIWithGST", loanCharges.getAdvEMIWithGST());
            update.set("additionalCharges", loanCharges.isAdditionalCharges());
            update.set("loanPercentage", loanCharges.getLoanPercentage());
            update.set("loanChargesList",loanCharges.getLoanChargesList());
            update.set("brokenPeriodDays",loanCharges.getBrokenPeriodDays());
            update.set("insuranceRequired",loanCharges.isInsuranceRequired());
            update.set("imd2Amount",loanCharges.getImd2Amount());
            update.set("imd2gstAmt",loanCharges.getImd2gstAmt());
            update.set("interestEmiStartDt",loanCharges.getInterestEmiStartDt());
            update.set("loanChargesDetails", loanCharges.getLoanChargesDetails());
            update.set("quarter",loanCharges.getQuarter());
            mongoTemplate.upsert(query, update, LoanCharges.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving save LoanCharges Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save LoanCharges Details {%s}", e.getMessage()));
        }

        return true;
    }

        @Override
    public LoanCharges fetchLoanChargesDetails(LoanChargesRequest loanChargesRequest) {
        logger.info("Inside fetchLoanChargesDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(loanChargesRequest.getRefId())
                    .and("institutionId").is(loanChargesRequest.getHeader().getInstitutionId()));

            return mongoTemplate.findOne(query, LoanCharges.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching LoanChargesDetails with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching LoanChargesDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public LoanCharges fetchLoanChargesDetailsByRefId(String refId, String institutionId) {
        logger.info("Inside fetchLoanChargesDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, LoanCharges.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching LoanChargesDetails with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching LoanChargesDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean triggerVerification(VerificationRequest verificationRequest) {
        logger.info("Inside triggerVerification()");
        try {
            int count;
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(verificationRequest.getRefId())
                    .and("institutionId").is(verificationRequest.getHeader().getInstitutionId());
            Update update = new Update();
            switch (verificationRequest.getVerificationType())
            {
                case EndPointReferrer.OFFICE_VERIFICATION :
                    criteria.and("officeVerification.status").is(ThirdPartyVerification.Status.PENDING.name());
                    query.addCriteria(criteria);
                    update.set("officeVerification.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
                    update.set("officeVerification.$.submissionTime", new Date());
                    break;
                case EndPointReferrer.RESIDENCE_VERIFICATION :
                    criteria.and("residenceVerification.status").is(ThirdPartyVerification.Status.PENDING.name());
                    query.addCriteria(criteria);
                    update.set("residenceVerification.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
                    update.set("residenceVerification.$.submissionTime", new Date());
                    break;
                case EndPointReferrer.BANK_VERIFICATION :
                    criteria.and("bankingVerification.status").is(ThirdPartyVerification.Status.PENDING.name());
                    query.addCriteria(criteria);
                    update.set("bankingVerification.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
                    update.set("bankingVerification.$.submissionTime", new Date());
                    break;
                case EndPointReferrer.REFERENCE_DETAILS :
                    criteria.and("referenceDetails.status").is(ThirdPartyVerification.Status.PENDING.name());
                    query.addCriteria(criteria);
                    update.set("referenceDetails.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
                    update.set("referenceDetails.$.submissionTime", new Date());
                    break;
                case EndPointReferrer.ITR_VERIFICATION :
                    criteria.and("itrVerification.status").is(ThirdPartyVerification.Status.PENDING.name());
                    query.addCriteria(criteria);
                    update.set("itrVerification.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
                    update.set("itrVerification.$.submissionTime", new Date());
                    break;
                default:
                    logger.warn("{} is incorrect",verificationRequest.getVerificationType());
                    break;
            }
            do {
                count = mongoTemplate.updateFirst(query, update, VerificationDetails.class).getN();
            } while (count != 0);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while TriggerVerification with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while TriggerVerification with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean triggerVerificationForLegal(VerificationRequest verificationRequest) {
        logger.info("Inside triggerVerificationForLegal()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(verificationRequest.getRefId())
                    .and("institutionId").is(verificationRequest.getHeader().getInstitutionId())
                    .and("status").is(ThirdPartyVerification.Status.PENDING.name()));
            Update update = new Update();
            update.set("status", ThirdPartyVerification.Status.SUBMITTED.name());
            update.set("submissionTime", new Date());
            logger.debug("Query for triggerVerificationForLegal() - {}", query.toString());
            mongoTemplate.updateFirst(query, update, LegalVerification.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while TriggerVerificationForLegal with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while TriggerVerificationForLegal with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public void updateValuationDetails(ValuationRequest valuationRequest) {
        logger.info("Inside updateValuationDetails() after collateral delete");
        Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(valuationRequest.getRefId())
                    .and("institutionId").is(valuationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("valuationDetailsList", valuationRequest.getValuation().getValuationDetailsList());
            mongoTemplate.upsert(query, update, Valuation.class);
    }

    @Override
    public boolean saveValuationDetails(ValuationRequest valuationRequest, Map<String, Set<String>> verificationStatusDealersMap,
                                        Map<String, Valuation> triggeredToAgencies) {
        logger.info("Inside saveValuationDetails()");
        try {
            Query query = new Query();
                Criteria criteria = Criteria.where("_id").is(valuationRequest.getRefId())
                    .and("institutionId").is(valuationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            Valuation dbVDObject = mongoTemplate.findOne(query, Valuation.class);
            query = new Query();
            Update update = new Update();
            String loggedInRole = valuationRequest.getHeader().getLoggedInUserRole();
            boolean isVerifier = (Cache.VERIFICATION_ROLE_URL_MAP.containsKey(loggedInRole));
            String statusVerified = ThirdPartyVerification.Status.VERIFIED.name();
            String statusSubmitted = ThirdPartyVerification.Status.SUBMITTED.name();
            boolean isCredit = false;
            if(Institute.isInstitute(valuationRequest.getHeader().getInstitutionId(),Institute.SBFC)) {
                if(valuationRequest.getHeader().getCredit() != null) {
                    isCredit = valuationRequest.getHeader().getCredit();
                }
            }

            List<ValuationDetails> valuationDetailsList = getValidValuationDetailsToUpdate(valuationRequest.getValuation(), dbVDObject, isVerifier,
                    statusVerified, statusSubmitted, valuationRequest.getHeader().getDealerId(), loggedInRole,
                    verificationStatusDealersMap, triggeredToAgencies, isCredit);

            update.set("valuationDetailsList", valuationDetailsList);

            query.addCriteria(criteria);
            mongoTemplate.upsert(query, update, Valuation.class);
        }catch (Exception e) {
            logger.error("Exception caught while saving Valuation Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving Valuation Details {%s}", e.getMessage()));
        }

        return true;
    }

    @Override
    public boolean deleteValuationDetails(ValuationRequest valuationRequest) {
        logger.info("Inside saveValuationDetails()");
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(valuationRequest.getRefId())
                    .and("institutionId").is(valuationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            Valuation dbVDObject = mongoTemplate.findOne(query, Valuation.class);
            query = new Query();
            Update update = new Update();

            List<ValuationDetails> valuationDetailsList = new ArrayList<>();
            dbVDObject.getValuationDetailsList().forEach(dbObj -> {
                boolean validAdd = false;
                for (ValuationDetails valuationDetails : valuationRequest.getValuation().getValuationDetailsList()) {
                    if(StringUtils.equals(dbObj.getCollateralId(), valuationDetails.getCollateralId()) &&
                            StringUtils.equals(dbObj.getAgencyCode(), valuationDetails.getAgencyCode())){
                        validAdd = true;
                        break;
                    }
                }
                if(validAdd){
                    valuationDetailsList.add(dbObj);
                }
            });

            update.set("valuationDetailsList", valuationDetailsList);

            query.addCriteria(criteria);
            mongoTemplate.upsert(query, update, Valuation.class);
        }catch (Exception e) {
            logger.error("Exception caught while saving Valuation Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving Valuation Details {%s}", e.getMessage()));
        }

        return true;
    }

    private List<ValuationDetails> getValidValuationDetailsToUpdate(Valuation reqValuationDetails, Valuation dbVDObject,
                boolean isVerifier, String statusVerified, String statusSubmitted, String agencyCode,
                String loggedInRole, Map<String, Set<String>> verificationStatusDealersMap,
                                                                    Map<String, Valuation> triggeredToAgencies, boolean isCredit) {
        List<ValuationDetails> valuationDetailsList;
        if(dbVDObject != null && dbVDObject.getValuationDetailsList() != null) {
            valuationDetailsList = new ArrayList<>();
            List<ValuationDetails> dbObjectList = dbVDObject.getValuationDetailsList();
            List<ValuationDetails> reqObjectList = reqValuationDetails.getValuationDetailsList();
            GoNoGoCustomerApplication goNoGoCustomerApplication = getGoNoGoCustomerApplicationByRefId(dbVDObject.getRefId());
            /*Filter all objects which are not going to update
             (1.all verified status object 2.if user is verifier then all those records which are not assigned to his agency)*/
            dbObjectList.forEach(obj -> {
                if(StringUtils.equals(statusVerified, obj.getStatus()) || (isVerifier && !StringUtils.equals(obj.getAgencyCode(), agencyCode))
                        || (!isVerifier && StringUtils.equals(statusSubmitted, obj.getStatus()))){
                    valuationDetailsList.add(obj);
                }
            });

            /* CPA can re initiate the application for verification again if it is already verified*/
            if(!isVerifier) {
                List<ValuationDetails> reInitiatedList = reqObjectList.stream().filter(reqObj -> reqObj.isReinitiate()).collect(Collectors.toList());
                removeObjectsFromFinalListValuation(valuationDetailsList, reInitiatedList);
            }
            if(Roles.isCpaRole(loggedInRole) || Roles.isCreditRole(loggedInRole)
                    || StringUtils.equalsIgnoreCase(loggedInRole, Roles.Role.CREDIT.name()) || isCredit){
                List<ValuationDetails> verifiedObjList = reqObjectList.stream().filter(reqObj ->
                        StringUtils.equalsIgnoreCase(statusVerified, reqObj.getStatus())).collect(Collectors.toList());
                removeObjectsFromFinalListValuation(valuationDetailsList, verifiedObjList);
            }

            /*get all objects from request which are not in above list*/
            reqObjectList.forEach(obj -> {
                boolean validAdd = true;
                for (ValuationDetails valuationDetails : valuationDetailsList) {
                    if(StringUtils.equals(obj.getCollateralId(), valuationDetails.getCollateralId()) &&
                            StringUtils.equals(obj.getAgencyCode(), valuationDetails.getAgencyCode())){
                        validAdd = false;
                        break;
                    }
                }
                if (obj.isReinitiate()) {
                    obj = copyCollateralRequest(obj, goNoGoCustomerApplication.getApplicationRequest().getRequest());
                }
                if(validAdd){
                    if(isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())
                            && obj.getVerificationTime() == null){
                        obj.setVerificationTime(new Date());
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode());
                    } else if(!isVerifier && StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())){
                        verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode());
                        // Add the obj in a map of agencycode vs ValuationDetails
                        addToTriggeredValuation(triggeredToAgencies, obj, reqValuationDetails);
                    }
                    valuationDetailsList.add(obj);
                }

            });

        }else {
            valuationDetailsList = reqValuationDetails.getValuationDetailsList();
            valuationDetailsList.forEach(obj -> {
                if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())) {
                    verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).add(obj.getAgencyCode());
                    // Add the obj in a map of agencycode vs ValuationDetails
                    addToTriggeredValuation(triggeredToAgencies, obj, reqValuationDetails);
                } else if(StringUtils.equals(obj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                    verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).add(obj.getAgencyCode());
                }
            });
        }
        return valuationDetailsList;
    }

    private void addToTriggeredValuation(Map<String, Valuation> triggeredToAgencies, ValuationDetails obj, Valuation valuationDetails) {
        Valuation triggeredEntries = triggeredToAgencies.get(obj.getAgencyCode() );
        if( triggeredEntries == null ) {
            triggeredEntries = new Valuation();
            triggeredEntries.setInstitutionId(valuationDetails.getInstitutionId());
            triggeredEntries.setRefId(valuationDetails.getRefId());
            triggeredEntries.setValuationDetailsList(new ArrayList<>() );
            triggeredToAgencies.put(obj.getAgencyCode(), triggeredEntries);
        }
        triggeredEntries.getValuationDetailsList().add(obj);
    }

    private void removeObjectsFromFinalListValuation(List<ValuationDetails> valuationDetailsList, List<ValuationDetails> removableList) {
        if(!CollectionUtils.isEmpty(removableList)) {
            removableList.forEach(obj -> {
                for (int i = 0; i < valuationDetailsList.size(); i++) {
                    if (StringUtils.equals(obj.getCollateralId(), valuationDetailsList.get(i).getCollateralId()) &&
                            StringUtils.equals(obj.getAgencyCode(), valuationDetailsList.get(i).getAgencyCode())) {
                        valuationDetailsList.remove(i);
                        break;
                    }
                }
            });
        }
    }

    @Override
    public Valuation fetchValuationDetails(ValuationRequest valuationRequest) {
        logger.info("Inside fetchValuationDetails()");
        List<String> statusList = new ArrayList<>();
        statusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
        statusList.add(ThirdPartyVerification.Status.VERIFIED.name());
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(valuationRequest.getRefId())
                    .and("institutionId").is(valuationRequest.getHeader().getInstitutionId());
            MongoRepositoryUtil.createCriteriaForVerifierRole(valuationRequest.getHeader(), criteria,
                    valuationRequest.getHeader().getLoggedInUserRole(), statusList);
            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, Valuation.class);
        }catch(DataAccessException e){
            //e.printStackTrace();
                logger.error("Error occurred while fetching Valuation with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching Valuation with probable cause [{%s}]", e.getMessage()));
        }
    }

    /*@Override
    public boolean saveEligibilityDetails(EligibilityDetails eligibilityDetails) {
        logger.info("saving eligibility details against refId %s",eligibilityDetails.getRefId());
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(eligibilityDetails.getRefId())
                    .and("institutionId").is(eligibilityDetails.getInstitutionId()));
            Update update = new Update();
            update.set("applicantName", eligibilityDetails.getApplicantName());
            update.set("applicantId", eligibilityDetails.getApplicantId());
            update.set("cashPrftList", eligibilityDetails.getCashPrftList());
            update.set("grossPrftList", eligibilityDetails.getGrossPrftList());
            update.set("iTRList", eligibilityDetails.getITRList());
            update.set("rTRList", eligibilityDetails.getRTRList());
            update.set("topUpList", eligibilityDetails.getTopUpList());
            update.set("salariedList", eligibilityDetails.getSalariedList());
             mongoTemplate.upsert(query, update, EligibilityDetails.class);
        }catch(Exception e){
            logger.error("Exception caught while saving eligibility Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving  eligibility Details {%s}",e.getMessage()));
        }
        return true;
    }
*/
    @Override
    public EligibilityDetails fetchEligibilityDetails(EligibilityRequest eligibilityRequest) {

        logger.info("Fetching eligibility details against refId %s", eligibilityRequest.getRefId());
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(eligibilityRequest.getRefId())
                    .and("institutionId").is(eligibilityRequest.getHeader().getInstitutionId()));
            return mongoTemplate.findOne(query, EligibilityDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching eligibility with probable cause [{%s}] ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching eligibility with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public EligibilityDetails fetchEligibilityDetails(String refId, String instId) {

        logger.info("Fetching eligibility details against refId %s", refId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            return mongoTemplate.findOne(query, EligibilityDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching eligibility with probable cause [{%s}] ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching eligibility with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public EligibilityDetails fetchEligibilityDetail(String refId, String instId) {

        logger.info("Fetching eligibility details against refId %s", refId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            return mongoTemplate.findOne(query, EligibilityDetails.class);
        } catch (DataAccessException e) {
            return null;
        }
    }


    @Override
    public boolean saveEligibilityDetails(EligibilityDetails eligibilityDetails) {

        logger.debug("eligibility Details repo started");
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(eligibilityDetails, dbObject);
            mongoTemplate.execute(EligibilityDetails.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(eligibilityDetails.getRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            logger.debug("eligibility Details repo started");
            return true;
        } catch (DataAccessException e) {
            logger.error("Error occurred while saving eligibility details" +
                            "  with probable cause [{}] "
                    , e.getMessage());
            return false;        //throw new SystemException(String.format("Error occurred while saving eligibility details with probable cause [%s] ", e.getMessage()));
        }
    }

    @Override
    public boolean triggerVerificationForValuation(VerificationRequest verificationRequest) {
        logger.info("Inside triggerVerificationForValuation()");
        try {
            int count;
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(verificationRequest.getRefId())
                    .and("institutionId").is(verificationRequest.getHeader().getInstitutionId());
            Update update = new Update();
            criteria.and("valuationDetailsList.status").is(ThirdPartyVerification.Status.PENDING.name());
            query.addCriteria(criteria);
            update.set("valuationDetailsList.$.status", ThirdPartyVerification.Status.SUBMITTED.name());
            update.set("valuationDetailsList.$.submissionTime", new Date());
            logger.debug("Query for triggerVerificationForValuation() - {}", query.toString());
            do {
                count = mongoTemplate.updateFirst(query, update, ValuationDetails.class).getN();
            } while (count != 0);
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while TriggerVerificationForValuation with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while TriggerVerificationForValuation with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean updateCompletedInfo(String refId, String institutionId, String completedStep, String userId, String role) {
        GoNoGoCustomerApplication gngApp = this.getGoNoGoCustomerApplicationByRefId(refId);
        return updateCompletedInfo(gngApp, completedStep, userId, role);
    }

    @Override
    public boolean updateCompletedInfo(GoNoGoCustomerApplication gngApp , String completedStep, String userId, String role){
        logger.info("Inside updateCompletedInfo() for refId: {}",gngApp.getGngRefId());
        try {
            CompletionInfo info;
            Map<String, CompletionInfo> completedSteps = gngApp.getCompleted();
            if(  CollectionUtils.isEmpty(completedSteps) || ! completedSteps.containsKey(completedStep)) {
                info = new CompletionInfo();
                completedSteps.put(completedStep, info);
            } else {
                info = completedSteps.get(completedStep);
                info.setUpdatedOn(new Date());
            }
            info.setUserId(userId);
            info.setRole(role);

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(gngApp.getGngRefId())
                    .and("applicationRequest.header.institutionId")
                    .is(gngApp.getApplicationRequest().getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("completed", completedSteps);
            update.set(GoNoGoCustomerApplicationKeyConstants.LASTUPDATEDDATE, new Date());
            logger.debug("Query for updateCompletedInfo() - {} and object is {}", query,completedSteps);
            mongoTemplate.upsert(query, update, GoNoGoCustomerApplication.class);
            logger.debug("CompletedInfo is updated in db successfully for refId: {}",gngApp.getGngRefId());
        } catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateStageId with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateStageId with probable cause [{%s}]", e.getMessage()));
        }
        return true;

    }

    @Override
    public boolean updateStageId(ApplicationRequest applicationRequest, String applicationStatus) {
        logger.info("Inside updateStageId()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            if(applicationStatus != null) {
                update.set("applicationStatus", applicationStatus);
            }
            update.set("applicationRequest.currentStageId", applicationRequest.getCurrentStageId());
            logger.debug("Query for updateStageId() - {}", query.toString());
            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateStageId with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateStageId with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean updateCurrentSatgeStageId(ApplicationRequest applicationRequest, String currentStageId) {
        logger.info("Inside updateCurrentStageId()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("applicationRequest.currentStageId", currentStageId);
            logger.debug("Query for updateCurrentStageId() - {}", query.toString());
            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateCurrentStageId with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateCurrentStageId with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean saveDeviation(String refId, String instId, DeviationDetails deviationDetails) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            Update update = new Update();
            update.set("deviationList", deviationDetails.getDeviationList());
            mongoTemplate.upsert(query, update, DeviationDetails.class);
        } catch (DataAccessException e) {
            logger.error("Exception caught while saving deviations " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving deviations %s", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean updateDeviation(String refId, String instId, DeviationDetails deviationDetails) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            Update update = new Update();
            update.set("deviationList", deviationDetails.getDeviationList());
            mongoTemplate.upsert(query, update, DeviationDetails.class);
        } catch (DataAccessException e) {
            logger.error("Exception caught while saving deviations " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving deviations %s", e.getMessage()));
        }
        return true;
    }

    @Override
    public DeviationDetails fetchDeviationDetails(String refId,String instId) {
        logger.info("Fetching deviation details against refId {} and institutionId {} ", refId, instId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            logger.debug("Query formed : {}" , query);
            return mongoTemplate.findOne(query, DeviationDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching deviation with probable cause {} ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching deviation with probable cause %s", e.getMessage()));
        }
    }

    @Override
    public boolean saveDMDetails(DMRequest dmRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(dmRequest.getRefId())
                    .and("institutionId").is(dmRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("repayment", dmRequest.getDisbursementMemo().getRepayment());
            update.set("loanCharges", dmRequest.getDisbursementMemo().getLoanCharges());
            update.set("camSummary", dmRequest.getDisbursementMemo().getCamSummary());
            update.set("dmInputList", dmRequest.getDisbursementMemo().getDmInputList());
            update.set("branchName", dmRequest.getDisbursementMemo().getBranchName());
            update.set("branchcode", dmRequest.getDisbursementMemo().getBranchcode());
            update.set("spokeName", dmRequest.getDisbursementMemo().getSpokeName());
            update.set("spokeCode", dmRequest.getDisbursementMemo().getSpokeCode());
            update.set("loanStationName", dmRequest.getDisbursementMemo().getLoanStationName());
            update.set("loanStationCode", dmRequest.getDisbursementMemo().getLoanStationCode());
            update.set("dmName", dmRequest.getDisbursementMemo().getDmName());
            update.set("propertyAddress", dmRequest.getDisbursementMemo().getPropertyAddress());
            update.set("valuer1", dmRequest.getDisbursementMemo().getValuer1());
            update.set("valuer2", dmRequest.getDisbursementMemo().getValuer2());
            update.set("linkLoan", dmRequest.getDisbursementMemo().getLinkLoan());
            update.set("finalConsiderValueType", dmRequest.getDisbursementMemo().getFinalConsiderValueType());
            update.set("coApplicant", dmRequest.getDisbursementMemo().getCoApplicant());
            update.set("customerId", dmRequest.getDisbursementMemo().getCustomerId());
            update.set("locationCode", dmRequest.getDisbursementMemo().getLocationCode());
            update.set("remarkList", dmRequest.getDisbursementMemo().getRemarkList());
            mongoTemplate.upsert(query, update, DisbursementMemo.class);
        } catch (DataAccessException e) {
            logger.error("Exception caught while saving DisbursementMemo " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving DisbursementMemo %s", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean updateExternalServiceFlag(ApplicationRequest applicationRequest, String serviceType) {
        logger.info("Inside updateExternalServiceFlag()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            if(StringUtils.equals(serviceType, GNGWorkflowConstant.MIFIN_API_CALL.name())) {
                update.set("applicationRequest.externalServiceCalls.pushToMifin", true);
                update.set("applicationRequest.miFinProspectCode", applicationRequest.getMiFinProspectCode());
                update.set("applicationRequest.miFinCustomerCode", applicationRequest.getMiFinCustomerCode());
                update.set(GoNoGoCustomerApplicationKeyConstants.LASTUPDATEDDATE, new Date());
                update.set(GoNoGoCustomerApplicationKeyConstants.ACTUALDISBDATE, new Date());
                logger.debug("Query for updateExternalServiceFlag() - {}", query.toString());
                mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            }
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateExternalServiceFlag with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateExternalServiceFlag with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public DisbursementMemo fetchDMDetails(DMRequest dmRequest) {
        return fetchDMDetails(dmRequest.getRefId(), dmRequest.getHeader().getInstitutionId());
    }

    @Override
    public DisbursementMemo fetchDMDetails(String refId,String insttId) {
        logger.info("Fetching DisbursementMemo details against refId {} ", refId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(insttId));
            logger.debug("Query formed : {}" , query);
            return mongoTemplate.findOne(query, DisbursementMemo.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching DisbursementMemo with probable cause {} ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching DisbursementMemo with probable cause %s", e.getMessage()));
        }
    }

    @Override
    public SanctionConditions fetchSanctionConditions(SanctionConditionRequest sanctionConditionRequest) {
        return fetchSanctionConditions(sanctionConditionRequest.getRefId(), sanctionConditionRequest.getHeader().getInstitutionId());
    }
    @Override
    public SanctionConditions fetchSanctionConditions( String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));
            logger.debug("Query for saving SanctionConditions - {}", query.toString());
            return mongoTemplate.findOne(query, SanctionConditions.class);

        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching SanctionConditions with probable cause [{%s}] ", e.getMessage());

            return null;        //throw new SystemException(String.format(" error occurred  while fetching SanctionConditions with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveSanctionConditions(SanctionConditionRequest sanctionConditionRequest) {
        logger.info("Inside saveSanctionConditions()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(sanctionConditionRequest.getRefId())
                    .and("institutionId").is(sanctionConditionRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("applicantId", sanctionConditionRequest.getSanctionConditions().getApplicantId());
            update.set("applicantName", sanctionConditionRequest.getSanctionConditions().getApplicantName());
            update.set("scDetailsList", sanctionConditionRequest.getSanctionConditions().getScDetailsList());
            mongoTemplate.upsert(query, update, SanctionConditions.class);
            logger.debug("Query for saving SanctionConditions - {}", query.toString());
        } catch (Exception e) {
            logger.error("Exception caught while saving SanctionConditions " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save SanctionConditions {%s}", e.getMessage()));
        }

        return true;
    }
    @Override
    public DedupeMatch fetchDedupeInfo(String refId, String instId) {
        logger.info("Fetching dedupe match details against refId {} and institutionId {} ", refId, instId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instId));
            logger.debug("Query formed : {}" , query);
            return mongoTemplate.findOne(query, DedupeMatch.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();

            logger.error("Error occurred while fetching dedupe match details with probable cause {} ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching dedupe match details with probable cause %s", e.getMessage()));
        }
    }

    @Override
    public boolean updateApplicationStage(String refId, String institutionId, String stageName, String applicationStatus) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId));
            Update update = new Update();
            if(applicationStatus != null) {
                update.set("applicationStatus", applicationStatus);
            }
            update.set("applicationRequest.currentStageId", stageName);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            if (writeResult.isUpdateOfExisting()){
                logger.debug("Changed stage of application " +refId+ " refId to " +stageName+ " and status to " +applicationStatus
                        + " on reassigning application");
                return true;
            }
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while updateStageId while reassigning [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updateStageId with probable cause [{%s}]", e.getMessage()));
        }
        return false;
    }

    @Override
    public VerificationDetails fetchVerificationDetailsByRefId(String refId, String institutionId) {
        logger.info("Inside fetchVerificationDetailsByRefId()");
        try{
            Query query= new Query();
            Criteria criteria =  Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId);
            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, VerificationDetails.class);
        }catch (Exception e){
            //e.printStackTrace();
            logger.error("Error occurred while fetching VerificationDetails with probable cause [{%s}] ", e.getMessage());
            return null;        //throw new SystemException(String.format(" error occurred  while fetching VerificationDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchGNGData(UpdateGNGRequest updateGNGRequest) {
        logger.info("Inside fetch GoNoGoCustomerApplication");
        List<GoNoGoCustomerApplication> resultList = new ArrayList<>();
        try{
            Query query= new Query();
            Criteria criteria =  Criteria.where("completed").exists(true)
                    .and("completed.submit-by-fos").exists(false)
                    .and("applicationRequest.header.institutionId").is(updateGNGRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            query.fields().include("_id")
                    .include("completed");
            logger.debug("Query for Completed Info - {}", query.toString());
            resultList = mongoTemplate.find(
                    query, GoNoGoCustomerApplication.class);
        }catch (Exception e) {
            e.getCause();
            //e.printStackTrace();
            logger.error("Error occurred while fetching completed info of GoNoGoCustomerApplication with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while fetching completed info of GoNoGoCustomerApplication with probable cause [{%s}]", e.getMessage()));
        }
        return resultList;
    }

    @Override
    public GoNoGoCustomerApplication fetchGNGDataByMob(GetGngByMobRequest getGngByMobRequest) {
        logger.info("Inside fetch GoNoGoCustomerApplication by Mobile Number");
        List<GoNoGoCustomerApplication> applicationData = new ArrayList<>();
        try{
            Query query= new Query();
            Criteria criteria =  Criteria.where("applicationRequest.request.applicant.phone.phoneNumber").is(getGngByMobRequest.getPhoneNumber())
                    .and("applicationRequest.request.application.channel").is(ThirdPartyIntegrationHelper.ONLINE)
                    .and("applicationRequest.header.institutionId").is(getGngByMobRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            //query.fields().include("_id");
            query.with(new Sort(Sort.Direction.DESC, "_id"));
            query.limit(1);
            logger.debug("Query for Completed Info - {}", query.toString());
            applicationData = mongoTemplate.find(query,GoNoGoCustomerApplication.class);
        }catch (Exception e) {
            e.getCause();
            //e.printStackTrace();
            logger.error("Error occurred while fetching completed info of GoNoGoCustomerApplication by Mobile number with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while fetching completed info of GoNoGoCustomerApplication by mobile number with probable cause [{%s}]", e.getMessage()));
        }
        //will give error if no data found as limit and sort is found
        if(applicationData.size() > 0) {
            return applicationData.get(0);
        }
        else{
            return null;
        }


    }


    @Override
    public int updateCompletedInfoGonogo(UpdateGNGRequest updateGNGRequest, List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList) {
        logger.info("Inside updateCompletedInfoGonogo");
        int count = 0;
        try{
            if(org.apache.commons.collections.CollectionUtils.isNotEmpty(goNoGoCustomerApplicationList)) {
                Date latest;
                List<Date> dates;
                for(GoNoGoCustomerApplication gngObj : goNoGoCustomerApplicationList) {
                    latest = null;
                    Map<String, CompletionInfo> completedSteps = gngObj.getCompleted();
                    List<CompletionInfo> completionInfoList = completedSteps.values().stream().filter(roleObj ->
                            StringUtils.equalsIgnoreCase(Roles.Role.FOS.name(), roleObj.getRole())).collect(Collectors.toList());
                    dates = new ArrayList<Date>();
                    if(org.apache.commons.collections.CollectionUtils.isNotEmpty(completionInfoList)) {
                        for (CompletionInfo completionInfo : completionInfoList) {
                            if(completionInfo != null)
                                dates.add(completionInfo.getCompletedOn());
                        }
                    }
                    if(dates.size() > 0) {
                        latest = Collections.max(dates);
                    }
                    CompletionInfo info1 = completedSteps.get(EndPointReferrer.STEP_REGISTRATION);
                    CompletionInfo completionInfo = new CompletionInfo();
                    completionInfo.setCompletedOn(latest);
                    completionInfo.setUpdatedOn(latest);
                    completionInfo.setRole(Roles.Role.FOS.name());
                    if(info1 != null) {
                        completionInfo.setUserId(info1.getUserId());
                    }
                    completedSteps.put("submit-by-fos",completionInfo);
                    count += updateGoNoGoApplication(gngObj, completedSteps, updateGNGRequest);
                }
            }
        }catch (Exception e) {
            e.getCause();
            //e.printStackTrace();
            logger.error("Error occurred while updating GoNoGoCustomerApplication with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while updating GoNoGoCustomerApplication with probable cause [{%s}]", e.getMessage()));
        }
        return count;
    }

    private int updateGoNoGoApplication(GoNoGoCustomerApplication goNoGoCustomerApplication, Map<String, CompletionInfo> completedSteps, UpdateGNGRequest updateGNGRequest) {
        logger.info("Inside update GoNoGoApplication()");
        int i = 0;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId())
                    .and("applicationRequest.header.institutionId").is(updateGNGRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("completed", completedSteps);
            update.set(GoNoGoCustomerApplicationKeyConstants.LASTUPDATEDDATE, new Date());
            logger.debug("Query for update GoNoGoApplication() - {}", query.toString());
            i = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class).getN();
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while update GoNoGoCustomerApplication with probable cause [{%s}] ", e.getMessage());
           // ////throw new SystemException(String.format(" error occurred  while update GoNoGoCustomerApplication with probable cause [{%s}]", e.getMessage()));
        }
        return i;
    }

    @Override
    public boolean saveCamSummary(ApplicationRequest applicationRequest, int noOfYearAtResidence) {
        logger.info("inside saveCamsummary for perpopulate in camsummary from registration in SBFC_PL");
        CamDetails camDetails = new CamDetails();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("noOfYearAtResidence", camDetails.getSummary().getNoOfYearAtResidence());
            mongoTemplate.upsert(query, update, CamDetails.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving camDetails " + e.getMessage());
            //e.printStackTrace();
            return false;        ////throw new SystemException(String.format("Exception caught while saving camDetails {%s}", e.getMessage()));
        }

    }

    @Override
    public boolean saveEligibilityDetails(CamDetailsRequest camDetailsRequest) {
        logger.info("saving eligibility details against refId %s",camDetailsRequest.getRefId());
        Double fixedOblgation = 0.0;
        CamDetails camDetails = camDetailsRequest.getCamDetails();
        List<RTRDetail> rtrList = camDetails.getRtrDetailList();
        for (RTRDetail tempList:rtrList) {
            fixedOblgation = tempList.getNetFoAmount();
        }
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(camDetailsRequest.getRefId())
                    .and("institutionId").is(camDetailsRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("fixedOblgation", fixedOblgation);
            mongoTemplate.upsert(query, update, EligibilityDetails.class);
        }catch(Exception e){
            logger.error("Exception caught while saving eligibility Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving  eligibility Details {%s}",e.getMessage()));
        }
        return true;

    }

    @Override
    public boolean savePerfiosData(PerfiosData perfiosData) {
        logger.info("Inside savePerfiosData()");
        try {
            Query query = new Query(Criteria.where("_id").is(perfiosData.getRefId()));
            PerfiosData dbObject = mongoTemplate.findOne(query, PerfiosData.class);
            if(dbObject != null) {
                Update update = new Update();
                update.set("saveDataList", perfiosData.getSaveDataList());
                mongoTemplate.upsert(query, update, PerfiosData.class);
            } else {
                mongoTemplate.save(perfiosData);
            }
        }catch(DataAccessException e){
            //e.printStackTrace();
            logger.error("Error occurred while update GoNoGoCustomerApplication with probable cause [{%s}] ", e.getMessage());
            //throw new SystemException(String.format(" error occurred  while update GoNoGoCustomerApplication with probable cause [{%s}]", e.getMessage()));
        }
       return true;
    }

    @Override
    public PerfiosData fetchPerfiosData(String refId, String institutionId, String acknowledgeId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                                        .and("institutionId").is(institutionId);
        if( StringUtils.isNotEmpty(acknowledgeId)) {
            criteria.and("acknowledgeId").is(acknowledgeId);
        }
        query.addCriteria(criteria) ;
        logger.debug("Query for fetching perfios data() - {}", query.toString());
        return mongoTemplate.findOne(query, PerfiosData.class);
    }

    @Override
    public String getProduct(String refId, String institutionId) {
        String value = null;

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId)
                .and("applicationRequest.header.institutionId").is(institutionId));
        query.fields().include("applicationRequest.request.application.loanType");
        GoNoGoCustomerApplication gng = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
        if( gng != null) value = gng.getApplicationRequest().getRequest().getApplication().getLoanType();

        return value;
    }
    @Override
    public CamDetails fetchCamSummaryByRefId(String refId, String instituteId) {
        logger.info("Inside fetchCamDetails()");
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(instituteId));
            query.fields().include("summary");
            return mongoTemplate.findOne(query, CamDetails.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching CamDetails with probable cause [{%s}] ", e.getMessage());

            return null;        //throw new SystemException(String.format(" error occurred  while fetching CamDetails with probable cause [{%s}]", e.getMessage()));
        }

    }

    @Override
    public long fetchCountAllApplications(String institutionId, List<String> refIds){
        if(mongoTemplate == null){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(org.apache.commons.collections.CollectionUtils.isNotEmpty(refIds)){
            criteria.and("_id").in(refIds);
        }
        Query query = new Query(criteria);
        return mongoTemplate.count(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchAllApplications(String institutionId, List<String> refIds, int skip){
        if(mongoTemplate == null){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(org.apache.commons.collections.CollectionUtils.isNotEmpty(refIds)){
            criteria.and("_id").in(refIds);
        }
        Query query = new Query(criteria);
        query.fields().include("applicantComponentResponse.multiBureauJsonRespose.mergedResponse.bureauFeeds");
        query.fields().include("applicantComponentResponseList.multiBureauJsonRespose.mergedResponse");
        query.fields().include("applicantComponentResponseList.applicantId");
        query.skip(skip);
        query.limit(500);
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean updateApplicantCoApplicantData(String refId, Map<String, CustomData> applicantCoApplicantData){
        if (mongoTemplate == null) {
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        Update update = new Update();
        update.set("applicantCoApplicantData", applicantCoApplicantData);
        mongoTemplate.upsert(query,update,GoNoGoCustomerApplication.class);
        return true;
    }

    @Override
    public long fetchCountOfAllCamDetails(String institutionId, List<String> refIds) {
        logger.info("Inside fetchCamDetails()");
        try {
            Criteria criteria = new Criteria();
            if(!CollectionUtils.isEmpty(refIds)){
                criteria.and("_id").in(refIds);
            }
            Query query = new Query();
            query.addCriteria(criteria);
            query.fields().include("plAndBSAnalysis");
            query.fields().include("refId");

            return mongoTemplate.count(query, CamDetails.class);
        } catch (DataAccessException e) {
            logger.error("Error occurred while fetching CamDetails with probable cause [{%s}] ", e.getMessage());
            return 0;        //throw new SystemException(String.format(" error occurred  while fetching CamDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public List<CamDetails> fetchAllCamDetails(String institutionId, List<String> refIds, int skip) {
        logger.info("Inside fetchCamDetails()");
        try {
            Criteria criteria = new Criteria();
            if(!CollectionUtils.isEmpty(refIds)){
                criteria.and("_id").in(refIds);
            }
            Query query = new Query();
            query.addCriteria(criteria);
            query.fields().include("plAndBSAnalysis");
            query.fields().include("refId");
            query.skip(skip);
            query.limit(1000);
            return mongoTemplate.find(query, CamDetails.class);
        } catch (DataAccessException e) {
            logger.error("Error occurred while fetching CamDetails with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching CamDetails with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean updateApplicantCoApplicantCamData(String refId, Map<String, CustomData> applicantCoApplicantData){
        if (mongoTemplate == null) {
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        Update update = new Update();
        update.set("applicantCoApplicantData", applicantCoApplicantData);
        mongoTemplate.upsert(query, update, CamDetails.class);
        return true;
    }

    @Override
    public boolean saveCoOriginationDetails(OriginationRequest originationRequest) {
        logger.info("Inside saveCoOriginationDetails()");
        String refId = originationRequest.getRefID();
        String institutionId = originationRequest.getHeader().getInstitutionId();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));
            Update update = new Update();
            update.set("coOriginationDetailsList", originationRequest.getCoOrigination().getCoOriginationDetailsList());
            update.set("roiMasterdata", originationRequest.getCoOrigination().getRoiMasterdata());
            update.set("populate", originationRequest.getCoOrigination().isPopulate());
            mongoTemplate.upsert(query, update, CoOrigination.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving save CoOrigination Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save CoOrigination Details {%s}", e.getMessage()));
        }

        return true;
    }

    @Override
    public CoOrigination fetchCoOriginationDetails(OriginationRequest originationRequest) {
        String refId = originationRequest.getRefID();
        String institutionId = originationRequest.getHeader().getInstitutionId();
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                .and("institutionId").is(institutionId);

        query.addCriteria(criteria) ;
        logger.debug("Query for fetching coOrigination data() - {}", query.toString());
        return mongoTemplate.findOne(query, CoOrigination.class);

    }

    @Override
    public CoOrigination fetchCoOriginationDetails(String refId, String institutionId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                .and("institutionId").is(institutionId);

        query.addCriteria(criteria) ;
        logger.debug("Query for fetching coOrigination data() - {}", query.toString());
        return mongoTemplate.findOne(query, CoOrigination.class);
    }

    @Override
    public SchemeDetails fetchSchemeDetails(String institutionId, String product, String scheme,
                                            String collateralUsage, String occupationType ){
        try{
            Query query = new Query();
            Criteria criteria = Criteria.where("institutionId").is(institutionId)
                    .and("product").is(product)
                    .and("scheme").is(scheme)
                    .and("occupationType").is(occupationType);

            if(StringUtils.isNotEmpty(collateralUsage))
                criteria.and("collateralUsage").is(collateralUsage);

            query.addCriteria(criteria);
            query.fields().include("schemeId");
            query.fields().include("promotionId");
            logger.debug("Query for fetching Scheme Details - {}", query.toString());
            return mongoTemplate.findOne(query, SchemeDetails.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public SourcingDetails fetchSourcingDetails(String institutionId, String product, String branchName, String location){
        try{
            Query query = new Query();
            Criteria criteria = Criteria.where("institutionId").is(institutionId)
                    .and("product").is(product)
                    .and("location").regex(location, "i")
                    .and("branchName").regex(branchName,"i");

            query.addCriteria(criteria);
            query.fields().include("rmEmpId")
                    .include("rcmEmpid")
                    .include("apsLocation")
                    .include("iDisburseProcessShop");
            logger.debug("Query for fetching Scheme Details - {}", query.toString());
            return mongoTemplate.findOne(query, SourcingDetails.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public PinCodeMaster fetchPinCodeDetails(String instId, String queryString) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(instId)
                    .and("zipCode").is(queryString).and("active").is(true));
            query.fields().include("city")
                    .include("state")
                    .include("country")
                    .include("cityCode")
                    .include("stateCode")
                    .include("countryCode")
                    .include("zipCode");

            return mongoTemplate.findOne(query, PinCodeMaster.class);
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public EmployerMaster fetchEmployerDetails(String instId, String queryString) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(instId)
                    .and("employerName").is(queryString).and("active").is(true));
            query.fields().include("employerId")
                    .include("employerName")
                    .include("iciciEmpCode");

            return mongoTemplate.findOne(query, EmployerMaster.class);
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchGoNoGoCustomerApplication(String institutionId, Date startDate, Date endDate) {
        Query query = new Query();
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(startDate != null && endDate != null){
            criteria.and("applicationRequest.header.dateTime").gte(startDate).lte(endDate);
        } else if (startDate != null){
            criteria.and("applicationRequest.header.dateTime").gte(startDate);
        } else if(endDate != null){
            criteria.and("applicationRequest.header.dateTime").lte(endDate);
        }
        query.addCriteria(criteria);
        query.fields().include("_id");
        query.fields().include("applicationRequest.header.dsaId");
        query.fields().include("applicationRequest.appMetaData.branchV2");
        query.fields().include("applicationRequest.appMetaData.branchV2.branchId");
        query.fields().include("applicationRequest.appMetaData.branchV2.branchName");

        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean updateApplicationWithBranch(String refId, Branch branch) {
        Query query = new Query(Criteria.where("_id").is(refId));
        Update update = new Update();
        update.set("applicationRequest.appMetaData.branchV2", branch);
        mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
        return true;
    }

    @Override
    public Valuation fetchValuationDetails(String refId, String institutionId){
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));
            logger.info("Query for fetching valuation details:{}",query.toString());
            return mongoTemplate.findOne(query, Valuation.class);
        }catch (DataAccessException e){
            logger.error("Error occurred while fetching Valuation Details with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching Valuation Details with probable cause [{%s}]", e.getMessage()));
        }

    }

    @Override
    public List<GoNoGoCustomerApplication> fetchAllCasesBeforeDefinedTime(Date startDate, Date endDate) {
        Query query = new Query();
        if(startDate == null){
            query = query.addCriteria(Criteria.where("applicationRequest.header.dateTime").lte(endDate));
        } else{
            query.addCriteria(Criteria.where("applicationRequest.header.dateTime").gte(startDate).lte(endDate));
        }
        query.fields().include("applicationRequest.request.applicant.kyc");
        query.fields().include("applicationRequest.request.coApplicant");
        logger.info("query {} " ,query);
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean updateKycDetails(GoNoGoCustomerApplication application, boolean isApplicant, boolean isCoApplicant) {
        Query query = new Query();
        query = query.addCriteria(Criteria.where("_id").is(application.getGngRefId()));

        Update update = new Update();
        ApplicationRequest appRequest = application.getApplicationRequest();
        if(isApplicant)
            update.set("applicationRequest.request.applicant.kyc", appRequest.getRequest().getApplicant().getKyc());
        if(isCoApplicant)
            update.set("applicationRequest.request.coApplicant", appRequest.getRequest().getCoApplicant());

        logger.info("query {} " ,query);
        mongoTemplate.upsert(query, update, GoNoGoCustomerApplication.class);
        return true;
    }
    @Override
    public PerfiosData fetchPerfiosData(String refId , String institutionId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                .and("institutionId").is(institutionId);
        query.addCriteria(criteria);
        logger.debug("Query for fetching perfios data() - {}", query.toString());
        return mongoTemplate.findOne(query, PerfiosData.class);
    }

    @Override
    public PerfiosData fetchPerfiosData(String ackId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("saveDataList.acknowledgeId").is(ackId);
        query.addCriteria(criteria);
        logger.debug("Query for fetching perfios data() - {}", query.toString());
        return mongoTemplate.findOne(query, PerfiosData.class);
    }

    private PropertyVerification copyAddressFromRequest(PropertyVerification obj, Request request) {

        if (StringUtils.equalsIgnoreCase(request.getApplicant().getApplicantId(),obj.getApplicantId())) {
            changeApplicantName(request.getApplicant().getAddress(), obj,request.getApplicant() );

        }else{
            for(CoApplicant coApplicant  : request.getCoApplicant()){
                if(StringUtils.equalsIgnoreCase(coApplicant.getApplicantId(),obj.getApplicantId())){
                    changeApplicantName(coApplicant.getAddress(), obj,coApplicant );
                }
            }
        }
        return obj;
    }

    private void changeApplicantName(List<CustomerAddress> address, PropertyVerification obj, Applicant applicant) {
        for (CustomerAddress addr : address) {
            if (StringUtils.equalsIgnoreCase(addr.getAddressType(), obj.getInput().getAddress().getAddressType())) {
                obj = moduleHelper.copyAddressesFromRequest(applicant, addr, obj.getInput().getProduct(), obj);
                obj.setApplicantId(applicant.getApplicantId());
                obj.setApplicantName(applicant.getApplicantName());

            }
        }
    }

    private ValuationDetails copyCollateralRequest(ValuationDetails obj, Request request) {
        ValuationInput valuationInput = obj.getValuationInput();
        ValuationOutput valuationOutput = obj.getValuationOutput();

        for (Collateral collateral : request.getApplication().getCollateral()) {
            if (StringUtils.equalsIgnoreCase(collateral.getCollateralId() , obj.getCollateralId())){
                valuationInput = moduleHelper.copyCollateralFromRequest(collateral, valuationInput);
                valuationInput.setApplicantName(request.getApplicant().getApplicantName());
                valuationOutput.setCurrentPropertyOwner(collateral.getOwnerNames().get(0));
            }
        }

        obj.setValuationInput(valuationInput);

        return obj;
    }

    private LegalVerificationDetails copyCollateralData( LegalVerificationDetails obj, Request request) {
        LegalVerificationInput legalVerificationInput = obj.getLegalVerificationInput();
        LegalVerificationOutput legalVerificationOutput = obj.getLegalVerificationOutput();

        for (Collateral collateral : request.getApplication().getCollateral()) {
            if (StringUtils.equalsIgnoreCase(collateral.getCollateralId() , obj.getCollateralId())){
                legalVerificationInput = moduleHelper.copyCollateralFromRequest(collateral, legalVerificationInput);
                legalVerificationOutput.setOwnerNameOnPapers(collateral.getOwnerNames());
                legalVerificationInput.setApplicantName(request.getApplicant().getApplicantName());
                obj.setApplicantName(request.getApplicant().getApplicantName());
            }
        }

        obj.setLegalVerificationInput(legalVerificationInput);

        return obj;
    }


    @Override
    public boolean deleteVerificationDetails(VerificationRequest verificationRequest, String verificationType ){
        logger.info("Inside save verification detail()");
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(verificationRequest.getRefId())
                    .and("institutionId").is(verificationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);
            VerificationDetails dbVDObject = mongoTemplate.findOne(query, VerificationDetails .class);
            query = new Query();
            Update update = new Update();
            switch (verificationType){
                case EndPointReferrer.RESIDENCE_VERIFICATION :
                    List<PropertyVerification> residenceVarificationList = new ArrayList<PropertyVerification>();
                    dbVDObject.getResidenceVerification().forEach(dbObj -> {
                        boolean validAdd = false;
                        validAdd =  deleteRvOvList(verificationRequest.getVerificationDetails().getResidenceVerification(), validAdd, dbObj);
                        logger.info("residenceVerification {}", validAdd);
                        if (validAdd) {
                            residenceVarificationList.add(dbObj);
                        }
                    });
                    update.set("residenceVerification", residenceVarificationList);
                    break;
                case EndPointReferrer.OFFICE_VERIFICATION :
                    List<PropertyVerification> officeVerificationList = new ArrayList<PropertyVerification>();
                    dbVDObject.getOfficeVerification().forEach(dbObj -> {
                        boolean validAdd = false;
                        validAdd = deleteRvOvList(verificationRequest.getVerificationDetails().getOfficeVerification(), validAdd, dbObj);
                        logger.info("residenceOfficeVerification {}", validAdd);
                        if (validAdd) {
                            officeVerificationList.add(dbObj);
                        }
                    });
                    update.set("officeVerification", officeVerificationList);
                    break;
            }

            query.addCriteria(criteria);
            mongoTemplate.upsert(query, update, VerificationDetails.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving Verification Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving Verification Details {%s}", e.getMessage()));
        }

        return true;
    }

    private boolean deleteRvOvList(List<PropertyVerification> verificationList, boolean validAdd,PropertyVerification dbObj ) {
        for (PropertyVerification verification : verificationList) {
            if (StringUtils.equals(dbObj.getApplicantId(), verification.getApplicantId()) &&
                    StringUtils.equals(dbObj.getAgencyCode(), verification.getAgencyCode())) {
                validAdd = true;
                break;
            }
        }
        return validAdd;
    }

    @Override
    public  boolean updatePropertyVistApplicantName(String refId, String institutionId, Name appName, List<PropertyVisitDetails> propertyVisitDetailsList){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId)
                .and("institutionId").is(institutionId));
        Update update = new Update();
        update.set("applicantName", appName);
        update.set("propertyVisitDetailsList",propertyVisitDetailsList);
        mongoTemplate.upsert(query, update, PropertyVisit.class);
        return true;
    }


    @Override
    public Name fetchApplicantName(String refId){
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        query.fields().include("applicationRequest.request.applicant.applicantName");
        goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
        return goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName();
    }

    @Override
    public boolean saveValuationDetails(Valuation valuation, String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));
            Update update = new Update();
            update.set("valuationDetailsList", valuation.getValuationDetailsList());
            mongoTemplate.upsert(query, update, Valuation.class);
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;

        }
    }

    @Override
    public EMandateDetails fetchEMandateDetails(String refId){
        logger.debug("fetching EMandateDetails for refId {}", refId);
        try {
            Query query = new Query(Criteria.where("_id").is(refId));
            return mongoTemplate.findOne(query, EMandateDetails.class);
        }catch(Exception e){
            logger.error("Error occurred while fetching EMandateDetails for refId {}, Exception {} ", refId,  ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @Override
    public EMandateDetails fetchEMandateDetailsByAcknowledgementId(String acknowledgementId){
        logger.debug("fetching EMandateDetails for acknowledgementId {}", acknowledgementId);
        try {
            Query query = new Query(Criteria.where("acknowledgementId").is(acknowledgementId));
            return mongoTemplate.findOne(query, EMandateDetails.class);
        }catch(Exception e){
            logger.error("Error occurred while fetching EMandateDetails for acknowledgementId {}, Exception {} ", acknowledgementId,  ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @Override
    public void updateEMandateDetails(EMandateDetails eMandateDetails){
        logger.debug("updating EMandateDetails for refId {}", eMandateDetails.getRefId());
        try{
            Query query = new Query(Criteria.where("_id").is(eMandateDetails.getRefId()));
            Update update = new Update();
            update.set("header", eMandateDetails.getHeader());
            update.set("processedRequests",eMandateDetails.getProcessedRequests());
            update.set("acknowledgementId",eMandateDetails.getAcknowledgementId());
            update.set("emandateURL",eMandateDetails.getEmandateURL());
            update.set("emandateCallBackResult",eMandateDetails.getEmandateCallBackResult());
            update.set("emandateStatus", eMandateDetails.getEmandateStatus());
            update.set("resendCount", eMandateDetails.getResendCount());

            mongoTemplate.upsert(query,update,EMandateDetails.class);
        }catch(Exception e){
            logger.error("Error occurred while updating EMandateDetails for refId {}, Exception {} ", eMandateDetails.getRefId(),  ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public void saveEMandateDetailsCallLog(EMandateDetailsCallLog eMandateDetailsCallLog) {
        logger.debug("Saving EMandateDetailsCallLog for refId {} and requestType {}", eMandateDetailsCallLog.getRefId(), eMandateDetailsCallLog.getRequestType());
        try {
            /*if (!mongoTemplate.collectionExists(EMandateDetailsCallLog.class))
                mongoTemplate.createCollection(EMandateDetailsCallLog.class);*/
            mongoTemplate.insert(eMandateDetailsCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while saveEMandateDetailsCallLog for refId {} and requestType {}, Exception {} ", eMandateDetailsCallLog.getRefId(),  eMandateDetailsCallLog.getRequestType(), ExceptionUtils.getStackTrace(e));
        }
    }


    @Override
    public ListOfDocs fetchListOfDocs(String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, ListOfDocs.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error("Error occurred while fetching ListOfDocs with probable cause [{%s}] ", e.getMessage());
            return null;
            //throw new SystemException(String.format(" error occurred  while fetching ListOfDocs with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean updateMifinCallLog(MiFinCallLog miFinCallLog, String refId, String requestType) {
        logger.info("inside miFinCallLog for topup update data in MiFinCallLog  in SBFC_PL");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("requestType").is(requestType));
            Update update = new Update();
            update.set("dedupeResponseList", miFinCallLog.getDedupeResponseList());
            mongoTemplate.upsert(query, update, MiFinCallLog.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving MiFinCallLog " + e.getMessage());
            //e.printStackTrace();
            return false;
            //throw new SystemException(String.format("Exception caught while saving MiFinCallLog {%s}", e.getMessage()));
        }
    }


    @Override
    public boolean updateMifinCallLog(MiFinCallLog miFinCallLog, String refId, String requestType, String appId) {
        logger.info("inside miFinCallLog for topup update data in MiFinCallLog  in SBFC_PL");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("requestType").is(requestType).and("appId").is(appId));
            Update update = new Update();
            update.set("dedupeResponseList", miFinCallLog.getDedupeResponseList());
            update.set("miFinRequest", miFinCallLog.getMiFinRequest());
            mongoTemplate.upsert(query, update, MiFinCallLog.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving MiFinCallLog " + e.getMessage());
            return false;
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving MiFinCallLog {%s}", e.getMessage()));
        }
    }


    @Override
    public boolean saveCamSummaryDetails(ApplicationRequest applicationRequest, CamDetails camDetails) {
        logger.info("inside saveCamsummary for perpopulate in camsummary from Demographic in FiveStar");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(applicationRequest.getRefID())
                    .and("institutionId").is(applicationRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            update.set("summary", camDetails.getSummary());
            mongoTemplate.upsert(query, update, CamDetails.class);
            return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving camSummaryDetails " + e.getMessage());
            //e.printStackTrace();
            return  false;
            //throw new SystemException(String.format("Exception caught while saving camSummaryDetails {%s}", e.getMessage()));
        }

    }


    @Override
    public ApplicationRequest getApplicantData(String refId, String instId) {
        logger.debug("getApplicantData");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).
                    and("applicationRequest.header.institutionId").is(instId));
            GoNoGoCustomerApplication goCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
            return goCustomerApplication.getApplicationRequest();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("error while finding data against ref id" + e.getMessage());
        }
        return null;
    }

    @Override
    public boolean saveLegalVerificationDetailsForTopUp(LegalVerificationRequest legalVerificationrequest, String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId).and("institutionId").is(institutionId));
            Update update = new Update();
            update.set("legalVerificationDetailsList", legalVerificationrequest.getLegalVerification().getLegalVerificationDetailsList());
            mongoTemplate.upsert(query, update, LegalVerification.class);
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }

    }


    @Override
    public boolean saveCoOriginationDetails(String refId, String institutionId, CoOrigination coOrigination){
        logger.info("Inside saveCoOriginationDetails()");

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));
            Update update = new Update();
            update.set("coOriginationDetailsList", coOrigination.getCoOriginationDetailsList());
            update.set("roiMasterdata", coOrigination.getRoiMasterdata());
            update.set("misReleatedData", coOrigination.getMisReleatedData());
            update.set("populate", coOrigination.isPopulate());
            mongoTemplate.upsert(query, update, CoOrigination.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving save CoOrigination Details " + e.getMessage());
            //e.printStackTrace();
            //throw new SystemException(String.format("Exception caught while saving save CoOrigination Details {%s}", e.getMessage()));
        }

        return true;
    }

    @Override
    public boolean updateBranchData(String refId, Branch branchV2) {
        Query query = new Query();
        Update update = new Update();
        boolean updated = false;
        try{
            query.addCriteria(Criteria.where("_id").is(refId));
            update.set("applicationRequest.appMetaData.branchV2",branchV2);
            logger.info("update Branch Data Criteria   {},update data {}", query,update) ;
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            return writeResult.wasAcknowledged();
        }catch(Exception ex){
            logger.error("{} Exception occur while updating branch", ExceptionUtils.getStackTrace(ex));
        }
        return   updated;
    }

    @Override
    public boolean updateMifinData(String refId, String applicantCode, String prospectCode) {
        Query query = new Query();
        Update update = new Update();
        boolean updated = false;
        try{
            query.addCriteria(Criteria.where("_id").is(refId));
            update.set("applicationRequest.miFinProspectCode",prospectCode);
            update.set("applicationRequest.miFinCustomerCode",applicantCode);
            update.set("applicationRequest.currentStageId","DISB");
            update.set("applicationStatus","Disbursed");
            logger.info("update Branch Data Criteria   {},update data {}", query,update) ;
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            if(writeResult.wasAcknowledged()){
                updated = true;
            }else{
                updated = false;
            }
        }catch(Exception ex){
            logger.error("{} Exception occur while updating branch", ExceptionUtils.getStackTrace(ex));
        }
        return updated;
    }
    @Override
    public boolean saveInsuranceData(InsuranceRequest insuranceRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(insuranceRequest.getRefId()).and("institutionId").is(insuranceRequest.getHeader().getInstitutionId()));
        Update update = new Update();
        update.set("initiationPoint", insuranceRequest.getInsuranceData().getInitiationPoint());
        update.set("applicantList", insuranceRequest.getInsuranceData().getApplicantList());
        update.set("product", insuranceRequest.getHeader().getProduct());
        update.set("changedLoanAmount", insuranceRequest.getInsuranceData().getChangedLoanAmount());
        update.set("changedTenorAmount", insuranceRequest.getInsuranceData().getChangedTenorAmount());
        update.set("currentCnt", insuranceRequest.getInsuranceData().getCurrentCnt());
        mongoTemplate.upsert(query, update, InsuranceData.class);
        return true;
    }

    @Override
    public InsuranceData fetchInsuranceData(String refId) {
        Query query = new Query();
        InsuranceData insuranceData = null;
        query.addCriteria(Criteria.where("_id").is(refId));
        insuranceData = mongoTemplate.findOne(query, InsuranceData.class);
        return insuranceData;
    }

    @Override
    public boolean saveInsuranceData(InsuranceData insuranceData, String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        Update update = new Update();
        update.set("applicantList", insuranceData.getApplicantList());
        WriteResult result = mongoTemplate.upsert(query, update, InsuranceData.class);
        if(result.wasAcknowledged()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public FinbitData fetchFinbitData(String refId , String institutionId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                .and("institutionId").is(institutionId);
        query.addCriteria(criteria);
        logger.debug("Query for fetching finbit data() - {}", query.toString());
        return mongoTemplate.findOne(query, FinbitData.class);
    }

    @Override
    public FinbitData fetchFinbitData(String ackId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("statements.acknowledgementId").is(ackId);
        query.addCriteria(criteria);
        logger.debug("Query for fetching finbit data() - {}", query.toString());
        return mongoTemplate.findOne(query, FinbitData.class);
    }

    @Override
    public boolean saveFinbitData(FinbitRequest finbitRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(finbitRequest.getRefId()).and("institutionId").is(finbitRequest.getHeader().getInstitutionId()));
        Update update = new Update();
        update.set("statements", finbitRequest.getStatements());
        update.set("data", finbitRequest.getData());
        update.set("summaryData", finbitRequest.getSummaryData());
        update.set("product", finbitRequest.getHeader().getProduct());
        mongoTemplate.upsert(query, update, FinbitData.class);
        return true;
    }

    @Override
    public GoNoGoCustomerApplication fetchScoringData(String refId){
        Query query = new Query();
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        try{
            query.addCriteria(Criteria.where("_id").is(refId));
            query.fields().include("applicantComponentResponse.scoringServiceResponse");
            goNoGoCustomerApplication = mongoTemplate.findOne(query,GoNoGoCustomerApplication.class);
        }catch (Exception e){
            logger.error("{}",e.getStackTrace());
        }
        return goNoGoCustomerApplication;
    }

    @Override
    public GoNoGoCustomerApplication getGngApplication(String referenceId) throws Exception {
        logger.debug("getGoNoGoCustomerApplicationByRefId repo started for refId {}", referenceId);
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(referenceId));

        GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                .findOne(query, GoNoGoCustomerApplication.class);
        logger.debug("fetched gngApp for refId {}", referenceId);
        return goNoGoCustomerApplication;
    }

    @Override
    public boolean updateBureauCount(String refId, String institutionId, String appId) {
        logger.info("Inside updateBureauCount()");
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId));
            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query,GoNoGoCustomerApplication.class);
            Update update = new Update();

            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
            if(StringUtils.equalsIgnoreCase(applicant.getApplicantId(),(appId))) {
                applicant.setBureauHitCnt(0);
                applicant.setBureauMsg("");
            }
            else{
                List<CoApplicant> CoApplicants = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
                for(CoApplicant coApplicant : CoApplicants) {
                    if(StringUtils.equalsIgnoreCase(coApplicant.getApplicantId(),(appId))) {
                        coApplicant.setBureauHitCnt(0);
                        coApplicant.setBureauMsg("");
                    }
                }
            }
            update.set("applicationRequest.request", goNoGoCustomerApplication.getApplicationRequest().getRequest());
            mongoTemplate.updateFirst(query,update,GoNoGoCustomerApplication.class);

        } catch (DataAccessException e) {
            logger.error("Error while updating bureau count due to {}",e.getStackTrace());
            return false;
        }
        return true;
    }

    @Override
    public void deleteOCRData(OCRRequest ocrRequest) {
        logger.debug("deleteOCRData repo started");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(ocrRequest.getRefId())
                    .and("institutionId").is(ocrRequest.getHeader().getInstitutionId()));

            mongoTemplate.remove(query, OCRDetails.class);

        } catch (Exception e) {
            logger.error("Exception caught while deleting OCR response " + e.getStackTrace());
        }
    }

    @Override
    public OCRDetails getOCRData(OCRRequest ocrRequest) {
        logger.debug("getOCRData repo started");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(ocrRequest.getRefId())
                    .and("institutionId").is(ocrRequest.getHeader().getInstitutionId()));

            return mongoTemplate.findOne(query, OCRDetails.class);

        } catch (Exception e) {
            logger.error("Exception caught while geting OCR response " + e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("Exception caught while geting OCR {%s}", e.getMessage()));
        }
    }

    @Override
    public OCRDetails getOCRData(String refId , String institutionId) {
        logger.debug("getOCRData repo started");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, OCRDetails.class);
        } catch (Exception e) {
            logger.error("Exception caught while geting OCR response " + e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("Exception caught while geting OCR {%s}", e.getMessage()));
        }
    }

    @Override
    public void saveOCRData(OCRRequest ocrRequest) {
        logger.debug("saveOCRDeatils repo started");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(ocrRequest.getRefId())
                    .and("institutionId").is(ocrRequest.getHeader().getInstitutionId()));
            Update update = new Update();
            if(null != ocrRequest.getOcrDetails() && !CollectionUtils.isEmpty(ocrRequest.getOcrDetails().getOcrDetailsList())) {
                update.set("ocrDetailsList", ocrRequest.getOcrDetails().getOcrDetailsList());
            }
            if(StringUtils.isNotEmpty(ocrRequest.getInitiatedBy()))
                update.set("initiatedBy", ocrRequest.getInitiatedBy());
            if(StringUtils.isNotEmpty(ocrRequest.getInitiatedByRole()))
                update.set("initiatedByRole", ocrRequest.getInitiatedByRole());
            if(ocrRequest.getRcuSummary() != null)
                update.set("rcuSummary", ocrRequest.getRcuSummary());
            if(!ocrRequest.isResetStatus()) {
                update.set("agencyCode", ocrRequest.getAgencyCode());
                update.set("agencyName", ocrRequest.getAgencyName());
                update.set("agencyStatus", ocrRequest.getAgencyStatus());
                update.set("status", ocrRequest.getStatus());
            }
            mongoTemplate.upsert(query, update, OCRDetails.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving OCRDeatils response " + e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("Exception caught while saving OCRDeatils {%s}", e.getMessage()));
        }
    }

    @Override
    public boolean resetApplicationStatus(String referenceId, String institutionId,
                                          String applicantId, String bucket, String status) {

        try {

            if(StringUtils.isEmpty(bucket) && StringUtils.isEmpty(status)){
                return false;
            }

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));
            Update update = new Update();

            String currentStage = null;

            if(StringUtils.isNotEmpty(bucket)) {
                update.set("applicationBucket", bucket);
                if(!Buckets.isSTPBucket(bucket) && !Buckets.isDeclinedBucket(bucket))
                    update.set("applicationStatus", GNGWorkflowConstant.QUEUED.toFaceValue());
                currentStage = GngUtils.getCurrentStageBasedOnApplicationBucket(bucket);
            }

            if (StringUtils.isNotEmpty(status)) {
                update.set("applicationStatus", status);
                if (StringUtils.isEmpty(currentStage))
                    currentStage = GngUtils.getCurrentStageBasedOnApplicationStatus(status);
            }

            if (StringUtils.isNotEmpty(currentStage)) {
                update.set("applicationRequest.currentStageId", currentStage);
            }

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(query, GoNoGoCustomerApplication.class);

            updateApplicationInElasticsearch(goNoGoCustomerApplication);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while updating " +
                    "applicationStatus  for institutionId {} and refId {} with probable cause [{}] ", institutionId, referenceId, e.getMessage());

            return false;
        }
    }

    @Override
    public GoNoGoCustomerApplication fetchApplicantEligibility(String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        query.fields().include("applicationRequest.request.applicant").include("applicationBucket");
        logger.info("query {} ", query);
        return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean saveApplicantEligibility(String refId, Applicant applicant) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));
            Update update = new Update();
            update.set("applicationRequest.request.applicant", applicant);
            logger.debug("Query for saveApplicantEligibility() - {}", query.toString());
            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
        }catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while saveApplicantEligibility with probable cause [{%s}] ", e.getMessage());
            throw new SystemException(String.format(" error occurred  while saveApplicantEligibility with probable cause [{%s}]", e.getMessage()));
        }
        return true;
    }

    @Override
    public boolean saveMismatchDetails(MismatchRequest mismatchRequest,List<Mismatch> mismatchList){
        logger.info("Inside saveMismatchDetails()");
        String refId = mismatchRequest.getRefID();
        String institutionId=mismatchRequest.getHeader().getInstitutionId();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("header.institutionId").is(institutionId));
            Update update = new Update();
            update.set("mismatchList", mismatchList);
            update.set("header",mismatchRequest.getHeader());
            mongoTemplate.upsert(query, update, MismatchRequest.class);
        } catch (Exception e) {
            logger.error("Exception caught while saving save Mismatch Details " + e.getStackTrace());
        }
        return true;
    }

    @Override
    public MismatchRequest fetchMismatchDetails(MismatchRequest mismatchRequest) {
        logger.info("Inside fetchMismatchDetails()");
        String refId = mismatchRequest.getRefID();
        String institutionId = mismatchRequest.getHeader().getInstitutionId();
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(refId)
                .and("header.institutionId").is(institutionId);
        query.addCriteria(criteria);
        logger.debug("Query for fetching Mismatch data() - {}", query.toString());
        return mongoTemplate.findOne(query, MismatchRequest.class);
    }

    @Override
    public boolean updateApplicationBucket(String referenceId, String institutionId) {
        logger.debug("updateApplicationBucket called for refId {}", referenceId);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(referenceId)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));
            Update update = new Update();

            update.set("applicationBucket", Buckets.Bucket.B6.name());

            mongoTemplate.updateFirst(query, update,
                    GoNoGoCustomerApplication.class);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while updating " +
                    "applicationBucket  for institutionId {} and refId {} with probable cause [{}] ", institutionId, referenceId, e.getMessage());

            return false;
        }
    }

    @Override
    public GoNoGoCustomerApplication fetchApplicationBucket(String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        query.fields().include("applicationBucket");
        logger.info("query {} ", query);
        return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean saveFaceMatchData(FaceMatchRequest faceMatchRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(faceMatchRequest.getRefId()).and("header.institutionId").is(faceMatchRequest.getHeader().getInstitutionId()));
        Update update = new Update();
        update.set("header",faceMatchRequest.getHeader());
        update.set("faceMatchDetails", faceMatchRequest.getFaceMatchDetails());
        return mongoTemplate.upsert(query, update, FaceMatchRequest.class).wasAcknowledged();
    }

    @Override
    public FaceMatchRequest fetchFaceMatchData(String refId){
        logger.debug("fetching fetchFaceMatchData for refId {}", refId);
        try {
            Query query = new Query(Criteria.where("_id").is(refId));
            return mongoTemplate.findOne(query, FaceMatchRequest.class);
        }catch(Exception e){
            logger.error("Error occurred while fetching FaceMatchData for refId {}, Exception {} ", refId,  ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @Override
    public void updateMifinVerificationId(VerificationDetails verificationDetails) {
        logger.info("Inside updateVerificationId()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(verificationDetails.getRefId())
                    .and("institutionId").is(verificationDetails.getInstitutionId()));
            Update update = new Update();
            update.set("residenceVerification", verificationDetails.getResidenceVerification());
            update.set("officeVerification", verificationDetails.getOfficeVerification());
            mongoTemplate.upsert(query, update, VerificationDetails.class);
        } catch (Exception e) {
            logger.error("Exception caught while updating mifin verficationIds" + e.getStackTrace());
        }
    }

        @Override
        public MasterSchedulerConfiguration findMasterSchedularConfiguration(String institutionId, String masterName) {
            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("masterName").is(masterName)
                    .and("active").is(true));
            return mongoTemplate.findOne(query, MasterSchedulerConfiguration.class);

        }

        @Override
        public MasterSchedulerConfiguration findMasterSchedularConfigurationBasisOnInstId(String institutionId, String masterName) {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("masterName").is(masterName));
            return mongoTemplate.findOne(query, MasterSchedulerConfiguration.class);
    }

    @Override
    public List<ActivityLogs> fetchUserActivityLogsByRefId(String refId, String institutionId) {
        Query query = new Query();
        List<ActivityLogs> activityLogsList = null;
        try{
            query.addCriteria(Criteria.where("refId").is(refId).and("institutionId").is(institutionId));
            activityLogsList = mongoTemplate.find(query,ActivityLogs.class);
        }catch (Exception e){
            logger.error("Error in method fetchUserActivityLogsByRefId due to  {}",e.getStackTrace());
        }
        return activityLogsList;
    }

    @Override
    public boolean saveFosRemark(CroApprovalRequest croApprovalRequest) {
        logger.debug("saveFosRemark ApplicationMongoRepo started ");
        String croID = croApprovalRequest.getHeader().getCroId();
        Query query = new Query();
        try {
            query.addCriteria(Criteria.where("_id").is(croApprovalRequest.getReferenceId())
                    .and("applicationRequest.header.institutionId")
                    .is(croApprovalRequest.getHeader().getInstitutionId()));

            Update update = new Update();
            List<CroJustification> croJustifications = croApprovalRequest.getCroJustification();
            if (croJustifications != null) {
                for (CroJustification croJustification : croJustifications) {
                    croJustification.setCroID(croID);
                    croJustification.setDecisionCase("");
                    croJustification.setSubjectTo(Cache.getUserName(croApprovalRequest.getHeader().getInstitutionId(),
                            croApprovalRequest.getHeader().getLoggedInUserId()));
                    croJustification.setRole(croApprovalRequest.getHeader().getLoggedInUserRole());
                }
                //replacing the whole croJustification list with new list
                update.set("croJustification",croJustifications);
            }else {
                return false;
            }
            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            logger.debug("saveFosRemark ApplicationMongoRepo Ended ");
            return  true;
        }catch (Exception e){
            logger.error("Error occurred while saving " +
                    "saveFosRemark  with probable cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    public List<GoNoGoCustomerApplication> findGNGRefIdListByLastUpdatedDate(String institutionId, Date startDate, Date endDate) {
        logger.debug("findGNGCustApplRefIdByLastUpdatedDate repo started for startDate {} & endDate {} ", startDate, endDate);
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
            if(startDate != null && endDate != null){
                criteria.and("lastUpdatedDate").gte(startDate).lte(endDate);
            } else if (startDate != null){
                criteria.and("lastUpdatedDate").gte(startDate);
            } else if(endDate != null){
                criteria.and("lastUpdatedDate").lte(endDate);
            }
            query.addCriteria(criteria);
            query.fields().include("_id");
            query.fields().include("applicationRequest.header");

            return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        }catch (Exception e){
            logger.error("Error occurred while fetching findGNGRefIdListByLastUpdatedDate for startDate {} & endDate {} , Exception {} ", startDate, endDate,  ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @Override
    public List<SmsServiceReqResLog> fetchSmsServiceLog(String identifier) {
        if(StringUtils.isBlank(identifier))
            return new LinkedList<>();

        return mongoTemplate.find(new Query().addCriteria(Criteria.where(GoNoGoCustomerApplicationKeyConstants.IDENTIFIER).is(identifier)), SmsServiceReqResLog.class);
    }

    @Override
    public void saveVerificationOtp(OtpVerificationData otpVerificationData) {
        mongoTemplate.save(otpVerificationData);
    }

    @Override
    public String isValidOtp(String institutionId, String uuid, String otp) {
        String result = "";

        Criteria criteria = Criteria.where("_id").is(uuid).and("institutionId").is(institutionId);
        OtpVerificationData otpVerificationObj = mongoTemplate.findOne(new Query().addCriteria(criteria), OtpVerificationData.class);
        if(Objects.nonNull(otpVerificationObj)){

            if(otpVerificationObj.getRetry() < 1){
                otpVerificationObj.setStatus(Constant.EXPIRED);
                result = ErrorCode.OTP_VERIFICATION_LIMIT_EXCEEDED;
            }else if(StringUtils.equalsIgnoreCase(otpVerificationObj.getOtp(), otp)){
                otpVerificationObj.setStatus(Constant.VERIFIED);
                result = Constant.VERIFIED;
            }else{
                otpVerificationObj.setRetry(otpVerificationObj.getRetry() - 1);
                result = ErrorCode.INVALID_OTP;
            }

            mongoTemplate.save(otpVerificationObj);

        }else{
            result = "OTP has not been sent, please Re-Send the OTP.";
        }
        return result;
    }
}
