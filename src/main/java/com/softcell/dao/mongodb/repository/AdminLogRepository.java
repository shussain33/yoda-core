package com.softcell.dao.mongodb.repository;

import com.softcell.config.ReportEmailConfiguration;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.DigitizationDealerEmailMaster;
import com.softcell.gonogo.model.masters.GNGDealerEmailMaster;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.gonogo.model.request.AdminLogRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;

import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */

public interface AdminLogRepository {

    /**
     * @param refID
     * @return
     */
    PanRequest getPanLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    ScoringApplicationRequest getScoringLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    RequestJsonDomain getMbLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    List<RequestJsonDomain> getMbLogWithCoapplicant(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    ApplicationRequest getMainLog(String refID) throws Exception;

    /**
     * @param dealerID
     * @return
     */
    List<DealerEmailMaster> getDealerEmailMapping(String dealerID) throws Exception;

    /**
     * @return
     */
    @Deprecated
    List<ApplicationRequest> getMainAllLog() throws Exception;

    /**
     * @return
     */
    @Deprecated
    List<GoNoGoCroApplicationResponse> getAllData() throws Exception;

    /**
     * @return
     */
    GoNoGoCroApplicationResponse getSingleBucketData(String refID) throws Exception;

    /**
     * @return
     */
    List<OtpLog> getLogData() throws Exception;

    /**
     * Temporary patch to update age
     */
    @Deprecated
    boolean updateAgeInAllDocuments() throws Exception;

    /**
     * Temporary patch to update deleted flag
     *
     * @return
     */
    @Deprecated
    boolean updateDeletedFlagOfAllDocuments() throws Exception;

    /**
     * @param getFileRequest
     * @return
     */
    boolean saveEmailLog(GetFileRequest getFileRequest) throws Exception;

    /**
     * @param getFileRequest
     * @return
     */
    boolean checkEmailDedupe(GetFileRequest getFileRequest) throws Exception;

    /**
     * @param reportType
     * @param institutionID
     * @return
     */
    @Deprecated
    List<ReportEmailConfiguration> getReportEmailConfiguration(String reportType, String institutionID) throws Exception;

    /**
     * @param refID
     * @return
     */
    AadhaarRequest getAadharLog(String refID) throws Exception;

    /**
     * @param productID
     * @param institutionID
     * @return
     */
    List<GNGDealerEmailMaster> getGngDealerEmailMapping(String productID, String institutionID) throws Exception;

    /**
     * @param refID
     * @param vendor
     * @return
     */
    List<SerialNumberInfo> getSerialByIDLog(String refID, String vendor) throws Exception;

    /**
     * @return
     */
    List<SerialNumberInfo> getSerialLog() throws Exception;

    /**
     * @return
     */
    List<SerialSaleConfirmationRequest> getSerialNumberRequestLog() throws Exception;

    /**
     * @param refID
     * @param vendor
     * @return
     */
    List<SerialSaleConfirmationRequest> getSerialNumberRequestLog(String refID, String vendor) throws Exception;

    /**
     * @return
     */
    List<EmailTemplateDetails> getMailLogReport() throws Exception;

    /**
     * @param refID
     * @return
     */
    List<EmailTemplateDetails> getMailLogReport(String refID) throws Exception;

    /**
     * @return
     */
    boolean updateStageAndStatus(String refID ,String appStatus ,String appStage) throws Exception;

    /**
     * @param dealerID
     * @param institutionId
     * @return
     */
    List<DealerEmailMaster> getDealerEmailMapping(String dealerID, String institutionId) throws Exception;

    /**
     * @param refID
     * @param institutionId
     * @return
     */
    ApplicationRequest getMainLog(String refID, String institutionId) throws Exception;

    /**
     *
     * @param refID
     * @param institutionId
     * @return
     */
    String getProcessedApplication(String refID, String institutionId);

    /**
     *
     * @param refId
     * @return
     */
    boolean updateDealerName(String refId) throws Exception;

    /**
     *
     * @param salesForceLog
     * @return
     */
    SalesForceLog getSalesForceLog(SalesForceLog salesForceLog);

    /**
     *
     * @param getImageRequest
     * @return
     * @throws Exception
     */
    boolean getEmailStatus(GetFileRequest getImageRequest) throws Exception;

    /**
     *
     * @param startDate
     * @param endDate
     * @param institutionId
     * @return
     */
    List<GoNoGoCustomerApplication> getApplicationBetweenFromAndToDateForDeclinePurpose(Date startDate , Date endDate ,String institutionId) throws Exception;


    /**
     *
     * @param date
     * @param institutionId
     * @return
     */
    List<GoNoGoCustomerApplication> getApplicationLessThanDateForDeclinePurpose(Date date,String institutionId ,int limit ,int skip) throws Exception;

    /**
     *
     * @param declineApplicationDetails
     */
    void saveDeclineApplicationLog(DeclineApplicationDetails declineApplicationDetails);

    /**
     *
     * @param adminLogRequest
     * @return
     */
    List<RawResponseLog> getRawResponse(AdminLogRequest adminLogRequest);

    /**
     *
     * @param startDate
     * @param institutionId
     * @param limit
     * @param skip
     * @return
     */
    List<GoNoGoCustomerApplication> getApplicationLessThanDateForCancelPurpose(Date startDate, String institutionId, int limit, int skip) throws Exception;

    /**
     *
     * @param cancelApplicationDetails
     */
    void saveCancelledApplicationLog(CancelApplicationDetails cancelApplicationDetails);

    /**
     *
     * @param cancelDoNotRaisedApplicationRequest
     * @param limit
     * @param skip
     * @return
     */
    List<GoNoGoCustomerApplication> getApplicationBetweenToAndFromDate(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest, int limit, int skip) throws Exception;

    /**
     *
     * @param dealerID
     * @param institutionId
     * @return
     */
    DigitizationDealerEmailMaster getEmailCcIdFromDigiEmailMaster(String dealerID, String institutionId) throws Exception;
}
