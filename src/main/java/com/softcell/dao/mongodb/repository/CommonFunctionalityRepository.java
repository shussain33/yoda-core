package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.CollectionConfig;
import com.softcell.gonogo.model.request.CollectionDataRequest;

import java.util.List;

public interface CommonFunctionalityRepository {

    List<?> fetchCollectionData (Class<?> className, CollectionDataRequest request);

    boolean saveDataInCollection(Object obj, String CollectionName);

    boolean insertCollectionConfig(CollectionConfig config);

    List<CollectionConfig> fetchCollectionConfig();
}
