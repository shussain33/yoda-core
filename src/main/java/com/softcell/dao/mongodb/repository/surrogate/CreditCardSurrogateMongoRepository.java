package com.softcell.dao.mongodb.repository.surrogate;

import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogatEntries;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

/**
 * @author yogeshb
 */
@Repository
public class CreditCardSurrogateMongoRepository implements
        CreditCardSurrogateRepository {
    Logger logger = LoggerFactory
            .getLogger(CreditCardSurrogateMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public CreditCardSurrogateMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public boolean saveCreditCardSurrogateTransaction(
            CreditCardSurrogatEntries creditCardSurrogatEntries) {
        try {
            /*if (!mongoTemplate
                    .collectionExists(CreditCardSurrogatEntries.class)) {
                mongoTemplate.createCollection(CreditCardSurrogatEntries.class);
            }*/
            mongoTemplate.insert(creditCardSurrogatEntries);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    public boolean isExist(CreditCardSurrogateRequest creditCardSurrogateRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria
                    .where("creditCardSurrogateRequest.creditCardNumber")
                    .is(creditCardSurrogateRequest.getCreditCardNumber())
                    .and("creditCardSurrogateResponse.success").is("true"));
            return mongoTemplate.exists(query, CreditCardSurrogatEntries.class);
        } catch (Exception exp) {
            logger.error("" + exp);
            return false;
        }

    }

    @Override
    public CreditCardSurrogateResponse getCreditCardSurrogateDetails(
            CreditCardSurrogateRequest creditCardSurrogateRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria
                    .where("creditCardSurrogateRequest.creditCardNumber")
                    .is(creditCardSurrogateRequest.getCreditCardNumber())
                    .and("creditCardSurrogateResponse.success").is("true"));
            CreditCardSurrogatEntries creditCardSurrogatEntries = mongoTemplate
                    .findOne(query, CreditCardSurrogatEntries.class);
            return creditCardSurrogatEntries.getCreditCardSurrogateResponse();
        } catch (Exception exp) {
            logger.error("" + exp);
            return null;
        }
    }
}
