package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.request.CollectionConfigRequest;
import com.softcell.gonogo.model.request.CollectionDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface CommonFunctionalityService {

    BaseResponse saveData(CollectionDataRequest collectionDataRequest);

    BaseResponse getData(CollectionDataRequest collectionDataRequest);

    BaseResponse saveCollectionConfiguration(CollectionConfigRequest request);

    BaseResponse getCollectionConfiguration();

    BaseResponse getDownloadedJsonData(CollectionDataRequest collectionDataRequest);
}
