package com.softcell.dao.mongodb.repository.workflow;

import com.mongodb.WriteResult;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.nextgen.domain.WFJobCommDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by prateek on 13/2/17.
 */
@Repository
public class WorkFlowCommunicationRepo implements WorkflowJobCommunication {

    private static final Logger logger = LoggerFactory.getLogger(WorkFlowCommunicationRepo.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public WorkFlowCommunicationRepo(){
        if(null==mongoTemplate){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
    }

    @Override
    @CachePut("workflowJobCommDomain")
    public List<WFJobCommDomain> findByInstitutionId(String institutionId) throws SystemException {

        logger.debug("Finding WFJobCommDomain using institutionId {} ", institutionId);

        Query query = new Query();

        query.addCriteria(Criteria.where(INSTITUTION_ID).is(institutionId));

        return mongoTemplate.find(query, WFJobCommDomain.class);

    }

    @Override
    @CachePut("workflowJobCommDomain")
    public List<WFJobCommDomain> findByInsNProduct(@NotNull String institutionId, @NotNull String productName) throws SystemException {


        logger.debug("find WFJobCommDomain using institutionId {} and productName {}", institutionId, productName);

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId),
                Criteria.where(PRODUCT_NAME).is(productName)));

        return mongoTemplate.find(query, WFJobCommDomain.class);

    }

    @Override
    public List<WFJobCommDomain> findAll() throws SystemException {

        logger.debug("finding all WFJobCommDomain exist in application ");

        return mongoTemplate.findAll(WFJobCommDomain.class);

    }

    @Override
    @CacheEvict(value = "workflowJobCommDomain" , allEntries = true)
    public void deleteByInstitutionId(String institutionId) throws SystemException {

        logger.debug("deleting domain WFJobCommDomain having institutionId {} ", institutionId);

        Query query = new Query();

        query.addCriteria(Criteria.where(INSTITUTION_ID).is(institutionId));

        mongoTemplate.remove(query, WFJobCommDomain.class);
    }

    @Override
    @CacheEvict(value = "workflowJobCommDomain" , allEntries = true)
    public Boolean deleteByInstNProd(String institutionId, String productName) throws SystemException {

        logger.debug("deleting domain WFJobCommDomain having institutionId {} and productName {} ", institutionId, productName);

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(INSTITUTION_ID).is(institutionId),
                Criteria.where(PRODUCT_NAME).is(productName))
        );

        WriteResult remove = mongoTemplate.remove(query, WFJobCommDomain.class);

        return remove.wasAcknowledged();

    }

    @Override
    public void saveByInstitutionId(@NotNull WFJobCommDomain wfJobCommDomain) {
        logger.debug("saving WfJobCommunication domain with values {} ", wfJobCommDomain);
        mongoTemplate.insert(wfJobCommDomain);

    }

    @Override
    public void findAndUpdate(@NotNull WFJobCommDomain wfJobCommDomain) throws SystemException {

        Update  update = new Update();

        update.set(INSTITUTION_ID, wfJobCommDomain.getInstitutionId()).
                set(PRODUCT_NAME, wfJobCommDomain.getProductName()).
                set(TYPE, wfJobCommDomain.getType()).
                set(IS_ENABLED, Boolean.TRUE);



        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(INSTITUTION_ID).is(wfJobCommDomain.getInstitutionId()),
                Criteria.where(PRODUCT_NAME).is(wfJobCommDomain.getProductName()),
                Criteria.where(TYPE).is(wfJobCommDomain.getType()),
                Criteria.where(IS_ENABLED).is("true")
        ));

        WriteResult writeResult = mongoTemplate.updateFirst(query, update, WFJobCommDomain.class);

        logger.debug("write result while updating a document is {}", writeResult);

    }

    @Override
    public void deleteWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException {

        logger.debug("deleting domain WFJobCommDomain  {} ", wfJobCommDomain);

        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(
                Criteria.where(INSTITUTION_ID).is(wfJobCommDomain.getInstitutionId()),
                Criteria.where(PRODUCT_NAME).is(wfJobCommDomain.getProductName()),
                Criteria.where(TYPE).is(wfJobCommDomain.getType())
        ));

         mongoTemplate.remove(query, WFJobCommDomain.class);


    }

    @Override
    public WFJobCommDomain getMultibureauCommDomain(String institutionId, String type) {

        logger.debug("getting domain MultibureauCommDomain having institutionId {} and productName {} ", institutionId, type);

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId).and(TYPE).is(type)));

        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public WFJobCommDomain getMultibureauCommDomain(String institutionId, String product, String type) {
        logger.debug("getting domain MultibureauCommDomain having institutionId {} and productName {} and type {} ", institutionId, product, type);

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId).and(TYPE).is(type).and(PRODUCT_NAME).is(product)));

        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public List<WFJobCommDomain> getServices(String institutionId, String type, String product) {
        logger.debug("getting domain MultibureauCommDomain having institutionId {} and productName {} and type {}", institutionId,product, type);

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId).and(TYPE).is(type).and(PRODUCT_NAME).is(product)));

        List<WFJobCommDomain> wfJobCommDomain = mongoTemplate.find(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public WFJobCommDomain getMultibureauCommDomain(String institutionId, String product, String serviceId, String type) {
        Query query = new Query();
        if (product != null) {
            query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId)
                    .and(PRODUCT_NAME).is(product)
                    .and(SERVICE_ID).is(serviceId)
                    .and(TYPE).is(type).and(IS_ENABLED).is(true)));
        }
        else {
            query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId)
                    .and(SERVICE_ID).is(serviceId)
                    .and(TYPE).is(type).and(IS_ENABLED).is(true)));
        }
        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public WFJobCommDomain getConfigFromIsEnabledFlag(String institutionId, String type) {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId).and(TYPE).is(type).and(IS_ENABLED).is(true)));

        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public WFJobCommDomain getConfigForProduct(String institutionId, String type, String product) {
        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId).and(PRODUCT_NAME).is(product).and(TYPE).is(type).and(IS_ENABLED).is(true)));
        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }

    @Override
    public WFJobCommDomain getMultibureauCommDomainByMemberId(String institutionId, String product, String memberId, String type) {
        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(Criteria.where(INSTITUTION_ID).is(institutionId)
                .and(PRODUCT_NAME).is(product)
                .and(MEMBER_ID).is(memberId)
                .and(TYPE).is(type).and(IS_ENABLED).is(true)));
        WFJobCommDomain wfJobCommDomain = mongoTemplate.findOne(query, WFJobCommDomain.class);

        return wfJobCommDomain;
    }
}
