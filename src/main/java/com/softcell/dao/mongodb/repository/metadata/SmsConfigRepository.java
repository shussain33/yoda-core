package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.config.sms.GngSmsServiceConfiguration;

import java.util.List;

/**
 * This dao layer will be used to do CRUD operations
 * related to sms config meta data
 *
 * @author bhuvneshk
 */
public interface SmsConfigRepository {

    public Boolean insertSmsConfiguration(GngSmsServiceConfiguration smsConfiguration);

    public Boolean updateSmsConfiguration(GngSmsServiceConfiguration smsConfiguration);

    public Boolean deleteSmsConfiguration(GngSmsServiceConfiguration smsConfiguration);

    public List<GngSmsServiceConfiguration> readSmsConfigByInstitution(String institutionId);

    public List<GngSmsServiceConfiguration> readActiveSmsConfigByInstitution(String institutionId);

    public boolean isSmsConfigExist(GngSmsServiceConfiguration smsConfigurations);
}
