package com.softcell.dao.mongodb.repository.workflow;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.nextgen.domain.WFJobCommDomain;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * Created by prateek on 13/2/17.
 */
public interface WorkflowJobCommunication {


    String Id = "id";

    String INSTITUTION_ID = "institutionId";

    String PRODUCT_NAME = "productName";

    String AGGREGRATOR_Id = "aggregratorId";

    String MEMBER_ID = "memberId";

    String PASSWORD = "password";

    String BASE_URL = "baseUrl";

    String END_POINT = "endPoint";

    String NO_OF_RETRY = "noOfRetry";

    String SERVICE_ID = "serviceId";

    String VERSION = "version";

    String IS_ENABLED = "isEnabled";

    String TYPE = "type";

    String KEYS = "keys";

    /**
     * @param institutionId
     * @return
     * @throws Exception
     */
    List<WFJobCommDomain> findByInstitutionId(@NotNull   final String institutionId) throws SystemException;

    /**
     * @param institutionId
     * @param productName
     * @return
     * @throws Exception
     */
    List<WFJobCommDomain> findByInsNProduct(String institutionId, String productName) throws SystemException;

    /**
     * @return
     * @throws Exception
     */
    List<WFJobCommDomain> findAll() throws SystemException;

    /**
     * @param institutionId
     * @return
     * @throws Exception
     */
    void deleteByInstitutionId(String institutionId) throws SystemException;

    /**
     * @param institutionId
     * @param productName
     * @return
     * @throws Exception
     */
    Boolean deleteByInstNProd(String institutionId, String productName) throws SystemException;

    /**
     * @param wfJobCommDomain
     * @throws Exception
     */
    void saveByInstitutionId(WFJobCommDomain wfJobCommDomain) throws SystemException;

    /**
     * @param wfJobCommDomain
     * @throws Exception
     */
    void findAndUpdate(WFJobCommDomain wfJobCommDomain) throws SystemException;


    void deleteWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException;

    WFJobCommDomain getMultibureauCommDomain(String institutionId , String type);

    WFJobCommDomain getMultibureauCommDomain(String institutionId , String product, String type);

    List<WFJobCommDomain> getServices(String institutionId, String type, String product);

    WFJobCommDomain getMultibureauCommDomain(String institutionId, String product, String serviceId, String type);
    WFJobCommDomain getConfigFromIsEnabledFlag(String institutionId , String type);

    WFJobCommDomain getConfigForProduct(String institutionId, String type, String product);

    WFJobCommDomain getMultibureauCommDomainByMemberId(String institutionId, String product, String memberId, String type);
}
