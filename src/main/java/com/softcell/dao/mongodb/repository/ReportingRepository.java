package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.softcell.config.reporting.GoNoGoSystemFields;
import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLog;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.LosUtrDetailsMaster;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.SerialNumberInfoLogRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorLog;
import com.softcell.reporting.domains.CustomReport;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.ReportDetail;
import com.softcell.reporting.domains.SalesReportResponse;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author kishorp
 */

/**
 * Interface for operation on reporting module
 *
 * @author prateek
 */
public interface ReportingRepository {

    /**
     * @param reportingModuleConfiguration Add new reporting app, which will used to add
     *                                     reporting app.
     * @return
     */
    boolean saveReportingConf(ReportingModuleConfiguration reportingModuleConfiguration);

    /**
     * @return List of all recipient to send report as per app.
     */
    List<ReportingModuleConfiguration> getRepotingConfig();

    /**
     * @return Add reporting app from csv file. which will used to add
     * reporting app.
     */
    //TODO Need to shift to FileUpload
    boolean saveReportingConfigFromCSVFile();

    /**
     * @param flatReportConfiguration
     * @param query
     * @return
     */
    CustomReport getCustomReport(FlatReportConfiguration flatReportConfiguration, Query query);

    /**
     * @param query Query is used to get diamension of <em> GoNoGo System
     *              <em> which will filter by institution and version optional.
     * @return all fields list of GoNoGo system.
     */
    GoNoGoSystemFields getGoNoGoDimensions(Query query);

    /**
     * @param query
     * @return
     */
    FlatReportConfiguration getFlatReportConfiguration(Query query);

    /**
     * @param gssoNoGoSystemFields all fields list of GoNoGo system.
     * @return true is transaction is successful.
     */
    boolean saveGoNoGoSystemFields(GoNoGoSystemFields gssoNoGoSystemFields);

    /**
     * @param reportingConfigurations
     * @return
     */
    boolean saveCustomReportConfiguration(ReportingConfigurations reportingConfigurations);

    /**
     * @param query get all report by institution ids.
     * @return return all report list which is define for particular
     * institutions. Max cap is 100.
     */
    List<ReportingConfigurations> reportSystemConfiguration(Query query);


    /**
     * @param query
     * @return
     */
    ReportingConfigurations reportConfiguration(Query query);


    /**
     * @param query Query to Find ReportName in Database.
     * @return all fields list of GoNoGo system.
     */
    List<ReportDetail> searchReportByName(Query query);

    /**
     * @param query query to  select  record.
     * @return all customer application of match with query.
     */
    Stream<GoNoGoCustomerApplication> getCustomerApplication(Query query);

    /**
     *
     * @param query
     * @return
     */
    Stream<GoNoGoCustomerApplication> getCustomerApplicationOld(Query query);

    /**
     * @param aggregation
     * @return
     */
    Stream salesIncentiveReport(TypedAggregation<SalesReportResponse> aggregation) throws Exception;


    /**
     * @param query
     * @return
     * @throws Exception{ }
     */
    Stream<GoNoGoCustomerApplication> zippedSalesReport(Query query) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    AggregationResults<BasicDBObject> otpReport(Aggregation aggregation) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    Stream<BasicDBObject> firstLastLoginReport(TypedAggregation aggregation) throws Exception;


    /**
     * @param query
     * @return
     * @throws Exception
     */
    PostIpaRequest getPostIpaRequest(Query query) throws Exception;

    /**
     * @param query
     * @return
     * @throws Exception
     */
    List<GoNoGoCustomerApplication> zippedCreditReport(Query query) throws Exception;


    /**
     * @param manufacturerSet
     * @return
     */
    Map<String, Set<String>> getCategoryAgainstManufacturer(
            Set<String> manufacturerSet);

    /**
     * @param ctg
     * @param name
     * @return
     */
    Set<String> getMake(String ctg, String name);

    /**
     * @param name
     * @param make
     * @param manufacturer
     * @return
     */
    Set<String> getModels(String name, String make, String manufacturer);


    /**
     * @return
     */
    List<SerialNumberInfo> getSerialNumberLog();

    /**
     * @param query
     * @return
     */
    List<SerialNumberInfo> getSerialNumberByIdOrVendorLog(Query query);

    /**
     * @param query
     * @return
     */
    SerialNumberInfo fetchSerialNumberByRefIdNStatus(Query query);

    /**
     *
     * @param query
     * @return
     */
    DealerEmailMaster fetchDealerEmailMasterBasedOnDealerId(Query query);

    /**
     *
     * @param institutionId
     * @return
     */
    Set<String> getAssetModelMasterManufacturer(String institutionId);

    /**
     *
     * @return
     */
    List<SerialSaleConfirmationRequest> getSerialNumberRequestLog();

    /**
     *
     * @param refID
     * @param vendor
     * @return
     */
    List<SerialSaleConfirmationRequest> getSerialNumberRequestLog(String refID, String vendor);

    /**
     *
     * @param refID
     * @return
     */
    List<EmailTemplateDetails> getMailLogReport(String refID);

    /**
     *
     * @return
     */
    List<EmailTemplateDetails> getMailLogReport();

    /**
     *
     * @param institutionId
     * @return
     */
    List<LosUtrDetailsMaster> getLosUtrMaster(String institutionId);

    /**
     *
     * @param serialNumberInfoLogRequest
     * @return
     */
    List<RollbackFeatureLog> getSerialNumberRollBackLogByVendor(SerialNumberInfoLogRequest serialNumberInfoLogRequest);

    /**
     *
     * @param refId
     * @return
     */
    PostIpaRequest getPostIpaByRefId(String refId);

    /**
     *
     * @param configuration
     * @return
     */
    CloseableIterator<GoNoGoCustomerApplication> getGoNoGoCustomerApplicationForTVR(ReportingModuleConfiguration configuration);

    /**
     *
     * @param query
     * @return
     */
    List<GoNoGoCustomerApplication> getGoNoGoCustomerApplicationForCredit(Query query);

    /**
     *
     * @param id
     * @return
     */
    SerialNumberInfo getSerialNoByIdAndStatus(String id);

    /**
     *
     * @param query
     * @return
     */
    SerialNumberInfo getSerialNoInfo(Query query);


    FileHandoverDetails getFileHandOverDetails(Query query);

    /**
     *
     * @param dealerId
     * @return
     */
    DealerEmailMaster getDealerEmailMasterById(String dealerId);

    /**
     *
     * @param query
     * @return
     */
    List<ApplicationRequest> getDEStageRecords(Query query);

    /**
     *
     * @param aadhaarLogRequest
     * @return
     */
    Stream<AadharLog> findAadhaarLogs(AadhaarLogRequest aadhaarLogRequest);

    /**
     * @param query
     * @return
     */
    FileHandoverDetails getFilehandOverDetails(Query query);

    /**
     *
     * @param dealerId
     * @return
     */
    DealerBranchMaster getBranchMaster(String dealerId,String institutionId);

    /**
     *
     * @param dmzRequest
     * @return
     */
    List<MbPushResponse> getMbPushResponseLogs(DmzRequest dmzRequest);

    /**
     *
     * @param refId
     * @return
     */
    List<TVRLogs> getTvrLogs(String refId);

    /**
     * @param query
     * @return
     */
    List<AccountNumberInfo> getAccountNumberLog(Query query);

    /**
     * @param query
     * @return
     */
    List<SerialNumberApplicableVendorLog> getSerialNumberApplicableVendorLog(Query query);

    /**
     *
     * @param query
     * @return
     */
    List getSBFCLMSLogByRefId(Query query);

    /**
     *
     * @param query
     * @return
     */
    List<SBFCLMSIntegrationLog> getSBFCLMSLog(Query query);
}
