/**
 * kishorp8:35:37 PM  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.masters.BureauStates;

import java.util.Map;

/**
 * @author kishorp
 *
 */
public interface ApplicationMetaDataRepository {
    /**
     *
     * @return
     */
    public Map<String, BureauStates> getBureauStateMap();

    /**
     *
     * @param bureauStates
     * @return
     */
    public boolean saveBureauStateMap(BureauStates bureauStates);

}
