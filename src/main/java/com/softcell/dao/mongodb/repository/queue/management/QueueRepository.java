/**
 * kishorp11:57:27 PM  Copyright Softcell Technolgy
 */
package com.softcell.dao.mongodb.repository.queue.management;

import com.mongodb.WriteResult;
import com.softcell.constants.Product;
import com.softcell.constants.SequenceType;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.PropertyVisit;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.gonogo.model.queueMgmt.CroQueueTracking;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.InvoiceDetails;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.ApplicationResponse;
import com.softcell.gonogo.model.response.CroQueue;
import com.softcell.gonogo.model.response.PosidexResponse;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.component.finished.AuditData;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author amit
 */
public interface QueueRepository {

    /**
     * Saves Cro work for auditing purpose.
     * @param croAudit
     */
    void saveCroAudit(CroAudit croAudit);


    List<CroAudit> getCroAudit(String userId, String institutionId) throws  Exception;

    List<CroAudit> getCroAudit(String userId, String institutionId, Date fromDate, Date toDate) throws  Exception;

    /**
     * Fetches CRO's statistics for today.
     * @param userId
     * @param institutionId
     * @return
     */
    Map<String,Long> fetchCroStatistics(String userId, String institutionId) throws  Exception;

    /**
     * Fetches CRO statistics for the period mentioned
     * @param userId
     * @param institutionId
     * @param fromDate
     * @param toDate
     * @return
     */
    Map<String,Long> fetchCroStatistics(String userId, String institutionId, Date fromDate, Date toDate) throws  Exception;


    public boolean saveActivityLog(List<ActivityLogs> activityLogs) throws Exception;


    List<String> fetchQueuedCaseIds(int casesPerUser, String institutionId, String role, RequestCriteria criteria, int skip);

    void updateCroQueueTracking(List<RealtimeUserStatus> userStatuses, List<Map<String, Object>> fieldValueMapList);

    public void updateCroQueueTracking(String userId, String institutionId, Map<String, Object> fieldValueMap);

    List<GoNoGoCustomerApplication> getAllUnassignedApplications();

    List<GoNoGoCustomerApplication> getAssignedCases(String institutionId);

    List<CroQueueTracking> getCroQueueTrackingList(String institutionId, List<String> users, boolean includeFlag);

    boolean isApplicationUnassigned(String refId, String userId);

    void updateOperator(String userId, List<String> refId);
}
