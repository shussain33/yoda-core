package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;

import java.util.Set;

public interface MultibureauRepository {

    /**
     * This method is to get co-applicant id for MB application based on
     * applicant id
     *
     * @param applicantId
     * @return
     */
    Set<String> getCoApplicantIds(String applicantId);

    /**
     * This method is to get coapplicant multi bureau response based on applicantid
     *
     * @param applicantId
     * @return
     */
    MultiBureauCoApplicantResponse getCoApplicantMBResponse(String ref, String applicantId) throws Exception;
}
