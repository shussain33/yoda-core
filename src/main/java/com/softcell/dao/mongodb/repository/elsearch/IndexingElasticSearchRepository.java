package com.softcell.dao.mongodb.repository.elsearch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ReportingMongoDBRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.service.factory.ESEntityBuilder;
import com.softcell.gonogo.service.factory.impl.EsEntityBuilderImpl;
import com.softcell.queuemanager.config.EsConnectionConfig;
import com.softcell.queuemanager.indexing.QueueIndexWriter;
import com.softcell.queuemanager.search.request.IndexingRequest;
import com.softcell.utils.GngDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;


/**
 * @author kishorp
 */
@Repository
public class IndexingElasticSearchRepository implements IndexingRepository {

    private static final Logger logger = LoggerFactory.getLogger(IndexingElasticSearchRepository.class);


    private ESEntityBuilder esEntityBuilder;


    @Override
    public boolean indexNotification(GoNoGoCustomerApplication customerApplication) {

        logger.debug("Indexing repo invoked to build notification index");

        esEntityBuilder = new EsEntityBuilderImpl();

        QueueIndexWriter queueIndexWriter = new QueueIndexWriter();

        try {

            IndexingRequest indexingRequest = esEntityBuilder.buildNotificationIndexRequest(customerApplication);

            EsConnectionConfig elasticSearchConfig = Cache.URL_CONFIGURATION.getEsConnectionConfig().get("DEFAULT");

            if (elasticSearchConfig == null) {


                logger.warn("elastic search communication domain is not registered");

                return false;

            }

            boolean status = queueIndexWriter.httpCroQueue(indexingRequest, elasticSearchConfig);

            logger.debug("Indexing repo completed with notification index build status {}", status);

            return status;

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Error in Indexing repo while building notification index {}", e.getMessage());

        }

        return false;
    }

    /**
     * @param startDate
     * @param endDate
     * @return
     */
    public boolean indexData(String startDate, String endDate, String instituionId) {

        logger.debug("Indexing repo started to build notification from start date {} to end data {}", startDate, endDate);

        ReportingMongoDBRepository reportingMongoDBRepository = new ReportingMongoDBRepository(MongoConfig.getMongoTemplate());

        esEntityBuilder = new EsEntityBuilderImpl();

        QueueIndexWriter queueIndexWriter = new QueueIndexWriter();

        Query query = new Query();

        if (null != startDate && null != endDate) {

            Date fromDate = GngDateUtil.getDate(startDate).toDate();

            Date toDate = GngDateUtil.getDate(endDate).toDate();

            query.addCriteria(
                    Criteria.where("applicationRequest.header.dateTime").gte(fromDate).lte(toDate)
                            .and("applicationRequest.header.institutionId").is(instituionId)

            );
        }

        logger.debug("Indexing repo query generated {}", query);

        final int[] count = {0};

        reportingMongoDBRepository.getCustomerApplication(query).forEachOrdered(goNoGoCustomerApplication -> {

            IndexingRequest indexingRequest = null;
            EsConnectionConfig elasticSearchConfig = null;

            String refId = "";
            if(null != goNoGoCustomerApplication) {
                 refId = goNoGoCustomerApplication.getGngRefId();
            }
            try {

                indexingRequest = esEntityBuilder.buildNotificationIndexRequest(goNoGoCustomerApplication);

                 elasticSearchConfig = Cache.URL_CONFIGURATION.getEsConnectionConfig().get("DEFAULT");

                boolean b = queueIndexWriter.httpCroQueue(indexingRequest, elasticSearchConfig);

                //if(b && logger.isDebugEnabled()){

                    logger.debug(" total no of records processed for indexing in elastic search are [{}] records " , count[0]++);

                //}


            } catch (JsonProcessingException e) {
                e.printStackTrace();
                logger.error("Error indexing gng customer application with refID {} cause {}",refId,e.getMessage());
            }catch (Exception e){
                e.printStackTrace();
                logger.error("Error while indexing gng customer application with refID {} probable cause [{}] ",refId,e.getMessage());
            }



        });


        return true;
    }
}