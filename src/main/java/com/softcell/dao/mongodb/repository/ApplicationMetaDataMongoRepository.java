package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.masters.BureauStates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @author kishorp
 */
@Repository
public class ApplicationMetaDataMongoRepository implements ApplicationMetaDataRepository {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationMetaDataMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;


    /* (non-Javadoc)
     * @see ApplicationMetaDataRepository#getBureauStateMap()
     */
    @Override
    public Map<String, BureauStates> getBureauStateMap() {
        Query query = new Query();
        return null;
    }

    /* (non-Javadoc)
     * @see ApplicationMetaDataRepository#saveBureauStateMap(com.softcell.gonogo.model.masters.BureauStates)
     */
    @Override
    public boolean saveBureauStateMap(BureauStates bureauStates) {
        return false;
    }

}
