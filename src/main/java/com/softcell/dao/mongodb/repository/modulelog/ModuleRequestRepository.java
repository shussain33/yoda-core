package com.softcell.dao.mongodb.repository.modulelog;

import com.softcell.aop.Retry;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.PanServiceResponse;
import com.softcell.gonogo.model.core.request.posidex.PosidexRequestLog;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequestV2;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.CommercialRequestResponseDomain;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;

import java.util.List;

/**
 * @author yogeshb
 */
public interface ModuleRequestRepository {
    /**
     * This method saves Aadhar request object in database.
     *
     * @param aadhaarRequest
     * @return
     */

    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveAadharRequest(AadhaarRequest aadhaarRequest);

    /**
     * his method saves Pan Request object in database.
     *
     * @param panRequest
     * @return
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean savePanRequest(PanRequest panRequest);

    /**
     * This method call by each module for partially save the module responses.
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean partialApplicationDbSave(
            GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     * @param scoringApplicationRequest
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveScoringRequest(
            ScoringApplicationRequest scoringApplicationRequest);

    /**
     * @param scoringApplicantRequest
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveScoringApplicantRequest(
            ScoringApplicantRequest scoringApplicantRequest);

    boolean saveScoringApplicantRequestV2(
            ScoringApplicantRequestV2 scoringApplicantRequest);

    /**
     * @param requestJsonDomain
     * @return
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveBureauRequest(RequestJsonDomain requestJsonDomain);


    /**
     * This store co-applicant related response from Multi Bureau
     *
     * @param coApplicantResponse
     * @return
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveCoApplicantMBResponse(MultiBureauCoApplicantResponse coApplicantResponse);

    /**
     * @param refId
     * @return
     */
    List<MultiBureauCoApplicantResponse> getCoApplicantResponseByAppRefId(
            String refId);

    /**
     * @param panServiceResponse
     * @return
     */
    boolean savePanResponse(PanServiceResponse panServiceResponse);

    /**
     * @param posidexRequestLog
     */
    void savePosidexRequest(final PosidexRequestLog posidexRequestLog);

    /**
     * @param commercialRequestResponseDomain
     * @return
     */
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    boolean saveComercialBureauRequest(CommercialRequestResponseDomain commercialRequestResponseDomain);
}
