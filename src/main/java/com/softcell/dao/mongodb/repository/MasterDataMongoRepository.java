/**
 * kishorp12:20:58 PM  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.google.common.base.CharMatcher;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Institute;
import com.softcell.constants.SourcingEnum;
import com.softcell.constants.Status;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.metadata.SchemeMaster;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author kishorp
 */

@Repository
public class MasterDataMongoRepository implements MasterDataRepository {
    private static final Logger logger = LoggerFactory
            .getLogger(MasterDataMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    private Criteria criteria;

    private static final CharSequence SPECIAL_CHAR = "\n\t";

    public MasterDataMongoRepository() {
    }

    /**
     * @param mongoTemplate
     */
    @Inject
    public MasterDataMongoRepository(MongoTemplate mongoTemplate) {
        this.setMongoTemplate(mongoTemplate);
    }

    @Inject
    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    /**
     * @param mongoTemplate
     */
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public List<PinCodeMaster> getPinCodeMaster(String institutionId) {
        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("active").is(true));
        query.fields().include("city");
        query.fields().include("state");
        query.fields().include("country");
        query.fields().include("zipCodeDescr");
        query.fields().include("institutionId");
        query.fields().include("zipCode");
        query.fields().include("negativeArea");
        query.fields().include("negativeAreaType");
        query.fields().include("outOfGeoLimit");
        query.fields().include("serviceablePin");
        query.fields().include("location");
        query.fields().include("region");
        query.fields().include("zone");
        query.fields().include("district");
        return mongoTemplate.find(query, PinCodeMaster.class);
    }

    @Override
    public boolean savePinCodeMaster(PinCodeMaster pinCodeMaster) {
        try {
            mongoTemplate.save(pinCodeMaster);
        } catch (Exception e) {
            logger.error("Error occurred while saving pin code master with probable cause [{}]", e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("Error occurred while saving pin code master with probable cause [{}]", e.getMessage()));
        }
        return true;
    }


    @Override
    public List<EmployerMaster> getEmployerMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.find(query, EmployerMaster.class);
    }


    @Override
    public boolean saveAssetMaster(AssetModelMaster assetModelMaster) {
        try {
            mongoTemplate.save(assetModelMaster);

        } catch (Exception e) {
            logger.error("Error occurred while saving asset model master with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Error occurred while saving asset model master with probable cause {}", e.getMessage()));
        }
        return true;
    }

    @Override
    public List<AssetModelMaster> getAssetMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.find(query, AssetModelMaster.class);
    }

    @Override
    public List<AssetModelMaster> getAssetMasterWeb(String institutionId,
                                                    String category) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("catgDesc").is(category).and("active").is(true));
        return mongoTemplate.find(query, AssetModelMaster.class);
    }

    @Override
    public List<SchemeMaster> getSchemeMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.find(query, SchemeMaster.class);
    }

    @Override
    public boolean saveScheme(SchemeMaster schemeMaster) {
        try {
            mongoTemplate.save(schemeMaster);
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while saving pin code master with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while saving pin code master with probable cause [{}]", e.getMessage()));
        }
    }

    /**
     * @param instId
     */
    public List<String> getAssetCategory(String instId) {
        List<String> assetCategory = new ArrayList<String>();
        DBObject dbObject = new BasicDBObject();
        dbObject.put("institutionId", instId);
        assetCategory = mongoTemplate.getCollection("assetModelMaster")
                .distinct("catgDesc", dbObject);
        return assetCategory;
    }

    /**
     * @param instId
     * @param queryString
     * @return
     */
    public PinCodeMaster getPostalCodeDetails(String instId, String queryString) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(instId)
                .and("zipCode").is(queryString).and("active").is(true));
        return mongoTemplate.findOne(query, PinCodeMaster.class);
    }

    /**
     * @param instId
     * @param queryString
     * @return
     */
    public Stream<EmployerMaster> getEmployerMaster(String instId, String queryString) {

        Stream<EmployerMaster> employerMaster = null;

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(instId)
                    .and("active")
                    .is(true)
                    .and("employerName")
                    .regex(GngUtils.sanitizeString(queryString), "i")).limit(100);

            query.fields().include("employerName");
            query.fields().include("iciciEmpCode");
            query.fields().include("employerId");
            query.fields().include("employerCatg");

            employerMaster = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, EmployerMaster.class, "employerMaster"));

        } catch (DataAccessException e) {
            logger.error(" error occurred while fetching employer master with probable cause [{}] ", e.getMessage());
        } catch (SystemException e) {
            logger.error(" error occurred while fetching employer master with probable cause [{}] ", e.getMessage());
        }

        return employerMaster;

    }


    @Override
    public List<CarSurrogateMaster> getCarSurrogateMaster(String instId,
                                                          String queryString) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionID").is(instId)
                .and("active").is(true));
        return mongoTemplate.find(query, CarSurrogateMaster.class);
    }

    /**
     * @param metadataRequest
     */
    public List<AssetModelMaster> getAssetDetails(
            ApplicationMetadataRequest metadataRequest) {
        Query query = new Query();
        criteria = new Criteria();
        if (StringUtils.isNotBlank(metadataRequest.getQuery())) {
            criteria = criteria.where("institutionId")
                    .is(metadataRequest.getHeader().getInstitutionId())
                    .and("catgDesc").regex(GngUtils.sanitizeString(metadataRequest.getQuery()))
                    .and("active").is(true);
        }
        if (StringUtils.isNotBlank(metadataRequest.getQuery2())) {
            criteria = criteria.and("manufacturerDesc")
                    .is(metadataRequest.getQuery2()).and("active").is(true);
        }
        query.addCriteria(criteria);
        return mongoTemplate.find(query, AssetModelMaster.class);
    }


    @Override
    public List<SchemeMasterData> getSchemeMasterDetails(String institutionID) {
        try {
            List<SchemeMasterData> schemeMasterDataList = mongoTemplate
                    .findAll(SchemeMasterData.class);
            return schemeMasterDataList;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while getSchemeMasterDetails for institutionId {} with probable cause [{}]", institutionID, e.getMessage());
            throw new SystemException(String.format("Error occurred while getSchemeMasterDetails for institutionId {} with probable cause [{}]", institutionID, e.getMessage()));
        }

    }

    @Override
    public SchemeMasterFliter getCcIdSchemeMasterDetails(String ccId) {
        SchemeMasterFliter schemeMasterFliter = new SchemeMasterFliter();
        try {

            List<SchemeMasterData> withoutFilterSchemeMasterList = new ArrayList<>();

            Query query = new Query();
            query.addCriteria(Criteria.where("ccid").is(ccId));

            List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(
                    query, SchemeMasterData.class);

            if (schemeMasterDataList != null && !schemeMasterDataList.isEmpty()) {
                schemeMasterFliter
                        .setWithFilterSchemeData(schemeMasterDataList);
            } else {
                query = new Query();
                query.addCriteria(Criteria.where("ccid").is(""));
                schemeMasterDataList = mongoTemplate.find(query,
                        SchemeMasterData.class);
                if (schemeMasterDataList != null
                        && !schemeMasterDataList.isEmpty()) {
                    for (SchemeMasterData schemeMaster : schemeMasterDataList) {

                        if (!StringUtils.startsWith(
                                schemeMaster.getSchemeDesc(), "VNL_")) {
                            withoutFilterSchemeMasterList.add(schemeMaster);
                        }
                    }
                    schemeMasterFliter
                            .setWithFilterSchemeData(withoutFilterSchemeMasterList);
                }
            }
            return schemeMasterFliter;
        } catch (Exception exp) {
            exp.printStackTrace();
            logger.error("Error occurred while getting CcIdSchemeMasterDetails with probable cause [{}]", exp.getMessage());
            throw new SystemException(String.format("Error occurred while getting CcIdSchemeMasterDetails with probable cause [{}]", exp.getMessage()));
        }

    }


    @Override
    public List<String> getSchemeIds(String institutionID, String dealerID) {
        List<String> schemeList = null;

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionID").is(institutionID)
                    .and("dealerID").is(dealerID).and("enabledFlag").is("Y"));
            List<DealerSchemeMapping> dealerScheemeInterLinkageList = mongoTemplate
                    .find(query, DealerSchemeMapping.class);
            schemeList = new ArrayList<>();
            for (DealerSchemeMapping dealerSchemeInterLinkage : dealerScheemeInterLinkageList) {
                schemeList.add(dealerSchemeInterLinkage.getSchemeID());
            }
            return schemeList;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getSchemeIds for institutionID {} and dealerId {} with probable cause [{}]", institutionID, dealerID, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getSchemeIds for institutionID {} and dealerId {} with probable cause [{}]", institutionID, dealerID, ex.getMessage()));
        }
    }

    @Override
    public List<String> getModelIds(String instId, String[] schemeIdArray) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelId")
                    .in((Object[]) schemeIdArray).and("enabledFlag").is(Status.Y.name()));

            List<String> assetModelList = new ArrayList<>();
            List<AssetSchemeMapping> assetSchemeInterLinkageList = mongoTemplate
                    .find(query, AssetSchemeMapping.class);
            assetModelList.addAll(assetSchemeInterLinkageList.stream().map(AssetSchemeMapping::getModelId).collect(Collectors.toList()));
            return assetModelList;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting ModelIds for institutionID {}  with probable cause [{}]", instId, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting ModelIds for institutionID {}  with probable cause [{}]", instId, ex.getMessage()));
        }
    }

    @Override
    public List<AssetModelMaster> getAssetModelMasterDetails(
            String modelIdArray[]) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelId")
                    .in((Object[]) modelIdArray).and("enabledFlag").is("Y")
                    .and("active").is(true));
            List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(
                    query, AssetModelMaster.class);
            return assetModelMasterList;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting AssetModelMasterDetails with probable cause [{}]", ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting AssetModelMasterDetails with probable cause [{}]", ex.getMessage()));
        }
    }

    @Override
    public List<SchemeMasterData> getSchemeMasterDataRecords(String instId,
                                                             String dealerID, String modelID) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelId").is(instId)
                    .and("enabledFlag").is(Status.Y.name()));
            return mongoTemplate.find(query, SchemeMasterData.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting SchemeMasterDataRecords for institutionId {} and dealerId {} and modelId {} with probable cause [{}]", instId, dealerID, modelID, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting SchemeMasterDataRecords for institutionId {} and dealerId {} and modelId {} with probable cause [{}]", instId, dealerID, modelID, ex.getMessage()));

        }
    }

    @Override
    public AssetModelMaster getAssetModelMasterByModelNo(String modelNo) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelNo").is(modelNo)
                    .and("active").is(true));
            return mongoTemplate.findOne(query, AssetModelMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting AssetModelMasterByModelNo for modelNo {} with probable cause [{}]", modelNo, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting AssetModelMasterByModelNo for modelNo {} with probable cause [{}]", modelNo, ex.getMessage()));
        }
    }

    @Override
    public AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                                String manufacturerDesc, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelNo").is(modelNo)
                    .and("catgDesc").is(catDesc).and("manufacturerDesc")
                    .is(manufacturerDesc).and("makeModelFlag").is("MO")
                    .and("active").is(true).and("institutionId")
                    .is(institutionId));
            return mongoTemplate.findOne(query, AssetModelMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting AssetModelMaster for" +
                            " modelNo {} and catDesc {} manufacturerDesc {} and institutionId {}" +
                            " with probable cause [{}]", modelNo, catDesc, manufacturerDesc,
                    institutionId, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting AssetModelMaster for" +
                            " modelNo {} and catDesc {} manufacturerDesc {} and institutionId {}" +
                            " with probable cause [{}]", modelNo, catDesc, manufacturerDesc,
                    institutionId, ex.getMessage()));

        }
    }

    @Override
    public AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                                String manufacturerDesc, String modelId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelNo").is(modelNo)
                    .and("catgDesc").is(catDesc).and("manufacturerDesc")
                    .is(manufacturerDesc).and("makeModelFlag").is("MO")
                    .and("modelId").is(modelId).and("active").is(true)
                    .and("institutionId").is(institutionId));
            return mongoTemplate.findOne(query, AssetModelMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting AssetModelMaster for" +
                            " modelNo {} and catDesc {} manufacturerDesc {} and modelId {} and institutionId {}" +
                            " with probable cause [{}]", modelNo, catDesc, manufacturerDesc, modelId,
                    institutionId, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting AssetModelMaster for" +
                            " modelNo {} and catDesc {} manufacturerDesc {} and modelId {} and institutionId {}" +
                            " with probable cause [{}]", modelNo, catDesc, manufacturerDesc, modelId,
                    institutionId, ex.getMessage()));
        }
    }

    @Override
    public List<ReferenceDetailsMaster> getReferenceRelationMasterDetails(ModelVariantMasterRequest modelVariantMasterRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(modelVariantMasterRequest.getHeader().getInstitutionId())
                    .and("active").is(true)).fields().include("description");
            return mongoTemplate.find(query, ReferenceDetailsMaster.class);
        } catch (Exception ex) {
            logger.error("Error occurred while getting ReferenceRelationMasterDetails with probable cause [{}]", ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting ReferenceRelationMasterDetails  with probable cause [{}]", ex.getMessage()));
        }
    }

    @Override
    public List<BankDetailsMaster> getBankNames(String institutionId) {
        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", institutionId);
        query.put("active", true);
        return mongoTemplate.getCollection("bankDetailsMaster").distinct("bankName", query);
    }

    @Override
    public List<BankDetailsMaster> getStatesByBankName(String institutionId, String bank) {
        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", institutionId);
        query.put("active", true);
        query.put("bankName", bank);
        return mongoTemplate.getCollection("bankDetailsMaster").distinct("state", query);
    }

    @Override
    public List<BankDetailsMaster> getBankDistrict(String institutionId, String bank, String state) {
        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", institutionId);
        query.put("active", true);
        query.put("bankName", bank);
        query.put("state", state);
        return mongoTemplate.getCollection("bankDetailsMaster").distinct("district", query);

    }

    @Override
    public List<BankDetailsMaster> getBankBranches(String institutionId, String bank, String state, String district) {
        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", institutionId);
        query.put("active", true);
        query.put("bankName", bank);
        query.put("state", state);
        query.put("district", district);
        return mongoTemplate.getCollection("bankDetailsMaster").distinct("branch", query);
    }

    @Override
    public BankDetailsMaster getBankMasterDetails(String institutionId, String bank, String state, String district, String branch) {
        Query query = new Query();
        query.addCriteria(Criteria.where("bankName").is(bank)
                .and("state").is(state)
                .and("district").is(district)
                .and("branch").is(branch)
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.findOne(query, BankDetailsMaster.class);
    }

    @Override
    public BankDetailsMaster getBankMasterDetailsByIfscCode(String institutionId, String ifsc) {
        Query query = new Query();
        query.addCriteria(Criteria.where("ifscCode").is(ifsc)
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.findOne(query, BankDetailsMaster.class);
    }

    @Override
    public BankDetailsMaster getBankMasterDetailsByMicrCode(String institutionId, String micr) {
        Query query = new Query();
        query.addCriteria(Criteria.where("micrCode").is(micr)
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        return mongoTemplate.findOne(query, BankDetailsMaster.class);
    }

    @Override
    public Set<String> getBankNameFromCGPM(String bankName){

        Set<String> bankNames = new TreeSet<>();

        Aggregation buildQueryForBankName = com.softcell.dao.mongodb.helper.QueryBuilder.buildQueryForBankName(bankName);

        AggregationResults<BasicDBObject> bankNameFromCGPM = mongoTemplate
                .aggregate(buildQueryForBankName, "commonGeneralParameterMaster", BasicDBObject.class);

        if(bankNameFromCGPM != null) {
            for (BasicDBObject bdo : bankNameFromCGPM) {
                bankNames.add((String) bdo.get("description"));
            }
        }
        return bankNames;
    }

    @Override
    public Set<String> getAccountTypeFromCGPM(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
        .and("active").is(true)
        .and("status").is("A")
        .and("key1").is(SourcingEnum.ACCTYPE.name()));
        query.fields().include("value");
        CloseableIterator<CommonGeneralParameterMaster> stream = mongoTemplate.stream(query, CommonGeneralParameterMaster.class);
        return StreamUtils.createStreamFromIterator(stream).collect(Collectors.mapping(obj -> obj.getValue(),Collectors.toSet()));
    }

    @Override
    public List<SchemeModelDealerCityMapping> getSchemeIdList(String modelId,
                                                              String dealerID, String city) {
        /**
         * Find SchemeIDs against method argument. Need to create Collection
         * then it is useful.
         */

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelID").is(modelId)
                    .and("validity").is(Status.Y.name()).and("active").is(true));
            return mongoTemplate
                    .find(query, SchemeModelDealerCityMapping.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting SchemeIdList for" +
                            "modelId {}  and dealerID {} city {} " +
                            " with probable cause [{}]", modelId, dealerID, city
                    , ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting SchemeIdList for" +
                            "modelId {}  and dealerID {} city {} " +
                            " with probable cause [{}]", modelId, dealerID, city
                    , ex.getMessage()));
        }
    }

    @Override
    public List<SchemeDateMapping> getSchemeDateMapping(String[] schemeIdArray) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID")
                    .in((Object[]) schemeIdArray).and("active").in(true));
            return mongoTemplate.find(query, SchemeDateMapping.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting SchemeDateMapping for schemeIdArray [{}] with probable cause [{}]", schemeIdArray, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting SchemeDateMapping for schemeIdArray [{}] with probable cause [{}]", schemeIdArray, ex.getMessage()));
        }
    }

    @Override
    public List<SchemeMasterData> getSchemeMasterData(String[] schemes) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").in((Object[]) schemes));
            return mongoTemplate.find(query, SchemeMasterData.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting SchemeMasterData for schemes [{}] with probable cause [{}]", schemes, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting SchemeMasterData for schemes [{}] with probable cause [{}]", schemes, ex.getMessage()));
        }
    }


    @Override
    public List<HitachiSchemeMaster> getHitachiSchemeMaster(String[] schemes) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").in((Object[]) schemes));
            return mongoTemplate.find(query, HitachiSchemeMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting HitachiSchemeMaster for schemes [{}] with probable cause [{}]", schemes, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting HitachiSchemeMaster for schemes [{}] with probable cause [{}]", schemes, ex.getMessage()));
        }
    }

    @Override
    public List<SchemeModelDealerCityMapping> getSchemeMappingList(
            Set<String> schemeIdSet, String modelNo, String city,
            String dealerId) {

        /**
         * Find SchemeIDs against method argument. Need to create Collection
         * then it is useful.
         */

        try {
            Query query = new Query();
            Criteria criteria = new Criteria();
            criteria.orOperator(
                    Criteria.where("schemeID").in(schemeIdSet).and("validity")
                            .is(Status.Y.name()).and("modelID").is(modelNo).and("dealerId")
                            .is(Status.ALL.name()).and("active").is(true),
                    Criteria.where("schemeID").in(schemeIdSet).and("validity")
                            .is(Status.Y.name()).and("modelID").is(modelNo)
                            .and("dealerInclusion").in(dealerId).and("active")
                            .is(true));
            query.addCriteria(criteria);
            return mongoTemplate
                    .find(query, SchemeModelDealerCityMapping.class);
        } catch (Exception ex) {
            logger.error("Error occurred while getting SchemeMappingList for schemes [{}] and modelNo {} and city {} and dealer {} with probable cause [{}]", schemeIdSet, modelNo, city,
                    dealerId, ex.getMessage());
            ex.printStackTrace();
            throw new SystemException(String.format("Error occurred while getting SchemeMappingList for schemes [{}] and modelNo {} and city {} and dealer {} with probable cause [{}]", schemeIdSet, modelNo, city,
                    dealerId, ex.getMessage()));

        }

    }

    @Override
    public List<SchemeModelDealerCityMapping> getSchemeMappingExcludedList(
            Set<String> schemeIdSet, String modelNo, String city,
            String dealerId) {

        /**
         * Find SchemeIDs against method argument. Need to create Collection
         * then it is useful.
         */

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").ne(schemeIdSet)
                    .and("validity").is(Status.Y.name()).and("modelID").is(modelNo)
                    .and("dealerId").is(Status.ALL.name()).and("active").is(true));
            return mongoTemplate
                    .find(query, SchemeModelDealerCityMapping.class);
        } catch (Exception ex) {
            logger.error("Error occurred while getting SchemeMappingExcludedList for schemes [{}] and modelNo {} and city {} and dealer {} with probable cause [{}]", schemeIdSet, modelNo, city,
                    dealerId, ex.getMessage());
            ex.printStackTrace();
            throw new SystemException(String.format("Error occurred while getting SchemeMappingExcludedList for schemes [{}] and modelNo {} and city {} and dealer {} with probable cause [{}]", schemeIdSet, modelNo, city,
                    dealerId, ex.getMessage()));
        }

    }

    @Override
    public List<HitachiSchemeMaster> getVanillaHitachiSchemeMaster(
            String[] schemes) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").in((Object[]) schemes));
            List<HitachiSchemeMaster> hitachiSchemeMastersNotVanilla = new ArrayList<>();

            List<HitachiSchemeMaster> HitachiSchemeMasterList = mongoTemplate
                    .find(query, HitachiSchemeMaster.class);
            hitachiSchemeMastersNotVanilla.addAll(HitachiSchemeMasterList.stream().filter(hitachiSchemeMaster -> !StringUtils.startsWithIgnoreCase(hitachiSchemeMaster
                    .getSchemeDesc().trim(), "VNL_")).collect(Collectors.toList()));
            return hitachiSchemeMastersNotVanilla;
        } catch (Exception ex) {
            logger.error("Error occurred while getting SchemeMappingExcludedList for schemes [{}] with probable cause [{}]", schemes
                    , ex.getMessage());
            ex.printStackTrace();
            throw new SystemException(String.format("Error occurred while getting SchemeMappingExcludedList for schemes [{}] with probable cause [{}]", schemes
                    , ex.getMessage()));
        }
    }


    @Override
    public SchemeMasterFliter getVanillaSchemeMasterDetails(String ccId) {
        SchemeMasterFliter schemeMasterFliter = new SchemeMasterFliter();
        try {
            List<SchemeMasterData> withoutFilterSchemeMasterList = new ArrayList<>();

            Query query = new Query();
            query.addCriteria(Criteria.where("ccid").is(FieldSeparator.BLANK));

            List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(
                    query, SchemeMasterData.class);

            if (schemeMasterDataList != null && !schemeMasterDataList.isEmpty()) {
                withoutFilterSchemeMasterList.addAll(schemeMasterDataList.stream().filter(schemeMaster -> StringUtils.startsWith(schemeMaster.getSchemeDesc(),
                        "VNL_")).collect(Collectors.toList()));
                schemeMasterFliter
                        .setWithFilterSchemeData(withoutFilterSchemeMasterList);
            }
            return schemeMasterFliter;
        } catch (Exception exp) {
            exp.printStackTrace();
            logger.error("Error occurred while getting VanillaSchemeMasterDetails for ccid {} with probable cause [{}]", ccId
                    , exp.getMessage());
            throw new SystemException(String.format("Error occurred while getting VanillaSchemeMasterDetails for ccid {} with probable cause [{}]", ccId
                    , exp.getMessage()));
        }

    }

    @Override
    public List<SchemeModelDealerCityMapping> getSchemeMappingList(
            Set<String> schemeIdSet, String modelNo, String city) {

        /**
         * Find SchemeIDs against method argument. Need to create Collection
         * then it is useful.
         */

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").in(schemeIdSet)
                    .and("validity").is(Status.Y.name()).and("modelID").is(modelNo)
                    .and("active").is(true));
            return mongoTemplate
                    .find(query, SchemeModelDealerCityMapping.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting SchemeMappingList for schemeIdSet [{}] and modelNo {} and city {} with probable cause [{}]", schemeIdSet, modelNo, city
                    , ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting SchemeMappingList for schemeIdSet [{}] and modelNo {} and city {} with probable cause [{}]", schemeIdSet, modelNo, city
                    , ex.getMessage()));
        }

    }

    @Override
    public String getDealerCity(String institutionId, String dealerID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerID").is(dealerID)
                .and("institutionID").is(institutionId).and("active").is(true));
        DealerEmailMaster dealerDetail = mongoTemplate.findOne(query,
                DealerEmailMaster.class);
        if (null != dealerDetail) {
            return dealerDetail.getCityId();
        }
        return null;
    }

    @Override
    public String getDealerState(String institutionId, String dealerID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerID").is(dealerID)
                .and("institutionID").is(institutionId).and("active").is(true));
        DealerEmailMaster dealerDetail = mongoTemplate.findOne(query,
                DealerEmailMaster.class);
        if (null != dealerDetail) {
            return dealerDetail.getStateId();
        }
        return null;
    }

    @Override
    public List<BankDetailsMaster> getBankDetailsMaster(String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));
            return mongoTemplate.find(query, BankDetailsMaster.class);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while getting DealerDetails for " +
                            "institutionId {} with probable cause [{}]",
                    institutionId, e.getMessage());
            throw new SystemException(String.format("Error occurred while getting DealerDetails for " +
                            "institutionId {} with probable cause [{}]",
                    institutionId, e.getMessage()));
        }
    }

    @Override
    public DealerEmailMaster getDealerDetails(String institutionId,
                                              String dealerID) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("dealerID").is(dealerID)
                    .and("institutionID").is(institutionId).and("active")
                    .is(true));
            return mongoTemplate.findOne(query, DealerEmailMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting DealerDetails for " +
                            "institutionId {} and dealerID {} with probable cause [{}]",
                    institutionId, dealerID, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting DealerDetails for " +
                            "institutionId {} and dealerID {} with probable cause [{}]",
                    institutionId, dealerID, ex.getMessage()));
        }

    }

    @Override
    public boolean saveEmployerMaster(EmployerMaster employerMaster) {
        try {
            mongoTemplate.save(employerMaster);
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while saveEmployerMaster " +
                    " with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while saveEmployerMaster " +
                    " with probable cause [{}]", e.getMessage()));
        }

    }

    @Override
    public SchemeMasterFliter getWithoutCcIdSchemeMasterDetails() {
        SchemeMasterFliter schemeMasterFliter = new SchemeMasterFliter();
        Query query = new Query();
        query.addCriteria(Criteria.where("ccid").is(FieldSeparator.BLANK));
        List<SchemeMasterData> withoutFilterSchemeMasterList = new ArrayList();
        List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query,
                SchemeMasterData.class);

        if (schemeMasterDataList != null && !schemeMasterDataList.isEmpty()) {
            withoutFilterSchemeMasterList.addAll(schemeMasterDataList.stream().filter(schemeMaster -> !StringUtils.startsWith(schemeMaster.getSchemeDesc(),
                    "VNL_")).collect(Collectors.toList()));
        }
        schemeMasterFliter
                .setWithoutFilterSchemeData(withoutFilterSchemeMasterList);
        return schemeMasterFliter;
    }


    @Override
    public AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                                String manufacturerDesc) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("modelNo").is(modelNo)
                    .and("catgDesc").is(catDesc).and("manufacturerDesc")
                    .is(manufacturerDesc).and("makeModelFlag").is("MO").and("active").is(true));
            return mongoTemplate.findOne(query, AssetModelMaster.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error occurred while getting AssetModelMaster " +
                    "for modelNo {} catDesc {} and manufacturerDesc {}" +
                    " with probable cause [{}]", modelNo, catDesc, manufacturerDesc, ex.getMessage());
            throw new SystemException(String.format("Error occurred while getting AssetModelMaster " +
                    "for modelNo {} catDesc {} and manufacturerDesc {}" +
                    " with probable cause [{}]", modelNo, catDesc, manufacturerDesc, ex.getMessage()));
        }
    }

    /**
     * @param request
     * @return
     */
    public Set<String> getModelVariantManufacturer(
            ModelVariantMasterRequest request) {
        Set<String> modelVariantManufacturers = null;
        List<ModelVariantMaster> modelVariantMasterList = null;
        Query query = new Query();
        try {

            switch (ModelVariantMasterRequest.RequestType.valueOf(request.getRequestType())) {
                case MANUFACTURER:
                    query.addCriteria(Criteria.where("institutionId").is(
                            request.getHeader().getInstitutionId()).and("active").is(true));

                    modelVariantMasterList = mongoTemplate.find(query,
                            ModelVariantMaster.class);

                    if (modelVariantMasterList != null
                            && modelVariantMasterList.size() > 0) {
                        modelVariantManufacturers = new HashSet<String>();
                        for (ModelVariantMaster modelVariant : modelVariantMasterList) {
                            modelVariantManufacturers.add(modelVariant
                                    .getManufacturer());
                        }
                    }

                    break;

                case MODEL:
                    query.addCriteria(Criteria.where("institutionId")
                            .is(request.getHeader().getInstitutionId())
                            .and("manufacturer").is(request.getManufacturer()).and("active").is(true));

                    modelVariantMasterList = mongoTemplate.find(query,
                            ModelVariantMaster.class);

                    if (modelVariantMasterList != null
                            && modelVariantMasterList.size() > 0) {
                        modelVariantManufacturers = new HashSet<String>();
                        for (ModelVariantMaster modelVariant : modelVariantMasterList) {
                            modelVariantManufacturers.add(modelVariant.getModel());
                        }
                    }

                    break;

                case VARIANT:
                    query.addCriteria(Criteria.where("institutionId")
                            .is(request.getHeader().getInstitutionId())
                            .and("model").is(request.getModel()).and("active").is(true));
                    modelVariantMasterList = mongoTemplate.find(query,
                            ModelVariantMaster.class);

                    if (modelVariantMasterList != null
                            && modelVariantMasterList.size() > 0) {
                        modelVariantManufacturers = new HashSet<String>();
                        for (ModelVariantMaster modelVariant : modelVariantMasterList) {
                            modelVariantManufacturers
                                    .add(modelVariant.getVariant());
                        }
                    }
                    break;
            }
            return modelVariantManufacturers;
        } catch (Exception e) {
            logger.error("Error occurred while getting ModelVariantManufacturer " +
                    " with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting ModelVariantManufacturer " +
                    " with probable cause [{}]", e.getMessage()));
        }

    }


    @Override
    public List<BankMaster> getBankNameFromBankMaster(String bankName, String institutionId){

        //Aggregation buildQueryForBankName = com.softcell.dao.mongodb.helper.QueryBuilder.buildQueryForBankNameFromBankMaster(bankName);

        Query query = new Query();

        Criteria criteria = Criteria.where("bankDescription").regex(Pattern.compile(CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(bankName),
                Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)).and("active").is(true).and("institutionId").is(institutionId);

        query.addCriteria(criteria).limit(5);
        query.fields().include("bankDescription");
        query.fields().include("mifinBankcode");

        List<BankMaster> bankMasterList = mongoTemplate.find(query, BankMaster.class);

        /*AggregationResults<BasicDBObject> bankNameFromCGPM = mongoTemplate
                .aggregate(buildQueryForBankName, BankMaster.class, BasicDBObject.class);*/

        return bankMasterList;
    }

    @Override
    public SupplierLocationMaster getSupplierLocationMaster(String supplierId, String branchId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("supplierId").is(supplierId)
                .and("branchId").is(branchId).and("active").is(true));
        return mongoTemplate.findOne(query, SupplierLocationMaster.class);
    }

    @Override
    public List<CityMaster>  getCityMaster(String institution, String cityQuery){
        Query query = new Query();
        query.addCriteria(Criteria.where("cityId").regex(GngUtils.sanitizeString(cityQuery)));
        return mongoTemplate.find(query,CityMaster.class);
    }

    @Override
    public List<CityMaster>  getCityMaster(String institution){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institution));
        return mongoTemplate.find(query,CityMaster.class);
    }

    @Override
    public boolean saveEmployerMaster(EmployerMaster employerMaster, boolean update, String instId) {
        try {
            Query query = new Query();
            Update updateData = new Update();

            query.addCriteria(Criteria.where("employerId").is(employerMaster.getEmployerId())
                    .and("institutionId").is(instId).and("active")
                    .is(true));
            updateData.set("employerName", employerMaster.getEmployerName());
            updateData.set("employerCatg", employerMaster.getEmployerCatg());
            updateData.set("iciciEmpCode", employerMaster.getIciciEmpCode());
            return  mongoTemplate.upsert(query, updateData, EmployerMaster.class).wasAcknowledged();
        } catch (Exception e) {

            logger.error("Error occurred while saveEmployerMaster " +
                    " with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while saveEmployerMaster " +
                    " with probable cause [{}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveBankMasterDetail(BankDetailsMaster bankDetailsMaster, boolean update, String insId, String userId) {
        Query query = new Query();
        Update updatedata = new Update();
        if(update){
            query.addCriteria(Criteria.where("institutionId").is(insId).and("ifscCode").is(bankDetailsMaster.getIfscCode())
                    .and("active").is(true));
            updatedata.set("bankName",bankDetailsMaster.getBankName());
            updatedata.set("branch",bankDetailsMaster.getBranch());
            updatedata.set("ifscCode",bankDetailsMaster.getIfscCode());
            updatedata.set("micrCode",bankDetailsMaster.getMicrCode());
            updatedata.set("city",bankDetailsMaster.getCity());
            updatedata.set("district",bankDetailsMaster.getDistrict());
            updatedata.set("state",bankDetailsMaster.getState());
            updatedata.set("state",bankDetailsMaster.getState());
            updatedata.set("country",bankDetailsMaster.getCountry());
            updatedata.set("updatedDate",new Date());
            updatedata.set("updateBy", userId);
            return  mongoTemplate.upsert(query, updatedata, BankDetailsMaster.class).wasAcknowledged();
        }else{
            bankDetailsMaster.setCreatedDate(new DateTime());
            bankDetailsMaster.setUpdatedDate(new DateTime());
            bankDetailsMaster.setInstitutionId(insId);
            bankDetailsMaster.setActive(true);
            mongoTemplate.insert(bankDetailsMaster);
            return  true;
        }
    }

    @Override
    public boolean deleteBankingIfscMaster(String ifscCode, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("ifscCode").is(ifscCode)
                .and("active").is(true));
        WriteResult remove = mongoTemplate.remove(query, BankDetailsMaster.class);
        return remove.wasAcknowledged() ;
    }

    @Override
    public boolean savePinCodeMaster(PinCodeMaster pinCodeMaster, boolean update, String institutionId, String loggedInUserId) {
        Query query = new Query();
        Update updateData  = new Update();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("zipCode").is(pinCodeMaster.getZipCode())
                .and("active").is(true));
        updateData.set("city",pinCodeMaster.getCity());
        updateData.set("state",pinCodeMaster.getState());
        updateData.set("zipCodeDescr",pinCodeMaster.getZipCodeDescr());
        updateData.set("zipCode",pinCodeMaster.getZipCode());
        updateData.set("cityCode",pinCodeMaster.getCityCode());
        updateData.set("stateCode",pinCodeMaster.getStateCode());
        updateData.set("countryCode",pinCodeMaster.getCountryCode());
        return  mongoTemplate.upsert(query, updateData, PinCodeMaster.class).wasAcknowledged();
    }

    @Override
    public boolean deletePincoideMaster(String pincode, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("zipCode").is(pincode)
                .and("active").is(true));
        WriteResult remove = mongoTemplate.remove(query, PinCodeMaster.class);
        return remove.wasAcknowledged() ;
    }

    @Override
    public boolean deleteEmployerMaster(String employerId, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("employerId").is(employerId)
                .and("active").is(true));
        WriteResult remove = mongoTemplate.remove(query, EmployerMaster.class);
        return remove.wasAcknowledged() ;
    }

    @Override
    public boolean saveBankMaster(BankMaster bankMaster, boolean update, String instId) {
        Query query = new Query();
        Update updateData  = new Update();
        query.addCriteria(Criteria.where("bankCode").is(bankMaster.getBankCode()).and("institutionId").is(instId)
                    .and("active").is(true));
        if (Institute.isInstitute(instId,Institute.AMBIT)){
            updateData.set("bankDescription",bankMaster.getBankDescription());
            updateData.set("mifinBankcode",bankMaster.getMifinBankcode());
            updateData.set("mifinBankName",bankMaster.getMifinBankName());
        } else {
            updateData.set("bankDescription",bankMaster.getBankDescription());
        }
        return  mongoTemplate.upsert(query, updateData, BankMaster.class).wasAcknowledged();
    }

    @Override
    public List<BankMaster> getBankMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("active").is(true));
        query.with(new Sort(Sort.Direction.DESC, "bankCode"));
        return mongoTemplate.find(query, BankMaster.class);
    }

    @Override
    public boolean deleteBankMaster(String bankCode, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("_id").is(bankCode)
                .and("active").is(true));
        WriteResult remove = mongoTemplate.remove(query, BankMaster.class);
        return remove.wasAcknowledged() ;
    }

    @Override
    public List<BankMaster> getBankInfoFromBankMaster(String bankName, String institutionId) {
        Query query = new Query();

        Criteria criteria = Criteria.where("bankDescription").regex(Pattern.compile(CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(bankName),
                Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)).and("active").is(true).and("institutionId").is(institutionId);

        query.addCriteria(criteria).limit(5);
        query.fields().include("bankDescription");
        query.fields().include("_id");
        query.fields().include("institutionId");
        query.fields().include("bankCode");
        query.fields().include("mifinBankcode");
        query.fields().include("mifinBankName");
        List<BankMaster> bankMasterList = mongoTemplate.find(query, BankMaster.class);
        return bankMasterList;
    }
}
