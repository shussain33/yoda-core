/**
 * bhuvneshk12:42:16 PM  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.dashboard.DashboardDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.dashboard.DashboardResponse;

import javax.validation.OverridesAttribute;
import java.util.Collection;
import java.util.List;

/**
 * @author bhuvneshk
 *
 */
public interface DashboardRepository {

    /**
     *
     * @param dashboardRequest
     * @return
     * @throws Exception
     */
    Collection<DashboardDetails> getDashboardData(DashboardRequest dashboardRequest) throws Exception;

    DashboardResponse getDashboardDataForQueueManagement(DashboardRequest dashboardRequest) throws Exception;
}
