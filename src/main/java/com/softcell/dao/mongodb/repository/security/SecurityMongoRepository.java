package com.softcell.dao.mongodb.repository.security;

import com.softcell.gonogo.model.security.Versions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yogeshb
 */
@Repository
public class SecurityMongoRepository implements SecurityRepository {

    private static final Logger logger = LoggerFactory.getLogger(SecurityMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public boolean addAllowedVersion(String institutionId, String version) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institution").is(institutionId));

            Update update = new Update();
            update.addToSet("versionName", version);
            mongoTemplate.upsert(query, update, Versions.class);
            return true;

        } catch (Exception exception) {
            logger.error(exception.getMessage());
            return false;
        }
    }

    @Override
    public boolean removeOldVersion(String instition, String version) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institution").is(instition));

            Update update = new Update();
            update.pull("versionName", version);
            mongoTemplate.upsert(query, update, Versions.class);
            return true;
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            return false;
        }
    }

    @Override
    public boolean updateVersion(String instition, String version) {
        return false;
    }

    @Override
    public List<Versions> showAllVersions() {
        return mongoTemplate.findAll(Versions.class);
    }

    @Override
    public boolean isValidVersion(String instition, String version) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institution").is(instition).and("versionName").is(version));
            logger.debug("Query {}", query.toString());
            return mongoTemplate.exists(query, Versions.class);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            return false;
        }
    }

}
