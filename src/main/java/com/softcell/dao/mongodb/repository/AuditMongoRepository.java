package com.softcell.dao.mongodb.repository;

import com.softcell.constants.AuditType;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.utils.GngDateUtil;
import com.softcell.workflow.component.finished.AuditData;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * @author vinodk
 */
@Repository
public class AuditMongoRepository implements AuditRepository {

    private static final Logger logger = LoggerFactory.getLogger(AuditMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<AuditData> getAuditData(String refId,
                                        String institutionId, AuditType auditType, String auditCollectionName) throws Exception {

        logger.debug("getAuditData repo started");

        try {

            MongoOperations mongoOperations = MongoConfig.getMongoTemplate();

            Query query = new Query();
            query.addCriteria(Criteria.where("auditData._id").is(refId)
                    .and("auditData.applicationRequest.header.institutionId")
                    .is(institutionId).and("auditType").is(auditType));

            return mongoOperations.find(query, AuditData.class,
                    auditCollectionName);

        } catch (Exception e) {

            logger.error("Error while fetching audit data [{}]",e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

}

