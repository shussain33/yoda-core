package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.request.ApplicationRequest;

import javax.validation.Valid;
import java.util.Set;

public interface FinalApplicationRepository {

    boolean saveFinalApplication(GoNoGoCustomerApplication goNoGoCustomerApplication);

    String getCroId(String dsaId);

    ApplicationRequest getApplication(String refId);

    GoNoGoCustomerApplication getGoNoGoApplication(String refId);

    boolean saveCroDsaMaster(CroDsaMaster croDsaMaster);

    Set<DedupeDetails> deDupeCustomer(@Valid final ApplicationRequest applicationRequest);

    Set<DedupeDetails> deDupeCustomerV2(@Valid final ApplicationRequest applicationRequest);

    public boolean saveDedupeInfo(DedupeMatch dedupeMatch);
}

