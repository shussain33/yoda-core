package com.softcell.dao.mongodb.repository;


import com.softcell.constants.Product;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.security.v2.Hierarchy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;


@Repository("hierarchyRepository")
public class HierarchyRepository {

    private static final Logger logger = LoggerFactory.getLogger(HierarchyRepository.class);


    private static MongoTemplate mongoTemplate;

    public static Criteria applyHierarchyQuery(RequestCriteria requestCriteria) {


        List<Integer> pincodeList = null;
        List<String> dealerList = null;

        if (null != requestCriteria) {

            Hierarchy hierarchy = requestCriteria.getHierarchy();

            if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.PL.name())) {

                pincodeList = getPincodesOnCriteriaPL(hierarchy);

            } else if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.CDL.name())) {

                dealerList = getDealersOnCriteriaCDL(hierarchy);
            }

        }

        return buildQueryforHierarchy(pincodeList, dealerList);

    }

    private static Criteria buildQueryforHierarchy(List<Integer> pincodeList,
                                                   List<String> dealerList) {

        Criteria criteria = null;

        if (!CollectionUtils.isEmpty(pincodeList)) {
            criteria = Criteria.where(
                    "applicationRequest.request.applicant.address").elemMatch(
                    Criteria.where("addressType").is("RESIDENCE").and("pin")
                            .in(pincodeList));
        }

        if (!CollectionUtils.isEmpty(dealerList )) {
            criteria = Criteria.where("applicationRequest.header.dealerId").in(
                    dealerList);
        }

        return criteria;

    }

    private static List<Integer> getPincodesOnCriteriaPL(Hierarchy hierarchy) {

        String hierarchyLevel = hierarchy.getHierarchyLevel();
        List<String> hierarchyValue = hierarchy.getHierarchyValue();

        String collectionName = "PlPincodeEmailMaster";
        String includeField = "pincode";

        Query query = new Query().addCriteria(Criteria.where(hierarchyLevel)
                .in(Arrays.asList(hierarchyValue)));

        return mongoTemplate.getCollection(collectionName).distinct(
                includeField, query.getQueryObject());

    }

    private static List<String> getDealersOnCriteriaCDL(Hierarchy hierarchy) {

        Assert.notNull(hierarchy, "Hierarchy must not be null or blank !!");

        String hierarchyLevel = hierarchy.getHierarchyLevel();
        List<String> hierarchyValue = hierarchy.getHierarchyValue();

        String collectionName = "CDLHierarchyMaster";

        String includeField = "dealerId";

        Query query = new Query().addCriteria(Criteria.where(hierarchyLevel)
                .in(hierarchyValue));

        List<String> distinctDealerIds = mongoTemplate.getCollection(
                collectionName).distinct(includeField, query.getQueryObject());


        return distinctDealerIds;

    }

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

}
