package com.softcell.dao.mongodb.repository.metadata;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.WriteResult;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class AppMetadataMongoRepository implements AppMetadataRepository {

    private static final Logger logger = LoggerFactory.getLogger(AppMetadataMongoRepository.class);

    @Autowired
    MongoTemplate mongoTemplate;

    private Query query;

    private Class collection = ActionConfiguration.class;


    @Override
    public void insertActionConfiguration(
            ActionConfiguration actionConfiguration) {
        mongoTemplate.insert(actionConfiguration);
    }

    @Override
    public Boolean updateActionConfiguration(
            ActionConfiguration actionConfiguration) {
        try {
            query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(actionConfiguration.getInstitutionId())
                    .and("productId").is(actionConfiguration.getProductId())
                    .and("actionName").is(actionConfiguration.getActionName()));
            Update update = new Update();
            update.set("enable", actionConfiguration.isEnable());
            update.set("lastUpdateDate", new Date());

            mongoTemplate.updateFirst(query, update, collection);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean deleteActionConfiguration(
            ActionConfiguration actionConfiguration) {
        try {
            query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(actionConfiguration.getInstitutionId())
                    .and("productId").is(actionConfiguration.getProductId())
                    .and("actionName").is(actionConfiguration.getActionName()));

            WriteResult remove = mongoTemplate.remove(query, collection);
            return remove.getN() > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }

    }

    @Override
    public List<String> readActionByInstitution(String institutionId) {

        List<String> actions = new ArrayList<String>();
        String includeField = "actionName";

        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", new BasicDBObject("$eq", institutionId));

        BasicDBObject fields = new BasicDBObject();
        fields.put(includeField, 1);

        DBCursor cursor = mongoTemplate.getCollection(
                mongoTemplate.getCollectionName(collection))
                .find(query, fields);
        while (cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            actions.add(obj.getString(includeField, "-1"));
        }

        return actions;
    }

    @Override
    public List<String> readActiveActionByInstitution(String institutionId) {
        List<String> actions = new ArrayList<String>();
        String includeField = "actionName";

        BasicDBObject query = new BasicDBObject();
        query.put("institutionId", new BasicDBObject("$eq", institutionId));
        query.put("enable", new BasicDBObject("$eq", true));

        BasicDBObject fields = new BasicDBObject();
        fields.put(includeField, 1);

        DBCursor cursor = mongoTemplate.getCollection(
                mongoTemplate.getCollectionName(collection))
                .find(query, fields);
        while (cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            actions.add(obj.getString(includeField, "-1"));
        }

        return actions;

    }

    @Override
    public boolean isActionConfigExist(ActionConfiguration actionConfiguration) {
        query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(actionConfiguration.getInstitutionId()).and("productId")
                .is(actionConfiguration.getProductId()).and("actionName")
                .is(actionConfiguration.getActionName()));
        return mongoTemplate.exists(query, collection);
    }

    @Override
    public List<ActionConfiguration> getAllActionConfiguration(String institutionId, String productId ,boolean active) {

        query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("enable").is(active));

        return mongoTemplate.find(query, collection);
    }
}
