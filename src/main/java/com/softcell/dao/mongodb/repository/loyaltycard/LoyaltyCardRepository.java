package com.softcell.dao.mongodb.repository.loyaltycard;

import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.masters.LoyaltyCardMaster;
import com.softcell.gonogo.model.request.loyaltycard.LoyaltyCardStatusRequest;

import java.util.Set;

/**
 * Created by mahesh on 12/12/17.
 */
public interface LoyaltyCardRepository {

    /**
     *
     * @param loyaltyCardStatusRequest
     * @return
     */
    LoyaltyCardMaster checkLoyaltyCardStatus(LoyaltyCardStatusRequest loyaltyCardStatusRequest);

    /**
     *
     * @param loyaltyCardStatusRequest
     * @return
     */
    boolean checkLoyaltyCardInDedupe(LoyaltyCardStatusRequest loyaltyCardStatusRequest);

    /**
     *
     * @param loyaltyCardDetails
     */
    void updateLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails);

    /**
     *
     * @param loyaltyCardDetails
     * @return
     */
    boolean softDeleteLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    LoyaltyCardDetails fetchLoyaltyCardDetails(String refId, String institutionId);

    /**
     *
     * @param dealerId
     * @param institutionId
     * @return
     */
    Set<String> getApplicableLoyaltyCardProviders(String dealerId, String institutionId) throws Exception;

}
