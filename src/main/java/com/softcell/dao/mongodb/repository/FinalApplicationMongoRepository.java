package com.softcell.dao.mongodb.repository;


import com.mongodb.BasicDBObject;
import com.softcell.constants.ActionName;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.elsearch.IndexingElasticSearchRepository;
import com.softcell.dao.mongodb.repository.elsearch.IndexingRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.scoring.response.DecisionResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.ApplicationStatusLogRequest;
import com.softcell.gonogo.model.request.DedupeApplicationDetails;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author kishorp
 */
@Repository
public class FinalApplicationMongoRepository implements FinalApplicationRepository {

    private static final Logger logger = LoggerFactory.getLogger(FinalApplicationMongoRepository.class);

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private IndexingRepository indexingRepository;

   @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private AdminLogRepository adminLogRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;


    public FinalApplicationMongoRepository(MongoTemplate mongoTemplate) {
        if (this.mongoTemplate == null) {
            this.mongoTemplate = mongoTemplate;
        }
        if (null == indexingRepository) {
            indexingRepository = new IndexingElasticSearchRepository();
        }

        if (null == applicationRepository) {
            applicationRepository = new ApplicationMongoRepository();
        }
        if (null == adminLogRepository) {
            adminLogRepository = new AdminLogMongoRepository(MongoConfig.getMongoTemplate(), MongoConfig.getGridFSTemplate());

        }
        if (null == uploadFileRepository) {
            uploadFileRepository = new UploadFileMongoRepository(MongoConfig.getMongoTemplate(), MongoConfig.getGridFSTemplate());

        }
        if(null == moduleRequestRepository){
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        }
        if (null==auditHelper){
            auditHelper=new AuditHelper();
        }
        if (null==activityEventPublisher){
            activityEventPublisher= GoNoGoEventContext.getApplicationEventPublisher();
        }

    }

    public boolean saveFinalApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        //Commented unwanted Stage change code.

       /* // For LAP product do not change stage, status or any other property.
        if( ! StringUtils.equalsIgnoreCase(Product.LAP.toProductId(),
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getProductID() ) ){

            if (goNoGoCustomerApplication != null) {
                String applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
                ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                if (null != applicationStatus && null != applicationRequest) {
                    applicationRequest.setCurrentStageId(GngUtils.getCurrentStageBasedOnApplicationStatus(applicationStatus));
                }
            }
            ScoringResponse scoringResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse();
            if (scoringResponse != null) {
                if (scoringResponse.getDecisionResponse() != null) {
                    DecisionResponse decisionResponse = scoringResponse.getDecisionResponse();
                    goNoGoCustomerApplication.setApplicationStatus(decisionResponse.getDecision());
                    goNoGoCustomerApplication.getApplicationRequest().
                            setCurrentStageId(GngUtils.getCurrentStageBasedOnApplicationStatus(decisionResponse.getDecision()));
                    if (!"4019".equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId())) {
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(GNGWorkflowConstant.DEFAULT.toFaceValue());
                    }
                } else {
                    if ("4011".equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId().trim())) {
                        goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
                        goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(GNGWorkflowConstant.DEFAULT.toFaceValue());
                    } else {
                        goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(GNGWorkflowConstant.DEFAULT.toFaceValue());
                        goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
                    }
                }
            } else {
                goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
                goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
                goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(GNGWorkflowConstant.DEFAULT.toFaceValue());
            }
            if (GNGWorkflowConstant.BRE.toFaceValue().equals(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(GNGWorkflowConstant.DEFAULT.toFaceValue());
            }

            *//**
             *  If the re-initiate count is greater than zero then override the
             *  status to Queued and stage to CR_Q and assign CRO as default i.e CRO1
             *//*

            if (goNoGoCustomerApplication.getReInitiateCount() > 0) {
                goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
                goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
                goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.DEFAULT.toString().toLowerCase());
                updateGoNoGoCustomerApplication(goNoGoCustomerApplication);
            }
        }
*/
        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        //mongoTemplate.save(goNoGoCustomerApplication);

        if (!goNoGoCustomerApplication.getApplicationRequest().isQdeDecision()){
            boolean elStatus = indexingRepository.indexNotification(goNoGoCustomerApplication);
            logger.debug(" application with referenceId {} persisted in  data store and elasticsearch with status {} ", goNoGoCustomerApplication.getGngRefId(), elStatus);
        } else {
            logger.debug(" application with referenceId {} persisted in  data store  ", goNoGoCustomerApplication.getGngRefId());
        }
        return false;
    }

    /**
     * @param dsaId
     * @return
     */
    public String getCroId(String dsaId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("dsaId").is(dsaId).and("active").is(true));

        CroDsaMaster croDsaMaster = mongoTemplate.findOne(query, CroDsaMaster.class);

        if (croDsaMaster != null)
            return croDsaMaster.getCroId();
        else {
            return "default";
        }
    }

    /**
     * @param refId
     * @return
     */
    public ApplicationRequest getApplication(String refId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("_id").is(refId));

        return mongoTemplate.findOne(query, ApplicationRequest.class);

    }

    /**
     * This is uses for Do generation.
     *
     * @param refId
     * @return
     */
    public GoNoGoCustomerApplication getGoNoGoApplication(String refId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));
            query.fields().include("applicationRequest");
            return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }


    /**
     * @param croDsaMaster
     * @return
     */
    public boolean saveCroDsaMaster(CroDsaMaster croDsaMaster) {
        if (croDsaMaster != null) {
            mongoTemplate.insert(croDsaMaster);
            return true;
        }
        return false;
    }

    /**
     * @param applicationRequest
     * @return
     */
    public Set<DedupeDetails> deDupeCustomer(final ApplicationRequest applicationRequest) {

        logger.info(" checking application against deduplication !!");

        Request request = applicationRequest.getRequest();

        List<String> phones = request.getApplicant().getPhone().parallelStream()
                .filter(phone -> GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue().equalsIgnoreCase(phone.getPhoneType()))
                .map(Phone::getPhoneNumber)
                .collect(Collectors.toList());


        List<String> kycs = request.getApplicant().getKyc().parallelStream()
                .filter(s -> GNGWorkflowConstant.PAN.toFaceValue().equalsIgnoreCase(s.getKycName()) || GNGWorkflowConstant.AADHAAR.toFaceValue().equalsIgnoreCase(s.getKycName()))
                .map(Kyc::getKycNumber)
                .collect(Collectors.toList());

        String dob = request.getApplicant().getDateOfBirth();

        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria = criteria
                .orOperator(
                        Criteria.where(
                                "applicationRequest.request.applicant.kyc.kycNumber")
                                .in(kycs)
                                .and("applicationRequest.request.applicant.phone.phoneNumber")
                                .in(phones)
                                .and("applicationRequest.request.application.loanType")
                                .is(applicationRequest.getRequest().getApplication().getLoanType()),
                        Criteria.where(
                                "applicationRequest.request.applicant.phone.phoneNumber")
                                .in(phones)
                                .and("applicationRequest.request.applicant.dateOfBirth")
                                .is(dob)
                                .and("applicationRequest.request.application.loanType")
                                .is(applicationRequest.getRequest().getApplication().getLoanType()),
                        Criteria.where(
                                "applicationRequest.request.applicant.kyc.kycNumber")
                                .in(kycs)
                                .and("applicationRequest.request.applicant.dateOfBirth")
                                .is(dob)
                                .and("applicationRequest.request.application.loanType")
                                .is(applicationRequest.getRequest().getApplication().getLoanType())
                )
                .and("_id")
                .ne(applicationRequest.getRefID())
                .and("applicationRequest.header.institutionId")
                .is(applicationRequest.getHeader().getInstitutionId());

        query.addCriteria(criteria);

        Set<DedupeDetails> deDupeIdSet;

        List<GoNoGoCustomerApplication> dedupeResult = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        if (CollectionUtils.isNotEmpty( dedupeResult)) {
            deDupeIdSet = dedupeResult.parallelStream()
                    .map(application -> fillDedupeDetails(application)).collect(Collectors.toSet());
        } else {
            query = new Query();
            criteria = new Criteria();
            criteria = criteria
                    .orOperator(
                            Criteria.where(
                                    "applicationRequest.request.applicant.phone.phoneNumber")
                                    .in(phones),
                            Criteria.where(
                                    "applicationRequest.request.applicant.kyc.kycNumber")
                                    .in(kycs))
                    .and("applicationRequest._id")
                    .ne(applicationRequest.getRefID())
                    .and("applicationRequest.header.institutionId")
                    .is(applicationRequest.getHeader().getInstitutionId());
            query.addCriteria(criteria);

            dedupeResult = mongoTemplate.find(query,
                    GoNoGoCustomerApplication.class);

            deDupeIdSet = dedupeResult.parallelStream()
                    .map(application -> fillDedupeDetails(application)).collect(Collectors.toSet());
        }
        //logger.debug(" checked application against existing application  and got following set {} ", deDupeIdSet);

        return deDupeIdSet;
    }

    @Override
    public Set<DedupeDetails> deDupeCustomerV2(final ApplicationRequest applicationRequest) {

        logger.info(" checking application for internal dedupe !!");

        Request request = applicationRequest.getRequest();

        List<String> phones = request.getApplicant().getPhone().stream()
                .filter(phone -> GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue().equalsIgnoreCase(phone.getPhoneType()))
                .map(Phone::getPhoneNumber)
                .collect(Collectors.toList());

        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria = criteria
                .and("applicationRequest.request.applicant.applicantName.firstName")
                .is(request.getApplicant().getApplicantName().getFirstName())
                .and("applicationRequest.request.applicant.phone.phoneNumber")
                .in(phones)
                .and("applicationRequest.header.institutionId")
                .is(applicationRequest.getHeader().getInstitutionId());

        Criteria refIdCriteria=null;
        if(StringUtils.isNotEmpty(applicationRequest.getRefID()))
            refIdCriteria=Criteria.where("_id").ne(applicationRequest.getRefID());

        if(null!= refIdCriteria)
            criteria.andOperator(refIdCriteria);

        query.addCriteria(criteria);
        query.with(new Sort(Sort.Direction.DESC, "_id"));

        Set<DedupeDetails> deDupeIdSet=null;

        List<GoNoGoCustomerApplication> dedupeResult = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        if (CollectionUtils.isNotEmpty( dedupeResult)) {
            deDupeIdSet = dedupeResult.parallelStream()
                    .map(application -> fillDedupeDetails(application)).collect(Collectors.toSet());
        }
        //logger.debug(" checked application against existing application  and got following set {} ", deDupeIdSet);

        return deDupeIdSet;
    }

    private DedupeDetails fillDedupeDetails(GoNoGoCustomerApplication application) {
        DedupeDetails dedupeDetails = new DedupeDetails(application.getGngRefId());
        dedupeDetails.setRefId(application.getGngRefId());
        if(null != application.getApplicationRequest()) {
            Applicant applicant = application.getApplicationRequest().getRequest().getApplicant();
            if (null != applicant) {
                dedupeDetails.setKyc(applicant.getKyc());
                dedupeDetails.setPhone(applicant.getPhone());
                dedupeDetails.setApplicantName(applicant.getApplicantName());
                dedupeDetails.setDateOfBirth(applicant.getDateOfBirth());
                dedupeDetails.setSourceSystem("GONOGO");
                dedupeDetails.setApplicantId(applicant.getApplicantId());
                dedupeDetails.setDedupeID(application.getDedupeID());
            }
        }
        return dedupeDetails;
    }

    /**
     * @param goNoGoCustomerApplication update the application in the database;
     * @return true if update successful.
     */
    public boolean updateGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) throws SystemException {
        boolean result = Boolean.FALSE;
        try {

            //mongoTemplate.save(goNoGoCustomerApplication, MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString());
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(goNoGoCustomerApplication, dbObject);
            mongoTemplate.execute(GoNoGoCustomerApplication.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return null;
            });

            indexingRepository = new IndexingElasticSearchRepository();

            indexingRepository.indexNotification(goNoGoCustomerApplication);

            logger.info("Elastic Search DB has been updated successfully for RefId [{}]", goNoGoCustomerApplication.getGngRefId());

            result = Boolean.TRUE;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception occurred while saving gonogoCustomerApplication with probable cause {} ", e.getMessage());

        }

        return result;
    }


    public BaseResponse getDedupeApplicationDetails(ApplicationStatusLogRequest applicationStatusLogRequest ,boolean updateDedupeStatus) throws Exception {

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationStatusLogRequest.getRefId(), applicationStatusLogRequest.getHeader().getInstitutionId());

        BaseResponse baseResponse;

        List<DedupeApplicationDetails> dedupeApplicationDetailsList;

        if (null != goNoGoCroApplicationResponse && CollectionUtils.isNotEmpty(goNoGoCroApplicationResponse.getDedupedApplications())) {

            dedupeApplicationDetailsList = new ArrayList<>();

            // get dedupe Ref ID set
            Set<String> dedupeRefIdsSet = goNoGoCroApplicationResponse.getDedupedApplications();

            // get every dedupe application details
            dedupeApplicationDetailsList.addAll(getEveryDedupeCaseDetails(dedupeRefIdsSet ,updateDedupeStatus,applicationStatusLogRequest.getRefId()));

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dedupeApplicationDetailsList);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }


    private List<DedupeApplicationDetails> getEveryDedupeCaseDetails(Set<String> dedupeRefIdSet ,boolean updateDedupeStatus,String parentRefId) throws Exception {

        List<DedupeApplicationDetails> dedupeApplicationDetailsList = new ArrayList<>();

        //get every dedupe case details
        for (String refId : dedupeRefIdSet) {

            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);

            if (null != goNoGoCustomerApplication) {

                //build Dedupe application details
                DedupeApplicationDetails dedupeApplicationDetails = buildDedupeApplicationDetails(goNoGoCustomerApplication ,updateDedupeStatus,parentRefId);

                dedupeApplicationDetailsList.add(dedupeApplicationDetails);
            }

            if (!CollectionUtils.isEmpty(dedupeApplicationDetailsList)) {
                Collections.sort(dedupeApplicationDetailsList);
            }

        }

        return dedupeApplicationDetailsList;
    }

    private DedupeApplicationDetails buildDedupeApplicationDetails(GoNoGoCustomerApplication goNoGoCustomerApplication ,boolean updateDedupeStatus,String parentRefId) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, goNoGoCustomerApplication.getApplicationRequest().getHeader());
        activityLog.setAction(ActionName.AUTO_CANCEL_DEDUPE_CASE.toString());
        activityLog.setRefId(goNoGoCustomerApplication.getGngRefId());
        activityLog.setStage(GNGWorkflowConstant.CNCLD.toFaceValue());
        activityLog.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        activityLog.setStatus(Status.FAIL.toString());

        DedupeApplicationDetails dedupeApplicationDetails = new DedupeApplicationDetails();

        String institutionId = null;
        String applicationStage =null;
        String applicationStatus=null;
        String gngRefId= null;
        String productId = null;

        gngRefId = goNoGoCustomerApplication.getGngRefId();
        dedupeApplicationDetails.setRefID(gngRefId);
        dedupeApplicationDetails.setLoginDate(GngDateUtil.getDDMMYYYYFormat(goNoGoCustomerApplication.getDateTime()));

        //Submit date is to sort the list so that the application submitted recently will come first.
        dedupeApplicationDetails.setSubmitDate(goNoGoCustomerApplication.getDateTime());

        applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
        dedupeApplicationDetails.setApplicationStatus(applicationStatus);

        if (null != goNoGoCustomerApplication.getApplicationRequest()) {
            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            applicationStage = applicationRequest.getCurrentStageId();
            dedupeApplicationDetails.setCurrentStage(applicationStage);
            if (null != applicationRequest.getHeader()) {
                Header header = applicationRequest.getHeader();
                dedupeApplicationDetails.setDealerId(header.getDealerId());
                institutionId = header.getInstitutionId();
                if(null!=header.getProduct()){
                    productId = header.getProduct().toProductId();
                }
            }
        }

        dedupeApplicationDetails.setDealerName(GngUtils.getDealerName(goNoGoCustomerApplication));
        dedupeApplicationDetails.setCibilScore(GngUtils.getCibilScore(goNoGoCustomerApplication));

        setDeliveryOrderStatus(gngRefId, institutionId, dedupeApplicationDetails);
        // check action to update dedupe application status i.e AUTO_CANCEL_DEDUPE_CASE
        LookupService lookupService  =new LookupServiceHandler();
        boolean actionFlag = lookupService.checkActionsAccess(institutionId, productId, ActionName.AUTO_CANCEL_DEDUPE_CASE);

       if (actionFlag && updateDedupeStatus) {
            //change the decision of previous dedupe application to cancelled if the application status is approved ,stage is APRV and do raised status is NO
            if (StringUtils.equals(applicationStatus, GNGWorkflowConstant.APPROVED.toFaceValue())
                    && StringUtils.equals(applicationStage, GNGWorkflowConstant.APRV.toFaceValue())
                    && StringUtils.equals(dedupeApplicationDetails.getDoRaisedStatus(), Status.NO.name())) {

                // build cancelledApplicationDetails object
                CancelApplicationDetails cancelApplicationDetails = buildCancelApplicationDetails(goNoGoCustomerApplication,
                        GNGWorkflowConstant.CANCELLED.toFaceValue(), GNGWorkflowConstant.CNCLD.toFaceValue() ,parentRefId);

                //Cro Justification needed to understand the reason for cancelling the approved application.
                String croJustificationRemark = String.format(ErrorCode.AUTO_DEDUPE_CANCEL_DESCRIPTION, parentRefId);


                // update dedupe application stage and status
                applicationRepository.updateDedupeApplicationStatus(institutionId, gngRefId, GNGWorkflowConstant.CANCELLED.toFaceValue(),
                        GNGWorkflowConstant.CNCLD.toFaceValue(), goNoGoCustomerApplication, croJustificationRemark);

                // save Cancel application log in the database
                adminLogRepository.saveCancelledApplicationLog(cancelApplicationDetails);
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg("Application Cancel Successfully");

            }
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return dedupeApplicationDetails;

    }

    private CancelApplicationDetails buildCancelApplicationDetails(GoNoGoCustomerApplication goNoGoCustomerApplication, String appStatus, String appStage ,String parentRefId) {

        CancelApplicationDetails cancelApplicationDetails = new CancelApplicationDetails();

        cancelApplicationDetails.setRefId(goNoGoCustomerApplication.getGngRefId());
        cancelApplicationDetails.setPreviousStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
        cancelApplicationDetails.setPreviousStatus(goNoGoCustomerApplication.getApplicationStatus());
        cancelApplicationDetails.setCurrentStage(appStage);
        cancelApplicationDetails.setCurrentStatus(appStatus);
        cancelApplicationDetails.setStatusChangeReason(Status.DEDUPE_FOUND_AUTO_CANCELLED.name());
        cancelApplicationDetails.setStatusUpdateDate(new Date());
        cancelApplicationDetails.setParentRefId(parentRefId);

        return cancelApplicationDetails;
    }

    private void setDeliveryOrderStatus(String gngRefId, String institutionId, DedupeApplicationDetails dedupeApplicationDetails) throws Exception {

        PostIPA postIPA = applicationRepository.getPostIPA(gngRefId, institutionId);

        //By default set DO mail status to "NO"
        dedupeApplicationDetails.setDoRaisedStatus(Status.NO.name());
        dedupeApplicationDetails.setDoMailStatus(Status.NO.name());


        // check post ipa details exist or not
        if (null != postIPA) {

            //form Delivery Order Name using RefId and DO_
            String deliveryOrderName = GngUtils.formDeliveryOrderName(gngRefId);

            // get Image Id using RefId and DeliveryOrderName
            String DOImageId = uploadFileRepository.getImageIdByRefIdAndFileName(gngRefId, deliveryOrderName);

            // get Delivery Order Image Status using DO Image Id

            if (StringUtils.isNotBlank(DOImageId)) {

                // here delivery order pdf found
                dedupeApplicationDetails.setDoRaisedStatus(Status.YES.name());

                GetFileRequest getImageRequest = new GetFileRequest();

                getImageRequest.setImageFileID(DOImageId);
                getImageRequest.setRefID(gngRefId);

                // check mail status in the mail log.
                if (adminLogRepository.checkEmailDedupe(getImageRequest)) {
                    //here delivery order mail status found
                    dedupeApplicationDetails.setDoMailStatus(Status.YES.name());
                }

            }

        }

    }

    public boolean saveDedupeInfo(DedupeMatch dedupeMatch){
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(dedupeMatch.getRefId())
                    .and("institutionId").is(dedupeMatch.getInstitutionId()));
            Update update = new Update();
            update.set("dedupeMatchDetails", dedupeMatch.getDedupeMatchDetails());
            mongoTemplate.upsert(query, update, DedupeMatch.class);
            logger.info("Dedupe match info saved successfully");
        } catch (DataAccessException e) {
            logger.error("Exception caught while saving dedupe Match details " + e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("Exception caught while saving dedupe match details %s", e.getMessage()));
        }
        return true;
    }
}
