package com.softcell.dao.mongodb.repository.reporting;

import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * @author kishor
 */
public interface CustomReportRepository {
    /**
     * @param query Query is used to filter data which required to build custom
     *              report; It has filter on date, product type, institution id
     *              and report. this query will execute on main customer
     *              application collection.
     * @return
     */
    public List<?> applicationReportData(Query query);

    /**
     * @param query Query is used to filter data which required to build custom
     *              report; It has filter on date, product type, institution id
     *              and report. this query will fire against partial application
     *              collection.
     * @return
     */
    public List<?> partialApplicationReportData(Query query);

    /**
     * @param query Query is used to filter data which required to build custom
     *              report; It has filter on date, product type, institution id
     *              and report. this query will execute on main customer
     *              application collection.
     * @return return true if local file of data is successfully generated. else
     * false.
     */
    public boolean applicationReportDataLocalCopy(Query query);

    /**
     * @param query Query is used to filter data which required to build custom
     *              report; It has filter on date, product type, institution id
     *              and report. this query will fire against partial application
     *              collection.
     * @return return true if local file of data is successfully generated. else
     * false.
     */
    public boolean partialApplicationReportDataLocalCopy(Query query);

}
