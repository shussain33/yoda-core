package com.softcell.dao.mongodb.repository;

import com.itextpdf.text.Image;
import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Institute;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.utils.DMSUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/**
 * @author prateek
 */
@Repository
public class UploadFileMongoRepository implements UploadFileRepository {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileMongoRepository.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    GridFsTemplate gridFsTemplate;

    @Inject
    public UploadFileMongoRepository(MongoTemplate mongoTemplate,
                                     GridFsTemplate gridFsTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.gridFsTemplate = gridFsTemplate;
    }

    @Override
    public String store(InputStream inputStream, String fileName,
                        String contentType, Object metaData) {
        return this.gridFsTemplate
                .store(inputStream, fileName, contentType, metaData).getId()
                .toString();
    }

    @Override
    public boolean updateStatus(FileUploadRequest fileUploadRequest) {
        try {
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria
                    .where("metadata.gonogoReferanceId")
                    .is(fileUploadRequest.getGonogoReferanceId()).and("_id")
                    .is(fileUploadRequest.getImageID())
                    .and("metadata.fileHeader.institutionId")
                    .is(fileUploadRequest.getFileHeader().getInstitutionId()));
            Update update = new Update();
            update.set("metadata.uploadFileDetails.status", fileUploadRequest
                    .getUploadFileDetails().getStatus());
            update.set("metadata.uploadFileDetails.reason", fileUploadRequest
                    .getUploadFileDetails().getReason());
            mongoTemplate.updateFirst(selectQuery, update, "fs.files");
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updating status of document images with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updating status of document images with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean updateImageStatus(String imageID, String refID,
                                     String status, String reason) {
        try {
            // first fetch the file and metadata.
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria
                    .where("metadata.gonogoReferanceId").is(refID).and("_id")
                    .is(imageID));
            GridFSDBFile docDetails = this.gridFsTemplate.findOne(selectQuery);

            /**
             * set the updated data to the metadata pojo.
             */

            DBObject dbObject = docDetails.getMetaData();
            DBObject fileHeaderDb = (DBObject) dbObject.get("fileHeader");
            Date uploadDate = (Date) fileHeaderDb.get("dateTime");

            DBObject uploadFileDetailsDb = (DBObject) dbObject
                    .get("uploadFileDetails");

            FileUploadRequest fileUploadRequest = new FileUploadRequest();
            fileUploadRequest.setGonogoReferanceId(dbObject.get(
                    "gonogoReferanceId").toString());

            FileHeader fileHeader = new FileHeader();
            fileHeader.setApplicationId(fileHeaderDb.get("applicationId")
                    .toString());
            fileHeader.setApplicantId(fileHeaderDb.get("applicantId")
                    .toString());
            fileHeader.setInstitutionId(fileHeaderDb.get("institutionId")
                    .toString());
            fileHeader.setDateTime(uploadDate);
            fileHeader.setCroId(fileHeaderDb.get("croId").toString());

            fileUploadRequest.setFileHeader(fileHeader);

            UploadFileDetails uploadFileDetails = new UploadFileDetails();
            uploadFileDetails.setFileId(uploadFileDetailsDb.get("fileId")
                    .toString());
            uploadFileDetails.setFileName(uploadFileDetailsDb.get("FileName")
                    .toString());
            uploadFileDetails.setFileType(uploadFileDetailsDb.get("fileType")
                    .toString());
            uploadFileDetails.setStatus(status);
            uploadFileDetails.setReason(reason);

            fileUploadRequest.setUploadFileDetails(uploadFileDetails);
            InputStream inputStream = docDetails.getInputStream();
            String id = gridFsTemplate
                    .store(inputStream, docDetails.getFilename(),
                            docDetails.getContentType(), fileUploadRequest)
                    .getId().toString();

            // delete old image
            Query deleteQuery = new Query();
            deleteQuery.addCriteria(Criteria.where("_id").is(imageID));
            gridFsTemplate.delete(deleteQuery);
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updating Image Status with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updating Image Status of document images with probable cause [{%s}]", e.getMessage()));

        }
    }

    @Override
    public GridFSDBFile getById(String id, String institutionID) {
        return this.gridFsTemplate.findOne(new Query(Criteria.where("_id")
                .is(id).and("metadata.fileHeader.institutionId")
                .is(institutionID)));
    }

    @Override
    public GridFSDBFile getByFilename(String fileName) {
        return gridFsTemplate.findOne(new Query(Criteria.where("filename").is(
            fileName)));
}

    @Override
    public GridFSDBFile getByFileId(String fileId, String institutionID) {
        return this.gridFsTemplate.findOne(
                new Query(Criteria.where("metadata.uploadFileDetails.fileId")
                                    .is(fileId).and("metadata.fileHeader.institutionId")
                .is(institutionID)));
    }

    @Override
    public GridFSDBFile retrive(String fileName) {
        return gridFsTemplate.findOne(new Query(Criteria.where(
                "metadata.gonogoReferanceId").is(fileName)));
    }

    @Override
    public GridFSDBFile getImage(String fileName, String refID,
                                 String applicantID) {
        return gridFsTemplate.findOne(new Query(Criteria
                .where("metadata.uploadFileDetails.FileName").is(fileName)
                .and("metadata.gonogoReferanceId").is(refID)
                .and("metadata.fileHeader.applicantId").is(applicantID)));
    }

    @Override
    public List<GridFSDBFile> getImages(String refID, String applicantID) {
        return gridFsTemplate.find(new Query(Criteria
                .where("metadata.gonogoReferanceId").is(refID)
                .and("metadata.fileHeader.applicantId").is(applicantID)));
    }

    public List<KycImageDetails> getKycImageDetailsOld(String refID,
                                                       String institutionId) {
        Sort sort = new Sort(Sort.Direction.ASC, "filename");
        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(new Query(
                Criteria.where("metadata.gonogoReferanceId").is(refID)
                        .and("metadata.fileHeader.institutionId")
                        .is(institutionId)
                        .and("metadata.uploadFileDetails.fileType")
                        .ne("application/pdf")).with(sort));

        Map<String, List<ImagesDetails>> uniqueImageMap = new HashMap<String, List<ImagesDetails>>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {
            DBObject dbObject = gridFSDBFile.getMetaData();
            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");
            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");
            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));
            if (uniqueImageMap.containsKey(applicantId)) {
                ImagesDetails imageDetail = new ImagesDetails();
                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                // TODO: Set below fileName in imageName once correction on UI
                // side is Done.
                imageDetail.setImageType(gridFSDBFile.getFilename());
                imageDetail.setStatus(String.valueOf(uploadFileDetails
                        .get("status")));
                imageDetail.setReason(String.valueOf(uploadFileDetails
                        .get("reason")));
                // TODO: Set below fileType in image type once correction on UI
                // side is Done.
                imageDetail.setImageName(String.valueOf(uploadFileDetails
                        .get("fileType") != null ? uploadFileDetails
                        .get("fileType") : ""));
                imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                        .get("fileExtension") != null ? uploadFileDetails
                        .get("fileExtension") : FieldSeparator.BLANK));
                uniqueImageMap.get(applicantId).add(imageDetail);
            } else {

                List<ImagesDetails> imageList = initiateImageObject(gridFSDBFile, uploadFileDetails);

                uniqueImageMap.put(applicantId, imageList);
            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        return kycImageDetailsList;
    }

    @Override
    public List<KycImageDetails> getKycImageDetails(Set<String> refIdSet,
                                                    String institutionId, String typeOfDocument) {

        logger.debug("getKycImageDetails repo started");

        Query query = new Query();

        /**
         *  if typeOfDocument is "IMAGES" then it will create query according to fetch only images otherwise
         *  this service will return all types of document i.e. application/pdf ,jpg etc. it will used
         *  for push all documents to DMS purpose.
         */

        List<String> contentTypeExcludeList = new ArrayList<>();
        contentTypeExcludeList.add("application/pdf");
        contentTypeExcludeList.add("application/csv");

        if (StringUtils.equals(typeOfDocument, Status.IMAGES.name())) {
            query.addCriteria(Criteria.where("metadata.gonogoReferanceId").in(refIdSet)
                    .and("metadata.fileHeader.institutionId").is(institutionId)
                    .and("metadata.uploadFileDetails.fileType")
                    .nin(contentTypeExcludeList)
                    .and("metadata.uploadFileDetails.deleted").is(false));
        } else {
            query.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                    .in(refIdSet).and("metadata.fileHeader.institutionId")
                    .is(institutionId)
                    .and("metadata.uploadFileDetails.deleted").is(false));
        }

        logger.debug("getKycImageDetails repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

        Map<String, List<ImagesDetails>> uniqueImageMap = new HashMap<String, List<ImagesDetails>>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

            DBObject dbObject = gridFSDBFile.getMetaData();

            Date uploadDate = gridFSDBFile.getUploadDate();

            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");

            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");

            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));

            if (uniqueImageMap.containsKey(applicantId)) {

                ImagesDetails imageDetail = new ImagesDetails();

                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));

                imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));


                Object statusObject = uploadFileDetails.get("status");

                if (null != statusObject) {

                    imageDetail.setStatus(String.valueOf(statusObject));

                }
                Object fileNameObject = uploadFileDetails.get("fileName");

                // TODO: Set below fileName in imageName once correction on UI side is Done.
                if (null != fileNameObject) {
                    imageDetail.setImageType(String.valueOf(fileNameObject));
                }

                if(null != uploadDate) {
                    imageDetail.setUploadDate(uploadDate);
                }
                Object reasonObject = uploadFileDetails.get("reason");

                if (null != reasonObject) {

                    imageDetail.setReason(String.valueOf(reasonObject));

                }

                Object collateralId = uploadFileDetails.get("collateralId");

                if (null != collateralId) {

                    imageDetail.setCollateralId(String.valueOf(collateralId));

                }

                Object applicantID = uploadFileDetails.get("applicantId");

                if (null != applicantID) {

                    imageDetail.setApplicantId(String.valueOf(applicantID));

                }

                Object remark = uploadFileDetails.get("remark");
                if (null != remark)
                    imageDetail.setRemark(String.valueOf(remark));
                // TODO: Set below fileType in image type once correction on UI
                // side is Done.

                imageDetail.setImageName(String.valueOf(uploadFileDetails
                        .get("fileType") != null ? uploadFileDetails
                        .get("fileType") : ""));

                imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                        .get("fileExtension") != null ? uploadFileDetails
                        .get("fileExtension") : ""));

                uniqueImageMap.get(applicantId).add(imageDetail);

            } else {

                List<ImagesDetails> imageList = initiateImageObject(gridFSDBFile, uploadFileDetails);

                uniqueImageMap.put(applicantId, imageList);

            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        logger.debug("getKycImageDetails repo completed with response size {}", kycImageDetailsList.size());

        return kycImageDetailsList;
    }


    @Override
    public List<ImagesDetails> getDocumentsForDmsPush(String refId,
                                                      String institutionId) {
        logger.debug("getDocumentsForDmsPush repo started");

        Query query = new Query();

        query.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                .is(refId).and("metadata.fileHeader.institutionId").is(institutionId)
                .and("metadata.uploadFileDetails.deleted").is(false));

        query.with(new Sort(Sort.Direction.DESC, "metadata.fileHeader.dateTime"));

        logger.debug("getDocumentsForDmsPush repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

        List<ImagesDetails> imageList = new ArrayList<>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

            ImagesDetails imageDetail = new ImagesDetails();

            DBObject dbObject = gridFSDBFile.getMetaData();

            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");

            String imageName = String.valueOf(uploadFileDetails.get("FileName"));
            String imageType = String.valueOf(uploadFileDetails.get("fileType"));

            boolean duplicateImage = false;

            if (StringUtils.isNotBlank(imageType) && StringUtils.isNotBlank(imageName)) {

                for (ImagesDetails imageDetail1 : imageList) {
                    if (StringUtils.equalsIgnoreCase(imageDetail1.getImageName(), imageName)
                            && StringUtils.equalsIgnoreCase(imageDetail1.getImageType(), imageType)
                            && !DMSUtils.getRepeatedAllowedDocument().contains(imageName)) {

                        duplicateImage = true;
                        break;

                    }
                }
                if (!duplicateImage) {
                    imageDetail.setImageName(imageName);
                    imageDetail.setImageType(imageType);
                    imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));
                    imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                    imageDetail.setStatus(String.valueOf(uploadFileDetails.get("status")));
                    imageDetail.setReason(String.valueOf(uploadFileDetails.get("reason")));
                    // add in list
                    imageList.add(imageDetail);
                }

            }
        }
        return imageList;

    }


    @Deprecated
    @Override
    public List<KycImageDetails> getAdditionalImageDetails(String refID,
                                                           String institutionId) {
        Sort sort = new Sort(Sort.Direction.ASC, "filename");
        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(new Query(
                Criteria.where("metadata.gonogoReferanceId").is(refID).and(""))
                .with(sort));

        TreeMap<String, List<ImagesDetails>> uniqueImageMap = new TreeMap<String, List<ImagesDetails>>();
        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {
            DBObject dbObject = gridFSDBFile.getMetaData();
            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");
            String blockName = getBlockName(gridFSDBFile.getFilename());
            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");
            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));
            if (uniqueImageMap.containsKey(blockName)) {
                ImagesDetails imageDetail = new ImagesDetails();
                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                imageDetail.setImageType(gridFSDBFile.getFilename());
                imageDetail.setStatus(String.valueOf(uploadFileDetails
                        .get("status")));
                imageDetail.setReason(String.valueOf(uploadFileDetails
                        .get("reason")));
                imageDetail.setBlockName(blockName);
                imageDetail.setApplicantId(applicantId);
                uniqueImageMap.get(blockName).add(imageDetail);
            } else {

                List<ImagesDetails> imageList = new ArrayList<ImagesDetails>();
                ImagesDetails imageDetail = new ImagesDetails();
                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                imageDetail.setImageType(gridFSDBFile.getFilename());
                imageDetail.setStatus(String.valueOf(uploadFileDetails
                        .get("status")));
                imageDetail.setReason(String.valueOf(uploadFileDetails
                        .get("reason")));
                imageDetail.setApplicantId(applicantId);
                imageDetail.setBlockName(blockName);
                imageList.add(imageDetail);
                uniqueImageMap.put(blockName, imageList);
            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        return kycImageDetailsList;
    }

    /**
     * @param filename
     * @return
     */
    private String getBlockName(String filename) {
        if (StringUtils.containsIgnoreCase(filename, "APPLICATION_FORM")) {
            return "Application Form";
        }
        if (StringUtils.containsIgnoreCase(filename, "DISBURSEMENT")) {
            return "Disbursement Kit";
        }

        if (StringUtils.containsIgnoreCase(filename, "AGREEMENT")) {
            return "Agreement";
        }
        if (StringUtils.containsIgnoreCase(filename, "ACH_")) {
            return "ACH/ECS & Cancelled Cheque";
        }
        if (StringUtils.containsIgnoreCase(filename, "ADDITIONAL_KYC")) {
            return "Additional KYC";
        }
        return "default";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * UploadFileRepository#getPDFByFilenameNRefID
     * (java.lang.String)
     */
    @Override
    public GridFSDBFile getPDFByFilenameNRefID(String refId ,String institutionId ,String deliveryOrderName ) {

        Query selectQuery = new Query();
        Pattern pattern = Pattern.compile("^" + Pattern.quote(deliveryOrderName));

        selectQuery.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                .is(refId)
                .and("metadata.uploadFileDetails.fileType")
                .is("application/pdf").and("metadata.fileHeader.institutionId")
                .is(institutionId)
                .and("metadata.uploadFileDetails.deleted").is(false)
                .and("metadata.uploadFileDetails.FileName").regex(pattern));
        return this.gridFsTemplate.findOne(selectQuery);
    }

    @Override
    public byte[] getCibilReport(String refID, String institutionID) {
        try {
            Query selectQuery = new Query();
            selectQuery
                    .addCriteria(Criteria
                            .where("_id")
                            .is(refID)
                            .and("applicationRequest.header.institutionId")
                            .is(institutionID)
                            .and("applicantComponentResponse.multiBureauJsonRespose.status")
                            .is("COMPLETED")
                            .and("applicantComponentResponse.multiBureauJsonRespose.finishedList.status")
                            .is("SUCCESS"));

            selectQuery.fields().include("applicantComponentResponse.multiBureauJsonRespose.finishedList.pdfReport");

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(selectQuery, GoNoGoCustomerApplication.class);
            if (goNoGoCustomerApplication != null
                    && goNoGoCustomerApplication
                    .getApplicantComponentResponse() != null
                    && goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getMultiBureauJsonRespose() != null
                    && goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getMultiBureauJsonRespose().getFinishedList()
                    .size() > 0) {

                byte[] cibilPdfReport = goNoGoCustomerApplication
                        .getApplicantComponentResponse()
                        .getMultiBureauJsonRespose().getFinishedList().get(0)
                        .getPdfReport();
                return cibilPdfReport;
            } else {
                return null;
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            logger.error("Error occurred while get Cibil Report for institutionId {} and referenceId {} with probable cause [{}]", exp.getMessage());
            throw new SystemException(String.format("Error occurred while get Cibil Report for institutionId {%s} and referenceId {%s} with probable cause [{%s}]", exp.getMessage()));

        }
    }

    @Override
    public boolean updateFileAmazonUrl(String imageId, String amazonFileUrl) {
        try {
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria.where("_id").is(imageId));
            Update update = new Update();
            update.set("metadata.uploadFileDetails.amazonFileUrl",
                    amazonFileUrl);
            mongoTemplate.updateFirst(selectQuery, update, "fs.files");
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updateFileAmazonUrl for imageId {} with probable cause [{}]", imageId, e.getMessage());
            throw new SystemException(String.format("Error occurred while updateFileAmazonUrl for imageId {%s} with probable cause [{%s}]", imageId, e.getMessage()));
        }
    }

    @Override
    public boolean isUploadedToAmazonS3(String imageId) {
        Query query = new Query();
        /**
         * Check if the metadata.uploadFileDetails.amazonFileUrl key exists for
         * the given imageId
         */
        query.addCriteria(Criteria
                .where("_id")
                .is(imageId)
                .andOperator(
                        Criteria.where(
                                "metadata.uploadFileDetails.amazonFileUrl")
                                .exists(true),
                        Criteria.where(
                                "metadata.uploadFileDetails.amazonFileUrl").ne(
                                false)));
        return mongoTemplate.exists(query, "fs.files");
    }

    @Override
    public Image getHdbfsImage(FileUploadRequest fileUploadRequest) {
        return getLogoImage(gridFsTemplate.findOne(new Query(Criteria
                .where("metadata.gonogoReferanceId")
                .is(fileUploadRequest.getGonogoReferanceId())
                .and("metadata.fileHeader.institutionId")
                .is(fileUploadRequest.getFileHeader().getInstitutionId())
                .and("metadata.uploadFileDetails.fileType")
                .is(fileUploadRequest.getUploadFileDetails().getFileType())
                .and("metadata.uploadFileDetails.FileName")
                .is(fileUploadRequest.getUploadFileDetails().getFileName()))));
    }

    @Override
    public boolean updateAppImageLinkage(FileUploadRequest fileUploadRequest) {

        try {
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria
                    .where("metadata.gonogoReferanceId")
                    .is(fileUploadRequest.getGonogoReferanceId()).and("_id")
                    .is(fileUploadRequest.getImageID())
                    .and("metadata.fileHeader.institutionId")
                    .is(fileUploadRequest.getFileHeader().getInstitutionId()));
            Update update = new Update();
            update.set("metadata.appImageLinkage",
                    fileUploadRequest.getAppImageLinkage());
            mongoTemplate.updateFirst(selectQuery, update, "fs.files");
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updateAppImageLinkage with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updateAppImageLinkage with probable cause [{%s}]", e.getMessage()));
        }

    }

    @Override
    public boolean updateAppImageLinkage(String refId,
                                         Set<String> appImageLinkage) {

        try {
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria
                    .where("metadata.gonogoReferanceId").is(refId));
            Update update = new Update();
            update.set("metadata.appImageLinkage", appImageLinkage);
            mongoTemplate.updateFirst(selectQuery, update, "fs.files");
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updateAppImageLinkage for refId {} with probable cause [{}]", refId, e.getMessage());
            throw new SystemException(String.format("Error occurred while updateAppImageLinkage for refId {%s}  with probable cause [{%s}]", refId, e.getMessage()));

        }

    }

    @Override
    public Set<String> getAppImageLinkageArray(String refID) {
        Set<String> appImageLinkage = null;
        Query query = new Query(Criteria.where("metadata.gonogoReferanceId")
                .is(refID).and("metadata.uploadFileDetails.fileType")
                .ne("application/pdf"));
        GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(query);

        if (gridFSDBFile != null) {
            DBObject dbObject = gridFSDBFile.getMetaData();
            Object appImageLinkageObj = dbObject.get("appImageLinkage");
            if (appImageLinkageObj != null) {
                BasicDBList appImagelist = (BasicDBList) appImageLinkageObj;
                appImageLinkage = new TreeSet(appImagelist);
            }
        }
        return appImageLinkage;

    }

    private Image getLogoImage(GridFSDBFile gridFSDBFile) {
        try {
            InputStream inputStream = gridFSDBFile.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BufferedImage bufImg = ImageIO.read(inputStream);
            ImageIO.write(bufImg, "png", baos);
            return setHdbfsLogoProperty(Image.getInstance(baos.toByteArray()));
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while getLogoImage  with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getLogoImage with probable cause [{%s}]", e.getMessage()));
        }

    }

    private Image setHdbfsLogoProperty(Image hdbfsLogoImage) {
        hdbfsLogoImage.setAlignment(Image.LEFT);
        hdbfsLogoImage.scaleAbsoluteHeight(20);
        hdbfsLogoImage.scaleAbsoluteWidth(20);
        hdbfsLogoImage.scalePercent(20);
        return hdbfsLogoImage;
    }

    @Override
    public List<KycImageDetails> getKycImageDetailsByApplicant(String refId,
                                                               String applicantID, String institutionId) {

        logger.debug("getKycImageDetailsByApplicant repo started");

        Query query = new Query();
        query.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                .is(refId).and("metadata.fileHeader.applicantId")
                .is(applicantID).and("metadata.fileHeader.institutionId")
                .is(institutionId).and("metadata.uploadFileDetails.fileType")
                .ne("application/pdf"));

        logger.debug("getKycImageDetailsByApplicant repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

        logger.debug("getKycImageDetailsByApplicant repo query result size {}", gridFSDBFileList.size());

        Map<String, List<ImagesDetails>> uniqueImageMap = new HashMap<String, List<ImagesDetails>>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

            DBObject dbObject = gridFSDBFile.getMetaData();

            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");

            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");

            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));

            if (uniqueImageMap.containsKey(applicantId)) {

                ImagesDetails imageDetail = new ImagesDetails();
                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));
                // TODO: Set below fileName in imageName once correction on  side is Done.
                imageDetail.setImageType(gridFSDBFile.getFilename());
                imageDetail.setStatus(String.valueOf(uploadFileDetails.get("status")));
                imageDetail.setReason(String.valueOf(uploadFileDetails.get("reason")));
                // TODO: Set below fileType in image type once correction on UI side is Done.
                imageDetail.setImageName(String.valueOf(uploadFileDetails
                        .get("fileType") != null ? uploadFileDetails
                        .get("fileType") : ""));

                imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                        .get("fileExtension") != null ? uploadFileDetails
                        .get("fileExtension") : ""));

                uniqueImageMap.get(applicantId).add(imageDetail);

            } else {

                List<ImagesDetails> imageList = initiateImageObject(gridFSDBFile, uploadFileDetails);

                uniqueImageMap.put(applicantId, imageList);
            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        logger.debug("getKycImageDetailsByApplicant repo call completed ");

        return kycImageDetailsList;
    }

    @Override
    public byte[] getBureauReport(String refID, String institutionId, String bureau) {
        try {
            Query selectQuery = new Query();
            selectQuery
                    .addCriteria(Criteria
                            .where("_id")
                            .is(refID)
                            .and("applicationRequest.header.institutionId")
                            .is(institutionId)
                            .and("applicantComponentResponse.multiBureauJsonRespose.status")
                            .is("COMPLETED")
                            .and("applicantComponentResponse.multiBureauJsonRespose.finishedList.status")
                            .is("SUCCESS")
                            .and("applicantComponentResponse.multiBureauJsonRespose.finishedList.bureau")
                            .is(bureau));

            selectQuery.fields().include("applicantComponentResponse.multiBureauJsonRespose.finishedList.pdfReport");
            selectQuery.fields().include("applicantComponentResponse.multiBureauJsonRespose.finishedList.bureau");

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate
                    .findOne(selectQuery, GoNoGoCustomerApplication.class);

            byte[] pdfReport = new byte[0];

            if (goNoGoCustomerApplication != null
                    && goNoGoCustomerApplication.getApplicantComponentResponse() != null
                    && goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose() != null
                    && !CollectionUtils.isEmpty(goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList())) {

                List<Finished> finishedList = goNoGoCustomerApplication
                        .getApplicantComponentResponse()
                        .getMultiBureauJsonRespose().getFinishedList();

                for (Finished finished : finishedList) {
                    if (StringUtils.equals(bureau, finished.getBureau())) {
                        pdfReport = finished.getPdfReport();
                        break;
                    }
                }
            }
            return pdfReport;
        } catch (Exception exp) {
            exp.printStackTrace();
            logger.error("Error occurred while getting {} PDF Report for institutionId {} and referenceId {} with probable cause [{}]", bureau, institutionId, refID, exp.getMessage());
            throw new SystemException(String.format("Error occurred while getting [%s] pdf Report for institutionId {%s} and referenceId {%s} with probable cause [{%s}]", bureau, institutionId, refID, exp.getMessage()));
        }
    }

    @Override
    public byte[] getCibilReportForCoApplicant(String coApplicantId) {
        try {
            Query selectQuery = new Query();
            selectQuery.addCriteria(Criteria.where("coApplicantRefId")
                    .is(coApplicantId).and("multiBureauJsonRespose.status")
                    .is("COMPLETED")
                    .and("multiBureauJsonRespose.finishedList.status")
                    .is("SUCCESS"));
            MultiBureauCoApplicantResponse mbCoApplicantResponse = mongoTemplate
                    .findOne(selectQuery, MultiBureauCoApplicantResponse.class);
            if (mbCoApplicantResponse != null
                    && mbCoApplicantResponse.getMultiBureauJsonRespose() != null
                    && mbCoApplicantResponse.getMultiBureauJsonRespose()
                    .getFinishedList().size() > 0) {
                byte[] cibilPdfReport = mbCoApplicantResponse
                        .getMultiBureauJsonRespose().getFinishedList().get(0)
                        .getPdfReport();
                return cibilPdfReport;
            } else {
                return null;
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            logger.error("Error occurred while getCibilReportForCoApplicant for coaaplicant {}  with probable cause [{}]", coApplicantId, exp.getMessage());
            throw new SystemException(String.format("Error occurred while getCibilReportForCoApplicant   for coaaplicant {%s} with probable cause [{%s}]", coApplicantId, exp.getMessage()));
        }
    }

    private List<ImagesDetails> initiateImageObject(GridFSDBFile gridFSDBFile, DBObject uploadFileDetails) {

        DBObject dbObject = gridFSDBFile.getMetaData();
        List<ImagesDetails> imageList = new ArrayList<ImagesDetails>();

        ImagesDetails imageDetail = new ImagesDetails();

        imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));
        imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
        imageDetail.setImageType(gridFSDBFile.getFilename());
        imageDetail.setStatus(String.valueOf(uploadFileDetails.get("status")));
        imageDetail.setReason(String.valueOf(uploadFileDetails.get("reason")));
        Object remarkObject = uploadFileDetails.get("remark");
        if (null != remarkObject) {
            imageDetail.setRemark(String.valueOf(remarkObject));
        }
        Object collateralId = uploadFileDetails.get("collateralId");
        if (null != collateralId) {
            imageDetail.setCollateralId(String.valueOf(collateralId));
        }
        Object applicantId = uploadFileDetails.get("applicantId");
        if (null != applicantId) {
            imageDetail.setApplicantId(String.valueOf(applicantId));
        }
        imageDetail.setUploadDate(gridFSDBFile.getUploadDate());

        imageDetail.setImageName(String.valueOf(uploadFileDetails
                .get("fileType") != null ? uploadFileDetails
                .get("fileType") : ""));

        imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                .get("fileExtension") != null ? uploadFileDetails
                .get("fileExtension") : ""));

        imageList.add(imageDetail);

        return imageList;
    }

    private List<KycImageDetails> populateKycImage(Map<String, List<ImagesDetails>> uniqueImageMap) {

        List<KycImageDetails> kycImageDetailsList = new ArrayList<KycImageDetails>();

        if (!uniqueImageMap.isEmpty()) {

            for (Entry<String, List<ImagesDetails>> custImages : uniqueImageMap
                    .entrySet()) {

                KycImageDetails kycImageDetails = new KycImageDetails();
                kycImageDetails.setApplicantId(custImages.getKey());
                kycImageDetails.setImageMap(custImages.getValue());
                kycImageDetailsList.add(kycImageDetails);

            }
        }

        return kycImageDetailsList;
    }


    /**
     * basically this method is used to get Delivery
     *
     * @param refId
     * @param deliveryOrderName
     * @return
     */
    @Override
    public String getImageIdByRefIdAndFileName(String refId, String deliveryOrderName) {

        Pattern pattern = Pattern.compile("^" + Pattern.quote(deliveryOrderName));

        GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria
                .where("metadata.uploadFileDetails.FileName").regex(pattern)
                .and("metadata.gonogoReferanceId").is(refId)
                .and("metadata.uploadFileDetails.fileType").is("application/pdf")
                .and("metadata.uploadFileDetails.deleted").is(false)));

        if (null != gridFSDBFile && null != gridFSDBFile.getId()) {
            return gridFSDBFile.getId().toString();
        }

        return "";

    }

    @Override
    public GridFSDBFile getDocument(GetFileRequest getFileRequest) {
        Query selectQuery = new Query();
        selectQuery.addCriteria(Criteria.where("metadata.uploadFileDetails.documentType")
                .is(getFileRequest.getDocumentType())
                .and("metadata.uploadFileDetails.documentSubType")
                .is(getFileRequest.getDocumentSubType())
                .and("metadata.uploadFileDetails.fileType")
                .is("application/pdf")
                .and("metadata.fileHeader.institutionId")
                .is(getFileRequest.getHeader().getInstitutionId())
                .and("metadata.uploadFileDetails.deleted").is(false));
        return this.gridFsTemplate.findOne(selectQuery);
    }

    @Override
    public GridFSDBFile getDocument(String docId, String institutionId, String refId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(docId)
                .and("metadata.fileHeader.institutionId").is(institutionId)
                .and("metadata.gonogoReferanceId").is(refId));
        return gridFsTemplate.findOne(query);
    }

    public Date getDeliveryOrderCreateDate(String refID, String deliveryOrderName) {

        Pattern pattern = Pattern.compile("^" + Pattern.quote(deliveryOrderName));

        GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria
                .where("metadata.uploadFileDetails.FileName").regex(pattern)
                .and("metadata.gonogoReferanceId").is(refID)
                .and("metadata.uploadFileDetails.fileType").is("application/pdf")
                .and("metadata.uploadFileDetails.deleted").is(false)));


        if (null != gridFSDBFile ) {
            DBObject dbObject = gridFSDBFile.getMetaData();

            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");

            Object dateObject = dbHeaderObject.get("dateTime");

            if (null != dateObject && dateObject instanceof Date) {
                return (Date) dateObject;
            }
        }
        // if not found it will set today's date as a delivery order date
        return new Date();

    }

    /**
     * Get document related to application
     * @param getFileRequest
     * @return
     */
    @Override
    public GridFSDBFile getApplicationDocument(GetFileRequest getFileRequest) {
        Query selectQuery = new Query();
        selectQuery.addCriteria(Criteria.where("metadata.uploadFileDetails.documentType")
                .is(getFileRequest.getDocumentType())
                .and("metadata.uploadFileDetails.documentSubType")
                .is(getFileRequest.getDocumentSubType())
                .and("metadata.gonogoReferanceId")
                .is(getFileRequest.getRefID())
                .and("metadata.uploadFileDetails.fileType")
                .is("application/csv")
                .and("metadata.fileHeader.institutionId")
                .is(getFileRequest.getHeader().getInstitutionId())
                .and("metadata.uploadFileDetails.deleted").is(false));
        return this.gridFsTemplate.findOne(selectQuery);
    }

    /**
     *
     * @param fileUploadRequest
     * @return
     */
    @Override
    public GridFSDBFile getLoanAppDocument(FileUploadRequest fileUploadRequest) {
        Query selectQuery = new Query();
        selectQuery.addCriteria(Criteria.where("metadata.uploadFileDetails.documentType")
                .is(fileUploadRequest.getUploadFileDetails().getDocumentType())
                .and("metadata.uploadFileDetails.documentSubType")
                .is(fileUploadRequest.getUploadFileDetails().getDocumentSubType())
                .and("metadata.gonogoReferanceId")
                .is(fileUploadRequest.getGonogoReferanceId())
                .and("metadata.uploadFileDetails.fileType")
                .is(fileUploadRequest.getUploadFileDetails().getFileType())
                .and("metadata.fileHeader.institutionId")
                .is(fileUploadRequest.getFileHeader().getInstitutionId())
                .and("metadata.uploadFileDetails.deleted").is(false));
        return this.gridFsTemplate.findOne(selectQuery);
    }

    @Override
    public List<ImagesDetails> getSignedFileDetails(CheckApplicationStatus checkApplicationStatus) {
        logger.debug("getSignedFileDetails repo started");

        Query query = new Query();
        query.addCriteria(Criteria.where("metadata.gonogoReferanceId").is(checkApplicationStatus.getGonogoRefId())
                .and("metadata.fileHeader.institutionId").is(checkApplicationStatus.getHeader().getInstitutionId())
                .and("metadata.uploadFileDetails.eSigned").is(true)
                .and("metadata.uploadFileDetails.deleted").is(false));

        logger.debug("getSignedFileDetails repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);
        List<ImagesDetails> imagesDetailList = null;
        if(!CollectionUtils.isEmpty(gridFSDBFileList)){
            imagesDetailList = new ArrayList<>();
            for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {
                DBObject dbObject = gridFSDBFile.getMetaData();
                DBObject uploadFileDetails = null;
                if (null != dbObject) {
                    uploadFileDetails = (DBObject) dbObject
                            .get("uploadFileDetails");
                }
                Object fileName = null;
                if (null != uploadFileDetails) {
                    fileName = uploadFileDetails.get("FileName");
                }
                ImagesDetails imagesDetails = new ImagesDetails();
                imagesDetails.setImageId(String.valueOf(gridFSDBFile.getId()));
                imagesDetails.setImageName(null!=fileName ? String.valueOf(fileName):"");
                imagesDetails.setImageExtension(gridFSDBFile.getContentType());
                imagesDetailList.add(imagesDetails);
            }
            return imagesDetailList;
        }else {
            return imagesDetailList;
        }

    }

    @Override
    public boolean deleteDocument(FileUploadRequest request) {
        logger.info("Inside delete documnet {}", request.getGonogoReferanceId());
        boolean deleted = false;
        try {
            // first fetch the file and metadata.
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(request.getImageID())
                    .and("metadata.fileHeader.institutionId").is(request.getFileHeader().getInstitutionId())
                    .and("metadata.gonogoReferanceId").is(request.getGonogoReferanceId()));
            GridFSDBFile docDetails = this.gridFsTemplate.findOne(query);

            /**
             * set the updated data to the metadata pojo.
             */
            if(docDetails != null) {
                DBObject dbObject = docDetails.getMetaData();
                if(dbObject != null) {
                    FileUploadRequest fileUploadRequest = new FileUploadRequest();
                    if (dbObject.get("gonogoReferanceId") != null)
                        fileUploadRequest.setGonogoReferanceId(dbObject.get("gonogoReferanceId").toString());

                    DBObject fileHeaderDb = (DBObject) dbObject.get("fileHeader");
                    DBObject uploadFileDetailsDb = (DBObject) dbObject.get("uploadFileDetails");
                    if (fileHeaderDb != null && uploadFileDetailsDb != null) {
                        setFileHeaderDetails(fileHeaderDb, fileUploadRequest);
                        setFileDetails(uploadFileDetailsDb, fileUploadRequest);
                        InputStream inputStream = docDetails.getInputStream();
                        gridFsTemplate.store(inputStream, docDetails.getFilename(),
                                        docDetails.getContentType(), fileUploadRequest).getId().toString();

                        // delete old image
                        Query deleteQuery = new Query();
                        deleteQuery.addCriteria(Criteria.where("_id").is(request.getImageID()));
                        gridFsTemplate.delete(deleteQuery);
                        deleted = true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updating delete flag with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updating delete flag of document images with probable cause [{%s}]", e.getMessage()));
        }
        return deleted;
    }

    private void setFileDetails(DBObject uploadFileDetailsDb, FileUploadRequest fileUploadRequest) {
        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        if(uploadFileDetailsDb.get("fileId") != null)
            uploadFileDetails.setFileId(uploadFileDetailsDb.get("fileId").toString());
        if(uploadFileDetailsDb.get("fileName") != null)
            uploadFileDetails.setFileName(uploadFileDetailsDb.get("fileName").toString());
        if(uploadFileDetailsDb.get("fileType") != null)
            uploadFileDetails.setFileType(uploadFileDetailsDb.get("fileType").toString());
        if(uploadFileDetailsDb.get("fileExtension") != null)
            uploadFileDetails.setFileExtension(uploadFileDetailsDb.get("fileExtension").toString());
        uploadFileDetails.setDeleted(true);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
    }

    private void setFileHeaderDetails(DBObject fileHeaderDb, FileUploadRequest fileUploadRequest) {
        Date uploadDate = (Date) fileHeaderDb.get("dateTime");
        FileHeader fileHeader = new FileHeader();
        if(fileHeaderDb.get("institutionId") != null)
            fileHeader.setInstitutionId(fileHeaderDb.get("institutionId").toString());
        if(fileHeaderDb.get("requestType") != null)
            fileHeader.setRequestType(fileHeaderDb.get("requestType").toString());
        if(fileHeaderDb.get("applicationSource") != null)
            fileHeader.setApplicationSource(fileHeaderDb.get("applicationSource").toString());
        fileHeader.setProduct(Product.LAP);
        if(fileHeaderDb.get("dsaId") != null)
            fileHeader.setDsaId(fileHeaderDb.get("dsaId").toString());
        if(fileHeaderDb.get("branchCode") != null)
            fileHeader.setBranchCode(fileHeaderDb.get("branchCode").toString());
        if(fileHeaderDb.get("loggedInUserId") != null)
            fileHeader.setLoggedInUserId(fileHeaderDb.get("loggedInUserId").toString());
        if(fileHeaderDb.get("loggedInUserRole") != null)
            fileHeader.setLoggedInUserRole(fileHeaderDb.get("loggedInUserRole").toString());
        if(fileHeaderDb.get("dealerId") != null)
            fileHeader.setDealerId(fileHeaderDb.get("dealerId").toString());
        if(uploadDate != null)
            fileHeader.setDateTime(uploadDate);
        if(fileHeaderDb.get("croId") != null)
            fileHeader.setCroId(fileHeaderDb.get("croId").toString());
        fileUploadRequest.setFileHeader(fileHeader);
    }

    @Override
    public List<KycImageDetails> getDocumentDetailsByFileName(String refID, String institutionId, String fileName) {
        logger.debug("getDocumentDetailsByFileName repo started");
        // fileName is ScreenType
        Query query = new Query();

        query.addCriteria(Criteria.where("metadata.gonogoReferanceId").in(refID)
                .and("metadata.fileHeader.institutionId").is(institutionId)
                .and("metadata.uploadFileDetails.deleted").is(false)
                .and("metadata.uploadFileDetails.fileName").regex(fileName,"i"));

        logger.debug("getDocumentDetailsByFileName repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

        Map<String, List<ImagesDetails>> uniqueImageMap = new HashMap<String, List<ImagesDetails>>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

            DBObject dbObject = gridFSDBFile.getMetaData();

            Date uploadDate = gridFSDBFile.getUploadDate();

            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");

            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");

            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));

            if (uniqueImageMap.containsKey(applicantId)) {

                ImagesDetails imageDetail = new ImagesDetails();

                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));

                imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));


                Object statusObject = uploadFileDetails.get("status");

                if (null != statusObject) {

                    imageDetail.setStatus(String.valueOf(statusObject));

                }
                Object fileNameObject = uploadFileDetails.get("fileName");

                if (null != fileNameObject) {
                    imageDetail.setImageType(String.valueOf(fileNameObject));
                }

                if(null != uploadDate) {
                    imageDetail.setUploadDate(uploadDate);
                }
                Object reasonObject = uploadFileDetails.get("reason");

                if (null != reasonObject) {

                    imageDetail.setReason(String.valueOf(reasonObject));

                }

                Object remarkObject = uploadFileDetails.get("remark");

                if (null != remarkObject) {

                    imageDetail.setRemark(String.valueOf(remarkObject));

                }

                imageDetail.setImageName(String.valueOf(uploadFileDetails
                        .get("fileType") != null ? uploadFileDetails
                        .get("fileType") : ""));

                imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                        .get("fileExtension") != null ? uploadFileDetails
                        .get("fileExtension") : ""));

                uniqueImageMap.get(applicantId).add(imageDetail);

            } else {

                List<ImagesDetails> imageList = initiateImageObject(gridFSDBFile, uploadFileDetails);

                uniqueImageMap.put(applicantId, imageList);

            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        logger.debug("getDocumentDetailsByFileName repo completed with response size {}", kycImageDetailsList.size());

        return kycImageDetailsList;
    }

    @Override
    public boolean deletedocument(String imageId){
        logger.debug("deleting image with id {} " ,imageId );
        boolean deleted = false;
        Query deleteQuery = new Query();
        deleteQuery.addCriteria(Criteria.where("_id").is(imageId));
        gridFsTemplate.delete(deleteQuery);
        deleted = true;
        return deleted;
    }

    @Override
    public String softDocument(FileUploadRequest request) {
        logger.info("Inside softDeleteDocument()");
        String res = null;
        try {
            // first fetch the file and metadata.
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(request.getImageID())
                    .and("metadata.fileHeader.institutionId").is(request.getFileHeader().getInstitutionId())
                    .and("metadata.gonogoReferanceId").is(request.getGonogoReferanceId()));
            GridFSDBFile docDetails = this.gridFsTemplate.findOne(query);

            /**
             * set the updated data to the metadata pojo.
             */
            if(docDetails != null) {
                DBObject dbObject = docDetails.getMetaData();
                if(dbObject != null) {
                    FileUploadRequest fileUploadRequest = new FileUploadRequest();
                    if (dbObject.get("gonogoReferanceId") != null)
                        fileUploadRequest.setGonogoReferanceId(dbObject.get("gonogoReferanceId").toString());

                    DBObject fileHeaderDb = (DBObject) dbObject.get("fileHeader");
                    DBObject uploadFileDetailsDb = (DBObject) dbObject.get("uploadFileDetails");
                    Boolean deleted = (Boolean) uploadFileDetailsDb.get("deleted");
                    if (fileHeaderDb != null && uploadFileDetailsDb != null && !deleted) {
                        setFileHeaderDetails(fileHeaderDb, fileUploadRequest);
                        setFileDetails(uploadFileDetailsDb, fileUploadRequest);
                        InputStream inputStream = docDetails.getInputStream();
                        res = gridFsTemplate.store(inputStream, docDetails.getFilename(),
                                docDetails.getContentType(), fileUploadRequest).getId().toString();

                        // delete old image
                        Query deleteQuery = new Query();
                        deleteQuery.addCriteria(Criteria.where("_id").is(request.getImageID()));
                        gridFsTemplate.delete(deleteQuery);
                    }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while updating delete flag with probable cause [{}]", e.getMessage());
            return null;//throw new SystemException(String.format("Error occurred while updating delete flag of document images with probable cause [{%s}]", e.getMessage()));
        }
        return res;
    }

    @Override
    public List<KycImageDetails> getKycImageDetails(Set<String> refIdSet,
                                                    String institutionId, String typeOfDocument, String var) {

        logger.debug("getKycImageDetails repo started");

        Query query = new Query();

        /**
         *  if typeOfDocument is "IMAGES" then it will create query according to fetch only images otherwise
         *  this service will return all types of document i.e. application/pdf ,jpg etc. it will used
         *  for push all documents to DMS purpose.
         */

        List<String> contentTypeExcludeList = new ArrayList<>();
        contentTypeExcludeList.add("application/pdf");
        contentTypeExcludeList.add("application/csv");

        if (StringUtils.equals(typeOfDocument, Status.IMAGES.name())) {
            query.addCriteria(Criteria.where("metadata.gonogoReferanceId").in(refIdSet)
                    .and("metadata.fileHeader.institutionId").is(institutionId)
                    .and("metadata.uploadFileDetails.fileType")
                    .nin(contentTypeExcludeList));
            query.addCriteria(Criteria.where("metadata.uploadFileDetails.deleted").is(false));
        } else {
            query.addCriteria(new Criteria().orOperator(Criteria.where("metadata.gonogoReferanceId")
                            .in(refIdSet).and("metadata.fileHeader.institutionId")
                            .is(institutionId).and("metadata.uploaded").exists(false),
                    Criteria.where("metadata.gonogoReferanceId")
                            .in(refIdSet).and("metadata.fileHeader.institutionId")
                            .is(institutionId).and("metadata.uploaded").is(true)));

            query.addCriteria(Criteria.where("metadata.uploadFileDetails.deleted").is(false));

        }

        logger.debug("getKycImageDetails repo query formed {}", query);

        List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

        Map<String, List<ImagesDetails>> uniqueImageMap = new HashMap<String, List<ImagesDetails>>();

        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

            DBObject dbObject = gridFSDBFile.getMetaData();

            Date uploadDate = gridFSDBFile.getUploadDate();

            DBObject dbHeaderObject = (DBObject) dbObject.get("fileHeader");

            DBObject uploadFileDetails = (DBObject) dbObject
                    .get("uploadFileDetails");

            String applicantId = String.valueOf(dbHeaderObject
                    .get("applicantId"));

            if (uniqueImageMap.containsKey(applicantId)) {

                ImagesDetails imageDetail = new ImagesDetails();

                imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));

                imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));


                Object statusObject = uploadFileDetails.get("status");

                if (null != statusObject) {

                    imageDetail.setStatus(String.valueOf(statusObject));

                }
                Object fileNameObject = uploadFileDetails.get("fileName");

                // TODO: Set below fileName in imageName once correction on UI side is Done.
                if (null != fileNameObject) {
                    imageDetail.setImageType(String.valueOf(fileNameObject));
                }

                if(null != uploadDate) {
                    imageDetail.setUploadDate(uploadDate);
                }
                Object reasonObject = uploadFileDetails.get("reason");

                if (null != reasonObject) {

                    imageDetail.setReason(String.valueOf(reasonObject));

                }

                Object collateralId = uploadFileDetails.get("collateralId");

                if (null != collateralId) {

                    imageDetail.setCollateralId(String.valueOf(collateralId));

                }

                Object applicantID = uploadFileDetails.get("applicantId");

                if (null != applicantID) {

                    imageDetail.setApplicantId(String.valueOf(applicantID));

                }

                Object fileId = uploadFileDetails.get("fileId");

                if (null != fileId) {

                    imageDetail.setFileId(String.valueOf(fileId));

                }

                Object latitude = uploadFileDetails.get("latitude");

                if (null != latitude) {

                    imageDetail.setLatitude(String.valueOf(latitude));

                }

                Object longitude = uploadFileDetails.get("longitude");

                if (null != longitude) {

                    imageDetail.setLongitude(String.valueOf(longitude));

                }

                Object remark = uploadFileDetails.get("remark");
                if (null != remark) {
                    imageDetail.setRemark(String.valueOf(remark));
                }

                Object sectionList = uploadFileDetails.get("sectionList");
                if (null != sectionList && CollectionUtils.isNotEmpty((Collection) sectionList)) {
                    imageDetail.setSectionList((List<String>) sectionList);
                }

                Object deleted = uploadFileDetails.get("deleted");
                if (null != deleted) {
                    imageDetail.setDeleted((Boolean) deleted);
                }

                Object imageSide = uploadFileDetails.get("imageSide");
                if (null != imageSide) {
                    imageDetail.setImageSide(String.valueOf(imageSide));
                }

                imageDetail.setImageName(String.valueOf(uploadFileDetails
                        .get("fileType") != null ? uploadFileDetails
                        .get("fileType") : ""));

                imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                        .get("fileExtension") != null ? uploadFileDetails
                        .get("fileExtension") : ""));

                Object ropedOut = uploadFileDetails.get("ropedOut");
                if (null != ropedOut) {
                    imageDetail.setRopedOut((Boolean) ropedOut);
                }

                Object rcuStatus = uploadFileDetails.get("rcuStatus");
                if (null != rcuStatus) {
                    imageDetail.setRcuStatus(String.valueOf(rcuStatus));
                }
                Object docValuation = uploadFileDetails.get("docValuation");
                if(null != docValuation){
                    imageDetail.setDocValuation((List<String>) docValuation);
                }
                Object documentNames = uploadFileDetails.get("documentNames");
                if(null != documentNames){
                    imageDetail.setDocumentNames((List<HashMap<String, String>>) documentNames);
                }
                Object showDropdown = uploadFileDetails.get("showDropdown");
                if(null != showDropdown){
                    imageDetail.setShowDropdown((Boolean) showDropdown);
                }
                putImageDetails(dbObject, uploadFileDetails, imageDetail);
                uniqueImageMap.get(applicantId).add(imageDetail);

            } else {

                List<ImagesDetails> imageList = initiateImageObject(gridFSDBFile, uploadFileDetails, "");

                uniqueImageMap.put(applicantId, imageList);

            }
        }

        List<KycImageDetails> kycImageDetailsList = populateKycImage(uniqueImageMap);

        logger.debug("getKycImageDetails repo completed with response size {}", kycImageDetailsList.size());

        return kycImageDetailsList;
    }

    private void putImageDetails(DBObject dbObject, DBObject uploadFileDetails, ImagesDetails imageDetail) {
        if(dbObject!= null && dbObject.containsField("uploaded") && dbObject.get("uploaded") != null
                && dbObject.get("uploaded") instanceof  Boolean
                && (Boolean) dbObject.get("uploaded") && uploadFileDetails != null){
            imageDetail.setImageName(String.valueOf(uploadFileDetails.get("fileType")));
            imageDetail.setImageType(String.valueOf(uploadFileDetails.get("fileName")));
        }
    }

    private List<ImagesDetails> initiateImageObject(GridFSDBFile gridFSDBFile, DBObject uploadFileDetails, String extra) {

        DBObject dbObject = gridFSDBFile.getMetaData();
        List<ImagesDetails> imageList = new ArrayList<ImagesDetails>();

        ImagesDetails imageDetail = new ImagesDetails();

        imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));
        imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
        imageDetail.setImageType(gridFSDBFile.getFilename());
        imageDetail.setStatus(String.valueOf(uploadFileDetails.get("status")));
        imageDetail.setReason(String.valueOf(uploadFileDetails.get("reason")));
        Object remarkObject = uploadFileDetails.get("remark");
        if (null != remarkObject) {
            imageDetail.setRemark(String.valueOf(remarkObject));
        }
        Object collateralId = uploadFileDetails.get("collateralId");
        if (null != collateralId) {
            imageDetail.setCollateralId(String.valueOf(collateralId));
        }
        Object applicantId = uploadFileDetails.get("applicantId");
        if (null != applicantId) {
            imageDetail.setApplicantId(String.valueOf(applicantId));
        }
        Object fileId = uploadFileDetails.get("fileId");
        if (null != fileId) {
            imageDetail.setFileId(String.valueOf(fileId));
        }
        Object latitude = uploadFileDetails.get("latitude");
        if (null != latitude) {
            imageDetail.setLatitude(String.valueOf(latitude));
        }
        Object longitude = uploadFileDetails.get("longitude");
        if (null != longitude) {
            imageDetail.setLongitude(String.valueOf(longitude));
        }
        Object docValuation = uploadFileDetails.get("docValuation");
        if(null != docValuation){
            imageDetail.setDocValuation((List<String>) docValuation);
        }
        imageDetail.setUploadDate(gridFSDBFile.getUploadDate());

        imageDetail.setImageName(String.valueOf(uploadFileDetails
                .get("fileType") != null ? uploadFileDetails
                .get("fileType") : ""));

        imageDetail.setImageExtension(String.valueOf(uploadFileDetails
                .get("fileExtension") != null ? uploadFileDetails
                .get("fileExtension") : ""));

        Object sectionList = uploadFileDetails.get("sectionList");
        if (null != sectionList && CollectionUtils.isNotEmpty((Collection) sectionList)) {
            imageDetail.setSectionList((List<String>) sectionList);
        }

        Object deleted = uploadFileDetails.get("deleted");
        if (null != deleted) {
            imageDetail.setDeleted((Boolean) deleted);
        }

        Object imageSide = uploadFileDetails.get("imageSide");
        if (null != imageSide) {
            imageDetail.setImageSide(String.valueOf(imageSide));
        }

        Object ropedOut = uploadFileDetails.get("ropedOut");
        if (null != ropedOut) {
            imageDetail.setRopedOut((Boolean) ropedOut);
        }

        Object rcuStatus = uploadFileDetails.get("rcuStatus");
        if (null != rcuStatus) {
            imageDetail.setRcuStatus(String.valueOf(rcuStatus));
        }

        Object documentNames = uploadFileDetails.get("documentNames");
        if(null != documentNames){
            imageDetail.setDocumentNames((List<HashMap<String, String>>) documentNames);
        }

        Object showDropdown = uploadFileDetails.get("showDropdown");
        if(null != showDropdown){
            imageDetail.setShowDropdown((Boolean) showDropdown);
        }
        putImageDetails(dbObject, uploadFileDetails, imageDetail);
        imageList.add(imageDetail);

        return imageList;
    }
}
