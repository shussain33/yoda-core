package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.softcell.config.reporting.GoNoGoSystemFields;
import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.dao.mongodb.config.MongoConfig.COLLECTIONS;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.dao.mongodb.utils.RepoUtils;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLog;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationResponse;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.SerialNumberInfoLogRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorLog;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.builder.ReportingQueryBuilder;
import com.softcell.reporting.domains.CustomReport;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.ReportDetail;
import com.softcell.reporting.domains.SalesReportResponse;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author kishorp
 */
@Repository
public class ReportingMongoDBRepository implements ReportingRepository {

    private static final Logger logger = LoggerFactory.getLogger(ReportingMongoDBRepository.class);

    public static SortedSet<String> keySet = new TreeSet<>();

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private RepoUtils repoUtils;

    /**
     * @param mongoTemplate
     */
    @Inject
    public ReportingMongoDBRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public ReportingMongoDBRepository() {

    }

    @Override
    @Deprecated
    public boolean saveReportingConf(ReportingModuleConfiguration reportingModuleConfiguration) {
        return true;
    }

    @Override
    public List<ReportingModuleConfiguration> getRepotingConfig() {

        List<ReportingModuleConfiguration> reportingModuleConfigurations = new ArrayList<>();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true));

            reportingModuleConfigurations = mongoTemplate.find(query, ReportingModuleConfiguration.class);

        } catch (Exception ex) {
            logger.error("Error occurred while fetching ReportingModuleConfiguration with probable cause [{}]", ex.getMessage());
            throw new SystemException(String.format("Error occurred while fetching ReportingModuleConfiguration with probable cause [{%s}]", ex.getMessage()));
        }

        return reportingModuleConfigurations;

    }

    @Override
    @Deprecated
    public boolean saveReportingConfigFromCSVFile() {

        mongoTemplate.remove(new Query(), ReportingModuleConfiguration.class);

        // CSV file header
        final String[] FILE_HEADER_MAPPING = {"institutionID", "reportType",
                "productType", "startDate", "endDate", "interval", "format",
                "localCopy", "active", "reportCycle"};

        // Create the CSVFormat object
        CSVFormat format = CSVFormat.RFC4180.withHeader(FILE_HEADER_MAPPING)
                .withSkipHeaderRecord();

        // initialize the CSVParser object
        CSVParser parser = null;

        try (FileReader fileReader = new FileReader("/home/yogeshb/Documents/masters/ReportingModuleConfiguration.csv")) {

            parser = new CSVParser(fileReader, format);

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
            return false;
        } catch (IOException e1) {
            e1.printStackTrace();
            return false;
        }

        for (CSVRecord record : parser) {
            ReportingModuleConfiguration reportingModuleConfiguration = new ReportingModuleConfiguration(
                    StringUtils.trim(record.get(0)),// InstitutionID
                    StringUtils.trim(record.get(1)),// reportType
                    StringUtils.trim(record.get(2)),// productType
                    StringUtils.trim(record.get(3)),// startDate
                    StringUtils.trim(record.get(4)),// endDate
                    Integer.parseInt(record.get(5)),// interval
                    StringUtils.trim(record.get(6)),// format
                    Boolean.parseBoolean(StringUtils.trim(record.get(7))),// localCopy
                    Boolean.parseBoolean(StringUtils.trim(record.get(8))),// active
                    new Date(),// insertDate
                    StringUtils.trim(record.get(9)));// reportCycle
            mongoTemplate.insert(reportingModuleConfiguration);
        }

        try {
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    @Override
    public CustomReport getCustomReport(FlatReportConfiguration flatReportConfiguration, Query query) {

        Assert.notNull(query, "query for custom report must not be blank or null !!");

        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = null;

        try {

            goNoGoCustomerApplications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred while fetching custom report data with probable cause [{}] ", e.getMessage());
        }


        CustomReport customReport = new CustomReport();

        List<Object> resultSet = new ArrayList<>();

        if (!CollectionUtils.isEmpty(goNoGoCustomerApplications)) {

            List<Object> collect = goNoGoCustomerApplications.parallelStream().map(customerApplication -> {

                GoNoGoApplicationToJsonParser applicationToJsonParser = new GoNoGoApplicationToJsonParser(customerApplication);

                return applicationToJsonParser.build().enrichJson(flatReportConfiguration);

            }).collect(Collectors.toList());

            resultSet.add(collect);

        }

        customReport.setReportData(resultSet);

        return customReport;
    }

    @Override
    public GoNoGoSystemFields getGoNoGoDimensions(Query query) {

        Assert.notNull(query, " query to fetch gonogo dimensions must not be null or blank !!");

        GoNoGoSystemFields goNoGoSystemFields = null;

        try {

            goNoGoSystemFields = mongoTemplate.findOne(query, GoNoGoSystemFields.class);


        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching gonogo dimensions with probable cause [{}] ", e.getMessage());
        }

        return goNoGoSystemFields;
    }

    @Override
    public boolean saveGoNoGoSystemFields(GoNoGoSystemFields goNoGoSystemFields) {

        Assert.notNull(goNoGoSystemFields, "GoNoGoSystemFields must not be null ");

        try {

            mongoTemplate.save(goNoGoSystemFields);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while saving gonogosystem fields with probable cause [{}] ", e.getMessage());
        }

        return true;
    }

    @Override
    public FlatReportConfiguration getFlatReportConfiguration(Query query) {

        Assert.notNull(query, "Query object for flatreport Configuration should not be null or blank");

        ReportingConfigurations reportingConfigurations = mongoTemplate
                .findOne(query, ReportingConfigurations.class);

        Assert.notNull(reportingConfigurations,
                "Reporting Configuration object must not be null");
        Assert.notNull(reportingConfigurations.getFlatReportConfiguration(),
                "FlatReportConfiguration object must not be null !!");

        return reportingConfigurations.getFlatReportConfiguration();
    }

    @Override
    public ReportingConfigurations reportConfiguration(Query query) {
        Assert.notNull(query, " query for report configuration must not be null or blank ");

        ReportingConfigurations reportingConfigurations = null;

        try {

            reportingConfigurations = mongoTemplate.findOne(query, ReportingConfigurations.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error(" error occurred while fetching report configuration with probable cause [{}] ", e.getMessage());
        }

        return reportingConfigurations;

    }

    @Override
    public boolean saveCustomReportConfiguration(ReportingConfigurations reportingConfigurations) {

        Assert.notNull(reportingConfigurations, "Reporting Configuration must not be null !!");

        Update update = new Update();

        update.set("inActive", true);

        Query query = ReportingQueryBuilder.getUpdateQueryReportConfig(reportingConfigurations);

        try {

            mongoTemplate.updateMulti(query, update, ReportingConfigurations.class);

            mongoTemplate.save(reportingConfigurations);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while saving custom report configuration with probable cause [{}]", e.getMessage());
        }


        return true;
    }

    @Override
    public List<ReportDetail> searchReportByName(Query query) {

        List<ReportingConfigurations> reportingConfigurations = new ArrayList<>();

        try {

            reportingConfigurations = mongoTemplate.find(query, ReportingConfigurations.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while  searching report by name with probable cause [{}] ", e.getMessage());
        }


        return reportingConfigurations.parallelStream().map(config -> {

            ReportDetail reportDetail = new ReportDetail();

            reportDetail.setReportId(config.getReportId());

            reportDetail.setReportName(config.getReportName());

            return reportDetail;

        }).collect(Collectors.toList());

    }

    @Override
    public Stream<GoNoGoCustomerApplication> getCustomerApplication(Query query) {
        Assert.notNull(query, "Query must  not be blank or null ");

        Stream<GoNoGoCustomerApplication> goNoGoCustomerApplication = null;

        try {

            goNoGoCustomerApplication = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, GoNoGoCustomerApplication.class, "goNoGoCustomerApplication"));

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching customer application list with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplication;
    }


    @Override
    public Stream<GoNoGoCustomerApplication> getCustomerApplicationOld(Query query) {
        Assert.notNull(query, "Query must  not be blank or null ");

        Stream<GoNoGoCustomerApplication> goNoGoCustomerApplication = null;

        try {

            goNoGoCustomerApplication = StreamUtils.createStreamFromIterator(mongoTemplate.stream(
                    query, GoNoGoCustomerApplication.class, "goNoGoCustomerApplication"));

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching customer application list with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplication;
    }

    @Override
    public List<ReportingConfigurations> reportSystemConfiguration(Query query) {
        return mongoTemplate.find(query, ReportingConfigurations.class);

    }

    /**
     * method to collect result of sales aggregation
     */
    @Override
    public Stream salesIncentiveReport(
            TypedAggregation aggregation) throws Exception {

        Assert.notNull(aggregation, " Sales Report Aggregation must not be null or blank !!");

        Stream stream = null;

        try {

            logger.debug("aggregation generated for sales incentive report is [{}]", aggregation.toString());


            stream = repoUtils.convertAggregation2Stream(aggregation, SalesReportResponse.class, COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString());

            System.out.println(stream.count());


        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error while generating  aggregation result for sales incentive report with probable cause [{}] ", e.getMessage());
        }

        return stream;
    }

    @Override
    public Stream<BasicDBObject> firstLastLoginReport(
            TypedAggregation aggregation) throws Exception {

        /*AggregationResults<BasicDBObject> aggregate = mongoTemplate.aggregate(aggregation, COLLECTIONS.APPLICATION_DOCUMENT.toString(),BasicDBObject.class);

        return aggregate.getMappedResults().parallelStream();*/

        return repoUtils.convertAggregation2Stream(aggregation, ApplicationRequest.class, COLLECTIONS.APPLICATION_DOCUMENT.toString());

        /*return mongoTemplate.aggregate(aggregation,
                COLLECTIONS.APPLICATION_DOCUMENT.toString(),
                BasicDBObject.class);*/

    }

    @Override
    public AggregationResults<BasicDBObject> otpReport(Aggregation aggregation)
            throws Exception {

        Assert.notNull(aggregation, "Aggregation pipeline for otp report must not be null or blank !!");

        AggregationResults<BasicDBObject> aggregate = null;

        try {

            aggregate = mongoTemplate.aggregate(aggregation,
                    COLLECTIONS.LOGGER_DOC.toString(), BasicDBObject.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error while creating aggregation for otp report with probable cause [{}] ", e.getMessage());
        }

        return aggregate;

    }

    @Override
    public Stream<GoNoGoCustomerApplication> zippedSalesReport(Query query)
            throws Exception {

        Assert.notNull(query, " Query for zipped sales report must not be null !! ");

        Stream<GoNoGoCustomerApplication> goNoGoCustomerApplication = null;

        try {


            goNoGoCustomerApplication = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, GoNoGoCustomerApplication.class, "goNoGoCustomerApplication"));


        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching records for zipped sales report with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplication;
    }

    @Override
    public PostIpaRequest getPostIpaRequest(Query query) throws Exception {

        Assert.notNull(query, "post IPA request query  must not be null or blank ");

        PostIpaRequest postIpaRequest = null;

        try {

            postIpaRequest = mongoTemplate.findOne(query, PostIpaRequest.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error(" error occurred while fetching post ipa request with probable cause [{}] ", e.getMessage());
        }


        return postIpaRequest;
    }

    @Override
    public List<GoNoGoCustomerApplication> zippedCreditReport(Query query)
            throws Exception {

        Assert.notNull(query, " zipped credit report query must not be null or blank ");

        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = new ArrayList<>();

        try {

            goNoGoCustomerApplications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred while fetching zipped credit report with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplications;

    }


    @Override
    public Set<String> getAssetModelMasterManufacturer(String institutionId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        query.fields().include("manufacturerDesc");

        List<AssetModelMaster> assetModelMasterList = new ArrayList<>();

        try {

            assetModelMasterList = mongoTemplate.find(query, AssetModelMaster.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching asset model master manufacturer with probable cause [{}] ", e.getMessage());
        }


        return assetModelMasterList.stream().map(AssetModelMaster::getManufacturerDesc).collect(Collectors.toSet());
    }

    @Override
    public Map<String, Set<String>> getCategoryAgainstManufacturer(Set<String> manufacturerSet) {

        Query query = new Query();

        query.addCriteria(Criteria.where("manufacturerDesc").in(manufacturerSet).and("active").is(true));

        query.fields().include("manufacturerDesc");

        query.fields().include("catgDesc");

        List<AssetModelMaster> assetModelMasterList = new ArrayList<>();

        try {

            assetModelMasterList = mongoTemplate.find(query, AssetModelMaster.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error("error occurred while fetching category against manufacturer with probable cause [{}] ", e.getMessage());
        }

        Map<String, Set<String>> manfacturerWiseCategory = new HashMap<String, Set<String>>();

        for (AssetModelMaster assetModelMaster : assetModelMasterList) {

            if (manfacturerWiseCategory.containsKey(assetModelMaster.getManufacturerDesc())) {

                manfacturerWiseCategory.get(assetModelMaster.getManufacturerDesc()).add(assetModelMaster.getCatgDesc());

            } else {

                Set<String> assetCategories = new HashSet<String>();

                assetCategories.add(assetModelMaster.getCatgDesc());

                manfacturerWiseCategory.put(assetModelMaster.getManufacturerDesc(), assetCategories);
            }
        }

        return manfacturerWiseCategory;

    }

    @Override
    public Set<String> getMake(String ctg, String manufacturer) {

        Query query = new Query();

        query.addCriteria(Criteria.where("catgDesc").is(ctg)
                .and("manufacturerDesc").is(manufacturer).and("active").is(true));

        query.fields().include("make");

        List<AssetModelMaster> assetModelMasterList = new ArrayList<>();

        try {

            assetModelMasterList = mongoTemplate.find(query, AssetModelMaster.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error(" error occurred while getting make from mongo data store with probable cause [{}] ", e.getMessage());
        }


        return assetModelMasterList.stream().map(AssetModelMaster::getMake).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getModels(String ctg, String make, String manufacturer) {

        Query query = new Query();

        query.addCriteria(Criteria.where("catgDesc").is(ctg)
                .and("manufacturerDesc").is(manufacturer).and("make").is(make).and("active").is(true));

        query.fields().include("modelNo");

        List<AssetModelMaster> assetModelMasterList = new ArrayList<>();

        try {

            assetModelMasterList = mongoTemplate.find(query, AssetModelMaster.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error("error occurred while fetching assetModelMaster from mongo data store with probable cause [{}] ", e.getMessage());
        }

        return assetModelMasterList.stream().map(AssetModelMaster::getModelNo).collect(Collectors.toSet());
    }

    @Override
    public List<SerialNumberInfo> getSerialNumberLog() {

        List<SerialNumberInfo> serialNumberInfos;

        try {

            serialNumberInfos = mongoTemplate.findAll(SerialNumberInfo.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error("Error occurred while fetching SerialNumberInfo with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching SerialNumberInfo with probable cause [{%s}]", e.getMessage()));
        }

        return serialNumberInfos;
    }

    @Override
    public List<SerialNumberInfo> getSerialNumberByIdOrVendorLog(Query query) {

        Assert.notNull(query, "Query must not be null or blank ");

        List<SerialNumberInfo> serialNumberInfos = new ArrayList<>();

        try {

            serialNumberInfos = mongoTemplate.find(query, SerialNumberInfo.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred while getting serial number by id or vendor with probable cause [{}] ", e.getMessage());
        }

        return serialNumberInfos;

    }

    @Override
    public SerialNumberInfo fetchSerialNumberByRefIdNStatus(Query query) {

        Assert.notNull(query, "Query for fetching serial number must not be null or blank");

        SerialNumberInfo serialNumberInfo = null;

        try {

            serialNumberInfo = mongoTemplate.findOne(query, SerialNumberInfo.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching serial number based on refId and status with probable cause [{}] ", e.getMessage());
        }

        return serialNumberInfo;

    }

    @Override
    public DealerEmailMaster fetchDealerEmailMasterBasedOnDealerId(Query query) {

        Assert.notNull(query, "Query for fetchDealerEmailMasterBasedOnDealerId  must not be null or blank ");

        DealerEmailMaster dealerEmailMaster = null;

        try {

            dealerEmailMaster = mongoTemplate.findOne(query, DealerEmailMaster.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error(" error occurred while fetching email master based on dealer id with probable cause [{}] ", e.getMessage());
        }

        return dealerEmailMaster;
    }

    @Override
    public List<SerialSaleConfirmationRequest> getSerialNumberRequestLog() {

        List<SerialSaleConfirmationRequest> serialSaleConfirmationRequests;

        try {

            serialSaleConfirmationRequests = mongoTemplate.findAll(SerialSaleConfirmationRequest.class);

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Error occurred while fetching SerialSaleConfirmationRequest with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching SerialSaleConfirmationRequest with probable cause [{%s}]",
                    e.getMessage()));
        }

        return serialSaleConfirmationRequests;
    }

    @Override
    public List<SerialSaleConfirmationRequest> getSerialNumberRequestLog(
            String refID, String vendor) {

        List<SerialSaleConfirmationRequest> serialSaleConfirmationRequests;

        try {
            Query query = new Query();

            if (null == vendor && null != refID) {

                query.addCriteria(Criteria.where("referenceID").is(refID));

            } else if (null == refID && null != vendor) {

                query.addCriteria(Criteria.where("vendor").is(vendor));

            } else if (null != refID && null != vendor) {

                query.addCriteria(Criteria.where("referenceID").is(refID)
                        .and("vendor").is(vendor));

            }

            serialSaleConfirmationRequests = mongoTemplate.find(query, SerialSaleConfirmationRequest.class);

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Error occurred while fetching SerialSaleConfirmationRequest with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching SerialSaleConfirmationRequest with probable cause [{%s}]", e.getMessage()));
        }

        return serialSaleConfirmationRequests;
    }

    @Override
    public List<EmailTemplateDetails> getMailLogReport(String refID) {

        List<EmailTemplateDetails> emailTemplateDetails;

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refID));

            emailTemplateDetails = mongoTemplate.find(query, EmailTemplateDetails.class);

        } catch (Exception e) {
            e.printStackTrace();

            logger.error("Error occurred while fetching EmailTemplateDetails for refId  {} with probable cause [{}]", refID, e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching EmailTemplateDetails for refId  {%s} with probable cause [{%s}]", refID, e.getMessage()));
        }

        return emailTemplateDetails;
    }

    @Override
    public List<EmailTemplateDetails> getMailLogReport() {

        List<EmailTemplateDetails> emailTemplateDetails;

        try {

            emailTemplateDetails = mongoTemplate.findAll(EmailTemplateDetails.class);

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Error occurred while fetching EmailTemplateDetails  with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching EmailTemplateDetails with probable cause [{%s}]", e.getMessage()));
        }

        return emailTemplateDetails;
    }

    @Override
    public List<LosUtrDetailsMaster> getLosUtrMaster(String institutionId) {

        List<LosUtrDetailsMaster> losUtrDetailsMasters;

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionID").is(institutionId));

            losUtrDetailsMasters = mongoTemplate.find(query, LosUtrDetailsMaster.class);

        } catch (DataAccessException e) {

            e.printStackTrace();

            logger.error("Exception occurred during get LosUtrMaster information against institutionId {} and exception is {}", institutionId, e.getMessage());

            throw new SystemException(String.format("Exception occurred during get LosUtrMaster information against institutionId {%s} and exception is [{%s}]", institutionId, e.getMessage()));
        }

        return losUtrDetailsMasters;
    }

    @Override
    public List<RollbackFeatureLog> getSerialNumberRollBackLogByVendor(SerialNumberInfoLogRequest serialNumberInfoLogRequest) {

        logger.debug("Get serial number rollback log started successfully ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("vendor").is(serialNumberInfoLogRequest.getVendor())
                    .and("dateTime").lte(serialNumberInfoLogRequest.getEndDate())
                    .gte(serialNumberInfoLogRequest.getDateStartFrom()));

            return mongoTemplate.find(query, RollbackFeatureLog.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Problem occured during get serialNumber rollBack log with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Problem occured during get serialNumber rollBack log with probable cause {%s}", e.getMessage()));
        }
    }

    @Override
    public PostIpaRequest getPostIpaByRefId(String refId) {

        Assert.notNull(refId, "reference id must not be null or blank !!");

        PostIpaRequest postIpaRequest = null;

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId));

            postIpaRequest = mongoTemplate.findOne(query, PostIpaRequest.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching postIpaRequest for refId [{}] with probable cause ", refId, e.getMessage());
        }

        return postIpaRequest;
    }

    @Override
    public CloseableIterator<GoNoGoCustomerApplication> getGoNoGoCustomerApplicationForTVR(ReportingModuleConfiguration configuration) {

        CloseableIterator<GoNoGoCustomerApplication> goNoGoCustomerApplicationStream = null;

        try {

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();

            logger.debug(" hey buddy on your request we started collecting data from your specified collection gonogo-customer application , have patience we are trying hard to make you feel awesome with us ");


            Date[] fromToDate = GngDateUtil.getFromToDate(configuration);

            Query gonogoCustomerQuery = QueryBuilder.buildGoNoGoApplicationQuery(
                    configuration.getInstitutionID(), fromToDate[0], fromToDate[1],
                    configuration.getProductType());


            goNoGoCustomerApplicationStream = mongoTemplate.stream(gonogoCustomerQuery, GoNoGoCustomerApplication.class, "goNoGoCustomerApplication");

            stopWatch.stop();

            logger.debug(" total time taken to get gonogo customer application is [{}] sec", stopWatch.getTotalTimeSeconds());

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred while fetching gonogo customer application data with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplicationStream;
    }

    @Override
    public List<GoNoGoCustomerApplication> getGoNoGoCustomerApplicationForCredit(Query query) {

        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = Collections.emptyList();

        try {

            goNoGoCustomerApplications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);


        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching gonogo customer application records for credit report with probable cause [{}] ", e.getMessage());
        }

        return goNoGoCustomerApplications;
    }

    @Override
    public SerialNumberInfo getSerialNoByIdAndStatus(String id) {

        SerialNumberInfo serialNumberInfo = null;

        try {

            Query serialNumberInfoQuery = new Query();

            serialNumberInfoQuery.addCriteria(Criteria.where("refID").is(id).and("status")
                    .is("VALID"));

            serialNumberInfo = mongoTemplate.findOne(serialNumberInfoQuery, SerialNumberInfo.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching serial no by id and status  with probable cuase [{}] ", e.getMessage());
        }

        return serialNumberInfo;
    }

    @Override
    public SerialNumberInfo getSerialNoInfo(Query query) {

        SerialNumberInfo serialNumberInfo = null;

        try {

            serialNumberInfo = mongoTemplate.findOne(query, SerialNumberInfo.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching serial no by id and status  with probable cuase [{}] ", e.getMessage());
        }

        return serialNumberInfo;

    }

    @Override
    public FileHandoverDetails getFileHandOverDetails(Query query) {

        FileHandoverDetails fileHandoverDetails = null;

        try {

            fileHandoverDetails = mongoTemplate.findOne(query, FileHandoverDetails.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching records for file handover details with probable cause [{}] ", e.getMessage());
        }

        return fileHandoverDetails;

    }

    @Override
    public DealerEmailMaster getDealerEmailMasterById(String dealerId) {

        DealerEmailMaster dealerEmailMaster = null;

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("dealerID").is(dealerId).and("active")
                    .is(true));

            dealerEmailMaster = mongoTemplate.findOne(query, DealerEmailMaster.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" error occurred while fetching dealers having dealer Id [{}] with probable cause [{}]", dealerId, e.getMessage());
        }

        return dealerEmailMaster;

    }

    @Override
    public List<ApplicationRequest> getDEStageRecords(Query query) {

        try {
            return mongoTemplate.find(query, ApplicationRequest.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred at the time of fetching DE stage records with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("error occurred at the time of fetching DE stage records with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public Stream<AadharLog> findAadhaarLogs(AadhaarLogRequest aadhaarLogRequest) {

        try {

            Query query = new Query();

            query.addCriteria(
                    Criteria.where("AadharClientRequest.header.institutionId").is(aadhaarLogRequest.getHeader().getInstitutionId())
                            .and("AadharClientRequest.header.dateTime").lte(aadhaarLogRequest.getEndDate()).gte(aadhaarLogRequest.getDateStartFrom())

            );


            logger.debug(" query used to get aadhar log [{}] ", query);

            Supplier<Stream<AadharLog>> streamSupplier = () -> StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, AadharLog.class));

            return streamSupplier.get();
        } catch (DataAccessException e) {
            logger.error("error occurred at the time of fetching aadhaar logs with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("error occurred at the time of fetching aadhaar logs with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public FileHandoverDetails getFilehandOverDetails(Query query) {
        try {
            return mongoTemplate.findOne(query, FileHandoverDetails.class);
        } catch (DataAccessException e) {
            logger.error("error occurred at the time of fetching FileHandoverDetails with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("error occurred at the time of fetching FileHandoverDetails with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public DealerBranchMaster getBranchMaster(String dealerId ,String institutionId) {
        logger.debug("get branchMaster against dealerId {} and institutionId {} repo started.",dealerId ,institutionId);
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("dealerId").is(dealerId).and("active")
                    .is(true).and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, DealerBranchMaster.class);

        } catch (DataAccessException e) {

            logger.error("error occurred at the time of fetching DealerBranchMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("error occurred at the time of fetching DealerBranchMaster with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public List<MbPushResponse> getMbPushResponseLogs(DmzRequest dmzRequest) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(dmzRequest.getGonogoRefId()));

            CloseableIterator<MbPushResponse> stream = mongoTemplate.stream(query, MbPushResponse.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (DataAccessException e) {

            logger.error("error occurred at the time of fetching MbPushResponse with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("error occurred at the time of fetching MbPushResponse with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public List<TVRLogs> getTvrLogs(String refId) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId));

            CloseableIterator<TVRLogs> stream = mongoTemplate.stream(query, TVRLogs.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (DataAccessException e) {

            logger.error("error occurred at the time of fetching TvrLogs with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("error occurred at the time of fetching TvrLogs with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public List<AccountNumberInfo> getAccountNumberLog(Query query) {

            Assert.notNull(query, "Query must not be null or blank ");

            try {

               return mongoTemplate.find(query, AccountNumberInfo.class);

            } catch (DataAccessException e) {
                logger.error("error occurred while getting account number log with probable cause [{}] ", e.getMessage());

                throw new SystemException(String.format("error occurred while getting account number log with probable cause [%s]", e.getMessage()));
            }
    }

    @Override
    public List<SerialNumberApplicableVendorLog> getSerialNumberApplicableVendorLog(Query query) {
        Assert.notNull(query, "Query must not be null or blank ");

        try {

            return mongoTemplate.find(query, SerialNumberApplicableVendorLog.class);

        } catch (DataAccessException e) {
            logger.error("error occurred while getting serial number applicable vendor log with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while getting serial number applicable vendor log with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public List getSBFCLMSLogByRefId(Query query) {

        try {

            return mongoTemplate.find(query, SBFCLMSIntegrationResponse.class);

        } catch (DataAccessException e) {
            logger.error("error occurred while getting SBFC-LMS log by refid with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while getting SBFC-LMS log by refid with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public List<SBFCLMSIntegrationLog> getSBFCLMSLog(Query query) {

        Assert.notNull(query, "Query must not be null or blank ");

        try {

            return mongoTemplate.find(query, SBFCLMSIntegrationLog.class);

        } catch (DataAccessException e) {
            logger.error("error occurred while getting SBFC-LMS log with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while getting SBFC-LMS log with probable cause [%s]", e.getMessage()));
        }
    }
}

