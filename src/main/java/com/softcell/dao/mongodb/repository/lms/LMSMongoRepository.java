package com.softcell.dao.mongodb.repository.lms;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLog;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationResponse;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kumar on 23/7/18.
 */
@Component
public class LMSMongoRepository {

    private static final Logger logger = LoggerFactory.getLogger(LMSMongoRepository.class);

    @Autowired
    MongoTemplate mongoTemplate;

    public GoNoGoCustomerApplication getClientInformation(String refId, String instId) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(instId));

            return mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);

        } catch (DataAccessException e) {
            logger.error("Exception occured at the time of fetching gonogoCustomer details with probable cause {}", ExceptionUtils.getStackTrace(e));
            throw new SystemException(String.format("Exception occured at the time of fetching gonogoCustomer details with probable cause [{}] ", ExceptionUtils.getStackTrace(e)));
        }
    }

    public void saveMasterData(String requestJson, String data) {

        logger.debug("Saving SBFC-LMS Master Data request with values {} ", requestJson);

        DBObject dbObject = (DBObject) JSON.parse(requestJson);

        if (dbObject != null) {

            dbObject.put("dateTime", new Date());
            DBCollection collection = mongoTemplate.getCollection(data);
            collection.insert(dbObject);
        }
    }

    public List getMasterDataByInstitutionId(String institutionId) {
        List masterDataList = new ArrayList();
        try {

            BasicDBObject query = new BasicDBObject();
            query.put("institutionId", institutionId);
            DBCursor list = mongoTemplate.getDb().getCollection("SBFCLMSMasterData").find(query);

            while (list.hasNext()) {
                masterDataList.add(list.next());
            }
        } catch (Exception e) {

            logger.error("Error occurred while fetching MasterData with probable cause [{}]", ExceptionUtils.getStackTrace(e));

            throw new SystemException(String.format("Error occurred while fetching MasterData with probable cause [{%s}]",
                    ExceptionUtils.getStackTrace(e)));
        }
        return masterDataList;
    }

    public List getMasterData() {

        List masterDataList = new ArrayList();

        try {

            DBCursor allLog = mongoTemplate.getDb().getCollection("SBFCLMSMasterData").find();

            while (allLog.hasNext()){
                masterDataList.add(allLog.next());
            }

        } catch (Exception e) {


            logger.error("Error occurred while fetching SBFC-LMS MasterData with probable cause [{}]", ExceptionUtils.getStackTrace(e));

            throw new SystemException(String.format("Error occurred while fetching SBFC-LMS MasterData with probable cause [{%s}]",
                    ExceptionUtils.getStackTrace(e)));
        }

        return masterDataList;
    }

    public void deleteMasterData() {

        mongoTemplate.getDb().getCollection("SBFCLMSMasterData").drop();
    }

    public LoanCharges fetchLoanChargesDetailsByRefId(String refId, String institutionId) {
        logger.info("Inside fetchLoanChargesDetails()");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, LoanCharges.class);
        } catch (DataAccessException e) {

            logger.error("Error occurred while fetching LoanChargesDetails with probable cause [{%s}] ", ExceptionUtils.getStackTrace(e));

            throw new SystemException(String.format(" error occurred  while fetching LoanChargesDetails with probable cause [{%s}]", ExceptionUtils.getStackTrace(e)));
        }
    }

    public void saveLog(SBFCLMSIntegrationResponse sbfclmsIntegrationResponse) {

        mongoTemplate.insert(sbfclmsIntegrationResponse);
    }

    public void saveLog(SBFCLMSIntegrationLog sbfclmsIntegrationLog) {

        mongoTemplate.insert(sbfclmsIntegrationLog);
    }

    public DisbursementMemo fetchDMDetails(String refID, String institutionId) {

        logger.info("Fetching DisbursementMemo details against refId {} ", refID);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("institutionId").is(institutionId));
            logger.debug("Query formed : {}" , query);
            return mongoTemplate.findOne(query, DisbursementMemo.class);
        } catch (DataAccessException e) {
            logger.error("Error occurred while fetching DisbursementMemo with probable cause {} ", ExceptionUtils.getStackTrace(e));
            throw new SystemException(String.format(" error occurred  while fetching DisbursementMemo with probable cause %s", ExceptionUtils.getStackTrace(e)));
        }
    }

    public CamDetails fetchCamDetailsByRefId(String refID, String institutionId) {

        logger.info("Fetching CamDetails against refId {} ", refID);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID)
                    .and("institutionId").is(institutionId));
            logger.debug("Query formed : {}", query);
            return mongoTemplate.findOne(query, CamDetails.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while fetching CamDetails with probable cause {} ", ExceptionUtils.getStackTrace(e));
            throw new SystemException(String.format(" error occurred  while fetching CamDetails with probable cause %s", ExceptionUtils.getStackTrace(e)));
        }
    }

        public Repayment fetchRepaymentDetailsByRefId(String refID, String institutionId) {

            logger.info("Fetching Repayment details against refId {} ", refID);
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where("_id").is(refID)
                        .and("institutionId").is(institutionId));
                logger.debug("Query formed : {}", query);
                return mongoTemplate.findOne(query, Repayment.class);
            } catch (DataAccessException e) {
                logger.error("Error occurred while fetching Repayment with probable cause {} ", ExceptionUtils.getStackTrace(e));
                throw new SystemException(String.format(" error occurred  while fetching RepaymentDetails with probable cause %s", ExceptionUtils.getStackTrace(e)));
            }
    }
}