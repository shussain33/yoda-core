package com.softcell.dao.mongodb.repository;

import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.utils.GngDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author bhuvneshk
 */
@Repository
public class ApplicationTrackingMongoRepository implements ApplicationTrackingRepository {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTrackingMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public ApplicationTrackingMongoRepository() {
        if (null == mongoTemplate)
            this.mongoTemplate = MongoConfig.getMongoTemplate();
    }


    @Override
    public void saveApplicationTrackingObj(ApplicationTracking applicationTracking) throws Exception {

        logger.debug("saveApplicationTrackingObj repo started ");

        try {

            mongoTemplate.insert(applicationTracking);

        } catch (Exception e) {

            logger.error("Error while saving app tracking obj [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<ApplicationTracking> getApplicationCaseHistory(String institutionId, String refId, String product) throws Exception {

        logger.debug("getApplicationCaseHistory repo started ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("gngRefId").is(refId));
            query.with(new Sort(Sort.Direction.ASC, "startDate"));

            return mongoTemplate.find(query, ApplicationTracking.class);

        } catch (Exception e) {

            logger.error("Error while fetching tracking history [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<ApplicationTracking> getApplicationHistory(ReportingModuleConfiguration configuration) throws Exception {

        logger.debug("getApplicationHistory repo started ");

        Date[] fromToDate = GngDateUtil.getFromToDate(configuration);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(configuration.getInstitutionID()).and("startDate").gt(fromToDate[0]).lt(fromToDate[1]));
            query.with(new Sort(Sort.Direction.ASC, "startDate"));

            return mongoTemplate.find(query, ApplicationTracking.class);

        }catch (Exception e){

            logger.error("Error while fetching getApplicationHistory [{}]", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


}
