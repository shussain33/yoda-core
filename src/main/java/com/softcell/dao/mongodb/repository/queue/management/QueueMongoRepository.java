package com.softcell.dao.mongodb.repository.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.AuditMongoRepository;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.gonogo.model.queueMgmt.CroQueueTracking;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.queue.management.QueueManagementHelper;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;
import com.softcell.gonogo.queue.management.Roles;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by amit on 9/3/18.
 */
@Repository
public class QueueMongoRepository implements QueueRepository {

    private static final Logger logger = LoggerFactory.getLogger(QueueMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveCroAudit(CroAudit croAudit) {
        try {
            /*if(!mongoTemplate.collectionExists(CroAudit.class))
                mongoTemplate.createCollection(CroAudit.class);*/
            mongoTemplate.insert(croAudit);
        } catch (Exception e) {
            logger.error("Error occurred while inserting CRO audit data with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting CRO audit data with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<CroAudit> getCroAudit(String userId, String institutionId) throws  Exception{
        return getCroAudit(userId, institutionId, null, null);
    }
    @Override
    public List<CroAudit> getCroAudit(String userId, String institutionId, Date fromDate, Date toDate) throws  Exception{
        // TODO : get On// Hold cases as well

        DateTime from = null, to = null;
        try {

            boolean todayFlag  = true;

            if( fromDate != null ){
                todayFlag = false;
                // retain only date part
                from = new DateTime(fromDate).withZoneRetainFields(DateTimeZone.UTC);
                // Check toDate for null. If It is null then set it for today
                if( toDate == null){
                    to = new DateTime(new Date()).withZoneRetainFields(DateTimeZone.UTC);
                }else {
                    to = new DateTime(toDate).withZoneRetainFields(DateTimeZone.UTC);
                }
            }
            // If fromDate is not provided ( = null) then today's statistics are fetched

            Query query = new Query();
            query.addCriteria(Criteria.where("userId").is(userId)
                    .and("institutionId").is(institutionId));
            if( todayFlag ) {
                Date start = GngDateUtil.getZeroTimeDate(new Date());
                Date end = DateUtils.addDays(start, 1);
                from = new DateTime(start).withZoneRetainFields(DateTimeZone.UTC);
                to = new DateTime(end).withZoneRetainFields(DateTimeZone.UTC);
            }

            query.addCriteria(Criteria.where("decisionDate").gte(from.toDate()).lte(to.toDate()) );
            query.fields().exclude("_id");
            query.with(new Sort(Sort.Direction.DESC, "decisionDate"));
            logger.debug("getCroAudit query : {} ", query.toString());
            return mongoTemplate.find(query, CroAudit.class);
        } catch (Exception e) {
            logger.error("Error while fetching croAudit data [{}]",e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Map<String, Long> fetchCroStatistics(String userId, String institutionId) throws  Exception {
        List<CroAudit> croAuditList = getCroAudit(userId, institutionId);
        Map<String, Long> result =
                croAuditList.stream().collect(
                        Collectors.groupingBy(
                                CroAudit::getDecision, Collectors.counting()
                        )
                );
        return result;
    }

    @Override
    public Map<String,Long> fetchCroStatistics(String userId, String institutionId, Date fromDate, Date toDate) throws  Exception{
        List<CroAudit> croAuditList = getCroAudit(userId, institutionId, fromDate, toDate);
        Map<String, Long> result =
                croAuditList.stream().collect(
                        Collectors.groupingBy(
                                CroAudit::getDecision, Collectors.counting()
                        )
                );
        return result;
    }

    @Override
    public boolean saveActivityLog(List<ActivityLogs> activityLogs) throws Exception {
        logger.debug("saveActivityLog repo started");

        try {
            /*if (!mongoTemplate.collectionExists(ActivityLogs.class)) {
                mongoTemplate.createCollection(ActivityLogs.class);
            }*/

            mongoTemplate.insert(activityLogs, ActivityLogs.class);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                logger.error("Error occurred while inserting activity logs " +
                        " with probable cause [{}] ", e.getMessage());
                throw new Exception(e.getMessage());
            }

            logger.debug("saveActivityLog repo completed");
            return true;
        } catch (Exception e) {
            logger.error("Error occurred while inserting activity log " +
                    " with probable cause [{}] ", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


    @Override
    public void updateCroQueueTracking(List<RealtimeUserStatus> userStatuses, List<Map<String, Object>> fieldValueMapList) {
        BulkOperations ops = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, CroQueueTracking.class);
        int i = 0;
        for (RealtimeUserStatus user : userStatuses) {
            Query query = new Query();
            query.addCriteria(Criteria.where("userId").is(user.getUserId()).and("institutionId").is(user.getInstitutionId()));

            Update update = new Update();
            Map<String, Object> fieldValueMap = fieldValueMapList.get(i);

            for (Map.Entry<String, Object> entry : fieldValueMap.entrySet()) {
                update.set(entry.getKey(), entry.getValue());
            }

            ops.updateOne(query, update);
            i++;
        }
        ops.execute();
    }

    @Override
    public void updateCroQueueTracking(String userId, String institutionId, Map<String, Object> fieldValueMap) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId).and("institutionId").is(institutionId));

        Update update = new Update();
        for (Map.Entry<String, Object> entry : fieldValueMap.entrySet()) {
            update.set(entry.getKey(), entry.getValue());
        }

        mongoTemplate.updateFirst(query, update, CroQueueTracking.class);
    }

    @Override
    public List<String> fetchQueuedCaseIds(int casesPerUser, String institutionId, String role, RequestCriteria criteria, int skip) {
        logger.debug("fetchQueuedCases repo started");
        List<String> caseIds = new ArrayList<>();
        try {
            /*Query query = new Query().limit(casesPerUser);
            query.addCriteria(Criteria.where("applicationRequest.currentStageId").is(GNGWorkflowConstant.CR_Q.toFaceValue())
                    .and("operator").is(null).and("applicationRequest.header.institutionId").is(institutionId))
                    .fields().include("_id");
            query.with(new Sort(Sort.Direction.ASC, "dateTime"));*/

            Query query = new Query().limit(casesPerUser);
            String croId = "default"; // ?? TODO : why is this needed??

            // Fetch query based on the role
            if (StringUtils.endsWithIgnoreCase(role, Roles.Role.CRO1.name()) || StringUtils.endsWithIgnoreCase(role, Roles.Role.CRO.name())) {
                query = getCRO1Query(croId, institutionId, criteria, skip, casesPerUser);
            } else if (StringUtils.endsWithIgnoreCase(role, Roles.Role.CRO2.name())) {
                query = getCRO2Query(croId, institutionId, skip, casesPerUser);
            } else if (StringUtils.endsWithIgnoreCase(role, Roles.Role.CRO3.name())) {
                query = getCRO3Query(croId, institutionId, skip, casesPerUser);
            } else if (StringUtils.endsWithIgnoreCase(role, Roles.Role.CRO4.name())) {
                query = getCRO4Query(croId, institutionId, skip, casesPerUser);
            }

            logger.debug("Query for fetchQueuedCaseIds for role {} : {}", role, query);
            // Execute query on db
            List<GoNoGoCustomerApplication> applications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
            // Get only ids to return
            caseIds = applications.stream().map(app -> app.getGngRefId()).collect(Collectors.toList());
            logger.debug("fetchQueuedCases repo completed");
            return caseIds;
        } catch (Exception e) {
            logger.error("Error occurred while fetchQueuedCases ", e.getMessage());
            throw new SystemException(e.getMessage());
        }
    }

    private Query getCRO1Query(String croID, String institutionId, RequestCriteria criteria, int skip, int limit) {
        return getCRO1Query(croID, institutionId, criteria, skip, limit, null);
    }

    private Query getCRO1Query(String croID, String institutionId, RequestCriteria criteria, int skip, int limit, Date limitingDate) {
        Query query = new Query();
        Criteria basicCriteria =
                Criteria.where("applicationRequest.header.croId").is(croID) // TODO : use decisionType
                        .and("applicationRequest.header.institutionId").is(institutionId)
                        //.and("applicationStatus").ne("NEW")
                        .and("applicationStatus").is(GNGWorkflowConstant.QUEUED.toFaceValue())
                        .and("applicationRequest.qdeDecision").ne(Boolean.TRUE)
                        .and("operator").is(null);
        if (limitingDate != null) {
            basicCriteria.and("dateTime").lt(limitingDate);
        }

            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */
        query.addCriteria(basicCriteria).skip(skip).limit(limit);

        query.fields()//.include("applicationRequest")
                .include("applicationStatus")
                .include("dateTime");

        if (QueueManagementHelper.CASES_SORT_BY_LATEST) {
            query.with(new Sort(Sort.Direction.DESC, "dateTime"));
        } else {
            query.with(new Sort(Sort.Direction.ASC, "dateTime"));
        }

        MongoRepositoryUtil.addHierarchyCriteria(criteria, query, mongoTemplate);

        return query;
    }

    private Query getCRO2Query(String croID, String institutionId, int skip, int limit) {
        List<String> allowedStatuses = new ArrayList<String>() {{
            add(GNGWorkflowConstant.APPROVED.toFaceValue());
            add(GNGWorkflowConstant.DECLINED.toFaceValue());
        }};
        Query query = new Query();
        Criteria basicCriteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                .and("applicationStatus").in(allowedStatuses)
                .and("applicationRequest.qdeDecision").ne(Boolean.TRUE)
                .and("operator").is(null);

            /*Criteria.where("applicationStatus").regex(
                    "APPROVED", "i"),*/
            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */
        query.addCriteria(basicCriteria).skip(skip).limit(limit);
        query.fields()//.include("applicationRequest")
                .include("applicationStatus")
                .include("dateTime");

        query.with(new Sort(Sort.Direction.DESC, "dateTime"));

        //MongoRepositoryUtil.addHierarchyCriteria(criteria, query,mongoTemplate);
        return query;
    }

    private Query getCRO3Query(String croID, String institutionId, int skip, int limit) {
        List<String> allowedStatuses = new ArrayList<String>() {{
            add(GNGWorkflowConstant.APPROVED.toFaceValue());
            add(GNGWorkflowConstant.DECLINED.toFaceValue());
        }};
        Query query = new Query();

        Criteria basicCriteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                .and("applicationStatus").in(allowedStatuses)
                .and("applicationRequest.qdeDecision").ne(Boolean.TRUE)
                .and("operator").is(null);
            /* NOTE :
                $ne selects the documents where the value of the field is not equal (i.e. !=) to the specified value.
                This includes documents that do not contain the field.
            */

        query.addCriteria(basicCriteria).skip(skip).limit(limit);

        query.fields()//.include("applicationRequest")
                .include("applicationStatus")
                .include("dateTime");

        query.with(new Sort(Sort.Direction.DESC, "dateTime"));

        //MongoRepositoryUtil.addHierarchyCriteria(criteria, query, mongoTemplate);

        return query;
    }

    private Query getCRO4Query(String croID, String institutionId, int skip, int limit) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where("header.institutionId").is(institutionId)
                        .and("currentStageId").is("DE")
                        .and("qdeDecision").ne(Boolean.TRUE)
                        .and("operator").is(null)
        ).skip(skip).limit(limit);
        query.with(new Sort(Sort.Direction.DESC, "dateTime"));

        // added for residence with pincode 0
        query.addCriteria(Criteria.where("request.applicant.address")
                .elemMatch(
                        Criteria.where("addressType").is("RESIDENCE")
                                .and("pin").is(0)));

        //MongoRepositoryUtil.addHierarchyCriteriaApplicationRequest( criteria, query, mongoTemplate);

        return query;
    }

    @Override
    public List<GoNoGoCustomerApplication> getAllUnassignedApplications() {
        Query query = new Query();
        Criteria basicCriteria =
                Criteria.where("applicationStatus").is(GNGWorkflowConstant.QUEUED.toFaceValue())
                        .and("applicationRequest.qdeDecision").ne(Boolean.TRUE)
                        .and("applicationRequest.qdeDecision").ne(null)
                        .and("operator").is(null);

        query.fields().include("applicationRequest.header.institutionId").include("assignedQueueGroupId")
                .include("applicationStatus").include("dateTime");

        MongoRepositoryUtil.addHierarchyCriteria(new RequestCriteria(), query, mongoTemplate);
        List<GoNoGoCustomerApplication> applications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        // Get only ids to return future use
        //List<String> caseIds = applications.stream().map(app -> app.getGngRefId()).collect(Collectors.toList());

        return applications;
    }

    @Override
    public List<GoNoGoCustomerApplication> getAssignedCases(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                .and("operator").ne(null));
        query.fields().include("operator").include("_id");

        Sort.Order operatorSort = new Sort.Order(Sort.Direction.ASC, "operator");
        Sort.Order idSort = new Sort.Order(Sort.Direction.DESC, "_id");
        Sort sort = new Sort(new ArrayList<Sort.Order>() {{
            add(operatorSort);
            add(idSort);
        }});
        query.with(sort);

        logger.debug("getAssignedCases(institutionId) repo query formed {} for institutionId {}", query.toString(), institutionId);

        List<GoNoGoCustomerApplication> resultList = mongoTemplate.find(
                query, GoNoGoCustomerApplication.class);

        return resultList;

    }

    @Override
    public List<CroQueueTracking> getCroQueueTrackingList(String institutionId, List<String> users, boolean includeFlag) {
        Query query = new Query();
        Criteria criteria = Criteria.where("institutionId").is(institutionId);

        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(users)) {
            if (includeFlag) {
                criteria.and("userId").in(users);
            } else {
                criteria.and("userId").nin(users);
            }
        }
        query.addCriteria(criteria);
        return mongoTemplate.find(query, CroQueueTracking.class);
    }

    @Override
    public boolean isApplicationUnassigned(String refId, String userId) {
        Query query = new Query();

        query.addCriteria(Criteria.where("_id").is(refId)
                .orOperator(Criteria.where("operator").is(null), Criteria.where("operator").is(userId)));

        return mongoTemplate.exists(query, ApplicationRequest.class);
    }

    @Override
    public void updateOperator(String userId, List<String> assignedIds) {

        logger.debug("updateOperator repo started");
        try {
            Query query = new Query();
            // Iterate thr assignedIds
            query.addCriteria(Criteria.where("_id").in(assignedIds));
            Update update = new Update();
            update.set("operator", userId);
            mongoTemplate.updateMulti(query, update, GoNoGoCustomerApplication.class);

            logger.debug("updateOperator repo saved operator to {} successfully", userId);
/*

            GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);
            updateApplicationInElasticsearch(goNoGoCroApplicationResponse);
*/

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while updating operator for refIds {}",
                    assignedIds, e.getMessage());
            throw new SystemException(e.getMessage());
        }
    }
}
