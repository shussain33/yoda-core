package com.softcell.dao.mongodb.repository;

import com.softcell.config.ComponentConfiguration;
import com.softcell.config.MultiBreComponentConfiguration;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vinodk
 *         <p/>
 *         This class implements ReAppraisalConfigurationRepository to help
 *         retrieve the ReAppraise app related details
 */
@Repository
public class ComponentConfigurationMongoRepository implements ComponentConfigurationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ComponentConfigurationMongoRepository.class);

    private MongoTemplate mongoTemplate;

    public ComponentConfigurationMongoRepository() {
        //FIXME It will be removed when new workflow will be introduced as It is not getting autowired in current workflow call
        if(mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
    }

    @Autowired
    public ComponentConfigurationMongoRepository(MongoTemplate mongoTemplate) {

        this.mongoTemplate = mongoTemplate;

        //FIXME It will be removed when new workflow will be introduced as It is not getting autowired in current workflow call
        if(mongoTemplate == null)
            this.mongoTemplate = MongoConfig.getMongoTemplate();
    }

    @Override
    public ComponentConfiguration getByInstitutionIdAndType(
            final String institutionId,
            ComponentConfigurationType componentConfigType) throws SystemException {

        logger.debug("getByInstitutionIdAndType repo is invoked for institution {}",institutionId);

        if (StringUtils.isBlank(institutionId))
            return null;

        // Check in cache if found then return app else fetch from  database.
        ComponentConfiguration componentConfiguration = getFromCache(institutionId, componentConfigType);

        if (componentConfiguration != null) {

            return componentConfiguration;

        }


        try {

            //Not found in cache fetch from database.

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("componentConfigType").is(componentConfigType));

            componentConfiguration = mongoTemplate.findOne(query, ComponentConfiguration.class);

            //Save component app in config cache
            saveInCache(institutionId, componentConfiguration);

            logger.debug("getByInstitutionIdAndType repo is completed for institution {}",institutionId);

            return componentConfiguration;

        } catch (Exception e) {

            logger.error("Error occurred while fetching componentConfiguration for institutionId {} with probable cause [{}]"
                    ,institutionId,e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching componentConfiguration " +
                    "for institutionId {%s} with probable cause [{%s}]",institutionId,e.getMessage()));
        }
    }

    @Override
    public boolean create(ComponentConfiguration reAppraisalConfiguration) throws SystemException {

        logger.debug("component app create is invoked insert component app");

        try {

            mongoTemplate.insert(reAppraisalConfiguration);

            return true;

        } catch (Exception e) {

            logger.error("Error occurred while inserting component app  [{}] ",e.getMessage());

            throw new SystemException(String.format("Error occurred while inserting component app  [%s] ",e.getMessage()));
        }
    }

    /**
     * Saves the app in the cache.
     *
     * @param institutionId   institution id for which app to be saved in Cache.
     * @param componentConfig component app which needs to be save in cache.
     */
    private void saveInCache(String institutionId, ComponentConfiguration componentConfig) {

        logger.debug("saveInCache is invoked to same component config in cache");

        List<ComponentConfiguration> componentConfigList = Cache.configCache.get(institutionId);

        // Check if list of component config exists for institute if yes then
        // add missing config to existing list of cache else create new list
        if (componentConfigList != null) {

            componentConfigList.add(componentConfig);

        } else {

            componentConfigList = new ArrayList<>();

            componentConfigList.add(componentConfig);

        }
        //Insert app in cache
        Cache.configCache.put(institutionId, componentConfigList);

        logger.debug("saveInCache is updated config in cache");

    }


    /**
     * @param institutionId       institution id for which app will be fetched.
     * @param componentConfigType type of the app @ComponentConfigurationType
     * @return @ComponentConfiguration if found in cache in null.
     */
    private ComponentConfiguration getFromCache(
            final String institutionId,
            ComponentConfigurationType componentConfigType) throws SystemException {

        logger.debug("getFromCache repo is invoked for institution {}",institutionId);

        if (Cache.configCache != null && Cache.configCache.get(institutionId) != null) {

            List<ComponentConfiguration> componentConfigs = Cache.configCache.get(institutionId);

            if(componentConfigs != null && !componentConfigs.isEmpty()) {

                for (ComponentConfiguration componentConfig : componentConfigs) {

                    if (componentConfig != null
                            && componentConfig.getComponentConfigType() != null
                            && componentConfig.getComponentConfigType().equals(
                            componentConfigType)) {

                        logger.debug("getFromCache repo is completed with componentConfig value");

                        return componentConfig;

                    }
                }
            }
        }

        logger.debug("getFromCache repo is completed with no response");

        return null;
    }

    @Override
    public List<ComponentConfiguration> getSystemComponentConfiguration(
            ComponentConfigurationType componentConfigurationType) throws SystemException {

        logger.debug("getSystemComponentConfiguration repo is invoked to get all component app ");

        Query query = new Query();

        query.addCriteria(Criteria.where("componentConfigType").is(componentConfigurationType).and("active").is(true));

        return mongoTemplate.find(query, ComponentConfiguration.class);
    }

    @Override
    public List<MultiBreComponentConfiguration> getMultiBreComponentConfiguration(
            ComponentConfigurationType componentConfigurationType) throws SystemException {

        logger.debug("getMultiBreComponentConfiguration repo is invoked to get all component app ");

        Query query = new Query();

        query.addCriteria(Criteria.where("componentConfigType").is(componentConfigurationType).and("active").is(true));

        return mongoTemplate.find(query, MultiBreComponentConfiguration.class);
    }

    @Override
    public List<ComponentConfiguration> getSystemComponentConfiguration(
            ComponentConfigurationType componentConfigurationType, final String institutionId) throws SystemException {

        logger.debug("getSystemComponentConfiguration repo is invoked to get {} component app ",institutionId);

        Query query = new Query();

        query.addCriteria(
                Criteria.where("componentConfigType").is(componentConfigurationType).and("institutionId")
                        .is(institutionId).and("active").is(Boolean.TRUE));

        return mongoTemplate.find(query, ComponentConfiguration.class);
    }
}
