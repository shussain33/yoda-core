package com.softcell.dao.mongodb.repository;

import com.mongodb.WriteResult;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.model.core.CollectionConfig;
import com.softcell.gonogo.model.request.CollectionDataRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommonFunctionalityMongoRepository implements CommonFunctionalityRepository{

    private static Logger logger = LoggerFactory.getLogger(CommonFunctionalityMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public CommonFunctionalityMongoRepository(){
        if(mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
    }

    @Override
    public List<?> fetchCollectionData (Class<?> className, CollectionDataRequest request) {
        Query query = new Query();

        String collectionName = request.getCollectionName();
        String refID = request.getRefID();
        String institutionId = request.getHeader().getInstitutionId();

        CollectionConfig config = mongoTemplate.findOne(new Query(Criteria.where("collectionName").is(collectionName)), CollectionConfig.class);
        if (null != config) {
            if (StringUtils.isNotEmpty(config.getRefIdMapping())) {
                query.addCriteria(Criteria.where(config.getRefIdMapping()).is(refID));
            }
            if (StringUtils.isNotEmpty(config.getInstitutionIdMapping())) {
                query.addCriteria(Criteria.where(config.getInstitutionIdMapping()).is(institutionId));
            }
        }

        List<?> dataList = mongoTemplate.find(query, className);
        return dataList;
    }

    @Override
    public boolean saveDataInCollection(Object obj, String collectionName) {
        try {
            mongoTemplate.save(obj, collectionName);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean insertCollectionConfig(CollectionConfig config) {
        Query query = new Query();
        query.addCriteria(Criteria.where("collectionName").is(config.getCollectionName()));

        Update update = new Update();
        update.set("collectionName", config.getCollectionName());
        update.set("refIdMapping", config.getRefIdMapping());
        update.set("institutionIdMapping", config.getInstitutionIdMapping());

        WriteResult result = mongoTemplate.upsert(query, update, CollectionConfig.class);
        return result.isUpdateOfExisting();
    }

    @Override
    public List<CollectionConfig> fetchCollectionConfig() {
        return mongoTemplate.findAll(CollectionConfig.class);
    }
}
