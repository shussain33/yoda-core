package com.softcell.dao.mongodb.repository;

import com.softcell.config.ServiceCommunication;
import com.softcell.dao.mongodb.config.MongoConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by ssg0268 on 11/6/19.
 */

@Repository
public class UrlConfigurationRepositoryImpl implements UrlConfigurationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationMongoRepository.class);
    @Autowired
    private MongoTemplate mongoTemplate;

    public UrlConfigurationRepositoryImpl() {
        //FIXME It will be removed when new workflow will be introduced as It is not getting autowired in current workflow call
        if (mongoTemplate == null) {
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
    }

    @Override
    public ServiceCommunication getServiceCommunication(String instId, String type){
         Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(instId)
                .and("type").is(type));
        ServiceCommunication serviceCommunication = mongoTemplate.findOne(query, ServiceCommunication.class);
        return serviceCommunication;

    }
}
