/**
 * yogeshb6:43:29 pm  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.emudra.SFTPLog;
import com.softcell.workflow.component.finished.AuditData;

/**
 * @author yogeshb
 *
 */
public interface LoggerRepository {
    /**
     *
     * @param workFlowLog
     * @return
     */
    boolean setLogDetails(OtpLog workFlowLog);

    /**
     *
     * @param aadharLog
     * @return
     */
    boolean setAadharLogDeatails(AadharLog aadharLog);

    /**
     * @param auditData
     * @param auditCollectionName
     * @return
     */
    boolean saveAuditData(AuditData auditData, String auditCollectionName);

    /**
     *
     * @param eSignedLog
     */
    void saveESignedLog(ESignedLog eSignedLog);

    void saveSFTFLog(SFTPLog sftpLog);
}
