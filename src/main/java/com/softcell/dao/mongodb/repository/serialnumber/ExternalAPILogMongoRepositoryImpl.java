package com.softcell.dao.mongodb.repository.serialnumber;


import com.softcell.config.KarzaProcessingInfo;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.ThirdPartyApiLog;
import com.softcell.gonogo.model.adroit.AdroitCallLog;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.finfort.FinfortCallLog;
import com.softcell.gonogo.model.kyc.LosKarzaLog;
import com.softcell.gonogo.model.kyc.TotalKycReqResLog;
import com.softcell.gonogo.model.lms.LmsCallLog;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.mifin.MiFinAmbitCallLog;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.qde.CustomerDataServiceCallLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AmbitMifinLog;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.sms.SmsServReqResLogRequest;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.model.tclos.log.TcLosReqReslog;
import com.softcell.gonogo.serialnumbervalidation.*;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.service.impl.KarzaHelper;
import com.softcell.service.thirdparty.utils.IciciHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

import static com.softcell.service.utils.MiFinHelper.MIFIN_DEDUPE_DETAIL;


/**
 * @author mahesh This class will check the samsung serial number is available
 *         in the database, if not available it will store in the database.
 */

@Repository
public class ExternalAPILogMongoRepositoryImpl implements
        ExternalAPILogRepository {

    Logger logger = LoggerFactory
            .getLogger(ExternalAPILogMongoRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public ExternalAPILogMongoRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public boolean saveSerialNumberInfo(SerialNumberInfo serialNumberInfo) {
        try {

            mongoTemplate.insert(serialNumberInfo);
            return true;
        } catch (Exception e) {
            logger.error("Error occurred while inserting serial number validation logs with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting serial number validation logs with probable cause [{%s}]", e));
        }
    }

    @Override
    public boolean checkSerialNumberInDedupe(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Query query = new Query();
        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                query.addCriteria(Criteria.where("serialNumber").is(serialSaleConfirmationRequest.getSerialNumber())
                        .and("status").is(Status.VALID.name()).and("vendor").is(serialSaleConfirmationRequest.getVendor()));

            } else if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())) {
                query.addCriteria(Criteria.where("imeiNumber").is(serialSaleConfirmationRequest.getImeiNumber())
                        .and("status").is(Status.VALID.name()).and("vendor").is(serialSaleConfirmationRequest.getVendor()));
            } else {
                return false;
            }

        } else {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                query.addCriteria(Criteria.where("serialNumber").is(serialSaleConfirmationRequest.getSerialNumber())
                        .and("status").is(Status.VALID.name()).and("vendor").is(serialSaleConfirmationRequest.getVendor()));
            } else {
                return false;
            }


        }

        return mongoTemplate.exists(query, SerialNumberInfo.class);

    }

    @Override
    public PostIpaRequest checkSerialNumberValidationFlag(
            SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest) {

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(
                serialNumberApplicableCheckRequest.getRefId()));
        query.fields().include("postIPA");

        return mongoTemplate.findOne(query,
                PostIpaRequest.class);


    }

    @Override
    public boolean saveSerialNumberRequest(
            SerialSaleConfirmationRequest serialsaleconfirmationrequest) {
        try {
            mongoTemplate.insert(serialsaleconfirmationrequest);
            return true;
        } catch (Exception e) {
            logger.error("Error occurred while inserting serial number validation request logs with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting serial number validation request logs with probable cause [{%s}]", e));
        }
    }

    @Override
    public SerialNumberInfo viewUpdatedSRNumberDetails(
            String referenceId) {
        logger.info("GetUpdatedSerialNo repo started successfully");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(referenceId));
            query.with(new Sort(Sort.Direction.DESC, "dateTime"));
            return mongoTemplate.findOne(query,
                    SerialNumberInfo.class);
        } catch (Exception e) {
            logger.error("Error occurred while getting UpdatedSerialNumberInfo with probable cause [{}]", e.getMessage());
            return null;
        }
    }

    @Override
    public SerialNumberInfo getSerialNumberDetails(
            String referenceId) {
        logger.info("Get Serial number repo started successfully");
        SerialNumberInfo serialNumberInfo = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(referenceId).and("status").is(Status.VALID.name()));
            query.with(new Sort(Sort.Direction.DESC, "dateTime"));

            if (null == (serialNumberInfo = mongoTemplate.findOne(query,
                    SerialNumberInfo.class))) {
                query = new Query();
                query.addCriteria(Criteria.where("refID").is(referenceId));
                query.with(new Sort(Sort.Direction.DESC, "dateTime"));
                serialNumberInfo = mongoTemplate.findOne(query,
                        SerialNumberInfo.class);
            }

        } catch (Exception e) {
            logger.error("Error occurred while getting UpdatedSerialNumberInfo with probable cause [{}]", e.getMessage());
        }
        return serialNumberInfo;
    }

    @Override
    public SerialNumberInfo getValidatedImeiOrSerialNumber(GetRollbackDetailsRequest getRollbackDetailsRequest) {
        try {
            Query query = new Query();
            /*if(StringUtils.isNotBlank(getRollbackDetailsRequest.getHeader().getDealerId())){
                query.addCriteria(Criteria.where("refID").is(getRollbackDetailsRequest.getReferenceID())
                .and("dealerId").is(getRollbackDetailsRequest.getHeader().getDealerId())
                        .and("status").is(Status.VALID.name()));
            }else{*/
            query.addCriteria(Criteria.where("refID").is(getRollbackDetailsRequest.getReferenceID())
                    .and("status").is(Status.VALID.name()));
//            }
            query.fields().include("serialNumber").include("imeiNumber").include("vendor").include("skuCode");
            return mongoTemplate.findOne(query,
                    SerialNumberInfo.class);
        } catch (DataAccessException e) {
            logger.error("Error occurred while getting UpdatedSerialNumberInfo with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting UpdatedSerialNumberInfo with probable cause [{%s}]", e.getMessage()));
        }

    }

    @Override
    public void updateSerialNumberInfoLog(RollbackRequest rollbackRequest, String status) {
        logger.info("updateSerialNumberInfoLog repo started successfully");
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(rollbackRequest.getReferenceID())
                    .and("status").is(Status.VALID.name()));
            Update update = new Update();
            update.set("status", status);
            mongoTemplate.updateFirst(query, update,
                    SerialNumberInfo.class);
        } catch (Exception e) {
            logger.error("Error occurred while getting updateSerialNumberInfoLog with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting updateSerialNumberInfoLog with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public void saveRollbackLog(RollbackFeatureLog rollbackFeatureLog) {
        try {
            mongoTemplate.insert(rollbackFeatureLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting RollbackFeatureLog with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting RollbackFeatureLog with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveTotalKycReqResLog(TotalKycReqResLog totalKycReqResLog) {
        try {
            /*if (!mongoTemplate.collectionExists(TotalKycReqResLog.class))
                mongoTemplate.createCollection(TotalKycReqResLog.class);*/
            mongoTemplate.insert(totalKycReqResLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Total Kyc Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Total Kyc Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveSmsServiceReqResLog(SmsServiceReqResLog smsServiceReqResLog) {
        try {
            /*if (!mongoTemplate.collectionExists(SmsServiceReqResLog.class))
                mongoTemplate.createCollection(SmsServiceReqResLog.class);*/
            mongoTemplate.insert(smsServiceReqResLog);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format(ErrorCode.SMSSERV_REQRESLOG_ERROR, e));
        }
    }

    @Override
    public void saveCustomerDataServiceCallLog(CustomerDataServiceCallLog customerDataServiceCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(CustomerDataServiceCallLog.class))
                mongoTemplate.createCollection(CustomerDataServiceCallLog.class);*/
            mongoTemplate.insert(customerDataServiceCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Customer Data Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Customer Data Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveDisbursementLog(DisbursementFeatureLog disbursementFeatureLog) {
        try {
            mongoTemplate.insert(disbursementFeatureLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting RollbackFeatureLog with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting RollbackFeatureLog with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveRawResonseLog(RawResponseLog rawResponseLog) {
        try {
            mongoTemplate.insert(rawResponseLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting rawResponseLog with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting rawResponseLog with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveCreditVidyaCallLog(GetUserProfileCallLog getUserProfileCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(GetUserProfileCallLog.class))
                mongoTemplate.createCollection(GetUserProfileCallLog.class);*/
            mongoTemplate.insert(getUserProfileCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Credit Vidya Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Credit Vidya Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveTvscdCallLog(SaathiCallLog saathiCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(SaathiCallLog.class))
                mongoTemplate.createCollection(SaathiCallLog.class);*/
            mongoTemplate.insert(saathiCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting TVS CD api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting TVS CD api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<GetUserProfileCallLog> fetchCreditVidyaResponse(CreditVidyaLogRequest creditVidyaLogRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("mobileNumber").is(creditVidyaLogRequest.getMobileNo()));
            query.with(new Sort(Sort.Direction.DESC, "_id"));
            return mongoTemplate.find(query, GetUserProfileCallLog.class);

        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching Credit Vidya Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<SaathiCallLog> fetchSaathiResponse(SaathiLogRequest saathiLogRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("mobileNumber").is(saathiLogRequest.getMobileNo())
                    .and("refId").is(saathiLogRequest.getRefId()));
            query.with(new Sort(Sort.Direction.DESC, "_id"));
            return mongoTemplate.find(query, SaathiCallLog.class);

        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching Saathi Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public TotalKycReqResLog getTKycLog(TKycLogRequest tKycLogRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("kycId").is(tKycLogRequest.getKycId())
                    .and("action").is(tKycLogRequest.getAction()));
            return mongoTemplate.findOne(query, TotalKycReqResLog.class);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching Total Kyc Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<SmsServiceReqResLog> getSmsServiceReqResLog(SmsServReqResLogRequest smsServReqResLogRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("mobileNumber").regex(smsServReqResLogRequest.getMobileNumber()));
            return mongoTemplate.find(query, SmsServiceReqResLog.class);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format(ErrorCode.SMSSERV_REQRESLOG_ERROR, e));

        }
    }

    @Override
    public void savetcLosReqResLog(TcLosReqReslog tcLosReqReslog) {
        try {
            /*if (!mongoTemplate.collectionExists(TcLosReqReslog.class))
                mongoTemplate.createCollection(TcLosReqReslog.class);*/
            mongoTemplate.insert(tcLosReqReslog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting TcLos Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Tc Los Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public TcLosReqReslog getTcLosLog(TcLosLogRequest tcLosLogRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(tcLosLogRequest.getRefId()));
            return mongoTemplate.findOne(query, TcLosReqReslog.class);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching Tc Los Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<AadharLog> getAadharLogByAadharNoAndInstId(String aadharNo, String institutionId) {
        try {
            Query query = new Query();
            query.with(new Sort(Sort.Direction.DESC, "AadharClientRequest.header.dateTime"));

            query.addCriteria(Criteria.where("AadharClientRequest.aadharNumber").is(aadharNo)
                    .and("AadharClientRequest.header.institutionId").is(institutionId));

            return mongoTemplate.find(query, AadharLog.class);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new SystemException(String.format("Error occur while fetching aadhar Log by aadharNo {%s} and institutionId {%s} with probable cause [{%s}]", aadharNo, institutionId, e));

        }
    }

    @Override
    public void saveSerialNumberApplicableVendorLog(SerialNumberApplicableVendorLog serialNumberApplicableVendorLog) {
        try {
            /*if (!mongoTemplate.collectionExists(SerialNumberApplicableVendorLog.class))
                mongoTemplate.createCollection(SerialNumberApplicableVendorLog.class);*/
            mongoTemplate.insert(serialNumberApplicableVendorLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting SerialNumberApplicableVendor Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting SerialNumberApplicableVendor Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveMiFinCallLog(MiFinCallLog miFinCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(MiFinCallLog.class))
                mongoTemplate.createCollection(MiFinCallLog.class);*/
            mongoTemplate.insert(miFinCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting MiFin api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting MiFin api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public MiFinCallLog fetchMiFinCallLog(String refId) {
        Query query = new Query(Criteria.where("refId").is(refId).and("api").ne(UrlType.MIFINPUSHIMD.toValue()));
        query.with(new Sort(Sort.Direction.DESC, "callDate") );
        return mongoTemplate.findOne(query, MiFinCallLog.class);
    }

    //fetch MIFIndedupe detail
    public MiFinCallLog fetchDedupetMiFinCallLog(String refId) {
        Query query = new Query(Criteria.where("refId").is(refId)
                .and("requestType").is(MIFIN_DEDUPE_DETAIL));
        return mongoTemplate.findOne(query, MiFinCallLog.class);

    }

    @Override
    public void saveMifinAmbitLog(MiFinAmbitCallLog miFinAmbitCallLog) {
        try{
            /*if(!mongoTemplate.collectionExists(MiFinAmbitCallLog.class)) {
                mongoTemplate.createCollection(MiFinAmbitCallLog.class);
            }*/
            mongoTemplate.insert(miFinAmbitCallLog);
        }   catch (Exception e)
        {
            logger.error("Error occurred while inserting Ambit MiFin api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Ambit MiFin api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<MiFinAmbitCallLog> fetchAmbitMifinCalllog(AmbitMifinLog ambitMifinLog) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(ambitMifinLog.getRefID()))
                    .addCriteria(Criteria.where("api").is(ambitMifinLog.getApi()));
            return  mongoTemplate.find(query, MiFinAmbitCallLog.class);
        }
        catch (Exception e){
            logger.error("Error occurred while fetching MiFin logs api Request Response Log with probable cause [{}]", e.getStackTrace());
            return null;
        }
    }

    @Override
    public void saveLMSCallLog(LmsCallLog LmsCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(LmsCallLog.class))
                mongoTemplate.createCollection(LmsCallLog.class);*/
            mongoTemplate.insert(LmsCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting MiFin api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting MiFin api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveAdroitCallLog(AdroitCallLog adroitCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(AdroitCallLog.class))
                mongoTemplate.createCollection(AdroitCallLog.class);*/
            mongoTemplate.insert(adroitCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting MiFin api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting MiFin api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<AdroitCallLog> fetchAdroitAckIds(String refId, List<String> collateralIds, String valuationInitiate) {
        try {
            Query query = new Query();
            query.with(new Sort(Sort.Direction.ASC, "collateralId")
                                .and(new Sort(Sort.Direction.DESC, "callDate")) );

            query.addCriteria(Criteria.where("refId").is(refId)
                    .and("collateralId").in(collateralIds)
                    .and("acknowledgementId").ne(null)
                    .and("requestType").is(valuationInitiate));
            query.fields().include("refId")
                            .include("institutionId")
                            .include("collateralId")
                            .include("acknowledgementId");

            return mongoTemplate.find(query, AdroitCallLog.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
            throw new SystemException(String.format("Exception while fetching adroit call log for {} {}",
                    refId, valuationInitiate, e));
        }
    }

    @Override
    public void saveFinfortCallLog(FinfortCallLog finfortCallLog) {
        try {
            /*if (!mongoTemplate.collectionExists(FinfortCallLog.class))
                mongoTemplate.createCollection(FinfortCallLog.class);*/
            mongoTemplate.insert(finfortCallLog);
        } catch (Exception e) {
            logger.error("Error occurred while Finfort api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Finfort api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public FinfortCallLog fetchFinfortCallLogByAckId(String ackId) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("acknowledgementId").is(ackId) );
            query.fields().include("refId")
                    .include("institutionId")
                    .include("requestType")
                    .include("acknowledgementId");

            return mongoTemplate.findOne(query, FinfortCallLog.class);
        } catch (DataAccessException e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
            throw new SystemException(String.format("Exception while fetching finfort call log for {} ",
                    ackId, e));

        }
    }

    @Override
    public boolean UpdateMifinCallog(MiFinCallLog apiLog, String refID) {
        logger.info("update reponse miFinCallLog");
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("refId").is(refID)
                   .and("miFinResponse").is(null);
            query.addCriteria(criteria);
            query.with(new Sort(Sort.Direction.DESC, "callDate") );
            Update update = new Update();
            update.set("miFinResponse", apiLog.getMiFinResponse());
           /* update.set("isAvailable", apiLog.isAvailable());*/
            mongoTemplate.upsert(query, update, MiFinCallLog.class);
            logger.info("update query {} " , query);
             return true;
        } catch (Exception e) {
            logger.error("Exception caught while saving MiFinCallLog " + e.getMessage());
            //e.printStackTrace();
            throw new SystemException(String.format("Exception caught while saving MiFinCallLog {%s}", e.getMessage()));
        }
    }

    @Override
    public MiFinCallLog ToverfyMifinCall(String refId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("refId").is(refId);
        query.addCriteria(criteria);
        query.with(new Sort(Sort.Direction.DESC, "callDate") );
        List<MiFinCallLog>  MifinCallLogList = mongoTemplate.find(query, MiFinCallLog.class);
        if(CollectionUtils.isNotEmpty(MifinCallLogList)){
            logger.info("mifinCall Fix {} " , MifinCallLogList.get(0));
            return  MifinCallLogList.get(0);
        }else{
            return null;
        }


    }

    //FETCH APPLICANT DATA
    @Override
    public MiFinCallLog fetchMiFinCallLogByRequestType(String refId, String requestType) {
        Query query = new Query(Criteria.where("refId").is(refId)
                .and("requestType").is(requestType));
        return mongoTemplate.findOne(query, MiFinCallLog.class);
    }

    public List<MiFinCallLog> fetchMiFinCallLogForDedupe(String refId, String requestType) {
        Query query = new Query(Criteria.where("refId").is(refId)
                .and("requestType").is(requestType));
        return mongoTemplate.find(query, MiFinCallLog.class);
    }

    @Override
    public void saveIciciCallLog(ThirdPartyApiLog apiLog){
        try {
            /*if (!mongoTemplate.collectionExists(IciciHelper.COLLECTION_NAME))
                mongoTemplate.createCollection(IciciHelper.COLLECTION_NAME);*/
            mongoTemplate.insert(apiLog, IciciHelper.COLLECTION_NAME);
        } catch (Exception e) {
            logger.error("Error occurred while ICICI api Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting ICICI api Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public ThirdPartyApiLog fetchIciciCallLog(String refId){
        try {
            Query query = new Query(Criteria.where("refId").is(refId));
            query.with(new Sort(Sort.Direction.DESC, "callDate") );

            query.fields().include("refId")
                    .include("institutionId")
                    .include("applicantId")
                    .include("requestType")
                    .include("responseString")
                    .include("response");

            return mongoTemplate.findOne(query, ThirdPartyApiLog.class, IciciHelper.COLLECTION_NAME);
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            throw new SystemException(String.format("Exception while fetching icici insurance call log for {} ",
                    refId, e));

        }
    }

    @Override
    public KarzaProcessingInfo fetchKarzaProcessingInfo(String institutionId, String kycNumber,
                                                        String applicationId, String applicantId, String kycName) {
        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("kycNumber").is(kycNumber)
                .and("refId").is(applicationId)
                .and("kycName").is(kycName)
                .and("status").is(KarzaHelper.VERIFIED));
        return mongoTemplate.findOne(query, KarzaProcessingInfo.class);
    }

    @Override
    public void saveKarzaProcessingInfo(KarzaProcessingInfo karzaProcessingInfo) {
        try {
            mongoTemplate.insert(karzaProcessingInfo);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Karza Processing Info with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Karza Processing Info with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveKarzaTotalKycReqResLog(LosKarzaLog losKarzaLog) {
        try {
            /*if (!mongoTemplate.collectionExists(LosKarzaLog.class))
                mongoTemplate.createCollection(LosKarzaLog.class);*/
            mongoTemplate.insert(losKarzaLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Karza Total Kyc Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Karza Total Kyc Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public void saveSobreCallLog(ScoringCallLog callLog) {

        logger.debug("Inside save SoBRE Request Response");
        try {
            // TODO: CollectionExists commented
            /*if (!mongoTemplate.collectionExists("scoringCallLog")) {
                mongoTemplate.createCollection("scoringCallLog");
            }*/
            mongoTemplate.insert(callLog);
        } catch (Exception e) {
            logger.error("Error occurred while inserting Scoring Request Response Log with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting Scoring Request Response Log with probable cause [{%s}]", e));
        }
    }

    @Override
    public List<ScoringCallLog> fetchScoringCallLogList(String instituteId, String refId, String requestType, List<String> includeFields) {
        List<ScoringCallLog> scoringCallLogList = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(instituteId).and("refId").is(refId).and("requestType").is(requestType));
            query.with(new Sort(Sort.Direction.DESC, "callDate"));
            if(CollectionUtils.isNotEmpty(includeFields)){
                includeFields.forEach(fieldMapping -> {
                    query.fields().include(fieldMapping);
                });
            }
            logger.debug("Query for fetchScoringCallLogList() - {}", query.toString());
            scoringCallLogList  = mongoTemplate.find(query,ScoringCallLog.class);
        } catch (Exception e) {
            logger.error("Error occurred while scoringCallLog List probable cause [{}]", e.getStackTrace());
        }
        return scoringCallLogList;
    }

    @Override
    public void saveCallLog(ThirdPartyApiLog apiLog, String collectionName) {
        try {
            /*if (!mongoTemplate.collectionExists(collectionName))
                mongoTemplate.createCollection(collectionName);*/
            mongoTemplate.insert(apiLog, collectionName);
        } catch (Exception e) {
            logger.error("Error occurred while inserting callLog into {} with probable cause [{}]", collectionName, e);
            throw new SystemException(String.format("Error occurred while inserting callLog into %s with probable cause [{%s}]",
                    collectionName, e));
        }
    }

}
