package com.softcell.dao.mongodb.repository.digitization;

import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.digitization.DownloadDigitizationFormRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.Document;


/**
 * @author yogeshb
 */
public interface DigitizationRepository {

    /**
     * @param applicantPhotoRequest
     * @return
     */
    public String getBase64Image(FileUploadRequest applicantPhotoRequest) throws Exception;

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    public GoNoGoCroApplicationResponse getApplication(String institutionId,
                                                       String refID) throws Exception;

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    public PostIPA getPostIpa(String institutionId, String refID) throws Exception;

    /**
     * @param logoId
     * @return
     */
    public String getBase64Image(String logoId) throws Exception;

    /**
     *
     * @param institutionId
     * @param refID
     * @return
     * @throws Exception
     */
    PostIpaRequest getPostIpaRequest(String institutionId, String refID)throws Exception;

    /**
     *
     * @param refID
     * @return
     */
    SerialNumberInfo getImeiNumberDetails(String refID);

    /**
     *
     * @param downloadDigitizationFormRequest
     * @return
     */
    Document getDigitizationForm(DownloadDigitizationFormRequest downloadDigitizationFormRequest);
}
