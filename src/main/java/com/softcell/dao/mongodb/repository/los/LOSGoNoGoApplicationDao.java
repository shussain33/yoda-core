package com.softcell.dao.mongodb.repository.los;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.AuditingInfo;
import com.softcell.gonogo.model.los.tvs.DocumentListForGroupFive;
import com.softcell.gonogo.model.los.tvs.TvsGroupServiceStatus;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;

import java.util.List;


public interface LOSGoNoGoApplicationDao {
	List<GoNoGoCustomerApplication> getAllgoNoGoCustomerApplicationslistRecord();

	GoNoGoCustomerApplication getGONOGOApplicationAsPerCaseId(String gngRefId);

	List<GoNoGoCustomerApplication> getsearchRecord(String date, String stage);

	PostIpaRequest getpostIpaRequestBysRefID(String sRefID);

	SerialNumberInfo getSerialNumberInfoBysRefID(String refID);

	InsurancePremiumDetails getinsurancePremiumDetailsBysRefID(String refID);

	ExtendedWarrantyDetails getextendedWarrantyDetailsBysRefID(String refID);

	SchemeMasterData  getschemeMasterDataBysRefID(String schemeID);

	List<ESignedLog> getesignedLogByrefIdBysRefID(String refID);

	FileUploadRequest getfileUploadRequestAsPerCaseId(String gngRefId);

	DocumentListForGroupFive getUploadFileDetailsAsPerCaseId(String gngRefId);

	TvsGroupServiceStatus getTvsGroupServiceStatus(String gngRefI);

	public void saveTvsGroupServicStatus(TvsGroupServiceStatus groupService_Status);

	public void postAuditingInfo(AuditingInfo PostAuditingInfo);

	List<AuditingInfo> getAuditLogById(String _id);

	List<AuditingInfo> getCompleteAuditLog();


}