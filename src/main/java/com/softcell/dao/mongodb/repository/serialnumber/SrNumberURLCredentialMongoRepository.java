package com.softcell.dao.mongodb.repository.serialnumber;

import com.softcell.config.SerialNumberAuthsCredential;
import com.softcell.config.SerialNumberURLConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SrNumberURLCredentialMongoRepository implements
        SrNumberURLCreditialRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public SrNumberURLCredentialMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public boolean saveCredentials(
            SerialNumberAuthsCredential serialNumberAuthsCredential) {

        try {
            /*if (!mongoTemplate
                    .collectionExists(SerialNumberAuthsCredential.class)) {
                mongoTemplate
                        .createCollection(SerialNumberAuthsCredential.class);
            }*/
            mongoTemplate.insert(serialNumberAuthsCredential);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Map<String, SerialNumberURLConfiguration> loadSrNumberConfiguaration() {

        SerialNumberURLConfiguration serialNumberURLConfiguration = new SerialNumberURLConfiguration();
        Map<String, SerialNumberAuthsCredential> companySerialNumberAuthMap = new HashMap<String, SerialNumberAuthsCredential>();
        Map<String, SerialNumberURLConfiguration> configMap = new HashMap<String, SerialNumberURLConfiguration>();
        SerialNumberAuthsCredential serialNumberAuthsCredential = new SerialNumberAuthsCredential();

        try {
            //companySerialNumberAuthMap.put(key, )
            //serialNumberURLConfiguration.setCompanySerialNumberAuthMap(companySerialNumberAuthMap);
            configMap.put("", serialNumberURLConfiguration);


        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }


        return null;
    }

    @Override
    public boolean deleteCredentials(
            SerialNumberAuthsCredential serialNumberAuthsCredential) {

        return false;
    }

}
