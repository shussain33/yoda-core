/**
 * vinodk2:07:17 pm  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.masters.*;

/**
 * @author vinodk
 *
 */
public interface MetaDataMasterRepository {

    /**
     *
     * @param accountType
     */
    public void saveAccountType(AccountType accountType);

    /**
     *
     * @param applicantTypeMaster
     */
    public void saveApplicantTypeMaster(ApplicantTypeMaster applicantTypeMaster);

    /**
     *
     * @param assetsOwned
     */
    public void saveAssetsOwned(AssetsOwned assetsOwned);

    /**
     *
     * @param businessTypeMaster
     */
    public void saveBusinessTypeMaster(BusinessTypeMaster businessTypeMaster);

    /**
     *
     * @param companyTypeMaster
     */
    public void saveCompanyTypeMaster(CompanyTypeMaster companyTypeMaster);

    /**
     *
     * @param documentMaster
     */
    public void saveDocumentMaster(DocumentMaster documentMaster);

    /**
     *
     * @param educationMaster
     */
    public void saveEducationMaster(EducationMaster educationMaster);

    /**
     *
     * @param gender
     */
    public void saveGender(Gender gender);

    /**
     *
     * @param identitiesMaster
     */
    public void saveIdentitiesMaster(IdentitiesMaster identitiesMaster);

    /**
     *
     * @param industryCodeMaster
     */
    public void saveIndustryCodeMaster(IndustryCodeMaster industryCodeMaster);

    /**
     *
     * @param investmentTypeMaster
     */
    public void saveInvestmentTypeMaster(
            InvestmentTypeMaster investmentTypeMaster);

    /**
     *
     * @param loanPurposeMaster
     */
    public void saveLoanPurposeMaster(LoanPurposeMaster loanPurposeMaster);

    /**
     *
     * @param otherSourceOfIncome
     */
    public void saveOtherSourceOfIncome(OtherSourceOfIncome otherSourceOfIncome);

    /**
     *
     * @param productsMaster
     */
    public void saveProductsMaster(ProductsMaster productsMaster);

    /**
     *
     * @param professionsMaster
     */
    public void saveProfessionsMaster(ProfessionsMaster professionsMaster);

    /**
     *
     * @param propertyClassificationMaster
     */
    public void savePropertyClassificationMaster(
            PropertyClassificationMaster propertyClassificationMaster);

    /**
     *
     * @param propertyTypeMaster
     */
    public void savePropertyTypeMaster(PropertyTypeMaster propertyTypeMaster);

    /**
     *
     * @param residenceTypeMaster
     */
    public void saveResidenceTypeMaster(ResidenceTypeMaster residenceTypeMaster);

    /**
     *
     * @param statesMaster
     */
    public void saveStatesMaster(StatesMaster statesMaster);

    /**
     * @param institutionId
     * @return
     */
    Master getMastersPL(String institutionId);


}
