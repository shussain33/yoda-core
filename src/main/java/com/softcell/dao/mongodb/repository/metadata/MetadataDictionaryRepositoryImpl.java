package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.dao.mongodb.repository.MetadataRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.metadata.MetadataEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * implementation of metadata dictionary repository {@link MetadataRepository}
 * with basic CRUD Operation performed on entity {@link MetadataEntity}
 * Created by prateek on 10/3/17.
 */
@Repository
public class MetadataDictionaryRepositoryImpl implements MetadataRepository {

    private static final Logger logger = LoggerFactory.getLogger(MetadataDictionaryRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;


    private static final String COLLECTION = "gng_metadata_dict";

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @return
     * @throws SystemException
     */
    @Override
    public Collection<String> findTypeValues(String institutionId, String type, String product) throws SystemException {

        MetadataEntity byInstProdTyp = findByInstProdTyp(institutionId, product, type);

        if (null != byInstProdTyp && !byInstProdTyp.getTransformedValue().isEmpty()) {
            return byInstProdTyp.getTransformedValue();
        } else if(null != byInstProdTyp) {
            return byInstProdTyp.getValues();
        }else {
            return Collections.EMPTY_LIST;
        }
    }

    private MetadataEntity findByInstProdTyp(String institutionId , String product, String type){

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        MetadataEntity metadataEntity = null;


        try{

            metadataEntity = mongoTemplate.findOne(query, MetadataEntity.class);

        }catch (DataAccessException e){
            e.printStackTrace();
            logger.error("error occurred while fetching details of MetadataEntity with probable cause [{}]" , e.getMessage());
        }

        return metadataEntity;

    }

    /**
     *
     * @param institutionId
     * @param product
     * @return
     */
    @Override
    public Optional<Collection<MetadataEntity>> findAllByInstitutionNProduct(String institutionId, String product) {

        Query query = new Query();

        query.addCriteria(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId)
                        .and(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true)
                        .and(MetadataEntity.MetadataEntityConst.PRODUCT).is(product));


        List<MetadataEntity> metadataEntities = mongoTemplate.find(query, MetadataEntity.class);

        return Optional.ofNullable(metadataEntities);

    }

    /**
     *
     * @param institutionId
     * @return
     */
    @Override
    public Optional<Collection<MetadataEntity>> findAllByInstitution(String institutionId) {

        Query query = new Query();

        query.addCriteria(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId)
                        .and(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true));


        List<MetadataEntity> metadataEntities = mongoTemplate.find(query, MetadataEntity.class);

        return Optional.ofNullable(metadataEntities);
    }

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @throws SystemException
     */
    @Override
    public void createTypeValues(String institutionId, String type, String product, Collection<String> values) throws SystemException {


        try {

            MetadataEntity metadataEntity = findByInstProdTyp(institutionId, product, type);

            if(null == metadataEntity){

                MetadataEntity entity = MetadataEntity.builder()
                        .institutionId(institutionId)
                        .isEnabled(true)
                        .product(product)
                        .type(type)
                        .values(values)
                        .transformedValue(Collections.emptySet())
                        .build();

                mongoTemplate.save(entity,COLLECTION);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occurred while saving metadata dictionary of type [{}] with probable cause [{}] ", type, e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @throws SystemException
     */
    @Override
    public void replaceTypeValues(String institutionId, String type, String product, Collection<String> values) throws SystemException {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        Update update = new Update();
        update.set(MetadataEntity.MetadataEntityConst.VALUES,values);

        mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true),
                MetadataEntity.class);


    }

    /**
     * method used to rename values for type {@link MetadataEntity#type}
     * based on institutionId {@link MetadataEntity#institutionId}
     * and product {@link MetadataEntity#product}
     * a map contains a key-value  pair where key represent previously inserted values
     * and value represent updated value , if key is present it will update its value
     * if key is not found it will inserted the new value in {@link MetadataEntity#values}
     *
     * @param institutionId
     * @param type
     * @param product
     * @param updatedFields
     * @throws SystemException
     */
    @Override
    public MetadataEntity renameTypeValues(String institutionId, String type, String product, Map<String, String > updatedFields) throws SystemException {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        Update update = new Update();

        MetadataEntity metadataEntity = null;
        try{

            update.pullAll(MetadataEntity.MetadataEntityConst.VALUES,updatedFields.keySet().toArray(new Object[updatedFields.keySet().size()]));

            mongoTemplate.findAndModify(query,update, MetadataEntity.class);

            update = new Update();

            update.push(MetadataEntity.MetadataEntityConst.VALUES).each(updatedFields.values().toArray(new Object[updatedFields.values().size()]));

            metadataEntity = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), MetadataEntity.class);


        }catch (DataAccessException e){
            e.printStackTrace();
            logger.error("error occurred while updating values of metadata for institutionId [{}] product [{}] type [{}] with probable cause [{}]", institutionId, product, type, e.getMessage());
        }

        return metadataEntity;

    }

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @return
     * @throws SystemException
     */
    @Override
    public MetadataEntity deleteType(String institutionId, String type, String product) throws SystemException {
        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        return mongoTemplate.findAndRemove(query, MetadataEntity.class);
    }

    /**
     *  method used to insert a blank set for values for defined type+product+institutionId
     *  combination
     * @param institutionId
     * @param type
     * @param product
     * @return
     * @throws SystemException
     */
    @Override
    public MetadataEntity factoryDefaultTypeValues(String institutionId, String type, String product) throws SystemException {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        Update update = new Update();

        update.set("values", Collections.EMPTY_SET);

        return mongoTemplate.findAndModify(query,update,new FindAndModifyOptions().returnNew(true),MetadataEntity.class);
    }

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @return
     * @throws SystemException
     */
    @Override
    public MetadataEntity removeTypeValues(String institutionId, String type, String product, Collection<String> values) throws SystemException {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        Update update = new Update();

        update.pullAll("values",values.toArray(new Object[values.size()]));

        return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), MetadataEntity.class);

    }

    @Override
    public void disableTypeValue(String institutionId, String type, String product) throws SystemException {

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(MetadataEntity.MetadataEntityConst.INSTITUTIONID).is(institutionId),
                Criteria.where(MetadataEntity.MetadataEntityConst.PRODUCT).is(product),
                Criteria.where(MetadataEntity.MetadataEntityConst.IS_ENABLED).is(true),
                Criteria.where(MetadataEntity.MetadataEntityConst.TYPE).is(type)
        ));

        Update update = new Update();

        update.set("isEnabled", false);

        mongoTemplate.findAndModify(query,update,new FindAndModifyOptions().returnNew(true), MetadataEntity.class);

    }
}
