package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.masters.*;

import java.util.List;

/**
 * This Repository contains database operatation regarding master upload , Dont
 * Use this Repository for other purpose.
 *
 * @author yogeshb
 */
public interface FileUploadRepository {

    /**
     * This method use to bulk insert schemeMaster data
     *
     * @param schemeMasterList
     * @return success or failure of database operation.
     */
    public boolean insertSchemeMaster(List<SchemeMasterData> schemeMasterList);

    /**
     * This method use to bulk insert AssetModelMaster data
     *
     * @param assetModelMasterList
     * @return
     */
    public boolean insertAssetModelMasterList(
            List<AssetModelMaster> assetModelMasterList);

    /**
     * This method use to bulk insert PinCodeMaster data
     *
     * @param pinCodeMasterList
     * @return
     */
    public boolean insertPinCodeMasterList(List<PinCodeMaster> pinCodeMasterList);

    /**
     * This method use to bulk insert EmployerMaster data
     *
     * @param employerMasterList
     * @return
     */
    public boolean insertEmployerMasterList(
            List<EmployerMaster> employerMasterList);

    /**
     * This method use to bulk insert DealerEmailMaster data
     *
     * @param dealerEmailMasterList
     * @return
     */
    public boolean insertDealerEmailMasterList(
            List<DealerEmailMaster> dealerEmailMasterList);

    /**
     * This method use to bulk insert GNGDealerEmailMaster data
     *
     * @param gngDealerEmailMasterList
     * @return
     */
    public boolean insertGNGDealerEmailMasterList(
            List<GNGDealerEmailMaster> gngDealerEmailMasterList);

    /**
     * This method use to bulk insert SchemeModelDealerCityMapping data
     *
     * @param schemeModelDealerCityMappingMasterList
     * @return
     */
    public boolean insertSchemeModelDealerCityMappingMasterList(
            List<SchemeModelDealerCityMapping> schemeModelDealerCityMappingMasterList);

    /**
     * This method use to bulk insert SchemeDateMapping data
     *
     * @param schemeDateMappingList
     * @return
     */
    public boolean insertSchemeDateMappingMasterList(
            List<SchemeDateMapping> schemeDateMappingList);

    /**
     * This method use to bulk insert HitachiSchemeMaster data
     *
     * @param hitachiSchemeMasterList
     * @return
     */
    public boolean insertHitachiSchemeMasterList(
            List<HitachiSchemeMaster> hitachiSchemeMasterList);

    /**
     * This method use to bulk insert CarSurrogateMaster data
     *
     * @param carSurrogateMasterList
     * @return
     */
    public boolean insertCarSurrogateMasterList(
            List<CarSurrogateMaster> carSurrogateMasterList);

    /**
     * @param plPincodeEmailMasterList
     * @return
     */
    public boolean insertPlPincodeEmailMasterList(
            List<PlPincodeEmailMaster> plPincodeEmailMasterList);

    /**
     *
     * @param cityStateMasterList
     * @return
     */
    public boolean insertCityStateMasterList(
            List<CityStateMappingMaster> cityStateMasterList);

    /**
     * This method is used to bulk insert bankDetails
     *
     * @param bankDetailsMasterList
     * @return
     */
    boolean insertBankDetailsMasterList(
            List<BankDetailsMaster> bankDetailsMasterList);

    /**
     * This method is used to bulk insert user Hierarchy details
     * @param hierarchyMasterList
     * @return
     */
    boolean insertHierarchyMasterList(List<HierarchyMaster> hierarchyMasterList);

    /**
     *
     * @param hierarchyMasterList
     * @return
     */
    boolean insertCDLHierarchyMasterList(
            List<CDLHierarchyMaster> hierarchyMasterList);

    /**
     * @param modelVariantMasterList
     * @return
     */
    boolean insertModelVariantMasterList(
            List<ModelVariantMaster> modelVariantMasterList);


    boolean insertAssetCategoryMasterList(
            List<AssetCategoryMaster> modelVariantMasterList);

    public boolean insertStateMasterList(List<StateMaster> stateMasterList);

    public boolean insertCityMasterList(List<CityMaster> cityMasterList);

    public boolean insertCreditPromoMasterList(
            List<CreditPromotionMaster> creditPromoMasterList);

    public boolean insertDsaBranchMasterList(
            List<DSABranchMaster> dsaBranchMasterList);

    public boolean insertDsaProductMasterList(
            List<DSAProductMaster> dsaProductMasterList);

    public boolean insertDealerBranchMasterList(
            List<DealerBranchMaster> dealerBranchMasterList);
    /**
     *
     * @param negativeAreaGeoLimitMasterList
     * @return
     */
	public boolean insertNegativeAreaFundingMaster(
			List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasterList);

    /**
     *
     * @param losUtrDetailsMasterRequest
     * @return
     */
    boolean updateLosDetails(LosUtrDetailsMaster losUtrDetailsMasterRequest);

    /**
     *
     * @param losUtrDetailsMasterRequest
     * @return
     */
    boolean insertLosMasterDetails(LosUtrDetailsMaster losUtrDetailsMasterRequest);

    /**
     *
     * @param dealerBranchMasterList
     * @return
     */
    boolean insertRelianceDealerBranchMasterList(List<RelianceDealerBranchMaster> dealerBranchMasterList);

    /**
     * @param referenceDetailsMasters
     * @return
     */
    boolean insertReferenceDetailsMasters(List<ReferenceDetailsMaster> referenceDetailsMasters);

    /**
     *
     * @param bankDetailsMasterList
     * @return
     */
    boolean insertBankingDetailsMasterList(List<BankDetailsMaster> bankDetailsMasterList);

    /**
     *
     * @param loyaltyCardMastersList
     * @return
     */
    boolean insertLoyaltyCardMasterList(List<LoyaltyCardMaster> loyaltyCardMastersList);

    /**
     *
     * @param commonGeneralParameterMasterList
     * @return
     */
    boolean insertCommonGeneralMaster(List<CommonGeneralParameterMaster> commonGeneralParameterMasterList);


    /**
     *
     * @param losBankMasters
     * @return
     */
    boolean insertLosBankMasterList(List<LosBankMaster> losBankMasters);

    /**
     *
     * @param ewInsGngAssetCategoryMasterList
     * @return
     */
    boolean insertEwInsGngAssetCategoryMaster(List<EwInsGngAssetCategoryMaster> ewInsGngAssetCategoryMasterList);

    /**
     *
     * @param ewPremiumMasterList
     * @return
     */
    boolean insertEwPremiumDataMaster(List<EWPremiumMaster> ewPremiumMasterList);

    /**
     *
     * @param insurancePremiumMasterList
     * @return
     */
    boolean insertInsurancePremiumMaster(List<InsurancePremiumMaster> insurancePremiumMasterList);

    /**
     * for uploading ElecServProvMaster
     * @param elecServProvMasterList
     * @return
     */
    boolean insertElecServProvMaster(List<ElecServProvMaster> elecServProvMasterList);


    boolean insertBranchMasterList(List<BranchMaster> branchMasterList);

    /**
     *
     * @param sourcingDetailMasterList
     * @return
     */
    boolean insertSourcingDetailMaster(List<SourcingDetailMaster> sourcingDetailMasterList);

    /**
     *
     * @param dealerBranchMasterList
     * @return
     */
    boolean insertTkilDealerBranchMasterList(List<TkilDealerBranchMaster> dealerBranchMasterList);


    /**
     *
     * @param promotionSchemeMasterList
     * @return
     */
    boolean insertPromotionSchemeMaster(List<PromotionalSchemeMaster> promotionSchemeMasterList);

    boolean insertTaxCodeMaster(List<TaxCodeMaster> taxCodeMasterList);

    boolean insertAccountsHeadMaster(List<AccountsHeadMaster> accountsHeadMasterList);

    boolean insertStateBranchMaster(List<StateBranchMaster> stateBranchMasterList);

    /**
     *
     * @param bankMasterList
     * @return
     */
    boolean insertBankMaster(List<BankMaster> bankMasterList);

    boolean insertTCLosGngMappingMaster(List<TCLosGngMappingMaster> tcLosGngMappings);

    boolean insertManufacturers(List<ManufacturerMaster> manufacturers);

    /**
     *
     * @param netDisbursalDetailsMaster
     * @return
     */
    boolean updateNetDisbursalAmount(NetDisbursalDetailsMaster netDisbursalDetailsMaster);

    /**
     *
     * @param netDisbursalDetailsRequest
     * @return
     */
    boolean insertNetDisbursalDetails(NetDisbursalDetailsMaster netDisbursalDetailsRequest);


    boolean insertSupplierLocationMaster(List<SupplierLocationMaster> supplierLocationMasterList);

    /**
     *
     * @param paynimoBankMasterList
     * @return
     */
    boolean insertPaynimoBankMasterList(List<PaynimoBankMaster> paynimoBankMasterList);

    /**
     *
     * @param digitizationEmailMaster
     * @return
     */
    boolean insertDigitizationEmailMaster(List<DigitizationDealerEmailMaster> digitizationEmailMaster);

    boolean insertDeviationMaster(List<DeviationMaster> deviationMasters);

    boolean insertOrganizationalHierarchyMaster(List<OrganizationalHierarchyMaster> organizationalHierarchyMasterList,
                                                String institutionId);

    boolean insertRoiSchemeMaster(List<RoiSchemeMaster> roiSchemeMasters, String institutionId, String product);

    boolean insertApiRoleAuthorisationMaster(List<ApiRoleAuthorisationMaster> apiRoleAuthorisationMasters,String institutionId,String sourceId);
}
