package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.request.CroApprovalRequest;
import com.softcell.gonogo.model.salesforce.SalesForceInfo;

public interface SalesForceRepository {

    /**
     * @param goNoGoCustomerApplication
     */

    public void connectSalesForce(GoNoGoCustomerApplication goNoGoCustomerApplication);


    /**
     * @param refId reference id of the application saved in the database.
     */
    public SalesForceInfo getByApplicationRefId(String refId);

    /**
     * @param newUrl             url to be updated in amazon S3.
     * @param leadId             sales force lead id
     * @param applicationRequest GonoGo ApplicationRequest.
     * @return
     */
    boolean updateAmazonS3UrlInSalesForce(String newUrl, String leadId,
                                          String institutionId, boolean isProfilePic,
                                          UploadFileDetails uploadDetails, SalesForceInfo salesForceInfo,
                                          String applicantId);

    /**
     * @param croApprovalRequest
     * @return true if updated successfully
     */
    boolean updateCroDecision(CroApprovalRequest croApprovalRequest) throws Exception;
}
