package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;

import java.util.List;

/**
 * @author bhuvneshk
 */
public interface ApplicationTrackingRepository {

    /**
     *
     * @param applicationTracking
     */
    void saveApplicationTrackingObj(ApplicationTracking applicationTracking) throws Exception;

    /**
     * @param institutionId Institution ID used for slice data by institution.
     * @param refId         This will used for application ID.
     * @param product       Product to filter the event.
     * @return Application Case history events with Stage and Step Calculation.
     */
    List<ApplicationTracking> getApplicationCaseHistory(String institutionId, String refId, String product) throws Exception;

    /**
     *
     *
     * @param configuration@return
     */
    List<ApplicationTracking> getApplicationHistory(ReportingModuleConfiguration configuration) throws Exception;

}
