package com.softcell.dao.mongodb.repository;

import com.mongodb.WriteResult;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mahesh on 30/8/17.
 */
@Repository
public class DoOperationMongoRepository implements DoOperationRepository {

    private static final Logger logger = LoggerFactory.getLogger(DMSMongoRepository.class);
    @Autowired
    GridFsTemplate gridFsTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public boolean updateDoStatusInPostIpa(String refID, String institutionId, String doStatus) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID).and("header.institutionId").is(institutionId));

            Update update = new Update();

            update.set("postIPA.deliveryOrderStatus", doStatus);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, PostIpaRequest.class);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occur at the time of updating Do status in postIpa with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of updating Do status in postIpa with probable cause [{}] ", e.getMessage()));

        }

    }


    @Override
    public void saveDoOperationLog(DoOperationLog doOperationLog) {

        try {
            mongoTemplate.insert(doOperationLog);

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occur at the time of saving DoOperationLog probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of saving DoOperationLog probable cause [{}] ", e.getMessage()));

        }

    }


    @Override
    public List<DoOperationLog> getDoOperationLog(String refId, String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(refId)
                    .and("header.institutionId").is(institutionId));
            query.with(new Sort(Sort.Direction.ASC, "dateTime"));

            return mongoTemplate.find(query, DoOperationLog.class);

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occur at the time of getting DoOperationLog probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of getting DoOperationLog probable cause [{}] ", e.getMessage()));

        }
    }
}
