package com.softcell.dao.mongodb.repository;

import com.softcell.constants.AuditType;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.workflow.component.finished.AuditData;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author vinodk
 */
public interface AuditRepository {

    /**
     * @param refId         ref id.
     * @param institutionId id of the institute
     * @param auditType     @AuditType
     * @return @List<@AuditData>
     */
    List<AuditData> getAuditData(String refId,
                                 String institutionId, AuditType auditType, String auditCollectionName) throws Exception;

    }
