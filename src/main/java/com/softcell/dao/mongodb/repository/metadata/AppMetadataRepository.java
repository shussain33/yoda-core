package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.gonogo.model.configuration.ActionConfiguration;

import java.util.List;

/**
 * This dao layer will be used to do CRUD operations
 * related to actions meta data
 *
 * @author bhuvneshk
 */
public interface AppMetadataRepository {

    void insertActionConfiguration(ActionConfiguration actionConfiguration);

    Boolean updateActionConfiguration(ActionConfiguration actionConfiguration);

    Boolean deleteActionConfiguration(ActionConfiguration actionConfiguration);

    List<String> readActionByInstitution(String institutionId);

    List<String> readActiveActionByInstitution(String institutionId);

    boolean isActionConfigExist(ActionConfiguration actionConfiguration);

    List<ActionConfiguration> getAllActionConfiguration(String institutionId, String productId ,boolean active);
}
