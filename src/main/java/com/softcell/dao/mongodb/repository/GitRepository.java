package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.git.GitRepositoryState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by prateek on 13/3/17.
 */
@Repository
public class GitRepository {

    private static final Logger logger = LoggerFactory.getLogger(GitRepository.class);

    private static final String GIT_PATH = "git.properties";

    private Properties properties = new Properties();

    private GitRepositoryState gitRepositoryState;

    public GitRepositoryState getGitRepositoryState() {

        if (null == gitRepositoryState) {

            try {

                properties.load(getClass().getClassLoader().getResourceAsStream(GIT_PATH));

            } catch (IOException e) {
                e.printStackTrace();
                logger.error("Error occurred while setting git repository status with probable cause [{}] ", e.getMessage());
            }


            gitRepositoryState = new GitRepositoryState(properties);
        }

        return gitRepositoryState;

    }


}
