package com.softcell.dao.mongodb.repository.imps;

import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.gonogo.model.request.imps.IMPSRequest;

import java.util.List;

/**
 * Created by sampat on 6/11/17.
 */
public interface IMPSRepository {

    /**
     * @param institutionId
     * @param refID
     */
    long getAccountValidationCount(String institutionId, String refID);

    /**
     * @param institutionId
     */
    int getAccountValidationAttempt(String institutionId);

    /**
     * @param institutionId
     * @param refID
     */
    List<AccountNumberInfo> getAccountValidatedBanks(String institutionId, String refID);

    /**
     * @param accountNumberInfo
     */
    void saveAccountNumberInfo(AccountNumberInfo accountNumberInfo);

    /**
     * @param attemptId
     * @param institutionId
     */
    boolean checkAttemptStatus(String attemptId,String institutionId);

    /**
     * @param impsRequest
     */
    boolean checkAccountNumberInDedup(IMPSRequest impsRequest);

    /**
     * @param institutionId
     * @param refID
     */
    boolean updateAccountNumberValidStatus(String institutionId, String refID,BankingDetails bankingDetails);

    /**
     * @param identifier
     * @param institutionId
     * @param refID
     */
    boolean checkAccountDetailsStatus(String identifier, String institutionId, String refID);

    /**
     * @param institutionId
     * @param bankName
     */
    boolean checkBankInPaynimoBankMaster(String institutionId, String bankName);

    /**
     * @param bankingDetails
     * @param institutionId
     * @param refID
     */
    void saveBankingDetails(BankingDetails bankingDetails, String institutionId, String refID);
}
