package com.softcell.dao.mongodb.repository.workflow;

import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.PlPincodeEmailMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yogeshb
 */
@Repository
public class WorkFlowMongoRepository implements WorkFlowRepository {

    private static final Logger logger = LoggerFactory.getLogger(WorkFlowMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public WorkFlowMongoRepository(MongoTemplate mongoTemplate){
        if(this.mongoTemplate==null){
            this.mongoTemplate=mongoTemplate;
        }
    }



    @Override
    public List<PlPincodeEmailMaster> getPincodeEmailMaster(String pincode) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("pincode").is(pincode));

            return mongoTemplate.find(query, PlPincodeEmailMaster.class);

        } catch (Exception e) {

            logger.error("Exception in fetch data from PlPincodeEmailMaster with error {} ", e);

            return null;
        }
    }

    @Override
    public boolean saveActivityLog(ActivityLogs activityLogs) {
        try {

            mongoTemplate.insert(activityLogs);

            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception while saving activity log with error {}", e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public boolean saveMailLog(EmailTemplateDetails emailTemplateDetails) {
        try {

            mongoTemplate.insert(emailTemplateDetails);
            return true;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception while saving mail log with error {}", e.getLocalizedMessage());
            return false;
        }
    }
}
