/**
 * vinodk2:07:30 pm  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.masters.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vinodk
 */
@Repository
public class MetaDataMasterMongoRepository implements MetaDataMasterRepository {

    private static final Logger logger = LoggerFactory.getLogger(MetaDataMasterMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public void saveAccountType(AccountType accountType) {
        try {
            mongoTemplate.insert(accountType);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting AccountType with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting AccountType with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public void saveApplicantTypeMaster(ApplicantTypeMaster applicantTypeMaster) {
        try {
            mongoTemplate.insert(applicantTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ApplicantTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting ApplicantTypeMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveAssetsOwned(AssetsOwned assetsOwned) {
        try {
            mongoTemplate.insert(assetsOwned);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting AssetsOwned with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting AssetsOwned with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveBusinessTypeMaster(BusinessTypeMaster businessTypeMaster) {
        try {
            mongoTemplate.insert(businessTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting BusinessTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting BusinessTypeMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveCompanyTypeMaster(CompanyTypeMaster companyTypeMaster) {
        try {
            mongoTemplate.insert(companyTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting CompanyTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting CompanyTypeMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveDocumentMaster(DocumentMaster documentMaster) {
        try {
            mongoTemplate.insert(documentMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting DocumentMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting DocumentMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveEducationMaster(EducationMaster educationMaster) {
        try {
            mongoTemplate.insert(educationMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting EducationMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting EducationMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveGender(Gender gender) {
        try {
            mongoTemplate.insert(gender);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting Gender with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting Gender with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveIdentitiesMaster(IdentitiesMaster identitiesMaster) {
        try {
            mongoTemplate.insert(identitiesMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting IdentitiesMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting IdentitiesMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveIndustryCodeMaster(IndustryCodeMaster industryCodeMaster) {
        try {
            mongoTemplate.insert(industryCodeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting IndustryCodeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting IndustryCodeMaster with probable cause [{%s}]", e.getMessage()));

        }
    }


    @Override
    public void saveInvestmentTypeMaster(
            InvestmentTypeMaster investmentTypeMaster) {
        try {
            mongoTemplate.insert(investmentTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting InvestmentTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting InvestmentTypeMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveLoanPurposeMaster(LoanPurposeMaster loanPurposeMaster) {
        try {
            mongoTemplate.insert(loanPurposeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting LoanPurposeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting LoanPurposeMaster with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public void saveOtherSourceOfIncome(OtherSourceOfIncome otherSourceOfIncome) {
        try {
            mongoTemplate.insert(otherSourceOfIncome);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting OtherSourceOfIncome with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting OtherSourceOfIncome with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveProductsMaster(ProductsMaster productsMaster) {
        try {
            mongoTemplate.insert(productsMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ProductsMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting ProductsMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveProfessionsMaster(ProfessionsMaster professionsMaster) {
        try {
            mongoTemplate.insert(professionsMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ProfessionsMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting ProfessionsMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void savePropertyClassificationMaster(
            PropertyClassificationMaster propertyClassificationMaster) {
        try {
            mongoTemplate.insert(propertyClassificationMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting PropertyClassificationMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting PropertyClassificationMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void savePropertyTypeMaster(PropertyTypeMaster propertyTypeMaster) {
        try {
            mongoTemplate.insert(propertyTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting PropertyTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting PropertyTypeMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveResidenceTypeMaster(ResidenceTypeMaster residenceTypeMaster) {
        try {

            mongoTemplate.insert(residenceTypeMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ResidenceTypeMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting ResidenceTypeMaster with probable cause [{%s}]", e.getMessage()));
        }

    }


    @Override
    public void saveStatesMaster(StatesMaster statesMaster) {
        try {
            mongoTemplate.insert(statesMaster);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting StatesMaster with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting StatesMaster with probable cause [{%s}]", e.getMessage()));

        }

    }


    @Override
    public Master getMastersPL(String institutionId) {
        Master master = new Master();
        master.setAccountType(getAccountType(institutionId));
        master.setApplicantTypeMaster(getApplicantTypeMaster(institutionId));
        master.setAssetsOwned(getAssetsOwned(institutionId));
        master.setBusinessTypeMaster(getBusinessTypeMaster(institutionId));
        master.setCompanyTypeMaster(getCompanyTypeMaster(institutionId));
        master.setDocumentMaster(getDocumentMaster(institutionId));
        master.setEducationMaster(getEducationMaster(institutionId));
        master.setGender(getGender(institutionId));
        master.setIdentitiesMaster(getIdentitiesMaster(institutionId));
        master.setIndustryCodeMaster(getIndustryCodeMaster(institutionId));
        master.setInvestmentTypeMaster(getInvestmentTypeMaster(institutionId));
        master.setLoanPurposeMaster(getLoanPurposeMaster(institutionId));
        master.setOtherSourceOfIncome(getOtherSourceOfIncome(institutionId));
        master.setProductsMaster(getProductsMaster(institutionId));
        master.setProfessionsMaster(getProfessionsMaster(institutionId));
        master.setPropertyTypeMaster(getPropertyTypeMaster(institutionId));
        master.setPropertyClassificationMaster(getPropertyClassificationMaster(institutionId));
        master.setResidenceTypeMaster(getResidenceTypeMaster(institutionId));
        master.setStatesMaster(getStatesMaster(institutionId));
        return master;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<StatesMaster> getStatesMaster(String institutionId) {
        List<StatesMaster> stateMasterList = new ArrayList<StatesMaster>();


        StatesMaster stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Andhra Pradesh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Arunachal Pradesh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Assam");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Bihar");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Chhattisgarh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Goa");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Gujarat");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Haryana");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Himachal Pradesh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Jammu and Kashmir");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Jharkhand");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Karnataka");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Kerala");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Madhya Pradesh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Maharashtra");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Manipur");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Meghalaya");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Mizoram");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Nagaland");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Odisha");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Punjab");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Rajasthan");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Sikkim");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Tamil Nadu");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Telangana");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Tripura");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Uttar Pradesh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Uttarakhand");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("West Bengal");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Andaman and Nicobar Islands");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Chandigarh");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Dadra and Nagar Haveli");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Daman and Diu");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Lakshadweep");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("National Capital Territory of Delhi");
        stateMasterList.add(stateMaster);
        stateMaster = new StatesMaster();
        stateMaster.setStateDescription("Puducherry");
        stateMasterList.add(stateMaster);

        return stateMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<ResidenceTypeMaster> getResidenceTypeMaster(String institutionId) {
        List<ResidenceTypeMaster> residenceTypeMasterList = new ArrayList<ResidenceTypeMaster>();
        ResidenceTypeMaster residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster
                .setResidenceTypeDescription("Owned by Self/Spouse");
        residenceTypeMaster.setMonthlyRentRequired("NO");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster
                .setResidenceTypeDescription("owned by Parent/Sibling");
        residenceTypeMaster.setMonthlyRentRequired("NO");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster
                .setResidenceTypeDescription("Rented - with Family");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster
                .setResidenceTypeDescription("Rented - with Friends");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster
                .setResidenceTypeDescription("Rented - staying alone");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster.setResidenceTypeDescription("Paying guest");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster.setResidenceTypeDescription("Hostel");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster.setResidenceTypeDescription("Hotel");
        residenceTypeMaster.setMonthlyRentRequired("YES");
        residenceTypeMasterList.add(residenceTypeMaster);

        residenceTypeMaster = new ResidenceTypeMaster();
        residenceTypeMaster.setResidenceTypeDescription("Company Provided");
        residenceTypeMaster.setMonthlyRentRequired("NO");
        residenceTypeMasterList.add(residenceTypeMaster);

        return residenceTypeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<PropertyTypeMaster> getPropertyTypeMaster(String institutionId) {
        List<PropertyTypeMaster> propertyTypeMasterList = new ArrayList<PropertyTypeMaster>();

        PropertyTypeMaster propertyTypeMaster = new PropertyTypeMaster();
        propertyTypeMaster.setPropertyCodeDescription("Residential");
        propertyTypeMasterList.add(propertyTypeMaster);

        propertyTypeMaster = new PropertyTypeMaster();
        propertyTypeMaster.setPropertyCodeDescription("Commercial");
        propertyTypeMasterList.add(propertyTypeMaster);
        return propertyTypeMasterList;
    }


    /**
     * @param institutionId
     * @return
     */
    private List<PropertyClassificationMaster> getPropertyClassificationMaster(String institutionId) {

        List<PropertyClassificationMaster> propertyClassificationMasterList = new ArrayList<PropertyClassificationMaster>();

        PropertyClassificationMaster propertyClassificationMaster = new PropertyClassificationMaster();
        propertyClassificationMaster.setPropertyCodeDescription("Flat");
        propertyClassificationMasterList.add(propertyClassificationMaster);

        propertyClassificationMaster = new PropertyClassificationMaster();
        propertyClassificationMaster.setPropertyCodeDescription("Row House");
        propertyClassificationMasterList.add(propertyClassificationMaster);

        propertyClassificationMaster = new PropertyClassificationMaster();
        propertyClassificationMaster.setPropertyCodeDescription("Bungalow");
        propertyClassificationMasterList.add(propertyClassificationMaster);

        propertyClassificationMaster = new PropertyClassificationMaster();
        propertyClassificationMaster.setPropertyCodeDescription("Shop");
        propertyClassificationMasterList.add(propertyClassificationMaster);

        propertyClassificationMaster = new PropertyClassificationMaster();
        propertyClassificationMaster.setPropertyCodeDescription("Office");
        propertyClassificationMasterList.add(propertyClassificationMaster);

        return propertyClassificationMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<ProfessionsMaster> getProfessionsMaster(String institutionId) {

        List<ProfessionsMaster> professionsMasterList = new ArrayList<ProfessionsMaster>();

        ProfessionsMaster professionsMaster = new ProfessionsMaster();
        professionsMaster.setProfessionCodeDesc("Doctors");
        professionsMasterList.add(professionsMaster);

        professionsMaster = new ProfessionsMaster();
        professionsMaster.setProfessionCodeDesc("CA");
        professionsMasterList.add(professionsMaster);

        professionsMaster = new ProfessionsMaster();
        professionsMaster.setProfessionCodeDesc("CS");
        professionsMasterList.add(professionsMaster);

        professionsMaster = new ProfessionsMaster();
        professionsMaster.setProfessionCodeDesc("Architect");
        professionsMasterList.add(professionsMaster);

        professionsMaster = new ProfessionsMaster();
        professionsMaster.setProfessionCodeDesc("Others");
        professionsMasterList.add(professionsMaster);

        return professionsMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<ProductsMaster> getProductsMaster(String institutionId) {
        List<ProductsMaster> productsMasterList = new ArrayList<ProductsMaster>();
        ProductsMaster productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("NO");
        productsMaster.setProductCode("PL");
        productsMaster.setProductDescr("personal Loan");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("NO");
        productsMaster.setProductCode("BL");
        productsMaster.setProductDescr("Business Loan");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("EBL");
        productsMaster.setProductDescr("Enterprise Business Loan");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("MTGPPT");
        productsMaster.setProductDescr("Loan against Property");
        productsMasterList.add(productsMaster);


        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("MTGGLD");
        productsMaster.setProductDescr("Loan against Gold Jewellery");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("OLDCAR");
        productsMaster.setProductDescr("Pre-Owned Car Loan");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("NEWCAR");
        productsMaster.setProductDescr("New Car Loan");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("MTGCAR");
        productsMaster.setProductDescr("Loan against Car");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("MTGSHR");
        productsMaster.setProductDescr("Loan against shares");
        productsMasterList.add(productsMaster);

        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("MTGSEC");
        productsMaster.setProductDescr("Loan against Securities");
        productsMasterList.add(productsMaster);


        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("COMVEH");
        productsMaster.setProductDescr("Commercial Vehicle Loan");
        productsMasterList.add(productsMaster);


        productsMaster = new ProductsMaster();
        productsMaster.setMortgageRequired("YES");
        productsMaster.setProductCode("CONEL");
        productsMaster.setProductDescr("Construction Equipment Loan");
        productsMasterList.add(productsMaster);


        return productsMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<OtherSourceOfIncome> getOtherSourceOfIncome(String institutionId) {
        List<OtherSourceOfIncome> otherSourceOfIncomeList = new ArrayList<OtherSourceOfIncome>();

        OtherSourceOfIncome otherSourceOfIncome = new OtherSourceOfIncome();
        otherSourceOfIncome.setOtherSourceOfIncome("Rental");
        otherSourceOfIncomeList.add(otherSourceOfIncome);

        otherSourceOfIncome = new OtherSourceOfIncome();
        otherSourceOfIncome.setOtherSourceOfIncome("Dividend");
        otherSourceOfIncomeList.add(otherSourceOfIncome);

        otherSourceOfIncome = new OtherSourceOfIncome();
        otherSourceOfIncome.setOtherSourceOfIncome("Interest");
        otherSourceOfIncomeList.add(otherSourceOfIncome);

        otherSourceOfIncome = new OtherSourceOfIncome();
        otherSourceOfIncome.setOtherSourceOfIncome("Others");
        otherSourceOfIncomeList.add(otherSourceOfIncome);

        return otherSourceOfIncomeList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<LoanPurposeMaster> getLoanPurposeMaster(String institutionId) {
        List<LoanPurposeMaster> loanPurposeMasterList = new ArrayList<LoanPurposeMaster>();

        LoanPurposeMaster loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Business");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Education");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Marriage");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Acquisition");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Debt Consolidation");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Home Renovation");
        loanPurposeMasterList.add(loanPurposeMaster);
        loanPurposeMaster = new LoanPurposeMaster();
        loanPurposeMaster.setLoanPurposeDescription("Others");
        loanPurposeMasterList.add(loanPurposeMaster);

        return loanPurposeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<InvestmentTypeMaster> getInvestmentTypeMaster(String institutionId) {
        List<InvestmentTypeMaster> investmentTypeMasterList = new ArrayList<InvestmentTypeMaster>();

        InvestmentTypeMaster investmentTypeMaster = new InvestmentTypeMaster();
        investmentTypeMaster.setInvestmentTypeDescription("Shares");
        investmentTypeMasterList.add(investmentTypeMaster);

        investmentTypeMaster = new InvestmentTypeMaster();
        investmentTypeMaster.setInvestmentTypeDescription("Mutual Fund Units");
        investmentTypeMasterList.add(investmentTypeMaster);

        investmentTypeMaster = new InvestmentTypeMaster();
        investmentTypeMaster.setInvestmentTypeDescription("Government Bonds");
        investmentTypeMasterList.add(investmentTypeMaster);

        investmentTypeMaster = new InvestmentTypeMaster();
        investmentTypeMaster.setInvestmentTypeDescription("Others");
        investmentTypeMasterList.add(investmentTypeMaster);

        return investmentTypeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<IndustryCodeMaster> getIndustryCodeMaster(String institutionId) {
        List<IndustryCodeMaster> industryCodeMasteListr = new ArrayList<IndustryCodeMaster>();

        IndustryCodeMaster industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Engineering Products");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Doctor");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Account consulting");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Agriculture");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("IT & Software");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Media & Entertainment");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Automobiles");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Banking");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Hotel & Hospitality");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Education");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Architect");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Pharma & Medicine");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Cloth & Textiles");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Insurance Civil Contractor");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Real Estate & Builder");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Freight & Logistics");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Contractor/Franchisee");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Advertising & Promotion");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Chemicals");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Jewellery & Ornaments");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Iron & Steel");
        industryCodeMasteListr.add(industryCodeMaster);

        industryCodeMaster = new IndustryCodeMaster();
        industryCodeMaster.setIndustryTypeDescription("Others");
        industryCodeMasteListr.add(industryCodeMaster);

        return industryCodeMasteListr;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<IdentitiesMaster> getIdentitiesMaster(String institutionId) {
        List<IdentitiesMaster> identitiesMasterList = new ArrayList<IdentitiesMaster>();

        IdentitiesMaster identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("PASSPORT");
        identitiesMaster.setIdTypeDescription("Passport");
        identitiesMaster.setCheckExpiry("YES");
        identitiesMaster.setIssuingAuthority("Ministry of External Affairs");
        identitiesMaster.setCheckExpiry("ID, Address Proof");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("Aadhar");
        identitiesMaster.setIdTypeDescription("Aadhar ID");
        identitiesMaster.setCheckExpiry("NO");
        identitiesMaster.setIssuingAuthority("Government of India");
        identitiesMaster.setCheckExpiry("ID, Address Proof");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("VOTER");
        identitiesMaster.setIdTypeDescription("Voter Card");
        identitiesMaster.setCheckExpiry("NO");
        identitiesMaster.setIssuingAuthority("Election Commission Of India");
        identitiesMaster.setCheckExpiry("ID, Address Proof");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("DL");
        identitiesMaster.setIdTypeDescription("Driving License");
        identitiesMaster.setCheckExpiry("YES");
        identitiesMaster.setIssuingAuthority("");
        identitiesMaster.setCheckExpiry("ID, Address Proof");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("PAN");
        identitiesMaster.setIdTypeDescription("PAN Card");
        identitiesMaster.setCheckExpiry("NO");
        identitiesMaster.setIssuingAuthority("Income Tax Department");
        identitiesMaster.setCheckExpiry("ID");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("GASBILL");
        identitiesMaster.setIdTypeDescription("Gas Bill");
        identitiesMaster.setCheckExpiry("NO");
        identitiesMaster.setIssuingAuthority("");
        identitiesMaster.setCheckExpiry("Address Proof");
        identitiesMasterList.add(identitiesMaster);

        identitiesMaster = new IdentitiesMaster();
        identitiesMaster.setIdTypeCode("PWRBILL");
        identitiesMaster.setIdTypeDescription("Electricity Bill");
        identitiesMaster.setCheckExpiry("NO");
        identitiesMaster.setIssuingAuthority("");
        identitiesMaster.setCheckExpiry("Address Proof");
        identitiesMasterList.add(identitiesMaster);


        return identitiesMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<Gender> getGender(String institutionId) {
        List<Gender> genderList = new ArrayList<Gender>();
        Gender gender = new Gender();
        gender.setGender("Male");
        genderList.add(gender);
        gender = new Gender();
        gender.setGender("Female");
        genderList.add(gender);
        return genderList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<EducationMaster> getEducationMaster(String institutionId) {
        List<EducationMaster> educationMasterList = new ArrayList<EducationMaster>();
        EducationMaster educationMaster = new EducationMaster();
        educationMaster.setEducationDescr("Under Graduate");
        educationMasterList.add(educationMaster);
        educationMaster = new EducationMaster();
        educationMaster.setEducationDescr("Graduate");
        educationMasterList.add(educationMaster);
        educationMaster = new EducationMaster();
        educationMaster.setEducationDescr("Post Graduate");
        educationMasterList.add(educationMaster);
        educationMaster = new EducationMaster();
        educationMaster.setEducationDescr("Others");
        educationMasterList.add(educationMaster);
        return educationMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<DocumentMaster> getDocumentMaster(String institutionId) {
        List<DocumentMaster> documentMasterList = new ArrayList<DocumentMaster>();
        DocumentMaster documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Address Proof");
        documentMasterList.add(documentMaster);

        documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Address Proof & ID Proof");
        documentMasterList.add(documentMaster);

        documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Address Proof");
        documentMasterList.add(documentMaster);

        documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Income Proof");
        documentMasterList.add(documentMaster);

        documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Banking Proof");
        documentMasterList.add(documentMaster);

        documentMaster = new DocumentMaster();
        documentMaster.setTypeOfDocument("Property Proof");
        documentMasterList.add(documentMaster);

        return documentMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<CompanyTypeMaster> getCompanyTypeMaster(String institutionId) {
        List<CompanyTypeMaster> companyTypeMasterList = new ArrayList<CompanyTypeMaster>();

        CompanyTypeMaster companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Private Limited");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Public Limited");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Public Sector Undertaking");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Central Government");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("State Government");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Multi National Company");
        companyTypeMasterList.add(companyTypeMaster);

        companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setCompanyTypeDescription("Others");
        companyTypeMasterList.add(companyTypeMaster);

        return companyTypeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<BusinessTypeMaster> getBusinessTypeMaster(String institutionId) {
        List<BusinessTypeMaster> businessTypeMasterList = new ArrayList<BusinessTypeMaster>();

        BusinessTypeMaster bussinessMaster = new BusinessTypeMaster();
        bussinessMaster.setBusinessTypeDescription("Manufacturing");
        businessTypeMasterList.add(bussinessMaster);

        bussinessMaster = new BusinessTypeMaster();
        bussinessMaster.setBusinessTypeDescription("Traders/Retailers/Wholesalers");
        businessTypeMasterList.add(bussinessMaster);

        bussinessMaster = new BusinessTypeMaster();
        bussinessMaster.setBusinessTypeDescription("Service Provider");
        businessTypeMasterList.add(bussinessMaster);

        bussinessMaster = new BusinessTypeMaster();
        bussinessMaster.setBusinessTypeDescription("Others");
        businessTypeMasterList.add(bussinessMaster);

        return businessTypeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<AssetsOwned> getAssetsOwned(String institutionId) {
        List<AssetsOwned> assetsOwnedList = new ArrayList<AssetsOwned>();

        AssetsOwned assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Vehicle");
        assetsOwnedList.add(assetsOwned);

        assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Shares");
        assetsOwnedList.add(assetsOwned);

        assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Mutual Funds");
        assetsOwnedList.add(assetsOwned);

        assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Agriculture Land");
        assetsOwnedList.add(assetsOwned);

        assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Property");
        assetsOwnedList.add(assetsOwned);

        assetsOwned = new AssetsOwned();
        assetsOwned.setAssetsOwned("Gold Jewellery");
        assetsOwnedList.add(assetsOwned);

        return assetsOwnedList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<ApplicantTypeMaster> getApplicantTypeMaster(String institutionId) {
        List<ApplicantTypeMaster> spplicantTypeMasterList = new ArrayList<ApplicantTypeMaster>();
        ApplicantTypeMaster applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("SAL");
        applicantTypeMaster.setApplicantTypeDescr("Salaried");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("SEB");
        applicantTypeMaster.setApplicantTypeDescr("Self Employed Business");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("SEP");
        applicantTypeMaster.setApplicantTypeDescr("Self Employed Professional");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("FIRM");
        applicantTypeMaster.setApplicantTypeDescr("Partnership Firm");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("LLP");
        applicantTypeMaster.setApplicantTypeDescr("Limited Liability Partnership");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("PVT");
        applicantTypeMaster.setApplicantTypeDescr("Private Limited Company");
        spplicantTypeMasterList.add(applicantTypeMaster);

        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("LLC");
        applicantTypeMaster.setApplicantTypeDescr("Limited Liability Company");
        spplicantTypeMasterList.add(applicantTypeMaster);


        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("PLC");
        applicantTypeMaster.setApplicantTypeDescr("Public Limited Company");
        spplicantTypeMasterList.add(applicantTypeMaster);


        applicantTypeMaster = new ApplicantTypeMaster();
        applicantTypeMaster.setApplicantTypeCode("PROP");
        applicantTypeMaster.setApplicantTypeDescr("Proprietorship Firm");
        spplicantTypeMasterList.add(applicantTypeMaster);
        return spplicantTypeMasterList;
    }

    /**
     * @param institutionId
     * @return
     */
    private List<AccountType> getAccountType(String institutionId) {
        List<AccountType> accountTypeList = new ArrayList<AccountType>();
        AccountType accountType = new AccountType();
        accountType.setAccountType("Savings");
        accountType.setConstitution("All");
        accountTypeList.add(accountType);

        accountType = new AccountType();
        accountType.setAccountType("Current");
        accountType.setConstitution("All");
        accountTypeList.add(accountType);

        accountType = new AccountType();
        accountType.setAccountType("OD/CC");
        accountType.setConstitution("All except SAL");
        accountTypeList.add(accountType);
        return accountTypeList;
    }


}
