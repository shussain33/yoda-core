package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

/**
 * Created by AmitBotre on 30/12/19.
 */
public interface DashboardDataRepository {

    long fetchDashboardRecordCount(Criteria criteria);

    List<GoNoGoCustomerApplication> fetchDashboardRecords(Criteria criteria, int skip, int limit);
}
