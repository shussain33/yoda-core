package com.softcell.dao.mongodb.repository.serialnumber;

import com.softcell.config.KarzaProcessingInfo;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.ThirdPartyApiLog;
import com.softcell.gonogo.model.adroit.AdroitCallLog;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.finfort.FinfortCallLog;
import com.softcell.gonogo.model.kyc.LosKarzaLog;
import com.softcell.gonogo.model.kyc.TotalKycReqResLog;
import com.softcell.gonogo.model.lms.LmsCallLog;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.mifin.MiFinAmbitCallLog;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.qde.CustomerDataServiceCallLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AmbitMifinLog;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.sms.SmsServReqResLogRequest;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.model.tclos.log.TcLosReqReslog;
import com.softcell.gonogo.serialnumbervalidation.*;

import java.util.List;

/**
 * @author mahesh
 */

public interface ExternalAPILogRepository {

    /**
     * This method check the Serial number, if it is valid (not already soldout)
     * then store into the database.
     *
     * @return TODO
     */
    boolean saveSerialNumberInfo(SerialNumberInfo serialNumberInfo);

    /**
     * @return return true boolean response if the serial number is already
     * available in the database otherwise return false boolean
     * response.
     */
    boolean checkSerialNumberInDedupe(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     * @param serialNumberApplicableCheckRequest
     * @return
     */
    PostIpaRequest checkSerialNumberValidationFlag(
            SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest);

    /**
     * Loggable samasung request to db
     *
     * @param serialsaleconfirmationrequest
     * @return
     */
    boolean saveSerialNumberRequest(
            SerialSaleConfirmationRequest serialsaleconfirmationrequest);

    /**
     * this method will return updated serial number against the reference id.
     *
     * @param referenceId
     * @return
     */
    SerialNumberInfo viewUpdatedSRNumberDetails(
            String referenceId);

    /**
     *
     * @param referenceId
     * @return
     */
    SerialNumberInfo getSerialNumberDetails(
            String referenceId);

    /**
     * @param getRollbackDetailsRequest
     * @return
     */
    SerialNumberInfo getValidatedImeiOrSerialNumber(GetRollbackDetailsRequest getRollbackDetailsRequest);

    /**
     * @param rollbackRequest
     */
    void updateSerialNumberInfoLog(RollbackRequest rollbackRequest, String status);

    /**
     * @param rollbackFeatureLog
     */
    void saveRollbackLog(RollbackFeatureLog rollbackFeatureLog);

    public void saveTotalKycReqResLog(TotalKycReqResLog totalKycReqResLog);

    public void saveSmsServiceReqResLog(SmsServiceReqResLog smsServiceReqResLog);

    public List<SmsServiceReqResLog> getSmsServiceReqResLog(SmsServReqResLogRequest smsServReqResLogRequest);


        void saveCustomerDataServiceCallLog(CustomerDataServiceCallLog customerDataServiceCallLog);

    /**
     * @param disbursementFeatureLog
     */
    void saveDisbursementLog(DisbursementFeatureLog disbursementFeatureLog);

    void saveRawResonseLog(RawResponseLog rawResponseLog);

    /**
     * Saves response for CreditVidya's get-user-profile request.
     * @param getUserProfileCallLog
     */
    void saveCreditVidyaCallLog(GetUserProfileCallLog getUserProfileCallLog);

    /**
     * Saves response for TVS CD's getCD request.
     * @param saathiCallLog
     */
    void saveTvscdCallLog(SaathiCallLog saathiCallLog);

    List<GetUserProfileCallLog> fetchCreditVidyaResponse(CreditVidyaLogRequest creditVidyaLogRequest);

    List<SaathiCallLog> fetchSaathiResponse(SaathiLogRequest saathiLogRequest);

    TotalKycReqResLog getTKycLog(TKycLogRequest tKycLogRequest);

    void savetcLosReqResLog(TcLosReqReslog totalKycReqResLog);

    TcLosReqReslog getTcLosLog(TcLosLogRequest tcLosLogRequest);

    /**
     *
     * @param aadharNo
     * @param institutionId
     * @return
     */
    List<AadharLog> getAadharLogByAadharNoAndInstId(String aadharNo, String institutionId);

    /**
     *
     * @param serialNumberApplicableVendorLog
     * @return
     */
    void saveSerialNumberApplicableVendorLog(SerialNumberApplicableVendorLog serialNumberApplicableVendorLog);

    void saveMiFinCallLog(MiFinCallLog miFinCallLog);

    MiFinCallLog fetchMiFinCallLog(String refId);

    /**
     * Save Mifin Ambit API call logs
     * @param miFinCallLog
     * @return
     */
    void saveMifinAmbitLog(MiFinAmbitCallLog miFinCallLog);

    List<MiFinAmbitCallLog> fetchAmbitMifinCalllog(AmbitMifinLog ambitMifinLog);

    void saveLMSCallLog(LmsCallLog apiLog);

    void saveAdroitCallLog(AdroitCallLog adroitCallLog);

    void saveFinfortCallLog(FinfortCallLog finfortCallLog);
    List<AdroitCallLog> fetchAdroitAckIds(String refID, List<String> collateralIds, String valuationInitiate);

    FinfortCallLog fetchFinfortCallLogByAckId(String ackId);

    boolean UpdateMifinCallog(MiFinCallLog apiLog, String refID);

	void saveIciciCallLog(ThirdPartyApiLog apiLog);
    ThirdPartyApiLog fetchIciciCallLog(String refId);

    MiFinCallLog fetchMiFinCallLogByRequestType(String refId, String requestType);

    List<MiFinCallLog> fetchMiFinCallLogForDedupe(String refId, String requestType) ;

    MiFinCallLog ToverfyMifinCall(String refId);

    KarzaProcessingInfo fetchKarzaProcessingInfo(String institutionId, String kycNumber, String applicationId, String applicantId, String kycName);

    void saveKarzaProcessingInfo(KarzaProcessingInfo karzaProcessingInfo);

    public void saveKarzaTotalKycReqResLog(LosKarzaLog losKarzaLog);

    void saveSobreCallLog(ScoringCallLog callLog);

    List<ScoringCallLog> fetchScoringCallLogList(String instituteId, String refId, String requestType, List<String> includeFields);

    void saveCallLog(ThirdPartyApiLog apiLog, String collectionName);
}

