package com.softcell.dao.mongodb.repository.multiproduct;


import com.softcell.config.MultiProductConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;

import java.util.List;
import java.util.Set;

/**
 * @author yogeshb
 */
public interface MultiProductRepository {
    /**
     * @param multiProductRequest
     * @return Number of products Allowed.
     * @author yogeshb
     */
    public MultiProductConfiguration getMultiProductConfiguration(
            MultiProductRequest multiProductRequest);

    /**
     * @param multiProductConfiguration
     * @return
     * @author yogeshb
     */
    public boolean add(MultiProductConfiguration multiProductConfiguration);

    /**
     * @param multiProductConfiguration
     * @return
     */
    public boolean delete(MultiProductConfiguration multiProductConfiguration);

    /**
     * @param multiProductConfiguration
     * @return
     */
    public boolean update(MultiProductConfiguration multiProductConfiguration);

    /**
     * @return
     */
    public List<MultiProductConfiguration> findAll();

    /**
     * get parent application
     *
     * @param refID
     */
    public GoNoGoCroApplicationResponse getParentApplication(String refID) throws Exception;

    /**
     * This method check the newly created Reference id already exist or not.
     *
     * @param refID
     * @return
     */
    public boolean checkRefernceIDExistance(String refID);

    /**
     * @param parentApplication
     * @return
     */
    public GoNoGoCroApplicationResponse cloneApplication(
            GoNoGoCroApplicationResponse parentApplication);

    /**
     * This service return number of products already added.
     *
     * @param rootID
     * @return
     */
    public long getProductCount(String rootID);

    /**
     * @param refID
     * @return
     */
    public PostIpaRequest getPostIPA(String refID);

    /**
     * @param rootID
     * @return
     */
    public Set<String> getAllRefenceIDAgainstRootApplication(String rootID);

    /**
     * @param refID
     * @return
     */
    public boolean getPostIPAExistance(String refID);

    /**
     * @param refID
     * @return
     */
    public boolean checkApplicationStage(String refID);

    /**
     * @param header
     * @return
     */
    public boolean checkDealerSubventionWaivedOff(Header header);

    double getSumOfNetFundingAmount(Set<String> refIDs);

}
