/**
 * kishorp12:17:30 PM  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import com.softcell.workflow.metadata.SchemeMaster;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author kishorp
 */
public interface MasterDataRepository {
    /**
     * @param institutionId
     * @return
     */
    List<PinCodeMaster> getPinCodeMaster(String institutionId);

    /**
     * @param pinCodeMaster
     * @return
     */
    boolean savePinCodeMaster(PinCodeMaster pinCodeMaster);

    /**
     * @param employerMaster
     * @return
     */
    boolean saveEmployerMaster(EmployerMaster employerMaster);

    /**
     * @param institutionId
     * @return
     */
    List<EmployerMaster> getEmployerMaster(String institutionId);

    /**
     * @param assetModelMaster
     * @return
     */
    boolean saveAssetMaster(AssetModelMaster assetModelMaster);

    /**
     * @param institutionId
     * @return
     */
    List<AssetModelMaster> getAssetMaster(String institutionId);

    /**
     * @param institutionId
     * @return
     */
    List<SchemeMaster> getSchemeMaster(String institutionId);

    /**
     * @param schemeMaster
     * @return
     */
    boolean saveScheme(SchemeMaster schemeMaster);

    // Yogesh Changes

    /**
     * @param instId
     * @param queryString
     * @return
     */
    PinCodeMaster getPostalCodeDetails(String instId, String queryString);

    /**
     * @param instId
     * @param queryString
     * @return
     */
    Stream<EmployerMaster> getEmployerMaster(String instId,
                                             String queryString);

    /**
     * @param instId
     * @return
     */
    List<String> getAssetCategory(String instId);

    /**
     * @param applicationMetadataRequest
     * @return
     */
    List<AssetModelMaster> getAssetDetails(
            ApplicationMetadataRequest applicationMetadataRequest);

    /**
     * @param institutionID
     * @return
     */
    List<SchemeMasterData> getSchemeMasterDetails(String institutionID);

    /**
     * @param institutionId
     * @param category
     * @return
     */
    List<AssetModelMaster> getAssetMasterWeb(String institutionId,
                                             String category);

    /**
     * @param institutionID
     * @param dealerID
     * @return
     */
    List<String> getSchemeIds(String institutionID, String dealerID);

    /**
     * @param schemeID
     * @return
     */
    List<AssetModelMaster> getAssetModelMasterDetails(String schemeID[]);

    /**
     * @param instId
     * @param dealerID
     * @param modelID
     * @return
     */
    List<SchemeMasterData> getSchemeMasterDataRecords(String instId,
                                                      String dealerID, String modelID);

    /**
     * @param instId
     * @param schemeIdArray
     * @return
     */
    List<String> getModelIds(String instId, String[] schemeIdArray);

    /**
     * @param modelNo
     * @return
     */
    AssetModelMaster getAssetModelMasterByModelNo(String modelNo);

    /**
     * @param modelNo
     * @param catDesc
     * @param manufacturerDesc
     * @return
     */
    AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                         String manufacturerDesc);

    /**
     * @param modelId
     * @param dealerId
     * @param city
     * @return
     */
    List<SchemeModelDealerCityMapping> getSchemeIdList(String modelId,
                                                       String dealerId, String city);

    /**
     * @param schemeIdSet
     * @param modelNo
     * @param city
     * @param dealerId
     * @return
     */
    List<SchemeModelDealerCityMapping> getSchemeMappingList(
            Set<String> schemeIdSet, String modelNo, String city,
            String dealerId);

    /**
     * @param schemeIdArray
     * @return
     */
    List<SchemeDateMapping> getSchemeDateMapping(String[] schemeIdArray);

    /**
     * @param schemes
     * @return
     */
    List<SchemeMasterData> getSchemeMasterData(String[] schemes);

    /**
     * @param schemes
     * @return
     */
    List<HitachiSchemeMaster> getHitachiSchemeMaster(String[] schemes);

    /**
     * @param schemes
     * @return
     */
    List<HitachiSchemeMaster> getVanillaHitachiSchemeMaster(
            String[] schemes);


    /**
     * @param instId
     * @param queryString
     * @return
     */
    List<CarSurrogateMaster> getCarSurrogateMaster(String instId,
                                                   String queryString);

    /**
     * @param institutionID
     * @return
     */
    SchemeMasterFliter getCcIdSchemeMasterDetails(String institutionID);

    /**
     * @param institutionID
     * @return
     */
    SchemeMasterFliter getVanillaSchemeMasterDetails(String institutionID);

    /**
     * @param schemeIdSet
     * @param modelNo
     * @param city
     * @return
     */
    List<SchemeModelDealerCityMapping> getSchemeMappingList(
            Set<String> schemeIdSet, String modelNo, String city);

    /**
     * @param schemeIdSet
     * @param modelNo
     * @param city
     * @param dealerId
     * @return
     */
    List<SchemeModelDealerCityMapping> getSchemeMappingExcludedList(
            Set<String> schemeIdSet, String modelNo, String city,
            String dealerId);

    /**
     * @return
     */
    SchemeMasterFliter getWithoutCcIdSchemeMasterDetails();

    /**
     * @param institutionId
     * @return bank details list for given institution id.
     */
    List<BankDetailsMaster> getBankDetailsMaster(String institutionId);

    /**
     * @param institutionId
     * @param dealerID
     * @return
     */
    String getDealerCity(String institutionId, String dealerID);

    /**
     * @param institutionId
     * @param dealerID
     * @return
     */
    String getDealerState(String institutionId, String dealerID);

    /**
     * @param modelNo
     * @param catDesc
     * @param manufacturerDesc
     * @param modelId
     * @return
     */
    AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                         String manufacturerDesc, String modelId);

    /**
     * @param institutionId
     * @param dealerID
     * @return
     */
    DealerEmailMaster getDealerDetails(String institutionId,
                                       String dealerID);

    /**
     * @param request
     * @return
     */
    Set<String> getModelVariantManufacturer(ModelVariantMasterRequest request);

    /**
     * @param modelNo
     * @param catDesc
     * @param manufacturerDesc
     * @param modelId
     * @param institutionId
     * @return
     */
    AssetModelMaster getAssetModelMaster(String modelNo, String catDesc,
                                         String manufacturerDesc, String modelId, String institutionId);

    /**
     * @param modelVariantMasterRequest
     * @return
     */
    List<ReferenceDetailsMaster> getReferenceRelationMasterDetails(ModelVariantMasterRequest modelVariantMasterRequest);

    /**
     *
     * @return
     * @param institutionId
     */
    List<BankDetailsMaster> getBankNames(String institutionId);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @return
     */
    List<BankDetailsMaster> getStatesByBankName(String institutionId, String bank);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @return
     */
    List<BankDetailsMaster> getBankDistrict(String institutionId, String bank, String state);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @param district
     * @return
     */
    List<BankDetailsMaster> getBankBranches(String institutionId, String bank, String state, String district);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @param district
     * @param branch
     * @return
     */
    BankDetailsMaster getBankMasterDetails(String institutionId, String bank, String state, String district, String branch);

    /**
     *
     *
     * @param institutionId
     * @param ifsc
     * @return
     */
    BankDetailsMaster getBankMasterDetailsByIfscCode(String institutionId, String ifsc);

    BankDetailsMaster getBankMasterDetailsByMicrCode(String institutionId, String ifsc);

    /**
     *
     * @param institutionId
     * @return
     */
    Set<String> getAccountTypeFromCGPM(String institutionId);

    /**
     * Fetch bank name from CGPM master based on KEY1=BANKNAME
     * @param bankName
     * @return
     *
     * @Depricate Use getBankNameFromBank master.
     */
    Set<String> getBankNameFromCGPM(String bankName);

    List<BankMaster> getBankNameFromBankMaster(String bankName, String institutionId);

    SupplierLocationMaster getSupplierLocationMaster(String supplierId,String branchId);

    List<CityMaster>  getCityMaster(String institution, String query);

    List<CityMaster>  getCityMaster(String institution);

    boolean saveEmployerMaster(EmployerMaster employerMaster,boolean update, String instId);

    boolean saveBankMasterDetail(BankDetailsMaster bankDetailsMaster, boolean update, String instId, String userId);

    boolean deleteBankingIfscMaster(String ifscCode, String institutionId);

    boolean savePinCodeMaster(PinCodeMaster pinCodeMaster, boolean update, String institutionId, String loggedInUserId);

    boolean deletePincoideMaster(String pincode, String institutionId);

    boolean deleteEmployerMaster(String employerId, String institutionId);

    boolean saveBankMaster(BankMaster bankMaster,boolean update, String instId );

    List<BankMaster> getBankMaster(String institutionId);

    boolean deleteBankMaster(String bankCode, String institutionId);

    List<BankMaster> getBankInfoFromBankMaster(String bankName, String institutionId);
}
