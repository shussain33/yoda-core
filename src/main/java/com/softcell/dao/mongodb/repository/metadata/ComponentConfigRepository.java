package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.config.ComponentConfiguration;

import java.util.List;

/**
 * This dao layer will be used to do CRUD operations
 * related to workflow component meta data
 *
 * @author bhuvneshk
 */
public interface ComponentConfigRepository {

    public Boolean insertComponentConfiguration(ComponentConfiguration componentConfig);

    public Boolean updateComponentConfiguration(ComponentConfiguration componentConfig);

    public Boolean deleteComponentConfiguration(ComponentConfiguration componentConfig);

    public List<ComponentConfiguration> readComponentConfigByInstitution(String institutionId);

    public List<ComponentConfiguration> readActiveComponentConfigByInstitution(String institutionId);

    public boolean isComponentConfigExist(ComponentConfiguration componentConfig);
}
