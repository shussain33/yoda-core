/**
 *
 */
package com.softcell.dao.mongodb.repository.systemConfiguration;

import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import com.softcell.config.KarzaConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.Product;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.configuration.AuthSkipConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.los.LosChargeConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import com.softcell.utils.GngDateUtil;
import com.softcell.gonogo.model.MasterSchedulerActivityLogs;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.constants.Constant;

import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yogeshb
 */
@Repository
public class ConfigurationMongoRepository implements ConfigurationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;
    public ConfigurationMongoRepository (MongoTemplate mongoTemplate) {
        if (this.mongoTemplate == null) {
            this.mongoTemplate = mongoTemplate;
        }
    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    @Override
    public boolean addInstitutionProductConfiguration(
            InstitutionProductConfiguration institutionProductConfiguration) {

        boolean flag;

        try {

            if (!recordAlreadyExist(institutionProductConfiguration)) {

                mongoTemplate.insert(institutionProductConfiguration);

                flag = true;

            } else {

                logger.trace(" institution product configuration already exists  ");

                flag = false;
            }


        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting a institution product configuration with probable cause [{}] ", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting a institution product configuration with probable cause [{%s}] ", e.getMessage()));
        }

        return flag;
    }

    /**
     * @param institutionProductConfiguration
     * @return
     */
    private boolean recordAlreadyExist(InstitutionProductConfiguration institutionProductConfiguration) {

        boolean exists = false;

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionID")
                    .is(institutionProductConfiguration.getInstitutionID())
                    .and("productID")
                    .is(institutionProductConfiguration.getProductID()));

            exists = mongoTemplate.exists(query,
                    InstitutionProductConfiguration.class);

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error(" error occurred while checking if product for institutionId is already configured or not  ");
        }


        return exists;
    }

    @Override
    public boolean removeInstitutionProductConfiguration(
            InstitutionProductConfiguration institutionProductConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionID")
                .is(institutionProductConfiguration.getInstitutionID())
                .and("productID")
                .is(institutionProductConfiguration.getProductID()));
        WriteResult writeResult = mongoTemplate.remove(query,
                InstitutionProductConfiguration.class);

        return writeResult.getN() > 0;

    }

    @Override
    public InstitutionProductConfiguration updateInstitutionProductConfiguration(
            InstitutionProductConfiguration institutionProductConfiguration) throws SystemException {

        logger.trace(" updating institution product configuration with incoming object [{}] ", institutionProductConfiguration);

        Assert.notNull(institutionProductConfiguration, "institutionProductConfiguration must not be null ");

        InstitutionProductConfiguration institutionProduct = null;
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionID")
                    .is(institutionProductConfiguration.getInstitutionID())
                    .and("productName")
                    .is(institutionProductConfiguration.getProductName()));


            Update update = new Update();

            update.set("status", institutionProductConfiguration.isStatus());

            update.set("productID", Product.getProductIdFromName(institutionProductConfiguration.getProductName()));

            update.set("aliasName", institutionProductConfiguration.getAliasName());

            institutionProduct = mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), InstitutionProductConfiguration.class);

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error(" error occurred while updating institution Product Configuration  with probable cause [{}] ", e.getMessage());
        } catch (SystemException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while updating institution product configuration for instituionId [{}] and productName [{}] ",
                    institutionProductConfiguration.getInstitutionID(), institutionProductConfiguration.getProductName());
            throw new SystemException("Error occurred while updating institution, product configuration supported product are ").set("AVAILABLE_PRODUCTS", Product.values());
        }

        return institutionProduct;

    }

    @Override
    public List<InstitutionProductConfiguration> findAllInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionID").is(
                institutionProductConfiguration.getInstitutionID()));
        return mongoTemplate.find(query, InstitutionProductConfiguration.class);
    }

    @Override
    public boolean isProductExist(String institution, String productID,
                                  String productName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionID").is(institution)
                .and("productID").is(productID).and("productName")
                .is(productName).and("status").is(true));
        return mongoTemplate.exists(query,
                InstitutionProductConfiguration.class);
    }

    @Override
    public boolean saveTemplateConfiguration(
            TemplateConfiguration templateConfiguration) {
        if (recordAlreadyExist(templateConfiguration)) {
            return false;
        }
        try {
            /*if (!mongoTemplate.collectionExists(TemplateConfiguration.class)) {
                mongoTemplate.createCollection(TemplateConfiguration.class);
            }*/

            mongoTemplate.insert(templateConfiguration);

            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving template configuration with probable cause [{}] ", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving template configuration with probable cause [{%s}] ", exception.getMessage()));
        }
    }

    private boolean recordAlreadyExist(
            TemplateConfiguration templateConfiguration) {
        return mongoTemplate.exists(getTemplateConfigurationUpdateFilters(templateConfiguration), TemplateConfiguration.class);
    }

    @Override
    public List<TemplateConfiguration> getTemplateConfiguration(
            TemplateConfiguration templateConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(templateConfiguration.getInstitutionId()).and("productId")
                .is(templateConfiguration.getProductId()));
        return mongoTemplate.find(query, TemplateConfiguration.class);
    }

    @Override
    public TemplateConfiguration getTemplateConfiguration(String institutionId,
                                                          String productId, String templateName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(institutionId).and("productId")
                .is(productId).and("templateName")
                .is(templateName).and("enable").is(true));
        return mongoTemplate.findOne(query, TemplateConfiguration.class);
    }

    @Override
    public boolean updateTemplatePath(
            TemplateConfiguration templateConfiguration) {
        Update update = new Update();
        update.set("templatePath", templateConfiguration.getTemplatePath());
        WriteResult writeResult = mongoTemplate.updateFirst(getTemplateConfigurationUpdateFilters(templateConfiguration), update, TemplateConfiguration.class);
        return writeResult.isUpdateOfExisting();
    }

    @Override
    public boolean updateTemplateLogo(
            TemplateConfiguration templateConfiguration) {
        Update update = new Update();
        update.set("logoId", templateConfiguration.getLogoId());
        WriteResult writeResult = mongoTemplate.updateFirst(getTemplateConfigurationUpdateFilters(templateConfiguration), update, TemplateConfiguration.class);
        return writeResult.isUpdateOfExisting();
    }

    private Query getTemplateConfigurationUpdateFilters(TemplateConfiguration templateConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(templateConfiguration.getInstitutionId()).and("productId")
                .is(templateConfiguration.getProductId()).and("templateName")
                .is(templateConfiguration.getTemplateName()));
        return query;
    }

    @Override
    public boolean enableTemplate(TemplateConfiguration templateConfiguration) {
        Update update = new Update();
        update.set("enable", true);
        WriteResult writeResult = mongoTemplate.updateFirst(
                getTemplateConfigurationUpdateFilters(templateConfiguration),
                update, TemplateConfiguration.class);
        return writeResult.isUpdateOfExisting();
    }

    @Override
    public boolean disableTemplate(TemplateConfiguration templateConfiguration) {
        Update update = new Update();
        update.set("enable", false);
        WriteResult writeResult = mongoTemplate.updateFirst(
                getTemplateConfigurationUpdateFilters(templateConfiguration),
                update, TemplateConfiguration.class);
        return writeResult.isUpdateOfExisting();
    }

    @Override
    public boolean saveEmailConfiguration(EmailConfiguration emailConfiguration) {
        if (recordAlreadyExist(emailConfiguration)) {
            return false;
        }
        try {
            /*if (!mongoTemplate.collectionExists(EmailConfiguration.class)) {
                mongoTemplate.createCollection(EmailConfiguration.class);
            }*/
            mongoTemplate.insert(emailConfiguration);
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving email configuration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving email configuration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    private boolean recordAlreadyExist(EmailConfiguration emailConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(emailConfiguration.getInstitutionId())
                .and("productId")
                .is(emailConfiguration.getProductId()));
        return mongoTemplate.exists(query,
                EmailConfiguration.class);
    }

    @Override
    public boolean updateEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();

            if (StringUtils.isNotBlank(emailConfiguration.getCopyRight())) {
                update.set("copyRight", emailConfiguration.getCopyRight());
            }
            if (StringUtils.isNotBlank(emailConfiguration.getEmailBody())) {
                update.set("emailBody", emailConfiguration.getEmailBody());
            }
            if (StringUtils.isNotBlank(emailConfiguration.getEmailSignature())) {
                update.set("emailSignature", emailConfiguration.getEmailSignature());
            }
            if (StringUtils.isNotBlank(emailConfiguration.getEmailSubject())) {
                update.set("emailSubject", emailConfiguration.getEmailSubject());
            }

            update.set("lastUpdateDate", new Date());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException exception) {
            logger.error("Error occurred while updating email configuration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while updating email configuration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public boolean addAttachmentInEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.addToSet("attachments").each(emailConfiguration.getAttachments());
            update.set("lastUpdateDate", new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);
            return writeResult.isUpdateOfExisting();
        } catch (Exception exception) {
            logger.error("Error occurred while addAttachmentInEmailConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while addAttachmentInEmailConfiguration with probable cause [{%s}]", exception.getMessage()));

        }
    }

    @Override
    public boolean removeAttachmentFromEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.pull("attachments", new BasicDBObject("$in", emailConfiguration.getAttachments()));
            update.set("lastUpdateDate", new Date());
            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException exception) {
            logger.error("Error occurred while removeAttachmentFromEmailConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while removeAttachmentFromEmailConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public boolean disableEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.set("enable", false);
            update.set("lastUpdateDate", new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);
            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException exception) {
            logger.error("Error occurred while disabling email configuration with probable cause [{%s}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while disabling email configuration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public boolean enableEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.set("enable", true);
            update.set("lastUpdateDate", new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);
            return writeResult.isUpdateOfExisting();
        } catch (Exception exception) {
            logger.error("Error occurred while enabling email configuration with probable cause [{}] ", exception.getMessage());
            throw new SystemException(String.format("Error occurred while enabling email configuration with probable cause [{%s}] ", exception.getMessage()));
        }
    }

    @Override
    public List<EmailConfiguration> getEmailConfiguration(EmailConfiguration emailConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(emailConfiguration.getInstitutionId()));
        return mongoTemplate.find(query, EmailConfiguration.class);
    }

    @Override
    public List<InstitutionProductConfiguration> getInstitutionProductConfiguration() {

        return mongoTemplate.findAll(InstitutionProductConfiguration.class);
    }

    @Override
    public ValidationVendorsConfiguration saveAllowedVendors(ValidationVendorsConfiguration validationVendorsConfiguration) {
        ValidationVendorsConfiguration vendorConfig = null;
        if (!isValidationVendorsConfigurationExist(validationVendorsConfiguration)) {
            try {
                mongoTemplate.insert(validationVendorsConfiguration);
                vendorConfig = validationVendorsConfiguration;
            } catch (DataAccessException e) {
                logger.error(" error occurred while saving validation configuration with probable cause {}", e.getMessage());
            }
        }
        return vendorConfig;


    }

    private boolean isValidationVendorsConfigurationExist(ValidationVendorsConfiguration validationVendorsConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(validationVendorsConfiguration.getInstitutionId())
                .and("productName").is(validationVendorsConfiguration.getProductName())
                .and("vendor").is(validationVendorsConfiguration.getVendor()));
        return mongoTemplate.exists(query, ValidationVendorsConfiguration.class);
    }

    @Override
    public List<ValidationVendorsConfiguration> getApplicableVendorConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, ValidationVendorsConfiguration.class)).collect(Collectors.toList());
    }

    @Override
    public boolean deleteValidationVendorConfiguration(ValidationVendorsConfiguration validationVendorsConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(validationVendorsConfiguration.getInstitutionId())
                .and("productName").is(validationVendorsConfiguration.getProductName())
                .and("vendor").is(validationVendorsConfiguration.getVendor()));

        WriteResult remove = mongoTemplate.remove(query, ValidationVendorsConfiguration.class);
        logger.debug("Query Result: {}", remove);
        if (remove.getN() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean addSecondaryAttachmentInEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.addToSet("secondaryAttachments").each(emailConfiguration.getSecondaryAttachments());
            update.set("lastUpdateDate", new Date());
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);
            return writeResult.isUpdateOfExisting();
        } catch (Exception exception) {
            logger.error("Error occurred while addSecondaryAttachmentInEmailConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while addSecondaryAttachmentInEmailConfiguration with probable cause [{%s}]", exception.getMessage()));

        }
    }

    @Override
    public boolean removeSecondaryAttachmentFromEmailConfiguration(EmailConfiguration emailConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(emailConfiguration.getInstitutionId())
                    .and("productId")
                    .is(emailConfiguration.getProductName().toProductId()));

            Update update = new Update();
            update.pull("secondaryAttachments", new BasicDBObject("$in", emailConfiguration.getSecondaryAttachments()));
            update.set("lastUpdateDate", new Date());
            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, EmailConfiguration.class);

            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException exception) {
            logger.error("Error occurred while removeSecondaryAttachmentFromEmailConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while removeSecondaryAttachmentFromEmailConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public boolean saveLoyaltyCardConfiguration(LoyaltyCardConfiguration loyaltyCardConfiguration) {
        try {
            mongoTemplate.insert(loyaltyCardConfiguration);
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving loyaltyCardConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving loyaltyCardConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public List<LoyaltyCardConfiguration> getLoyaltyCardConfiguration(String institutionId, String productId) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("productId").is(productId)
                    .and("enable").is(true));

            return mongoTemplate.find(query, LoyaltyCardConfiguration.class);

        } catch (DataAccessException e) {

            logger.error("Error occurred while getting loyaltyCardConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting loyaltyCardConfiguration with probable cause [{%s}]", e.getMessage()));

        }

    }

    @Override
    public boolean removeSmsTemplateConfiguration(String institutionId, String productId, String smsType) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(institutionId)
                    .and("productId")
                    .is(productId)
                    .and("smsType")
                    .is(smsType));

            WriteResult writeResult = mongoTemplate.remove(query, SmsTemplateConfiguration.class);
            return writeResult.getN() > 0;

        } catch (DataAccessException e) {
            logger.error("Error occurred while deleting smsTemplateConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting smsTemplateConfiguration with probable cause [{%s}]", e.getMessage()));

        }
    }

    @Override
    public boolean removeLoyaltyCardConfiguration(String institutionId, String productId ,LoyaltyCardType loyaltyCardType) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("productId").is(productId)
            .and("loyaltyCardType").is(loyaltyCardType));

            Update update = new Update();

            update.set("enable", false);
            update.set("lastUpdateDate", new Date());

            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, LoyaltyCardConfiguration.class);
            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("Error occurred while deleting loyaltyCardConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting loyaltyCardConfiguration with probable cause [{%s}]", e.getMessage()));

        }

    }

    @Override
    public boolean updateLoyaltyCardConfiguration(LoyaltyCardConfiguration loyaltyCardConfiguration) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(loyaltyCardConfiguration.getInstitutionId())
                    .and("productId").is(loyaltyCardConfiguration.getProductId())
                    .and("loyaltyCardType").is(loyaltyCardConfiguration.getLoyaltyCardType())
                    .and("enable").is(true));

            Update update = new Update();

            update.set("loyaltyCardPrice", loyaltyCardConfiguration.getLoyaltyCardPrice());
            update.set("lastUpdateDate", new Date());

            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, LoyaltyCardConfiguration.class);
            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("Error occurred while updating loyaltyCardConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updating loyaltyCardConfiguration with probable cause [{%s}]", e.getMessage()));

        }
    }


    @Override
    public boolean saveDmsFolderConfiguration(List<DmsFolderConfiguration> dmsFolderConfigurationList) {
        try {
            mongoTemplate.insertAll(dmsFolderConfigurationList);
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving dmsFolderConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving dmsFolderConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }


    @Override
    public List<DmsFolderConfiguration> getDmsFolderConfiguration(String institutionId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId));

            return mongoTemplate.find(query, DmsFolderConfiguration.class);

        } catch (DataAccessException e) {

            logger.error("Error occurred while getting dmsFolderConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting dmsFolderConfiguration with probable cause [{%s}]", e.getMessage()));

        }

    }

    @Override
    public boolean updateDmsFolderConfiguration(DmsFolderConfiguration dmsFolderConfiguration) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(dmsFolderConfiguration.getInstitutionId())
                    .and("indexName")
                    .is(dmsFolderConfiguration.getIndexName()));

            Update update = new Update();

            update.set("indexId", dmsFolderConfiguration.getIndexId());
            update.set("indexType", dmsFolderConfiguration.getIndexType());
            update.set("lastUpdateDate", new Date());

            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, DmsFolderConfiguration.class);
            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("Error occurred while updating dmsFolderConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while updating dmsFolderConfiguration with probable cause [{%s}]", e.getMessage()));

        }

    }


    @Override
    public boolean removeDmsFolderConfiguration(String institutionId, String indexName) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("indexName").is(indexName));

            Update update = new Update();

            update.set("enable", false);
            update.set("lastUpdateDate", new Date());

            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateMulti(query, update, DmsFolderConfiguration.class);
            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("Error occurred while deleting DmsFolderConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting DmsFolderConfiguration with probable cause [{%s}]", e.getMessage()));

        }
    }

    @Override
    public boolean updateSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(smsTemplateConfiguration.getInstitutionId())
                    .and("productId")
                    .is(smsTemplateConfiguration.getProductId())
                    .and("smsType")
                    .is(smsTemplateConfiguration.getSmsType()));
            Update update = new Update();
            update.set("enable", smsTemplateConfiguration.isEnable());
            update.set("smsText", smsTemplateConfiguration.getSmsText());
            update.set("lastUpdateDate", new Date());
            logger.debug("Update {}", update);
            WriteResult writeResult = mongoTemplate.updateFirst(query, update, SmsTemplateConfiguration.class);
            return writeResult.isUpdateOfExisting();
        } catch (DataAccessException e) {
            logger.error("Error occurred while updating smsTemplateConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting smsTemplateConfiguration with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean saveSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration) {
        try {
            if (recordAlreadyExist(smsTemplateConfiguration)) {
                logger.info("Record already Exists");
                return false;
            }
            mongoTemplate.insert(smsTemplateConfiguration);
            logger.info("smsTemplateConfiguration inserted successfully");
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving smsTemplateConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving smsTemplateConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public List<SmsTemplateConfiguration> getSmsTemplateConfiguration(String institutionId) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId));

            return mongoTemplate.find(query, SmsTemplateConfiguration.class);

        } catch (DataAccessException e) {

            logger.error("Error occurred while getting smsTemplateConfiguration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while getting smsTemplateConfiguration with probable cause [{%s}]", e.getMessage()));

        }

    }

    @Override
    public boolean saveMasterMappingConfiguration(List<MasterMappingConfiguration> masterMappingConfigurationList) {
        try {

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(masterMappingConfigurationList.get(0).getInstitutionId())
                        .and("enable").is(true));
                Update update = new Update();
                update.set("enable", false);
                mongoTemplate.updateMulti(query, update,
                        MasterMappingConfiguration.class);

            mongoTemplate.insertAll(masterMappingConfigurationList);
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving saveMasterMappingConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving saveMasterMappingConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    @Override
    public MasterMappingConfiguration getMasterMappingConfiguration(String institutionId, String masterName) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("destinationMasterName").is(masterName).and("enable").is(true));

            return mongoTemplate.findOne(query, MasterMappingConfiguration.class);

        } catch (Exception exception) {

            logger.error("Error occurred while saving getMasterMappingConfiguration with probable cause [{}]", exception.getMessage());

            throw new SystemException(String.format("Error occurred while saving getMasterMappingConfiguration with probable cause [{%s}]",
                    exception.getMessage()));
        }
    }

    @Override
    public List<LosChargeConfig> getLosChargeConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(institutionId));
        return mongoTemplate.find(query, LosChargeConfig.class);
    }

    @Override
    public LosChargeConfig createLosChargeConfiguration(LosChargeConfig losChargeConfig) {

        try {
            /*if (!mongoTemplate.collectionExists(LosChargeConfig.class)) {
                mongoTemplate.createCollection(LosChargeConfig.class);
            }*/
            if (!losChargeConfigExists(losChargeConfig)) {
                mongoTemplate.insert(losChargeConfig);
            }
            return losChargeConfig;
        } catch (Exception exception) {
            logger.error("Error occurred while saving saveMasterMappingConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving saveMasterMappingConfiguration with probable cause [{%s}]", exception.getMessage()));
        }
    }

    private boolean losChargeConfigExists(LosChargeConfig losChargeConfig) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(losChargeConfig.getInstitutionId())
                .and("chargeType").is(losChargeConfig.getChargeType())
                .and("productId").is(losChargeConfig.getProductId()));
        return mongoTemplate.exists(query, LosChargeConfig.class);
    }

    @Override
    public LosChargeConfig getLosChargeConfiguration(String institutionId, LosChargeConfig.LosChargeType chargeType,String productId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(institutionId).and("chargeType").is(chargeType).and("productId").is(productId));

        return mongoTemplate.findOne(query,LosChargeConfig.class);
    }

    private boolean recordAlreadyExist(SmsTemplateConfiguration smsTemplateConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(smsTemplateConfiguration.getInstitutionId())
                .and("productId")
                .is(smsTemplateConfiguration.getProductId())
                .and("smsType")
                .is(smsTemplateConfiguration.getSmsType()));

        return mongoTemplate.exists(query,
                SmsTemplateConfiguration.class);
    }

    @Override
    public boolean saveCaseCancelConfig(CaseCancellationJobConfig caseCancellationJobConfig) {

        try {

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(caseCancellationJobConfig.getInstitutionId())
                        .and("enable").is(true));
                Update update = new Update();
                update.set("enable", false);
                mongoTemplate.updateMulti(query, update,
                        CaseCancellationJobConfig.class);

            mongoTemplate.insert(caseCancellationJobConfig);
            return true;
        } catch (Exception exception) {
            logger.error("Error occurred while saving caseCancelConfiguration with probable cause [{}]", exception.getMessage());
            throw new SystemException(String.format("Error occurred while saving caseCancelConfiguration with probable cause [{%s}]", exception.getMessage()));
        }


    }


    @Override
    public CaseCancellationJobConfig getCaseCancellationConfiguration(String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(institutionId).and("enable").is(true));

        return mongoTemplate.findOne(query, CaseCancellationJobConfig.class);
    }


    @Override
    public boolean deleteCaseCancellationConfiguration(String institutionId) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("enable").is(true));

            Update update = new Update();

            update.set("enable", false);
            update.set("lastUpdateDate", new Date());

            logger.debug("Update {}", update);

            WriteResult writeResult = mongoTemplate.updateMulti(query, update, CaseCancellationJobConfig.class);
            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            logger.error("Error occurred while deleting CaseCancellation Configuration with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting CaseCancellation Configuration with probable cause [{%s}]", e.getMessage()));

        }

    }

    @Override
    public boolean addIMPSConfiguration(@NotNull IMPSConfigDomain impsConfigDomain) throws SystemException {
        logger.debug("inserting IMPSConfigDomain domain with values {} ", impsConfigDomain);
        try{
            mongoTemplate.insert(impsConfigDomain);
            return true;

        } catch (Exception e) {
            logger.error("Error occurred while inserting IMPSConfigDomain with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while inserting IMPSConfigDomain with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public IMPSConfigDomain findIMPSConfigByInstitutionId(String institutionId) throws SystemException {
        logger.debug("Finding IMPSConfigDomain using institutionId {} ", institutionId);
        try{
            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, IMPSConfigDomain.class);

        } catch (DataAccessException e) {
            logger.error("Error occurred while fetching IMPSConfigDomain with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching IMPSConfigDomain with probable cause [{%s}]", e.getMessage()));
        }

    }

    @Override
    public boolean deleteIMPSConfig(IMPSConfigDomain impsConfigDomain) throws SystemException {
        logger.debug("deleting domain IMPSConfigDomain  {} ", impsConfigDomain);
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(impsConfigDomain.getInstitutionId()));
            WriteResult remove = mongoTemplate.remove(query, IMPSConfigDomain.class);
            logger.debug("Query Result: {}", remove);
            if (remove.getN() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            logger.error("Error occurred while deleting IMPSConfigDomain with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while deleting IMPSConfigDomain with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public List<String> fetchActiveInstitutionIds() {

        String collectionName = "InstProdConfig";
        String includeField = "institutionID";

        Query query = new Query().addCriteria(Criteria.where("status").is(true));

        return mongoTemplate.getCollection(collectionName).distinct(
                includeField, query.getQueryObject());
    }

    @Override
    public KarzaConfiguration findKarzaConfiguration(String institutionId, String product) {

        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("productName").is(product)
                .and("active").is(true));


        return mongoTemplate.findOne(query, KarzaConfiguration.class);
    }

    @Override
    public boolean insertMasterSchedularConfig(String instId, String masterName, String fromTime, String toTime) {
        boolean status=true;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(instId)
                    .and("masterName").is(masterName)
                    .and("active").is(true));
            Update update = new Update();
            update.set("fromTime", fromTime);
            update.set("toTime", toTime);
            update.set("updatedDate", new Date());

            LocalDateTime dateByToTime = GngDateUtil.getDateByToTime(toTime);
            update.set("expiryTime", dateByToTime);
            mongoTemplate.upsert(query, update, MasterSchedulerConfiguration.class);

        }catch (Exception e){
            logger.error("Exception occurred while adding/updating document in MasterSchedulerConfiguration and exception is:{}",e);
            status=false;
        }
        return status;
    }


    @Override
    public List<MasterSchedulerConfiguration> fetchMasterSchedulerConfig(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        List<MasterSchedulerConfiguration> masterSchedulerConfigurations = mongoTemplate.find(query, MasterSchedulerConfiguration.class);
        return masterSchedulerConfigurations;
    }


    @Override
    public void insterMasterActivityLogs(MasterSchedulerActivityLogs activityLogs) {
        logger.error("Inside insterMasterActivityLogs");
        try {
            mongoTemplate.insert(activityLogs);
        }catch (Exception e){
            logger.error("Exception occured while saving activity logs in db exception is:{}",e);
        }
    }

    @Override
    public AuthSkipConfiguration getAuthConfigByInstId(String institutionID) {
        AuthSkipConfiguration authSkipConfiguration = null;
        try {
            logger.info("Inside fetch authentication configuration by institutionId");
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionID));
            authSkipConfiguration= mongoTemplate.findOne(query, AuthSkipConfiguration.class);
        }catch (Exception e){
            logger.error("Exception occured while fetching details from authSkipConfiguration:{}",e);
        }
        return authSkipConfiguration;
    }

    @Override
    public MasterMappingConfiguration getMasterMappingConfiguration(String institutionId, String masterName, Product product) {
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where(Constant.INSTITUTION_ID).is(institutionId)
                    .and(Constant.DESTINATION_MASTER).is(masterName).and(Constant.ENABLE).is(true);
            if (product !=null){
                criteria.and("product").is(product);
            }
            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, MasterMappingConfiguration.class);
        } catch (Exception exception) {
            logger.error("Error occurred while saving getMasterMappingConfiguration with probable cause [{}]", exception);
            throw new SystemException(String.format("Error occurred while saving getMasterMappingConfiguration with probable cause [{%s}]",
                    exception));
        }
    }
}