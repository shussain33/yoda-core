package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.los.LosLogging;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.reliance.RelianceDigitalLogging;
import com.softcell.gonogo.model.response.dmz.DmzServiceCheckRequest;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.model.tkil.TKILLogging;

import java.util.List;

/**
 * This class is specific to mongo repository task for DMZ related issues
 *
 * @author bhuvneshk
 */
public interface DMZRepository {

    /**
     * Save response specific to reliance service
     *
     * @param relianceDigitalLogging
     */
    public void saveRelianceDigitalObj(RelianceDigitalLogging relianceDigitalLogging) throws Exception;


    /**
     * Save response specific to los service
     *
     * @param logLogging
     */
    public void saveLosLogObj(LosLogging logLogging) throws Exception;

    /**
     * This will fetch data from LosLogging to get los response data
     * which can be shown on UI Like losstage and losid
     *
     * @param refId
     * @return
     */
    public List<LosLogging> getLosLogData(String refId) throws Exception;

    /**
     * This method will be used to check if service has already used
     * @param dmzServiceCheckReq
     * @return
     */
    public Object getServiceStatus(DmzServiceCheckRequest dmzServiceCheckReq) throws Exception;

    /**
     * This service will check invoce objec wheter it is updated or not
     * If it is not updated send SUCCESS
     * @param dmzServiceCheckReq
     * @return
     */
    public boolean checkInvoiceObject(DmzServiceCheckRequest dmzServiceCheckReq) throws Exception;


    /**
     * This service is to check if los is successfully pushed
     * @param refId
     * @return
     */
    boolean checkLosStatus(String refId) throws Exception;

    /**
     * It fetch all the reliance log data based on reference id
     * @param gonogoRefId
     * @return
     */
    List<RelianceDigitalLogging> getRelianceLogData(String gonogoRefId) throws Exception;


    /**
     * This service is to check if reliance is successfully pushed
     * @param refId
     * @return
     */
    boolean checkRelianceStatus(String refId) throws Exception;

    /**
     * This service is to check if reliance is successfully pushed
     * @param refId
     * @return
     */
    boolean checkTkilStatus(String refId) throws Exception;

    /**
     * Check if dealer has access to application
     * @param dealerName
     * @return
     */
    boolean checkRelianceDealerAccess(String dealerName) throws Exception;

    /**
     *
     * @param mbPushResponse
     */
    void losMbPushLogs(MbPushResponse mbPushResponse);

    /**
     *
     * @param tvrResponse
     */
    void saveTvrLog(TVRLogs tvrResponse);

    /**
     *
     * @param tkilLogging
     */
    void saveTkilLog(TKILLogging tkilLogging) throws Exception;

    /**
     * Check if tkil dealer has access to application
     * @param dealerName
     * @return
     */
    boolean checkTkilDealerAccess(String institutionId, String dealerName) throws Exception;

    /**
     * It fetch all the tkil log data based on reference id
     * @param gonogoRefId
     * @return
     */
    List<TKILLogging> getTkilLogData(String gonogoRefId) throws Exception;
}
