package com.softcell.dao.mongodb.repository;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.dms.DMSLogResponse;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.dms.collection.DMSCreatedFolderInformation;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.dms.postdocument.DocumentInformation;
import com.softcell.gonogo.model.dms.searchfolder.FolderInformation;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.DMSUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by mahesh on 24/6/17.
 */
@Repository
public class DMSMongoRepository implements DMSRepository {

    private static final Logger logger = LoggerFactory.getLogger(DMSMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    GridFsTemplate gridFsTemplate;

    @Override
    public LOSDetails getLOSInformation(String refId ,String institutionId) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId));

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);

            if (null != goNoGoCustomerApplication &&
                    null != goNoGoCustomerApplication.getLosDetails()) {
                return goNoGoCustomerApplication.getLosDetails();
            } else {
                return null;
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occured at the time of fetching los details with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occured at the time of fetching los details with probable cause [{}] ", e.getMessage()));

        }

    }

    @Override
    public boolean checkFolderAvailability(String refId, String institutionId) {

        logger.info("inside check folder availability for reference Id {} and institution Id {}",refId,institutionId);

        try{
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

           return mongoTemplate.exists(query, DMSCreatedFolderInformation.class);

        }catch(DataAccessException e){
            e.printStackTrace();
            logger.error("Exception occur at the time of checking folder availability with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of checking folder availability with probable cause [{}] ", e.getMessage()));

        }
    }

    @Override
    public Set<String> alreadyPushedDocumentNames(String refId, String institutionId) {
        try {
            Set<String> documentNames = new TreeSet<>();
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            DMSPushDocumentInformation dmsPushDocumentInformation = mongoTemplate.findOne(query, DMSPushDocumentInformation.class);

            if (null != dmsPushDocumentInformation
                    && !CollectionUtils.isEmpty(dmsPushDocumentInformation.getSuccessDocumentInformation())) {

                for (DocumentInformation documentInformation : dmsPushDocumentInformation.getSuccessDocumentInformation()) {
                    documentNames.add(documentInformation.getDocObjectId());

                }

            }

            return documentNames;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of getting pushed document information with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of getting pushed document information with probable cause [{}] ", e.getMessage()));

        }
    }

    @Override
    public long getFolderIdAgainstFolderName(String refId, String institutionId, String folderName) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            DMSCreatedFolderInformation dmsCreatedFolderInformation = mongoTemplate.findOne(query, DMSCreatedFolderInformation.class);

            long folderIndex = 0;

            if (null != dmsCreatedFolderInformation
                    && null != dmsCreatedFolderInformation.getFolderInformation()
                    && !dmsCreatedFolderInformation.getFolderInformation().isEmpty()) {

                for (FolderInformation folderInformation : dmsCreatedFolderInformation.getFolderInformation()) {
                    if (StringUtils.equals(folderInformation.getFolderName(), folderName)) {
                        folderIndex = folderInformation.getFolderIndex();
                        break;
                    }

                }
            }
            return folderIndex;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of getting folder Id against folder name ,refId  with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of getting folder Id against folder name ,refId  with probable cause [{}] ", e.getMessage()));

        }
    }

    @Override
    public void saveCreatedFolderInformation(DMSCreatedFolderInformation dmsCreatedFolderInformation) {

        try {
            Query query = new Query();
            query.addCriteria(
                    Criteria.where("_id").is(dmsCreatedFolderInformation.getRefId())
                            .and("institutionId").is(dmsCreatedFolderInformation.getInstitutionId()));

            Object[] folderInformationObjects = dmsCreatedFolderInformation.getFolderInformation().toArray();

            Update update = new Update();
            update.push("folderInformation").each(folderInformationObjects);

            mongoTemplate.upsert(query, update, DMSCreatedFolderInformation.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of saving DMSCreatedFolderInformation  with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception Occur at the time of saving DMSCreatedFolderInformation  with probable cause [{}]", e.getMessage()));
        }


    }

    @Override
    public void savePushDocumentInformation(DMSPushDocumentInformation dmsPushDocumentInformation) {

        try {
            Query query = new Query();
            query.addCriteria(
                    Criteria.where("_id").is(dmsPushDocumentInformation.getRefId())
                            .and("institutionId").is(dmsPushDocumentInformation.getInstitutionId()));

            // create a collection at first document push
            if (!mongoTemplate.exists(query, DMSPushDocumentInformation.class)) {

                if (!CollectionUtils.isEmpty(dmsPushDocumentInformation.getSuccessDocumentInformation())) {
                    dmsPushDocumentInformation.setSuccessDocumentCount(1);
                } else {
                    dmsPushDocumentInformation.setFailedDocumentCount(1);
                }

                dmsPushDocumentInformation.setTotalNoOfDocumentCount(1);

                mongoTemplate.insert(dmsPushDocumentInformation);

            } else {

                List<DocumentInformation> failedDocumentInformation = null;
                List<DocumentInformation> successDocumentInformation = null;
                int successCount = 0;
                int failedCount = 0;

                if (!CollectionUtils.isEmpty(dmsPushDocumentInformation.getSuccessDocumentInformation())) {

                    successDocumentInformation = dmsPushDocumentInformation.getSuccessDocumentInformation();

                } else {
                    failedDocumentInformation = dmsPushDocumentInformation.getFailedDocumentInformation();
                }

                DMSPushDocumentInformation existingDocumentInfo = mongoTemplate.findOne(query, DMSPushDocumentInformation.class);

                failedCount = existingDocumentInfo.getFailedDocumentCount();
                successCount = existingDocumentInfo.getSuccessDocumentCount();
                List<DocumentInformation> existingFailedDocInfo = existingDocumentInfo.getFailedDocumentInformation();


                Update update = new Update();

                if (!CollectionUtils.isEmpty(successDocumentInformation)) {

                    // check if current success document failed previously then decrement the previous failed document count
                    if (!CollectionUtils.isEmpty(existingFailedDocInfo)) {

                        if (checkDuplicateOfSuccessDoc(existingFailedDocInfo, successDocumentInformation.get(0).getDocObjectId())) {
                            update.set("failedDocumentCount", --failedCount);
                        }
                    }
                    update.push("successDocumentInformation").each(successDocumentInformation.toArray());
                    update.set("successDocumentCount", ++successCount);

                } else {
                    update.push("failedDocumentInformation").each(failedDocumentInformation.toArray());

                    // check if current Failed document failed previously then don't increment the failed document count
                    if (CollectionUtils.isEmpty(existingFailedDocInfo) || checkDuplicateOfFailedDoc(existingFailedDocInfo, failedDocumentInformation.get(0).getDocObjectId())) {

                        update.set("failedDocumentCount", ++failedCount);
                    }

                }

                update.set("totalNoOfDocumentCount", successCount + failedCount);

                mongoTemplate.updateFirst(query, update, DMSPushDocumentInformation.class);
            }

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of saving pushDocumentInformation  with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception Occur at the time of saving pushDocumentInformation  with probable cause [{}]", e.getMessage()));
        }

    }

    private boolean checkDuplicateOfSuccessDoc(List<DocumentInformation> existingfailedDocInfo, String successDocObjectId) {

        for (DocumentInformation information : existingfailedDocInfo) {
            if (StringUtils.equals(successDocObjectId, information.getDocObjectId())) {
                return true;
            }

        }
        return false;

    }

    private boolean checkDuplicateOfFailedDoc(List<DocumentInformation> existingfailedDocInfo, String failedDocId) {
        for (DocumentInformation information : existingfailedDocInfo) {
            if (StringUtils.equals(failedDocId, information.getDocObjectId())) {
                return false;
            }

        }
        return true;
    }

    @Override
    public boolean checkWFJobCommDomainConfiguration(String institutionId) {

        for (String urlType : DMSUtils.getUrlTypeForDMS()) {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("type").is(urlType));

            if (!mongoTemplate.exists(query, WFJobCommDomain.class)) {
                return false;
            }

        }

        return checkDmsFolderConfiguration(institutionId);

    }

    private boolean checkDmsFolderConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("enable").is(true));
        if (mongoTemplate.exists(query, DmsFolderConfiguration.class)) {
            return true;
        }
        return false;
    }

    @Override
    public byte[] getCibilReportBase64Code(String institutionId, String refId) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("applicationRequest.header.institutionId").is(institutionId));

            GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query, GoNoGoCustomerApplication.class);

            byte[] base64code = null;
            if (null != goNoGoCustomerApplication &&
                    null != goNoGoCustomerApplication.getApplicantComponentResponse()
                    && null != goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose()
                    && null != goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList()) {

                List<Finished> finishedList = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList();

                for (Finished finished : finishedList) {
                    if (StringUtils.equals(finished.getBureau(), GNGWorkflowConstant.CIBIL.toFaceValue())
                            && StringUtils.equals(finished.getStatus(), Status.SUCCESS.name())) {
                        base64code = finished.getPdfReport();
                        break;
                    }
                }
            }
            return base64code;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of fetching cibil report bytecode with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of fetching cibil report bytecode with probable cause [{}] ", e.getMessage()));

        }
    }


    @Override
    public DMSLogResponse getPushDocumentsInfo(String refId, String institutionId) {

        try {
            DMSLogResponse dmsLogResponse = new DMSLogResponse();
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            DMSCreatedFolderInformation dmsCreatedFolderInformation = mongoTemplate.findOne(query, DMSCreatedFolderInformation.class);
            if (null != dmsCreatedFolderInformation
                    && null != dmsCreatedFolderInformation.getFolderInformation()) {
                dmsLogResponse.setFolderInformationList(dmsCreatedFolderInformation.getFolderInformation());
            }
            DMSPushDocumentInformation dmsPushDocumentInformation = mongoTemplate.findOne(query, DMSPushDocumentInformation.class);
            if (null != dmsPushDocumentInformation) {
                dmsLogResponse.setDmsPushDocumentInformation(dmsPushDocumentInformation);

            }
            return dmsLogResponse;

        } catch (DataAccessException e) {

            e.printStackTrace();
            logger.error("Exception occur at the time of fetching DMS log with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of fetching  DMS log with probable cause [{}] ", e.getMessage()));

        }

    }


    @Override
    public boolean checkApplicationAgreementFormExistance(String refId, String institutionId) {

        Query agreementFormQuery = QueryBuilder.buildApplicationAgreementFormExistanceQuery(refId, institutionId, GNGWorkflowConstant.AGREEMENT_FORM.toFaceValue());

        Query applicationFormQuery = QueryBuilder.buildApplicationAgreementFormExistanceQuery(refId, institutionId, GNGWorkflowConstant.APPLICATION_FORM.toFaceValue());


        if (CollectionUtils.isEmpty(gridFsTemplate.find(agreementFormQuery))
                && CollectionUtils.isEmpty(gridFsTemplate.find(applicationFormQuery))) {
            return true;
        }

        return false;
    }

    @Override
    public DMSPushDocumentInformation getSuccessFailureCountOfPushDocumentsWithCause(String institutionId, String refId) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(refId)
                    .and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, DMSPushDocumentInformation.class);

        } catch (DataAccessException e) {

            e.printStackTrace();
            logger.error("Exception occur at the time of fetching dMSPushDocumentInformation with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of fetching dMSPushDocumentInformation  with probable cause [{}] ", e.getMessage()));

        }

    }

    @Override
    public void saveDmsErrorLog(PushDMSDocumentsRequest dmsErrorLog) {
        try {
            mongoTemplate.insert(dmsErrorLog);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of saving DmsErrorLog with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time of saving DmsErrorLog with probable cause [{}] ", e.getMessage()));


        }

    }

    @Override
    public List<PushDMSDocumentsRequest> getDMSErrorLog(String refId, String institutionId) {

        try {
            Query dmsErrorLog = new Query();

            dmsErrorLog.addCriteria(Criteria.where("header.institutionId").is(institutionId).and("refId").is(refId));

            return mongoTemplate.find(dmsErrorLog, PushDMSDocumentsRequest.class);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occur at the time of getting DmsErrorLog with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Exception occur at the time  of getting DmsErrorLog with probable cause [{}] ", e.getMessage()));


        }

    }
}

