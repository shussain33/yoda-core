package com.softcell.dao.mongodb.repository;

import com.mongodb.BasicDBObject;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * @author yogeshb
 */
public interface AnalyticsRepository {


    /**
     * @param stackRequest which consist of Reference id , Application id, Institution Id
     * @return {@link Object} score tree object
     */
    public Object fetchStackScoreData(Query query, StackRequest stackRequest) throws Exception;

    /**
     * @return
     */
    public List<GoNoGoCroApplicationResponse> getDsaReport() throws Exception;

    /**
     * method to count total document passes the criteria
     *
     * @param query
     * @return
     * @throws Exception
     */
    public long count(Query query) throws Exception;


    /**
     * @param aggregration
     * @param collection
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> fetchLoginStatusGraphData(Aggregation aggregration, String collection) throws Exception;


    /**
     * @param query
     * @param collection
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> fetchLoginStatusTableData(Aggregation query, String collection) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getStageWiseLoginReport(Aggregation aggregation) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getLoginDealersWise(Aggregation aggregation) throws Exception;


    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getAssetManufactureReport(
            Aggregation aggregation) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getUniqueAssetManufacturers(
            Aggregation aggregation) throws Exception;

    /**
     * @param query
     * @param collection
     * @return
     * @throws Exception
     */
    List getRoleBasedDistinctFields(Query query, String collection, String field) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getDailyDisbursalReport(
            Aggregation aggregation) throws Exception;

    /**
     * @param aggregation
     * @return
     * @throws Exception
     */
    public AggregationResults<BasicDBObject> getPostIPARequest(Aggregation aggregation) throws  Exception;


}