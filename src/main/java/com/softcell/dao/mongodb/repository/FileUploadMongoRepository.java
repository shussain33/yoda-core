package com.softcell.dao.mongodb.repository;

import com.mongodb.WriteResult;
import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.elsearch.IndexingElasticSearchRepository;
import com.softcell.dao.mongodb.repository.elsearch.IndexingRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.PostIpaRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */
@Repository
public class FileUploadMongoRepository implements FileUploadRepository {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    private IndexingRepository indexingRepository;


    @Override
    public boolean insertSchemeMaster(List<SchemeMasterData> schemeMasterList) {
        try {

            if (null == schemeMasterList || schemeMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(schemeMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate
                        .updateMulti(query, update, SchemeMasterData.class);

            mongoTemplate.insert(schemeMasterList, SchemeMasterData.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertAssetModelMasterList(
            List<AssetModelMaster> assetModelMasterList) {
        try {
            if (null == assetModelMasterList
                    || assetModelMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(assetModelMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate
                        .updateMulti(query, update, AssetModelMaster.class);

            mongoTemplate.insert(assetModelMasterList, AssetModelMaster.class);
            return true;
        } catch (Exception exp) {
            exp.printStackTrace();
            return false;

        }
    }

    @Override
    public boolean insertPinCodeMasterList(List<PinCodeMaster> pinCodeMasterList) {
        try {

            if (null == pinCodeMasterList || pinCodeMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(pinCodeMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, PinCodeMaster.class);

            mongoTemplate.insert(pinCodeMasterList, PinCodeMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertEmployerMasterList(
            List<EmployerMaster> employerMasterList) {
        try {
            if (null == employerMasterList || employerMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(employerMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, EmployerMaster.class);


            mongoTemplate.insert(employerMasterList, EmployerMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertDealerEmailMasterList(
            List<DealerEmailMaster> dealerEmailMasterList) {
        try {

            if (null == dealerEmailMasterList
                    || dealerEmailMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(dealerEmailMasterList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        DealerEmailMaster.class);

            mongoTemplate
                    .insert(dealerEmailMasterList, DealerEmailMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertGNGDealerEmailMasterList(
            List<GNGDealerEmailMaster> gngDealerEmailMasterList) {
        try {

            if (null == gngDealerEmailMasterList
                    || gngDealerEmailMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(gngDealerEmailMasterList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        GNGDealerEmailMaster.class);

            mongoTemplate.insert(gngDealerEmailMasterList,
                    GNGDealerEmailMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertSchemeModelDealerCityMappingMasterList(
            List<SchemeModelDealerCityMapping> schemeModelDealerCityMappingMasterList) {
        try {

            if (null == schemeModelDealerCityMappingMasterList
                    || schemeModelDealerCityMappingMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria
                        .where("institutionID")
                        .is(schemeModelDealerCityMappingMasterList.get(0)
                                .getInstitutionID()).and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        SchemeModelDealerCityMapping.class);

            mongoTemplate.insert(schemeModelDealerCityMappingMasterList,
                    SchemeModelDealerCityMapping.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertSchemeDateMappingMasterList(
            List<SchemeDateMapping> schemeDateMappingList) {
        try {

            if (null == schemeDateMappingList
                    || schemeDateMappingList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(schemeDateMappingList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        SchemeDateMapping.class);

            mongoTemplate
                    .insert(schemeDateMappingList, SchemeDateMapping.class);
            return true;
        } catch (Exception exp) {
            exp.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean insertHitachiSchemeMasterList(
            List<HitachiSchemeMaster> hitachiSchemeMasterList) {
        try {

            if (null == hitachiSchemeMasterList
                    || hitachiSchemeMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(hitachiSchemeMasterList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        HitachiSchemeMaster.class);

            mongoTemplate.insert(hitachiSchemeMasterList,
                    HitachiSchemeMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertCarSurrogateMasterList(
            List<CarSurrogateMaster> carSurrogateMasterList) {
        try {
            if (null == carSurrogateMasterList
                    || carSurrogateMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(carSurrogateMasterList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        CarSurrogateMaster.class);

            mongoTemplate.insert(carSurrogateMasterList,
                    CarSurrogateMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertPlPincodeEmailMasterList(
            List<PlPincodeEmailMaster> plPincodeEmailMasterList) {
        try {
            if (null == plPincodeEmailMasterList
                    || plPincodeEmailMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionID")
                        .is(plPincodeEmailMasterList.get(0).getInstitutionID())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        PlPincodeEmailMaster.class);

            mongoTemplate.insert(plPincodeEmailMasterList,
                    PlPincodeEmailMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertBankDetailsMasterList(
            List<BankDetailsMaster> bankDetailsMasterList) {
        try {
            if (null == bankDetailsMasterList
                    || bankDetailsMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(bankDetailsMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        BankDetailsMaster.class);

            mongoTemplate
                    .insert(bankDetailsMasterList, BankDetailsMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertHierarchyMasterList(
            List<HierarchyMaster> hierarchyMasterList) {
        try {
            if (null == hierarchyMasterList || hierarchyMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(hierarchyMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, HierarchyMaster.class);

            mongoTemplate.insert(hierarchyMasterList, HierarchyMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertCDLHierarchyMasterList(
            List<CDLHierarchyMaster> cdlHierarchyMasterList) {
        try {
            if (null == cdlHierarchyMasterList
                    || cdlHierarchyMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(cdlHierarchyMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        CDLHierarchyMaster.class);

            mongoTemplate.insert(cdlHierarchyMasterList,
                    CDLHierarchyMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertCityStateMasterList(
            List<CityStateMappingMaster> cityStateMasterList) {

        try {
            if (null == cityStateMasterList || cityStateMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(cityStateMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        CityStateMappingMaster.class);

            mongoTemplate.insert(cityStateMasterList,
                    CityStateMappingMaster.class);
            return true;

        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertModelVariantMasterList(
            List<ModelVariantMaster> modelVariantMasterList) {

        try {
            if (null == modelVariantMasterList
                    || modelVariantMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(modelVariantMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        ModelVariantMaster.class);

            mongoTemplate.insert(modelVariantMasterList,
                    ModelVariantMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    public boolean insertAssetCategoryMasterList(
            List<AssetCategoryMaster> assetCatgMasterList) {
        try {

            if (null == assetCatgMasterList || assetCatgMasterList.size() == 0) {
                return false;
            }

                {
                    Query query = new Query();
                    query.addCriteria(Criteria.where("institutionId")
                            .is(assetCatgMasterList.get(0).getInstitutionId())
                            .and("active").is(true));
                    Update update = new Update();
                    update.set("active", false);
                    mongoTemplate.updateMulti(query, update,
                            AssetCategoryMaster.class);
                }

            mongoTemplate
                    .insert(assetCatgMasterList, AssetCategoryMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }

    }

    @Override
    public boolean insertStateMasterList(List<StateMaster> stateMasterList) {
        try {
            if (null == stateMasterList || stateMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(stateMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, StateMaster.class);

            mongoTemplate.insert(stateMasterList, StateMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }

    }

    @Override
    public boolean insertCityMasterList(List<CityMaster> cityMasterList) {

        try {
            if (null == cityMasterList || cityMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(cityMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, CityMaster.class);

            mongoTemplate.insert(cityMasterList, CityMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }

    }

    @Override
    public boolean insertCreditPromoMasterList(
            List<CreditPromotionMaster> creditPromoMasterList) {

        try {
            if (null == creditPromoMasterList
                    || creditPromoMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(creditPromoMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        CreditPromotionMaster.class);

            mongoTemplate.insert(creditPromoMasterList,
                    CreditPromotionMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }

    }

    @Override
    public boolean insertDsaBranchMasterList(
            List<DSABranchMaster> dsaBranchMasterList) {
        try {
            if (null == dsaBranchMasterList || dsaBranchMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(dsaBranchMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, DSABranchMaster.class);

            mongoTemplate.insert(dsaBranchMasterList, DSABranchMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertDsaProductMasterList(
            List<DSAProductMaster> dsaProductMasterList) {
        try {
            if (null == dsaProductMasterList
                    || dsaProductMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(dsaProductMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate
                        .updateMulti(query, update, DSAProductMaster.class);

            mongoTemplate.insert(dsaProductMasterList, DSAProductMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertDealerBranchMasterList(
            List<DealerBranchMaster> dealerBranchMasterList) {
        try {
            if (null == dealerBranchMasterList
                    || dealerBranchMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(dealerBranchMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        DealerBranchMaster.class);

            mongoTemplate.insert(dealerBranchMasterList,
                    DealerBranchMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertNegativeAreaFundingMaster(
            List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasterList) {

        try {
            if (null == negativeAreaGeoLimitMasterList
                    || negativeAreaGeoLimitMasterList.size() == 0) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("institutionId")
                        .is(negativeAreaGeoLimitMasterList.get(0)
                                .getInstitutionId()).and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        NegativeAreaGeoLimitMaster.class);

            mongoTemplate.insert(negativeAreaGeoLimitMasterList,
                    NegativeAreaGeoLimitMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }

    }

    @Override
    public boolean updateLosDetails(LosUtrDetailsMaster losUtrDetailsMasterRequest) {

        logger.debug("updateLosDetails repo started");

        try {
            Query query = new Query();
            Update update = new Update();

            query.addCriteria(Criteria
                    .where("_id")
                    .is(losUtrDetailsMasterRequest.getRefID())
                    .and("applicationRequest.header.institutionId")
                    .is(losUtrDetailsMasterRequest.getInstitutionID()).and("applicationStatus").is(GNGWorkflowConstant.APPROVED.toFaceValue()));

            update.set("losDetails.losID", losUtrDetailsMasterRequest.getLosID());
            update.set("losDetails.utrNumber", losUtrDetailsMasterRequest.getUtrNumber());
            update.set("losDetails.status", losUtrDetailsMasterRequest.getLosStatus());
            update.set("applicationRequest.currentStageId", losUtrDetailsMasterRequest.getLosStatus());

            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);
            updateApplicationInElasticsearch(goNoGoCroApplicationResponse);

            logger.debug("updateLosDetails repo saved los status successfully ");

            return true;
        } catch (Exception e) {
            logger.error("Error occurred while updating los details " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    losUtrDetailsMasterRequest.getInstitutionID(), losUtrDetailsMasterRequest.getRefID(), e.getMessage());

            return false;
        }
    }

    private void updateApplicationInElasticsearch(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse) {
        try {
            indexingRepository = new IndexingElasticSearchRepository();
            indexingRepository.indexNotification(goNoGoCroApplicationResponse);
        } catch (Exception e) {
            logger.error("Error occurred while updating application in elasticsearch " +
                            "for institutionId {} and refId {} with probable cause [{}] ",
                    goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId(),
                    goNoGoCroApplicationResponse.getGngRefId(), e.getMessage());
        }
    }


    @Override
    public boolean insertLosMasterDetails(LosUtrDetailsMaster losUtrDetailsMasterRequest) {

        logger.info("insertLosMasterDetails repo started Successfully");

        try {

            mongoTemplate.insert(losUtrDetailsMasterRequest);
            logger.info("LosRecord Successfully Inserted");

            return true;
        } catch (Exception exp) {
            logger.error("Error occurred while inserting los details " +
                            " for institutionId {} and refId {} with probable cause [{}] ",
                    losUtrDetailsMasterRequest.getInstitutionID(), losUtrDetailsMasterRequest.getRefID(), exp.getMessage());
            return false;
        }
    }


    @Override
    public boolean insertRelianceDealerBranchMasterList(List<RelianceDealerBranchMaster> dealerBranchMasterList) {

        logger.debug("insertRelianceDealerBranchMasterList repo started");

        try {
            if (null == dealerBranchMasterList
                    || dealerBranchMasterList.isEmpty()) {
                return false;
            }



                Query query = new Query();
                query.addCriteria(Criteria
                        .where("institutionId")
                        .is(dealerBranchMasterList.get(0)
                                .getInstitutionId()).and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        RelianceDealerBranchMaster.class);


            mongoTemplate.insert(dealerBranchMasterList,
                    RelianceDealerBranchMaster.class);

            return true;
        } catch (Exception e) {

            logger.error(e.getMessage());

            return false;

        }

    }

    @Override
    public boolean insertReferenceDetailsMasters(List<ReferenceDetailsMaster> referenceDetailsMasters) {
        logger.debug("insertReferenceDetailsMasters repo started");

        try {
            if (null == referenceDetailsMasters
                    || referenceDetailsMasters.isEmpty()) {
                return false;
            }



                Query query = new Query();
                query.addCriteria(Criteria
                        .where("institutionId").is(referenceDetailsMasters.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        ReferenceDetailsMaster.class);


            mongoTemplate.insert(referenceDetailsMasters,
                    ReferenceDetailsMaster.class);

            return true;

        } catch (Exception e) {

            logger.error(e.getMessage());

            return false;

        }
    }

    @Override
    public boolean insertBankingDetailsMasterList(List<BankDetailsMaster> bankDetailsMasterList) {
        logger.debug("insertReferenceDetailsMasters repo started");

        try {
            if (null == bankDetailsMasterList
                    || bankDetailsMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(bankDetailsMasterList.get(0).getInstitutionId()));
                mongoTemplate.remove(query,
                        BankDetailsMaster.class);


            mongoTemplate.insert(bankDetailsMasterList,
                    BankDetailsMaster.class);

            return true;

        } catch (Exception e) {

            logger.error("Error occurred while inserting banking master with probable cause [{}]", e.getMessage());

            return false;

        }
    }

    public boolean insertLoyaltyCardMasterList(List<LoyaltyCardMaster> loyaltyCardMastersList) {

        try {

            if (null == loyaltyCardMastersList || loyaltyCardMastersList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(loyaltyCardMastersList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate
                        .updateMulti(query, update, LoyaltyCardMaster.class);

            mongoTemplate.insert(loyaltyCardMastersList, LoyaltyCardMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting loyalty card master for institutionId {}",
                    loyaltyCardMastersList.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting LoyaltyCard Master with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public boolean insertCommonGeneralMaster(List<CommonGeneralParameterMaster> commonGeneralParameterMasterList) {

        try {

            if (null == commonGeneralParameterMasterList || commonGeneralParameterMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(commonGeneralParameterMasterList.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, CommonGeneralParameterMaster.class);


            mongoTemplate.insert(commonGeneralParameterMasterList, CommonGeneralParameterMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting Common General Parameter master for institutionId {}",
                    commonGeneralParameterMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while inserting Common General Parameter Master with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertLosBankMasterList(List<LosBankMaster> losBankMasters) {
        try {

            if (null == losBankMasters || losBankMasters.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(losBankMasters.get(0).getInstitutionId())
                        .and("status").is(true));
                Update update = new Update();
                update.set("status", false);
                mongoTemplate
                        .updateMulti(query, update, LosBankMaster.class);

            mongoTemplate.insert(losBankMasters, LosBankMaster.class);
            return true;

        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting loyalty card master for institutionId {}",
                    losBankMasters.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting LoyaltyCard Master with probable cause [%s]", e.getMessage()));

        }

    }

    @Override
    public boolean insertEwInsGngAssetCategoryMaster(List<EwInsGngAssetCategoryMaster> ewInsGngAssetCategoryMasterList) {
        try {

            if (null == ewInsGngAssetCategoryMasterList || ewInsGngAssetCategoryMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(ewInsGngAssetCategoryMasterList.get(0).getInstitutionId())
                        .and("active").is(true));

                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, EwInsGngAssetCategoryMaster.class);

            mongoTemplate.insert(ewInsGngAssetCategoryMasterList, EwInsGngAssetCategoryMaster.class);
            return true;

        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting EwInsGngAssetCategoryMaster for institutionId {}",
                    ewInsGngAssetCategoryMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting EwInsGngAssetCategoryMaster with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public boolean insertEwPremiumDataMaster(List<EWPremiumMaster> ewPremiumMasterList) {

        try {

            if (null == ewPremiumMasterList || ewPremiumMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(ewPremiumMasterList.get(0).getInstitutionId())
                        .and("active").is(true));

                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, EWPremiumMaster.class);

            mongoTemplate.insert(ewPremiumMasterList, EWPremiumMaster.class);
            return true;

        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting EWPremiumMaster for institutionId {}",
                    ewPremiumMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting EWPremiumMaster with probable cause [%s]", e.getMessage()));

        }

    }


    @Override
    public boolean insertInsurancePremiumMaster(List<InsurancePremiumMaster> insurancePremiumMasterList) {


        try {

            if (null == insurancePremiumMasterList || insurancePremiumMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(insurancePremiumMasterList.get(0).getInstitutionId())
                        .and("active").is(true));

                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, InsurancePremiumMaster.class);

            mongoTemplate.insert(insurancePremiumMasterList, InsurancePremiumMaster.class);
            return true;

        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting InsurancePremiumMaster for institutionId {}",
                    insurancePremiumMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting InsurancePremiumMaster with probable cause [%s]", e.getMessage()));

        }

    }

    @Override
    public boolean insertElecServProvMaster(List<ElecServProvMaster> elecServProvMasterList) {
        try {
            if (null == elecServProvMasterList || elecServProvMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(elecServProvMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate
                        .updateMulti(query, update, ElecServProvMaster.class);

            mongoTemplate.insert(elecServProvMasterList, ElecServProvMaster.class);
            return true;
        } catch (Exception exp) {
            logger.error("Error occurred while inserting ElecServProvMaster for institutionId {}",
                    elecServProvMasterList.get(0).getInstitutionId());
            return false;
        }
    }

    @Override
    public boolean insertBranchMasterList(List<BranchMaster> branchMasterList) {

        logger.debug("insertBranchMasterList repo started");

        try {
            if (null == branchMasterList || branchMasterList.isEmpty()) {
                return false;
            }



                Query query = new Query();

                query.addCriteria(Criteria.where("institutionId")
                        .is(branchMasterList.get(0).getInstitutionId()).and("active").is(true));

                Update update = new Update();
                update.set("active", false);

                mongoTemplate.updateMulti(query, update, BranchMaster.class);


            mongoTemplate.insert(branchMasterList, BranchMaster.class);

            return true;
        } catch (Exception e) {

            logger.error(e.getMessage());

            return false;

        }
    }

    @Override
    public boolean insertTkilDealerBranchMasterList(List<TkilDealerBranchMaster> dealerBranchMasterList) {
        logger.debug("insertTkilDealerBranchMasterList repo started");

        try {
            if(CollectionUtils.isEmpty(dealerBranchMasterList)){
                return false;
            }



                Query query = new Query();
                query.addCriteria(Criteria
                        .where("institutionId")
                        .is(dealerBranchMasterList.get(0)
                                .getInstitutionId()).and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update,
                        TkilDealerBranchMaster.class);


            mongoTemplate.insert(dealerBranchMasterList,
                    TkilDealerBranchMaster.class);

            return true;
        } catch (DataAccessException e) {
            logger.error("Error occurred while inserting tkil dealer master for institutionId {}",
                    dealerBranchMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format(" error occurred  while inserting tkil dealer master with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public boolean insertTaxCodeMaster(List<TaxCodeMaster> taxCodeMasterList) {
        try {
            if (null == taxCodeMasterList || taxCodeMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(taxCodeMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, TaxCodeMaster.class);

            mongoTemplate.insert(taxCodeMasterList, TaxCodeMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertAccountsHeadMaster(List<AccountsHeadMaster> accountsHeadMasterList) {
        try {
            if (null == accountsHeadMasterList || accountsHeadMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(accountsHeadMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, AccountsHeadMaster.class);

            mongoTemplate.insert(accountsHeadMasterList, AccountsHeadMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertStateBranchMaster(List<StateBranchMaster> stateBranchMasterList) {
        try {
            if (null == stateBranchMasterList || stateBranchMasterList.size() == 0) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId")
                        .is(stateBranchMasterList.get(0).getInstitutionId())
                        .and("active").is(true));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, StateBranchMaster.class);

            mongoTemplate.insert(stateBranchMasterList, StateBranchMaster.class);
            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    @Override
    public boolean insertSourcingDetailMaster(List<SourcingDetailMaster> sourcingDetailMasterList) {

        try {

            if (null == sourcingDetailMasterList || sourcingDetailMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(sourcingDetailMasterList.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, SourcingDetailMaster.class);


            mongoTemplate.insert(sourcingDetailMasterList, SourcingDetailMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting sourcing detail Parameter master for institutionId {}",
                    sourcingDetailMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while inserting sourcing detail Parameter Master with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertPromotionSchemeMaster(List<PromotionalSchemeMaster> promotionSchemeMasterList) {

        try {

            if (null == promotionSchemeMasterList || promotionSchemeMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(promotionSchemeMasterList.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, PromotionalSchemeMaster.class);


            mongoTemplate.insert(promotionSchemeMasterList, PromotionalSchemeMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting promotional scheme Parameter master for institutionId {}",
                    promotionSchemeMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while inserting promotional scheme Parameter Master with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertBankMaster(List<BankMaster> bankMasterList) {

        try {

            if (null == bankMasterList || bankMasterList.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(bankMasterList.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, BankMaster.class);


            mongoTemplate.insert(bankMasterList, BankMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting Bank master for institutionId {}",
                    bankMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while Bank Master with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertTCLosGngMappingMaster(List<TCLosGngMappingMaster> tcLosGngMappings) {

        try {

            if (null == tcLosGngMappings || tcLosGngMappings.isEmpty()) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(tcLosGngMappings.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, TCLosGngMappingMaster.class);


            mongoTemplate.insert(tcLosGngMappings, TCLosGngMappingMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting TCLosGngMappings for institutionId {}",
                    tcLosGngMappings.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  inserting TCLosGngMappings while Bank Master with probable cause [%s]", e.getMessage()));
        }
    }

    public boolean insertManufacturers(List<ManufacturerMaster> manufacturers)
    {
        try {
            if (null == manufacturers || manufacturers.isEmpty()) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(manufacturers.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, ManufacturerMaster.class);

            mongoTemplate.insert(manufacturers, ManufacturerMaster.class);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting ManufacturerMaster for institutionId {}",
                    manufacturers.get(0).getInstitutionId());
            throw new SystemException(String.format("Error occurred  inserting ManufacturerMaster with probable cause [%s]", e.getMessage()));
        }
    }


    @Override
    public boolean updateNetDisbursalAmount(NetDisbursalDetailsMaster netDisbursalDetailsMaster) {
        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(netDisbursalDetailsMaster.getRefID())
                    .and("header.institutionId").is(netDisbursalDetailsMaster.getInstitutionID()));

            Update update = new Update();
            update.set("postIPA.netDisbursalAmount", netDisbursalDetailsMaster.getNewNetDisbursalAmt());

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, PostIpaRequest.class);

            logger.info("Write Result:->", writeResult);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occured while updating netDisbursalAmount amount in postIpa collection with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("error occured while updating netDisbursalAmount amount in postIpa collection with probable cause [{%s}]", e.getMessage()));
        }
    }


    @Override
    public boolean insertNetDisbursalDetails(NetDisbursalDetailsMaster netDisbursalDetailsMaster) {
        try {

            mongoTemplate.insert(netDisbursalDetailsMaster);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting netDisbursalDetails for institutionId {}");
            throw new SystemException(String.format("Error occurred  inserting netDisbursalDetails with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertSupplierLocationMaster(List<SupplierLocationMaster> supplierLocationMasterList) {
        try {
            if (null == supplierLocationMasterList || supplierLocationMasterList.isEmpty()) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(supplierLocationMasterList.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);
                mongoTemplate.updateMulti(query, update, SupplierLocationMaster.class);

            mongoTemplate.insert(supplierLocationMasterList, SupplierLocationMaster.class);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting SupplierLocationMaster for institutionId {}",
                    supplierLocationMasterList.get(0).getInstitutionId());
            throw new SystemException(String.format("Error occurred  inserting SupplierLocationMaster with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertPaynimoBankMasterList(List<PaynimoBankMaster> paynimoBankMasterList) {

            try {

                if (null == paynimoBankMasterList || paynimoBankMasterList.isEmpty()) {
                    return false;
                }


                    Query query = new Query();
                    query.addCriteria(Criteria
                            .where("active").is(true)
                            .and("institutionId").is(paynimoBankMasterList.get(0).getInstitutionId()));
                    Update update = new Update();
                    update.set("active", false);

                    mongoTemplate
                            .updateMulti(query, update, PaynimoBankMaster.class);


                mongoTemplate.insert(paynimoBankMasterList, PaynimoBankMaster.class);
                return true;

            } catch (DataAccessException e) {
                e.printStackTrace();
                logger.error("Error occurred while inserting Paynimo bank master for institutionId {}",
                        paynimoBankMasterList.get(0).getInstitutionId());
                throw new SystemException(String.format("error occurred  while inserting Paynimo bank master with probable cause [%s]", e.getMessage()));
            }
    }

    @Override
    public boolean insertDigitizationEmailMaster(List<DigitizationDealerEmailMaster> digitizationEmailMaster) {
        try {

            if (CollectionUtils.isEmpty(digitizationEmailMaster)) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("institutionId").is(digitizationEmailMaster.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("updateDate",new Date());
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, DigitizationDealerEmailMaster.class);


            mongoTemplate.insert(digitizationEmailMaster, DigitizationDealerEmailMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting digitizationEmailMaster  for institutionId {}",
                    digitizationEmailMaster.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while inserting digitizationEmailMaster with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertDeviationMaster(List<DeviationMaster> deviationMasters) {
        try {

            if (CollectionUtils.isEmpty(deviationMasters)) {
                return false;
            }


                Query query = new Query();
                query.addCriteria(Criteria
                        .where("active").is(true)
                        .and("product").is(deviationMasters.get(0).getProduct())
                        .and("institutionId").is(deviationMasters.get(0).getInstitutionId()));
                Update update = new Update();
                update.set("active", false);

                mongoTemplate
                        .updateMulti(query, update, DeviationMaster.class);


            mongoTemplate.insert(deviationMasters, DeviationMaster.class);
            return true;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting deviationMaster  for institutionId {}",
                    deviationMasters.get(0).getInstitutionId());
            throw new SystemException(String.format("error occurred  while inserting deviationMaster with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertOrganizationalHierarchyMaster(List<OrganizationalHierarchyMaster> organizationalHierarchyMasterList,
                                                       String institutionId) {
        try {

            if (CollectionUtils.isEmpty(organizationalHierarchyMasterList)) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId").is(institutionId));
                mongoTemplate.remove(query, OrganizationalHierarchyMaster.class);


            mongoTemplate.insert(organizationalHierarchyMasterList, OrganizationalHierarchyMaster.class);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting OrganizationalHierarchyMaster  for institutionId {}");
            throw new SystemException(String.format("error occurred  while inserting OrganizationalHierarchyMaster with probable cause [%s]", e.getMessage()));
        }


    }

    @Override
    public boolean insertRoiSchemeMaster(List<RoiSchemeMaster> roiSchemeMasters,
                                         String institutionId, String product) {
        try {
            if (CollectionUtils.isEmpty(roiSchemeMasters)) {
                return false;
            }

                Query query = new Query();
                query.addCriteria(Criteria.where("institutionId").is(institutionId)
                        .and("product").is(product));
                mongoTemplate.remove(query, RoiSchemeMaster.class);

            mongoTemplate.insert(roiSchemeMasters, RoiSchemeMaster.class);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error occurred while inserting RoiSchemeMaster  for institutionId {}");
            throw new SystemException(String.format("error occurred  while inserting RoiSchemeMaster with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public boolean insertApiRoleAuthorisationMaster(List<ApiRoleAuthorisationMaster> apiRoleAuthorisationMasters, String institutionId,String sourceId) {
        try {
            if (CollectionUtils.isEmpty(apiRoleAuthorisationMasters)) {
                return false;
            }
            Query query = new Query();
            query.addCriteria(Criteria
                    .where("institutionId").is(institutionId)
                    .and(Constant.SOURCE_ID).is(sourceId)
                    .and(Constant.ACTIVE).is(true));

            Update update = new Update();
            update.set(Constant.ACTIVE, false);

            mongoTemplate.updateMulti(query, update, ApiRoleAuthorisationMaster.class);
            mongoTemplate.insert(apiRoleAuthorisationMasters, ApiRoleAuthorisationMaster.class);

            return true;
        } catch (DataAccessException e) {
            logger.error("Error occurred while insertApiRoleAuthorisationMaster for institutionId as {}", e);
            return false;
        }
    }
}
