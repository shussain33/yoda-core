package com.softcell.dao.mongodb.repository.systemConfiguration;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.gonogo.model.configuration.admin.InstitutionConfig;
import com.softcell.gonogo.model.configuration.admin.IntimationConfiguration;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.request.TemplateRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by amit on 30/5/18.
 */
public interface AppConfigurationRepository {

    public void save(Object object);

    void saveAll(List<LoginServiceResponse> list, Class classname);

    void saveAll(Class classname, List<?> list);

    void removeAllUsers(Class className, String institutionId);

    void update(LoginServiceResponse user);

    Object fetch(Class<?> classz, String id);

    public boolean saveHierarchyMaster(String institutionId, List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List);

    public List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId);

    List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId, String productName);

    List<LoginServiceResponse> fetchUserDetails(String institutionId, String productName);

    List<LoginServiceResponse> fetchUserDetails(String institutionId, String searchType, String[] searchCriteria);

    OrganizationalHierarchyMasterV2 fetchHierarchyMasterForProductBranch(String institutionId,
                                                                         String productName, int branchCode);

    List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId,
                                   List<String> branchNameList);

    List<OrganizationalHierarchyMasterV2> fetchHierarchyMaster(String institutionId, String productName, String role);

    List<IntimationConfiguration> fetchIntimitionConfiguration(String institutionId);

    public IntimationConfiguration fetchIntimitionConfiguration(String institutionId, String product);

    void removeIntimationConfiguration(Class<IntimationConfiguration> intimationConfigurationClass, String institutionId, String productName);

    void updateIntimation(IntimationConfiguration intimationConfiguration);

    public InstitutionConfig fetchInstitutionConfiguration(String institutionId);

    InstitutionConfig fetchInstitutionConfiguration(String institutionId, String configType);

    //template

    List<TemplateDetails> findTemplateDetailsByInstituteId(String instituteId);

    Boolean saveTemplateDetail(TemplateRequest templateRequest);

    TemplateDetails findTemplateDetailsByInstituteIdAndTemplateName(String institutionId, String productName,
                                                                    String templateName);

    TemplateDetails findTemplateDetailsByInstituteIdAndTemplateKey(String institutionId, String productName,
                                                                   String templateKey);

    boolean deleteTemplate(TemplateDetails templateDetails);
    Map<String,String>  getTemplateConfigrationData(String refId, List<String> fields, String instituteId, String verificationType ) throws Exception;

    List<TemplateConfiguration> findVelocityTemplateByInstitution(String instituteId);

    LoginServiceResponse fetchLoggedUserDetail(String institutionId, String productName, String branchId,String role, String loginId);

    TemplateDetails findTemplateDetailsByInstituteIdAndProduct(String institutionId, String productName);

    boolean checkCacheEnable();
}
