package com.softcell.dao.mongodb.repository.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

@Repository
public class ApplicationHealthMongoRepository implements
        ApplicationHealthRepository {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationHealthMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public ApplicationHealthMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Object getStats() throws Exception {

        logger.debug("Health repo started for fetching stats");

        try {

            return mongoTemplate.getDb().getStats();

        } catch (Exception e) {

            logger.error("Error while fetching stats in health repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching stats in health repo possible cause {%s}", e.getMessage()));

        }

    }

    @Override
    public Object getCollectionNames() throws Exception {

        logger.debug("Health repo started for fetching collections ");

        try {

            return mongoTemplate.getDb().getCollectionNames();

        } catch (Exception e) {

            logger.error("Error while fetching collections in health repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching collections in health repo possible cause {%s}", e.getMessage()));

        }
    }

    @Override
    public Object getServerStatusInfo() throws Exception {

        logger.debug("Health repo started for fetching server status");

        try {
            return mongoTemplate.getDb().command("serverStatus");

        } catch (Exception e) {

            logger.error("Error while fetching server status in health repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching server status in health repo possible cause {%s}", e.getMessage()));

        }

    }

    @Override
    public Object getHostInfo() throws Exception {

        logger.debug("Health repo started for fetching host info");

        try {

            return mongoTemplate.getDb().command("hostInfo");

        } catch (Exception e) {

            logger.error("Error while fetching host info in health repo possible cause {}", e.getMessage());

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Error while fetching host info in health repo possible cause {%s}", e.getMessage()));

        }
    }
}
