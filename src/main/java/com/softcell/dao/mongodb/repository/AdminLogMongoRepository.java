package com.softcell.dao.mongodb.repository;

import com.mongodb.WriteResult;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.config.ReportEmailConfiguration;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.DigitizationDealerEmailMaster;
import com.softcell.gonogo.model.masters.GNGDealerEmailMaster;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.gonogo.model.request.AdminLogRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author yogeshb
 */
@Repository
public class AdminLogMongoRepository implements AdminLogRepository {

    private static final Logger logger = LoggerFactory.getLogger(AdminLogMongoRepository.class);

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ApplicationMongoRepository applicationRepository;


    public AdminLogMongoRepository(MongoTemplate mongoTemplate, GridFsTemplate gridFsTemplate)  {
        this.gridFsTemplate = gridFsTemplate;
        this.mongoTemplate = mongoTemplate;
        this.applicationRepository = new ApplicationMongoRepository();
    }

    @Override
    public PanRequest getPanLog(String refID) throws Exception {

        logger.debug("getPanLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, PanRequest.class);

        } catch (Exception e) {

            logger.error("getPanLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public AadhaarRequest getAadharLog(String refID) throws Exception {

        logger.debug("getAadharLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, AadhaarRequest.class);

        } catch (Exception e) {

            logger.error("getAadharLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.softcell.dao.mongodb.ConfiguartionRepository#getScoringLog
     * (java.lang.String)
     */
    @Override
    public ScoringApplicationRequest getScoringLog(String refID) throws Exception {

        logger.debug("getScoringLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, ScoringApplicationRequest.class);

        } catch (Exception e) {

            logger.error("getScoringLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }


    @Override
    public RequestJsonDomain getMbLog(String refID) throws Exception {

        logger.debug("getMbLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, RequestJsonDomain.class);

        } catch (Exception e) {

            logger.error("getMbLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public ApplicationRequest getMainLog(String refID) throws Exception {

        logger.debug("getMainLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, ApplicationRequest.class);

        } catch (Exception e) {

            logger.error("getMainLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<SerialNumberInfo> getSerialByIDLog(String refID, String vendor) throws Exception {

        logger.debug("getSerialByIDLog repo is started ");

        try {

            Criteria[] andCriterias = new Criteria[2];

            if (null != refID) {
                andCriterias[0] = Criteria.where("refID").is(refID);
            }

            if (null != vendor) {
                andCriterias[1] = Criteria.where("vendor").is(vendor);
            }

            Query query = new Query();

            query.addCriteria(new Criteria().andOperator(andCriterias));

            logger.debug("getSerialByIDLog repo formed query {}", query);

            return mongoTemplate.find(query, SerialNumberInfo.class);

        } catch (Exception e) {

            logger.error("getSerialByIDLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<SerialNumberInfo> getSerialLog() throws Exception {

        logger.debug("getSerialLog repo is started ");

        try {

            return mongoTemplate.findAll(SerialNumberInfo.class);

        } catch (Exception e) {

            logger.error("getSerialLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<SerialSaleConfirmationRequest> getSerialNumberRequestLog() throws Exception {

        logger.debug("getSerialNumberRequestLog repo is started ");

        try {

            return mongoTemplate.findAll(SerialSaleConfirmationRequest.class);

        } catch (Exception e) {

            logger.error("getSerialNumberRequestLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<SerialSaleConfirmationRequest> getSerialNumberRequestLog(
            String refID, String vendor) throws Exception {

        logger.debug("getSerialNumberRequestLog repo is started ");

        try {

            Criteria[] andCriterias = new Criteria[2];

            if (null != refID) {
                andCriterias[0] = Criteria.where("referenceID").is(refID);
            }

            if (null != vendor) {
                andCriterias[1] = Criteria.where("vendor").is(vendor);
            }

            Query query = new Query();

            query.addCriteria(new Criteria().andOperator(andCriterias));

            logger.debug("getSerialNumberRequestLog repo formed query {}", query);

            return mongoTemplate.find(query, SerialSaleConfirmationRequest.class);

        } catch (Exception e) {

            logger.error("getSerialNumberRequestLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<EmailTemplateDetails> getMailLogReport(String refID) throws Exception {

        logger.debug("getMailLogReport repo is started for refId {}", refID);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refId").is(refID));

            return mongoTemplate.find(query, EmailTemplateDetails.class);

        } catch (Exception e) {

            logger.error("getMailLogReport repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<EmailTemplateDetails> getMailLogReport() throws Exception {

        logger.debug("getMailLogReport repo is started to fetch all record");

        try {

            return mongoTemplate.findAll(EmailTemplateDetails.class);

        } catch (Exception e) {

            logger.error("getMailLogReport repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<DealerEmailMaster> getDealerEmailMapping(String dealerID) throws Exception {

        logger.debug("getDealerEmailMapping repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("dealerID").is(dealerID)
                    .and("active").is(true));

            return mongoTemplate.find(query, DealerEmailMaster.class);

        } catch (Exception e) {

            logger.error("getDealerEmailMapping repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    @Override
    public List<ApplicationRequest> getMainAllLog() throws Exception {

        logger.debug("getMainAllLog repo is started ");

        try {

            return mongoTemplate.findAll(ApplicationRequest.class);

        } catch (Exception e) {

            logger.error("getMainAllLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    @Override
    public List<GoNoGoCroApplicationResponse> getAllData() throws Exception {

        logger.debug("getAllData repo is started ");

        try {

            return mongoTemplate.findAll(GoNoGoCroApplicationResponse.class);

        } catch (Exception e) {

            logger.error("getAllData repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public GoNoGoCroApplicationResponse getSingleBucketData(String refID) throws Exception {

        logger.debug("getSingleBucketData repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            return mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);

        } catch (Exception e) {

            logger.error("getSingleBucketData repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<OtpLog> getLogData() throws Exception {

        logger.debug("getLogData repo is started ");

        try {

            return mongoTemplate.findAll(OtpLog.class);

        } catch (Exception e) {

            logger.error("getLogData repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public boolean updateAgeInAllDocuments() throws Exception {

        logger.debug("updateAgeInAllDocuments repo is started ");

        try {

            List<GoNoGoCroApplicationResponse> goNoGoCroApplicationResponseList = mongoTemplate
                    .findAll(GoNoGoCroApplicationResponse.class);

            if (goNoGoCroApplicationResponseList != null && !goNoGoCroApplicationResponseList.isEmpty()) {

                logger.debug("updateAgeInAllDocuments repo gngResponse size {} ", goNoGoCroApplicationResponseList.size());

                for (GoNoGoCroApplicationResponse goNoGoCroApplicationResponse : goNoGoCroApplicationResponseList) {

                    if (goNoGoCroApplicationResponse != null &&
                            goNoGoCroApplicationResponse.getApplicationRequest() != null
                            && goNoGoCroApplicationResponse.getApplicationRequest().getRequest() != null
                            && goNoGoCroApplicationResponse.getApplicationRequest().getRequest().getApplicant() != null) {

                        String dob = goNoGoCroApplicationResponse
                                .getApplicationRequest().getRequest()
                                .getApplicant().getDateOfBirth();

                        if (StringUtils.isNotBlank(dob)) {

                            goNoGoCroApplicationResponse.getApplicationRequest().getRequest().getApplicant()
                                    .setAge(GngDateUtil.getAge(dob));

                            mongoTemplate.save(goNoGoCroApplicationResponse);

                        }
                    }

                }
            }
            return true;

        } catch (Exception e) {

            logger.error("updateAgeInAllDocuments repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public boolean updateDeletedFlagOfAllDocuments() throws Exception {

        logger.debug("updateDeletedFlagOfAllDocuments repo is started ");

        try {

            List<GridFSDBFile> gridFSDBFiles = gridFsTemplate.find(null);

            if (gridFSDBFiles != null && !gridFSDBFiles.isEmpty()) {

                logger.debug("updateDeletedFlagOfAllDocuments repo gridFSDBFiles size {} ", gridFSDBFiles.size());

                for (GridFSDBFile gridFSDBFile : gridFSDBFiles) {

                    Object id = gridFSDBFile.getId();

                    updateDeleteFlag(id);
                }

            }

            return true;
        } catch (Exception e) {

            logger.error("updateDeletedFlagOfAllDocuments repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    /**
     * @param id
     */
    private boolean updateDeleteFlag(Object id) throws Exception {

        logger.debug("updateDeleteFlag repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(id));

            Update update = new Update();
            update.set("metadata.uploadFileDetails.deleted", false);

            mongoTemplate.updateMulti(query, update, "fs.files");

            return true;

        } catch (Exception e) {

            logger.error("updateDeleteFlag repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    @Override
    public boolean checkEmailDedupe(GetFileRequest getFileRequest) throws Exception {

        logger.debug("checkEmailDedupe repo is started ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refID").is(getFileRequest.getRefID())
                    .and("imageFileID").is(getFileRequest.getImageFileID())
                    .and("emailProcessStatus").is(Status.COMPLETE.toString()));

            return mongoTemplate.exists(query, GetFileRequest.class);

        } catch (Exception e) {

            logger.error("checkEmailDedupe repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public boolean saveEmailLog(GetFileRequest getFileRequest) throws Exception {

        logger.debug("saveEmailLog repo is started ");

        try {

            mongoTemplate.insert(getFileRequest);

            return true;

        } catch (Exception e) {

            logger.error("saveEmailLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    @Override
    public List<ReportEmailConfiguration> getReportEmailConfiguration(
            String reportType, String institutionID) throws Exception {

        logger.debug("getReportEmailConfiguration repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("reportType").is(reportType)
                    .and("institutionID").is(institutionID));

            return mongoTemplate.find(query, ReportEmailConfiguration.class);

        } catch (Exception e) {

            logger.error("getReportEmailConfiguration repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<GNGDealerEmailMaster> getGngDealerEmailMapping(
            String productID, String institutionID) throws Exception {

        logger.debug("getGngDealerEmailMapping repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("productId").is(productID)
                    .and("institutionID").is(institutionID).and("active")
                    .is(true));


            return mongoTemplate.find(query, GNGDealerEmailMaster.class);

        } catch (Exception e) {

            logger.error("getGngDealerEmailMapping repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public boolean updateStageAndStatus(String refID ,String appStatus ,String appStage) throws Exception {

        logger.debug("updateStageAndStatus repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID));

            Update update = new Update();
            update.set("applicationStatus", appStatus);
            update.set("applicationRequest.currentStageId", appStage);

            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            return true;
        } catch (Exception e) {

            logger.error("updateStageAndStatus repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<RequestJsonDomain> getMbLogWithCoapplicant(String refID) throws Exception {

        logger.debug("getMbLogWithCoapplicant repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("gngRefId").is(refID));

            return mongoTemplate.find(query, RequestJsonDomain.class);

        } catch (Exception e) {

            logger.error("getMbLogWithCoapplicant repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }

    @Override
    public List<DealerEmailMaster> getDealerEmailMapping(String dealerID, String institutionId) throws Exception {

        logger.debug("getDealerEmailMapping repo is started ");

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("dealerID").is(dealerID)
                    .and("institutionID").is(institutionId).and("active").is(true));


            return mongoTemplate.find(query, DealerEmailMaster.class);

        } catch (Exception e) {

            logger.error("getDealerEmailMapping repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public ApplicationRequest getMainLog(String refID, String institutionId) throws Exception {

        logger.debug("getMainLog repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refID).and("header.institutionId").is(institutionId));

            return mongoTemplate.findOne(query, ApplicationRequest.class);

        } catch (Exception e) {

            logger.error("getMainLog repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public String getProcessedApplication(String refID, String institutionId) {
        logger.debug("getMainLog repo is started ");

        logger.debug("getSingleBucketData repo is started ");

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refID).and("applicationRequest.header.institutionId").is(institutionId));
        query.fields().include("applicationRequest.header");

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = mongoTemplate.findOne(query, GoNoGoCroApplicationResponse.class);

        if (null != goNoGoCroApplicationResponse) {
            return goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId();
        } else {
            return null;
        }
    }

    @Override
    public boolean updateDealerName(String refId) throws Exception {

        String dealerName = getDealerName(refId);

        if (StringUtils.isNotBlank(dealerName)) {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(refId));

            Update update = new Update();
            update.set("applicationRequest.request.application.asset.0.dlrName", dealerName);

            WriteResult writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            return writeResult.isUpdateOfExisting();

        } else {
            return false;
        }

    }


    private String getDealerName(String refId) throws Exception {
        GoNoGoCroApplicationResponse singleBucketData = getSingleBucketData(refId);

        String dealerName = null;

        if (null != singleBucketData
                && null != singleBucketData.getApplicationRequest()
                && null != singleBucketData.getApplicationRequest().getHeader()) {

            Header header = singleBucketData.getApplicationRequest().getHeader();

            DealerEmailMaster dealerEmailMaster = applicationRepository
                    .getDealerEmailMaster(header);

            if (null != dealerEmailMaster) {
                dealerName = dealerEmailMaster.getSupplierDesc();
            }

        }
        return dealerName;
    }

    @Override
    public SalesForceLog getSalesForceLog(SalesForceLog salesForceLog) {
        logger.debug("getSalesForceLog repo is started ");
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(salesForceLog.getRefId()).
                and("header.institutionId").is(salesForceLog.getHeader().getInstitutionId()));

        return mongoTemplate.findOne(query, SalesForceLog.class);
    }

    @Override
    public boolean getEmailStatus(GetFileRequest getImageRequest) throws Exception {
        try {

            Query query = new Query();

            Criteria criteria = new Criteria();
            criteria
                    .orOperator(
                            Criteria.where(
                                    "emailProcessStatus")
                                    .is(Status.COMPLETE.name()),
                            Criteria.where(
                                    "emailProcessStatus")
                                    .is(Status.IN_PROCESS.name()))
                    .and("refID").is(getImageRequest.getRefID())
                    .and("imageFileID").is(getImageRequest.getImageFileID());

            query.addCriteria(criteria);

            return mongoTemplate.exists(query, GetFileRequest.class);

        } catch (Exception e) {

            logger.error("checkEmailStatus repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }
    }

    @Override
    public List<GoNoGoCustomerApplication> getApplicationBetweenFromAndToDateForDeclinePurpose(Date startDate, Date endDate, String institutionId) throws Exception {

        try {

            Query query = new Query();

            Criteria criteria = new Criteria();
            criteria = criteria.where("applicationRequest.header.dateTime").gte(startDate).lte(endDate)
                    .and("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                    .orOperator(
                            Criteria.where("applicationRequest.currentStageId").is(GNGWorkflowConstant.APRV.toFaceValue()),
                            Criteria.where("applicationRequest.header.product").is(Product.DPL.name()).and("applicationRequest.currentStageId").in(GNGWorkflowConstant.SRNV.toFaceValue(), GNGWorkflowConstant.IMEIV.toFaceValue()));

            query.addCriteria(criteria);

            CloseableIterator<GoNoGoCustomerApplication> stream = mongoTemplate.stream(query, GoNoGoCustomerApplication.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("getApplicationBetweenFromAndToDate repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    @Override
    public List<GoNoGoCustomerApplication> getApplicationLessThanDateForDeclinePurpose(Date date, String institutionId, int limit, int skip) throws Exception {
        try {
            Query query = new Query();
            query.limit(limit);
            query.skip(skip);

            Criteria criteria = new Criteria();
            criteria = criteria.where("applicationRequest.header.dateTime").lt(date)
                    .and("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                    .orOperator(
                            Criteria.where("applicationRequest.currentStageId").is(GNGWorkflowConstant.APRV.toFaceValue()),
                            Criteria.where("applicationRequest.header.product").is(Product.DPL.name()).and("applicationRequest.currentStageId").in(GNGWorkflowConstant.SRNV.toFaceValue(), GNGWorkflowConstant.IMEIV.toFaceValue()));

            query.addCriteria(criteria);

            CloseableIterator<GoNoGoCustomerApplication> stream = mongoTemplate.stream(query, GoNoGoCustomerApplication.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("getApplicationLessThanDate repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


    @Override
    public void saveDeclineApplicationLog(DeclineApplicationDetails declineApplicationDetails) {
        logger.debug("saveDeclineApplicationLog repo started");
        try {
            mongoTemplate.insert(declineApplicationDetails);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("saveDeclineApplicationLog repo exception {}", e.getMessage());
        }
    }

    @Override
    public List<RawResponseLog> getRawResponse(AdminLogRequest adminLogRequest) {

        logger.debug("getRawResponse repo is started ");

        Query query = new Query();
        query.addCriteria(Criteria.where("refId").is(adminLogRequest.getRefID()).
                and("responseType").is(adminLogRequest.getResponseType()));

        return mongoTemplate.find(query, RawResponseLog.class);
    }


    @Override
    public List<GoNoGoCustomerApplication> getApplicationLessThanDateForCancelPurpose(Date startDate, String institutionId, int limit, int skip) throws Exception {
        try {
            Query query = new Query();
            query.limit(limit);
            query.skip(skip);


            Criteria criteria = new Criteria();
            criteria = criteria.where("applicationRequest.header.dateTime").lte(startDate)
                    .and("applicationRequest.header.product").in(Product.DPL.name(), Product.CDL.name())
                    .and("applicationRequest.header.institutionId").is(institutionId)
                    .orOperator(

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                                    .and("applicationRequest.currentStageId").is(GNGWorkflowConstant.APRV.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(startDate)
                                    ),

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                                    .and("applicationRequest.header.product").is(Product.DPL.name()).and("applicationRequest.currentStageId")
                                    .in(GNGWorkflowConstant.SRNV.toFaceValue(), GNGWorkflowConstant.IMEIV.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(startDate)
                                    ),

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.ON_HOLD.toFaceValue(), GNGWorkflowConstant.QUEUED.toFaceValue())
                                    .and("applicationRequest.currentStageId").in(GNGWorkflowConstant.CR_H.toFaceValue(), GNGWorkflowConstant.CR_Q.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(startDate)
                                    )

                    );

            query.addCriteria(criteria);

            CloseableIterator<GoNoGoCustomerApplication> stream = mongoTemplate.stream(query, GoNoGoCustomerApplication.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("getApplicationLessThanDate repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public void saveCancelledApplicationLog(CancelApplicationDetails cancelApplicationDetails) {
        logger.debug("saveCancelledApplicationLog repo started");
        try {
            mongoTemplate.insert(cancelApplicationDetails);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("saveCancelledApplicationLog repo exception {}", e.getMessage());
        }
    }

    @Override
    public List<GoNoGoCustomerApplication> getApplicationBetweenToAndFromDate(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest, int limit, int skip) throws Exception {

        try {
            Query query = new Query();
            query.limit(limit);
            query.skip(skip);

            String institutionId = cancelDoNotRaisedApplicationRequest.getHeader().getInstitutionId();

            Date formattedFrmDt = GngDateUtil.getZeroTimeFromDate(cancelDoNotRaisedApplicationRequest.getStartDate());

            Date formattedToDt =  GngDateUtil.getEndTimeToDate(cancelDoNotRaisedApplicationRequest.getEndDate());

            Criteria criteria = new Criteria();
            criteria = criteria.where("applicationRequest.header.dateTime").gte(formattedFrmDt).lte(formattedToDt)
                    .and("applicationRequest.header.product").in(Product.DPL.name(), Product.CDL.name())
                    .and("applicationRequest.header.institutionId").is(institutionId)
                    .orOperator(

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                                    .and("applicationRequest.currentStageId").is(GNGWorkflowConstant.APRV.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(formattedToDt)
                                    ),

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APPROVED.name())
                                    .and("applicationRequest.header.product").is(Product.DPL.name()).and("applicationRequest.currentStageId")
                                    .in(GNGWorkflowConstant.SRNV.toFaceValue(), GNGWorkflowConstant.IMEIV.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(formattedToDt)
                                    ),

                            Criteria.where("applicationStatus").in(GNGWorkflowConstant.ON_HOLD.toFaceValue(), GNGWorkflowConstant.QUEUED.toFaceValue())
                                    .and("applicationRequest.currentStageId").in(GNGWorkflowConstant.CR_H.toFaceValue(), GNGWorkflowConstant.CR_Q.toFaceValue())
                                    .orOperator(
                                            Criteria.where("croJustification").exists(false),
                                            Criteria.where("croJustification").exists(true).in("[ ],null"),
                                            Criteria.where("croJustification").exists(true).and("croJustification.croJustificationUpdateDate").lt(formattedToDt)
                                    )

                    );

            query.addCriteria(criteria);

            CloseableIterator<GoNoGoCustomerApplication> stream = mongoTemplate.stream(query, GoNoGoCustomerApplication.class);

            return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("getApplicationLessThanDate repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    @Override
    public DigitizationDealerEmailMaster getEmailCcIdFromDigiEmailMaster(String dealerID, String institutionId) throws Exception{
        logger.debug("getEmailCcIdFromDigiEmailMaster repo is started ");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("supplierID").is(dealerID)
                    .and("active").is(true).and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, DigitizationDealerEmailMaster.class);

        } catch (Exception e) {

            logger.error("getEmailCcIdFromDigiEmailMaster repo exception {}", e.getMessage());

            throw new Exception(e.getMessage());

        }

    }
}

