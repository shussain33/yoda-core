package com.softcell.dao.mongodb.repository;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.Stages;
import com.softcell.gonogo.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.dashboard.DashboardDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.security.v2.Hierarchy;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Created by suraj on 11/9/20.
 */
@Component
public class DashboardDataHelper {

    private static final Logger logger = LoggerFactory.getLogger(DashboardDataHelper.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public static final List<String> fosDefaultIdList = new ArrayList<String>(){{ add("Paisabazaar_FOS"); add("11173"); add("LoanCompare_FOS"); add("MYMONEYKARMA_FOS");
        add("WishFin_FOS"); add("LENDINGADDA_FOS"); add("MyMoneyMantra_fos"); add("DIGIPL_FOS"); add("CASHTAP_FOS"); add("BAJAJ_FOS");}};

    public void addDefaultCriteria(DashboardRequest dashboardRequest, Map<String, String> filterMap, Criteria criteria, boolean refCriteria) {
        criteria.and("applicationRequest.header.institutionId").is(dashboardRequest.getHeader().getInstitutionId())
                .and("applicationRequest.request.application.loanType").in(dashboardRequest.getHeader().getProduct().name());

        if(!refCriteria) {
            CalculateTimeRange caculateTimeRange = new CalculateTimeRange(dashboardRequest).invoke();
            int days = caculateTimeRange.getDays();
            DateTime fromDate = caculateTimeRange.getFromDate();
            DateTime toDate = caculateTimeRange.getToDate();
            if (!CollectionUtils.isEmpty(filterMap) && !filterMap.containsKey("dateTime")) {
                if (days == NumberUtils.INTEGER_ZERO) {
                    criteria.and("applicationRequest.header.dateTime").is(fromDate.toDate());
                } else {
                    criteria.and("applicationRequest.header.dateTime").gte(fromDate.toDate()).lte(toDate.toDate());
                }
            }
        }
    }

    public void addHierarchyCriteria(DashboardRequest dashboardRequest, Criteria criteria) {
        RequestCriteria requestCriteria = dashboardRequest.getCriteria();
        boolean defaultFilter = (requestCriteria.getHierarchy() == null);
        if (defaultFilter) {
            criteria.and("applicationRequest.appMetaData.branchV2.branchId").in(requestCriteria.getBranches());
        } else {
            Hierarchy hierarchy = requestCriteria.getHierarchy();
            MongoRepositoryUtil.getCriteriaMapping(hierarchy, criteria, mongoTemplate);
        }
    }

    public void addRoleSpecificCriteria(String userId, String role, Criteria criteria, Header header, boolean showMyRecords) {
        // Role wise query
        if (Roles.isCreditRole(role)) {
            if (showMyRecords) {
                // add stage check and also allocation check
                List<String> creditStages = Stages.getCreditStages();
                Criteria recommendCriteria = createCriteriaForRecommandedRecords(userId, creditStages);

                List<String> refIds = getRefIdsBasedOnDeviations(header.getInstitutionId(), role);
                Criteria deviationCriteria = null;
                if (!CollectionUtils.isEmpty(refIds)) {
                    deviationCriteria = Criteria.where("_id").in(refIds);
                    criteria.orOperator(recommendCriteria, deviationCriteria);
                } else {
                    criteria.andOperator(recommendCriteria);
                }
            } else {
                /* Do nothing as
                 The query for general view has been already cerated, no need to create any here. */
            }
        } else {
            if (StringUtils.equalsIgnoreCase(Roles.Role.FOS.name(),role)) {
                //remove hardcoded paisabazar role check
                criteria.orOperator(Criteria.where("applicationRequest.header.dsaId").is(userId),
                        Criteria.where("applicationRequest.header.dsaId").in(fosDefaultIdList)) ;
            }
            if (showMyRecords) {
                if (Roles.isOpsRole(role)) {
                    List<String> opsStages = new ArrayList<>();
                    opsStages.addAll(Stages.getOpsStages());
                    Criteria recommendCriteria = createCriteriaForRecommandedRecords(userId, opsStages);
                    criteria.orOperator(Criteria.where("applicationRequest.currentStageId").
                            in(Cache.ROLE_ENABLED_STAGES_MAP.get(role)), recommendCriteria);
                } else if(Roles.isCoOriginationRole(role)){
                    Criteria stageCriteria = new Criteria();
                    stageCriteria.and("applicationRequest.currentStageId").in(Cache.ROLE_ENABLED_STAGES_MAP.get(role));
                    criteria.andOperator(Criteria.where("applicationRequest.request.application.coOrigination").is(true)
                            , stageCriteria);
                } else {
                    criteria.and("applicationRequest.currentStageId").in(Cache.ROLE_ENABLED_STAGES_MAP.get(role) != null ? Cache.ROLE_ENABLED_STAGES_MAP.get(role) : new ArrayList<>());
                }
            }
        }
    }

    public void addFilterSpecificCriteria(Criteria criteria,Map<String, String> filterMap, boolean refCriteria) {
        List<Criteria> criteriaList = new ArrayList<>();
        int criIndex = 0;
        for(Map.Entry<String, String> entry: filterMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String fieldMapping = Cache.CRITERIA_FIELD_MAPPING_MAP.get(key);
            if(StringUtils.isNotEmpty(fieldMapping)){
                if(refCriteria){
                    refCriteria = false;
                    criteriaList.add((Criteria.where(fieldMapping).is(value)));
                }else if(StringUtils.equals(key, "customerName")){
                    Criteria searchCriteria = new Criteria();
                    String[] searchedWords = value.split(" ");

                    for (int i = 0; i< searchedWords.length; i++, criIndex++) {
                        String word = searchedWords[i];
                        Criteria orCriteria1 = new Criteria();
                        orCriteria1.orOperator(Criteria.where("applicationRequest.request.applicant.applicantName.firstName").regex(word, "i"),
                                Criteria.where("applicationRequest.request.applicant.applicantName.middleName").regex(word, "i"),
                                Criteria.where("applicationRequest.request.applicant.applicantName.lastName").regex(word, "i"));
                        criteriaList.add(orCriteria1);
                    }
                }else if(StringUtils.equalsIgnoreCase(key, "bucket")){
                    criteriaList.add(Criteria.where(fieldMapping).is(value));
                }else if(StringUtils.equals(key, "dateTime")){
                    Date date = new Date(value);
                    Date start = DateUtils.getStartOfDay(date);
                    Date end = DateUtils.getEndOfDay(date);
                    criteriaList.add(Criteria.where(fieldMapping).gte(start).lte(end));
                }else if(StringUtils.equals(key, "loanAmt")){
                    criteriaList.add(Criteria.where(fieldMapping).is(Double.parseDouble(value)));
                } else {
                    criteriaList.add((Criteria.where(fieldMapping).regex(value, "i")));
                }
            }
        }
        Criteria[] criteriaArray = criteriaList.toArray(new Criteria[criteriaList.size()]);
        try{
            criteria.andOperator(criteriaArray);
        } catch(Exception e){
            logger.debug("Exception occurred while adding filter criteria !" + e.getMessage());
        }
    }

    private Criteria createCriteriaForRecommandedRecords(String userId, List<String> stages) {
        Criteria recommendCriteria = new Criteria();
        Criteria roleCriteria = new Criteria();
        roleCriteria.orOperator(Criteria.where("allocationInfo.operator").is(userId),
                Criteria.where("allocationInfo.assignedBy").is(userId));
        Criteria stageCriteria = Criteria.where("applicationRequest.currentStageId").in(stages);
        recommendCriteria.andOperator(roleCriteria, stageCriteria);
        return recommendCriteria;
    }


    private List<String> getRefIdsBasedOnDeviations(String institutionId, String role) {
        List<String> refIds = new ArrayList<>();
        Query query = new Query();
        Criteria orCriteria = new Criteria();
        orCriteria.orOperator(
                Criteria.where("deviationList.authority").is(role), Criteria.where("deviationList.authorities").is(role));
        Criteria criteria = Criteria.where("institutionId").is(institutionId).andOperator(orCriteria);
        query.addCriteria(criteria);
        query.fields().include("_id");
        List<DeviationDetails> deviationDetailsList = mongoTemplate.find(query, DeviationDetails.class);

        deviationDetailsList.forEach(deviationDetail -> {
            refIds.add(deviationDetail.getRefId());
        });
        return refIds;
    }

    public Map<String,String> addCriteraForVerifierAndGetData(Criteria criteria, Header header, String role, boolean showMyRecords) {
        // Check Role of user is verifier, if yse then add criteria for verifier
        Map<String, String> appIdStatusMap = createRefIdsForVerifier(role, header, showMyRecords);
        criteria.and("_id").in(appIdStatusMap.keySet());
        return appIdStatusMap;
    }

    private Map<String, String> createRefIdsForVerifier(String role, Header header, boolean showMyRecords) {
        List<Object> tempList = Cache.VERIFICATION_ROLE_URL_MAP.get(role);
        Class<?>  cls = (Class)tempList.get(0);

        List<String> verificationStatusList = new ArrayList<>();
        verificationStatusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
        if(!showMyRecords){
            verificationStatusList.add(ThirdPartyVerification.Status.VERIFIED.name());
        }

        Criteria criteria = new Criteria();
        MongoRepositoryUtil.createCriteriaForVerifierRole(header, criteria, role, verificationStatusList);

        Query query = new Query().addCriteria(criteria);
        logger.info("Query for verifier request {}", query);List lObj  = mongoTemplate.find(query, cls);

        Map<String, String> refIds = new HashMap<>();
        String agencyCode = header.getDealerId();
        lObj.forEach(dbObj -> {

            if(dbObj instanceof VerificationDetails){
                lObj.forEach( obj -> {
                    VerificationDetails verificationDetails = (VerificationDetails)obj;
                    String id = verificationDetails.getRefId();
                    List<String> statusList = getStatusListForVerificationDetails(verificationDetails, role, agencyCode, header);
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }else if(dbObj instanceof LegalVerification){
                lObj.forEach( obj -> {
                    LegalVerification legalVerification = (LegalVerification)obj;
                    String id = legalVerification.getRefId();
                    List<String> statusList = new ArrayList<>();
                    //for multiple legalverification
                    /*statusList.add(legalVerification.getStatus());*/
                    legalVerification.getLegalVerificationDetailsList().forEach(legalVerificationDetails -> {
                        if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                                && StringUtils.isNotEmpty(legalVerificationDetails.getAgencyCode()) && header.getDealerIds().contains(legalVerificationDetails.getAgencyCode())){
                            statusList.add(legalVerificationDetails.getStatus());
                        }else if(StringUtils.equals(agencyCode, legalVerificationDetails.getAgencyCode())){
                            statusList.add(legalVerificationDetails.getStatus());
                        }
                    });
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }else if(dbObj instanceof Valuation){
                lObj.forEach( obj -> {
                    Valuation valuation = (Valuation)obj;
                    String id = valuation.getRefId();
                    List<String> statusList = new ArrayList<>();
                    valuation.getValuationDetailsList().forEach(valuationDetails -> {
                        if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                                && StringUtils.isNotEmpty(valuationDetails.getAgencyCode()) && header.getDealerIds().contains(valuationDetails.getAgencyCode())){
                            statusList.add(valuationDetails.getStatus());
                        }else if(StringUtils.equals(agencyCode, valuationDetails.getAgencyCode())){
                            statusList.add(valuationDetails.getStatus());
                        }
                    });
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }
        });
        return refIds;
    }

    private List<String> getStatusListForVerificationDetails(VerificationDetails verificationDetails, String role, String agencyCode, Header header) {
        List<String> statusList = new ArrayList<>();
        if(role.equals(Roles.Role.RESI_VERIFIER.name())){
            verificationDetails.getResidenceVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.OFC_VERIFIER.name())){
            verificationDetails.getOfficeVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.BANK_VERIFIER.name())){
            verificationDetails.getBankingVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.ITR_VERIFIER.name())){
            verificationDetails.getItrVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        }
        return statusList;
    }

    private String getStatusForVerifierDashBoard(List<String> status) {
        if(status.contains(ThirdPartyVerification.Status.PENDING.name()) || status.contains(ThirdPartyVerification.Status.SUBMITTED.name())){
            return Status.VerifierStatus.WIP.name();
        } else{
            return Status.VerifierStatus.Verified.name();
        }
    }

    public List<DashboardDetails> buildDashboardDetail(List<GoNoGoCustomerApplication> applications, Map<String, String> appIdStatusMap) {
        Set<String> refIds = new TreeSet<>();
        List<DashboardDetails> details = new ArrayList<>();

        for(GoNoGoCustomerApplication application : applications) {
            // Dashboard list detail building shouldn't be halted if there is error in a single record
            // So try-catch required for single record iteration
            try {

                refIds.add(application.getApplicationRequest().getRefID());

                DashboardDetails detail = new DashboardDetails();
                details.add(detail);

                if (application.getApplicationRequest().getRequest().getApplicant() != null) {

                    detail.setName(GngUtils.convertNameToFullName(application.getApplicationRequest().getRequest().getApplicant().getApplicantName()));
                    detail.setDob(application.getApplicationRequest().getRequest().getApplicant().getDateOfBirth());

                    Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE,
                            application.getApplicationRequest().getRequest().getApplicant().getPhone()).orElse(null);
                    if (phone != null)
                        detail.setMobileNumber(phone.getPhoneNumber());

                    List<Kyc> kycList = application.getApplicationRequest().getRequest().getApplicant().getKyc();
                    if (kycList != null) {
                        detail.setAadhaarNumber(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));
                        detail.setPAN(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), kycList, ValidationPattern.PAN));
                    }
                }

                // Set up loan amounts
                //      Applied loan amount
                detail.setAmountApplied(application.getApplicationRequest().getRequest().getApplication().getLoanAmount() );
                //      Approved loan amount
                if( application.getCroDecisions() != null ) {
                    detail.setAmountApproved(getAmountApproved(application.getCroDecisions()));
                }

                detail.setDate(application.getDateTime());
                detail.setAgreementNum(application.getAgreementNum());
                //if loggedIn user is verifier then show virifier specific(configurable) status
                String status;
                if(appIdStatusMap != null && appIdStatusMap.containsKey(application.getGngRefId())){
                    status = appIdStatusMap.get(application.getGngRefId());
                } else{
                    status = application.getApplicationStatus();
                }
                detail.setStatus(status);
                detail.setBucket(application.getApplicationBucket());
                detail.setStages(application.getApplicationRequest().getCurrentStageId());

                detail.setReferenceID(application.getApplicationRequest().getRefID());
                detail.setApplicantType(application.getApplicationRequest().getApplicantType());
                detail.setQdeDecision(application.getApplicationRequest().isQdeDecision());

                detail.setInvoiceDetails(application.getInvoiceDetails());

                if (application.getApplicationRequest().getReAppraiseDetails() != null) {
                    detail.setReAppraiseCount(application
                            .getApplicationRequest().getReAppraiseDetails()
                            .getReAppraiseCount());//
                }

                Query queryForPostIpa = new Query();
                queryForPostIpa.addCriteria(Criteria.where("_id").is(application.getApplicationRequest().getRefID()));
                queryForPostIpa.fields().include("postIPA.scheme").include("postIPA.financeAmount");

                PostIpaRequest postIpaRequest = mongoTemplate.findOne(queryForPostIpa, PostIpaRequest.class);
                if (postIpaRequest != null && postIpaRequest.getPostIPA() != null) {
                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getScheme())) {
                        detail.setScheme(postIpaRequest.getPostIPA().getScheme());
                    }

                    detail.setFinanceAmount(postIpaRequest.getPostIPA().getFinanceAmount());//
                } else {
                    detail.setScheme(FieldSeparator.HYPHEN);
                }

                detail.setLosDetails(application.getLosDetails());

                List<CroDecision> croDecisions = application.getCroDecisions();
                if (!CollectionUtils.isEmpty(croDecisions)) {
                    Collections.sort(croDecisions, (CroDecision d1, CroDecision d2) -> d2.getDecisionUpdateDate()
                            .compareTo(d1.getDecisionUpdateDate()));
                }

                detail.setCroDecisions(croDecisions);

                // Set fields from AppMetadata
                detail.setBranchName(application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
                detail.setDsaName(GngUtils.convertNameToFullName(application.getApplicationRequest().getAppMetaData().getDsaName()));

                /**
                 * @Amit commented  with new requirenment
                 * Changes for CPA login
                 */
                /*details.setBureauScore(application.getIntrimStatus().getCibilModuleResult().getFieldValue());

                details.setAppScore(application.getIntrimStatus().getScoringModuleResult().getFieldValue());

                String isStp = (application.getApplicationRequest().getHeader().getCroId() == "STP")? "Y" : "N";

                details.setIsStp(isStp);
                //TODO Amit Take proper requirement for DealerName
                details.setDealerName("");

                String dsaName = application.getApplicationRequest().getAppMetaData().getDsaName().getFirstName() +
                        " " + application.getApplicationRequest().getAppMetaData().getDsaName().getLastName();
                details.setDsaName(dsaName);

                details.setBranchName(application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());*/

                if (application.getCroJustification() != null && application.getCroJustification().size() > 0) {
                    detail.setCroJustificationList(application.getCroJustification());
                }

                //set allocation status
                if(application.getAllocationInfo() != null){
                    detail.setCurrentUser(Cache.getUserName(application.getApplicationRequest().getHeader().getInstitutionId(),
                            application.getAllocationInfo().getOperator()));
                    detail.setAllocationInfo(application.getAllocationInfo());
                }

                if(application.getApplicationRequest() != null && application.getApplicationRequest().getRequest() != null &&
                        application.getApplicationRequest().getRequest().getApplication() != null && StringUtils.isNotEmpty(application.getApplicationRequest().getRequest().getApplication().getPromoCode())) {
                    detail.setPromocode(application.getApplicationRequest().getRequest().getApplication().getPromoCode());
                }

            } catch (Exception e ) {
                logger.error("Exception while setting dashboard data {}: \nfor refid {} :application : {}",
                        e, application.getApplicationRequest().getRefID(), application);
            }
        }
        logger.debug("Fetched from db : count {}; Dashboard list items count {}", applications.size() , details.size());
        return details;
    }

    private double getAmountApproved(List<CroDecision> croDecisions) {

        if (null != croDecisions && !croDecisions.isEmpty()) {

            return croDecisions.parallelStream()
                    .map(croDecision -> croDecision.getAmtApproved())
                    .findFirst()
                    .get();

        } else {
            return 0D;
        }
    }

    private class CalculateTimeRange {
        private DashboardRequest dashboardRequest;
        private DateTime fromDate;
        private DateTime toDate;
        private int days;

        public CalculateTimeRange(DashboardRequest dashboardRequest) {
            this.dashboardRequest = dashboardRequest;
        }

        public DateTime getFromDate() {
            return fromDate;
        }

        public DateTime getToDate() {
            return toDate;
        }

        public int getDays() {
            return days;
        }

        public CalculateTimeRange invoke() {
            // getting today's time component from date
            DateTime now = new DateTime();

            fromDate = new DateTime(dashboardRequest.getFromDate()).withHourOfDay(now.getHourOfDay())
                    .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                    .withZoneRetainFields(DateTimeZone.UTC);

            toDate = new DateTime(dashboardRequest.getToDate()).withHourOfDay(now.getHourOfDay())
                    .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                    .withZoneRetainFields(DateTimeZone.UTC);

            days = Days.daysBetween(fromDate, toDate).getDays();
            return this;
        }
    }
}
