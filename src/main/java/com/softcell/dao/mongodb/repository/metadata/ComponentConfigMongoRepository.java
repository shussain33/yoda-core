package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.config.ComponentConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class ComponentConfigMongoRepository implements ComponentConfigRepository {

    private static final Logger logger = LoggerFactory.getLogger(ComponentConfigMongoRepository.class);

    @Autowired
    MongoTemplate mongoTemplate;

    private Query query;
    private Class collection = ComponentConfiguration.class;


    @Override
    public Boolean insertComponentConfiguration(
            ComponentConfiguration componentConfig) {
        try {
            /*if (!mongoTemplate.collectionExists(collection)) {
                mongoTemplate.createCollection(collection);
            }*/
            mongoTemplate.insert(componentConfig);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean updateComponentConfiguration(
            ComponentConfiguration componentConfig) {
        try {
            query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(componentConfig.getInstitutionId()).and("productId")
                    .is(""));
            componentConfig.getComponentSettings().getComponentMap().get(1).get(1).getModuleSettingMap().get(1).setActive(false);
            Update update = new Update();
            update.set("active", componentConfig.isActive());
            update.set("lastUpdateDate", new Date());
            mongoTemplate.updateFirst(query, update, collection);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean deleteComponentConfiguration(
            ComponentConfiguration componentConfig) {
        return null;
    }

    @Override
    public List<ComponentConfiguration> readComponentConfigByInstitution(
            String institutionId) {
        return null;
    }

    @Override
    public List<ComponentConfiguration> readActiveComponentConfigByInstitution(
            String institutionId) {
        return null;
    }

    @Override
    public boolean isComponentConfigExist(ComponentConfiguration componentConfig) {
        return false;
    }

}
