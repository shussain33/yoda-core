package com.softcell.dao.mongodb.repository;

import com.mongodb.DBCollection;
import com.mongodb.WriteResult;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationMongoRepository;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.request.MigrationRequest;
import com.softcell.service.MigrationManager;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by amit on 11/3/19.
 */
@Repository
public class MigrationMongoRepository implements MigrationRepository {
    private static final Logger logger = LoggerFactory.getLogger(MigrationMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void save(Object object) {
        mongoTemplate.save(object);
    }

    @Override
    public Object fetch(Class<?> classz, String id) {
        Query query;
        if(id != null) {
            query = new Query(Criteria.where("_id").is(id));
        } else{
            query = new Query();
        }
        return mongoTemplate.findOne(query, classz);
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds) {
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(StringUtils.isNotEmpty(product))
            criteria.and("applicationRequest.request.application.loanType").is(product);
        if(CollectionUtils.isNotEmpty(refIds))
            criteria.and("_id").in(refIds);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds,
                                                            List<String> includes) {
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(StringUtils.isNotEmpty(product))
            criteria.and("applicationRequest.request.application.loanType").is(product);
        if(CollectionUtils.isNotEmpty(refIds))
            criteria.and("_id").in(refIds);
        Query query = new Query(criteria);
        if(CollectionUtils.isNotEmpty(includes)){
            includes.forEach(field -> {
                query.fields().include(field);
            });
        }
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchApplication(String institutionId, String product, List<String> refIds,
                                                            Map<String, Object> conditions, List<String> includes) {
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        if(StringUtils.isNotEmpty(product))
            criteria.and("applicationRequest.request.application.loanType").is(product);
        if(CollectionUtils.isNotEmpty(refIds))
            criteria.and("_id").in(refIds);
        if(conditions != null && !conditions.isEmpty()){
            conditions.forEach((field, value) ->{
                criteria.and(field).is(value);
            });
        }
        Query query = new Query(criteria);
        if(CollectionUtils.isNotEmpty(includes)){
            includes.forEach(field -> {
                query.fields().include(field);
            });
        }
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public boolean updateApplication(String refId, Map<String, Object> updateData) {
        Criteria criteria = Criteria.where("_id").is(refId);
        Query query = new Query(criteria);
        if(updateData != null && !updateData.isEmpty()){
            Update update = new Update();
            updateData.forEach((field, value) ->{
                update.set(field, value);
            });
            mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);
            return true;
        }
        return false;
    }

    @Override
    public Object fetch(Class className, Map<String, Object> conditions, List<String> includes) {
        Criteria criteria = new Criteria();
        if(conditions != null && !conditions.isEmpty()){
            for(Map.Entry<String, Object> entry : conditions.entrySet()){
                String field = entry.getKey();
                Object value = entry.getValue();
                if(criteria == null) {
                    criteria = new Criteria();
                    criteria.where(field).is(value);
                } else {
                    if(field.charAt(field.toCharArray().length -1) == '!'){
                        criteria.and(field.replace("!", "")).ne(value);
                    } else if(field.charAt(field.toCharArray().length -1) == '$'){
                        criteria.and(field.replace("$", "")).in(value);
                    } else {
                        criteria.and(field).is(value);
                    }
                }
            }
        }

        Query query = new Query(criteria);
        if(CollectionUtils.isNotEmpty(includes)){
            includes.forEach(field -> {
                query.fields().include(field);
            });
        }
        return mongoTemplate.find(query, className);
    }


    public int updateDsaId(MigrationRequest migrationRequest, List<String> secIdList, String primaryId, Name empName, Email emailId) {
        boolean updated =false;
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(migrationRequest.getHeader().getInstitutionId());
        criteria.and("applicationRequest.header.dsaId").in(secIdList);
        Update update = new Update();
        update.set("applicationRequest.header.dsaId",primaryId);
        update.set("applicationRequest.appMetaData.empId",primaryId);
        if(empName != null){
           update.set("applicationRequest.appMetaData.dsaName.firstName", empName.getFirstName());
           update.set("applicationRequest.appMetaData.dsaName.lastName", empName.getLastName());
           update.set("applicationRequest.appMetaData.dsaEmailId.emailAddress", emailId.getEmailAddress());
        }
        update.set("updateBy","Samruddhi.P");
        Query query = new Query(criteria);
        logger.info("query {}" , query);
        logger.info("update {}", update);
        WriteResult result = mongoTemplate.updateMulti(query, update,GoNoGoCustomerApplication.class);
        logger.info("updated count {}",result.getN());
        return  result.getN();
    }

    public int updateAllocationInfo(MigrationRequest migrationRequest, String secId, String primaryId) {
        boolean updated =false;
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(migrationRequest.getHeader().getInstitutionId());
        criteria.and("allocationInfo.operator").is(secId);
        Update update = new Update();
        update.set("allocationInfo.operator",primaryId);
        Query query = new Query(criteria);
        WriteResult result = null;
        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        if(CollectionUtils.isNotEmpty(goNoGoCustomerApplicationList)){
            for(GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplicationList){
                Criteria criteria1 = Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId());
                Query query1 =  new Query(criteria1) ;
                 result = mongoTemplate.upsert(query1, update,GoNoGoCustomerApplication.class);
            }
        }
        if(result != null){
           return result.getN();
        }else{
         return 0;
        }
    }

    @Override
    public long countDsaId(MigrationRequest migrationRequest, String secondaryIdList) {
        boolean updated =false;
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(migrationRequest.getHeader().getInstitutionId());
        criteria.and("applicationRequest.header.dsaId").is(secondaryIdList);
        Query query = new Query(criteria);
        logger.info("to count query {}", query);
        long cnt = mongoTemplate.count(query, GoNoGoCustomerApplication.class);
        return cnt;
    }

    @Override
    public List<CamDetails> fetchCamDetails(String instituteId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(instituteId));
        return mongoTemplate.find(query, CamDetails.class);
    }



    @Override
    public GoNoGoCustomerApplication fetchGonogoCustomerByDsaId( String institutionId, String primaryId) {
        Criteria criteria = Criteria.where("applicationRequest.header.institutionId").is(institutionId);
        criteria.and("applicationRequest.header.dsaId").is(primaryId);
        Query query = new Query(criteria);
        GoNoGoCustomerApplication goNoGoCustomerApplication = mongoTemplate.findOne(query,GoNoGoCustomerApplication.class);
        if(goNoGoCustomerApplication != null){
            return goNoGoCustomerApplication;
        }else{
           return  null;
        }
    }

    @Override
    public boolean updateCamDetail(String refId, CamDetails camDetails) {
        try{
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(refId));
        Update update = new Update();
        update.set("totalAbb", camDetails.getTotalAbb());
        update.set("avgCreditSummation", camDetails.getAvgCreditSummation());
        update.set("sbfc", camDetails.isSbfc());
        mongoTemplate.upsert(query, update, CamDetails.class);
        return true;
        }catch (Exception e){
            logger.error("{}",e.getStackTrace());
            return  false;
        }
    }
}
