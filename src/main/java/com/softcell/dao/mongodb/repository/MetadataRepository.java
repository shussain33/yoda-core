package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.metadata.MetadataEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Created by prateek on 11/3/17.
 */
@CacheConfig(cacheNames={"metadata_dict"})
public interface MetadataRepository {

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @return
     * @throws SystemException
     */
    @Cacheable
    Collection<String> findTypeValues(final String institutionId, final String type, final String product) throws SystemException;

    /**
     *
     * @param institutionId
     * @param product
     * @return
     */
    @Cacheable(condition = "#p1")
    Optional<Collection<MetadataEntity >> findAllByInstitutionNProduct(final String institutionId, final String product);

    /**
     *
     * @param institutionId
     * @return
     */
    @Cacheable(condition = "#p1")
    Optional<Collection<MetadataEntity >> findAllByInstitution(final String institutionId);

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @throws SystemException
     */
    @Cacheable(condition = "#p1")
    void replaceTypeValues(final String institutionId, final String type, final String product, Collection<String> values)
            throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param updatedFields
     * @throws SystemException
     */
    MetadataEntity renameTypeValues(final String institutionId, final String type, final String product, Map<String, String > updatedFields) throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @return
     * @throws SystemException
     */
    @CacheEvict(allEntries = true, condition = "#p1")
    MetadataEntity deleteType(final String institutionId, final String type, final String product) throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @throws SystemException
     */
    @CachePut(condition = "#p1")
    void createTypeValues(final String institutionId, final String type, final String product, Collection<String> values) throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @return  metadataEntity
     * @throws SystemException
     */
    @CacheEvict(allEntries = true, condition = "#p1")
    MetadataEntity factoryDefaultTypeValues(final String institutionId, final String type, final String product) throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @param values
     * @return
     * @throws SystemException
     */
    @Cacheable(unless = "#result.values")
    MetadataEntity removeTypeValues(final String institutionId, final String type, final String product, Collection<String> values) throws SystemException;

    /**
     *
     * @param institutionId
     * @param type
     * @param product
     * @throws SystemException
     */
    @CacheEvict
    void disableTypeValue(final String institutionId, final String type, final String product) throws SystemException;

}
