package com.softcell.dao.mongodb.repository.serialnumber;

import com.softcell.config.SerialNumberAuthsCredential;
import com.softcell.config.SerialNumberURLConfiguration;

import java.util.Map;

/**
 * @author mahesh
 */
public interface SrNumberURLCreditialRepository {

    public boolean saveCredentials(SerialNumberAuthsCredential serialNumberAuthsCredential);

    public Map<String, SerialNumberURLConfiguration> loadSrNumberConfiguaration();

    public boolean deleteCredentials(SerialNumberAuthsCredential serialNumberAuthsCredential);
}
