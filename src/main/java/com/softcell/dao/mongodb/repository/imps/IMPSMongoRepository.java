package com.softcell.dao.mongodb.repository.imps;

import com.google.common.base.CharMatcher;
import com.mongodb.WriteResult;
import com.softcell.constants.IMPSStatus;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.masters.PaynimoBankMaster;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by sampat on 6/11/17.
 */
@Repository
public class IMPSMongoRepository implements IMPSRepository{

    private static final Logger logger = LoggerFactory.getLogger(IMPSMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    private static final CharSequence SPECIAL_CHAR = "\n\t";

    public IMPSMongoRepository(){
        if(null==mongoTemplate){
            mongoTemplate = MongoConfig.getMongoTemplate();
        }
    }

    @Override
    public long getAccountValidationCount(String institutionId, String refID) {
        logger.debug("fetching validationCount using institutionId {} and refID {} ", institutionId, refID);

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("refID").is(refID));
            return mongoTemplate.count(query, AccountNumberInfo.class);
        } catch (Exception e) {
            logger.error("Error occurred while fetching validationCount count with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while validationCount with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public int getAccountValidationAttempt(String institutionId) {
        logger.debug("fetching validationAttempt using institutionId {} ", institutionId);
                int count=0;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("isEnabled").is(true));
            IMPSConfigDomain impsConfigDomain =  mongoTemplate.findOne(query, IMPSConfigDomain.class);
            if(null!=impsConfigDomain){
                count =  impsConfigDomain.getNoOfRetry();
            }
            return count;

        } catch (Exception e) {
            logger.error("Error occurred while fetching validationAttempt with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching validationAttempt with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public List<AccountNumberInfo> getAccountValidatedBanks(String institutionId, String refID) {

        logger.debug("fetching validatedBanks using institutionId {} and refID {} ", institutionId, refID);
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("refID").is(refID));
            List<AccountNumberInfo> accountNumberInfos = mongoTemplate.find(query, AccountNumberInfo.class);
            return accountNumberInfos;

        } catch (Exception e) {
            logger.error("Error occurred while fetching validatedBanks with probable cause [{}]", e.getMessage());
            throw new SystemException(String.format("Error occurred while fetching validatedBanks with probable cause [{%s}]", e.getMessage()));
        }
    }

    @Override
    public void saveAccountNumberInfo(AccountNumberInfo accountNumberInfo) {
        logger.debug("inserting AccountNumberInfo with values {} ", accountNumberInfo);
        try {

            mongoTemplate.insert(accountNumberInfo);
        } catch (Exception e) {
            logger.error("Error occurred while inserting account number validation logs with probable cause [{}]", e);
            throw new SystemException(String.format("Error occurred while inserting account number validation logs with probable cause [{%s}]", e));
        }
    }

    @Override
    public boolean checkAttemptStatus(String attemptID, String institutionId) {
        logger.debug("IMPS repo invoked to check validation attempt status for attemptID {}", attemptID);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("attemptID").is(attemptID).and("institutionId").is(institutionId));

            return mongoTemplate.exists(query, AccountNumberInfo.class);

        } catch (Exception e) {

            logger.error("Error while checking IMPS account validation attempt {}", e.getMessage());

            throw new SystemException(String.format("Error while checking IMPS account validation attempt [{%s}]", e.getMessage()));

        }
    }

    @Override
    public boolean checkAccountNumberInDedup(IMPSRequest impsRequest) {

        logger.debug("IMPS repo invoked to check account number in dedupe for valus {}", impsRequest);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("refID").is(impsRequest.getReferenceID())
                    .and("accountNumber").is(impsRequest.getAccountNumber())
                    .and("ifscCode").is(impsRequest.getIFSCCode())
                    .and("status").in(IMPSStatus.VALID.name(),IMPSStatus.INVALID.name()));

            return mongoTemplate.exists(query, AccountNumberInfo.class);

        } catch (Exception e) {

            logger.error("Error while checking account number in dedupe {}", e.getMessage());

            throw new SystemException(String.format("Error while checking account number in dedupe  [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean updateAccountNumberValidStatus(String institutionId, String refID, BankingDetails bankingDetails) {

        logger.debug("IMPS repo invoked to update accountNumberInfo against institutionId {} and refID {}", institutionId,refID);

        WriteResult writeResult;
        try {

            Query query = new Query();
            query.addCriteria(Criteria
                    .where("_id")
                    .is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            GoNoGoCustomerApplication customerApplication = mongoTemplate.findOne(query,GoNoGoCustomerApplication.class);

            List<BankingDetails> bankingDetailsList = null;
            if(null!=customerApplication && null!=customerApplication.getApplicationRequest()){

                bankingDetailsList = customerApplication.getApplicationRequest().getRequest().getApplicant().getBankingDetails();

                if(CollectionUtils.isEmpty(bankingDetailsList)){
                    bankingDetailsList = new ArrayList<BankingDetails>();
                    bankingDetailsList.add(bankingDetails);

                }else {
                    boolean flag = false;
                    for(BankingDetails bankingDetails1 : bankingDetailsList){
                        if(bankingDetails1.isEquals(bankingDetails) && flag==false){
                            bankingDetails1.setSelected(true);
                            flag = true;
                        }else{
                            bankingDetails1.setSelected(false);
                        }
                    }

                    if(!flag) {
                        bankingDetails.setSelected(true);
                        bankingDetailsList.add(bankingDetails);
                    }
                }
            }

            Update update = new Update();

            update.set("applicationRequest.request.applicant.bankingDetails", bankingDetailsList);

            writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.info("Write Result:->", writeResult);

            return writeResult.isUpdateOfExisting();

        } catch (DataAccessException e) {

            logger.error("error occurred while BankingDetails list with probable cause {[%s]}", e.getMessage());

            throw new SystemException(String.format("error occurred while saving BankingDetails list with probable cause [%s]", e.getMessage()));

        }
    }

    @Override
    public boolean checkAccountDetailsStatus(String identifier,String institutionId, String refID) {

        logger.debug("IMPS repo invoked to checking accountNumberInfo against account identifier {}", identifier);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("accountIdentifier").is(identifier)
                    .and("refID").is(refID)
                    .and("institutionId").is(institutionId)
                    .and("status").is(Status.INVALID.name()));

            return mongoTemplate.exists(query, AccountNumberInfo.class);

        } catch (Exception e) {

            e.printStackTrace();
            logger.error("Error while checking accountDetailsStatus {}", e.getMessage());

            throw new SystemException(String.format("Error while checking accountDetailsStatus [{%s}]", e.getMessage()));
        }
    }

    @Override
    public boolean checkBankInPaynimoBankMaster(String institutionId, String bankName) {
        logger.debug("IMPS repo invoked to checking bankName is exist or not in PaynimoBankMaster against institutionId {} and bankName {}", institutionId,bankName);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("bankName").regex(Pattern.compile(CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(bankName),
                            Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE))
                    .and("apiHit").is("Yes"));

            return mongoTemplate.exists(query, PaynimoBankMaster.class);

        } catch (Exception e) {

            e.printStackTrace();
            logger.error("Error while checking bankName is exist or not in PaynimoBankMaster {}", e.getMessage());

            throw new SystemException(String.format("Error while checking bankName is exist or not in PaynimoBankMaster [{%s}]", e.getMessage()));
        }
    }

    @Override
    public void saveBankingDetails(BankingDetails bankingDetails, String institutionId, String refID) {
        logger.debug("saving banking details angainst institutionId {} and refID {}", institutionId, refID);

        WriteResult writeResult;
        try {

            Query query = new Query();
            query.addCriteria(Criteria
                    .where("_id")
                    .is(refID)
                    .and("applicationRequest.header.institutionId")
                    .is(institutionId));

            GoNoGoCustomerApplication customerApplication = mongoTemplate.findOne(query,GoNoGoCustomerApplication.class);

            List<BankingDetails> bankingDetailsList = null;
            if(null!=customerApplication && null!=customerApplication.getApplicationRequest()){

                bankingDetailsList = customerApplication.getApplicationRequest().getRequest().getApplicant().getBankingDetails();

                if( CollectionUtils.isEmpty(bankingDetailsList)){
                    bankingDetailsList = new ArrayList<BankingDetails>();
                }

                bankingDetailsList.add(bankingDetails);

            }


            Update update = new Update();

            update.set("applicationRequest.request.applicant.bankingDetails", bankingDetailsList);

            writeResult = mongoTemplate.updateFirst(query, update, GoNoGoCustomerApplication.class);

            logger.info("Write Result:->", writeResult);

        } catch (DataAccessException e) {

            logger.error("error occurred while BankingDetails list with probable cause {[%s]}", e.getMessage());

            throw new SystemException(String.format("error occurred while saving BankingDetails list with probable cause [%s]", e.getMessage()));

        }
    }

}
