package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.request.dooperation.DoOperationLog;

import java.util.List;

/**
 * Created by mahesh on 30/8/17.
 */
public interface DoOperationRepository {

    /**
     *
     * @param refID
     * @param institutionId
     * @param doStatus
     * @return
     */
    boolean updateDoStatusInPostIpa(String refID, String institutionId, String doStatus);

    /**
     *
     * @param doOperationLog
     */
    void saveDoOperationLog(DoOperationLog doOperationLog);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    List<DoOperationLog> getDoOperationLog(String refId, String institutionId);

}
