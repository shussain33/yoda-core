package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Repository
public class MultibureauMongoRepository implements MultibureauRepository {

    private static final Logger logger = LoggerFactory.getLogger(MultibureauMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Set<String> getCoApplicantIds(String applicantId) {
        Set<String> coApplicantId = new TreeSet<String>();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("applicantRefId").is(applicantId));
            List<MultiBureauCoApplicantResponse> multiBureauCoApplicantResponse = mongoTemplate
                    .find(query, MultiBureauCoApplicantResponse.class);
            if (multiBureauCoApplicantResponse != null && multiBureauCoApplicantResponse.size() > 0) {
                coApplicantId.addAll(multiBureauCoApplicantResponse.stream().map(MultiBureauCoApplicantResponse::getCoApplicantRefId).collect(Collectors.toList()));
            }
            return coApplicantId;
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());

            logger.error("Error occurred while fetching CoApplicant with cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching CoApplicant with cause [{%s}]", e.getMessage()));
        }


    }

    @Override
    public MultiBureauCoApplicantResponse getCoApplicantMBResponse(String refId, String applicantId) throws Exception {

        logger.debug("getCoApplicantMBResponse repo started");

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("applicantRefId").is(refId).and("coApplicantRefId").is(applicantId));

            logger.debug("getCoApplicantMBResponse repo formed query {}", query);
            return mongoTemplate
                    .findOne(query, MultiBureauCoApplicantResponse.class);


        } catch (Exception e) {

            logger.error("{}",e.getStackTrace());

            logger.error("Error occurred while fetching CoApplicant with cause [{}]", e.getMessage());

            throw new SystemException(String.format("Error occurred while fetching CoApplicant with cause [{%s}]", e.getMessage()));


        }

    }
}
