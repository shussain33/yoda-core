/**
 * kishorp11:57:27 PM  Copyright Softcell Technolgy
 */
package com.softcell.dao.mongodb.repository;

import com.mongodb.WriteResult;
import com.softcell.constants.Product;
import com.softcell.constants.SequenceType;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.OtpVerificationData;
import com.softcell.gonogo.model.coorgination.icici.SchemeDetails;
import com.softcell.gonogo.model.coorgination.icici.SourcingDetails;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.ItrVerification;
import com.softcell.gonogo.model.core.verification.PropertyVisit;
import com.softcell.gonogo.model.core.verification.PropertyVisitDetails;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.finbit.FinbitData;
import com.softcell.gonogo.model.finbit.FinbitRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.quickcheck.EMandateDetails;
import com.softcell.gonogo.model.quickcheck.EMandateDetailsCallLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.InvoiceDetails;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.ApplicationResponse;
import com.softcell.gonogo.model.response.CroQueue;
import com.softcell.gonogo.model.response.PosidexResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.component.finished.AuditData;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author kishorp
 */
public interface ApplicationRepository {

    /**
     * @param applicationRequest New Application request from database
     * @return return gonogo refId if method save successfully.
     */
    String saveApplication(ApplicationRequest applicationRequest) ;


    /**
     * @param applicationId
     * @return
     */
    ApplicationRequest getCheckStatus(String applicationId);


    /**
     * @param croID
     * @param institutionId
     * @return
     */
    List<ApplicationRequest> fetchByCroIDAndInstitution(String croID,
                                                        String institutionId);

    /**
     * @param goNoGoCustomerApplication
     * @return
     */
    boolean saveGoNoGoCustomerApplication(
            GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     * cro update their decision here (Approved ,declined,pending)
     *
     * @param croApprovalRequest
     * @return
     */
    boolean updateCroDecision(CroApprovalRequest croApprovalRequest) ;

    /**
     * @param applicationID
     * @param institutionID
     * @param croID
     * @param referenceIdreferenceId
     * @param status
     * @return
     */
    boolean resetStatus(String applicationID, String institutionID,
                        String croID, String referenceIdreferenceId, String status);

    /**
     * @param croApprovalRequest
     * @return
     */
    boolean updateOnHoldCroDecision(CroApprovalRequest croApprovalRequest);

    /**
     * @param referenceId
     * @param institutionID
     * @return
     */
    ApplicationResponse checkStatus(String referenceId, String institutionID) ;


    /**
     * @param croQueueRequest
     * @return
     */
    Stream<CroQueue> getCroQueue(CroQueueRequest croQueueRequest) ;

    /**
     * @param croQueueRequest
     * @return List<CroQueue>
     */
    List<CroQueue> getCroQueueCriteria(CroQueueRequest croQueueRequest) ;

    /**
     * @param croQueueRequest
     * @return
     */
    List<CroQueue> getCro2Queue(CroQueueRequest croQueueRequest) ;

    /**
     * @param croQueueRequest
     * @return List<CroQueue>
     */
    List<CroQueue> getCro3Queue(CroQueueRequest croQueueRequest) ;

    /**
     * @param croQueueRequestObj
     * @return List<CroQueue>
     */
    List<CroQueue> getCro4Queue(CroQueueRequest croQueueRequestObj) ;

    /**
     * @param refID           cro -screen second call
     * @param requestResource
     * @return
     */
    GoNoGoCroApplicationResponse getApplicationByRefId(String refID, String requestResource, String institutionId) ;

    /**
     * @param refID
     * @param requestSource
     * @param institutionId
     * @return
     * @
     */
    GoNoGoCroApplicationResponse getApplicationByRefIdForAuditLog(String refID,
                                                                  String requestSource, String institutionId) ;

    /**
     * This method is return coapplicant based on coapplicant id and ref id for gonogocustomerapplication
     *
     * @param refId
     * @param coApplicantId
     * @param institutionId
     * @return
     */
    CoApplicant getCoApplicantBycoApplicantId(String refId, String coApplicantId, String institutionId) ;

    /**
     * @param dsaId
     * @return
     */
    String getCroId(String dsaId, String institutionId);

    /**
     * @param croDsaMaster
     * @return
     */
    boolean saveCroDsaMaster(CroDsaMaster croDsaMaster);

    /**
     * @param postIpaRequest
     * @return
     */
    boolean savePostIpaDetails(PostIpaRequest postIpaRequest) ;

    /**
     * @param refID
     * @return
     */
    PostIpaRequest fetchPostIpaDetails(String refID, String institutionId) ;

    /**
     * @param applicationRequest request to be updated or inserted in database
     * @return true if successfull operation else false
     */
    boolean upsertApplication(ApplicationRequest applicationRequest) ;

    /**
     * @param retrieveApplicationRequest request object with DOB and Phone.
     * @return retrieved Application
     */
    ApplicationRequest getApplication(
            RetrieveApplicationRequest retrieveApplicationRequest);

    /**
     * based on dealerId latestSequence is fetched form DB
     *
     * @param dealerId
     * @param institutionId
     * @return
     */
    String getSequence(String dealerId, String institutionId);

    /**
     * @param productID
     * @param institutionId
     * @return
     */
    String getSequenceForNewProducts(String productID, String institutionId);

    /**
     * update stage for postIpa
     *
     * @param refrenceId
     * @param stage
     * @param institutionId
     * @return
     */
    String setPostIpaStage(String refrenceId, String stage, String institutionId) ;

    /**
     * @param refID
     * @return
     */
    @Deprecated
    ApplicationRequest getApplicationRequestByRefId(String refID, String intitutionId) ;

    /**
     * @param partialSaveRequest
     * @return
     */
    ApplicationRequest getPartialSaveRequest(
            PartialSaveRequest partialSaveRequest);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    HashMap<String, List<String>> getKycMap(String refId, String institutionId);

    /**
     * @param refID
     * @return
     */
    PostIPA getPostIPA(String refID, String institutionId) ;

    /**
     * @param refID
     * @param losDetails
     */
    boolean updateLosDetails(String refID, LOSDetails losDetails, String institutionId) ;

    /**
     * @param refID
     */
    boolean softDeleteDOReport(String refID, String institutionId, String fileName) ;

    /**
     * @param activityLogs
     */
    boolean saveActivityLog(ActivityLogs activityLogs) ;

    /**
     * @param refID
     * @return
     */
    boolean isSubmited(String refID, String action, String institutionId);

    /**
     * @param auditDataRequest All parameter of auditdatarequest is mandatory.
     * @return list of all audit records.
     */
    List<AuditData> getBreAuditData(AuditDataRequest auditDataRequest);

    /**
     * @param refID
     * @param invoiceDetail
     * @return
     */
    boolean updateInvoiceDetails(String refID,
                                 InvoiceDetails invoiceDetail, String institutionId) ;

    /**
     * This is newely added fix to get Dealer name against dealer id.
     *
     * @param header
     * @return
     */
    DealerEmailMaster getDealerEmailMaster(Header header) ;

    /**
     * This method returns the application tracking log
     * based on refrecnce id
     *
     * @param checkApplicationStatus
     * @return
     */
    List<ApplicationTracking> getAppTrackingLog(CheckApplicationStatus checkApplicationStatus);

    /**
     * This method retrieves application based on RefId and InstId.
     *
     * @param refID
     * @param intitutionId
     * @return
     */
    GoNoGoCroApplicationResponse getGoNoGoCustomerApplicationByRefId(String refID, String intitutionId) ;

    GoNoGoCustomerApplication getGoNoGoCustomerApplicationByRefId(String refID) ;

    /**
     * This method returns parent reference id for requested reference id
     *
     * @param refId
     * @return
     */
    String getParentRefId(String refId);


    /**
     * @param institution
     * @param productID
     * @param productName
     * @return
     */
    boolean isProductExist(String institution, String productID,
                           String productName);

    /**
     * @param fileHandoverDetails
     * @return
     */
    boolean upsertFileHandoverDetails(FileHandoverDetails fileHandoverDetails) ;

    /**
     * @param fileHandoverDetailsRequest
     * @return
     */
    FileHandoverDetails getFileHandoverDetails(FileHandoverDetailsRequest fileHandoverDetailsRequest) ;

    /**
     * This method is used to get negative area funding details against the pincode entered by the user.
     *
     * @param negativeAreaFundingRequest
     * @return
     */
    List<NegativeAreaGeoLimitMaster> getNegativeAreaFundingDetails(NegativeAreaFundingRequest negativeAreaFundingRequest) ;

    /**
     * @param postIpaRequest
     */
    void setMultiProductUpdatedParameter(PostIpaRequest postIpaRequest);

    /**
     * @param refId
     * @return
     */
    boolean isGoNoGoCustomerAppPresent(String refId);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    boolean isApplicationRequestExist(String refId, String institutionId);

    /**
     * @param updateReferencesRequest
     */
    WriteResult updateApplicantReferences(UpdateReferencesRequest updateReferencesRequest);

    /**
     * @param bankingDetailsRequest
     * @return
     */
    WriteResult updateBankingDetails(BankingDetailsRequest bankingDetailsRequest);

    /**
     * @param lOSDetailsRequest
     * @return
     */
    boolean updateOpsData(LOSDetailsRequest lOSDetailsRequest);

    /**
     * @param institutionId
     * @param productAliasName
     * @param gngAssetCategory
     * @return
     */
    EwInsGngAssetCategoryMaster getAssetCategoryAndOemWarranty(String institutionId, String productAliasName, String gngAssetCategory);

    /**
     * @param institutionId
     * @param productAliasName
     * @param eWAssetCategory
     * @param gngAssetPrice
     * @return
     */
    EWPremiumMaster getExtendedWarrantyPremium(String institutionId, String productAliasName, String eWAssetCategory, double gngAssetPrice);

    /**
     * @param institutionId
     * @param productAliasName
     * @param eWAssetCategory
     * @return
     */
    boolean checkGngAssetAndEWAssetMapping(String institutionId, String productAliasName, String eWAssetCategory);

    /**
     * @param extendedWarrantyDetails
     * @return
     */
    void updateExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    ExtendedWarrantyDetails fetchExtendedWarrantyDetails(String refId, String institutionId);

    /**
     * @param institutionId
     * @param productAliasName
     * @param insuranceAssetCat
     * @return
     */
    boolean checkGngAssetAndInsuranceAssetMapping(String institutionId, String productAliasName, String insuranceAssetCat);

    /**
     * @param institutionId
     * @param productAliasName
     * @param insuranceAssetCat
     * @param gngAssetPrice
     * @return
     */
    InsurancePremiumMaster getInsurancePremiumData(String institutionId, String productAliasName, String insuranceAssetCat, double gngAssetPrice);

    /**
     * @param insurancePremiumDetails
     * @return
     */
    void updateInsuranceDetails(InsurancePremiumDetails insurancePremiumDetails);

    /**
     * @param creditVidyaDetails
     * @return
     */
    void updateCreditVidyaDetails(CreditVidyaDetails creditVidyaDetails);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    InsurancePremiumDetails fetchInsuranceDetails(String refId, String institutionId);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    CreditVidyaDetails fetchCreditVidyaDetails(String refId, String institutionId);

    /**
     * @param referenceId
     * @return
     */
    SerialNumberInfo getSerialNumberInfo(String referenceId);

    /**
     * @param updateRevisedEmiRequest
     * @return
     */
    boolean updateRevisedEmiAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest);

    /**
     * @param extendedWarrantyDetails
     * @return
     */
    boolean softDeleteExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails);

    /**
     * @param refId
     * @return
     */
    boolean softDeleteInsuranceDetails(String refId);

    /**
     * @param refID
     * @param institutionId
     * @return
     */
    boolean postIpaExists(String refID, String institutionId);


    /**
     * @param gstDetailsRequest
     * @return
     */
    boolean updateGstDetails(GstDetailsRequest gstDetailsRequest);

    /**
     * @param customerFinancialProfileRequest
     */
    void submitCustomerFinancialProfile(final CustomerFinancialProfileRequest customerFinancialProfileRequest);

    /**
     * @param refId
     * @return
     */
    GoNoGoCustomerApplication getGoNoGoCustomerApplicationOnlyByRefId(String refId);

    /**
     * Update the TVR remark and status
     *
     * @param updateTvrStatusRequest
     * @return
     * @
     */
    boolean updateTvrStatus(UpdateTvrStatusRequest updateTvrStatusRequest) ;

    /**
     * @param institutionId
     * @param sequenceType
     * @return
     */
    String getSequenceForProspectNumber(String institutionId, String sequenceType);


    /**
     * @param institutionId
     * @param dealerId
     * @param name
     * @return
     */
    boolean isSerialOrImeiValidationSkipApplicableToDealer(String institutionId, String dealerId, String name);

    /**
     * <<<<<<< HEAD
     *
     * @param branchName
     * @param instId
     * @return
     */
    BranchMaster getDealerBranchState(String branchName, String instId);

    /**
     * @param instId
     * @param refId
     * @return
     */
    List<BankingDetails> getBankAccountHolderName(String instId, String refId);

    /**
     * @param refId
     * @param instId
     * @return
     */
    GstDetails fetchGstDetails(String refId, String instId);


    String getSequenceForTvs(String institutionId, Product product, String branchCode);

    /**
     * @param refId
     * @param agreementNum
     * @param stage
     * @return
     * @
     */
    boolean updateDisbursementData(String refId, String agreementNum, String stage, String dealerId) ;


    /**
     * @param applicationStatusLogRequest
     * @return
     */
    PosidexResponse getPosidexResponse(ApplicationStatusLogRequest applicationStatusLogRequest);

    /**
     * @param refID
     * @return
     */
    GoNoGoCustomerApplication getGonogoDocument(String refID);

    /**
     * @param additionalDedupeFieldsRequest
     * @return
     */
    boolean updateDedupeFields(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest);

    /**
     * @param institutionId
     * @param dedupedRefId
     */
    void updateDedupeApplicationStatus(String institutionId, String dedupedRefId, String appStatus, String appStage, GoNoGoCustomerApplication goNoGoCustomerApplication, String croJustificationRemark);

    /**
     * @param refId
     * @param instId
     * @return
     */
    ApplicationRequest getApplicationRequest(String refId, String instId);

    /**
     * @param additionalDedupeFieldsRequest
     * @return
     */
    boolean updateDedupeFieldsInApplicationRequest(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest);

    /**
     * @param institutionId
     * @param branchCode
     * @param product
     * @param seqType
     * @return
     */
    long getIncrIdBySeqType(String institutionId, String branchCode, Product product, SequenceType seqType, int n);


    /**
     * @param institutionId
     * @return
     */
    String getUpdatedCountForEMandate(String institutionId);

    InsurancePremiumMaster getInsurancePremium(String institutionId, String productAliasName);

    InsurancePremiumMaster getInsurancePremiumAgainstProvider(String institutionId, String insuranceType);

    boolean updateAddonServiceRevisedAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest);

    /**
     * @param refID
     * @param institutionId
     * @return
     */
    boolean isApplicationStatusCancelled(String refID, String institutionId);

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    String getApplicationStage(String institutionId, String refID);

    /**
     * @param applicationRequest
     * @return
     * @
     */
    boolean updateGoNoGoCustomerApplication(ApplicationRequest applicationRequest) ;

    void updateApplicationInElasticsearch(GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     * @param geoLimitRequest
     * @return
     */
    boolean checkGeoLimitApplicableForBranch(GeoLimitRequest geoLimitRequest, String branchId);

    /**
     * @param institutionId
     * @param dealerId
     * @return
     */
    String getBranchIdAgainstDealerId(String institutionId, String dealerId);

    /**
     * @param institutionId
     * @param branchId
     * @param pin
     * @return
     */
    boolean checkGeoLimitIsApplicableForBranch(String institutionId, String branchId, long pin);

    boolean saveCamDetails(CamDetailsRequest camDetailsRequest, String camType);

    boolean saveCamDetails(String refId, String institutionId, CamDetails camDetails, String camType);
    CamDetails fetchCamDetails(CamDetailsRequest camDetailsRequest);

    CamDetails fetchCamDetails(String refId, String institutionId, String camType);

    boolean saveVerificationDetails(VerificationRequest verificationRequest, String verificationType, Map<String, Set<String>> verificationStatusDealersMap);

    boolean saveItrVerificationDetails(String refId , String institutionId, List<ItrVerification> itrVerificationList);

    boolean saveVerificationDetails(String refId, Header header, String verificationType,
                                    VerificationDetails verificationDetails,
                                    Map<String, Set<String>> verificationStatusDealersMap, ApplicationRequest applicationRequest);

    boolean saveVerificationDetails(String refId, Header header, String verificationType,
                                    VerificationDetails verificationDetails,
                                    Map<String, Set<String>> verificationStatusDealersMap);

    VerificationDetails fetchVerificationDetailsByRefId(VerificationRequest verificationRequest);

    boolean savePropertyVisitDetails(PropertyVisitRequest propertyVisitRequest);

    PropertyVisit fetchPropertyVisitDetailsByRefId(PropertyVisitRequest propertyVisitRequest);

    boolean savePersonalDiscussionDetails(ApplicationRequest applicationRequest);

    Repayment fetchRepaymentDetails(RepaymentRequest repaymentRequest);

    boolean saveRepaymentDetails(Repayment repayment);

    Repayment fetchRepaymentDetails(String refId , String institutionId);

    // boolean saveLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest);

    void updateLegalVerificationMultiDetails(LegalVerificationRequest legalVerificationRequest);

    boolean saveLegalVerificationMultiDetails(LegalVerificationRequest legalVerificationRequest, Map<String, Set<String>> verificationStatusDealersMap);

    LegalVerification fetchLegalVerificatioDetailsByRefId(LegalVerificationRequest legalVerificationRequest);

    LegalVerification fetchLegalVerificatioDetails(String refId, String instId);

    void releaseAllocatedApplications(String userId, String institutionId);

    void changeApplicationLockStatus(String userId, String institutionId, String refId, boolean isLocked);

    boolean updateGoNoGoCustomerApplication(String refId, AllocationInfo allocationInfo, String institutionId) ;

    boolean saveCustomerCreditDocs(CustomerCreditRequest customerCreditRequest);

    CustomerCreditDocs fetchCustomerCreditDocs(CustomerCreditRequest customerCreditRequest);

    boolean saveEligibilityDetails(EligibilityDetails eligibilityDetails);

    boolean saveEligibilityDetails(EligibilityRequest eligibilityRequest);

    EligibilityDetails fetchEligibilityDetails(EligibilityRequest eligibilityRequest);

    boolean updateApplicationForAllocationInfo(String refId, AllocationInfo allocationInfo, String institutionId) ;

    EligibilityDetails fetchEligibilityDetails(String refId, String instId);

    EligibilityDetails fetchEligibilityDetail(String refId, String instId);

    boolean saveListOfDocs(ListOfDocsRequest listOfDocsRequest);

    ListOfDocs fetchListOfDocs(ListOfDocsRequest listOfDocsRequest);

    CustomerCreditDocs fetchCustomerCreditDocs(String refId, String institutionId);

    boolean saveLoanChargesDetails(LoanChargesRequest loanChargesRequest);
    boolean saveLoanChargesDetails(String refId, String institutionId,  LoanCharges loanCharges);

    LoanCharges fetchLoanChargesDetails(LoanChargesRequest loanChargesRequest);
    LoanCharges fetchLoanChargesDetailsByRefId(String refId, String institutionId);

    boolean triggerVerification(VerificationRequest verificationRequest);

    boolean triggerVerificationForLegal(VerificationRequest verificationRequest);

    void updateValuationDetails(ValuationRequest valuationRequest);

    boolean saveValuationDetails(ValuationRequest valuationRequest, Map<String, Set<String>> verificationStatusDealersMap,
                                 Map<String, Valuation> triggeredToAgencies);

    boolean deleteValuationDetails(ValuationRequest valuationRequest);

    Valuation fetchValuationDetails(ValuationRequest valuationRequest);

    boolean triggerVerificationForValuation(VerificationRequest verificationRequest);

    boolean updateStageId(ApplicationRequest applicationRequest, String applicationStatus);

    boolean updateCurrentSatgeStageId(ApplicationRequest applicationRequest, String currentStageId);

    boolean updateCompletedInfo(String refId, String institutionId, String stepId, String userId, String role);

    boolean updateCompletedInfo(GoNoGoCustomerApplication gngApp, String completedStep, String userId, String role);

    boolean saveDeviation(String refId, String instId, DeviationDetails deviationDetails);

    boolean updateDeviation(String refId, String instId, DeviationDetails deviationDetails);

    DeviationDetails fetchDeviationDetails(String refId, String instId);

    DisbursementMemo fetchDMDetails(DMRequest dmRequest);

    DisbursementMemo fetchDMDetails(String refId, String institutionId);

    boolean saveDMDetails(DMRequest dmRequest);

    boolean updateExternalServiceFlag(ApplicationRequest applicationRequest, String serviceType);

    SanctionConditions fetchSanctionConditions(SanctionConditionRequest sanctionConditionRequest);

    SanctionConditions fetchSanctionConditions(String refId, String institutionId);

    boolean saveSanctionConditions(SanctionConditionRequest sanctionConditionRequest);

//    boolean saveDededupeRemarks( DedupeRemarkRequest dedupeRemarkRequest);

    DedupeMatch fetchDedupeInfo(String refId, String instId);

    boolean updateApplicationStage(String refId, String institutionId, String stageName, String applicationStatus);
    VerificationDetails fetchVerificationDetailsByRefId(String refId, String institutionId);

    int updateCompletedInfoGonogo(UpdateGNGRequest updateGNGRequest, List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList);

    List<GoNoGoCustomerApplication> fetchGNGData(UpdateGNGRequest updateGNGRequest);

    GoNoGoCustomerApplication fetchGNGDataByMob(GetGngByMobRequest getGngByMobRequest);

    LegalVerificationDetails fetchOldLegalVerificatioDetailsByRefId(LegalVerificationRequest legalVerificationRequest);

    boolean updateExternalServiceFlagForLMS(ApplicationRequest applicationRequest, String name);

    boolean saveCamSummary(ApplicationRequest applicationRequest , int noOfYearAtResidence);

    boolean saveEligibilityDetails(CamDetailsRequest camDetailsRequest);

    boolean savePerfiosData(PerfiosData perfiosData);

    PerfiosData fetchPerfiosData(String refId, String institutionId, String acknowledgeId);
    String getProduct(String refId, String institutionId);

    CamDetails fetchCamSummaryByRefId(String refId, String instituteId);

    long fetchCountAllApplications(String institutionId, List<String> refIds);

    List<GoNoGoCustomerApplication> fetchAllApplications(String institutionId, List<String> refIds, int skip);

    boolean updateApplicantCoApplicantData(String refId, Map<String, CustomData> applicantCoApplicantData);

    long fetchCountOfAllCamDetails(String institutionId, List<String> refIds);

    List<CamDetails> fetchAllCamDetails(String institutionId, List<String> refIds, int skip);

    boolean updateApplicantCoApplicantCamData(String refId, Map<String, CustomData> applicantCoApplicantData);

    boolean saveCoOriginationDetails(OriginationRequest originationRequest);

    CoOrigination fetchCoOriginationDetails(OriginationRequest originationRequest);

    CoOrigination fetchCoOriginationDetails(String refId, String institutionId);

    SchemeDetails fetchSchemeDetails(String institutionId, String product, String scheme, String collateralUsage, String occupationType );

    SourcingDetails fetchSourcingDetails(String institutionId, String product, String branchName, String location);

    PinCodeMaster fetchPinCodeDetails(String institutionId, String zipCode);

    EmployerMaster fetchEmployerDetails(String institutionId, String employerName);

    List<GoNoGoCustomerApplication> fetchGoNoGoCustomerApplication(String institutionId, Date startDate, Date endDate);

    List<GoNoGoCustomerApplication> findGNGRefIdListByLastUpdatedDate(String institutionId, Date startDate, Date endDate);

    boolean updateApplicationWithBranch(String key, Branch value);

    Valuation fetchValuationDetails(String refId, String institutionId);

    List<GoNoGoCustomerApplication> fetchAllCasesBeforeDefinedTime(Date startDate, Date endDate);
    boolean updateKycDetails(GoNoGoCustomerApplication application, boolean isApplicant, boolean isCoApplicant);

    PerfiosData fetchPerfiosData(String refId, String institutionId);

    PerfiosData fetchPerfiosData(String ackId);

    boolean deleteVerificationDetails(VerificationRequest verificationRequest, String verificationType);

    boolean updatePropertyVistApplicantName(String refId, String institutionId, Name appName, List<PropertyVisitDetails> propertyVisitDetailsList);

    Name fetchApplicantName(String refId);

    boolean saveValuationDetails(Valuation valuation,String refId , String institutionId);

    EMandateDetails fetchEMandateDetails(String refId);

    EMandateDetails fetchEMandateDetailsByAcknowledgementId(String acknowledgementId);

    void updateEMandateDetails(EMandateDetails eMandateDetails);

    void saveEMandateDetailsCallLog(EMandateDetailsCallLog eMandateDetailsCallLog);

    ListOfDocs fetchListOfDocs(String refId, String institutionId);

    boolean updateMifinCallLog(MiFinCallLog miFinCallLog , String refId , String requestType);
    boolean updateMifinCallLog(MiFinCallLog miFinCallLog , String refId , String requestType , String appId);

    boolean saveCamSummaryDetails(ApplicationRequest applicationRequest, CamDetails camDetails);

    ApplicationRequest getApplicantData(String refId, String instId);

    boolean saveLegalVerificationDetailsForTopUp(LegalVerificationRequest legalVerificationrequest , String refId , String institutionId);


    boolean saveCoOriginationDetails(String refId, String institutionId, CoOrigination coOrigination);

    boolean updateBranchData(String refId, Branch branchV2);

    boolean updateMifinData(String refId, String applicantCode, String prospectCode);

    boolean saveInsuranceData(InsuranceRequest insuranceRequest);

    InsuranceData fetchInsuranceData(String refId);
    boolean saveInsuranceData(InsuranceData insuranceData, String refId);

    FinbitData fetchFinbitData(String ackId);

    FinbitData fetchFinbitData(String refId, String institutionId);

    boolean saveFinbitData(FinbitRequest finbitRequest);

    GoNoGoCustomerApplication fetchScoringData(String refId);

    GoNoGoCustomerApplication getGngApplication(String refId) throws Exception;

    boolean updateBureauCount(String refId, String institutionId, String appId);

    void deleteOCRData(OCRRequest ocrRequest);

    OCRDetails getOCRData(OCRRequest ocrRequest);

    OCRDetails getOCRData(String refId , String institutionId);

    void saveOCRData(OCRRequest ocrRequest);

    boolean resetApplicationStatus(String referenceId, String institutionId, String applicantId, String bucket, String status);

    GoNoGoCustomerApplication fetchApplicantEligibility(String refId);

    boolean saveApplicantEligibility(String refId, Applicant applicant);

    boolean saveMismatchDetails(MismatchRequest mismatchRequest,List<Mismatch> mismatchList);

    MismatchRequest fetchMismatchDetails(MismatchRequest mismatchRequest);

    boolean updateApplicationBucket(String refId, String institutionId);

    GoNoGoCustomerApplication fetchApplicationBucket(String refId);

    boolean saveFaceMatchData(FaceMatchRequest faceMatchRequest);

    FaceMatchRequest fetchFaceMatchData(String refId);

    void updateMifinVerificationId(VerificationDetails verificationDetails);

    MasterSchedulerConfiguration findMasterSchedularConfiguration(String institutionId, String masterName);

    MasterSchedulerConfiguration findMasterSchedularConfigurationBasisOnInstId(String institutionId, String masterName);

    List<ActivityLogs> fetchUserActivityLogsByRefId(String refId, String institutionId);

    /**
     * added for FOS (can add remark for SBFC)
     */
    public boolean saveFosRemark(CroApprovalRequest croApprovalRequest);

    List<SmsServiceReqResLog> fetchSmsServiceLog(String identifier);

    void saveVerificationOtp(OtpVerificationData otpVerificationData);

    String isValidOtp(String institutionId, String uuid, String otp);
}

