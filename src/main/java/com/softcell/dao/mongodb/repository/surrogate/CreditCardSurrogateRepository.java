package com.softcell.dao.mongodb.repository.surrogate;

import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogatEntries;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateResponse;

/**
 * @author yogeshb
 */
public interface CreditCardSurrogateRepository {

    /**
     * This method saves request and response.
     *
     * @param creditCardSurrogatStore
     * @return
     */
    public boolean saveCreditCardSurrogateTransaction(
            CreditCardSurrogatEntries creditCardSurrogatStore);

    /**
     * This method check credit card entry already available in db or not.
     *
     * @param creditCardSurrogateRequest
     * @return
     */
    public boolean isExist(CreditCardSurrogateRequest creditCardSurrogateRequest);

    /**
     * this method get Credit Card Response from database.
     *
     * @param creditCardSurrogateRequest
     * @return
     */
    public CreditCardSurrogateResponse getCreditCardSurrogateDetails(
            CreditCardSurrogateRequest creditCardSurrogateRequest);

}
