package com.softcell.dao.mongodb.repository;

import com.softcell.config.ServiceCommunication;

/**
 * Created by ssg0268 on 11/6/19.
 */


public interface UrlConfigurationRepository {


    ServiceCommunication getServiceCommunication(String instId, String type);
}
