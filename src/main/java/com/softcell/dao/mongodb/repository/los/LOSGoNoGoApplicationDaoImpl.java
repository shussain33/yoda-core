
package com.softcell.dao.mongodb.repository.los;

import com.google.gson.Gson;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class LOSGoNoGoApplicationDaoImpl implements LOSGoNoGoApplicationDao {

	private final Logger logger = LoggerFactory.getLogger(LOSGoNoGoApplicationDaoImpl.class);

	private static final String COLLECTION_GoNoGoCustomerApplication = "goNoGoCustomerApplication";
	private static final String COLLECTION_PostIpaRequest = "postIpaDoc";
	private static final String COLLECTION_SerialNumberInfo = "serialNumberInfo";
	private static final String COLLECTION_InsurancePremiumDetailsObject = "insurancePremiumDetails";
	private static final String COLLECTION_ExtendedWarrantyDetails = "extendedWarrantyDetails";
	private static final String COLLECTION_SchemeMasterData = "schemeMaster";
	private static final String COLLECTION_ESignedLog = "eSignedLog";
	private static final String COLLECTION_FileUploadRequest="fs.files";

	Gson gson = new Gson();

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	GridFsTemplate gridFsTemplte;

	@Override
	public List<GoNoGoCustomerApplication> getAllgoNoGoCustomerApplicationslistRecord() {
		logger.info("in getAllgoNoGoCustomerApplicationslistRecord daoIMPL");
		Query query = new Query();
		query.limit(10);
		query.with(new Sort(Sort.Direction.DESC, "lastModifiedDate"));

		// mongoOperation.find(query, Domain.class);

		return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
	}

	@Override
	public GoNoGoCustomerApplication getGONOGOApplicationAsPerCaseId(String gngRefId) {
		//List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList = new ArrayList<GoNoGoCustomerApplication>();
		GoNoGoCustomerApplication goNoGoCustomerApplication = new GoNoGoCustomerApplication();
		logger.info("in getGONOGOApplicationAsPerCaseId daoIMPL");
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("_id").is(gngRefId));
			//goNoGoCustomerApplicationList = mongoTemplate.find(query1, GoNoGoCustomerApplication.class,
			//COLLECTION_GoNoGoCustomerApplication);
			List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList = mongoTemplate.find(query1, GoNoGoCustomerApplication.class);

			if (goNoGoCustomerApplicationList.size() > 0) {
				goNoGoCustomerApplication = goNoGoCustomerApplicationList.get(0);
				return goNoGoCustomerApplication;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Not Matched in getGONOGOApplicationAsPerCaseId daoIMPL");
			logger.error("Error occured in getGONOGOApplicationAsPerCaseId daoIMPL",e);
		}
		return null;
	}

	@SuppressWarnings("static-access")
	@Override
	public List<GoNoGoCustomerApplication> getsearchRecord(String date, String stage) {
		try {
			Query query1 = new Query();
			date = date.replace(" ", "+");

			// @@
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

			LocalDateTime dt = LocalDateTime.parse(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

			LocalDateTime dt1 = dt.plusDays(1);

			Date utildt = Date.from(dt.atZone(ZoneId.systemDefault()).toInstant());
			Date utildt1 = Date.from(dt1.atZone(ZoneId.systemDefault()).toInstant());

			Query query2 = new Query();
			query2.addCriteria((Criteria.where("applicationRequest.currentStageId").is(stage)));

			Criteria criteria = new Criteria();

			Criteria findCriteria = Criteria.where("goNoGoCustomerApplication.applicationRequest.currentStageId")
					.is(stage);

			List<GoNoGoCustomerApplication> list1 = mongoTemplate.find(query2, GoNoGoCustomerApplication.class,
					COLLECTION_GoNoGoCustomerApplication);
			logger.info("COLLECTION_GoNoGoCustomerApplication size::"+list1.size());
			query1.addCriteria(Criteria.where("dateTime").gte(utildt).lte(utildt1));
			logger.info(query1.toString());
			List<GoNoGoCustomerApplication> list = mongoTemplate.find(query1, GoNoGoCustomerApplication.class,
					COLLECTION_GoNoGoCustomerApplication);

			return list1;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getsearchRecord daoIMPL",e);
			return null;
		}

	}

	@Override
	public PostIpaRequest getpostIpaRequestBysRefID(String RefID) {
		List<PostIpaRequest> PostIpaRequestList = new ArrayList<PostIpaRequest>();
		PostIpaRequest postIpaRequestect = new PostIpaRequest();
		try {
			logger.info("in getpostIpaRequestBysRefID");
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("_id").is(RefID));
			PostIpaRequestList = mongoTemplate.find(query1, PostIpaRequest.class, COLLECTION_PostIpaRequest);

			if (PostIpaRequestList.size() > 0) {
				postIpaRequestect = PostIpaRequestList.get(0);
				return postIpaRequestect;
			} else {
				return null;
			}

		} catch (Exception e) {
			logger.info("NOT MATCHED in getpostIpaRequestBysRefID daoIMPL");
			logger.error("Error occured in getpostIpaRequestBysRefID daoIPL",e);
		}
		return null;
	}

	@Override
	public SerialNumberInfo getSerialNumberInfoBysRefID(String refID) {
		List<SerialNumberInfo> serialNumberInfoList = new ArrayList<SerialNumberInfo>();
		SerialNumberInfo serialNumberInfotect = null;
		logger.info("in getSerialNumberInfoBysRefID:");
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("refID").is(refID));
			serialNumberInfoList = mongoTemplate.find(query1, SerialNumberInfo.class, COLLECTION_SerialNumberInfo);


			if (serialNumberInfoList.size() > 0) {
				serialNumberInfotect = serialNumberInfoList.get(0);
			}

			return serialNumberInfotect;
		} catch (Exception e) {
			logger.error("in getSerialNumberInfoBysRefID daoIMPL:",e);
			return null;
		}

	}

	@Override
	public InsurancePremiumDetails getinsurancePremiumDetailsBysRefID(String refID) {
		List<InsurancePremiumDetails> insurancePremiumDetailsList = new ArrayList<InsurancePremiumDetails>();
		InsurancePremiumDetails insurancePremiumDetailsect = new InsurancePremiumDetails();
		logger.info("in getinsurancePremiumDetailsBysRefID daoIMPL");
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("refId").is(refID));
			insurancePremiumDetailsList = mongoTemplate.find(query1, InsurancePremiumDetails.class,
					COLLECTION_InsurancePremiumDetailsObject);

			if (insurancePremiumDetailsList.size() > 0) {
				insurancePremiumDetailsect = insurancePremiumDetailsList.get(0);
				return insurancePremiumDetailsect;
			} else {
				return null;
			}

		} catch (Exception e) {
			logger.info("getinsurancePremiumDetailsBysRefID daoIMPL");
			logger.error("in getinsurancePremiumDetailsBysRefID daoIMPL",e);
		}
		return null;
	}

	@Override
	public ExtendedWarrantyDetails getextendedWarrantyDetailsBysRefID(String refID) {
		logger.info("in getextendedWarrantyDetailsBysRefID:");
		List<ExtendedWarrantyDetails> extendedWarrantyDetailsList = new ArrayList<ExtendedWarrantyDetails>();
		ExtendedWarrantyDetails extendedWarrantyDetailsect = new ExtendedWarrantyDetails();
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("refId").is(refID));
			extendedWarrantyDetailsList = mongoTemplate.find(query1, ExtendedWarrantyDetails.class,
					COLLECTION_ExtendedWarrantyDetails);

			if (extendedWarrantyDetailsList.size() > 0) {
				extendedWarrantyDetailsect = extendedWarrantyDetailsList.get(0);
				return extendedWarrantyDetailsect;
			} else {
				return null;
			}

		} catch (Exception e) {
			logger.info("in getextendedWarrantyDetailsBysRefID NOT MATCHED");
			logger.error("in getextendedWarrantyDetailsBysRefID:",e);
		}
		return null;
	}

	@Override
	public SchemeMasterData getschemeMasterDataBysRefID(String schemeID) {
		List<SchemeMasterData> schemeMasterDataList = new ArrayList<SchemeMasterData>();
		SchemeMasterData schemeMasterDataect = new SchemeMasterData();
		logger.info("in getschemeMasterDataBysRefID daoIMPL::");
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("schemeID").is(schemeID));
			schemeMasterDataList = mongoTemplate.find(query1, SchemeMasterData.class, COLLECTION_SchemeMasterData);

			if (schemeMasterDataList.size() > 0) {
				schemeMasterDataect = schemeMasterDataList.get(0);
				return schemeMasterDataect;
			} else {
				return null;
			}

		} catch (Exception e) {
			logger.error("in getschemeMasterDataBysRefID DAOIPL",e);
		}
		return null;
	}


	@Override
	public List<ESignedLog> getesignedLogByrefIdBysRefID(String refID) {
		List<ESignedLog> eSignedLogList = new ArrayList<ESignedLog>();

		logger.info("in getesignedLogByrefIdBysRefID daoIMPL");
		try {
			Query query1 = new Query();
			logger.info("refID:::"+refID);
			query1.addCriteria(Criteria.where("refId").is(refID));
			eSignedLogList = mongoTemplate.find(query1, ESignedLog.class, COLLECTION_ESignedLog);

		} catch (Exception e) {
			logger.error("Error occured in getesignedLogByrefIdBysRefID daoIMPL",e);
			logger.info("NOT MATCHED in Error occured in getesignedLogByrefIdBysRefID daoIMPL");
		}
		return eSignedLogList;
	}


	@Override
	public FileUploadRequest getfileUploadRequestAsPerCaseId(String gngRefId) {
		List<FileUploadRequest> fileUploadRequestList = new ArrayList<FileUploadRequest>();
		FileUploadRequest fileUploadRequest = new FileUploadRequest();
		logger.info("in getfileUploadRequestAsPerCaseId daoIMPL");
		try {
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("metadata.gonogoReferanceId").is(gngRefId));
			fileUploadRequestList = mongoTemplate.find(query1, FileUploadRequest.class, COLLECTION_FileUploadRequest);
			logger.info("Size in dAO " + fileUploadRequestList.size());

			if (fileUploadRequestList.size() > 0) {
				fileUploadRequest = fileUploadRequestList.get(0);
				logger.info("fileupload" + fileUploadRequest.getUploadFileDetails().getFileName());
				return fileUploadRequest;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getfileUploadRequestAsPerCaseId daoIMPL",e);
		}
		return fileUploadRequest;

	}

	@Override
	public DocumentListForGroupFive getUploadFileDetailsAsPerCaseId(String gngRefId) {
		DocumentListForGroupFive documentListForGroupFive = new DocumentListForGroupFive();
		List<DMSDocuments> dmsDocList = new ArrayList<DMSDocuments>();
		List<Pdfs> pdfsDocList = new ArrayList<Pdfs>();

		logger.info("in getUploadFileDetailsAsPerCaseId daoIMPL");
		try {

			Query query1 = new Query();
			query1.addCriteria(Criteria.where("metadata.gonogoReferanceId").is(gngRefId));
			//query1.with(new Sort(Sort.Direction.DESC, "uploadDate"));
			logger.info("query1" + query1);
			List<GridFSDBFile> docDetails = gridFsTemplte.find(query1);
			// GridFSDBFile docDetails = gridFsTemplate.findOne(query1);
			logger.info("docDetails" + docDetails);
			for (int i = 0; i < docDetails.size(); i++) {
				boolean flag = true;
				DMSDocuments dmsDoc = new DMSDocuments();
				Pdfs pdfs = new Pdfs();
				DBObject dbObject = docDetails.get(i).getMetaData();
				DBObject uploadFileDetailsDb = (DBObject) dbObject.get("uploadFileDetails");
				String Filename = (String) uploadFileDetailsDb.get("FileName");
				if(dmsDocList.size()!=0){
					for (int j=0;j<dmsDocList.size();j++)
					{
						logger.info("FileName:"+dmsDocList.get(j).getFilename()+ " "+Filename);
						if(StringUtils.equalsIgnoreCase(dmsDocList.get(j).getFilename(),Filename)){
							flag=false;
						}
					}
					if(flag){
						dmsDoc.setFilename(Filename);
						pdfs.setFilename(Filename);
						dmsDocList.add(dmsDoc);
						pdfsDocList.add(pdfs);
					}
				}else{
					dmsDoc.setFilename(Filename);
					pdfs.setFilename(Filename);
					dmsDocList.add(dmsDoc);
					pdfsDocList.add(pdfs);
				}
			}
			documentListForGroupFive.setDmsDocList(dmsDocList);
			documentListForGroupFive.setPdfsDocList(pdfsDocList);
			return documentListForGroupFive;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getUploadFileDetailsAsPerCaseId daoIMPL",e);
		}
		return null;
	}



	@Override
	public void saveTvsGroupServicStatus(TvsGroupServiceStatus tvsGroupServiceStatus) {

		Query query = new Query();
		String case_id = tvsGroupServiceStatus.getCaseId();
		query.addCriteria(Criteria.where("caseId").is(case_id));

		TvsGroupServiceStatus tvsGroupServiceStatusSave = mongoTemplate.findOne(query, TvsGroupServiceStatus.class);
		if(tvsGroupServiceStatusSave!=null){
			mongoTemplate.save(tvsGroupServiceStatus);
		}else{
			mongoTemplate.insert(tvsGroupServiceStatus);
		}
	}

	@Override
	public TvsGroupServiceStatus getTvsGroupServiceStatus(String gngRefI) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("caseId").is(gngRefI));
			return mongoTemplate.findOne(query, TvsGroupServiceStatus.class);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}


	@Override
	public void postAuditingInfo(AuditingInfo PostAuditingInfo) {

		try {

			mongoTemplate.insert(PostAuditingInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<AuditingInfo> getAuditLogById(String _id) {

		List<AuditingInfo> AuditingInfoList = new ArrayList<AuditingInfo>();
		Query query = new Query();

		query.addCriteria(Criteria.where("ProspectId").is(_id));

		AuditingInfoList = mongoTemplate.find(query, AuditingInfo.class);

		return AuditingInfoList;
	}

	@Override
	public List<AuditingInfo> getCompleteAuditLog() {

		return mongoTemplate.findAll(AuditingInfo.class);

	}
}