package com.softcell.dao.mongodb.repository;

import com.softcell.config.ComponentConfiguration;
import com.softcell.config.MultiBreComponentConfiguration;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.gonogo.exceptions.SystemException;

import java.util.List;

/**
 * @author vinodk
 *         <p/>
 *         Interface for getting reAppraisal app related details.
 */
public interface ComponentConfigurationRepository {

    /**
     * @param institutionId institution id for which the ReAppraisalConfiguration is needed.
     * @return ReAppraiseConfiguration for institution
     */
    ComponentConfiguration getByInstitutionIdAndType(String institutionId,
                                                     ComponentConfigurationType componentConfigType) throws SystemException;

    /**
     * @param reAppraisalConfiguration the app to be saved
     * @return true if saved successfully else false;
     */
    boolean create(ComponentConfiguration reAppraisalConfiguration) throws SystemException;

    /**
     * @param componentConfigurationType NORMAL : for loading default app.
     * @return all default app of Components;
     */
    List<ComponentConfiguration> getSystemComponentConfiguration(ComponentConfigurationType componentConfigurationType) throws SystemException;

    List<MultiBreComponentConfiguration> getMultiBreComponentConfiguration(ComponentConfigurationType componentConfigurationType) throws SystemException;


    /**
     *
     * @param componentConfigurationType
     * @param institutionId
     * @return
     */
    List<ComponentConfiguration> getSystemComponentConfiguration(ComponentConfigurationType componentConfigurationType, String institutionId) throws SystemException;


}
