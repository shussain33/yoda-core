package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;

/**
 * @author mahesh
 */
public interface ApplicationStageRepository {

    boolean updateApplicationStage(SerialSaleConfirmationRequest confirmationRequest, String stage) throws Exception;

    void updateApplicationStage(String refId, String stage) throws Exception;
}
