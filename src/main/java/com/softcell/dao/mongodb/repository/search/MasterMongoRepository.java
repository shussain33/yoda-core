package com.softcell.dao.mongodb.repository.search;

import com.mongodb.WriteResult;
import com.softcell.constants.Institute;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.ThirdPartyFieldValues;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author yogeshb
 */
@Repository
public class MasterMongoRepository implements MasterRepository {
    private Logger logger = LoggerFactory
            .getLogger(MasterMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public MasterMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Deprecated
    @Override
    public Set<String> getManufacturer(
            ApplicationMetadataRequest metadataRequest, String productAliasName) {
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(metadataRequest.getHeader().getInstitutionId())
                    .and("productFlag").is(productAliasName).and("active").is(true));

            query.fields().include("manufacturerDesc");

            List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(
                    query, AssetModelMaster.class);

            Set<String> categoriesSet = new HashSet<>();

           /* for (AssetModelMaster assetModelMaster : assetModelMasterList) {
                categoriesSet.add(assetModelMaster.getManufacturerDesc());
            }
*/
            if(CollectionUtils.isNotEmpty(assetModelMasterList)){

                categoriesSet = assetModelMasterList.stream()
                        .map(assetModelMaster -> {return assetModelMaster.getManufacturerDesc();})
                        .filter(StringUtils:: isNotBlank).distinct().collect(Collectors.toSet());
            }

            return categoriesSet;
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    /**
     * Added in TVS development. Need all details of manufacturer.
     * @param metadataRequest
     * @return
     */
    @Override
    public List<ManufacturerMaster> getManufacturerDetailsList(
            ApplicationMetadataRequest metadataRequest) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(metadataRequest.getHeader().getInstitutionId())
                    .and("active").is(true));
            return mongoTemplate.find(query, ManufacturerMaster.class);
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public Set<String> getCategories(ApplicationMetadataRequest metadataRequest, String productAliasName) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(metadataRequest.getHeader().getInstitutionId())
                    .and("manufacturerDesc").is(metadataRequest.getManufacturer()).and("productFlag").is(productAliasName)
                    .and("active").is(true));
            query.fields().include("catgDesc");
            List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(
                    query, AssetModelMaster.class);
            Set<String> categorySet = new HashSet<>();
            for (AssetModelMaster assetModelMaster : assetModelMasterList) {
                categorySet.add(assetModelMaster.getCatgDesc());
            }
            return categorySet;
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public Set<String> getMakes(ApplicationMetadataRequest metadataRequest, String productAliasName) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(metadataRequest.getHeader().getInstitutionId())
                    .and("productFlag").is(productAliasName)
                    .and("catgDesc").is(metadataRequest.getCategory())
                    .and("manufacturerDesc")
                    .is(metadataRequest.getManufacturer()).and("active").is(true));
            query.fields().include("make");
            List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(
                    query, AssetModelMaster.class);
            Set<String> makeSet = new HashSet<>();
            for (AssetModelMaster assetModelMaster : assetModelMasterList) {
                makeSet.add(assetModelMaster.getMake());
            }
            return makeSet;
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public Set<String> getModelNumbers(
            ApplicationMetadataRequest metadataRequest, String productAliasName) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(metadataRequest.getHeader().getInstitutionId())
                    .and("productFlag").is(productAliasName)
                    .and("catgDesc").is(metadataRequest.getCategory())
                    .and("manufacturerDesc")
                    .is(metadataRequest.getManufacturer()).and("makeModelFlag").is("MO")
                    .and("make").is(metadataRequest.getMake()).and("active").is(true));

            query.fields().include("modelNo");
            List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(
                    query, AssetModelMaster.class);
            Set<String> modelNoSet = new HashSet<>();
            for (AssetModelMaster assetModelMaster : assetModelMasterList) {
                modelNoSet.add(assetModelMaster.getModelNo());
            }
            return modelNoSet;
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public List<AssetModelMaster> getAssetModelMasterDetails(String modelNo, String institutionId, String productAliasName) {
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("modelNo").regex(GngUtils.sanitizeString(modelNo)).and("active").is(true)
                    .and("productFlag").is(productAliasName).and("institutionId").is(institutionId));
            return mongoTemplate.find(query, AssetModelMaster.class);

        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public List<AssetModelMaster> getAssetModelMasterByFullTextSearch(
            String query) {
        try {
            TextCriteria criteria = TextCriteria.forDefaultLanguage()
                    .matchingAny(query);
            Query mongoQuery = TextQuery.queryText(criteria);
            // .with(new PageRequest(0, 5)
            return mongoTemplate.find(mongoQuery, AssetModelMaster.class);
        } catch (Exception e) {
            logger.error("" + e);
            return null;
        }
    }

    @Override
    public Set<String> getAllUniqueCategories(
            ApplicationMetadataRequest metadataRequest, String productAliasName) throws Exception {

        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId")
                .is(metadataRequest.getHeader().getInstitutionId()).and("productFlag").is(productAliasName)
                .and("active")
                .is(true));


        query.fields().include("catgDesc");

        List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(query,
                AssetModelMaster.class);

        Set<String> assetCategories = new HashSet<String>();

        for (AssetModelMaster modelMaster : assetModelMasterList) {
            assetCategories.add(modelMaster.getCatgDesc());
        }

        return assetCategories;
    }

    @Override
    public DealerEmailMaster getDealerRanking(String dealerId,
                                              String institutionId) {

        logger.debug("getDealerRanking repo started ");

        Query query = new Query();
        query.addCriteria(Criteria.where("dealerID").is(dealerId)
                .and("institutionID").is(institutionId).and("active").is(true));

        return mongoTemplate.findOne(query, DealerEmailMaster.class);
    }

    @Override
    public List<AssetModelMaster> getModelsWithOtherDetails(ApplicationMetadataRequest metadataRequest, String productAliasName) {
        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId")
                .is(metadataRequest.getHeader().getInstitutionId())
                .and("manufacturerDesc").is(metadataRequest.getManufacturer())
                .and("modelNo").regex(GngUtils.sanitizeString(metadataRequest.getModelNo()), "i")
                .and("makeModelFlag").is("MO")
                .and("productFlag").is(productAliasName)
                .and("active").is(true))
                .fields().include("catgDesc").include("make").include("modelNo").include("weightUnloaded");
        query.limit(100);

        Supplier<Stream<AssetModelMaster>> streamSupplier = () -> StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, AssetModelMaster.class));

        return streamSupplier.get().collect(Collectors.toList());

    }

    @Override
    public StateMaster getState(String stateDesc, String institutionId){
        Query query = new Query();
        query.addCriteria(Criteria.where("stateDesc").is(stateDesc)
                .and("institutionId").is(institutionId).and("active").is(true));
        return mongoTemplate.findOne(query, StateMaster.class);
    }


    @Override
    public AccountsHeadMaster getAccountHeadMaster(String tranAccDesc, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("tranAccDesc").is(tranAccDesc)
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        AccountsHeadMaster accountsHeadMaster = mongoTemplate.findOne(query, AccountsHeadMaster.class);
        return accountsHeadMaster;

    }

   @Override
    public StateBranchMaster getStateBranchMaster(String branchCode, String institutionId) {
       Query query = new Query();
       query.addCriteria(Criteria.where("branchCode").is(branchCode)
               .and("institutionId").is(institutionId)
               .and("active").is(true));
       StateBranchMaster sapStateMaster = mongoTemplate.findOne(query, StateBranchMaster.class);
       return sapStateMaster;
    }


    @Override
    public TaxCodeMaster getTaxCodeMaster(String stateCode, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("stateCode").is(stateCode)
                .and("institutionId").is(institutionId)
                .and("active").is(true));
        TaxCodeMaster taxCodeMaster = mongoTemplate.findOne(query, TaxCodeMaster.class);
        return taxCodeMaster;
    }

    @Override
    public List<TCLosGngMappingMaster> getTCLosGngMappingMaster(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId));
        query.with(new Sort(Sort.Direction.ASC, "srNo"));
        List<TCLosGngMappingMaster> tcLosGngMappings = mongoTemplate.find(query, TCLosGngMappingMaster.class);
        return tcLosGngMappings;
    }

    @Override
    public ManufacturerMaster getManufacturer(String mfrCode, String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId).and("mfrCode").is(mfrCode));
        ManufacturerMaster manufacturerMasters = mongoTemplate.findOne(query, ManufacturerMaster.class);
        return manufacturerMasters;
    }

    @Override
    public boolean saveDropDownMaster(DropdownMaster dropdownMaster) {
        logger.debug("saveDropDownMaster repo started");

        try {

            /*if (!mongoTemplate.collectionExists(DropdownMaster.class)) {
                mongoTemplate.createCollection(DropdownMaster.class);
            }*/
            Query query = new Query();
            query.addCriteria(Criteria.where("header.institutionId").is(dropdownMaster.getHeader().getInstitutionId())
                    .and("dropDownType").is(dropdownMaster.getDropDownType()));
            Update update = new Update();
            update.set("productList", dropdownMaster.getProductList());
            update.set("dropDownValueDetailsList", dropdownMaster.getDropDownValueDetailsList());
            update.set("createdDate", dropdownMaster.getCreatedDate());
            update.set("createdBy", dropdownMaster.getCreatedBy());
            update.set("updatedDate", dropdownMaster.getUpdatedDate());
            update.set("updateBy", dropdownMaster.getUpdateBy());
            update.set("rolesList", dropdownMaster.getRolesList());
            update.set("version", dropdownMaster.getVersion());

            WriteResult writeResult = mongoTemplate.upsert(query, update, DropdownMaster.class);

            return writeResult.isUpdateOfExisting();

        } catch (Exception e) {
            logger.error("Exception caught while saving saveDropDownMaster response " + e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("Exception caught while saving saveDropDownMaster {%s}", e.getMessage()));
        }
    }


    @Override
    public List<DropdownMaster> fetchDropDownMaster(DropdownMasterRequest dropdownMasterRequest) {
        logger.debug("fetchDropDownMaster repo started");
        List<DropdownMaster> dropdownMasterList = new ArrayList<>();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("header.institutionId").is(dropdownMasterRequest.getHeader().getInstitutionId()));

            if(dropdownMasterRequest.getQueryType().equalsIgnoreCase(EndPointReferrer.ONE)) {
                query.addCriteria(Criteria.where("dropDownType").is(dropdownMasterRequest.getDropDownType()));
            } else if(dropdownMasterRequest.getQueryType().equalsIgnoreCase(EndPointReferrer.MULTIPLE)) {
                query.addCriteria(Criteria.where("dropDownValueDetailsList.Product").is(dropdownMasterRequest.getHeader().getProduct()));
                query.fields().include("dropDownValueDetailsList").include("productList").include("dropDownType");
            }

            dropdownMasterList = mongoTemplate.find(query, DropdownMaster.class);

        } catch (Exception e) {
            logger.error("Exception caught while fetchDropDownMaster response " + e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("Exception caught while fetchDropDownMaster {%s}", e.getMessage()));
        }
        return dropdownMasterList;

    }


    @Override
    public Map<String, Map<String, String>> getThirdPartyFieldValues(Institute institute, String apiName) {

        Map<String, Map<String, String>> fieldMap = null;
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("instituteName").is(institute.name()));
                    /*.and("thirdParyApi").is(apiName));*/
            query.fields().include("thirdParyApi."+ apiName);
            ThirdPartyFieldValues fieldValues = mongoTemplate.findOne(query, ThirdPartyFieldValues.class);
            if( fieldValues == null){
                logger.info("{} not configured for 3rd party values for {}", institute, apiName);
                return null;
            }
            return fieldValues.getThirdParyApi().get(apiName);
        }catch (Exception ex){
            ex.printStackTrace();
            logger.debug(ex.getMessage());
        }
        return fieldMap;
    }

    public Map<String, Map<String, Map<String, String>>> getThirdPartyAPI(Institute institute, String apiName) {

        Map<String, Map<String, Map<String, String>>> fieldMap = null;
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("instituteName").is(institute.name()));
                    /*.and("thirdParyApi").is(apiName));*/
            ThirdPartyFieldValues fieldValues = mongoTemplate.findOne(query, ThirdPartyFieldValues.class);
            if( fieldValues == null){
                logger.info("{} not configured for 3rd party values for {}", institute, apiName);
                return null;
            }
            return fieldValues.getThirdParyApi();
        }catch (Exception ex){
            ex.printStackTrace();
            logger.debug(ex.getMessage());
        }
        return fieldMap;
    }

    @Override
    public List<DropdownMaster> fetchDropDownMaster(String instId, String dropDownType, String queryType, String product) {
        logger.debug("fetchDropDownMaster repo started");
        List<DropdownMaster> dropdownMasterList = new ArrayList<>();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("header.institutionId").is(instId));
            if(queryType.equalsIgnoreCase(EndPointReferrer.ONE)) {
                query.addCriteria(Criteria.where("dropDownType").is(dropDownType));
            } else if(queryType.equalsIgnoreCase(EndPointReferrer.MULTIPLE)) {
                query.addCriteria(Criteria.where("productList").in(product));
                query.fields().include("dropDownType").include("productList").include("createdDate").include("createdBy")
                        .include("updatedDate")
                        .include("updateBy")
                        .include("version");
            }

            dropdownMasterList = mongoTemplate.find(query, DropdownMaster.class);

        } catch (Exception e) {
            logger.error("Exception caught while fetchDropDownMaster response " + e.getMessage());
            throw new SystemException(String.format("Exception caught while fetchDropDownMaster {%s}", e.getMessage()));
        }
        return dropdownMasterList;

    }

    @Override
    public Map<String, Map<String, String>> fetchLogsMaster(String institutionId, String type, String masterValue) {
        Map<String, Map<String, String>> fieldMap = null;
        try{
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("type").is(type));
            query.fields().include("masterValues."+ masterValue);
            LogsMaster fieldValues = mongoTemplate.findOne(query, LogsMaster.class);
            if( fieldValues == null){
                logger.info("{} not configured for type {} and masterValue. {}", institutionId, type, masterValue);
                return null;
            }
            return fieldValues.getMasterValues().get(masterValue);
        }catch (Exception ex){
            ex.printStackTrace();
            logger.debug(ex.getMessage());
        }
        return fieldMap;
    }

}
