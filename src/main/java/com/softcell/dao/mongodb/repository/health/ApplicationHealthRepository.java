package com.softcell.dao.mongodb.repository.health;

public interface ApplicationHealthRepository {

    Object getStats() throws Exception;

    Object getCollectionNames() throws Exception;

    Object getServerStatusInfo() throws Exception;

    Object getHostInfo() throws Exception;

}
