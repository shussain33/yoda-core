package com.softcell.dao.mongodb.repository.metadata;

import com.softcell.config.sms.GngSmsServiceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class SmsConfigMongoRepository implements SmsConfigRepository {

    private static final Logger logger = LoggerFactory.getLogger(SmsConfigMongoRepository.class);

    @Autowired
    MongoTemplate mongoTemplate;

    private Query query;
    private Class collection = GngSmsServiceConfiguration.class;


    @Override
    public Boolean insertSmsConfiguration(
            GngSmsServiceConfiguration smsConfiguration) {
        try {
            /*if (!mongoTemplate.collectionExists(collection)) {
                mongoTemplate.createCollection(collection);
            }*/
            mongoTemplate.insert(smsConfiguration);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean updateSmsConfiguration(
            GngSmsServiceConfiguration smsConfigurations) {
        try {
            query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(smsConfigurations.getInstitutionId()).and("productId")
                    .is(smsConfigurations.getProductId()));
            Update update = new Update();
            update.set("enable", smsConfigurations.isEnable());
            update.set("content", smsConfigurations.getContent());
            update.set("lastUpdateDate", new Date());
            mongoTemplate.updateFirst(query, update, collection);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public Boolean deleteSmsConfiguration(
            GngSmsServiceConfiguration smsConfigurations) {
        try {
            query = new Query();
            query.addCriteria(Criteria.where("institutionId")
                    .is(smsConfigurations.getInstitutionId()).and("productId")
                    .is(smsConfigurations.getProductId()).and("vendorName")
                    .is(smsConfigurations.getVendorName()));
            mongoTemplate.remove(query, collection);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public List<GngSmsServiceConfiguration> readSmsConfigByInstitution(
            String institutionId) {
        query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return mongoTemplate.find(query, collection);
    }

    @Override
    public List<GngSmsServiceConfiguration> readActiveSmsConfigByInstitution(
            String institutionId) {
        query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId));
        return mongoTemplate.find(query, collection);
    }

    @Override
    public boolean isSmsConfigExist(GngSmsServiceConfiguration smsConfigurations) {
        query = new Query();
        query.addCriteria(Criteria.where("institutionId")
                .is(smsConfigurations.getInstitutionId()).and("productId")
                .is(smsConfigurations.getProductId()).and("vendorName")
                .is(smsConfigurations.getVendorName()));
        return mongoTemplate.exists(query, collection);
    }

}
