package com.softcell.dao.mongodb.repository;

import com.itextpdf.text.Image;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.GetFileRequest;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author prateek
 */
public interface UploadFileRepository {

    /**
     * @param inputStream
     * @param fileName
     * @param contentType
     * @param metaData
     * @return
     */
    String store(InputStream inputStream, String fileName,
                 String contentType, Object metaData);

    /**
     * @param imageID
     * @param refID
     * @param status
     * @param reason
     * @return
     */
    boolean updateImageStatus(String imageID, String refID,
                              String status, String reason);

        /**
     * @param fileName
     * @return
     */
    GridFSDBFile retrive(String fileName);

    /**
     * @param id
     * @param institutionID
     * @return
     */
    GridFSDBFile getById(String id, String institutionID);

    /**
     * @param filename
     * @return
     */
    GridFSDBFile getByFilename(String filename);

    GridFSDBFile getByFileId(String fileId, String institutionID);

    /**
     * @param refId
     * @param institutionId
     * @param deliveryOrderName
     * @return
     */
    GridFSDBFile getPDFByFilenameNRefID(String refId, String institutionId, String deliveryOrderName);

    /**
     * @param fileName
     * @param refID
     * @param applicantID
     * @return
     */
    GridFSDBFile getImage(String fileName, String refID,
                          String applicantID);

    /**
     * @param refID
     * @param applicantID
     * @return
     */
    List<GridFSDBFile> getImages(String refID, String applicantID);

    /**
     * @param refID
     * @param institutionId
     * @return
     */
    List<KycImageDetails> getKycImageDetails(Set<String> refID,
                                             String institutionId, String typeOfDocument);

    /**
     * @param refID
     * @return
     */
    @Deprecated
    List<KycImageDetails> getAdditionalImageDetails(String refID,
                                                    String institutionId);

    /**
     * @param fileUploadRequest
     * @return
     */
    boolean updateStatus(FileUploadRequest fileUploadRequest);

    /**
     * @param refID
     * @param institutionID
     * @return
     */
    byte[] getCibilReport(String refID, String institutionID);

    /**
     * This method will fetch cibil report from CoApplicantMbResponse document based
     * on co applicant id
     *
     * @param coApplicantId
     * @return
     */
    byte[] getCibilReportForCoApplicant(String coApplicantId);

    /**
     * @param imageId
     * @param amazonFileUrl
     * @return
     */
    boolean updateFileAmazonUrl(String imageId, String amazonFileUrl);

    /**
     * @param imageId
     * @return
     */
    boolean isUploadedToAmazonS3(String imageId);

    /**
     * This method return the byte array against the reference Id
     *
     * @param fileUploadRequest
     * @return
     */
    Image getHdbfsImage(FileUploadRequest fileUploadRequest);

    /**
     * This method is to update application image linkage set
     *
     * @param fileUploadRequest
     * @return
     */
    boolean updateAppImageLinkage(FileUploadRequest fileUploadRequest);

    /**
     * This method is use to update application image linkage set
     *
     * @param refId
     * @param appImageLinkage
     * @return
     */
    boolean updateAppImageLinkage(String refId, Set<String> appImageLinkage);

    /**
     * To get application image linkage set
     *
     * @param refID
     * @return
     */
    Set<String> getAppImageLinkageArray(String refID);

    /**
     * @param refId
     * @param applicantID
     * @param institutionId
     * @return
     */
    List<KycImageDetails> getKycImageDetailsByApplicant(String refId,
                                                        String applicantID, String institutionId);

    /**
     * @param refID
     * @param institutionId
     * @param bureau
     * @return
     */
    byte[] getBureauReport(String refID, String institutionId, String bureau);

    /**
     * @param refId
     * @param fileName
     * @return
     */
    String getImageIdByRefIdAndFileName(String refId, String fileName);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    List<ImagesDetails> getDocumentsForDmsPush(String refId, String institutionId);

    /**
     * Get Document Request
     *
     * @param getFileRequest
     * @return
     */
    GridFSDBFile getDocument(GetFileRequest getFileRequest);

    /**
     *
     * @param docId
     * @param institutionId
     * @param refId
     * @return
     */
    GridFSDBFile getDocument(String docId, String institutionId, String refId);

    /* @param refID
     * @param deliveryOrderName
     * @return
     */
    Date getDeliveryOrderCreateDate(String refID, String deliveryOrderName);

    /**
     *
     * @param getFileRequest
     * @return
     */
    GridFSDBFile getApplicationDocument(GetFileRequest getFileRequest);

    /**
     *
     * @param fileUploadRequest
     * @return
     */
    GridFSDBFile getLoanAppDocument(FileUploadRequest fileUploadRequest);

    /**
     *
     * @param checkApplicationStatus
     * @return
     */
    List<ImagesDetails> getSignedFileDetails(CheckApplicationStatus checkApplicationStatus);

    boolean deleteDocument(FileUploadRequest fileUploadRequest);

    List<KycImageDetails> getDocumentDetailsByFileName(String refID, String institutionId, String fileName);

    public boolean deletedocument(String imageId);

    String softDocument(FileUploadRequest fileUploadRequest);

    List<KycImageDetails> getKycImageDetails(Set<String> refID,
                                             String institutionId, String typeOfDocument, String var);
}
