package com.softcell.dao.mongodb.repository.master;

import com.mongodb.WriteResult;
import com.softcell.constants.Product;
import com.softcell.constants.SourcingEnum;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.SourcingDetailsRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.UpdateCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.GetSchemeDateMappingDetailsRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.SchemeApproveDeclineRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.UpdateSchemeDateMappingDetailsRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.gonogo.model.response.Table;
import com.softcell.gonogo.model.response.master.commongeneralmaster.SourcingDetails;
import com.softcell.gonogo.model.response.master.schemedatemapping.AssetCostValidationResponse;
import com.softcell.gonogo.model.response.master.schemedatemapping.ManufacturerDescWithCcid;
import com.softcell.gonogo.model.response.master.schemedatemapping.SchemeInformationResponse;
import com.softcell.gonogo.service.factory.MasterDataViewBuilder;
import com.softcell.gonogo.service.factory.impl.MasterDataViewBuilderImpl;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mahesh
 */
@Repository
public class MasterDataViewMongoRepository implements MasterDataViewRepository {

    private static final Logger logger = LoggerFactory.getLogger(MasterDataViewMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MasterDataViewBuilder masterDataViewBuilder;

    @Autowired
    private LookupService lookupService;

    public MasterDataViewMongoRepository(MongoTemplate mongoTemplate) {
        if (null == this.mongoTemplate) {
            this.mongoTemplate = mongoTemplate;
        }
        if (null == lookupService) {
            lookupService = new LookupServiceHandler();
        }
        if (null == masterDataViewBuilder) {
            masterDataViewBuilder = new MasterDataViewBuilderImpl();
        }
    }

    @Override
    public Table getAssetModelDetails(String institutionId, int limit, int skip, String productAliasName) {

        logger.debug("Master Data View repo invoked to fetch AssetModel details for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));
        dataQuery.limit(limit);
        dataQuery.skip(skip);
        dataQuery.with(new Sort(Sort.Direction.ASC, "modelId"));

        logger.debug("Master Data View repo query formed for fetching AssetModel [{}]", dataQuery);

        Query countQuery = new Query();
        countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, AssetModelMaster.class));

        masterTable.setData(mongoTemplate.find(dataQuery, AssetModelMaster.class));

        logger.debug("Master Data View repo fetching AssetModel data completed");

        return masterTable;
    }

    @Override
    public List<SchemeInformationResponse> viewAllSchemeIds(String institutionId, String productAliasName) {

        logger.debug("Master Data View repo invoked to fetch all scheme ids");

        Query query = new Query();

        query.with(new Sort(Sort.Direction.ASC, "schemeDesc"));
        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId).and("productFlag").is(productAliasName));

        List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query, SchemeMasterData.class);

        List<SchemeInformationResponse> schemeInformationResponsesList = new ArrayList<>();

        if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

            for (SchemeMasterData schemeMasterData : schemeMasterDataList) {

                SchemeInformationResponse schemeInformationResponse = new SchemeInformationResponse();

                schemeInformationResponse.setSchemeDesc(schemeMasterData.getSchemeDesc());
                schemeInformationResponse.setSchemeID(schemeMasterData.getSchemeID());
                schemeInformationResponse.setCcid(schemeMasterData.getCcid());
                schemeInformationResponsesList.add(schemeInformationResponse);
            }

        }

        logger.debug("Master Data View repo all Scheme ids count [{}]", schemeInformationResponsesList.size());

        return schemeInformationResponsesList;
    }

    @Override
    public Table getPinCodeMasterDetails(String institutionId, int limit, int skip) {

        logger.debug("Master Data View repo invoked to fetch pincode master for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        dataQuery.limit(limit);

        dataQuery.skip(skip);

        dataQuery.with(new Sort(Sort.Direction.ASC, "zipCode"));

        logger.debug("Master Data View repo formed query {} to fetch pincode master ", dataQuery);

        Query countQuery = new Query();

        countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, PinCodeMaster.class));

        masterTable.setData(mongoTemplate.find(dataQuery, PinCodeMaster.class));

        logger.debug("Master Data View repo fetching pincode master data completed");

        return masterTable;
    }

    @Override
    public Table getDealerBranchMasterDetails(String institutionId, String productAliasName, int limit, int skip) {

        logger.debug("Master Data View repo invoked to fetch dealer branch master for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));

        dataQuery.limit(limit);

        dataQuery.skip(skip);

        dataQuery.with(new Sort(Sort.Direction.ASC, "dealerId"));

        logger.debug("Master Data View repo formed query {} to fetch dealer branch master ", dataQuery);

        Query countQuery = new Query();

        countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, DealerBranchMaster.class));

        masterTable.setData(mongoTemplate.find(dataQuery, DealerBranchMaster.class));

        logger.debug("Master Data View repo fetching dealer branch master data completed");

        return masterTable;
    }

    @Override
    public Table getDealerEmailMasterDetails(String institutionId, int limit, int skip) {

        logger.debug("Master Data View repo invoked to fetch dealer email master for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionID").is(institutionId).and("active").is(true));

        dataQuery.limit(limit);

        dataQuery.skip(skip);

        dataQuery.with(new Sort(Sort.Direction.ASC, "dealerID"));

        logger.debug("Master Data View repo formed query {} to fetch dealer email master ", dataQuery);

        Query countQuery = new Query();

        countQuery.addCriteria(Criteria.where("institutionID").is(institutionId).and("active").is(true));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, DealerEmailMaster.class));

        masterTable.setData(mongoTemplate.find(dataQuery, DealerEmailMaster.class));

        logger.debug("Master Data View repo fetching dealer email master data completed");

        return masterTable;
    }

    @Override
    public Table getEmployerMasterDetails(String institutionId, int limit, int skip) {

        logger.debug("Master Data View repo invoked to fetch employer master for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        dataQuery.limit(limit);

        dataQuery.skip(skip);

        dataQuery.with(new Sort(Sort.Direction.ASC, "employerId"));

        logger.debug("Master Data View repo formed query {} to fetch employer master ", dataQuery);

        Query countQuery = new Query();

        countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, EmployerMaster.class));

        masterTable.setData(mongoTemplate.find(dataQuery, EmployerMaster.class));

        logger.debug("Master Data View repo fetching employer master data completed");

        return masterTable;
    }

    @Override
    public Table getSchemeModelDealerCityMappingDetails(String institutionId, int limit, int skip) {

        logger.debug("Master Data View repo invoked to fetch scheme dealer city master for institution {}", institutionId);

        Table masterTable = new Table();

        Query dataQuery = new Query();

        dataQuery.addCriteria(Criteria.where("institutionID").is(institutionId).and("active").is(true));

        dataQuery.limit(limit);

        dataQuery.skip(skip);

        dataQuery.with(new Sort(Sort.Direction.ASC, "schemeID"));

        logger.debug("Master Data View repo formed query {} to fetch scheme dealer city master ", dataQuery);

        Query countQuery = new Query();
        countQuery.addCriteria(Criteria.where("institutionID").is(institutionId).and("active").is(true).and("productFlag"));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, SchemeModelDealerCityMapping.class));
        masterTable.setData(mongoTemplate.find(dataQuery, SchemeModelDealerCityMapping.class));

        logger.debug("Master Data View repo fetching scheme dealer city master data completed");

        return masterTable;
    }


    @Override
    public AssetCostValidationResponse validateAssetCost(
            AssetCostValidationRequest assetCostValidationRequest, String productAliasName) {

        logger.debug("Master Data View repo invoked to validate asset cost");

        AssetCostValidationResponse assetCostValidationResponse = new AssetCostValidationResponse();

        String modelNo = assetCostValidationRequest.getAssetCostDetails().getModelNo();

        String assetMnfctr = assetCostValidationRequest.getAssetCostDetails().getAssetManufacture();

        double assetPrice = assetCostValidationRequest.getAssetCostDetails().getPrice();

        String assetCtg = assetCostValidationRequest.getAssetCostDetails().getAssetCtg();

        String instId = assetCostValidationRequest.getHeader().getInstitutionId();

        String assetModelMake = assetCostValidationRequest.getAssetCostDetails().getAssetModelMake();


        Query query = new Query();

        query.addCriteria(Criteria.where("institutionId").is(instId)
                .and("modelNo").is(modelNo).and("manufacturerDesc")
                .is(assetMnfctr).and("catgDesc").is(assetCtg).and("make")
                .is(assetModelMake).and("active").is(true).and("productFlag").is(productAliasName));

        AssetModelMaster assetModelMaster = mongoTemplate.findOne(query, AssetModelMaster.class);

        if (null != assetModelMaster) {

            if (StringUtils.equals(Status.MO.name(), assetModelMaster.getMakeModelFlag())) {

                if (assetPrice <= assetModelMaster.getWeightUnloaded()) {

                    assetCostValidationResponse.setStatus(Status.SUCCESS.name());
                    assetCostValidationResponse.setResMsg("Entered Price Is Valid");

                    logger.debug("MO MakeModelFlag(Entered Price Valid)");

                    return assetCostValidationResponse;

                } else {

                    assetCostValidationResponse.setStatus(Status.FAILED.name());
                    assetCostValidationResponse.setResMsg("Entered Price Should Not Greater Than Asset Price");

                    logger.debug("MO MakeModelFlag(Entered Price Not Valid)");

                    return assetCostValidationResponse;
                }

            } else {

                assetCostValidationResponse.setStatus(Status.SUCCESS.name());

                logger.debug("Inside Asset Cost Validation (MA-MakeModelFlag)");

                assetCostValidationResponse.setResMsg("No Need To Check Price For MA MakeModelFlag");

                logger.debug("MA MakeModelFlag(Asset Cost Validation Skip.)");

                return assetCostValidationResponse;

            }
        } else {

            assetCostValidationResponse.setStatus(Status.FAILED.name());

            assetCostValidationResponse.setResMsg("Asset Record Not Found.Please Provide Correct Data");

            return assetCostValidationResponse;

        }


    }

    @Override
    public Set<String> getManufacturerAgainstCcid(GetSchemeInformationRequest getSchemeInformationRequest, String productAliasName) throws Exception {

        logger.debug("Master Data View repo invoked to fetch manufacture against ccid ");

        Set<String> manufDescSet = new TreeSet<>();
        String institutionId = getSchemeInformationRequest.getHeader().getInstitutionId();

        try {

            Query query = new Query();

            if (StringUtils.isNotBlank(getSchemeInformationRequest.getCcid())) {

                query.addCriteria(Criteria.where("manufacturerId")
                        .is(getSchemeInformationRequest.getCcid()).and("institutionId")
                        .is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));

                AssetModelMaster assetModelMaster = mongoTemplate.findOne(query, AssetModelMaster.class);
                /**
                 * if assetmaster found against ccid ,then check equality otherwise return all manufacturer
                 */
                if (null != assetModelMaster &&
                        StringUtils.equals(getSchemeInformationRequest.getCcid(), assetModelMaster.getManufacturerId())) {

                    manufDescSet.add(assetModelMaster.getManufacturerDesc());
                    return manufDescSet;

                }

            }

            return getAllManfacturer(institutionId, productAliasName);

        } catch (DataAccessException e) {

            logger.error("Error while fetching manufacture against ccid {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error while fetching manufacture against ccid {%s}", e.getMessage()));
        }

    }

    @Override
    public Table getAllElecServProviders(String institutionId) {
        logger.debug("Master Data View repo invoked to fetch Electricity Service Provider master for institution {}",
                institutionId);

        Table masterTable = new Table();
        Query dataQuery = new Query();
        dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));
        dataQuery.with(new Sort(Sort.Direction.ASC, "code"));

        logger.debug("Query to fetch Electricity Service Provider Master : {}", dataQuery);

        Query countQuery = new Query();
        countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

        masterTable.setTotalCount(mongoTemplate.count(countQuery, ElecServProvMaster.class));
        masterTable.setData(mongoTemplate.find(dataQuery, ElecServProvMaster.class));

        logger.debug("Master Data View repo fetching Electricity Service Provider master data completed");

        return masterTable;
    }


    private Set<String> getAllManfacturer(String institutionId, String productAliasName) {

        logger.debug("Master Data View repo invoked to fetch distinct manufacturer for institution {} and product {}", institutionId, productAliasName);

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true).and("productFlag").is(productAliasName));

            List<String> distinctManufacturerDesc = mongoTemplate.getDb()
                    .getCollection("assetModelMaster")
                    .distinct("manufacturerDesc", query.getQueryObject());

            logger.debug("Master Data View repo successfully fetched distinct manufacturer ");

            return new TreeSet<>(distinctManufacturerDesc);
        } catch (DataAccessException e) {
            logger.error("Error while fetching all manufacture  with probable cause ", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error while fetching manufacture with probable cause {%s}", e.getMessage()));
        }

    }

    @Override
    public Set<String> getAllState(String institutionId) throws Exception {

        logger.debug("Master Data View repo invoked to fetch distinct states for institution {}", institutionId);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionID").is(institutionId)
                    .and("dealerID").ne("").and("active").is(true));

            List<String> allStateIdWithDealer = mongoTemplate.getDb()
                    .getCollection("DealerEmailMaster")
                    .distinct("stateId", query.getQueryObject());

            return new TreeSet<>(getStateNameUsingStateId(allStateIdWithDealer ,institutionId));

        } catch (DataAccessException e) {

            logger.error("Error while fetching distinct states with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error while fetching  distinct states with probable cause [%s]", e.getMessage()));

        }

    }

    private List<String> getStateNameUsingStateId(List<String> allStateIdWithDealer ,String institutionId) throws Exception {

        logger.debug("Master Data View repo invoked to fetch state names against state ids ");

        try {
            Query query = new Query();

            query.addCriteria(Criteria
                    .where("stateId").in(allStateIdWithDealer)
                    .and("active").is(true)
                    .and("institutionId").is(institutionId));

            List<String> cityNameList = mongoTemplate.getDb()
                    .getCollection("cityStateMappingMaster")
                    .distinct("stateDesc", query.getQueryObject());

            logger.debug("Master Data View repo successfully fetched state names against state ids ");

            return cityNameList;

        } catch (DataAccessException e) {

            logger.error("Error while fetching state names against state ids with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error while fetching state names against state ids with probable cause {%}", e.getMessage()));
        }
    }

    @Override
    public  Set<InclCityWithStateName> getAllCityAgainstState(GetAllCityAgainstStateRequest getAllCityAgainstStateRequest) {

        logger.debug("Master Data View repo invoked to fetch city set against state");

        Set<InclCityWithStateName>  inclCityWithStateNames =new HashSet<>();

        String institutionId = getAllCityAgainstStateRequest.getHeader().getInstitutionId();

        List<String> stateDescList = getAllCityAgainstStateRequest.getStateDesc();

        stateDescList.stream()
                    .filter(stateName-> StringUtils.isNotBlank(stateName))
                    .forEach( stateName -> {

                        Query query = new Query();
                        query.addCriteria(Criteria.where("stateDesc").is(stateName).and("active").is(true)
                                .and("institutionId").is(institutionId));

                        CityStateMappingMaster cityStateMappingMaster = mongoTemplate.findOne(
                                query, CityStateMappingMaster.class);

                        if (cityStateMappingMaster != null && StringUtils.isNotBlank(cityStateMappingMaster.getStateId())) {

                            logger.debug("Master Data View repo found cityMaster against state");

                            String stateId = cityStateMappingMaster.getStateId();

                            getCityIdHavingDealer(stateId, institutionId, inclCityWithStateNames);
                        }else {
                            logger.error("State id not found against the state  {} in cityStateMapping master", stateName);
                        }

                    });

        return  inclCityWithStateNames;

    }

    private  void getCityIdHavingDealer(String stateId ,String institutionId ,Set<InclCityWithStateName> inclCityWithStateNames) {

        logger.debug("Master Data View repo invoked to fetch city id against state id {}", stateId);

        Query query = new Query();

        query.addCriteria(Criteria.where("stateId").is(stateId)
                .and("dealerID")
                .ne("").and("active").is(true)
                .and("institutionID").is(institutionId));

        List<String> cityIdHavingDealer = mongoTemplate.getDb()
                .getCollection("DealerEmailMaster")
                .distinct("cityId", query.getQueryObject());

        if (CollectionUtils.isEmpty(cityIdHavingDealer)) {

            logger.debug("Master Data View repo unable to fetch city id against state id {}",stateId);

        }else{
             getCityNameUsingCityId(cityIdHavingDealer, stateId, institutionId, inclCityWithStateNames);
        }

    }

    private void getCityNameUsingCityId(List<String> cityIdHavingDealer, String stateId, String institutionId ,Set<InclCityWithStateName> inclCityWithStateNames) {

        logger.debug("Master Data View repo invoked to fetch cityName list against cityId list and stateId");

        Query query = new Query();

        query.addCriteria(Criteria.where("cityId").in(cityIdHavingDealer)
                .and("stateId").is(stateId).and("active").is(true)
                .and("institutionId").is(institutionId));

        List<CityStateMappingMaster> cityStateMappingMasters = mongoTemplate.find(query, CityStateMappingMaster.class);

        Set<InclCityWithStateName> cityNameSet = cityStateMappingMasters.stream().filter(Objects::nonNull)
                .filter(cityStateMappingMaster -> StringUtils.isNotBlank(cityStateMappingMaster.getCityName()) && StringUtils.isNotBlank(cityStateMappingMaster.getStateDesc()))
                .map(cityStateMappingMaster -> {
                    InclCityWithStateName inclCityWithStateName = new InclCityWithStateName();
                    inclCityWithStateName.setCityName(cityStateMappingMaster.getCityName());
                    inclCityWithStateName.setStateName(cityStateMappingMaster.getStateDesc());

                    return inclCityWithStateName;
                }).collect(Collectors.toSet());

        if (!CollectionUtils.isEmpty(cityNameSet)) {
            inclCityWithStateNames.addAll(cityNameSet);
        } else {
            logger.debug("unable to find city name against  state id {}", stateId);
        }

    }

    @Override
    public boolean updateSchemeDealerMappingDetails(SchemeDealerMappingDetails schemeDealerMappingDetails, String productAliasName) throws Exception {

        logger.debug("Master Data View repo invoked to update scheme dealer master");
        String institutionId = schemeDealerMappingDetails.getHeader().getInstitutionId();

        try {

            Query queryForDealerMapping = QueryBuilder
                    .buildQueryForSchemeMapping(schemeDealerMappingDetails.getSchemeID(), institutionId, productAliasName, Status.DEALER_MAPPING.name());

            SchemeDealerMappingDetails schemeDealerMapping = mongoTemplate
                    .findOne(queryForDealerMapping, SchemeDealerMappingDetails.class);

            if (schemeDealerMapping != null) {

                logger.debug("Master Data View repo schemeId {} exist", schemeDealerMappingDetails.getSchemeID());

                Query queryForDateMapping = QueryBuilder
                        .buildQueryForSchemeMapping(schemeDealerMappingDetails.getSchemeID(), institutionId, productAliasName, Status.DATE_MAPPING.name());

                /**
                 * here user trying to update previouly approved scheme.i.e schemeStatus:Approved and isapproveschemesatus :false
                 */
                if (schemeDealerMapping.getSchemeStatus().equals(Status.APPROVED.name())) {

					/*
                     * then create new document with scheme status pending and
					 * after approve this pending scheme merge two collection
					 * i.e. approve scheme and pending scheme
					 */
                    setApproveFlagForSchemeDateMapping(queryForDateMapping);
                    setApproveFlagForSchemeDealerMapping(queryForDealerMapping);

                    makeReplicaOfSchemeDealerMapping(schemeDealerMappingDetails, productAliasName);

                    updatePreviousSchemeDealerMapping(schemeDealerMapping.getSchemeID(), institutionId, productAliasName);

                    logger.debug("Master Data View repo inserted replica of existing scheme with approved status pending");

                    return true;
                    /**
                     * if user modify the pending scheme then no problem.
                     */
                } else {
                    // change status to pending
                    takeSchemeDealerMappingBackupBySoftDelete(schemeDealerMappingDetails, queryForDealerMapping);

                    // change status to pending
                    applyChangesInSchemeDateMapping(schemeDealerMappingDetails, queryForDateMapping);

                }

            }
            schemeDealerMappingDetails.setProductFlag(productAliasName);
            mongoTemplate.insert(schemeDealerMappingDetails);

            logger.debug("Master Data View repo inserted new scheme ");

            return true;

        } catch (DataAccessException e) {

            logger.error("Error in MDVR while performing scheme operations {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR while performing scheme operations {%s}", e.getMessage()));
        }

    }

    private void makeReplicaOfSchemeDealerMapping(SchemeDealerMappingDetails schemeDealerMappingDetails, String productAliasName) {

        logger.debug("MDVR invoked to save modification in schemeDealer master");

        try {
            schemeDealerMappingDetails.setSchemeStatus(Status.PENDING.name());
            schemeDealerMappingDetails.setApproveSchemeUpdateStatus(false);
            schemeDealerMappingDetails.setUpdateStatus(false);
            schemeDealerMappingDetails.setProductFlag(productAliasName);

            mongoTemplate.insert(schemeDealerMappingDetails);

            logger.debug("MDVR saved modification in schemeDealer master successfully");
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occure during save modification of approve scheme dealer mapping data with probable cause ", e.getMessage());
        }
    }

    private void updatePreviousSchemeDateMapping(String schemeId, String institutionId, String productAliasName) {

        logger.debug("MDVR invoked to save modification in schemeDate master");

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").is(schemeId)
                    .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(true).and("institutionID").is(institutionId)
                    .and("productFlag").is(productAliasName));

            SchemeDetailsDateMapping schemeDetailsDateMapping = mongoTemplate
                    .findOne(query, SchemeDetailsDateMapping.class);

            if (null != schemeDetailsDateMapping) {

                schemeDetailsDateMapping.setSchemeStatus(Status.PENDING.name());
                schemeDetailsDateMapping.setApproveSchemeUpdateStatus(false);
                schemeDetailsDateMapping.setProductFlag(productAliasName);

                mongoTemplate.insert(schemeDetailsDateMapping);

                logger.debug("MDVR inserted new pending scheme in schemeDate master successfully");

            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occure during save modification of approve scheme date mapping data with probable cause [{}] ", e.getMessage());
        }


    }

    private void applyChangesInSchemeDateMapping(SchemeDealerMappingDetails schemeDealerMappingDetails, Query query) {

        logger.debug("MDVR invoked to update scheme status in schemeDate master");

        Update update = new Update();
        update.set("schemeStatus", schemeDealerMappingDetails.getSchemeStatus());

        mongoTemplate.updateFirst(query, update, SchemeDetailsDateMapping.class);

        logger.debug("MDVR successfully updated scheme status in schemeDate master");

    }

    private void takeSchemeDealerMappingBackupBySoftDelete(SchemeDealerMappingDetails schemeDealerMappingDetails, Query query) {

        logger.debug("MDVR invoked to modify scheme makeId {} and makeDate {} in schemeDealer master",
                schemeDealerMappingDetails.getMakerID(), schemeDealerMappingDetails.getMakeDate());

        Update update = new Update();

        update.set("updateStatus", true);
        update.set("makerID", schemeDealerMappingDetails.getMakerID());
        update.set("makeDate", schemeDealerMappingDetails.getMakeDate());

        mongoTemplate.updateFirst(query, update, SchemeDealerMappingDetails.class);

        logger.debug("MDVR modified scheme makeId , makeDate status in schemeDealer master");

    }

    @Override
    public SchemeDealerMappingDetails getSchemeDealerMappingDataAgainstSchemeId(
            GetSchemeDealerMappingRequest schemeDealerMappingRequest, String productAliasName) throws Exception {

        logger.debug("MDVR invoked to fetch  scheme from schemeDealer Master for scheme id {}", schemeDealerMappingRequest.getSchemeID());

        SchemeDealerMappingDetails schemeDealerMappingDetailsResponse = new SchemeDealerMappingDetails();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").is(schemeDealerMappingRequest.getSchemeID())
                    .and("institutionId").is(schemeDealerMappingRequest.getHeader().getInstitutionId())
                    .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(false).and("productFlag").is(productAliasName));

            SchemeDealerMappingDetails schemeDealerMappingDetails = mongoTemplate.findOne(query, SchemeDealerMappingDetails.class);

            if (null != schemeDealerMappingDetails) {

                logger.debug("MDVR fetched scheme from schemeDealer Master successfully");

                getExcludedManufacturerData(schemeDealerMappingRequest, schemeDealerMappingDetails);

                getExcludedStateData(schemeDealerMappingRequest, schemeDealerMappingDetails);

                schemeDealerMappingDetails.setStatus(Status.SUCCESS.name());

                return schemeDealerMappingDetails;

            }

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching scheme from schemeDealer with cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR fetching scheme from schemeDealer with cause {%s}", e.getMessage()));
        }

        schemeDealerMappingDetailsResponse.setStatus(Status.FAILED.name());

        return schemeDealerMappingDetailsResponse;

    }

    private void getExcludedManufacturerData(GetSchemeDealerMappingRequest schemeDealerMappingRequest,
                                             SchemeDealerMappingDetails schemeDealerMappingDetails) throws Exception {

        logger.debug("MDVR invoked method to get excluded manufacturer data ");

        Header header = new Header();
        GetSchemeInformationRequest schemeInformationRequest = new GetSchemeInformationRequest();

        String institutionId = schemeDealerMappingRequest.getHeader().getInstitutionId();
        String productName = schemeDealerMappingRequest.getHeader().getProduct().name();

        header.setInstitutionId(institutionId);
        schemeInformationRequest.setCcid(schemeDealerMappingRequest.getCcid());
        schemeInformationRequest.setHeader(header);
        /**
         * get productAlias Name from cache against the product name and institutionId
         */
//        String productAliasName = Cache.getAliasNameByInstitutionIdAndProductName(productName ,institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        Set<String> allExludedManfcSet = getManufacturerAgainstCcid(schemeInformationRequest, productAliasName);

        allExludedManfcSet.removeAll(schemeDealerMappingDetails.getIncludedMnfcr());

        schemeDealerMappingDetails.setExcludedMnfcr(allExludedManfcSet);

        logger.debug("MDVR completed executing excluded manufacturer data ");


    }

    private void getExcludedStateData(GetSchemeDealerMappingRequest getSchemeDealerMappingRequest,
                                      SchemeDealerMappingDetails schemeDealerMappingDetails) throws Exception {

        logger.debug("MDVR invoked method to get excluded state data ");

        Set<String> allExcludedSet = getAllState(getSchemeDealerMappingRequest.getHeader().getInstitutionId());

        allExcludedSet.removeAll(schemeDealerMappingDetails.getIncludedState());

        schemeDealerMappingDetails.setExcludedState(allExcludedSet);

        logger.debug("MDVR completed executing method to get excluded state data ");

    }

    @Override
    public Set<IncludedAssetCtgWithManfc> viewAllAssetCtgrAgainstManfctr(GetAllAssetCtgrAgainstMnfcRequest assetCtgrAgainstMnfcRequest, String productAliasName) {

        logger.debug("MDVR invoked to fetch  all asset category against manufacturer [{}]", assetCtgrAgainstMnfcRequest.getManufacturer());

        Query query = new Query();

        query.addCriteria(Criteria.where("manufacturerDesc").in(assetCtgrAgainstMnfcRequest.getManufacturer())
                .and("institutionId").is(assetCtgrAgainstMnfcRequest.getHeader()
                        .getInstitutionId()).and("productFlag").is(productAliasName).and("active").is(true));

        List<AssetModelMaster> assetModelMasters = mongoTemplate.find(query, AssetModelMaster.class);

        Set<IncludedAssetCtgWithManfc> assetCtgWithManfcs = assetModelMasters.stream()
                .filter(Objects::nonNull)
                .filter(assetMaster -> StringUtils.isNotBlank(assetMaster.getManufacturerDesc()) && StringUtils.isNotBlank(assetMaster.getCatgDesc()))
                .map(assetMaster -> {
                    IncludedAssetCtgWithManfc includedAssetCtgWithManfc = new IncludedAssetCtgWithManfc();
                    includedAssetCtgWithManfc.setIncludedManfc(assetMaster.getManufacturerDesc());
                    includedAssetCtgWithManfc.setIncludedACtgr(assetMaster.getCatgDesc());
                    return includedAssetCtgWithManfc;
                }).collect(Collectors.toSet());

        return assetCtgWithManfcs;
    }

    @Override
    public   Set<IncludedModelIdWithInformation> viewAllModelAgainstAsstCtgMnfc(
            GetAllModelAgainstAsstCtgRequest modelAgainstAsstCtgRequest, String productAliasName) {

        try {

            Set<IncludedModelIdWithInformation> modelNoWithAllInfos = new HashSet<>();

            List<IncludedMakeWithCtgMnfcr> includedMakeWithCtgMnfcrs = modelAgainstAsstCtgRequest.getIncludedMakeWithCtgMnfcrs();


            includedMakeWithCtgMnfcrs.stream()
                    .filter(Objects::nonNull)
                    .filter(makeWithCatManf -> StringUtils.isNotBlank(makeWithCatManf.getIncludeAsstMake())
                            && StringUtils.isNotBlank(makeWithCatManf.getIncludedAsstCtg())
                            && StringUtils.isNotBlank(makeWithCatManf.getIncludeManfcr()))
                            .forEach(makeWithCatManf -> {
                                Query query = new Query();

                                query.addCriteria(Criteria.where("manufacturerDesc").is(makeWithCatManf.getIncludeManfcr())
                                        .and("institutionId").is(modelAgainstAsstCtgRequest.getHeader().getInstitutionId()).and("catgDesc")
                                        .is(makeWithCatManf.getIncludedAsstCtg()).and("make").is(makeWithCatManf.getIncludeAsstMake())
                                        .and("makeModelFlag").is("MO").and("productFlag").is(productAliasName).and("active").is(true));


                                List<AssetModelMaster> assetModelMasters = mongoTemplate.find(query, AssetModelMaster.class);

                                Set<IncludedModelIdWithInformation> modelNoWithAllInfo = assetModelMasters.stream()
                                        .filter(Objects::nonNull)
                                        .filter(assetModelMaster -> StringUtils.isNotBlank(assetModelMaster.getModelNo()))
                                        .map(assetMaster -> {
                                            IncludedModelIdWithInformation modelInfo = new IncludedModelIdWithInformation();
                                            modelInfo.setIncludeManfcr(assetMaster.getManufacturerDesc());
                                            modelInfo.setIncludedAsstCtg(assetMaster.getCatgDesc());
                                            modelInfo.setIncludeAsstMake(assetMaster.getMake());
                                            modelInfo.setModelId(assetMaster.getModelId());
                                            modelInfo.setIncludedModel(assetMaster.getModelNo());
                                            return modelInfo;
                                        }).collect(Collectors.toSet());

                                if (!CollectionUtils.isEmpty(modelNoWithAllInfo)) {
                                    modelNoWithAllInfos.addAll(modelNoWithAllInfo);
                                }

                            });

            return modelNoWithAllInfos;
        } catch (Exception e) {

            logger.error("Error in MDVR fetching asset model set with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error fetching asset model set with probable cause {%s}", e.getMessage()));
        }

    }

    @Override
    public Set<InclDealerIdWithStateCity> viewAllDealerAgainstCity(GetAllDealerAgainstCityRequest dealerAgainstCityRequest) {

        try {

            Set<InclDealerIdWithStateCity> inclDealerIdWithDetails = new HashSet<>();
            String institutionId = dealerAgainstCityRequest.getHeader().getInstitutionId();

            List<InclCityWithStateName> inclCityWithStateNames = dealerAgainstCityRequest.getInclCityWithStateNames();

            inclCityWithStateNames.stream()
                    .filter(Objects::nonNull)
                    .filter(cityWithStateName -> StringUtils.isNotBlank(cityWithStateName.getCityName())
                            && StringUtils.isNotBlank(cityWithStateName.getStateName()))
                            .forEach(cityWithStateName -> {

                                Query query = new Query();

                                String stateName = cityWithStateName.getStateName();
                                String cityName = cityWithStateName.getCityName();

                                query.addCriteria(Criteria.where("cityName").is(cityName)
                                        .and("stateDesc").is(stateName)
                                        .and("institutionId").is(institutionId).and("active").is(true));

                                CityStateMappingMaster cityStateMappingMaster = mongoTemplate.findOne(query, CityStateMappingMaster.class);

                                if (null != cityStateMappingMaster) {

                                    logger.debug("MDVR fetched city record for {}", cityStateMappingMaster.getCityName());
                                    getAllDealerWithName(cityStateMappingMaster.getCityId(), cityName, stateName, institutionId, inclDealerIdWithDetails);

                                } else {
                                    logger.debug("MDVR city record not found against the state {} and city {}", stateName, cityName);
                                }

                            });

            return inclDealerIdWithDetails;
        } catch (Exception e) {

            logger.error("Error in MDVR fetching dealer details with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error fetching dealer details with probable cause {%s}", e.getMessage()));
        }

    }

    private void getAllDealerWithName(String cityId, String cityName, String stateName, String institutionId, Set<InclDealerIdWithStateCity> inclDealerIdWithDetails) {
        logger.debug("MDVR invoked to fetch all dealer name against city id {}", cityId);

        Query query = new Query();

        query.addCriteria(Criteria.where("cityId").is(cityId).and("institutionID").is(institutionId).and("active").is(true));

        List<DealerEmailMaster> dealerEmailMasterList = mongoTemplate.find(query, DealerEmailMaster.class);

        Set<InclDealerIdWithStateCity> dealerDetails = dealerEmailMasterList.stream()
                .filter(Objects::nonNull)
                .filter(dealerEmailMaster -> StringUtils.isNotBlank(dealerEmailMaster.getDealerID()) && StringUtils.isNotBlank(dealerEmailMaster.getSupplierDesc()))
                .map(dealerEmailMaster1 -> {

                    InclDealerIdWithStateCity inclDealerIdWithStateCity = new InclDealerIdWithStateCity();

                    inclDealerIdWithStateCity.setDealerID(dealerEmailMaster1.getDealerID());
                    inclDealerIdWithStateCity.setSupplierDesc(dealerEmailMaster1.getSupplierDesc());
                    inclDealerIdWithStateCity.setCityName(cityName);
                    inclDealerIdWithStateCity.setStateName(stateName);
                    return inclDealerIdWithStateCity;
                }).collect(Collectors.toSet());

        if (!CollectionUtils.isEmpty(dealerDetails)) {
            inclDealerIdWithDetails.addAll(dealerDetails);
        }

    }

    @Override
    public DealerEmailMaster getDealerDetails(String institutionId, String dealerid) throws Exception {

        logger.debug("MDVR invoked to fetch dealer detail against institutionId {} and dealerId {}", institutionId, dealerid);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("dealerID").is(dealerid)
                    .and("institutionID").is(institutionId).and("active").is(true));

            return mongoTemplate.findOne(query, DealerEmailMaster.class);

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching dealer detail probable cause ", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR fetching dealer detail probable cause [%s]", e.getMessage()));

        }

    }

    @Override
    public AssetModelMaster getAssetModelMaster(String modelNo,
                                                String catgDesc, String maufacturerDesc, String assetMake, String productAliasName, String institutionId) throws Exception {

        logger.debug("MDVR invoked to fetch asset detail against modelNo {}, catg {}, manufacutrer {}, assetMake {}",
                modelNo, catgDesc, maufacturerDesc, assetMake);

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true)
                            .and("institutionId").is(institutionId)
                            .and("productFlag").is(productAliasName)
                            .and("modelNo").is(modelNo)
                            .and("catgDesc").is(catgDesc)
                            .and("manufacturerDesc").is(maufacturerDesc)
                            .and("make").is(assetMake)
                            .and("makeModelFlag").is("MO"));

            return mongoTemplate.findOne(query, AssetModelMaster.class);

        } catch (DataAccessException e) {

            logger.error("Error im MDVR while fetching asset details {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error im MDVR while fetching asset details {%s}", e.getMessage()));

        }


    }

    @Override
    public Set<String> getNonVanillaSchemeIds(String ccId, String productAliasName, String institutionId) throws Exception {

        logger.debug("MDVR invoked to fetch schemeMaster data with and without filter against ccId {}", ccId);

        Set<String> schemeIdSet = new TreeSet<>();

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("ccid").is(ccId).and("productFlag").is(productAliasName)
                    .and("institutionId").is(institutionId).and("active").is(true));

            List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query, SchemeMasterData.class);

            if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

                logger.debug("MDVR fetched schemeMaster filter data of size {}", schemeMasterDataList.size());

                if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

                    for (SchemeMasterData schemeMaster : schemeMasterDataList) {

                        if (!StringUtils.startsWith(schemeMaster.getSchemeDesc(), "VNL_")) {

                            schemeIdSet.add(schemeMaster.getSchemeID());

                        }
                    }

                    logger.debug("MDVR fetched schemeId data of size {}", schemeIdSet.size());

                }
            }

            return schemeIdSet;

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching schemeMaster data with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR fetching schemeMaster data with probable cause {%s}", e.getMessage()));

        }
    }

    @Override
    public List<SchemeMasterData> getVanillaSchemeMasterDetails(String ccid, String productAliasName, String institutionId) throws Exception {

        logger.debug("MDVR invoked to fetch schemeMaster data for vanilla against ccId {}", ccid);
        List<SchemeMasterData> filterSchemeMasterList = new ArrayList<>();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("ccid").is("").and("productFlag").is(productAliasName).and("institutionId").is(institutionId).and("active").is(true));

            List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query, SchemeMasterData.class);

            if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

                for (SchemeMasterData schemeMaster : schemeMasterDataList) {

                    if (StringUtils.startsWith(schemeMaster.getSchemeDesc(), "VNL_")) {

                        filterSchemeMasterList.add(schemeMaster);
                    }
                }
            }
            return filterSchemeMasterList;

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching schemeMaster data for vanila with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR fetching schemeMaster data for vanila with probable cause {%s}", e.getMessage()));

        }

    }

    @Override
    public List<SchemeMasterData> getSchemeMasterData(Set<String> schemeIdSet, String productAliasName, String institutionId) throws Exception {

        logger.debug("MDVR invoked to fetch schemeMaster data for schemeId set of size {}", schemeIdSet.size());

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").in(schemeIdSet).and("productFlag").is(productAliasName)
                    .and("institutionId").is(institutionId).and("active").is(true));

            return mongoTemplate.find(query, SchemeMasterData.class);

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching schemeMaster data for schemeId set with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR fetching schemeMaster data for scheme set with probable cause {%s}", e.getMessage()));
        }
    }

    @Override
    public List<SchemeDetailsDateMapping> getSchemeDetailsDateMapping(Set<String> finalSchemeIdSet, String productAliasName, String institutionId) throws Exception {

        logger.debug("MDVR invoked to fetch schemeDateMaster data for schemeId set of size {}", finalSchemeIdSet.size());

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").in(finalSchemeIdSet)
                    .and("updateStatus").is(false).and("schemeStatus").is(Status.APPROVED.name()).and("institutionID").is(institutionId).and("productFlag").is(productAliasName));

            return mongoTemplate.find(query, SchemeDetailsDateMapping.class);

        } catch (DataAccessException e) {

            logger.error("Error in MDVR fetching schemDateMaster data for schemeId set with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error fetching schemeDateMaster data for scheme set with probable cause {%s}", e.getMessage()));
        }

    }

    @Override
    public List<SchemeMasterData> getGeneralSchemeMasterDetails(String productAliasName, String institutionId) {

        logger.debug("MDVR invoked to fetch schemeMaster no filter scheme for vanilla against");

        List<SchemeMasterData> generalSchemeMasterDataList = new ArrayList();
        Query query = new Query();
        query.addCriteria(Criteria.where("ccid").is("").and("productFlag").is(productAliasName).and("institutionId").is(institutionId).and("active").is(true));

        List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query, SchemeMasterData.class);

        if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

            logger.debug("MDVR fetched schemeMaster not filter scheme vanila of size {}", schemeMasterDataList.size());

            for (SchemeMasterData schemeMasterData : schemeMasterDataList) {

                if (!StringUtils.startsWith(schemeMasterData.getSchemeDesc(), "VNL_")) {

                    generalSchemeMasterDataList.add(schemeMasterData);
                }
            }
        }

        return generalSchemeMasterDataList;
    }

    @Override
    public boolean checkDealerSchemeMapping(String schemeId, String dealerId, String productAliasName, String institutionId) {

        logger.debug("MDVM is invoked to check dealer exist in scheme dealer master for schemeId {} and dealerId {}", schemeId, dealerId);

        Query query = new Query();

        boolean flag = false;

        query.addCriteria(Criteria.where("schemeID").is(schemeId)
                .and("updateStatus").is(false).and("schemeStatus").is(Status.APPROVED.name())
                .and("productFlag").is(productAliasName).and("institutionId").is(institutionId));

        SchemeDealerMappingDetails schemeDealerMappingDetails = mongoTemplate.findOne(query, SchemeDealerMappingDetails.class);

        if (null != schemeDealerMappingDetails) {

            if (!schemeDealerMappingDetails.isAllStateCityDealer()) {

                InclDealerIdWithStateCity[] dealerIdWithNameArray = schemeDealerMappingDetails.getInclDealerIdWithStateCity();

                for (InclDealerIdWithStateCity inclDealerIdWithStateCity : dealerIdWithNameArray) {

                    if (StringUtils.equals(inclDealerIdWithStateCity.getDealerID(), dealerId)) {

                        flag = true;
                        break;
                    }
                }

            } else {

                flag = true;

            }
        }

        logger.debug("MDVM completed with  dealer status exist {} in scheme dealer master", flag);

        return flag;

    }

    @Override
    public boolean checkModelSchemeMapping(String schemeId, String modelId, String productAliasName, String institutionId) {

        logger.debug("MDVM is invoked to check model exist in scheme dealer master for schemeId {} and modelId {}", schemeId, modelId);

        boolean flag = false;

        Query query = new Query();

        query.addCriteria(Criteria.where("schemeID").is(schemeId)
                .and("updateStatus").is(false).and("schemeStatus").is(Status.APPROVED.name()).and("productFlag").is(productAliasName).and("institutionId").is(institutionId));

        SchemeDealerMappingDetails schemeDealerMappingDetails = mongoTemplate.findOne(query, SchemeDealerMappingDetails.class);

        if (null != schemeDealerMappingDetails && null != schemeDealerMappingDetails.getIncludedModelIdWithInformation()) {

            IncludedModelIdWithInformation[] modelIdSet = schemeDealerMappingDetails.getIncludedModelIdWithInformation();

            for (IncludedModelIdWithInformation modelObject : modelIdSet) {

                if (StringUtils.equals(modelObject.getModelId(), modelId)) {

                    flag = true;
                    break;
                }

            }

        }

        logger.debug("MDVM completed with  model status exist {} in scheme dealer master", flag);

        return flag;

    }

    @Override
    public  Set<IncludedMakeWithCtgMnfcr> getAssetMakeAgainstMnfcCtg(GetAllAssetMakeAgainstMnfCtgRequest makesAgnstMnfCtgReq, String productAliasName) throws Exception {

        try {

            Set<IncludedMakeWithCtgMnfcr> includedMakeWithCtgMnfcrList = new HashSet<>();

            List<IncludedAssetCtgWithManfc> includedAssetCtgWithManfcs = makesAgnstMnfCtgReq.getIncludedAssetCtgWithManfcs();

            includedAssetCtgWithManfcs.stream()
                    .filter(Objects::nonNull)
                    .filter(ctgWithManfcr -> StringUtils.isNotBlank(ctgWithManfcr.getIncludedManfc()) && StringUtils.isNotBlank(ctgWithManfcr.getIncludedACtgr()))
                            .forEach(ctgWithManfcr -> {
                                Query query = new Query();

                                query.addCriteria(Criteria.where("catgDesc").is(ctgWithManfcr.getIncludedACtgr())
                                        .and("manufacturerDesc").is(ctgWithManfcr.getIncludedManfc())
                                        .and("institutionId").is(makesAgnstMnfCtgReq.getHeader()
                                                .getInstitutionId()).and("modelNo").ne("").and("active").is(true)
                                        .and("productFlag").is(productAliasName));

                                List<AssetModelMaster> assetModelMasters = mongoTemplate.find(query, AssetModelMaster.class);

                                Set<IncludedMakeWithCtgMnfcr> assetCtgWithManfcsandMake = assetModelMasters.stream()
                                        .filter(Objects::nonNull)
                                        .filter(assetModelMaster -> StringUtils.isNotBlank(assetModelMaster.getMake()))
                                        .map(assetMaster -> {
                                            IncludedMakeWithCtgMnfcr includedMakeWithCtgMnfcr = new IncludedMakeWithCtgMnfcr();
                                            includedMakeWithCtgMnfcr.setIncludeManfcr(assetMaster.getManufacturerDesc());
                                            includedMakeWithCtgMnfcr.setIncludedAsstCtg(assetMaster.getCatgDesc());
                                            includedMakeWithCtgMnfcr.setIncludeAsstMake(assetMaster.getMake());
                                            return includedMakeWithCtgMnfcr;
                                        }).collect(Collectors.toSet());

                                if (!CollectionUtils.isEmpty(assetCtgWithManfcsandMake)) {
                                    includedMakeWithCtgMnfcrList.addAll(assetCtgWithManfcsandMake);
                                }

                            });

            return includedMakeWithCtgMnfcrList;
        } catch (Exception e) {

            logger.error("Error in MDVR fetching asset make set with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error fetching asset make set with probable cause {%s}", e.getMessage()));
        }

    }

    @Override
    public List<SchemeInformationResponse> getSchemeAgainstManufacturer(String manufacturerId, String institutionId, String productAliasName) {

        logger.debug("MDVM is invoked to fetch schemes in scheme master against manufacturer {}", manufacturerId);

        Query query = new Query();

        query.addCriteria(Criteria.where("ccid").is(manufacturerId).and("active").is(true).and("institutionId").is(institutionId).and("productFlag").is(productAliasName));
        query.with(new Sort(Sort.Direction.ASC, "schemeDesc"));

        List<SchemeMasterData> schemeMasterDataList = mongoTemplate.find(query, SchemeMasterData.class);

        List<SchemeInformationResponse> schemeInformationResponsesList = new ArrayList<>();


        if (null != schemeMasterDataList && !schemeMasterDataList.isEmpty()) {

            logger.debug("MDVM fetched scheme count {}", schemeMasterDataList.size());

            for (SchemeMasterData schemeMasterData : schemeMasterDataList) {

                SchemeInformationResponse schemeInformationResponse = new SchemeInformationResponse();

                schemeInformationResponse.setSchemeDesc(schemeMasterData.getSchemeDesc());
                schemeInformationResponse.setSchemeID(schemeMasterData.getSchemeID());
                schemeInformationResponse.setCcid(schemeMasterData.getCcid());
                schemeInformationResponsesList.add(schemeInformationResponse);

            }

        }

        logger.debug("MDVM fetching scheme completed successfully for manufacturer ");

        return schemeInformationResponsesList;

    }

    @Override
    public List<SchemeInformationResponse> getVanillaScheme(String institutionId, String productAliasName) {

        logger.debug("MDVM is invoked to fetch all vanilla schemes in scheme master");

        Query query = new Query();

        query.addCriteria(Criteria.where("ccid").is("").and("active").is(true).and("institutionId").is(institutionId).and("productFlag").is(productAliasName));

        query.with(new Sort(Sort.Direction.ASC, "schemeDesc"));

        List<SchemeInformationResponse> vanillaSchemeList = new ArrayList<>();

        List<SchemeMasterData> schemeMasterDateList = mongoTemplate.find(query, SchemeMasterData.class);

        if (null != schemeMasterDateList && !schemeMasterDateList.isEmpty()) {

            logger.debug("MDVM fetched {} schemes from  scheme master ", schemeMasterDateList.size());

            for (SchemeMasterData schemeMasterData : schemeMasterDateList) {

                if (StringUtils.startsWith(schemeMasterData.getSchemeDesc(), "VNL_")) {

                    SchemeInformationResponse schemeInformationResponse = new SchemeInformationResponse();

                    schemeInformationResponse.setSchemeDesc(schemeMasterData.getSchemeDesc());
                    schemeInformationResponse.setSchemeID(schemeMasterData.getSchemeID());
                    schemeInformationResponse.setCcid(schemeMasterData.getCcid());

                    vanillaSchemeList.add(schemeInformationResponse);

                }
            }

        }

        logger.debug("MDVM completed fetching all vanilla schemes with scheme count {}", vanillaSchemeList.size());

        return vanillaSchemeList;
    }

    @Override
    public List<SchemeInformationResponse> getGeneralScheme(String institutionId, String productAliasName) {

        logger.debug("MDVM is invoked to fetch general schemes in scheme master");

        Query query = new Query();

        query.addCriteria(Criteria.where("ccid").is("").and("active").is(true).and("institutionId").is(institutionId).and("productFlag").is(productAliasName));
        query.with(new Sort(Sort.Direction.ASC, "schemeDesc"));

        List<SchemeInformationResponse> generalSchemeList = new ArrayList<>();

        List<SchemeMasterData> schemeMasterDateList = mongoTemplate.find(query, SchemeMasterData.class);

        if (null != schemeMasterDateList && !schemeMasterDateList.isEmpty()) {
            for (SchemeMasterData schemeMasterData : schemeMasterDateList) {

                if (!StringUtils.startsWith(schemeMasterData.getSchemeDesc(), "VNL_")) {

                    SchemeInformationResponse schemeInformationResponse = new SchemeInformationResponse();

                    schemeInformationResponse.setSchemeDesc(schemeMasterData.getSchemeDesc());
                    schemeInformationResponse.setSchemeID(schemeMasterData.getSchemeID());
                    schemeInformationResponse.setCcid(schemeMasterData.getCcid());

                    generalSchemeList.add(schemeInformationResponse);
                }
            }

        }

        logger.debug("MDVM fetched {} general schemes from scheme master", generalSchemeList.size());

        return generalSchemeList;

    }

    @Override
    public List<ManufacturerDescWithCcid> getAllManufacturer(GetAllManufacturerRequest getAllManufacturerRequest, String productAliasName) {

        logger.debug("MDVM is invoked to fetch all manufacturer from scheme master");

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("ccid").ne("").and("active").is(true).
                    and("institutionId").is(getAllManufacturerRequest.getHeader().getInstitutionId())
                    .and("productFlag").is(productAliasName));

            List<String> distinctManufacturerIdList = mongoTemplate.getDb().getCollection("schemeMaster")
                    .distinct("ccid", query.getQueryObject());

            return getManufacturerWithCcid(distinctManufacturerIdList, productAliasName, getAllManufacturerRequest.getHeader().getInstitutionId());
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error fetching asset manufactuer set with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Error fetching asset manufactuer set with probable cause {%s}", e.getMessage()));
        }
    }

    private List<ManufacturerDescWithCcid> getManufacturerWithCcid(List<String> distinctManufacturerIdList, String productAliasName, String institutionId) {

        logger.debug("MDVM is invoked to fetch all manufacturer and ccid from asset model master againt provided manufacture id list");

        ManufacturerDescWithCcid manufacturerDescWithCcid;

        List<ManufacturerDescWithCcid> manufacturerDescWithCcidList = new ArrayList<>();

        Query query = new Query();

        query.addCriteria(Criteria.where("manufacturerId").in(distinctManufacturerIdList).and("active").is(true)
                .and("productFlag").is(productAliasName).and("institutionId").is(institutionId));

        List<AssetModelMaster> assetModelMasterList = mongoTemplate.find(query, AssetModelMaster.class);

        if (null != assetModelMasterList && !assetModelMasterList.isEmpty()) {

            logger.info("Asset model fetched {} asset against provided manufacture ", assetModelMasterList.size());

            for (AssetModelMaster assetModelMaster : assetModelMasterList) {

                boolean flag = true;

                manufacturerDescWithCcid = new ManufacturerDescWithCcid();

                manufacturerDescWithCcid.setManufactureId(assetModelMaster.getManufacturerId());

                manufacturerDescWithCcid.setManufactureDesc(assetModelMaster.getManufacturerDesc());

                if (!manufacturerDescWithCcidList.isEmpty()) {

                    for (ManufacturerDescWithCcid manufacturerDescWithCcid1 : manufacturerDescWithCcidList) {

                        if (StringUtils.equals(manufacturerDescWithCcid1.getManufactureDesc(), manufacturerDescWithCcid
                                .getManufactureDesc())) {

                            flag = false;

                            break;
                        }
                    }

                }
                if (flag) {
                    manufacturerDescWithCcidList.add(manufacturerDescWithCcid);
                }

            }

        }

        logger.debug("MDVM completed fetching manufacturer and ccid from asset model master ");

        return manufacturerDescWithCcidList;
    }

    @Override
    public List<SchemeDetailsDateMapping> getAllSchemeDateMappingScheme(String schemeStatus, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to fetch all scheme from schemeDate master base on schemeStatus {}", schemeStatus);

        try {
            Query query = new Query();

            query.with(new Sort(Sort.Direction.ASC, "schemeID"));
            query.addCriteria(Criteria.where("updateStatus").is(false).and("schemeStatus").is(schemeStatus).and("productFlag").is(productAliasName)
                    .and("institutionID").is(institutionId));

            return mongoTemplate.find(query, SchemeDetailsDateMapping.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Error fetching asset all scheme date mapping set  probable cause {}", e.getMessage());
            throw new SystemException(String.format("Error fetching  all scheme date mapping with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public SchemeDealerMappingDetails getApplicableAssetDetails(
            String schemeID, String typeOfScheme, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to fetch asset from schemeDate master base on scheme id {} , type {}", schemeID, typeOfScheme);

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").is(schemeID)
                    .and("updateStatus").is(false).and("schemeStatus").is(typeOfScheme)
                    .and("institutionId").is(institutionId).and("productFlag").is(productAliasName));

            return mongoTemplate.findOne(query, SchemeDealerMappingDetails.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("Problem occured to get applicable asset details with probable cause {} ", e.getMessage());
            throw new SystemException(String.format("problem occure to get applicable asset details with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public boolean changeSchemeStatus(SchemeApproveDeclineRequest schemeApproveDeclineRequest, String productAliasName) throws Exception {

        logger.info("MDVM invoked to update scheme id [{}] status in schemeDate master ", schemeApproveDeclineRequest.getSchemeID());

        String institutionId = schemeApproveDeclineRequest.getHeader().getInstitutionId();
        try {

            Query queryForDateMapping = QueryBuilder.buildQueryForSchemeMapping(
                    schemeApproveDeclineRequest.getSchemeID(), institutionId, productAliasName, Status.DATE_MAPPING.name());

            if (mongoTemplate.exists(queryForDateMapping, SchemeDetailsDateMapping.class)) {
                // here we check if previous any updation on APPROVE scheme.
                // if yes then execute the if block.
                Query queryForDealerMapping = QueryBuilder.buildQueryForSchemeMapping(
                        schemeApproveDeclineRequest.getSchemeID(), institutionId, productAliasName, Status.DEALER_MAPPING.name());

                if (checkIfModificationInApproveScheme(schemeApproveDeclineRequest.getSchemeID(), productAliasName, institutionId)) {
                    // update old date mapping part and set update flag true
                    backupPreviousSchemeDateMapping(schemeApproveDeclineRequest, productAliasName);
                    // update old dealer mapping part and set update flag true
                    backupPreviousSchemeDealerMapping(schemeApproveDeclineRequest, productAliasName);
                    updateSchemeDateMappingWithStatus(schemeApproveDeclineRequest, queryForDateMapping);
                    updateSchemeDealerMappingWithStatus(schemeApproveDeclineRequest, queryForDealerMapping);

                    return true;

                } else {
                    updateSchemeDateMappingStatusForPendingOrRejectedScheme(schemeApproveDeclineRequest, queryForDateMapping);
                    updateSchemeDealerMappingStatusForPendingOrRejectedScheme(schemeApproveDeclineRequest, queryForDealerMapping);

                    return true;
                }

            }

            logger.info("MDVM invoked to update scheme status in schemeDate master, scheme does not exist ");

            return false;

        } catch (DataAccessException e) {

            logger.error("Error in MDVR updating scheme in schemeMaster with probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR updating scheme in schemeMaster with probable cause {%s}", e.getMessage()));
        }
    }

    private boolean checkIfModificationInApproveScheme(String schemeId, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to check scheme id [{}] exist in schemeDate master ", schemeId);

        Query query = new Query();

        query.addCriteria(Criteria.where("schemeID").is(schemeId)
                .and("updateStatus").is(false).and("approveSchemeUpdateStatus")
                .is(true).and("schemeStatus").is(Status.APPROVED.name()).and("institutionID").is(institutionId).and("productFlag").is(productAliasName));


        return mongoTemplate.exists(query, SchemeDetailsDateMapping.class);
    }

    private void updateSchemeDealerMappingWithStatus(SchemeApproveDeclineRequest schemeApproveDeclineRequest, Query queryForDealerMapping) {

        logger.info("MDVM invoked to update scheme status [{}] in schemeDealer master ", schemeApproveDeclineRequest.getSchemeStatus());

        Update update = new Update();

        update.set("approveSchemeUpdateStatus", false);
        update.set("schemeStatus", schemeApproveDeclineRequest.getSchemeStatus());

        mongoTemplate.updateFirst(queryForDealerMapping, update, SchemeDealerMappingDetails.class);

        logger.info("MDVM updated scheme status in schemeDealer master successfully ", schemeApproveDeclineRequest.getSchemeStatus());

    }

    private void updateSchemeDateMappingWithStatus(SchemeApproveDeclineRequest schemeApproveDeclineRequest, Query queryForDateMapping) {

        logger.info("MDVM invoked to update scheme [{}] status, checker Id and date schemeDate master ",
                schemeApproveDeclineRequest.getSchemeID());

        Update update = new Update();

        update.set("approveSchemeUpdateStatus", false);
        update.set("schemeStatus", schemeApproveDeclineRequest.getSchemeStatus());
        update.set("checkerId", schemeApproveDeclineRequest.getCheckerId());
        update.set("checkDate", schemeApproveDeclineRequest.getCheckDate());

        mongoTemplate.updateFirst(queryForDateMapping, update, SchemeDetailsDateMapping.class);

        logger.info("MDVM updated scheme status, checker Id and date in schemeDate master successfully");

    }

    private void backupPreviousSchemeDealerMapping(SchemeApproveDeclineRequest schemeApproveDeclineRequest, String productAliasName) {

        logger.info("MDVM invoked to update scheme [{}] updateStatus filed in schemeDealer master ", schemeApproveDeclineRequest.getSchemeID());

        Query query = new Query();

        query.addCriteria(Criteria.where("schemeID").is(schemeApproveDeclineRequest.getSchemeID())
                .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(true)
                .and("institutionId").is(schemeApproveDeclineRequest.getHeader().getInstitutionId()).and("productFlag").is(productAliasName));

        Update update = new Update();

        update.set("updateStatus", true);

        mongoTemplate.updateFirst(query, update, SchemeDealerMappingDetails.class);

        logger.info("MDVM updated scheme updateStatus filed in schemeDealer master successfully ");

    }

    private void backupPreviousSchemeDateMapping(SchemeApproveDeclineRequest schemeApproveDeclineRequest, String productAliasName) {

        logger.info("MDVM invoked to update scheme [{}] updateStatus filed in schemeDate master ", schemeApproveDeclineRequest.getSchemeID());

        Query query = new Query();

        query.addCriteria(Criteria.where("schemeID").is(schemeApproveDeclineRequest.getSchemeID())
                .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(true)
                .and("institutionID").is(schemeApproveDeclineRequest.getHeader().getInstitutionId()).and("productFlag").is(productAliasName));

        Update update = new Update();

        update.set("updateStatus", true);

        mongoTemplate.updateFirst(query, update, SchemeDetailsDateMapping.class);

        logger.info("MDVM updated scheme updateStatus filed in schemeDate master successfully ");

    }

    private void updateSchemeDealerMappingStatusForPendingOrRejectedScheme(SchemeApproveDeclineRequest schemeApproveDeclineRequest, Query queryForDealerMapping) {

        logger.info("MDVM invoked to update scheme [{}] updateStatus filed in schemeDealer master ", schemeApproveDeclineRequest.getSchemeID());


        Update update = new Update();

        update.set("schemeStatus", schemeApproveDeclineRequest.getSchemeStatus());

        mongoTemplate.updateFirst(queryForDealerMapping, update, SchemeDealerMappingDetails.class);

    }

    private void updateSchemeDateMappingStatusForPendingOrRejectedScheme(SchemeApproveDeclineRequest schemeApproveDeclineRequest, Query queryForDateMapping) {

        logger.info("MDVM invoked to update scheme [{}] status, checker Id and date schemeDate master ",
                schemeApproveDeclineRequest.getSchemeID());

        Update update = new Update();

        update.set("schemeStatus", schemeApproveDeclineRequest.getSchemeStatus());
        update.set("checkerId", schemeApproveDeclineRequest.getCheckerId());
        update.set("checkDate", schemeApproveDeclineRequest.getCheckDate());

        mongoTemplate.updateFirst(queryForDateMapping, update, SchemeDetailsDateMapping.class);

        logger.info("MDVM updated scheme status, checker Id and date in schemeDate master ");

    }

    /**
     * allow the "Self_checker" to approve or decline scheme when, maker and
     * checker are not same person i.e. self_checker "checker" role has
     * permission to approve scheme
     */
    @Override
    public boolean checkCheckerDetails(SchemeApproveDeclineRequest schemeApproveDeclineRequest, String productAliasName) {

        logger.info("MDVM invoked to check scheme [{}] checker Details in schemeDate master ", schemeApproveDeclineRequest.getSchemeID());

        Query query = new Query();

        query.addCriteria(Criteria.where("schemeID").is(schemeApproveDeclineRequest.getSchemeID())
                .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(false)
                .and("productFlag").is(productAliasName).and("InstitutionID").is(schemeApproveDeclineRequest.getHeader().getInstitutionId()));

        SchemeDetailsDateMapping schemeDetailsDateMapping = mongoTemplate.findOne(query, SchemeDetailsDateMapping.class);

        if (null != schemeDetailsDateMapping && StringUtils.isNotBlank(schemeDetailsDateMapping.getMakerID())) {

            String userRole = schemeApproveDeclineRequest.getUserRole();

            if ((userRole.equals(Status.SELF_CHECKER.name())
                    && !StringUtils.equals(schemeDetailsDateMapping.getMakerID(), schemeApproveDeclineRequest.getCheckerId()))
                    || userRole.equals(Status.CHECKER.name()) || userRole.equals(Status.SCHEME_ADMIN.name())) {

                return true;
            }
        }

        logger.info("MDVM checked scheme checker details in schemeDate master ");

        return false;

    }

    @Override
    public List<SchemeDealerMappingDetails> getAllPartialDealerMappingSchemeData(List<String> partialSchemeList, String schemeStatus, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to fetch all partial scheme in schemeDealer master with status [{}]", schemeStatus);

        List<SchemeDealerMappingDetails> schemeDealerMappingDetailsList = new ArrayList<>();

        for (String partialSchemeId : partialSchemeList) {

            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").is(partialSchemeId).and("updateStatus").is(false).and("schemeStatus")
                    .is(schemeStatus).and("approveSchemeUpdateStatus").is(false)
                    .and("productFlag").is(productAliasName).and("InstitutionID").is(institutionId));

            if (!mongoTemplate.exists(query, SchemeDetailsDateMapping.class)) {

                fetchActualSchemeDealerData(schemeDealerMappingDetailsList, partialSchemeId, productAliasName, institutionId);

            }
        }

        logger.info("MDVM fetched [{}] partial scheme in schemeDealer master successfully ", schemeDealerMappingDetailsList.size());

        return schemeDealerMappingDetailsList;
    }

    private void fetchActualSchemeDealerData(List<SchemeDealerMappingDetails> schemeDealerMappingDetailsList, String partialSchemeId, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to fetch actual scheme in schemeDealer master for partial scheme [{}]", partialSchemeId);
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("schemeID").is(partialSchemeId)
                    .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(false)
                    .and("productFlag").is(productAliasName).and("InstitutionId").is(institutionId));

            schemeDealerMappingDetailsList.add(mongoTemplate.findOne(query, SchemeDealerMappingDetails.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occure at the time of fetching scheme dealer data with cause {}", e.getMessage());

        }

    }

    @Override
    public List<String> getAllPartialDealerMappingSchemeId(String schemeStatus, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to fetch all partial scheme in schemeDealer master with status [{}]", schemeStatus);

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("updateStatus").is(false).and("schemeStatus").is(schemeStatus)
                    .and("approveSchemeUpdateStatus").is(false).and("productFlag").is(productAliasName).and("institutionId").is(institutionId));

            return mongoTemplate.getDb().getCollection("schemeDealerMappingDetails").distinct("schemeID", query.getQueryObject());
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occured at the time get partial dealer mapping schemeId with probable cause {}", e.getMessage());
            throw new SystemException(String.format("problem occured at the time get partial dealer mapping schemeId with probable cause [%s]", e.getMessage()));
        }

    }

    @Override
    public boolean checkPartialSchemeDetails(SchemeApproveDeclineRequest schemeApproveDeclineRequest, String productAliasName) throws Exception {

        logger.info("MDVM invoked to check partial scheme [{}] in schemeDate master", schemeApproveDeclineRequest.getSchemeID());

        String institutionId = schemeApproveDeclineRequest.getHeader().getInstitutionId();

        try {

            Query queryForDateMapping = QueryBuilder.buildQueryForSchemeMapping(schemeApproveDeclineRequest.getSchemeID()
                    , institutionId, productAliasName, Status.DATE_MAPPING.name());

            if (mongoTemplate.exists(queryForDateMapping, SchemeDetailsDateMapping.class)) {

                return checkSchemeDealerPartialData(schemeApproveDeclineRequest.getSchemeID(), productAliasName, institutionId);

            }
            return false;

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occured at the time get partial scheme details with probable cause {}", e.getMessage());
            throw new SystemException(String.format("Error occured at the time of  get partial scheme details with probable cause [%s]", e.getMessage()));

        }

    }

    private boolean checkSchemeDealerPartialData(String schemeID, String productAliasName, String institutionId) {

        logger.info("MDVM invoked to check partial scheme in schemeDealer mapping ");

        try {
            Query queryForDealerMapping = QueryBuilder.buildQueryForSchemeMapping(schemeID
                    , institutionId, productAliasName, Status.DEALER_MAPPING.name());

            return mongoTemplate.exists(queryForDealerMapping, SchemeDealerMappingDetails.class);

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error(" problem occure at the time of checking scheme dealer mapping partial data");
            throw new SystemException(String.format("problem occured at the time get partial scheme dealer mapping details with cause[%s]", e.getMessage()));

        }
    }

    @Override
    public SchemeDetailsDateMapping viewSchemeDateMappingDeatils(GetSchemeDateMappingDetailsRequest schemeDateMappingDetailsRequest, String productAliasName) throws Exception {

        logger.info("MDVM invoked to view scheme [{}] from schemeDateMappingMaster ", schemeDateMappingDetailsRequest.getSchemeID());

        String institutionId = schemeDateMappingDetailsRequest.getHeader().getInstitutionId();

        try {
            Query queryForDateMapping = QueryBuilder.buildQueryForSchemeMapping(schemeDateMappingDetailsRequest.getSchemeID()
                    , institutionId, productAliasName, Status.DATE_MAPPING.name());

            return mongoTemplate.findOne(queryForDateMapping, SchemeDetailsDateMapping.class);

        } catch (DataAccessException e) {

            logger.error("Error occured at the time of accessing schemedateMappingDetailsWith probable cause {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error occured at the time of accessing schemedateMappingDetailsWith probable cause [%s]", e.getMessage()));


        }
    }

    /**
     * This method is used to update multiple scheme which having same scheme
     * date mapping records. and rename previous record.
     */
    @Override
    public boolean updateSchemeDateMappingDetails(UpdateSchemeDateMappingDetailsRequest updateSchemeDateMappingDetailsRequest, String productAliasName) throws Exception {

        logger.info("MDVM invoked to udpate multiple scheme [{}] in schemeDate master");

        List<SchemeInformationResponse> schemeInformationResponseList = updateSchemeDateMappingDetailsRequest
                .getSchemeDetailsDateMapping().getSchemeInformationResponseList();

        String institutionId = updateSchemeDateMappingDetailsRequest.getHeader().getInstitutionId();

        try {

            for (SchemeInformationResponse schemeInformationResponse : schemeInformationResponseList) {

                String schemeId = schemeInformationResponse.getSchemeID();
                String schmeDesc = schemeInformationResponse.getSchemeDesc();

                Query queryForDateMapping = QueryBuilder
                        .buildQueryForSchemeMapping(schemeId, institutionId, productAliasName, Status.DATE_MAPPING.name());


                SchemeDetailsDateMapping schemeDetailsDateMapping = mongoTemplate.findOne(queryForDateMapping, SchemeDetailsDateMapping.class);

                if (null != schemeDetailsDateMapping) {

                    Query queryForDealerMapping = QueryBuilder
                            .buildQueryForSchemeMapping(schemeId, institutionId, productAliasName, Status.DEALER_MAPPING.name());

                    // if user modify Approve Scheme
                    if (StringUtils.equals(Status.APPROVED.name(), schemeDetailsDateMapping.getSchemeStatus())
                            && !schemeDetailsDateMapping.isApproveSchemeUpdateStatus()) {
                        /*
                         * then create new document with scheme status pending
						 * and after approve this pending scheme merge two
						 * collection i.e. approve scheme and pending scheme
						 */
                        setApproveFlagForSchemeDateMapping(queryForDateMapping);

                        setApproveFlagForSchemeDealerMapping(queryForDealerMapping);

                        makeReplicaOfSchemeDateMapping(schemeId, schmeDesc, updateSchemeDateMappingDetailsRequest, productAliasName);

                        updatePreviousSchemeDealerMapping(schemeId, productAliasName, institutionId);

                        return true;
                        // if user modify Pending/Decline scheme then no
                        // problem.
                    } else {
                        /**
                         *  save previous scheme details date mapping data for backup purpose with updating only update status flag.
                         *  and insert new updated scheme date mapping collection
                         */

                        takeSchemeDateMappingBackupBySoftDelete(queryForDateMapping);
                        /**
                         * keep previous data only change update status i.e. update date.,macker etc
                         */
                        applyChangesInSchemeDealerMapping(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping(), queryForDealerMapping);
                    }
                }

                SchemeDetailsDateMapping schemeDetailsDateMappingBuild = masterDataViewBuilder.buildSchemeDetailsDateMapping(updateSchemeDateMappingDetailsRequest, schmeDesc, schemeId, productAliasName);

                mongoTemplate.insert(schemeDetailsDateMappingBuild);

            }


            logger.info("MDVM updated multiple scheme in schemeDate master successfully");

            return true;

        } catch (DataAccessException e) {

            logger.error("Error in MDVR updating  multiple scheme in schemeDate master {}", e.getMessage());

            e.printStackTrace();

            throw new SystemException(String.format("Error in MDVR at the time updating schemedateMappingDetails master {%s}", e.getMessage()));
        }

    }

    /**
     * set "approveSchemeUpdateStatus" to true i.e user trying to  modify the approve scheme
     */
    private void setApproveFlagForSchemeDealerMapping(Query query) {

        logger.debug("schemeDealer master status update is invoked  for query", query);

        try {
            Update update = new Update();

            update.set("approveSchemeUpdateStatus", true);

            mongoTemplate.updateFirst(query, update, SchemeDealerMappingDetails.class);

            logger.debug("schemeDealer master status updated successfully");

        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("error occur at the time of modify Previous Approved SchemeDealerMapping status with probable cause ", e.getMessage());
        }

    }

    /**
     * set approveSchemeUpdateStatus to true i.e user modify the approve scheme
     */
    private void setApproveFlagForSchemeDateMapping(Query query) {

        logger.debug("schemeDate master status update is invoked  for query  {}", query);

        try {
            Update update = new Update();
            update.set("approveSchemeUpdateStatus", true);

            mongoTemplate.updateFirst(query, update, SchemeDetailsDateMapping.class);

            logger.debug("schemeDate master status update successfully");
        } catch (DataAccessException e) {
            e.printStackTrace();

            logger.error("problem occure at the time of modify Previous Aprrove SchemeDateMapping status with probable cause ", e.getMessage());
        }
    }

    private void updatePreviousSchemeDealerMapping(String schemeId, String productAliasName, String institutionId) {

        logger.debug("Master Data View repo invoked to insert modified scheme dealer mapping {}", schemeId);

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("schemeID").is(schemeId)
                    .and("updateStatus").is(false).and("approveSchemeUpdateStatus").is(true)
                    .and("institutionId").is(institutionId).and("productFlag").is(productAliasName));

            SchemeDealerMappingDetails schemeDealerMappingDetails = mongoTemplate
                    .findOne(query, SchemeDealerMappingDetails.class);

            if (null != schemeDealerMappingDetails) {

                schemeDealerMappingDetails.setSchemeStatus(Status.PENDING.toString());

                schemeDealerMappingDetails.setApproveSchemeUpdateStatus(false);
                schemeDealerMappingDetails.setProductFlag(productAliasName);

                mongoTemplate.insert(schemeDealerMappingDetails);

                logger.debug("Master Data View repo insert modified scheme dealer mapping successfully ");

            }
        } catch (DataAccessException e) {

            e.printStackTrace();
            logger.error("problem occure at the time of save modification of approved scheme dealer mapping with probable cause ", e.getMessage());
        }

    }

    private void makeReplicaOfSchemeDateMapping(String schemeId, String schemeDesc, UpdateSchemeDateMappingDetailsRequest updateSchemeDateMappingDetailsRequest, String productAliasName) {

        logger.debug("Master Data View repo invoked to insert scheme date mapping for schemeId {} and schemeDesc {}", schemeId, schemeDesc);

        try {
            SchemeDetailsDateMapping schemeDetailsDateMapping = updateSchemeDateMappingDetailsRequest
                    .getSchemeDetailsDateMapping();

            schemeDetailsDateMapping.setSchemeID(schemeId);
            schemeDetailsDateMapping.setSchemeDesc(schemeDesc);
            schemeDetailsDateMapping.setProductFlag(productAliasName);

            mongoTemplate.insert(schemeDetailsDateMapping);

            logger.debug("Master Data View repo successfully inserted scheme date mapping ");
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occure at the time of inserting scheme date mapping collection with probable cause  ", e.getMessage());

        }

    }

    private void applyChangesInSchemeDealerMapping(SchemeDetailsDateMapping schemeDetailsDateMapping, Query query) {

        logger.debug("Master Data View repo invoked to update scheme dealer master for query {}", query);

        try {
            Update update = new Update();

            update.set("schemeStatus", schemeDetailsDateMapping.getSchemeStatus());
            update.set("makerID", schemeDetailsDateMapping.getMakerID());
            update.set("makeDate", schemeDetailsDateMapping.getMakeDate());

            mongoTemplate.updateFirst(query, update, SchemeDealerMappingDetails.class);

            logger.debug("Master Data View repo successfully updated scheme dealer master");
        } catch (DataAccessException e) {
            e.printStackTrace();
            logger.error("problem occure at the time updating scheme dealer mapping collection with probable cause");

        }

    }

    private void takeSchemeDateMappingBackupBySoftDelete(Query query) {

        logger.debug("Master Data View repo invoked to update scheme date master for query {}", query);

        try {
            Update update = new Update();
            update.set("updateStatus", true);

            mongoTemplate.updateFirst(query, update, SchemeDetailsDateMapping.class);

            logger.debug("Master Data View repo successfully updated scheme date master");
        } catch (DataAccessException e) {

            logger.error("problem at the time of updating schemeDateMapping collection with probable cause {}", e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public WriteResult deleteCommonGeneralMaster( List<String> uniqueIds, String institutionId) {

        WriteResult writeResult = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").in(uniqueIds)
                    .and("institutionId").is(institutionId).and("active").is(true));

            Update update = new Update();
            update.set("active", false);
            update.set("updateDate", new Date());

            writeResult = mongoTemplate
                    .updateFirst(query, update, CommonGeneralParameterMaster.class);

            logger.info("Write Result:->", writeResult);

            return writeResult;

        } catch (DataAccessException e) {

            logger.error("problem occured at the time of deleting CommonGeneralMaster with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occured at the time of deleting CommonGeneralMaster with probable cause {%s}", e.getMessage()));
        }


    }

    @Override
    public WriteResult updateCommonGeneralParameterMaster(UpdateCommonGeneralMasterRequest updateCommonGeneralMasterRequest ) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(updateCommonGeneralMasterRequest.getUniqueId())
                    .and("institutionId").is(updateCommonGeneralMasterRequest.getHeader().getInstitutionId()).and("active").is(true));

            Update update = QueryBuilder.buildCGPMasterQuery(updateCommonGeneralMasterRequest.getCommonGeneralParameterMaster());

            WriteResult writeResult= mongoTemplate.updateFirst(query, update, CommonGeneralParameterMaster.class);

            logger.info("writeResult:{}", writeResult);

            return writeResult;

        } catch (DataAccessException e) {

            logger.error("problem occured at the time of updating CommonGeneralMaster with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occured at the time of updating CommonGeneralMaster with probable cause {%s}", e.getMessage()));
        }

    }

    @Override
    public void insertCommonGeneralMaster(CommonGeneralParameterMaster commonGeneralParameterMaster) {

        try {
            mongoTemplate.insert(commonGeneralParameterMaster);

        } catch (DataAccessException e) {

            logger.error("problem occured at the time of inserting new CommonGeneralMaster record with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occured at the time of inserting new CommonGeneralMaster record with probable cause {%s}", e.getMessage()));
        }
    }

    @Override
    public Table getCommonGeneralMasterRecord(String institutionId, Integer limit, Integer skip) {

        Query dataQuery = new Query();
        Query countQuery = new Query();
        Table resultList =new Table();

        try {

            dataQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true)).skip(skip).limit(limit);

            resultList.setData(mongoTemplate.find(dataQuery, CommonGeneralParameterMaster.class));

            countQuery.addCriteria(Criteria.where("institutionId").is(institutionId).and("active").is(true));

            resultList.setTotalCount(mongoTemplate.count(countQuery, CommonGeneralParameterMaster.class));

            return resultList;

        }catch(DataAccessException e){

            logger.error("problem occured at the time of fetching CommonGeneralMaster record with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occured at the time of fetching CommonGeneralMaster record with probable cause {%s}", e.getMessage()));
        }

    }

    /**
     * Get sourcing details s1 ,s2 ,s3 ,s4 depending on the employee id provided in the header .i.e. dsa id
     * @param sourcingDetailsRequest
     * @return
     */
    @Override
    public SourcingDetails getSourcingDetails(SourcingDetailsRequest sourcingDetailsRequest) {

        SourcingDetails sourcingDetails = new SourcingDetails();
        String institutionId = sourcingDetailsRequest.getHeader().getInstitutionId();
        SourcingDetailMaster s3 = null;
        SourcingDetailMaster s1 = null;
        List<SourcingDetailMaster> s2 = null;
        SourcingDetailMaster s4 = null;

        try {

            Query s4Query = QueryBuilder.buildSourcingDetailsQuery(institutionId, sourcingDetailsRequest.getHeader().getDsaId(), SourcingEnum.SOURCEID.name());

            s4 = mongoTemplate.findOne(s4Query, SourcingDetailMaster.class);
            if(null !=s4) {
                sourcingDetails.setS4(s4);
            }

            if (null != s4 && StringUtils.isNotBlank(s4.getKey2())) {

                Query s3Query = QueryBuilder.buildSourcingDetailsQuery(institutionId, s4.getKey2(), SourcingEnum.VALIDSRC.name());

                s3 = mongoTemplate.findOne(s3Query, SourcingDetailMaster.class);

                sourcingDetails.setS3(s3);

            }

            if (null != s3 && StringUtils.isNotBlank(s3.getKey2())) {

                Query s1Query = QueryBuilder.buildSourcingDetailsQuery(institutionId, s3.getKey2(), SourcingEnum.SOURCE.name());

                s1 = mongoTemplate.findOne(s1Query, SourcingDetailMaster.class);

                sourcingDetails.setS1(s1);
            }

            if (null != s1 && StringUtils.isNotBlank(s1.getValue())) {

                Query s2Query = new Query();
                Date currentDate =new Date();
                s2Query.addCriteria(Criteria.where("institutionId").is(institutionId)
                        .and("moduleId").is(SourcingEnum.LEA.name())
                        .and("disableFlag").is(SourcingEnum.Y.name())
                        .and("key1").is(SourcingEnum.SOURCEID.name())
                        .and("key2").is(s1.getValue())
                        .and("cgpStartDate").lte(currentDate)
                        .and("cgpEndDate").gte(currentDate)
                        .and("active").is(true));

                s2 = mongoTemplate.find(s2Query, SourcingDetailMaster.class);

                sourcingDetails.setS2(s2);

            }

            return sourcingDetails;

        } catch (Exception e) {
            logger.error("problem occured at the time of getting sourcing details with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occured at the time of getting sourcing details with probable cause {%s}", e.getMessage()));
        }
    }

    @Override
    public SchemeMasterData getSchemeDetails(String schemeId, String institutionId, String productAliasName) {

        Query query = new Query();

        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId)
                .and("productFlag").is(productAliasName).and("schemeID").is(schemeId));

        return mongoTemplate.findOne(query, SchemeMasterData.class);

    }

    public String getAssetCatIdFromAssetCatMaster(String institutionId ,String assetCategory){

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true)
                    .and("institutionId").is(institutionId)
                    .and("assetCatgDesc").is(assetCategory));

            String assetCatId = null;

            AssetCategoryMaster assetCategoryMaster = mongoTemplate.findOne(query, AssetCategoryMaster.class);
            if (null != assetCategoryMaster) {
                assetCatId = assetCategoryMaster.getAssetCatgId();
            }
            return assetCatId;

        } catch (Exception e) {
            e.printStackTrace();

            logger.error("problem occur at the time of getting asset category from assetCatMaster with probable cause {}", e.getMessage());
            e.printStackTrace();
            throw new SystemException(String.format("problem occur at the time of getting asset category from assetCatMaster with probable cause{%s}", e.getMessage()));
        }

    }

    @Override
    public List<DeviationMaster> fetchAllDeviationMasters(String institutionID, Product product){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionID)
        .and("product").is(product).and("active").is(true));
        return mongoTemplate.find(query, DeviationMaster.class);
    }

    @Override
    public DeviationMaster fetchDeviationMastersRecord(String institutionID, Product product, String deviation, String category){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionID)
                .and("product").is(product).and("active").is(true)
                .and("category").is(category).and("deviation").is(deviation));
        return mongoTemplate.findOne(query, DeviationMaster.class);
    }

    @Override
    public List<OrganizationalHierarchyMaster> fetchAllHierarchyMasters(String institutionID){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionID));
        return mongoTemplate.find(query, OrganizationalHierarchyMaster.class);
    }

    /*@Override
    public List<OrganizationalHierarchyMaster> fetchOrganizationalHierarchyMasters(String institutionId, Integer branchCode,
                                                                                     String userId, String productName){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("product").is(productName)
                .and("branchCode").is(branchCode).and("hierarchyNodes.userId").is(userId));
        logger.debug("Query for hierarchy fetching : " + query.toString());
        return mongoTemplate.find(query, OrganizationalHierarchyMaster.class);
    }*/

    @Override
    public List<OrganizationalHierarchyMasterV2> fetchOrganizationalHierarchyMasters(String institutionId, Integer branchCode,
                                               String userId, String productName){
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("product").is(productName)
                            .and("branch.branchId").is(branchCode).and("roleWiseUsersList.users.userId").is(userId));
        logger.debug("Query for hierarchy fetching : " + query.toString());
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    @Override
    public List<RoiSchemeMaster> getRoiSchemeMasterDetails(String institutionId, String product) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("product").is(product));
        return mongoTemplate.find(query, RoiSchemeMaster.class);
    }

    @Override
    public RoiSchemeMaster getSchemeCompanySpecificData(String companyName, String scheme) {
        Query query = new Query();
        query.addCriteria(Criteria.where("companyName").is(companyName).and("schemeName").is(scheme));
        return mongoTemplate.findOne(query, RoiSchemeMaster.class);
    }
}