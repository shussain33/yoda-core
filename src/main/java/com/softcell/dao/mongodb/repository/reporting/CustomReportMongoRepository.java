package com.softcell.dao.mongodb.repository.reporting;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

/**
 * @author kishor
 */
@Repository
public class CustomReportMongoRepository implements CustomReportRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Inject
    public CustomReportMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<?> applicationReportData(Query query) {
        return mongoTemplate.find(query, GoNoGoCustomerApplication.class);
    }

    @Override
    public List<?> partialApplicationReportData(Query query) {
        return mongoTemplate.find(query, ApplicationRequest.class);
    }

    @Override
    public boolean applicationReportDataLocalCopy(Query query) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean partialApplicationReportDataLocalCopy(Query query) {
        // TODO Auto-generated method stub
        return false;
    }


}
