package com.softcell.dao.mongodb.repository;

import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.masters.AssetModelMaster;

import java.util.List;

public interface LosRepository {

    /**
     * Return supplier id based on supplier desc
     *
     * @param supplierDesc
     * @param institutionId
     * @return
     */
    String getSupplierIdFromDesc(String supplierDesc, String institutionId);

    /**
     * Return Asset Category Id based on Asset category description
     *
     * @param assetCatgDesc
     * @param institutionId
     * @return
     */
    String getAssetCatgIdbyCatgDesc(String assetCatgDesc, String institutionId);

    /**
     * Return make id based on make and manucature name when model id is blank
     *
     * @param make
     * @param manufacture
     * @param institutionId
     * @return
     */
    String getMakeIdbyMakeDesc(String make, String manufacture, String institutionId);

    /**
     * Returns state id based on state name
     *
     * @param state
     * @param institutionId
     * @return
     */
    String getStateIdByState(String state, String institutionId);

    /**
     * Return city id based on city name
     *
     * @param city
     * @param institutionId
     * @return
     */
    String getCityIdByCity(String city, String institutionId);

    /**
     * Returns credit promotion id based on scheme id
     *
     * @param scheme
     * @param institutionId
     * @return
     */
    String getCreditPromotionIdByScheme(String scheme, String institutionId);

    /**
     * Return branch id based on dealer id
     *
     * @param dealerId
     * @param productId
     * @param institutionId
     * @return
     */
    String getBranchByDealerId(String dealerId, String productId, String institutionId);

    /**
     * Returns multiple dsaid based on branch id
     *
     * @param branchId
     * @param institutionId
     * @return
     */
    List<String> getDSAByBranchId(String branchId, String institutionId);

    /**
     * Returns a dsaId based on product as we get multiple dsa id based on dsa id and branch id
     *
     * @param _dsaId
     * @param productId
     * @param institutionId
     * @return
     */
    String getDSAByProdcutId(String _dsaId, String productId, String institutionId);

    /**
     * Returns education code based on education type
     *
     * @param eduDesc
     * @return
     */
    String getEducationByEducationDesc(String eduDesc);

    /**
     * Return residence code based on residence type
     *
     * @param resDesc
     * @return
     */
    String getResidenceByResidenceDesc(String resDesc);

    /**
     * Returns employer id based on employer desc but if employer is not present
     * It will should return default value
     *
     * @param employerName
     * @param institutionId
     * @return
     */
    String getEmployerIdByName(String employerName, String institutionId);

    /**
     * Returns assetModel details for extracting asset related details
     *
     * @param make
     * @param modelNo
     * @param manufacture
     * @param category
     * @param institutionId
     * @return
     */
    AssetModelMaster getAssetDetails(String make, String modelNo,
                                     String manufacture, String category, String institutionId);

    /**
     * Returns true if dealer is specific to hyderabad branch
     * It is done for pilot period launch for los integration
     *
     * @param dealerId      dealer id for which application is punched
     * @param institutionId
     * @return
     */
    boolean isItHyderabadDealer(String dealerId, String institutionId);

    /**
     *  Returns true if dealer is allowed to push data to los,
     *  It checks dealerBranch Master dealerFlag value to validate
     * @param dealerId
     * @param institutionId
     * @return
     */
    boolean isValidDealerToPushLos(String dealerId, String institutionId);

	 /**
	  * Return serial no only if serial no is valid
	  * @param refId
	  * @return
	  */
	 String getProductSerialNo(String refId);

    /**
     *
     * @return
     */
	 long getMbPushSequenceNumber(String instituteId);

    /**
     *
     * @param refID
     * @param institutionId
     * @return
     */
    LoyaltyCardDetails getLoyaltyCardsDetails(String refID, String institutionId);

    String getBankIdFromBankName(String bankName);

    /**
     * This method will be used to fetch value from HDBFS  cgpm(Common General Parameter Master) master based on key1 and description.
     * CGPM master basically keep value code for descrition used in gonogo.
     * For example Relationship description Friend in gonogo is equivalent to F in CGPM master.
     * F will be fetched for key1 = REFRELT and description = Friend
     * @param key1 Constant key value form CGPM master
     * @param description description based on which single record need to fetch
     * @return value from cgpm base on key1 and description
     */
    String getValueFromCGPMOnKey1AndDescription(String key1, String description);

    /**
     * This will fetch state id based on branch name
     * @param branchName branch name of dsa
     * @return
     */
    String getStateIdByBranchName(String branchName);

    /**
     * Promotional scheme will be fetched based on surrogate and product name
     * @param surrogateName
     * @param  productName
     * @return
     */
    String getPromotionSchemeBySurrogateAndProduct(String surrogateName, String productName);

    /**
     *
     * @param refId
     * @return
     */
    LosMbData getLosMbData(String refId);

    /**
     *
     * @param losMbData
     */
    void saveLosMbData(LosMbData losMbData);
}
