
/**
 * bhuvneshk12:39:54 PM  Copyright Softcell Technolgy
 **/
package com.softcell.dao.mongodb.repository;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.Stages;
import com.softcell.gonogo.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.dashboard.DashboardDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.dashboard.DashboardResponse;
import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.security.v2.Hierarchy;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author bhuvneshk
 */

@Repository
public class DashboardMongoRepository implements DashboardRepository {


    private static final Logger logger = LoggerFactory.getLogger(DashboardMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public static final List<String> fosDefaultIdList = new ArrayList<String>(){{ add("Paisabazaar_FOS"); add("11173"); add("LoanCompare_FOS"); add("MYMONEYKARMA_FOS");
        add("WishFin_FOS"); add("LENDINGADDA_FOS"); add("MyMoneyMantra_fos"); add("DIGIPL_FOS"); add("CASHTAP_FOS"); add("BAJAJ_FOS");}};


    @Inject
    public DashboardMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Collection<DashboardDetails> getDashboardData(DashboardRequest dashboardRequest) throws Exception {

        logger.debug("getDashboardData repo started");

        List<DashboardDetails> detailsList = Collections.emptyList();

        String dsaId = dashboardRequest.getDsaId();

        int skip = dashboardRequest.getSkip();
        int limit = dashboardRequest.getLimit();

        // getting today's time component from date
        DateTime now = new DateTime();

        DateTime fromDate = new DateTime(dashboardRequest.getFromDate()).withHourOfDay(now.getHourOfDay())
                .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                .withZoneRetainFields(DateTimeZone.UTC);

        DateTime toDate = new DateTime(dashboardRequest.getToDate()).withHourOfDay(now.getHourOfDay())
                .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                .withZoneRetainFields(DateTimeZone.UTC);

        Query query = new Query();

        int days = Days.daysBetween(fromDate, toDate).getDays();


        try {

            logger.info("Dashboard Request 1 {}", dashboardRequest.toString());

            if (days == NumberUtils.INTEGER_ZERO) {
                query.addCriteria(Criteria.where("applicationRequest.header.dsaId").is(dsaId)
                        .and("applicationRequest.header.dateTime").is(fromDate.toDate())
                        .and("applicationRequest.header.institutionId").is(dashboardRequest.getHeader().getInstitutionId())
                        .and("applicationRequest.request.application.loanType").in(dashboardRequest.getHeader().getProduct().name()));
            } else {

                query.addCriteria(Criteria.where("applicationRequest.header.dsaId").is(dsaId)
                        .and("applicationRequest.header.dateTime").gte(fromDate.toDate()).lte(toDate.toDate())
                        .and("applicationRequest.header.institutionId").is(dashboardRequest.getHeader().getInstitutionId())
                        .and("applicationRequest.request.application.loanType").in(dashboardRequest.getHeader().getProduct().name()))
                        .skip(skip).limit(limit);
            }

            query.fields().include("applicationRequest")
                    .include("croDecisions")
                    .include("invoiceDetails")
                    .include("croJustification")
                    .include("losDetails")
                    .include("applicationStatus")
                    .include("dateTime")
                    .include("prospectNum")
                    .include("agreementNum");


            logger.info("Query for dashboard request {}", query);


            Stream<GoNoGoCustomerApplication> streamFromIterator = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, GoNoGoCustomerApplication.class));

            Set<String> refIds = new TreeSet<>();

            Stream<DashboardDetails> finalGngResponseList = streamFromIterator.map(object -> {

                GoNoGoCustomerApplication application = object;

                refIds.add(application.getApplicationRequest().getRefID());

                DashboardDetails details = new DashboardDetails();

                List<Kyc> kycList = application.getApplicationRequest().getRequest().getApplicant().getKyc();

                details.setAadhaarNumber(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));

                details.setAmountApproved(getAmountApproved(application.getCroDecisions()));

                details.setDate(application.getDateTime());

                details.setDob(application.getApplicationRequest().getRequest().getApplicant().getDateOfBirth());

                details.setApplicantType(application.getApplicationRequest().getApplicantType());

                details.setAgreementNum(application.getAgreementNum());

                details.setQdeDecision(application.getApplicationRequest().isQdeDecision());

                Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE,
                        application.getApplicationRequest().getRequest().getApplicant().getPhone()).orElse(null);

                if (phone != null)
                    details.setMobileNumber(phone.getPhoneNumber());

                details.setName(GngUtils.convertNameToFullName(application.getApplicationRequest().getRequest().getApplicant().getApplicantName()));

                details.setPAN(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), kycList, ValidationPattern.PAN));

                details.setReferenceID(application.getApplicationRequest().getRefID());

                details.setInvoiceDetails(application.getInvoiceDetails());

                if (application.getApplicationRequest().getReAppraiseDetails() != null) {
                    details.setReAppraiseCount(application
                            .getApplicationRequest().getReAppraiseDetails()
                            .getReAppraiseCount());
                }


                Query queryForPostIpa = new Query();

                queryForPostIpa.addCriteria(Criteria.where("_id").is(application.getApplicationRequest().getRefID()));

                queryForPostIpa.fields().include("postIPA.scheme").include("postIPA.financeAmount");

                PostIpaRequest postIpaRequest = mongoTemplate.findOne(queryForPostIpa, PostIpaRequest.class);


                if (postIpaRequest != null && postIpaRequest.getPostIPA() != null) {

                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getScheme())) {
                        details.setScheme(postIpaRequest.getPostIPA().getScheme());
                    }

                    details.setFinanceAmount(postIpaRequest.getPostIPA().getFinanceAmount());

                } else {

                    details.setScheme(FieldSeparator.HYPHEN);

                }

                details.setStatus(application.getApplicationStatus());

                details.setStages(application.getApplicationRequest().getCurrentStageId());

                details.setLosDetails(application.getLosDetails());

                List<CroDecision> croDecisions = application.getCroDecisions();

                if (!CollectionUtils.isEmpty(croDecisions)) {

                    Collections.sort(croDecisions, (CroDecision d1, CroDecision d2) -> d2.getDecisionUpdateDate()
                            .compareTo(d1.getDecisionUpdateDate()));
                }

                details.setCroDecisions(croDecisions);

                if (application.getCroJustification() != null && application.getCroJustification().size() > 0) {
                    details.setCroJustificationList(application.getCroJustification());
                }

                return details;

            });


            query = new Query();

            query.addCriteria(Criteria.where("header.dsaId").is(dsaId)
                    .and("_id").nin(refIds)
                    .and("currentStageId").is(GNGWorkflowConstant.DE.toFaceValue())
                    .and("header.dateTime").gte(fromDate.toDate()).lte(toDate.toDate())
                    .and("header.institutionId").is(dashboardRequest.getHeader().getInstitutionId())
                    .and("header.product").is(dashboardRequest.getHeader().getProduct().name()))
                    .skip(skip).limit(limit);

            logger.debug("getDashboardData repo formed query to fetch data from applicationRequest {}", query.toString());


            Stream<ApplicationRequest> streamFromIterator1 = StreamUtils.createStreamFromIterator(mongoTemplate.stream(query, ApplicationRequest.class));


            Stream<DashboardDetails> dashboardDetailsStream = streamFromIterator1.map(object -> {

                ApplicationRequest application = object;

                DashboardDetails details = new DashboardDetails();

                List<Kyc> kycList = application.getRequest().getApplicant().getKyc();

                details.setAadhaarNumber(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));

                details.setAmountApproved(0);

                details.setDate(application.getHeader().getDateTime());

                details.setDob(application.getRequest().getApplicant().getDateOfBirth());

                details.setApplicantType(application.getApplicantType());

                Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE, application.getRequest().getApplicant().getPhone()).orElse(null);

                if (phone != null)
                    details.setMobileNumber(phone.getPhoneNumber());


                details.setName(GngUtils.convertNameToFullName(application.getRequest().getApplicant().getApplicantName()));

                details.setPAN(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), kycList, ValidationPattern.PAN));

                details.setReferenceID(application.getRefID());

                details.setScheme(FieldSeparator.HYPHEN);

                details.setStatus(FieldSeparator.HYPHEN);

                details.setStages(application.getCurrentStageId());


                return details;

            });

            detailsList = Stream.concat(finalGngResponseList, dashboardDetailsStream)
                    .distinct()
                    .sorted(Comparator.comparing(DashboardDetails::getDate).reversed())
                    .collect(Collectors.toList());

            logger.debug("getDashboardData repo completed with record count {}", detailsList.size());

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception fetching records for dashboard details {}", e.getMessage());
            throw new Exception(String.format("Error parsing form or to date for in getDashboardDetail [{%s}]", e.getMessage()));
        }

        return detailsList;
    }


    /**
     * @param croDecisions
     * @return
     */
    private double getAmountApproved(List<CroDecision> croDecisions) {

        if (null != croDecisions && !croDecisions.isEmpty()) {

            return croDecisions.parallelStream()
                    .map(croDecision -> croDecision.getAmtApproved())
                    .findFirst()
                    .get();

        } else {
            return 0D;
        }
    }

    @Override
    public DashboardResponse getDashboardDataForQueueManagement(DashboardRequest dashboardRequest) throws Exception {

        logger.debug("getDashboardData repo started");
        DashboardResponse dashboardResponse = new DashboardResponse();
        List<DashboardDetails> detailsList = Collections.emptyList();
        long totalRecordCount = 0;
        int skip = dashboardRequest.getSkip();
        int limit = dashboardRequest.getLimit();

        // getting today's time component from date
        DateTime now = new DateTime();

        DateTime fromDate = new DateTime(dashboardRequest.getFromDate()).withHourOfDay(now.getHourOfDay())
                .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                .withZoneRetainFields(DateTimeZone.UTC);

        DateTime toDate = new DateTime(dashboardRequest.getToDate()).withHourOfDay(now.getHourOfDay())
                .withMinuteOfHour(now.getMinuteOfHour()).withSecondOfMinute(now.getSecondOfMinute())
                .withZoneRetainFields(DateTimeZone.UTC);

        Query query = new Query();

        int days = Days.daysBetween(fromDate, toDate).getDays();

        try {
            logger.info("Dashboard Request 1 {}", dashboardRequest.toString());
            // @Amit new parameters passed through request
            String userId = dashboardRequest.getHeader().getLoggedInUserId();
            //@Amit No need as only FOS is going to access the dashboard
            String role = dashboardRequest.getHeader().getLoggedInUserRole();
            //for credit role hierarchy user can select to see only records recommended to him/ deviation records
            boolean showMyRecords = dashboardRequest.isShowMyRecords();
            // get all filters applied on dashboard
            Map<String, String> filterMap = dashboardRequest.getFilters();

            if(dashboardRequest.getHeader().getCredit() != null){
                if(dashboardRequest.getHeader().getCredit()) {
                    role = Roles.Role.CREDIT.name();
                }
            }

            Criteria criteria = new Criteria();
            // Add loggedin role specific criteria in query
            addRoleSpecificCriteria(userId, role, criteria, dashboardRequest.getHeader(), showMyRecords);

            // Add filter criteria
            if(!CollectionUtils.isEmpty(filterMap)){
                addFilterSpecificCriteria(criteria, filterMap);
            }

            Map<String, String> appIdStatusMap = null;
            if(Cache.VERIFICATION_ROLE_URL_MAP.containsKey(role)) {
                appIdStatusMap = addCriteraForVerifierAndGetData(criteria, dashboardRequest.getHeader(), role, showMyRecords);
            }
                criteria.and("applicationRequest.header.institutionId").is(dashboardRequest.getHeader().getInstitutionId())
                    .and("applicationRequest.request.application.loanType").in(dashboardRequest.getHeader().getProduct().name());

            /*
            *   Hierarchy criteria creation
             */
            RequestCriteria requestCriteria = dashboardRequest.getCriteria();
            boolean defaultFilter = (requestCriteria.getHierarchy() == null);
            if(defaultFilter){
                criteria.and("applicationRequest.appMetaData.branchV2.branchId").in(requestCriteria.getBranches());
            } else{
                Hierarchy hierarchy = requestCriteria.getHierarchy();
                MongoRepositoryUtil.getCriteriaMapping(hierarchy, criteria, mongoTemplate);
            }

            if(!CollectionUtils.isEmpty(filterMap) && !filterMap.containsKey("dateTime")) {
                if (days == NumberUtils.INTEGER_ZERO) {
                    criteria.and("applicationRequest.header.dateTime").is(fromDate.toDate());
                } else {
                    criteria.and("applicationRequest.header.dateTime").gte(fromDate.toDate()).lte(toDate.toDate());
                }
            }
            query.addCriteria(criteria);
            query.with(new Sort(Sort.Direction.DESC, "applicationRequest.header.dateTime"));

            query.fields().include("applicationRequest")
                    .include("croDecisions")
                    .include("invoiceDetails")
                    .include("croJustification")
                    .include("losDetails")
                    .include("applicationStatus")
                    .include("applicationBucket")
                    .include("dateTime")
                    .include("prospectNum")
                    .include("agreementNum")
                    .include("intrimStatus")
                    .include("allocationInfo");

            logger.info("Query for dashboard by userId {} of role {} and request {}", userId, role, query);

            List<GoNoGoCustomerApplication> tempList;
            totalRecordCount = mongoTemplate.count(query, GoNoGoCustomerApplication.class);
            query.skip(skip).limit(limit);
            List<GoNoGoCustomerApplication> applications = mongoTemplate.find(query, GoNoGoCustomerApplication.class);

            List<DashboardDetails> finalGngResponseList = buildDashboardDetail(applications, appIdStatusMap);

            detailsList = finalGngResponseList.stream().sorted(Comparator.comparing(DashboardDetails::getDate).reversed())
                    .collect(Collectors.toList());

            logger.debug("getDashboardData repo completed with record count {}", detailsList.size());

        } catch (DataAccessException e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception fetching records for dashboard details {}", e.getMessage());
            throw new Exception(String.format("Error parsing form or to date for in getDashboardDetail [{%s}]", e.getMessage()));
        }

        dashboardResponse.setDashboardDetailsList(detailsList);
        dashboardResponse.setRecordCount(totalRecordCount);

        return dashboardResponse;
    }

    private Map<String,String> addCriteraForVerifierAndGetData(Criteria criteria, Header header, String role, boolean showMyRecords) {
        // Check Role of user is verifier, if yse then add criteria for verifier
        Map<String, String> appIdStatusMap = createRefIdsForVerifier(role, header, showMyRecords);
        criteria.and("_id").in(appIdStatusMap.keySet());
        return appIdStatusMap;
    }

    private void addFilterSpecificCriteria(Criteria criteria, Map<String, String> filterMap) {
        List<String> combineFilterKeys = new ArrayList<>();
        combineFilterKeys.add("customerName");
        combineFilterKeys.add("bucket");
        List<String> dateFilterKeys = new ArrayList<>();
        dateFilterKeys.add("dateTime");
        List<String> numericFilterKeys = new ArrayList<>();
        numericFilterKeys.add("loanAmt");
        List<Criteria> criteriaList = new ArrayList<>();
        int criIndex = 0;
        for(Map.Entry<String, String> entry: filterMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String fieldMapping = Cache.CRITERIA_FIELD_MAPPING_MAP.get(key);
            if(StringUtils.isNotEmpty(fieldMapping)){
                if(combineFilterKeys.contains(key)){
                    if(StringUtils.equalsIgnoreCase(key, "customerName")){
                        Criteria searchCriteria = new Criteria();
                        String[] searchedWords = value.split(" ");

                        for (int i = 0; i < searchedWords.length; i++, criIndex++) {
                            String word = searchedWords[i];
                            Criteria orCriteria1 = new Criteria();
                            orCriteria1.orOperator(Criteria.where("applicationRequest.request.applicant.applicantName.firstName").regex(word, "i"),
                                    Criteria.where("applicationRequest.request.applicant.applicantName.middleName").regex(word, "i"),
                                    Criteria.where("applicationRequest.request.applicant.applicantName.lastName").regex(word, "i"));
                            criteriaList.add(orCriteria1);
                        }
                    }else if(StringUtils.equalsIgnoreCase(key, "bucket")){
                        criteriaList.add(Criteria.where(fieldMapping).is(value));
                    }
                }else if(dateFilterKeys.contains(key)){
                    Date date = new Date(value);
                    Date start = DateUtils.getStartOfDay(date);
                    Date end = DateUtils.getEndOfDay(date);
                    criteriaList.add(Criteria.where(fieldMapping).gte(start).lte(end));
                }else if(numericFilterKeys.contains(key)){
                    criteriaList.add(Criteria.where(fieldMapping).is(Double.parseDouble(value)));
                } else {
                    criteriaList.add((Criteria.where(fieldMapping).regex(value, "i")));
                }
            }
        }
        Criteria[] criteriaArray = criteriaList.toArray(new Criteria[criteriaList.size()]);
        try{
            criteria.andOperator(criteriaArray);
        } catch(Exception e){
            logger.debug("Exception occurred while adding filter criteria !" + e.getMessage());
        }
    }


    private void addRoleSpecificCriteria(String userId, String role, Criteria criteria, Header header, boolean showMyRecords) {

        // Role wise query
        if (Roles.isCreditRole(role)) {
            if (showMyRecords) {
                // add stage check and also allocation check
                List<String> creditStages = Stages.getCreditStages();
                Criteria recommendCriteria = createCriteriaForRecommandedRecords(userId, creditStages);

                List<String> refIds = getRefIdsBasedOnDeviations(header.getInstitutionId(), role);
                Criteria deviationCriteria = null;
                if (!CollectionUtils.isEmpty(refIds)) {
                    deviationCriteria = Criteria.where("_id").in(refIds);
                    criteria.orOperator(recommendCriteria, deviationCriteria);
                } else {
                    criteria.andOperator(recommendCriteria);
                }
            } else {
                /* Do nothing as
                 The query for general view has been already cerated, no need to create any here. */
            }
        } else {
            if (StringUtils.equalsIgnoreCase(Roles.Role.FOS.name(),role)) {
                //remove hardcoded paisabazar role check
                criteria.orOperator(Criteria.where("applicationRequest.header.dsaId").is(userId),
                        Criteria.where("applicationRequest.header.dsaId").in(fosDefaultIdList)) ;
            }
            if (showMyRecords) {
                if (Roles.isOpsRole(role)) {
                    List<String> opsStages = new ArrayList<>();
                    opsStages.addAll(Stages.getOpsStages());
                    Criteria recommendCriteria = createCriteriaForRecommandedRecords(userId, opsStages);
                    criteria.orOperator(Criteria.where("applicationRequest.currentStageId").
                            in(Cache.ROLE_ENABLED_STAGES_MAP.get(role)), recommendCriteria);
                } else if(Roles.isCoOriginationRole(role)){
                    Criteria stageCriteria = new Criteria();
                    stageCriteria.and("applicationRequest.currentStageId").in(Cache.ROLE_ENABLED_STAGES_MAP.get(role));
                    criteria.andOperator(Criteria.where("applicationRequest.request.application.coOrigination").is(true)
                            , stageCriteria);
                } else {
                    criteria.and("applicationRequest.currentStageId").in(Cache.ROLE_ENABLED_STAGES_MAP.get(role) != null ? Cache.ROLE_ENABLED_STAGES_MAP.get(role) : new ArrayList<>());
                }
            }
        }
    }

    private List<String> getRefIdsBasedOnDeviations(String institutionId, String role) {
        List<String> refIds = new ArrayList<>();
        Query query = new Query();
        Criteria orCriteria = new Criteria();
        orCriteria.orOperator(
                Criteria.where("deviationList.authority").is(role), Criteria.where("deviationList.authorities").is(role));
        Criteria criteria = Criteria.where("institutionId").is(institutionId).andOperator(orCriteria);
        query.addCriteria(criteria);
        query.fields().include("_id");
        List<DeviationDetails> deviationDetailsList = mongoTemplate.find(query, DeviationDetails.class);

        deviationDetailsList.forEach(deviationDetail -> {
            refIds.add(deviationDetail.getRefId());
        });
        return refIds;
    }



    private Criteria createCriteriaForRecommandedRecords(String userId, List<String> stages) {
        Criteria recommendCriteria = new Criteria();
        Criteria roleCriteria = new Criteria();
        roleCriteria.orOperator(Criteria.where("allocationInfo.operator").is(userId),
                Criteria.where("allocationInfo.assignedBy").is(userId));
        Criteria stageCriteria = Criteria.where("applicationRequest.currentStageId").in(stages);
        recommendCriteria.andOperator(roleCriteria, stageCriteria);
        return recommendCriteria;
    }

    private Map<String, String> createRefIdsForVerifier(String role, Header header, boolean showMyRecords) {
        List<Object> tempList = Cache.VERIFICATION_ROLE_URL_MAP.get(role);
        Class<?>  cls = (Class)tempList.get(0);

        List<String> verificationStatusList = new ArrayList<>();
        verificationStatusList.add(ThirdPartyVerification.Status.SUBMITTED.name());
        if(!showMyRecords){
            verificationStatusList.add(ThirdPartyVerification.Status.VERIFIED.name());
        }

        Criteria criteria = new Criteria();
        MongoRepositoryUtil.createCriteriaForVerifierRole(header, criteria, role, verificationStatusList);

        Query query = new Query().addCriteria(criteria);
        logger.info("Query for verifier request {}", query);List lObj  = mongoTemplate.find(query, cls);

        Map<String, String> refIds = new HashMap<>();
        String agencyCode = header.getDealerId();
        lObj.forEach(dbObj -> {

            if(dbObj instanceof VerificationDetails){
                lObj.forEach( obj -> {
                    VerificationDetails verificationDetails = (VerificationDetails)obj;
                    String id = verificationDetails.getRefId();
                    List<String> statusList = getStatusListForVerificationDetails(verificationDetails, role, agencyCode, header);
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }else if(dbObj instanceof LegalVerification){
                lObj.forEach( obj -> {
                    LegalVerification legalVerification = (LegalVerification)obj;
                    String id = legalVerification.getRefId();
                    List<String> statusList = new ArrayList<>();
                    //for multiple legalverification
                    /*statusList.add(legalVerification.getStatus());*/
                    legalVerification.getLegalVerificationDetailsList().forEach(legalVerificationDetails -> {
                        if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                                && StringUtils.isNotEmpty(legalVerificationDetails.getAgencyCode()) && header.getDealerIds().contains(legalVerificationDetails.getAgencyCode())){
                            statusList.add(legalVerificationDetails.getStatus());
                        }else if(StringUtils.equals(agencyCode, legalVerificationDetails.getAgencyCode())){
                            statusList.add(legalVerificationDetails.getStatus());
                        }
                    });
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }else if(dbObj instanceof Valuation){
                lObj.forEach( obj -> {
                    Valuation valuation = (Valuation)obj;
                    String id = valuation.getRefId();
                    List<String> statusList = new ArrayList<>();
                    valuation.getValuationDetailsList().forEach(valuationDetails -> {
                        if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                                && StringUtils.isNotEmpty(valuationDetails.getAgencyCode()) && header.getDealerIds().contains(valuationDetails.getAgencyCode())){
                            statusList.add(valuationDetails.getStatus());
                        }else if(StringUtils.equals(agencyCode, valuationDetails.getAgencyCode())){
                            statusList.add(valuationDetails.getStatus());
                        }
                    });
                    String status = getStatusForVerifierDashBoard(statusList);
                    refIds.put(id, status);
                });
            }
        });
        return refIds;
    }

    private List<String> getStatusListForVerificationDetails(VerificationDetails verificationDetails, String role, String agencyCode, Header header) {
        List<String> statusList = new ArrayList<>();
        if(role.equals(Roles.Role.RESI_VERIFIER.name())){
            verificationDetails.getResidenceVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.OFC_VERIFIER.name())){
            verificationDetails.getOfficeVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.BANK_VERIFIER.name())){
            verificationDetails.getBankingVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        } else if(role.equals(Roles.Role.ITR_VERIFIER.name())){
            verificationDetails.getItrVerification().forEach( tempObj -> {
                if(!(CollectionUtils.isEmpty(header.getDealerIds())) && Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)
                        && StringUtils.isNotEmpty(tempObj.getAgencyCode()) && header.getDealerIds().contains(tempObj.getAgencyCode())){
                    statusList.add(tempObj.getStatus());
                }else if(StringUtils.equals(tempObj.getAgencyCode(), agencyCode)){
                    statusList.add(tempObj.getStatus());
                }
            });
        }
        return statusList;
    }

    private String getStatusForVerifierDashBoard(List<String> status) {
        if(status.contains(ThirdPartyVerification.Status.PENDING.name()) || status.contains(ThirdPartyVerification.Status.SUBMITTED.name())){
            return Status.VerifierStatus.WIP.name();
        } else{
            return Status.VerifierStatus.Verified.name();
        }
    }

    private List<DashboardDetails> buildDashboardDetail(List<GoNoGoCustomerApplication> applications, Map<String, String> appIdStatusMap) {
        Set<String> refIds = new TreeSet<>();
        List<DashboardDetails> details = new ArrayList<>();

        for(GoNoGoCustomerApplication application : applications) {
            // Dashboard list detail building shouldn't be halted if there is error in a single record
            // So try-catch required for single record iteration
          try {

                refIds.add(application.getApplicationRequest().getRefID());

                DashboardDetails detail = new DashboardDetails();
                details.add(detail);

                if (application.getApplicationRequest().getRequest().getApplicant() != null) {

                    detail.setName(GngUtils.convertNameToFullName(application.getApplicationRequest().getRequest().getApplicant().getApplicantName()));
                    detail.setDob(application.getApplicationRequest().getRequest().getApplicant().getDateOfBirth());

                    Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE,
                            application.getApplicationRequest().getRequest().getApplicant().getPhone()).orElse(null);
                    if (phone != null)
                        detail.setMobileNumber(phone.getPhoneNumber());

                    List<Kyc> kycList = application.getApplicationRequest().getRequest().getApplicant().getKyc();
                    if (kycList != null) {
                        detail.setAadhaarNumber(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));
                        detail.setPAN(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), kycList, ValidationPattern.PAN));
                    }
                }

                // Set up loan amounts
                //      Applied loan amount
                detail.setAmountApplied(application.getApplicationRequest().getRequest().getApplication().getLoanAmount() );
                //      Approved loan amount
                if( application.getCroDecisions() != null ) {
                    detail.setAmountApproved(getAmountApproved(application.getCroDecisions()));
                }

                detail.setDate(application.getDateTime());
                detail.setAgreementNum(application.getAgreementNum());
                //if loggedIn user is verifier then show virifier specific(configurable) status
                String status;
                if(appIdStatusMap != null && appIdStatusMap.containsKey(application.getGngRefId())){
                    status = appIdStatusMap.get(application.getGngRefId());
                } else{
                    status = application.getApplicationStatus();
                }
                detail.setStatus(status);
                detail.setBucket(application.getApplicationBucket());
                detail.setStages(application.getApplicationRequest().getCurrentStageId());

                detail.setReferenceID(application.getApplicationRequest().getRefID());
                detail.setApplicantType(application.getApplicationRequest().getApplicantType());
                detail.setQdeDecision(application.getApplicationRequest().isQdeDecision());

                detail.setInvoiceDetails(application.getInvoiceDetails());

                if (application.getApplicationRequest().getReAppraiseDetails() != null) {
                    detail.setReAppraiseCount(application
                            .getApplicationRequest().getReAppraiseDetails()
                            .getReAppraiseCount());//
                }

                Query queryForPostIpa = new Query();
                queryForPostIpa.addCriteria(Criteria.where("_id").is(application.getApplicationRequest().getRefID()));
                queryForPostIpa.fields().include("postIPA.scheme").include("postIPA.financeAmount");

                PostIpaRequest postIpaRequest = mongoTemplate.findOne(queryForPostIpa, PostIpaRequest.class);
                if (postIpaRequest != null && postIpaRequest.getPostIPA() != null) {
                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getScheme())) {
                        detail.setScheme(postIpaRequest.getPostIPA().getScheme());
                    }

                    detail.setFinanceAmount(postIpaRequest.getPostIPA().getFinanceAmount());//
                } else {
                    detail.setScheme(FieldSeparator.HYPHEN);
                }

                detail.setLosDetails(application.getLosDetails());

                List<CroDecision> croDecisions = application.getCroDecisions();
                if (!CollectionUtils.isEmpty(croDecisions)) {
                    Collections.sort(croDecisions, (CroDecision d1, CroDecision d2) -> d2.getDecisionUpdateDate()
                            .compareTo(d1.getDecisionUpdateDate()));
                }

                detail.setCroDecisions(croDecisions);

                // Set fields from AppMetadata
                detail.setBranchName(application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
                detail.setDsaName(GngUtils.convertNameToFullName(application.getApplicationRequest().getAppMetaData().getDsaName()));

                /**
                 * @Amit commented  with new requirenment
                 * Changes for CPA login
                 */
                /*details.setBureauScore(application.getIntrimStatus().getCibilModuleResult().getFieldValue());

                details.setAppScore(application.getIntrimStatus().getScoringModuleResult().getFieldValue());

                String isStp = (application.getApplicationRequest().getHeader().getCroId() == "STP")? "Y" : "N";

                details.setIsStp(isStp);
                //TODO Amit Take proper requirement for DealerName
                details.setDealerName("");

                String dsaName = application.getApplicationRequest().getAppMetaData().getDsaName().getFirstName() +
                        " " + application.getApplicationRequest().getAppMetaData().getDsaName().getLastName();
                details.setDsaName(dsaName);

                details.setBranchName(application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());*/

                if (application.getCroJustification() != null && application.getCroJustification().size() > 0) {
                    detail.setCroJustificationList(application.getCroJustification());
                }

                //set allocation status
                if(application.getAllocationInfo() != null){
                    detail.setCurrentUser(Cache.getUserName(application.getApplicationRequest().getHeader().getInstitutionId(),
                            application.getAllocationInfo().getOperator()));
                    detail.setAllocationInfo(application.getAllocationInfo());
                }

                if(application.getApplicationRequest() != null && application.getApplicationRequest().getRequest() != null &&
                        application.getApplicationRequest().getRequest().getApplication() != null && StringUtils.isNotEmpty(application.getApplicationRequest().getRequest().getApplication().getPromoCode())) {
                    detail.setPromocode(application.getApplicationRequest().getRequest().getApplication().getPromoCode());
                }


            } catch (Exception e ) {
                logger.error("Exception while setting dashboard data {}: \nfor refid {} :application : {}",
                        e, application.getApplicationRequest().getRefID(), application);
            }
        }
        logger.debug("Fetched from db : count {}; Dashboard list items count {}", applications.size() , details.size());
        return details;
    }
}