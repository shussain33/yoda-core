package com.softcell.dao.mongodb.repository.modulelog;


import com.mongodb.BasicDBObject;
import com.softcell.aop.Retry;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.PanServiceResponse;
import com.softcell.gonogo.model.core.request.posidex.PosidexRequestLog;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequestV2;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.CommercialRequestResponseDomain;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.constants.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yogeshb
 */
@Repository
public class ModuleRequestMongoRepository implements ModuleRequestRepository {

    private static final Logger logger = LoggerFactory.getLogger(ModuleRequestMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    //TODO remove after spinning new workflow
    public ModuleRequestMongoRepository(MongoTemplate mongoTemplate) {
        if (this.mongoTemplate == null) {
            this.mongoTemplate = mongoTemplate;
        }
    }

    @Override
    public boolean saveAadharRequest(final AadhaarRequest aadhaarRequest) {
        try {

            BasicDBObject dbObject = new BasicDBObject();

            mongoTemplate.getConverter().write(aadhaarRequest, dbObject);

            mongoTemplate.execute(AadhaarRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(aadhaarRequest.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return true;
            });
            return true;

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while saving Aadhaar request with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    public boolean partialApplicationDbSave(final GoNoGoCustomerApplication goNoGoCustomerApplication) {
        try {
            logger.debug("partialApplicationDbSave for refId {}", goNoGoCustomerApplication.getApplicationRequest().getRefID());
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(goNoGoCustomerApplication, dbObject);
            mongoTemplate.execute(GoNoGoCustomerApplication.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                logger.debug("partialApplicationDbSave SAVED for refId {}", goNoGoCustomerApplication.getApplicationRequest().getRefID());

                return true;
            });

            return true;

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(" Error occurred while saving partial application with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override

    public boolean savePanRequest(final PanRequest panRequest) {
        try {


            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(panRequest, dbObject);
            mongoTemplate.execute(PanRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(panRequest.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return true;
            });
            return true;

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while saving PAN request with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    public boolean saveScoringRequest(final ScoringApplicationRequest scoringApplicationRequest) {
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(scoringApplicationRequest, dbObject);
            mongoTemplate.execute(ScoringApplicationRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(scoringApplicationRequest.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return true;
            });
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error occurred while saving scoring request with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    @Retry(times = 5, on = org.springframework.dao.OptimisticLockingFailureException.class)
    public boolean saveScoringApplicantRequest(final ScoringApplicantRequest scoringApplicantRequest) {
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(scoringApplicantRequest, dbObject);
            mongoTemplate.execute(ScoringApplicantRequest.class, collection -> {
                collection.update(new Query(Criteria.where("_id").is(scoringApplicantRequest.getGngRefId())).getQueryObject(),
                        dbObject,
                        true,  // means upsert - true
                        false  // multi update – false
                );
                return true;
            });
            return true;
        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while saving scoring request with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    public boolean saveScoringApplicantRequestV2(final ScoringApplicantRequestV2 scoringApplicantRequest) {
        try {
            BasicDBObject dbObject = new BasicDBObject();
            mongoTemplate.getConverter().write(scoringApplicantRequest, dbObject);
            mongoTemplate.execute(ScoringApplicantRequestV2.class, collection -> {
                if(StringUtils.isNotEmpty(scoringApplicantRequest.getPolicyName()) &&
                        !scoringApplicantRequest.getPolicyName().equalsIgnoreCase(Constant.DEFAULT))
                    collection.update(new Query(Criteria.where("gngRefId").is(scoringApplicantRequest.getGngRefId())
                                    .and("policyName").is(scoringApplicantRequest.getPolicyName()))
                                    .getQueryObject(),
                            dbObject,
                            true,  // means upsert - true
                            false  // multi update – false
                    );
                else
                    collection.update(new Query(Criteria.where("gngRefId").is(scoringApplicantRequest.getGngRefId())).getQueryObject(),
                            dbObject,
                            true,  // means upsert - true
                            false  // multi update – false
                    );
                return true;
            });
            return true;
        } catch (Exception e) {
          //  e.printStackTrace();
            logger.error("Error occurred while saving scoring request with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    public boolean saveBureauRequest(final RequestJsonDomain requestJsonDomain) {

        logger.debug(" saving multi bureau initial request  {} ", requestJsonDomain);

        try {

            mongoTemplate.insert(requestJsonDomain);
            return true;

        } catch (Exception e) {
          //  e.printStackTrace();
            logger.error("Error occurred while saving multi bureau request with cause [{}]", e.getMessage());
            return false;
        }
    }

    @Override
    public boolean saveCoApplicantMBResponse(final MultiBureauCoApplicantResponse coApplicantResponse) {

        try {

            mongoTemplate.insert(coApplicantResponse);

            return true;

        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while saving multi bureau co-applicant response  with cause [{}]", e.getMessage());
            return false;
        }
    }

    public List<MultiBureauCoApplicantResponse> getCoApplicantResponseByAppRefId(String refId) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where("applicantRefId").is(refId));

            return mongoTemplate.find(query, MultiBureauCoApplicantResponse.class);

        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while fetching Co-Applicant response by appRefId with cause [{}] ", e.getMessage());
            return null;
        }
    }

    @Override
    public boolean savePanResponse(final PanServiceResponse panServiceResponse) {
        try {

            mongoTemplate.insert(panServiceResponse);

            return true;
        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while saving PAN response with cause [{}] ", e.getMessage());
            return false;
        }
    }

    @Override
    public void savePosidexRequest(final PosidexRequestLog posidexRequestLog) {
        try {
            mongoTemplate.insert(posidexRequestLog);
        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while saving Posidex request with cause [{}] ", e.getMessage());
        }
    }

    @Override
    public boolean saveComercialBureauRequest(CommercialRequestResponseDomain commercialRequestResponseDomain) {
        logger.debug(" saving multi bureau initial request  {} ", commercialRequestResponseDomain);
        try {
            mongoTemplate.insert(commercialRequestResponseDomain);
            return true;
        } catch (Exception e) {
           // e.printStackTrace();
            logger.error("Error occurred while saving multi bureau request with cause [{}]", e.getMessage());
            return false;
        }
    }
}
