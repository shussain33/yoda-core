package com.softcell.dao.mongodb.repository;

import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by AmitBotre on 30/12/19.
 */
@Repository
public class DashboardDataMongoRepository implements DashboardDataRepository {

    private static final Logger logger = LoggerFactory.getLogger(DashboardMongoRepository.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public DashboardDataMongoRepository() {
        if(mongoTemplate == null)
            mongoTemplate = MongoConfig.getMongoTemplate();
    }
    @Inject
    public DashboardDataMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public long fetchDashboardRecordCount(Criteria criteria) {
        logger.info("Count Query started ");
        StopWatch queryTimer = new StopWatch();
        queryTimer.start();
        Query query = new Query(criteria);
        long count = mongoTemplate.count(query, GoNoGoCustomerApplication.class);
        queryTimer.stop();
        logger.info("Count query ended in ms : " + queryTimer.getLastTaskTimeMillis());
        return count;
    }

    @Override
    public List<GoNoGoCustomerApplication> fetchDashboardRecords(Criteria criteria, int skip, int limit) {
        StopWatch queryTimer = new StopWatch();
        queryTimer.start();
        Query query = new Query(criteria);
        query.with(new Sort(Sort.Direction.DESC, "applicationRequest.header.dateTime"));
        query.skip(skip).limit(limit);

        query.fields().include("applicationRequest")
                .include("croDecisions")
                .include("invoiceDetails")
                .include("croJustification")
                .include("losDetails")
                .include("applicationStatus")
                .include("applicationBucket")
                .include("dateTime")
                .include("prospectNum")
                .include("agreementNum")
                .include("intrimStatus")
                .include("allocationInfo");

        List<GoNoGoCustomerApplication> list = mongoTemplate.find(query, GoNoGoCustomerApplication.class);
        queryTimer.stop();
        logger.info("GoNoGoCustomerApplication fetching executed in ms : " + queryTimer.getLastTaskTimeMillis());
        return list;
    }
}
