package com.softcell.dao.mongodb.repository.workflow;


import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.PlPincodeEmailMaster;

import java.util.List;

/**
 * This Repository is used for getting database resources when unable to find @Autowired
 * Resources in workFlow Execution.
 *
 * @author yogeshb
 */
public interface WorkFlowRepository {
    /**
     * @param pincode
     * @return
     */
    public List<PlPincodeEmailMaster> getPincodeEmailMaster(String pincode);

    /**
     * @param activityLogs
     * @return
     */
    public boolean saveActivityLog(ActivityLogs activityLogs);

    /**
     * @param emailTemplateDetails
     * @return
     */
    public boolean saveMailLog(EmailTemplateDetails emailTemplateDetails);
}
