package com.softcell.dao.mongodb.helper;

import com.google.common.base.CharMatcher;
import com.mongodb.BasicDBObject;
import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.constants.Product;
import com.softcell.constants.SourcingEnum;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.HierarchyRepository;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.masters.CommonGeneralParameterMaster;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.security.v2.Hierarchy;
import com.softcell.reporting.request.ReportRequest;
import com.softcell.reporting.request.ReportSearchRequest;
import com.softcell.reporting.spring.data.CustomProjectionOperation;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.regex.Pattern;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;


/**
 * @author kishor
 */
@Component
public class QueryBuilder {


    private static final Logger logger = LoggerFactory.getLogger(QueryBuilder.class);
    @Autowired
    public HierarchyRepository hierarchyRepository;

    private static final CharSequence SPECIAL_CHAR = "\n\t";

    /**
     * @param institutionID
     * @param fromDate
     * @param toDate
     * @param loanType
     * @return This method build Query using given arguments.
     */
    public static Query buildGoNoGoApplicationQuery(String institutionID,
                                                    Date fromDate, Date toDate, String loanType) {
        Query query = new Query();
        query.addCriteria(Criteria.where("applicationRequest.header.dateTime")
                .gte(fromDate).lte(toDate)
                .and("applicationRequest.header.institutionId")
                .is(institutionID)
                .and("applicationRequest.request.application.loanType")
                .is(loanType));
        return query;
    }

    /**
     * @param institutionID is used to filter data of particular member.
     * @param fromDate      to select date from where you need the data
     * @param toDate        upto which date you need data
     * @param loanType      loan type
     * @return query for application request.
     */
    public static Query applicationRequestQuery(String institutionID,
                                                Date fromDate, Date toDate, String loanType) {
        Query query = new Query();
        query.addCriteria(Criteria.where("header.dateTime").gte(fromDate)
                .lte(toDate).and("header.institutionId").is(institutionID)
                .and("request.application.loanType").is(loanType)
                .and("currentStageId").is("DE"));
        return query;
    }

    /**
     * It build query based on criteria to fetch pin code from pincode master
     *
     * @param criteria RequestCriteria is to get requested queue based on criteria
     *                 provided in user profile
     * @return
     */
    public static BasicDBObject buildQueryOnHierarchyCriteria(
            RequestCriteria criteria) {
        String branch = "branchCode";
        BasicDBObject query = null;
        List<String> branchList = criteria.getBranches();
        if (branchList != null && !branchList.isEmpty()) {
            query = new BasicDBObject();
            query.put(branch, new BasicDBObject("$in", criteria.getBranches()));
        }
        return query;
    }

    /**
     * It build query based on criteria to fetch pin code from pl pincode master
     *
     * @param hierarchy
     * @return
     */
    @Deprecated
    public static BasicDBObject buildQueryOnHierarchyCriteriaPL(
            com.softcell.gonogo.model.security.v2.Hierarchy hierarchy) {
        String branch = hierarchy.getHierarchyLevel();
        BasicDBObject query = new BasicDBObject();
        List<String> hierarchyValList = hierarchy.getHierarchyValue();
        if (hierarchyValList != null && hierarchyValList.size() > 0) {
            query.put(branch, new BasicDBObject("$in", hierarchyValList));
        }
        return query;
    }

    /**
     * It build query based on criteria to fetch pin code from cdl dealer master
     *
     * @param hierarchy
     * @return
     */
    @Deprecated
    public static BasicDBObject buildQueryOnHierarchyCriteriaCDL(
            Hierarchy hierarchy) {
        String branch = hierarchy.getHierarchyLevel();
        BasicDBObject query = new BasicDBObject();
        List<String> hierarchyValList = hierarchy.getHierarchyValue();
        if (hierarchyValList != null && hierarchyValList.size() > 0) {
            query.put(branch, new BasicDBObject("$in", hierarchyValList));
        }
        return query;
    }

    /**
     * @param query       Query is the mongo query on that need to do modify based on
     *                    Queue criteria
     * @param pincodeList
     * @param productList Pincode list received based on Queue criteria
     */
    public static void buildQueryforCroQueue(Query query,
                                             List<Integer> pincodeList, Collection<String> productList,
                                             List<String> dealerList) {
        if (pincodeList != null && !pincodeList.isEmpty()) {
            query.addCriteria(Criteria.where(
                    "applicationRequest.request.applicant.address").elemMatch(
                    Criteria.where("addressType").is("RESIDENCE").and("pin")
                            .in(pincodeList)));
        }
        if (productList != null && !productList.isEmpty()) {
            query.addCriteria(Criteria.where(
                    "applicationRequest.request.application.loanType").in(
                    productList));
        }
        if (dealerList != null && !dealerList.isEmpty()) {
            query.addCriteria(Criteria.where(
                    "applicationRequest.header.dealerId").in(dealerList));
        }

    }

    /**
     * Build stack graph query based on hierarchy
     *
     * @param pincodeList
     * @param productList
     * @param dealerList
     * @return
     */
    @Deprecated
    public static Criteria buildQueryforAnalyticGraphHierarchy(
            List<Integer> pincodeList, Collection<String> productList,
            List<String> dealerList) {

        Criteria criteriaPin = null;
        Criteria criteriaProd = null;
        Criteria criteriaDlr = null;

        if (pincodeList != null && !pincodeList.isEmpty()) {
            criteriaPin = Criteria.where(
                    "applicationRequest.request.applicant.address").elemMatch(
                    Criteria.where("addressType").is("RESIDENCE").and("pin")
                            .in(pincodeList));
        }

        if (productList != null && !productList.isEmpty()) {
            criteriaProd = Criteria.where(
                    "applicationRequest.request.application.loanType").in(
                    productList);
        }

        if (dealerList != null && !dealerList.isEmpty()) {
            criteriaDlr = Criteria.where("applicationRequest.header.dealerId")
                    .in(dealerList);
        }

        if (criteriaPin != null) {
            if (criteriaProd != null) {
                criteriaPin.and(
                        "applicationRequest.request.application.loanType").in(
                        productList);
                if (criteriaDlr != null) {
                    criteriaPin.and("applicationRequest.header.dealerId").in(
                            dealerList);
                }

            } else if (criteriaDlr != null) {

                criteriaPin.and("applicationRequest.header.dealerId").in(
                        dealerList);

            }

            return criteriaPin;

        } else if (criteriaProd != null) {
            if (criteriaDlr != null) {
                criteriaProd.and("applicationRequest.header.dealerId").in(
                        dealerList);
            }
            return criteriaProd;
        } else {
            return criteriaDlr;
        }

    }

    /**
     * It bulid query based on queue criteria from request document
     *
     * @param query       Query is the mongo query on that need to do modify based on
     *                    Queue criteria
     * @param pincodeList Pincode list recived based on Queue criteria
     * @param productList product list recived based on Queue criteria
     * @param dealerList  dealer list recived based on Queue criteria
     */
    public static void buildQueryforCroQueueRequestDocument(Query query,
                                                            List<Integer> pincodeList, Collection<String> productList,
                                                            List<String> dealerList) {

        if (pincodeList != null && !pincodeList.isEmpty()) {
            query.addCriteria(Criteria.where("request.applicant.address")
                    .elemMatch(
                            Criteria.where("addressType").is("RESIDENCE")
                                    .and("pin").in(pincodeList)));
        }

        if (productList != null && !productList.isEmpty()) {
            query.addCriteria(Criteria.where("request.application.loanType")
                    .in(productList));
        }

        if (dealerList != null && !dealerList.isEmpty()) {
            query.addCriteria(Criteria.where("header.dealerId").in(dealerList));
        }
    }

    /**
     * It bulid query based on queue criteria
     *
     * @param matchField
     * @param pincodeList Pincode list received based on Queue criteria
     */
    public static void buildQueryforCroQueue(BasicDBObject matchField,
                                             List<Integer> pincodeList) {
        matchField.append("applicationRequest.request.applicant.address.pin",
                new BasicDBObject("$in", pincodeList)).append(
                "applicationRequest.request.applicant.address.addressType",
                new BasicDBObject("$eq", "RESIDENCE"));
    }

    /**
     * @param from          Date which will used report start date.
     * @param to            end to select report.
     * @param institutionId Institution id is compulsory which used to filter data among
     *                      all institute.
     * @param loanType      this will be one or more types
     * @param limit         number of record to be fetch and return by query.
     * @param skip          this will be used for pagination.
     * @return query to generate report.
     */
    public static Query applicationRequestQueryBuilder(Date from, Date to,
                                                       String institutionId, Set<String> loanType, int limit, int skip) {
        Query query = new Query();
        return query
                .addCriteria(
                        Criteria.where("applicationRequest.header.dateTime")
                                .gte(from)
                                .lte(to)
                                .and("applicationRequest.header.institutionId")
                                .is(institutionId)
                                .and("currentStageId")
                                .is("DE")
                                .and("applicationRequest.request.application.loanType")
                                .in(loanType)).limit(limit).skip(limit);
    }

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return Mongo Query for report
     */
    public static Query buildReportingConfigurationQuery(
            ReportingConfigurations reportingConfigurations) {
        Query query = new Query();
        return query
                .addCriteria(
                        Criteria.where(
                                "applicationRequest.header.institutionId")
                                .is(reportingConfigurations.getHeader()
                                        .getInstitutionId())
                                .and("applicationRequest.request.application.loanType")
                                .in(reportingConfigurations.getProducts()))
                .limit(reportingConfigurations.getPagination().getLimit())
                .skip(reportingConfigurations.getPagination().getSkip());
    }

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return Mongo Query for report
     */
    public static Query buildReportingPreviewConfigQuery(
            ReportingConfigurations reportingConfigurations) {
        Query query = new Query();
        return query
                .addCriteria(
                        Criteria.where(
                                "applicationRequest.header.institutionId")
                                .is(reportingConfigurations.getHeader()
                                        .getInstitutionId())
                                .and("applicationRequest.request.application.loanType")
                                .in(reportingConfigurations.getProducts()))
                .limit(reportingConfigurations.getPagination().getLimit())
                .skip(reportingConfigurations.getPagination().getSkip());
    }

    /**
     * @param reportRequest
     * @return
     */
    public static Query buildDimensionQuery(ReportRequest reportRequest) {
        Query query = new Query();
        if (null != reportRequest) {
            return query.addCriteria(Criteria.where("institutionId").is(
                    reportRequest.getHeader().getInstitutionId()));
        }

        return query;

    }

    /**
     * @param reportRequest
     * @return
     */
    public static Query buildReportConfigurationQuery(
            ReportRequest reportRequest) {
        Query query = new Query();
        return query.addCriteria(Criteria.where("header.institutionId").is(
                reportRequest.getHeader().getInstitutionId()));

    }

    /**
     * @param reportRequest
     * @return
     */
    public static Query buildReportConfigByIdQuery(ReportRequest reportRequest) {
        Query query = new Query();
        return query.addCriteria(Criteria.where("header.institutionId")
                .is(reportRequest.getHeader().getInstitutionId())
                .and("reportId").is(reportRequest.getReportId()).and("userId")
                .is(reportRequest.getUserId()).and("inActive").is(false));

    }

    /**
     * @param reportingConfiguration
     * @return
     */
    public static Query buildFlatReportConfigQuery(
            ReportingConfigurations reportingConfiguration) {
        Query query = new Query();
        return query.addCriteria(Criteria.where("reportId")
                .is(reportingConfiguration.getReportId())
                .and("header.institutionId")
                .is(reportingConfiguration.getHeader().getInstitutionId())
                .and("userId").is(reportingConfiguration.getUserId())
                .and("inActive").is(false));
    }

    /**
     * @param reportSearchRequest
     * @return
     */
    public static Query buildReportSearchQuery(
            ReportSearchRequest reportSearchRequest) {
        Query query = new Query();
        return query.addCriteria(Criteria.where("reportName")
                .regex(GngUtils.sanitizeString(reportSearchRequest.getReportName()), "i")
                .and("header.institutionId")
                .is(reportSearchRequest.getHeader().getInstitutionId())
                .and("inActive").is(false));
    }

    /**
     * Stack graph query to fetch 30days record from current date.
     *
     * @param stackRequest It consist of institution id
     * @return
     */
    public static Aggregation buildLoginStatusGraphQuery(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest.getFromDate(),
                "Stack request from date should not be null or blank !!");

        Assert.notNull(stackRequest.getToDate(),
                "Stack request to date should not be null or blank !! ");

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "Stack request institutionId should not be null or blank !!");

        Assert.notNull(stackRequest.getCriteria(),
                "Stack request criteria should not be null or blank !!");

        Date fromDate = new DateTime(stackRequest.getFromDate()).toDate();
        Date toDate = new DateTime(stackRequest.getToDate()).toDate();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());
        Collection<String> cities = criteria.getCities();
        Collection<String> dealerIds = criteria.getDealerIds();
        Collection<String> dsaIds = criteria.getDsaIds();
        Collection<String> stages = criteria.getStages();
        Collection<String> statuses = criteria.getStatuses();

        List<Criteria> criterias = new ArrayList<>();

        if (null != products && !products.isEmpty()) {
            criterias.add(
                    Criteria.where("applicationRequest.request.application.loanType")
                            .in(products)
            );
        }

        if (null != cities && !cities.isEmpty()) {
            criterias.add(
                    Criteria.where("applicationRequest.request.applicant.address.city")
                            .in(cities)
            );
        }

        if (null != dealerIds && !dealerIds.isEmpty()) {
            criterias.add(
                    Criteria.where("applicationRequest.header.dealerId")
                            .in(dealerIds)
            );
        }

        if (null != dsaIds && !dsaIds.isEmpty()) {
            criterias.add(
                    Criteria.where("applicationRequest.header.dsaId")
                            .in(dsaIds)
            );
        }

        if (null != stages && !stages.isEmpty()) {
            criterias.add(Criteria.where("applicationRequest.currentStageId")
                    .in(stages));
        }

        if (null != statuses && !statuses.isEmpty()) {
            criterias.add(Criteria.where("applicationStatus").in(statuses));
        }

        criterias.add(Criteria.where("applicationRequest.header.institutionId").is(stackRequest.getHeader().getInstitutionId()));

        criterias.add(Criteria.where("applicationRequest.header.dateTime").gte(fromDate).lte(toDate));

        Criteria applyHierarchyCriteria = HierarchyRepository.applyHierarchyQuery(stackRequest.getCriteria());

        if (null != applyHierarchyCriteria) {
            criterias.add(applyHierarchyCriteria);
        }


        return newAggregation(
                match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                project().and(DateOperators.dateOf("$applicationRequest.header.dateTime")
                        .toString("%Y-%m-%d")).as("date").and("$applicationStatus").toUpper().as("applicationStatus"),
                match(Criteria.where("applicationStatus").in(Arrays.asList("APPROVED", "DECLINED", "QUEUE", "ONHOLD","DEDUPEQUEUE","CANCELLED"))),
                group(fields().and("$date").and("$applicationStatus")).count().as("count"),
                sort(Sort.Direction.ASC, "_id.dateTime")


        ).withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public static Aggregation buildLoginStatusGraphQueryOld(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "Stack request institutionId should not be null or blank !!");

        Date fromDate = new DateTime().minusDays(30).withHourOfDay(00).toDate();
        Date toDate = new DateTime().withHourOfDay(23).toDate();

        String dsaId = stackRequest.getHeader().getDsaId();

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        List<Criteria> criterias = new ArrayList<Criteria>();

        criterias.add(Criteria.where("applicationRequest.header.institutionId")
                .is(stackRequest.getHeader().getInstitutionId()));
        criterias.add(Criteria.where("applicationRequest.header.dateTime")
                .gte(fromDate).lte(toDate));

        if (StringUtils.isNotBlank(dsaId)) {
            criterias.add(Criteria.where("applicationRequest.header.dsaId").in(
                    dsaId));
        }

        //FIXME change hard coded product type
        RequestCriteria criteria = stackRequest.getCriteria();
        if (criteria != null) {
            Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());
            criterias.add(Criteria.where(
                    "applicationRequest.request.application.loanType").in(products));
        } else {
            criterias.add(Criteria.where(
                    "applicationRequest.request.application.loanType").in(Product.CDL.name()));
        }


        Criteria applyHierarchyCriteria = HierarchyRepository
                .applyHierarchyQuery(stackRequest.getCriteria());

        if (null != applyHierarchyCriteria) {
            criterias.add(applyHierarchyCriteria);
        }

        Criteria[] criteriaArray = new Criteria[criterias.size()];

        pipelineOperation.add(match(new Criteria().andOperator(criterias
                .toArray(criteriaArray))));

        BasicDBObject basicDBObject = new BasicDBObject("$project",
                new LinkedHashMap<String, Object>() {
                    {
                        put("date", new BasicDBObject("$dateToString",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("format", "%Y-%m-%d");
                                        put("date",
                                                "$applicationRequest.header.dateTime");
                                    }
                                }));
                        put("status", new BasicDBObject("$toUpper",
                                "$applicationStatus"));
                    }
                });

        pipelineOperation.add(new CustomProjectionOperation(basicDBObject));

        pipelineOperation.add(match(Criteria.where("status").in(
                new ArrayList<String>() {
                    {
                        add("APPROVED");
                        add("DECLINED");
                        add("QUEUE");
                    }
                })));

        pipelineOperation.add(group(fields().and("date").and("status")).count()
                .as("count"));

        pipelineOperation.add(sort(Sort.Direction.ASC, "_id.dateTime"));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }

    public static Query buildPostIpaRequest(String referenceId) {

        Assert.notNull(referenceId, "Reference Id must not be null or empty");

        return new Query(Criteria.where("_id").is(referenceId));

    }

    @Deprecated
    public static Query buildLoginStatusTable(StackRequest stackRequest) {

        Query query = new Query();

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "institutionId for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getFromDate(),
                "from date for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getToDate(),
                "To date for LoginStatus Table data must not be null or blank !!");

        String applicationStatus = stackRequest.getStatus();

        String dsaId = stackRequest.getHeader().getDsaId();

        String frDateStr = stackRequest.getFromDate();

        String toDateStr = stackRequest.getToDate();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        int skip = stackRequest.getSkip();

        int limit = stackRequest.getLimit();

        Date fromDate = new DateTime(frDateStr).toDate();
        Date toDate = new DateTime(toDateStr).toDate();

        query.addCriteria(Criteria
                .where("applicationRequest.header.institutionId")
                .is(stackRequest.getHeader().getInstitutionId())
                .and("applicationRequest.request.application.loanType")
                .in(products)
                .and("applicationRequest.header.dateTime").gte(fromDate)
                .lt(toDate));

        if (StringUtils.isNotBlank(applicationStatus)) {

            query.addCriteria(Criteria.where("applicationStatus").regex(
                    applicationStatus, "i"));

        } else {
            query.addCriteria(new Criteria().orOperator(
                    Criteria.where("applicationStatus").regex("APPROVED", "i"),
                    Criteria.where("applicationStatus").regex("DECLINED", "i"),
                    Criteria.where("applicationStatus").regex("QUEUE", "i")));

        }

        if (StringUtils.isNotBlank(dsaId)) {

            query.addCriteria(Criteria.where("applicationRequest.header.dsaId")
                    .is(dsaId));

        }

        Criteria hierarchyCriteria = HierarchyRepository
                .applyHierarchyQuery(criteria);

        if (null != hierarchyCriteria) {
            query.addCriteria(hierarchyCriteria);
        }

        query.skip(skip).limit(limit);

        query.with(new Sort(Sort.Direction.DESC,
                "applicationRequest.header.dateTime"));

        return query;
    }

    public static Aggregation buildAggregrationForLoginByStatusTabularForDtc(
            StackRequest stackRequest, Boolean needCount) {

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "institutionId for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getFromDate(),
                "from date for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getToDate(),
                "To date for LoginStatus Table data must not be null or blank !!");

        String applicationStatus = stackRequest.getStatus();


        String institutionId = stackRequest.getHeader().getInstitutionId();

        String frDateStr = stackRequest.getFromDate();

        String toDateStr = stackRequest.getToDate();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());

        int skip = stackRequest.getSkip();

        int limit = stackRequest.getLimit() > 0 ? stackRequest.getLimit() : 50;

        Date fromDate = new DateTime(frDateStr).withHourOfDay(00).toDate();
        Date toDate = new DateTime(toDateStr).withHourOfDay(23).toDate();

        List<AggregationOperation> pipelineOperations = new ArrayList<AggregationOperation>();

        pipelineOperations.add(match(Criteria
                .where("applicationRequest.header.institutionId")
                .is(institutionId)
                .and("applicationRequest.request.application.loanType")
                .in(products)
                .and("applicationRequest.header.dateTime").gte(fromDate)
                .lt(toDate)));

        if (StringUtils.isNotBlank(applicationStatus)) {

            pipelineOperations.add(match(Criteria.where("applicationStatus")
                    .regex(GngUtils.sanitizeString(applicationStatus), "i")));

        }


        Criteria hierarchyCriteria = HierarchyRepository
                .applyHierarchyQuery(criteria);

        if (null != hierarchyCriteria) {
            pipelineOperations.add(match(hierarchyCriteria));
        }

        pipelineOperations.add(new CustomProjectionOperation(new BasicDBObject(
                "$project", new LinkedHashMap<String, Object>() {
            {
                put("institutionId",
                        "$applicationRequest.header.institutionId");
                put("applicationDate",
                        "$applicationRequest.header.dateTime");
                put("loanType",
                        new BasicDBObject(
                                "$toUpper",
                                new Object[]{"$applicationRequest.request.application.loanType"}));
                put("applicationStatus", new BasicDBObject("$toUpper",
                        new Object[]{"$applicationStatus"}));
                put("dealerId", "$applicationRequest.header.dealerId");
                put("dsaId", "$applicationRequest.header.dsaId");
                put("loanAmount",
                        "$applicationRequest.request.application.loanAmount");
                put("croDecisions",
                        new BasicDBObject("$arrayElemAt", new Object[]{
                                "$croDecisions.amtApproved", 0}));
                put("stage", "$applicationRequest.currentStageId");
                put("applicantName",
                        "$applicationRequest.request.applicant.applicantName");
                put("intrimStatus", "$intrimStatus");
            }
        })));


        pipelineOperations.add(new CustomProjectionOperation(new BasicDBObject(
                "$project", new LinkedHashMap<String, Object>() {
            {

                put("applicationDate", "$applicationDate");
                put("applicantName", new BasicDBObject("$concat",
                        new Object[]{"$applicantName.firstName", " ",
                                "$applicantName.middleName", " ",
                                "$applicantName.lastName"}));
                put("loanAmount", "$loanAmount");
                put("amountApproved", "$croDecisions");
                put("applicationStatus", new BasicDBObject("$toUpper",
                        "$applicationStatus"));
                put("stage", "$stage");
                put("bureauScore", new BasicDBObject("$cond",
                        new LinkedHashMap<String, Object>() {
                            {
                                put("if",
                                        new BasicDBObject(
                                                "$eq",
                                                new Object[]{
                                                        "$intrimStatus.cibilScore",
                                                        "COMPLETE"}));
                                put("then",
                                        "$intrimStatus.cibilModuleResult.fieldValue");
                                put("else", "");
                            }
                        }));
                put("appScore", new BasicDBObject("$cond",
                        new LinkedHashMap<String, Object>() {
                            {
                                put("if",
                                        new BasicDBObject(
                                                "$eq",
                                                new Object[]{
                                                        "$intrimStatus.scoreStatus",
                                                        "COMPLETE"}));
                                put("then",
                                        "$intrimStatus.scoringModuleResult.fieldValue");
                                put("else", "");
                            }
                        }));
            }
        })));

        Collection<String> statuses = criteria.getStatuses();
        Collection<String> requiredStatus = new ArrayList<String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
                add("APPROVED");
                add("DECLINED");
                add("QUEUE");
            }
        };


        if (null != statuses && !statuses.isEmpty()) {

            pipelineOperations.add(match(Criteria.where("applicationStatus")
                    .in(statuses)));

        } else {
            pipelineOperations.add(match(Criteria.where("applicationStatus")
                    .in(requiredStatus)));
        }


        if (needCount) {
            pipelineOperations.add(group().count().as("count"));
        } else {

            pipelineOperations.add(skip(skip));

            pipelineOperations.add(limit(limit));
        }

        return Aggregation.newAggregation(pipelineOperations).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public static Aggregation buildAggregrationForLoginByStatusTabular(
            StackRequest stackRequest, Boolean needCount) {

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "institutionId for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getFromDate(),
                "from date for LoginStatus Table data must not be null or blank !!");

        Assert.notNull(stackRequest.getToDate(),
                "To date for LoginStatus Table data must not be null or blank !!");


        String applicationStatus = stackRequest.getStatus();

        List<String> statuses = new ArrayList<>();

        if(StringUtils.equals("APPROVED",applicationStatus)){
            statuses.add("Approved");
        }else if(StringUtils.equals("DECLINED",applicationStatus)){
            statuses.add("Declined");
            statuses.add("declined");
        }
        else if(StringUtils.equals("ONHOLD",applicationStatus)){
            statuses.add("OnHold");
        }
        else if(StringUtils.equals("QUEUE",applicationStatus)){
            statuses.add("QUEUE");
            statuses.add("Queue");
        }else if(StringUtils.equals("DEDUPEQUEUE",applicationStatus)){
            statuses.add("DedupeQueue");
        } else if (StringUtils.equals("CANCELLED", applicationStatus)) {
            statuses.add("Cancelled");
        }


        String institutionId = stackRequest.getHeader().getInstitutionId();

        /*String dsaId = stackRequest.getHeader().getDsaId();*/

        String frDateStr = stackRequest.getFromDate();

        String toDateStr = stackRequest.getToDate();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());

        int skip = stackRequest.getSkip();

        int limit = stackRequest.getLimit();

        Date fromDate = new DateTime(frDateStr).withHourOfDay(00).toDate();
        Date toDate = new DateTime(toDateStr).withHourOfDay(23).toDate();

        List<AggregationOperation> pipelineOperations = new ArrayList<>();

        List<Criteria> criterias = new ArrayList<>();

        criterias.add(
                Criteria.where("applicationRequest.header.institutionId")
                        .is(institutionId)
                        .and("applicationRequest.request.application.loanType")
                        .in(products)
                        .and("applicationStatus")
                        .in(statuses)
                        .and("applicationRequest.header.dateTime")
                        .gte(fromDate)
                        .lt(toDate));



        /*if (StringUtils.isNotBlank(dsaId)) {

            criterias.add(Criteria.where("applicationRequest.header.dsaId").is(dsaId));

        }*/


        Criteria hierarchyCriteria = HierarchyRepository.applyHierarchyQuery(criteria);

        if (null != hierarchyCriteria) {
            criterias.add(hierarchyCriteria);
        }

        pipelineOperations.add(match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))));
        pipelineOperations.add(project(
                fields().and("institutionId", "$applicationRequest.header.institutionId")
                        .and("applicationDate", "$applicationRequest.header.dateTime")
                        .and("loanType", "$applicationRequest.request.application.loanType")
                        .and("dealerId", "$applicationRequest.header.dealerId")
                        .and("dsaId", "$applicationRequest.header.dsaId")
                        .and("loanAmount", "$applicationRequest.request.application.loanAmount")
                        .and("stage", "$applicationRequest.currentStageId")
                )
                .and(ArrayOperators.arrayOf("$applicationRequest.request.applicant.address.city").elementAt(0)).as("city")
                        .and(ArrayOperators.arrayOf("$croDecisions.amtApproved").elementAt(0)).as("amountApproved")
                        .and("$applicationStatus").toUpper().as("applicationStatus")
                        .and(
                                StringOperators.valueOf("$applicationRequest.request.applicant.applicantName.firstName")
                                        .concat(" ")
                                        .concatValueOf("$applicationRequest.request.applicant.applicantName.middleName")
                                        .concat(" ").concatValueOf("$applicationRequest.request.applicant.applicantName.lastName")
                        )
                        .as("applicantName")
                        .and("bureauScore")
                        .applyCondition(ConditionalOperators.when(Criteria.where("intrimStatus.cibilScore").is("COMPLETE"))
                                .then("$intrimStatus.cibilModuleResult.fieldValue").otherwise(""))
                        .and("appScore").applyCondition(ConditionalOperators.when(Criteria.where("intrimStatus.scoreStatus").is("COMPLETE"))
                                .then("$intrimStatus.scoringModuleResult.fieldValue").otherwise(""))
        );

        criterias = new ArrayList<>();


        if (!CollectionUtils.isEmpty(criteria.getStatuses())) {
            criterias.add(Criteria.where("applicationStatus").in(criteria.getStatuses()));
        }

/*        if (!CollectionUtils.isEmpty(criteria.getStages())  || null != stackRequest.getStatus()) {

            String status = stackRequest.getStatus();

            Collection statuses  = new ArrayList();

            if(null != criteria.getStages()){

                statuses.add(criteria.getStages());

            }

            if("DISBURSED".equalsIgnoreCase(status)){

                statuses.add(GNGWorkflowConstant.LOS_DISB.toFaceValue());

            }else if("OPS QUEUE".equalsIgnoreCase(status)){

                statuses.addAll(Arrays.asList(GNGWorkflowConstant.LOS_APRV.toFaceValue(),
                        GNGWorkflowConstant.LOS_QDE.toFaceValue()
                        ,GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue(),
                        "INV_GEN", GNGWorkflowConstant.INV_GNR.toFaceValue()));

            }else if("APPROVED".equalsIgnoreCase(status)){

                statuses.addAll(Arrays.asList(GNGWorkflowConstant.APRV.toFaceValue() ,
                        GNGWorkflowConstant.SRNV.toFaceValue() ,
                        GNGWorkflowConstant.DO.toFaceValue() ,
                        "SERIAL-NO VALIDATION"));

            }else if("DECLINED".equalsIgnoreCase(status)){

                statuses.addAll(Arrays.asList(GNGWorkflowConstant.DCLN.toFaceValue() ,
                        GNGWorkflowConstant.LOS_DCLN.toFaceValue()));

            }else if("NO DECISION".equalsIgnoreCase(status)){

            }

            if(!CollectionUtils.isEmpty(statuses)){
                criterias.add(Criteria.where("stage").in(statuses));
            }

        }*/

        if (!CollectionUtils.isEmpty(criteria.getDealerIds())) {
            criterias.add(Criteria.where("dealerId").in(criteria.getDealerIds()));
        }

        if (!CollectionUtils.isEmpty(criteria.getDsaIds())) {
            criterias.add(Criteria.where("dsaId").in(criteria.getDsaIds()));
        }

        if (!CollectionUtils.isEmpty(criteria.getBranches())) {

            criterias.add(Criteria.where("city").in(criteria.getBranches()));

        }

        if(!criterias.isEmpty()){
            pipelineOperations.add(match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))));
        }


        if (needCount) {

            pipelineOperations.add(group().count().as("count"));

        } else {

            pipelineOperations.add(new SkipOperation(skip));

            pipelineOperations.add(limit(limit));
        }

        logger.debug(" aggregation initiated for login_by_status_table_data is generated as {} ", newAggregation(pipelineOperations));


        return Aggregation.newAggregation(pipelineOperations).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }


    public static Query buildAggregationForRoleBasedFilter(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest.getHeader().getInstitutionId(),
                "institutionId for LoginStatus Table data must not be null or blank !!");

        String institutionId = stackRequest.getHeader().getInstitutionId();

        RequestCriteria criteria = stackRequest.getCriteria();

        Query query = new Query();

        Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());

        query.addCriteria(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.institutionId").is(institutionId),
                Criteria.where("applicationRequest.request.application.loanType").in(products)
        ));

        Criteria criteria1 = HierarchyRepository.applyHierarchyQuery(criteria);

        if (null != criteria1) {
            query.addCriteria(criteria1);
        }

        return query;

    }


    public static Query buildSerialNumberInfoQuery(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        Assert.notNull(goNoGoCustomerApplication, "goNoGoCustomerApplication must not be null or blank ");

        Query query = new Query();

        query.addCriteria(Criteria.where("refID")
                .is(goNoGoCustomerApplication.getGngRefId()).and("status")
                .is("VALID"));

        return query;


    }

    public static Query buildQueryForSchemeMapping(String schemeID, String institutionId, String productAliasName ,String queryFor) {

        Query query = new Query();

        if (StringUtils.equals(Status.DEALER_MAPPING.name(), queryFor)) {
            query.addCriteria(Criteria.where("schemeID").is(schemeID)
                    .and("institutionId").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("updateStatus").is(false)
                    .and("approveSchemeUpdateStatus").is(false));

        } else {
            query.addCriteria(Criteria.where("schemeID").is(schemeID)
                    .and("institutionID").is(institutionId)
                    .and("productFlag").is(productAliasName)
                    .and("updateStatus").is(false)
                    .and("approveSchemeUpdateStatus").is(false));
        }
        return query;
    }

    public static Query buildApplicationRequestQuery(String institutionId, Date startDate, Date endDate, String loanType) {
        Query query = new Query();
        query.addCriteria(Criteria.where("header.dateTime").gte(startDate)
                .lte(endDate)
                .and("header.institutionId").is(institutionId)
                .and("request.application.loanType").is(loanType)
                .and("currentStageId").is("DE"));
        return query;
    }

    public static Query buildFilehandOverDetails(String gngRefId) {
        Assert.notNull(gngRefId, "Reference Id must not be null or empty");

        return new Query(Criteria.where("_id").is(gngRefId));
    }

    /**
     * @param from          Date which will used report start date.
     * @param to            end to select report.
     * @param institutionId Institution id is compulsory which used to filter data among
     *                      all institute.
     * @param loanType      this will be one or more types
     * @param limit         number of record to be fetch and return by query.
     * @param skip          this will be used for pagination.
     * @return query to generate report.
     */
    public Query gonogoCustomerQueryBuilder(Date from, Date to,
                                            String institutionId, Set<String> loanType, int limit, int skip) {
        Query query = new Query();
        return query
                .addCriteria(
                        Criteria.where("applicationRequest.header.dateTime")
                                .gte(from)
                                .lte(to)
                                .and("applicationRequest.header.institutionId")
                                .is(institutionId)
                                .and("applicationRequest.request.application.loanType")
                                .in(loanType)).limit(limit).skip(limit);
    }

    public static Update buildCGPMasterQuery(CommonGeneralParameterMaster commonGeneralParameterMaster) {

        Update update =new Update();
        update.set("branchId",commonGeneralParameterMaster.getBranchId());
        update.set("makerId",commonGeneralParameterMaster.getMakerId());
        update.set("makeDate",commonGeneralParameterMaster.getMakeDate());
        update.set("authId",commonGeneralParameterMaster.getAuthId());
        update.set("authDate",commonGeneralParameterMaster.getAuthDate());
        update.set("moduleId",commonGeneralParameterMaster.getModuleId());
        update.set("key1",commonGeneralParameterMaster.getKey1());
        update.set("key2",commonGeneralParameterMaster.getKey2());
        update.set("value",commonGeneralParameterMaster.getValue());
        update.set("description",commonGeneralParameterMaster.getDescription());
        update.set("status",commonGeneralParameterMaster.getStatus());
        update.set("moduleFlag",commonGeneralParameterMaster.getModuleFlag());
        update.set("appFlag",commonGeneralParameterMaster.getAppFlag());
        update.set("disableFlag",commonGeneralParameterMaster.getDisableFlag());
        update.set("cgpStartDate",commonGeneralParameterMaster.getCgpStartDate());
        update.set("cgpEndDate",commonGeneralParameterMaster.getCgpEndDate());
        update.set("mApplyFlag",commonGeneralParameterMaster.getmApplyFlag());
        update.set("institutionId",commonGeneralParameterMaster.getInstitutionId());
        update.set("updateDate" ,new Date());
      return update;

    }

    public static Query buildSourcingDetailsQuery(String institutionId, String empId, String key1) {

        Query sourcingQuery = new Query();
        Date currentDate = new Date();
        sourcingQuery.addCriteria(Criteria.where("institutionId").is(institutionId)
                .and("moduleId").is(SourcingEnum.LEA.name())
                .and("disableFlag").is(SourcingEnum.Y.name())
                .and("value").is(empId)
                .and("key1").is(key1)
                .and("cgpStartDate").lte(currentDate)
                .and("cgpEndDate").gte(currentDate)
                .and("active").is(true));

        return sourcingQuery;

    }

    public static Query buildApplicationAgreementFormExistanceQuery(String refId, String institutionId, String formName) {

        Query formQuery = new Query();

        formQuery.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                .is(refId).and("metadata.fileHeader.institutionId")
                .is(institutionId).and("metadata.uploadFileDetails.fileType")
                .is("application/pdf")
                .and("metadata.uploadFileDetails.FileName").is(formName));
        return formQuery;

    }

    public static Aggregation buildQueryForBankName(String bankName){


        List<AggregationOperation> pipelineOperations = new ArrayList<>();

        pipelineOperations.add(match(Criteria.where("key1").is("BANKNAME")
                .and("description").regex(Pattern.compile(CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(bankName),
                        Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE))));

        pipelineOperations.add(project(fields().and("key1").and("key2").and("description")));

        pipelineOperations.add(group(fields().and("key2")).first("description").as("description"));

        pipelineOperations.add(limit(5));

        return  Aggregation.newAggregation(pipelineOperations ).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }

    public static Aggregation buildQueryForBankNameFromBankMaster(String bankName){


        List<AggregationOperation> pipelineOperations = new ArrayList<>();

        pipelineOperations.add(match(Criteria.where("bankDescription").regex(Pattern.compile(CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(bankName),
                        Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE))));


        pipelineOperations.add(project(fields().and("bankDescription")));

        pipelineOperations.add(limit(5));

        return  Aggregation.newAggregation(pipelineOperations ).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }
}
