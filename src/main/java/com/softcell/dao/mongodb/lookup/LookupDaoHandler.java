package com.softcell.dao.mongodb.lookup;

import com.softcell.config.AadharVersionConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.ActionName;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.masters.ApiRoleAuthorisationMaster;
import com.softcell.gonogo.model.masters.LookupMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by yogeshb on 10/5/17.
 */
@Repository
public class LookupDaoHandler implements LookupDao {

    private static final Logger logger = LoggerFactory.getLogger(LookupDaoHandler.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    public LookupDaoHandler(){
        mongoTemplate = MongoConfig.getMongoTemplate();
    }


    public List<ActionConfiguration> actionConfigurationReferrer(String institutionId, String productId, ActionName actionName) {

        Query query = new Query();

        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("actionName").is(actionName));

        /*CloseableIterator<ActionConfiguration> stream = mongoTemplate.stream(query, ActionConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());*/

        return mongoTemplate.find(query, ActionConfiguration.class);


        /*return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toMap(x -> StringUtils.join(CacheName.ACTION.name(), x.getInstitutionId(), x.getProductId(), x.getActionName().name()), x -> x));*/
    }

    @Override
    public List<TemplateConfiguration> templateConfigurationReferrer(String institutionId, String productId, TemplateName templateName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("templateName").is(templateName));

        CloseableIterator<TemplateConfiguration> stream = mongoTemplate.stream(query, TemplateConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());


       /* return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toMap(x -> StringUtils.join(CacheName.TEMPLATE.name(), x.getInstitutionId(), x.getProductId(), x.getTemplateName().name()), x -> x));*/
    }

    @Override
    public List<EmailConfiguration> emailConfigurationReferrer(String institutionId, String productId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId));

        CloseableIterator<EmailConfiguration> stream = mongoTemplate.stream(query, EmailConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());

        /*return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toMap(x -> StringUtils.join(CacheName.EMAIL.name(), x.getInstitutionId(), x.getProductId()), x -> x));*/

    }

    @Override
    public Set<String> applicableVendorsConfigurationReferrer(String institutionId, String productId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId));

        CloseableIterator<ValidationVendorsConfiguration> stream = mongoTemplate.stream(query, ValidationVendorsConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.mapping(obj -> obj.getVendor().toFaceValue(), Collectors.toSet()));

        /*return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.groupingBy(obj -> StringUtils.join(CacheName.APPLICABLE_VENDOR.name(), obj.getInstitutionId(), obj.getProductId()), Collectors.mapping(obj -> obj.getVendor().toFaceValue(), Collectors.toSet())));*/
    }

    @Override
    public List<InstitutionProductConfiguration> aliasNameByProductNameAndInstitutionIdReferrer(String institutionId, String productName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("status").is(true)
                .and("institutionID").is(institutionId)
                .and("productName").is(productName));
        CloseableIterator<InstitutionProductConfiguration> stream = mongoTemplate.stream(query, InstitutionProductConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());

        /*return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toMap(obj -> StringUtils.join(CacheName.ALIAS_NAME.name(), obj.getInstitutionID(), obj.getProductName()), obj -> obj.getAliasName()));*/

    }

    @Override
    public List<LoyaltyCardConfiguration> loyaltyCardConfigurationReferral(String institutionId, String productId,LoyaltyCardType loyaltyCardType) {

        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("loyaltyCardType").is(loyaltyCardType));

        CloseableIterator<LoyaltyCardConfiguration> stream = mongoTemplate.stream(query, LoyaltyCardConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());

    }

    @Override
    public List<DmsFolderConfiguration> getDmsFolderInformation(String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId));

        CloseableIterator<DmsFolderConfiguration> stream = mongoTemplate.stream(query, DmsFolderConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());

    }

    @Override
    public List<SmsTemplateConfiguration> getSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(smsTemplateConfiguration.getInstitutionId())
                .and("productId").is(smsTemplateConfiguration.getProductId())
                .and("smsType").is(smsTemplateConfiguration.getSmsType()));

        CloseableIterator<SmsTemplateConfiguration> stream = mongoTemplate.stream(query, SmsTemplateConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());
    }

    @Override
    public AadharVersionConfiguration getAadharVersionConfiguration(String institutionId, String kycRequestType) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("enable").is(true)
                    .and("institutionId").is(institutionId)
                    .and("kycReqType").is(kycRequestType));

            return mongoTemplate.findOne(query, AadharVersionConfiguration.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException(String.format("problem occur at the time getting AadharVersionConfiguration with probable cause{%s}", e.getMessage()));
        }

    }

    @Override
    public CaseCancellationJobConfig getCaseCancellationJobConfig(String institutionId) {

        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("enable").is(true)
                    .and("institutionId").is(institutionId));

            return mongoTemplate.findOne(query, CaseCancellationJobConfig.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException(String.format("problem occur at the time getting caseCancellationJobConfig with probable cause{%s}", e.getMessage()));
        }
    }

    @Override
    public IMPSConfigDomain getImpsConfiguration(String institutionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("institutionId").is(institutionId).and("isEnabled").is(true));

        return mongoTemplate.findOne(query, IMPSConfigDomain.class);


    }

    @Override
    public Object templateConfigurationReferrerById(String institutionId, String productId, TemplateName templateName, String templateId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("templateId").is(templateId)
                .and("templateName").is(templateName));

        CloseableIterator<TemplateConfiguration> stream = mongoTemplate.stream(query, TemplateConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());


       /* return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toMap(x -> StringUtils.join(CacheName.TEMPLATE.name(), x.getInstitutionId(), x.getProductId(), x.getTemplateName().name()), x -> x));*/
    }

    public String getLookupMasterValue(String institutionId, ActionName actionName){
        String value = null;
        try {
            Query query = new Query(Criteria.where("institutionId").is(institutionId).
                    and("lookupIdentifier").is(actionName).
                    and("active").is(true));

            LookupMaster lookupMaster = mongoTemplate.findOne(query, LookupMaster.class);
            if(null != lookupMaster)
                value = lookupMaster.getLookupValue();
        }catch(Exception e){
            value = null;
        }
        return value;
    }

    @Override
    public ApiRoleAuthorisationMaster apiRoleAccessCheck(String institutionId, String sourceId , String apiName, String role) {

        Query query = new Query();
        query.addCriteria(Criteria.where("active").is(true)
                .and("apiName").is(apiName)
                .and("institutionId").is(institutionId)
                .and("sourceId").is(sourceId));

        return mongoTemplate.findOne(query,ApiRoleAuthorisationMaster.class);
    }


    public ActionConfiguration getActionConfigurationReferrer(String institutionId, String productId, ActionName actionName) {

        Query query = new Query();

        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("productId").is(productId)
                .and("actionName").is(actionName));

        logger.debug("query for actionConfigurationReferrer is:{}", query);


        ActionConfiguration actionConfiguration = mongoTemplate.findOne(query, ActionConfiguration.class);

        return actionConfiguration;
    }


    @Override
    public List<ActionConfiguration> actionsConfigurationReferrer(String institutionId, String sourceId, ActionName actionName) {

        Query query = new Query();

        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("sourceId").is(sourceId)
                .and("actionName").is(actionName));

        logger.debug("query for actionConfigurationReferrer is:{}", query);

        CloseableIterator<ActionConfiguration> stream = mongoTemplate.stream(query, ActionConfiguration.class);

        return StreamUtils.createStreamFromIterator(stream).collect(Collectors.toList());
    }


    @Override
    public List<ActionConfiguration> actionConfigurationReferrer(String institutionId, ActionName actionName) {

        Query query = new Query();

        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("actionName").is(actionName.name()));

        logger.debug("query for actionConfigurationReferrer is:{}", query);

        CloseableIterator<ActionConfiguration> stream = mongoTemplate.stream(query, ActionConfiguration.class);
        return StreamUtils.createStreamFromIterator(stream)
                .collect(Collectors.toList());
    }


    @Override
    public ActionConfiguration getActionsConfigurationReferrer(String institutionId, String sourceId,ActionName actionName) {
        Query query = new Query();

        query.addCriteria(Criteria.where("enable").is(true)
                .and("institutionId").is(institutionId)
                .and("sourceId").is(sourceId)
                .and("actionName").is(actionName));

        logger.debug("query for actionConfigurationReferrer is:{}", query);


        ActionConfiguration actionConfiguration = mongoTemplate.findOne(query, ActionConfiguration.class);

        return actionConfiguration;
    }

}
