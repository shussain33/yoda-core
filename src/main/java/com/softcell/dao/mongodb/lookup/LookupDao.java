package com.softcell.dao.mongodb.lookup;

import com.softcell.config.AadharVersionConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.ActionName;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.masters.ApiRoleAuthorisationMaster;

import java.util.List;
import java.util.Set;

/**
 * Created by yogeshb on 10/5/17.
 */
public interface LookupDao {

    /**
     *
     * @param institutionId
     * @param productId
     * @param actionName
     * @return
     */
    List<ActionConfiguration> actionConfigurationReferrer(final String institutionId , final  String  productId , final ActionName actionName);


    /**
     *
     * @param institutionId
     * @param productId
     * @param templateName
     * @return
     */
    List<TemplateConfiguration> templateConfigurationReferrer(String institutionId, String productId, TemplateName templateName);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    List<EmailConfiguration> emailConfigurationReferrer(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    Set<String> applicableVendorsConfigurationReferrer(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    List<InstitutionProductConfiguration> aliasNameByProductNameAndInstitutionIdReferrer(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    List<LoyaltyCardConfiguration> loyaltyCardConfigurationReferral(String institutionId, String productId ,LoyaltyCardType loyaltyCardType);

    /**
     * Get SmsTemplateConfiguration from the database.
     * @return
     */
    List<SmsTemplateConfiguration> getSmsTemplateConfiguration(SmsTemplateConfiguration smsTemplateConfiguration);

    /**
     *
     * @param institutionId
     * @return
     */
    List<DmsFolderConfiguration> getDmsFolderInformation(String institutionId);

    /**
     *
     * @param institutionId
     * @param kycRequestType
     * @return
     */
    AadharVersionConfiguration getAadharVersionConfiguration(String institutionId, String kycRequestType);

    /**
     *
     * @param institutionId
     * @return
     */
    CaseCancellationJobConfig getCaseCancellationJobConfig(String institutionId);


    /**
     * @param institutionId
     * @return
     */
    IMPSConfigDomain getImpsConfiguration(String institutionId);

    Object templateConfigurationReferrerById(String institutionId, String productId, TemplateName templateName, String templateId);

    String getLookupMasterValue(String institutionId, ActionName actionName);

    ActionConfiguration getActionConfigurationReferrer(String institutionId, String productId, ActionName actionName);

    ActionConfiguration getActionsConfigurationReferrer(String institutionId,  String sourceId ,ActionName actionName);

    List<ActionConfiguration> actionConfigurationReferrer(final String institutionId , final ActionName actionName);

    List<ActionConfiguration> actionsConfigurationReferrer(final String institutionId , final  String  productId , final ActionName actionName);

    ApiRoleAuthorisationMaster apiRoleAccessCheck(final String institutionId, final String sourceId , final String apiName, final String role);
}
