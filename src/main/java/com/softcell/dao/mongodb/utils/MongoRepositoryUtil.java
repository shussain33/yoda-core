package com.softcell.dao.mongodb.utils;


import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.softcell.config.CriteriaMapping;
import com.softcell.constants.Institute;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.configuration.admin.InstitutionConfig;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.security.v2.Hierarchy;
import com.softcell.utils.GngUtils;

import java.lang.reflect.Field;
import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * @author bhuvneshk
 *         <p/>
 *         Utility inside this call will be used by Mongo Repository throughout
 *         the application
 */

public class MongoRepositoryUtil {

    private MongoRepositoryUtil() {

    }

    /**
     * To fetch all the pincode based on Hierarchy Criteria for PL sales queue
     *
     * @param hierarchy
     * @param mongoTemplate It is used to connect to data mongodb
     * @return
     */
    public static List<Integer> getPincodesOnCriteriaPL(Hierarchy hierarchy,
                                                        MongoTemplate mongoTemplate) {

        Set<Integer> pincode = new TreeSet<Integer>();
        List<Integer> pincodeArr = new ArrayList<Integer>();

        String collectionName = "PlPincodeEmailMaster";
        String includeField = "pincode";

        BasicDBObject query = QueryBuilder.buildQueryOnHierarchyCriteriaPL(hierarchy);

        BasicDBObject fields = new BasicDBObject();
        fields.put(includeField, 1);

        DBCursor cursor = mongoTemplate.getCollection(collectionName).find(
                query, fields);
        while (cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            Integer pinInt = Integer
                    .parseInt(obj.getString(includeField, "-1"));
            pincode.add(pinInt);
        }
        if (!pincode.isEmpty()) {
            pincodeArr.addAll(pincode);
        }
        return pincodeArr;
    }

    /**
     * To fetch all the dealers based on Hierarchy Criteria for CDL queue
     *
     * @param hierarchy
     * @param mongoTemplate It is used to connect to data mongodb
     * @return
     */
    public static List<String> getDealersOnCriteriaCDL(Hierarchy hierarchy,
                                                       MongoTemplate mongoTemplate) {

        Set<String> dealer = new TreeSet<String>();
        List<String> dealerList = new ArrayList<String>();

        String collectionName = "CDLHierarchyMaster";

        String includeField = "dealerId";

        BasicDBObject query = QueryBuilder.buildQueryOnHierarchyCriteriaCDL(hierarchy);

        BasicDBObject fields = new BasicDBObject();

        fields.put(includeField, 1);

        DBCursor cursor = mongoTemplate.getCollection(collectionName).find(query, fields);

        while (cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            dealer.add(obj.getString(includeField, "-1"));
        }

        if (!dealer.isEmpty()) {
            dealerList.addAll(dealer);
        }

        return dealerList;
    }

    /**
     * To add Hierarchy criteria to query
     *
     * @param criteria      Request criteria can be branch , pincode , zone Hierarchy is
     *                      added for hierarchy level
     * @param mongoTemplate It is used to connect to data mongodb
     * @return
     */
    public static void addHierarchyCriteria(RequestCriteria criteria,
                                            Query query, MongoTemplate mongoTemplate) {

        List<Integer> pincodeList = null;
        Collection<String> productList = null;
        List<String> dealerList = null;

        if (criteria != null) {
            Hierarchy hierarchy = criteria.getHierarchy();

            if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.PL.name())) {
                pincodeList = getPincodesOnCriteriaPL(hierarchy, mongoTemplate);
            } else if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.CDL.name())) {
                dealerList = getDealersOnCriteriaCDL(hierarchy, mongoTemplate);
            }
        }
        productList = GngUtils.convertToCollectionOfString(criteria.getProducts());

        QueryBuilder.buildQueryforCroQueue(query, pincodeList, productList,
                dealerList);
    }

    public static void getCriteriaMapping(Hierarchy hierarchy, Criteria criteria, MongoTemplate mongoTemplate) {

        if (hierarchy != null) {
            String hierarchyLevel = hierarchy.getHierarchyLevel();
            List<String> hierarchyValList = hierarchy.getHierarchyValue();

            Query query = new Query();
            query.addCriteria(Criteria.where("hierarchyLevel").is(hierarchyLevel));
            CriteriaMapping hierarchyGoNoGoMapping = mongoTemplate.findOne(query, CriteriaMapping.class);

            if (hierarchyGoNoGoMapping != null)
                criteria.and(hierarchyGoNoGoMapping.getFieldMapping()).in(hierarchyValList);
        }
    }

    /**
     * To add Hierarchy criteria to query
     *
     * @param criteria      Request criteria can be branch , pincode , zone Hierarchy is
     *                      added for hierarchy level
     * @param mongoTemplate It is used to connect to data mongodb
     * @return
     */
    public static void addHierarchyCriteriaApplicationRequest(
            RequestCriteria criteria, Query query, MongoTemplate mongoTemplate) {

        List<Integer> pincodeList = null;
        Collection<String> productList = null;
        List<String> dealerList = null;

        if (criteria != null) {
            Hierarchy hierarchy = criteria.getHierarchy();

            if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.PL.name())) {
                pincodeList = getPincodesOnCriteriaPL(hierarchy, mongoTemplate);
            } else if (hierarchy != null
                    && StringUtils.equalsIgnoreCase(
                    hierarchy.getHierarchyType(),
                    Product.CDL.name())) {
                dealerList = getDealersOnCriteriaCDL(hierarchy, mongoTemplate);
            }

            productList = GngUtils.convertToCollectionOfString(criteria.getProducts());

            QueryBuilder.buildQueryforCroQueueRequestDocument(query,
                    pincodeList, productList, dealerList);
        }
    }

    /**
     * To add Hierarchy criteria to query
     *
     * @param stackRequest
     * @param mongoTemplate It is used to connect to data mongodb
     * @return
     */
    @Deprecated
    public static MatchOperation addHierarchyCriteria(
            StackRequest stackRequest, MongoTemplate mongoTemplate) {
        List<Integer> pincodeList = null;
        Collection<String> productList = null;
        List<String> dealerList = null;

        if (stackRequest != null) {

            RequestCriteria criteria = stackRequest.getCriteria();
            if (criteria != null) {
                Hierarchy hierarchy = criteria.getHierarchy();

                if (hierarchy != null
                        && StringUtils.equalsIgnoreCase(
                        hierarchy.getHierarchyType(),
                        Product.PL.name())) {
                    pincodeList = getPincodesOnCriteriaPL(hierarchy,
                            mongoTemplate);
                } else if (hierarchy != null
                        && StringUtils.equalsIgnoreCase(
                        hierarchy.getHierarchyType(),
                        Product.CDL.name())) {
                    dealerList = getDealersOnCriteriaCDL(hierarchy,
                            mongoTemplate);
                }

                productList = GngUtils.convertToCollectionOfString(criteria.getProducts());
            }

            Criteria c = QueryBuilder.buildQueryforAnalyticGraphHierarchy(
                    pincodeList, productList, dealerList);
            if (c != null)
                return new MatchOperation(c);

        }

        return null;
    }

    /**
     * @param auditObject Any Object which will modified after after this action
     *                    but need to save last state of an object. This will be
     *                    moved into another database.
     * @return OriginalCallectinName appended by Audit String.
     */
    public static String getAuditCollectionName(Object auditObject) {
        String collectionName = ((Class<? extends Object>) auditObject).getSimpleName() + MongoGngUtilCollection._Audit;
        for (Field field : auditObject.getClass().getFields()) {
            Document document = field.getAnnotation(Document.class);
            field.setAccessible(true);
            if (document != null)
                return collectionName = document.collection() + MongoGngUtilCollection._Audit;
        }
        return collectionName;
    }

    public static void createCriteriaForVerifierRole(Header header, Criteria criteria, String role, List<String> statusList) {

        if (Cache.VERIFICATION_ROLE_URL_MAP.containsKey(role)) {
            List<Object> tempList = Cache.VERIFICATION_ROLE_URL_MAP.get(role);

            String agencyCodeAppPath = (String) tempList.get(1);
            String statusAppPath = "status";
            if (agencyCodeAppPath.contains("agencyCode")) {
                statusAppPath = agencyCodeAppPath.replace("agencyCode", "status");
            }

            /* Query to fetch all application which has verification agency same as varifiers agency
                        and verification status as SUBMITTED.
                For verifier role we are passing agency code as dealerId in header */
            if(CollectionUtils.isNotEmpty(header.getDealerIds()) && Institute.isInstitute(header.getInstitutionId(), Institute.SBFC)) {
                criteria.and(agencyCodeAppPath).in(header.getDealerIds())
                        .and(statusAppPath).in(statusList);
            }else {
                criteria.and(agencyCodeAppPath).is(header.getDealerId())
                        .and(statusAppPath).in(statusList);
            }
        }
    }

    public enum MongoGngUtilCollection {
        _Audit;
    }

    public static List<CriteriaMapping> getAllCriteriaMapping(MongoTemplate mongoTemplate) {
        Query query = new Query();
        return mongoTemplate.find(query, CriteriaMapping.class);
    }

    public static List<InstitutionConfig> getInstitutionConfig(MongoTemplate mongoTemplate) {
        Query query = new Query();
        return mongoTemplate.find(query, InstitutionConfig.class);
    }

    public static List<OrganizationalHierarchyMasterV2> getOrganisationalHierarchyMaster(MongoTemplate mongoTemplate, boolean addProjection) {
        Query query = new Query();
        if (addProjection) {
            query.fields().include("institutionId");
            query.fields().include("product");
            query.fields().include("branch");
        }
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    public static List<OrganizationalHierarchyMasterV2> getOrganisationalHierarchyMaster(MongoTemplate mongoTemplate) {
        Query query = new Query();
        query.fields().include("institutionId");
        query.fields().include("roleWiseUsersList.users.userId");
        query.fields().include("roleWiseUsersList.users.userName");
        return mongoTemplate.find(query, OrganizationalHierarchyMasterV2.class);
    }

    public static OrganizationalHierarchyMasterV2 getOrganisationalHierarchyMaster(MongoTemplate mongoTemplate,
                                                                                   String institutionId, String userId) {
        Query query = new Query(Criteria.where("institutionId").is(institutionId)
                .and("roleWiseUsersList.users.userId").is(userId));
        query.fields().include("institutionId");
        query.fields().include("roleWiseUsersList.users.userId");
        query.fields().include("roleWiseUsersList.users.userName");
        return mongoTemplate.findOne(query, OrganizationalHierarchyMasterV2.class);
    }

    public static void getHiererchyCriteria(Hierarchy hierarchy, Criteria criteria, MongoTemplate mongoTemplate) {

        if (hierarchy != null) {
            String hierarchyLevel = hierarchy.getHierarchyLevel();
            List<String> hierarchyValList = hierarchy.getHierarchyValue();

            Query query = new Query();
            query.addCriteria(Criteria.where("hierarchyLevel").is(hierarchyLevel));
            CriteriaMapping hierarchyGoNoGoMapping = mongoTemplate.findOne(query, CriteriaMapping.class);

            if (hierarchyGoNoGoMapping != null)
                criteria.and(hierarchyGoNoGoMapping.getFieldMapping()).in(hierarchyValList);
        }
    }

}
