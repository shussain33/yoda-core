package com.softcell.dao.mongodb.utils;

import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;

import java.util.*;

/**
 * @author kishorp Use to do set operation on collection. and it's collection
 */
public class GngCollectionUtils {

    /**
     * private constructor to hide object creation
     */
    private GngCollectionUtils() {
    }

    /**
     * @param columnConfigurations
     * @param flatReportConfiguration
     * @return
     */
    public static Set<ColumnConfiguration> getUnSelectedColumns(
            Set<ColumnConfiguration> columnConfigurations,
            FlatReportConfiguration flatReportConfiguration) {

        if (null != flatReportConfiguration) {

            Map<Integer, ColumnConfiguration> headerMap = flatReportConfiguration.getHeaderMap();

            if (null != headerMap && !headerMap.isEmpty()) {
                Collection<ColumnConfiguration> selectedColumnConfigurations = headerMap.values();
                Set<ColumnConfiguration> set = new HashSet<>(selectedColumnConfigurations);
                return complement(columnConfigurations, set);

            }
        }

        return columnConfigurations;
    }

    private static Set<ColumnConfiguration> complement(
            Set<ColumnConfiguration> a,
            Set<ColumnConfiguration> b) {
        HashSet<String> keySet = new HashSet<>();

        LinkedHashSet<ColumnConfiguration> complement = new LinkedHashSet<>();
        for (ColumnConfiguration columnConfig : b) {
            keySet.add(columnConfig.getColumnKey());
        }

        for (ColumnConfiguration columnConfig : a) {
            if (!keySet.contains(columnConfig.getColumnKey())) {
                complement.add(columnConfig);
            }
        }
        return complement;
    }

}
