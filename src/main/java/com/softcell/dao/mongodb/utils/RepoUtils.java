package com.softcell.dao.mongodb.utils;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.QueryMapper;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by prateek on 2/4/17.
 */
@Component
public class RepoUtils {

    private static final Logger logger = LoggerFactory.getLogger(RepoUtils.class);

    @Autowired
    public MongoTemplate mongoTemplate;



    public <T> Stream<T> convertAggregationToStream(List<AggregationOperation> aggregationOperations, Class<T> entityType, String collectionName){

        Assert.notEmpty(aggregationOperations, "Query must not be null!");
        Assert.notNull(entityType, "Entity type must not be null!");
        Assert.notNull(collectionName, " collection name must not be null or blank");

        TypedAggregation<T> agg = Aggregation.newAggregation(entityType, aggregationOperations);

        MongoConverter converter = mongoTemplate.getConverter();

        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext = converter.getMappingContext();

        QueryMapper queryMapper = new QueryMapper(converter);

        AggregationOperationContext context = new TypeBasedAggregationOperationContext(entityType, mappingContext, queryMapper);

        BasicQuery query = new BasicQuery(agg.toDbObject(collectionName, context));

        CloseableIterator<T> stream = mongoTemplate.stream(query, entityType);

         return StreamUtils.createStreamFromIterator(stream);

    }

    public <T> Stream  convertAggregation2Stream(TypedAggregation<T> aggregation , Class<T> entityType , String collectionName){

        Assert.notNull(aggregation, "Query must not be null!");
        Assert.notNull(entityType, "Entity type must not be null!");
        Assert.notNull(collectionName, " collection name must not be null or blank");

        MongoConverter converter = this.mongoTemplate.getConverter();

        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext = converter.getMappingContext();

        QueryMapper queryMapper = new QueryMapper(converter);

        AggregationOperationContext context = new TypeBasedAggregationOperationContext(entityType, mappingContext, queryMapper);

        BasicQuery query = new BasicQuery(aggregation.toDbObject(collectionName, context));

        CloseableIterator<T> stream = this.mongoTemplate.stream(query, entityType);

        int count =0;
        if(stream.hasNext()){
            count++;
        }

        logger.debug(" [{}] total no of records fetched from collection [{}] ", collectionName);

        return StreamUtils.createStreamFromIterator(stream);

    }

}
