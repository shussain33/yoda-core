package com.softcell.service;

import com.softcell.gonogo.model.kyc.KycVerificationRequest;
import com.softcell.gonogo.model.kyc.request.*;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstAuthDetailRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstIdentyRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by anupamad on 13/7/17.
 */
public interface TotalKycManager {
    /**
     * Get LPG Details.
     * @param totalKycLpgDetailRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getLpgDetail(TotalKycLpgDetailRequest totalKycLpgDetailRequest) throws Exception;

    /**
     * Get Driving License Details.
     * @param totalKycDlDetailRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getDlDetail(TotalKycDlDetailRequest totalKycDlDetailRequest) throws Exception;

    /**
     * Get Electricity Details.
     * @param totalKycElecDtlRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getElecDetail(TotalKycElecDtlRequest totalKycElecDtlRequest) throws Exception;

    /**
     * Voter ID details.
     * @param totalKycVoterDetailRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getVoterDetail(TotalKycVoterDetailRequest totalKycVoterDetailRequest) throws Exception;

    public BaseResponse getLpgDetailV2(TotalKycLpgDetailRequestV2 totalKycLpgDetailRequestV2) throws Exception;

    public BaseResponse getDlDetailV2(TotalKycDlDetailRequestV2 totalKycDlDetailRequestV2) throws Exception;

    public BaseResponse getVoterDetailV2(TotalKycVoterDetailRequestV2 totalKycVoterDetailRequestV2) throws Exception;

    public BaseResponse getElecDetailV2(TotalKycElecDetailRequestV2 totalKycElecDetailRequestV2) throws Exception;

    public BaseResponse getGstAuthDetail(TotalKycGstAuthDetailRequest totalKycGstAuthDetailRequest) throws Exception;

    public BaseResponse getGstIdentityDetails(TotalKycGstIdentyRequest totalKycGstIdentyRequest) throws Exception;

    public BaseResponse getPanAuthDetail(TotalKycPanAuthDetailRequest totalKycPanAuthDetailRequest) throws Exception;

    public BaseResponse getKycDetailByKarza(ApplicationRequest applicationRequest, String kycType) throws Exception;

    BaseResponse verifyKYC(KycVerificationRequest kycVerificationRequest) throws Exception;
}
