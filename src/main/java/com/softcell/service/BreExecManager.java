package com.softcell.service;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.BreExecRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.springframework.http.HttpRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yogesh on 6/2/18.
 */
public interface BreExecManager {
    public BaseResponse verifyKyc(ApplicationRequest applicationRequest,HttpServletRequest httpServletRequest, String kycName);

    public BaseResponse callMultibureau(BreExecRequest breExecRequest, HttpServletRequest httpServletRequest);

    public BaseResponse executeSobre(BreExecRequest breExecRequest, HttpServletRequest httpServletRequest);

    public BaseResponse executeSobre(LoginDataRequest loginDataRequest, HttpServletRequest httpServletRequest);
}
