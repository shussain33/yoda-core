package com.softcell.service;

import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;

/**
 * Created by mahesh on 27/7/17.
 */
public interface DigitizationStoreManager {

    /**
     * @param invoiceDetailsRequest
     * @param fileName
     */
    String saveApplicationForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName);


    /**
     * @param invoiceDetailsRequest
     * @param fileName
     */
    String saveAgreementForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName);

    /**
     *
     * @param postIpaRequest
     * @return
     */
    String saveDeliveryOrderForm(PostIpaRequest postIpaRequest, PostIPA postIPA ,String fileOperationType) throws Exception;

    /**
     *
     * @param invoiceDetailsRequest
     * @param fileName
     * @return
     */
    String saveAchMandateForm(InvoiceDetailsRequest invoiceDetailsRequest ,String fileName);


}
