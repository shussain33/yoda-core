package com.softcell.service;

import com.softcell.constants.master.MasterDetails;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.model.request.MasterActivityDetails;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;

/**
 * @author yogeshb
 */
public interface FileUploadManager {
    /**
     * This service use to upload scheme master csv ,parse it it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadSchemesMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload AssetModelMaster csv ,parse it it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadAssetModelMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload uploadCityStateMaster csv ,parse it it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadCityStateMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload PinCode csv ,parse it it and store it in
     * mongodb with rename previous collection with collection name followed by
     * current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadPincodeMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload EmployerMaster csv ,parse it it and store it
     * in mongodb with rename previous collection with collection name followed
     * by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadEmployerMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload DealerEmailMaster csv ,parse it it and store
     * it in mongodb with rename previous collection with collection name
     * followed by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadDealerEmailMaster(File file, String institutionId) throws Exception;


    /**
     * This service use to upload gng cc/bcc email master csv ,parse it it and store
     * it in mongodb with rename previous collection with collection name
     * followed by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadGNGDealerEmailMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload SchemeModelDealerCityMapping csv ,parse it it
     * and store it in mongodb with rename previous collection with collection
     * name followed by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadSchemeModelDealerCityMappingMaster(File file, String institutionId) throws Exception;

    /**
     * This service use to upload SchemeDateMapping csv ,parse it it and store
     * it in mongodb with rename previous collection with collection name
     * followed by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadSchemeDateMappingMaster(File file, String institutionId) throws Exception;


    /**
     * This service use to upload CarSurrogateMaster csv ,parse it it and store
     * it in mongodb with rename previous collection with collection name
     * followed by current date .
     *
     * @param file
     * @return
     */
    BaseResponse uploadCarSurrogateMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @return
     */
    BaseResponse uploadPlPincodeEmailMaster(File file, String institutionId) throws Exception;

    /**
     * @param file to be saved or uploaded.
     * @return FileUploadStatus
     */
    BaseResponse uploadBankDetailsMaster(File file, String institutionId) throws Exception;

    /**
     * @param file to be saved or uploaded.
     * @return FileUploadStatus
     */
    BaseResponse uploadHierarchyMaster(File file, String institutionId) throws Exception;

    /**
     * @param file to be saved or uploaded.
     * @return FileUploadStatus
     */
    BaseResponse uploadCDLHierarchyMaster(File file, String institutionId) throws Exception;


    /**
     * @param file
     * @param institutionId
     * @return FileUploadStatus
     * @throws Exception { Generic exception }
     */
    BaseResponse uploadBranchMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @return
     */
    BaseResponse uploadVersionMaster(File file) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadModelVariantMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadAssetCategoryMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadStateMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadCityMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadCreditPromotionMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadDsaBranchMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadDsaProductMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     */
    BaseResponse uploadDealerBranchMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     * @throws Exception
     */
    BaseResponse uploadNegativeAreaGeoLimitMaster(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     * @throws Exception
     */
    BaseResponse uploadLosMasterDetails(File file, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     * @throws IOException
     */
    BaseResponse uploadRelianceDealerBranchMaster(File file, String institutionId) throws IOException;

    /**
     * @param uploadedFile
     * @param institutionId
     * @param product
     * @return
     * @throws IOException
     */
    BaseResponse uploadReferenceDetailsMaster(File uploadedFile, String institutionId, String product) throws IOException;

    /**
     * @param uploadedFile
     * @return
     */
    BaseResponse uploadBankingMaster(File uploadedFile, String institutionId) throws IOException;

    /**
     * @param uploadFile
     * @param institutionId
     * @return
     * @throws IOException
     */
    BaseResponse uploadLoyaltyCardMaster(File uploadFile, String institutionId) throws IOException;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     */
    BaseResponse uploadCommonGeneralParameterMaster(File uploadedFile, String institutionId) throws Exception;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     * @throws IOException
     */
    BaseResponse uploadLosBankingMaster(File uploadedFile, String institutionId) throws IOException;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     * @throws Exception
     */
    BaseResponse uploadEwInsGngAssetCategoryMaster(File uploadedFile, String institutionId) throws Exception;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     */
    BaseResponse uploadEwPremiumDataMaster(File uploadedFile, String institutionId) throws Exception;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     */
    BaseResponse uploadInsurancePremiumMaster(File uploadedFile, String institutionId) throws Exception;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     */
    BaseResponse uploadElecServProvMaster(File uploadedFile, String institutionId) throws Exception;

    /**
     * @param file
     * @param institutionId
     * @return
     * @throws IOException
     */
    BaseResponse uploadTkilDealerBranchMaster(File file, String institutionId) throws IOException;

    BaseResponse uploadSourcingDetailMaster(File uploadedFile, String institutionId) throws IOException;

    BaseResponse uploadPromotionalSchemeMaster(File uploadedFile, String institutionId) throws IOException;

    BaseResponse uploadTaxCodeMaster(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadAccountsHeadMaster(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadStateBranchMaster(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadBankMaster(File uploadedFile, String institutionId) throws IOException;

    BaseResponse uploadGenericMaster(File file, String institutionId, MasterDetails masterName) throws Exception;

    BaseResponse uploadTCLosGngMappingMaster(File file, String institutionId) throws Exception;

    BaseResponse uploadManufacturers(File file, String institutionId) throws Exception;

    BaseResponse updateNetDisbursementAmount(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadSupplierLocationMaster(File uploadedFile, String institutionId) throws IOException;

    /**
     * @param uploadedFile
     * @param institutionId
     * @return
     * @throws IOException
     */
    BaseResponse uploadPaynimoBankMaster(File uploadedFile, String institutionId) throws IOException;

    BaseResponse uploadDigitizationEmailMaster(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadDeviationMaster(File uploadedFile, String institutionId, String product) throws Exception;

    BaseResponse uploadOrganizationalHierarchyMaster(File uploadedFile, String institutionId) throws Exception;

    BaseResponse uploadPotentialCustomer(File uploadedFile, String institutionId, String product) throws Exception;

    BaseResponse uploadRoiSchemeMaster(File uploadedFile, String institutionId, String product) throws IOException;

    boolean checkMasterUploadRestriction(String institutionId, String masterName, FileUploadStatus fileUploadStatus);

    MasterActivityDetails masterActivityDetailsInitializer(String institutionId, String masterName, String emailId, String userName, String userRole, String deviceInfo);

    ResponseEntity<BaseResponse> isInvalidUpload(String institutionId, File uploadedFile, MasterActivityDetails masterActivityDetails);

    BaseResponse uploadApiRoleAuthorisationMaster(File file, String institutionId, MasterActivityDetails  masterActivityDetails,String sourceId) throws Exception;
}

