package com.softcell.service.parser;

import com.google.common.base.CharMatcher;
import com.softcell.constants.*;
import com.softcell.constants.csv.CSVHeaderConstant;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.VersionMaster;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.MasterRecords;
import com.softcell.gonogo.model.security.Hierarchy;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.service.DataEntryManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.RequestAggregator;
import jodd.util.CollectionUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class contains csv parsing logic and building objects.
 *
 * @author yogeshb
 */
@Component
public class MasterFileCSVParserImpl implements MasterFileCSVParser {

    @Autowired
    private DataEntryManager dataEntryManager;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    private static final Logger logger = LoggerFactory.getLogger(MasterFileCSVParserImpl.class);
    private static final CharSequence SPECIAL_CHAR = "\n\t";

    @Override
    public MasterRecords parseSchemeMaster(CSVParser parser, String institutionId) {
        return populateSchemeDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseAssetModelMaster(CSVParser parser,
                                               String institutionId) {
        return populateAssetModelDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parsePinCodeMaster(CSVParser parser,
                                            String institutionId) {
        return populatePinCodeDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseEmployerMaster(CSVParser parser,
                                             String institutionId) {
        return populateEmployerMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseDealerEmailMaster(CSVParser parser,
                                                String institutionId) {
        return populateDealerEmailMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseGNGDealerEmailMaster(CSVParser parser,
                                                   String institutionId) {
        return populateGNGDealerEmailMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseSchemeModelDealerCityMapping(CSVParser parser,
                                                           String institutionId) {
        return populateSchemeModelDealerCityMappingDomain(
                getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseHitachiSchemeMaster(CSVParser parser,
                                                  String institutionId) {
        return populateHitachiSchemeMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseSchemeDateMapping(CSVParser parser,
                                                String institutionId) {
        return populateSchemeDateMappingDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseCarSurrogateMaster(CSVParser parser,
                                                 String institutionId) {
        return populateCarSurrogateMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parsePlPincodeEmailMaster(CSVParser parser,
                                                   String institutionId) {
        return populatePlPincodeEmailMasterDomain(getRecordList(parser),
                institutionId);
    }

    public MasterRecords parseBankDetailsMaster(CSVParser parser,
                                                String institutionId) {
        return populateBankDetailsDomain(getRecordList(parser), institutionId);
    }

    public MasterRecords parseHierarchyMaster(CSVParser parser,
                                              String institutionId) {
        return populateHierarchyDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseCityStateMaster(CSVParser parser,
                                              String institutionId) {

        return populateCityStateMaster(getRecordList(parser), institutionId);
    }

    public MasterRecords parseCDLHierarchyMaster(CSVParser parser,
                                                 String institutionId) {
        return populateCDLHierarchyDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseVersionCofigurationMaster(CSVParser parser) {
        return populateVerisonMasterDomain(getRecordList(parser));
    }

    @Override
    public MasterRecords parseModelVariantMaster(CSVParser parser,
                                                 String institutionId) {
        return populateModelVariantDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseAssetCategoryMaster(CSVParser parser,
                                                  String institutionId) {
        return populateAssetCategoryDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseStateMaster(CSVParser parser, String institutionId) {
        return populateStateDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseCityMaster(CSVParser parser, String institutionId) {
        return populateCityDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseCreditPromotionMaster(CSVParser parser,
                                                    String institutionId) {
        return populateCreditPromotionDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseDsaBranchMaster(CSVParser parser,
                                              String institutionId) {
        return populateDsaBranchDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseDsaProductMaster(CSVParser parser,
                                               String institutionId) {
        return populateDsaProductDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseDealerBranchMaster(CSVParser parser,
                                                 String institutionId) {
        return populateDealerBranchDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseNegativeAreaGeoLimitMaster(CSVParser parser,
                                                         String institutionId) {

        return populateNegativeAreaGeoLimitDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseLosDetailsMaster(CSVParser parser, String institutionId) {
        return populateLosDetailsMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseNetDisbursalMaster(CSVParser parser, String institutionId) {
        return populateNetDisbursalMasterDomain(getRecordList(parser),
                institutionId);
    }

    @Override
    public MasterRecords parseRelianceDealerBranchMaster(CSVParser parser,
                                                         String institutionId) {
        return populateRelianceDealerBranchDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseReferenceDetailsMaster(CSVParser parser, String institutionId, String product) {
        return populateReferenceDetailsDomain(getRecordList(parser), institutionId, product);
    }

    @Override
    public MasterRecords parseEwInsGngAssetCategoryMaster(CSVParser parser, String institutionId) {
        return populateEwInsGngAssetCategoryMaster(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseInsurancePremiumMaster(CSVParser parser, String institutionId) {
        return populateInsurancePremiumMaster(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseElecServProvMaster(CSVParser parser, String institutionId) {
        return populateElecServProvMaster(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseBranchMaster(CSVParser parser, String institutionId) {
        return populateBranchMaster(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseSourcingDetailMaster(CSVParser parser, String institutionId) {
        return populateSourcingDetailDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseTkilDealerBranchMaster(CSVParser parser, String institutionId) {
        return populateTkilDealerBranchDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parsePromotionalSchemeMaster(CSVParser parser, String institutionId) {
        return populatePromotionalSchemeDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseBankMaster(CSVParser parser, String institutionId) {
        return populateBankDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parsePaynimoBankMaster(CSVParser parser, String institutionId) {
        return populatePaynimoBankMasterDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseDigitizationEmailMaster(CSVParser parser, String institutionId) {
        return populateDigitizationEmailMasterDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseDeviationMaster(CSVParser parser, String institutionId, String product) {
        return populateDeviationMasterDomain(getRecordList(parser), institutionId, product);
    }

    private MasterRecords populateDeviationMasterDomain(List<CSVRecord> recordList, String institutionId, String product) {

        String[] csvHeaders = CSVHeaderConstant.DEVIATION_MASTER.split(",");

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<DeviationMaster> deviationMasterArrayList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            DeviationMaster deviationMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                deviationMaster = new DeviationMaster();
                deviationMaster.setInstitutionId(institutionId);
                deviationMaster.setProduct(product);

                if (record != null && record.size() == csvHeaders.length) {

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                deviationMaster.setDeviationId(fieldValue);
                            }
                            break;
                            case 1: {
                                deviationMaster.setDeviation(fieldValue);
                            }
                            break;
                            case 2: {
                                deviationMaster.setCategory(fieldValue);
                            }
                            break;
                            case 3: {
                                String[] authorities = StringUtils.stripAll(fieldValue.split(","));
                                deviationMaster.setAuthorities(Arrays.asList(authorities));
                            }
                            break;
                            case 4: {
                                deviationMaster.setLevel(fieldValue);
                            }
                            break;
                            case 5: {
                                deviationMaster.setMitigation(fieldValue);
                            }
                            break;
                            case 6: {
                                deviationMaster.setSeverity(fieldValue);
                            }
                            break;
                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                deviationMasterArrayList.add(deviationMaster);
            }
        }
        masterRecords.setMasterList(deviationMasterArrayList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateDigitizationEmailMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<DigitizationDealerEmailMaster> digitizationEmailMasters = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            DigitizationDealerEmailMaster digitizationDealerEmailMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                digitizationDealerEmailMaster = new DigitizationDealerEmailMaster();

                if (record != null && record.size() == header.length) {

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                digitizationDealerEmailMaster.setSupplierID(fieldValue);
                            }
                            break;
                            case 1: {
                                digitizationDealerEmailMaster.setSupplierDesc(fieldValue);
                            }
                            break;
                            case 2: {
                                if (StringUtils.isNotBlank(fieldValue)) {

                                    Set<String> emailIdsSet = new HashSet<>();
                                    if (StringUtils.contains(fieldValue, ",")) {
                                        emailIdsSet = new HashSet<>(Arrays.asList(fieldValue.split(",")));
                                    } else {
                                        emailIdsSet.add(fieldValue);
                                    }
                                    digitizationDealerEmailMaster.setDeliveryOrderCcEmailIds(emailIdsSet);
                                }
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                digitizationDealerEmailMaster.setInstitutionId(institutionId);
                digitizationEmailMasters.add(digitizationDealerEmailMaster);
            }
        }
        masterRecords.setMasterList(digitizationEmailMasters);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }


    @Override
    public MasterRecords parseBankingMaster(HSSFWorkbook wb, String institutionId) {
        List<BankDetailsMaster> bankDetailsMasters = new ArrayList<>();
        long numberOfRows = 0;
        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            HSSFSheet sheet = wb.getSheetAt(i);
            int physicalNumberOfRows = sheet.getPhysicalNumberOfRows() - 1;
            Iterator<Row> rowItr = sheet.iterator();
            rowItr.next();
            while (rowItr.hasNext()) {
                HSSFRow row = (HSSFRow) rowItr.next();
                BankDetailsMaster bankDetailsMaster = getAllColumnFromRow(row, institutionId);
                bankDetailsMasters.add(bankDetailsMaster);
            }
            numberOfRows += physicalNumberOfRows;
        }

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        fileUploadStatus.setNumberOfRecords(numberOfRows);
        fileUploadStatus.setNumberOfRecordSuccess(numberOfRows);
        fileUploadStatus.setStatus(Status.SUCCESS.name());

        MasterRecords masterRecords = new MasterRecords();
        masterRecords.setMasterList(bankDetailsMasters);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private BankDetailsMaster getAllColumnFromRow(HSSFRow RowObject, String institutionId) {
        BankDetailsMaster bankDetailsMaster = new BankDetailsMaster();
        Iterator<Cell> itr = RowObject.iterator();
        HSSFRow headerRow = RowObject.getSheet().getRow(0);
        int headerCellCount = 0;

        while (itr.hasNext()) {
            Cell cell = itr.next();
            Cell headerValue = headerRow.getCell(headerCellCount++);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            switch (headerValue.getColumnIndex()) {
                case 0: {

                    bankDetailsMaster.setBankName(cell.getStringCellValue());

                }
                break;
                case 1: {

                    bankDetailsMaster.setIfscCode(cell.getStringCellValue());

                }
                break;
                case 2: {
                    bankDetailsMaster.setMicrCode(cell.getStringCellValue());

                }
                break;
                case 3: {
                    bankDetailsMaster.setBranch(cell.getStringCellValue());

                }
                break;
                case 4: {
                    bankDetailsMaster.setAddress(cell.getStringCellValue());

                }
                break;
                case 5: {
                    bankDetailsMaster.setContact(String.valueOf(cell.getStringCellValue()));

                }
                break;
                case 6: {
                    bankDetailsMaster.setCity(cell.getStringCellValue());

                }
                break;
                case 7: {
                    bankDetailsMaster.setDistrict(cell.getStringCellValue());

                }
                break;
                case 8: {
                    bankDetailsMaster.setState(cell.getStringCellValue());

                }
                break;
            }
        }
        bankDetailsMaster.setActive(true);
        bankDetailsMaster.setInstitutionId(institutionId);
        bankDetailsMaster.setDate(new Date());
        return bankDetailsMaster;
    }

    @Override
    public MasterRecords parseLoyaltyCardMaster(CSVParser parser, String institutionId) {
        return populateLoyaltyCardMasterDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseGeneralParameterMaster(CSVParser parser, String institutionId) {
        return populateGeneralParameterMasterDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseBankDetailsCsvMaster(CSVParser parser, String institutionId) {
        return populateBankDetailsDomainFromCsv(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseLosBankMaster(CSVParser parser, String institutionId) {
        return populateLosBankMasterDomain(getRecordList(parser), institutionId);
    }

    @Override
    public MasterRecords parseEwPremiumDataMaster(CSVParser parser, String institutionId) {
        return populateEwPremiumDataMasterDomain(getRecordList(parser), institutionId);
    }

    private MasterRecords populateInsurancePremiumMaster(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.INSURANCE_PREMIUM_MASTER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<InsurancePremiumMaster> insurancePremiumMasterList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            InsurancePremiumMaster insurancePremiumMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    insurancePremiumMaster = new InsurancePremiumMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                insurancePremiumMaster.setInsuranceAssetCat(fieldValue);
                            }
                            break;
                            case 1: {
                                insurancePremiumMaster.setMinAssetValue(parseStringToDouble(fieldValue));
                            }
                            break;
                            case 2: {

                                insurancePremiumMaster.setMaxAssetValue(parseStringToDouble(fieldValue));
                            }
                            break;
                            case 3: {
                                insurancePremiumMaster.setInsurancePremium(parseStringToDouble(fieldValue));
                            }
                            break;

                            case 4: {
                                insurancePremiumMaster.setProductFlag(fieldValue);
                            }
                            break;

                            case 5: {
                                insurancePremiumMaster.setInsuranceType(fieldValue);
                            }
                            break;

                            case 6: {
                                insurancePremiumMaster.setInsuranceTypeDesc(fieldValue);
                            }
                            break;
                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                insurancePremiumMaster.setInstitutionId(institutionId);
                insurancePremiumMasterList.add(insurancePremiumMaster);
            }
        }
        masterRecords.setMasterList(insurancePremiumMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;


    }

    private MasterRecords populateElecServProvMaster(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.ELEC_SERV_PROV_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<ElecServProvMaster> elecServProvMasterList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {
            ElecServProvMaster elecServProvMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    elecServProvMaster = new ElecServProvMaster();
                    for (int index = 0; index < record.size(); index++) {
                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());
                        switch (index) {
                            case 0: {
                                elecServProvMaster.setCode(fieldValue);
                            }
                            break;
                            case 1: {
                                elecServProvMaster.setName(fieldValue);
                            }
                            break;
                            case 2: {

                                elecServProvMaster.setState(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                elecServProvMaster.setInstitutionId(institutionId);
                elecServProvMasterList.add(elecServProvMaster);
            }
        }
        masterRecords.setMasterList(elecServProvMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateEwPremiumDataMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.EW_PREMIUM_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<EWPremiumMaster> ewPremiumMasterList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            EWPremiumMaster ewPremiumMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    ewPremiumMaster = new EWPremiumMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                ewPremiumMaster.setEwAssetCategory(fieldValue);
                            }
                            break;
                            case 1: {
                                ewPremiumMaster.setMinAssetValue(parseStringToDouble(fieldValue));
                            }
                            break;
                            case 2: {

                                ewPremiumMaster.setMaxAssetValue(parseStringToDouble(fieldValue));
                            }
                            break;
                            case 3: {
                                ewPremiumMaster.setOneYearPremium(parseStringToDouble(fieldValue));
                            }
                            break;

                            case 4: {
                                ewPremiumMaster.setTwoYearPremium(parseStringToDouble(fieldValue));
                            }
                            break;

                            case 5: {
                                ewPremiumMaster.setThreeYearPremium(parseStringToDouble(fieldValue));
                            }
                            break;

                            case 6: {
                                ewPremiumMaster.setProductFlag(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                ewPremiumMaster.setInstitutionId(institutionId);
                ewPremiumMasterList.add(ewPremiumMaster);
            }
        }
        masterRecords.setMasterList(ewPremiumMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;


    }

    private double parseStringToDouble(String stringValue) {
        double doubleValue;
        try {
            doubleValue = Double.parseDouble(stringValue);
        } catch (Exception e) {
            doubleValue = 0;
        }
        return doubleValue;
    }

    private MasterRecords populateEwInsGngAssetCategoryMaster(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.EW_INS_GNG_ASSET_CAT_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<EwInsGngAssetCategoryMaster> ewInsGngAssetCategoryMasterList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            EwInsGngAssetCategoryMaster ewInsGngAssetCategoryMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    ewInsGngAssetCategoryMaster = new EwInsGngAssetCategoryMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                ewInsGngAssetCategoryMaster.setInsuranceAssetCat(fieldValue);
                            }
                            break;
                            case 1: {
                                ewInsGngAssetCategoryMaster.setExtendedWarrantyAssetCat(fieldValue);
                            }
                            break;
                            case 2: {
                                ewInsGngAssetCategoryMaster.setGngAssetCat(fieldValue);
                            }
                            break;
                            case 3: {
                                ewInsGngAssetCategoryMaster.setManufacturerWarranty(parseStringToDouble(fieldValue));
                            }
                            break;

                            case 4: {
                                ewInsGngAssetCategoryMaster.setProductFlag(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                ewInsGngAssetCategoryMaster.setInstitutionId(institutionId);
                ewInsGngAssetCategoryMasterList.add(ewInsGngAssetCategoryMaster);
            }
        }
        masterRecords.setMasterList(ewInsGngAssetCategoryMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;


    }


    private MasterRecords populateGeneralParameterMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.COMMON_GENERAL_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<CommonGeneralParameterMaster> generalParameterMastersList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            CommonGeneralParameterMaster commonGeneralParameterMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    commonGeneralParameterMaster = new CommonGeneralParameterMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                commonGeneralParameterMaster.setBranchId(fieldValue);
                            }
                            break;
                            case 2: {
                                commonGeneralParameterMaster.setMakerId(fieldValue);
                            }
                            break;
                            case 3: {

                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date makeDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy HH:mm:ss a", fieldValue);
                                    if (null != makeDate) {
                                        commonGeneralParameterMaster.setMakeDate(makeDate);
                                    }
                                }
                            }
                            break;
                            case 4: {
                                commonGeneralParameterMaster.setAuthId(fieldValue);
                            }
                            break;

                            case 5: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date authDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy HH:mm:ss a", fieldValue);
                                    if (null != authDate) {
                                        commonGeneralParameterMaster.setAuthDate(authDate);
                                    }
                                }
                            }
                            break;

                            case 6: {
                                commonGeneralParameterMaster.setModuleId(fieldValue);
                            }
                            break;
                            case 7: {
                                commonGeneralParameterMaster.setKey1(fieldValue);
                            }
                            break;
                            case 8: {
                                commonGeneralParameterMaster.setKey2(fieldValue);
                            }
                            break;
                            case 9: {
                                commonGeneralParameterMaster.setValue(fieldValue);
                            }
                            break;
                            case 10: {
                                commonGeneralParameterMaster.setDescription(fieldValue);
                            }
                            break;
                            case 11: {
                                commonGeneralParameterMaster.setStatus(fieldValue);
                            }
                            break;
                            case 12: {
                                commonGeneralParameterMaster.setModuleFlag(fieldValue);
                            }
                            break;
                            case 13: {
                                commonGeneralParameterMaster.setAppFlag(fieldValue);
                            }
                            break;
                            case 14: {
                                commonGeneralParameterMaster.setDisableFlag(fieldValue);
                            }
                            break;
                            case 15: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date cgpStartDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy", fieldValue);
                                    if (null != cgpStartDate) {
                                        commonGeneralParameterMaster.setCgpStartDate(cgpStartDate);
                                    }
                                }
                            }
                            break;
                            case 16: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date cgpEndDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy", fieldValue);
                                    if (null != cgpEndDate) {
                                        commonGeneralParameterMaster.setCgpEndDate(cgpEndDate);
                                    }
                                }
                            }
                            break;
                            case 17: {
                                commonGeneralParameterMaster.setmApplyFlag(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                commonGeneralParameterMaster.setInstitutionId(institutionId);
                commonGeneralParameterMaster.setInsertDate(new Date());
                generalParameterMastersList.add(commonGeneralParameterMaster);
            }
        }
        masterRecords.setMasterList(generalParameterMastersList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }


    private MasterRecords populateLosBankMasterDomain(List<CSVRecord> recordList, String institutionId) {
        String[] header = CSVHeaderConstant.LOS_BANK_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<LosBankMaster> losBankMasters = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            LosBankMaster losBankMaster;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                losBankMaster = new LosBankMaster();

                if (record != null && record.size() == header.length) {

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                losBankMaster.setBankId(fieldValue);
                            }
                            break;
                            case 2: {
                                losBankMaster.setBankName(fieldValue);
                            }
                            break;
                            case 3: {
                                losBankMaster.setBankCode(fieldValue);
                            }
                            break;
                            case 4: {
                                losBankMaster.setCityId(fieldValue);
                            }
                            break;
                            case 5: {
                                losBankMaster.setCity(fieldValue);
                            }
                            break;
                            case 6: {
                                losBankMaster.setBankBranchId(fieldValue);
                            }
                            break;
                            case 7: {
                                losBankMaster.setBankBranchName(fieldValue);
                            }
                            break;
                            case 8: {
                                losBankMaster.setMicrFlag(fieldValue);
                            }
                            break;
                            case 9: {
                                losBankMaster.setMicrCode(fieldValue);
                            }
                            break;
                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                losBankMaster.setStatus(true);
                losBankMaster.setInstitutionId(institutionId);
                losBankMaster.setDateTime(new Date());
                losBankMasters.add(losBankMaster);
            }
        }
        masterRecords.setMasterList(losBankMasters);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateBankDetailsDomainFromCsv(List<CSVRecord> recordList, String institutionId) {
        String[] header = CSVHeaderConstant.RBI_BANK_DETAILS_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<BankDetailsMaster> bankDetailsMasters = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            BankDetailsMaster bankDetailsMaster;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                bankDetailsMaster = new BankDetailsMaster();

                if (record != null && record.size() == header.length) {

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                bankDetailsMaster.setBankName(fieldValue);
                            }
                            break;
                            case 1: {
                                bankDetailsMaster.setIfscCode(fieldValue);
                            }
                            break;
                            case 2: {
                                bankDetailsMaster.setMicrCode(fieldValue);
                            }
                            break;
                            case 3: {
                                bankDetailsMaster.setBranch(fieldValue);
                            }
                            break;
                            case 4: {
                                bankDetailsMaster.setAddress(fieldValue);
                            }
                            break;
                            case 5: {
                                bankDetailsMaster.setContact(fieldValue);
                            }
                            break;
                            case 6: {
                                bankDetailsMaster.setCity(fieldValue);
                            }
                            break;
                            case 7: {
                                bankDetailsMaster.setDistrict(fieldValue);
                            }
                            break;
                            case 8: {
                                bankDetailsMaster.setState(fieldValue);
                            }
                            break;
                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                bankDetailsMaster.setActive(true);
                bankDetailsMaster.setInstitutionId(institutionId);
                bankDetailsMaster.setDate(new Date());
                bankDetailsMasters.add(bankDetailsMaster);
            }
        }
        masterRecords.setMasterList(bankDetailsMasters);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateLoyaltyCardMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.LOYALTY_CARD_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<LoyaltyCardMaster> loyaltyCardMastersList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            LoyaltyCardMaster loyaltyCardMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                loyaltyCardMaster = new LoyaltyCardMaster();

                if (record != null && record.size() == header.length) {

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                loyaltyCardMaster.setLoyaltyCardNo(fieldValue);
                            }
                            break;
                            case 1: {
                                loyaltyCardMaster.setCardStatus(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                loyaltyCardMaster.setInstitutionId(institutionId);
                loyaltyCardMaster.setDateTime(new Date());
                loyaltyCardMastersList.add(loyaltyCardMaster);
            }
        }
        masterRecords.setMasterList(loyaltyCardMastersList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateReferenceDetailsDomain(List<CSVRecord> recordList, String institutionId, String product) {

        String[] header = CSVHeaderConstant.REFERENCE_DETAILS_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<ReferenceDetailsMaster> referenceDetailsMasters = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            ReferenceDetailsMaster referenceDetailsMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    referenceDetailsMaster = new ReferenceDetailsMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 6: {
                                referenceDetailsMaster.setModuleId(fieldValue);
                            }
                            break;
                            case 7: {
                                referenceDetailsMaster.setKey1(fieldValue);
                            }
                            break;
                            case 8: {
                                referenceDetailsMaster.setKey2(fieldValue);
                            }
                            break;
                            case 9: {
                                referenceDetailsMaster.setValue(fieldValue);
                            }
                            break;
                            case 10: {
                                referenceDetailsMaster.setDescription(fieldValue);
                            }
                            break;


                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                referenceDetailsMaster.setInstitutionId(institutionId);
                referenceDetailsMaster.setDateTime(new Date());
                referenceDetailsMaster.setProduct(product);
                referenceDetailsMasters.add(referenceDetailsMaster);
            }
        }
        masterRecords.setMasterList(referenceDetailsMasters);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private List<CSVRecord> getRecordList(CSVParser parser) {
        List<CSVRecord> recordList = new ArrayList<>();
        for (CSVRecord record : parser) {
            recordList.add(record);
        }
        return recordList;
    }


    private MasterRecords populateNetDisbursalMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.LOS_MASTER_UPLOAD_HEADER
                .split(",");
        List<NetDisbursalDetailsMaster> netDisbursalDetailsMasterList = new ArrayList<>();
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        int successRecords = 0;
        if (recordList != null) {
            NetDisbursalDetailsMaster netDisbursalDetailsMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    netDisbursalDetailsMaster = new NetDisbursalDetailsMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        if (index == 5 && StringUtils.isBlank(fieldValue.trim())) {

                            fileUploadStatus.setStatus(Status.ERROR.name());
                            fileUploadStatus.setErrorDesc("Row no:" + " " + (record.getRecordNumber() + 1) + " " + "and Column no :" + " " + (index + 1) + " " + "position value should not blank in the csv file");
                            masterRecords.setFileUploadStatus(fileUploadStatus);
                            return masterRecords;

                        } else {
                            switch (index) {
                                case 0: {
                                    netDisbursalDetailsMaster.setRefID(fieldValue);
                                }
                                break;
                                case 1: {
                                    netDisbursalDetailsMaster.setPersonalMobile(fieldValue);
                                }
                                break;
                                case 2: {
                                    netDisbursalDetailsMaster.setLosID(fieldValue);
                                }
                                break;
                                case 3: {
                                    netDisbursalDetailsMaster.setUtrNumber(fieldValue);
                                }
                                break;
                                case 4: {
                                    netDisbursalDetailsMaster.setLosStatus(fieldValue);
                                }
                                break;
                                case 5: {
                                    double netDisbursalAmt = StringUtils.isNumeric(fieldValue) ? Double.parseDouble(fieldValue) : 0;
                                    netDisbursalDetailsMaster.setNewNetDisbursalAmt(netDisbursalAmt);
                                }
                                break;

                            }
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                netDisbursalDetailsMaster.setInstitutionID(institutionId);
                netDisbursalDetailsMaster.setDateTime(new Date());
                netDisbursalDetailsMaster.setProduct("CDL");
                netDisbursalDetailsMasterList.add(netDisbursalDetailsMaster);
            }

        }

        masterRecords.setMasterList(netDisbursalDetailsMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateLosDetailsMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.LOS_MASTER_UPLOAD_HEADER
                .split(",");
        List<LosUtrDetailsMaster> losUtrDetailsMasterList = new ArrayList<>();
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        int successRecords = 0;
        if (recordList != null) {
            LosUtrDetailsMaster losUtrDetailsMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    losUtrDetailsMaster = new LosUtrDetailsMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        if (StringUtils.isBlank(fieldValue.trim()) && index != 1) {

                            fileUploadStatus.setStatus(Status.ERROR.name());
                            fileUploadStatus.setErrorDesc("Row no:" + " " + (record.getRecordNumber() + 1) + " " + "and Column no :" + " " + (index + 1) + " " + "position value should not blank in the csv file");
                            masterRecords.setFileUploadStatus(fileUploadStatus);
                            return masterRecords;

                        } else {
                            switch (index) {
                                case 0: {
                                    losUtrDetailsMaster.setRefID(fieldValue);
                                }
                                break;
                                case 1: {
                                    losUtrDetailsMaster.setPersonalMobile(fieldValue);
                                }
                                break;
                                case 2: {
                                    losUtrDetailsMaster.setLosID(fieldValue);
                                }
                                break;
                                case 3: {
                                    losUtrDetailsMaster.setUtrNumber(fieldValue);
                                }
                                break;
                                case 4: {
                                    losUtrDetailsMaster.setLosStatus(fieldValue);
                                }
                                break;
                                case 5: {
                                    double netDisbursalAmt = StringUtils.isNumeric(fieldValue) ? Double.parseDouble(fieldValue) : 0;
                                    losUtrDetailsMaster.setNetDisbursalAmt(netDisbursalAmt);
                                }
                                break;

                            }
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                losUtrDetailsMaster.setInstitutionID(institutionId);
                losUtrDetailsMaster.setDateTime(new Date());
                losUtrDetailsMaster.setProduct("CDL");
                losUtrDetailsMasterList.add(losUtrDetailsMaster);
            }

        }

        masterRecords.setMasterList(losUtrDetailsMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateNegativeAreaGeoLimitDomain(
            List<CSVRecord> recordList, String institutionId) {


        String[] header = CSVHeaderConstant.NEGATIVE_AREA_GEO_LIMIT_MASTER_HEADER
                .split(",");
        List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasterList = new ArrayList<>();
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        int successRecords = 0;
        if (recordList != null) {
            NegativeAreaGeoLimitMaster negativeAreaGeoLimitMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    negativeAreaGeoLimitMaster = new NegativeAreaGeoLimitMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0: {
                                negativeAreaGeoLimitMaster.setBranchCode(fieldValue);
                            }
                            break;
                            case 1: {
                                negativeAreaGeoLimitMaster.setBranchName(fieldValue);
                            }

                            case 2: {
                                negativeAreaGeoLimitMaster.setPinCode(fieldValue);
                            }
                            break;
                            case 3: {
                                negativeAreaGeoLimitMaster.setCity(fieldValue);
                            }
                            break;
                            case 4: {
                                negativeAreaGeoLimitMaster.setArea(fieldValue);
                            }
                            break;
                            case 5: {
                                negativeAreaGeoLimitMaster.setAreaType(fieldValue);
                            }
                            break;
                            case 6: {
                                negativeAreaGeoLimitMaster.setReason(fieldValue);
                            }
                            break;

                            case 7: {
                                negativeAreaGeoLimitMaster.setIsNegative(fieldValue);
                            }
                            break;

                            case 8: {
                                negativeAreaGeoLimitMaster.setIsGeoAreaActive(fieldValue);
                            }
                            break;

                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                negativeAreaGeoLimitMaster.setInstitutionId(institutionId);
                negativeAreaGeoLimitMaster.setActive(true);
                negativeAreaGeoLimitMaster.setInsertDate(new Date());
                negativeAreaGeoLimitMaster.setProductName("CDL");
                negativeAreaGeoLimitMasterList.add(negativeAreaGeoLimitMaster);
            }

        }
        masterRecords.setMasterList(negativeAreaGeoLimitMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateVerisonMasterDomain(List<CSVRecord> recordList) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.VERSION_MASTER_HEADER.split(",");
        List<VersionMaster> versionMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            VersionMaster versionMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    versionMaster = new VersionMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {

                                versionMaster.setInstition(fieldValue);

                            }
                            break;
                            case 1: {
                                versionMaster.setVersion(fieldValue);
                            }
                            break;
                            case 2: {
                                versionMaster.setAction(fieldValue);
                            }
                            break;

                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                versionMasterList.add(versionMaster);
            }
        }
        masterRecords.setMasterList(versionMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    /**
     * @param recordList
     * @return
     */
    private MasterRecords populateCityStateMaster(List<CSVRecord> recordList, String institutionId) {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        MasterRecords masterRecords = new MasterRecords();
        String[] header = CSVHeaderConstant.CITY_STATE_MASTER_HEADER.split(",");
        List<CityStateMappingMaster> cityStateMappingMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            CityStateMappingMaster cityStateMappingMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    cityStateMappingMaster = new CityStateMappingMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0: {
                                cityStateMappingMaster.setSrNo(fieldValue);
                            }
                            break;
                            case 1: {
                                cityStateMappingMaster.setCityName(fieldValue);
                            }
                            break;
                            case 2: {
                                cityStateMappingMaster.setStateId(fieldValue);
                            }
                            break;
                            case 3: {
                                cityStateMappingMaster.setStateDesc(fieldValue);
                            }
                            break;
                            case 4: {
                                cityStateMappingMaster.setCityId(fieldValue);
                            }
                            break;

                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                cityStateMappingMaster.setInstitutionId(institutionId);
                cityStateMappingMaster.setActive(true);
                cityStateMappingMaster.setInsertDate(new Date());
                cityStateMappingMaster.setProductName("CDL");
                cityStateMappingMasterList.add(cityStateMappingMaster);
            }

        }
        masterRecords.setMasterList(cityStateMappingMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    /**
     * @param recordList
     * @return
     */
    private MasterRecords populatePlPincodeEmailMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.PL_PINCODE_EMAIL_MASTER.split(",");
        List<PlPincodeEmailMaster> plPincodeEmailMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            PlPincodeEmailMaster plPincodeEmailMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    plPincodeEmailMaster = new PlPincodeEmailMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                plPincodeEmailMaster.setSrNo(fieldValue);
                            }
                            break;
                            case 1: {
                                plPincodeEmailMaster.setPincodeMaster(fieldValue);
                            }
                            break;
                            case 2: {
                                plPincodeEmailMaster.setProduct(fieldValue);
                            }
                            break;
                            case 3: {
                                plPincodeEmailMaster.setApplicantType(fieldValue);
                            }
                            break;
                            case 4: {
                                plPincodeEmailMaster.setConstitution(fieldValue);
                            }
                            break;
                            case 5: {
                                plPincodeEmailMaster.setPincode(fieldValue);
                            }
                            break;
                            case 6: {
                                plPincodeEmailMaster.setBranchId(fieldValue);
                            }
                            break;
                            case 7: {
                                plPincodeEmailMaster.setBranchName(fieldValue);
                            }
                            break;
                            case 8: {
                                plPincodeEmailMaster.setFprName(fieldValue);
                            }
                            break;
                            case 9: {
                                plPincodeEmailMaster.setFprMailId(fieldValue);
                            }
                            break;
                            case 10: {
                                plPincodeEmailMaster.setFprMobileNumber(fieldValue);
                            }
                            break;
                            case 11: {
                                plPincodeEmailMaster.setSecondFprName(fieldValue);
                            }
                            break;
                            case 12: {
                                plPincodeEmailMaster.setSecondFprMailId(fieldValue);
                            }
                            break;
                            case 13: {
                                plPincodeEmailMaster
                                        .setSecondFprMobileNumber(fieldValue);
                            }
                            break;

                            case 14: {
                                plPincodeEmailMaster.setUserId(fieldValue);
                            }
                            break;

                            case 15: {
                                plPincodeEmailMaster.setCity(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                plPincodeEmailMaster.setInstitutionID(institutionId);
                plPincodeEmailMaster.setActive(true);
                plPincodeEmailMaster.setDate(new Date());
                plPincodeEmailMasterList.add(plPincodeEmailMaster);
            }
        }
        masterRecords.setMasterList(plPincodeEmailMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateCarSurrogateMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        MasterRecords masterRecords = new MasterRecords();
        String[] header = CSVHeaderConstant.CAR_SURROGATE_MASTER_HEADER
                .split(",");
        List<CarSurrogateMaster> carSurrogateMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            CarSurrogateMaster carSurrogateMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    carSurrogateMaster = new CarSurrogateMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0: {
                                carSurrogateMaster.setSegment(fieldValue);
                            }
                            break;

                            case 1: {
                                carSurrogateMaster.setClassification(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                carSurrogateMaster.setInstitutionID(institutionId);
                carSurrogateMaster.setActive(true);
                carSurrogateMaster.setDate(new Date());
                carSurrogateMasterList.add(carSurrogateMaster);
            }
        }
        masterRecords.setMasterList(carSurrogateMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }


    private MasterRecords populateHitachiSchemeMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        MasterRecords masterRecords = new MasterRecords();
        String[] header = CSVHeaderConstant.HITACHI_SCHEME_MASTER_HEADER
                .split(",");
        List<HitachiSchemeMaster> hitachiSchemeMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            HitachiSchemeMaster hitachiSchemeMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    hitachiSchemeMaster = new HitachiSchemeMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0: {
                                hitachiSchemeMaster.setSupplierID(fieldValue);
                            }
                            break;

                            case 1: {
                                hitachiSchemeMaster.setSchemeID(fieldValue);
                            }
                            break;
                            case 2: {
                                hitachiSchemeMaster.setSchemeDesc(fieldValue);
                            }
                            break;
                            case 3: {
                                hitachiSchemeMaster.setCurrencyID(fieldValue);
                            }
                            break;
                            case 4: {
                                hitachiSchemeMaster.setMaxTenure(fieldValue);
                            }
                            break;
                            case 5: {
                                hitachiSchemeMaster.setMinTenure(fieldValue);
                            }
                            break;
                            case 6: {
                                hitachiSchemeMaster.setSdAmt(fieldValue);
                            }
                            break;
                            case 7: {
                                hitachiSchemeMaster.setSdInt(fieldValue);
                            }
                            break;
                            case 8: {
                                hitachiSchemeMaster.setMinAmtFin(fieldValue);
                            }
                            break;
                            case 9: {
                                hitachiSchemeMaster.setCustMinAmtFin(fieldValue);
                            }
                            break;
                            case 10: {
                                hitachiSchemeMaster.setTotalBd(fieldValue);
                            }
                            break;
                            case 11: {
                                hitachiSchemeMaster.setInsertDate(fieldValue);
                            }
                            break;
                            case 12: {
                                hitachiSchemeMaster.setSchemeStartDate(fieldValue);
                            }
                            break;
                            case 13: {
                                hitachiSchemeMaster.setSchemeEndDate(fieldValue);
                            }
                            break;
                            case 14: {
                                hitachiSchemeMaster.setMakerID(fieldValue);
                            }
                            break;
                            case 15: {
                                hitachiSchemeMaster.setMakeDate(fieldValue);
                            }
                            break;
                            case 16: {
                                hitachiSchemeMaster.setAuthID(fieldValue);
                            }
                            break;
                            case 17: {
                                hitachiSchemeMaster.setAuthDate(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                hitachiSchemeMaster.setInstitutionID(institutionId);
                hitachiSchemeMaster.setActive(true);
                hitachiSchemeMasterList.add(hitachiSchemeMaster);
            }
        }

        masterRecords.setMasterList(hitachiSchemeMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateSchemeDateMappingDomain(
            List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.SCHEME_DATE_MAPPING_HEADER
                .split(",");
        List<SchemeDateMapping> schemeDateMappingList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            SchemeDateMapping schemeDateMapping = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    schemeDateMapping = new SchemeDateMapping();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                schemeDateMapping.setSchemeID(fieldValue);
                            }
                            break;
                            case 1: {
                                schemeDateMapping.setValidFromDate(fieldValue);
                            }
                            break;
                            case 2: {
                                schemeDateMapping.setValidToDate(fieldValue);
                            }
                            break;
                            case 3: {
                                schemeDateMapping.setExcludedDate(fieldValue);
                            }
                            break;
                            case 4: {
                                schemeDateMapping.setIncludedDate(fieldValue);
                            }
                            break;
                            case 5: {
                                schemeDateMapping.setInsertDate(fieldValue);
                            }
                            break;
                            case 6: {
                                schemeDateMapping.setMakerID(fieldValue);
                            }
                            break;
                            case 7: {
                                schemeDateMapping.setMakeDate(fieldValue);
                            }
                            break;
                            case 8: {
                                schemeDateMapping.setAuthID(fieldValue);
                            }
                            break;
                            case 9: {
                                schemeDateMapping.setAuthDate(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                schemeDateMapping.setInstitutionID(institutionId);
                schemeDateMapping.setActive(true);
                schemeDateMappingList.add(schemeDateMapping);
            }
        }
        masterRecords.setMasterList(schemeDateMappingList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateSchemeModelDealerCityMappingDomain(
            List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.SCHEME_MODEL_DEALER_CITY_MAPPING_HEADER
                .split(",");
        List<SchemeModelDealerCityMapping> schemeModelDealerCityMappingList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            SchemeModelDealerCityMapping schemeModelDealerCityMapping = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    schemeModelDealerCityMapping = new SchemeModelDealerCityMapping();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                schemeModelDealerCityMapping.setSchemeID(fieldValue);
                            }
                            break;
                            case 1: {
                                schemeModelDealerCityMapping.setModelID(fieldValue);
                            }
                            break;
                            case 2: {
                                schemeModelDealerCityMapping.setValidity(fieldValue);
                            }
                            break;
                            case 3: {
                                schemeModelDealerCityMapping.setStateID(fieldValue);
                            }
                            break;
                            case 4: {
                                schemeModelDealerCityMapping
                                        .setIncludedStateID(fieldValue);
                            }
                            break;
                            case 5: {
                                schemeModelDealerCityMapping
                                        .setExcludedStateID(fieldValue);
                            }
                            break;
                            case 6: {
                                schemeModelDealerCityMapping.setCityID(fieldValue);
                            }
                            break;
                            case 7: {
                                schemeModelDealerCityMapping
                                        .setIncludedCityID(fieldValue);
                            }
                            break;
                            case 8: {
                                schemeModelDealerCityMapping
                                        .setExcludedCityID(fieldValue);
                            }
                            break;
                            case 9: {
                                schemeModelDealerCityMapping.setDealerId(fieldValue);
                            }
                            break;
                            case 10: {
                                schemeModelDealerCityMapping
                                        .setDealerInclusion(GngUtils
                                                .getDealerList(fieldValue));
                            }
                            break;
                            case 11: {
                                schemeModelDealerCityMapping
                                        .setExcludedDealerID(fieldValue);
                            }
                            break;
                            case 12: {
                                schemeModelDealerCityMapping.setInsertDate(fieldValue);
                            }
                            break;
                            case 13: {
                                schemeModelDealerCityMapping.setMakerID(fieldValue);
                            }
                            break;
                            case 14: {
                                schemeModelDealerCityMapping.setAuthID(fieldValue);
                            }
                            break;
                            case 15: {
                                schemeModelDealerCityMapping.setAuthDate(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                schemeModelDealerCityMapping.setInstitutionID(institutionId);
                schemeModelDealerCityMapping.setActive(true);
                schemeModelDealerCityMappingList
                        .add(schemeModelDealerCityMapping);
            }
        }
        masterRecords.setMasterList(schemeModelDealerCityMappingList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateDealerEmailMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.DEALER_EMAIL_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<DealerEmailMaster> dealerEmailMasterList = new ArrayList<>();

        int successRecords = 0;

        if (recordList != null) {

            DealerEmailMaster dealerEmailMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    dealerEmailMaster = new DealerEmailMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1: {
                                setSupplierId(dealerEmailMaster, record, index);
                            }
                            break;
                            case 2: {
                                setSupplierDesc(dealerEmailMaster, record, index);
                            }
                            break;
                            case 6:{
                                dealerEmailMaster.setContactPerson(fieldValue);
                            }
                            break;
                            case 9: {

                                dealerEmailMaster.setCityId(fieldValue);
                            }
                            break;
                            case 11: {
                                dealerEmailMaster.setStateId(fieldValue);
                            }
                            break;
                            case 12:{
                                dealerEmailMaster.setZipCode(fieldValue);
                            }
                            break;
                            case 13:{
                                dealerEmailMaster.setPhoneNumber(fieldValue);
                            }
                            break;

                            case 17: {
                                dealerEmailMaster
                                        .setEmail(fieldValue);
                            }
                            break;
                            case 21:{
                                dealerEmailMaster.setAddress1(fieldValue);
                            }
                            break;
                            case 22:{
                                dealerEmailMaster.setAddress2(fieldValue);
                            }
                            break;
                            case 23:{
                                dealerEmailMaster.setAddress3(fieldValue);
                            }
                            break;
                            case 45: {
                                dealerEmailMaster
                                        .setItNum(fieldValue);
                            }
                            break;
                            case 46: {
                                double rank;
                                try {
                                    rank = Double.parseDouble(fieldValue);
                                } catch (Exception e) {

                                    rank = 0;
                                }
                                dealerEmailMaster.setDealerComm(rank);
                            }
                            break;

                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                dealerEmailMaster.setInstitutionID(institutionId);
                setEmailMasterDefaultValue(dealerEmailMaster, "TO");
                dealerEmailMasterList.add(dealerEmailMaster);
            }
        }
        masterRecords.setMasterList(dealerEmailMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateGNGDealerEmailMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.GNG_DEALER_EMAIL_MASTER_HEADER
                .split(",");
        List<GNGDealerEmailMaster> gngDealerEmailMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            GNGDealerEmailMaster gngDealerEmailMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    gngDealerEmailMaster = new GNGDealerEmailMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                gngDealerEmailMaster.setProductId(fieldValue);
                            }
                            break;
                            case 1: {
                                gngDealerEmailMaster.setGroupId(fieldValue);
                            }
                            break;
                            case 2: {
                                gngDealerEmailMaster.setInstitutionID(fieldValue);
                            }
                            break;
                            case 3: {
                                gngDealerEmailMaster.setEmail(fieldValue);
                            }
                            break;
                            case 4: {
                                gngDealerEmailMaster.setRecipientType(fieldValue);
                            }
                            break;
                            case 5: {
                                gngDealerEmailMaster.setRegion(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;

                gngDealerEmailMaster.setActive(true);
                gngDealerEmailMaster.setInstitutionID(institutionId);
                gngDealerEmailMaster.setInsertDate(insertDate);
                gngDealerEmailMasterList.add(gngDealerEmailMaster);
            }
        }
        masterRecords.setMasterList(gngDealerEmailMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private void setEmailMasterDefaultValue(
            DealerEmailMaster dealerEmailMaster, String recipent) {
        dealerEmailMaster.setRecipientsType(recipent);
        dealerEmailMaster.setActive(true);
    }

    private void setSupplierDesc(DealerEmailMaster dealerEmailMaster,
                                 CSVRecord record, int index) {

        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

        dealerEmailMaster.setSupplierDesc(fieldValue);
    }

    private void setSupplierId(DealerEmailMaster dealerEmailMaster,
                               CSVRecord record, int index) {

        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

        dealerEmailMaster.setSupplierID(fieldValue);
        dealerEmailMaster.setDealerID(fieldValue);
    }

    private MasterRecords populateEmployerMasterDomain(
            List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.EMPLOYER_MASTER_HEADER.split(",");
        List<EmployerMaster> employerMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            EmployerMaster employerMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    employerMaster = new EmployerMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                employerMaster.setEmployerId(fieldValue);
                            }
                            break;
                            case 2: {
                                employerMaster.setEmployerName(fieldValue);
                            }
                            break;
                            case 3: {
                                employerMaster.setDasEmpFlag(fieldValue);
                            }
                            break;
                            case 4: {
                                employerMaster.setWorksideEmpFlag(fieldValue);
                            }
                            break;
                            case 5: {
                                employerMaster.setEmployerCatg(fieldValue);
                            }
                            break;
                            case 6: {
                                employerMaster.setLemEndDate(fieldValue);
                            }
                            break;
                            case 7: {
                                employerMaster.setIciciEmpCode(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                employerMaster.setInstitutionId(institutionId);
                employerMaster.setActive(true);
                employerMasterList.add(employerMaster);
            }
        }

        masterRecords.setMasterList(employerMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populatePinCodeDomain(List<CSVRecord> recordList,
                                                String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.PIN_CODE_HEADER.split(",");
        List<PinCodeMaster> pinCodeMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            PinCodeMaster pinCodeMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    pinCodeMaster = new PinCodeMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                pinCodeMaster.setCity(fieldValue);
                            }
                            break;
                            case 2: {
                                pinCodeMaster.setZipCode(fieldValue);
                            }
                            break;
                            case 3: {
                                pinCodeMaster.setZipCodeDescr(fieldValue);
                            }
                            break;
                            case 4: {
                                pinCodeMaster.setState(fieldValue);
                            }
                            break;
                            case 7: {
                                pinCodeMaster.setCityCode(fieldValue);
                            }
                            break;
                            case 8: {
                                pinCodeMaster.setStateCode(fieldValue);
                            }
                            break;
                            case 9: {
                                pinCodeMaster.setCountryCode(fieldValue);
                            }
                            break;
                            case 10: {
                                pinCodeMaster.setBranchCode(fieldValue);
                            }
                            break;
                            case 11: {
                                pinCodeMaster.setBranchName(fieldValue);
                            }
                            break;
                            case 12: {
                                pinCodeMaster.setLocation(fieldValue);
                            }
                            break;
                            case 13: {
                                pinCodeMaster.setCluster(fieldValue);
                            }
                            break;
                            case 14: {
                                pinCodeMaster.setRegion(fieldValue);
                            }
                            break;
                            case 15: {
                                pinCodeMaster.setZone(fieldValue);
                            }
                            break;
                            case 16: {
                                pinCodeMaster.setCountry(fieldValue);
                            }
                            break;
                            case 17: {
                                pinCodeMaster.setPinEligibility(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                pinCodeMaster.setInstitutionId(institutionId);
                pinCodeMaster.setActive(true);
                pinCodeMasterList.add(pinCodeMaster);
            }
        }
        masterRecords.setMasterList(pinCodeMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    /**
     * @param recordList
     * @return
     */
    private MasterRecords populateAssetModelDomain(List<CSVRecord> recordList,
                                                   String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.ASSET_MODEL_HEADER.split(",");
        List<AssetModelMaster> assetModelMasterList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            AssetModelMaster assetModelMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    assetModelMaster = new AssetModelMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                assetModelMaster.setBlockId(fieldValue);
                            }
                            break;
                            case 2: {
                                assetModelMaster.setBlockDesc(fieldValue);
                            }
                            break;
                            case 3: {
                                assetModelMaster.setProductFlag(fieldValue);
                            }
                            break;
                            case 4: {
                                assetModelMaster.setModelId(fieldValue);
                            }
                            break;
                            case 5: {
                                assetModelMaster.setModelNo(fieldValue);
                            }
                            break;
                            case 6: {
                                assetModelMaster.setMake(fieldValue);
                            }
                            break;
                            case 7: {
                                assetModelMaster.setManufacturerId(fieldValue);
                            }
                            break;
                            case 8: {
                                assetModelMaster.setManufacturerDesc(fieldValue);
                            }
                            break;
                            case 9: {
                                assetModelMaster.setLmmLcmCatgId(fieldValue);
                            }
                            break;
                            case 10: {
                                assetModelMaster.setCatgDesc(fieldValue);
                            }
                            break;
                            case 11: {
                                assetModelMaster.setMakeModelFlag(fieldValue);
                            }
                            break;
                            case 12: {
                                double weightUnloaded;
                                try {
                                    weightUnloaded = Double.parseDouble(fieldValue);
                                } catch (Exception e) {

                                    weightUnloaded = 0;
                                }
                                assetModelMaster.setWeightUnloaded(weightUnloaded);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                assetModelMaster.setInstitutionId(institutionId);
                assetModelMaster.setActive(true);
                assetModelMasterList.add(assetModelMaster);
            }
        }
        masterRecords.setMasterList(assetModelMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    /**
     * @param recordList
     * @return It return Model and if error exist then error description which
     * contains in MasterRecords class.
     */
    private MasterRecords populateSchemeDomain(List<CSVRecord> recordList, String institutionId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.SCHEME_HEADER.split(",");
        List<SchemeMasterData> schemeMasterDataList = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            SchemeMasterData schemeMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    schemeMaster = new SchemeMasterData();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1: {
                                schemeMaster.setSchemeID(fieldValue);
                            }
                            break;
                            case 2: {
                                schemeMaster
                                        .setSchemeDesc(fieldValue);
                            }
                            break;
                            case 3: {
                                schemeMaster
                                        .setCurrencyID(fieldValue);
                            }
                            break;
                            case 4: {
                                schemeMaster.setEnabledFlag(fieldValue);
                            }
                            break;
                            case 5: {
                                schemeMaster.setMaxAmtFin(fieldValue);
                            }
                            break;
                            case 6: {
                                schemeMaster.setMinAmtFin(fieldValue);
                            }
                            break;
                            case 7: {
                                schemeMaster.setIntRate(fieldValue);
                            }
                            break;
                            case 8: {
                                schemeMaster
                                        .setMinIntRate(fieldValue);
                            }
                            break;
                            case 9: {
                                schemeMaster.setIntType(fieldValue);
                            }
                            break;
                            case 10: {
                                schemeMaster.setTenure(fieldValue);
                            }
                            break;
                            case 11: {
                                schemeMaster.setFrequency(fieldValue);
                            }
                            break;
                            case 12: {
                                schemeMaster.setInstlType(fieldValue);
                            }
                            break;
                            case 13: {
                                schemeMaster.setNumInstl(fieldValue);
                            }
                            break;
                            case 14: {
                                schemeMaster.setSdRate(fieldValue);
                            }
                            break;
                            case 15: {
                                schemeMaster.setSdAmt(fieldValue);
                            }
                            break;
                            case 16: {
                                schemeMaster.setSdInt(fieldValue);
                            }
                            break;
                            case 17: {
                                schemeMaster.setSdIntType(fieldValue);
                            }
                            break;
                            case 18: {
                                schemeMaster.setInstlMode(fieldValue);
                            }
                            break;
                            case 19: {
                                schemeMaster.setFloatingRateFlag(fieldValue);
                            }
                            break;
                            case 20: {
                                schemeMaster.setLoanType(fieldValue);
                            }
                            break;
                            case 21: {
                                schemeMaster
                                        .setMaxIntRate(fieldValue);
                            }
                            break;

                            case 22: {
                                schemeMaster.setMinTenure(fieldValue);
                            }
                            break;
                            case 23: {
                                schemeMaster.setMaxTenure(fieldValue);
                            }
                            break;
                            case 24: {
                                schemeMaster.setDpErYr(fieldValue);
                            }
                            break;
                            case 25: {
                                schemeMaster.setPrePayPenalty(fieldValue);
                            }
                            break;
                            case 26: {
                                schemeMaster.setForEcloseLockLn(fieldValue);
                            }
                            break;
                            case 27: {
                                schemeMaster.setProductFlag(fieldValue);
                            }
                            break;
                            case 28: {
                                schemeMaster.setPdcFlag(fieldValue);
                            }
                            break;
                            case 29: {
                                schemeMaster.setDecimals(fieldValue);
                            }
                            break;
                            case 30: {
                                schemeMaster.setStatus(fieldValue);
                            }
                            break;
                            case 31: {
                                schemeMaster.setCcid(fieldValue);
                            }
                            break;
                            case 32: {
                                schemeMaster.setPmnTmode(fieldValue);
                            }
                            break;
                            case 33: {
                                schemeMaster.setAdjExintComp(fieldValue);
                            }
                            break;
                            case 34: {
                                schemeMaster.setLpiCriteria(fieldValue);
                            }
                            break;
                            case 35: {
                                schemeMaster.setLpiDateToBeused(fieldValue);
                            }
                            break;
                            case 36: {
                                schemeMaster.setLpiDoneOn(fieldValue);
                            }
                            break;
                            case 37: {
                                schemeMaster.setMcStatus(fieldValue);
                            }
                            break;
                            case 38: {
                                schemeMaster.setMakeRID(fieldValue);
                            }
                            break;
                            case 39: {
                                schemeMaster.setMakeDate(fieldValue);
                            }
                            break;
                            case 40: {
                                schemeMaster.setAuthId(fieldValue);
                            }
                            break;
                            case 41: {
                                schemeMaster.setAuthDate(fieldValue);
                            }
                            break;
                            case 42: {
                                schemeMaster.setLpiChargeID(fieldValue);
                            }
                            break;
                            case 43: {
                                schemeMaster.setLoc(fieldValue);
                            }
                            break;
                            case 44: {
                                schemeMaster.setMinIRR(fieldValue);
                            }
                            break;
                            case 45: {
                                schemeMaster.setProcessedFlag(fieldValue);
                            }
                            break;
                            case 46: {
                                schemeMaster.setMultiLoanFlag(fieldValue);
                            }
                            break;
                            case 47: {
                                schemeMaster.setFloatingFrequency(fieldValue);
                            }
                            break;
                            case 48: {
                                schemeMaster.setIntRoundOffPara(fieldValue);
                            }
                            break;
                            case 49: {
                                schemeMaster.setIntRoundTill(fieldValue);
                            }
                            break;
                            case 50: {
                                schemeMaster.setInstlAmtRoundOffPara(record.get(
                                        index).trim());
                            }
                            break;
                            case 51: {
                                schemeMaster.setInstlAmtRoundTill(fieldValue);
                            }
                            break;
                            case 52: {
                                schemeMaster.setPrinBaseUnit(fieldValue);
                            }
                            break;
                            case 53: {
                                schemeMaster.setDepositLink(fieldValue);
                            }
                            break;
                            case 54: {
                                schemeMaster.setFixedTerm(fieldValue);
                            }
                            break;
                            case 55: {
                                schemeMaster.setPrimeType(fieldValue);
                            }
                            break;
                            case 56: {
                                schemeMaster.setLtvMultiplier(fieldValue);
                            }
                            break;
                            case 57: {
                                schemeMaster.setInsurProductid(fieldValue);
                            }
                            break;
                            case 58: {
                                schemeMaster.setBonusFreq(fieldValue);
                            }
                            break;
                            case 59: {
                                schemeMaster.setLpiChargeDon(fieldValue);
                            }
                            break;
                            case 60: {
                                schemeMaster.setPrePayNotChargable(record
                                        .get(index).trim());
                            }
                            break;
                            case 61: {
                                schemeMaster.setPartPrepaymentPrin(record
                                        .get(index).trim());
                            }
                            break;
                            case 62: {
                                schemeMaster.setPrePayAmount(fieldValue);
                            }
                            break;
                            case 63: {
                                schemeMaster.setFixedForMonths(fieldValue);
                            }
                            break;
                            case 64: {
                                schemeMaster.setMinIrr(fieldValue);
                            }
                            break;
                            case 65: {
                                schemeMaster.setMaxIrr(fieldValue);
                            }
                            break;
                            case 66: {
                                schemeMaster.setSchemeStartDate(fieldValue);
                            }
                            break;
                            case 67: {
                                schemeMaster.setSchemeEndDate(fieldValue);
                            }
                            break;
                            case 68: {
                                schemeMaster
                                        .setBucketCode(fieldValue);
                            }
                            break;
                            case 69: {
                                schemeMaster.setMaxInsr(fieldValue);
                            }
                            break;
                            case 70: {
                                schemeMaster.setLsmLvmVericodeC(fieldValue);
                            }
                            break;
                            case 71: {
                                schemeMaster.setLsmScoreMergeCriteriaC(record.get(
                                        index).trim());
                            }
                            break;
                            case 72: {
                                schemeMaster.setSchID(fieldValue);
                            }
                            break;
                            case 73: {
                                schemeMaster.setLsmAssetMinageN(fieldValue);
                            }
                            break;
                            case 74: {
                                schemeMaster.setLsmAssetMaxageN(fieldValue);
                            }
                            break;
                            case 75: {
                                schemeMaster.setLsmLwwWorkflowIDC(fieldValue);
                            }
                            break;
                            case 76: {
                                schemeMaster.setLsmLffmFormIdC(fieldValue);
                            }
                            break;
                            case 77: {
                                schemeMaster.setLsmLpmPolicySetC(fieldValue);
                            }
                            break;
                            case 78: {
                                schemeMaster.setLsmLsmScoreTableC(fieldValue);
                            }
                            break;
                            case 79: {
                                schemeMaster.setLsmLmmModel(fieldValue);
                            }
                            break;
                            case 80: {
                                schemeMaster
                                        .setLsmLmmMake(fieldValue);
                            }
                            break;
                            case 81: {
                                schemeMaster.setLsmCollRatioN(fieldValue);
                            }
                            break;
                            case 82: {
                                schemeMaster.setLsmForEclosureIntDaysN(record.get(
                                        index).trim());
                            }
                            break;
                            case 83: {
                                schemeMaster.setLsmLtv(fieldValue);
                            }
                            break;
                            case 84: {
                                schemeMaster.setEquivalentCode(fieldValue);
                            }
                            break;
                            case 85: {
                                schemeMaster.setUsedFlag(fieldValue);
                            }
                            break;
                            case 86: {
                                schemeMaster.setBounceChargeId(fieldValue);
                            }
                            break;
                            case 87: {
                                schemeMaster.setExInterestRefund(fieldValue);
                            }
                            break;
                            case 88: {
                                schemeMaster.setGapInterestFlag(fieldValue);
                            }
                            break;
                            case 89: {
                                schemeMaster.setCommenceDate(fieldValue);
                            }
                            break;
                        }

                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                schemeMaster.setInstitutionId(institutionId);
                schemeMaster.setInsertDate(new Date());
                schemeMasterDataList.add(schemeMaster);
            }
        }
        masterRecords.setMasterList(schemeMasterDataList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateBankDetailsDomain(List<CSVRecord> recordList,
                                                    String instId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.BANK_DETAILS_MASTER_HEADER
                .split(",");
        List<BankDetailsMaster> bankDetailsMasters = new ArrayList<>();
        int successRecords = 0;
        if (recordList != null) {
            BankDetailsMaster bankDetailsMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    bankDetailsMaster = new BankDetailsMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                bankDetailsMaster.setBankName(fieldValue);
                            }
                            break;
                            case 2: {
                                bankDetailsMaster.setBranch(fieldValue);
                            }
                            break;
                            case 3: {
                                bankDetailsMaster.setIfscCode(fieldValue);
                            }
                            break;
                            case 4: {
                                bankDetailsMaster.setMicrCode(fieldValue);
                            }
                            break;
                            case 5: {
                                bankDetailsMaster.setCity(fieldValue);
                            }
                            break;
                            case 6: {
                                bankDetailsMaster.setDistrict(fieldValue);
                            }
                            break;
                            case 7: {
                                bankDetailsMaster
                                        .setState(fieldValue);
                            }
                            break;
                            case 8: {
                                bankDetailsMaster
                                        .setState(fieldValue);
                            }
                            break;
                            case 9: {
                                bankDetailsMaster.setCountry(fieldValue);
                            }
                            break;
                            case 10: {
                                bankDetailsMaster.setPincode(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                bankDetailsMaster.setInstitutionId(instId);
                bankDetailsMaster.setActive(true);
                bankDetailsMasters.add(bankDetailsMaster);
            }
        }
        masterRecords.setMasterList(bankDetailsMasters);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateHierarchyDomain(List<CSVRecord> recordList,
                                                  String instId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        String[] header = CSVHeaderConstant.HIERARCHY_MASTER_HEADER.split(",");
        Map<String, HierarchyMaster> hierarchyMasterMap = new HashMap<>();

        int successRecords = 0;
        if (recordList != null) {
            HierarchyMaster hierarchyMaster = null;
            List<String> hierarchyType = null;
            Map<String, List<String>> hierarchyLevel = null;
            Map<String, String> hierarchyCollection = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                String type = null;
                if (record != null && record.size() == header.length) {
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {


                                String instiId = fieldValue;

                                if (hierarchyMasterMap.containsKey(instiId)) {
                                    hierarchyMaster = hierarchyMasterMap
                                            .get(instiId);
                                    hierarchyType = hierarchyMaster.getType();
                                    hierarchyLevel = hierarchyMaster.getLevel();
                                    hierarchyCollection = hierarchyMaster
                                            .getCollection();
                                } else {
                                    hierarchyMaster = new HierarchyMaster();
                                    hierarchyType = new ArrayList<>();
                                    hierarchyLevel = new HashMap<String, List<String>>();
                                    hierarchyCollection = new HashMap<String, String>();

                                    hierarchyMaster.setInstitutionId(Integer
                                            .parseInt(instiId));
                                    hierarchyMaster.setType(hierarchyType);
                                    hierarchyMaster.setLevel(hierarchyLevel);
                                    hierarchyMaster
                                            .setCollection(hierarchyCollection);
                                    hierarchyMasterMap
                                            .put(instiId, hierarchyMaster);
                                }
                            }
                            break;
                            case 1: {
                                type = fieldValue;
                                hierarchyType.add(type);
                            }
                            break;
                            case 2: {
                                hierarchyLevel.put(
                                        type,
                                        Arrays.asList(fieldValue
                                                .split(",")));
                            }
                            break;
                            case 3: {
                                hierarchyCollection.put(type, fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;
                hierarchyMaster.setActive(true);
                hierarchyMaster.setInsertDt(insertDate);
            }
        }
        List<HierarchyMaster> list = new ArrayList<HierarchyMaster>(
                hierarchyMasterMap.values());

        masterRecords.setMasterList(list);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateCDLHierarchyDomain(
            List<CSVRecord> recordList, String instId) {

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<CDLHierarchyMaster> cdlHierarchyMasterList = new ArrayList<>();

        String[] header = CSVHeaderConstant.DEALER_BRANCH_MASTER_HEADER
                .split(",");

        int successRecords = 0;
        if (recordList != null) {
            CDLHierarchyMaster cdlHierarchyMaster = null;
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                if (record != null && record.size() == header.length) {
                    cdlHierarchyMaster = new CDLHierarchyMaster();
                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 1: {
                                cdlHierarchyMaster.setDealerId(fieldValue);
                            }
                            break;
                            case 2: {
                                cdlHierarchyMaster.setDealerDesc(fieldValue);
                            }
                            break;
                            case 3: {
                                cdlHierarchyMaster.setBranchName(fieldValue);
                            }
                            break;
                            case 5: {
                                cdlHierarchyMaster.setBranchCode(fieldValue);
                            }
                            break;
                            case 8: {
                                cdlHierarchyMaster.setLsomBranchName(fieldValue);
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided .Corrupted or blank record found at row no:" + " " + (record.getRecordNumber() + 1));
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                // add to list
                successRecords++;

                cdlHierarchyMaster.setInstitutionId(instId);
                cdlHierarchyMaster.setArea("A01");
                cdlHierarchyMaster.setRegion("R01");
                cdlHierarchyMaster.setZone("Z01");
                cdlHierarchyMaster.setCountry("India");
                cdlHierarchyMaster.setActive(true);

                cdlHierarchyMaster.setInsertDt(insertDate);

                cdlHierarchyMasterList.add(cdlHierarchyMaster);
            }
        }
        masterRecords.setMasterList(cdlHierarchyMasterList);
        fileUploadStatus.setStatus(Constant.SUCCESS);
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateModelVariantDomain(
            List<CSVRecord> recordList, String instId) {
        String[] header = CSVHeaderConstant.MODEL_VARIANT_MASTER_HEADER
                .split(",");

        List<ModelVariantMaster> modelVariantMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            ModelVariantMaster modelVariantMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    modelVariantMaster = new ModelVariantMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                modelVariantMaster.setManufacturer(fieldValue);
                                break;
                            case 1:
                                modelVariantMaster.setModel(fieldValue);
                                break;
                            case 2:
                                modelVariantMaster.setVariant(fieldValue);
                                break;
                        }
                    }
                    successRecords++;
                    modelVariantMaster.setInstitutionId(instId);
                    modelVariantMaster.setActive(true);
                    modelVariantMasterList.add(modelVariantMaster);
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(modelVariantMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populateAssetCategoryDomain(
            List<CSVRecord> recordList, String institutionId) {
        String[] header = CSVHeaderConstant.ASSET_CATEGORY_MASTER_HEADER
                .split(",");

        List<AssetCategoryMaster> assetCategoryMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            AssetCategoryMaster assetCategoryMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    assetCategoryMaster = new AssetCategoryMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                assetCategoryMaster.setAssetCatgId(fieldValue);
                                break;
                            case 1:
                                assetCategoryMaster.setAssetCatgDesc(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                assetCategoryMaster.setInstitutionId(institutionId);
                assetCategoryMaster.setActive(true);
                assetCategoryMasterList.add(assetCategoryMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(assetCategoryMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populateStateDomain(List<CSVRecord> recordList,
                                              String institutionId) {

        String[] header = CSVHeaderConstant.STATE_MASTER_HEADER.split(",");

        List<StateMaster> stateMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            StateMaster stateMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    stateMaster = new StateMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                stateMaster.setStateId(fieldValue);
                                break;
                            case 1:
                                stateMaster.setStateDesc(fieldValue);
                                break;
                            case 11:
                                stateMaster.setStateCode(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                stateMaster.setInstitutionId(institutionId);
                stateMaster.setActive(true);
                stateMasterList.add(stateMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(stateMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateCityDomain(List<CSVRecord> recordList,
                                             String institutionId) {

        String[] header = CSVHeaderConstant.CITY_MASTER_HEADER.split(",");

        List<CityMaster> cityMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            CityMaster cityMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length-1) {
                    cityMaster = new CityMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1:
                                cityMaster.setCityId(fieldValue);
                                break;
                            case 2:
                                cityMaster.setCityDesc(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                cityMaster.setInstitutionId(institutionId);
                cityMaster.setActive(true);
                cityMasterList.add(cityMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(cityMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateCreditPromotionDomain(
            List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.CREDIT_PROMOTION_MASTER_HEADER
                .split(",");

        List<CreditPromotionMaster> creditPromoMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            CreditPromotionMaster creditPromoMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    creditPromoMaster = new CreditPromotionMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1:
                                creditPromoMaster.setPromotionId(fieldValue);
                                break;
                            case 3:
                                creditPromoMaster.setSchemeId(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                creditPromoMaster.setInstitutionId(institutionId);
                creditPromoMaster.setActive(true);
                creditPromoMasterList.add(creditPromoMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(creditPromoMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateDsaBranchDomain(List<CSVRecord> recordList,
                                                  String institutionId) {

        String[] header = CSVHeaderConstant.DSA_BRANCH_MASTER_HEADER.split(",");

        List<DSABranchMaster> dsaBranchMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            DSABranchMaster dsaBranchMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    dsaBranchMaster = new DSABranchMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1:
                                dsaBranchMaster.setBrokerId(fieldValue);
                                break;
                            case 2:
                                dsaBranchMaster.setBranchId(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                dsaBranchMaster.setInstitutionId(institutionId);
                dsaBranchMaster.setActive(true);
                dsaBranchMasterList.add(dsaBranchMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(dsaBranchMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateDsaProductDomain(List<CSVRecord> recordList,
                                                   String institutionId) {

        String[] header = CSVHeaderConstant.DSA_PRODUCT_MASTER_HEADER
                .split(",");

        List<DSAProductMaster> dsaProductMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            DSAProductMaster dsaProductMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    dsaProductMaster = new DSAProductMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1:
                                dsaProductMaster.setBrokerId(fieldValue);
                                break;
                            case 2:
                                dsaProductMaster.setProductId(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                dsaProductMaster.setInstitutionId(institutionId);
                dsaProductMaster.setActive(true);
                dsaProductMasterList.add(dsaProductMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(dsaProductMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateDealerBranchDomain(
            List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.DEALER_BRANCH_MASTER_HEADER
                .split(",");

        List<DealerBranchMaster> dealerBranchMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            DealerBranchMaster dealerBranchMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    dealerBranchMaster = new DealerBranchMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1: {
                                dealerBranchMaster.setDealerId(fieldValue);
                            }
                            break;

                            case 3: {
                                dealerBranchMaster.setBranchName(fieldValue);
                            }
                            break;

                            case 5: {
                                dealerBranchMaster.setBranchId(fieldValue);
                            }
                            break;

                            case 7: {
                                dealerBranchMaster.setProductId(fieldValue);
                            }
                            break;

                            case 12: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    dealerBranchMaster.setDealerFlag(true);
                                }
                            }
                            break;

                            case 13: {
                                if (StringUtils.equalsIgnoreCase(Status.YES.name(), fieldValue)) {
                                    dealerBranchMaster.setSkipSerialOrImeiValidation(true);
                                }
                            }

                            case 14: {
                                    if (StringUtils.isNotBlank(fieldValue)) {

                                        Set<String> loyaltyCards = new HashSet<>();
                                        if (StringUtils.contains(fieldValue, "|")) {
                                            loyaltyCards = new HashSet<>(Arrays.asList(fieldValue.split("\\|")));
                                        } else {
                                            loyaltyCards.add(fieldValue);
                                        }
                                        dealerBranchMaster.setLoyaltyCards(loyaltyCards);
                                    }
                            }
                            break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                dealerBranchMaster.setInstitutionId(institutionId);
                dealerBranchMaster.setActive(true);
                dealerBranchMasterList.add(dealerBranchMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(dealerBranchMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;

    }

    private MasterRecords populateRelianceDealerBranchDomain(
            List<CSVRecord> recordList, String institutionId) {


        String[] header = CSVHeaderConstant.RELIANCE_DEALER_BRANCH_MASTER_HEADER
                .split(FieldSeparator.COMMA_STR);

        List<RelianceDealerBranchMaster> dealerBranchMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            RelianceDealerBranchMaster dealerBranchMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    dealerBranchMaster = new RelianceDealerBranchMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                dealerBranchMaster.setDealerId(fieldValue);
                                break;
                            case 1:
                                dealerBranchMaster.setBranchName(fieldValue);
                                break;
                            case 2:
                                dealerBranchMaster.setDealerName(fieldValue);
                                break;
                        }
                    }
                } else {

                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc(SystemErrorConstant.CURRUPT_RECORD
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                dealerBranchMaster.setInstitutionId(institutionId);
                dealerBranchMaster.setActive(true);
                dealerBranchMasterList.add(dealerBranchMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(dealerBranchMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populateBranchMaster(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.BRANCH_MASTER_HEADER
                .split(",");

        List<BranchMaster> branchMasterList = new ArrayList<>();

        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {

            BranchMaster branchMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    branchMaster = new BranchMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 1:
                                branchMaster.setBranchId(fieldValue);
                                break;

                            case 3:
                                branchMaster.setBranchName(fieldValue);
                                break;

                            case 29:
                                branchMaster.setStateId(fieldValue);
                                break;

                            case 30:
                                branchMaster.setStateName(fieldValue);
                                break;
                        }
                    }
                } else {
                    logger.error("Index Number : " + successRecords);
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc("Corrupt or blank record at index"
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;

                }
                successRecords++;
                branchMaster.setInstitutionId(institutionId);
                branchMaster.setActive(true);
                branchMasterList.add(branchMaster);
            }
            masterRecords = new MasterRecords();
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.SUCCESS);
            fileUploadStatus.setNumberOfRecords(recordList.size());
            fileUploadStatus.setNumberOfRecordSuccess(successRecords);
            masterRecords.setMasterList(branchMasterList);
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populateSourcingDetailDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.SOURCING_DETAIL_MASTER_HEADER
                .split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<SourcingDetailMaster> sourcingDetailMasterList = new ArrayList<>();
        int successRecords = 0;

        if (recordList != null) {

            SourcingDetailMaster sourcingDetailMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    sourcingDetailMaster = new SourcingDetailMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                sourcingDetailMaster.setBranchId(fieldValue);
                            }
                            break;
                            case 1: {
                                sourcingDetailMaster.setMakerId(fieldValue);
                            }
                            break;
                            case 2: {

                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date makeDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy HH:mm:ss a", fieldValue);
                                    if (null != makeDate) {
                                        sourcingDetailMaster.setMakeDate(makeDate);
                                    }
                                }
                            }
                            break;
                            case 3: {
                                sourcingDetailMaster.setAuthId(fieldValue);
                            }
                            break;

                            case 4: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date authDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy HH:mm:ss a", fieldValue);
                                    if (null != authDate) {
                                        sourcingDetailMaster.setAuthDate(authDate);
                                    }
                                }
                            }
                            break;

                            case 5: {
                                sourcingDetailMaster.setModuleId(fieldValue);
                            }
                            break;
                            case 6: {
                                sourcingDetailMaster.setKey1(fieldValue);
                            }
                            break;
                            case 7: {
                                sourcingDetailMaster.setKey2(fieldValue);
                            }
                            break;
                            case 8: {
                                sourcingDetailMaster.setValue(fieldValue);
                            }
                            break;
                            case 9: {
                                sourcingDetailMaster.setDescription(fieldValue);
                            }
                            break;
                            case 10: {
                                sourcingDetailMaster.setStatus(fieldValue);
                            }
                            break;
                            case 11: {
                                sourcingDetailMaster.setModuleFlag(fieldValue);
                            }
                            break;
                            case 12: {
                                sourcingDetailMaster.setAppFlag(fieldValue);
                            }
                            break;
                            case 13: {
                                sourcingDetailMaster.setDisableFlag(fieldValue);
                            }
                            break;
                            case 14: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date cgpStartDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy", fieldValue);
                                    if (null != cgpStartDate) {
                                        sourcingDetailMaster.setCgpStartDate(cgpStartDate);
                                    }
                                }
                            }
                            break;
                            case 15: {
                                if (StringUtils.isNotBlank(fieldValue)) {
                                    Date cgpEndDate = GngDateUtil.getDateFromSpecificFormat("MM/dd/yyyy", fieldValue);
                                    if (null != cgpEndDate) {
                                        sourcingDetailMaster.setCgpEndDate(cgpEndDate);
                                    }
                                }
                            }
                            break;
                            case 16: {
                                sourcingDetailMaster.setmApplyFlag(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                sourcingDetailMaster.setInstitutionId(institutionId);
                sourcingDetailMaster.setInsertDate(new Date());
                sourcingDetailMasterList.add(sourcingDetailMaster);
            }
        }
        masterRecords.setMasterList(sourcingDetailMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populatePromotionalSchemeDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.PROMOTIONAL_SCHEME_HEADER.split(",");

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<PromotionalSchemeMaster> promotionalSchemeMasterList = new ArrayList<>();

        int successRecords = 0;

        if (recordList != null) {

            PromotionalSchemeMaster promotionalSchemeMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {

                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {

                    promotionalSchemeMaster = new PromotionalSchemeMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {
                            case 0: {
                                promotionalSchemeMaster.setPromotionId(fieldValue);
                            }
                            break;

                            case 1: {
                                promotionalSchemeMaster.setProductCode(fieldValue);
                            }
                            break;

                            case 6: {
                                promotionalSchemeMaster.setMcStatus(fieldValue);
                            }
                            break;

                            case 7: {
                                promotionalSchemeMaster.setPromotionDesc(fieldValue);
                            }
                            break;

                            case 8: {
                                promotionalSchemeMaster.setPromotionCode(fieldValue);
                            }
                            break;

                        }
                    }

                } else {
                    fileUploadStatus.setStatus(Status.ERROR.name());
                    fileUploadStatus.setErrorDesc("Wrong csv file provided");
                    masterRecords.setFileUploadStatus(fileUploadStatus);
                    return masterRecords;
                }
                successRecords++;
                promotionalSchemeMaster.setInstitutionId(institutionId);
                promotionalSchemeMaster.setInsertDate(new Date());
                promotionalSchemeMasterList.add(promotionalSchemeMaster);
            }
        }
        masterRecords.setMasterList(promotionalSchemeMasterList);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;

    }

    private MasterRecords populateTkilDealerBranchDomain(
            List<CSVRecord> recordList, String institutionId) {


        String[] header = CSVHeaderConstant.TKIL_DEALER_BRANCH_MASTER_HEADER
                .split(FieldSeparator.COMMA_STR);

        List<TkilDealerBranchMaster> dealerBranchMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            TkilDealerBranchMaster dealerBranchMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    dealerBranchMaster = new TkilDealerBranchMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                dealerBranchMaster.setDealerId(fieldValue);
                                break;
                            case 1:
                                dealerBranchMaster.setDealerName(fieldValue);
                                break;
                        }
                    }
                } else {

                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc(SystemErrorConstant.CURRUPT_RECORD
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                dealerBranchMaster.setInstitutionId(institutionId);
                dealerBranchMaster.setActive(true);
                dealerBranchMasterList.add(dealerBranchMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(dealerBranchMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populateBankDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.BANK_MASTER_HEADER.split(FieldSeparator.COMMA_STR);

        List<BankMaster> bankMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            BankMaster bankMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    bankMaster = new BankMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                bankMaster.setBankDescription(fieldValue);
                                break;
                            case 1:
                                bankMaster.setBankCode(fieldValue);
                                break;

                            case 4:
                                bankMaster.setMifinBankcode(fieldValue);
                                break;

                            case 5:
                                bankMaster.setMifinBankName(fieldValue);
                                break;
                        }
                    }
                } else {

                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc(SystemErrorConstant.CURRUPT_RECORD
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                bankMaster.setInstitutionId(institutionId);
                bankMaster.setActive(true);
                bankMasterList.add(bankMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(bankMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    private MasterRecords populatePaynimoBankMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] header = CSVHeaderConstant.PAYNIMO_BNNK_MASTER_HEADER.split(FieldSeparator.COMMA_STR);

        List<PaynimoBankMaster> paynimoBankMasterList = new ArrayList<>();
        int successRecords = 0;
        FileUploadStatus fileUploadStatus = null;
        MasterRecords masterRecords = null;

        if (recordList != null) {
            PaynimoBankMaster paynimoBankMaster = null;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == header.length) {
                    paynimoBankMaster = new PaynimoBankMaster();

                    for (int index = 0; index < record.size(); index++) {

                        String fieldValue = CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(record.get(index).trim());

                        switch (index) {

                            case 0:
                                break;
                            case 1:
                                paynimoBankMaster.setBankName(fieldValue);
                                break;
                            case 2:
                                paynimoBankMaster.setPhoneToAccount(fieldValue);
                                break;
                            case 3:
                                paynimoBankMaster.setBankType(fieldValue);
                                break;
                            case 4:
                                paynimoBankMaster.setApiHit(fieldValue);
                                break;
                        }
                    }
                } else {

                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus
                            .setErrorDesc(SystemErrorConstant.CURRUPT_RECORD
                                    + recordIndex++);
                    fileUploadStatus.setStatus(Constant.ERROR);
                    fileUploadStatus.setNumberOfRecords(recordList.size());
                    fileUploadStatus.setNumberOfRecordSuccess(0);

                }
                successRecords++;
                paynimoBankMaster.setInstitutionId(institutionId);
                paynimoBankMaster.setActive(true);
                paynimoBankMasterList.add(paynimoBankMaster);
            }
            masterRecords = new MasterRecords();
            if (fileUploadStatus != null) {
                masterRecords.setMasterList(null);
            } else {
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.SUCCESS);
                fileUploadStatus.setNumberOfRecords(recordList.size());
                fileUploadStatus.setNumberOfRecordSuccess(successRecords);
                masterRecords.setMasterList(paynimoBankMasterList);
            }
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        return masterRecords;
    }

    @Override
    public MasterRecords parseOrganizationalHierarchyMaster(CSVParser parser, String institutionId) {
        return populateOrganizationalHierarchyMasterDomain(getRecordList(parser), institutionId);
    }

    private MasterRecords populateOrganizationalHierarchyMasterDomain(List<CSVRecord> recordList, String institutionId) {

        String[] csvHeaders = CSVHeaderConstant.ORGANISATIONAL_HIERARCHY_MASTER.split(",");

        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        int successRecords = 0;

        List<OrganizationalHierarchyMaster> organizationalHierarchyMasters = new ArrayList<>();

        if (recordList != null) {

            OrganizationalHierarchyMaster organizationalHierarchyMaster = null;
            List<List<HierarchyNode>> parentNodesList;

            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);

                if (record != null && record.size() == csvHeaders.length) {
                    parentNodesList = new ArrayList<>();
                    String branchCode = null;
                    String product = null;
                    String channel = null;
                    int level = 1;
                    for (int index = 0; index < record.size(); index++) {
                        String indexValue = record.get(index).trim();
                        switch(index) {
                            case 0:
                                branchCode = indexValue;
                                break;
                            case 1:
                                //add setProduct code
                                product = indexValue;
                                break;
                            case 2:
                                //add setChannel code
                                channel = indexValue;
                                break;
                            default:
                                if(StringUtils.isNotEmpty(indexValue)){
                                    String[] values = indexValue.split(",");
                                    String roleName = csvHeaders[index];

                                    List<HierarchyNode> nodesList = new ArrayList<>();
                                    for (String value : values) {
                                        String[] userData = value.split("-");
                                        HierarchyNode node = new HierarchyNode();
                                        node.setRole(roleName);
                                        node.setUserId(userData[1].trim());
                                        node.setUserName(userData[0].trim());
                                        node.setLevel(level);
                                        nodesList.add(node);
                                    }

                                    if(CollectionUtils.isEmpty(parentNodesList)){
                                        for (HierarchyNode node : nodesList){
                                            List<HierarchyNode> nodes = new ArrayList<>();
                                            nodes.add(node);
                                            parentNodesList.add(nodes);
                                        }
                                    }else {
                                        List<List<HierarchyNode>> multipleUserNOdesList = new ArrayList<>();
                                        for (HierarchyNode node : nodesList) {
                                            List<List<HierarchyNode>> incrementalList = new ArrayList<>(parentNodesList.size());
                                            for (List<HierarchyNode> nodes: parentNodesList) {
                                                List<HierarchyNode> deepClonedList = nodes.stream().map(hNode ->
                                                        new HierarchyNode(hNode)).collect(Collectors.toList());
                                                incrementalList.add(deepClonedList);
                                            }
                                            incrementalList.forEach(list -> {
                                                list.add(node);
                                            });
                                            multipleUserNOdesList.addAll(incrementalList);
                                        }
                                        parentNodesList = new ArrayList<>();
                                        parentNodesList.addAll(multipleUserNOdesList);
                                    }
                                    level++;
                                }
                                break;
                        }
                    }
                    for (List<HierarchyNode> finalNodes : parentNodesList) {
                        organizationalHierarchyMaster = new OrganizationalHierarchyMaster();
                        organizationalHierarchyMaster.setInstitutionId(institutionId);
                        organizationalHierarchyMaster.setBranchCode(branchCode);
                        organizationalHierarchyMaster.setProduct(product);
                        organizationalHierarchyMaster.setChannel(channel);
                        organizationalHierarchyMaster.setHierarchyNodes(finalNodes);
                        successRecords++;
                        organizationalHierarchyMasters.add(organizationalHierarchyMaster);
                    }
                }
            }
        }
        masterRecords.setMasterList(organizationalHierarchyMasters);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(successRecords);
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    @Override
    public MasterRecords parseCustomerDetailsMaster(XSSFSheet sheet, String institutionId, String product) {
        return populateCustomerDetailsMaster(sheet,  institutionId, product);
    }

    @Override
    public MasterRecords parseRoiSchemeMasterDetails(CSVParser parser, String institutionId, String product) {
        return populateRoiSchemeMaster(getRecordList(parser), institutionId, product);
    }

    private MasterRecords populateRoiSchemeMaster(List<CSVRecord> recordList, String institutionId, String product) {

        String[] csvHeaders = CSVHeaderConstant.ROI_SCHEME_MASTER.split(",");
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<RoiSchemeMaster> list = new ArrayList<>();
        RoiSchemeMaster schemeMaster = null;
        if (recordList != null) {
            for (int recordIndex = 0; recordIndex < recordList.size(); recordIndex++) {
                CSVRecord record = recordList.get(recordIndex);
                try {
                    schemeMaster = new RoiSchemeMaster();
                    schemeMaster.setInstitutionId(institutionId);
                    schemeMaster.setProduct(product);
                    if (record != null && record.size() == csvHeaders.length) {
                        for (int index = 0; index < record.size(); index++) {
                            String indexValue = record.get(index).trim();
                            switch (index) {
                                case 0:
                                    schemeMaster.setCompanyName(indexValue);
                                    break;
                                case 1:
                                    schemeMaster.setSchemeName(indexValue);
                                    break;
                                case 2:
                                    schemeMaster.setExternalPercentage(Double.valueOf(indexValue));
                                    break;
                                case 3:
                                    schemeMaster.setInternalPercentage(Double.valueOf(indexValue));
                                    break;
                                case 4:
                                    schemeMaster.setRoi(Double.valueOf(indexValue));
                                    break;
                            }
                        }
                    }
                    list.add(schemeMaster);
                } catch (Exception e){
                    logger.debug("Unable to create master record for row position :: " + recordIndex + 2);
                }
            }
        }
        masterRecords.setMasterList(list);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(recordList.size());
        fileUploadStatus.setNumberOfRecordSuccess(list.size());
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private MasterRecords populateCustomerDetailsMaster(XSSFSheet sheet,String institutionId, String product) {
        MasterRecords masterRecords = new MasterRecords();
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List = appConfigurationRepository.fetchHierarchyMaster(
                institutionId, product);
        Map<String, Branch> nameBranchMap = getNameBranchMap(hierarchyMasterV2List);

        Iterator<Row> rowItr = sheet.iterator();
        rowItr.next();
        int rowCount = 0;
        List<String> refIdsGenerated = new ArrayList<>();

        for(int rowNo = 1; rowNo < sheet.getLastRowNum(); rowNo++){
            XSSFRow row = (XSSFRow) sheet.getRow(rowNo);
            if(row != null && row.getLastCellNum() > 0) {
                try {
                    ApplicationRequest applicationRequest = new ApplicationRequest();
                    Header header = new Header();
                    header.setInstitutionId(institutionId);
                    header.setProduct(Product.getProductFromName(product));
                    Request request = null;
                    Applicant applicant = null;
                    Application application = null;
                    AppMetaData appMetaData = null;
                    Branch branchV2 = null;
                    CustomerAddress addr = null;
                    applicationRequest.setHeader(header);
                    rowCount++;
                    Iterator<Cell> itr = row.iterator();
                    XSSFRow headerRow = row.getSheet().getRow(0);
                    for (int cellNo = 0; cellNo < row.getLastCellNum(); cellNo++) {
                        Cell cell = row.getCell(cellNo, Row.CREATE_NULL_AS_BLANK);
                        Cell headerValue = headerRow.getCell(cellNo);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        String value = cell.getStringCellValue();
                        if (StringUtils.isNotEmpty(headerValue.getStringCellValue())) {
                            try {
                                String headerSwitch = headerValue.getStringCellValue().trim().replaceAll(" ", "").toUpperCase();
                                switch (headerSwitch) {
                                    case "EMPLOYEEID":
                                        break;
                                    case "DSAID":
                                        header.setDsaId(value);
                                        break;
                                    case "BRANCHNAME":
                                        if (nameBranchMap.containsKey(value.replaceAll(" ", "").toLowerCase())) {
                                            branchV2 = nameBranchMap.get(value.replaceAll(" ", "").toLowerCase());
                                        }
                                        if (branchV2 == null) {
                                            branchV2 = new Branch();
                                        }
                                        branchV2.setBranchName(value);
                                        break;
                                    case "BRANCHMANAGER":
                                        if (nameBranchMap.containsKey(value.replaceAll(" ", "").toLowerCase())) {
                                            branchV2 = nameBranchMap.get(value.replaceAll(" ", "").toLowerCase());
                                        }
                                        if (branchV2 == null) {
                                            branchV2 = new Branch();
                                        }
                                        branchV2.setBranchManagerName(value);
                                        break;
                                    case "PRODUCT":
                                        if (application == null) {
                                            application = new Application();
                                            request = (request == null) ? new Request() : request;
                                            request.setApplication(application);
                                        }
                                        application.setProductID(Product.getProductIdFromName(value));
                                        application.setLoanType(value);
                                        applicationRequest.getHeader().setProduct(Product.getProductFromName(value));
                                        applicationRequest.setRequest(request);
                                        break;
                                    case "CUSTOMERTYPE":
                                        applicationRequest.setApplicantType(value);
                                        break;
                                    case "LOANPURPOSE":
                                        if (application == null) {
                                            application = new Application();
                                            request = (request == null) ? new Request() : request;
                                            request.setApplication(application);
                                        }
                                        application.setLoanPurpose(value);
                                        applicationRequest.setRequest(request);
                                        break;
                                    case "SOURCINGCHANNEL":
                                        if (application == null) {
                                            application = new Application();
                                            request = (request == null) ? new Request() : request;
                                            request.setApplication(application);
                                        }
                                        application.setChannel(value);
                                        break;
                                    case "DOCUMENTRECEIVED":
                                        if (application == null) {
                                            application = new Application();
                                            request = (request == null) ? new Request() : request;
                                            request.setApplication(application);
                                        }
//                                application.setDocReceivedDate(new Date(value));
                                        break;
                                    case "TENORREQUESTED":
                                        if (application == null) {
                                            application = new Application();
                                            request = (request == null) ? new Request() : request;
                                            request.setApplication(application);
                                        }
                                        application.setTenorRequested(value);
                                        applicationRequest.setRequest(request);
                                        break;
                                    case "APPLICANTTYPE":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        applicant.setApplicantType(value);
                                        break;
                                    case "APPLICANTCATEGORY":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        applicant.setEmployment(new ArrayList<>());
                                        Employment employment = new Employment();
                                        employment.setEmploymentType(value);
                                        break;
                                    case "APPLICANTNAME":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        Name applicantName = new Name();
                                        applicantName.setFirstName(value);
                                        applicant.setApplicantName(applicantName);
                                        break;
                                    case "ADDRESSTYPE":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        List<CustomerAddress> address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        CustomerAddress customerAddress = new CustomerAddress();
                                        customerAddress.setAddressType(value);
                                        address.add(customerAddress);
                                        applicant.setAddress(address);
                                        break;
                                    case "ADDRESSLINE1":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setAddressLine1(value);
                                        break;
                                    case "ADDRESSLINE2":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setAddressLine2(value);
                                        break;
                                    case "LANDMARK":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setLandMark(value);
                                        break;
                                    case "PINCODE":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setPin(Long.valueOf(value));
                                        break;
                                    case "CITY":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setCity(value);
                                        break;
                                    case "STATE":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        address = (CollectionUtils.isEmpty(applicant.getAddress())) ? new ArrayList<>() : applicant.getAddress();
                                        if (address.get(0) == null) {
                                            addr = new CustomerAddress();
                                            address.add(addr);
                                        } else {
                                            addr = address.get(0);
                                        }
                                        addr.setState(value);
                                        break;
                                    case "MOBILE":
                                        if (applicant == null) {
                                            applicant = new Applicant();
                                            applicant.setApplicantId("0");
                                            request = (request == null) ? new Request() : request;
                                            request.setApplicant(applicant);
                                        }
                                        applicant.setPhone(new ArrayList<>());
                                        Phone phone = new Phone();
                                        phone.setPhoneNumber(value);
                                        applicant.getPhone().add(phone);
                                        break;
                                    default:
                                        logger.debug("unable to find mapping for" + headerValue.getStringCellValue());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                logger.debug("Error occurred while processing record on line" + rowCount);
                            }
                        }
                    }
                    appMetaData = new AppMetaData();
                    appMetaData.setBranchV2(branchV2);
                    applicationRequest.setAppMetaData(appMetaData);
                    //set another address
                    if(CollectionUtils.isNotEmpty(applicant.getAddress())){
                        CustomerAddress tempAddr = applicant.getAddress().get(0);
                        CustomerAddress newAddr = new CustomerAddress();
                        String addrType = "RESIDENCE";
                        if(StringUtils.equalsIgnoreCase(tempAddr.getAddressType(), "RESIDENCE")){
                            tempAddr.setAddressType(tempAddr.getAddressType().toUpperCase());
                            addrType = "OFFICE";
                        }
                        newAddr.setAddressType(addrType);
                        applicant.getAddress().add(newAddr);
                    }
                    applicationRequest.setRequest(request);
                    Object object = dataEntryManager.generateAndSetRefId(applicationRequest);
                    if (object instanceof Acknowledgement) {
                        logger.debug("Error in refId generation");
                    } else {
                        applicationRepository.saveApplication(applicationRequest);
                        GoNoGoCustomerApplication goNoGoCustomerApplication = RequestAggregator.getGoNoGo(applicationRequest);
                        // Save GnGCustomerApplication
                        applicationRepository.saveGoNoGoCustomerApplication(goNoGoCustomerApplication);
                        refIdsGenerated.add("Successfully uploaded row" + rowNo + " RefId : " + applicationRequest.getRefID());
                    }
                } catch (Exception e) {
                    logger.debug("Unable to process row : " + rowNo);
                    refIdsGenerated.add("Unable to upload case data for row no :: " + rowNo);
                }
            }
        }
        masterRecords.setMasterList(refIdsGenerated);
        fileUploadStatus.setStatus(Status.SUCCESS.name());
        fileUploadStatus.setNumberOfRecords(sheet.getLastRowNum()-1);
        fileUploadStatus.setNumberOfRecordSuccess(refIdsGenerated.size());
        masterRecords.setFileUploadStatus(fileUploadStatus);
        return masterRecords;
    }

    private Map<String,Branch> getNameBranchMap(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        Map<String,Branch> branchIdObjMap = new HashMap<>();
        hierarchyMasterV2List.forEach(master -> {
            if(!branchIdObjMap.containsKey(master.getBranch().getBranchName())){
                String branchName = master.getBranch().getBranchName().replaceAll(" ","").toLowerCase();
                branchIdObjMap.put(branchName, master.getBranch());
            }
        });
        return branchIdObjMap;
    }
}
