package com.softcell.service.parser;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.MasterRecords;
import org.apache.commons.csv.CSVParser;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Date;

/**
 * @author yogeshb
 */
public interface MasterFileCSVParser {

    Date insertDate = new Date();

    /**
     * @param parser
     * @return This method return SchemeMaster list and error if any exist.
     */
    MasterRecords parseSchemeMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return AssetModelMaster list and error if any exist.
     */
    MasterRecords parseAssetModelMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return PinCodeMaster list and error if any exist.
     */
    MasterRecords parsePinCodeMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return EmployerMaster list and error if any exist.
     */
    MasterRecords parseEmployerMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return DealerEmailMaster list and error if any exist.
     */
    MasterRecords parseDealerEmailMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return GNGDealerEmailMaster list and error if any exist.
     */
    MasterRecords parseGNGDealerEmailMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return SchemeModelDealerCityMapping list and error if any exist.
     */
    MasterRecords parseSchemeModelDealerCityMapping(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return SchemeDateMapping list and error if any exist.
     */
    MasterRecords parseSchemeDateMapping(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return HitachiSchemeMaster list and error if any exist.
     */
    MasterRecords parseHitachiSchemeMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return This method return CarSurrogateMaster list and error if any exist.
     */
    MasterRecords parseCarSurrogateMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return
     */
    MasterRecords parsePlPincodeEmailMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return bankDetailsMaster list and error if any exists.
     */
    MasterRecords parseBankDetailsMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return bankDetailsMaster list and error if any exists.
     */
    MasterRecords parseHierarchyMaster(CSVParser parser, String institutionId);


    /**
     * @param parser
     * @param institutionId
     * @return bankDetailsMaster list and error if any exists.
     */
    MasterRecords parseCDLHierarchyMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return errors if any exists
     */
    MasterRecords parseCityStateMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @return
     */
    MasterRecords parseVersionCofigurationMaster(CSVParser parser);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseModelVariantMaster(CSVParser parser,
                                          String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseAssetCategoryMaster(CSVParser parser,
                                           String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseStateMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseCityMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseCreditPromotionMaster(CSVParser parser,
                                             String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseDsaBranchMaster(CSVParser parser,
                                       String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseDsaProductMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseDealerBranchMaster(CSVParser parser,
                                          String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseNegativeAreaGeoLimitMaster(CSVParser parser,
                                                  String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseLosDetailsMaster(CSVParser parser,
                                        String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseNetDisbursalMaster(CSVParser parser,
                                        String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseRelianceDealerBranchMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @param product
     * @return
     */
    MasterRecords parseReferenceDetailsMaster(CSVParser parser, String institutionId, String product);

    MasterRecords parseDeviationMaster(CSVParser parser, String InstitutionId, String product);

    /**
     *
     * @param wb
     * @return
     */
    MasterRecords parseBankingMaster(HSSFWorkbook wb,String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseLoyaltyCardMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseGeneralParameterMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseBankDetailsCsvMaster(CSVParser parser,String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseLosBankMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseEwInsGngAssetCategoryMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseEwPremiumDataMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseInsurancePremiumMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseElecServProvMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseBranchMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseSourcingDetailMaster(CSVParser parser, String institutionId);

    /**
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseTkilDealerBranchMaster(CSVParser parser, String institutionId);


    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parsePromotionalSchemeMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseBankMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parsePaynimoBankMaster(CSVParser parser, String institutionId);

    /**
     *
     * @param parser
     * @param institutionId
     * @return
     */
    MasterRecords parseDigitizationEmailMaster(CSVParser parser, String institutionId);

    MasterRecords parseOrganizationalHierarchyMaster(CSVParser parser, String institutionId);

    MasterRecords parseCustomerDetailsMaster(XSSFSheet sheet, String institutionId, String product);

    MasterRecords parseRoiSchemeMasterDetails(CSVParser parser, String institutionId, String product);
}
