package com.softcell.service;

import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yogeshb
 */
public interface CroManager {
    /**
     * @param checkApplicationStatus
     * @return This method get all Data of Application against refID.
     */
    public BaseResponse getApplicationData(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    public BaseResponse getApplicationDataByMob(
            GetGngByMobRequest getGngByMobRequest) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return This method use to get all image id with few more image details.
     */
    public BaseResponse getApplicationImages(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return This method retrieve ApplicationRequest object to on dashboard.
     */
    public BaseResponse getDashboardData(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return This method get all Data of Application for Cro 2 against refID.
     */
    public BaseResponse getApplicationDataForCroTwoScreen(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return This method get all partial saved data in Application Documents
     */
    public BaseResponse getPartialSavedApplicationData(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param croApprovalRequest
     * @param httpHeaders
     * @param httpRequest
     * @return This service is used to set cro decision of application either
     * Approved or Declined.
     */
    public BaseResponse setCroDecesion(
            CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception;

    /**
     * @param croApprovalRequest
     * @return This service is used to reset Status of Application.
     */
    public BaseResponse resetStatus(CroApprovalRequest croApprovalRequest) throws Exception;

    /**
     * @param croApprovalRequest
     * @param httpHeaders
     * @param httpRequest
     * @return This service is used to set cro decision of application in case
     * of On-Hold.
     */
    public BaseResponse setCroDecesionOnHold(
            CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception;

    /**
     * @param croQueueRequest
     * @return This method pull Application metadata with pagination.
     */
    public BaseResponse getCroQueue(CroQueueRequest croQueueRequest) throws Exception;

    /**
     * @param croQueueRequest
     * @return This method pull Application metadata with pagination for CRO2
     * Screen.
     */
    public BaseResponse getCroTwoQueue(CroQueueRequest croQueueRequest) throws Exception;

    /**
     * @param croQueueRequest
     * @return Information unavailable
     */
    public BaseResponse getCroQueueCriteria(CroQueueRequest croQueueRequest) throws Exception;

    /**
     * @param croQueueRequest
     * @return Information unavailable
     */
    public BaseResponse getCro3Queue(CroQueueRequest croQueueRequest) throws Exception;

    /**
     * @param croQueueRequest
     * @return Information unavailable
     */
    public BaseResponse getCro4Queue(CroQueueRequest croQueueRequest) throws Exception;


    /**
     * @param checkApplicationStatus
     * @return This method pull post ipa details for dsa.
     * @category DSA need to move on DSA controller
     */
    public BaseResponse getpostIpo(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param lOSDetailsRequest
     * @return This method update los details.
     */
    public BaseResponse updateLosDetails(LOSDetailsRequest lOSDetailsRequest) throws Exception;

    /**
     * @param invoiceDetailsRequest
     * @return
     */
    public BaseResponse updateInvoiceDetails(
            InvoiceDetailsRequest invoiceDetailsRequest) throws Exception;

    /**
     * This method is use to get co-applicant data
     *
     * @param checkApplicationStatus
     * @return
     */
    public BaseResponse getCoApplicationData(CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * This method will merge application request and gonogo request into
     * common applicationRequest list
     * @param checkApplicationStatus
     * @return
     */
    BaseResponse getMergeApplicationRequest(CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     *
     * @param applicationStatusLogRequest
     * @return
     * @throws Exception
     */
    BaseResponse getApplicationStatusLog(ApplicationStatusLogRequest applicationStatusLogRequest)throws Exception;

    /**
     *
     * @param applicationStatusLogRequest
     * @return
     */
    BaseResponse getDedupeApplicationDetails(ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception;

    /**
     * update Tvr Status and Remarks
     * @param updateTvrStatusRequest
     * @param httpHeaders
     * @param httpRequest
     * @return
     * @throws Exception
     */
    BaseResponse setTVRStatus(UpdateTvrStatusRequest updateTvrStatusRequest, HttpHeaders httpHeaders,
                                     HttpServletRequest httpRequest) throws Exception;

    /**
     *
     * @param getTvrStatusRequest
     * @param httpHeaders
     * @param httpRequest
     * @return
     * @throws Exception
     */
    BaseResponse getTVRStatus(GetTvrStatusRequest getTvrStatusRequest, HttpHeaders httpHeaders,
                              HttpServletRequest httpRequest) throws Exception;

    /**
     *
     * @param checkApplicationStatus
     * @return
     * @throws Exception
     */
    BaseResponse getDocumentsForDmsPush(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     *
     * @param applicationStatusLogRequest
     * @return
     */
    BaseResponse getPosidexDedupeApplicationDetails(ApplicationStatusLogRequest applicationStatusLogRequest);

    /**
     *
     * @param additionalDedupeFieldsRequest
     * @return
     */
    BaseResponse updateDedupeParameter(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest);

    BaseResponse reassignApplication(ReassignApplicationRequest request, HttpServletRequest httpRequest) throws Exception;

    BaseResponse resetAllocationInfo(ChangeAllocationInfoRequest reassignApplicationRequest,
                                     HttpServletRequest httpRequest) throws Exception;

    void updateDecision(CroApprovalRequest croApprovalRequest, HttpServletRequest httpRequest) throws Exception;

    /**
     * added for FOS (can add remark for SBFC)
     */
    public BaseResponse fosAddRemark(
            CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception;
}
