package com.softcell.service;

import com.softcell.gonogo.model.request.queue.*;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by archana on 14/7/17.
 */
public interface QueueAdminManager {

    BaseResponse getStatus(QueueStatusRequest queueStatusRequest) throws Exception  ;

    BaseResponse getCroAuditData(CroAuditRequest croAuditRequest) throws Exception;

    BaseResponse getCroStatistics(CroAuditRequest croAuditRequest) throws Exception;

    BaseResponse assignCases(CaseAllocationRequest caseAllocationRequest) throws Exception;

    BaseResponse getSchedulerInfo(SchedulerInfoRequest schedulerInfoRequest);

    BaseResponse runScheduler(RunSchedulerRequest runSchedulerRequest);

    BaseResponse changeAvailability(ChangeAvailabilityRequest changeAvailabilityRequest);

    BaseResponse getAllUnassignedApplicationIds();
}
