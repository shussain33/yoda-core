package com.softcell.service;

import com.softcell.gonogo.model.core.error.FileUploadStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yogeshb
 */
public interface FileParserManager {

    FileUploadStatus parseSchemesMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseAssetModelMaster(MultipartFile file, String institutionId);

    FileUploadStatus parsePincodeMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseEmployerMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseDealerEmailMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseSchemeModelDealerCityMappingMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseCarSurrogateMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseHitachiSchemeMaster(MultipartFile file, String institutionId);

    FileUploadStatus parseSchemeDateMappingMaster(MultipartFile file, String institutionId);

}
