package com.softcell.service;

import com.softcell.gonogo.model.core.ThirdPartyRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;


public interface DataConversionManager {

    BaseResponse downloadExcel(ThirdPartyRequest thirdPartyRequest);
}
