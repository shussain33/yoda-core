package com.softcell.service;

import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by AmitBotre on 30/12/19.
 */
public interface DashboardManger {

    BaseResponse getDashBoardData(DashboardRequest dashboardRequest) throws Exception;
}
