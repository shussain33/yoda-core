package com.softcell.service;

import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface ApplicationTrackerManager {

    public BaseResponse getApplicationCaseHistory(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    public BaseResponse getApplicationCSV(ReportingModuleConfiguration configuration) throws Exception;
}
