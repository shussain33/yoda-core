/**
 * yogeshb10:11:10 am  Copyright Softcell Technology
 **/
package com.softcell.service.docs;

import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.softcell.constants.CustomFormat;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.UploadFileMongoRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * @author yogeshb
 *
 */
public class DemoPdfGenerator {
    private static final Logger logger = LoggerFactory.getLogger(DemoPdfGenerator.class);

    public static void main(String[] args) {
        DeliveryOrderReport deliveryOrderReport = new DeliveryOrderReport();
        deliveryOrderReport.setDealerName("Chellamani & Co Nungambakkam ");
        deliveryOrderReport.setCustomerName("Anil Cyril");
        deliveryOrderReport.setAssetModelName("AssetModelName");
        deliveryOrderReport
                .setCustomerAddress("Mahaveer Flats, Abith Colony, Alandur Road, Saidapet, Chennai 600");
        deliveryOrderReport.setDealerSubvention(800);
        deliveryOrderReport.setManufSubBorneByDealer(845);
        deliveryOrderReport.setManufSubventionMbd(500);
        deliveryOrderReport.setProductMake("TOP LOAD");
        deliveryOrderReport.setProductBrand("GODREJ");
        deliveryOrderReport.setProductCategory("WASHING MACHINE");
        deliveryOrderReport.setProductModel("LED 21 HD1");
        deliveryOrderReport.setSchemeCode("00168");
        deliveryOrderReport.setSchemeDscr("TABV30K4B18");
        deliveryOrderReport.setSchemeType("(18/4)");
        deliveryOrderReport.setInstID("4045");
        deliveryOrderReport.setProductCost(100000);
        deliveryOrderReport.setEmi(1994.4);
        deliveryOrderReport.setProcessingFees(50000);
        deliveryOrderReport.setMarginMoney(5000.84);
        deliveryOrderReport.setAdvanceEMI(2000.55);
        deliveryOrderReport.setFinanceAmount(deliveryOrderReport
                .getProductCost() - deliveryOrderReport.getMarginMoney());
        deliveryOrderReport.setDeductionsByHDBFS(900);
        deliveryOrderReport.setFinalDisbursementAmount(deliveryOrderReport
                .getFinanceAmount()
                - deliveryOrderReport.getDeductionsByHDBFS());
        deliveryOrderReport.setOtherChargesIfAny(850);

        deliveryOrderReport.setSumOfDeductions(deliveryOrderReport
                .getProcessingFees()
                + deliveryOrderReport.getAdvanceEMI()
                + deliveryOrderReport.getDealerSubvention()
                + deliveryOrderReport.getManufSubBorneByDealer()
                + deliveryOrderReport.getManufSubventionMbd()
                + deliveryOrderReport.getOtherChargesIfAny());

        DemoPdfGenerator pdfGenerator = new DemoPdfGenerator();

        ByteArrayOutputStream outputStream = null;
        // now write the PDF content to the output stream
        try {
            outputStream = new ByteArrayOutputStream();
            pdfGenerator.writePdf(outputStream, deliveryOrderReport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] bytes = outputStream.toByteArray();

    }

    private void writePdf(OutputStream outputStream,
                          DeliveryOrderReport deliveryOrderReport) throws Exception {
        try {
            Document doc = new Document(PageSize.A4);
            PdfWriter.getInstance(doc, outputStream);


            Font f3 = new Font(FontFamily.TIMES_ROMAN, 10.0f, Font.BOLD,
                    BaseColor.BLACK);

            Font commonFont = new Font(FontFamily.TIMES_ROMAN, 9.0f,
                    Font.NORMAL, BaseColor.BLACK);

            Font commonFontBold = new Font(FontFamily.TIMES_ROMAN, 9.0f,
                    Font.BOLD, BaseColor.BLACK);

            doc.addAuthor("www.gonogo.com");
            doc.addTitle("DO");
            doc.open();

            FileUploadRequest fileUploadRequest = getFileUploadRequest(deliveryOrderReport);

            UploadFileRepository uploadFileRepository = new UploadFileMongoRepository(
                    MongoConfig.getMongoTemplate(),
                    MongoConfig.getGridFSTemplate());

            Image SoftcellLogoImage = uploadFileRepository
                    .getHdbfsImage(fileUploadRequest);

            if (SoftcellLogoImage != null) {
                doc.add(SoftcellLogoImage);

            } else {
                logger.error("Problem in Fetching Softcell Logo");
            }

            PdfPTable pdfPTable2 = new PdfPTable(2);
            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.setHorizontalAlignment(Element.ALIGN_RIGHT);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable.addCell(pdfPCell);

            Paragraph paragraph = new Paragraph();
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            paragraph.setFont(commonFontBold);

            Chunk chunk = new Chunk("Softcell Financial Services Limited");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("Softcell 301,Mayfair Towers,");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("28, Mumbai-Pune Road,");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("Wakdewadi,Pune - 411 005,");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("Maharashtra.");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("Web: www.Softcell.com");
            chunk.append("\n");
            paragraph.add(chunk);

            chunk = new Chunk("CIN : SOFTCELLUAUUCIN1237");
            chunk.append("\n");
            paragraph.add(chunk);
            chunk = new Chunk("Email:reporter-noreply@Softcell.com");
            chunk.append("\n");
            chunk.append("\n");
            paragraph.add(chunk);

            DateTime date = new DateTime();

            chunk = new Chunk("Date : " + GngDateUtil.getDdMmYyyy(date));

            chunk.append("\n");
            Font font = new Font();
            font.setSize(8);
            chunk.setFont(font);
            paragraph.add(chunk);

            pdfPCell = new PdfPCell(paragraph);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable.addCell(pdfPCell);
            pdfPTable.addCell(pdfPCell);
            pdfPTable.getDefaultCell().setBorder(0);
            pdfPTable.setWidthPercentage(100);
            float[] columnWidths1 = new float[]{40f, 60f};
            pdfPTable.setWidths(columnWidths1);

            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable2.addCell(pdfPCell);
            pdfPCell = new PdfPCell(pdfPTable);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable2.addCell(pdfPCell);
            pdfPTable2.setWidthPercentage(100);
            doc.add(pdfPTable2);

            Paragraph titleParagraph = new Paragraph();
            titleParagraph.setSpacingBefore(3f);
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setFont(f3);
            Chunk tittle = new Chunk("Delivery Order");
            tittle.setUnderline(0.5f, -2f);
            titleParagraph.add(tittle);
            doc.add(titleParagraph);

            Paragraph greetParagraph = new Paragraph();
            greetParagraph.setSpacingAfter(2);
            greetParagraph.setFont(commonFont);
            greetParagraph.add(deliveryOrderReport.getDealerName());
            doc.add(greetParagraph);

            greetParagraph = new Paragraph();
            greetParagraph.setSpacingAfter(2);
            greetParagraph.setFont(commonFont);
            greetParagraph.add("Dear Sir/Madam,");
            doc.add(greetParagraph);

            Paragraph subParagraph = new Paragraph();
            subParagraph.setSpacingAfter(3f);
            subParagraph.setFont(commonFont);
            subParagraph.add("Sub-Delivery Order - Softcell/CD/DO/ "
                    + deliveryOrderReport.getRefID() + "- Dealer Copy");

            doc.add(subParagraph);

            Paragraph mainParagraph = new Paragraph();
            mainParagraph.setSpacingAfter(2f);
            mainParagraph.setFont(commonFont);
            mainParagraph
                    .add("We have sanctioned a loan facility for the purchase of ");
            Phrase phrase = new Phrase();
            phrase.setFont(commonFontBold);

            Chunk productName = new Chunk(deliveryOrderReport.getProductBrand()
                    + " " + deliveryOrderReport.getProductModel());
            productName.setUnderline(0.5f, -2f);
            productName.setFont(commonFontBold);

            mainParagraph.add(productName);
            mainParagraph.add(" to our Customer ");
            phrase = new Phrase();
            phrase.setFont(commonFontBold);

            Chunk underline = new Chunk(deliveryOrderReport.getCustomerName());
            underline.setUnderline(0.5f, -2f);
            phrase.add(underline);
            mainParagraph.add(phrase);
            mainParagraph
                    .add(". Pursuant to the agreement executed by the Customer in our favour with respect to the Loan facility and on the basis of the instructions of the Customer we are disbursing the following amount in your favour.");
            doc.add(mainParagraph);

            Paragraph instructionPara = new Paragraph();
            instructionPara.setSpacingAfter(5);
            instructionPara.setFont(commonFont);
            instructionPara
                    .add("Kindly arrange the delivery of the Product to the customer at the below address only.");
            doc.add(instructionPara);

            Paragraph productDetailsParagraph = new Paragraph();
            productDetailsParagraph.setFont(commonFont);
            productDetailsParagraph.add("Customer Name:  "
                    + deliveryOrderReport.getCustomerName());
            doc.add(productDetailsParagraph);

            Paragraph customerAddrParagraph = new Paragraph();
            customerAddrParagraph.setFont(commonFont);
            customerAddrParagraph.add("Customer Address: "
                    + deliveryOrderReport.getCustomerAddress());
            doc.add(customerAddrParagraph);

            Paragraph prodBrandParagraph = new Paragraph();
            prodBrandParagraph.setFont(commonFont);
            prodBrandParagraph.add("Product Brand: "
                    + deliveryOrderReport.getProductBrand());
            doc.add(prodBrandParagraph);

            Paragraph prodMakeParagraph = new Paragraph();
            prodMakeParagraph.setFont(commonFont);
            prodMakeParagraph.add("Product Catg & Make: "
                    + deliveryOrderReport.getProductCategory() + " - "
                    + deliveryOrderReport.getProductMake());
            doc.add(prodMakeParagraph);

            Paragraph prodModelParagraph = new Paragraph();
            prodModelParagraph.setFont(commonFont);
            prodModelParagraph.add("Product Model:  "
                    + deliveryOrderReport.getProductModel());
            doc.add(prodModelParagraph);

            Paragraph SchemeCodeAndTypeParagraph = new Paragraph();
            SchemeCodeAndTypeParagraph.setFont(commonFont);
            SchemeCodeAndTypeParagraph.add("Scheme Code & EMI:  "
                    + deliveryOrderReport.getSchemeCode()
                    + " - "
                    + deliveryOrderReport.getSchemeDscr()
                    + " - "
                    + deliveryOrderReport.getSchemeType()
                    + " & EMI Rs "
                    + CustomFormat.NUMBER_0.format(deliveryOrderReport
                    .getEmi()));
            SchemeCodeAndTypeParagraph.setSpacingAfter(10f);
            doc.add(SchemeCodeAndTypeParagraph);

            PdfPTable table = new PdfPTable(4);
            float[] columnWidths = new float[]{30f, 10f, 30f, 10f};
            table.setWidths(columnWidths);

            table.setWidthPercentage(100);
            table.setHorizontalAlignment(table.ALIGN_JUSTIFIED);
            PdfPCell c1 = new PdfPCell(new Phrase("Details", commonFontBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);

            c1.setBackgroundColor(BaseColor.GRAY);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Amount (Rs)", commonFontBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.GRAY);
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Deductions by Softcell", commonFontBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.GRAY);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Amount (Rs)", commonFontBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.GRAY);

            table.addCell(c1);

            table.setHeaderRows(1);

            table.addCell(new Phrase("A. Product Cost", commonFont));

            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getProductCost()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("1. Processing Fees", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getProcessingFees()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("B. Margin Money", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getMarginMoney()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);
            table.addCell(new Phrase("2. Advance EMI", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getAdvanceEMI()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("C. Finance Amount (A-B)", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getFinanceAmount()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);
            table.addCell(new Phrase("3. Dealer Subvention", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getDealerSubvention()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("D. Deductions by Softcell", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getDeductionsByHDBFS()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);
            table.addCell(new Phrase(
                    "4. Manufacturer Subvention borne by Dealer", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getManufSubBorneByDealer()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase(
                    "E. Total Amount to be collected from the customer ( 1 + 2 + 5 + B )",
                    commonFontBold));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getTotalAmtCollectedFromCustomer()), commonFontBold));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);

            table.addCell(c1);
            table.addCell(new Phrase("5. Other Charges if any", commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getOtherChargesIfAny()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("F. Net Loan Amount (C - 2)",
                    commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getNetFundingAmount()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("Sum of Deductions (1+2+3+4+5)",
                    commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getSumOfDeductions()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            table.addCell(new Phrase("G. Final Disbursement Amount ( C - D )",
                    commonFont));
            c1 = new PdfPCell(new Phrase(
                    CustomFormat.NUMBER_0.format(deliveryOrderReport
                            .getFinalDisbursementAmount()), commonFont));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(c1);

            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPTable.addCell(pdfPCell);
            table.addCell(pdfPCell);
            table.addCell(pdfPCell);

            doc.add(table);

            Paragraph bulletPointParagraph = new Paragraph();
            bulletPointParagraph.setFont(commonFontBold);
            bulletPointParagraph.setSpacingBefore(3f);

            Chunk term = new Chunk("Terms");
            term.setUnderline(0.5f, -2f);
            term.setFont(commonFontBold);

            bulletPointParagraph.add(term);
            doc.add(bulletPointParagraph);
            Chunk customerAddress = new Chunk("Customer Address");
            Chunk leftAddressContent = new Chunk(
                    "Funded Product should be delivered at the above ");
            leftAddressContent.setFont(commonFont);
            Chunk rightAddressContent = new Chunk(
                    " only. Change in the place of delivery will not to be entertained at any point of time.");
            rightAddressContent.setFont(commonFont);
            customerAddress.setUnderline(0.5f, -2f);
            customerAddress.setFont(commonFontBold);
            Phrase addressPhrase = new Phrase();
            addressPhrase.add(leftAddressContent);
            addressPhrase.add(customerAddress);
            addressPhrase.add(rightAddressContent);

            List unorderedTermsList = new List(List.UNORDERED);
            unorderedTermsList.setAutoindent(true);
            unorderedTermsList
                    .add(new ListItem(
                            "Any amount more than the Final Disbursement Amount is to be collected from the customer before release of the Product.",
                            commonFont));
            unorderedTermsList
                    .add(new ListItem(
                            "The Final Disbursement Amount would be paid to you on the explicit understanding that any sum payable/ refundable by you to the customer, would be paid/ refunded by you, only to us (SOFTCELL FINANCIAL SERVICES LTD)",
                            commonFont));
            unorderedTermsList
                    .add(new ListItem(
                            "Original Financier copy of the Invoice with HYP marked to SOFTCELL FINANCIAL SERVICES LTD, copy of duly receipted Delivery Challan/Invoice/Delivery Receipt/Margin Money Receipt should be sent to SOFTCELL FINANCIAL SERVICES LTD for processing the Final Disbursement Amount.",
                            commonFont));
            unorderedTermsList.add(new ListItem(addressPhrase));
            unorderedTermsList
                    .add(new ListItem(
                            "This Delivery Order is valid for 15 days from the date of issue and automatically expires unless renewed. It can also be revoked by us during it currency before the delivery with intimation to you number of days is changed.",
                            commonFont));
            unorderedTermsList
                    .add(new ListItem(
                            "Assets where serial number validation is mandatory, Liability of Softcell Ltd arise only after successful validation of Serial number.",
                            commonFont));
            doc.add(unorderedTermsList);

            Chunk paymentTerms = new Chunk("Payment Terms");
            paymentTerms.setUnderline(0.5f, -2f);
            paymentTerms.setFont(commonFontBold);

            doc.add(paymentTerms);

            List unorderedPaymentTermList = new List(List.UNORDERED);
            unorderedPaymentTermList
                    .add(new ListItem(
                            "We shall release payment as per the agreed terms mutually.",
                            commonFont));
            doc.add(unorderedPaymentTermList);

            Paragraph noteParagraph = new Paragraph();
            noteParagraph.setSpacingBefore(3f);

            phrase = new Phrase();
            phrase.setFont(commonFontBold);
            phrase.add("Note:");
            noteParagraph.add(phrase);

            phrase = new Phrase();
            phrase.setFont(commonFont);
            phrase.add(" This is computer generated Delivery Order hence Signature not required.");
            noteParagraph.add(phrase);

            doc.add(noteParagraph);

            Paragraph officeAddress = new Paragraph();
            officeAddress.setSpacingBefore(5f);
            officeAddress.setFont(commonFontBold);
            officeAddress.setAlignment(Element.ALIGN_CENTER);
            officeAddress
                    .add("Registered Ofiice: Softcell | 504, Mayfair Towers, 28, Mumbai-Pune Road Wakdewadi, Pune - 411 005");
            doc.add(officeAddress);
            doc.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private FileUploadRequest getFileUploadRequest(
            DeliveryOrderReport deliveryOrderReport) {
        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId(deliveryOrderReport.getInstID());

        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("Softcell.png");
        uploadFileDetails.setFileType("png");

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId("Softcell-DO-LOGO-01");
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        return fileUploadRequest;
    }

    public byte[] getPdfByte(PostIpaRequest postIpaRequest) {

        DeliveryOrderReport deliveryOrderReport = new DeliveryOrderReport();
        ByteArrayOutputStream outputStream = null;
        byte[] bytes = null;
        try {
            outputStream = new ByteArrayOutputStream();
            getCustomerData(deliveryOrderReport, postIpaRequest);
            writePdf(outputStream, deliveryOrderReport);
            bytes = outputStream.toByteArray();
            return bytes;
        } catch (Exception e) {
            e.printStackTrace();
            return bytes;
        }
    }

    /**
     * @param deliveryOrderReport
     */
    private void getCustomerData(DeliveryOrderReport deliveryOrderReport,
                                 PostIpaRequest postIpaRequest) {

        FinalApplicationMongoRepository applicationRepository = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
        GoNoGoCustomerApplication goNoGoapplicationRequest = applicationRepository
                .getGoNoGoApplication(postIpaRequest.getRefID());

        ApplicationRequest applicationRequest = goNoGoapplicationRequest
                .getApplicationRequest();
        Name name = applicationRequest.getRequest().getApplicant()
                .getApplicantName();
        StringBuffer nameBuffer = new StringBuffer();
        if (StringUtils.isNotBlank(name.getFirstName()))
            nameBuffer.append(name.getFirstName()).append(' ');
        if (StringUtils.isNotBlank(name.getMiddleName()))
            nameBuffer.append(name.getMiddleName()).append(' ');
        if (StringUtils.isNotBlank(name.getLastName()))
            nameBuffer.append(name.getLastName()).append(' ');
        deliveryOrderReport.setCustomerName(nameBuffer.toString());

        java.util.List<AssetDetails> assetList = postIpaRequest.getPostIPA()
                .getAssetDetails();
        if (assetList != null) {
            for (AssetDetails asset : assetList) {
                deliveryOrderReport.setProductMake(asset.getAssetModelMake());
                deliveryOrderReport.setProductBrand(asset.getAssetMake());
                deliveryOrderReport.setProductCategory(asset.getAssetCtg());
                deliveryOrderReport.setProductModel(asset.getModelNo());
                deliveryOrderReport.setDealerName(asset.getDlrName());
                break;
            }
        }

        java.util.List<CustomerAddress> addresses = applicationRequest
                .getRequest().getApplicant().getAddress();
        if (addresses != null) {
            for (CustomerAddress address : addresses) {

                StringBuffer addressBuffer = new StringBuffer();
                if (StringUtils.isNotBlank(address.getAddressLine1())) {
                    addressBuffer.append(address.getAddressLine1()).append(',')
                            .append(' ');
                }

                if (StringUtils.isNotBlank(address.getAddressLine2())) {
                    addressBuffer.append(address.getAddressLine2()).append(',')
                            .append(' ');
                }

                if (StringUtils.isNotBlank(address.getLine3())) {
                    addressBuffer.append(address.getLine3()).append(',')
                            .append(' ');
                }

                if (StringUtils.isNotBlank(address.getCity())) {
                    addressBuffer.append(address.getCity()).append(',')
                            .append(' ');
                }

                addressBuffer.append(" - ").append(address.getPin())
                        .append(',').append(' ');
                deliveryOrderReport
                        .setCustomerAddress(addressBuffer.toString());

                break;
            }
            if (postIpaRequest.getPostIPA() != null) {
                PostIPA postIpa = postIpaRequest.getPostIPA();
                deliveryOrderReport.setEmi(postIpa.getEmi());
                deliveryOrderReport.setRefID(postIpaRequest.getRefID());
                deliveryOrderReport.setProductCost(postIpa.getTotalAssetCost());// Pending
                deliveryOrderReport.setProcessingFees(postIpa
                        .getProcessingFees());
                deliveryOrderReport.setMarginMoney(postIpa.getMarginMoney());
                deliveryOrderReport.setAdvanceEMI(postIpa.getAdvanceEmi());
                deliveryOrderReport.setSchemeCode(postIpa.getScheme());
                deliveryOrderReport.setSchemeDscr(postIpa.getSchemeDscr());
                deliveryOrderReport.setSchemeType("("
                        + postIpa.getTenor() + "/"
                        + postIpa.getAdvanceEMITenor() + ")");
                deliveryOrderReport.setDealerSubvention(postIpa
                        .getDealerSubvention());
                deliveryOrderReport.setManufSubBorneByDealer(postIpa
                        .getManufSubBorneByDealer());
                deliveryOrderReport.setManufSubventionMbd(postIpa
                        .getManufSubventionMbd());
                deliveryOrderReport.setFinanceAmount(deliveryOrderReport
                        .getProductCost()
                        - deliveryOrderReport.getMarginMoney());

                deliveryOrderReport.setDeductionsByHDBFS(postIpa
                        .getProcessingFees()
                        + postIpa.getAdvanceEmi()
                        + postIpa.getManufSubBorneByDealer()
                        + postIpa.getDealerSubvention()
                        + postIpa.getOtherChargesIfAny());
                deliveryOrderReport.setSumOfDeductions(postIpa
                        .getProcessingFees()
                        + postIpa.getAdvanceEmi()
                        + postIpa.getManufSubBorneByDealer()
                        + postIpa.getDealerSubvention()
                        + postIpa.getOtherChargesIfAny());
                deliveryOrderReport
                        .setFinalDisbursementAmount(deliveryOrderReport
                                .getFinanceAmount()
                                - deliveryOrderReport.getDeductionsByHDBFS());

                deliveryOrderReport.setTotalAmtCollectedFromCustomer(postIpa
                        .getProcessingFees()
                        + postIpa.getAdvanceEmi()
                        + postIpa.getMarginMoney()
                        + postIpa.getOtherChargesIfAny());// 1+2+B+5 as per
                // kanhaiya

                deliveryOrderReport.setNetFundingAmount(deliveryOrderReport.getFinanceAmount() - postIpaRequest.getPostIPA().getAdvanceEmi());
                deliveryOrderReport.setInstID(postIpaRequest.getHeader()
                        .getInstitutionId());// added for Softcell logo purpose.
            }
        }

    }

    private DeliveryOrderReport setDeliveryOrderReport(PostIPA postIpa) {
        DeliveryOrderReport deliveryOrderReport = new DeliveryOrderReport();
        deliveryOrderReport.setCustomerName("");// Pending
        deliveryOrderReport.setAssetModelName("");// Pending
        deliveryOrderReport.setCustomerAddress("");// Pending
        deliveryOrderReport.setDealerSubvention(0);// Pending
        deliveryOrderReport.setManufSubBorneByDealer(0);// Pending
        deliveryOrderReport.setProductMake("");
        deliveryOrderReport.setProductModel("");
        deliveryOrderReport.setSchemeCode("");
        deliveryOrderReport.setProductCost(postIpa.getTotalAssetCost());// Pending
        deliveryOrderReport.setProcessingFees(postIpa.getProcessingFees());
        deliveryOrderReport.setMarginMoney(postIpa.getMarginMoney());
        deliveryOrderReport.setAdvanceEMI(postIpa.getAdvanceEmi());
        deliveryOrderReport.setFinanceAmount(0);// Pending
        deliveryOrderReport.setDeductionsByHDBFS(0);// Pending
        deliveryOrderReport.setFinalDisbursementAmount(0);// Pending
        deliveryOrderReport.setOtherChargesIfAny(0);// Pending
        deliveryOrderReport.setSumOfDeductions(0);// Pending
        return deliveryOrderReport;

    }

    public FileUploadRequest setFileUploadRequest(PostIpaRequest postIpaRequest) {
        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileId(postIpaRequest.getRefID());
        uploadFileDetails.setFileType("application/pdf");
        uploadFileDetails.setFileName("DO_"
                + postIpaRequest.getRefID()
                + "_"
                + postIpaRequest.getPostIPA().getAssetDetails().get(0)
                .getModelNo());

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId(postIpaRequest.getRefID());
        fileUploadRequest.setFileHeader(postIpaRequest.getHeader());
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        return fileUploadRequest;
    }
}
