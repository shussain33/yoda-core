package com.softcell.service;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.email.EmailRequest;
import com.softcell.gonogo.model.email.GenericMailRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface EmailSender {
    boolean sendCustomerAckEmail(
            GoNoGoCustomerApplication goNoGoCustomerApplication, String[] to,
            String[] cc, String subjectLine, String templateName);
    public BaseResponse sendVerificationMail(EmailRequest emailRequest);

    public BaseResponse sendMail(GenericMailRequest emailRequest);
}
