package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by ibrar on 04/12/17.
 */

public interface OnidaSerialNumberValidation {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateOnidaSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;
}
