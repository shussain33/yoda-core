package com.softcell.service.serialnumbervalidation;


import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;


/**
 * @author ibrar
 */
public interface ComioSerialNumberValidationManager {

     /*
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateComioImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


    /**
     *
     * @param rollbackRequest
     * @return
     */
    BaseResponse callToComioRollback(RollbackRequest rollbackRequest, ComioRequest comioRequest,WFJobCommDomain comioConfig) throws Exception;


}

