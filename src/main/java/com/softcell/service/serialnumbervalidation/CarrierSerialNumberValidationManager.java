package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by ibrar on 28/12/17.
 */

public interface CarrierSerialNumberValidationManager {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateCarrierSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param carrierConfig
     * @return
     */
    BaseResponse goForCarrierRollback(RollbackRequest rollbackRequest, CarrierRequest carrierRequest, WFJobCommDomain carrierConfig) throws Exception;

}
