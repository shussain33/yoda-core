package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * @author prasenjit wadmare
 * @date 02/01/2018
 */

public interface LavaSerialNumberValidationManager {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return BaseResponse
     * @throws Exception
     */
    BaseResponse validateLava(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

}
