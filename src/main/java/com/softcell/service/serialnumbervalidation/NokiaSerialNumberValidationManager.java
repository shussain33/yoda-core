package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

public interface NokiaSerialNumberValidationManager {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateNokia(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param rollbackRequest, nokiaRequest, nokiaConfig
     * @return
     */
    BaseResponse getNokiaRollback(RollbackRequest rollbackRequest, NokiaRequest nokiaRequest,WFJobCommDomain nokiaConfig) throws Exception;




}
