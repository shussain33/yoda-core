package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoRequest;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.YuhoSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.YuhoSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.YuhoSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by prasenjit wadmare on 19/11/17.
 */
@Service
public class YuhoSerialNumberValidationManagerImpl implements YuhoSerialNumberValidationManager {

    private static final Logger logger = LoggerFactory.getLogger(YuhoSerialNumberValidationManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private YuhoSerialNumberValidationEngine yuhoSerialNumberValidationEngine;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private YuhoSerialNumberValidationBuilder yuhoSerialNumberValidationBuilder;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;



    @Override
    public BaseResponse validateYuho(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode());


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForYuhoImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = GngUtils.getInvalidSerialOrIMEIErrorList();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;



    }


    private BaseResponse goForYuhoImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        activityLog.setRefId(serialSaleConfirmationRequest.getReferenceID());
        // Set status to success only for valid IMEI number
        activityLog.setStatus(Status.FAIL.toString());

        //call to Yuho validation
        Collection<Error> errors = yuhoSerialNumberValidationEngine.validationForYuhoImei(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {
            //getting Yuho config from WFJobCommDomain
            WFJobCommDomain yuhoConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.YUHO_IMEI.toValue());

            if (null != yuhoConfig) {

                YuhoRequest yuhoRequest = yuhoSerialNumberValidationBuilder.buildYuhoImeiNumberRequest(serialSaleConfirmationRequest, yuhoConfig);
                YuhoResponse yuhoResponse = callToYuho(yuhoConfig, yuhoRequest);

                if (null != yuhoResponse && null == yuhoResponse.getError()) {

                    serialNumberResponse = yuhoSerialNumberValidationBuilder.buildYuhoImeiNumberResponse(yuhoResponse);

                    serialNumberInfoBuilder.originalResponse(serialNumberResponse.getMessage());
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    // update application stage to IMEIV
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository.
                                updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        activityLog.setCustomMsg(ErrorCode.VALID_IMEI);

                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(ErrorCode.INVALID_IMEI);
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                } else {
                    //not getting response from Yuho
                    logger.warn("get error from Connector api for {}", UrlType.YUHO_IMEI.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.YUHO.toFaceValue()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.YUHO_IMEI.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }




        } else {
            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for Yuho IMEI number information to the database.
         */
        logger.debug("Saving IMEI number info for {}", UrlType.YUHO_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;


    }


    private YuhoResponse callToYuho(WFJobCommDomain yuhoConfig, YuhoRequest yuhoRequest) throws Exception {
        String url = Arrays.asList(yuhoConfig.getBaseUrl(), yuhoConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (YuhoResponse) TransportUtils.postJsonRequest(yuhoRequest, url, YuhoResponse.class);

    }


}
