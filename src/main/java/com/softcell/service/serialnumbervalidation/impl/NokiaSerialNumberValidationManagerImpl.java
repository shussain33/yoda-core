package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.NokiaSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.NokiaSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.NokiaSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

@Service
public class NokiaSerialNumberValidationManagerImpl implements NokiaSerialNumberValidationManager {

    private static final Logger logger = LoggerFactory.getLogger(NokiaSerialNumberValidationManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private NokiaSerialNumberValidationEngine nokiaSerialNumberValidationEngine;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private NokiaSerialNumberValidationBuilder nokiaSerialNumberValidationBuilder;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;



    @Override
    public BaseResponse validateNokia(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode());


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForNokiaImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = GngUtils.getInvalidSerialOrIMEIErrorList();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;

    }


    private BaseResponse goForNokiaImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception{

        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        activityLog.setRefId(serialSaleConfirmationRequest.getReferenceID());
        // Set status to success only for valid IMEI number
        activityLog.setStatus(Status.FAIL.toString());

        //call to Nokia validation
        Collection<Error> errors = nokiaSerialNumberValidationEngine.validationForNokiaImei(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());

        if (errors.isEmpty()) {

            //getting Nokia config from WFJobCommDomain
            WFJobCommDomain nokiaConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.NOKIA_IMEI.toValue());

            if (null != nokiaConfig) {

                NokiaRequest nokiaRequest = nokiaSerialNumberValidationBuilder.buildNokiaImeiNumberRequest(serialSaleConfirmationRequest, nokiaConfig);
                NokiaResponse nokiaResponse = callToNokia(nokiaConfig, nokiaRequest);

                if (null != nokiaResponse && null == nokiaResponse.getError()) {

                    serialNumberResponse = nokiaSerialNumberValidationBuilder.buildNokiaImeiNumberResponse(nokiaResponse);

                    serialNumberInfoBuilder.originalResponse(serialNumberResponse.getMessage());
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    // update application stage to IMEIV
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository.
                                updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        activityLog.setCustomMsg(ErrorCode.VALID_IMEI);

                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(ErrorCode.INVALID_IMEI);
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);


                } else {
                    //not getting response from Nokia
                    logger.warn("get error from Connector api for {}", UrlType.NOKIA_IMEI.name());
                    errors = GngUtils.getThirdPartyFailureErrorList();

                    serialNumberInfoBuilder.status(Status.ERROR.name());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.NOKIA.toFaceValue()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.NOKIA_IMEI.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }

        } else {
            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for Nokia IMEI number information to the database.
         */
        logger.debug("Saving IMEI number info for {}", UrlType.NOKIA_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;



    }

    @Override
    public BaseResponse getNokiaRollback(RollbackRequest rollbackRequest, NokiaRequest nokiaRequest, WFJobCommDomain nokiaConfig) throws Exception {

        BaseResponse baseResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
        activityLog.setAction(ActionName.IMEI_ROLLBACK.name());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        //getting response from Nokia
        NokiaResponse nokiaResponse = callToNokia(nokiaConfig, nokiaRequest);

        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.imeiNumber(rollbackRequest.getImeiNumber());
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != nokiaResponse && null == nokiaResponse.getError()) {

            SerialNumberResponse serialNumberResponse = nokiaSerialNumberValidationBuilder
                    .buildNokiaImeiRollbackResponse(nokiaResponse);

            if (rollbackRequest.isResetStage() && StringUtils.equalsIgnoreCase(Status.SUCCESS.name(),serialNumberResponse.getStatus()) ) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

                /**
                 * update IMEI-number valid to rollback,because we rollback this IMEI number.
                 */
                externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());


                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.APRV.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {

                rollbackFeatureLogbuilder.status(Status.FAILED.name());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);


        } else {
            //not getting response from Nokia
            logger.warn("get error from Connector api for {}", UrlType.NOKIA_IMEI_ROLLBACK.name());
            activityLog.setCustomMsg(null != nokiaResponse ? nokiaResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, "Nokia"));
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors = GngUtils.getThirdPartyFailureErrorList();
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }

        /**
         * Saving logs for Nokia Rollback to the database.
         */
        logger.debug("Saving Rollback info for {}", UrlType.NOKIA_IMEI_ROLLBACK.name());
        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;

    }

    private NokiaResponse callToNokia(WFJobCommDomain nokiaConfig, NokiaRequest nokiaRequest) throws Exception {
        String url = Arrays.asList(nokiaConfig.getBaseUrl(), nokiaConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (NokiaResponse) TransportUtils.postJsonRequest(nokiaRequest, url, NokiaResponse.class);

    }


}
