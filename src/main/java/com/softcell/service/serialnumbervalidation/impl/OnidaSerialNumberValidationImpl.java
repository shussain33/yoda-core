package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaRequest;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.OnidaSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.OnidaSerialNumberValidation;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.OnidaSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by ibrar on 12/12/17.
 */

@Service
public class OnidaSerialNumberValidationImpl implements OnidaSerialNumberValidation{

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private OnidaSerialNumberValidationEngine onidaSerialNumberValidationEngine;

    @Autowired
    private OnidaSerialNumberValidationBuilder onidaSerialNumberValidationBuilder;


    @Override
    public BaseResponse validateOnidaSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {
        BaseResponse baseResponse;
        Collection<Error> errors;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(ActionName.SERIAL_NUMBER_VALIDATION.name());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        serialNumberInfoBuilder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        serialNumberInfoBuilder.refID(serialSaleConfirmationRequest.getReferenceID());
        serialNumberInfoBuilder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        serialNumberInfoBuilder.vendor(serialSaleConfirmationRequest.getVendor());
        serialNumberInfoBuilder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        serialNumberInfoBuilder.skuCode(serialSaleConfirmationRequest.getSkuCode());

        WFJobCommDomain onidaConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.ONIDA_SERIAL_NUMBER.toValue());

        if (null == onidaConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = onidaSerialNumberValidationEngine.validationForOnidaSerialNumber(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {
                OnidaRequest onidaRequest = onidaSerialNumberValidationBuilder.buildOnidaSerialNumberRequest(serialSaleConfirmationRequest);
                OnidaResponse onidaResponse = callToOnida(onidaConfig, onidaRequest);

                if(null == onidaResponse) {
                    serialNumberInfoBuilder.status(Status.NO_RESPONSE.name());
                    errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.ONIDA))
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.ONIDA));

                } else if (null == onidaResponse.getError()) {
                        SerialNumberResponse serialNumberResponse = onidaSerialNumberValidationBuilder.buildOnidaSerialNumberResponse(onidaResponse);
                        serialNumberInfoBuilder.originalResponse(String.valueOf(onidaResponse.getResponseCode()));
                        serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                        if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {

                            // update application stage to SRNV
                            applicationStagerepository.updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                            activityLog.setStatus(Status.SUCCESS.toString());
                        }
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                    } else {
                        errors.add(Error.builder()
                                .id(onidaResponse.getError().getType())
                                .message(onidaResponse.getError().getMessage())
                                .level(Error.SEVERITY.HIGH.name())
                                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                .build());
                        activityLog.setCustomMsg(onidaResponse.getError().getMessage());
                        baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    }
                } else {
                        serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            } else {
                    serialNumberInfoBuilder.status(Status.PRODUCT_ALLOWED_FAILED.name());
                    errors = GngUtils.getInvalidProductErrorList();

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
                    activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        //  save onida serial number information to the database.
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private OnidaResponse callToOnida(WFJobCommDomain onidaConfig, OnidaRequest onidaRequest) throws Exception {
        String url = Arrays.asList(onidaConfig.getBaseUrl(), onidaConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        return (OnidaResponse) TransportUtils.postJsonRequest(onidaRequest, url, OnidaResponse.class);

    }
}
