package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.ComioSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.ComioSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.ComioSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author ibrar
 */

@Service
public class ComioSerialNumberValidationManagerImpl implements ComioSerialNumberValidationManager{

    @Autowired
    private ComioSerialNumberValidationEngine comioSerialNumberValidationEngine;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private ComioSerialNumberValidationBuilder comioSerialNumberValidationBuilder;

    private static final Logger logger = LoggerFactory.getLogger(ComioSerialNumberValidationManagerImpl.class);


    @Override
    public BaseResponse validateComioImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse;
        Collection<Error> errors;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.skuCode(serialSaleConfirmationRequest.getSkuCode());
        builder.storeCode(serialSaleConfirmationRequest.getStoreCode());

        WFJobCommDomain comioConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.COMIO_IMEI.toValue());

        if (null == comioConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = comioSerialNumberValidationEngine.validationForComioImei(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {
                ComioRequest comioRequest = comioSerialNumberValidationBuilder.buildComioImeiRequest(serialSaleConfirmationRequest, comioConfig);
                ComioResponse comioResponse = callToComio(comioConfig, comioRequest);

                if(null == comioResponse) {
                    builder.status(Status.NO_RESPONSE.name());
                    errors = new ArrayList<>();
                    errors.add(Error.builder().message(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.COMIO)).errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).errorType(Error.ERROR_TYPE.SYSTEM.toValue()).level(Error.SEVERITY.HIGH.name()).build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.COMIO));

                } else if (null != comioResponse && null == comioResponse.getError()) {

                    SerialNumberResponse serialNumberResponse = comioSerialNumberValidationBuilder.buildComioImeiResponse(comioResponse);
                    builder.status(serialNumberResponse.getStatus());
                    builder.originalResponse(serialNumberResponse.getStatus() + ":" + serialNumberResponse.getMessage());
                    serialNumberInfo = builder.build();

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {

                        // update application stage to IMEIV
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    errors = new ArrayList<>();
                    String message = comioResponse.getError().getType()+ " : " + comioResponse.getError().getMessage();
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(message);
                }
            } else {
                builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            builder.status(Status.PRODUCT_ALLOWED_FAILED.name());
            errors = GngUtils.getInvalidProductErrorList();

            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        /**
         * Saving logs
         */
        serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private ComioResponse callToComio(WFJobCommDomain comioConfig, ComioRequest comioRequest) throws Exception {
        String url = Arrays.asList(comioConfig.getBaseUrl(), comioConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (ComioResponse) TransportUtils.postJsonRequest(comioRequest, url, ComioResponse.class);

    }

    public BaseResponse callToComioRollback(RollbackRequest rollbackRequest, ComioRequest comioRequest, WFJobCommDomain comioConfig) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
        activityLog.setAction(ActionName.IMEI_ROLLBACK.name());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        ComioResponse comioResponse = callToComioRollback(comioRequest, comioConfig);

        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.imeiNumber(rollbackRequest.getImeiNumber());
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != comioResponse && null == comioResponse.getError()) {

            SerialNumberResponse serialNumberResponse = comioSerialNumberValidationBuilder
                    .buildComioImeiRollbackResponse(comioResponse);

            if (rollbackRequest.isResetStage() && StringUtils.equalsIgnoreCase(Status.SUCCESS.name(),serialNumberResponse.getStatus())) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

                /**
                 * update IMEI-number valid to rollback,because we rollback this IMEI number.
                 */
                externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());

                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.APRV.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {

                rollbackFeatureLogbuilder.status(Status.FAILED.name());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);


        } else {
            activityLog.setCustomMsg((null != comioResponse && null != comioResponse.getError()) ? comioResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.COMIO));
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message((null != comioResponse && null != comioResponse.getError())? comioResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.COMIO))
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }

        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;

    }

    private ComioResponse callToComioRollback(ComioRequest comioRequest, WFJobCommDomain comioConfig) throws Exception {

        String url = Arrays.asList(comioConfig.getBaseUrl(), comioConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (ComioResponse) TransportUtils.postJsonRequest(comioRequest, url, ComioResponse.class);

    }

}
