package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaRequest;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.LavaSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.LavaSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.LavaSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author prasenjit wadmare
 * @date 02/01/2018
 */
@Service
public class LavaSerialNumberValidationManagerImpl implements LavaSerialNumberValidationManager {

    private static final Logger logger = LoggerFactory.getLogger(LavaSerialNumberValidationManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private LavaSerialNumberValidationEngine lavaSerialNumberValidationEngine;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private LavaSerialNumberValidationBuilder lavaSerialNumberValidationBuilder;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Override
    public BaseResponse validateLava(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode());

        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForLavaImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = GngUtils.getInvalidSerialOrIMEIErrorList();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;
    }

    private BaseResponse goForLavaImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        activityLog.setRefId(serialSaleConfirmationRequest.getReferenceID());
        // Set status to success only for valid IMEI number
        activityLog.setStatus(Status.FAIL.toString());

        //call to Lava validation
        Collection<Error> errors = lavaSerialNumberValidationEngine.validationForLavaImei(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());

        if (errors.isEmpty()) {

            //getting Lava config from WFJobCommDomain
            WFJobCommDomain lavaConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.LAVA_IMEI.toValue());

            if (null != lavaConfig) {

                PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(serialSaleConfirmationRequest.getReferenceID(), serialSaleConfirmationRequest.getHeader().getInstitutionId());
                if(null != postIpaRequest && null != postIpaRequest.getPostIPA()){

                    LavaRequest lavaRequest = lavaSerialNumberValidationBuilder.buildLavaImeiNumberRequest(serialSaleConfirmationRequest, lavaConfig , postIpaRequest);
                    LavaResponse lavaResponse = callToLava(lavaConfig, lavaRequest);

                    if (null != lavaResponse && null == lavaResponse.getError()) {

                        serialNumberResponse = lavaSerialNumberValidationBuilder.buildLavaImeiNumberResponse(lavaResponse);

                        serialNumberInfoBuilder.originalResponse(serialNumberResponse.getMessage());
                        serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                        // update application stage to IMEIV
                        if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                            applicationStagerepository.
                                    updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                            serialNumberInfoBuilder.customMsg(ErrorCode.VALID_IMEI);
                            activityLog.setStatus(Status.SUCCESS.toString());
                            activityLog.setCustomMsg(ErrorCode.VALID_IMEI);

                        } else {
                            serialNumberInfoBuilder.customMsg(ErrorCode.INVALID_IMEI);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ErrorCode.INVALID_IMEI);
                        }

                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                    } else {
                        //not getting response from LAVA
                        logger.warn("get error from Connector api for {}", UrlType.LAVA_IMEI.name());
                        errors = GngUtils.getThirdPartyFailureErrorList();

                        serialNumberInfoBuilder.status(Status.FAILED.name());
                        serialNumberInfoBuilder.customMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.LAVA.toFaceValue()));

                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.LAVA.toFaceValue()));

                        baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                    }

                } else {
                    //not getting postIpaRequest data
                    logger.warn("not getting postIpaRequest data for {}", UrlType.LAVA_IMEI.name());
                    errors = GngUtils.getThirdPartyFailureErrorList();

                    serialNumberInfoBuilder.status(Status.FAILED.name());
                    serialNumberInfoBuilder.customMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.LAVA.toFaceValue()));

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.LAVA.toFaceValue()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.LAVA_IMEI.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                serialNumberInfoBuilder.status(Status.FAILED.name());
                serialNumberInfoBuilder.customMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {
            //Business validation failed.
            logger.warn("Business validation failed for {}", UrlType.LAVA_IMEI.name());
            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(FieldSeparator.COMMA_SPACE);
            }
            activityLog.setCustomMsg(validationErrorMsg.toString());
            serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());

            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for LAVA IMEI number information to the database.
         */
        logger.debug("Saving IMEI number info for {}", UrlType.LAVA_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }


    private LavaResponse callToLava(WFJobCommDomain lavaConfig, LavaRequest lavaRequest) throws Exception {
        String url = Arrays.asList(lavaConfig.getBaseUrl(), lavaConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (LavaResponse) TransportUtils.postJsonRequest(lavaRequest, url, LavaResponse.class);

    }

}
