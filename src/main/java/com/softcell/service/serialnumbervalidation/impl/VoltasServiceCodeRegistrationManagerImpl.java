package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.voltas.ServiceCodeRegResponse;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasRequest;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.VoltasRequestNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.VoltasServiceCodeRegistrationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.VoltasRequestNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by prasenjit wadmare on 13/12/17.
 */
@Service
public class VoltasServiceCodeRegistrationManagerImpl implements VoltasServiceCodeRegistrationManager {

    private static final Logger logger = LoggerFactory.getLogger(VoltasServiceCodeRegistrationManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private VoltasRequestNumberValidationEngine voltasRequestNumberValidationEngine;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private VoltasRequestNumberValidationBuilder voltasRequestNumberValidationBuilder;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    @Override
    public BaseResponse validateVoltas(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        // logs for SerialNumberInfo
        //serviceCodeRegistrationInfoBuilder.dateTime(new Date());
        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder
                .institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .serviceRequestNumber(serialSaleConfirmationRequest.getServiceRequestNumber());


        baseResponse = goForVoltasServiceRequestNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);

        return baseResponse;

    }


    private BaseResponse goForVoltasServiceRequestNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        ServiceCodeRegResponse serviceCodeRegResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        if(Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()){
            activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
            activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        } else if(Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()){
            activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
            activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        }
        activityLog.setRefId(serialSaleConfirmationRequest.getReferenceID());
        // Set status to success only for valid`request number
        activityLog.setStatus(Status.FAIL.toString());


        //call to Voltas validation
        Collection<Error> errors = voltasRequestNumberValidationEngine.validationForVoltas(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());

        if (errors.isEmpty()) {

            //getting Voltas config from WFJobCommDomain
            WFJobCommDomain voltasConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.toValue());

            if (null != voltasConfig) {

                VoltasRequest voltasRequest = voltasRequestNumberValidationBuilder.buildVoltasRequest(serialSaleConfirmationRequest, voltasConfig);
                VoltasResponse voltasResponse = callToVoltas(voltasConfig, voltasRequest);

                if (null != voltasResponse && null == voltasResponse.getError()) {

                    serviceCodeRegResponse = voltasRequestNumberValidationBuilder.buildVoltasResponse(voltasResponse);

                    //build Voltas log builder
                    serialNumberInfoBuilder = voltasRequestNumberValidationBuilder.buildVoltasServiceCodeLogs(voltasResponse, serialNumberInfoBuilder);

                    serialNumberInfoBuilder.status(serviceCodeRegResponse.getStatus());
                    serialNumberInfoBuilder.customMsg(serviceCodeRegResponse.getMessage());


                    // update application stage to SRNV or IMEIV depending on product
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serviceCodeRegResponse.getStatus())) {

                        if(Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()){
                            applicationStagerepository.
                                    updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        } else if(Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()){
                            applicationStagerepository.
                                    updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        }
                        activityLog.setStatus(Status.SUCCESS.toString());
                        activityLog.setCustomMsg(ErrorCode.VALID_SERVICE_REQUEST_NUM);

                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(ErrorCode.INVALID_SERVICE_REQUEST_NUM);
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serviceCodeRegResponse);

                } else {
                    //not getting response from Voltas
                    logger.error("get error from Connector api for {}", UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    serialNumberInfoBuilder.customMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.VOLTAS.toFaceValue()));
                    serialNumberInfoBuilder.originalResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.VOLTAS.toFaceValue()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                }



            } else {
                //configuration not found.
                logger.error("Configuration not found for {}", UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                serialNumberInfoBuilder.status(Status.ERROR.name());
                serialNumberInfoBuilder.customMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                serialNumberInfoBuilder.originalResponse(ErrorCode.CONFIGURATION_NOT_FOUND);

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {
            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        /**
         * Saving logs for Voltas IMEI ServiceCodeRegistration information to the database.
         */
        logger.debug("Saving ServiceCodeRegistration info for {}", UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;

    }

    private VoltasResponse callToVoltas(WFJobCommDomain voltasConfig, VoltasRequest voltasRequest) throws Exception {
        String url = Arrays.asList(voltasConfig.getBaseUrl(), voltasConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (VoltasResponse) TransportUtils.postJsonRequest(voltasRequest, url, VoltasResponse.class);

    }

}
