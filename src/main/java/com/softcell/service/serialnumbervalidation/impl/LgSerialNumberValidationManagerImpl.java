package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.lg.LgRequest;
import com.softcell.gonogo.serialnumbervalidation.lg.LgResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.LgSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.LgSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.LgSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author prasenjit wadmare
 * @date 09 Jan 2018
 */

@Service
public class LgSerialNumberValidationManagerImpl implements LgSerialNumberValidationManager {

    private static final Logger logger = LoggerFactory.getLogger(LgSerialNumberValidationManagerImpl.class);

    private String strStatus;
    private int strStatusCode;

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private LgSerialNumberValidationBuilder lgSerialNumberValidationBuilder;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private LgSerialNumberValidationEngine lgSerialNumberValidationEngine;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;


    @Override
    public BaseResponse validateLg(
            SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {


        BaseResponse baseResponse = null;
        Collection<Error> errors;
        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();

        serialNumberInfoBuilder.refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .scheme(serialSaleConfirmationRequest.getScheme())
                .dealId(serialSaleConfirmationRequest.getReferenceID());


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .id("imeiNumber")
                        .fieldName("sImeiNumber")
                        .message(ErrorCode.ONLY_IMEI_NUMBER_REQUIRED)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            } else if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForLgImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {

            baseResponse = goForLgSerialNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);

        } else {

            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;

    }

    private BaseResponse goForLgSerialNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        LgRequest lgRequest = null;
        LgResponse lgResponse = null;
        WFJobCommDomain lgConfig = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid` number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = lgSerialNumberValidationEngine.validationForLgSerialNumber(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {
            //getting Lg config from WFJobCommDomain
            lgConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.LG_SERIAL.toValue());

            if (null != lgConfig) {


                //Fetch postIPA for creating custom scheme for Lg Serial validation.
                PostIPA postIPA = applicationRepository.getPostIPA(serialSaleConfirmationRequest.getReferenceID(),
                        serialSaleConfirmationRequest.getHeader().getInstitutionId());
                //Set scheme for apple like  "MaxTenure" x "MinTenure"
                if (null != postIPA) {
                    serialSaleConfirmationRequest.setScheme(GngUtils.getSchemeForLg(postIPA));
                    logger.info("Custom scheme for LG is {}", serialSaleConfirmationRequest.getScheme());
                    serialNumberInfoBuilder.scheme(serialSaleConfirmationRequest.getScheme());
                }



                lgRequest = lgSerialNumberValidationBuilder.buildLgSerialRequest(serialSaleConfirmationRequest);
                lgResponse = callToLg(lgRequest, lgConfig);

                stopWatch.stop();

                if (null != lgResponse && null == lgResponse.getError()) {
                    String element = lgResponse.getOriginalResponse();
                    JSONObject xmlJSONObj = XML.toJSONObject(element);
                    logger.info("LG Serial Number Original Response in Xml format:{}", element);
                    if (element.contains("STATUS") && element.contains("STATUSCODE")) {
                        strStatus = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getString("STATUS");
                        strStatusCode = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getInt("STATUSCODE");
                        if ((strStatus.equals(ResponseConstants.SERIAL_BLOCK_SUCCESS) && strStatusCode == 2) || (strStatus.equals(ResponseConstants.SERIAL_NUMBER_EXIST) && strStatusCode == 0)) {
                            //Valid case
                            //getting response from lg is 'Serial Block Successfully' and StatusCode = 2 or 'Serial Number Exists' and StatusCode = 0
                            serialNumberResponseBuilder.message(ResponseConstants.LG_VALID_SERIAL)
                                    .status(Status.VALID.name());

                            serialNumberInfoBuilder.status(Status.VALID.name())
                                    .originalResponse(element);

                            // update SRNV stage for lg.
                            applicationStagerepository.updateApplicationStage(serialSaleConfirmationRequest,
                                    GNGWorkflowConstant.SRNV.toFaceValue());

                            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                            activityLog.setStatus(Status.SUCCESS.name());

                        } else if (strStatus.equals(ResponseConstants.SERIAL_ALREADY__BLOCK) && strStatusCode == 4) {
                            //Invalid case
                            //getting response from lg is 'Serial Already Block' and StatusCode = 4
                            serialNumberResponseBuilder.message(ResponseConstants.LG_SERIAL_ALREADY_BLOCK)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.LG_SERIAL_ALREADY_BLOCK);

                        } else if (strStatus.equals(ResponseConstants.SERIAL_NUMBER_NOT_EXIST) && strStatusCode == 1) {
                            //Invalid case
                            //getting response from lg is 'Serial Number does not Exists' and StatusCode = 1
                            serialNumberResponseBuilder.message(ResponseConstants.SERIAL_NUMBER_NOT_EXIST)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.SERIAL_NUMBER_NOT_EXIST);

                        } else {
                            serialNumberResponseBuilder.message(ResponseConstants.LG_INVALID_SERIAL)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.LG_INVALID_SERIAL);
                        }
                    } else {
                        strStatus = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getString("STATUS");

                        serialNumberResponseBuilder.message(strStatus)
                                .status(Status.INVALID.name());

                        serialNumberInfoBuilder.status(Status.INVALID.name())
                                .originalResponse(element);
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(strStatus);
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponseBuilder.build());
                } else {
                    logger.warn("get error from Connector api for {}", UrlType.LG_SERIAL.name());

                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .errorCode(Error.SEVERITY.CRITICAL.name())
                            .build());

                    serialNumberInfoBuilder.customMsg(ErrorCode.EXTERNAL_SERVICE_FAILURE);

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                }

                logger.info("serial number response log for refId [{}] for lg.", serialSaleConfirmationRequest.getReferenceID());

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.LG_SERIAL.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {

            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

        }

        // save lg serial number information into the database.
        logger.debug("Saving serial number info for {}", UrlType.LG_SERIAL.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());

        eventPublisher.publishEvent(activityLog);
        return baseResponse;

    }


    private BaseResponse goForLgImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = lgSerialNumberValidationEngine.validationForLgImeiNumber(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {

            WFJobCommDomain lgConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.LG_IMEI.toValue());

            if (null != lgConfig) {

                //Fetch postIPA for creating custom scheme for Lg Serial validation.
                PostIPA postIPA = applicationRepository.getPostIPA(serialSaleConfirmationRequest.getReferenceID(),
                        serialSaleConfirmationRequest.getHeader().getInstitutionId());
                //Set scheme for apple like  "MaxTenure" x "MinTenure"
                if (null != postIPA) {
                    serialSaleConfirmationRequest.setScheme(GngUtils.getSchemeForLg(postIPA));
                    logger.info("Custom scheme for LG is {}", serialSaleConfirmationRequest.getScheme());
                    serialNumberInfoBuilder.scheme(serialSaleConfirmationRequest.getScheme());
                }


                LgRequest lgRequest = lgSerialNumberValidationBuilder.buildLgImeiRequest(serialSaleConfirmationRequest);
                LgResponse lgResponse = callToLg(lgRequest, lgConfig);

                stopWatch.stop();

                if (null != lgResponse && null == lgResponse.getError()) {
                    String element = lgResponse.getOriginalResponse();
                    JSONObject xmlJSONObj = XML.toJSONObject(element);
                    logger.info("LG IMEI Number Original Response in Xml format:{}", element);
                    if (element.contains("STATUS") && element.contains("STATUSCODE")) {
                        strStatus = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getString("STATUS");
                        strStatusCode = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getInt("STATUSCODE");
                        if ((strStatus.equals(ResponseConstants.SERIAL_BLOCK_SUCCESS)) && (strStatusCode == 2)) {
                            //Valid case
                            //getting response from lg is 'Serial Block Successfully' and StatusCode is 2
                            serialNumberResponseBuilder.message(ResponseConstants.LG_VALID_IMEI)
                                    .status(Status.VALID.name());

                            serialNumberInfoBuilder.status(Status.VALID.name())
                                    .originalResponse(element);

                            // update IMEIV stage for lg.
                            applicationStagerepository.updateApplicationStage(serialSaleConfirmationRequest,
                                    GNGWorkflowConstant.IMEIV.toFaceValue());

                            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                            activityLog.setStatus(Status.SUCCESS.name());

                        } else if ((strStatus.equals(ResponseConstants.SERIAL_ALREADY__BLOCK)) && (strStatusCode == 4)) {
                            //Invalid case
                            //getting response from lg is 'Serial Already Block' and StatusCode is 4
                            serialNumberResponseBuilder.message(ResponseConstants.LG_IMEI_ALREADY_BLOCK)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.LG_IMEI_ALREADY_BLOCK);

                        } else if ((strStatus.equals(ResponseConstants.SERIAL_NUMBER_NOT_EXIST)) && (strStatusCode == 1)) {
                            //Invalid case
                            //getting response from lg is 'Serial Number does not Exists' and StatusCode is 1
                            serialNumberResponseBuilder.message(ResponseConstants.LG_IMEI_NUMBER_NOT_EXIST)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.LG_IMEI_NUMBER_NOT_EXIST);

                        } else {
                            serialNumberResponseBuilder.message(ResponseConstants.LG_INVALID_IMEI)
                                    .status(Status.INVALID.name());

                            serialNumberInfoBuilder.status(Status.INVALID.name())
                                    .originalResponse(element);
                            activityLog.setStatus(Status.FAILED.name());
                            activityLog.setCustomMsg(ResponseConstants.LG_INVALID_IMEI);
                        }
                    } else {
                        strStatus = xmlJSONObj.getJSONObject("SerialValidate").getJSONObject("Data").getString("STATUS");

                        strStatus = GngUtils.convertLgMessageForIMEI(strStatus);

                        serialNumberResponseBuilder.message(strStatus)
                                .status(Status.INVALID.name());

                        serialNumberInfoBuilder.status(Status.INVALID.name())
                                .originalResponse(element);
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg(strStatus);
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponseBuilder.build());
                } else {

                    logger.warn("get error from Connector api for {}", UrlType.LG_IMEI.name());

                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .errorCode(Error.SEVERITY.CRITICAL.name())
                            .build());

                    serialNumberInfoBuilder.customMsg(ErrorCode.EXTERNAL_SERVICE_FAILURE);

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }

                logger.info("serial number response log for refId [{}] for lg.", serialSaleConfirmationRequest.getReferenceID());

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.LG_IMEI.name());
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {

            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

        }

        // save lg IMEI number info into the database.
        logger.debug("Saving IMEI number info for {}", UrlType.LG_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());

        eventPublisher.publishEvent(activityLog);
        return baseResponse;

    }


    private LgResponse callToLg(LgRequest lgRequest, WFJobCommDomain lgConfig) throws Exception {
        String url = Arrays.asList(lgConfig.getBaseUrl(), lgConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (LgResponse) TransportUtils.postJsonRequest(lgRequest, url, LgResponse.class);

    }

}
