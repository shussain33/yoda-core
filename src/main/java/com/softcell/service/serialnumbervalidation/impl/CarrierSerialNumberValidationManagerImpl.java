package com.softcell.service.serialnumbervalidation.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.CarrierSerialNumberValidationBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.serialnumbervalidation.CarrierSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.serialnumbervalidation.CarrierSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by ibrar on 28/12/17.
 */

@Service
public class CarrierSerialNumberValidationManagerImpl implements CarrierSerialNumberValidationManager{

    private static final Logger logger = LoggerFactory.getLogger(CarrierSerialNumberValidationManagerImpl.class);

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private CarrierSerialNumberValidationBuilder carrierSerialNumberValidationBuilder;

    @Autowired
    private CarrierSerialNumberValidationEngine carrierSerialNumberValidationEngine;

    @Override
    public BaseResponse validateCarrierSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse;
        Collection<Error> errors;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.skuCode(serialSaleConfirmationRequest.getSkuCode());
        builder.storeCode(serialSaleConfirmationRequest.getStoreCode());
        builder.scheme(serialSaleConfirmationRequest.getScheme());

        WFJobCommDomain carrierConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.CARRIER_SERIAL_NUMBER.toValue());

        if (null == carrierConfig) {
            errors = GngUtils.getConfigurationNotFoundErrorList();
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = carrierSerialNumberValidationEngine.validationForCarrier(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {

                CarrierRequest carrierRequest = carrierSerialNumberValidationBuilder.buildCarrierRequest(serialSaleConfirmationRequest, carrierConfig);

                CarrierResponse carrierResponse = callToCarrier(carrierConfig, carrierRequest);
                if(null == carrierResponse) {
                    builder.status(Status.NO_RESPONSE.name());
                    errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.CARRIER))
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.CARRIER));

                } else if (null == carrierResponse.getError()) {

                        SerialNumberResponse serialNumberResponse = carrierSerialNumberValidationBuilder.buildCarrierResponse(carrierResponse);
                        builder.status(serialNumberResponse.getStatus());
                        builder.originalResponse(serialNumberResponse.getStatus() + ":" + serialNumberResponse.getMessage());
                        serialNumberInfo = builder.build();

                        if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {

                            // update application stage to SRNV
                            applicationStagerepository
                                    .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                            activityLog.setStatus(Status.SUCCESS.toString());
                        }

                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                    } else {
                    errors = new ArrayList<>();
                    String message = carrierResponse.getError().getType() + " : " + carrierResponse.getError().getMessage();
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(message);
                }
            } else {
                builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        /**
         * Saving logs
         */
        serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private CarrierResponse callToCarrier(WFJobCommDomain carrierConfig, CarrierRequest carrierRequest) throws Exception {

        String url = Arrays.asList(carrierConfig.getBaseUrl(), carrierConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (CarrierResponse) TransportUtils.postJsonRequest(carrierRequest, url, CarrierResponse.class);

    }

    public BaseResponse goForCarrierRollback(RollbackRequest rollbackRequest, CarrierRequest carrierRequest, WFJobCommDomain carrierConfig) throws Exception{

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_ROLLBACK.toFaceValue());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());
        CarrierResponse carrierResponse = callToCarrier(carrierConfig, carrierRequest);
        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSkuCode());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != carrierResponse && null == carrierResponse.getError()) {

            SerialNumberResponse serialNumberResponse = carrierSerialNumberValidationBuilder
                    .buildCarrierSerialRollbackResponse(carrierResponse);

            rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

            /**
             * update serial-number valid to rollback,because we rollback this serial number.
             */
            externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());

            if (rollbackRequest.isResetStage()&& StringUtils.equalsIgnoreCase(Status.SUCCESS.name(),serialNumberResponse.getStatus())) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());
                /**
                 * update serial-number valid to rollback,because we rollback this serial number.
                 */
                externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());
                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {
                rollbackFeatureLogbuilder.status(Status.FAILED.name());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            activityLog.setCustomMsg((null != carrierResponse && null != carrierResponse.getError()) ? carrierResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.CARRIER));
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message((null != carrierResponse && null != carrierResponse.getError()) ? carrierResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.CARRIER))
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

}
