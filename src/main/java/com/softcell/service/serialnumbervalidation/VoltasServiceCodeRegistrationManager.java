package com.softcell.service.serialnumbervalidation;


import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by prasenjit wadmare on 13/12/17.
 */

public interface VoltasServiceCodeRegistrationManager {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateVoltas(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


}
