package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * @author prasenjit wadmare
 * @date 09 Jan 2018
 */

public interface LgSerialNumberValidationManager {

    /**
     * @param serialSaleConfirmationRequest
     * @return return true response if serial number is valid otherwise return
     * success response(for Lg)
     */
    BaseResponse validateLg(
            SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


}
