package com.softcell.service.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by prasenjit wadmare on 19/11/17.
 */

public interface YuhoSerialNumberValidationManager {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateYuho(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


}
