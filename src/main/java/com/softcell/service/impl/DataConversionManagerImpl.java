package com.softcell.service.impl;

import com.google.gson.Gson;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Institute;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.core.FieldsMapping;
import com.softcell.gonogo.model.core.ThirdPartyRequest;
import com.softcell.gonogo.model.core.excelConfiguration.ExcelConfiguration;
import com.softcell.gonogo.model.core.excelConfiguration.ExcelResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.FileStagingUtils;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.service.DataConversionManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by ssg0268 on 2/7/19.
 */
@Service
public class DataConversionManagerImpl implements DataConversionManager {

    private static Logger logger = LoggerFactory.getLogger(DataConversionManagerImpl.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AppConfigurationHelper appConfigurationHelper;

    private GngUtils gngUtils;

    @Override
    public BaseResponse downloadExcel(ThirdPartyRequest thirdPartyRequest) {
        BaseResponse baseResponse = null;
        ExcelResponse excelResponse = new ExcelResponse();
        File csvFile = null;
        String type = thirdPartyRequest.getType();
        List<String> refId = thirdPartyRequest.getRefIdList();
        if(CollectionUtils.isEmpty(refId) || thirdPartyRequest.getHeader().getInstitutionId() == null){
            return GngUtils.getBaseResponse(HttpStatus.OK, "RefIds or institutionId can not be null for download excel in system");
        }
        List<Object> objects = null;
        List<String> valueList = null;
        List<Map<String, Map<String, String>>> tempList = new ArrayList<>();
        Map<String, List<String>> info = null;
        ExcelConfiguration excelConfiguration = fetchExcelConfig(thirdPartyRequest);
        if(excelConfiguration != null) {
            List<FieldsMapping> fieldsMappings = excelConfiguration.getMappings();
            if(CollectionUtils.isNotEmpty(fieldsMappings)){
                for (FieldsMapping fields : fieldsMappings) {
                    Map<String, Map<String, String>> tempMap = new HashMap<>();
                    String name = fields.getClassName();
                    Map<String, String> fieldMapping = fields.getFields();
                    tempMap.put(name, fieldMapping);
                    tempList.add(tempMap);
                }
            }
        }

        try {
            /*Data is Extract from databse from Given collection Name  and mapped value as per given value in Excel Configuration*/
            info = formattingMap(tempList, valueList, refId, type, thirdPartyRequest.getHeader().getInstitutionId());
            if(info != null) {
                //Conver Map into json structure
                objects = convertFlatStructure(info);
                logger.info("{}",info);
                if (CollectionUtils.isNotEmpty(objects)) {
                    Gson gson = new Gson();
                    String json = "{\"inFile\":" + gson.toJson(objects) + "}";

                   /* Required during local testing
                    csvFile = new File("/home/ssg0268/Videos/test.csv");*/
                   csvFile = new File(FileStagingUtils.getFileUploadDirectory() + FieldSeparator.FORWARD_SLASH + UUID.randomUUID().toString().substring(0, 5));
                    if (gngUtils == null) {
                        gngUtils = new GngUtils();
                    }
                    if(MapUtils.isNotEmpty(info)){
                        gngUtils.mapToCsv(info, csvFile);
                    }
                  //  gngUtils.jsonToCsv(json, csvFile);
                    byte[] fileContent = Files.readAllBytes(csvFile.toPath());

                    //xls();
                    excelResponse.setFileContent(fileContent);
                    excelResponse.setFormat(excelConfiguration.getFormat());
                    excelResponse.setExcelName(excelConfiguration.getType());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, excelResponse);

                }
            }else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Error found in parsing Data");
            }
        }catch (Exception ex){
            ex.getMessage();
            logger.error("Error While Download Excel {}", ex.getStackTrace());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getStackTrace());
        }
        finally {
            if(csvFile != null) {
                FileUtils.deleteQuietly(csvFile);    // deletes the zip file
                // FileUtils.deleteQuietly(extractFolder);   // deletes the created temp directory
            }
        }
        return baseResponse;
    }

    private List<Object> convertFlatStructure(Map<String, List<String>> info) {
        List<Object> objects = null;
        Map<String, String> oneToOneMap = new LinkedHashMap<>();
        boolean parse = false;
        int maxCount = 0;
        String key = null;
        for (Map.Entry<String, List<String>> temp : info.entrySet()){
            // Map<String , String > oneObj = new HashMap<>();
            if(temp.getValue().size() > 1){
                parse = true;
                break;
            }
        }

        for (Map.Entry<String, List<String>> temp : info.entrySet()){
            // Map<String , String > oneObj = new HashMap<>();
            if(temp.getValue().size() > 1){
                if(temp.getValue().size() > maxCount){
                    maxCount = temp.getValue().size();
                    key = temp.getKey();
                }
            }
        }

        if(parse){
            objects  = parseJson(key, maxCount, info);
        }else {
            objects = new ArrayList<>();
            for (Map.Entry<String, List<String>> temp : info.entrySet()){
                oneToOneMap.put(temp.getKey(), temp.getValue().get(0));
            }
            objects.add(oneToOneMap);
        }
        return objects;

    }

    public List<Object> parseJson(String key, int size, Map<String , List<String>> map){
        List<Object> objectList = new ArrayList<>();
        for(int i=0 ; i<size ; i++) {
            Map<String, String> convertedMap = new LinkedHashMap<>();
            for (Map.Entry<String, List<String>> my : map.entrySet()) {
                if(my.getValue().size() > 1) {
                    if (my.getKey().equals(key)) {
                        convertedMap.put(my.getKey(), my.getValue().get(i));
                    }else {
                        if(my.getValue().size() > i) {
                            convertedMap.put(my.getKey(), my.getValue().get(i));
                        }
                    }
                }else {
                    convertedMap.put(my.getKey(), my.getValue().get(0));
                }
            }
            objectList.add(convertedMap);
        }
        return objectList;
    }

    private Map<String, List<String>> formattingMap(List<Map<String, Map<String, String>>> tempList, List<String> valueList, List<String> refId, String type, String institutionId) {
        Map<String, List<String>> info = null;
        try {
            if (CollectionUtils.isNotEmpty(tempList)) {
                for (Map<String, Map<String, String>> value : tempList) {
                    for (Map.Entry<String, Map<String, String>> entry : value.entrySet()) {
                        String key = entry.getKey();
                        Class tClass = (Class)Class.forName(key);
                        Query query = new Query();
                        query.addCriteria(Criteria.where("_id").in(refId));
                        List<Object> object = mongoTemplate.find(query, tClass);
                        if(CollectionUtils.isNotEmpty(object)) {
                            for (Object obj : object) {
                                if (entry.getValue() != null) {
                                    for (Map.Entry<String, String> items : entry.getValue().entrySet()) {
                                        String values = appConfigurationHelper.getRequestStringValue(obj, items.getValue(), Class.forName(key));
                                        if(values != null){
                                            String[] temp1 = values.split("\\^");
                                            // if more than one value find then spilit it
                                            valueList = Arrays.asList(temp1);
                                            List<String> val = new ArrayList<>();
                                            val.addAll(valueList);
                                            if(CollectionUtils.isNotEmpty(valueList)){
                                                if(info == null){
                                                    info = new LinkedHashMap<>();
                                                }
                                                // List<String> tempLists = info.get(items.getKey());
                                                if(info.get(items.getKey()) != null){
                                                    val.addAll(info.get(items.getKey()));
                                                    info.put(items.getKey(), val);
                                                }else {
                                                    info.put(items.getKey(), val);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            return info;
        }catch (Exception ex){
            ex.getMessage();
            logger.error("Error While Converting into Map {}", ex.getStackTrace());
            return null;
        }

    }

    private ExcelConfiguration fetchExcelConfig(ThirdPartyRequest request) {
        logger.debug("Inside fetchExcelConfig()");
        String institutionId = request.getHeader().getInstitutionId();
        try {
            Query query = new Query();
            Criteria criteria = Criteria.where("type").is(request.getType()).and("institutionId").is(institutionId);
            query.addCriteria(criteria);
            return mongoTemplate.findOne(query, ExcelConfiguration.class);
        }catch (Exception ex){
            logger.debug("Error while fetching excelConfig {}", ex.getMessage());
            return null;
        }

    }
}
