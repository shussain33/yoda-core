package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by archana on 24/1/19.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "responseCode",
        "BREDecision",
        "responseRemarks",
        "modalPremium",
        "BREAction",
        "baseCounterOffer",
        "lifeOption",
        "transID",
        "annualPremiumWithTax",
        "ciCounterOffer",
        "URL",
        "adbrCounterOffer",
        "isMedical"
})
public class IciciResponse {
    /*
    * {
  "responseCode": "E00",
  "BREDecision": "ST",
  "responseRemarks": "LC.Easy Home Finance",
  "modalPremium": "3135",
  "BREAction": "Accept",
  "baseCounterOffer": "0",
  "lifeOption": "",
  "transID": "OS06163306",
  "annualPremiumWithTax": "null",
  "ciCounterOffer": "0",
  "URL": "https://www.iprusalesbeta.com/digiuatnew/apptrackerhomeNew.htm?appNumber=R21VeURmTGY3RHdqZm5JSld6Q3U4dz09&dob=bzdueisxWFIzY2thR00reFk3QzNUUT09&source=1",
  "adbrCounterOffer": "0",
  "isMedical": "false"
}*/
    @JsonProperty("responseCode")
    public String responseCode;
    @JsonProperty("BREDecision")
    public String bREDecision;
    @JsonProperty("responseRemarks")
    public String responseRemarks;
    @JsonProperty("modalPremium")
    public String modalPremium;
    @JsonProperty("BREAction")
    public String bREAction;
    @JsonProperty("baseCounterOffer")
    public String baseCounterOffer;
    @JsonProperty("lifeOption")
    public String lifeOption;

    // This is loan application number
    @JsonProperty("transID")
    public String transID;

    @JsonProperty("annualPremiumWithTax")
    public String annualPremiumWithTax;
    @JsonProperty("ciCounterOffer")
    public String ciCounterOffer;
    @JsonProperty("URL")
    public String uRL;
    @JsonProperty("adbrCounterOffer")
    public String adbrCounterOffer;
    @JsonProperty("isMedical")
    public String isMedical;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}