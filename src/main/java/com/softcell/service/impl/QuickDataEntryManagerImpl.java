package com.softcell.service.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.search.MasterMongoRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.qde.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.factory.CustomerDataBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.QuickDataEntryManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.DeviceTraceUtility;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.PartialWorkFlow;
import com.softcell.workflow.RequestAggregator;
import com.softcell.workflow.SequenceGenerator;
import com.softcell.workflow.WorkFlowManager;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by archana on 27/7/17.
 */
@Service
public class QuickDataEntryManagerImpl implements QuickDataEntryManager {

    private static  Logger logger = LoggerFactory.getLogger(QuickDataEntryManagerImpl.class);
    private static final String STEP_1 = "step1";
    private static final String STEP_2 = "step2";
    private static final String STEP_UPDATE = "update";
    private static final String STEP_PARTIAL_SAVE = "partial-save";
    private static final String WS_ERROR = "Failed to get customer's data in response.";
    private static final String NO_RECORDS_MESSAGE = "No record found";

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private WorkFlowManager workFlowManager;

    @Autowired
    private PartialWorkFlow  partialWorkFlow;

    @Autowired
    private CustomerDataBuilder customerDataBuilder;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private RequestAggregator requestAggregator;

    private CustomerDataServiceCallLog customerDataServiceCallLog;

    private ActivityLogs activityLog;

    private Acknowledgement acknowledgement;

    @Override
    public BaseResponse submitQuickData(ApplicationRequest applicationRequest,
                                        String step, HttpServletRequest httpRequest) throws Exception {
        // Check customer and step id
        BaseResponse response = null;
        applicationRequest.getHeader().setDateTime(new Date());

        try {
            switch (step) {
                case STEP_1: {
                    // check Customer type
                    if (applicationRequest.getApplicantType().equals(ApplicantType.EXPRESS.value())) {
                        StopWatch stopWatch = new StopWatch();
                        stopWatch.start();

                        activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());
                        activityLog.setAction(GNGWorkflowConstant.SAVE_QDE_EXPRESS_DATA.toFaceValue());
                        activityLog.setStage(GNGWorkflowConstant.QDE.toFaceValue());

                        // Save Data in the system
                        response = GngUtils.getBaseResponse(HttpStatus.OK, saveCustomerData(applicationRequest, httpRequest));

                        activityLog.setRefId(applicationRequest.getRefID());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        // Save activity log
                        stopWatch.stop();
                        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                        this.activityEventPublisher.publishEvent(activityLog);

                        checkPinCodeforgeoLimit(applicationRequest);
                        // run BRE
                        executeBRE(applicationRequest, httpRequest);

                        logger.debug("Application request value at STEP {} is {}",STEP_1,applicationRequest.getCurrentStageId());
                    } else if (applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value())
                            || applicationRequest.getApplicantType().equals(ApplicantType.SOLICITED.value())) {

                        // Validate date format and any other fields required
                        validateRequest(applicationRequest);
                        // Activity Log
                        activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());
                        activityLog.setAction(GNGWorkflowConstant.EXTERNAL_API_CALL.toFaceValue());

                        // Set fields of customerDataServiceCallLog
                        buildRequestLog(applicationRequest);
                        // Get data from webservice
                        response =  getCustomerData(applicationRequest);

                        logger.debug("Application request value at STEP {} is {}",STEP_1,applicationRequest.getCurrentStageId());
                        // save Log
                        externalAPILogRepository.saveCustomerDataServiceCallLog(customerDataServiceCallLog);
                    } else {
                        // Normal customer  !! shouldnt come here
                        throw new Exception("Full Data Entry required for Normal customer");
                    }
                    break;
                }
                case STEP_UPDATE : {
                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());
                    if( applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value()) ){
                        activityLog.setAction(GNGWorkflowConstant.SAVE_QDE_EXISTING_DATA.toFaceValue());
                    } else {
                        activityLog.setAction(GNGWorkflowConstant.SAVE_QDE_SOLICITED_DATA.toFaceValue());
                    }
                    activityLog.setStage(GNGWorkflowConstant.QDE.toFaceValue());

                    // Save Data in the system
                    response = GngUtils.getBaseResponse(HttpStatus.OK, saveCustomerData(applicationRequest, httpRequest));

                    activityLog.setRefId(applicationRequest.getRefID());
                    activityLog.setStatus(Status.SUCCESS.toString());
                    // Save activity log
                    stopWatch.stop();
                    activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                    this.activityEventPublisher.publishEvent(activityLog);

                    // run BRE
                    executeBRE(applicationRequest, httpRequest);

                    logger.debug("Application request value at STEP {} is {}",STEP_UPDATE,applicationRequest.getCurrentStageId());

                    break;
                }
                case STEP_2: {
                    // run SoBRE
                    logger.debug("Application request value at STEP {} is {} before SoBRE",STEP_2 ,applicationRequest.getCurrentStageId());

                    response = executeSoBRE(applicationRequest);

                    logger.debug("Application request value at STEP {} is {} after SoBRE",STEP_2 ,applicationRequest.getCurrentStageId());

                    break;
                }
                case STEP_PARTIAL_SAVE : {

                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();

                    boolean applicantTypeFlag = true;

                    activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());
                    if (applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value())) {
                        activityLog.setAction(GNGWorkflowConstant.PARTIAL_SAVE_QDE_EXISTING_DATA.toFaceValue());
                    } else if (applicationRequest.getApplicantType().equals(ApplicantType.SOLICITED.value())) {
                        activityLog.setAction(GNGWorkflowConstant.PARTIAL_SAVE_QDE_SOLICITED_DATA.toFaceValue());
                    } else if (applicationRequest.getApplicantType().equals(ApplicantType.EXPRESS.value())) {
                        activityLog.setAction(GNGWorkflowConstant.PARTIAL_SAVE_QDE_EXPRESS_DATA.toFaceValue());
                    }else{
                        applicantTypeFlag =false;
                        acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Status.ERROR.name(), ErrorCode.INVALID_REQUEST_CODE,
                                ErrorCode.INVALID_APPLICANT_TYPE, null, null);

                        activityLog.setStatus(Status.ERROR.name());
                        activityLog.setCustomMsg(ErrorCode.INVALID_APPLICANT_TYPE);
                    }

                    activityLog.setRefId(applicationRequest.getRefID());

                    //check applicant type here
                    if (applicantTypeFlag) {

                        // set necessary parameters in applicationRequest i.e currentStageId ,qdeDecision ,dealerRank
                        setNecessaryParameters(applicationRequest);

                        //upsert application request in applicationRequest collection
                        if (applicationRepository.upsertApplication(applicationRequest)) {

                            //update application request in the gonogoCustomerApplication
                            if (applicationRepository.updateGoNoGoCustomerApplication(applicationRequest)) {
                                acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Status.SUCCESS.name(), null, null,
                                        null, null);
                                activityLog.setStatus(Status.SUCCESS.name());
                                activityLog.setCustomMsg(ErrorCode.DATA_SAVE_MESSAGE);
                            } else {
                                getInvalidRequestAcknowledgement(applicationRequest ,ErrorCode.INVALID_REQUEST_DSCR);
                            }
                        } else {
                            getInvalidRequestAcknowledgement(applicationRequest ,ErrorCode.INVALID_REQUEST_DSCR);

                        }
                    }

                    response = GngUtils.getBaseResponse(HttpStatus.OK, acknowledgement);

                    // Save activity log
                    stopWatch.stop();
                    activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                    this.activityEventPublisher.publishEvent(activityLog);

                    break;
                }

            }
        } catch (GoNoGoException ge) {
            response = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, ge.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            response = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return response;
    }


    private void checkPinCodeforgeoLimit(ApplicationRequest applicationRequest) {

        if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAddress())){
           List<CustomerAddress> addressList= applicationRequest.getRequest().getApplicant().getAddress();
           addressList.forEach(customerAddress -> {
               if (StringUtils.equals(customerAddress.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())) {

                   // get branchId from dealerBranchMaster against the dealerId
                   String branchId = applicationRepository.getBranchIdAgainstDealerId(applicationRequest.getHeader().getInstitutionId(), applicationRequest.getHeader().getDealerId());

                   logger.debug("Branch Id {} found against dealerId {}", branchId, applicationRequest.getHeader().getDealerId());

                   // check pincode is present in negativeAreaGeoLimitMasters
                   if (applicationRepository.checkGeoLimitIsApplicableForBranch(applicationRequest.getHeader().getInstitutionId(), branchId, customerAddress.getPin())) {
                       customerAddress.setOutOfGeoLimit("NO");
                   } else {
                       customerAddress.setOutOfGeoLimit("YES");
                   }
               }
           });
            applicationRequest.getRequest().getApplicant().setAddress(addressList);
        }else{
            logger.debug("Address is not present against refId is {}",STEP_UPDATE,applicationRequest.getRefID());

        }
    }

    private void setNecessaryParameters(ApplicationRequest applicationRequest) {

        applicationRequest.setCurrentStageId(applicationRepository.getApplicationStage(applicationRequest.getHeader().getInstitutionId(), applicationRequest.getRefID()));
        //set activity log stage
        activityLog.setStage(applicationRequest.getCurrentStageId());
        applicationRequest.setQdeDecision(true);
        applicationRequest.setDealerRank(getDealerRank(applicationRequest.getHeader()));

    }

    private double getDealerRank(Header header) {

        MasterRepository masterRepository = new MasterMongoRepository(
                MongoConfig.getMongoTemplate());
        DealerEmailMaster dealerEmailMaster = masterRepository
                .getDealerRanking(header
                        .getDealerId(), header
                        .getInstitutionId());

        if (null != dealerEmailMaster) {
            return dealerEmailMaster.getDealerComm();
        } else {
            return 0;
        }
    }

    private void getInvalidRequestAcknowledgement(ApplicationRequest applicationRequest ,String errorMessage) {
        acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Status.ERROR.name(), ErrorCode.INVALID_REQUEST_CODE,
                errorMessage, null, null);

        activityLog.setStatus(Status.ERROR.name());
        activityLog.setCustomMsg(errorMessage);

    }



    private void validateRequest(ApplicationRequest applicationRequest) throws GoNoGoException {
        // Date of birth
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();
        if( dob != null) {
            Date parsed = GngDateUtil.getDateFromSpecificFormat(GngDateUtil.ddMMyyyy, dob);
            if (parsed == null ){
                throw new GoNoGoException("Data in incorrect format");
            }
        }
    }

    private void buildRequestLog(ApplicationRequest applicationRequest) {
        customerDataServiceCallLog = CustomerDataServiceCallLog.builder()
                .applicantType(applicationRequest.getApplicantType())
                .build();
        if (applicationRequest.getApplicantType().equals(ApplicantType.SOLICITED.value()) ){
            // Coupon no.
            customerDataServiceCallLog.setCouponCode(applicationRequest.getCouponCode());
        } else if (applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value()) ){
            // Date of birth
            String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();
            if (dob != null ){
                customerDataServiceCallLog.setDateOfBirth(dob);
            }
            // phone number
            List<Phone> phoneList = applicationRequest.getRequest().getApplicant().getPhone();
            if( phoneList != null && !phoneList.isEmpty() ) {
              customerDataServiceCallLog.setMobileNumber(phoneList.get(0).getPhoneNumber());
            }
            // KYC Document Number
            List<Kyc> kycList = applicationRequest.getRequest().getApplicant().getKyc();
            if( kycList != null && ! kycList.isEmpty() ) {
                Kyc kycDoc = (Kyc)kycList.get(0);
                customerDataServiceCallLog.setKycType(kycDoc.getKycName());
                customerDataServiceCallLog.setKycNumber(kycDoc.getKycNumber());
            }
        }
    }
    private void buildResponseLog(CustomerDataResponse customerDataResponse) {
        if( customerDataResponse != null ) {
            customerDataServiceCallLog.setResponseCode(customerDataResponse.getResponseCode());
            customerDataServiceCallLog.setSuccess(customerDataResponse.getSuccess());
            customerDataServiceCallLog.setDescription(customerDataResponse.getDescription());
        }
    }
    private BaseResponse executeSoBRE(ApplicationRequest applicationRequest) throws Exception {
        applicationRequest.setQdeDecision(false);
        applicationRequest.setCurrentStageId(GNGWorkflowConstant.BRE.toFaceValue());
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();
        // Set age
        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }
        MasterRepository masterRepository = new MasterMongoRepository(
                MongoConfig.getMongoTemplate());
        DealerEmailMaster dealerEmailMaster = masterRepository
                .getDealerRanking(applicationRequest.getHeader()
                        .getDealerId(), applicationRequest.getHeader()
                        .getInstitutionId());

        if (null != dealerEmailMaster) {
            applicationRequest.setDealerRank(dealerEmailMaster.getDealerComm());
        }

        applicationRepository.upsertApplication(applicationRequest);
        return GngUtils.getBaseResponse(HttpStatus.OK, workFlowManager.executeQdeComponents(applicationRequest));
    }

    private void executeBRE(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {
        applicationRequest.getHeader().setDateTime(new Date());
        // IMEI and client details put into DeviceInfo domain under Header.
        DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
        deviceTraceUtility.populateDeviceInfo(applicationRequest.getHeader(), httpRequest);
        // execute BRE
        applicationRequest.setQdeDecision(true);
        applicationRequest.setCurrentStageId(GNGWorkflowConstant.BRE.toFaceValue());
        partialWorkFlow.executeBRE(applicationRequest);

    }

    private Object saveCustomerData(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception{
        /*  Generate ref id
            Generate Application Id
            Set dealer rank
            Set stageto BRE
            save applicationRequest
        */
        // Ref Id
        Object object = requestAggregator.generateReferenceId(applicationRequest);
        if (object instanceof Acknowledgement) {
            return (Acknowledgement) object;
        }
        String refID = (String) object;
        applicationRequest.setRefID(refID);
        // Appliocation Id
        applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());
        // Set dealer rank

        applicationRequest.setDealerRank(getDealerRank(applicationRequest.getHeader()));

        // Set stage to QDE
        applicationRequest.setCurrentStageId(GNGWorkflowConstant.QDE.toFaceValue());
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();
        // Set age
        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }
        // Save application

        //check once for applicatioRequest...
        if (applicationRepository.saveApplication(applicationRequest) != null) {
            // Save GonogoApplcn
            applicationRepository.saveGoNoGoCustomerApplication(RequestAggregator
                    .getGoNoGo(applicationRequest));
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                    null, null);
        } else {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }

    }

    private BaseResponse getCustomerData(ApplicationRequest applicationRequest) throws Exception {
        BaseResponse baseResponse = null;
        CustomerDataResponse customerDataResponse = CustomerDataResponse.builder().build();
        boolean error = false;
        String errorDescription = null;
        HttpStatus httpStatus = HttpStatus.OK;
        Collection<Error> errors;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            if (applicationRequest.getApplicantType().equalsIgnoreCase(ApplicantType.SOLICITED.value())) {
                SolicitedCustomerDataResponse solicitedCustomerDataResponse = getSolicitedCustomerData(applicationRequest);

                if (solicitedCustomerDataResponse != null ) {
                    BeanUtils.copyProperties(customerDataResponse , solicitedCustomerDataResponse);
                    customerDataResponse.setResPincode(solicitedCustomerDataResponse.getPincode());

                   if (Boolean.valueOf(solicitedCustomerDataResponse.getSuccess())) {

                        // Check for "Data not found" and set status HttpStatus.CustomHttpStatus.THIRD_PARTY_NO_CONTENT
                        if( StringUtils.endsWithIgnoreCase( NO_RECORDS_MESSAGE, solicitedCustomerDataResponse.getDescription())){

                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_NO_CONTENT,GngUtils.getNoContentErrorList());
                        } else {
                            if(StringUtils.equals(customerDataResponse.getDealerId(),applicationRequest.getHeader().getDealerId())){
                                baseResponse = GngUtils.getBaseResponse(httpStatus, customerDataResponse);
                            }else{
                                errors = new ArrayList<>();
                                errors.add(Error.builder()
                                        .message(ErrorCode.DEALER_ID_NOT_MATCH)
                                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                                        .level(Error.SEVERITY.MEDIUM.toString())
                                        .build());
                               baseResponse= GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, errors);
                            }
                        }
                    } else {
                        error = true;
                        errorDescription = solicitedCustomerDataResponse.getDescription();
                    }
                    // Update response log
                    buildResponseLog(customerDataResponse);
                    activityLog.setStatus(Status.SUCCESS.toString());
                }
            } else if (applicationRequest.getApplicantType().equalsIgnoreCase(ApplicantType.EXISTING.value())) {
                ExistingCustomerDataResponse existingCustomerDataResponse = getExistingCustomerData(applicationRequest);

                if (existingCustomerDataResponse != null ) {
                    BeanUtils.copyProperties(customerDataResponse , existingCustomerDataResponse);
                    customerDataResponse.setPhoneNumber(existingCustomerDataResponse.getMobileNumber());
                    if (Boolean.valueOf(existingCustomerDataResponse.getSuccess())) {
                        // Check for "Data not found" and set status HttpStatus.CustomHttpStatus.THIRD_PARTY_NO_CONTENT
                        if( StringUtils.endsWithIgnoreCase(NO_RECORDS_MESSAGE, existingCustomerDataResponse.getDescription())){
                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_NO_CONTENT,GngUtils.getNoContentErrorList());
                        }else {
                            baseResponse = GngUtils.getBaseResponse(httpStatus, customerDataResponse);
                        }
                    } else {
                        error = true;
                        errorDescription = existingCustomerDataResponse.getDescription();
                    }
                    // Update response log
                    buildResponseLog(customerDataResponse);
                    activityLog.setStatus(Status.SUCCESS.toString());
                }
            }

            if( error ){
                baseResponse = buildErrorResponse(errorDescription,
                                Integer.toString(Error.ERROR_TYPE.BUSINESS.toCode()),
                                Integer.toString(Error.ERROR_TYPE.BUSINESS.toCode()), Error.SEVERITY.HIGH.name(),
                                CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR);
                activityLog.setCustomMsg( new StringBuilder().append(errorDescription)
                        .append(" | " ). append(Error.ERROR_TYPE.BUSINESS.toCode())
                        .append(" | " ). append(Error.SEVERITY.HIGH.name())
                        .append(" | " ). append(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR).toString()
                );

                activityLog.setStatus(Status.FAIL.toString());
            }
            if( customerDataResponse == null ){
                baseResponse = buildErrorResponse(WS_ERROR,
                        Integer.toString(Error.ERROR_TYPE.SYSTEM.toCode()),
                        Integer.toString(Error.ERROR_TYPE.SYSTEM.toCode()), Error.SEVERITY.HIGH.name(),
                        CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR);
                activityLog.setCustomMsg( new StringBuilder().append(WS_ERROR)
                        .append(" | " ). append(Error.ERROR_TYPE.SYSTEM.toCode())
                        .append(" | " ). append(Error.SEVERITY.HIGH.name())
                        .append(" | " ). append(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR).toString()
                );

                activityLog.setStatus(Status.FAIL.toString());
            }
        } catch (GoNoGoException e){
            baseResponse = buildErrorResponse( ErrorCode.CONFIGURATION_NOT_FOUND, String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()),
                    Error.ERROR_TYPE.SYSTEM.toValue(), Error.SEVERITY.CRITICAL.name() ,
                    CustomHttpStatus.CONFIGURATION_NOT_FOUND );
            activityLog.setCustomMsg(new StringBuilder().append(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .append(" | " ). append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .append(" | " ). append(Error.ERROR_TYPE.SYSTEM.toValue())
                    .append(" | " ). append(Error.SEVERITY.CRITICAL.name() )
                    .append(" | " ). append(CustomHttpStatus.CONFIGURATION_NOT_FOUND).toString());

            activityLog.setStatus(Status.FAIL.toString());
        }

        // Save activity log
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        this.activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    private BaseResponse buildErrorResponse( String message, String errorCode, String  errorType,
                                     String level, CustomHttpStatus errorStatus) {

        return GngUtils.getBaseResponse(errorStatus,  buildError( message,  errorCode,  errorType, level));
    }

    private ArrayList<Error>  buildError(String message, String errorCode, String errorType, String level) {
        ArrayList<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(message)
                .errorCode(errorCode)
                .errorType(errorType)
                .level(level)
                .build());
        return errors;
    }

    private ExistingCustomerDataResponse getExistingCustomerData(ApplicationRequest applicationRequest) throws Exception {
        String urlType = UrlType.HDB_EXISTING_QUICK_DATA.toValue();
        WFJobCommDomain customerDataPullConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);
        //Build request for the connector call
        ExistingCustomerServiceRequest existingServiceRequest
                = customerDataBuilder.buildExistingServiceRequest(applicationRequest);

        String url = Arrays.asList(customerDataPullConfig.getBaseUrl(), customerDataPullConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        //Make the API call
        ExistingCustomerDataResponse existingCustomerDataResponse = (ExistingCustomerDataResponse) TransportUtils.postJsonRequest(existingServiceRequest,
                url, ExistingCustomerDataResponse.class);

        if( existingCustomerDataResponse != null && existingCustomerDataResponse.getDob() != null ) {
            existingCustomerDataResponse.setDob(GngDateUtil.changeDateFormat(
                    GngDateUtil.getDateFromSpecificFormat(GngDateUtil.yyyy_MM_dd,
                            existingCustomerDataResponse.getDob()),
                    GngDateUtil.ddMMyyyy));
        }
        return existingCustomerDataResponse;
    }

    private SolicitedCustomerDataResponse getSolicitedCustomerData(ApplicationRequest applicationRequest) throws Exception {
        String urlType = UrlType.HDB_SOLICITED_QUICK_DATA.toValue();
        WFJobCommDomain customerDataPullConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);
        //Build request for the connector call
        SolicitedCustomerServiceRequest solicitedServiceRequest
                = customerDataBuilder.buildSolicitedServiceRequest(applicationRequest);

        String url = Arrays.asList(customerDataPullConfig.getBaseUrl(), customerDataPullConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        //Make the API call
        SolicitedCustomerDataResponse solicitedCustomerDataResponse = (SolicitedCustomerDataResponse) TransportUtils.postJsonRequest(solicitedServiceRequest,
                url, SolicitedCustomerDataResponse.class);

        if( solicitedCustomerDataResponse != null && solicitedCustomerDataResponse.getDob() != null) {
            solicitedCustomerDataResponse.setDob(GngDateUtil.changeDateFormat(
                    GngDateUtil.getDateFromSpecificFormat(GngDateUtil.yyyyMMdd_PATTERN,
                            solicitedCustomerDataResponse.getDob()),
                    GngDateUtil.ddMMyyyy));
        }
        return solicitedCustomerDataResponse;
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId, String urlType) throws GoNoGoException{
        WFJobCommDomain customerDataPullConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                institutionId, urlType);

        if (null == customerDataPullConfig) {
            // TODO log
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType) );
        }
        return customerDataPullConfig;
    }
}
