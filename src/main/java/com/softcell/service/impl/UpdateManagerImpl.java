package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.MasterDataRepository;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.BankMaster;
import com.softcell.gonogo.model.masters.EmployerMaster;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.UpadateResponse;
import com.softcell.gonogo.model.request.core.UpdateDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.controllers.DataEntryController;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.UpdateManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg0268 on 4/9/19.
 */
@Service
public class UpdateManagerImpl implements UpdateManager {

    private static final Logger logger = LoggerFactory.getLogger(DataEntryController.class);
    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Override
    public BaseResponse updateData(UpdateDataRequest updateDataRequest, String stepId, HttpServletRequest httpRequest) {
        String msg = null;
        BaseResponse baseResponse = null;
        UpadateResponse updateResponse = new UpadateResponse();
        List<ApplicationRequest> applicationRequestList = new ArrayList<>();
        logger.debug("save() service called for step {}", stepId);
        try{
            switch (stepId){
                case EndPointReferrer.BRANCH_UPDATE :
                    activityLogs(updateDataRequest,EndPointReferrer.BRANCH_UPDATE,httpRequest, stepId);
                    updateBranchData(updateDataRequest);
                    break;
                case EndPointReferrer.UPDATE_DISBURSAL_DATA:
                    activityLogs(updateDataRequest,EndPointReferrer.UPDATE_DISBURSAL_DATA,httpRequest, stepId);
                    updateDisbursalData(updateDataRequest, applicationRequestList );
                    break;
                case EndPointReferrer.ENCRYPT_PASSWORD:
                    activityLogs(updateDataRequest,EndPointReferrer.ENCRYPT_PASSWORD,httpRequest, stepId);
                    String encrypted = GngUtils.encodeToSHA1(updateDataRequest.getDataToUpdate().getPassword());
                    updateResponse.setEncyptedpassword(encrypted);
                    break;
                case EndPointReferrer.ALLOCATIONDATA :
                    activityLogs(updateDataRequest,EndPointReferrer.ALLOCATIONDATA,httpRequest, stepId);
                    logger.debug(EndPointReferrer.ALLOCATIONDATA);
                    boolean status = applicationRepository.updateApplicationForAllocationInfo(updateDataRequest.getRefId(),
                            null,updateDataRequest.getHeader().getInstitutionId() );
                    break;
                case EndPointReferrer.IFSC_CODE :
                    logger.debug(EndPointReferrer.IFSC_CODE);
                    activityLogs(updateDataRequest,EndPointReferrer.IFSC_CODE,httpRequest, stepId);
                    updateIfscCode(updateDataRequest,updateResponse);
                    break;
                case EndPointReferrer.DELETE_IFSC :
                    logger.debug(EndPointReferrer.DELETE_IFSC);
                    activityLogs(updateDataRequest,EndPointReferrer.DELETE_IFSC,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getBankDetailsMaster() != null){
                        masterDataRepository.deleteBankingIfscMaster(updateDataRequest.getDataToUpdate().getBankDetailsMaster().getIfscCode(),
                                updateDataRequest.getHeader().getInstitutionId());
                    }
                    updateResponse.setMessage("deleted");
                    break;
                case EndPointReferrer.PINCODE_UPADATE_CODE :
                    logger.debug(EndPointReferrer.PINCODE_UPADATE_CODE);
                    activityLogs(updateDataRequest,EndPointReferrer.PINCODE_UPADATE_CODE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getPinCodeMaster() != null){
                        masterDataRepository.savePinCodeMaster(updateDataRequest.getDataToUpdate().getPinCodeMaster(),
                                updateDataRequest.getDataToUpdate().isUpdate(),updateDataRequest.getHeader().getInstitutionId(),
                                updateDataRequest.getHeader().getLoggedInUserId());
                    }
                    updateResponse.setPinCodeMaster(updateDataRequest.getDataToUpdate().getPinCodeMaster());
                    break;
                case EndPointReferrer.DELETE_PINCODE :
                    logger.debug(EndPointReferrer.DELETE_PINCODE);
                    activityLogs(updateDataRequest,EndPointReferrer.DELETE_PINCODE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getPinCodeMaster() != null){
                        masterDataRepository.deletePincoideMaster(updateDataRequest.getDataToUpdate().getPinCodeMaster().getZipCode()
                                ,updateDataRequest.getHeader().getInstitutionId());
                    }
                    updateResponse.setBankDetailsMaster(updateDataRequest.getDataToUpdate().getBankDetailsMaster());
                    updateResponse.setMessage("Deleted");
                    break;

                case EndPointReferrer.COMPANY_MASTER_UPDATE :
                    logger.debug(EndPointReferrer.COMPANY_MASTER_UPDATE);
                    activityLogs(updateDataRequest,EndPointReferrer.COMPANY_MASTER_UPDATE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getEmploymentMaster() != null){
                        if(!updateDataRequest.getDataToUpdate().isUpdate()){
                            if(StringUtils.isEmpty(updateDataRequest.getDataToUpdate().getEmploymentMaster().getEmployerId())){
                                List <EmployerMaster> employerMasterList = masterDataRepository.getEmployerMaster(updateDataRequest.getHeader().getInstitutionId());
                                if(CollectionUtils.isNotEmpty(employerMasterList)){
                                    int maxCnt = Integer.parseInt(employerMasterList.get(0).getEmployerId()) +1;
                                    updateDataRequest.getDataToUpdate().getEmploymentMaster().setEmployerId(String.valueOf(maxCnt));
                                }
                            }
                        }
                        masterDataRepository.saveEmployerMaster(updateDataRequest.getDataToUpdate().getEmploymentMaster(),
                                updateDataRequest.getDataToUpdate().isUpdate(), updateDataRequest.getHeader().getInstitutionId());
                    }
                    updateResponse.setEmployerMaster(updateDataRequest.getDataToUpdate().getEmploymentMaster());
                    updateResponse.setMessage("Updated");
                    break;
                case EndPointReferrer.COMPANY_MASTER_DELETE :
                    activityLogs(updateDataRequest,EndPointReferrer.COMPANY_MASTER_DELETE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getEmploymentMaster() != null){
                        masterDataRepository.deleteEmployerMaster(updateDataRequest.getDataToUpdate().getEmploymentMaster().getEmployerId(),
                                updateDataRequest.getHeader().getInstitutionId());
                    }
                    break;
                case  EndPointReferrer.BANK_MASTER_UPDATE :
                    activityLogs(updateDataRequest,EndPointReferrer.BANK_MASTER_UPDATE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getBankMaster() != null) {
                        masterDataRepository.saveBankMaster(updateDataRequest.getDataToUpdate().getBankMaster(),updateDataRequest.getDataToUpdate().isUpdate(),
                                updateDataRequest.getHeader().getInstitutionId());
                        updateResponse.setBankMaster(updateDataRequest.getDataToUpdate().getBankMaster());
                    }
                    break;
                case EndPointReferrer.BANK_MASTER_DELETE :
                    logger.debug(EndPointReferrer.BANK_MASTER_DELETE);
                    activityLogs(updateDataRequest,EndPointReferrer.BANK_MASTER_DELETE,httpRequest, stepId);
                    if(updateDataRequest.getDataToUpdate().getBankMaster() != null){
                        masterDataRepository.deleteBankMaster(updateDataRequest.getDataToUpdate().getBankMaster().get_id()
                                ,updateDataRequest.getHeader().getInstitutionId());
                    }
                    updateResponse.setBankMaster(updateDataRequest.getDataToUpdate().getBankMaster());
                    updateResponse.setMessage("Deleted");
                    break;
                case EndPointReferrer.GET_BANK_MASTER :
                    logger.debug(EndPointReferrer.GET_BANK_MASTER);
                    activityLogs(updateDataRequest,EndPointReferrer.GET_BANK_MASTER,httpRequest, stepId);
                    List <BankMaster> bankMasterList = masterDataRepository.getBankInfoFromBankMaster(
                            updateDataRequest.getDataToUpdate().getBankMaster().getBankDescription(),
                            updateDataRequest.getHeader().getInstitutionId());
                    if(CollectionUtils.isNotEmpty(bankMasterList)){
                        updateResponse.setBankMasterList(bankMasterList);
                    }else{
                        return   GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, updateResponse );
                    }
                    break;
                case EndPointReferrer.BUREAU_HIT_COUNT :
                    activityLogs(updateDataRequest,EndPointReferrer.BUREAU_HIT_COUNT,httpRequest, stepId);
                    updateBureauHitCount(updateDataRequest,updateResponse);
                    break;
            }
            if(CollectionUtils.isNotEmpty(applicationRequestList)){
                updateResponse.setApplicationRequest(applicationRequestList);
            }
            baseResponse =  GngUtils.getBaseResponse(HttpStatus.OK, updateResponse );
            return baseResponse;
        }catch(Exception ex){
            logger.error("{} updateManagerImpl", ExceptionUtils.getStackTrace(ex));
        }
        return null;
    }

    private void updateIfscCode(UpdateDataRequest updateDataRequest, UpadateResponse updateResponse ) {
        if(updateDataRequest.getDataToUpdate().getBankDetailsMaster() != null){
            masterDataRepository.saveBankMasterDetail(updateDataRequest.getDataToUpdate().getBankDetailsMaster(),
                    updateDataRequest.getDataToUpdate().isUpdate(),updateDataRequest.getHeader().getInstitutionId(),
                    updateDataRequest.getHeader().getLoggedInUserId());
        }
        updateResponse.setBankDetailsMaster(updateDataRequest.getDataToUpdate().getBankDetailsMaster());
    }

    private void updateDisbursalData(UpdateDataRequest updateDataRequest, List<ApplicationRequest> applicationRequestList) {
        logger.debug("{} refId " ,updateDataRequest.getRefId()+EndPointReferrer.UPDATE_DISBURSAL_DATA +" will update");
        if(StringUtils.isNotEmpty(updateDataRequest.getDataToUpdate().getProspectCode())  &&
                StringUtils.isNotEmpty(updateDataRequest.getDataToUpdate().getApplicantCode()))
        {
            applicationRepository.updateMifinData(updateDataRequest.getRefId(),updateDataRequest.getDataToUpdate().getApplicantCode(),
                    updateDataRequest.getDataToUpdate().getProspectCode());
        }
        try{
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(updateDataRequest.getRefId());
            applicationRequestList.add(goNoGoCustomerApplication.getApplicationRequest());
        }catch (Exception ex){
            logger.error("{}",ex.getStackTrace());
        }

    }

    private void updateBranchData(UpdateDataRequest updateDataRequest) {
        boolean update = applicationRepository.updateBranchData(updateDataRequest.getRefId(),updateDataRequest.getDataToUpdate().getBranchV2());
        logger.debug("update Branch{}",update);
    }

    private void activityLogs(UpdateDataRequest updateDataRequest, String branchUpdate, HttpServletRequest httpRequest, String stepId) {
        String msg = null;
        logger.debug("{} refId branch will update To {}" ,updateDataRequest.getRefId(), branchUpdate);
        msg = updateDataRequest.getRefId()+"refId case will change branch Name ";
        ActivityLogs  activityLog = auditHelper.createActivityLog(updateDataRequest, httpRequest,
                null, stepId, null, updateDataRequest.getHeader().getLoggedInUserId(),msg);
        activityEventPublisher.publishEvent(activityLog);
    }

    private void updateBureauHitCount(UpdateDataRequest updateDataRequest, UpadateResponse updateResponse ) {
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(updateDataRequest.getRefId());

            List<Applicant> ApplicantList = new ArrayList<Applicant>();
            ApplicantList.add(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant());
            if(CollectionUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant())){
                for(CoApplicant coApp : goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant()) {
                    ApplicantList.add(coApp);
                }
            }
            if (!updateDataRequest.getDataToUpdate().isUpdate()) {

                updateResponse.setApplicant(ApplicantList);
            } else {
                for(Applicant applicant: ApplicantList) {
                    String appId = (updateDataRequest.getDataToUpdate().getAppId().stream().filter(appIdData ->
                            StringUtils.equalsIgnoreCase(applicant.getApplicantId(),appIdData))).findAny().orElse(null);
                    if(appId != null) {
                        boolean response = applicationRepository.updateBureauCount(updateDataRequest.getRefId(), updateDataRequest.getHeader().getInstitutionId(), appId);
                    }
                }
            }
        }
        catch (Exception e){
            logger.error("{}",e.getStackTrace());
        }
    }
}
