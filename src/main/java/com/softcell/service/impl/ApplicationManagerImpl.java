package com.softcell.service.impl;


import com.mongodb.WriteResult;
import com.softcell.constants.*;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.DashboardRepository;
import com.softcell.dao.mongodb.repository.DoOperationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.imps.IMPSMongoRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.dashboard.DashboardDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.dashboard.DashboardResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyPremiumRequest;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.GetInsurancePremiumRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.*;
import com.softcell.gonogo.model.response.core.*;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.extendedwarranty.ExtendedWarrantyPremiumResponse;
import com.softcell.gonogo.model.response.extendedwarranty.UpdateExtendedWarrantyDetailsResponse;
import com.softcell.gonogo.model.response.geolimit.GeoLimitResponse;
import com.softcell.gonogo.model.response.insurance.InsurancePremiumResponse;
import com.softcell.gonogo.model.response.insurance.UpdateInsuranceDetailsResponse;
import com.softcell.gonogo.service.factory.DoOperationBuilder;
import com.softcell.gonogo.service.factory.GngResponseEntityBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.service.*;
import com.softcell.service.docs.DemoPdfGenerator;
import com.softcell.service.docs.PdfGenerator;
import com.softcell.service.validator.ApplicationValidationEngine;
import com.softcell.utils.DeviceTraceUtility;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.RequestAggregator;
import com.softcell.workflow.SequenceGenerator;
import com.softcell.workflow.component.manager.ComponentManager;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;


/**
 * @author yogeshb
 */
@Service
public class ApplicationManagerImpl implements ApplicationManager {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationManagerImpl.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private DashboardRepository dashboardRepository;

    @Autowired
    private DMZConnector dmzConnector;

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ApplicationValidationEngine applicationValidationEngine;

    @Autowired
    private DigitizationStoreManager digitizationStoreManager;

    @Autowired
    private MasterDataViewManager masterDataViewManager;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    @Autowired
    private DoOperationRepository doOperationRepository;

    @Autowired
    private DoOperationBuilder doOperationBuilder;

    @Autowired
    private IMPSMongoRepository impsMongoRepository;

    @Override
    public BaseResponse submitApplication(
            ApplicationRequest applicationRequest) throws Exception {

        logger.debug("submitApplication service started");

        String refId = SequenceGenerator.getApplicationId();

        applicationRequest.setRefID(refId);
        applicationRequest.getHeader().setApplicationId(
                SequenceGenerator.getApplicationId());
        applicationRequest.getHeader().setDateTime(new Date());

        String dob = applicationRequest.getRequest().getApplicant()
                .getDateOfBirth();

        if (StringUtils.isNotBlank(dob)) {

            applicationRequest.getRequest().getApplicant()
                    .setAge(GngDateUtil.getAge(dob));
        }


        String gonogoRefId = applicationRepository
                .saveApplication(applicationRequest);

        GoNoGoCustomerApplication goNoGoCustomerApplication = RequestAggregator
                .getGoNoGo(applicationRequest);

        logger.debug("saveApplication service started ComponentManager thread to process application request");

        ComponentManager componentManager = new ComponentManager(
                goNoGoCustomerApplication);

        componentManager.start();

        Acknowledgement ack = new Acknowledgement();

        if (gonogoRefId != null) {

            AckHeader ackHeader = new AckHeader();
            ackHeader.setApplicationId(applicationRequest.getHeader()
                    .getApplicationId());
            ackHeader.setInstitutionId(applicationRequest.getHeader()
                    .getInstitutionId());
            ackHeader.setRequestReceivedTime(new Date());
            ackHeader.setResponseDateTime(new Date());
            ackHeader.setResponseType("");
            ack.setHeader(ackHeader);
            ack.setGonogoRefId(gonogoRefId);
            ack.setStatus(Status.SUCCESS.toString());
            Warnings warnings = new Warnings();
            warnings.setCode("");
            warnings.setDescription("");
            ack.setWarnings(warnings);
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.setCode("");
            errorMessage.setDescription("");
            ack.setErrors(errorMessage);
        } else {
            AckHeader ackHeader = new AckHeader();
            ackHeader.setApplicationId(applicationRequest.getHeader()
                    .getApplicationId());
            ackHeader.setInstitutionId(applicationRequest.getHeader()
                    .getInstitutionId());
            ackHeader.setRequestReceivedTime(new Date());
            ackHeader.setResponseDateTime(new Date());
            ackHeader.setResponseType("");
            ack.setHeader(ackHeader);
            ack.setGonogoRefId(gonogoRefId);
            ack.setStatus(Status.ERROR.name());
            Warnings warnings = new Warnings();
            warnings.setCode("");
            warnings.setDescription("");
            ack.setWarnings(warnings);
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.setCode("");
            errorMessage.setDescription("");
            ack.setErrors(errorMessage);
        }


        logger.debug("submitApplication service completed ");

        return GngUtils.getBaseResponse(HttpStatus.OK, ack);

    }

    @Override
    public BaseResponse appExists(final String refId) {
        try {
            return GngUtils.getBaseResponse(HttpStatus.OK, applicationRepository.isGoNoGoCustomerAppPresent(refId));
        } catch (Exception ex) {
            return BaseResponse.builder().status(com.softcell.gonogo.model.response.core.Status.builder()
                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .statusValue(HttpStatus.INTERNAL_SERVER_ERROR.name()).build())
                    .errors(GngUtils.getCouldNotProcessErrorList()).build();
        }
    }

    @Override
    public BaseResponse getStatus(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("getStatusValue service started");

        ApplicationResponse applicationResponse = applicationRepository
                .checkStatus(checkApplicationStatus.getGonogoRefId(),
                        checkApplicationStatus.getHeader()
                                .getInstitutionId());
        BaseResponse baseResponse = null;
        if (null != applicationResponse) {
            applicationResponse.setGonogoRefId(checkApplicationStatus
                    .getGonogoRefId());

            AckHeader ackHeader = new AckHeader();
            ackHeader.setApplicationId(checkApplicationStatus.getHeader()
                    .getApplicationId());
            ackHeader.setInstitutionId(checkApplicationStatus.getHeader()
                    .getInstitutionId());
            ackHeader.setRequestReceivedTime(new Date());
            ackHeader.setResponseDateTime(new Date());
            ackHeader.setResponseType(MediaType.APPLICATION_JSON_VALUE);
            applicationResponse.setHeader(ackHeader);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationResponse);

            logger.debug("getStatusValue service completed");

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;

    }

    @Override
    public BaseResponse setpostIpaPdf(PostIpaRequest postIpaRequest,
                                      HttpServletRequest httpRequest) throws Exception {

        logger.debug("setpostIpaPdf service started");

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(postIpaRequest.getRefID(), postIpaRequest.getHeader().getInstitutionId());

        if (null != goNoGoCroApplicationResponse) {
            if (StringUtils.equalsIgnoreCase(goNoGoCroApplicationResponse.getApplicationStatus(), GNGWorkflowConstant.DECLINED.toFaceValue()) &&
                    StringUtils.equalsIgnoreCase(goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId(), GNGWorkflowConstant.DCLN.toFaceValue())) {

                return GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
            }
            if (null != goNoGoCroApplicationResponse.getApplicationRequest() &&
                    null != goNoGoCroApplicationResponse.getApplicationRequest().getHeader() &&
                    !StringUtils.equals(goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId(), postIpaRequest.getHeader().getDealerId())) {

                return GngUtils.getBaseResponse(HttpStatus.CONFLICT, GngUtils.getDealerMissMatchErrorList(goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId(),
                        postIpaRequest.getHeader().getDealerId()));
            }
        }

        BaseResponse baseResponse = null;

        Document doc = new Document();

        postIpaRequest.getHeader().setDateTime(new Date());

        DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
        deviceTraceUtility.populateDeviceInfo(postIpaRequest.getHeader(), httpRequest);

        DealerEmailMaster dealerEmailMaster = applicationRepository
                .getDealerEmailMaster(postIpaRequest.getHeader());

        String dealerName = null;

        String itNum = null;

        if (null != dealerEmailMaster) {

            dealerName = dealerEmailMaster.getSupplierDesc();
            itNum = dealerEmailMaster.getItNum();

            if (StringUtils.equals(Status.N.name(), itNum)) {

                postIpaRequest.getPostIPA().setDlrSbvnWaivedAtPOS(true);

            } else {

                postIpaRequest.getPostIPA().setDlrSbvnWaivedAtPOS(false);

            }
        }

        if (null != dealerName) {

            postIpaRequest.getPostIPA().getAssetDetails().get(0)
                    .setDlrName(dealerName);

        } else {

            doc.setDocName(GNGWorkflowConstant.DO_REPORT.toFaceValue());
            doc.setStatus(Status.FAILED.toString());

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.DATABASE.name())
                    .errorCode(Error.ERROR_TYPE.DATABASE.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .message(ErrorCode.DEALER_NOT_FOUND)
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);

            logger.warn("Dependency occurred while fetching DealerEmailMaster for referenceId {}  dealer not found in master.", postIpaRequest.getRefID());

            return baseResponse;
        }

        /**
         * Save post ipa details in database.
         */
        applicationRepository.savePostIpaDetails(postIpaRequest);

        /**
         * Update balance Amount
         */
        applicationRepository.setMultiProductUpdatedParameter(postIpaRequest);


        if (StringUtils.equals(Status.N.name(), itNum) && postIpaRequest.getPostIPA().getDealerSubvention() > 0) {

            postIpaRequest.getPostIPA().setDealerSubvention(0);

        }

        byte[] pdfBytes;

        PdfGenerator pdfGenerator;

        FileUploadRequest fileUploadRequest;


        try {

            /**
             * app for demo Institution
             */
            if (lookupService.checkActionsAccess(postIpaRequest.getHeader()
                    .getInstitutionId(), "", ActionName.DELIVERY_ORDER)) {

                DemoPdfGenerator demoPdfGenerator = new DemoPdfGenerator();

                pdfBytes = demoPdfGenerator.getPdfByte(postIpaRequest);

                fileUploadRequest = demoPdfGenerator.setFileUploadRequest(postIpaRequest);

            } else {

                pdfGenerator = new PdfGenerator();

                pdfBytes = pdfGenerator.getPdfByte(postIpaRequest);

                fileUploadRequest = pdfGenerator.setFileUploadRequest(postIpaRequest);
            }

            InputStream inputStream = new ByteArrayInputStream(pdfBytes);

            applicationRepository.softDeleteDOReport(postIpaRequest
                    .getRefID(), postIpaRequest.getHeader()
                    .getInstitutionId(), fileUploadRequest.getUploadFileDetails().getFileName());

            String fileID = uploadFileRepository.store(inputStream,
                    postIpaRequest.getRefID(), MediaType.APPLICATION_PDF_VALUE,
                    fileUploadRequest);

            logger.debug("setpostIpaPdf service file stored with id {}", fileID);

            byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);

            doc.setByteCode(new String(encodeBase64));

            doc.setDocID(fileID);

            doc.setDocName(GNGWorkflowConstant.DO_REPORT.toFaceValue());

            doc.setStatus(Status.SUCCESS.name());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);

        } catch (Exception e) {

            logger.error("Error occurred  while generating DO report with probable cause [{}]", e.getMessage());

            throw new Exception(String.format("Error occurred  while generating DO report with probable cause [{%s}]", e.getMessage()));

        }


        logger.debug("set postIpaPdf service completed.");

        return baseResponse;

    }

    @Override
    public BaseResponse savePostIpaRequest(PostIpaRequest postIpaRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("save postIpaRequest service started");

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(postIpaRequest.getRefID(), postIpaRequest.getHeader().getInstitutionId());

        if (null != goNoGoCroApplicationResponse) {
            if (StringUtils.equalsIgnoreCase(goNoGoCroApplicationResponse.getApplicationStatus(), GNGWorkflowConstant.DECLINED.toFaceValue()) &&
                    StringUtils.equalsIgnoreCase(goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId(), GNGWorkflowConstant.DCLN.toFaceValue())) {

                return GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
            }
            if (null != goNoGoCroApplicationResponse.getApplicationRequest() &&
                    null != goNoGoCroApplicationResponse.getApplicationRequest().getHeader() &&
                    !StringUtils.equals(goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId(), postIpaRequest.getHeader().getDealerId())) {

                return GngUtils.getBaseResponse(HttpStatus.CONFLICT, GngUtils.getDealerMissMatchErrorList(goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId(),
                        postIpaRequest.getHeader().getDealerId()));
            }
        }


        postIpaRequest.getHeader().setDateTime(new Date());

        DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
        deviceTraceUtility.populateDeviceInfo(postIpaRequest.getHeader(), httpRequest);

        DealerEmailMaster dealerEmailMaster = applicationRepository
                .getDealerEmailMaster(postIpaRequest.getHeader());

        String dealerName = null;

        String itNum = null;

        if (null != dealerEmailMaster) {

            dealerName = dealerEmailMaster.getSupplierDesc();
            itNum = dealerEmailMaster.getItNum();

            if (StringUtils.equals(Status.N.name(), itNum)) {

                postIpaRequest.getPostIPA().setDlrSbvnWaivedAtPOS(true);

            } else {

                postIpaRequest.getPostIPA().setDlrSbvnWaivedAtPOS(false);

            }
        }

        if (null != dealerName) {

            postIpaRequest.getPostIPA().getAssetDetails().get(0)
                    .setDlrName(dealerName);
            //set delivery order by default status "NOT_CREATED"
            postIpaRequest.getPostIPA().setDeliveryOrderStatus(DoStatusEnum.NOT_CREATED.name());

        } else {

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.DATABASE.name())
                    .errorCode(Error.ERROR_TYPE.DATABASE.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .message(ErrorCode.DEALER_NOT_FOUND)
                    .build());

            logger.warn("Dependency occurred while fetching DealerEmailMaster for referenceId {}  dealer not found in master.", postIpaRequest.getRefID());

            GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);

        }

        /**
         * Save post ipa details in database.
         */
        applicationRepository.savePostIpaDetails(postIpaRequest);

        /**
         * Update balance Amount
         */
        applicationRepository.setMultiProductUpdatedParameter(postIpaRequest);

        logger.debug("save postIpaRequest service completed.");

        return GngUtils.getBaseResponse(HttpStatus.OK, Status.SUCCESS.name());


    }

    @Override
    public BaseResponse getpostIpaPdf(PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("getpostIpaPdf service started");

        PostIPA postIpa = applicationRepository.getPostIPA(postIpaRequest.getRefID(),
                postIpaRequest.getHeader().getInstitutionId());

        if (null != postIpa) {
            return GngUtils.getBaseResponse(HttpStatus.OK, postIpa);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

    }

    @Override
    public BaseResponse getDashBoardData(DashboardRequest dashboardRequest) throws Exception {

        logger.debug("getDashBoardData service started");

        BaseResponse baseResponse = null;

        if (configurationManager.isValidVersion(dashboardRequest.getHeader().getInstitutionId(),
                dashboardRequest.getHeader().getApplicationSource())) {

            DashboardResponse dashboardResponse = dashboardRepository.getDashboardDataForQueueManagement(dashboardRequest);

            if (!CollectionUtils.isEmpty(dashboardResponse.getDashboardDetailsList())) {
                if(dashboardRequest.getHeader().getApplicationSource().contains("WEB")){
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dashboardResponse);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dashboardResponse.getDashboardDetailsList());
                }
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse setPostIpaStage(PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("setPostIpaStage service started");

        BaseResponse response;

        ActivityLogs activityLog = getActivityLog(postIpaRequest, GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(postIpaRequest.getRefID()
                , postIpaRequest.getHeader().getInstitutionId());

        PostIPA postIPA = applicationRepository.getPostIPA(postIpaRequest.getRefID(), postIpaRequest.getHeader().getInstitutionId());
        String applicationStatus = goNoGoCroApplicationResponse.getApplicationStatus();
        String currentStageId = goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId();

        activityLog.setStatus(applicationStatus);
        // TODO : what is action to set for activitylog ?

        boolean checkForDeclineCase = null == postIPA
                && StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.DECLINED.toFaceValue())
                && StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.DCLN.toFaceValue());

        boolean checkForApprovedCase = null != postIPA
                && StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.APPROVED.toFaceValue());
        /**
         * set post ipa stage only if application is approved and post ipa is not null or application is declined and post Ipa is null
         */
        if (checkForDeclineCase || checkForApprovedCase) {
            postIpaRequest.getHeader().setDateTime(new Date());
            String status = applicationRepository.setPostIpaStage(
                    postIpaRequest.getRefID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue(), postIpaRequest
                            .getHeader().getInstitutionId());

            Acknowledgement acknowledgement = new Acknowledgement();
            acknowledgement.setStatus(status);

            logger.debug("setPostIpaStage service completed with status {}", status);

            try {

                /*Cache.checkActionsAccess(ActionName.RELIANCE_INTERFACE, postIpaRequest.getHeader().getInstitutionId(),
                        postIpaRequest.getHeader().getProduct().toProductId());*/

                if (lookupService.checkActionsAccess(postIpaRequest.getHeader().getInstitutionId(),
                        postIpaRequest.getHeader().getProduct().toProductId(),
                        ActionName.RELIANCE_INTERFACE)) {

                    logger.debug("setPostIpaStage calling reliance service");

                    DmzRequest dmzRequest = new DmzRequest();
                    dmzRequest
                            .setGonogoRefId(postIpaRequest.getRefID());
                    dmzRequest.setHeader(postIpaRequest.getHeader());
                    dmzRequest.setActionName(ActionName.RELIANCE_INTERFACE);
                    DMZResponse dmzResponse = GngUtils.convertBaseResponsePayload(dmzConnector.connectRelianceDigitalService(dmzRequest), DMZResponse.class);

                    if (dmzResponse != null) {
                        logger.debug("connectRelianceDigitalService service completed with status {}", dmzResponse);
                    }
                }
                if (lookupService.checkActionsAccess(postIpaRequest.getHeader().getInstitutionId(),
                        postIpaRequest.getHeader().getProduct().toProductId(),
                        ActionName.TKIL_INTERFACE)) {

                    logger.debug("setPostIpaStage calling tkil service");

                    DmzRequest dmzRequest = new DmzRequest();
                    dmzRequest
                            .setGonogoRefId(postIpaRequest.getRefID());
                    dmzRequest.setHeader(postIpaRequest.getHeader());
                    dmzRequest.setActionName(ActionName.TKIL_INTERFACE);
                    DMZResponse dmzResponse = GngUtils.convertBaseResponsePayload(dmzConnector.pushTkil(dmzRequest), DMZResponse.class);

                    if (dmzResponse != null) {
                        logger.debug("pushTkil service completed with status {}", dmzResponse);
                    }
                }
            } catch (Exception e) {
                logger.error("error while posting data {}", e.getMessage());
            }


            response = GngUtils.getBaseResponse(HttpStatus.OK, acknowledgement);
        } else {
            response = GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
        }

        // Save activity log
        stopwatch.stop();
        activityLog.setDuration(stopwatch.getLastTaskTimeMillis());
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return response;
    }

    @Override
    public BaseResponse setPostIpaStageAndStoreDO(PostIpaRequest postIpaRequest) throws Exception {

        logger.debug("setPostIpaStage service started");

        BaseResponse baseResponse;

        Document document = null;

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(postIpaRequest.getRefID()
                , postIpaRequest.getHeader().getInstitutionId());

        PostIPA postIPA = applicationRepository.getPostIPA(postIpaRequest.getRefID(), postIpaRequest.getHeader().getInstitutionId());
        String applicationStatus = goNoGoCroApplicationResponse.getApplicationStatus();
        String currentStageId = goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId();

        boolean checkForDeclineCase = null == postIPA
                && StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.DECLINED.toFaceValue())
                && StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.DCLN.toFaceValue());

        boolean checkForApprovedCase = null != postIPA
                && StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.APPROVED.toFaceValue());
        /**
         * set post ipa stage only if application is approved and post ipa is not null or application is declined and post Ipa is null
         */
        if (checkForDeclineCase || checkForApprovedCase) {

            postIpaRequest.getHeader().setDateTime(new Date());

            //set the application stage to PD_DE
            String status = applicationRepository.setPostIpaStage(
                    postIpaRequest.getRefID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue(), postIpaRequest
                            .getHeader().getInstitutionId());

            // Save activity log
            ActivityLogs activityLog = getActivityLog(postIpaRequest, GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());

            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLog);

            logger.debug("setPostIpaStage service completed with status {}", status);

            try {

                if (lookupService.checkActionsAccess(postIpaRequest.getHeader().getInstitutionId(),
                        postIpaRequest.getHeader().getProduct().toProductId(),
                        ActionName.RELIANCE_INTERFACE)) {

                    logger.debug("setPostIpaStage calling reliance service");

                    DmzRequest dmzRequest = new DmzRequest();
                    dmzRequest
                            .setGonogoRefId(postIpaRequest.getRefID());
                    dmzRequest.setHeader(postIpaRequest.getHeader());
                    dmzRequest.setActionName(ActionName.RELIANCE_INTERFACE);
                    DMZResponse dmzResponse = GngUtils.convertBaseResponsePayload(dmzConnector.connectRelianceDigitalService(dmzRequest), DMZResponse.class);

                    if (dmzResponse != null) {

                        logger.debug("connectRelianceDigitalService service completed with status {}", dmzResponse);

                    }
                }
                if (lookupService.checkActionsAccess(postIpaRequest.getHeader().getInstitutionId(),
                        postIpaRequest.getHeader().getProduct().toProductId(),
                        ActionName.TKIL_INTERFACE)) {

                    logger.debug("setPostIpaStage calling tkil service");

                    DmzRequest dmzRequest = new DmzRequest();
                    dmzRequest
                            .setGonogoRefId(postIpaRequest.getRefID());
                    dmzRequest.setHeader(postIpaRequest.getHeader());
                    dmzRequest.setActionName(ActionName.TKIL_INTERFACE);
                    DMZResponse dmzResponse = GngUtils.convertBaseResponsePayload(dmzConnector.pushTkil(dmzRequest), DMZResponse.class);

                    if (dmzResponse != null) {

                        logger.debug("pushTkil service completed with status {}", dmzResponse);

                    }
                }

                document = new Document();
                document.setDocName(GNGWorkflowConstant.DO_REPORT.name());
                document.setStatus(Status.ERROR.name());

                // check for PD_DE stage update
                if (StringUtils.equals(status, Status.SUCCESS.name())) {

                    // if application is approve then generate the Delivery Order
                    if (checkForApprovedCase) {

                        String deliveryOrderFileId = digitizationStoreManager.saveDeliveryOrderForm(postIpaRequest, postIPA, DoStatusEnum.CREATED.name());

                        if (StringUtils.isNotBlank(deliveryOrderFileId)) {
                            logger.info("Delivery order store in database successfully with  fileId {}", deliveryOrderFileId);

                            DoOperationLog doOperationLog = doOperationBuilder.buildDoOperationLog(postIpaRequest.getRefID()
                                    , postIpaRequest.getHeader(), DoStatusEnum.CREATED.name());
                            //save doOperationLog
                            doOperationRepository.saveDoOperationLog(doOperationLog);

                            //update DoStatus in the Post Ipa
                            doOperationRepository.updateDoStatusInPostIpa(postIpaRequest.getRefID(), postIpaRequest.getHeader().getInstitutionId(),
                                    DoStatusEnum.CREATED.name());

                            document.setStatus(Status.SUCCESS.name());
                            document.setDocID(deliveryOrderFileId);

                        } else {
                            // revert PD_DE stage to previous stage
                            applicationRepository.setPostIpaStage(
                                    postIpaRequest.getRefID(), currentStageId, postIpaRequest
                                            .getHeader().getInstitutionId());
                        }
                        // if application is decline then only update the stage ,do not generate Delivery Order.
                    } else {
                        document.setStatus(Status.SUCCESS.name());
                    }

                }

            } catch (Exception e) {
                logger.error("{}",e.getStackTrace());
                logger.error("Exception occur at the time updating post ipa stage and generating DO with probable cause", e.getMessage());
                throw new Exception(e.getMessage());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, document);

        } else {
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
        }

        return baseResponse;

    }

    @Override
    public BaseResponse getPartialSaveRequest(
            PartialSaveRequest partialSaveRequest) {

        logger.debug("getPartialSaveRequest service started");

        ApplicationRequest applicationResponse = applicationRepository
                .getPartialSaveRequest(partialSaveRequest);

        if (null != applicationResponse) {

            logger.debug("getPartialSaveRequest service completed with a record");
            return GngUtils.getBaseResponse(HttpStatus.OK, applicationResponse);

        } else {

            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
    }

    @Override
    public BaseResponse setFileHandOverDetails(FileHandoverDetails fileHandoverDetails) throws Exception {

        logger.debug("setFileHandoverDetails service started");

        applicationRepository.upsertFileHandoverDetails(fileHandoverDetails);

        logger.debug("setFileHandoverDetails service completed with status");

        Acknowledgement acknowledgement = GngResponseEntityBuilder
                .getSuccesAcknowledgement(fileHandoverDetails.getRefId());

        return GngUtils.getBaseResponse(HttpStatus.OK, acknowledgement);
    }

    @Override
    public BaseResponse getFileHandoverDetails(
            FileHandoverDetailsRequest fileHandoverDetailsRequest) throws Exception {

        logger.debug("getFileHandoverDetails service started");

        FileHandoverDetails fileHandoverDetails = applicationRepository
                .getFileHandoverDetails(fileHandoverDetailsRequest);

        BaseResponse baseResponse;
        if (fileHandoverDetails != null) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, fileHandoverDetails);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        logger.debug("getFileHandoverDetails service completed");

        return baseResponse;
    }

    @Override
    public BaseResponse getNegativeAreaFundingDetails(NegativeAreaFundingRequest negativeAreaFundingRequest) throws Exception {

        logger.debug("getNegativeAreaFundingDetails service started");

        NegativeAreaFundingResponse negativeAreaFundingResponse = new NegativeAreaFundingResponse();

        List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMastersList = applicationRepository.getNegativeAreaFundingDetails(negativeAreaFundingRequest);
        BaseResponse baseResponse;

        if (negativeAreaGeoLimitMastersList != null && !negativeAreaGeoLimitMastersList.isEmpty()) {

            negativeAreaFundingResponse = negativeAreaFundingResponse.populateResponse(negativeAreaGeoLimitMastersList);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, negativeAreaFundingResponse.populateResponse(negativeAreaGeoLimitMastersList));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }

        logger.debug("getNegativeAreaFundingDetails service completed");

        return baseResponse;
    }

    @Override
    public BaseResponse getApplicationStatus(CheckApplicationStatus checkApplicationStatus) throws Exception {

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(checkApplicationStatus.getGonogoRefId(),
                checkApplicationStatus.getHeader().getInstitutionId());

        BaseResponse baseResponse;

        if (null != goNoGoCustomerApplication
                && StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationStatus())
                && StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {

            ApplicationStatusResponse applicationStatusResponse = new ApplicationStatusResponse();

            applicationStatusResponse.setApplicationStatus(goNoGoCustomerApplication.getApplicationStatus());
            applicationStatusResponse.setApplicationStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
            applicationStatusResponse.setRefID(goNoGoCustomerApplication.getGngRefId());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationStatusResponse);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse updateApplicantReferences(UpdateReferencesRequest updateReferencesRequest) {
        WriteResult writeResult = applicationRepository.updateApplicantReferences(updateReferencesRequest);
        BaseResponse baseResponse;
        if (null != writeResult && writeResult.isUpdateOfExisting()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, true);
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse updateBankingDetails(BankingDetailsRequest bankingDetailsRequest) {

        WriteResult writeResult = applicationRepository.updateBankingDetails(bankingDetailsRequest);
        BaseResponse baseResponse;
        if (null != writeResult && writeResult.isUpdateOfExisting()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, true);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getExtendedWarrantyPremium(ExtendedWarrantyPremiumRequest extendedWarrantyPremiumRequest) {

        BaseResponse baseResponse;
        String institutionId = extendedWarrantyPremiumRequest.getHeader().getInstitutionId();
        String gngAssetCategory = extendedWarrantyPremiumRequest.getGngAssetCategory();
        double gngAssetPrice = extendedWarrantyPremiumRequest.getAssetPrice();
        String productName = extendedWarrantyPremiumRequest.getHeader().getProduct().name();
        ExtendedWarrantyPremiumResponse extendedWarrantyPremiumResponse = new ExtendedWarrantyPremiumResponse();

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            EwInsGngAssetCategoryMaster ewAssetCategoryAndOemWarranty = applicationRepository.getAssetCategoryAndOemWarranty(institutionId, productAliasName, gngAssetCategory);

            String eWAssetCategory = null != ewAssetCategoryAndOemWarranty && StringUtils.isNotBlank(ewAssetCategoryAndOemWarranty.getExtendedWarrantyAssetCat()) ?
                    ewAssetCategoryAndOemWarranty.getExtendedWarrantyAssetCat() : "";

            if (StringUtils.isBlank(eWAssetCategory)) {
                extendedWarrantyPremiumResponse.setStatus(Status.FAILED.name());
                extendedWarrantyPremiumResponse.setResMessage("Gng Asset Category Not Found In Master ");

                return GngUtils.getBaseResponse(HttpStatus.OK, extendedWarrantyPremiumResponse);
            }
            if (!applicationRepository.checkGngAssetAndEWAssetMapping(institutionId, productAliasName, eWAssetCategory)) {
                extendedWarrantyPremiumResponse.setStatus(Status.FAILED.name());
                extendedWarrantyPremiumResponse.setResMessage("Gng Asset Category And Extended Warranty Asset Category Mapping Not Found In Master ");

                return GngUtils.getBaseResponse(HttpStatus.OK, extendedWarrantyPremiumResponse);

            }

            EWPremiumMaster extendedWarrantyPremium = applicationRepository.getExtendedWarrantyPremium(institutionId, productAliasName, eWAssetCategory, gngAssetPrice);

            if (null != extendedWarrantyPremium) {

                String srNoOrImeiNo = getSerialNumberInfo(extendedWarrantyPremiumRequest.getRefId());

                if (StringUtils.isNotBlank(srNoOrImeiNo)) {
                    extendedWarrantyPremiumResponse.setSerialOrImeiNumber(srNoOrImeiNo);
                }

                extendedWarrantyPremiumResponse.setStatus(Status.SUCCESS.name());
                extendedWarrantyPremiumResponse.setResMessage("Extended Warranty Premium Data Found Successfully");
                extendedWarrantyPremiumResponse.setManufacturerWarranty(ewAssetCategoryAndOemWarranty.getManufacturerWarranty());
                extendedWarrantyPremiumResponse.setEwPremiumMaster(extendedWarrantyPremium);

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, extendedWarrantyPremiumResponse);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }


        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }

        return baseResponse;
    }

    private String getSerialNumberInfo(String refId) {

        SerialNumberInfo serialNumberInfo = applicationRepository.getSerialNumberInfo(refId);
        String srNoOrImeiNo = null;

        if (null != serialNumberInfo) {
            srNoOrImeiNo = StringUtils.isNotBlank(serialNumberInfo.getSerialNumber()) ? serialNumberInfo.getSerialNumber() : serialNumberInfo.getImeiNumber();
        }
        return srNoOrImeiNo;
    }

    @Override
    public BaseResponse updateExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails) {
        UpdateExtendedWarrantyDetailsResponse updateExtendedWarrantyDetailsResponse = new UpdateExtendedWarrantyDetailsResponse();
        updateExtendedWarrantyDetailsResponse.setStatus(Status.FAILED.name());
        updateExtendedWarrantyDetailsResponse.setResMessage(ErrorCode.FAILED_MESSAGE);

        /**
         * if activeFlag is true then update existing record otherwise insert new record if current record not found
         * if activeFlg is false then soft delete the existing record
         */
        if (extendedWarrantyDetails.isActiveFlag()) {

            applicationRepository.updateExtendedWarrantyDetails(extendedWarrantyDetails);
            updateExtendedWarrantyDetailsResponse.setStatus(Status.SUCCESS.name());

            updateExtendedWarrantyDetailsResponse.setResMessage(ErrorCode.DATA_SAVE_MESSAGE);

        } else if (applicationRepository.softDeleteExtendedWarrantyDetails(extendedWarrantyDetails)) {
            updateExtendedWarrantyDetailsResponse.setStatus(Status.SUCCESS.name());
            updateExtendedWarrantyDetailsResponse.setResMessage(ErrorCode.DATA_DELETE_MESSAGE);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, updateExtendedWarrantyDetailsResponse);
    }

    @Override
    public BaseResponse fetchExtendedWarrantyDetails(String refId, String institutionId) {

        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(refId, institutionId);
        BaseResponse baseResponse;

        if (null != extendedWarrantyDetails) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, extendedWarrantyDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getInsurancePremiumAgainstAssetCategory(GetInsurancePremiumRequest getInsurancePremiumRequest) {

        BaseResponse baseResponse;
        String institutionId = getInsurancePremiumRequest.getHeader().getInstitutionId();
        String gngAssetCategory = getInsurancePremiumRequest.getGngAssetCategory();
        double gngAssetPrice = getInsurancePremiumRequest.getAssetPrice();
        String productName = getInsurancePremiumRequest.getHeader().getProduct().name();
        InsurancePremiumResponse insurancePremiumResponse = new InsurancePremiumResponse();

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            EwInsGngAssetCategoryMaster ewInsGngAssetCategoryMaster = applicationRepository.getAssetCategoryAndOemWarranty(institutionId, productAliasName, gngAssetCategory);


            String insuranceAssetCat = null != ewInsGngAssetCategoryMaster && StringUtils.isNotBlank(ewInsGngAssetCategoryMaster.getInsuranceAssetCat()) ?
                    ewInsGngAssetCategoryMaster.getInsuranceAssetCat() : "";

            if (StringUtils.isBlank(insuranceAssetCat)) {

                insurancePremiumResponse.setStatus(Status.FAILED.name());
                insurancePremiumResponse.setResMessage("Gng Asset Category Not Found In Master ");

                return GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumResponse);
            }

            if (!applicationRepository.checkGngAssetAndInsuranceAssetMapping(institutionId, productAliasName, insuranceAssetCat)) {

                insurancePremiumResponse.setStatus(Status.FAILED.name());
                insurancePremiumResponse.setResMessage("Gng Asset Category And Insurance Asset Category Mapping Not Found In Master ");

                return GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumResponse);

            }

            InsurancePremiumMaster insurancePremiumMaster = applicationRepository.getInsurancePremiumData(institutionId, productAliasName, insuranceAssetCat, gngAssetPrice);

            if (null != insurancePremiumMaster) {

                String srNoOrImeiNo = getSerialNumberInfo(getInsurancePremiumRequest.getRefId());

                if (StringUtils.isNotBlank(srNoOrImeiNo)) {
                    insurancePremiumResponse.setSerialOrImeiNumber(srNoOrImeiNo);
                }

                insurancePremiumResponse.setStatus(Status.SUCCESS.name());
                insurancePremiumResponse.setResMessage("Insurance Premium Data Found Successfully");
                insurancePremiumResponse.setInsurancePremiumMaster(insurancePremiumMaster);

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumResponse);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse updateInsurancePremiumDetails(InsurancePremiumDetails insurancePremiumDetails) {

        UpdateInsuranceDetailsResponse updateInsuranceDetailsResponse = new UpdateInsuranceDetailsResponse();

        updateInsuranceDetailsResponse.setStatus(Status.FAILED.name());

        updateInsuranceDetailsResponse.setResMessage(ErrorCode.FAILED_MESSAGE);

        /**
         * if activeFlag is true then update existing record otherwise insert new record if current record not found
         * if activeFlg is false then soft delete the existing record.
         */
        if (insurancePremiumDetails.isActiveFlag()) {
            applicationRepository.updateInsuranceDetails(insurancePremiumDetails);
            updateInsuranceDetailsResponse.setStatus(Status.SUCCESS.name());
            updateInsuranceDetailsResponse.setResMessage(ErrorCode.DATA_SAVE_MESSAGE);

        } else if (applicationRepository.softDeleteInsuranceDetails(insurancePremiumDetails.getRefId())) {
            updateInsuranceDetailsResponse.setStatus(Status.SUCCESS.name());
            updateInsuranceDetailsResponse.setResMessage(ErrorCode.DATA_DELETE_MESSAGE);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, updateInsuranceDetailsResponse);
    }


    @Override
    public BaseResponse updateCreditVidyaDetails(CreditVidyaDetails creditVidyaDetails) {

        GenericMsgResponse genericMsgResponse = new GenericMsgResponse();

        genericMsgResponse.setStatus(Status.FAILED.name());

        genericMsgResponse.setMessage(ErrorCode.FAILED_MESSAGE);

        /**
         * if activeFlag is true then update existing record otherwise insert new record if current record not found
         * if activeFlg is false then soft delete the existing record.
         */
        if (creditVidyaDetails.isActiveFlag()) {
            applicationRepository.updateCreditVidyaDetails(creditVidyaDetails);
            genericMsgResponse.setStatus(Status.SUCCESS.name());
            genericMsgResponse.setMessage(ErrorCode.DATA_UPDATE_MESSAGE);

        } else if (applicationRepository.softDeleteInsuranceDetails(creditVidyaDetails.getRefId())) {
            genericMsgResponse.setStatus(Status.SUCCESS.name());
            genericMsgResponse.setMessage(ErrorCode.DATA_DELETE_MESSAGE);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
    }


    @Override
    public BaseResponse fetchInsuranceDetailsByRefId(String refId, String institutionId) {

        InsurancePremiumDetails insurancePremiumDetails = applicationRepository.fetchInsuranceDetails(refId, institutionId);
        BaseResponse baseResponse;

        if (null != insurancePremiumDetails) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse fetchCreditVidyaDetailsByRefId(String refId, String institutionId) {

        CreditVidyaDetails creditVidyaDetails = applicationRepository.fetchCreditVidyaDetails(refId, institutionId);
        BaseResponse baseResponse;

        if (null != creditVidyaDetails) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, creditVidyaDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse updateRevisedEmiAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest) {

        return GngUtils.getBaseResponse(HttpStatus.OK, applicationRepository.updateRevisedEmiAmount(updateRevisedEmiRequest));

    }

    @Override
    public BaseResponse updateGstDetails(GstDetailsRequest gstDetailsRequest) {
        BaseResponse baseResponse;
        Collection<Error> errors = applicationValidationEngine.validateGstRequest(gstDetailsRequest);

        if (org.apache.commons.collections.CollectionUtils.isEmpty(errors)) {

            if (applicationRepository.updateGstDetails(gstDetailsRequest)) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, true);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

        }

        return baseResponse;
    }

    @Override
    public BaseResponse submitCustomerFinancialProfile(final CustomerFinancialProfileRequest customerFinancialProfileRequest) {
        try {
            applicationRepository.submitCustomerFinancialProfile(customerFinancialProfileRequest);
            return GngUtils.getBaseResponse(HttpStatus.OK);
        } catch (Exception ex) {
            return BaseResponse.builder().status(com.softcell.gonogo.model.response.core.Status.builder()
                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .statusValue(HttpStatus.INTERNAL_SERVER_ERROR.name()).build())
                    .errors(GngUtils.getCouldNotProcessErrorList()).build();
        }
    }

    private ActivityLogs getActivityLog(PostIpaRequest applicationRequest, String stage) {

        ActivityLogs activityLog = auditHelper.createActivityLog(null, applicationRequest.getHeader());
        activityLog.setRefId(applicationRequest.getRefID());
        activityLog.setStage(stage);

        return activityLog;
    }

    @Override
    public BaseResponse generateRefNum(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse) throws Exception {
        logger.info("generateRefNum service started.");
        BaseResponse baseResponse;
        String institutionId = goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId();
        String refID = goNoGoCroApplicationResponse.getGngRefId();
        String productId = goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getProduct().toProductId();
        //Generate Agreement Num
        if (lookupService.checkActionsAccess(institutionId, productId, ActionName.GENERATE_AGGREMENTNUM)) {

            StateBranchMaster stateBranchMaster = masterRepository.getStateBranchMaster(
                    goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getBranchCode()
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());

            if (stateBranchMaster == null) {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.NO_STATE_DETAILS_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                logger.error("No State Details/StateCode found for Branch {} for Ref Id: {} "
                        , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getBranchCode()
                        , refID);
                return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, errors);
            }

            logger.info("State Code for Ref ID {} ", stateBranchMaster);

            String agreementNumber;
            if (StringUtils.isNotBlank(stateBranchMaster.getStateCode())) {
                agreementNumber = stateBranchMaster.getStateCode() + refID;
            } else {
                agreementNumber = refID;
            }
            goNoGoCroApplicationResponse.setAgreementNum(agreementNumber);
            logger.info("Agreement generated for Ref ID {}", agreementNumber);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, false);
        } else {
            logger.info("Action {} not defined/enabled for InstituionId {} & ProductId {} "
                    , ActionName.GENERATE_AGGREMENTNUM.name(), institutionId, productId);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
        }
        logger.info("generateRefNum service completed.");
        return baseResponse;
    }

    @Override
    public BaseResponse initializePostIpa(SchemeMetadataRequest schemeMetadataRequest) {
        logger.info("initializePostIpa service started.");
        InitializePostIPAResponse initializePostIPAResponse = new InitializePostIPAResponse();
        Double ltv = null;
        Collection<Error> errors;
        Map<String, Object> fieldMap = new HashMap<String, Object>();
        try {
            //Get application sober details.
            GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository
                    .getGoNoGoCustomerApplicationByRefId(schemeMetadataRequest.getRefID()
                            , schemeMetadataRequest.getHeader().getInstitutionId());
            if (goNoGoCroApplicationResponse != null
                    && goNoGoCroApplicationResponse.getApplicantComponentResponse() != null
                    && goNoGoCroApplicationResponse.getApplicantComponentResponse().getScoringServiceResponse() != null
                    && goNoGoCroApplicationResponse.getApplicantComponentResponse().getScoringServiceResponse().getEligibilityResponse() != null
                    && goNoGoCroApplicationResponse.getApplicantComponentResponse().getScoringServiceResponse().getEligibilityResponse().getAdditionalFields() != null) {
                fieldMap = goNoGoCroApplicationResponse
                        .getApplicantComponentResponse()
                        .getScoringServiceResponse()
                        .getEligibilityResponse()
                        .getAdditionalFields();
                if (!CollectionUtils.isEmpty(fieldMap)) {
                    if (StringUtils.isNotEmpty((String) fieldMap.get("LTV"))) {
                        ltv = Double.parseDouble((String) fieldMap.get("LTV"));
                        initializePostIPAResponse.setAdditionalFields(fieldMap);
                    }
                }
            }

            //Override the CRO LTV only if any cro justification is present then get the latest cro decision value form list.
            List<CroDecision> croDecisionList = goNoGoCroApplicationResponse.getCroDecisions();
            List<CroJustification> croJustificationList = goNoGoCroApplicationResponse.getCroJustification();

            if (!CollectionUtils.isEmpty(croDecisionList) && !CollectionUtils.isEmpty(croJustificationList)) {

                ltv = croDecisionList.get(croDecisionList.size() - 1).getLtv();

                fieldMap.put("LTV", ltv);
                initializePostIPAResponse.setAdditionalFields(fieldMap);
            }

            if (ltv == null || initializePostIPAResponse.getAdditionalFields() == null) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.ERR_APP_NTFND_SCORES_NTFND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                logger.error(ErrorCode.ERR_APP_NTFND_SCORES_NTFND);
                return GngUtils.getBaseResponse(HttpStatus.PRECONDITION_REQUIRED, errors);
            }

            //Get Scheme Master data.
            BaseResponse schemeDataResponse = masterDataViewManager.getFilteredSchemes(schemeMetadataRequest);

            if (schemeDataResponse != null && schemeDataResponse.getPayload() != null) {
                List<SchemeMasterData> schemeMasterList = (List<SchemeMasterData>) schemeDataResponse.getPayload().getT();
                if (!CollectionUtils.isEmpty(schemeMasterList)) {
                    initializePostIPAResponse.setSchemeMasters(schemeMasterList);
                } else {
                    logger.error(ErrorCode.SCHEMEMASTERDATA_NOT_FOUND);
                    errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.SCHEMEMASTERDATA_NOT_FOUND)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    return GngUtils.getBaseResponse(HttpStatus.PRECONDITION_REQUIRED, errors);
                }
            } else {
                logger.error(ErrorCode.SCHEMEMASTERDATA_NOT_FOUND);
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.SCHEMEMASTERDATA_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                return GngUtils.getBaseResponse(HttpStatus.PRECONDITION_REQUIRED, errors);
            }
            return GngUtils.getBaseResponse(HttpStatus.OK, initializePostIPAResponse);
        } catch (Exception e) {
            logger.error(String.format(ErrorCode.ERR_INIT_POST_IPA, e.getMessage()));
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.ERR_INIT_POST_IPA)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.PRECONDITION_REQUIRED, errors);
        }
    }

    @Override
    public BaseResponse getInsurancePremium(GetInsurancePremiumRequest getInsurancePremiumRequest) {
        {

            logger.debug("getInsurance premimum service invoked ");

            BaseResponse baseResponse;
            String institutionId = getInsurancePremiumRequest.getHeader().getInstitutionId();
            String productName = getInsurancePremiumRequest.getHeader().getProduct().name();
            InsurancePremiumResponse insurancePremiumResponse = new InsurancePremiumResponse();

            String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

            if (StringUtils.isNotBlank(productAliasName)) {


                InsurancePremiumMaster insurancePremiumMaster = applicationRepository.getInsurancePremium(institutionId, productAliasName);

                if (null != insurancePremiumMaster) {

                    String srNoOrImeiNo = getSerialNumberInfo(getInsurancePremiumRequest.getRefId());

                    if (StringUtils.isNotBlank(srNoOrImeiNo)) {
                        insurancePremiumResponse.setSerialOrImeiNumber(srNoOrImeiNo);
                    }

                    insurancePremiumResponse.setStatus(Status.SUCCESS.name());
                    insurancePremiumResponse.setResMessage("Insurance Premium Data Found Successfully");
                    insurancePremiumResponse.setInsurancePremiumMaster(insurancePremiumMaster);

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumResponse);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                }

            } else {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
            }
            return baseResponse;
        }
    }

    @Override
    public BaseResponse getInsurancePremiumAgainstProvider(GetInsurancePremiumRequest getInsurancePremiumRequest) {

        logger.debug("getInsurancePremiumAgainstProvider service invoked ");
        BaseResponse baseResponse;

        InsurancePremiumMaster insurancePremiumMaster = applicationRepository.getInsurancePremiumAgainstProvider(getInsurancePremiumRequest.getHeader().getInstitutionId()
                , getInsurancePremiumRequest.getInsuranceType().toString());

        if (null != insurancePremiumMaster) {
            logger.info("Insurance premium data found successfully against provider", getInsurancePremiumRequest.getInsuranceType());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insurancePremiumMaster);
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse updateAddonServiceRevisedAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest) {

        return GngUtils.getBaseResponse(HttpStatus.OK, applicationRepository.updateAddonServiceRevisedAmount(updateRevisedEmiRequest));

    }

    @Override
    public BaseResponse checkPinCodeGeographicalLimit(GeoLimitRequest geoLimitRequest) {

        logger.info("Inside check pincode geographical limit");

        GeoLimitResponse geoLimitResponse = new GeoLimitResponse();
        geoLimitResponse.setStatus(Status.SUCCESS.name());

        try {

            if (geoLimitRequest.getApplicantType() == ApplicantType.EXPRESS) {

                if (StringUtils.equals(geoLimitRequest.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())) {

                    String institutionId = geoLimitRequest.getHeader().getInstitutionId();
                    String dealerId = geoLimitRequest.getHeader().getDealerId();

                    // get branchId from dealerBranchMaster against the dealerId
                    String branchId = applicationRepository.getBranchIdAgainstDealerId(institutionId, dealerId);

                    logger.debug("Branch Id {} found against dealerId {}",branchId ,dealerId);

                        if (applicationRepository.checkGeoLimitApplicableForBranch(geoLimitRequest ,branchId)) {
                            geoLimitResponse.setMessage(ErrorCode.PIN_FND_IN_GEO_LMT);

                        } else {
                            geoLimitResponse.setStatus(Status.FAILED.name());
                            geoLimitResponse.setMessage(ErrorCode.PIN_NT_FND_IN_GEO_LMT);
                        }
                    } else {
                        logger.debug(String.format(ErrorCode.GEOLMT_NT_APPICBL_ADDR_TPE, geoLimitRequest.getAddressType()));
                         geoLimitResponse.setMessage(String.format(ErrorCode.GEOLMT_NT_APPICBL_ADDR_TPE, geoLimitRequest.getAddressType()));

                    }

                } else {
                    logger.debug(String.format(ErrorCode.GEOLMT_NT_APPICBL_APLCNT_TPE, geoLimitRequest.getApplicantType().name()));
                    geoLimitResponse.setMessage(String.format(ErrorCode.GEOLMT_NT_APPICBL_APLCNT_TPE, geoLimitRequest.getApplicantType().name()));

                }

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Problem occured at the time of checking pincode geo limit with probable cause {}",e.getMessage());
            geoLimitResponse.setStatus(Status.FAILED.name());
            geoLimitResponse.setMessage(ErrorCode.SOMETHING_WENT_WRG);

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, geoLimitResponse);
    }
}

