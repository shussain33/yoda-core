package com.softcell.service.impl;


import com.mongodb.BasicDBObject;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.dao.mongodb.repository.AnalyticsMongoRepository;
import com.softcell.dao.mongodb.repository.HierarchyRepository;
import com.softcell.gonogo.model.analytics.*;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.reporting.builder.ReportMongoPipeLines;
import com.softcell.service.AnalyticsManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;


@Service
public class AnalyticsManagerImpl implements AnalyticsManager {

    private final static Logger logger = LoggerFactory.getLogger(AnalyticsManagerImpl.class);

    @Autowired
    public AnalyticsMongoRepository analyticsMongoRepository;

    public BaseResponse getLoginStatusGraphData(StackRequest stackRequest)
            throws Exception {

        Assert.notNull(stackRequest,
                "Stack request must not be null or blank !!");

        Aggregation aggregation = QueryBuilder
                .buildLoginStatusGraphQuery(stackRequest);


        AggregationResults<BasicDBObject> aggregationResult = analyticsMongoRepository
                .fetchLoginStatusGraphData(aggregation,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString());

        Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
                put("APPROVED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("DECLINED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("QUEUE",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("ONHOLD",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("DEDUPEQUEUE",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("CANCELLED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
            }
        };

        List<BasicDBObject> mappedResults = aggregationResult.getMappedResults();

        for (BasicDBObject basicDBObject : mappedResults) {

            Integer count = basicDBObject.getInt("count");
            String date = basicDBObject.getString("date");
            String status = basicDBObject.getString("applicationStatus");

            if (map.containsKey(status)) {
                Map<String, Integer> map2 = map.get(status);
                if (map2.containsKey(date)) {
                    map2.put(date, count);
                }
            }

        }

        List<StackGraph> stackGraphs = new ArrayList<StackGraph>();
        for (String key : map.keySet()) {
            StackGraph graph = new StackGraph();
            graph.setName(key);
            Map<String, Integer> map2 = map.get(key);
            graph.setX(map2.keySet());
            graph.setY(map2.values());
            stackGraphs.add(graph);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, stackGraphs);
    }

    public BaseResponse getLoginStatusTableData(StackRequest stackRequest) throws Exception {

        Assert.notNull(stackRequest, "stack request must not be null or blank !! ");

        Aggregation buildAggregrationForLoginByStatusTabular = QueryBuilder
                .buildAggregrationForLoginByStatusTabular(stackRequest, false);

        LoginStatusTable loginStatusTable = new LoginStatusTable();

        List<StackTable> responseList = new ArrayList<>();

        AggregationResults<BasicDBObject> fetchLoginStatusTableData = analyticsMongoRepository
                .fetchLoginStatusTableData(buildAggregrationForLoginByStatusTabular,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString());

        fetchLoginStatusTableData.iterator().forEachRemaining(basicDBObject -> {

            responseList.add(StackTable.builder()
                    .applicationId(basicDBObject.getString("_id", ""))
                    .applicationStatus(basicDBObject.getString("applicationStatus", ""))
                    .dealerId(basicDBObject.getString("dealerId", ""))
                    .loanAmount(basicDBObject.getDouble("loanAmount", 0D))
                    .stageID(basicDBObject.getString("stage", ""))
                    .applicantName(basicDBObject.getString("applicantName", ""))
                    .dealerId(basicDBObject.getString("dealerId", ""))
                    .date(basicDBObject.getDate("applicationDate"))
                    .approvedAmount(basicDBObject.getDouble("amountApproved", 0D))
                    .dsaId(basicDBObject.getString("dsaId", ""))
                    .bureauScore(basicDBObject.getString("bureauScore", ""))
                    .applicationScore(basicDBObject.getString("appScore", ""))
                    .branch(basicDBObject.get("city").toString()).build()
            );

        });

        loginStatusTable.setStackTable(responseList);

        Aggregation countAggregation = QueryBuilder
                .buildAggregrationForLoginByStatusTabular(stackRequest, true);

        AggregationResults<BasicDBObject> fetchLoginStatusTableData2 = analyticsMongoRepository
                .fetchLoginStatusTableData(countAggregation,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString());

        for (BasicDBObject basicDBObject : fetchLoginStatusTableData2) {
            loginStatusTable.setTotalCount(basicDBObject.getInt("count"));
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, loginStatusTable);
    }


    public BaseResponse getLoginStatusTableDataForDtc(StackRequest stackRequest) throws Exception {

        Assert.notNull(stackRequest, "stack request must not be null or blank !! ");

        Aggregation buildAggregrationForLoginByStatusTabular = QueryBuilder
                .buildAggregrationForLoginByStatusTabularForDtc(stackRequest, false);

        LoginStatusTable loginStatusTable = new LoginStatusTable();

        List<StackTable> responseList = new ArrayList<>();

        AggregationResults<BasicDBObject> fetchLoginStatusTableData = analyticsMongoRepository
                .fetchLoginStatusTableData(
                        buildAggregrationForLoginByStatusTabular,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString());

        for (BasicDBObject object : fetchLoginStatusTableData) {
            StackTable stackTable = new StackTable();
            stackTable.setApplicationId(object.getString("_id", ""));
            stackTable.setApplicationStatus(object.getString(
                    "applicationStatus", ""));
            stackTable.setDealerId(object.getString("dealerId", ""));
            stackTable.setLoanAmount(object.getDouble("loanAmount", 0D));
            stackTable.setStageID(object.getString("stage", ""));
            stackTable.setApplicantName(object.getString("applicantName", ""));
            stackTable.setDealerId(object.getString("dealerName", ""));
            stackTable.setDate(object.getDate("applicationDate"));
            stackTable
                    .setApprovedAmount(object.getDouble("amountApproved", 0D));
            stackTable.setDsaId(object.getString("dsaName", ""));
            stackTable.setBranch(object.getString("branch", ""));
            stackTable.setBureauScore(object.getString("bureauScore", ""));
            stackTable.setApplicationScore(object.getString("appScore", ""));

            responseList.add(stackTable);

        }

        loginStatusTable.setStackTable(responseList);

        Aggregation countAggregation = QueryBuilder
                .buildAggregrationForLoginByStatusTabularForDtc(stackRequest, true);

        logger.info("AMI service aggregation result count [{}]", countAggregation);

        AggregationResults<BasicDBObject> fetchLoginStatusTableData2 = analyticsMongoRepository
                .fetchLoginStatusTableData(countAggregation,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString());

        for (BasicDBObject basicDBObject : fetchLoginStatusTableData2) {
            loginStatusTable.setTotalCount(basicDBObject.getInt("count"));
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, loginStatusTable);
    }

    @Override
    public BaseResponse generateLoginByStagesReport(
            StackRequest stackRequest) throws Exception {

        Aggregation buildAggregationForloginByStage = ReportMongoPipeLines
                .buildAggregationForloginByStage(stackRequest);

        AggregationResults<BasicDBObject> stageWiseLoginReport = analyticsMongoRepository
                .getStageWiseLoginReport(buildAggregationForloginByStage);

        Map<String, Map<String, Integer>> map = new LinkedHashMap<String, Map<String, Integer>>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {

                put("DISBURSED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("OPS QUEUE",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));

                put("APPROVED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("DECLINED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));

                put("NO DECISION",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));

            }
        };

        for (BasicDBObject basicDBObject : stageWiseLoginReport) {

            String stage = basicDBObject.getString("stage");
            String date = basicDBObject.getString("date");
            Integer count = basicDBObject.getInt("count");

            if (map.containsKey(stage)) {
                Map<String, Integer> map2 = map.get(stage);
                if (map2.containsKey(date)) {
                    map2.put(date, count);
                }
            }

        }

        List<StackGraph> stackGraphs = new ArrayList<StackGraph>();
        for (String key : map.keySet()) {
            StackGraph graph = new StackGraph();
            graph.setName(key);
            Map<String, Integer> map2 = map.get(key);
            graph.setX(map2.keySet());
            graph.setY(map2.values());
            stackGraphs.add(graph);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, stackGraphs);
    }

    @Override
    public BaseResponse generateAssetManufacturerReport(
            StackRequest stackRequest) throws Exception {

        Aggregation buildAggregationForAssetManufacture = ReportMongoPipeLines
                .assetManufacturerAggregationBuilder(stackRequest);

        AggregationResults<BasicDBObject> assetManufactureReport = analyticsMongoRepository
                .getAssetManufactureReport(buildAggregationForAssetManufacture);

        Aggregation buildAggregatorForUniqueManufacturers = ReportMongoPipeLines
                .buildAggregatorForTopManufacturers(stackRequest);

        AggregationResults<BasicDBObject> uniqueAssetManufacturers = analyticsMongoRepository
                .getUniqueAssetManufacturers(buildAggregatorForUniqueManufacturers);

        Map<String, Map<String, Integer>> map = new LinkedHashMap<String, Map<String, Integer>>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {

                put("TELEVISION",
                        getUniqueManufactureMap(uniqueAssetManufacturers));
                put("AIR CONDITIONER",
                        getUniqueManufactureMap(uniqueAssetManufacturers));
                put("REFRIGERATORS",
                        getUniqueManufactureMap(uniqueAssetManufacturers));
                put("MICROWAVE",
                        getUniqueManufactureMap(uniqueAssetManufacturers));
                put("WASHING MACHINE",
                        getUniqueManufactureMap(uniqueAssetManufacturers));
                put("OTHERS", getUniqueManufactureMap(uniqueAssetManufacturers));
            }
        };

        for (BasicDBObject basicDBObject : assetManufactureReport) {

            Integer count = basicDBObject.getInt("count");
            String assetCtg = basicDBObject.getString("assetCtg");
            String assetMake = basicDBObject.getString("assetMake");

            if (map.containsKey(assetCtg)) {
                Map<String, Integer> map2 = map.get(assetCtg);
                if (map2.containsKey(assetMake)) {
                    map2.put(assetMake, count);
                }
            }

        }

        List<StackGraph> stackGraphs = new ArrayList<StackGraph>();
        for (String key : map.keySet()) {
            StackGraph graph = new StackGraph();
            graph.setName(key);
            Map<String, Integer> map2 = map.get(key);
            graph.setX(map2.keySet());
            graph.setY(map2.values());
            stackGraphs.add(graph);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, stackGraphs);
    }

    @Override
    public BaseResponse getLoginByTopDealer(StackRequest stackRequest) throws Exception {

        Aggregation buildAggregratorForLoginByDealers = ReportMongoPipeLines
                .buildAggregratorForLoginByDealers(stackRequest);

        AggregationResults<BasicDBObject> loginDealersWise = analyticsMongoRepository
                .getLoginDealersWise(buildAggregratorForLoginByDealers);

        //FIXME: null pointer check for loginDealersWise
        List<BasicDBObject> mappedResults2 = loginDealersWise.getMappedResults();

        Map<String, Map<String, Integer>> map = new LinkedHashMap<String, Map<String, Integer>>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {

                for (BasicDBObject basicDBObject : mappedResults2) {
                    String dealerName = basicDBObject.getString("supplierDesc");
                    String date = basicDBObject.getString("date");
                    Integer count = basicDBObject.getInt("count");
                    if (!this.containsKey(dealerName)) {
                        put(dealerName, GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                    } else {
                        Map<String, Integer> map2 = this.get(dealerName);
                        if (map2.containsKey(date)) {
                            map2.put(date, count);
                        }
                    }

                }
            }
        };

        List<StackGraph> stackGraphs = new ArrayList<StackGraph>();
        int limit = 10;
        int count = 0;
        for (Entry<String, Map<String, Integer>> entrySet : map.entrySet()) {
            if (count >= limit) {
                break;
            } else if (map.size() == count) {
                break;
            } else {
                StackGraph graph = new StackGraph();
                graph.setName(entrySet.getKey());
                Map<String, Integer> map2 = entrySet.getValue();
                graph.setX(map2.keySet());
                graph.setY(map2.values());
                stackGraphs.add(graph);
                count++;

            }

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, stackGraphs);

    }

    private Map<String, Integer> getUniqueManufactureMap(
            AggregationResults<BasicDBObject> uniqueAssetManufacturers) {

        Map<String, Integer> innerMap = new LinkedHashMap<String, Integer>();

        for (BasicDBObject basicDBObject : uniqueAssetManufacturers) {
            String manufacturerDesc = basicDBObject.getString("_id");
            innerMap.put(manufacturerDesc, 0);
        }

        return innerMap;
    }

    @Override
    public BaseResponse getDsaReport() throws Exception {

        try {

            List<GoNoGoCroApplicationResponse> dsaData = analyticsMongoRepository.getDsaReport();

            List<DsaReport> dsaReports = dsaData.parallelStream().map(dsa -> {
                DsaReport dsaReport = new DsaReport();
                if (dsa != null) {
                    dsaReport.setApplicationStatus(dsa.getApplicationStatus());

                    dsaReport.setDate(dsa.getDateTime());

                    if (dsa.getApplicationRequest() != null
                            && dsa.getApplicationRequest().getRequest() != null
                            && dsa.getApplicationRequest().getRequest().getApplication() != null) {

                        dsaReport.setAppliedAmount(dsa.getApplicationRequest()
                                .getRequest()
                                .getApplication()
                                .getLoanAmount());
                    }

                    if (StringUtils.equalsIgnoreCase(
                            dsaReport.getApplicationStatus(), GNGWorkflowConstant.APPROVED.toFaceValue())) {

                        if (dsa.getCroDecisions() != null
                                && !CollectionUtils.isEmpty(dsa.getCroDecisions())) {
                            dsaReport.setApprovedAmount(dsa.getCroDecisions()
                                    .get(0).getAmtApproved());
                            dsaReport.setEligibleAmount(dsa.getCroDecisions()
                                    .get(0).getEligibleAmt());
                        }
                    }
                }
                return dsaReport;
            }).collect(Collectors.toList());

            return GngUtils.getBaseResponse(HttpStatus.OK, dsaReports);

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

    }

    @Override
    public BaseResponse getStackScoreData(StackRequest stackRequest) throws Exception {

        String refrenceId = stackRequest.getReferenceID();
        String institutionId = stackRequest.getHeader().getInstitutionId();
        RequestCriteria requestCriteria = stackRequest.getCriteria();

        Query query = new Query();

        query.fields().include(
                "applicantComponentResponse.scoringServiceResponse.scoreTree");

        query.addCriteria(Criteria
                .where("applicationRequest.header.institutionId")
                .is(institutionId).and("_id").is(refrenceId));

        HierarchyRepository hierarchyUtils = new HierarchyRepository();

        Criteria hierarchyCriteria = hierarchyUtils.applyHierarchyQuery(requestCriteria);

        if (null != hierarchyCriteria) {
            query.addCriteria(hierarchyCriteria);
        }

        GoNoGoCustomerApplication response = analyticsMongoRepository
                .fetchStackScoreData(query, stackRequest);

        if (null != response
            && null != response.getApplicantComponentResponse()
            && null != response.getApplicantComponentResponse()
                               .getScoringServiceResponse()) {

            return GngUtils.getBaseResponse(HttpStatus.OK, response.getApplicantComponentResponse()
                    .getScoringServiceResponse().getScoreTree());
        } else {

            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }


    }

    @Override
    public BaseResponse getRoleBasedDistinctBranches(
            StackRequest stackRequest) throws Exception {

        Collection<String> branches;

        Query query = QueryBuilder.buildAggregationForRoleBasedFilter(stackRequest);

        branches = analyticsMongoRepository
                .getRoleBasedDistinctFields(
                        query,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString(), "applicationRequest.request.applicant.address.city");

        branches.removeAll(Arrays.asList(FieldSeparator.SPACE_STR , FieldSeparator.BLANK));

        return GngUtils.getBaseResponse(HttpStatus.OK, branches);
    }

    @Override
    public BaseResponse getRoleBasedOnDistinctDealers(StackRequest stackRequest) throws Exception {

        Collection<String> dealers;

        Query query = QueryBuilder.buildAggregationForRoleBasedFilter(stackRequest);

        dealers = analyticsMongoRepository.getRoleBasedDistinctFields(
                query,
                MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(), "applicationRequest.header.dealerId");

        dealers.removeAll(Arrays.asList(FieldSeparator.SPACE_STR , FieldSeparator.BLANK));

        return GngUtils.getBaseResponse(HttpStatus.OK, dealers);

    }

    @Override
    public BaseResponse getRoleBasedDistinctStages(StackRequest stackRequest) throws Exception {

        Collection<String > stages;


        Query query = QueryBuilder.buildAggregationForRoleBasedFilter(stackRequest);

        stages =  analyticsMongoRepository.getRoleBasedDistinctFields(
                query,
                MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION.toString(), "applicationRequest.currentStageId");

        BaseResponse baseResponse;

        if(!CollectionUtils.isEmpty(stages)){
            stages.removeAll(Arrays.asList(FieldSeparator.SPACE_STR , FieldSeparator.BLANK));
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, stages);

        }else{

            logger.debug(" no content found while searching for distinct stages with query [{}]", query);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }


        return baseResponse;


    }

    @Override
    public BaseResponse getRoleBasedOnDistinctDSA(StackRequest stackRequest) throws Exception {

        Collection<String> dsas;

        Query query = QueryBuilder.buildAggregationForRoleBasedFilter(stackRequest);


        dsas = analyticsMongoRepository
                .getRoleBasedDistinctFields(query,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString(), "applicationRequest.header.dsaId");

        dsas.removeAll(Arrays.asList(FieldSeparator.SPACE_STR , FieldSeparator.BLANK));

        return GngUtils.getBaseResponse(HttpStatus.OK, dsas);

    }

    @Override
    public BaseResponse getLoginStatusGraphDataOld(StackRequest stackRequest)
            throws Exception {


        Assert.notNull(stackRequest,
                "Stack request must not be null or blank !!");

        if (StringUtils.isBlank(stackRequest.getFromDate()) && StringUtils.isBlank(stackRequest.getToDate())) {

            stackRequest.setFromDate(new DateTime().minusDays(30).withHourOfDay(00).toString("yyyy-MM-dd"));
            stackRequest.setToDate(new DateTime().withHourOfDay(23).toString("yyyy-MM-dd"));

        }

        Aggregation aggregation = QueryBuilder
                .buildLoginStatusGraphQueryOld(stackRequest);


        AggregationResults<BasicDBObject> aggregationResult = analyticsMongoRepository
                .fetchLoginStatusGraphData(aggregation,
                        MongoConfig.COLLECTIONS.GONOGO_CUSTOMER_APPLICATION
                                .toString());

        Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
                put("APPROVED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("DECLINED",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
                put("QUEUE",
                        GngDateUtil.getTimeSeriesMap(
                                stackRequest.getFromDate(),
                                stackRequest.getToDate()));
            }
        };

        List<BasicDBObject> mappedResults = aggregationResult
                .getMappedResults();

        for (BasicDBObject basicDBObject : mappedResults) {

            Integer count = basicDBObject.getInt("count");
            String date = basicDBObject.getString("date");
            String status = basicDBObject.getString("status");

            if (map.containsKey(status)) {
                Map<String, Integer> map2 = map.get(status);
                if (map2.containsKey(date)) {
                    map2.put(date, count);
                }
            }

        }

        List<StackGraph> stackGraphs = new ArrayList<StackGraph>();
        for (String key : map.keySet()) {
            StackGraph graph = new StackGraph();
            graph.setName(key);
            Map<String, Integer> map2 = map.get(key);
            graph.setX(map2.keySet());
            graph.setY(map2.values());
            stackGraphs.add(graph);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, stackGraphs);

    }

}
