package com.softcell.service.impl;

import com.softcell.constants.CustomHttpStatus;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.loyaltycard.LoyaltyCardRepository;
import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.masters.LoyaltyCardMaster;
import com.softcell.gonogo.model.request.loyaltycard.LoyaltyCardStatusRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.UpdateLoyaltyCardDetailsResponse;
import com.softcell.gonogo.model.response.loyaltycard.LoyaltyCardStatusResponse;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.service.LoyaltyCardManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Set;

/**
 * Created by mahesh on 12/12/17.
 */
@Service
public class LoyaltyCardManagerImpl implements LoyaltyCardManager {

    private static final Logger logger = LoggerFactory.getLogger(LoyaltyCardManagerImpl.class);

    @Autowired
    private LoyaltyCardRepository loyaltyCardRepository;

    @Autowired
    private LookupService lookupService;


    @Override
    public BaseResponse checkLoyaltyCardStatus(LoyaltyCardStatusRequest loyaltyCardStatusRequest) {

        /**
         * check loyalty card status in the master whether it is Lost or Block
         */
        LoyaltyCardMaster loyaltyCardMaster = loyaltyCardRepository.checkLoyaltyCardStatus(loyaltyCardStatusRequest);

        LoyaltyCardStatusResponse loyaltyCardStatusResponse = new LoyaltyCardStatusResponse();

        if (null != loyaltyCardMaster) {

            loyaltyCardStatusResponse.setLoyaltyCardNo(loyaltyCardMaster.getLoyaltyCardNo());

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Loyalty Card Status is ");
            stringBuilder.append(loyaltyCardMaster.getCardStatus());
            loyaltyCardStatusResponse.setLoyaltyCardStatus(stringBuilder.toString());
            loyaltyCardStatusResponse.setStatus(Status.FAILED.name());

            /**
             * check loyalty card in the database if found then provide failed response.
             */
        } else if (loyaltyCardRepository.checkLoyaltyCardInDedupe(loyaltyCardStatusRequest)) {

            loyaltyCardStatusResponse.setLoyaltyCardNo(loyaltyCardStatusRequest.getLoyaltyCardNo());
            loyaltyCardStatusResponse.setLoyaltyCardStatus(ErrorCode.LOYALTY_CARD_DEDUPE_MSG);
            loyaltyCardStatusResponse.setStatus(Status.FAILED.name());

        } else {

            loyaltyCardStatusResponse.setLoyaltyCardNo(loyaltyCardStatusRequest.getLoyaltyCardNo());

            LoyaltyCardType loyaltyCardType;

            if (null == loyaltyCardStatusRequest.getLoyaltyCardType()) {
                loyaltyCardType = LoyaltyCardType.HDBFS;
            } else {
                loyaltyCardType = loyaltyCardStatusRequest.getLoyaltyCardType();
            }
            double loyaltyCardPrice = lookupService.getLoyaltyCardPrice(loyaltyCardStatusRequest.getHeader().getInstitutionId(),
                    loyaltyCardStatusRequest.getHeader().getProduct().toProductId() ,loyaltyCardType);

            if (loyaltyCardPrice != 0) {
                loyaltyCardStatusResponse.setLoyaltyCardPrice(loyaltyCardPrice);
                loyaltyCardStatusResponse.setStatus(Status.SUCCESS.name());

            } else {
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }


        }
        return GngUtils.getBaseResponse(HttpStatus.OK, loyaltyCardStatusResponse);

    }

    @Override
    public BaseResponse updateLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails) {

        UpdateLoyaltyCardDetailsResponse updateLoyaltyCardDetailsResponse = new UpdateLoyaltyCardDetailsResponse();

        updateLoyaltyCardDetailsResponse.setStatus(Status.FAILED.name());

        updateLoyaltyCardDetailsResponse.setResponseMsg(ErrorCode.FAILED_MESSAGE);


        /**
         * if activeFlag is true then update existing record otherwise insert new record if current record not found
         * if activeFlg is false then soft delete the existing record
         */
        if (loyaltyCardDetails.isActiveFlag()) {

            // first check loyalty card status ie .it is bloked ,lost or deduped

            LoyaltyCardStatusRequest loyaltyCardStatusRequest = buildLoyaltyCardStatusRequest(loyaltyCardDetails);

            BaseResponse baseResponse = checkLoyaltyCardStatus(loyaltyCardStatusRequest);

            LoyaltyCardStatusResponse loyaltyCardStatusResponse = GngUtils.convertBaseResponsePayload(baseResponse, LoyaltyCardStatusResponse.class);

            if (null == loyaltyCardStatusResponse) {

                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());

            } else if (null != loyaltyCardStatusResponse && StringUtils.equals(loyaltyCardStatusResponse.getStatus(), Status.FAILED.name())) {

                updateLoyaltyCardDetailsResponse.setStatus(loyaltyCardStatusResponse.getStatus());
                updateLoyaltyCardDetailsResponse.setResponseMsg(loyaltyCardStatusResponse.getLoyaltyCardStatus());

            } else {
                loyaltyCardRepository.updateLoyaltyCardDetails(loyaltyCardDetails);
                updateLoyaltyCardDetailsResponse.setStatus(Status.SUCCESS.name());
                updateLoyaltyCardDetailsResponse.setResponseMsg(ErrorCode.DATA_SAVE_MESSAGE);
            }

        } else if (loyaltyCardRepository.softDeleteLoyaltyCardDetails(loyaltyCardDetails)) {
            updateLoyaltyCardDetailsResponse.setStatus(Status.SUCCESS.name());
            updateLoyaltyCardDetailsResponse.setResponseMsg(ErrorCode.DATA_DELETE_MESSAGE);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, updateLoyaltyCardDetailsResponse);
    }

    private LoyaltyCardStatusRequest buildLoyaltyCardStatusRequest(LoyaltyCardDetails loyaltyCardDetails) {

        LoyaltyCardStatusRequest loyaltyCardStatusRequest = new LoyaltyCardStatusRequest();

        loyaltyCardStatusRequest.setHeader(loyaltyCardDetails.getHeader());
        loyaltyCardStatusRequest.setLoyaltyCardNo(loyaltyCardDetails.getLoyaltyCardNo());
        loyaltyCardStatusRequest.setRefId(loyaltyCardDetails.getRefId());
        loyaltyCardStatusRequest.setLoyaltyCardType(loyaltyCardDetails.getLoyaltyCardType());

        return loyaltyCardStatusRequest;

    }

    @Override
    public BaseResponse fetchLoyaltyCardDetails(String refId, String institutionId) {
        LoyaltyCardDetails loyaltyCardDetails = loyaltyCardRepository.fetchLoyaltyCardDetails(refId, institutionId);
        BaseResponse baseResponse;

        if (null != loyaltyCardDetails) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loyaltyCardDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getApplicableLoyaltyCardProviders(String dealerId, String institutionId) throws Exception {
        BaseResponse baseResponse;
        Set<String> applLoyaltyCardProviders = loyaltyCardRepository.getApplicableLoyaltyCardProviders(dealerId, institutionId);

        if (!CollectionUtils.isEmpty(applLoyaltyCardProviders)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applLoyaltyCardProviders);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }
}
