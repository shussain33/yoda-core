package com.softcell.service.impl;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.config.watermark.WaterMarkDetails;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ReportingRepository;
import com.softcell.dao.mongodb.repository.digitization.DigitizationRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.digitization.*;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.ops.SanctionConditions;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.digitization.DigitizationApplicationReportRequest;
import com.softcell.gonogo.model.request.digitization.DownloadDigitizationFormRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.master.commongeneralmaster.SourcingDetails;
import com.softcell.gonogo.model.surrogate.CarSurrogate;
import com.softcell.gonogo.model.surrogate.CreditCardSurrogate;
import com.softcell.gonogo.model.surrogate.DebitCardSurrogate;
import com.softcell.gonogo.model.surrogate.Surrogate;
import com.softcell.gonogo.service.factory.DMSBuilder;
import com.softcell.gonogo.service.factory.DigitizationBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.factory.TemplateFactory;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.service.DigitizationManager;
import com.softcell.service.DigitizationStoreManager;
import com.softcell.service.DocumentStoreManager;
import com.softcell.service.docs.HtmlToPdfConverter;
import com.softcell.service.thirdparty.utils.PolicyHelper;
import com.softcell.service.thirdparty.utils.ReligareHelper;
import com.softcell.utils.DMSUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngNumUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */
@Service
public class DigitizationManagerImpl implements DigitizationManager {

    private Logger logger = LoggerFactory
            .getLogger(DigitizationManagerImpl.class);

    @Autowired
    private DigitizationRepository digitizationRepository;

    @Autowired
    private DigitizationBuilder digitizationBuilder;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ReportingRepository reportingRepository;

    @Autowired
    private MasterDataViewRepository masterDataViewRepository;

    @Autowired
    private DigitizationStoreManager digitizationStoreManager;

    @Autowired
    private DocumentStoreManager documentStoreManager;

    @Autowired
    private DMSBuilder dmsBuilder;

    @Autowired
    private AppConfigurationHelper appConfigurationHelper;


    @Override
    public BaseResponse getApplicationForm(String institutionId, String productId, String refID) throws Exception {

        Document document = new Document();
        TemplateConfiguration templateConfiguration = null;

        boolean actionFlag = lookupService.checkActionsAccess(institutionId, productId, ActionName.DIGITIZATION);

        if (actionFlag) {

            templateConfiguration = lookupService.getTemplate(institutionId, productId, TemplateName.APPLICATION_FORM);

            if (null == templateConfiguration) {
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }

        } else {
            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, GngUtils.getNonAuthoritativeInformationErrorList());
        }


        GoNoGoCroApplicationResponse application = digitizationRepository
                .getApplication(institutionId, refID);

        PostIPA postIpa = null;
        String base64String = null;
        if (null != application) {
            postIpa = digitizationRepository.getPostIpa(institutionId,
                    refID);
            if (null != postIpa) {

                ApplicationForm applicationForm = populateDigitizationFormFields(application,
                        postIpa, templateConfiguration);

                String applicationFormHtmlDocument = getApplicationFormHtmlDocument(applicationForm, templateConfiguration);

                byte[] bytes = HtmlToPdfConverter.generatePdfByteArray(applicationFormHtmlDocument, null, null);
                if (null != bytes) {
                    base64String = new String(Base64.encodeBase64(bytes));
                }

            }
        }

        if (null != application && StringUtils.isNotBlank(base64String)) {
            document.setByteCode(base64String);
            document.setDocID("AF01");
            document.setDocName(TemplateName.APPLICATION_FORM.name());
            document.setStatus(Constant.SUCCESS);
            return GngUtils.getBaseResponse(HttpStatus.OK, document);
        }


        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

    }

    @Override
    public BaseResponse getDeliveryOrder(String institutionId, String productId, String refID, String fileOperationType) throws Exception {

        DeliveryOrderReport deliveryOrderReport = null;
        Document document = new Document();
        TemplateConfiguration templateConfiguration = null;

        // check action
        boolean actionFlag = lookupService.checkActionsAccess(institutionId, productId, ActionName.DIGITIZATION);

        if (actionFlag) {

            // check configuration
            templateConfiguration = lookupService.getTemplate(institutionId, productId, TemplateName.DELIVERY_ORDER);

            if (null == templateConfiguration) {
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }
        } else {

            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, GngUtils.getNonAuthoritativeInformationErrorList());
        }

        PostIpaRequest postIpaRequest = digitizationRepository.getPostIpaRequest(institutionId, refID);
        InsurancePremiumDetails insurancePremiumDetails = applicationRepository.fetchInsuranceDetails(refID, institutionId);
        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(refID, institutionId);

        if (null != postIpaRequest && null != postIpaRequest.getPostIPA()) {
            if (insurancePremiumDetails!=null&&extendedWarrantyDetails!=null){
                postIpaRequest.getPostIPA().setSumExtWrntAmtInsuranceAmt(insurancePremiumDetails.getInsuranceDPAmt()+extendedWarrantyDetails.getEwDPAmt());
            }
            DealerEmailMaster dealerEmailMaster = applicationRepository
                    .getDealerEmailMaster(postIpaRequest.getHeader());

            String itNum = null != dealerEmailMaster ? dealerEmailMaster.getItNum() : "";


            if (StringUtils.equals(Status.N.name(), itNum) && postIpaRequest.getPostIPA().getDealerSubvention() > 0) {

                postIpaRequest.getPostIPA().setDealerSubvention(0);

            }

            // build delivery order object
            deliveryOrderReport = digitizationBuilder.buildDeliveryOrderReport(postIpaRequest, fileOperationType);

            // set logo in do object
            deliveryOrderReport.setLogoByteCode(digitizationRepository.getBase64Image(templateConfiguration.getLogoId()));

            // get delivery order html document
            String deliveryOrderHtmlDocument = getDeliveryOrderHtmlDocument(deliveryOrderReport, templateConfiguration);

            //get watermark configuration
            List<WaterMarkDetails> waterMarkDetailsList = templateConfiguration.getWaterMarkDetails();

            // convert html document to pdf with watermark
            byte[] bytes = HtmlToPdfConverter.generatePdfByteArray(deliveryOrderHtmlDocument, waterMarkDetailsList, fileOperationType);

            String base64String = null;
            if (null != bytes) {
                // encode bytes in Base 64 format
                base64String = new String(Base64.encodeBase64(bytes));
            }

            if (StringUtils.isNotBlank(base64String)) {

                document.setByteCode(base64String);
                document.setDocID("AF01");
                document.setDocName(TemplateName.DELIVERY_ORDER.name());
                document.setStatus(Constant.SUCCESS);
                return GngUtils.getBaseResponse(HttpStatus.OK, document);
            }
        }


        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    private String getDeliveryOrderHtmlDocument(DeliveryOrderReport deliveryOrderReport, TemplateConfiguration templateConfiguration) {

        Template template = null;
        StringWriter writer = null;

        try {
            TemplateFactory templatesFactory = new TemplateFactory();
            template = templatesFactory.fetchTemplate(templateConfiguration
                    .getTemplatePath());
            VelocityContext context =   DeliveryOrderMapper.mapDeliveryOrder(deliveryOrderReport);


            writer = new StringWriter();
            template.merge(context, writer);

            return writer.toString();
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;

        } finally {

            try {
                if (null != writer) {
                    writer.flush();
                    writer.close();
                }
            } catch (IOException e) {
            }

        }


    }



    @Override
    public BaseResponse getAgreementForm(String institutionId, String productId, String refID) throws Exception {

        Document document = new Document();
        TemplateConfiguration templateConfiguration;
        //check action
        boolean actionFlag = lookupService.checkActionsAccess(institutionId, productId, ActionName.DIGITIZATION);

        if (actionFlag) {

            //check configuration
            templateConfiguration = lookupService.getTemplate(institutionId, productId, TemplateName.AGREEMENT_FORM);

            if (null == templateConfiguration) {
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }
        } else {

            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, GngUtils.getNonAuthoritativeInformationErrorList());
        }
        // get application data
        GoNoGoCroApplicationResponse application = digitizationRepository
                .getApplication(institutionId, refID);

        PostIPA postIpa = null;
        String base64String = null;
        if (null != application) {
            // get post ipa data
            postIpa = digitizationRepository.getPostIpa(institutionId, refID);
            if (null != postIpa) {

                // build application form
                ApplicationForm applicationForm = populateDigitizationFormFields(application,
                        postIpa, templateConfiguration);

                // get html document of application form
                String htmlDoc = getAgreementFormHtmlDocument(applicationForm, templateConfiguration);

                //convert html document to pdf
                byte[] bytes = HtmlToPdfConverter.generatePdfByteArray(htmlDoc, null, null);
                if (null != bytes) {
                    //encode pdf bytes to base64
                    base64String = new String(Base64.encodeBase64(bytes));
                }
            }
        }
        if (null != application && StringUtils.isNotBlank(base64String)) {
            document.setByteCode(base64String);
            document.setDocID("AF01");
            document.setDocName(TemplateName.AGREEMENT_FORM.name());
            document.setStatus(Constant.SUCCESS);

            return GngUtils.getBaseResponse(HttpStatus.OK, document);

        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }


    /**
     * @param applicationForm
     * @param templateConfiguration
     * @return
     */
    private String getApplicationFormHtmlDocument(
            ApplicationForm applicationForm, TemplateConfiguration templateConfiguration) {
        Template template = new Template();
        StringWriter writer = null;
        try {
            TemplateFactory templatesFactory = new TemplateFactory();
            template = templatesFactory
                    .fetchTemplate(templateConfiguration.getTemplatePath());
            VelocityContext context = ApplicationFormMapper.mapApplicationForm(applicationForm);

            writer = new StringWriter();
            template.merge(context, writer);
            return writer.toString();
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;
        } finally {
            try {
                writer.flush();

                if (null != writer)
                    writer.close();
            } catch (IOException e) {
            }

        }

    }

    private String getAgreementFormHtmlDocument(ApplicationForm applicationForm, TemplateConfiguration templateConfiguration) {

        Template template = new Template();
        StringWriter writer = null;

        try {
            TemplateFactory templatesFactory = new TemplateFactory();

            template = templatesFactory.fetchTemplate(templateConfiguration
                    .getTemplatePath());

            VelocityContext context = AgreementFormMapper.mapAgreementForm(applicationForm);

            writer = new StringWriter();
            template.merge(context, writer);

            return writer.toString();
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;

        } finally {
            writer.flush();
            try {
                if (null != writer)
                    writer.close();
            } catch (IOException e) {
            }

        }

    }


    /**
     * @param application
     * @param postIpa
     * @param templateConfiguration
     * @return
     */
    private ApplicationForm populateDigitizationFormFields(
            GoNoGoCroApplicationResponse application, PostIPA postIpa, TemplateConfiguration templateConfiguration) throws Exception {

        ApplicationForm applicationForm = new ApplicationForm();

        try {
            applicationForm.setLogo(digitizationRepository
                    .getBase64Image(templateConfiguration.getLogoId()));

            applicationForm.setApplicationDate(GngDateUtil
                    .getDDMMYYFormat(application.getDateTime()));
            applicationForm.setDateOfApplication(GngDateUtil
                    .getDDMMYYYYFormat(application.getDateTime()));

            if (null != application.getLosDetails()) {
                applicationForm.setLosNumber(application.getLosDetails()
                        .getLosID());
            }
            if (null != application.getGngRefId()) {
                applicationForm.setGngRefID(application.getGngRefId());
            }

            if (null != application.getApplicationRequest()
                    && null != application.getApplicationRequest().getRequest()) {

                applicationForm.setApplicantType(application.getApplicationRequest().getApplicantType());

                if (null != application.getApplicationRequest().getRequest().getGstDetails()) {
                    GstDetails gstDetails = application.getApplicationRequest().getRequest().getGstDetails();

                    applicationForm.setGstInNo(gstDetails.getGstNumber());
                    if (null != gstDetails.getFromDate()) {
                        applicationForm.setGstEffectiveDate(GngDateUtil.getDDMMYYYYFormat(gstDetails.getFromDate()));
                    }


                }

                if (null != application.getApplicationRequest().getRequest()
                        .getApplicant()) {

                    Applicant applicant = application.getApplicationRequest().getRequest()
                            .getApplicant();
                    //credit card loan amount number
                    List<String> creditCardLnAccNumbers = applicant.getCreditCardLnAccNumbers();
                    if (!CollectionUtils.isEmpty(creditCardLnAccNumbers)) {
                        applicationForm.setCreditCardLnAccNo(creditCardLnAccNumbers.get(0));
                    }
                    applicationForm.setTotalFamMember(String.valueOf(applicant.getNoOfFamilyMembers()));
                    applicationForm.setEarningMember(String.valueOf(applicant.getNoOfEarningMembers()));

                    if (null != applicant.getIncomeDetails()) {

                        setApplicantIncomeDetails(applicant.getIncomeDetails(), applicationForm);
                    }

                    setApplicantNameAndOccupationDetails(applicationForm, applicant);


                    if (!CollectionUtils.isEmpty(applicant.getKyc())) {

                        List<Kyc> kycList = applicant.getKyc();
                        setKycDetails(applicationForm, kycList);

                        boolean addressSameAsAbove = applicant.isResidenceAddSameAsAbove();

                        applicationForm.setResAddressSameAsPer(String.valueOf(addressSameAsAbove));

                    }

                    if (null != applicant.getPhone()
                            && !applicant.getPhone()
                            .isEmpty()) {

                        List<Phone> contactList = applicant.getPhone();

                        setPhoneDetails(applicationForm, contactList);

                    }
                    applicationForm.setReligion(applicant.getReligion());
                    applicationForm.setCast(null);

                    if (null != applicant.getEmail()
                            && !applicant.getEmail()
                            .isEmpty()) {
                        List<Email> emailList = applicant.getEmail();

                        setEmailDetails(applicationForm, emailList);
                    }

                    if (null != applicant.getAddress()
                            && !applicant.getAddress()
                            .isEmpty()) {
                        List<CustomerAddress> addresses = applicant.getAddress();
                        setCustomerAddress(applicationForm, addresses);

                    }

                    if (null != applicant.getEmployment()
                            && !applicant.getEmployment().isEmpty()) {
                        Employment employment = applicant.getEmployment().get(0);

                        setEmploymentDetails(applicationForm, employment);
                    }

                    if (null != application.getApplicationRequest().getRequest().getApplicant().getSurrogate()) {
                        Surrogate surrogate = application.getApplicationRequest().getRequest().getApplicant().getSurrogate();
                        setSurrogateInformation(applicationForm, surrogate);
                    }


                    if (!CollectionUtils.isEmpty(applicant.getBankingDetails())) {


                        BankingDetails bankingDetails = applicant.getBankingDetails().get(0);

                        setBankingDetails(applicationForm, bankingDetails);

                    }

                    if (null != application.getApplicationRequest()
                            .getRequest().getApplication()) {
                        Application application1 = application.getApplicationRequest()
                                .getRequest().getApplication();

                        applicationForm.setLoanAmount(application1.getLoanAmount());

                        if (application1.getDedupeEmiPaid() == 0 && application1.getDedupeTenor() == 0) {
                            applicationForm.setRelationWithTvs("false");
                        } else {
                            applicationForm.setRelationWithTvs("true");
                        }

                    }

                    if (null != applicant
                            .getApplicantReferences()
                            && !applicant.getApplicantReferences().isEmpty()) {

                        List<ApplicantReference> references = application
                                .getApplicationRequest().getRequest()
                                .getApplicant().getApplicantReferences();

                        getReferenceDetails(applicationForm, references);

                    }

                    if (null != application.getApplicationRequest().getHeader()) {
                        Header header = application.getApplicationRequest().getHeader();

                        applicationForm.setBranchName(setDealerBranchName(header.getDealerId(), header.getInstitutionId()));

                        applicationForm.setDsaCode(header.getDsaId());
                        applicationForm.setDealerCode(header.getDealerId());
                        applicationForm.setInstitutionId(header.getInstitutionId());
                        applicationForm.setProductName(header.getProduct().name());
                    }

                    applicationForm.setLocation(null);
                    applicationForm.setDateAndTimeOfReceipt(null);
                    applicationForm.setHdbContactPersonName(null);
                    applicationForm.setContactNumber(null);

                    applicationForm
                            .setApplicantPhoto(digitizationRepository
                                    .getBase64Image(getApplicantPhotoRequest(application.getApplicationRequest())));

                }

                if (null != application.getApplicationRequest().getAppMetaData()) {

                    AppMetaData appMetaData = application.getApplicationRequest().getAppMetaData();

                    if (null != appMetaData.getBranchV2()) {
                        applicationForm.setBranchName(appMetaData.getBranchV2().getBranchName());
                    }
                    if (null != appMetaData.getPhone()) {
                        applicationForm.setDsaMoNumber(appMetaData.getPhone().getPhoneNumber());
                    }
                    if (null != appMetaData.getDsaName()) {
                        applicationForm.setDsaName(getFatherOrMotherName(appMetaData.getDsaName()));
                    }


                    if (null != appMetaData.getSourcingDetails()) {
                        SourcingDetails sourcingDetails = appMetaData.getSourcingDetails();
                        getSourcingDetails(applicationForm, sourcingDetails);
                    }


                }

            }

            if (null != postIpa) {
                setPostIpaDetails(applicationForm, postIpa);

                String institutionId = application.getApplicationRequest().getHeader().getInstitutionId();

                if (null != application.getApplicationRequest().getHeader().getProduct()) {

                    String productName = application.getApplicationRequest().getHeader().getProduct().name();
                    String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId,
                            productName);

                    SchemeMasterData schemeDetails = masterDataViewRepository.getSchemeDetails(postIpa.getScheme(), institutionId, productAliasName);

                    if (null != schemeDetails) {
                        applicationForm.setCustomerIRR(schemeDetails.getMaxIrr());
                        applicationForm.setRateOfInterest(schemeDetails.getIntRate());
                    }

                }

            }
            getInsuranceExtendedWarrantyAmount(application.getGngRefId(), application.getApplicationRequest().getHeader().getInstitutionId(), applicationForm);


            return applicationForm;
        } catch (NullPointerException nullPointerException) {
            nullPointerException.printStackTrace();
            logger.error("Exception in set Application Form : "
                    + nullPointerException);
        }

        return applicationForm;
    }

    private void getInsuranceExtendedWarrantyAmount(String gngRefId, String institutionId, ApplicationForm applicationForm) {
        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(gngRefId, institutionId);
        double ewInsuranceAmtTotal = 0;
        if (null != extendedWarrantyDetails) {
            ewInsuranceAmtTotal = ewInsuranceAmtTotal + extendedWarrantyDetails.getEwWarrantyPremium();
            applicationForm.setEwEmi(extendedWarrantyDetails.getRevisedTotalEmi());
        }
        InsurancePremiumDetails insurancePremiumDetails = applicationRepository.fetchInsuranceDetails(gngRefId, institutionId);

        if (null != insurancePremiumDetails) {
            ewInsuranceAmtTotal = ewInsuranceAmtTotal + insurancePremiumDetails.getInsurancePremium();
            applicationForm.setInsuranceEmi(insurancePremiumDetails.getInsuranceEmi());
        }
        applicationForm.setInsEwAmtTotal(String.valueOf(ewInsuranceAmtTotal));

    }

    private void setApplicantIncomeDetails(IncomeDetails incomeDetails, ApplicationForm applicationForm) {

        if (incomeDetails.getGrossMonthlyAmt() != 0) {
            applicationForm.setGrossMonthlyIncome(String.valueOf(incomeDetails.getGrossMonthlyAmt()));
        }
        if (incomeDetails.getOtherSourceIncomeAmount() != 0) {
            applicationForm.setAdditionalIncomeSource(String.valueOf(incomeDetails.getOtherSourceIncomeAmount()));
        }

    }

    private void setApplicantNameAndOccupationDetails(ApplicationForm applicationForm, Applicant applicant) {

        applicationForm.setDateOfBirth(applicant.getDateOfBirth());

        if (null != applicant.getApplicantName()) {

            Name applicantName = applicant.getApplicantName();
            applicationForm.setPrefix(applicantName.getPrefix());

            applicationForm.setApplicantName(getFatherOrMotherName(applicantName));
        }

        if (null != applicant.getFatherName()) {

            applicationForm.setFatherName(getFatherOrMotherName(applicant.getFatherName()));
        }

        if (null != applicant.getMotherName()) {

            applicationForm.setMotherName(getFatherOrMotherName(applicant.getMotherName()));
        }
        if (null != applicant.getSpouseName()) {

            applicationForm.setSpouseName(getFatherOrMotherName(applicant.getSpouseName()));
        }


        String gender = applicant.getGender();
        String maritalStatus = applicant.getMaritalStatus();

        applicationForm.setSalutation(getSalutation(gender, maritalStatus));


        if (!CollectionUtils.isEmpty(applicant.getEmployment())) {
            applicationForm.setOccuption(applicant.getEmployment().get(0)
                    .getConstitution());
        }

        applicationForm.setGender(applicant.getGender());
        applicationForm.setAge(String.valueOf(applicant.getAge()));

        applicationForm.setMaritalStatus(applicant.getMaritalStatus());

        applicationForm.setNumberOfDependent(String
                .valueOf(applicant.getNoOfDependents()));

        applicationForm.setEducation(applicant.getEducation());
    }

    private String getFatherOrMotherName(Name fatherOrMotherName) {

        StringBuilder stringBuilder = new StringBuilder();

        if (StringUtils.isNotBlank(fatherOrMotherName.getFirstName())) {

            stringBuilder.append(fatherOrMotherName.getFirstName());
            stringBuilder.append(' ');
        }
        if (StringUtils.isNotBlank(fatherOrMotherName.getMiddleName())) {

            stringBuilder.append(fatherOrMotherName.getMiddleName());
            stringBuilder.append(' ');
        }
        if (StringUtils.isNotBlank(fatherOrMotherName.getLastName())) {

            stringBuilder.append(fatherOrMotherName.getLastName());
        }
        return stringBuilder.toString();

    }

    private String setDealerBranchName(String dealerId, String institutionId) {


        DealerBranchMaster dealerBranchMaster = reportingRepository.getBranchMaster(dealerId, institutionId);

        if (null != dealerBranchMaster && StringUtils.isNotBlank(dealerBranchMaster.getBranchName())) {
            if (StringUtils.equals(institutionId, "4019")) {
                return StringUtils.remove(dealerBranchMaster.getBranchName(), "-SF");
            } else {
                return dealerBranchMaster.getBranchName();
            }

        }
        return null;
    }

    private String getSalutation(String gender, String maritalStatus) {

        if (StringUtils.equals(gender, GNGWorkflowConstant.MALE.toFaceValue())) {
            return GNGWorkflowConstant.MR.toFaceValue();
        } else if (StringUtils.equals(gender, GNGWorkflowConstant.FEMALE.toFaceValue())
                && StringUtils.equals(maritalStatus, GNGWorkflowConstant.SINGLE.toFaceValue())) {
            return GNGWorkflowConstant.MS.toFaceValue();
        } else {
            return GNGWorkflowConstant.MRS.toFaceValue();
        }
    }

    private void setEmailDetails(ApplicationForm applicationForm, List<Email> emailList) {

        String emailID = null;
        String employerEmail = null;
        for (Email email : emailList) {
            if (StringUtils.equalsIgnoreCase("PERSONAL",
                    email.getEmailType())) {
                emailID = email.getEmailAddress();
            } else if (StringUtils.equalsIgnoreCase("WORK",
                    email.getEmailType())) {
                employerEmail = email.getEmailAddress();

            }
        }
        applicationForm.setEmailID(emailID);
        applicationForm.setOfficeEmailID(employerEmail);

    }

    private void setCustomerAddress(ApplicationForm applicationForm, List<CustomerAddress> addresses) {

        String monthlyRent = null;
        String residenceAddress = null;
        String residenseCity = null;
        String residenceLandMark = null;
        String residensePin = null;
        String residenceState = null;
        String yearAtResidense = null;
        String yearAtCity = null;
        String presentResidence = null;

        String permanentAddress = null;
        String permanentCity = null;
        String permanentState = null;
        String permanentPin = null;
        String permanentLandMark = null;
        String permanentResidence = null;
        String permanentMontlyRent = null;
        String combPermntAddress =null;

        String employerAddress = null;
        String employerCity = null;
        String employerState = null;
        String employerPin = null;
        String employerLandMark = null;
        String monthAtAddress = null;
        String employerCombAddress =null;


        for (CustomerAddress address : addresses) {
            if (StringUtils.equalsIgnoreCase("RESIDENCE",
                    address.getAddressType())) {
                monthlyRent = String.format("%.2f", address.getRentAmount());
                residenceAddress = getAddressString(address);
                residenseCity = address.getCity();
                residenceLandMark = address.getLandMark();
                residenceState = address.getState();

                residensePin = String.valueOf(address.getPin());
                yearAtResidense = String
                        .valueOf(Math.round((address
                                .getMonthAtAddress() / 12)));
                yearAtCity = String.valueOf(Math.round((address
                        .getMonthAtCity() / 12)));
                presentResidence = address
                        .getResidenceAddressType();
                monthAtAddress = String.valueOf(address.getMonthAtAddress());
            } else if (StringUtils.equalsIgnoreCase(
                    "PERMANENT", address.getAddressType())) {
                permanentAddress = getAddressString(address);
                permanentCity = address.getCity();
                permanentState = address.getState();
                permanentPin = String.valueOf(address.getPin());
                permanentLandMark = address.getLandMark();
                permanentResidence = address.getResidenceAddressType();
                permanentMontlyRent = String.format("%.2f", address.getRentAmount());
                combPermntAddress = getCombinedAddress(address);


            } else if (StringUtils.equalsIgnoreCase("OFFICE",
                    address.getAddressType())) {

                employerAddress = getAddressString(address);
                employerCity = address.getCity();
                employerState = address.getState();
                employerPin = String.valueOf(address.getPin());
                employerLandMark = address.getLandMark();
                employerCombAddress = getCombinedAddress(address);

            }
        }
        applicationForm.setMonthlyResidenceAddRent(monthlyRent);
        applicationForm.setResidenseLandMark(residenceLandMark);
        applicationForm.setResidenseAddress(residenceAddress);
        applicationForm.setResidenseCity(residenseCity);
        applicationForm.setResidenseState(residenceState);
        applicationForm.setResidensePin(residensePin);
        applicationForm.setMonthAtAddress(monthAtAddress);

        applicationForm.setYearAtResidense(yearAtResidense);
        applicationForm.setYearAtCity(yearAtCity);

        applicationForm.setPresentResidence(presentResidence);

        applicationForm.setPermanentAddress(permanentAddress);
        applicationForm.setPermanentCity(permanentCity);
        applicationForm.setPermanentPin(permanentPin);
        applicationForm.setPermanentState(permanentState);
        applicationForm.setPermanentLandMark(permanentLandMark);
        applicationForm
                .setPermanentResidence(permanentResidence);
        applicationForm.setMonthlyPermenentAddRent(permanentMontlyRent);
        applicationForm.setPermanentCombAddress(combPermntAddress);

        /**
         * office Address setter
         */
        applicationForm.setEmployerAddress(employerAddress);
        applicationForm.setEmployerState(employerState);
        applicationForm.setEmployerPin(employerPin);
        applicationForm.setEmployerCity(employerCity);
        applicationForm.setEmployerLandMark(employerLandMark);
        applicationForm.setEmployerCombAddress(employerCombAddress);
    }

    private String getCombinedAddress(CustomerAddress address) {

        StringBuilder stringBuilder = new StringBuilder();

        String addressString = getAddressString(address);

        if (StringUtils.isNotBlank(addressString)) {
            stringBuilder.append(addressString).append(' ');
        }
        if (StringUtils.isNotBlank(address.getLandMark())) {
            stringBuilder.append(address.getLandMark()).append(' ');
        }
        if (StringUtils.isNotEmpty(address.getCity())) {
            stringBuilder.append(address.getCity()).append(' ');
        }
        if (StringUtils.isNotEmpty(address.getState())) {
            stringBuilder.append(address.getState()).append(' ');
        }
        if (address.getPin() != 0) {
            stringBuilder.append(String.valueOf(address.getPin()));
        }
        return stringBuilder.toString();

    }

    private void setEmploymentDetails(ApplicationForm applicationForm, Employment employment) {

        applicationForm.setEmployerName(employment
                .getEmploymentName());
        applicationForm.setEmploymentType(employment.getEmploymentType());
        applicationForm.setNumberOfYearInCurrentBusiness(String
                .valueOf(Math.round(employment.getTimeWithEmployer() / 12)));
        applicationForm.setAnnualSalary(String.format("%.2f", employment.getMonthlySalary() * 12));
        applicationForm.setDesignation(employment.getDesignation());

    }

    private void setSurrogateInformation(ApplicationForm applicationForm, Surrogate surrogate) {


        if (!CollectionUtils.isEmpty(surrogate.getCreditCardSurrogate())) {
            CreditCardSurrogate creditCardSurrogate = surrogate.getCreditCardSurrogate().get(0);

            applicationForm.setCreditCardNumber(creditCardSurrogate.getCardNumber());
            if (null != creditCardSurrogate.getCardCategory()) {
                String cardCategory = creditCardSurrogate.getCardCategory();
                applicationForm.setCreditCardType(getCreditCardType(cardCategory).name());
            }
        }
        if (!CollectionUtils.isEmpty(surrogate.getCarSurrogate())) {
            CarSurrogate carSurrogate = surrogate.getCarSurrogate().get(0);

            if (null != carSurrogate.getCategory()) {

                applicationForm.setCarType(getCarType(carSurrogate.getCategory()).name());
            }
        }

        if (!CollectionUtils.isEmpty(surrogate.getDebitCardSurrogate())) {

            DebitCardSurrogate debitCardSurrogate = surrogate.getDebitCardSurrogate().get(0);

            applicationForm.setDebitCardNumber(debitCardSurrogate.getCardNumber());
        }

        if (!CollectionUtils.isEmpty(surrogate.getBankSurragates())) {
            applicationForm.setBankSurrogateProof("true");
        }
        if (!CollectionUtils.isEmpty(surrogate.getTraderSurrogate())) {
            applicationForm.setBusinessSurrogateProof("true");
        }
        if (!CollectionUtils.isEmpty(surrogate.getSalariedSurrogate())) {
            applicationForm.setSalarySlipSurrogate("true");
        }
    }

    private CarType getCarType(String carCategory) {
        CarType carType = null;
        if (StringUtils.containsIgnoreCase(carCategory, CarType.COMPACT.name())) {
            carType = CarType.COMPACT;
        }
        if (StringUtils.containsIgnoreCase(carCategory, CarType.MINI.name())) {
            carType = CarType.MINI;
        }
        if (StringUtils.containsIgnoreCase(carCategory, CarType.MUV.name())) {
            carType = CarType.MUV;
        }
        if (StringUtils.containsIgnoreCase(carCategory, CarType.SUV.name())) {
            carType = CarType.SUV;
        }
        if (StringUtils.containsIgnoreCase(carCategory, CarType.PREMIUM.name())) {
            carType = CarType.PREMIUM;
        }
        return carType;

    }

    private CreditCardType getCreditCardType(String cardCategory) {

        CreditCardType creditCardType = null;

        if (StringUtils.containsIgnoreCase(cardCategory, CreditCardType.CLASSIC.name())) {
            creditCardType = CreditCardType.CLASSIC;
        }
        if (StringUtils.containsIgnoreCase(cardCategory, CreditCardType.GOLD.name())) {
            creditCardType = CreditCardType.GOLD;
        }
        if (StringUtils.containsIgnoreCase(cardCategory, CreditCardType.PLATINUM.name())) {
            creditCardType = CreditCardType.PLATINUM;
        }
        if (StringUtils.containsIgnoreCase(cardCategory, CreditCardType.SILVER.name())) {
            creditCardType = CreditCardType.SILVER;
        }
        return creditCardType;

    }

    private void setKycDetails(ApplicationForm applicationForm, List<Kyc> kycList) {

        String panNumber = null;
        String aadharNumber = null;
        String drivingLicenseNumber = null;
        String passportNumber = null;
        String voterID = null;
        String electricityNo = null;
        String addressProof = null;
        String lpgId = null;
        String drivingLicenseExpDate=null;
        String passportExpDate=null;

        for (Kyc kyc : kycList) {
            if (StringUtils.equalsIgnoreCase("PAN",
                    kyc.getKycName())) {
                panNumber = kyc.getKycNumber();
                addressProof = kyc.getKycName();
            } else if (StringUtils.equalsIgnoreCase("AADHAAR",
                    kyc.getKycName())) {
                aadharNumber = kyc.getKycNumber();
                addressProof = kyc.getKycName();
            } else if (StringUtils.equalsIgnoreCase(
                    "DRIVING-LICENSE", kyc.getKycName())) {
                drivingLicenseNumber = kyc.getKycNumber();
                drivingLicenseExpDate=kyc.getExpiryDate();
            } else if (StringUtils.equalsIgnoreCase("PASSPORT",
                    kyc.getKycName())) {
                addressProof = kyc.getKycName();
                passportNumber = kyc.getKycNumber();
                passportExpDate=kyc.getExpiryDate();

            } else if (StringUtils.equalsIgnoreCase("VOTER-ID",
                    kyc.getKycName())) {
                voterID = kyc.getKycNumber();
                addressProof = kyc.getKycName();
            } else if (StringUtils.equalsIgnoreCase("ELECTRICITY-BILL",
                    kyc.getKycName())) {

                electricityNo = kyc.getKycNumber();
            }else if(StringUtils.equalsIgnoreCase("LPG-NO",
                    kyc.getKycName())){
                lpgId = kyc.getKycNumber();

            }
        }

        applicationForm.setPanNumber(panNumber);

        applicationForm.setAadharNumber(aadharNumber);

        applicationForm
                .setDrivingLicenseNumber(drivingLicenseNumber);

        applicationForm.setDrivingLicenseExpDate(drivingLicenseExpDate);

        applicationForm.setPassportNumber(passportNumber);

        applicationForm.setPassportExpiryDate(passportExpDate);

        applicationForm.setVoterIDNumber(voterID);

        applicationForm.setElectricityNo(electricityNo);

        applicationForm.setAddressProof(addressProof);

        applicationForm.setLpgId(lpgId);
    }

    private void setPhoneDetails(ApplicationForm applicationForm, List<Phone> contactList) {

        String mobile = null;
        String residenseTelephone = null;
        String residenseStd = null;

        String permanentStd = null;
        String permanentTelephone = null;

        String employerPhone = null;
        String employerStd = null;
        String employerExtention = null;
        String officeMobile = null;
        String residenceMobile = null;
        for (Phone phone : contactList) {
            if (StringUtils.equalsIgnoreCase("PERSONAL_MOBILE",
                    phone.getPhoneType())) {
                mobile = phone.getPhoneNumber();
            } else if (StringUtils.equalsIgnoreCase("OFFICE_MOBILE",
                    phone.getPhoneType())) {
                officeMobile = phone.getPhoneNumber();

            } else if (StringUtils.equalsIgnoreCase("RESIDENCE_MOBILE",
                    phone.getPhoneType())) {
                residenceMobile = phone.getPhoneNumber();

            } else if (StringUtils.equalsIgnoreCase(
                    "RESIDENCE_PHONE", phone.getPhoneType())) {
                residenseTelephone = phone.getPhoneNumber();
                residenseStd = phone.getAreaCode();
            } else if (StringUtils.equalsIgnoreCase(
                    "PERSONAL_PHONE", phone.getPhoneType())) {
                permanentTelephone = phone.getPhoneNumber();
                permanentStd = phone.getAreaCode();
            } else if (StringUtils.equalsIgnoreCase(
                    "OFFICE_PHONE", phone.getPhoneType())) {
                employerPhone = phone.getPhoneNumber();
                employerStd = phone.getAreaCode();
                employerExtention = phone.getExtension();
            }
        }

        applicationForm.setOfficeMobile(officeMobile);
        applicationForm.setMobile(mobile);
        applicationForm
                .setResidenseTelephone(residenseTelephone);
        applicationForm.setResidenseStd(residenseStd);

        applicationForm.setPermanentStd(permanentStd);
        applicationForm
                .setPermanentTelephone(permanentTelephone);

        applicationForm.setEmployerPhone(employerPhone);
        applicationForm.setEmployerExtension(employerExtention);
        applicationForm.setEmployerStd(employerStd);
        applicationForm.setResidenceMobile(residenceMobile);

    }

    private void setPostIpaDetails(ApplicationForm applicationForm, PostIPA postIpa) {

        if (null != postIpa.getAssetDetails() && !postIpa.getAssetDetails().isEmpty()) {

            applicationForm.setAssetOwned(setAssetOwned(postIpa.getAssetDetails().get(0).getAssetCtg().toUpperCase()));
            applicationForm.setManufacturer(postIpa.getAssetDetails().get(0).getAssetMake());
            applicationForm.setModel(postIpa.getAssetDetails().get(0).getModelNo());
            applicationForm.setDealerName(postIpa.getAssetDetails().get(0).getDlrName());
            applicationForm.setProductType(postIpa.getAssetDetails().get(0).getAssetCtg());
            applicationForm.setAssetModelMake(postIpa.getAssetDetails().get(0).getAssetModelMake());

        }
        applicationForm.setTenure(String.valueOf(postIpa.getTenor()));
        applicationForm.setEmi(postIpa.getEmi());
        applicationForm.setAdvanceEMI(String.format("%.2f", postIpa.getAdvanceEmi()));
        applicationForm.setAdvanceEMITenor(String.valueOf(postIpa.getAdvanceEMITenor()));
        applicationForm.setApplicationFees(String.valueOf(postIpa.getProcessingFees()));
        applicationForm.setSchemeCode(postIpa.getScheme());

        //for tvs
        applicationForm.setFinanceAmount(String.format("%.2f", postIpa.getFinanceAmount()));
        applicationForm.setTotalAssetCost(String.format("%.2f", postIpa.getTotalAssetCost()));
        applicationForm.setOtherCharges(String.format("%.2f", postIpa.getOtherChargesIfAny()));

        applicationForm.setRevisedEmiAmount(String.format("%.2f", postIpa.getRevisedEmi()));
        applicationForm.setRevisedLoanAmount(String.format("%.2f", postIpa.getRevisedLoanAmount()));
        applicationForm.setProceFees(String.format("%.2f", postIpa.getProcessingFees()));

        if (null != postIpa.getEmiStartDate()) {
            applicationForm.setFirstEmiDueDt(GngDateUtil.getFormattedDate(postIpa.getEmiStartDate(), "dd/MM/yyyy"));
            applicationForm.setFirstEmiDueDtInDDmmYYYY(GngDateUtil.getDDMMYYYYFormat(postIpa.getEmiStartDate()));
        }
        if (null != postIpa.getEmiEndDate()) {
            applicationForm.setLastEmiDueDate(GngDateUtil.getFormattedDate(postIpa.getEmiEndDate(), "dd/MM/yyyy"));
        }

        applicationForm.setTotalDownPayment(String.valueOf(postIpa.getDownPaymentAmt()));

    }

    private void getReferenceDetails(ApplicationForm applicationForm, List<ApplicantReference> references) {

        String refName1 = null;
        String refRelationshipWithApplicant1 = null;
        String refResidentialAddress1 = null;
        String refPhone1 = null;
        String refCity1 = null;
        String refState1 = null;
        String refPin1 = null;
        String refStd1 = null;
        String refMob1 = null;

        String refName2 = null;
        String refRelationshipWithApplicant2 = null;
        String refResidentialAddress2 = null;
        String refPhone2 = null;
        String refCity2 = null;
        String refState2 = null;
        String refPin2 = null;
        String refStd2 = null;
        String refMob2 = null;

        if (references != null && !references.isEmpty()) {

            for (int index = 0; index < references.size(); index++) {
                switch (index) {
                    case 0: {
                        if (null != references.get(0).getName()) {
                            refName1 = getName(references.get(0).getName());
                        }
                        if (StringUtils.isNotBlank(references
                                .get(0).getRelationType())) {
                            refRelationshipWithApplicant1 = references
                                    .get(0).getRelationType();
                        }
                        if (null != references.get(0).getPhones() && !references.get(0).getPhones().isEmpty()) {
                            List<Phone> phones = new ArrayList<>();
                            phones.addAll(references.get(0).getPhones());
                            for (Phone phone : phones) {
                                if (StringUtils.equals(phone.getPhoneType(), GNGWorkflowConstant.PERSONAL_PHONE.name())) {
                                    refPhone1 = phone.getPhoneNumber();
                                    if (StringUtils.isNotBlank(phone.getAreaCode())) {
                                        refStd1 = phone.getAreaCode();
                                    }

                                }
                                if (StringUtils.equals(phone.getPhoneType(), GNGWorkflowConstant.PERSONAL_MOBILE.name())) {
                                    refMob1 = phone.getPhoneNumber();

                                }
                            }
                        }

                        if (null != references.get(0)
                                .getAddress()) {

                            refResidentialAddress1 = getAddress(references.get(0)
                                    .getAddress());

                            if (null != references.get(0).getAddress().getCity()) {
                                refCity1 = references.get(0).getAddress().getCity();
                            }
                            if (null != references.get(0).getAddress().getState()) {
                                refState1 = references.get(0).getAddress().getState();
                            }

                            refPin1 = String.valueOf(references.get(0).getAddress().getPin());


                        }
                    }
                    break;
                    case 1: {
                        if (null != references.get(1).getName()) {
                            refName2 = getName(references.get(1).getName());
                        }
                        refRelationshipWithApplicant2 = references
                                .get(1).getRelationType();

                        if (null != references.get(1).getPhones() && !references.get(1).getPhones().isEmpty()) {
                            List<Phone> phones = new ArrayList<>();
                            phones.addAll(references.get(1).getPhones());

                            for (Phone phone : phones) {
                                if (StringUtils.equals(phone.getPhoneType(), GNGWorkflowConstant.PERSONAL_PHONE.name())) {
                                    refPhone2 = phone.getPhoneNumber();
                                    if (StringUtils.isNotBlank(phone.getAreaCode())) {
                                        refStd2 = phone.getAreaCode();
                                    }

                                }
                                if (StringUtils.equals(phone.getPhoneType(), GNGWorkflowConstant.PERSONAL_MOBILE.name())) {
                                    refMob2 = phone.getPhoneNumber();
                                }
                            }
                        }

                        if (null != references.get(1)
                                .getAddress()) {

                            refResidentialAddress2 = getAddress(references.get(1)
                                    .getAddress());

                            if (null != references.get(1).getAddress().getCity()) {
                                refCity2 = references.get(1).getAddress().getCity();
                            }
                            if (null != references.get(1).getAddress().getState()) {
                                refState2 = references.get(1).getAddress().getState();
                            }

                            refPin2 = String.valueOf(references.get(1).getAddress().getPin());


                        }
                    }
                    break;
                    default: {

                    }
                    break;
                }
            }

        }
        applicationForm.setRefName1(refName1);
        applicationForm
                .setRefRelationshipWithApplicant1(refRelationshipWithApplicant1);
        applicationForm
                .setRefResidentialAddress1(refResidentialAddress1);
        applicationForm.setRefCity1(refCity1);
        applicationForm.setRefState1(refState1);
        applicationForm.setRefPin1(refPin1);
        applicationForm.setRefSTD1(refStd1);
        applicationForm.setRefPhone1(refPhone1);
        applicationForm.setRefMobile1(refMob1);

        applicationForm.setRefName2(refName2);
        applicationForm
                .setRefRelationshipWithApplicant2(refRelationshipWithApplicant2);
        applicationForm
                .setRefResidentialAddress2(refResidentialAddress2);
        applicationForm.setRefCity2(refCity2);
        applicationForm.setRefState2(refState2);
        applicationForm.setRefPin2(refPin2);
        applicationForm.setRefSTD2(refStd2);
        applicationForm.setRefPhone2(refPhone2);
        applicationForm.setRefMobile2(refMob2);

    }

    private String getName(Name name) {
        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(name.getFirstName())) {
            sb.append(name.getFirstName());
            sb.append(" ");
        }
        if (StringUtils.isNotBlank(name.getMiddleName())) {
            sb.append(name.getMiddleName());
            sb.append(" ");
        }
        if (StringUtils.isNotBlank(name.getLastName())) {
            sb.append(name.getLastName());
        }
        return sb.toString();

    }

    private String getAddress(Address address) {
        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(address.getAddressLine1())) {
            sb.append(address.getAddressLine1());
            sb.append(" ");
        }
        if (StringUtils.isNotBlank(address.getAddressLine2())) {
            sb.append(address.getAddressLine2());
        }
        return sb.toString();
    }

    private void getSourcingDetails(ApplicationForm applicationForm, SourcingDetails sourcingDetails) {

        if (null != sourcingDetails.getS4()) {
            applicationForm.setSoCode(sourcingDetails.getS4().getValue());
        }
        if (null != sourcingDetails.getS3()) {
            applicationForm.setSmCode(sourcingDetails.getS3().getValue());
        }
        if (null != sourcingDetails.getS2()) {
            applicationForm.setTmAsmCode(sourcingDetails.getS2().get(0).getValue());
        }

        if (null != sourcingDetails.getS1()) {
            applicationForm.setAsmCmCode(sourcingDetails.getS1().getValue());
        }
    }

    private void setBankingDetails(ApplicationForm applicationForm, BankingDetails bankingDetails) {

        if (StringUtils.isNotBlank(bankingDetails.getBankName())) {
            applicationForm.setBankName(bankingDetails.getBankName());
            applicationForm.setIsHdfcBankAccount(isHdfcBankAccount(bankingDetails.getBankName()));
        }

        if (StringUtils.isNotBlank(bankingDetails.getAccountNumber())) {
            applicationForm.setAccountNumber(bankingDetails.getAccountNumber());
        }

        if (StringUtils.isNotBlank(bankingDetails.getBranchName())) {
            applicationForm.setBankBranchName(bankingDetails.getBranchName());
        }
        if (StringUtils.isNotBlank(bankingDetails.getIfscCode())) {
            applicationForm.setIfscCode(bankingDetails.getIfscCode());

        }
        if (StringUtils.isNotBlank(bankingDetails.getAccountType())) {
            applicationForm.setTypeOfAccount(bankingDetails.getAccountType());

        }
        applicationForm.setYearHeld(String.valueOf(bankingDetails.getYearHeld()));

        if (StringUtils.isNotBlank(bankingDetails.getChequeNumber())) {
            applicationForm.setChequeNumber(bankingDetails.getChequeNumber());

        }

        Name name = null;
        if (null != bankingDetails.getAccountHolderName()) {
            name = bankingDetails.getAccountHolderName();

            StringBuilder buildName = new StringBuilder();

            if (StringUtils.isNotBlank(name.getFirstName())) {
                buildName.append(name.getFirstName());
                buildName.append(" ");
            }
            if (StringUtils.isNotBlank(name.getMiddleName())) {
                buildName.append(name.getMiddleName());
                buildName.append(" ");
            }
            if (StringUtils.isNotBlank(name.getLastName())) {
                buildName.append(name.getLastName());
            }
            applicationForm.setAccountHolderName(buildName.toString());
        }


    }

    private String isHdfcBankAccount(String bankName) {
        if (StringUtils.containsIgnoreCase(bankName, "hdfc")) {
            return Status.YES.name();
        }
        return Status.NO.name();
    }

    private String getAddressString(CustomerAddress address) {

        StringBuilder stringBuilder = new StringBuilder();
        if (address.getAddressLine1() != null) {
            stringBuilder.append(address.getAddressLine1());
            stringBuilder.append(" ");
        }
        if (address.getAddressLine2() != null) {
            stringBuilder.append(address.getAddressLine2());
            stringBuilder.append(" ");
        }
        if (address.getLine3() != null) {
            stringBuilder.append(address.getLine3());
            stringBuilder.append(" ");
        }
        if (address.getLine4() != null) {
            stringBuilder.append(address.getLine4());
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }

    private FileUploadRequest getSoftcellLogoRequest() {
        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId("4045");

        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("softcell_logo.jpg");
        uploadFileDetails.setFileType("jpg");

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId("SOFTCELL-LOGO-01");
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        return fileUploadRequest;
    }

    /**
     * @param applicationRequest
     * @return
     */
    private FileUploadRequest getApplicantPhotoRequest(ApplicationRequest applicationRequest) {

        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId(applicationRequest.getHeader().getInstitutionId());


        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("APPLICANT-PHOTO");
        uploadFileDetails.setFileType("jpg");
        uploadFileDetails.setDeleted(false);

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId(applicationRequest.getRefID());
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        return fileUploadRequest;
    }


    public String setAssetOwned(String assetOwned) {

        String newAssetOwned = null;
        if (StringUtils.containsIgnoreCase(assetOwned, "LED")) {
            newAssetOwned = "LED";
        } else if (StringUtils.containsIgnoreCase(assetOwned, "LCD")) {
            newAssetOwned = "LCD";
        } else if (StringUtils.containsIgnoreCase(assetOwned, "WASHING MACHINE")) {
            newAssetOwned = "WASHING MACHINE";
        } else if (StringUtils.containsIgnoreCase(assetOwned, "AIR CONDITIONER")) {
            newAssetOwned = "AIR CONDITIONER";
        } else {
            newAssetOwned = assetOwned;
        }
        return newAssetOwned;
    }


    @Override
    public BaseResponse downloadDigitizationForm(DownloadDigitizationFormRequest downloadDigitizationFormRequest) throws Exception {

        BaseResponse baseResponse = null;
        GNGWorkflowConstant fileName = downloadDigitizationFormRequest.getFileName();
        String institutionId = downloadDigitizationFormRequest.getHeader().getInstitutionId();
        String refID = downloadDigitizationFormRequest.getRefID();
        Product product = downloadDigitizationFormRequest.getHeader().getProduct();


        if (fileName != GNGWorkflowConstant.APPLICATION_FORM && fileName != GNGWorkflowConstant.AGREEMENT_FORM) {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sFileName")
                    .id("fileName")
                    .message(ErrorCode.INVALID_FILE_NAME)
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        // check digitization form existance
        Document responseDocument = digitizationRepository.getDigitizationForm(downloadDigitizationFormRequest);

        //if document is null(digitization form is not exist) ,then save digitization form and return the same
        if (null == responseDocument) {

            logger.info("{} not found for refId {} and InstId {} ,need to save in the database", fileName.toFaceValue(), refID, institutionId);

            // set by default document status

            InvoiceDetailsRequest invoiceDetailsRequest = DMSUtils.buildAgreementApplicationFormRequest(refID, institutionId, product);

            if (fileName == GNGWorkflowConstant.APPLICATION_FORM) {

                // save the application form and get applicationFormId
                String applicationFormId = digitizationStoreManager.saveApplicationForm(invoiceDetailsRequest, fileName.toFaceValue());

                if (null != applicationFormId) {

                    //build request to get documentBase64 code using application form image Id
                    GetFileRequest getImageRequest = dmsBuilder.buildGetDocumentBase64ImageRequest(applicationFormId, institutionId);

                    // fetch application form base64code using image id
                    BaseResponse applicationBaseResponse = documentStoreManager.getDocumentBase64ImageByRefId(getImageRequest);

                    Document applicationFormDocument = GngUtils.convertBaseResponsePayload(applicationBaseResponse, Document.class);

                    if (null != applicationFormDocument && null != applicationFormDocument.getByteCode()) {
                        responseDocument = new Document();
                        responseDocument.setByteCode(applicationFormDocument.getByteCode());
                        responseDocument.setDocID(applicationFormId);
                        responseDocument.setDocName(fileName.toFaceValue());
                        responseDocument.setStatus(Status.SUCCESS.name());
                    }


                }

            } else if (fileName == GNGWorkflowConstant.AGREEMENT_FORM) {

                // save the agreement form
                String agreementFormId = digitizationStoreManager.saveAgreementForm(invoiceDetailsRequest, fileName.toFaceValue());

                if (null != agreementFormId) {

                    //build request to get documentBase64 code
                    GetFileRequest getImageRequest = dmsBuilder.buildGetDocumentBase64ImageRequest(agreementFormId, institutionId);

                    // fetch agreement form base64code using image id
                    BaseResponse agreementBaseResponse = documentStoreManager.getDocumentBase64ImageByRefId(getImageRequest);

                    Document agreementFormDocument = GngUtils.convertBaseResponsePayload(agreementBaseResponse, Document.class);

                    if (null != agreementFormDocument && null != agreementFormDocument.getByteCode()) {
                        responseDocument = new Document();
                        responseDocument.setByteCode(agreementFormDocument.getByteCode());
                        responseDocument.setDocName(fileName.toFaceValue());
                        responseDocument.setDocID(agreementFormId);
                        responseDocument.setStatus(Status.SUCCESS.name());
                    }

                }

            }
        } else {
            logger.info("{} found successfully for refId {} and InstId {}", fileName.toFaceValue(), refID, institutionId);
        }

        if (null != responseDocument) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, responseDocument);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;

    }

    @Override
    public BaseResponse downloadAchMandateForm(String institutionId, String productId,
                                               String refID) throws Exception{
        Document document = new Document();
        TemplateConfiguration templateConfiguration = null;

        boolean actionFlag = lookupService.checkActionsAccess(institutionId, productId, ActionName.DIGITIZATION);

        if (actionFlag) {

            templateConfiguration = lookupService.getTemplate(institutionId, productId, TemplateName.ACH_MANDATE_FORM);

            if (null == templateConfiguration) {
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }

        } else {
            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, GngUtils.getNonAuthoritativeInformationErrorList());
        }


        GoNoGoCroApplicationResponse application = digitizationRepository
                .getApplication(institutionId, refID);

        PostIPA postIpa = null;
        String base64String = null;
        if (null != application) {
            postIpa = digitizationRepository.getPostIpa(institutionId,
                    refID);
            if (null != postIpa) {

                AchMandateForm achMandateForm = digitizationBuilder.buildAchMandateForm(application,
                        postIpa, templateConfiguration);

                String applicationFormHtmlDocument = getAchMandateFormHtmlDocument(achMandateForm, templateConfiguration);

                byte[] bytes = HtmlToPdfConverter.generatePdfByteArray(applicationFormHtmlDocument, null, null);
                if (null != bytes) {
                    base64String = new String(Base64.encodeBase64(bytes));
                }

            }
        }

        if (null != application && StringUtils.isNotBlank(base64String)) {
            document.setByteCode(base64String);
            document.setDocID("AF01");
            document.setDocName(TemplateName.ACH_MANDATE_FORM.name());
            document.setStatus(Constant.SUCCESS);
            return GngUtils.getBaseResponse(HttpStatus.OK, document);
        }


        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse downloadSanctionLetter(DigitizationApplicationReportRequest request) {
        Document document = new Document();
        String base64String = null;
        TemplateConfiguration templateConfiguration = null;
        BaseResponse baseResponse = null;

        String institutionId = request.getHeader().getInstitutionId();
        String productId = request.getHeader().getProduct().toProductId();
        String refId = request.getRefID();
        Header header = request.getHeader();
        String templateId = request.getTemplateId();
        String appId = request.getAppId();
        String insuranceId = request.getInsuranceId();
        SanctionReport sanctionReport = null;
        boolean action = lookupService.checkActionsAccess(institutionId, productId, ActionName.DIGITIZATION);
        if (action) {
            templateConfiguration = lookupService.getTemplateById(institutionId, productId, TemplateName.SANCTION_LETTER, templateId);
            logger.debug("Configured Template Configuration Dtls:  {}", templateConfiguration.toString());
            if (templateConfiguration == null) {
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, GngUtils.getNonAuthoritativeInformationErrorList());
        }

        try {

            logger.debug("Constructing Sanction Report object");
            // SanctionReport sanctionReport = populateApplicationReport(header, refId, templateConfiguration);
            if(StringUtils.isNotEmpty(appId)){
                sanctionReport = populateSanctionReport(header, refId, templateConfiguration, appId,insuranceId);
            }else {
                sanctionReport = populateSanctionReport(header, refId, templateConfiguration, null,null);
            }
           // logger.debug("Constructed Application Report Object : {}", sanctionReport.toString());
            if (sanctionReport != null) {
                //String ApplicationReportForHtmlDoc = getApplicationReportForHtmlDoc(applicationReport, templateConfiguration);
                String sanctionReportForHtmlDoc = getSanctionReportForHtmlDoc(sanctionReport, templateConfiguration, document);
                logger.debug("Generated Document String : {}", sanctionReportForHtmlDoc);
                byte[] bytes = HtmlToPdfConverter.generatePdfByteArray(sanctionReportForHtmlDoc, null, null);

                if (null != bytes) {
                    //encode pdf bytes to base64
                    base64String = new String(Base64.encodeBase64(bytes));
                }
            }

            if (null != sanctionReport && StringUtils.isNotBlank(base64String)) {
                document.setByteCode(base64String);
                document.setDocID("AF01");
                document.setDocName(TemplateName.SANCTION_LETTER.name());
                document.setStatus(Constant.SUCCESS);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, document);

            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return baseResponse;
    }

    private String getSanctionReportForHtmlDoc(SanctionReport sanctionReport, TemplateConfiguration templateConfiguration, Document document) {
        Template template = null;
        StringWriter writer = null;
        try {
            TemplateFactory templateFactory = new TemplateFactory();
            template = templateFactory.fetchTemplate(templateConfiguration.getTemplatePath());
            VelocityContext context = ApplicationReportMapper.getSanctionReportMap(sanctionReport, true);
            writer = new StringWriter();

            if(StringUtils.equalsIgnoreCase(templateConfiguration.getTemplateId(),"08") && StringUtils.equalsIgnoreCase(templateConfiguration.getProductId(),"06")){
                templateFactory.merge(templateConfiguration.getTemplatePath(),"UTF-8",context,writer);
            } else{
                template.merge(context, writer);
            }
            logger.info("into staring writter{}", writer.toString());
            document.setHtmlContent(writer.toString());
            return writer.toString();
        } catch (Exception e) {
            logger.debug("Generated Application Report document error:  {}" + writer.toString());
            logger.error("{}",e.getStackTrace());
            return null;

        } finally {
            if (writer == null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    logger.error("{}",e.getStackTrace());
                }
            }
        }


    }


    private SanctionReport populateSanctionReport(Header header, String refId, TemplateConfiguration templateConfiguration,String appId, String insuranceId) {
        SanctionReport sanctionReport = new SanctionReport();
        // finding all data required from db
        try {
            DateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
            Date today = new Date();
            String todayWithZeroTime = formatter1.format(today);
            sanctionReport.setDate(todayWithZeroTime);
            // fetching value from db
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, header.getInstitutionId());
            LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, header.getInstitutionId());
            SanctionConditions sanctionConditions = applicationRepository.fetchSanctionConditions(refId, header.getInstitutionId());
            CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId, header.getInstitutionId());
            RepaymentRequest repaymentRequest = new RepaymentRequest();
            repaymentRequest.setRefId(refId);
            repaymentRequest.setHeader(header);
            Repayment repayment = applicationRepository.fetchRepaymentDetails(repaymentRequest);
            DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(refId, header.getInstitutionId());
            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(refId);
            List<Applicant> insuranceApplicantData = null;
            if(insuranceData != null) {
                insuranceApplicantData = insuranceData.getApplicantList();
            }

            //Setting Fields in sanctionReport
            if(disbursementMemo != null){
                sanctionReport.setDisbursementMemo(disbursementMemo);
            }
            if (goNoGoCustomerApplication != null) {
                sanctionReport.setCurrentStageId(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                sanctionReport.setBranch(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
                Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
                List<CoApplicant> coApplicantList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
                Application application = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication();
                List<Collateral> collateral = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getCollateral();
                sanctionReport.setBranch(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
                sanctionReport.setProduct(goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().name());
                sanctionReport.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
                if(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),GNGWorkflowConstant.DISB.toFaceValue())){
                    sanctionReport.setCurrentDateWithMonths(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(),
                            GngDateUtil.ddMMMyyyy_WITH_HYPHEN));
                    sanctionReport.setCurrentDateInWords(GngDateUtil.getDateIntoWords(loanCharges.getDisbursedDate()));
                }else{
                    sanctionReport.setCurrentDateWithMonths(GngDateUtil.changeDateFormat(new Date(),
                            GngDateUtil.ddMMMyyyy_WITH_HYPHEN));
                    sanctionReport.setCurrentDateInWords(GngDateUtil.getDateIntoWords(new Date()));
                }
                sanctionReport.setCurrentDate(GngDateUtil.changeDateFormat(new Date(),
                        GngDateUtil.ddMMyyyy_WITH_HYPHEN));

                if (applicant != null) {
                    sanctionReport.setApplicant(applicant);
                    if(applicant.getProfessionIncomeDetails() != null) {
                        sanctionReport.setTypeofloan(applicant.getProfessionIncomeDetails().getLoanScheme());
                    }
                    if(! CollectionUtils.isEmpty(applicant.getAddress())){
                        for (CustomerAddress add : applicant.getAddress()) {
                            if(StringUtils.equalsIgnoreCase(add.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())){
                                sanctionReport.setResidentialAddress(appConfigurationHelper.getCompleteAddress(add));
                            }
                        }
                    }
                }
                if (! CollectionUtils.isEmpty(coApplicantList)) {
                    sanctionReport.setCoApplicantList(coApplicantList);
                }

                if(insuranceApplicantData != null) {
                    sanctionReport.setApplicants(insuranceApplicantData);
                }
                sanctionReport.setRefId(goNoGoCustomerApplication.getGngRefId());
                if(application!= null){
                    sanctionReport.setLoanPurpose(application.getLoanPurpose());
                }

                if(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2() != null){
                    sanctionReport.setLocation(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
                }

                if(! CollectionUtils.isEmpty(collateral)){
                    sanctionReport.setCollateralList(collateral);
                }

                if(application != null){
                    //IMD2 for LAP SBFC
                    if(Institute.isInstitute(header.getInstitutionId(),Institute.SBFC) && StringUtils.equalsIgnoreCase(header.getProduct().name(),Product.LAP.name())){
                        InitialMoneyDeposit imd = application.getInitialMoneyDeposit();
                        List<InitialMoneyDeposit> imd2 = application.getImd2();
                        if (imd != null && !StringUtils.isEmpty(imd.getStatus()) && imd.getClearingDate() != null) {
                            sanctionReport.setImdAmt(Integer.toString((int) imd.getAmount()));
                        }else if(imd2 != null){
                            double sum = 0;
                            for(InitialMoneyDeposit initialMoneyDeposit:imd2){
                                if(!StringUtils.isEmpty(initialMoneyDeposit.getStatus()) && initialMoneyDeposit.getClearingDate() != null){
                                    if(StringUtils.equalsIgnoreCase(initialMoneyDeposit.getStatus(),"Cleared")){
                                        sum += initialMoneyDeposit.getAmount();
                                    }
                                }
                            }
                            sanctionReport.setImdAmt(Integer.toString((int) sum));
                        }
                    }else{
                        if (application.getInitialMoneyDeposit() != null) {
                            sanctionReport.setImdAmt(Integer.toString((int) application.getInitialMoneyDeposit().getAmount()));
                        }
                    }
                }

                sanctionReport.setCreditSubmit(header.getLoggedInUserId());
                if(insuranceData != null && StringUtils.isNotEmpty(appId)){
                    populateInsuranceData(insuranceData,appId, insuranceId,sanctionReport);
                }
            }

            // Fields related to camSummary
            if (camDetails != null && camDetails.getSummary() != null) {
                sanctionReport.setCamSummary(camDetails.getSummary());
                sanctionReport.setEmi(Integer.toString((int)camDetails.getSummary().getEmi()));
            }

            if (loanCharges != null) {
                int sanction = 0;
                int sanctionAmtBt = 0;
                if (loanCharges.getLoanDetails() != null) {
                    if (loanCharges.getLoanDetails().getFinalLoanApprovedAmt() > 0) {
                        sanction = (int) loanCharges.getLoanDetails().getFinalLoanApprovedAmt();
                        sanctionReport.setSanctionLoanAmt(Integer.toString(sanction));
                        sanctionReport.setFinalLoanApprovedValueInWords(GngNumUtil.
                                convertToWords(Double.valueOf(loanCharges.getLoanDetails().getFinalLoanApprovedAmt()).longValue()));
                    }
                    if (loanCharges.getTenureDetails().getTopUpEMIAmt() > 0) {
                        sanctionAmtBt = (int) loanCharges.getLoanDetails().getBtLoanAmt();
                        sanctionReport.setSanctionAmtBt(Integer.toString(sanctionAmtBt));
                    }
                    if (sanction != 0 && sanctionAmtBt != 0) {
                        int payableMonthlyAmount = sanction - sanctionAmtBt;
                        sanctionReport.setPayableMonthlyAmount(Integer.toString(payableMonthlyAmount));
                    }
                    if(loanCharges.getDisbursedDate() != null){
                        sanctionReport.setDisbDate(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(),
                                GngDateUtil.ddMMyyyy_WITH_HYPHEN));
                    }
                    if(loanCharges.getLoanDetails().getFinalDisbAmt() > 0){
                        sanctionReport.setNumberValueInWords(GngNumUtil
                        .convertToWords(Double.valueOf(loanCharges.getLoanDetails().getFinalDisbAmt()).longValue()));
                }
                if(loanCharges.getNetDisbAmt() > 0){
                    sanctionReport.setDisbValueInWords(GngNumUtil
                            .convertToWords(Double.valueOf(loanCharges.getNetDisbAmt()).longValue()));
                }

                }
                if(loanCharges.getTenureDetails() != null){
                    if(loanCharges.getTenureDetails().getBtEMIAmt() > 0) {
                        int btEmi = (int) loanCharges.getTenureDetails().getBtEMIAmt();
                        sanctionReport.setBtEmiAmt(Integer.toString(btEmi));

                    }
                    if(loanCharges.getEmiStartDate() != null){
                        sanctionReport.setEmiStartDate(GngDateUtil.changeDateFormat(loanCharges.getEmiStartDate(),
                                GngDateUtil.ddMMyyyy_WITH_HYPHEN));
                    }
                    if(loanCharges.getEmiEndDate() != null){
                        sanctionReport.setEmiEndDate(GngDateUtil.changeDateFormat(loanCharges.getEmiEndDate(),
                                GngDateUtil.ddMMyyyy_WITH_HYPHEN));
                    }
                    if(loanCharges.getTenureDetails().getTopUpEMIAmt() > 0 ) {
                        int topUpEmi = (int) loanCharges.getTenureDetails().getTopUpEMIAmt();
                        sanctionReport.setTopUpEmiAmt(Integer.toString(topUpEmi));

                    }
                    if(loanCharges.getTenureDetails().getDisbTenureMonths() > 0){
                        sanctionReport.setTermOfloans(Integer.toString(loanCharges.getTenureDetails().getDisbTenureMonths()));
                    }
                }
                sanctionReport.setLoanCharges(loanCharges);
            }

            if(repayment != null){
                sanctionReport.setRepayment(repayment);
            }


            if(sanctionConditions != null){
                sanctionReport.setSanctionConditionDetails(sanctionConditions.getScDetailsList());
            }
            if(org.apache.commons.collections.CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())){
                logger.info("scDetailsList{}",sanctionConditions.getScDetailsList());
            }
            if(templateConfiguration != null){
                sanctionReport.setLogoId(templateConfiguration.getLogoId());
            }
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }

        return sanctionReport;
    }

    private void populateInsuranceData(InsuranceData insuranceData, String appId, String insuranceId, SanctionReport sanctionReport) {
        try{
            Applicant applicant = insuranceData.getApplicantList().stream().
                    filter(applicant1 -> StringUtils.equalsIgnoreCase(appId,applicant1.getApplicantId())).findAny().orElse(null);
            if(applicant != null){
                sanctionReport.setApplicant(applicant);
                sanctionReport.setAge(applicant.getAge());
                sanctionReport.setEmail(applicant.getEmail().get(0).getEmailAddress());
                sanctionReport.setMobile(applicant.getPhone().get(0).getPhoneNumber());
                if (org.apache.commons.collections.CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())) {
                    InsurancePolicy insurancePolicy = applicant.getInsurancePolicyList().stream().
                            filter(insurancePolicy1 -> StringUtils.equalsIgnoreCase(insuranceId, insurancePolicy1.getInsuranceId())).findFirst().orElse(null);
                    if (insurancePolicy != null) {
                        if(PolicyHelper.InsuranceCompany.isInsuranceCompany(insurancePolicy.getCompany(), PolicyHelper.InsuranceCompany.ICICI)){
                            if (StringUtils.equalsIgnoreCase(insuranceData.getProduct(),Product.PL.toDisplayName())) {
                                sanctionReport.setInsurenceTypeIcici(insurancePolicy.getProduct());
                            }
                            populateInsuranceData(insurancePolicy, sanctionReport, insuranceData);
                        }else if (PolicyHelper.InsuranceCompany.isInsuranceCompany(insurancePolicy.getCompany(), PolicyHelper.InsuranceCompany.RELIGARE)
                                || PolicyHelper.InsuranceCompany.isInsuranceCompany(insurancePolicy.getCompany(),
                                PolicyHelper.InsuranceCompany.RELIGARE_LOAN_PROTECTION)){
                            populateInsuranceData(insurancePolicy, sanctionReport, insuranceData);
                            sanctionReport.setReligareSumAssured(insurancePolicy.getScheme());
                            if(StringUtils.equalsIgnoreCase(insurancePolicy.getName(), "CO_PAY")){
                                sanctionReport.setCoPayAvailable(true);
                            }
                            applicant.setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                            (GngDateUtil.ddMMyyyy,
                                                    applicant.getDateOfBirth()),
                                    GngDateUtil.ddMMyyyy_WITH_HYPHEN));
                        }
                    }
                }
            }
        }catch(Exception ex){
            logger.info("{} error in Insurance Data", ex.getStackTrace());
        }
    }

    private void populateInsuranceData(InsurancePolicy insurancePolicy, SanctionReport sanctionReport, InsuranceData insuranceData) {
        sanctionReport.setInsurenceTypeIcici(insurancePolicy.getScheme());
        sanctionReport.setPremiumIcici(Double.toString(insurancePolicy.getPremium()));
        sanctionReport.setInsurancePolicy(insurancePolicy);
        NomineeInfo nomineeInfo = (insurancePolicy.getNomineeInfo() != null) ?
                insurancePolicy.getNomineeInfo() : new NomineeInfo();
        if (insurancePolicy.getPremium() != null) {
            sanctionReport.setPremiumIcici(Double.toString(insurancePolicy.getPremium()));
        }
        if (StringUtils.isNotEmpty(nomineeInfo.getDateOfBirth())) {
            nomineeInfo.setTempDob(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                            (GngDateUtil.ddMMyyyy,
                                    nomineeInfo.getTempDob()),
                    GngDateUtil.ddMMyyyy_WITH_HYPHEN));
        }
        if (StringUtils.isNotEmpty(nomineeInfo.getAppointee().getDateOfBirth())) {
            nomineeInfo.getAppointee().setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                            (GngDateUtil.ddMMyyyy,
                                    nomineeInfo.getAppointee().getTempDob()),
                    GngDateUtil.ddMMyyyy_WITH_HYPHEN));
        }
        if (insurancePolicy.getSumAssured() != null) {
            sanctionReport.setSumAssured(insurancePolicy.getSumAssured());
        }
        if(org.apache.commons.collections.CollectionUtils.isNotEmpty(insurancePolicy.getOptDetailsList())){
            for(OptDetails optDetails : insurancePolicy.getOptDetailsList()){
                if(StringUtils.equalsIgnoreCase("Wife", optDetails.getRelationship()) ||
                        StringUtils.equalsIgnoreCase("Husband", optDetails.getRelationship())){
                    if (StringUtils.isNotEmpty(optDetails.getDateOfBirth())) {
                        optDetails.setTempDob(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                        (GngDateUtil.ddMMyyyy,
                                                optDetails.getTempDob()),
                                GngDateUtil.ddMMyyyy_WITH_HYPHEN));
                    }
                    sanctionReport.setOptDetails(optDetails);
                }
            }
        }
        if(nomineeInfo != null && StringUtils.isNotEmpty(nomineeInfo.getName().getFirstName())){
            sanctionReport.setNomineeInfo(nomineeInfo);
        }

    }



    private String getAchMandateFormHtmlDocument(AchMandateForm achMandateForm, TemplateConfiguration templateConfiguration) {
        Template template = new Template();
        StringWriter writer = null;

        try {
            TemplateFactory templatesFactory = new TemplateFactory();

            template = templatesFactory.fetchTemplate(templateConfiguration
                    .getTemplatePath());

            VelocityContext context = AchMandateFormMapper.mapAchMandateForm(achMandateForm);

            writer = new StringWriter();
            template.merge(context, writer);

            return writer.toString();
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;

        } finally {
            writer.flush();
            try {
                if (null != writer)
                    writer.close();
            } catch (IOException e) {
            }

        }

    }

}
