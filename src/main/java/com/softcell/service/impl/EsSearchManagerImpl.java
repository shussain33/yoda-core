package com.softcell.service.impl;

import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.elsearch.IndexingElasticSearchRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.queuemanager.search.SearchESRepository;
import com.softcell.queuemanager.search.request.ESQueueRequest;
import com.softcell.service.EsSearchManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;

/**
 * @author kishorp
 */
@Service
public class EsSearchManagerImpl implements EsSearchManager {

    private static final Logger logger = LoggerFactory.getLogger(EsSearchManagerImpl.class);

    @Override
    public BaseResponse croQueue(ESQueueRequest esQueueRequest) {

        SearchESRepository searchESRepository = new SearchESRepository();

        if (null != esQueueRequest && null != esQueueRequest.getHeader()
                && StringUtils.isNotBlank(esQueueRequest.getHeader().getInstitutionId())) {

            try {

                return GngUtils.getBaseResponse(HttpStatus.OK ,searchESRepository.booleanSearch(esQueueRequest,
                        Cache.getClient()));

            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse updateDatabaseIndex(String startDate, String endDate, String instituionId) {
        GenericResponse genericResponse = new GenericResponse();
        try {

            IndexingElasticSearchRepository indexingElasticSearchRepository = new IndexingElasticSearchRepository();

            if (indexingElasticSearchRepository.indexData(startDate, endDate, instituionId))
                genericResponse.setStatus(Status.SUCCESS.name());
            else {
                genericResponse.setStatus(Status.FAIL.name());
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(" error occurred while reindexing data in elasticsearch service with probable cause [{}]" , e.getMessage());
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT.OK, genericResponse);
    }

}
