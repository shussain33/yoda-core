package com.softcell.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.ScoringCommunication;
import com.softcell.config.ScoringConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.BreExecRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.digitalcore.EligibilityData;
import com.softcell.gonogo.model.response.LoginDataResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.factory.impl.ScoringJsonBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.BreExecManager;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.RequestAggregator;
import com.softcell.workflow.aggregators.WfResultAggregatorImpl;
import com.softcell.workflow.executors.kyc.AadharExecutor;
import com.softcell.workflow.executors.kyc.PanVerificationExecutor;
import com.softcell.workflow.executors.multibureau.MultiBureauExecutor;
import com.softcell.workflow.executors.sobre.ScoringExecutor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by yogesh on 6/2/18.
 */
@Service
public class BreExecManagerImpl implements BreExecManager {

    private static Logger logger = LoggerFactory.getLogger(BreExecManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN.get(CacheConstant.BEAN_FACTORY);

    @Override
    public BaseResponse verifyKyc(ApplicationRequest applicationRequest,HttpServletRequest httpServletRequest, String kycName) {

        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(),applicationRequest.getRefID()
                ,httpServletRequest,null,null,null,applicationRequest.getHeader().getLoggedInUserId());
        try {
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }
        switch (kycName) {
            case EndPointReferrer.PAN:
                PanVerificationExecutor panVerificationExecutor = new PanVerificationExecutor();
                // set the pan related info in gongoapp from appRequest
                goNoGoCustomerApplication = buildGonogoCustomerApplication(goNoGoCustomerApplication, applicationRequest, KYC_TYPES.PAN.name());
                // set gonogo in executor
                panVerificationExecutor.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
                // execute pan service
                panVerificationExecutor.run();
                break;

            case EndPointReferrer.AADHAAR:
                AadharExecutor aadharExecutor = new AadharExecutor();
                goNoGoCustomerApplication = buildGonogoCustomerApplication(goNoGoCustomerApplication, applicationRequest, KYC_TYPES.AADHAR.name());
                // set gonogo in executor
                aadharExecutor.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
                // execute aadhar service
                aadharExecutor.run();
                break;
            default:

        }
        // TODO return the required attributes from gonogoapp
        return GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
    }

    private static GoNoGoCustomerApplication buildGonogoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication,
                                                                            ApplicationRequest applicationRequest, String kycType) {

        List<Kyc> gonogoKycList = null;
        // collecting only specific kyc info in kyclist from appRequest.
        List<Kyc> applicantKycList = applicationRequest.getRequest().getApplicant().getKyc();
        List<Kyc> kycList1 = applicantKycList
                                .stream()
                                .filter(kyc -> kycType.equals(kyc.getKycName()) && kyc.getKycNumber() != null)
                                .collect(Collectors.toList());

        if (!kycList1.isEmpty()) {
            gonogoKycList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc();
            for (Kyc kyc : gonogoKycList) {
                if (kyc.getKycName().equalsIgnoreCase(kycType)) {
                    // use only fist element
                    // clear other attributes
                    kyc.setKycNumber(kycList1.get(0).getKycNumber());
                    kyc.setExpiryDate(kycList1.get(0).getExpiryDate());
                    kyc.setIssueDate(kycList1.get(0).getIssueDate());
                    kyc.setKycStatus(kycList1.get(0).isKycStatus());
                    break;
                }
            }
        }
        return goNoGoCustomerApplication;
    }

    @Override
    public BaseResponse callMultibureau(BreExecRequest breExecRequest, HttpServletRequest httpServletRequest) {

        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(breExecRequest.getHeader(),breExecRequest.getRefID()
                ,httpServletRequest,null,null,null,breExecRequest.getHeader().getLoggedInUserId());
        try {
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(breExecRequest.getRefID());
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }
        MultiBureauExecutor multiBureauExecutor = new MultiBureauExecutor();
        multiBureauExecutor.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
        multiBureauExecutor.setExecuteForCoApplicant(true);
        multiBureauExecutor.setLoggedInUserRole(breExecRequest.getHeader().getLoggedInUserRole());
        if( StringUtils.isEmpty(breExecRequest.getApplicantId())){
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,"Provide applicant id");
        }
        multiBureauExecutor.runForApplicant(breExecRequest.getApplicantId());
        // Call sobre
        logger.info("MB done for applicant {} for {}", breExecRequest.getApplicantId(), breExecRequest.getRefID());
        executeSobre(breExecRequest, httpServletRequest);
        activityEventPublisher.publishEvent(activityLogs);
        return GngUtils.getBaseResponse(HttpStatus.OK,goNoGoCustomerApplication);
    }

    @Override
    public BaseResponse executeSobre(BreExecRequest breExecRequest, HttpServletRequest httpServletRequest) {
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
       ActivityLogs activityLogs = auditHelper.createActivityLog(breExecRequest.getHeader(),breExecRequest.getRefID()
                ,httpServletRequest,null,null,null,breExecRequest.getHeader().getLoggedInUserId());
        try {
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(breExecRequest.getRefID());
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("{}",e.getStackTrace());
            throw new SystemException(String.format("error occured while fetching gonogo customer application by ref id with probable cause [%s]", e.getMessage()));
        }
        WfResultAggregatorImpl workflowAggregator = new WfResultAggregatorImpl();
        ScoringExecutor scoringExecutor =  new ScoringExecutor(workflowAggregator);
        scoringExecutor.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
        scoringExecutor.sobreCall(breExecRequest.getApplicantId());
        return GngUtils.getBaseResponse(HttpStatus.OK,goNoGoCustomerApplication);
    }

    private GoNoGoCustomerApplication getGoNoGo(LoginDataRequest loginDataRequest){

        Application application = new Application();
        application.setLoanType(loginDataRequest.getHeader().getProduct().name());

        Applicant applicant = new Applicant();
        applicant.setApplicantId("0");

        Request request= new Request();
        request.setApplication(application);
        request.setApplicant(applicant);

        ApplicationRequest applicationRequest = new ApplicationRequest();
        applicationRequest.setHeader(loginDataRequest.getHeader());
        applicationRequest.setRequest(request);

        GoNoGoCustomerApplication goNoGoCustomerApplication = new GoNoGoCustomerApplication();
        goNoGoCustomerApplication.setApplicationRequest(applicationRequest);
        return goNoGoCustomerApplication;
    }

    @Override
    public BaseResponse executeSobre(LoginDataRequest loginDataRequest , HttpServletRequest httpServletRequest){
        BaseResponse baseResponse = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication=null;
        String refId = loginDataRequest.getRefId();
        try{
            if(StringUtils.isNotEmpty(refId))
                goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);

            if(null == goNoGoCustomerApplication)
                goNoGoCustomerApplication = getGoNoGo(loginDataRequest);

            baseResponse = generateRequestAndCallSobre(loginDataRequest , goNoGoCustomerApplication, "EligibleOffers", false);
        }catch(Exception e){
            logger.error(e.getMessage());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }
        return baseResponse;
    }

    private EligibilityDetails mappingEligibilityDetails(EligibilityData eligibilityData)throws Exception{
        String tenure = "0";
        if (StringUtils.isNotEmpty(eligibilityData.getAppliedTenor())) tenure = eligibilityData.getAppliedTenor();

        EligibilityDetails eligibilityDetails=new EligibilityDetails();
        BeanUtils.copyProperties(eligibilityDetails, eligibilityData);
        eligibilityDetails.setAppliedTenor(tenure);
        eligibilityDetails.setFixedOblgation(eligibilityData.getFixedOblgation());
        return eligibilityDetails;
    }
    private String connectSobre(String valueJson, ScoringCommunication scoringCommunication) throws IOException {
        String url = scoringCommunication.getScoringServiceUrl();
        StringBuilder endpoint = new StringBuilder(url) ;
        endpoint.append("/processApplication?")
                .append("INSTITUTION_ID=").append(scoringCommunication.getInstitutionId())
                .append("&USER_ID=").append(scoringCommunication.getUserId())
                .append("&PASSWORD=").append(scoringCommunication.getPassword());

        logger.debug("Sending SOBRE objectMapper for url {} is {}",endpoint.toString(), valueJson);
        String response = new HttpTransportationService().postRequest(endpoint.toString(), valueJson, MediaType.APPLICATION_JSON_VALUE);
        if(response != null) {
            response = response.replaceAll("customerId", "sApplID");
        }
        return response;
    }

    private BaseResponse generateRequestAndCallSobre(LoginDataRequest loginDataRequest, GoNoGoCustomerApplication goNoGoCustomerApplication, String policyName, boolean saveReqRes ) throws Exception{
        BaseResponse baseResponse = null;
        ScoringCommunication scoringCommunication = null;
        String instituteId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        EligibilityDetails eligibilityDetails = null;
        if (null != loginDataRequest.getEligibilityData())
            eligibilityDetails = mappingEligibilityDetails(loginDataRequest.getEligibilityData());

        if (null == eligibilityDetails)
            eligibilityDetails = new EligibilityDetails();

        goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setEligibilityDetails(eligibilityDetails);
        goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setLoanAmount(loginDataRequest.getEligibilityData().getAppliedLoan());
        goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setTenorRequested(loginDataRequest.getEligibilityData().getAppliedTenor());

        if (instituteId == null) {
            logger.info("Institution id not provided !");
            throw new Exception("Institution id not provided !");
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instituteId);
            if (scoringCommunication == null) {
                logger.info("{} - Scoring not configured for institute {} ! Cannot execute scoring for {}", instituteId, goNoGoCustomerApplication.getGngRefId());
                throw new Exception("Scoring not configured for institute");
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
        ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantScoringJSonVersion2(goNoGoCustomerApplication);
        scoringApplicantRequest.setPolicyName(policyName);
        scoringApplicantRequest.getApplication().setPolicyName(policyName);
        scoringApplicantRequest.getApplication().setCallDetails("Eligibility1");
        logger.debug("SOBRE objectMapper formed for {} as {}", goNoGoCustomerApplication.getGngRefId(), scoringApplicantRequest);

        ObjectMapper objectMapper = new ObjectMapper();

        String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
        valueJson = valueJson.replaceAll("sApplID", "customerId");
        scoringApplicantRequest.setRequest(valueJson);
        if(saveReqRes)
            moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);

        String response = connectSobre(valueJson, scoringCommunication);
        response = response.replaceAll("customerId", "sApplID");
        logger.debug("{} SOBRE response received as {}", goNoGoCustomerApplication.getGngRefId(), response);

        if(saveReqRes)
            saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());

        ScoringApplicantResponse scoringApplicantResponse = null;
        if (StringUtils.isNotEmpty(response)) {
            scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
        }
        saveSobreRequestResponse(scoringApplicantRequest,scoringApplicantResponse);
        if (null != scoringApplicantResponse) {
            final String applicantId = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId();
            ScoringApplicantResponse primaryResponse = new ScoringApplicantResponse();
            primaryResponse.setSummary(scoringApplicantResponse.getSummary());
            primaryResponse.setApplicantResult(scoringApplicantResponse.getApplicantResult().stream().filter(result ->
                    StringUtils.equalsIgnoreCase(applicantId, result.getApplicantId()))
                    .collect(Collectors.toList()));
            ScoringResponse scoringResponse = new ScoringResponse();
            scoringResponse.setApplicantResponse(primaryResponse);
            if (null != scoringResponse.getApplicantResponse().getSummary()){
                goNoGoCustomerApplication.setApplicationStatus(scoringResponse.getApplicantResponse().getSummary().getApplicantDecesion());
            }
            goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResponse);
            LoginDataResponse loginDataResponse = new LoginDataResponse();
            loginDataResponse.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loginDataResponse);
        } else {
            logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
            String errorMsg = String.format("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(errorMsg)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
        }
        return baseResponse;
    }

    protected void saveRawResponse(String rawResponse, String refId) {
        logger.debug("Saving data for {}", ActionName.SOBRE_RESPONSE_SAVE);
        RawResponseLog rawResponseLog = RawResponseLog.builder().rawResponse(rawResponse)
                .refId(refId)
                .responseType(ActionName.SOBRE_RESPONSE_SAVE).build();

        externalAPILogRepository.saveRawResonseLog(rawResponseLog);
    }

    private void saveSobreRequestResponse(ScoringApplicantRequest scoringApplicantRequest, ScoringApplicantResponse scoringApplicantResponse) {
        try {
            ScoringCallLog callLog = new ScoringCallLog();
            callLog.setRefId(scoringApplicantRequest.getHeader().getApplicationId());
            callLog.setStep(scoringApplicantRequest.getPolicyName());
            callLog.setInstitutionId(scoringApplicantRequest.getHeader().getInstitutionId());
            callLog.setCallDate(new Date());
            callLog.setRequestString(JsonUtil.ObjectToString(scoringApplicantRequest));
            callLog.setRequest(scoringApplicantRequest);

            if (scoringApplicantResponse != null) {
                callLog.setResponseString(JsonUtil.ObjectToString(scoringApplicantResponse));
                callLog.setResponse(scoringApplicantResponse);
            }
            callLog.setRequestType(EndPointReferrer.GET_SCORING_LOG);
            externalAPILogRepository.saveSobreCallLog(callLog);
        } catch (Exception e) {
            logger.error("Exception occured while saving sobreLogs {}", e.getStackTrace());
        }
    }
}


