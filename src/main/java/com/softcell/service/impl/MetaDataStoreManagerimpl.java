package com.softcell.service.impl;

import com.softcell.constants.Institute;
import com.softcell.dao.mongodb.repository.MasterDataRepository;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.SchemeMetadataRequest;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import com.softcell.gonogo.model.response.ModelVariantMasterResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.service.MetaDataStoreManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.TreeSet;
/**
 * @author yogeshb
 */
@Service
public class MetaDataStoreManagerimpl implements MetaDataStoreManager {

    private static final Logger logger = LoggerFactory.getLogger(MetaDataStoreManagerimpl.class);

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Override
    public List<SchemeMasterData> getFilteredSchemes(
            SchemeMetadataRequest schemeMetadataRequest) {
        logger.info("Inside filtered-scheme-master");

        String modelNo = schemeMetadataRequest.getModelNo();
        String maufacturerDesc = schemeMetadataRequest.getManufacturerDesc();
        String catgDesc = schemeMetadataRequest.getCatgDesc();
        String dealerID = schemeMetadataRequest.getHeader().getDealerId();

        DealerEmailMaster dealerEmailMaster = masterDataRepository
                .getDealerDetails(schemeMetadataRequest.getHeader()
                        .getInstitutionId(), dealerID);
        if (null != dealerEmailMaster) {
            String city = dealerEmailMaster.getCityId();
            String state = dealerEmailMaster.getStateId();

            SchemeMasterFliter schemeMasterFliter = new SchemeMasterFliter();
            AssetModelMaster assetModelMaster = masterDataRepository
                    .getAssetModelMaster(modelNo, catgDesc, maufacturerDesc);
            // Non- Vanilla models
            if (!StringUtils.contains(modelNo, "VANILLA")
                    && StringUtils.isNotBlank(modelNo)) {
                // Get Scheme against manufacturer
                /**
                 * 1.Non Vanilla Schemes
                 */
                try {
                    schemeMasterFliter = getNonVanilla(assetModelMaster);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                // Vanilla models
                try {
                    schemeMasterFliter = masterDataRepository
                            .getVanillaSchemeMasterDetails("");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            Set<String> filterSchemeIdSet = new HashSet<>();
            addToSet(schemeMasterFliter, filterSchemeIdSet);

            if (!filterSchemeIdSet.isEmpty()) {
                assetModelMaster = getAssetModelMaster(modelNo, catgDesc,
                        maufacturerDesc);

                List<SchemeModelDealerCityMapping> schemeModelDealerCityMappingList = masterDataRepository
                        .getSchemeMappingList(filterSchemeIdSet,
                                assetModelMaster.getModelId(), city);
                Set<String> finalSchemeIds = new HashSet<String>();

                if (schemeModelDealerCityMappingList.isEmpty()) {
                    for (String schemeId : filterSchemeIdSet) {
                        finalSchemeIds.add(schemeId);
                    }
                } else {
                    schemeModelDealerCityMappingList = masterDataRepository
                            .getSchemeMappingList(filterSchemeIdSet,
                                    assetModelMaster.getModelId(), city,
                                    dealerID);
                }

                if (schemeModelDealerCityMappingList.isEmpty()) {
                    schemeModelDealerCityMappingList = getSchemeMappingExcluded(
                            filterSchemeIdSet, assetModelMaster.getModelId(),
                            city, dealerID);
                }
                Set<String> removedSchemeIds = new HashSet<String>();
                if (!schemeModelDealerCityMappingList.isEmpty()) {
                    for (SchemeModelDealerCityMapping schemeModelDealerCityMapping : schemeModelDealerCityMappingList) {
                        // If City is ALL, check if the current city is in
                        // Exclusion
                        // List
                        // If State is ALL, check if the current state is in
                        // Exclusion List
                        finalSchemeIds = checkStateSchemeMapping(
                                schemeModelDealerCityMapping, state,
                                finalSchemeIds, removedSchemeIds);
                        finalSchemeIds = checkCityMapping(
                                schemeModelDealerCityMapping, city,
                                finalSchemeIds, removedSchemeIds);
                        finalSchemeIds = checkDealerIdSchemeMapping(
                                schemeModelDealerCityMapping, dealerID,
                                finalSchemeIds, removedSchemeIds);
                        finalSchemeIds.removeAll(removedSchemeIds);
                        // If Dealer is ALL, check if the current dealer is in
                        // Exclusion List
                    }

                }

                String[] schemeIdArray = finalSchemeIds
                        .toArray(new String[finalSchemeIds.size()]);

                Set<String> finalDateFilteredSchemeIds = null;
                if (schemeIdArray.length > 0) {
                    finalDateFilteredSchemeIds = getFinalDateFilteredScheme(schemeIdArray);
                }
                if (null != finalDateFilteredSchemeIds
                        && finalDateFilteredSchemeIds.size() > 0) {
                    String schemes[] = finalDateFilteredSchemeIds
                            .toArray(new String[finalDateFilteredSchemeIds
                                    .size()]);
                    List<SchemeMasterData> manaufacturerSchemeMasters = masterDataRepository
                            .getSchemeMasterData(schemes);
                    if (manaufacturerSchemeMasters != null
                            && !manaufacturerSchemeMasters.isEmpty()) {
                        return manaufacturerSchemeMasters;
                    } else {
                        if (schemeMasterFliter.getWithoutFilterSchemeData() != null)
                            schemeMasterFliter.getWithoutFilterSchemeData();
                    }
                }
            }
            schemeMasterFliter = null;
            schemeMasterFliter = masterDataRepository
                    .getWithoutCcIdSchemeMasterDetails();
            return schemeMasterFliter.getWithoutFilterSchemeData();
        } else {
            List<SchemeMasterData> schemeMasterDataList = new ArrayList<SchemeMasterData>();
            logger.error("Dealer not available in Dealer Email Master, we couldn't render schemes.");
            return schemeMasterDataList;
        }

    }

    private Set<String> getFinalDateFilteredScheme(String[] schemeIdArray) {
        // 3. From Scheme-Validity Mapping, get the subset of schemeids
        // which are valid on the date of submission.
        Set<String> finalDateFilteredSchemeIds = new HashSet<String>();
        List<SchemeDateMapping> schemeDateMappingList = masterDataRepository
                .getSchemeDateMapping(schemeIdArray);
        for (SchemeDateMapping schemeDateMapping : schemeDateMappingList) {
            String validFrom = schemeDateMapping.getValidFromDate();
            String validTo = schemeDateMapping.getValidToDate();
            // Check current date is between two dates.
            if (GngDateUtil.isWithinRange(validFrom, validTo)) {
                // Check if the current date is in exclusion list
                String excludedDates = schemeDateMapping.getExcludedDate();
                String includedDates = schemeDateMapping.getIncludedDate();
                if (StringUtils.isNotBlank(excludedDates)) {
                    List<String> excludedDatesList = Arrays.asList(StringUtils
                            .split(excludedDates, '$'));
                    if (excludedDatesList.size() > 0) {
                        for (String excludedDate : excludedDatesList) {
                            if (GngDateUtil.isDateMatch(excludedDate)) {
                                // current date is in exclusion list
                                finalDateFilteredSchemeIds
                                        .remove(schemeDateMapping.getSchemeID());
                                break;
                            }
                        }
                    } else {
                        finalDateFilteredSchemeIds.add(schemeDateMapping
                                .getSchemeID());
                    }
                } else if (StringUtils.isNotBlank(includedDates)) {
                    List<String> includedDatesList = Arrays.asList(StringUtils
                            .split(includedDates, '$'));
                    if (includedDatesList.size() > 0) {
                        for (String includedDate : includedDatesList) {
                            if (GngDateUtil.isDateMatch(includedDate)) {
                                // current date is in inclusion list
                                finalDateFilteredSchemeIds
                                        .add(schemeDateMapping.getSchemeID());
                                break;
                            }
                        }
                    } else {
                        finalDateFilteredSchemeIds.add(schemeDateMapping
                                .getSchemeID());
                    }
                } else {
                    finalDateFilteredSchemeIds.add(schemeDateMapping
                            .getSchemeID());
                }
            }
        }
        return finalDateFilteredSchemeIds;
    }

    private Set<String> checkDealerIdSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String dealerID, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        if (StringUtils.equalsIgnoreCase("All",
                StringUtils.trim(schemeModelDealerCityMapping.getDealerId()))) {
            return checkAllDealerIdSchemeMapping(schemeModelDealerCityMapping,
                    dealerID, finalSchemeIds, removedSchemeIds);
        } else {
            return checkSelectedDealerIdSchemeMapping(
                    schemeModelDealerCityMapping, dealerID, finalSchemeIds,
                    removedSchemeIds);
        }
    }

    private Set<String> checkAllDealerIdSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String dealerID, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {

        String excludedDealerIDs = schemeModelDealerCityMapping
                .getExcludedDealerID();
        if (StringUtils.isNotBlank(excludedDealerIDs)) {
            List<String> excludedDealerList = Arrays.asList(StringUtils.split(
                    excludedDealerIDs, '$'));
            if (excludedDealerList != null && excludedDealerList.size() > 0) {
                for (String excludedDealer : excludedDealerList) {
                    if (StringUtils.equalsIgnoreCase(dealerID,
                            StringUtils.trim(excludedDealer))) {
                        finalSchemeIds.remove(schemeModelDealerCityMapping
                                .getSchemeID());
                        removedSchemeIds.add(schemeModelDealerCityMapping
                                .getSchemeID());
                        break;
                    }
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else if (StringUtils.isNotBlank(excludedDealerIDs)) {
            List<String> excludedDealerList = Arrays.asList(StringUtils.split(
                    excludedDealerIDs, '$'));
            if (excludedDealerList != null && excludedDealerList.size() > 0) {
                for (String excludedDealer : excludedDealerList) {
                    if (StringUtils.equalsIgnoreCase(dealerID,
                            StringUtils.trim(excludedDealer))) {
                        finalSchemeIds.remove(schemeModelDealerCityMapping
                                .getSchemeID());
                        break;
                    }
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }
        return finalSchemeIds;
    }

    private Set<String> checkSelectedDealerIdSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String dealerId, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {

        // dealer has no value like ALL
        // What to do?
        String includedDealers = schemeModelDealerCityMapping
                .getIncludedStateID();
        boolean foundFlag = false;
        if (StringUtils.isNotBlank(includedDealers)) {
            List<String> includedDealerList = Arrays.asList(StringUtils.split(
                    includedDealers, '$'));
            if (includedDealerList != null && includedDealerList.size() > 0) {
                for (String includedDealer : includedDealerList) {
                    if (StringUtils.equalsIgnoreCase(dealerId,
                            StringUtils.trim(includedDealer))) {
                        foundFlag = true;
                        break;
                    }
                }
                if (!foundFlag) {
                    finalSchemeIds.remove(schemeModelDealerCityMapping
                            .getSchemeID());
                    removedSchemeIds.add(schemeModelDealerCityMapping
                            .getSchemeID());
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }

        return finalSchemeIds;
    }

    private Set<String> checkStateSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String state, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        if (StringUtils.equalsIgnoreCase("All",
                StringUtils.trim(schemeModelDealerCityMapping.getStateID()))) {
            return checkALLStateSchemeMapping(schemeModelDealerCityMapping,
                    state, finalSchemeIds, removedSchemeIds);
        } else {
            return checkSelectedStateSchemeMapping(
                    schemeModelDealerCityMapping, state, finalSchemeIds,
                    removedSchemeIds);
        }
    }

    private Set<String> checkSelectedStateSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String state, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        // StateID has no value like ALL
        // What to do?
        String includedStates = schemeModelDealerCityMapping
                .getIncludedStateID();
        boolean foundFlag = false;
        if (StringUtils.isNotBlank(includedStates)) {
            List<String> includedStateList = Arrays.asList(StringUtils.split(
                    includedStates, '$'));
            if (includedStateList != null && includedStateList.size() > 0) {
                for (String includedState : includedStateList) {
                    if (StringUtils.equalsIgnoreCase(state,
                            StringUtils.trim(includedState))) {
                        foundFlag = true;
                        break;
                    }
                }
                if (!foundFlag) {
                    finalSchemeIds.remove(schemeModelDealerCityMapping
                            .getSchemeID());
                    removedSchemeIds.add(schemeModelDealerCityMapping
                            .getSchemeID());
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }
        return finalSchemeIds;
    }

    private Set<String> checkALLStateSchemeMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String state, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        String excludedStates = schemeModelDealerCityMapping
                .getExcludedStateID();

        if (StringUtils.isNotBlank(excludedStates)) {
            List<String> excludedStateList = Arrays.asList(StringUtils.split(
                    excludedStates, '$'));
            if (excludedStateList != null && excludedStateList.size() > 0) {
                for (String excludedState : excludedStateList) {
                    if (StringUtils.equalsIgnoreCase(state,
                            StringUtils.trim(excludedState))) {
                        finalSchemeIds.remove(schemeModelDealerCityMapping
                                .getSchemeID());
                        removedSchemeIds.add(schemeModelDealerCityMapping
                                .getSchemeID());
                        break;
                    }
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }
        return finalSchemeIds;
    }

    private Set<String> checkCityMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String city, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        if (StringUtils.equalsIgnoreCase("All",
                StringUtils.trim(schemeModelDealerCityMapping.getCityID()))) {
            return checkCityMappingAllCity(schemeModelDealerCityMapping, city,
                    finalSchemeIds, removedSchemeIds);
        } else {
            return checkCityMappingSelectedCity(schemeModelDealerCityMapping,
                    city, finalSchemeIds, removedSchemeIds);
        }
    }

    private Set<String> checkCityMappingSelectedCity(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String city, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {

        // CityID has no value like ALL
        // What to do?
        String includedCities = schemeModelDealerCityMapping
                .getIncludedCityID();
        boolean foundFlag = false;
        if (StringUtils.isNotBlank(includedCities)) {
            List<String> includedCityList = Arrays.asList(StringUtils.split(
                    includedCities, '$'));
            if (includedCityList != null && includedCityList.size() > 0) {
                for (String includedCity : includedCityList) {
                    if (StringUtils.equalsIgnoreCase(city,
                            StringUtils.trim(includedCity))) {
                        foundFlag = true;
                        break;
                    }
                }
                if (!foundFlag) {
                    finalSchemeIds.remove(schemeModelDealerCityMapping
                            .getSchemeID());
                    removedSchemeIds.add(schemeModelDealerCityMapping
                            .getSchemeID());
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }
        return finalSchemeIds;

    }

    private Set<String> checkCityMappingAllCity(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping,
            String city, Set<String> finalSchemeIds,
            Set<String> removedSchemeIds) {
        String excludedCities = schemeModelDealerCityMapping
                .getExcludedCityID();
        if (StringUtils.isNotBlank(excludedCities)) {
            List<String> excludedCityList = Arrays.asList(StringUtils.split(
                    excludedCities, '$'));
            if (excludedCityList != null && excludedCityList.size() > 0) {
                for (String excludedCity : excludedCityList) {
                    if (StringUtils.equalsIgnoreCase(city,
                            StringUtils.trim(excludedCity))) {
                        finalSchemeIds.remove(schemeModelDealerCityMapping
                                .getSchemeID());
                        removedSchemeIds.add(schemeModelDealerCityMapping
                                .getSchemeID());
                        break;
                    }
                }
            } else {
                // No record found in exclusion list
                // What to do?
                finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
            }
        } else {
            // No record found in exclusion list
            // What to do?
            finalSchemeIds.add(schemeModelDealerCityMapping.getSchemeID());
        }
        return finalSchemeIds;
    }

    private List<SchemeModelDealerCityMapping> getSchemeMappingExcluded(
            Set<String> filterSchemeIdSet, String modelId, String city,
            String dealerID) {
        return masterDataRepository.getSchemeMappingExcludedList(
                filterSchemeIdSet, modelId, city, dealerID);
    }

    private AssetModelMaster getAssetModelMaster(String modelNo,
                                                 String catgDesc, String maufacturerDesc) {
        return masterDataRepository.getAssetModelMaster(modelNo, catgDesc,
                maufacturerDesc);
    }

    private void addToSet(SchemeMasterFliter schemeMasterFliter,
                          Set<String> schemeIdSet) {
        if (!schemeMasterFliter.getWithFilterSchemeData().isEmpty()) {
            for (SchemeMasterData schemeMaster : schemeMasterFliter
                    .getWithFilterSchemeData()) {
                schemeIdSet.add(schemeMaster.getSchemeID());
            }
        }

    }

    private SchemeMasterFliter getNonVanilla(AssetModelMaster assetModelMaster) {
        return masterDataRepository.getCcIdSchemeMasterDetails(assetModelMaster
                .getManufacturerId());
    }

    @Override
    public BaseResponse getBankDetails(String instId) {

        BaseResponse baseResponse;
        List<BankDetailsMaster> bankDetailsMastersList;

        bankDetailsMastersList = masterDataRepository.getBankDetailsMaster(instId);

        if (null != bankDetailsMastersList && !bankDetailsMastersList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankDetailsMastersList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCarSurrogateMaster(String instId,
                                              String queryString) {
        BaseResponse baseResponse;
        List<CarSurrogateMaster> carSurrogateMasterList;

        carSurrogateMasterList = masterDataRepository.getCarSurrogateMaster(instId, queryString);

        if (null != carSurrogateMasterList && !carSurrogateMasterList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, carSurrogateMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getEmployerMasterDetails(String instId, String queryString) {

        BaseResponse baseResponse;

        List<EmployerMaster> employerMasterList = masterDataRepository.getEmployerMaster(instId, queryString).collect(Collectors.toList());

        if (null != employerMasterList && !employerMasterList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, employerMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getPinCodeDetails(String instId, String queryString) {

        BaseResponse baseResponse;
        PinCodeMaster pinCodeMaster;

        pinCodeMaster = masterDataRepository.getPostalCodeDetails(instId, queryString);

        if (null != pinCodeMaster) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, pinCodeMaster);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getAssetMaster(String instId, String category) {

        List<AssetModelMaster> assetModelMasterList = masterDataRepository
                .getAssetMasterWeb(instId, category);
        Set<String> assetSet = new HashSet<String>();
        BaseResponse baseResponse;

        if (null != assetModelMasterList && !assetModelMasterList.isEmpty()) {
            for (AssetModelMaster asset : assetModelMasterList) {
                assetSet.add(asset.getManufacturerDesc());
            }
        }

        if (null != assetSet && !assetSet.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assetSet);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getModelVariantManufacturer(
            ModelVariantMasterRequest request) {

        ModelVariantMasterResponse modelVariantMasterResponse = new ModelVariantMasterResponse();

        Set<String> modelVariants = masterDataRepository
                .getModelVariantManufacturer(request);
        if (modelVariants != null && !modelVariants.isEmpty()) {
            switch (ModelVariantMasterRequest.RequestType.valueOf(request.getRequestType())) {
                case MANUFACTURER:
                    modelVariantMasterResponse
                            .setManufacturers(modelVariants);
                    break;
                case MODEL:
                    modelVariantMasterResponse
                            .setModels(modelVariants);
                    break;
                case VARIANT:
                    modelVariantMasterResponse
                            .setVariants(modelVariants);
                    break;
            }
            return GngUtils.getBaseResponse(HttpStatus.OK, modelVariantMasterResponse);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getReferenceDetails(ModelVariantMasterRequest modelVariantMasterRequest) {
        BaseResponse baseResponse;
        List<ReferenceDetailsMaster> referenceDetailsMasters = masterDataRepository.getReferenceRelationMasterDetails(modelVariantMasterRequest);
        if (null != referenceDetailsMasters && !referenceDetailsMasters.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, referenceDetailsMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankNames(String institutionId) {
        BaseResponse baseResponse;
        List<BankDetailsMaster> bankingDetailsMasters = masterDataRepository.getBankNames(institutionId);
        if(null != bankingDetailsMasters && !bankingDetailsMasters.isEmpty()){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getStates(String institutionId, String bank) {
        BaseResponse baseResponse;
        List<BankDetailsMaster> bankingDetailsMasters = masterDataRepository.getStatesByBankName(institutionId,bank);
        if(null != bankingDetailsMasters && !bankingDetailsMasters.isEmpty()){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getDistrict(String institutionId, String bank, String state) {
        BaseResponse baseResponse;
        List<BankDetailsMaster> bankingDetailsMasters = masterDataRepository.getBankDistrict(institutionId,bank,state);
        if(null != bankingDetailsMasters && !bankingDetailsMasters.isEmpty()){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBranches(String institutionId, String bank, String state, String district) {
        BaseResponse baseResponse;
        List<BankDetailsMaster> bankingDetailsMasters = masterDataRepository.getBankBranches(institutionId,bank,state,district);
        if(null != bankingDetailsMasters && !bankingDetailsMasters.isEmpty()){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankMasterDetails(String institutionId, String bank, String state, String district, String branch) {
        BaseResponse baseResponse;
        BankDetailsMaster bankingDetailsMaster = masterDataRepository.getBankMasterDetails(institutionId,bank,state,district,branch);
        if(null != bankingDetailsMaster){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMaster);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankMasterDetailsByIfscCode(String institutionId, String ifsc) {
        BaseResponse baseResponse;
        BankDetailsMaster bankingDetailsMaster = masterDataRepository.getBankMasterDetailsByIfscCode(institutionId,ifsc);
        if(null != bankingDetailsMaster){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMaster);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankMasterDetailsByMicrCode(String institutionId, String micr) {
        BaseResponse baseResponse;
        BankDetailsMaster bankingDetailsMaster = masterDataRepository.getBankMasterDetailsByMicrCode(institutionId,micr);
        if(null != bankingDetailsMaster){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankingDetailsMaster);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankNamesFromLosMaster(String institutionId, String bankName) {

        BaseResponse baseResponse;

        logger.debug("Fetching bank name {} from cgpm master", bankName);
        Set<String> cgpmMasterList = masterDataRepository.getBankNameFromCGPM(bankName);

        if(null != cgpmMasterList && !cgpmMasterList.isEmpty()){

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, cgpmMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAccountType(String institutionId) {
        BaseResponse baseResponse;
        Set<String> accountTypeFromCGPM = masterDataRepository.getAccountTypeFromCGPM(institutionId);

        if(!CollectionUtils.isEmpty(accountTypeFromCGPM)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, accountTypeFromCGPM);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getBankNamesFromBankMaster(String institutionId, String bankName) {
        BaseResponse baseResponse;

        logger.debug("Fetching bank name {} from bank master", bankName);

        List<BankMaster> bankMasterList = masterDataRepository.getBankNameFromBankMaster(bankName, institutionId);

        if (Institute.isInstitute(institutionId,Institute.AMBIT)){
            if(null != bankMasterList && !bankMasterList.isEmpty()){

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankMasterList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            Set<String> bankNameList = new TreeSet<>();
            if(CollectionUtils.isNotEmpty(bankMasterList)) {
                for (BankMaster bankMaster : bankMasterList) {
                    bankNameList.add(bankMaster.getBankDescription());
                }
            }
            if(!bankNameList.isEmpty()){

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, bankNameList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCity(String institutionId, String cityQuery){

        BaseResponse baseResponse;
        logger.debug("Fetching city from City Master");

        String cityQuery1 = cityQuery.substring(0, 1).toUpperCase() + cityQuery.substring(1);
        logger.info("Query For City cityQuery1{} ", cityQuery1);

        List<CityMaster> cityMasterList = masterDataRepository.getCityMaster(institutionId,cityQuery1);
        if (CollectionUtils.isNotEmpty(cityMasterList)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,cityMasterList);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }

    @Override
    public BaseResponse getPincodeMaster(String instId) {

        BaseResponse baseResponse;
        List<PinCodeMaster> pinCodeMaster;

        pinCodeMaster = masterDataRepository.getPinCodeMaster(instId);

        if (CollectionUtils.isNotEmpty(pinCodeMaster)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, pinCodeMaster);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getEmployerMaster(String instId) {

        BaseResponse baseResponse;

        List<EmployerMaster> employerMasterList = masterDataRepository.getEmployerMaster(instId);

        if (CollectionUtils.isNotEmpty(employerMasterList)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, employerMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCityMaster(String institutionId){

        BaseResponse baseResponse;
        logger.debug("Fetching city from City Master");

        List<CityMaster> cityMasterList = masterDataRepository.getCityMaster(institutionId);
        if (CollectionUtils.isNotEmpty(cityMasterList)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,cityMasterList);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }

}

