package com.softcell.service.impl;


import com.softcell.config.KarzaProcessingInfo;
import com.softcell.constants.*;
import com.softcell.constants.AmbitMifin.AccomodationTypeEnum;
import com.softcell.constants.AmbitMifin.AddressTypeEnum;
import com.softcell.constants.AmbitMifin.CustEntityEnum;
import com.softcell.constants.AmbitMifin.Gender;
import com.softcell.constants.AmbitMifin.EducationTypeEnum;
import com.softcell.constants.AmbitMifin.SocialCategoryEnum;
import com.softcell.constants.AmbitMifin.MaritalStatus;
import com.softcell.constants.AmbitMifin.ConstitutionEnum;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.search.MasterMongoRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.core.BankingTransaction;
import com.softcell.gonogo.model.core.BankingStatement;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.PLAndBSAnalysis;
import com.softcell.gonogo.model.core.verification.ItrVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.DropdownMasterRequest;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.mifin.MiFinAmbitCallLog;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AmbitMifinLog;
import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.CrDecisionRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant.DeleteApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker.SaveDisbursalMakerRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan.NewLoanCreationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress.SaveApplicantAddressRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch.PanSearchRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe.ProcessDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails.SaveApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.SaveFinancialInfoRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SavePersonalInsurance.SavePersonalInsuranceRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification.SaveVerificationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails.UpdateLoanDetailsRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch.SearchExistingApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.ApplicantdedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.CoapplicantDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.UpdateDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateStatusDedupeResquest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceData;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.AmbitMifinResponse.DeleteApplicant.DeleteApplicantBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.DisbursalMaker.DisbursalDetailResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.DisbursalMaker.MifinDisbursalMakerBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.MifinAmbitResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch.AmbitMifinCustomerDetails;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveApplicantAddress.MiFinSaveApplicantAddressBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveApplicantAddress.SaveApplicantAddressBody;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveFinancialInfo.BankingIdResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveFinancialInfo.MiFinSaveFinancialInfoBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveFinancialInfo.SaveFinancialInfoBody;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SavePersonalInsurance.InsuranceDetails;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SavePersonalInsurance.SavePersonalInsuranceBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveVerification.MifinSaveVerificationBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SaveVerification.VerificationIdResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.UpdateLoanDetails.MifinLoanDetailBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch.MiFinPanSearchBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.ApplicantMatchedInfo;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.CustomerMatchInfo;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.MifinSaveProcessDedupeBaseResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SearchExistingApplicant.ExistingApplicantAddress;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SearchExistingApplicant.PersonalInfo;
import com.softcell.gonogo.model.response.AmbitMifinResponse.SearchExistingApplicant.SearchExistingApplicantBaseResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.service.factory.AmbitMifinRequestBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AmbitMifinManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.thirdparty.utils.ThirdPartyFieldConstant;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Optional;
import java.util.HashMap;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AmbitMifinManagerImpl implements AmbitMifinManager
{

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private AmbitMifinRequestBuilder ambitMifinRequestBuilder;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private AppConfigurationRepository appConfigurationRepository;

    @Autowired
    MasterMongoRepository masterMongoRepository;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Override
    public MiFinPanSearchBaseResponse panSearch(ApplicationRequest applicationRequest,Applicant applicant, String userId) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_PAN_SEARCH);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        PanSearchRequest panSearchRequest= ambitMifinRequestBuilder.buildPanSearchRequest(applicationRequest,applicant, userId);
        log.debug("MiFin PAN-Card search request for refId No {} : {}", applicationRequest.getRefID(), JsonUtil.ObjectToString(panSearchRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(panSearchRequest, url);
        log.info("MiFin PAN-Card search response for refId No {} : {} ", applicationRequest.getRefID(), mifinAmbitResponseStr);

        Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
        MiFinPanSearchBaseResponse miFinPanSearchBaseResponse = null;

        if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
            Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
            if (null != mifin && null != mifin.get(MifinConstants.panCardSearch)) {
                Map<String, Object> panCardSearch = (Map<String, Object>) mifin.get(MifinConstants.panCardSearch);
                if (null != panCardSearch && null != panCardSearch.get(MifinConstants.bodyStr)) {
                    String responseStr = (String) panCardSearch.get(MifinConstants.bodyStr);
                    responseStr = responseStr.replace("\"[", "[");
                    responseStr = responseStr.replace("]\"", "]");
                    miFinPanSearchBaseResponse = JsonUtil.StringToObject(responseStr, MiFinPanSearchBaseResponse.class);

                } else {
                    Map<String, Object> body = (Map<String, Object>) panCardSearch.get(MifinConstants.body);
                    miFinPanSearchBaseResponse = MiFinPanSearchBaseResponse.builder()
                            .status(body.containsKey(MifinConstants.STATUS) ? (String) body.get(MifinConstants.STATUS) : MifinConstants.STATUS_F)
                            .message(body.containsKey(MifinConstants.MESSAGE) ? (String) body.get(MifinConstants.MESSAGE) :null)
                            .build();
                }
            }
        }
        if(miFinPanSearchBaseResponse.getStatus().equals(MifinConstants.STATUS_F)){
            miFinPanSearchBaseResponse.setMessage(ErrorCode.NO_PAN_DATA_FOUND);
        }
        if (null != applicant.getAmbitMifinData()) {
            applicant.getAmbitMifinData().setMifinPancardMessage(miFinPanSearchBaseResponse.getMessage());
        }

        saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_PAN_SEARCH, panSearchRequest, applicationRequest.getRefID(), miFinPanSearchBaseResponse.getStatus());
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return  miFinPanSearchBaseResponse;
    }

    @Override
    public BaseResponse panSearchAndSearchExistingApplicant(ApplicationRequest applicationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Applicant applicant=getPanMatchedApplicant(applicationRequest);
        MiFinPanSearchBaseResponse miFinPanSearchBaseResponse=null;
        String userId = getUser(applicationRequest.getHeader(), applicationRequest.getHeader().getLoggedInUserId());
        try {
            if (null != applicant) {
                log.info("Pan Search API call initiated for AppID" + applicant.getApplicantId());
                miFinPanSearchBaseResponse = panSearch(applicationRequest, applicant, userId);

                if (StringUtils.equalsIgnoreCase(miFinPanSearchBaseResponse.getStatus(), MifinConstants.STATUS_S)) {
                    log.info("Search Existing API call initiated for AppID" + applicant.getApplicantId());
                    baseResponse = searchExistingApplicantSearch(applicationRequest, applicant, miFinPanSearchBaseResponse, userId);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinPanSearchBaseResponse);
                }
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "No Applicant PAN matched with given Pan {} " + applicationRequest.getRequest().getPanSearchNo());
            }
        }
        catch (Exception e){
            log.error("Exception caught while calling Pan Search API");
            e.printStackTrace();
            setKarzaResponse(applicationRequest,applicant);
            baseResponse=GngUtils.getBaseResponse(HttpStatus.OK,applicationRequest);
        }
        return baseResponse;
    }

    private void setKarzaResponse(ApplicationRequest applicationRequest,Applicant applicant){
        KarzaProcessingInfo karzaProcessingInfo = externalAPILogRepository.fetchKarzaProcessingInfo(applicationRequest.getHeader().getInstitutionId(), applicationRequest.getRequest().getPanSearchNo(),
                applicationRequest.getRefID(), "", KarzaHelper.PAN_REQUEST_TYPE);
        if(null!=karzaProcessingInfo) {
            Map<String, Object> res = (Map<String, Object>) karzaProcessingInfo.getResponseObj();
            if (null !=res) {
                String name = res.get("name").toString();
                if (StringUtils.isNotEmpty(name)) {
                    String names[] = name.split(FieldSeparator.SPACE_STR);
                    if (null != name && ApplicantType.isIndividual(applicant.getApplicantType())) {
                        applicant.getApplicantName().setFirstName(StringUtils.isNotEmpty(names[0]) ? names[0] : Constant.BLANK);
                        applicant.getApplicantName().setMiddleName(StringUtils.isNotEmpty(names[1]) ? names[1] : Constant.BLANK);
                        applicant.getApplicantName().setLastName(StringUtils.isNotEmpty(names[2]) ? names[2] : Constant.BLANK);
                    } else {
                        applicant.getApplicantName().setFirstName(StringUtils.isNotEmpty(name) ? name : Constant.BLANK);
                        applicant.getApplicantName().setLastName(StringUtils.isNotEmpty(name) ? name : Constant.BLANK);
                    }
                }
            }
        }
    }

    private Applicant getPanMatchedApplicant(ApplicationRequest applicationRequest) {
        List<Applicant> applicant=new ArrayList<>();
        Applicant applicantPassed=null;

        if(null!=applicationRequest.getRequest().getApplicant() && null!=applicationRequest.getRequest().getApplicant().getAmbitMifinData()
        && !(StringUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMiFinApplicantCode()))){
            applicant.add(applicationRequest.getRequest().getApplicant());
        }
        else if(CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
            applicant.clear();
            applicant.addAll(applicationRequest.getRequest().getCoApplicant());
        }

        if (CollectionUtils.isNotEmpty(applicant)) {
            for (Applicant coApplicant : applicant) {
                if (CollectionUtils.isNotEmpty(coApplicant.getKyc())) {
                    Optional<Kyc> kycDetails = coApplicant.getKyc().stream()
                            .filter(obj -> StringUtils.equalsIgnoreCase(obj.getKycName(), GNGWorkflowConstant.PAN.toFaceValue())
                            && StringUtils.equalsIgnoreCase(obj.getKycNumber(),applicationRequest.getRequest().getPanSearchNo())).findAny();
                    if (kycDetails.isPresent()) {
                        if (null != coApplicant.getAmbitMifinData()
                                && !(coApplicant.getAmbitMifinData().isExistingMifinApplicant())
                                && !(StringUtils.isNotEmpty(coApplicant.getAmbitMifinData().getMiFinApplicantCode()))) {
                            applicantPassed = coApplicant;
                            break;
                        }
                    }
                }
            }
        }
        return applicantPassed;
    }


    @Override
    public void createLoan(ApplicationRequest applicationRequest, String userId)throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_CREATE_LOAN);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS);
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        NewLoanCreationRequest newLoanCreationRequest = ambitMifinRequestBuilder.buildLoanCreationRequest(applicationRequest,userId);
        log.debug("MiFin Loan Creation request for refid {} : {}", refId, JsonUtil.ObjectToString(newLoanCreationRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(newLoanCreationRequest, url);
        log.info("MiFin Loan Creation response for refId {} : {} ", refId, mifinAmbitResponseStr);

        updateNewLoanGonogoDetails( newLoanCreationRequest ,mifinAmbitResponseStr , applicationRequest);
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
    }

    @Override
    public void saveApplicantDetails(ApplicationRequest applicationRequest, String userId) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_DETAILS);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS);
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        SaveApplicantRequest saveApplicantRequest = ambitMifinRequestBuilder.buildSaveApplicantDetailsRequest(applicationRequest, applicationRequest.getRequest().getApplicant().getApplicantId(),userId);
        log.debug("MiFin Applicant Creation request for refid {} : {}", refId, JsonUtil.ObjectToString(saveApplicantRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(saveApplicantRequest, url);
        log.info("MiFin Application Creation response for refId {} : {} ", refId, mifinAmbitResponseStr);

        updateSaveApplicantGoNoGoDetails(saveApplicantRequest, mifinAmbitResponseStr, applicationRequest, applicationRequest.getRequest().getApplicant());

        if (null != applicationRequest.getRequest().getCoApplicant()) {

            for (CoApplicant coApplicant : applicationRequest.getRequest().getCoApplicant()) {
                SaveApplicantRequest saveCoApplicantRequest = ambitMifinRequestBuilder.buildSaveApplicantDetailsRequest(applicationRequest, coApplicant.getApplicantId(),userId);
                log.debug("MiFin Applicant Creation request for refid {} : {}", refId, JsonUtil.ObjectToString(saveCoApplicantRequest));

                String mifinAmbitResponseStrCoApp = TransportUtils.postJsonRequest(saveCoApplicantRequest, url);
                log.info("MiFin Application Creation response for refId {} : {} ", refId, mifinAmbitResponseStrCoApp);

                updateSaveApplicantGoNoGoDetails(saveCoApplicantRequest, mifinAmbitResponseStrCoApp, applicationRequest, coApplicant);
            }
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
    }

    @Override
    public BaseResponse saveFinancialAndFetchDedupe(ApplicationRequest applicationRequest) throws Exception {
        if(StringUtils.isNotEmpty(applicationRequest.getMiFinProspectCode()) && null !=  applicationRequest.getRequest()) {

            String userId = getUser(applicationRequest.getHeader(), applicationRequest.getHeader().getLoggedInUserId());

            if ( null != applicationRequest.getRequest().getApplicant() &&
                    null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()){
                if (StringUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMiFinApplicantCode())) {
                    saveApplicantDetails(applicationRequest, userId);
                    saveApplicantAddress(applicationRequest, userId);
                }
            }

            if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
                saveCoApplicantAddress(applicationRequest, userId);
            }
            if(saveFinancialInfo(applicationRequest, userId)) {
                processDedupe(applicationRequest, userId);
            }
        }
        return GngUtils.getBaseResponse(HttpStatus.OK,applicationRepository.getGngApplication(applicationRequest.getRefID()));
    }

    @Override
    public boolean saveFinancialInfo(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_FINANCIAL_INFO);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SaveFinancialInfoRequest saveFinancialInfoRequest = ambitMifinRequestBuilder.buildSaveFinancialInfoRequest(applicationRequest, userId);
        log.debug("MiFin Save Financial Info request for refid {} : {}", refId, JsonUtil.ObjectToString(saveFinancialInfoRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(saveFinancialInfoRequest, url);
        log.info("MiFin Save Financial Info response for refId {} : {} ", refId, mifinAmbitResponseStr);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return updateFinancialInfoGonogoDetails( saveFinancialInfoRequest , mifinAmbitResponseStr , applicationRequest);
    }

    private boolean updateFinancialInfoGonogoDetails(SaveFinancialInfoRequest saveFinancialInfoRequest, String mifinAmbitResponseStr, ApplicationRequest applicationRequest) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        MiFinSaveFinancialInfoBaseResponse miFinSaveFinancialInfoBaseResponse = null;
        Map<String, Object> body =null;
        try {
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.saveFinancialInfo)) {
                    Map<String, Object> saveFinancialInfo = (Map<String, Object>) mifin.get(MifinConstants.saveFinancialInfo);
                    if (null != saveFinancialInfo && null != saveFinancialInfo.get(MifinConstants.bodyStr)) {
                        String responseStr = (String) saveFinancialInfo.get(MifinConstants.bodyStr);
                        responseStr = responseStr.replace("\"[", "[");
                        responseStr = responseStr.replace("]\"", "]");
                        SaveFinancialInfoBody saveFinancialInfoBody = JsonUtil.StringToObject(responseStr, SaveFinancialInfoBody.class);
                        miFinSaveFinancialInfoBaseResponse = saveFinancialInfoBody.getMiFinSaveFinancialInfoBaseResponse();

                    } else {
                        body = (Map<String, Object>) saveFinancialInfo.get(MifinConstants.body);
                        if (body.containsKey(MifinConstants.RECEIVE) && null != body.get(MifinConstants.RECEIVE)) {
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinSaveFinancialMessage((String) receive.get(MifinConstants.MESSAGE));
                            }
                        }
                    }
                }
            }

            if(null != miFinSaveFinancialInfoBaseResponse && MifinConstants.STATUS_S.equals(miFinSaveFinancialInfoBaseResponse.getStatus())) {

                if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                    applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinSaveFinancialMessage(miFinSaveFinancialInfoBaseResponse.getMessage());

                    if (CollectionUtils.isNotEmpty(miFinSaveFinancialInfoBaseResponse.getEmployeeDetailsIdResponseList())) {
                        applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinEmploymentDetailsId(miFinSaveFinancialInfoBaseResponse.getEmployeeDetailsIdResponseList().get(0).getEmploymentDetailsId());
                    }
                }

                CamDetails camDetails = applicationRepository.fetchCamDetails(refId, institutionId, EndPointReferrer.ALL_CAM_DETAILS);
                if(null != camDetails){
                    if(CollectionUtils.isNotEmpty(miFinSaveFinancialInfoBaseResponse.getBankingIdResponseList())
                            && CollectionUtils.isNotEmpty(camDetails.getBankingStatements())) {
                        List<BankingIdResponse> bankingIdResponseList = miFinSaveFinancialInfoBaseResponse.getBankingIdResponseList();
                        int bankingIndex = 0;
                        for(BankingStatement bankingStatement : camDetails.getBankingStatements()) {
                            if(bankingIdResponseList.size() > bankingIndex  && !bankingStatement.isConsolidated()) {
                                bankingStatement.setMifinBankingId(bankingIdResponseList.get(bankingIndex).getBankDetailsId());

                                if(CollectionUtils.isNotEmpty(bankingStatement.getBankingTransactions())) {
                                    for(BankingTransaction bankingTransaction : bankingStatement.getBankingTransactions()) {
                                        if(bankingIdResponseList.size() > bankingIndex) {
                                            bankingTransaction.setMifinAccountDetailsId(bankingIdResponseList.get(bankingIndex).getAccDtlId());
                                        }
                                        bankingIndex++;
                                    }
                                }
                            }

                        }
                    }
                    applicationRepository.saveCamDetails(refId, institutionId, camDetails, EndPointReferrer.CAM_BANKING_DETAILS);
                    if(CollectionUtils.isNotEmpty(miFinSaveFinancialInfoBaseResponse.getFinancialIdResponseList())
                            && CollectionUtils.isNotEmpty(camDetails.getPlAndBSAnalysis())) {
                        int financialIndex = 0;
                        for(PLAndBSAnalysis plAndBSAnalysis : camDetails.getPlAndBSAnalysis()) {
                            if(miFinSaveFinancialInfoBaseResponse.getFinancialIdResponseList().size() > financialIndex) {
                                plAndBSAnalysis.setMifinFinancialTabId(miFinSaveFinancialInfoBaseResponse.getFinancialIdResponseList().get(financialIndex).getFinancialTabId());
                                financialIndex++;
                            }
                        }
                    }
                    applicationRepository.saveCamDetails(refId, institutionId, camDetails, EndPointReferrer.CAM_PL_BS_DETAILS);
                    if(CollectionUtils.isNotEmpty(miFinSaveFinancialInfoBaseResponse.getOtherLoanDetailIdResponseList())
                            && CollectionUtils.isNotEmpty(camDetails.getRtrDetailList())) {
                        int otherLoanDetailsIndex = 0;
                        for(RTRDetail rtrDetail : camDetails.getRtrDetailList()) {
                            if(miFinSaveFinancialInfoBaseResponse.getOtherLoanDetailIdResponseList().size() > otherLoanDetailsIndex) {
                                rtrDetail.setMifinOtherLoanDetailsId(miFinSaveFinancialInfoBaseResponse.getOtherLoanDetailIdResponseList().get(otherLoanDetailsIndex).getOtherLoanDetailsId());
                                otherLoanDetailsIndex++;
                            }
                        }
                    }
                    applicationRepository.saveCamDetails(refId, institutionId, camDetails, EndPointReferrer.CAM_RTR_DETAILS);
                }
                VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, institutionId);
                if(CollectionUtils.isNotEmpty(miFinSaveFinancialInfoBaseResponse.getItrIdResponseList()) && null != verificationDetails
                        && CollectionUtils.isNotEmpty(verificationDetails.getItrVerification())) {
                    int itrIndex = 0;
                    for(ItrVerification itrVerification : verificationDetails.getItrVerification()) {
                        if(miFinSaveFinancialInfoBaseResponse.getItrIdResponseList().size() > itrIndex) {
                            itrVerification.setMifinItrId(miFinSaveFinancialInfoBaseResponse.getItrIdResponseList().get(itrIndex).getItrId());
                            itrIndex++;
                        }
                    }
                    applicationRepository.saveItrVerificationDetails(refId, institutionId, verificationDetails.getItrVerification());
                }
                saveCallLogforAmbit(miFinSaveFinancialInfoBaseResponse, ThirdPartyFieldConstant.MIFIN_SAVE_FINANCIAL_INFO, saveFinancialInfoRequest, refId, MifinConstants.STATUS_S);

                log.info("Data pushed to Mifin Successfully for refId: {}; Received ProspectCode {} and EmploymentDetailsId {}", applicationRequest.getRefID(),
                        miFinSaveFinancialInfoBaseResponse.getProspectCode(), miFinSaveFinancialInfoBaseResponse.getEmployeeDetailsIdResponseList().get(0));
            } else {
                saveCallLogforAmbit(body, ThirdPartyFieldConstant.MIFIN_SAVE_FINANCIAL_INFO, saveFinancialInfoRequest, refId, MifinConstants.STATUS_F);
            }
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        } catch(Exception e) {
            log.error("Error occured {} ", e.getMessage());
        }
        return Optional.ofNullable(miFinSaveFinancialInfoBaseResponse).isPresent()
                && MifinConstants.STATUS_S.equals(miFinSaveFinancialInfoBaseResponse.getStatus());
    }

    @Override
    public void processDedupe(ApplicationRequest applicationRequest, String userId) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_PROCESS_DEDUPE);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS);
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ProcessDedupeRequest processDedupeRequest = ambitMifinRequestBuilder.buildProcessDedupeRequest(applicationRequest, applicationRequest.getRequest().getApplicant(), userId);
        log.debug("MiFin Process Dedupe request for refid {} : {}", refId, JsonUtil.ObjectToString(processDedupeRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(processDedupeRequest, url);

        processdedupeUpdateGonogoDetails( mifinAmbitResponseStr,applicationRequest,applicationRequest.getRequest().getApplicant(),processDedupeRequest);

        if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
            for (CoApplicant coApplicant : applicationRequest.getRequest().getCoApplicant()) {

                processDedupeRequest = ambitMifinRequestBuilder.buildProcessDedupeRequest(applicationRequest, coApplicant,userId);
                log.debug("MiFin Process Dedupe request for refid {} : {}", refId, JsonUtil.ObjectToString(processDedupeRequest));

                mifinAmbitResponseStr = TransportUtils.postJsonRequest(processDedupeRequest, url);

                processdedupeUpdateGonogoDetails(mifinAmbitResponseStr, applicationRequest, coApplicant,processDedupeRequest);

            }
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
    }

    @Override
    public BaseResponse updateProcessDedupe(UpdateDedupeRequest updateDedupeRequest) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,updateDedupeRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_UPDATE_DEDUPE_STATUS);
        activityLogs.setRefId(updateDedupeRequest.getRefId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ApplicationRequest applicationRequest=null;
        String userId = null;
        applicationRequest = applicationRepository.getApplicationRequest(updateDedupeRequest.getRefId(), updateDedupeRequest.getInstId());

        if(null != updateDedupeRequest.getHeader()) {
            userId = getUser(updateDedupeRequest.getHeader(), updateDedupeRequest.getHeader().getLoggedInUserId());
        } else {
            userId = getUser(applicationRequest.getHeader(),applicationRequest.getHeader().getLoggedInUserId());
        }

        if (null != updateDedupeRequest.getApplicantdedupeRequest()) {
            ApplicantdedupeRequest applicantdedupeRequest = updateDedupeRequest.getApplicantdedupeRequest();
            applicationRequest=updateDedupeRequestforAppCoaaplicant(updateDedupeRequest,applicantdedupeRequest, userId);

        }
        if(CollectionUtils.isNotEmpty(updateDedupeRequest.getCoapplicantDedupeRequest())){

            for ( CoapplicantDedupeRequest coapplicantDedupeRequest : updateDedupeRequest.getCoapplicantDedupeRequest()) {
                applicationRequest=updateDedupeRequestforAppCoaaplicant(updateDedupeRequest, coapplicantDedupeRequest, userId);
            }
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return GngUtils.getBaseResponse(HttpStatus.OK,applicationRepository.getGngApplication(applicationRequest.getRefID()));
    }

    @Override
    public BaseResponse searchExistingApplicantSearch(ApplicationRequest applicationRequest,Applicant applicant,MiFinPanSearchBaseResponse miFinPanSearchBaseResponse, String userId) throws Exception {
        BaseResponse baseResponse=null;
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SEARCH_EXISTING_APPLICANT);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SearchExistingApplicantRequest searchExistingApplicantRequest=null;
        String mifinAmbitResponseStr=null;
        searchExistingApplicantRequest = ambitMifinRequestBuilder.buildSearchExistingApplicantRequest(applicationRequest,miFinPanSearchBaseResponse,false,userId);
        log.debug("MiFin Search Applicant request for refid {} : {}", refId, JsonUtil.ObjectToString(searchExistingApplicantRequest));

        mifinAmbitResponseStr = TransportUtils.postJsonRequest(searchExistingApplicantRequest, url);
        log.info("MiFin Search Applicant response for refId {} : {} ", refId, mifinAmbitResponseStr);

        SearchExistingApplicantBaseResponse searchExistingApplicantBaseResponse = getBaseReponseObject(mifinAmbitResponseStr);
        if(null!=searchExistingApplicantBaseResponse){
            if(searchExistingApplicantBaseResponse.getStatus().equals(MifinConstants.STATUS_F)){
                searchExistingApplicantRequest = ambitMifinRequestBuilder.buildSearchExistingApplicantRequest(applicationRequest,miFinPanSearchBaseResponse,true, userId);
                log.debug("MiFin Search Applicant request for refid {} : {}", refId, JsonUtil.ObjectToString(searchExistingApplicantRequest));

                mifinAmbitResponseStr = TransportUtils.postJsonRequest(searchExistingApplicantRequest, url);
                log.info("MiFin Search Applicant response for refId {} : {} ", refId, mifinAmbitResponseStr);

                searchExistingApplicantBaseResponse = getBaseReponseObject(mifinAmbitResponseStr);
            }
        }
        baseResponse=getApplicationRequest(searchExistingApplicantBaseResponse,applicationRequest,applicant);
        saveCallLogforAmbit(mifinAmbitResponseStr,ThirdPartyFieldConstant.MIFIN_SEARCH_EXISTING_APPLICANT , searchExistingApplicantRequest,applicationRequest.getRefID() , searchExistingApplicantBaseResponse.getStatus());

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    private BaseResponse getApplicationRequest(SearchExistingApplicantBaseResponse searchExistingApplicantBaseResponse,ApplicationRequest applicationRequest,Applicant applicant){
        BaseResponse baseResponse=null;
        if(null !=searchExistingApplicantBaseResponse){
            if(StringUtils.equalsIgnoreCase(searchExistingApplicantBaseResponse.getStatus(),MifinConstants.STATUS_S)) {
                baseResponse = buildApplicationRequest(searchExistingApplicantBaseResponse,applicationRequest,applicant);
            }
            else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, searchExistingApplicantBaseResponse);
            }
            if(null!=applicant.getAmbitMifinData())
            {
                applicant.getAmbitMifinData().setMifinsearchApplicantMessage(searchExistingApplicantBaseResponse.getMessage());
            }

        }
        return baseResponse;
    }


    private SearchExistingApplicantBaseResponse getBaseReponseObject(String mifinAmbitResponseStr) {

        SearchExistingApplicantBaseResponse searchExistingApplicantBaseResponse=null;
        try{
            mifinAmbitResponseStr=mifinAmbitResponseStr.replace("\"PERSONAL_INFO\":\"\""," \"PERSONAL_INFO\": []");
            mifinAmbitResponseStr=mifinAmbitResponseStr.replace("\"ADDRESS\":\"\"","\"ADDRESS\":[]");
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.searchExistingApplicant)) {
                    Map<String, Object> searchExistingApplicant = (Map<String, Object>) mifin.get(MifinConstants.searchExistingApplicant);
                    if (null != searchExistingApplicant) {
                        Map<String, Object> body = null;
                        if (null != searchExistingApplicant.get(MifinConstants.bodyStr)) {
                            String bodyStr = searchExistingApplicant.get(MifinConstants.bodyStr).toString();
                            bodyStr = bodyStr.replace("\"[]\"", "[]");
                            bodyStr = bodyStr.replace("\"[{", "[{");
                            bodyStr = bodyStr.replace("}]\"", "}]");
                            body = JsonUtil.StringToObject(bodyStr, Map.class);
                        } else if (null != searchExistingApplicant.get(MifinConstants.body)) {
                            body = (Map<String, Object>) searchExistingApplicant.get(MifinConstants.body);
                        }
                        searchExistingApplicantBaseResponse = JsonUtil.MapToObject((Map<String, Object>) body.get(MifinConstants.RECEIVE), SearchExistingApplicantBaseResponse.class);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("Exception Occured while calling New Loan API ",e.getMessage());
            e.printStackTrace();
        }
        return searchExistingApplicantBaseResponse;
    }


    private BaseResponse buildApplicationRequest(SearchExistingApplicantBaseResponse response,ApplicationRequest applicationRequest,Applicant applicant){

        List<Kyc> kycList = applicant.getKyc();
        List<CustomerAddress> address=new ArrayList<>();
        String CinNo = applicationRequest.getRequest().getApplicant().getCinNo();

        for(PersonalInfo personalInfo:response.getPersonalInfo()){
            applicant.getApplicantName().setFirstName(personalInfo.getFname());
            applicant.getApplicantName().setLastName(null!=personalInfo.getLname()?personalInfo.getLname(): Constant.BLANK);
            if(null!=applicant.getAmbitMifinData()) {
                applicant.getAmbitMifinData().setMiFinApplicantCode(null!=personalInfo.getApplicantCode()?personalInfo.getApplicantCode():Constant.BLANK);
                applicant.getAmbitMifinData().setExistingMifinApplicant(true);
            }
            if(null!=personalInfo.getCustEntityType()){
                if(StringUtils.equalsIgnoreCase(ApplicantType.INDIVIDUAL.value(),CustEntityEnum.getGonogoCustEntity(personalInfo.getCustEntityType()))) {
                    applicant.setDateOfBirth(null!=personalInfo.getDob()? GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                            (GngDateUtil.ddMMMyyyy_WITH_HYPHEN, personalInfo.getDob()), GngDateUtil.ddMMyyyy):Constant.BLANK);
                    applicant.setSocialCategory(SocialCategoryEnum.getCatgoryfromCode(null!=personalInfo.getCasteCategory()?personalInfo.getCasteCategory():Constant.BLANK));
                    applicant.setMaritalStatus(MaritalStatus.getGonogoMaritalType(null!=personalInfo.getMaritalStatus()?personalInfo.getMaritalStatus():Constant.BLANK));
                    applicant.setGender(Gender.getGenderfromCode(null!=personalInfo.getGender()?personalInfo.getGender():Constant.BLANK));
                    applicant.setEducation(EducationTypeEnum.getGonogoTypefromMifinCOde(null!=personalInfo.getEducation()?personalInfo.getEducation():Constant.BLANK));
                    applicant.getFatherName().setFirstName(personalInfo.getFatherFname());
                    applicant.getFatherName().setLastName(personalInfo.getFatherLname());
                    if(CollectionUtils.isNotEmpty(applicant.getEmployment())) {
                        applicant.getEmployment().get(0).setEmploymentType(ConstitutionEnum.getGonogoTypeFromMifincode(null != personalInfo.getCostitution() ? personalInfo.getCostitution() : Constant.BLANK));
                    }
                }
                else{
                    if(CollectionUtils.isNotEmpty(applicant.getEmployment())) {
                        applicant.getEmployment().get(0).setDateOfJoining(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                (GngDateUtil.ddMMMyyyy_WITH_HYPHEN, personalInfo.getDob()),GngDateUtil.ddMMyyyy));
                    }
                    applicant.setApplicantType(ConstitutionEnum.getGonogoTypeFromMifincode(personalInfo.getCostitution()));
                    applicant.setContactPerson(personalInfo.getKeyContactPerson());
                }
            }

            if(CollectionUtils.isNotEmpty(kycList)) {
                KarzaProcessingInfo karzaProcessingInfo = externalAPILogRepository.fetchKarzaProcessingInfo(applicationRequest.getHeader().getInstitutionId(), personalInfo.getPanNo(),
                        applicationRequest.getRefID(), "", KarzaHelper.PAN_REQUEST_TYPE);

                KarzaProcessingInfo karzaProcessingInfoDetails = externalAPILogRepository.fetchKarzaProcessingInfo(applicationRequest.getHeader().getInstitutionId(), CinNo,
                        applicationRequest.getRefID(), "", KarzaHelper.COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE);

                Optional<Kyc> kycDetails = kycList.stream().filter(obj -> StringUtils.equalsIgnoreCase(obj.getKycName(), GNGWorkflowConstant.PAN.toFaceValue())).findAny();
                kycDetails.ifPresent(value -> value.setKycNumber(personalInfo.getPanNo()));
                if(karzaProcessingInfo != null && StringUtils.equalsIgnoreCase(personalInfo.getPanNo(), karzaProcessingInfo.getKycNumber())) {
                    kycDetails.ifPresent(value -> value.setKycStatus(true));
                }

                if(CinNo != null) {
                    Kyc kycDetails1 = new Kyc();
                    kycDetails1.setKycNumber(CinNo);
                    kycDetails1.setKycName(KarzaHelper.COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE);
                    if(karzaProcessingInfoDetails != null && StringUtils.equalsIgnoreCase(karzaProcessingInfoDetails.getStatus(), KarzaHelper.VERIFIED)) {
                        kycDetails1.setKycStatus(true);
                        kycList.add(kycDetails1);
                    }
                }
            }
            break;
        }

        if (CollectionUtils.isNotEmpty(response.getAddress())) {
            for(ExistingApplicantAddress obj:response.getAddress())
            {
                CustomerAddress customerAddress=new CustomerAddress();
                customerAddress.setLandLoard(obj.getLandmark());
                customerAddress.setState(obj.getStateName());
                customerAddress.setResidenceAddressType(AccomodationTypeEnum.getAccomodationCodeFromGonogoType(obj.getAccomodationType()));
                customerAddress.setAddressLine1(null!=obj.getFlatNo()?obj.getFlatNo():Constant.BLANK);
                customerAddress.setAddressLine2(obj.getBuildingName());
                customerAddress.setAddressType(AddressTypeEnum.getAddressfromCode(obj.getAddressType()));
                customerAddress.setCity(obj.getCityName());
                customerAddress.setAddrCompanyName(null!=obj.getCompanyName()?obj.getCompanyName():Constant.BLANK);
                customerAddress.setPin(Long.parseLong(obj.getPincode()));
                customerAddress.setTimeAtAddress(Integer.parseInt(null!=obj.getNoOfYears()?obj.getNoOfYears():"0"));
                customerAddress.setMifinAddressId(null!=obj.getAddressId()?obj.getAddressId():Constant.BLANK);
                customerAddress.setLandLineNumber(null!=obj.getPhone1()?obj.getPhone1():Constant.BLANK);
                customerAddress.setFlatNo(null!=obj.getFlatNo()?obj.getFlatNo():Constant.BLANK);
                customerAddress.setMailingAddress(null!=obj.getMailingAddress()?(obj.getMailingAddress().equalsIgnoreCase(Constant.BLOCKSTATUS)?true:false):false);
                if(CollectionUtils.isNotEmpty(applicant.getEmail())){
                    applicant.getEmail().get(0).setEmailAddress(null!=obj.getEmail()?obj.getEmail():Constant.BLANK);
                }
                if(CollectionUtils.isNotEmpty(applicant.getPhone())){
                    applicant.getPhone().get(0).setPhoneNumber(null!=obj.getMobile()?obj.getMobile():Constant.BLANK);
                }
                if (!StringUtils.isNotEmpty(applicant.getGstNo())){
                    applicant.setGstNo(obj.getGstinNo());
                }
                address.add(customerAddress);
            }
        }

        CustomerAddress addresscheck=null;
        int index=-1;
        String addressype;
        if(StringUtils.equalsIgnoreCase(ApplicantType.INDIVIDUAL.value(),applicant.getApplicantType()))
            addressype=GNGWorkflowConstant.RESIDENCE.toFaceValue();
        else
            addressype=GNGWorkflowConstant.OFFICE.toFaceValue();

        Optional<CustomerAddress> addressOptional=address.stream().filter(var->StringUtils.equalsIgnoreCase(var.getAddressType(),addressype )).findFirst();
        if(!(addressOptional.isPresent())){
            CustomerAddress customerAddress=new CustomerAddress();
            customerAddress.setAddressType(addressype);
            address.add(0,customerAddress);
        }
        if(!(StringUtils.equalsIgnoreCase(address.get(0).getAddressType(),addressype))) {
            addresscheck=address.stream().filter(var->StringUtils.equalsIgnoreCase(var.getAddressType(),addressype)).findAny().orElse(null);
            index=address.indexOf(addresscheck);
            if(index>=0) {
                address.remove(index);
                address.add(0, addresscheck);
            }
        }

        applicant.setAddress(address);
        return GngUtils.getBaseResponse(HttpStatus.OK, applicationRequest);
    }

    public ApplicationRequest updateDedupeRequestforAppCoaaplicant(UpdateDedupeRequest updateDedupeRequest, ApplicantdedupeRequest applicantdedupeRequest, String userId)throws Exception{

        String refId = updateDedupeRequest.getRefId();
        String institutionId = updateDedupeRequest.getInstId();
        String url = getUrl(institutionId);
        ApplicationRequest applicationRequest=null;

        UpdateStatusDedupeResquest updateStatusDedupeResquest = ambitMifinRequestBuilder.buildUpdateProcessDedupeRequest(updateDedupeRequest,applicantdedupeRequest, userId);
        log.debug("MiFin Update dedupe status Info request for refid {} : {}", refId, JsonUtil.ObjectToString(updateStatusDedupeResquest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(updateStatusDedupeResquest, url);
        log.info("MiFin Update dedupe status response for refId {} : {} ", refId, mifinAmbitResponseStr);
        applicationRequest = updateDedupeGonoGoCustomerApplication(mifinAmbitResponseStr,updateStatusDedupeResquest,updateDedupeRequest ,applicantdedupeRequest);

        return applicationRequest;
    }

    private ApplicationRequest updateDedupeGonoGoCustomerApplication(String mifinAmbitResponseStr,UpdateStatusDedupeResquest updateStatusDedupeResquest,UpdateDedupeRequest updateDedupeRequest,ApplicantdedupeRequest applicantdedupeRequest) throws Exception {

        ApplicationRequest applicationRequest=null;
        try {
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.updateDedupeStatus)) {
                    Map<String, Object> updateDedupeStatus = (Map<String, Object>) mifin.get(MifinConstants.updateDedupeStatus);

                    if (null != updateDedupeStatus) {
                        Map<String, Object> body = null;
                        if (null != updateDedupeStatus.get(MifinConstants.bodyStr)) {
                            String bodyStr = updateDedupeStatus.get(MifinConstants.bodyStr).toString();
                            bodyStr = bodyStr.replace("\"[", "[");
                            bodyStr = bodyStr.replace("]\"", "]");
                            body = JsonUtil.StringToObject(bodyStr, Map.class);
                        } else if (null != updateDedupeStatus.get(MifinConstants.body)) {
                            body = (Map<String, Object>) updateDedupeStatus.get(MifinConstants.body);
                        }
                        if (null != body && null != body.get(MifinConstants.RECEIVE)) {
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            applicationRequest=getUpdatedDedupeApplicationRequest(receive,updateDedupeRequest,applicantdedupeRequest);
                            saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_UPDATE_DEDUPE_STATUS, updateStatusDedupeResquest, updateDedupeRequest.getRefId(), receive.get(MifinConstants.STATUS).toString());
                        }
                    }
                }

            }
        } catch (Exception e) {
            log.error("Exception caught while calling Update Process Dedupe API");
            e.printStackTrace();
        }
        return applicationRequest;
    }

    public ApplicationRequest getUpdatedDedupeApplicationRequest(Map<String,Object> receive,UpdateDedupeRequest updateDedupeRequest,ApplicantdedupeRequest applicantdedupeRequest)throws Exception{

        Applicant applicant=null;
        ApplicationRequest applicationRequest= applicationRepository.getApplicantData(updateDedupeRequest.getRefId(),updateDedupeRequest.getInstId());

        if(Integer.parseInt(applicantdedupeRequest.getAppId()) > 0 ){
            applicant= applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                    coApplicant.getApplicantId(),applicantdedupeRequest.getAppId())).findFirst().orElse(null);
        }
        else if (applicantdedupeRequest.getAppId().equals("0")){
            applicant = applicationRequest.getRequest().getApplicant();
        }
        if (null != applicant.getAmbitMifinData()) {

            applicationRequest.getRequest().setMifinReason(updateDedupeRequest.getMifinReason());
            applicationRequest.getRequest().setMifinDedupeDecision(updateDedupeRequest.getMifinDedupeDecision());
            applicant.getAmbitMifinData().setCustomerDedupeStatus(applicantdedupeRequest.getCuomerDedupeStatus());
            applicant.getAmbitMifinData().setRemarks(applicantdedupeRequest.getRemarks());

            if (StringUtils.equalsIgnoreCase(receive.get(MifinConstants.STATUS).toString(), MifinConstants.STATUS_F)) {
                applicant.getAmbitMifinData().setMifinUpdateDedupeMessage(receive.get(MifinConstants.MESSAGE).toString());
                applicant.getAmbitMifinData().setMifinDedupeflag(false);
            } else {
                if (null != applicant) {
                    ApplicantMatchedInfo applicantMatchedInfo = applicantdedupeRequest.getUpdateDedupeBaseRequest().getApplicantMatchedInfoList();

                    if (null != applicantMatchedInfo) {
                        if(StringUtils.isNotEmpty(applicantMatchedInfo.getActualCustid())) {
                            applicant.getAmbitMifinData().setMiFinApplicantCode(applicantMatchedInfo.getActualCustid());
                        }
                    }
                    applicant.getAmbitMifinData().setMifinUpdateDedupeMessage(receive.get(MifinConstants.MESSAGE).toString());
                    applicant.getAmbitMifinData().setMifinDedupeflag(true);
                }
            }
        }
        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        return applicationRequest;
    }

    private void processdedupeUpdateGonogoDetails(String mifinAmbitResponseStr, ApplicationRequest applicationRequest,Applicant applicant,ProcessDedupeRequest processDedupeRequest) throws Exception {

        MifinSaveProcessDedupeBaseResponse mifinSaveProcessDedupeBaseResponse = null;
        String apiStatus= MifinConstants.STATUS_F;
        try {

            if(null != mifinAmbitResponseStr) {
                Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
                if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                    Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                    if (null != mifin && null != mifin.get(MifinConstants.processDedupe)) {
                        Map<String, Object> processdedupe = (Map<String, Object>) mifin.get(MifinConstants.processDedupe);
                        if (null != processdedupe) {
                            Map<String, Object> body = null;
                            if (null != processdedupe.get(MifinConstants.bodyStr)) {
                                String bodyStr = processdedupe.get(MifinConstants.bodyStr).toString();
                                bodyStr = bodyStr.replace("\"[", "[");
                                bodyStr = bodyStr.replace("]\"", "]");
                                body = JsonUtil.StringToObject(bodyStr, Map.class);
                            } else if (null != processdedupe.get(MifinConstants.body)) {
                                body = (Map<String, Object>) processdedupe.get(MifinConstants.body);
                            }
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            if (null != receive && null != applicant.getAmbitMifinData()) {

                                applicant.getAmbitMifinData().setMifinDedupeMessage(receive.get(MifinConstants.MESSAGE).toString());
                                apiStatus = receive.get(MifinConstants.STATUS).toString();
                                log.info("MiFin Process Dedupe response for Applicant code {} : {} ", processDedupeRequest.getProcessDedupeBasicInfo().getApplicantCode(), receive.get(MifinConstants.MESSAGE).toString());

                                if (MifinConstants.STATUS_S.equals(apiStatus)) {

                                    mifinSaveProcessDedupeBaseResponse = JsonUtil.MapToObject((Map<String, Object>) body.get(MifinConstants.RECEIVE), MifinSaveProcessDedupeBaseResponse.class);

                                    ArrayList<CustomerMatchInfo> customerMatchInfoArrayList = new ArrayList<>();
                                    CustomerMatchInfo customerMatchInfo=null;
                                    if (null != mifinSaveProcessDedupeBaseResponse && CollectionUtils.isNotEmpty(mifinSaveProcessDedupeBaseResponse.getCustomermatchInfo())) {
                                        List<ApplicantMatchedInfo> applicantMatchedInfosList = mifinSaveProcessDedupeBaseResponse.getCustomermatchInfo().get(0).getApplicantMatchedInfoList();

                                        if (CollectionUtils.isNotEmpty(applicantMatchedInfosList)) {
                                            customerMatchInfo = CustomerMatchInfo.builder()
                                                    .applicantMatchedInfoList(applicantMatchedInfosList)
                                                    .build();

                                        } else {
                                            ArrayList<ApplicantMatchedInfo> applicantMatchedInfoArrayList=new ArrayList<>();
                                            ApplicantMatchedInfo applicantMatchedInfo=new ApplicantMatchedInfo();
                                            applicantMatchedInfoArrayList.add(applicantMatchedInfo);
                                            customerMatchInfo=CustomerMatchInfo.builder().applicantMatchedInfoList(applicantMatchedInfoArrayList).build();

                                        }
                                        customerMatchInfoArrayList.add(customerMatchInfo);

                                    }
                                    applicant.getAmbitMifinData().setMifinAmbitDedupeApplicationList(customerMatchInfoArrayList);
                                    applicant.getAmbitMifinData().setMifinDedupeflag(false);
                                    applicationRequest.getRequest().setMifinDedupeReinitiateFlag(false);
                                    applicant.getAmbitMifinData().setMifinProcessDedupeFlag(true);
                                } else {
                                    applicant.getAmbitMifinData().setMifinDedupeflag(false);
                                    applicant.getAmbitMifinData().setMifinProcessDedupeFlag(false);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e){
            log.error("Exception caught while calling Process Dedupe API");
            e.printStackTrace();
        }

        if (MifinConstants.STATUS_S.equals(apiStatus)) {
            saveCallLogforAmbit(mifinSaveProcessDedupeBaseResponse, ThirdPartyFieldConstant.MIFIN_PROCESS_DEDUPE, processDedupeRequest, applicationRequest.getRefID(), apiStatus);

        }else{
            saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_PROCESS_DEDUPE, processDedupeRequest, applicationRequest.getRefID(), apiStatus);
        }
        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

    }


    private void updateNewLoanGonogoDetails(NewLoanCreationRequest newLoanCreationRequest  , String mifinAmbitResponseStr, ApplicationRequest applicationRequest)throws Exception {
        try {
            mifinAmbitResponseStr = mifinAmbitResponseStr.replace("at \\\"QC_LOS.PKG_API_WRAPPER\\\", line 1012\\n", "");
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            String apiStatus = MifinConstants.STATUS_F;

            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.newLoan)) {
                    Map<String, Object> newApplicant = (Map<String, Object>) mifin.get(MifinConstants.newLoan);
                    if (null != newApplicant) {
                        Map<String, Object> body = null;
                        if (null != newApplicant.get(MifinConstants.bodyStr)) {
                            String bodyStr = newApplicant.get(MifinConstants.bodyStr).toString();
                            bodyStr = bodyStr.replace("\"[", "[");
                            bodyStr = bodyStr.replace("]\"", "]");
                            body = JsonUtil.StringToObject(bodyStr, Map.class);
                        } else if (null != newApplicant.get(MifinConstants.body)) {
                            body = (Map<String, Object>) newApplicant.get(MifinConstants.body);
                        }

                        if (null != body && body.containsKey(MifinConstants.RECEIVE)) {
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                if (null != receive && receive.containsKey(MifinConstants.STATUS) && receive.get(MifinConstants.STATUS).equals(MifinConstants.STATUS_S)) {
                                    apiStatus = MifinConstants.STATUS_S;
                                    applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMiFinApplicantCode(receive.containsKey(MifinConstants.APPLICANTCODE) ? (String) receive.get(MifinConstants.APPLICANTCODE) : "");
                                    applicationRequest.setMiFinProspectCode(receive.containsKey(MifinConstants.PROSPECTCODE) ? (String) receive.get(MifinConstants.PROSPECTCODE) : "");
                                    applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinEmploymentDetailsId(receive.containsKey(MifinConstants.EMPLOYMENTDETAILS_ID) ? (String) receive.get(MifinConstants.EMPLOYMENTDETAILS_ID) : "");
                                    log.info("updateNewLoanGonogoDetails for ref : {} APPLICANTCODE : {}  PROSPECTCODE : {} EMPLOYMENTDETAILS_ID : {}",applicationRequest.getRefID(),
                                            applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMiFinApplicantCode(), applicationRequest.getMiFinProspectCode(),
                                            applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMifinEmploymentDetailsId());
                                    updateGoNoGoAddressId(receive, applicationRequest);

                                }
                            }
                            applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinApplicantMessage(receive.containsKey(MifinConstants.MESSAGE) ? (String) receive.get(MifinConstants.MESSAGE) : "");
                        }
                    }
                }
            }
            saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_CREATE_LOAN, newLoanCreationRequest, applicationRequest.getRefID(), apiStatus);
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        }
        catch (Exception e) {
            log.error("Exception Occured while calling New Loan API ");
            e.printStackTrace();
        }
    }

    //update address ids
    private void updateGoNoGoAddressId(Map<String , Object> receive, ApplicationRequest applicationRequest) {
        Applicant applicant = applicationRequest.getRequest().getApplicant();
        int addressIndex = 0;
        for(CustomerAddress address: applicant.getAddress())
        {
            if(StringUtils.equals(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())) {
                if(!receive.get(MifinConstants.RESIADDRESS_ID).toString().contains("null")){
                    applicant.getAddress().get(addressIndex).setMifinAddressId(receive.containsKey(MifinConstants.RESIADDRESS_ID) ? (String)receive.get(MifinConstants.RESIADDRESS_ID) :"" );
                    log.info("updateGoNoGoAddressId for ref : {} addressIndex : {} RESIADDRESS_ID : {}",applicationRequest.getRefID(),addressIndex,
                            applicant.getAddress().get(addressIndex).getMifinAddressId());
                }
            } else if((StringUtils.equals(address.getAddressType(), GNGWorkflowConstant.PERMANENT.toFaceValue()))) {
                if(!receive.get(MifinConstants.PERMAADDRESS_ID).toString().contains("null")){
                    applicant.getAddress().get(addressIndex).setMifinAddressId(receive.containsKey(MifinConstants.PERMAADDRESS_ID) ? (String)receive.get(MifinConstants.PERMAADDRESS_ID) :"" );
                    log.info("updateGoNoGoAddressId for ref : {} addressIndex : {} PERMAADDRESS_ID : {}",applicationRequest.getRefID(),addressIndex,
                            applicant.getAddress().get(addressIndex).getMifinAddressId());
                }
            } else if((StringUtils.equals(address.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue()))) {
                if(!receive.get(MifinConstants.OFFADDRESS_ID).toString().contains("null")){
                    applicant.getAddress().get(addressIndex).setMifinAddressId(receive.containsKey(MifinConstants.OFFADDRESS_ID) ? (String)receive.get(MifinConstants.OFFADDRESS_ID) :"" );
                    log.info("updateGoNoGoAddressId for ref : {} addressIndex : {} OFFADDRESS_ID : {}",applicationRequest.getRefID(),addressIndex,
                            applicant.getAddress().get(addressIndex).getMifinAddressId());
                }
            }
            addressIndex++;
        }
    }


    private void updateSaveApplicantGoNoGoDetails(SaveApplicantRequest saveApplicantRequest, String mifinAmbitResponseStr , ApplicationRequest applicationRequest, Applicant applicant) throws Exception {

        try{
            Map<String , Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr , Map.class);
            String apiStatus = MifinConstants.STATUS_F;

            if (null != mifinAmbitResponse && null!= mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String , Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if(null != mifin && null != mifin.get(MifinConstants.saveApplicantDetails) ) {
                    Map<String , Object> newApplicant = (Map<String, Object>) mifin.get(MifinConstants.saveApplicantDetails);
                    if(null != newApplicant && null != newApplicant.get(MifinConstants.body)){
                        Map<String , Object> body = (Map<String, Object>) newApplicant.get(MifinConstants.body);
                        if(null != body && body.containsKey(MifinConstants.RECEIVE)) {
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            if (null != applicant.getAmbitMifinData()) {
                                if (null != receive && receive.containsKey(MifinConstants.STATUS) && receive.get(MifinConstants.STATUS).equals(MifinConstants.STATUS_S)) {
                                    apiStatus = MifinConstants.STATUS_S;
                                    applicant.getAmbitMifinData().setMiFinApplicantCode(receive.containsKey(MifinConstants.APPLICANTCODE) ? (String) receive.get(MifinConstants.APPLICANTCODE) : "");
                                    applicant.getAmbitMifinData().setMifinEmploymentDetailsId(receive.containsKey(MifinConstants.EMPLOYMENTDETAILS) ? (String) receive.get(MifinConstants.EMPLOYMENTDETAILS) : "");
                                    log.info("updateSaveApplicantGoNoGoDetails for ref : {} applicantId : {} APPLICANTCODE : {}  EMPLOYMENTDETAILS : {}",applicationRequest.getRefID(),
                                            applicant.getApplicantId(), applicant.getAmbitMifinData().getMiFinApplicantCode(), applicant.getAmbitMifinData().getMifinEmploymentDetailsId());

                                }
                                applicant.getAmbitMifinData().setMifinApplicantMessage(receive.containsKey(MifinConstants.MESSAGE) ? (String) receive.get(MifinConstants.MESSAGE) : "");
                            }
                        }
                    }
                }
            }

            saveCallLogforAmbit(mifinAmbitResponseStr,ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_DETAILS , saveApplicantRequest,applicationRequest.getRefID() , apiStatus);
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

        } catch(Exception e) {
            log.error("Exception Occured while calling Save Applicant API", e.getMessage());
            e.printStackTrace();
            saveCallLogforAmbit(mifinAmbitResponseStr,ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_DETAILS , saveApplicantRequest,applicationRequest.getRefID() , "F");
        }
    }

    @Override
    public BaseResponse saveApplicantAddress(ApplicationRequest applicationRequest, String userId) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_ADDRESS);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS);
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);
        MifinAmbitResponse mifinAmbitResponse = null;
        String appId = null;
        int addressIndex = 0;
        Applicant applicant = applicationRequest.getRequest().getApplicant();
        appId = applicant.getApplicantId();
        for(CustomerAddress address : applicant.getAddress()) {
            String addressType = address.getAddressType();
            SaveApplicantAddressRequest saveApplicantAddressRequest = ambitMifinRequestBuilder.buildSaveApplicantAddressRequest(applicationRequest, appId, addressType, userId);
            log.debug("MiFin Applicant Address request for refId {} : {}", refId, JsonUtil.ObjectToString(saveApplicantAddressRequest));

            String mifinAmbitResponseStr  = TransportUtils.postJsonRequest(saveApplicantAddressRequest, url);
            log.info("MiFin Applicant Address response for refId {} : {} ", refId, mifinAmbitResponseStr);

            updateSaveApplicantAddressId(saveApplicantAddressRequest,mifinAmbitResponseStr,applicationRequest,applicant ,addressIndex);

            addressIndex++;
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return GngUtils.getBaseResponse(HttpStatus.OK, mifinAmbitResponse);
    }

    @Override
    public BaseResponse saveCoApplicantAddress(ApplicationRequest applicationRequest, String userId) throws Exception {
        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction("MIFIN_SAVE_COAPPLICANT_ADDRESS");
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS);
        activityLogs.setRefId(applicationRequest.getRefID());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);
        MifinAmbitResponse mifinAmbitResponse = null;
        String appId = null;
        int addressIndex = 0;

        for( CoApplicant coApplicant : applicationRequest.getRequest().getCoApplicant()) {
            appId = coApplicant.getApplicantId();
            addressIndex = 0;
            if(null != coApplicant.getAmbitMifinData() && StringUtils.isNotEmpty(coApplicant.getAmbitMifinData().getMiFinApplicantCode())) {

                if (CollectionUtils.isNotEmpty(coApplicant.getAddress())) {
                    for (CustomerAddress address : coApplicant.getAddress()) {
                        String addressType = address.getAddressType();
                        SaveApplicantAddressRequest saveApplicantAddressRequest = ambitMifinRequestBuilder.buildSaveApplicantAddressRequest(applicationRequest, appId, addressType, userId);
                        log.debug("MiFin Coapplicant Address request for refId {} : {}", refId, JsonUtil.ObjectToString(saveApplicantAddressRequest));

                        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(saveApplicantAddressRequest, url);
                        log.info("MiFin Applicant Address response for refId {} : {} ", refId, mifinAmbitResponseStr);

                        updateSaveApplicantAddressId(saveApplicantAddressRequest, mifinAmbitResponseStr, applicationRequest, coApplicant, addressIndex);
                        addressIndex++;
                    }
                }
            }
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return GngUtils.getBaseResponse(HttpStatus.OK, mifinAmbitResponse);
    }

    public void  updateSaveApplicantAddressId(SaveApplicantAddressRequest saveApplicantAddressRequest , String  mifinAmbitResponseStr,ApplicationRequest applicationRequest, Applicant applicant, int addressIndex) throws Exception {
        String apiStatus = MifinConstants.STATUS_F;
        try {
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);

            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.saveApplicantAddress)) {
                    Map<String, Object> saveApplicantAddress = (Map<String, Object>) mifin.get(MifinConstants.saveApplicantAddress);
                    if (null != saveApplicantAddress && null != saveApplicantAddress.get(MifinConstants.bodyStr)) {
                        String responseStr = (String) saveApplicantAddress.get(MifinConstants.bodyStr);
                        responseStr = responseStr.replace("\"[", "[");
                        responseStr = responseStr.replace("]\"", "]");
                        SaveApplicantAddressBody saveApplicantAddressBody = JsonUtil.StringToObject(responseStr, SaveApplicantAddressBody.class);
                        MiFinSaveApplicantAddressBaseResponse miFinSaveApplicantAddressBaseResponse = saveApplicantAddressBody.getMiFinSaveApplicantAddressBaseResponse();

                        if(null != miFinSaveApplicantAddressBaseResponse && miFinSaveApplicantAddressBaseResponse.getStatus().equals(MifinConstants.STATUS_S)) {
                            apiStatus = MifinConstants.STATUS_S;
                            applicant.getAddress().get(addressIndex).setMifinAddrMessage(miFinSaveApplicantAddressBaseResponse.getMessage());
                            String addressId=miFinSaveApplicantAddressBaseResponse.getOther().get(MifinConstants.APPLICANT_ADDRESS).toString();
                            addressId = addressId.replace("[{ADDRESSID=", "");
                            addressId = addressId.replace("}]", "");
                            applicant.getAddress().get(addressIndex).setMifinAddressId(addressId);
                            log.info("updateSaveApplicantAddressId for ref : {} applicant : {} addressIndex : {} MESSAGE : {} mifinAddressId : {} ",applicationRequest.getRefID(),
                                    applicant.getApplicantId(), addressIndex, applicant.getAddress().get(addressIndex).getMifinAddrMessage(), applicant.getAddress().get(addressIndex).getMifinAddressId());
                        }

                    } else {
                        Map<String, Object> body = (Map<String, Object>) saveApplicantAddress.get(MifinConstants.body);
                        if (body.containsKey(MifinConstants.RECEIVE) && null != body.get(MifinConstants.RECEIVE)) {
                            Map<String, Object> receive = (Map<String, Object>) body.get(MifinConstants.RECEIVE);
                            applicant.getAddress().get(addressIndex).setMifinAddrMessage((String) receive.get(MifinConstants.MESSAGE));
                            log.info("updateSaveApplicantAddressId for ref : {} inside else",applicationRequest.getRefID());
                        }
                    }
                }
            }
        } catch(Exception e) {
            log.error("Error occured while saving Address Response {}", e.getMessage());
        }
        saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_ADDRESS, saveApplicantAddressRequest, applicationRequest.getRefID(), apiStatus);
        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
    }

    @Override
    public void deleteMifinApplicant(ApplicationRequest applicationRequest, List<String> coApplicantIds, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_DELETE_APPLICANT);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        DeleteApplicantRequest deleteApplicantRequest = ambitMifinRequestBuilder.buildDeleteApplicantRequest(applicationRequest, coApplicantIds, userId);
        log.debug("MiFin Delete Applicant request for refid {} : {}", refId, JsonUtil.ObjectToString(deleteApplicantRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(deleteApplicantRequest, url);
        log.info("MiFin Delete Applicant response for refId {} : {} ", refId, mifinAmbitResponseStr);

        try{
            Map<String , Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr , Map.class);
            String apiStatus = MifinConstants.STATUS_F;
            if (null != mifinAmbitResponse && null!= mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String , Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if(null != mifin && null != mifin.get(MifinConstants.deleteApplicant) ) {
                    Map<String , Object> deleteApplicant = (Map<String, Object>) mifin.get(MifinConstants.deleteApplicant);
                    if(null != deleteApplicant){
                        if( null != deleteApplicant.get(MifinConstants.body)){
                            Map<String , Object> body = (Map<String, Object>) deleteApplicant.get(MifinConstants.body);
                            if(body.containsKey(MifinConstants.STATUS) && body.get(MifinConstants.STATUS).equals(MifinConstants.STATUS_S)){
                                apiStatus = MifinConstants.STATUS_S;
                            }
                        }else  if( null != deleteApplicant.get(MifinConstants.bodyStr)){
                            String responseStr = (String) deleteApplicant.get(MifinConstants.bodyStr);
                            responseStr = responseStr.replace("\"[", "[");
                            responseStr = responseStr.replace("]\"", "]");
                            DeleteApplicantBaseResponse deleteApplicantBaseResponse= JsonUtil.StringToObject(responseStr, DeleteApplicantBaseResponse.class);
                            apiStatus = null != deleteApplicantBaseResponse ? deleteApplicantBaseResponse.getStatus(): MifinConstants.STATUS_F;
                        }

                    }
                }
            }
            saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_DELETE_APPLICANT, deleteApplicantRequest, refId, apiStatus);
        } catch(Exception e){
            log.info("Error Occured while saving Delete Applicant Response{}", e.getMessage());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
    }

    @Override
    public boolean crDecision(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_CR_DECISION);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        CrDecisionRequest crDecisionRequest = ambitMifinRequestBuilder.buildCrdecisionRequest(applicationRequest, userId);
        log.debug("MiFin CR Decision request for refid {} : {}", refId, JsonUtil.ObjectToString(crDecisionRequest));

        String mifinAmbitResponseStr =  TransportUtils.postJsonRequest(crDecisionRequest, url);
        log.info("MiFin CR Decision status Info response for refId {} : {} ", refId, mifinAmbitResponseStr);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return updateCrDecisionGonoGoApplicaion(mifinAmbitResponseStr,applicationRequest,crDecisionRequest);
    }


    public boolean updateCrDecisionGonoGoApplicaion(String mifinAmbitResponseStr ,ApplicationRequest applicationRequest,CrDecisionRequest crDecisionRequest){
        String apiStatus = MifinConstants.STATUS_F;
        try {
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.saveCRDecision)) {
                    Map<String, Object> saveCrDecision = (Map<String, Object>) mifin.get(MifinConstants.saveCRDecision);
                    if (null != saveCrDecision ) {
                        Map<String, Object> body=null;
                        if(null !=saveCrDecision.get(MifinConstants.bodyStr)){
                            String bodyStr =  saveCrDecision.get(MifinConstants.bodyStr).toString();
                            bodyStr = bodyStr.replace("\"[", "[");
                            bodyStr = bodyStr.replace("]\"", "]");
                            body = JsonUtil.StringToObject(bodyStr, Map.class);
                        }
                        else if(null != saveCrDecision.get(MifinConstants.body)) {
                            body = (Map<String, Object>) saveCrDecision.get(MifinConstants.body);
                        }
                        if (null != body) {
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinCRDecisionMessage(body.get(MifinConstants.MESSAGE).toString());
                                if (null != body.get(MifinConstants.CUSTOMERDETAIL)) {
                                    List<Map<String, Object>> customerDetailsList = (List<Map<String, Object>>) body.get(MifinConstants.CUSTOMERDETAIL);
                                    for (Map<String, Object> customerDetails : customerDetailsList) {
                                        if (null != customerDetails.get(MifinConstants.CREDITDECISIONID)) {
                                            applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinCRDecisionId(customerDetails.get(MifinConstants.CREDITDECISIONID).toString());
                                        }
                                    }
                                }
                            }
                            saveCallLogforAmbit(mifinAmbitResponseStr,ThirdPartyFieldConstant.MIFIN_SAVE_CR_DECISION , crDecisionRequest,applicationRequest.getRefID() , body.get(MifinConstants.MESSAGE).toString());
                        }
                        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
                    }
                }
            }
        }catch (Exception e){
            log.info("Error Occured while saving CRDecision Response{}", e.getMessage());
        }
        return MifinConstants.STATUS_S.equals(apiStatus);
    }

    @Override
    public boolean savePersonalInsurance(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_PERSONAL_INSURANCE);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        List<SavePersonalInsuranceRequest> savePersonalInsuranceRequestList = ambitMifinRequestBuilder.buildSavePersonalInsuranceRequest(applicationRequest, userId);
        log.debug("MiFin SavePersonalInsurance request for refid {} : {}", refId, JsonUtil.ObjectToString(savePersonalInsuranceRequestList));

        if(CollectionUtils.isNotEmpty(savePersonalInsuranceRequestList)) {
            List<String> mifinAmbitResponseStrList = savePersonalInsuranceRequestList.stream()
                    .filter(savePersonalInsuranceRequest-> (null != savePersonalInsuranceRequest))
                    .map( savePersonalInsuranceRequest-> {
                        try {
                            return TransportUtils.postJsonRequest(savePersonalInsuranceRequest, url);
                        } catch (Exception e) {
                            log.error("Error occured {}", e.getMessage());
                            return null;
                        }
                    })
                    .collect(Collectors.toList());
            log.info("MiFin Save Personal Insurance Response for refId {} : {} ", refId, mifinAmbitResponseStrList);
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLogs);
            return updatePersonalInsuranceDetails(savePersonalInsuranceRequestList, mifinAmbitResponseStrList, applicationRequest);
        } else {
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLogs);
            return true;
        }
    }

    private boolean updatePersonalInsuranceDetails(List<SavePersonalInsuranceRequest> savePersonalInsuranceRequestList, List<String> mifinAmbitResponseStrList, ApplicationRequest applicationRequest) throws Exception {
        SavePersonalInsuranceBaseResponse savePersonalInsuranceBaseResponse = null;
        String apiStatus = MifinConstants.STATUS_F;
        Optional<InsuranceData> insuranceData = Optional.empty();
        try {
            for (String mifinAmbitResponseStr : mifinAmbitResponseStrList) {
                Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
                if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                    Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                    if (null != mifin && null != mifin.get(MifinConstants.savePersonalInsurance)) {
                        Map<String, Object> savePersonalInsurance = (Map<String, Object>) mifin.get(MifinConstants.savePersonalInsurance);
                        if (null != savePersonalInsurance && null != savePersonalInsurance.get(MifinConstants.bodyStr)) {
                            String responseStr = (String) savePersonalInsurance.get(MifinConstants.bodyStr);
                            responseStr = responseStr.replace("\"[", "[");
                            responseStr = responseStr.replace("]\"", "]");
                            savePersonalInsuranceBaseResponse = JsonUtil.StringToObject(responseStr, SavePersonalInsuranceBaseResponse.class);
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinInsuranceMesssage(savePersonalInsuranceBaseResponse.getMessage());
                            }
                        } else {
                            Map<String, Object> body = (Map<String, Object>) savePersonalInsurance.get(MifinConstants.body);
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinInsuranceMesssage((String) body.get(MifinConstants.MESSAGE));
                            }
                        }
                    }
                }
            }
            if (null != savePersonalInsuranceBaseResponse && null != savePersonalInsuranceBaseResponse.getInsuranceDetailsList()) {
                List<InsuranceDetails> insuranceDetailsList = savePersonalInsuranceBaseResponse.getInsuranceDetailsList();
                insuranceData = Optional.ofNullable(applicationRepository.fetchInsuranceData(applicationRequest.getRefID()));
                if (insuranceData.isPresent()) {
                    Optional<List<Applicant>> applicantList = Optional.ofNullable(insuranceData.get().getApplicantList());
                    if (applicantList.isPresent()) {
                        for (Applicant applicant : applicantList.get()) {
                            Optional<List<InsurancePolicy>> insurancePolicyList = Optional.ofNullable(applicant.getInsurancePolicyList());
                            List<String> insuranceIdList = null;
                            for (InsuranceDetails insuranceDetails : insuranceDetailsList) {
                                if (null != applicant.getAmbitMifinData()) {

                                    if (insuranceDetails.getApplicantCode().equals(applicant.getAmbitMifinData().getMiFinApplicantCode())) {
                                        if (null == insuranceIdList) {
                                            insuranceIdList = new ArrayList<>();
                                        }
                                        insuranceIdList.add(insuranceDetails.getPersonalInsId());
                                    }
                                }
                            }
                            if (insurancePolicyList.isPresent() && null != insuranceIdList) {
                                int counter = 0;
                                for (InsurancePolicy insurancePolicy : insurancePolicyList.get()) {
                                    insurancePolicy.setMifinInsuranceId(insuranceIdList.size() > counter ? insuranceIdList.get(counter) : "");
                                    counter++;
                                }
                            }
                        }
                    }
                }
                apiStatus = savePersonalInsuranceBaseResponse.getStatus();
            }
            if (insuranceData.isPresent()) {
                applicationRepository.saveInsuranceData(InsuranceRequest.builder()
                        .header(applicationRequest.getHeader())
                        .refId(applicationRequest.getRefID())
                        .insuranceData(insuranceData.get())
                        .build());
            }
            saveCallLogforAmbit(mifinAmbitResponseStrList, ThirdPartyFieldConstant.MIFIN_SAVE_PERSONAL_INSURANCE, savePersonalInsuranceRequestList, applicationRequest.getRefID(), apiStatus);
        }catch (Exception e){
            log.info("Error Occured while saving Insurance response {}", e.getMessage());
        }
        return MifinConstants.STATUS_S.equals(apiStatus);
    }

    @Override
    public boolean saveVerification(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);
        boolean apiStatus = true;

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_SAVE_VERIFICATION);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SaveVerificationRequest saveVerificationRequest = ambitMifinRequestBuilder.buildSaveVerificationBasicInfoRequest(applicationRequest, userId);
        log.debug("Mifin Save Verification Request {}", JsonUtil.ObjectToString(saveVerificationRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(saveVerificationRequest, url);
        log.info("MiFin Save Verification response for refId {} : {} ", refId, mifinAmbitResponseStr);

        apiStatus = updateGoNoGoVerificationDetails(applicationRequest, saveVerificationRequest, mifinAmbitResponseStr);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return apiStatus;
    }

    private boolean updateGoNoGoVerificationDetails(ApplicationRequest applicationRequest,SaveVerificationRequest saveVerificationRequest, String mifinAmbitResponseStr ) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        MifinSaveVerificationBaseResponse mifinSaveVerificationBaseResponse = null ;
        Map<String, Object> mifinAmbitResponse = null;
        int index = 0;
        String apiStatus = MifinConstants.STATUS_F;
        try {
            mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);

            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if(null != mifin && null != mifin.get(MifinConstants.saveVerification)) {
                    Map<String, Object> saveVerification = (Map<String, Object>) mifin.get(MifinConstants.saveVerification);
                    //BodyStr
                    if(null != saveVerification && null != saveVerification.get(MifinConstants.bodyStr)) {
                        String responseStr = (String) saveVerification.get(MifinConstants.bodyStr);
                        responseStr = responseStr.replace("\"[", "[");
                        responseStr = responseStr.replace("]\"", "]");
                        mifinSaveVerificationBaseResponse = JsonUtil.StringToObject(responseStr, MifinSaveVerificationBaseResponse.class);
                        if(null != mifinSaveVerificationBaseResponse && StringUtils.equals(MifinConstants.STATUS_S, mifinSaveVerificationBaseResponse.getStatus())) {
                            List<VerificationIdResponse> verificationIdResponses = mifinSaveVerificationBaseResponse.getVerificationDetails();
                            if(null != verificationIdResponses) {
                                for(VerificationIdResponse verificationIdResponse : verificationIdResponses) {
                                    log.info("Mifin SAVE VERIFICATION is Successful for ProspectID= {} , verificationId= {}", verificationIdResponse.getProspectId(),
                                            verificationIdResponse.getVerificationId());
                                }
                                apiStatus = MifinConstants.STATUS_S;
                                if(null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                    applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinVerificationMessage(mifinSaveVerificationBaseResponse.getMessage());
                                }
                            }
                        }
                    }
                    //body response
                    if(null != saveVerification && null != saveVerification.get(MifinConstants.body)) {
                        Map<String, Object> body = (Map<String, Object>) saveVerification.get(MifinConstants.body);
                        if(body.containsKey(MifinConstants.STATUS))
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                log.info("Mifin Verification Response: Status={}, Message= {}", body.get(MifinConstants.STATUS), body.get(MifinConstants.MESSAGE));
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinVerificationMessage((String) body.get(MifinConstants.MESSAGE));
                            }
                    }
                    applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
                }
            }
            // update gonogoDetails if response status is success
            if(StringUtils.equals(MifinConstants.STATUS_S, apiStatus)) {
                VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, institutionId);
                if(null != mifinSaveVerificationBaseResponse) {
                    List<VerificationIdResponse> verificationIds = mifinSaveVerificationBaseResponse.getVerificationDetails();
                    if(null != verificationIds) {
                        int verfIndex = 0;
                        VerificationIdResponse verificationIdResponse = verificationIds.get(verfIndex);
                        //Residence verification
                        List<PropertyVerification> propertyVerifications = verificationDetails.getResidenceVerification();
                        if(null != propertyVerifications) {
                            index = 0;
                            for (PropertyVerification propertyVerification : propertyVerifications) {
                                verificationIdResponse = verificationIds.get(verfIndex);
                                verificationDetails.getResidenceVerification().get(index).setMifinVerificationId(verificationIdResponse.getVerificationId());
                                verfIndex++;
                                index++;
                            }
                        }
                        //Office Verification
                        propertyVerifications = verificationDetails.getOfficeVerification();
                        if(null != propertyVerifications) {
                            index = 0;
                            for (PropertyVerification propertyVerification : propertyVerifications) {
                                verificationIdResponse = verificationIds.get(verfIndex);
                                verificationDetails.getOfficeVerification().get(index).setMifinVerificationId(verificationIdResponse.getVerificationId());
                                verfIndex++;
                                index++;
                            }
                        }
                        applicationRepository.updateMifinVerificationId(verificationDetails);
                    }
                }
            }
            saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_SAVE_VERIFICATION, saveVerificationRequest, refId, apiStatus);
        }catch (Exception e){
            log.error("Error occured while saving VERIFICATION Response {}",e.getMessage());
            saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_SAVE_VERIFICATION, saveVerificationRequest, refId, MifinConstants.STATUS_F);
        }
        return MifinConstants.STATUS_S.equals(apiStatus);
    }

    @Override
    public boolean updateLoanDetail(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);
        Map<String, Object> mifinAmbitResponse = null;

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_LOAN_DETAILS);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        UpdateLoanDetailsRequest updateLoanDetailsRequest = ambitMifinRequestBuilder.buildLoanDeatilsBasicInfoRequest(applicationRequest, userId);
        log.debug("MiFin Update Loan Details Info request for refid {} : {}", refId, JsonUtil.ObjectToString(updateLoanDetailsRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(updateLoanDetailsRequest, url);
        log.info("MiFin Update Loan Details Info response for refId {} : {} ", refId, mifinAmbitResponseStr);
        String apiStatus = MifinConstants.STATUS_F;
        try {
            mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if(null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if(null != mifin && null != mifin.get(MifinConstants.updateLoanDetails)) {
                    Map<String, Object> updateLoanDetails = (Map<String, Object>) mifin.get(MifinConstants.updateLoanDetails);
                    //BodyStr
                    if(null != updateLoanDetails && null != updateLoanDetails.get(MifinConstants.bodyStr)) {
                        MifinLoanDetailBaseResponse mifinLoanDetailBaseResponse = null;
                        String responseStr = (String) updateLoanDetails.get(MifinConstants.bodyStr);
                        responseStr = responseStr.replace("\"[", "[");
                        responseStr = responseStr.replace("]\"", "]");
                        mifinLoanDetailBaseResponse = JsonUtil.StringToObject(responseStr, MifinLoanDetailBaseResponse.class);
                        if(null != mifinLoanDetailBaseResponse && StringUtils.equals(MifinConstants.STATUS_S, mifinLoanDetailBaseResponse.getStatus())) {
                            List<AmbitMifinCustomerDetails> loanDetailList = mifinLoanDetailBaseResponse.getLoanDetailList();
                            if(null != loanDetailList) {
                                for (AmbitMifinCustomerDetails ambitMifinloanDetail : loanDetailList) {
                                    log.info("Mifin Update Loan Details is Successful for ProspectCode= {} , ProspectId= {}", ambitMifinloanDetail.getProspectId(),
                                            ambitMifinloanDetail.getProspectCode());
                                }
                                apiStatus = MifinConstants.STATUS_S;
                                if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                    applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinLoanDeatilMessage(mifinLoanDetailBaseResponse.getMessage());
                                }
                            }
                        }
                    }
                    //body response
                    if(null != updateLoanDetails && null != updateLoanDetails.get(MifinConstants.body)) {
                        Map<String, Object> body = (Map<String, Object>) updateLoanDetails.get(MifinConstants.body);
                        if(body.containsKey(MifinConstants.STATUS)) {
                            log.info("Mifin Update loan Details Response: Status={}, Message= {}", body.get(MifinConstants.STATUS), body.get(MifinConstants.MESSAGE));
                            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinLoanDeatilMessage(body.get(MifinConstants.MESSAGE).toString());
                            }
                        }
                    }
                }
            }
            saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_LOAN_DETAILS, updateLoanDetailsRequest, refId, apiStatus);
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        } catch (Exception e) {
            log.error("Error occured while saving update LOAN DETAILS Response {}", e.getMessage());
            saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_LOAN_DETAILS, updateLoanDetailsRequest, refId, MifinConstants.STATUS_F);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return MifinConstants.STATUS_S.equals(apiStatus);
    }

    @Override
    public boolean saveDisbursalMaker(ApplicationRequest applicationRequest, String userId) throws Exception {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String url = getUrl(institutionId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction(ThirdPartyFieldConstant.MIFIN_DISBURSAL_MAKER);
        activityLogs.setStage(applicationRequest.getCurrentStageId());
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SaveDisbursalMakerRequest saveDisbursalMakerRequest = ambitMifinRequestBuilder.buildSaveDisbursalMaker(applicationRequest, userId);
        log.debug("Mifin Save DisbursalMaker Request {}", JsonUtil.ObjectToString(saveDisbursalMakerRequest));

        String mifinAmbitResponseStr = TransportUtils.postJsonRequest(saveDisbursalMakerRequest, url);
        log.info("MiFin Save DisbursalMaker response for refId {} : {} ", refId, mifinAmbitResponseStr);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        log.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return updateDisbursalGoNoGoInfo(applicationRequest, saveDisbursalMakerRequest, mifinAmbitResponseStr);
    }

    private boolean updateDisbursalGoNoGoInfo(ApplicationRequest applicationRequest, SaveDisbursalMakerRequest saveDisbursalMakerRequest, String mifinAmbitResponseStr) throws Exception {
        String refId = applicationRequest.getRefID();
        String apiStatus = MifinConstants.STATUS_F;
        try {
            Map<String, Object> mifinAmbitResponse = JsonUtil.StringToObject(mifinAmbitResponseStr, Map.class);
            if (null != mifinAmbitResponse && null != mifinAmbitResponse.get(MifinConstants.mifin)) {
                Map<String, Object> mifin = (Map<String, Object>) mifinAmbitResponse.get(MifinConstants.mifin);
                if (null != mifin && null != mifin.get(MifinConstants.saveDisbursalMaker)) {
                    Map<String, Object> saveDisbursalMaker = (Map<String, Object>) mifin.get(MifinConstants.saveDisbursalMaker);
                    //BodyStr
                    if (null != saveDisbursalMaker && null != saveDisbursalMaker.get(MifinConstants.bodyStr)) {
                        MifinDisbursalMakerBaseResponse mifinDisbursalMakerBaseResponse = null;
                        String responseStr = (String) saveDisbursalMaker.get(MifinConstants.bodyStr);
                        responseStr = responseStr.replace("\"[", "[");
                        responseStr = responseStr.replace("]\"", "]");
                        mifinDisbursalMakerBaseResponse = JsonUtil.StringToObject(responseStr, MifinDisbursalMakerBaseResponse.class);
                        if (null != mifinDisbursalMakerBaseResponse && StringUtils.equals(MifinConstants.STATUS_S, mifinDisbursalMakerBaseResponse.getStatus())) {
                            List<DisbursalDetailResponse> disbursalDetailResponseList = mifinDisbursalMakerBaseResponse.getDisbursalDetailResponses();
                            if (null != disbursalDetailResponseList) {
                                for (DisbursalDetailResponse disbursalDetailResponse : disbursalDetailResponseList) {
                                    log.info("Mifin Save Disbursal Maker is Successful for ProspectCode= {} , ProspectId= {} , DisbursalId= {}, LoanChargeId={},",disbursalDetailResponse.getProspectCode(),
                                            disbursalDetailResponse.getProspectId(), disbursalDetailResponse.getDisbursalId(), disbursalDetailResponse.getLoanChargeId());
                                }
                                apiStatus = MifinConstants.STATUS_S;
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinDisbDetailList(disbursalDetailResponseList);
                                applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinDisbMakerMessage(mifinDisbursalMakerBaseResponse.getMessage());
                            }
                        }
                    }
                    //body response
                    if (null != saveDisbursalMaker && null != saveDisbursalMaker.get(MifinConstants.body)) {
                        Map<String, Object> body = (Map<String, Object>) saveDisbursalMaker.get(MifinConstants.body);
                        if (body.containsKey(MifinConstants.STATUS)) {
                            log.info("Mifin Save DisbursalMaker Response: Status={}, Message= {}", body.get(MifinConstants.STATUS), body.get(MifinConstants.MESSAGE));
                            applicationRequest.getRequest().getApplicant().getAmbitMifinData().setMifinDisbMakerMessage(body.get(MifinConstants.MESSAGE).toString());
                        }
                    }
                }
            }
            saveCallLogforAmbit(mifinAmbitResponse, ThirdPartyFieldConstant.MIFIN_DISBURSAL_MAKER, saveDisbursalMakerRequest, refId, apiStatus);
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        }catch (Exception e) {
            log.error("Error while saving Mifin Save DisbursalMaker Response {}", e.getMessage());
            saveCallLogforAmbit(mifinAmbitResponseStr, ThirdPartyFieldConstant.MIFIN_DISBURSAL_MAKER, saveDisbursalMakerRequest, refId, MifinConstants.STATUS_F);
        }
        return MifinConstants.STATUS_S.equals(apiStatus);
    }

    private String getUrl(String institutionId) throws GoNoGoException {
        String urlType = UrlType.MIFIN.toValue();
        WFJobCommDomain customerDataPushConfig = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, urlType);

        if (null == customerDataPushConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }

        return Arrays.asList(customerDataPushConfig.getBaseUrl(), customerDataPushConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

    }

    private void saveCallLogforAmbit(Object mifinAmbitResponse, String type , Object request , String refId, String apiHit)throws Exception {
        log.info("MiFin response for refId {} : {} ", refId, mifinAmbitResponse);

        MiFinAmbitCallLog apiLog = MiFinAmbitCallLog.builder()
                .api(type)
                .refId(refId)
                .miFinRequest(request)
                .miFinResponse(mifinAmbitResponse)
                .request(JsonUtil.ObjectToString(request))
                .apiHit(apiHit).build();

        externalAPILogRepository.saveMifinAmbitLog(apiLog);
    }

    @Override
    public BaseResponse getMifinAmbitLogs(AmbitMifinLog ambitMifinLog) throws Exception {

        return GngUtils.getBaseResponse(HttpStatus.OK, externalAPILogRepository.fetchAmbitMifinCalllog(ambitMifinLog));
    }

    public String fetchUserId(Header header, String loggedInUserRole, String loggedInUserId) {
        LoginServiceResponse response = appConfigurationRepository.fetchLoggedUserDetail(header.getInstitutionId(), header.getProduct().toProductName(), header.getBranchCode(),
                loggedInUserRole, loggedInUserId);
        log.debug("Inside fetchUserId(), response = {} ", response);
        return (null != response && StringUtils.isNotEmpty(response.getEmployeeId()) ?
                response.getEmployeeId() : "LPU0000023");
    }

    public String getUser(Header header, String loggedInUserId) {
        String user = null;

        List<DropdownMaster> dropdownMasters = masterMongoRepository.fetchDropDownMaster(DropdownMasterRequest.builder()
                .dropDownType("USER_ID_Master")
                .header(header)
                .queryType(EndPointReferrer.ONE)
                .build());
        if(CollectionUtils.isNotEmpty(dropdownMasters)){
              DropdownMaster dropdownMaster = dropdownMasters.get(0);
              for(HashMap<String, String> map : dropdownMaster.getDropDownValueDetailsList()) {
                 if(StringUtils.equals(map.get("UAM_LOGIN_ID"), loggedInUserId)) {
                     user = map.get("MIFIN_USERID");
                     break;
                 }
             }
        }
        log.debug("LoggedIn UserId={},  MIFIN User Id={}" ,loggedInUserId, user);
        return user;
    }

}
