package com.softcell.service.impl;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.Buckets;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.ModuleManager;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.ThirdPartyApiLog;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.coorgination.icici.CoorginationAPIRequest;
import com.softcell.gonogo.model.coorgination.icici.CorporateAPIRequest;
import com.softcell.gonogo.model.coorgination.icici.CorporateAPIResponse;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.request.ThirdPartyRequest;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.core.scoring.response.RulesDetails;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.finbit.*;
import com.softcell.gonogo.model.finfort.FileUploadInfo;
import com.softcell.gonogo.model.finfort.FinfortCallLog;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dmz.DMZRequest;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.LoanChargesRepaymentResponse;
import com.softcell.gonogo.model.response.LoginDataResponse;
import com.softcell.gonogo.model.response.MCPAggregatorResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.security.v2.LoginBaseResponse;
import com.softcell.gonogo.model.security.v2.LogoutBaseResponse;
import com.softcell.gonogo.model.ssl.perfios.GenerateFileRequest;
import com.softcell.gonogo.model.ssl.perfios.GenerateFileResponse;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataEntryManager;
import com.softcell.service.ThirdPartyIntegrationManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.thirdparty.utils.ThirdPartyIntegrationHelper;
import com.softcell.service.utils.OriginationHelper;
import com.softcell.ssl2.finfort.model.FileUploadRequest;
import com.softcell.ssl2.finfort.model.FinfortStatusRequest;
import com.softcell.ssl2.perfios.PerfiosDataRequest;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.RequestAggregator;
import com.softcell.workflow.executors.extapi.ThirdPartyExecutor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.softcell.gonogo.model.core.BankingStatement.AppIdComparator;

/**
 * Created by ssg408 on 24/9/18.
 */
@Service
public class ThirdPartyIntegrationManagerImpl implements ThirdPartyIntegrationManager {
    private static Logger logger = LoggerFactory.getLogger(ThirdPartyIntegrationManagerImpl.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private DataEntryManager dataEntryManager;

    @Autowired
    private ModuleManager moduleManager;

    @Autowired
    private ThirdPartyIntegrationHelper thirdPartyIntegrationHelper;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private OriginationHelper originationHelper;

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Autowired
    private ModuleHelper moduleHelper;

    @Override
    public BaseResponse saveData(
            ApplicationRequest applicationRequest, String stepId, HttpServletRequest httpRequest) throws Exception {

        BaseResponse baseResponse = null;
        setAuthenticationDetails(httpRequest, applicationRequest );

        logger.debug("Request received from {} is \n {}\n==============", EndPointReferrer.PAISA_BAZAR, applicationRequest);
        String msg = "Request for " + stepId + " not handled.";
        Map<String, String> message = null;
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest, httpRequest,
                null, stepId, null, applicationRequest.getHeader().getLoggedInUserId());
        if (applicationRequest.getRefID() != null) {
            activityLogs.setRefId(applicationRequest.getRefID());
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            switch (stepId) {
                case EndPointReferrer.PAISA_BAZAR:
                case EndPointReferrer.AGGREGATOR:
                    activityLogs.setStage(GNGWorkflowConstant.DE.name());
                    activityLogs.setStatus(Status.NEW.toString());

                    String aggregatorUserId = applicationRequest.getAggregatorData().getLoginRole();
                    String aggregatorPassword = applicationRequest.getAggregatorData().getPassword();
                    LogoutBaseResponse logoutBaseResponse = null;

                    // Get user details from UAMThirdPartyIntegrationHelper
                    LoginBaseResponse loginBaseResponse =  thirdPartyIntegrationHelper.setLoginDetails(applicationRequest, aggregatorUserId, aggregatorPassword);
                    /*Validating authentication success*/
                    if(loginBaseResponse != null && loginBaseResponse.getBody() != null && loginBaseResponse.getBody().getLoginServiceResponse() != null
                            && loginBaseResponse.getBody().getLoginServiceResponse().getStatusCode() == 409){
                        logger.error("saveData: error received for OSOL while UAM login: {}",loginBaseResponse.getBody().getLoginServiceResponse().getMessage());
                        String tmpMsg = loginBaseResponse.getBody().getLoginServiceResponse().getMessage();

                        message = new HashMap<>();
                        message.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(),tmpMsg);
                    }else if(loginBaseResponse != null && loginBaseResponse.getError() != null){
                        logger.error("saveData: error received while UAM login: {}",loginBaseResponse.getError().getMessage());
                        String tmpMsg = loginBaseResponse.getError().getMessage();

                        message = new HashMap<>();
                        message.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(),tmpMsg);
                    }else if(loginBaseResponse != null && loginBaseResponse.getBody() != null){
                        setValues(stepId, applicationRequest);
                        baseResponse = dataEntryManager.save(applicationRequest, EndPointReferrer.STEP_THIRD_PARTY_DATA, httpRequest);
                        setStage(ConfigurationConstants.NODE_THIRD_PARTY_SAVE, ConfigurationConstants.WF_NODE_TYPE_SCREEN,
                                ConfigurationConstants.WF_EVENT_SAVE, baseResponse);

                        // Set refId for activity log
                        activityLogs.setRefId(applicationRequest.getRefID());
                        message = getMessage(baseResponse, institutionId);
                    }else{
                        message = new HashMap<>();
                        message.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(),ThirdPartyIntegrationHelper.AGGREGATOR.get(GNGWorkflowConstant.AUTH_ERROR.toFaceValue()));
                    }

                    //call logout, after every login
                    logoutBaseResponse = thirdPartyIntegrationHelper.setLogoutDetails(aggregatorUserId,institutionId);

                    break;
                default:
                    break;
            }
        } catch(Exception e) {
            logger.error("Exception occurred while call from {} : {}" , stepId, e.getMessage());
            e.printStackTrace();
        }

        // Save activity log
        stopWatch.stop();
        //activityLogs.setStatus(applicationRequest.getApplicationStatus());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        if( message != null && !message.isEmpty()){
            for (Map.Entry<String,String> entry : message.entrySet()){
                if(StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.QUEUED.toFaceValue()) ||
                        StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.DECLINED.toFaceValue())
                        || StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.APPROVED.toFaceValue())){
                    httpStatus = HttpStatus.OK;
                }else{
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                }
                msg = entry.getValue();
            }
        }
        return GngUtils.getBaseResponse(httpStatus, msg);
    }

    private void setAuthenticationDetails(HttpServletRequest httpRequest, ApplicationRequest applicationRequest ) {

        AggregatorData aggregatorData = null;
        aggregatorData = applicationRequest.getAggregatorData();
        if(aggregatorData == null){
            aggregatorData = new AggregatorData();
            applicationRequest.setAggregatorData(aggregatorData);
        }
        /*aggregatorData.setPassword(password);
        aggregatorData.setLoginRole(userName);*/
        aggregatorData.setUri(httpRequest.getRequestURI());
        aggregatorData.setContextPath(httpRequest.getContextPath());
        aggregatorData.setRemoteAddress(httpRequest.getRequestURI());
        aggregatorData.setPathInfo( httpRequest.getPathInfo());
        aggregatorData.setRemoteUser(httpRequest.getRemoteUser());
    }


    @Override
    public BaseResponse initiateFinfort(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse ;
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String product = applicationRequest.getHeader().getProduct().name();
        try {
            /*
            * Finfort to be triggered for applicant and coapplicants if they have agreed for consent.
            * Applicant> ThirdPartyCall > finfortConsent tells whether applicant has allowed us to get data from Finfort
            * We trigger finfort for these applicants in async manner and wait for all calls to finish
            */
            ExecutorService es = Executors.newCachedThreadPool();
            WFJobCommDomain serviceConfigForIndividual = null, serviceConfigForCorporate = null;
            // Load configurations
            Applicant applicant = applicationRequest.getRequest().getApplicant();
            if( applicant.getThirdPartyCall().isFinfortConsent()) {
                // Get the communication configuration based on applicant type

                boolean useIndConfig = true;
                if (GngUtils.isIndividual(applicant)) {
                    serviceConfigForIndividual = moduleManager.getServiceConfiguration(institutionId, product, UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL);
                } else {
                    useIndConfig = false;
                    serviceConfigForCorporate = moduleManager.getServiceConfiguration(institutionId, product, UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.INITIATE_CORPORATE);
                }
                // call ThirdPartyExecutor
                ThirdPartyExecutor executor = new ThirdPartyExecutor();
                executor.init(refId, institutionId,
                        useIndConfig ? serviceConfigForIndividual : serviceConfigForCorporate,  applicationRequest, applicant);
                es.execute(executor);
                logger.debug("{} Finfort calls initiated for applicant {}", refId, applicant.getApplicantId() );
            }
            // Call for co-applicants which have agreed upon calling Finfort
            List<CoApplicant> coApplicantList = applicationRequest.getRequest().getCoApplicant();
            if( CollectionUtils.isNotEmpty(coApplicantList)){
                // get all the coapplicants for whom finfort consent is set and  finfort not initiated
                List<CoApplicant> finfortCoApps = coApplicantList.stream()
                                                    .filter(coapp -> coapp.getThirdPartyCall().isFinfortConsent()
                                                                && !coapp.getThirdPartyCall().isFinfortInitiated())
                                                    .collect(Collectors.toList());

                for(CoApplicant coapp : finfortCoApps){
                    boolean useIndConfig = true;
                    if (GngUtils.isIndividual(coapp)) {
                        if( serviceConfigForIndividual == null)
                            serviceConfigForIndividual = moduleManager.getServiceConfiguration(institutionId, product,
                                    UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL);
                    } else {
                        useIndConfig = false;
                        if( serviceConfigForCorporate == null )
                            serviceConfigForCorporate = moduleManager.getServiceConfiguration(institutionId, product,
                                    UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.INITIATE_CORPORATE);
                    }
                    // call ThirdPartyExecutor
                    ThirdPartyExecutor executor = new ThirdPartyExecutor();
                    executor.init(refId, institutionId,
                            useIndConfig ? serviceConfigForIndividual : serviceConfigForCorporate,  applicationRequest, coapp);
                    es.execute(executor);
                    logger.debug("{} Finfort calls initiated for coapp {}", refId, coapp.getApplicantId() );
                }
            }
            es.shutdown();
            // Wait till all triggers are done
            try {
                boolean finshed = es.awaitTermination(2, TimeUnit.MINUTES);
            } catch (InterruptedException ie){
                logger.error("{}",ie.getStackTrace());
                logger.error("Error while running 3piExecutors {}", ie.getStackTrace());
            }
            // Save application request thirdparty calls
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

            logger.debug("{} Finfort calls ended", refId );
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationRequest);
        } catch ( Exception e ){
            logger.error("{}",e.getStackTrace());
            baseResponse = buildBaseResponse( e );
        }
        return baseResponse;
    }

    @Override
    public BaseResponse processFinfortStatus(FinfortStatusRequest statusRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = new BaseResponse();
        try {
        /* If status is success call SSL service to get the report.
        * */
            String ackId = statusRequest.getAckId();
            boolean success = true;
            if (StringUtils.isNotEmpty(ackId) ){
                // Get ref id for the ackId
                // Get camdetails of the case
                // Get service configuration to call SSL report

                String refId, institutionId, product, applicantId;
                refId = institutionId = product = null;
                FinfortCallLog finfortCallLog = externalAPILogRepository.fetchFinfortCallLogByAckId(ackId);
                if( finfortCallLog != null){
                    finfortCallLog.setCallDate(new Date());
                    refId = finfortCallLog.getRefId();
                    institutionId = finfortCallLog.getInstitutionId();
                    applicantId = finfortCallLog.getApplicantId();

                    // save finfort log for this call
                    finfortCallLog.setRequestType(ThirdPartyIntegrationHelper.STATUS_REPORT);
                    externalAPILogRepository.saveFinfortCallLog(finfortCallLog);
                    // product
                    ApplicationRequest applicationRequest = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId).getApplicationRequest();
                    product = applicationRequest.getRequest().getApplication().getProductID();
                    // Get service configuration
                    WFJobCommDomain serviceConfig = moduleManager.getServiceConfiguration(institutionId, product,
                            UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.FETCH_REPORT);

                    ThirdPartyExecutor thirdPartyExecutor = new ThirdPartyExecutor();
                    thirdPartyExecutor.init(refId, institutionId, serviceConfig, applicationRequest, finfortCallLog);
                    success = thirdPartyExecutor.execute();

                    if( success ){
                        baseResponse = GngUtils.getBaseResponse( HttpStatus.OK, "Fetched report");
                    } else {
                        baseResponse = GngUtils.getBaseResponse( HttpStatus.NO_CONTENT, "Problem while fetching report");
                    }
                } else {
                    baseResponse = GngUtils.getBaseResponse( HttpStatus.NO_CONTENT, "Ackid not found !");
                }
            }
        } catch (Exception e){
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    /*
    *  Store the file list in DB
       download the documents from ftp server
    */
    @Override
    public BaseResponse processFileList(FileUploadRequest fileUploadRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse ;
        logger.debug("Finfort ackId {} file list received {}",  fileUploadRequest.getAckId(), fileUploadRequest.getFileUploadData());
        try{
            // Fetch refId from db
            String ackId = fileUploadRequest.getAckId();
            FinfortCallLog fincallLog = externalAPILogRepository.fetchFinfortCallLogByAckId(ackId);

            String refId = fincallLog.getRefId();
            String institutionId = fincallLog.getInstitutionId();
            String applicantId = fincallLog.getApplicantId();
            String product = applicationRepository.getProduct(refId, institutionId);

            logger.debug("{} Finfort ackId {} ",  refId, ackId);

            fincallLog = new FinfortCallLog();
            fincallLog.setAcknowledgementId(ackId);
            fincallLog.setRequestType(ThirdPartyIntegrationHelper.FILE_LIST);
            fincallLog.setRefId(refId);
            fincallLog.setInstitutionId(institutionId);
            fincallLog.setApplicantId(applicantId);
            fincallLog.setRequest(fileUploadRequest);
            externalAPILogRepository.saveFinfortCallLog(fincallLog);
            logger.debug("{} Finfort ackId {} saved into calllog", refId, ackId);

            // Get service configuration for ftp connection
            WFJobCommDomain serviceConfig = moduleManager.getServiceConfiguration(institutionId, product,
                    UrlType.FINFORT.name(), ThirdPartyIntegrationHelper.SFTP_CONNECTION);

            FileUploadInfo uploadInfo = new FileUploadInfo();
            uploadInfo.setUploadRequest(fileUploadRequest);
            uploadInfo.setApplicantId(applicantId);

            // Doenload files from ftp site
            ExecutorService es = Executors.newCachedThreadPool();
            ThirdPartyExecutor executor = new ThirdPartyExecutor();
            executor.init(refId, institutionId, serviceConfig,  uploadInfo, product);
            es.execute(executor);
            logger.debug("{} Finfort ackId {} saved into calllog filedownload ended...", refId, ackId);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);

        } catch ( Exception e ){
            logger.error("{}",e.getStackTrace());
            baseResponse = buildBaseResponse( e );
        }
        return baseResponse;
    }

    private void setStage(String nodeValue,  String nodeType, String event,
                          BaseResponse baseResponse) throws Exception{
        /* If Bureau report is not good ( error in report/ error in pull )
                then set the stage to DE so that the errors can be rectified on UI.
        */
        Object response = baseResponse.getPayload().getT();
        if( response instanceof GoNoGoCustomerApplication){
            GoNoGoCustomerApplication gngApplication = (GoNoGoCustomerApplication)response;
            ApplicationRequest applicationRequest = gngApplication.getApplicationRequest();
            String incomingStage = gngApplication.getApplicationRequest().getCurrentStageId();
            setCompletedInfo(gngApplication);
            if( moduleManager.isMBSuccessful ( gngApplication.getApplicantComponentResponse()) ) {
                moduleManager.setStage(nodeValue,  nodeType, event, gngApplication);
            } else {
                gngApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.DE.toFaceValue());
                moduleManager.updateStageId(applicationRequest, null);
                logger.info("cibilFlag while hit by aggregator {}", gngApplication.getApplicationRequest().getAggregatorData().isCibilHit());
                gngApplication.getApplicationRequest().getAggregatorData().setCibilHit(true);
                applicationRepository.updateGoNoGoCustomerApplication(gngApplication.getApplicationRequest());
            }
            if( StringUtils.equalsIgnoreCase(incomingStage, applicationRequest.getCurrentStageId()) ){
                String stepId = "submit-by-" + applicationRequest.getHeader().getLoggedInUserId();
                applicationRepository.updateCompletedInfo(gngApplication, stepId, applicationRequest.getHeader().getLoggedInUserId(),
                        applicationRequest.getHeader().getLoggedInUserRole());
            }

        }
    }

    private void setCompletedInfo(GoNoGoCustomerApplication gngApplication) {
        Map<String, CompletionInfo> completedInfoMap = gngApplication.getCompleted();
        if( completedInfoMap != null ){
            String userId = gngApplication.getApplicationRequest().getHeader().getLoggedInUserId();
            String role = gngApplication.getApplicationRequest().getHeader().getLoggedInUserRole();
            CompletionInfo info = new CompletionInfo();
            info.setUserId(userId);
            info.setRole(role);
            completedInfoMap.put(EndPointReferrer.STEP_REGISTRATION, info);
            completedInfoMap.put(EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS, info);
            completedInfoMap.put(EndPointReferrer.STEP_PROFESSION_INCOME_DETAILS, info);
        }
    }

    private void setValues(String thirdParty, ApplicationRequest applicationRequest) {
        if( StringUtils.equalsIgnoreCase(thirdParty, EndPointReferrer.PAISA_BAZAR) ||
                StringUtils.equalsIgnoreCase(thirdParty, EndPointReferrer.AGGREGATOR)){
            Application application = applicationRequest.getRequest().getApplication();
            Applicant applicant = applicationRequest.getRequest().getApplicant();

            //make all names in upper case letter for sbfc
            if(Institute.isInstitute(applicationRequest.getHeader().getInstitutionId(),Institute.SBFC)){
                moduleHelper.makeNameCapital(applicationRequest.getRequest());
            }

            // Sourcing channel
            application.setChannel(
                    ThirdPartyIntegrationHelper.AGGREGATOR.get(ThirdPartyIntegrationHelper.CHANNEL));
            if( CollectionUtils.isNotEmpty(applicant.getPhone()) ){
                Phone phone = GngUtils.getPhone( GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(), applicant.getPhone()).orElse(null);;
                if( phone != null){
                    phone.setVerified(true);
                }
                /* If there is Office phone then copy it in addres of type office ( if present ) as landline number
                        and delete this phone number from th elist
                */
                phone = GngUtils.getPhone( GNGWorkflowConstant.OFFICE_PHONE.toFaceValue(), applicant.getPhone()).orElse(null);
                if( phone != null){
                    // Check whether there is office address present
                    CustomerAddress address = GngUtils.getAddressObject(GNGWorkflowConstant.OFFICE.toFaceValue(), applicant.getAddress());
                    if( address != null ){
                        // .oBody.payLoad.oAppReq.oReq.oApplicant.aAddr[1].sLandLine
                        address.setLandLine(phone.getAreaCode()+phone.getPhoneNumber());
                        // Remove the phone number
                        applicant.getPhone().remove(applicant.getPhone().indexOf(phone));
                    }
                }
            }

            // Copy existing emi from Banking details into ProfessionalIncomeDetails
            // NOTE : This is tmporary fix till Paisa Bazar rectifies the request
            if(CollectionUtils.isNotEmpty( applicant.getBankingDetails() ) ){
                String existingEmi = applicant.getBankingDetails().get(0).getExistingEmi();
                if( applicant.getProfessionIncomeDetails() == null ){
                    applicant.setProfessionIncomeDetails(new ProfessionIncomeDetails());
                }
                applicant.getProfessionIncomeDetails().setExistingEmi(existingEmi);
            }
        }
    }

    private Map<String,String> getMessage(BaseResponse baseResponse, String institutionId) throws Exception {
        // Default message - makes code free from NullPointerExcpn and removes null checks
        String message = ThirdPartyIntegrationHelper.AGGREGATOR.get(GNGWorkflowConstant.OTHER.toFaceValue());
        String status = GNGWorkflowConstant.OTHER.toFaceValue();
        String bucket = "";
        MCPAggregatorResponse mcpAggregatorResponse = null;
        String sResult = "";

        Map<String, String> messageWithStatus = new HashMap<>();
        String applicantName = null;
        String refId = "";
        GoNoGoCustomerApplication gngdata = null;
        Name name = null;;

        //Setting response message to PaisaBazar

        Object response = baseResponse.getPayload().getT();
        if( response instanceof GoNoGoCustomerApplication) {

            gngdata = (GoNoGoCustomerApplication) response;
            name = gngdata.getApplicationRequest().getRequest().getApplicant().getApplicantName();
            status = null;
            refId = gngdata.getGngRefId();
            if (gngdata.getApplicationRequest().getRefID() != null && gngdata.getApplicantComponentResponse() != null) {
                String result = null;
                ScoringResponse scoringServiceResponse = gngdata.getApplicantComponentResponse().getScoringServiceResponse();
                if (scoringServiceResponse.getApplicantResponse() != null
                        && scoringServiceResponse.getApplicantResponse().getSummary() != null) {
                    result = scoringServiceResponse.getApplicantResponse().getSummary().getApplicantDecesion();

                    if (StringUtils.equalsIgnoreCase(result, GNGWorkflowConstant.APPROVED.toFaceValue())
                            || StringUtils.equalsIgnoreCase(result, GNGWorkflowConstant.QUEUED.toFaceValue())) {
                        message = ThirdPartyIntegrationHelper.AGGREGATOR.get(GNGWorkflowConstant.APPROVED.toFaceValue());
                        status = GNGWorkflowConstant.APPROVED.toFaceValue();
                    } else if (StringUtils.equalsIgnoreCase(result, GNGWorkflowConstant.DECLINED.toFaceValue())) {
                        message = ThirdPartyIntegrationHelper.AGGREGATOR.get(GNGWorkflowConstant.DECLINED.toFaceValue());
                        status = GNGWorkflowConstant.DECLINED.toFaceValue();
                    } else {
                        message = ThirdPartyIntegrationHelper.AGGREGATOR.get(GNGWorkflowConstant.OTHER.toFaceValue());
                        status = GNGWorkflowConstant.OTHER.toFaceValue();
                    }
                }
            }
            // getting the full name of applicant
            applicantName = GngUtils.convertNameToFullName(name);
        }else if(response instanceof LoginDataResponse){
            LoginDataResponse loginDataResponse = (LoginDataResponse) response;
            gngdata = loginDataResponse.getGoNoGoCustomerApplication();
            if(null != gngdata) {
                boolean isCibilHit = moduleManager.isMBSuccessful(gngdata.getApplicantComponentResponse());
                sResult = isCibilHit ? GNGWorkflowConstant.BUREAU_SUCCESS.toFaceValue() : GNGWorkflowConstant.BUREAU_ERROR.toFaceValue();
                name = gngdata.getApplicationRequest().getRequest().getApplicant().getApplicantName();
                status = gngdata.getApplicationStatus();
                if(StringUtils.isEmpty(status) || StringUtils.equalsIgnoreCase(status,GNGWorkflowConstant.DECLINED.toFaceValue())){
                    message = ThirdPartyIntegrationHelper.MCPAPI.get(GNGWorkflowConstant.OTHER.toFaceValue());
                }else{
                    message = ThirdPartyIntegrationHelper.MCPAPI.get(GNGWorkflowConstant.APPROVED.toFaceValue());
                }
                bucket = gngdata.getApplicationBucket();
                message = message.replaceAll("#BT#", bucket);
                message = message.replaceAll("#CIBRES#", sResult);
                refId = gngdata.getGngRefId();
                mcpAggregatorResponse = new MCPAggregatorResponse();
            }
        }  // No need of else - default message already set

        applicantName = GngUtils.convertNameToFullName(name);
        message = message.replaceAll("########", refId);
        if(applicantName != null){
            message = message.replaceAll("applicantName", applicantName);
        }

        messageWithStatus.put(status, message);
        if(null != mcpAggregatorResponse) {
            mcpAggregatorResponse.setRefId(refId);
            mcpAggregatorResponse.setApplicantName(applicantName);
            mcpAggregatorResponse.setBucket(bucket);
            mcpAggregatorResponse.setCibilHitResult(sResult);
            mcpAggregatorResponse.setMessage(message);
            mcpAggregatorResponse.setApplicationStatus(status);
            baseResponse.setPayload(new Payload<>(mcpAggregatorResponse));
        }

        return messageWithStatus;
    }

    private MCPAggregatorResponse getDeclinedReasons(BaseResponse baseResponse){
        String mssg = ThirdPartyIntegrationHelper.MCPAPI.get(GNGWorkflowConstant.DECLINED.toFaceValue());
        String remarks = "";
        Object response = baseResponse.getPayload().getT();
        if( response instanceof GoNoGoCustomerApplication) {
            GoNoGoCustomerApplication gngdata = (GoNoGoCustomerApplication) response;
            String applicantName = GngUtils.convertNameToFullName(gngdata.getApplicationRequest().getRequest().getApplicant().getApplicantName());
            if(StringUtils.isNotEmpty(applicantName)) mssg = mssg.replaceAll("applicantName",applicantName);

            if(gngdata.getApplicantComponentResponse() != null) {
                ScoringResponse scoringServiceResponse = gngdata.getApplicantComponentResponse().getScoringServiceResponse();
                if (scoringServiceResponse.getApplicantResponse() != null
                        && scoringServiceResponse.getApplicantResponse().getApplicantResult().get(0) != null
                        && CollectionUtils.isNotEmpty(scoringServiceResponse.getApplicantResponse().getApplicantResult().get(0).getRules())) {
                    remarks = scoringServiceResponse.getApplicantResponse().getApplicantResult().get(0).getRules().stream()
                            .map(RulesDetails::getRemark).collect(Collectors.joining(" | "));
                }
            }
        }
        return MCPAggregatorResponse.builder().message(mssg).remarks(remarks).build();
    }

    private BaseResponse buildBaseResponse(Exception e){
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(e.getMessage())
                .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                .level(Error.SEVERITY.MEDIUM.name())
                .build());
        return GngUtils.getBaseResponse(HttpStatus.OK, errors);
    }


    @Override
    public BaseResponse updatePerfiosData(PerfiosDataRequest perfiosDataRequest, HttpServletRequest httpRequest) {
        String ackId = perfiosDataRequest.getAckId();
        Object data = perfiosDataRequest.getPayload();
        logger.info("UpdatePerfiosData started for AckId : " + ackId);
        PerfiosData perfiosData = applicationRepository.fetchPerfiosData(perfiosDataRequest.getAckId());

        if(perfiosData != null){
            logger.info("Updating perfios response for AckId : " + ackId + " ApplicationId : " + perfiosData.getRefId());
            perfiosData.getSaveDataList().forEach(bankdata -> {
                if(StringUtils.equalsIgnoreCase(ackId, bankdata.getAcknowledgeId())){
                    bankdata.setData(data);
                }
            });
            applicationRepository.savePerfiosData(perfiosData);
            logger.info("Update process of perfios data completed");
        }
        logger.info("UpdatePerfiosData ended for AckId : " + ackId);
        return GngUtils.getBaseResponse(HttpStatus.OK);
    }

    @Override
    public BaseResponse getFinbitDetails(FinbitRequest finbitRequest, HttpServletRequest httpServletRequest) throws Exception {
        {
            BaseResponse baseResponse = null;
            ActivityLogs activityLogs = auditHelper.createActivityLog(httpServletRequest,finbitRequest.getHeader());
            activityLogs.setAction(EndPointReferrer.FINBIT_DETAILS);
            activityLogs.setRefId(finbitRequest.getRefId());
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            String responseString = null;
            String institutionId = finbitRequest.getHeader().getInstitutionId();
            String refId = finbitRequest.getRefId();
            Product product = finbitRequest.getHeader().getProduct();
            String urlType = finbitRequest.getType();

            WFJobCommDomain wfJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product.toString(), urlType);
            if (wfJobCommDomain == null) {
                return baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "No Configurations Found against this institutionID and Product");
            }
            try {
                String url = wfJobCommDomain.getBaseUrl();
                UploadBankRequest uploadBankRequest = new UploadBankRequest();

                List<BankStatement> bankStatementsDetails = new ArrayList<>();

                 finbitRequest.getStatements().stream()
                        .map((BankStatement obj) -> {
                            obj.getBankStatementDetails().stream()
                                    .map((Statement objData) -> {
                                        BankStatement bankStatement = new BankStatement();
                                        Statement statement = new Statement();
                                        statement.setContent(objData.getContent());
                                        statement.setFileName(objData.getFileName());
                                        bankStatement.setBank(obj.getBank());
                                        bankStatement.setAccountType(obj.getAccountType());
                                        bankStatement.setStatement(statement);
                                        bankStatement.setFinbitStatus(obj.isFinbitStatus());
                                        bankStatement.setStatus(obj.getStatus());
                                        bankStatement.setPassword(objData.getPassword());
                                        bankStatementsDetails.add(bankStatement);
                                        return statement;
                                    }).collect(Collectors.toList());
                            return obj;
                        }).collect(Collectors.toList());


                uploadBankRequest.setStatements(bankStatementsDetails);
                uploadBankRequest.setCallbackUrl(wfJobCommDomain.getEndpoint());

                ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                apiLog.setRefId(finbitRequest.getRefId());
                apiLog.setInstitutionId(institutionId);
                apiLog.setRequestType(urlType);
                apiLog.setRequestString(JsonUtil.ObjectToString(finbitRequest));
                apiLog.setRequest(finbitRequest);

                Map<String, String> headerMap = new HashMap<>();
                if (CollectionUtils.isNotEmpty(wfJobCommDomain.getKeys())) {
                    for (HeaderKey key : wfJobCommDomain.getKeys()) {
                        headerMap.put(key.getKeyName(), key.getKeyValue());
                    }
                }
                logger.debug("{} request {} for url {}", finbitRequest.getRefId(), JsonUtil.ObjectToString(uploadBankRequest), url);

                responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(uploadBankRequest), headerMap, MediaType.APPLICATION_JSON_VALUE);
                logger.debug("{} response {}", finbitRequest.getRefId(), responseString);

                apiLog.setResponseString(responseString);
                FinbitResponse finbitResponse = JsonUtil.StringToObject(responseString, FinbitResponse.class);
                apiLog.setResponse(finbitResponse);

                FinbitData finbitData = applicationRepository.fetchFinbitData(refId, institutionId);
                if (finbitData != null) {
                    List<BankStatement> bankStatements = finbitData.getStatements();
                    List<BankStatement> bankingStatementList = bankStatements.stream().filter(dbBankData -> (
                            StringUtils.isNotEmpty(dbBankData.getAcknowledgementId()))).collect(Collectors.toList());

                    List<BankStatement> bankStatementData = new ArrayList<>();
                    if (finbitResponse != null ) {
                        for (BankStatement bankStatement : bankStatements) {
                            for (BankStatement bk : finbitRequest.getStatements()) {
                                for (Statement statementDetails : bk.getBankStatementDetails()){
                                    if (StringUtils.equalsIgnoreCase(bankStatement.getStatement().getFileName(), statementDetails.getFileName())) {
                                        if(finbitResponse.getError() != null){
                                            bankStatement.setError(finbitResponse.getError());
                                        }
                                        if(finbitResponse.getAcknowledgementId() != null) {
                                            bankStatement.setAcknowledgementId(finbitResponse.getAcknowledgementId());
                                            bankStatement.setStatus(Constant.PENDING);
                                        }
                                        bankStatementData.add(bankStatement);
                                    }
                            }
                            }
                        }
                        List<BankStatement> bankStatementDataList = bankStatements.stream().filter(dbBankData -> (
                                !StringUtils.isNotEmpty(dbBankData.getAcknowledgementId()))).collect(Collectors.toList());

                        bankStatementData.addAll(bankStatementDataList);
                        bankStatementData.addAll(bankingStatementList);
                        finbitRequest.setStatements(bankStatementData);
                    }
                    saveFinbitDetails(finbitRequest, null);
                    ArrayList<Integer> list = new ArrayList<>();
                    if (finbitResponse != null && CollectionUtils.isNotEmpty(finbitResponse.getErrors())) {
                        List<ErrorDetails> errorDetails = finbitResponse.getErrors();

                        if (errorDetails != null) {
                            for (ErrorDetails errorList : errorDetails) {
                                String filename = errorList.getError();
                                for (BankStatement bankStatement : bankStatements) {
                                    if (StringUtils.equalsIgnoreCase(filename, bankStatement.getStatement().getFileName())) {
                                        bankStatement.setError(errorList.getStatus());
                                        saveFinbitDetails(finbitRequest, null);
                                        int pos = bankStatements.indexOf(bankStatement);
                                        list.add(pos);
                                    }
                                }
                            }
                        }
                    }
                    if (finbitResponse != null && finbitResponse.getPayload() != null) {
                        List<AccountListDetails> accountListDetails = finbitResponse.getPayload().getAccountList();
                        if (accountListDetails != null) {
                            for (AccountListDetails accountList : accountListDetails) {
                                String accId = accountList.getAccountUID();
                                for (BankStatement bankStatement : bankStatements) {
                                    if (StringUtils.equalsIgnoreCase(bankStatement.getStatus(),Constant.PENDING)) {
                                        if (!list.contains(bankStatements.indexOf(bankStatement)) && bankStatement.getAccountUID() == null
                                                && StringUtils.equalsIgnoreCase(finbitResponse.getAcknowledgementId(),bankStatement.getAcknowledgementId())) {
                                            bankStatement.setAccountUID(accId);
                                            saveFinbitDetails(finbitRequest, null);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (finbitResponse != null) {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, finbitResponse);
                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
                    }

                }
            }catch (Exception e) {
                logger.debug("Error while fetching Finbit details {}", ExceptionUtils.getStackTrace(e));
            }
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLogs);
            return baseResponse;
        }
    }

    @Override
    public BaseResponse saveFinbitData(FinbitRequest finbitRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(httpRequest,finbitRequest.getHeader());
        activityLogs.setAction(EndPointReferrer.SAVE_FINBIT_DATA);
        activityLogs.setRefId(finbitRequest.getRefId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = finbitRequest.getRefId();
        if(CollectionUtils.isEmpty(finbitRequest.getSummaryData())){
            finbitRequest.setSummaryData(new ArrayList<Object>());
        }
        try{
            List<BankStatement> bankStatementsDetails = new ArrayList<>();
            logger.debug("In saveFinbitDetails ");

            finbitRequest.getStatements().stream()
                    .map((BankStatement obj) -> {
                        obj.getBankStatementDetails().stream()
                                .map((Statement objData) -> {
                                    BankStatement bankStatement = new BankStatement();
                                    Statement statement = new Statement();
                                    statement.setContent(objData.getContent());
                                    statement.setFileName(objData.getFileName());
                                    statement.setPassword(objData.getPassword());
                                    bankStatement.setBank(obj.getBank());
                                    bankStatement.setMifinBankCode(obj.getMifinBankCode());
                                    bankStatement.setAccountType(obj.getAccountType());
                                    bankStatement.setStatement(statement);
                                    bankStatement.setFinbitStatus(obj.isFinbitStatus());
                                    bankStatement.setStatus(obj.getStatus());
                                    bankStatement.setError(obj.getError());
                                    bankStatement.setPassword(obj.getPassword());
                                    bankStatement.setAccountUID(obj.getAccountUID());
                                    bankStatement.setAcknowledgementId(obj.getAcknowledgementId());
                                    bankStatement.setAppId(obj.getAppId());
                                    bankStatement.setApplicantName(obj.getApplicantName());
                                    bankStatementsDetails.add(bankStatement);
                                    return statement;
                                }).collect(Collectors.toList());
                        return obj;
                    }).collect(Collectors.toList());


            finbitRequest.setStatements(bankStatementsDetails);
            applicationRepository.saveFinbitData(finbitRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Finbit data saved");

        } catch (Exception e) {
            logger.error("{ } refId, Exception occurred while saving finbit data {}. Exception {}",
                    refId  , e.getMessage(), ExceptionUtils.getStackTrace(e));
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    @Override
    public BaseResponse saveFinbitDataTemp(FinbitRequest finbitRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = null;
        String refId = finbitRequest.getRefId();
        if(CollectionUtils.isEmpty(finbitRequest.getSummaryData())){
            finbitRequest.setSummaryData(new ArrayList<Object>());
        }
        try{
            List<BankStatement> bankStatementsDetails = new ArrayList<>();
            logger.debug("In saveFinbitDetails API");
            finbitRequest.getStatements().stream()
                    .map(obj -> {
                        BankStatement bankStatement = new BankStatement();
                        Statement statement;
                        if (null != obj.getStatement()) {
                            statement = obj.getStatement();
                        } else {
                            statement = new Statement();
                        }
                        bankStatement.setBank(obj.getBank());
                        if (obj.getMifinBankCode() != null) {
                            bankStatement.setMifinBankCode(obj.getMifinBankCode());
                        }
                        bankStatement.setAccountType(obj.getAccountType());
                        bankStatement.setStatement(statement);
                        bankStatement.setFinbitStatus(obj.isFinbitStatus());
                        bankStatement.setStatus(obj.getStatus());
                        if (obj.getError() != null) {
                            bankStatement.setError(obj.getError());
                        }
                        bankStatement.setPassword(obj.getPassword());
                        bankStatement.setAccountUID(obj.getAccountUID());
                        bankStatement.setAcknowledgementId(obj.getAcknowledgementId());
                        bankStatement.setAppId(obj.getAppId());
                        bankStatement.setApplicantName(obj.getApplicantName());
                        bankStatementsDetails.add(bankStatement);
                        return statement;
                    })
                    .collect(Collectors.toList());

            finbitRequest.setStatements(bankStatementsDetails);
            applicationRepository.saveFinbitData(finbitRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Finbit data saved");

        } catch (Exception e) {
            logger.error("{} refId, Exception occurred while saving finbit data {}. Exception {}",
                    refId  , e.getMessage(), ExceptionUtils.getStackTrace(e));
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    public BaseResponse saveFinbitDetails(FinbitRequest finbitRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = null;
        String refId = finbitRequest.getRefId();
        if(CollectionUtils.isEmpty(finbitRequest.getSummaryData())){
            finbitRequest.setSummaryData(new ArrayList<Object>());
        }
        try{
            applicationRepository.saveFinbitData(finbitRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Finbit data saved");

        } catch (Exception e) {
            logger.error("{} refId, Exception occurred while saving finbit data {}. Exception {}",
                    refId  , e.getMessage(), ExceptionUtils.getStackTrace(e));
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getFinbitData(FinbitRequest finbitRequest, HttpServletRequest httpRequest){
        BaseResponse baseResponse = null;
        String refId = finbitRequest.getRefId();
        String institutionId = finbitRequest.getHeader().getInstitutionId();

        try {
            FinbitData finbitData = applicationRepository.fetchFinbitData(refId,institutionId);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, finbitData);
        }catch (Exception e){
            logger.error("{} refId, Exception occurred while fetching finbit data {}. Exception {}",
                    refId  , e.getMessage(), ExceptionUtils.getStackTrace(e));
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse finbitCallback(FinbitCallbackRequest finbitCallbackRequest, HttpServletRequest httpServletRequest)
    {
        BaseResponse baseResponse = null;
        FinbitData finbitData = applicationRepository.fetchFinbitData(finbitCallbackRequest.getAcknowledgementId());
        if(finbitData!= null) {
            for (BankStatement bankStatementData : finbitData.getStatements()){
                if (StringUtils.equalsIgnoreCase(bankStatementData.getAcknowledgementId(), finbitCallbackRequest.getAcknowledgementId())) {
                    String refId = finbitData.getRefId();
                    String institutionId = finbitData.getInstitutionId();
                    FinbitRequest finbitRequest = new FinbitRequest();
                    finbitRequest.setRefId(refId);
                    List<BankStatement> bankStatements = finbitData.getStatements();
                    Header header = new Header();
                    header.setInstitutionId(institutionId);
                    finbitRequest.setHeader(header);
                    ArrayList<Integer> list = new ArrayList<>();

                    logger.debug("{} Finbit Callback request {}", finbitRequest.getRefId(), finbitCallbackRequest);
                    if (finbitCallbackRequest != null && CollectionUtils.isNotEmpty(finbitCallbackRequest.getErrors())) {
                        List<ErrorDetails> errorDetails = finbitCallbackRequest.getErrors();

                        if (errorDetails != null) {
                            for (ErrorDetails errorList : errorDetails) {
                                String filename = errorList.getError();
                                for (BankStatement bankStatement : bankStatements) {
                                    if (filename.equalsIgnoreCase(bankStatement.getStatement().getFileName())) {
                                        bankStatement.setStatus(errorList.getStatus());
                                        finbitRequest.setStatements(bankStatements);
                                        saveFinbitDataTemp(finbitRequest, null);
                                        int pos = bankStatements.indexOf(bankStatement);
                                        list.add(pos);
                                    }
                                }
                            }
                        }
                    }
                    if (finbitCallbackRequest != null && finbitCallbackRequest.getPayload() != null){
                        List<AccountListDetails> accountListDetails = finbitCallbackRequest.getPayload().getAccountList();
                        if (CollectionUtils.isNotEmpty(accountListDetails)) {
                            for (AccountListDetails accountList : accountListDetails) {
                                String accId = accountList.getAccountUID();
                                for (BankStatement bankStatement : bankStatements) {
                                    if (!list.contains(bankStatements.indexOf(bankStatement)) && StringUtils.isEmpty(bankStatement.getAccountUID())) {
                                        bankStatement.setAccountUID(accId);
                                        bankStatement.setStatus(Constant.PENDING);
                                        finbitRequest.setStatements(bankStatements);
                                        saveFinbitDataTemp(finbitRequest, null);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
                    break;
                }else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_FOUND);
                }
            }
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_FOUND);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getFinbitMonthlySummary(FinbitRequest finbitRequest, HttpServletRequest httpServletRequest) {
        BaseResponse baseResponse = null;
        String responseString = null;
        String institutionId = finbitRequest.getHeader().getInstitutionId();
        Product product = finbitRequest.getHeader().getProduct();
        String urlType = finbitRequest.getType();
        String refId = finbitRequest.getRefId();
        String ackId = finbitRequest.getAcknowledgementId();
        String accountUid = finbitRequest.getAccountUID();

        WFJobCommDomain wfJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product.toString(), urlType);
        if (wfJobCommDomain == null) {
            return baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "No Configurations Found against this institutionID and Product");
        }
        try {
            String url = Arrays.asList(wfJobCommDomain.getBaseUrl(), ackId, finbitRequest.getAccountUID())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            url =  Arrays.asList(url, "date=2,8,15,22,27")
                    .parallelStream().collect(Collectors.joining(FieldSeparator.QUESTION_MARK));
            ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
            apiLog.setRefId(refId);
            apiLog.setInstitutionId(institutionId);
            apiLog.setRequestType(urlType);
            apiLog.setRequestString(JsonUtil.ObjectToString(finbitRequest));
            apiLog.setRequest(finbitRequest);

            Map<String, String> headerMap = new HashMap<>();
            if (CollectionUtils.isNotEmpty(wfJobCommDomain.getKeys())) {
                for (HeaderKey key : wfJobCommDomain.getKeys()) {
                    headerMap.put(key.getKeyName(), key.getKeyValue());
                }
            }

            logger.debug("{}  url {}", finbitRequest.getRefId(), url);

            responseString = new HttpTransportationService().getRequest(url, headerMap);
            logger.debug("{} response {}", finbitRequest.getRefId(), responseString);
            apiLog.setResponseString(responseString);

            MonthlyResponse monthlyResponse = JsonUtil.StringToObject(responseString, MonthlyResponse.class);
            apiLog.setResponse(monthlyResponse);

            CamDetails camDetails = applicationRepository.fetchCamDetails(refId, institutionId, "banking-details");
            FinbitData finbitData = applicationRepository.fetchFinbitData(refId,institutionId);

            SummaryDetails summaryDetails = monthlyResponse.getPayload().getSummary();
            MonthlyTotalData totalNetCredit = summaryDetails.getTotalNetCredit();
            MonthlyTotalData totalNetDebit = summaryDetails.getTotalNetDedit();
            MonthlyTotalData monthlyAvgBal = summaryDetails.getMonthlyAvgBal();
            MonthlyTotalData debits = summaryDetails.getDebits();
            MonthlyTotalData credits = summaryDetails.getCredits();
            MonthlyTotalData configurableMonthlyAvgBal = summaryDetails.getConfigurableMonthlyAvgBal();
            MonthlyTotalData inwBounce = summaryDetails.getInwBounce();
            MonthlyTotalData outwBounce = summaryDetails.getOutwBounce();
            MonthlyTotalData cashDeposit = summaryDetails.getCashDeposit();
            MonthlyTotalData interestReceived = summaryDetails.getInterestReceived();
            MonthlyTotalData interestPaid = summaryDetails.getInterestPaid();
            MonthlyTotalData chqDeposit = summaryDetails.getChqDeposit();
            MonthlyTotalData peakUtilization = summaryDetails.getPeakUtilization();

            List<BankingStatement> bankingStatements = new ArrayList<>();

            if (camDetails != null && CollectionUtils.isNotEmpty(camDetails.getBankingStatements())) {
                List<BankingStatement> bankingStatementList = camDetails.getBankingStatements();
                bankingStatements.addAll(bankingStatementList);
            }
            for (BankStatement bankStatement1 : finbitData.getStatements()) {

                BankingStatement bankStatement = new BankingStatement();
                double inwardTotal = 0;
                double outwardTotal = 0;

                if (StringUtils.equalsIgnoreCase(bankStatement1.getAcknowledgementId(),ackId) &&
                        StringUtils.equalsIgnoreCase(bankStatement1.getAccountUID(), accountUid) && bankStatement1.isFinbitStatus()){
                    bankStatement.setApplicantId(bankStatement1.getAppId());
                    if(bankStatement1.getApplicantName().getFirstName() != null) {
                        if (bankStatement1.getApplicantName().getLastName() != null) {
                            bankStatement.setAccountHolder((bankStatement1.getApplicantName().getFirstName().concat(bankStatement1.getApplicantName().getLastName())));
                        } else {
                            bankStatement.setAccountHolder((bankStatement1.getApplicantName().getFirstName()));
                        }
                    }
                    if(StringUtils.equalsIgnoreCase(bankStatement1.getAccountType(),Constant.CURRENT)){
                          bankStatement.setAccountType(Constant.CURRENT);
                    }else{
                        bankStatement.setAccountType(Constant.SAVING);
                    }
                    bankStatement.setBankName(bankStatement1.getBank());
                    bankStatement.setNetBusinessCreditTransctionsAmt(totalNetCredit.getTotalValue());
                    if (totalNetDebit != null) {
                        bankStatement.setNetBusinessDebitTransctionsAmt(totalNetDebit.getTotalValue());
                    }
                    bankStatement.setAverageAbb(monthlyAvgBal.getAverage());
                    bankStatement.setAverageCreditSum(totalNetCredit.getAverage());
                    bankStatement.setTotalNoOfInwardTransaction(((int) debits.getTotal()));
                    bankStatement.setTotalNoOfOutwardTransaction(((int) credits.getTotal()));
                    bankStatement.setPercentOfInwardReturns(inwBounce.getTotal());
                    bankStatement.setPercentOfOutwardReturns(outwBounce.getTotal());
                    bankStatement.setTotalCashCredit(cashDeposit.getTotalValue());
                    bankStatement.setTotalCredit(credits.getTotalValue());
                    bankStatement.setTotalIntColl(interestReceived.getTotalValue());
                    bankStatement.setTotalOtherCredit(chqDeposit.getTotalValue());
                    bankStatement.setTotalCCUtilisation(peakUtilization.getTotalValue());

                    List<BankingTransaction> bankingTransactions = new ArrayList<>();

                    for (MonthWiseData configurablemonthWiseData:configurableMonthlyAvgBal.getMonthlyDetails()) {
                        BankingTransaction  bankingTransaction = new BankingTransaction();

                        if (CollectionUtils.isNotEmpty(configurableMonthlyAvgBal.getMonthlyDetails())) {
                            Object dayBalanceMapDetails = configurablemonthWiseData.getDayBalanceMap();
                            HashMap<String, Double> dayBalanceDetails = (HashMap<String, Double>) dayBalanceMapDetails;

                            if (dayBalanceDetails != null) {
                                List<AmountForDate> inMonthBalance = dayBalanceDetails.keySet().stream().map(key -> {
                                    AmountForDate amountForDate = new AmountForDate();
                                    amountForDate.setMonthDate(Integer.valueOf(key));
                                    amountForDate.setBalanceAmt(dayBalanceDetails.get(key));
                                    return amountForDate;
                                }).collect(Collectors.toList());

                                bankingTransaction.setInMonthBalance(inMonthBalance);
                                String date = configurablemonthWiseData.getMonthAndYear();
                                date = date.replace(' ', '-');
                                bankingTransaction.setAbbAmount(configurablemonthWiseData.getNetAverageBalance());
                                bankingTransaction.setDate(date);
                            }
                        }
                        if (CollectionUtils.isNotEmpty(credits.getMonthlyDetails())) {
                            credits.getMonthlyDetails().forEach(monthWiseData -> {
                                if (monthWiseData.getMonthYear().equalsIgnoreCase(configurablemonthWiseData.getMonthAndYear())) {
                                    List<Double> businessCredit = new ArrayList<>();
                                    businessCredit.add(monthWiseData.getValue());
                                    bankingTransaction.setBusinessCredit(businessCredit);
                                    bankingTransaction.setCreditTotal(monthWiseData.getValue());
                                }
                            });
                        }
                        if (CollectionUtils.isNotEmpty(debits.getMonthlyDetails())) {
                            debits.getMonthlyDetails().forEach(monthWiseData ->{
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())){
                                    List<Double> businessDebit = new ArrayList<>();
                                    businessDebit.add(monthWiseData.getValue());
                                    bankingTransaction.setBusinessDebit(businessDebit);
                                }
                            });
                        }

                        if (CollectionUtils.isNotEmpty(interestPaid.getMonthlyDetails())) {
                            interestPaid.getMonthlyDetails().forEach(monthWiseData ->{
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())) {
                                    bankingTransaction.setInterestDebited(monthWiseData.getValue());
                                }
                            });
                        }

                        if (CollectionUtils.isNotEmpty(inwBounce.getMonthlyDetails())) {
                            inwBounce.getMonthlyDetails().forEach(monthWiseData ->{
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())) {
                                    bankingTransaction.setChqAndEmiReturnsInward(monthWiseData.getValue());
                                    bankingTransaction.setNumberOfIW(monthWiseData.getTotal());
                                }
                            });
                        }

                        if (CollectionUtils.isNotEmpty(inwBounce.getMonthlyDetails())) {
                            inwardTotal = inwBounce.getMonthlyDetails().stream()
                                    .collect(Collectors.summingDouble( monthWiseData -> monthWiseData.getTotal()));
                        }

                        if (CollectionUtils.isNotEmpty(outwBounce.getMonthlyDetails())) {
                            outwBounce.getMonthlyDetails().forEach(monthWiseData -> {
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())){
                                    bankingTransaction.setChqAndEmiReturnsOutward(monthWiseData.getValue());
                                    bankingTransaction.setNumberOfOW(monthWiseData.getTotal());
                                }
                            });
                        }

                        if (CollectionUtils.isNotEmpty(outwBounce.getMonthlyDetails())) {
                            outwardTotal = outwBounce.getMonthlyDetails().stream()
                                    .collect(Collectors.summingDouble(monthWiseData -> monthWiseData.getTotal()));
                        }

                        if (CollectionUtils.isNotEmpty(interestReceived.getMonthlyDetails())) {
                            interestReceived.getMonthlyDetails().forEach(monthWiseData -> {
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())) {
                                    bankingTransaction.setIntColl(monthWiseData.getValue());
                                }
                            });
                        }

                        if (CollectionUtils.isNotEmpty(cashDeposit.getMonthlyDetails())) {
                            cashDeposit.getMonthlyDetails().forEach(monthWiseData -> {
                                if(StringUtils.equalsIgnoreCase(monthWiseData.getMonthYear(), configurablemonthWiseData.getMonthAndYear())){
                                    bankingTransaction.setCashCredit(monthWiseData.getValue());
                                }
                            });
                        }
                        bankingTransactions.add(bankingTransaction);
                    }
                    bankStatement.setTotalIW(inwardTotal);
                    bankStatement.setTotalOW(outwardTotal);
                    if((int) debits.getTotal()>0) {
                        bankStatement.setPercentOfInwardReturns((inwardTotal / (int) debits.getTotal())*100);
                    }
                    if((int) credits.getTotal()>0) {
                        bankStatement.setPercentOfOutwardReturns((outwardTotal / (int) credits.getTotal())*100);
                    }

                    bankStatement.setBankingTransactions(bankingTransactions);

                    bankingStatements.add(bankStatement);

                    bankStatement1.setFinbitStatus(false);
                }
                if(bankStatement1.getAccountUID() != null) {
                    bankStatement1.setStatus("Completed");
                }
                FinbitRequest finbitRequest1 = new FinbitRequest();
                finbitRequest1.setStatements(finbitData.getStatements());
                finbitRequest1.setRefId(refId);
                Header header = new Header();
                header.setInstitutionId(finbitRequest.getHeader().getInstitutionId());
                finbitRequest1.setHeader(header);
                saveFinbitDetails(finbitRequest1, httpServletRequest);
            }
            CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
            Header header = new Header();
            CamDetails camDetails1 = new CamDetails();
            Collections.sort(bankingStatements,AppIdComparator);
            camDetails1.setBankingStatements(bankingStatements);
            camDetails1.setSbfc(true);
            camDetailsRequest.setCamDetails(camDetails1);
            camDetailsRequest.setRefId(finbitRequest.getRefId());
            header.setInstitutionId(finbitRequest.getHeader().getInstitutionId());
            camDetailsRequest.setHeader(header);
            applicationRepository.saveCamDetails(camDetailsRequest, "banking-details");


            if (monthlyResponse != null) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, monthlyResponse);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            logger.debug("Error while fetching Finbit Monthly details {}", ExceptionUtils.getStackTrace(e));
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCoorginationDetails(CoorginationAPIRequest coorginationAPIRequest, HttpServletRequest httpServletRequest) throws Exception {
        {
            BaseResponse baseResponse = null;
            String institutionId = coorginationAPIRequest.getHeader().getInstitutionId();
            Product product = coorginationAPIRequest.getHeader().getProduct();
            String urlType = StringUtils.isEmpty(coorginationAPIRequest.getType()) ? UrlType.ICICI_CORPORATE_API.toValue(): coorginationAPIRequest.getType();

            WFJobCommDomain wfJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product.toString(), urlType);
            if (wfJobCommDomain == null) {
                return GngUtils.getBaseResponse(HttpStatus.OK, "No Configurations Found against this institutionID and Product for type "+ urlType);
            }
            try {
                GoNoGoCustomerApplication goNoGoCustomerApplication  = applicationRepository.getGoNoGoCustomerApplicationByRefId(coorginationAPIRequest.getRefId());
                CorporateAPIRequest corporateAPIRequest = originationHelper.getCoorginationDetails(goNoGoCustomerApplication);
                coorginationAPIRequest.setCorporateAPIRequest(corporateAPIRequest);

                ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                apiLog.setRefId(coorginationAPIRequest.getRefId());
                apiLog.setInstitutionId(institutionId);
                apiLog.setRequestType(urlType);
                apiLog.setRequestString(JsonUtil.ObjectToString(coorginationAPIRequest));
                apiLog.setRequest(coorginationAPIRequest);

                Map<String, String> headerMap = new HashMap<>();
                if (CollectionUtils.isNotEmpty(wfJobCommDomain.getKeys())) {
                    for (HeaderKey key : wfJobCommDomain.getKeys()) {
                        headerMap.put(key.getKeyName(), key.getKeyValue());
                    }
                }
                String url = wfJobCommDomain.getBaseUrl();
                DMZRequest connectorRequest = new DMZRequest();
                connectorRequest.setRequestType(urlType);
                connectorRequest.setRequestURL(headerMap.get("URL"));
                connectorRequest.setRequestString(JsonUtil.ObjectToString(corporateAPIRequest));
                logger.debug("{} CorporateAPIrequest {} for url {}", coorginationAPIRequest.getRefId(), JsonUtil.ObjectToString(corporateAPIRequest), url);
                String responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);

                logger.debug("{} response {}", coorginationAPIRequest.getRefId(), responseString);
                DMZResponse dmzResponse;
                CorporateAPIResponse response = null;
                try {
                    dmzResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
                    try {
                        response = JsonUtil.StringToObject(dmzResponse.getResponseString(), CorporateAPIResponse.class);
                        apiLog.setResponse(response);
                        apiLog.setResponseString(JsonUtil.ObjectToString(response));
                    } catch (Exception e) {
                        logger.error("Error occured while Converting response String {}", ExceptionUtils.getStackTrace(e));
                        response = null;
                        apiLog.setResponse(dmzResponse);
                        apiLog.setResponseString(JsonUtil.ObjectToString(dmzResponse));
                    }
                }catch(Exception e){
                    dmzResponse = null;
                    apiLog.setResponseString(responseString);
                }
                externalAPILogRepository.saveIciciCallLog(apiLog);

                if (response != null) {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, response);
                } else if(dmzResponse !=null ){
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
                }else{
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
                }

            } catch (Exception e) {
                logger.debug("Error while fetching co-origination details {}", ExceptionUtils.getStackTrace(e));
            }
            return baseResponse;
        }
    }

    @Override
    public BaseResponse saveLoanChargesAndRepaymentDetails(LoanChargesRepaymentRequest loanChargesRepaymentRequest, HttpServletRequest httpRequest){
        BaseResponse baseResponse = null;
        String refId = loanChargesRepaymentRequest.getRefId();
        Header header = loanChargesRepaymentRequest.getHeader();
        LoanCharges loanCharges = loanChargesRepaymentRequest.getLoanCharges();
        Repayment repayment = loanChargesRepaymentRequest.getRepayment();
        try {
            String instituteId = header.getInstitutionId();
            if (StringUtils.isNotEmpty(instituteId)) {
                GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                if (null != goNoGoCustomerApplication) {
                    loanCharges.setBrokenPeriodDays(thirdPartyIntegrationHelper.getBrokenPeriodDays(loanCharges.getDisbursedDate()));
                    goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setLoanCharges(loanCharges);
                    baseResponse = thirdPartyIntegrationHelper.generateLoanChargesRepaymentRequestAndCallSobre(instituteId, goNoGoCustomerApplication);
                    Object response = baseResponse.getPayload().getT();
                    if( response instanceof LoanCharges) {
                        LoanChargesRepaymentResponse loanChargesRepaymentResponse = new LoanChargesRepaymentResponse();
                        loanCharges = (LoanCharges)response;
                        LoanChargesRequest loanChargesRequest = new LoanChargesRequest();
                        loanChargesRequest.setRefId(refId);
                        loanChargesRequest.setHeader(header);
                        loanChargesRequest.setLoanCharges(loanCharges);

                        logger.debug("saveLoanChargesAndRepaymentDetails: Saving LoanCharges Details for RefId: {}", refId);
                        BaseResponse loanChargesResponse = dataEntryManager.saveLoanChargesDetails(loanChargesRequest, null);
                        Object oLoanChargesResponse = loanChargesResponse.getPayload().getT();
                        if(oLoanChargesResponse instanceof LoanCharges){
                            LoanCharges savedLoanCharges = (LoanCharges)oLoanChargesResponse;
                            loanChargesRepaymentResponse.setRefId(refId);
                            loanChargesRepaymentResponse.setLoanCharges(savedLoanCharges);

                            if(CollectionUtils.isNotEmpty(savedLoanCharges.getLoanChargesList()) && savedLoanCharges.getLoanChargesList().get(0).getInterestDetails() != null){
                                try {
                                    CamDetails camDetails = applicationRepository.fetchCamDetails(goNoGoCustomerApplication.getGngRefId(), instituteId, EndPointReferrer.CAM_SUMMARY);
                                    String applicationBucket = goNoGoCustomerApplication.getApplicationBucket();
                                    if (camDetails != null && camDetails.getSummary() != null) {
                                        camDetails.getSummary().setPfPercentage(String.valueOf(savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getPfPercentage()));
                                        camDetails.getSummary().setRoi(savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getDisbInterestRate());
                                        camDetails.getSummary().setFoir((savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getFoir()) * 100);
                                        camDetails.getSummary().setFinalFoir((savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getFoir()) * 100);
                                        if(StringUtils.equalsIgnoreCase(applicationBucket, Buckets.Bucket.B1.name())) {
                                            camDetails.getSummary().setApprovalDate(savedLoanCharges.getLoanChargesList().get(0).getDisbursedDate());
                                        }
                                        logger.debug("savingCamDetails: Saving cam Details for RefId: {}", refId);
                                        applicationRepository.saveCamDetails(refId, instituteId, camDetails, EndPointReferrer.CAM_SUMMARY);
                                    }
                                }catch(Exception e){
                                    logger.error("Exception occurred in saveCamDetails method for refId {}, Exception is {}", refId, e.getStackTrace());
                                }
                                try {
                                    EligibilityDetails eligibilityDetails = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getDigiPLEligibility();
                                    if (null != eligibilityDetails) {
                                        eligibilityDetails.setEligibilityRoi(savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getDisbInterestRate());
                                        eligibilityDetails.setPolicyFoir((savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getFoir()) * 100);
                                        eligibilityDetails.setPF(String.valueOf(savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getPfPercentage()));
                                        eligibilityDetails.setFinalFoir((savedLoanCharges.getLoanChargesList().get(0).getInterestDetails().getFinalFoir()));

                                        EligibilityRequest eligibilityRequest = EligibilityRequest.builder()
                                                .eligibilityDetails(eligibilityDetails)
                                                .header(header)
                                                .refId(refId)
                                                .build();

                                        logger.debug("savingEligibilityDetails: Saving eligibility Details for RefId: {}", refId);
                                        dataEntryManager.saveDigiPLEligibilityDetails(eligibilityRequest, null);
                                    }
                                }catch (Exception e){
                                    logger.error("Exception occurred in saveEligibilityDetails method for refId {}, Exception is {}", refId, e.getStackTrace());
                                }
                                try {
                                    if(!loanChargesRepaymentRequest.getLoanCharges().isInsuranceRequired()) {
                                        InsuranceRequest insuranceRequest = new InsuranceRequest();
                                        List<Applicant> applicantList = new ArrayList<Applicant>();
                                        List<InsurancePolicy> InsurancePolicyList = new ArrayList<>();
                                        InsuranceData insuranceData = new InsuranceData();
                                        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
                                        InsurancePolicy insurancePolicy = new InsurancePolicy();
                                        insurancePolicy.setCompany(savedLoanCharges.getLoanChargesList().get(0).getInsurenceCatList().get(0).getCompanyName());
                                        insurancePolicy.setName("CO_PAY");
                                        insurancePolicy.setProduct("OPTION1");
                                        insurancePolicy.setScheme("CRITICAL ILLNESS");
                                        insurancePolicy.setTenorRequested(String.valueOf(loanChargesRepaymentRequest.getLoanCharges().getTenureDetails().getTenureMonths()));
                                        insurancePolicy.setPremium(savedLoanCharges.getLoanChargesList().get(0).getInsurenceCatList().get(0).getAmount());
                                        insurancePolicy.setProposedAmount(String.valueOf(savedLoanCharges.getLoanChargesList().get(0).getLoanDetails().getApprovedLoanAmt()));
                                        InsurancePolicyList.add(insurancePolicy);
                                        applicant.setInsurancePolicyList(InsurancePolicyList);
                                        applicantList.add(applicant);

                                        insuranceRequest.setHeader(header);
                                        insuranceRequest.setRefId(refId);
                                        insuranceData.setApplicantList((applicantList));
                                        insuranceRequest.setInsuranceData(insuranceData);
                                        logger.debug("savingInsuranceDetails: Saving insurance Details for RefId: {}", refId);
                                        applicationRepository.saveInsuranceData(insuranceRequest);
                                    }
                                }catch (Exception e){
                                    logger.error("Exception occurred in saveInsuranceDetails method for refId {}, Exception is {}", refId, e.getStackTrace());
                                }
                            }
                        }
                        if(null != repayment && CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())) {
                            RepaymentRequest repaymentRequest = new RepaymentRequest();
                            repaymentRequest.setRefId(refId);
                            repaymentRequest.setHeader(header);
                            repaymentRequest.setRepayment(repayment);
                            logger.debug("saveLoanChargesAndRepaymentDetails: Saving Repayment Details for RefId: {}", refId);
                            BaseResponse repaymentResponse = dataEntryManager.saveRepaymentDetails(repaymentRequest, null);
                            Object oRepaymentResponse = repaymentResponse.getPayload().getT();
                            if (oRepaymentResponse instanceof Repayment) {
                                Repayment savedRepayment = (Repayment) oRepaymentResponse;
                                loanChargesRepaymentResponse.setRefId(refId);
                                loanChargesRepaymentResponse.setRepayment(savedRepayment);
                            }
                        }

                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loanChargesRepaymentResponse);

                        if(null != loanChargesRepaymentResponse.getRepayment())
                            dataEntryManager.updateDM(goNoGoCustomerApplication.getApplicationRequest(), loanChargesRepaymentResponse.getRepayment());
                    }
                } else
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,"No GoNoGoCustomer Found !");
            } else
                baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, "Institution id not provided !");
        }catch(Exception e){
            logger.error("Exception occurred in saveLoanChargesAndRepaymentDetails method for refId {}, Exception is {}", refId, e.getStackTrace());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse saveMcpData(
            LoginDataRequest loginDataRequest, String stepId, HttpServletRequest httpRequest) throws Exception {
        BaseResponse baseResponse = null;
        Collection<Error> errors = null;
        getAuthenticationDetails(httpRequest, loginDataRequest);

        logger.debug("Request received from {} is \n {}\n==============", EndPointReferrer.MCP_SAVE_APPLICATION_DATA, loginDataRequest);

        String msg = "Request for " + stepId + " not handled.";
        Map<String, String> message = null;
        httpRequest.getAuthType();
        HttpStatus httpStatus = null;
        String instituteId = loginDataRequest.getHeader().getInstitutionId();
        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(loginDataRequest.getHeader(), loginDataRequest.getRefId(), httpRequest,
                null, stepId, null, loginDataRequest.getHeader().getLoggedInUserId());
        if (loginDataRequest.getRefId() != null) {
            activityLogs.setRefId(loginDataRequest.getRefId());
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            switch (stepId) {
                case EndPointReferrer.MCP_SAVE:
                    activityLogs.setStage(GNGWorkflowConstant.DE.name());
                    activityLogs.setStatus(Status.NEW.toString());
                    GoNoGoCustomerApplication goNoGoCustomerApplication=null;
                    ApplicationRequest applicationRequest = null;
                    errors = dataEntryManager.validateLoginDataRequest(loginDataRequest, "saveMcpData");
                    if(errors.isEmpty()) {
                        // Get user details from UAMThirdPartyIntegrationHelper
                        LoginBaseResponse loginBaseResponse = thirdPartyIntegrationHelper.setLoginDetails(loginDataRequest, loginDataRequest.getAggregatorData().getLoginRole(), loginDataRequest.getAggregatorData().getPassword());
                    /*Validating authentication success*/
                        if (loginBaseResponse != null && loginBaseResponse.getBody() != null) {
                            loginDataRequest.setStepName(EndPointReferrer.MCP_SAVE);
                            applicationRequest = dataEntryManager.mappingApplicationRequest(loginDataRequest);
                            goNoGoCustomerApplication = RequestAggregator.getGoNoGo(applicationRequest);
                            baseResponse = thirdPartyIntegrationHelper.generateRequestAndCallSobre(goNoGoCustomerApplication, "MCP_POLICY", true);
                            if(thirdPartyIntegrationHelper.decisionToSaveLoginData(baseResponse)) {
                                baseResponse = dataEntryManager.saveLoginData(loginDataRequest, httpRequest);
                                setStage(ConfigurationConstants.NODE_THIRD_PARTY_SAVE, ConfigurationConstants.WF_NODE_TYPE_SCREEN,
                                        ConfigurationConstants.WF_EVENT_SAVE, baseResponse);

                                // Set refId for activity log
                                activityLogs.setRefId(loginDataRequest.getRefId());
                                message = getMessage(baseResponse, instituteId);
                            }else{
                                baseResponse.setPayload(new Payload<>(getDeclinedReasons(baseResponse)));
                            }
                        } else {
                            message = new HashMap<>();
                            message.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(), ThirdPartyIntegrationHelper.MCPAPI.get(GNGWorkflowConstant.AUTH_ERROR.toFaceValue()));
                        }
                    }else{
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                    }
                    break;
                default:
                    break;
            }
        } catch(Exception e) {
            logger.error("Exception occurred while call from {} : {}" , stepId, ExceptionUtils.getStackTrace(e));
        }
        stopWatch.stop();
        //activityLogs.setStatus(applicationRequest.getApplicationStatus());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        if( message != null && !message.isEmpty()){
            for (Map.Entry<String,String> entry : message.entrySet()){
                if(StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.QUEUED.toFaceValue()) ||
                        StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.DECLINED.toFaceValue())
                        || StringUtils.equalsIgnoreCase(entry.getKey(),GNGWorkflowConstant.APPROVED.toFaceValue())){
                    httpStatus = HttpStatus.OK;
                }else{
                    httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                }
                msg = entry.getValue();
            }
        }
        if(!errors.isEmpty() ) return baseResponse;
        else if(null != baseResponse && baseResponse.getPayload().getT() instanceof MCPAggregatorResponse) return baseResponse;
        else return GngUtils.getBaseResponse(httpStatus, msg);
    }

    private void getAuthenticationDetails(HttpServletRequest httpRequest,  LoginDataRequest loginDataRequest ) {
        AggregatorData aggregatorData = null;
        if(loginDataRequest.getAggregatorData() == null){
            aggregatorData = new AggregatorData();
        }else{
            aggregatorData = loginDataRequest.getAggregatorData();
        }
        aggregatorData.setUri(httpRequest.getRequestURI());
        aggregatorData.setContextPath(httpRequest.getContextPath());
        aggregatorData.setRemoteAddress(httpRequest.getRequestURI());
        aggregatorData.setPathInfo( httpRequest.getPathInfo());
        aggregatorData.setRemoteUser(httpRequest.getRemoteUser());
        loginDataRequest.setAggregatorData(aggregatorData);
    }

    @Override
    public BaseResponse getScoringCallLog(ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = null;
        String base64String;
        String refId = thirdPartyRequest.getRefId();
        String instituteId = thirdPartyRequest.getHeader().getInstitutionId();
        String ackId = thirdPartyRequest.getAcknowledgementId();
        try {
            base64String = getScoringSavedRequestResponse(refId,instituteId,ackId);
            Document doc = new Document();
            if (StringUtils.isNotEmpty(base64String)) {
                doc.setByteCode(new String(base64String));
                doc.setStatus(Status.SUCCESS.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }

        } catch (Exception e) {
            logger.error("{}, Exception while creating scoring request base64 string in managerImpl ; stacktrace : {}", refId, e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        return baseResponse;
    }

    private String getScoringSavedRequestResponse(String refId, String instituteId, String ackId) {

        String base64String = null, response = null;
        ScoringCallLog thirdPartyApiLog = null;
        List<String> includeFields = new ArrayList<>();
        includeFields.add("refId");
        includeFields.add("step");
        includeFields.add("institutionId");
        includeFields.add("callDate");
        includeFields.add("requestString");
        includeFields.add("responseString");

        List<ScoringCallLog> thirdPartyApiLogList = externalAPILogRepository.fetchScoringCallLogList(instituteId, refId, EndPointReferrer.GET_SCORING_LOG, includeFields);
        if (CollectionUtils.isNotEmpty(thirdPartyApiLogList)) {

            if (StringUtils.isNotEmpty(ackId)) {
                //Fetch by index
                int index = Integer.parseInt(ackId) - 1;
                thirdPartyApiLog = thirdPartyApiLogList.get(index);
            } else {
                //Fetch latest
                thirdPartyApiLog = thirdPartyApiLogList.get(0);
            }
        }

        if (thirdPartyApiLog != null) {
            String request = thirdPartyApiLog.getRequestString();
            String merged = request;
            if (StringUtils.isNotEmpty(thirdPartyApiLog.getResponseString())) {

                request = request.substring(0, request.length() - 1);
                response = thirdPartyApiLog.getResponseString();
                response = response.substring(1);
                merged = request + "," + response;
            }

            if (StringUtils.isNotEmpty(merged)) {

                byte[] bytes = merged.getBytes();
                base64String = new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytes));
            }
        }
        return base64String;
    }

    @Override
    public BaseResponse getScoringCallDetails(com.softcell.gonogo.model.core.request.ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest) {

        BaseResponse baseResponse = null;
        String refId = thirdPartyRequest.getRefId();
        String instituteId = thirdPartyRequest.getHeader().getInstitutionId();
        List<String> includeFields = new ArrayList<>();
        includeFields.add("refId");
        includeFields.add("step");
        includeFields.add("institutionId");
        includeFields.add("callDate");

        List<ScoringCallLog> thirdPartyApiLogList = externalAPILogRepository.fetchScoringCallLogList(instituteId, refId, EndPointReferrer.GET_SCORING_LOG, includeFields);
        if(null!=thirdPartyApiLogList)
            logger.debug("method getScoringCallDetails(): {} records found for refId {}", thirdPartyApiLogList.size(), thirdPartyRequest.getRefId());

        if (CollectionUtils.isNotEmpty(thirdPartyApiLogList)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, thirdPartyApiLogList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse generatePerfiosFile(GenerateFileRequest generateFileRequest, HttpServletRequest httpRequest){
        BaseResponse baseResponse = null;
        String institutionId = generateFileRequest.getHeader().getInstitutionId();
        String product = generateFileRequest.getHeader().getProduct().name();
        String urlType = UrlType.PERFIOS_GENERATE_FILE.toValue();
        GenerateFileResponse generateFileResponse = null;

        WFJobCommDomain wfJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product, urlType);

        if(wfJobCommDomain == null){
            return  GngUtils.getBaseResponse(HttpStatus.OK, "No Configuration Found against this institutionID and Product");
        }
        try {
            String url = wfJobCommDomain.getBaseUrl();

            ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
            apiLog.setRefId(generateFileRequest.getRefId());
            apiLog.setInstitutionId(institutionId);
            apiLog.setRequestType(urlType);
            apiLog.setRequestString(JsonUtil.ObjectToString(generateFileRequest));
            apiLog.setRequest(generateFileRequest);

            HashMap<String, String> headerParams = ThirdPartyExecutor.setSSLHeader(wfJobCommDomain, generateFileRequest.getRefId());
            logger.debug("{} request {} for url {}", generateFileRequest.getRefId(), JsonUtil.ObjectToString(generateFileRequest), url);

            String response = httpTransportationService.postRequestSSL(url, JsonUtil.ObjectToString(generateFileRequest), headerParams, javax.ws.rs.core.MediaType.APPLICATION_JSON);
            logger.debug("{} response {}", generateFileRequest.getRefId(), response);

            try{
                generateFileResponse = JsonUtil.StringToObject(response, GenerateFileResponse.class);
                apiLog.setResponse(generateFileResponse);
            }catch(Exception e){
                logger.debug("Error while fetching Perfios Link {}", e.getStackTrace());
            }
            if(generateFileResponse != null){
                generateFileResponse.getPayload().setAcknowledgementId(generateFileResponse.getAcknowledgementId());
                generateFileResponse.getPayload().setPath(generateFileResponse.getPath());
                generateFileResponse.getPayload().setTimestamp(generateFileResponse.getTimestamp());
                generateFileResponse.getPayload().setVendorTransactionId(generateFileResponse.getVendorTransactionId());
            }

            apiLog.setResponseString(response);
            apiLog.setResponse(generateFileResponse);

            externalAPILogRepository.saveCallLog(apiLog, ThirdPartyIntegrationHelper.PERFIOS_GET_FILE_DB_LOG);

            if (generateFileResponse != null) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, generateFileResponse.getPayload());
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            logger.debug("Error while fetching Gst Perfios {}", e.getStackTrace());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse saveFaceMatchData(FaceMatchRequest faceMatchRequest) {
        BaseResponse baseResponse;
        try{
            logger.debug("saveFaceMatchData method started for refId : {}", faceMatchRequest.getRefId());
            applicationRepository.saveFaceMatchData(faceMatchRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "FaceMatch data saved");
        } catch (Exception e) {
            logger.error("{ } refId, Exception occurred while saving faceMatch data {}. Exception {}",
                    faceMatchRequest.getRefId()  , e.getMessage(), ExceptionUtils.getStackTrace(e));
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

}


