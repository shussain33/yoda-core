package com.softcell.service.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.Stages;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.ThirdPartyApiLog;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.InitialMoneyDeposit;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.BankingVerification;
import com.softcell.gonogo.model.core.verification.ItrVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.insurance.icici.IciciPremiumCalculationRequest;
import com.softcell.gonogo.model.insurance.icici.IciciPremiumCalculationResponse;
import com.softcell.gonogo.model.insurance.icici.IciciRequest;
import com.softcell.gonogo.model.insurance.icici.IciciResponse;
import com.softcell.gonogo.model.insurance.religare.CreatePolicySecure;
import com.softcell.gonogo.model.insurance.religare.religareResponse.Religare;
import com.softcell.gonogo.model.lms.LmsRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.mifin.AddressDetails;
import com.softcell.gonogo.model.mifin.*;
import com.softcell.gonogo.model.mifin.BankDetails;
import com.softcell.gonogo.model.mifin.BasicInfo;
import com.softcell.gonogo.model.mifin.ChargeDetails;
import com.softcell.gonogo.model.mifin.Charges;
import com.softcell.gonogo.model.mifin.CreditSanction;
import com.softcell.gonogo.model.mifin.Customer;
import com.softcell.gonogo.model.mifin.CustomerDetails;
import com.softcell.gonogo.model.mifin.DisbursalDetails;
import com.softcell.gonogo.model.mifin.IMD.IMDMiFinRequest;
import com.softcell.gonogo.model.mifin.Verification;
import com.softcell.gonogo.model.mifin.crm.CrmResponse;
import com.softcell.gonogo.model.mifin.topup.*;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dmz.DMZRequest;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.CreditVidyaResponse;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.saathi.SaathiInsertCdRequest;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.AmbitMifinManager;
import com.softcell.service.ExternalApiManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.thirdparty.utils.IciciHelper;
import com.softcell.service.thirdparty.utils.InsurancePolicyEvaluator;
import com.softcell.service.thirdparty.utils.PolicyHelper;
import com.softcell.service.thirdparty.utils.ReligareHelper;
import com.softcell.service.utils.MiFinHelper;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.softcell.service.utils.MiFinHelper.*;

/**
 * Created by archana on 4/10/17.
 */
@Service
public class ExternalApiManagerImpl implements ExternalApiManager {

    private static final Logger logger = LoggerFactory.getLogger(ExternalApiManagerImpl.class);

    private static final String PROSPECTNO = "prospectno";
    private static final String MOBILENO = "mobileno";
    private static final String CUSTOMERNAME = "customername";
    private static final String DEALERNAME = "dealername";
    private static final String BRANCH = "branch";

    private static final String STATUS = "status";
    private static final String CURRENT_STATUS = "currentStatus";
    private static final String PORTFOLIO = "portfolio";
    private static final String MODEL_DESC = "modelDesc";
    private static final String TENOR = "tenur";
    private static final String EMI = "emi";
    private static final String AREACODE = "areaCode";
    private static final String ADVANCED_EMI_COUNT = "advancedEmiCount";
    private static final String CUSTOMER_IRR = "customerIRR";
    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private ExternalAPILogRepository extenalApiMongoRepository;


    @Autowired
    private  MiFinHelper miFinHelper;

    @Autowired
    private AmbitMifinManager ambitMifinManager;

    @Autowired
    private PolicyHelper policyHelper;

    @Autowired
    IciciHelper iciciHelper;

    @Autowired
    ReligareHelper religareHelper;

    @Autowired
    private AuditHelper auditHelper;

    /**
     * Pushes data to external application if there is configuration present for the customer
     * @param applicationRequest
     */
    @Override
    public SaathiResponse pushDataToSaathi(ApplicationRequest applicationRequest) throws Exception {
        if (org.springframework.util.CollectionUtils.isEmpty(applicationRequest.getRequest().getApplicant().getPhone())) {
            throw new GoNoGoException("Phone number not present");
        }

        // Activity log
        ActivityLogs activityLog = AuditHelper.createActivityLog( applicationRequest,
                GNGWorkflowConstant.DE.toFaceValue(), GNGWorkflowConstant.TVS_CD_API_CALL.toFaceValue(),
                GNGWorkflowConstant.TVS_CD_API_CALL.toFaceValue(), GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String urlType = UrlType.TVS_CD_INSERT_CD.toValue();
        WFJobCommDomain customerDataPushConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);

        //Build request for the connector call
        SaathiInsertCdRequest saathiInsertCdRequest = new SaathiInsertCdRequest();
        saathiInsertCdRequest.setData(formInsertCdRequest(applicationRequest));
        saathiInsertCdRequest.setHeaderKeyList(customerDataPushConfig.getKeys());

        String url = Arrays.asList(customerDataPushConfig.getBaseUrl(), customerDataPushConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        //Make the API call
        SaathiResponse saathiInsertCdResponse = (SaathiResponse) TransportUtils.postJsonRequest(saathiInsertCdRequest,
                url, SaathiResponse.class);

        // Save to external API log
        SaathiCallLog apiLog = new SaathiCallLog();
        logger.debug("Tvs CD response : {}", saathiInsertCdResponse);
        apiLog.setApi(urlType);
        apiLog.setMobileNumber(applicationRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber());
        apiLog.setRefId(applicationRequest.getRefID());
        apiLog.setResponse(saathiInsertCdResponse);
        externalAPILogRepository.saveTvscdCallLog(apiLog);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityLog.setStatus(Status.COMPLETE.toString());
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return saathiInsertCdResponse;
    }

    private String formInsertCdRequest(ApplicationRequest applicationRequest) throws JsonProcessingException {
        Map<String, String> requestData = new HashMap<>();

        requestData.put(MOBILENO, applicationRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber());
        requestData.put(PROSPECTNO, applicationRequest.getRefID());

        requestData.put(CUSTOMERNAME, GngUtils.convertNameToFullName(applicationRequest.getRequest().getApplicant().getApplicantName()));
        requestData.put(DEALERNAME, applicationRequest.getHeader().getDealerId());
        requestData.put(BRANCH, applicationRequest.getHeader().getBranchCode());
/*

        String statusValue = GNGWorkflowConstant.YES.toFaceValue();
        String currentStatusValue = "NEW";
        String portfoliValie = "TW";
        String modelDescValue =""; // TODO
        String tenorValue = Integer.toString(applicationRequest.getRequest().getApplication().getLoanTenor());
        String emiValue = Double.toString(applicationRequest.getRequest().getApplication().getEmi());
        String areaCodeValue = ""; /// TODO
        String advancedEmiCountValue = Integer.toString(applicationRequest.getRequest().getApplication().getNumberOfAdvanceEmi());
        String customerIRRValue = ""; // TODO

        requestData.put(STATUS, statusValue);
        requestData.put(CURRENT_STATUS, currentStatusValue);
        requestData.put(PORTFOLIO, portfoliValie);
        requestData.put(MODEL_DESC, modelDescValue);
        requestData.put(TENOR, tenorValue);
        requestData.put(EMI, emiValue);
        requestData.put(AREACODE, areaCodeValue);
        requestData.put(ADVANCED_EMI_COUNT, advancedEmiCountValue);
        requestData.put(CUSTOMER_IRR, customerIRRValue);
*/

        return JsonUtil.ObjectToString(requestData);
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId, String urlType) throws GoNoGoException {
        WFJobCommDomain customerDataPushPullConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                institutionId, urlType);

        if (null == customerDataPushPullConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return customerDataPushPullConfig;
    }

    @Override
    public MiFinResponse pushDataToMiFin(GoNoGoCustomerApplication goCustomerApplication) throws Exception {
        // Activity log
        ApplicationRequest applicationRequest = goCustomerApplication.getApplicationRequest();

        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String product = applicationRequest.getHeader().getProduct().toProductName();
        String currentStageId = applicationRequest.getCurrentStageId();

        boolean isDigiPl = StringUtils.equalsIgnoreCase(product, Product.DIGI_PL.toProductName());
        boolean isDEStage = StringUtils.equalsIgnoreCase(Stages.Stage.DE.name(), currentStageId) ;
        boolean isTranchHopsStage = StringUtils.equalsIgnoreCase(Stages.Stage.TRANCH_HOPS.name(), currentStageId);
        boolean isTranchWise = false;

        ActivityLogs activityLog = AuditHelper.createActivityLog( applicationRequest,
                currentStageId, GNGWorkflowConstant.MIFIN_API_CALL.toFaceValue(),
                "MIFIN_API_CALL", GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);

        if(isDigiPl && !StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(), currentStageId)
                && null != loanCharges){
            logger.debug("Getting tranch-wise loan charges.");
            loanCharges =  miFinHelper.getTranchWiseLoanCharges(applicationRequest , loanCharges.getLoanChargesList());
            isTranchWise = true;
        }

        WFJobCommDomain customerDataPushConfig;
        String urlType;
        String url;
        Map<String, String> headerMap = new HashMap<>();
        if(isDigiPl) {
            urlType =  UrlType.DIGI_MIFIN.toValue();
            customerDataPushConfig = getServiceConfiguration(institutionId, product, urlType);
            url = customerDataPushConfig.getBaseUrl();
            if (CollectionUtils.isNotEmpty(customerDataPushConfig.getKeys())) {
                for (HeaderKey key : customerDataPushConfig.getKeys()) {
                    headerMap.put(key.getKeyName(), key.getKeyValue());
                }
            }
        }else {
            urlType = UrlType.MIFIN.toValue();
            customerDataPushConfig = getServiceConfiguration(institutionId, urlType);
            url = Arrays.asList(customerDataPushConfig.getBaseUrl(), customerDataPushConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        }

        //Build request for the connector call
        MiFinRequest miFinRequest = null;
        String mifinRequest;
        if(isTranchWise && isTranchHopsStage) {
            miFinRequest = buildMiFinRequestForSecondTranch(applicationRequest, loanCharges);
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            mifinRequest = mapper.writeValueAsString(miFinRequest);
        }else {
            miFinRequest = buildMiFinRequest(goCustomerApplication, loanCharges);
            mifinRequest = JsonUtil.ObjectToString(miFinRequest);
        }
        logger.debug("MiFin request for refid {} : {}", refId, mifinRequest);

        //Make the API call
        MiFinResponse miFinResponse = null;
        if(isDigiPl) {
            DMZRequest connectorRequest = new DMZRequest();
            connectorRequest.setRequestType(urlType);
            if(isTranchHopsStage)
                connectorRequest.setRequestURL(headerMap.get("SECOND_TRANCH_URL"));
            else
                connectorRequest.setRequestURL(headerMap.get("FIRST_TRANCH_URL"));

            connectorRequest.setRequestString(mifinRequest);
            String responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);
            logger.info("Mifin response String for DIGI_PL : {}", responseString);
            if(StringUtils.isNotEmpty(responseString)) {
                DMZResponse dmzResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
                miFinResponse = JsonUtil.StringToObject(dmzResponse.getResponseString(), MiFinResponse.class);
            }
        }else {
            miFinResponse = (MiFinResponse) TransportUtils.postJsonRequest(miFinRequest,
                    url, MiFinResponse.class);
        }

        // Save to external API log
        MiFinCallLog apiLog = new MiFinCallLog();
        logger.info("MiFin response : {} for refId: {}", miFinResponse,refId);
        apiLog.setApi(urlType);
        apiLog.setRefId(refId);
        apiLog.setMiFinRequest(miFinRequest);
        apiLog.setMiFinResponse(miFinResponse);
        apiLog.setRequest(mifinRequest);
        externalAPILogRepository.saveMiFinCallLog(apiLog);

        if(miFinResponse != null && miFinResponse.getMiFinBaseResponse() != null
                && StringUtils.isNotEmpty(miFinResponse.getMiFinBaseResponse().getProspectCode())
                && StringUtils.isNotEmpty(miFinResponse.getMiFinBaseResponse().getDisbursalAmt())
                && StringUtils.isNotEmpty(miFinResponse.getMiFinBaseResponse().getCustomerCode())
                && StringUtils.equals(miFinResponse.getMiFinBaseResponse().getStatus(), MiFinHelper.STATUS_SUCCESS)) {
            logger.info("Data pushed to Mifin Successfully for refId: {}; Received customerCode {}, prospectCode{}"
                    ,applicationRequest.getRefID(), miFinResponse.getMiFinBaseResponse().getCustomerCode(),
                    miFinResponse.getMiFinBaseResponse().getProspectCode());
            // serviceType = MIFIN_API_CALL to make this method generic
            applicationRequest.setMiFinCustomerCode(miFinResponse.getMiFinBaseResponse().getCustomerCode());
            applicationRequest.setMiFinProspectCode(miFinResponse.getMiFinBaseResponse().getProspectCode());

            if(isTranchWise && isDEStage) {
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.TRANCH_BOPS.toFaceValue());
                applicationRequest.setStProcessed(true);
            }else
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.DISB.toFaceValue());

            applicationRepository.updateExternalServiceFlag(applicationRequest, GNGWorkflowConstant.MIFIN_API_CALL.name());
            try {
                logger.debug("******* Call to Insurance API *****");
                InsuranceData insuranceData = null;
                insuranceData = applicationRepository.fetchInsuranceData(refId);
                if(insuranceData != null){
                    InsuranceRequest insuranceRequest = new InsuranceRequest();
                    insuranceRequest.setRefId(refId);
                    insuranceRequest.setHeader(applicationRequest.getHeader());
                    insuranceRequest.setInsuranceData(insuranceData);
                    pushPolicy(insuranceRequest, loanCharges);
                    //callToIciciInsurance(applicationRequest, loanCharges);
                }
            } catch (Exception exp) {
                logger.error("exception while calling icici {}", exp.getStackTrace());
            }
            /*******Call to creditVidya*****/
            Applicant applicant = applicationRequest.getRequest().getApplicant();
            List<CoApplicant> coApplicants =  applicationRequest.getRequest().getCoApplicant();
            if(StringUtils.equalsIgnoreCase(currentStageId,GNGWorkflowConstant.DISB.toFaceValue()) ){
                List<CoApplicant> withConsentToCV = null;
                CreditVidyaResponse creditVidyaResponse = null;
                // TODO : Move the consent flag to Consent list
                if(applicant.isCreditVidyaFlag())
                {
                    logger.info("Calling credit vidya for Applicant with applId {}", applicant.getApplicantId());
                   creditVidyaResponse  = callToCreditVidya(applicationRequest,MiFinHelper.APPLICANT.toString(),applicant.getApplicantId());

                    //Save response to db.
                    if(creditVidyaResponse.getError()!= null) {
                        Object orgResp = creditVidyaResponse.getOriginalResp();
                        // TODO : Refator GetUserProfileCallLog to use it in generic manner
                        GetUserProfileCallLog userProfileCallLog = GetUserProfileCallLog.builder()
                                .refId(refId).applicantId(applicant.getApplicantId()).orgCreditVidyaResp(orgResp).build();
                        externalAPILogRepository.saveCreditVidyaCallLog(userProfileCallLog);
                    }
                }
                if(coApplicants != null)
                {
                   withConsentToCV = coApplicants.stream()
                            .filter(coApplicant -> coApplicant.isCreditVidyaFlag() == true).collect(Collectors.toList()); // returns coApplicants with creditVidya consent true.
                   // TODO isEmpty check on  withConsentToCV
                   for (CoApplicant coApplicant:withConsentToCV)                    {
                        logger.info("Calling credit vidya for Applicant with applId {}", applicant.getApplicantId());
                        creditVidyaResponse = callToCreditVidya(applicationRequest,MiFinHelper.COAPPLICANT.toString(),coApplicant.getApplicantId());
                        if(creditVidyaResponse.getError()!= null) {
                            logger.info("Saving response from creditVidya direct to DB...creditVidyaCallLog collection");
                            Object orgResp = creditVidyaResponse.getOriginalResp();
                            GetUserProfileCallLog userProfileCallLog = GetUserProfileCallLog.builder()
                                    .refId(refId).applicantId(applicant.getApplicantId()).orgCreditVidyaResp(orgResp).build();
                            externalAPILogRepository.saveCreditVidyaCallLog(userProfileCallLog);
                        }
                   }
                }
            } else {
                logger.info("Credit vidya not called since failure in disbursement");
            }
        } else {
            //reverting stage in case of Mifin data push failure.
            if(isTranchWise && isDEStage) {
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.BOPS.toFaceValue());
            }else if(isTranchWise && isTranchHopsStage) {
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.TRANCH_HOPS.toFaceValue());
                applicationRequest.setFinalTranchCancelled(false);
            }else
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.HOPS.toFaceValue());
//            applicationRepository.updateCurrentSatgeStageId(applicationRequest,GNGWorkflowConstant.HOPS.toFaceValue());
            logger.info("Data push to Mifin failed!!");
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityLog.setStatus(Status.COMPLETE.toString());
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return miFinResponse;
    }

    @Override
    public CreditVidyaResponse callToCreditVidya(String refId, String instId){
        ApplicationRequest applicationRequest = null;
        List<CoApplicant> coApplicants = null;
        Applicant applicant = null;
        try {
            applicationRepository = new ApplicationMongoRepository();
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            if(goNoGoCustomerApplication!=null){
                logger.info("rendered gngApplication");
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                applicant = applicationRequest.getRequest().getApplicant();
            }
        } catch (Exception e) {
            // logger.error("{}",e.getStackTrace());
        }
        CreditVidyaResponse creditVidyaResponse = callToCreditVidya(applicationRequest,MiFinHelper.APPLICANT.toString(),applicant.getApplicantId());
        return creditVidyaResponse;
    }

    public CreditVidyaResponse callToCreditVidya(ApplicationRequest applicationRequest, String type, String applicantId) {
        WFJobCommDomain commDomain = null;
        CreditVidyaBaseRequest creditVidyaBaseRequest = null;
        CreditVidyaRequest creditVidyaRequest = null;
        CreditVidyaResponse creditVidyaResponse = null;
        String url = null;
        try {
            commDomain =  getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(),
                    applicationRequest.getHeader().getProduct().name(), UrlType.CREDIT_VIDYA.toValue());
            url = commDomain.getBaseUrl();

            logger.info("building credit vidya request for url {}", url);
            creditVidyaBaseRequest = buildCreditVidyaRequest(applicationRequest, type, applicantId, commDomain);
            logger.debug(" credit vidya request {} ", creditVidyaBaseRequest);
        } catch (Exception e) {
            logger.info("Config not found for inst {}, {}, product {}", applicationRequest.getHeader().getInstitutionId(),
                    UrlType.CREDIT_VIDYA.toValue(), applicationRequest.getHeader().getProduct().name() );
            // logger.error("{}",e.getStackTrace());
        }
        if(creditVidyaBaseRequest!= null){
            String requestParams = null;
            try {
                requestParams = JsonUtil.ObjectToString(creditVidyaBaseRequest);
                logger.info("credit vidya request for connector {}", requestParams);
            } catch (JsonProcessingException e) {
                // logger.error("{}",e.getStackTrace());
            }
            creditVidyaRequest = new CreditVidyaRequest();
            creditVidyaRequest.setRequetsParams(requestParams);
        }
        try {
            logger.info("Request String : {}",creditVidyaRequest);
            creditVidyaResponse  = (CreditVidyaResponse)  TransportUtils.postJsonRequest(creditVidyaRequest,
                    url, CreditVidyaResponse.class);
            if(creditVidyaResponse != null){
                if(creditVidyaResponse.getError()!= null){
                    logger.info("Error occured from connector {}", creditVidyaResponse.getError().getMessage());
                }
            }
        } catch (Exception e){
            logger.info("Exception while getting response ");
            // logger.error("{}",e.getStackTrace());
        }
        //TODO save actual respnse in externalAPICalls per applId.
        return creditVidyaResponse;
    }

    private CreditVidyaBaseRequest buildCreditVidyaRequest(ApplicationRequest applicationRequest, String type,
                                                           String applicantId, WFJobCommDomain commDomain) throws Exception{
        logger.info("Inside buildCreditVidyaRequest()");
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        CustomerData customerData = null;
        if(goNoGoCustomerApplication!=null) {
            customerData = getCustomerData(goNoGoCustomerApplication, type,applicantId);
        } else {
            logger.info("No data found");
        }

        return CreditVidyaBaseRequest.builder()
                .requestType(commDomain.getServiceId())//.requestType("FFR")
                .apiAccessKey(commDomain.getLicenseKey())//.apiAccessKey("b37a79dc-4d79-4eca-afbc-d875305d9b8b")
                .partnerCode(commDomain.getMemberId())//.partnerCode("SBFC")
                .sendEmail("Y")
                .customerData(customerData)
                .build();
    }

    private CustomerData getCustomerData(GoNoGoCustomerApplication gngCustAppl, String type, String applicantId) {
        logger.info("Preparing customer data for CreditVidya");
        String firstName = null;
        String lastName = null;
        String email = null;
        String customerSource = null;
        ResponseMultiJsonDomain multiBureauJsonRespose = null;
        Applicant applicant ;
        List<CoApplicant> coApplicants;
        ApplicationRequest applicationRequest = gngCustAppl.getApplicationRequest();
        if(applicationRequest!=null){
            customerSource = applicationRequest.getHeader().getProduct().toProductName();
        }

        if(StringUtils.equalsIgnoreCase(type, MiFinHelper.APPLICANT)){
            logger.info("Collecting data for Applicant");
            applicant = gngCustAppl.getApplicationRequest().getRequest().getApplicant();
            firstName =applicant.getApplicantName().getFirstName();
            lastName = applicant.getApplicantName().getLastName();
            email = applicant.getEmail().get(0)!=null?applicant.getEmail().get(0).getEmailAddress():"";
            multiBureauJsonRespose = gngCustAppl.getApplicantComponentResponse() != null?gngCustAppl.getApplicantComponentResponse().getMultiBureauJsonRespose():null;
        }
        if(StringUtils.equalsIgnoreCase(type, MiFinHelper.COAPPLICANT)){
            logger.info("Collecting data for CoApplicant");
            coApplicants = gngCustAppl.getApplicationRequest().getRequest().getCoApplicant();
            //filtering coApplicant from list By ApplicantID.
            CoApplicant coApplicant = coApplicants.stream()
                    .filter(coApp -> StringUtils.equalsIgnoreCase(coApp.getApplicantId(),applicantId))
                    .findAny()
                    .orElse(null);

            if (coApplicant!= null) {
                firstName = coApplicant.getApplicantName().getFirstName();
                lastName = coApplicant.getApplicantName().getLastName();
                email = coApplicant.getEmail().get(0)!= null? coApplicant.getEmail().get(0).getEmailAddress():"";
            }
            multiBureauJsonRespose = gngCustAppl.getApplicantComponentResponseList() != null || gngCustAppl.getApplicantComponentResponseList().size() != 0 ? findMBResp(gngCustAppl.getApplicantComponentResponseList(),applicantId) : null;
        }

        CreditVidyaMBObj creditVidyaMBObj = prepareMBForCredit(multiBureauJsonRespose);

        return CustomerData.builder()
                .clientId(gngCustAppl.getGngRefId())
                .customerSource(customerSource)
                .emailAddress1(email)
                .firstName(firstName)
                .lastName(lastName)
                .cibilString(creditVidyaMBObj)
                .build();
    }


    private CreditVidyaMBObj prepareMBForCredit(ResponseMultiJsonDomain multiBureauJsonRespose) {

        List<Finished> finisheds = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(multiBureauJsonRespose.getFinishedList())){
            //IN SBFC there is only one bereu ""
            com.softcell.gonogo.model.multibureau.pickup.Finished finished = multiBureauJsonRespose.getFinishedList().get(0);
            Finished f = Finished.builder()
                    .bureau(finished.getBureau())
                    .product(finished.getProduct())
                    .status(finished.getStatus())
                    .trackingId(finished.getTrackingId())
                    .responseJsonObject(finished.getResponseJsonObject())
                    .build();
            finisheds.add(f);
        }

        return CreditVidyaMBObj.builder()
                .status(multiBureauJsonRespose.getStatus())
                .header(multiBureauJsonRespose.getHeader())
                .acknowledgmentId(multiBureauJsonRespose.getAcknowledgmentId())
                .finishedList(finisheds)
                .build();

    }

    private ResponseMultiJsonDomain findMBResp(List<ComponentResponse> applicantComponentResponseList, String applicantId) {
        ResponseMultiJsonDomain mbResponse = null;
        if(applicantComponentResponseList!=null) {
        ComponentResponse componentResp = applicantComponentResponseList.stream()
                .filter(componentResponse -> StringUtils.equalsIgnoreCase(componentResponse.getApplicantId(), applicantId)).findAny().orElse(null);
        if (componentResp != null){
            mbResponse = componentResp.getMultiBureauJsonRespose();
        }
        }
        return mbResponse;
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId,  String product, String urlType) throws GoNoGoException {
        logger.info("fetching config against instId {} , utlType {}, product {}",institutionId,urlType,product);
        WFJobCommDomain customerDataPushPullConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                institutionId,  product, urlType);

        if (null == customerDataPushPullConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return customerDataPushPullConfig;
    }

    @Override
    public MiFinResponse pushDataToMiFin(String refId) throws Exception {
        MiFinResponse miFinResponse = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
        if(goNoGoCustomerApplication != null) {
            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            miFinResponse = pushDataToMiFin(goNoGoCustomerApplication);
        }
        else {
            logger.info("No DAT found Against provided request");
        }
        return miFinResponse;
    }

   /* pushing data to LMS
    @Override
    public LmsResponse pushDataToLMS(ApplicationRequest applicationRequest) throws Exception {
        // Activity log
        ActivityLogs activityLog = AuditHelper.createActivityLog( applicationRequest,
                applicationRequest.getCurrentStageId(), GNGWorkflowConstant.LMS_API_CALL.toFaceValue(),
                GNGWorkflowConstant.LMS_API_CALL.toFaceValue(), GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String urlType = UrlType.LMS.toValue();
        WFJobCommDomain customerDataPushConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);
       // WFJobCommDomain customerDataPushConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), applicationRequest.getHeader().getProduct().toProductName(),urlType);

        String url = Arrays.asList(customerDataPushConfig.getBaseUrl(), customerDataPushConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

        //Build request for the connector call
        LmsRequest lmsRequest = buildLMSRequest(applicationRequest);
        logger.debug("LMS request for refid {} : {}", applicationRequest.getRefID(), lmsRequest);

        //Make the API call
        LmsResponse lmsResponse = (LmsResponse) TransportUtils.postJsonRequest(lmsRequest,
                url, LmsResponse.class);
        logger.debug("lmsResponse {} ", lmsResponse);

        // Save to external API log
        LmsCallLog apiLog = new LmsCallLog();
        logger.info("LMS response : {} for refId: {}", lmsResponse,applicationRequest.getRefID());
        apiLog.setApi(urlType);
        apiLog.setRefId(applicationRequest.getRefID());
        apiLog.setLmsRequest(lmsRequest);
        apiLog.setLmsResponse(lmsResponse);
        externalAPILogRepository.saveLMSCallLog(apiLog);

        if( lmsResponse != null && lmsResponse.getError_description() !=null) {
            logger.info("Data pushed to LMS Successfully for refId: {}; Received clientId {}, resourceId{}"
                    ,applicationRequest.getRefID(), lmsResponse.getClientId(),
                    lmsResponse.getResourceId());
            if(applicationRequest.getExternalServiceCalls() == null) {
                applicationRequest.setExternalServiceCalls(new ExternalServiceCalls());
                applicationRequest.getExternalServiceCalls().setLmsMsg(lmsResponse.getError_description().toString());
                applicationRepository.updateExternalServiceFlagForLMS(applicationRequest, GNGWorkflowConstant.LMS_API_CALL.name());
            }else{
                applicationRequest.getExternalServiceCalls().setLmsMsg(lmsResponse.getError_description().toString());
                applicationRepository.updateExternalServiceFlagForLMS(applicationRequest, GNGWorkflowConstant.LMS_API_CALL.name());
            }
        } else {

            applicationRequest.setCurrentStageId(GNGWorkflowConstant.HOPS.toFaceValue());
//            applicationRepository.updateCurrentSatgeStageId(applicationRequest,GNGWorkflowConstant.HOPS.toFaceValue());
            logger.info("Data push to LMS failed!!");
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityLog.setStatus(Status.COMPLETE.toString());
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return lmsResponse;
    }*/

    private LmsRequest buildLMSRequest(ApplicationRequest applicationRequest) {
        LmsRequest request = LmsRequest.builder().build();
        request.setHeader(applicationRequest.getHeader());
        request.setRefID(applicationRequest.getRefID());
        return request;
    }

    private MiFinRequest buildMiFinRequestForSecondTranch(ApplicationRequest applicationRequest, LoanCharges loanCharges){
        logger.debug("buildMiFinRequestForSecondTranch method started");
        MiFinRequest request = MiFinRequest.builder().build();
        // Authentication
        Authentication authentication = getAuthenticationDetails(applicationRequest);
        authentication.setAppPass("PASS@123");
        authentication.setDeviceId("FINAL_TRANCHE");
        request.setAuthentication(authentication);
        logger.debug("1) Section Authentication populated");

        BasicInfo basicInfo = getBasicInfo(applicationRequest, loanCharges);
        request.setBasicInfo(basicInfo);
        logger.debug("2) Section BasicInfo populated");

        Charges charges = getChargeDetails(applicationRequest, loanCharges);
        request.setCharges(charges);
        logger.debug("3) Section Charges populated");

        logger.debug("buildMiFinRequestForSecondTranch method ended");
        return request;
    }

    private BasicInfo getBasicInfo(ApplicationRequest applicationRequest, LoanCharges loanCharges){
        BasicInfo basicInfo = new BasicInfo();
        if(null != applicationRequest){
            if(StringUtils.isNotEmpty(applicationRequest.getMiFinProspectCode())) {
                basicInfo.setProspectCodee(applicationRequest.getMiFinProspectCode());

                if(applicationRequest.isFinalTranchCancelled())
                    basicInfo.setFinalTranchFlag("P"); // P - Previous Tranch
                else
                    basicInfo.setFinalTranchFlag("F"); // F - Final Tranch
            }else {
                basicInfo.setProspectCodee(Constant.BLANK);
                basicInfo.setFinalTranchFlag(Constant.BLANK);
            }
            if (applicationRequest.getAppMetaData() != null) {
                if (applicationRequest.getAppMetaData().getBranchV2() != null) {
                    if(applicationRequest.getAggregatorData() != null && StringUtils.isNotEmpty(applicationRequest.getAggregatorData().getLoginRole())){
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
                    }else{
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
                    }
                    if (StringUtils.equalsIgnoreCase("Spoke", applicationRequest.getAppMetaData().getBranchV2().getBranchType()) ||
                            StringUtils.equalsIgnoreCase("LoanStation", applicationRequest.getAppMetaData().getBranchV2().getBranchType())){
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getHubBranchName()));
                    }
                    basicInfo.setRelationshipManager(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchManagerName()));
                }
            }
            if(null != loanCharges) {
                LoanAmtDetails loanAmtDetails = loanCharges.getLoanDetails();
                if(null != loanAmtDetails) {
                    basicInfo.setPreviousTranchAmount(applicationRequest.getRequest().getApplication().getTranchAmount());
                    basicInfo.setFinalTranchAmount(setAmount(loanAmtDetails.getFinalDisbAmt()));
                }else {
                    basicInfo.setPreviousTranchAmount(Constant.ZERO);
                    basicInfo.setFinalTranchAmount(Constant.ZERO);
                }
                basicInfo.setTranchDate(GngUtils.setValueForNull(GngDateUtil.getYYYYMMDDFormat(new Date())));
            }
        }
        return basicInfo;
    }

    private MiFinRequest buildMiFinRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, LoanCharges loanCharges) throws Exception {
        MiFinRequest request = MiFinRequest.builder().build();
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String product = applicationRequest.getHeader().getProduct().toProductName();
        boolean isDigiPl = StringUtils.equalsIgnoreCase(product, Product.DIGI_PL.toProductName());

        CamDetails camDetails = getCamDetails(applicationRequest);
        EligibilityDetails eligibilityDetails;
        if(isDigiPl)
            eligibilityDetails = applicationRequest.getRequest().getApplicant().getDigiPLEligibility();
        else
            eligibilityDetails = applicationRepository.fetchEligibilityDetails(refId, institutionId);

        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetails(refId, institutionId);
        CoOrigination origination  = applicationRepository.fetchCoOriginationDetails(refId, institutionId);
        Repayment repayment = applicationRepository.fetchRepaymentDetails(refId, institutionId);
        logger.info( "Fetched camdetails : {}, loancharges : {}, eligibilityDetails : {}, legalVerification : {}",
                camDetails != null ? true : false, loanCharges != null ? true : false,
                eligibilityDetails != null ? true : false, legalVerification != null ? true : false );

        // Authentication
        Authentication authentication = getAuthenticationDetails(applicationRequest);
        request.setAuthentication(authentication);
        logger.debug("1) Section Authentication populated");
        //Basic info
        BasicInfo basicInfo = getBasicInfo(applicationRequest, camDetails, origination);
        request.setBasicInfo(basicInfo);
        logger.debug("2) Section BasicInfo populated");
        // Credit Sanction
        CreditSanction creditSanction = getCreditSanction(applicationRequest, camDetails, loanCharges, eligibilityDetails,origination);
        request.setCreditSanction(creditSanction);
        logger.debug("3) Section CreditSanction populated");
        // Charges
        Charges charges = getChargeDetails(applicationRequest, loanCharges);
        request.setCharges(charges);
        logger.debug("4) Section Charges populated");
        // Verification details : Currently we are not sending verification detailsbut an empty object
        Verification verification;//= getVerificationDetails();
        verification = getVerificationDetails(applicationRequest);
        request.setVerification(verification);
        logger.debug("5) Section Verification populated");
        // Documents
        Documents documents = getDocumentsDetails(applicationRequest);
        request.setDocuments(documents);
        logger.debug("6) Section Documents populated");
        // Asset details
        Asset asset = miFinHelper.getAssetDetails(applicationRequest, legalVerification, camDetails);
        request.setAsset(asset);
        logger.debug("7) Section Asset populated");
        // Bank details
        Bank bank = getBankDetails(applicationRequest, repayment);
        request.setBank(bank);
        logger.debug("8) Section Bank populated");
        // Disbursal details
        Disbursal disbursal = getDisbursalDetails(applicationRequest, origination);
        request.setDisbusal(disbursal);
        logger.debug("9) Section Disbursal populated");
        // Customer details
        Customer customer = getCustomerDetails(goNoGoCustomerApplication, eligibilityDetails);
        request.setCustomer(customer);
        logger.debug("10) Section Customer populated");
        return request;
    }

    private Documents getDocumentsDetails(ApplicationRequest applicationRequest) {
        Documents document = new Documents();
        List<DocumentDetails> documnetDetailsList = new ArrayList<>();
        DocumentDetails documnetDetails = new DocumentDetails();
        ListOfDocs listOfDocs = applicationRepository.fetchListOfDocs(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());

        if (listOfDocs != null) {
            List<DocDetail> docDetailList = listOfDocs.getDocDetails();
            if (CollectionUtils.isNotEmpty(docDetailList)) {
                for (DocDetail docDetail : docDetailList) {
                    if (StringUtils.isNotEmpty(docDetail.getDocType())) {
                        documnetDetails.setDocumentType(docDetail.getDocType());
                    }
                    if (StringUtils.isNotEmpty(docDetail.getDesc())) {
                        documnetDetails.setDocumentName(docDetail.getDesc());
                    }
                    if (StringUtils.isNotEmpty(docDetail.getRemarks().get(0).getComment())) {
                        documnetDetails.setDocumentDes(docDetail.getRemarks().get(0).getComment());
                    }
                    if (StringUtils.isNotEmpty(docDetail.getOpsDocStatus())) {
                        documnetDetails.setStatus(docDetail.getOpsDocStatus());
                    }
                    if (docDetail.getOpsReceiptDt() != null) {
                        documnetDetails.setReceivedDate(docDetail.getOpsReceiptDt().toString());
                    }
                    documnetDetailsList.add(documnetDetails);
                }
            }
        }
        document.setDocumentDetailsList(documnetDetailsList);
        return document;
    }

    private Disbursal getDisbursalDetails(ApplicationRequest applicationRequest, CoOrigination origination) {
        Product product = applicationRequest.getHeader().getProduct();
        Disbursal disbursal = new Disbursal();
        List<DisbursalDetails> disbursalDetailsList = new ArrayList<>();
        disbursal.setDisbusalDetails(disbursalDetailsList);

        DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(applicationRequest.getRefID()
                , applicationRequest.getHeader().getInstitutionId());
        if(null != disbursementMemo){
            CamSummary camSummary = null;
            if (disbursementMemo.getCamSummary() != null) {
                camSummary = disbursementMemo.getCamSummary();
            }
            //DMInput disbursement = null;
            if (CollectionUtils.isNotEmpty(disbursementMemo.getDmInputList())) {
                if (product == Product.DIGI_PL &&
                        !StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(), applicationRequest.getCurrentStageId())) {
                    DMInput disbursement = miFinHelper.getTranchWiseDMInput(applicationRequest, disbursementMemo.getDmInputList());
                    if (disbursement != null)
                        getDisbursalDetails(applicationRequest, origination, camSummary, disbursement, disbursalDetailsList);
                } else {
                    for (DMInput disbursement : disbursementMemo.getDmInputList()) {
                        if (disbursement != null)
                            getDisbursalDetails(applicationRequest, origination, camSummary, disbursement, disbursalDetailsList);
                    }
                }
            }
        }

        return disbursal;
    }

    private void getDisbursalDetails(ApplicationRequest applicationRequest, CoOrigination origination, CamSummary camSummary, DMInput disbursement, List<DisbursalDetails> disbursalDetailsList) {
        DisbursalDetails disbursalDetails = new DisbursalDetails();
        disbursalDetails.setBankId(trim(disbursement.getDmBankName(), 20));
        disbursalDetails.setBankBranchId(Constant.BLANK);
        disbursalDetails.setInstrumentType(MiFinHelper.PAYMENT_MODE.get(disbursement.getDmPaymentMode()));
        disbursalDetails.setInstrumentNo(disbursement.getInstrumentNo());
        if (disbursement.getDmDate() != null) {
            disbursalDetails.setInstrumentDate(GngUtils.setValueForNull(GngDateUtil.getYYYYMMDDFormat(disbursement.getDmDate())));
        }
        if (camSummary != null) {
            disbursalDetails.setBopsId(camSummary.getFinalLoanApprovedBy());
        } else {
            logger.info("CamSummary in dmDetails is blank");
            disbursalDetails.setBopsId(Constant.BLANK);
        }
        if (applicationRequest.getRequest().getApplication().isCoOrigination()
                && origination != null && CollectionUtils.isNotEmpty(origination.getCoOriginationDetailsList())) {
            double sbfcAmt = GngCalculationUtils.
                    calculateApprovedAMt(origination.getRoiMasterdata().getInternalPercentage(),
                            GngCalculationUtils.roundValue(Double.parseDouble(disbursement.getDmAmt()),2),Institute.SBFC.name());

            double partnerBankAMt = GngCalculationUtils.
                    calculateApprovedAMt(origination.getRoiMasterdata().getExternalPercentage(),
                            GngCalculationUtils.roundValue(Double.parseDouble(disbursement.getDmAmt()),2),Institute.SBFC.name());

            if(GngCalculationUtils.roundValue(sbfcAmt %1,1) > GngCalculationUtils.roundValue(partnerBankAMt %1,1)){
                disbursalDetails.setDisbursalAmtSBFC(GngUtils.setValueForZero(setAmount(Math.ceil(sbfcAmt))));
                disbursalDetails.setDisbursalAmtPartnerBank(GngUtils.setValueForZero(setAmount(Math.floor(partnerBankAMt))));
            }else{
                disbursalDetails.setDisbursalAmtSBFC(GngUtils.setValueForZero(setAmount(Math.floor(sbfcAmt))));
                disbursalDetails.setDisbursalAmtPartnerBank(GngUtils.setValueForZero(setAmount(Math.ceil(partnerBankAMt))));
            }

        } else {
            disbursalDetails.setDisbursalAmtSBFC(Constant.ZERO);
            disbursalDetails.setDisbursalAmtPartnerBank(Constant.ZERO);
        }
        disbursalDetails.setIssueInFavourOf(GngUtils.setValueForBlank(disbursement.getDmFavourting()));
        disbursalDetails.setTowards(GngUtils.setValueForBlank(disbursement.getDmFavourting()));
        disbursalDetails.setHopsId(disbursement.getDmApprovedBy());
        disbursalDetails.setDisbursalAmt(setAmount(StringUtils.trim(disbursement.getDmAmt())));
        disbursalDetailsList.add(disbursalDetails);
    }

    private String trim(String value, int trimIndex) {
        return StringUtils.substring(StringUtils.trim(value), 0, trimIndex);
    }


    private Authentication getAuthenticationDetails(ApplicationRequest applicationRequest) {
        //TODO get from configuration
        Authentication authentication = new Authentication();
        authentication.setAppName("MIFIN");
        authentication.setAppPass("PASS@1234");
        authentication.setIpAddress("127.0.0.1");
        authentication.setDeviceId("GONOGO");
        authentication.setLatitude("53");
        authentication.setLongitude("43");
        return authentication;
    }

    private Customer getCustomerDetails(GoNoGoCustomerApplication goNoGoCustomerApplication, EligibilityDetails eligibilityDetails) throws Exception {
        Customer customer = new Customer();
        List<CustomerDetails> customerDetailsList = new ArrayList<>();
        CustomerDetails customerDetails = null;
        Applicant applicant;
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

        String cibilScore = null;
        if (applicationRequest != null && applicationRequest.getRequest() != null) {
            if (applicationRequest.getRequest().getApplicant() != null) {
                applicant = applicationRequest.getRequest().getApplicant();
                customerDetails = setCustomerDetails(applicant, MiFinHelper.APPLICANT);
                customerDetails.setCategory(GngUtils.setValueForNull(MiFinHelper.CUSTOMER_CATEGORY.get(applicationRequest.getApplicantType())));
                if(applicationRequest.getRequest().getTopupInfo() != null){
                    customerDetails.setCustomerCode(GngUtils.setValueForBlank(applicant.getOldCustomerCode()));
                }
                if(applicationRequest.getHeader().getProduct() == Product.PL){
                    customerDetails.setCustomerCode(GngUtils.setValueForBlank(applicationRequest.getRequest().getApplication().getExistingLoanAccNumber()));
                }
                List<AddressDetails> addressDetailsList = getAddressDetails(applicant);
                customerDetails.setAddressDetailsList(addressDetailsList);

                if (eligibilityDetails != null && customerDetails.getGrossIncome() == null) {
                    customerDetails.setGrossIncome(setAmount(eligibilityDetails.getGrossIncome()));
                    customerDetails.setNetIncome(setAmount(eligibilityDetails.getIncome()));
                }
                if (goNoGoCustomerApplication.getApplicantComponentResponse() != null &&
                        goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose() != null) {
                    ResponseMultiJsonDomain responseMultiJsonDomain = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose();
                    cibilScore = GngUtils.getCibilScore(responseMultiJsonDomain);
                    if (StringUtils.isNotEmpty(cibilScore)) {
                        customerDetails.setCibilScore(cibilScore);
                    } else {
                        customerDetails.setCibilScore(Constant.ZERO);
                    }
                } else {
                    customerDetails.setCibilScore(Constant.ZERO);
                }


                customerDetailsList.add(customerDetails);
            }

            List<CoApplicant> coappList = applicationRequest.getRequest().getCoApplicant();
            if (CollectionUtils.isNotEmpty(coappList)) {
                for (CoApplicant coapplicant : coappList) {
                    customerDetails = setCustomerDetails(coapplicant, MiFinHelper.COAPPLICANT);
                    customerDetails.setCategory(GngUtils.setValueForNull(applicationRequest.getApplicantType()));
                    List<AddressDetails> addressDetailsList = getAddressDetails(coapplicant);
                    customerDetails.setAddressDetailsList(addressDetailsList);
                    if (eligibilityDetails != null) {
                        customerDetails.setGrossIncome(setAmount(eligibilityDetails.getGrossIncome()));
                        customerDetails.setNetIncome(setAmount(eligibilityDetails.getIncome()));
                    }
                    if (StringUtils.isNotEmpty(cibilScore)) {
                        customerDetails.setCibilScore(cibilScore);
                    } else {
                        customerDetails.setCibilScore(Constant.ZERO);
                    }
                    if (goNoGoCustomerApplication.getApplicantComponentResponse() != null &&
                            goNoGoCustomerApplication.getApplicantComponentResponse().getMbCorpJsonResponse() != null) {
                        OutputAckDomain outputAckDomain = goNoGoCustomerApplication.getApplicantComponentResponse().getMbCorpJsonResponse();
                        cibilScore = GngUtils.getCibilScore(outputAckDomain);
                        if (StringUtils.isNotEmpty(cibilScore)) {
                            customerDetails.setCibilScore(cibilScore);
                        } else {
                            customerDetails.setCibilScore(Constant.ZERO);
                        }
                    } else {
                        customerDetails.setCibilScore(Constant.ZERO);
                    }
                    if(applicationRequest.getRequest().getTopupInfo() != null){
                        customerDetails.setCustomerCode(GngUtils.setValueForBlank(coapplicant.getOldCustomerCode()));
                    }
                    customerDetailsList.add(customerDetails);
                }

            }
            customer.setCustomerDetailsList(customerDetailsList);
        }
        return customer;
    }

    private CustomerDetails setCustomerDetails(Applicant applicant, String applicant1) {
        Employment employment = null;
        if (CollectionUtils.isNotEmpty(applicant.getEmployment())) {
            List<Employment> employmentList = applicant.getEmployment();
            employment = employmentList.get(0);
        }

        CustomerDetails customerDetails = getDefaultCustomerDetails();
        customerDetails.setCustomerType(MiFinHelper.CUSTOMER_TYPE.get(applicant1));
        customerDetails.setEntityType(MiFinHelper.getCustomerEntityType(applicant.getApplicantType()));

        // todo
        //customerDetails.setOtherEntity();

        if (ApplicantType.INDIVIDUAL.value().
                equalsIgnoreCase(applicant.getApplicantType())) {
            customerDetails.setGender(GngUtils.setValueForNull(MiFinHelper.GENDER_TYPE.get(applicant.getGender())));
            customerDetails.setMaritalStatus(GngUtils.setValueForNull(MiFinHelper.MARITAL_STATUS.get(applicant.getMaritalStatus())));
            customerDetails.setQualification(GngUtils.setValueForNull(MiFinHelper.QUALIFICATION.get(
                    applicant.getEducation())));
            if (StringUtils.isNotEmpty(applicant.getDateOfBirth())) {
                Date parsed = GngDateUtil.getDateFromSpecificFormat(GngDateUtil.ddMMyyyy,
                        applicant.getDateOfBirth());
                String dob = GngDateUtil.changeDateFormat(parsed, GngDateUtil.yyyy_MM_dd);
                customerDetails.setDob(GngUtils.setValueForNull(dob));
            }

            if (applicant instanceof CoApplicant) {
                customerDetails.setRelation(GngUtils.setValueForNull(((CoApplicant) applicant).getRelationWithApplicant()));
            } else {
                customerDetails.setRelation(Constant.BLANK);
            }

            // employment
            if (employment != null) {
                customerDetails.setEmploymentType(employment.getEmploymentType());//dempgraphic screen
                customerDetails.setCurrentEemployer(trim(employment.getEmploymentName(), 20));//this field map if ApplicanT Catogory is salaried
                //Organization_Type  < Map to master (GOVERNMENT / PSU) < applicant.employment[0].employerType
                customerDetails.setOrganizationType(MiFinHelper.getOrganizationType(employment.getEmployerType()));
                /*// Not capturing
                customerDetails.setIndustryType(employment.getIndustryType());
                // No field available in system
                customerDetails.setIndustryCode();
                // PROFESSION_Code - not capturing
                customerDetils.setProfessionCode();
                */
            }
            if (applicant.getFatherName() != null) {
                customerDetails.setFatherFName(GngUtils.setValueForNull(trim(applicant.getFatherName().getFirstName(), 35)));
                customerDetails.setFatherLName(GngUtils.setValueForNull(trim(applicant.getFatherName().getLastName(), 35)));
                customerDetails.setFatherMName(GngUtils.setValueForNull(trim(applicant.getFatherName().getMiddleName(), 35)));
            }
            if (applicant.getSpouseName() != null) {
                customerDetails.setSpouseFName(GngUtils.setValueForNull(trim(applicant.getSpouseName().getFirstName(), 35)));
                customerDetails.setSpouseMName(GngUtils.setValueForNull(trim(applicant.getSpouseName().getMiddleName(), 35)));
                customerDetails.setSpouseLName(GngUtils.setValueForNull(trim(applicant.getSpouseName().getLastName(), 35)));
            }
            if (applicant.getMotherName() != null) {
                customerDetails.setMotherMaidenName(GngUtils.setValueForNull(trim(applicant.getMotherName().getFirstName(), 35)));

            }
            //As gstNo is not present for Individual
            customerDetails.setGstin(Constant.ZERO);
        } else { // CORPORATE
            // date of incorporation
            if (StringUtils.isNotEmpty(applicant.getGstNo())) {
                customerDetails.setGstin(applicant.getGstNo());
            } else {
                customerDetails.setGstin(Constant.ZERO);
            }
            if (applicant.getApplicantName().getFirstName() != null) {
                customerDetails.setFname(applicant.getApplicantName().getFirstName().toUpperCase());
            }
            if (employment != null) {
                Date parsed = GngDateUtil.getDateFromSpecificFormat(GngDateUtil.ddMMyyyy,
                        employment.getDateOfJoining());
                String dob = GngDateUtil.changeDateFormat(parsed, GngDateUtil.yyyy_MM_dd);
                customerDetails.setDob(GngUtils.setValueForNull(dob));
            }
        }

        if (applicant.getApplicantName() != null) {
            customerDetails.setFname(GngUtils.setValueForNull(trim(applicant.getApplicantName().getFirstName(), 200)).toUpperCase());
            customerDetails.setMname(GngUtils.setValueForNull(trim(applicant.getApplicantName().getMiddleName(), 35)).toUpperCase());
            customerDetails.setLname(GngUtils.setValueForNull(trim(applicant.getApplicantName().getLastName(), 35)).toUpperCase());
        }

        String NoOfDependents = String.valueOf(applicant.getNoOfDependents());
        customerDetails.setNoOfDependents(NoOfDependents == null ? NoOfDependents : Constant.ZERO);
        customerDetails.setTanNo(GngUtils.setValueForNull(applicant.getTanNo()));

        if (employment != null) {
            if (!StringUtils.isEmpty(employment.getEmploymentType())) {
                customerDetails.setOccupationType(GngUtils.setValueForNull(MiFinHelper.OCCUPATION_TYPE.get(employment.getEmploymentType())));

                /*if(StringUtils.equalsIgnoreCase(EmploymentType.SALARIED, employment.getEmploymentType())){
                    customerDetails.setGrossIncome(setAmount(employment.getMonthlySalary()));
                   *//*TODO after field confirm by Mifin *//*
                    // customerDetails.setNetIncome(setAmount(employment.getMonthlySalary()));
                }else if (StringUtils.equalsIgnoreCase(EmploymentType.SEP, employment.getEmploymentType())){
                    customerDetails.setGrossIncome(setAmount(employment.getItrAmount()));
                    *//*TODO after field confirm by Mifin *//*
                    // customerDetails.setNetIncome(setAmount(employment.getItrAmount()));
                }*/
            } else {
                if (StringUtils.equalsIgnoreCase(customerDetails.getEntityType(), ApplicantType.CORPORATE.value())) {
                    customerDetails.setOccupationType(GngUtils.setValueForNull(MiFinHelper.OCCUPATION_TYPE.get(
                            ApplicantType.CORPORATE.value())));
                } else {
                    // This should not be reached
                    logger.info("Customer occupation type set to BLANK !! Check data.");
                    customerDetails.setOccupationType(Constant.BLANK);
                }

                customerDetails.setGrossIncome(setAmount(employment.getItrAmount()));
                customerDetails.setNetIncome(setAmount(employment.getItrAmount()));
            }
        }
        customerDetails.setOtherEntity(Constant.BLANK);
        customerDetails.setIndustryCode(Constant.ZERO);
        customerDetails.setProfesstionCode(Constant.ZERO);
        setKycDetails(customerDetails, applicant);

        return customerDetails;
    }

    private CustomerDetails getDefaultCustomerDetails() {

        CustomerDetails customerDetails = new CustomerDetails();

        customerDetails.setGender(Constant.BLANK);
        customerDetails.setMaritalStatus(Constant.BLANK);
        customerDetails.setQualification(Constant.BLANK);
        customerDetails.setFname(Constant.BLANK); //purposely dont change.
        customerDetails.setMname(Constant.BLANK);
        customerDetails.setLname(Constant.BLANK);
        customerDetails.setFatherFName(Constant.BLANK);
        customerDetails.setFatherLName(Constant.BLANK);
        customerDetails.setFatherMName(Constant.BLANK);
        customerDetails.setSpouseFName(Constant.BLANK);
        customerDetails.setSpouseMName(Constant.BLANK);
        customerDetails.setSpouseLName(Constant.BLANK);
        customerDetails.setWorkExperience(Constant.BLANK);
        customerDetails.setRationCard(Constant.BLANK);
        customerDetails.setBankPassbook(Constant.BLANK);
        customerDetails.setCustomerCode(Constant.BLANK);
        customerDetails.setGstin(Constant.BLANK);
        customerDetails.setIndustryType(Constant.BLANK);
        customerDetails.setProfession(Constant.BLANK);
        customerDetails.setEmploymentType(Constant.BLANK);
        customerDetails.setDob(Constant.BLANK);

        return customerDetails;
    }

    private void setKycDetails(CustomerDetails customerDetails, Applicant applicant) {
        customerDetails.setPanNo(Constant.BLANK);
        customerDetails.setAadharNo(Constant.BLANK);
//        customerDetails.setAdharEnrolmentNo(Constant.BLANK);
        customerDetails.setDrivingLicense(Constant.BLANK);
        customerDetails.setDrivingLicenseExpDate(Constant.BLANK);
        customerDetails.setPassportNo(Constant.BLANK);
        customerDetails.setPassportExpDate(Constant.BLANK);
        customerDetails.setVoterId(Constant.BLANK);

        List<Kyc> kycList = applicant.getKyc();
        if (CollectionUtils.isNotEmpty(applicant.getKyc())) {
            for (Kyc kyc : kycList) {
                if (kyc != null) {
                    if (kyc.getKycName().equals(KYC_TYPES.PAN.name())) {
                        customerDetails.setPanNo(kyc.getKycNumber());
                    } else if (kyc.getKycName().equals(MiFinHelper.AADHAAR)) {
                        customerDetails.setAadharNo(kyc.getKycNumber().replace(
                                kyc.getKycNumber().substring(4, kyc.getKycNumber().length() - 4), "0000"));
                    } else if (kyc.getKycName().equals(KYC_TYPES.DRIVING_LICENSE.name())) {
                        customerDetails.setDrivingLicense(kyc.getKycNumber());
                        customerDetails.setDrivingLicenseExpDate(kyc.getExpiryDate());
                    } else if (kyc.getKycName().equals(KYC_TYPES.PASSPORT.name())) {
                        customerDetails.setPassportNo(kyc.getKycNumber());
                    } else if (kyc.getKycName().equals(KYC_TYPES.VOTER_ID.name())) {
                        customerDetails.setVoterId(kyc.getKycNumber());
                    }
                }
            }
        }
    }

    private List<AddressDetails> getAddressDetails(Applicant applicant) {
        List<AddressDetails> addressDetailsList = new ArrayList<>();
        boolean isIndividual = ApplicantType.isIndividual(applicant.getApplicantType());
        List<CustomerAddress> addressList = applicant.getAddress();
        for (CustomerAddress customerAddress : addressList) {
            if (customerAddress != null) {
                AddressDetails addressDetails = new AddressDetails();
                addressDetails.setAddressType(GngUtils.setValueForNull(MiFinHelper.ADDRESS_TYPE.get(customerAddress.getAddressType())));
                addressDetails.setAdd1(GngUtils.setValueForNull(trim(customerAddress.getAddressLine1(), 200)));
                addressDetails.setAdd2(GngUtils.setValueForNull(trim(customerAddress.getAddressLine2(), 200)));
                addressDetails.setCity(GngUtils.setValueForNull(customerAddress.getCity()));
                addressDetails.setDist(GngUtils.setValueForNull(customerAddress.getDistrict()));
                addressDetails.setLandmark(GngUtils.setValueForNull(trim(customerAddress.getLandMark(), 50)));
                addressDetails.setState(GngUtils.setValueForNull(customerAddress.getState()));
                addressDetails.setPincode(GngUtils.setValueForNull(String.valueOf(customerAddress.getPin())));
                addressDetails.setYearOfStay(GngUtils.setValueForNull(String.valueOf(customerAddress.getTimeAtAddress())));
                addressDetails.setMonthsOfStay(GngUtils.setValueForNull(String.valueOf(customerAddress.getTimeAtAddress() * 12)));
                if (CollectionUtils.isNotEmpty(applicant.getPhone() ))
                    setContactDetails(addressDetails, applicant);
                if (CollectionUtils.isNotEmpty(applicant.getEmail() ) ) {
                    setEmailDetails(addressDetails, applicant);
                }
                addressDetails.setOccupancyStatus(MiFinHelper.OTHERS);

                // If non-individual then OFFICE address is mailing address
                if(  isIndividual ) {
                    if( StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name(),
                            customerAddress.getAddressType()) ) {
                        addressDetails.setMailingAddress(GNGWorkflowConstant.YES.toFaceValue());
                    } else {
                        addressDetails.setMailingAddress(GNGWorkflowConstant.NO.toFaceValue());
                    }
                } else {
                    if( StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name(),
                            customerAddress.getAddressType()) ) {
                        addressDetails.setMailingAddress(GNGWorkflowConstant.YES.toFaceValue());
                    } else {
                        addressDetails.setMailingAddress(GNGWorkflowConstant.NO.toFaceValue());
                    }
                }

                //Following fields are not available currently,setting fields to blank
                addressDetails.setNameOfCompany(Constant.BLANK);
                addressDetails.setAdd3(trim(Constant.BLANK, 50));

                addressDetailsList.add(addressDetails);
            }
        }

        return addressDetailsList;
    }

    private void setEmailDetails(AddressDetails addressDetails, Applicant applicant) {
        List<Email> emailList = applicant.getEmail();
        if(emailList.size() > 0) {
            for(Email email : emailList) {
                if(email != null) {
                    if(email.getEmailType().equals("PERSONAL") && StringUtils.isNotEmpty(email.getEmailAddress())){
                        addressDetails.setEmail(GngUtils.setValueForNull(email.getEmailAddress()));
                    }else {
                        addressDetails.setEmail(Constant.BLANK);
                    }
                }
            }
        }
    }

    private void setContactDetails(AddressDetails addressDetails, Applicant applicant) {
        List<Phone> phoneList = applicant.getPhone();
        if(phoneList.size() > 0) {
            for(Phone phone : phoneList) {
                if(phone != null) {
                   if(phone.getPhoneType().equals("PERSONAL_MOBILE") && StringUtils.isNotEmpty(phone.getPhoneNumber())){
                       addressDetails.setPhone(GngUtils.setValueForNull(phone.getPhoneNumber()));
                       addressDetails.setPhoneStdCode(GngUtils.setValueForNull(phone.getAreaCode()));
                       addressDetails.setMobile(GngUtils.setValueForNull(phone.getPhoneNumber()));
                   }
                }
            }
        }
    }

    private Charges getChargeDetails(ApplicationRequest applicationRequest, LoanCharges loanCharges) {
        Charges charges = new Charges();
        List<ChargeDetails> chargeDetailsList = new ArrayList<>();
        ChargeDetails chargeDetails;
        Applicant applicant = applicationRequest.getRequest().getApplicant();
        List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant();
        double cersaiFee = 0 ;
        double gstAmt = 0;
        //cersaii charge
        if (loanCharges != null) {
            // Cersai fees
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_CERSAI);
            gstAmt = loanCharges.getCersaiFeeWithGST() - loanCharges.getCersaiFees();
            logger.info("{} refId gstAmt {} ",applicationRequest.getRefID(), gstAmt);
            chargeDetails.setChargeAmt(setAmount(loanCharges.getCersaiFees()));
            chargeDetails.setGstOnCharge(setAmount(gstAmt));
            chargeDetails.setTotalChargeAmt(setAmount(loanCharges.getCersaiFeeWithGST()));
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);

            chargeDetailsList.add(chargeDetails);
            if(applicationRequest.getHeader().getProduct() == Product.PL ||
                    applicationRequest.getHeader().getProduct() == Product.BL
                    || applicationRequest.getHeader().getProduct() == Product.DIGI_PL){
                double gst = 0;
                chargeDetails = new ChargeDetails();
                if(!loanCharges.isAdditionalCharges()){
                     gst = loanCharges.getAdvEMIWithGST() - loanCharges.getAdvanceEMI();
                    chargeDetails.setChargeAmt(setAmount(loanCharges.getAdvanceEMI()));
                    chargeDetails.setTotalChargeAmt(setAmount(loanCharges.getAdvEMIWithGST()));
                }else{
                    chargeDetails.setChargeAmt(Constant.ZERO);
                    chargeDetails.setTotalChargeAmt(Constant.ZERO);
                }
              chargeDetails.setChargeId(MiFinHelper.ADDITIONAL_CHARGES);
              chargeDetails.setGstOnCharge(Double.toString(gst));
              chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
              chargeDetailsList.add(chargeDetails);
            }else{
                // Advance EMI
                chargeDetails = new ChargeDetails();
                chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_ADVANCE_EMI);
                chargeDetails.setChargeAmt(setAmount(loanCharges.getAdvanceEMI()));
                chargeDetails.setGstOnCharge(Constant.ZERO);
                chargeDetails.setTotalChargeAmt(chargeDetails.getChargeAmt());
                chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
                chargeDetailsList.add(chargeDetails);
            }
            // Pre EMI
            /*If   <ADVANCE_EMI_FLAG>N</ADVANCE_EMI_FLAG>  is N then
                    <NO_ADVANCE_INSTALMENT>1</NO_ADVANCE_INSTALMENT> should be  0
                    and <CHARGEID>Pre Emi</CHARGEID>  should be 0
            PreEmi is used only in Leviosa to compute disbursal amt,
            and this amt is not to be sent to Mifin.
            Based on other parameters Mifin calculates pre-emi and disbursal
            and cross-checks the value of disbursal amt sent by leviosa.
            */
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_PRE_EMI);
            chargeDetails.setChargeAmt(Constant.ZERO);
            chargeDetails.setGstOnCharge(Constant.ZERO);
            chargeDetails.setTotalChargeAmt(Constant.ZERO);
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
            chargeDetailsList.add(chargeDetails);
            // Insurance
            List<InsurenceCat> insuranceList = loanCharges.getInsurenceCatList();
            if (CollectionUtils.isNotEmpty(insuranceList)) {
                for (InsurenceCat temp : insuranceList) {
                    if (StringUtils.isNotEmpty(temp.getCompanyName()) && temp.getAmount() != null) {
                        chargeDetails = new ChargeDetails();
                        chargeDetails.setChargeId(temp.getCompanyName());
                        chargeDetails.setChargeAmt(setAmount(temp.getAmount()));
                        chargeDetails.setGstOnCharge(Constant.ZERO);
                        chargeDetails.setTotalChargeAmt(chargeDetails.getChargeAmt());
                        chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
                        chargeDetailsList.add(chargeDetails);
                    }
                }
            }

            // IMD
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_IMD);
            chargeDetails.setChargeAmt(setAmount(loanCharges.getImdCollectedAmt()));
            chargeDetails.setGstOnCharge(Constant.ZERO);
            chargeDetails.setTotalChargeAmt(chargeDetails.getChargeAmt());
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
            chargeDetailsList.add(chargeDetails);

            if(applicationRequest.getHeader().getProduct() == Product.LAP) {
                chargeDetails = new ChargeDetails();
                chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_IMD2);
                double gstChargeAmt = loanCharges.getImd2gstAmt() - loanCharges.getImd2Amount();
                chargeDetails.setChargeAmt(setAmount(loanCharges.getImd2Amount()));
                chargeDetails.setGstOnCharge(setAmount(gstChargeAmt));
                chargeDetails.setTotalChargeAmt(setAmount(loanCharges.getImd2gstAmt()));
                chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
                chargeDetailsList.add(chargeDetails);
            }
            // Processing fees
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_PROCESSING);
            chargeDetails.setChargeAmt(setAmount(loanCharges.getProFeesAmt()));
            chargeDetails.setGstOnCharge(setAmount(loanCharges.getGstAmt()));
            chargeDetails.setTotalChargeAmt(setAmount(loanCharges.getProFeesAmt() + loanCharges.getGstAmt()));
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
            chargeDetailsList.add(chargeDetails);

            //creditVidya Charges.
            //checking coapplicant are present or not
            if (CollectionUtils.isNotEmpty(coApplicants)) {
                Applicant withConsentToCV = coApplicants.stream()
                        .filter(coApplicant -> coApplicant.isCreditVidyaFlag() == true).findAny().orElse(null); // returns coApplicants with creditVidya consent true.
            }
            //if(applicant.isCreditVidyaFlag() || withConsentToCV != null){
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_CREDITVIDYA);
            chargeDetails.setChargeAmt(setAmount(loanCharges.getCreditVidyaCharge()));
            chargeDetails.setGstOnCharge(Constant.ZERO);
            chargeDetails.setTotalChargeAmt(chargeDetails.getChargeAmt());
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
            chargeDetailsList.add(chargeDetails);


            //Existing Loan Closure charges will pass only in case of TopUpGross
            if(null !=applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails()) {
                String scheme = applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme();
                if (StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_TOPUP_GROSS)) {
                    chargeDetails = new ChargeDetails();
                    chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_EXISTINAMT);
                    chargeDetails.setChargeAmt(setAmount(loanCharges.getLoanDetails().getExistingLoanAmt()));
                    chargeDetails.setGstOnCharge(Constant.ZERO);
                    chargeDetails.setTotalChargeAmt(chargeDetails.getChargeAmt());
                    chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
                    chargeDetailsList.add(chargeDetails);
                }
            }
            //}
        } else {
            chargeDetails = new ChargeDetails();
            chargeDetails.setChargeId(MiFinHelper.CHARGE_FEES_AMT_IMD);
            chargeDetails.setChargeAmt(Constant.ZERO);
            chargeDetailsList.add(chargeDetails);
            chargeDetails.setGstOnCharge(Constant.ZERO);
            chargeDetails.setTotalChargeAmt(Constant.ZERO);
            chargeDetails.setChargeAppliedOn(Constant.ROLLBACKSTATUS);
        }
        if(applicationRequest.getRequest().getApplication().isCoOrigination()){
            miFinHelper.setChargeDetailsForOrigination(chargeDetailsList);
        }
        charges.setChargeDetailsList(chargeDetailsList);
        return charges;
    }

    private Bank getBankDetails(ApplicationRequest applicationRequest, Repayment repayment) {
        Bank bank = new Bank();
        List<BankDetails> bankDetailsList = new ArrayList<>();
        BankDetails bankDetails;
        if (repayment != null && CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())) {
            for (RepaymentDetails repaymentDetails : repayment.getRepaymentDetailsList()) {
                bankDetails = new BankDetails();
                bankDetailsList.add(bankDetails);
                bankDetails.setMicr(GngUtils.setValueForNull(repaymentDetails.getmICRCode()));
                //Setting MICR null only for PL
                if (StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(),Product.PL.toProductName())
                        || StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName())) {
                    bankDetails.setMicr(null);
                }
                bankDetails.setIfscCode(GngUtils.setValueForNull(repaymentDetails.getIfscCode()));
                //fix length to 20 chars.
                bankDetails.setBankId(GngUtils.setValueForNull(StringUtils.substring(StringUtils.trim(repaymentDetails.getBankName()), 0, 20)));
                bankDetails.setBranchId(GngUtils.setValueForNull(repaymentDetails.getBranchName()));
                if (repaymentDetails.getBranchaddress() != null)
                    bankDetails.setBranchCityId(GngUtils.setValueForNull(repaymentDetails.getBranchaddress().getCity()));
                bankDetails.setAcNo(repaymentDetails.getAccountNumber());
                if (repaymentDetails.getAccountHolderName() != null) {
                    bankDetails.setBeneficeryName(GngUtils.setValueForNull(trim(GngUtils.convertNameToFullName(repaymentDetails.getAccountHolderName()), 100)));
                }
                bankDetails.setBankType(GngUtils.setValueForNull(repaymentDetails.getBankingType()));
                bankDetails.setAccountType(GngUtils.setValueForNull(MiFinHelper.BANK_ACCT_TYPE.get(repaymentDetails.getAccountType())));
                // TODO bank acct period - we are not capturing
                bankDetails.setNoOfYear(repaymentDetails.getYearHeld());
                bankDetails.setBankType(Constant.BLANK);
            }
        }

        bank.setBankDetailsList(bankDetailsList);
        return bank;
    }

    private CreditSanction getCreditSanction(ApplicationRequest applicationRequest, CamDetails camDetails,
                                             LoanCharges loanCharges, EligibilityDetails eligibility, CoOrigination origination) {
        CreditSanction creditSanction = new CreditSanction();
        Repayment repayment = getRepaymentDetails(applicationRequest);
        Product product = applicationRequest.getHeader().getProduct();
        //Product
        creditSanction.setProductId("SBL"); //only this product is configured at MiFin.
        //SchemeId
        if (product == Product.PL || (product == Product.BL && StringUtils.equalsIgnoreCase("Normal",
                applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme())) ) {
            creditSanction.setSchemeId(product.toString());
        } else if (applicationRequest != null && applicationRequest.getRequest() != null &&
                applicationRequest.getRequest().getApplicant() != null &&
                applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails() != null) {
            /*********** To convert Leviosa value in to mifin Value ***********/
            if(applicationRequest.getRequest().getApplication().isCoOrigination()){
                creditSanction.setSchemeId(
                        miFinHelper.setSchemeWiseMaster(
                        GngUtils.setValueForNull(MiFinHelper.SCHEME.get(
                        applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme())),
                        applicationRequest.getRequest().getApplication().getSchemeName()));
            }else{
                creditSanction.setSchemeId(GngUtils.setValueForNull(MiFinHelper.SCHEME.get(
                applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme())));
            }
        }

        //Applied loan Amount
        if (applicationRequest.getRequest().getApplication() != null) {
            creditSanction.setAppliedLoanAmt(
                    setAmount(applicationRequest.getRequest().getApplication().getLoanAmount()));
        } else {
            creditSanction.setAppliedLoanAmt(Constant.ZERO);
        }

        //sanctioned laon Amount
        if (loanCharges != null) {
            LoanAmtDetails loanAmtDetails = loanCharges.getLoanDetails();
            TenureDetails tenureDetails = loanCharges.getTenureDetails();
            InterestDetails interestDetails = loanCharges.getInterestDetails();

            creditSanction.setEmiStartDate(GngUtils.setValueForNull(GngDateUtil.getYYYYMMDDFormat(loanCharges.getEmiStartDate())));
            creditSanction.setDisbDate(GngUtils.setValueForNull(GngDateUtil.getYYYYMMDDFormat(loanCharges.getDisbursedDate())));
                /*If   <ADVANCE_EMI_FLAG>N</ADVANCE_EMI_FLAG>  is N
                    then <NO_ADVANCE_INSTALMENT>1</NO_ADVANCE_INSTALMENT> should be  0
                        and <CHARGEID>Pre Emi</CHARGEID>  should be 0*/
            creditSanction.setAdvEmiFlag(GNGWorkflowConstant.NO.toFaceValue());
            creditSanction.setNoOfAdvInstallments(Constant.ZERO);

            creditSanction.setInterestType(GngUtils.setValueNA(loanCharges.getTypeOfROI()));
            // Tenor
            if (tenureDetails != null) {
                creditSanction.setTenor(Integer.toString(tenureDetails.getDisbTenureMonths()));
            } else {
                creditSanction.setTenor(Constant.ZERO);
            }
            // roi
            if (interestDetails != null) {
                creditSanction.setRoi(GngUtils.setValueForNull(Double.toString(interestDetails.getDisbInterestRate())));
            }
            if (StringUtils.equalsIgnoreCase(creditSanction.getRoi(), Constant.BLANK)) {
                creditSanction.setRoi(Constant.ZERO);
            }
            if (loanAmtDetails != null) {
                creditSanction.setSanctionedAmt(setAmount(loanAmtDetails.getFinalDisbAmt()));
            } else {
                logger.info("Check loanCharges data!!");
                creditSanction.setSanctionedAmt(Constant.ZERO);
            }

            if (camDetails != null && camDetails.getSummary() != null) {
                creditSanction.setSanctionAprvBy(GngUtils.setValueForNull(camDetails.getSummary().getFinalLoanApprovedBy()));
                // installment
                creditSanction.setInstallmentType("EMI");
                // frequency
                creditSanction.setFrequency(GngUtils.setValueForNull(MiFinHelper.FREQUENCY.get(camDetails.getSummary().getInstallmentType())));
            }
        }

        if (eligibility != null) {         //setting DBR and LTV to two decimals
            creditSanction.setDbr(GngUtils.setValueForNull(Double.toString(setNoOfDecimals(eligibility.getFoir(), MiFinHelper.TWO_DECIMALS))));
            if (eligibility.getFoirDetail() != null && eligibility.getFoirDetail().getLtv() != 0) {
                creditSanction.setLtv(GngUtils.setValueForNull(Double.toString(setNoOfDecimals(eligibility.getFoirDetail().getLtv(),MiFinHelper.TWO_DECIMALS))));
            } else {
                creditSanction.setLtv(Constant.SEVENTY);

            }
        } else {
            //This shouldn't be reached.
            // Setting default values for DBR and LTV
            creditSanction.setDbr(Constant.ZERO);
            creditSanction.setLtv(Constant.SEVENTY);

            logger.info("Eligibility details are blank for refId {} !! Please check data", applicationRequest.getRefID());
        }
        // LTV
        if (StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(),
                Product.PL.toProductName())) {
            creditSanction.setLtv(Constant.ONE);
        }
        if (repayment != null) {
            creditSanction.setRepaymentMode(MiFinHelper.REPAYMENT_MODE.get(repayment.getRepaymentDetailsList().get(0).getRepaymentType()));
        }

        if(product == Product.DIGI_PL){
            creditSanction.setSchemeId(Product.DIGI_PL.toDisplayName());
            creditSanction.setRepaymentMode(MiFinHelper.REPAYMENT_MODE.get("ACH"));
            creditSanction.setFrequency(GngUtils.setValueForNull(MiFinHelper.FREQUENCY.get("Monthly")));

            if(StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DE.toFaceValue(), applicationRequest.getCurrentStageId())) {
                creditSanction.setTrancheApplicable("Y");
                if(loanCharges != null && loanCharges.getLoanDetails() != null ) {
                    creditSanction.setSanctionedAmt(setAmount(loanCharges.getLoanDetails().getFinalLoanApprovedAmt()));
                    creditSanction.setTranchAmount(setAmount(loanCharges.getLoanDetails().getFinalDisbAmt()));
                }
            }else {
                creditSanction.setTrancheApplicable("N");
                if(loanCharges != null && loanCharges.getLoanDetails() != null ) {
                    creditSanction.setSanctionedAmt(setAmount(loanCharges.getLoanDetails().getFinalDisbAmt()));
                    creditSanction.setTranchAmount(creditSanction.getSanctionedAmt());
                }
            }
            applicationRequest.getRequest().getApplication().setTranchAmount(creditSanction.getTranchAmount());

            if(eligibility != null)
                creditSanction.setAppliedLoanAmt(setAmount(eligibility.getElgLnAmt()));
            else
                creditSanction.setAppliedLoanAmt(Constant.ZERO);
        }

        if(product == Product.PL && StringUtils.equalsIgnoreCase(applicationRequest.getApplicantType(), ApplicantType.EXISTING.value())){
            creditSanction.setSchemeId("PL TOPUP");
        }
        //Co-origination
        creditSanction.setPromationDesc(Constant.ZERO);
        if(applicationRequest.getRequest().getApplication().isCoOrigination() &&
                origination != null && CollectionUtils.isNotEmpty(origination.getCoOriginationDetailsList())){
                List<CoOriginationDetails> originationDetailsList = origination.getCoOriginationDetailsList();
                for( CoOriginationDetails originationDetail : originationDetailsList){
                    if(StringUtils.equalsIgnoreCase("ICICI", originationDetail.getCompanyName())){
                        creditSanction.setRoiOfPartnerBank(GngUtils.setValueForNull(Double.toString(loanCharges.getInterestDetails().getDisbInterestRate())));
                        creditSanction.setSanctionedAmountPartnerBank(GngUtils.setValueForZero(Double.toString(originationDetail.getAppLoan())));
                        if(origination.getRoiMasterdata() != null){
                            creditSanction.setPartnerBankRatio(GngUtils.setValueForZero(Double.toString(origination.getRoiMasterdata().getExternalPercentage())));
                        }
                    }else if(StringUtils.equalsIgnoreCase("SBFC",originationDetail.getCompanyName())){
                        creditSanction.setSanctionedAmountSbfc(GngUtils.setValueForBlank(Double.toString(originationDetail.getAppLoan())));
                        if(origination.getRoiMasterdata() != null){
                            creditSanction.setSbfcRatio(GngUtils.setValueForZero(Double.toString(origination.getRoiMasterdata().getInternalPercentage())));
                        }
                    }
                }
        } else {
            creditSanction.setRoiOfPartnerBank(Constant.ZERO);
            creditSanction.setSanctionedAmountPartnerBank(Constant.ZERO);
            creditSanction.setPartnerBankRatio(Constant.ZERO);
            creditSanction.setSanctionedAmountSbfc(Constant.ZERO);
            creditSanction.setSbfcRatio(Constant.ZERO);
            creditSanction.setSchemeIdOfPaertnerBank(Constant.ZERO);
        }
        creditSanction.setSchemeIdOfPaertnerBank(Constant.ZERO);
        creditSanction.setPromationDesc(Constant.BLANK);

        if(camDetails.getSummary().isPsl()){
            creditSanction.setPslFlag(Constant.BLOCKSTATUS);
        }else{
            creditSanction.setPslFlag(Constant.ROLLBACKSTATUS);
        }
        creditSanction.setPslCode(Constant.ZERO);
        return creditSanction;
    }

    private String toIntegerString(String s) {
        // This is needed since the value is copied from some other field which are double.
        Double dd = new Double(s);
        int i = (int) dd.doubleValue();
        return String.valueOf(i);
    }


    private BasicInfo getBasicInfo(ApplicationRequest applicationRequest, CamDetails camDetails,CoOrigination origination) {
        BasicInfo basicInfo = new BasicInfo();

        if (applicationRequest != null) {
            if (applicationRequest.getAppMetaData() != null) {
                if (applicationRequest.getAppMetaData().getBranchV2() != null) {
                    if(applicationRequest.getAggregatorData() != null && StringUtils.isNotEmpty(applicationRequest.getAggregatorData().getLoginRole())){
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
                    }else{
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
                    }
                    if (StringUtils.equalsIgnoreCase("Spoke", applicationRequest.getAppMetaData().getBranchV2().getBranchType()) ||
                            StringUtils.equalsIgnoreCase("LoanStation", applicationRequest.getAppMetaData().getBranchV2().getBranchType())){
                        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getHubBranchName()));
                    }
                    basicInfo.setRelationshipManager(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchManagerName()));
                }
                if (applicationRequest.getAppMetaData().getDsaName() != null) {
                    /*basicInfo.setSourcingChannelName(setValueForNull(applicationRequest.getAppMetaData().getDsaName().getFirstName()));
                    basicInfo.setFullfillmentChannelName(setValueForNull(applicationRequest.getAppMetaData().getDsaName().getFirstName()));*/
                    basicInfo.setSourcingChannelName("SBFC"); //TODO dsaId from header.
                    basicInfo.setFullfillmentChannelName("SBFC");
                }
            }
            if (applicationRequest.getRequest() != null && applicationRequest.getRequest().getApplication() != null)
                basicInfo.setPurposeOfLoan(GngUtils.setValueForNull(MiFinHelper.LOANPURPOSE.get(applicationRequest.getRequest().getApplication().getLoanPurpose())));

            //To append value as -C in case of co_origination
            if(applicationRequest.getRequest().getApplication().isCoOrigination()){
                basicInfo.setAppFormNo(applicationRequest.getRefID().concat(Constant.CO_ORIGINATION));
            }else{
                basicInfo.setAppFormNo(applicationRequest.getRefID());
            }
            basicInfo.setSourcingChannelType(applicationRequest.getRequest().getApplication().getChannel());
            basicInfo.setFullfillmentChannelType(applicationRequest.getRequest().getApplication().getChannel());
        }
        // TODO fill Topup info
        // How to identify  the loan is topup - by scheme
        if(null != applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails()) {
            String scheme = applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme();
            if (applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value())
                    && (StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_TOPUP_GROSS) ||
                    StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_TOPUP_PARALLEL)
                    || StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_BT_TOPUP))
                    ) {
                basicInfo.setLoanTopupType(MiFinHelper.LOAN_TOPUP_TYPE.get(scheme));
                if (applicationRequest.getRequest().getTopupInfo() != null &&
                        CollectionUtils.isNotEmpty(applicationRequest.getRequest().getTopupInfo().getMiFinOldProspectCode())) {
                    basicInfo.setOldProspectCode(applicationRequest.getRequest().getTopupInfo().getMiFinOldProspectCode());
                }
            } else {
                basicInfo.setOldProspectCode(new ArrayList<>());
            }
            // TODO fill Topup info
            // How to identify  the loan is topup - by scheme
            scheme = applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme();
            if (applicationRequest.getApplicantType().equals(ApplicantType.EXISTING.value()) &&
                    (StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_TOPUP_GROSS) ||
                            StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_TOPUP_PARALLEL)
                            || StringUtils.equalsIgnoreCase(scheme, LoanConstants.LOAN_BT_TOPUP))) {
                basicInfo.setLoanTopupType(MiFinHelper.LOAN_TOPUP_TYPE.get(scheme));
                // TODO : Get mifin prospect code of old case from mifin log where reequest type = ... and flag ... is set
                //  in case of Parallel there could be more than 1 prospect codes; how to send those ??
                //basicInfo.setOldProspectCode();
            } else {
                basicInfo.setLoanTopupType(Constant.BLANK);
            }
        }

        if(applicationRequest.getRequest().getApplication().isCoOrigination()){
            basicInfo.setCompanyName(GngUtils.setValueForBlank(applicationRequest.getRequest().getApplication().getCompanyName()));
            basicInfo.setCooriginFlag(Constant.BLOCKSTATUS);
        }else{
            basicInfo.setCompanyName(Constant.BLANK);
            basicInfo.setCooriginFlag(Constant.ROLLBACKSTATUS);
        }
        if(applicationRequest.getHeader().getProduct() == Product.DIGI_PL){
            basicInfo.setLoanTopupType(Constant.BLANK);
            basicInfo.setSourcingChannelName("SBFC");
            basicInfo.setFullfillmentChannelName("SBFC");
        }
        basicInfo.setBranchCode(Constant.BLANK);
        basicInfo.setBranchCode(Constant.ZERO);
        basicInfo.setConstId(Constant.ZERO);
        return basicInfo;
    }

    private Valuation getValuationDetails(ApplicationRequest applicationRequest) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setRefId(applicationRequest.getRefID());
        valuationRequest.setHeader(applicationRequest.getHeader());
        return applicationRepository.fetchValuationDetails(valuationRequest);
    }

    private CamDetails getCamDetails(ApplicationRequest applicationRequest) {
        CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
        camDetailsRequest.setRefId(applicationRequest.getRefID());
        camDetailsRequest.setHeader(applicationRequest.getHeader());
        return applicationRepository.fetchCamDetails(camDetailsRequest);
    }

    private Repayment getRepaymentDetails(ApplicationRequest applicationRequest) {
        RepaymentRequest repaymentRequest = new RepaymentRequest();
        repaymentRequest.setRefId(applicationRequest.getRefID());
        repaymentRequest.setHeader(applicationRequest.getHeader());
        return applicationRepository.fetchRepaymentDetails(repaymentRequest);
    }

    private Verification getVerificationDetails() {
        Verification verification = new Verification();
        VerificationDetailMiFin verificationDetailMiFin = new VerificationDetailMiFin();
        List<VerificationDetailMiFin> verificationDetailMiFinList = new ArrayList<>();
        verificationDetailMiFinList.add(verificationDetailMiFin);
        return verification;
    }

    private Verification getVerificationDetails(ApplicationRequest applicationRequest) {

        Verification verification = new Verification();
        VerificationDetailMiFin verificationDetailMiFin = null;
        List<VerificationDetailMiFin> verificationDetailMiFinList = new ArrayList<>();
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();

        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, institutionId);
        if(null != verificationDetails) {
            List<PropertyVerification> residenceVerificationList = verificationDetails.getResidenceVerification();
            List<PropertyVerification> officeVerificationList = verificationDetails.getOfficeVerification();
            List<ItrVerification> itrVerificationList = verificationDetails.getItrVerification();
            List<BankingVerification> bankingVerificationList = verificationDetails.getBankingVerification();


            if (CollectionUtils.isNotEmpty(residenceVerificationList)) {
                for (PropertyVerification residenceVerification : residenceVerificationList
                        ) {
                    verificationDetailMiFin = new VerificationDetailMiFin();
                    verificationDetailMiFinList.add(verificationDetailMiFin);
                    verificationDetailMiFin.setVerificationStatus(residenceVerification.getStatus());
                    if (StringUtils.isNotEmpty(residenceVerification.getStatus()) &&
                            (StringUtils.equalsIgnoreCase(residenceVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_POSITIVE) ||
                                    (StringUtils.equalsIgnoreCase(residenceVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_NEGATIVE)))) {
                        verificationDetailMiFin.setVerificationResult(residenceVerification.getStatus());
                        verificationDetailMiFin.setVerificationStatus(Constant.BLANK);
                    } else {
                        verificationDetailMiFin.setVerificationStatus(residenceVerification.getStatus());
                        verificationDetailMiFin.setVerificationResult(Constant.BLANK);
                    }
                    verificationDetailMiFin.setTarget(MiFinHelper.APPLICANT);
                    verificationDetailMiFin.setVerificationPoint(MiFinHelper.VERIFICATION_POINT_RESIDENCE);
                    verificationDetailMiFin.setVerificationType(MiFinHelper.VERIFICATION_TYPE_RESIDENCE);
                    verificationDetailMiFin.setNoOfAttempts("1");
                }

            }
            if (CollectionUtils.isNotEmpty(officeVerificationList)) {
                for (PropertyVerification officeVerification : officeVerificationList
                        ) {
                    verificationDetailMiFin = new VerificationDetailMiFin();
                    verificationDetailMiFinList.add(verificationDetailMiFin);
                    verificationDetailMiFin.setNoOfAttempts("1");
                    verificationDetailMiFin.setVerificationStatus(officeVerification.getStatus());
                    verificationDetailMiFin.setTarget(MiFinHelper.APPLICANT);
                    if (StringUtils.isNotEmpty(officeVerification.getStatus()) &&
                            (StringUtils.equalsIgnoreCase(officeVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_POSITIVE) ||
                                    (StringUtils.equalsIgnoreCase(officeVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_NEGATIVE)))) {
                        verificationDetailMiFin.setVerificationResult(officeVerification.getStatus());
                        verificationDetailMiFin.setVerificationStatus(Constant.BLANK);
                    } else {
                        verificationDetailMiFin.setVerificationStatus(GngUtils.setValueForNull(officeVerification.getStatus()));
                        verificationDetailMiFin.setVerificationResult(Constant.BLANK);
                    }
                    verificationDetailMiFin.setVerificationPoint(MiFinHelper.VERIFICATION_POINT_OFFICE);
                    verificationDetailMiFin.setVerificationType(MiFinHelper.VERIFICATION_TYPE_OFFICE);
                }
            }//ends here

            if (CollectionUtils.isNotEmpty(itrVerificationList)) {
                for (ItrVerification itrVerification : itrVerificationList
                        ) {
                    verificationDetailMiFin = new VerificationDetailMiFin();
                    verificationDetailMiFinList.add(verificationDetailMiFin);
                    verificationDetailMiFin.setNoOfAttempts("1");
                    verificationDetailMiFin.setVerificationStatus(itrVerification.getStatus());
                    if (StringUtils.isNotEmpty(itrVerification.getStatus()) &&
                            (StringUtils.equalsIgnoreCase(itrVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_POSITIVE) ||
                                    (StringUtils.equalsIgnoreCase(itrVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_NEGATIVE)))) {
                        verificationDetailMiFin.setVerificationResult(itrVerification.getStatus());
                        verificationDetailMiFin.setVerificationStatus(Constant.BLANK);
                    } else {
                        verificationDetailMiFin.setVerificationResult(Constant.BLANK);
                        verificationDetailMiFin.setVerificationStatus(itrVerification.getStatus());
                    }
                    verificationDetailMiFin.setVerificationPoint(MiFinHelper.VERIFICATION_TYPE_ITR);
                    verificationDetailMiFin.setVerificationType(MiFinHelper.VERIFICATION_TYPE_ITR);

                }
            }

            if (CollectionUtils.isNotEmpty(bankingVerificationList)) {
                for (BankingVerification bankingVerification : bankingVerificationList
                        ) {
                    verificationDetailMiFin = new VerificationDetailMiFin();
                    verificationDetailMiFinList.add(verificationDetailMiFin);
                    verificationDetailMiFin.setNoOfAttempts("1");
                    verificationDetailMiFin.setTarget(MiFinHelper.APPLICANT);
                    verificationDetailMiFin.setVerificationPoint(MiFinHelper.VERIFICATION_TYPE_BANK_STMT);
                    // if status is positive or negative then it set value for this
                    if (StringUtils.isNotEmpty(bankingVerification.getStatus()) &&
                            (StringUtils.equalsIgnoreCase(bankingVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_POSITIVE) ||
                                    (StringUtils.equalsIgnoreCase(bankingVerification.getStatus(), MiFinHelper.VERIFICATION_RESULT_NEGATIVE)))) {
                        verificationDetailMiFin.setVerificationResult(bankingVerification.getStatus());
                        verificationDetailMiFin.setVerificationStatus(Constant.BLANK);
                    } else {
                        verificationDetailMiFin.setVerificationStatus(bankingVerification.getStatus());
                        verificationDetailMiFin.setVerificationResult(Constant.BLANK);
                    }
                    verificationDetailMiFin.setVerificationType(MiFinHelper.VERIFICATION_TYPE_BANK_STMT);
                }
            }
        }

        // TODO : Legal verification ??
        verification.setVerificationDetailsList(verificationDetailMiFinList);
        return verification;
    }


    private String setAmount(double amount){
        BigDecimal bigDecimal = BigDecimal.valueOf(amount);

        return bigDecimal.toPlainString() != null ? Long.toString( Math.round(bigDecimal.doubleValue()) ): Constant.ZERO;
    }
    private String setAmount(String amount){
        return StringUtils.isEmpty(amount) ? Constant.ZERO : amount;
    }
    private double setNoOfDecimals(double d,String noOfDecimals){
        DecimalFormat df = new DecimalFormat(noOfDecimals);
        if(d<0){
            df.setRoundingMode(RoundingMode.FLOOR);
        }
        df.setRoundingMode(RoundingMode.CEILING);
        double result = new Double(df.format(d));
        return result;
    }
    private LegalVerification  fetchLegalVerification(String refId, String instId) {
        LegalVerificationRequest legalVerificationRequest = new LegalVerificationRequest();
        legalVerificationRequest.setHeader(new Header());
        legalVerificationRequest.setRefId(refId);
        legalVerificationRequest.getHeader().setInstitutionId(instId);
        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
        if(legalVerification !=null && legalVerification.getLegalVerificationDetailsList()==null) {
            LegalVerificationDetails legalVerificatioDetailsByRefId = applicationRepository.fetchOldLegalVerificatioDetailsByRefId(legalVerificationRequest);
            if (legalVerificatioDetailsByRefId != null) {
                legalVerificatioDetailsByRefId.setCollateralId("0");
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                legalVerificationDetailsList.add(legalVerificatioDetailsByRefId);
                legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            }
        }
        return legalVerification;
    }


    @Override
    public TopUpDedupeResponse sendDataToMIFINApplicantDetail(TopUpDedupeRequest topUpDedupeRequest, String refId)
            {
        TopUpDedupeResponse topUpDedupeMifinResponse = null;
        try{
        ActivityLogs activityLog = AuditHelper.createActivityLogForTopUp(topUpDedupeRequest,
                GNGWorkflowConstant.MIFIN_DEDUPE_CHECk_CALL.toFaceValue(),
                GNGWorkflowConstant.MIFIN_DEDUPE_CHECk_CALL.toFaceValue(),
                GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String urlType = UrlType.TOP_UP_DEDUPE.toValue();
        WFJobCommDomain mifinApplicantDataConfig = getServiceConfiguration(topUpDedupeRequest.getHeader().getInstitutionId(), urlType);

        String url = Arrays.asList(mifinApplicantDataConfig.getBaseUrl(), mifinApplicantDataConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

        // create mifin dedupecheck  request  object
        TopUpMifinDedupeRequest miFinRequest = buildMiFinApplicantLoanDetailRequest(topUpDedupeRequest);
        MifinCallRequest mifinCallRequest = getMifinCallRequest(miFinRequest, EXISTINGLOANDETAILS, mifinApplicantDataConfig);

        MifinTopUpResponse mifinTopUpresponse = null;

        logger.debug("MiFin request for  EXISTINGLOANDETAILSrefid {} : {}", miFinRequest);

        logger.info("request after String {}", mifinCallRequest);

        List<TopUpDedupeResponse> topUpDedupeMifinResponseList = new ArrayList<TopUpDedupeResponse>();
        MifinReciveResponse mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                url, MifinReciveResponse.class);
        if(mifinReciveResponse != null){
            topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
        }
        logger.info("MiFin response for applicant details : {} for refId: {}", topUpDedupeMifinResponse);
        //to save mifin reponse
        topUpDedupeMifinResponseList.add(topUpDedupeMifinResponse);
        checkData(topUpDedupeMifinResponseList, refId, EXISTINGLOANDETAILS, miFinRequest, urlType, null);
        } catch (Exception e){
        logger.error("Error in topUp API{}",e.getStackTrace());
        }
        return topUpDedupeMifinResponse;
    }


    // for mifn build request
    private TopUpMifinDedupeRequest buildMiFinApplicantLoanDetailRequest(TopUpDedupeRequest topUpDedupeRequest) {
        TopUpMifinDedupeRequest miFinRequest = TopUpMifinDedupeRequest.builder().build();

        com.softcell.gonogo.model.mifin.topup.BasicInfo basicInfo = com.softcell.gonogo.model.mifin.topup.BasicInfo.builder().build();
        if (topUpDedupeRequest != null) {
            /*basicInfo.setProspectCode("PR00000001");
            basicInfo.setApplicantId("AP00000001");*/
            if (StringUtils.equalsIgnoreCase(topUpDedupeRequest.getFieldName(), "Loan Account No")) {
                basicInfo.setProspectCode(topUpDedupeRequest.getFieldValue());
                basicInfo.setApplicantId(Constant.BLANK);
                basicInfo.setMobNo(Constant.BLANK);
                basicInfo.setPan(Constant.BLANK);
            }
            if (StringUtils.equalsIgnoreCase(topUpDedupeRequest.getFieldName(), "Customer ID")) {
                basicInfo.setApplicantId(topUpDedupeRequest.getFieldValue());
                basicInfo.setMobNo(Constant.BLANK);
                basicInfo.setPan(Constant.BLANK);
                basicInfo.setProspectCode(Constant.BLANK);
            }
            if (StringUtils.equalsIgnoreCase(topUpDedupeRequest.getFieldName(), "Mobile NO")) {
                basicInfo.setMobNo(topUpDedupeRequest.getFieldValue());
                basicInfo.setPan(Constant.BLANK);
                basicInfo.setProspectCode(Constant.BLANK);
                basicInfo.setApplicantId(Constant.BLANK);
            }
            if (StringUtils.equalsIgnoreCase(topUpDedupeRequest.getFieldName(), "PAN")) {
                basicInfo.setPan(topUpDedupeRequest.getFieldValue());
                basicInfo.setProspectCode(Constant.BLANK);
                basicInfo.setApplicantId(Constant.BLANK);
                basicInfo.setMobNo(Constant.BLANK);
            }
            miFinRequest.setBasicInfo(basicInfo);
        }

        return miFinRequest;
    }


    private Authentication getAuthenticationDetails(WFJobCommDomain wfConfig, String requestType) {
        //ApplicantDetails
        Authentication authentication = new Authentication();
        if (wfConfig.getKeys() != null) {
            List<HeaderKey> keyList = wfConfig.getKeys();
            for (HeaderKey headerKey : keyList) {
                if (StringUtils.equalsIgnoreCase("APP_NAME", headerKey.getKeyName())) {
                    authentication.setAppName(headerKey.getKeyValue());
                }
                if (StringUtils.equalsIgnoreCase(headerKey.getKeyName(), "APP_PASS")) {
                    authentication.setAppPass(headerKey.getKeyValue());
                }
                if (StringUtils.equalsIgnoreCase(headerKey.getKeyName(), "IPADDRESS")) {
                    authentication.setIpAddress(headerKey.getKeyValue());
                }
                if (StringUtils.equalsIgnoreCase(headerKey.getKeyName(), "DEVICE_ID")) {
                    if (StringUtils.equalsIgnoreCase(EXISTINGLOANDETAILS, requestType)) {
                        authentication.setDeviceId("APPLICANT_BASICINFO");
                    } else if (StringUtils.equalsIgnoreCase(MIFINLOANDETAIL, requestType)) {
                        authentication.setDeviceId("MIFIN_LOANDETAIL");
                    }else if (StringUtils.equalsIgnoreCase(MIFIN_DEDUPE_DETAIL, requestType)) {

                        authentication.setDeviceId("MIFIN_DEDUPE");
                    }else {
                        authentication.setDeviceId(headerKey.getKeyValue());
                    }
                }
                if (StringUtils.equalsIgnoreCase(headerKey.getKeyName(), "LONGITUDE")) {
                    authentication.setLongitude(headerKey.getKeyValue());
                }
                if (StringUtils.equalsIgnoreCase(headerKey.getKeyName(), "LATITUDE")) {
                    authentication.setLatitude(headerKey.getKeyValue());
                }

            }
        }

        return authentication;

    }

    public List<TopUpDedupeResponse> sendDataToMifinDedupe(ApplicationRequest applicationRequest, Applicant applicant,
                                                           List<CoApplicant> coApplicantList)  {
        ActivityLogs activityLog = AuditHelper.createActivityLog(applicationRequest,
                applicationRequest.getCurrentStageId(), GNGWorkflowConstant.MIFIN_DEDUPE_CALL.toFaceValue(),
                GNGWorkflowConstant.MIFIN_DEDUPE_CALL.toFaceValue(), GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<TopUpDedupeResponse> topUpDedupeMifinResponseList = new ArrayList<TopUpDedupeResponse>();
        try{
        String urlType = UrlType.TOP_UP_DEDUPE.toValue();
        WFJobCommDomain mifinDedupeConfiguaration = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);

        Authentication authentication = getAuthenticationDetails(mifinDedupeConfiguaration, MIFIN_DEDUPE_DETAIL);
        TopUpDedupeResponse topUpDedupeMifinResponse = null;
        MifinTopUpResponse mifinTopUpresponse = null;

        String url = Arrays.asList(mifinDedupeConfiguaration.getBaseUrl(), mifinDedupeConfiguaration.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

        MiFinCallLog apiLog = new MiFinCallLog();

        MifinCallRequest mifinCallRequest = new MifinCallRequest();
        mifinCallRequest.setReqType(MIFIN_DEDUPE_DETAIL);
        TopUpMifinDedupeRequest miFinRequest = null;
        MifinReciveResponse mifinReciveResponse = null;
        String requestParams = null;
        String appId = null;
        // Calling for Applicant
        if (applicant != null) {
            miFinRequest = buildMiFinDedupeRequest(applicationRequest, applicant);
            miFinRequest.setAuthentication(authentication);
            requestParams = JsonUtil.ObjectToString(miFinRequest);
            logger.info("request sending to mifin {}", requestParams);
            mifinCallRequest.setRequest(requestParams);
            logger.info("request after String {}", mifinCallRequest);
            logger.info("MiFin request in applicant : {} for refId: {}", mifinCallRequest, applicationRequest.getRefID());
            try {
                if (miFinRequest != null) {
                    String mifinRespnseString = "test";
                    // mifinRespnseString = getFixedResponseForTesting();
                    mifinReciveResponse =
                            (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                                    url, MifinReciveResponse.class);
                    /// mifinReciveResponse = miFinHelper.getTempResponseForMifinDedupe();
                    appId = applicationRequest.getRequest().getApplicant().getApplicantId();
                    topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                    List<MiFinCallLog> miFinCallLog = externalAPILogRepository.fetchMiFinCallLogForDedupe(applicationRequest.getRefID(), MIFINLOANDETAIL);
                    if(CollectionUtils.isNotEmpty(miFinCallLog)  && CollectionUtils.isNotEmpty(miFinCallLog.get(0).getDedupeResponseList()) ){
                    topUpDedupeMifinResponse.apiHit = miFinCallLog.get(0).getDedupeResponseList().get(0).apiHit;
                    }
                    topUpDedupeMifinResponseList.add(topUpDedupeMifinResponse);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        logger.info("responseList {}", topUpDedupeMifinResponseList);

        //Calling for coApplicant
        if (CollectionUtils.isNotEmpty(coApplicantList)) {
            for (CoApplicant coApplicant : coApplicantList) {
                logger.info("{} MiFin for co -applicnt : {} ", applicationRequest.getRefID(), coApplicant.getApplicantId());
                if (coApplicant != null) {
                    miFinRequest = buildMiFinDedupeRequestForCoApplicant(coApplicant);
                    miFinRequest.setAuthentication(authentication);
                    mifinCallRequest.setReqType(MIFIN_DEDUPE_DETAIL);
                    requestParams = JsonUtil.ObjectToString(miFinRequest);
                    mifinCallRequest.setRequest(requestParams);
                    appId = coApplicant.getApplicantId();
                    logger.info("request after String {}", mifinCallRequest);
                }

                if (miFinRequest != null) {
                    mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                            url, MifinReciveResponse.class);
                    topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                    logger.info("mifinTopUpresponse {}", mifinTopUpresponse);
                    topUpDedupeMifinResponseList.add(topUpDedupeMifinResponse);
                }
            }
        }
        checkData(topUpDedupeMifinResponseList, applicationRequest.getRefID(), MIFIN_DEDUPE_DETAIL, miFinRequest, urlType, appId);
        logger.info("responseList {}", topUpDedupeMifinResponseList);
        }catch (Exception e){
            logger.error("TopUpApi Error {}",e.getStackTrace());
        }
        return topUpDedupeMifinResponseList;
    }


    private String getFixedResponseForTesting() {
        String fixedResponse = "{\n" +
                "\"STATUS\": \"S\",\n" +
                "\"MESSAGE\": \"Process Completed\",\n" +
                "\"MATCH_COUNT\": \"\",\n" +
                "\"APPLICANTDETAILS\" : \n" +
                "{\n" +
                "\"CUSTID\": \" \",\n" +
                "\"DOB\": \" \",\n" +
                "\"FNAME\": \"POOJA PATIL\",\n" +
                "\"MNAME\": \" \",\n" +
                "\"LNAME\": \"FCGHVJBN\",\n" +
                "\"FATHERNAME\": \" \",\n" +
                "\"MOTHERNAME\": \" \",\n" +
                "\"PAN\": \" \",\n" +
                "\"VOTERIDCARDNO\": \" \",\n" +
                "\"PASSPORTNO\": \" \",\n" +
                "\"DL\": \" \",\n" +
                "\"SCORE\": \" \",\n" +
                "\"POTENTIAL_MATCH\": \" \",\n" +
                "\"STRONG_MATCH\": \" \",\n" +
                "\"CUSTTYPE\": \" \",\n" +
                "\"EXACTMATCH_MSGSTATUS\": \" \",\n" +
                "\"MATCHED_APPLICANTCODE\": \" \",\n" +
                "\"POTENTIALMATCH_MSGSTATUS\": \" \",\n" +
                "\"POTENTIAL_APPLICANTCODE\": \" \",\n" +
                "\"ADDRESSDETAIL\" : \n" +
                "{\n" +
                "\"ADDRESSTYPE\": \" \",\n" +
                "\"ADDRESS\": \" \",\n" +
                "\"CITY\": \" \",\n" +
                "\"STATE\": \" \",\n" +
                "\"COUNTRY\": \" \",\n" +
                "\"PINCODE\": \" \"\n" +
                "}\n" +
                "}\n" +
                ",\n" +
                "\"MATCHEDAPPLICANTS\": {\n" +
                "\"CUSTID\": \" \",\n" +
                "\"DOB\": \"7-9-2018 \",\n" +
                "\"FNAME\": \"POOJA PATIL\",\n" +
                "\"MNAME\": \" \",\n" +
                "\"LNAME\": \" \",\n" +
                "\"FATHERNAME\": \" \",\n" +
                "\"MOTHERNAME\": \" \",\n" +
                "\"PAN\": \" AAAPA1234Q\",\n" +
                "\"VOTERIDCARDNO\": \" \",\n" +
                "\"PASSPORTNO\": \" \",\n" +
                "\"DL\": \" \",\n" +
                "\"SCORE\": \" \",\n" +
                "\"POTENTIAL_MATCH\": \" \",\n" +
                "\"STRONG_MATCH\": \" \",\n" +
                "\"CUSTTYPE\": \" \",\n" +
                "\"EXACTMATCH_MSGSTATUS\": \" \",\n" +
                "\"MATCHED_APPLICANTCODE\": \" \",\n" +
                "\"POTENTIALMATCH_MSGSTATUS\": \" \",\n" +
                "\"POTENTIAL_APPLICANTCODE\": \" \",\n" +
                "\"ADDRESSDETAIL\" : \n" +
                "{\n" +
                "\"ADDRESSTYPE\": \" \",\n" +
                "\"ADDRESS\": \" \",\n" +
                "\"CITY\": \" \",\n" +
                "\"STATE\": \" \",\n" +
                "\"COUNTRY\": \" \",\n" +
                "\"PINCODE\": \" \"\n" +
                "}\n" +
                "}\n" +
                "}\n";
        return fixedResponse;
    }

    public TopUpMifinDedupeRequest buildMiFinDedupeRequest(ApplicationRequest applicationRequest) throws ParseException {
        TopUpMifinDedupeRequest miFinRequest = TopUpMifinDedupeRequest.builder().build();
        Applicant applicant = applicationRequest.getRequest().getApplicant();
        com.softcell.gonogo.model.mifin.topup.BasicInfo basicInfo = com.softcell.gonogo.model.mifin.topup.BasicInfo.builder().build();
        logger.info("MiFin buildMiFinDedupeRequest");
        if (GngUtils.isIndividual(applicant.getApplicantType())){
            basicInfo.setCustType(MiFinHelper.getCustomerEntityType(applicant.getApplicantType()));
            basicInfo.setApplicantCode(Constant.BLANK);

            if (StringUtils.isNotEmpty(applicant.getApplicantName().getFirstName())) {
                basicInfo.setFirstName(applicant.getApplicantName().getFirstName());
            }

            if (StringUtils.isNotEmpty(applicant.getApplicantName().getMiddleName())) {
                basicInfo.setMiddleName(applicant.getApplicantName().getMiddleName());
            }

            if (StringUtils.isNotEmpty(applicant.getApplicantName().getLastName())) {
                basicInfo.setLastName(applicant.getApplicantName().getLastName());
            }

            if (CollectionUtils.isNotEmpty(applicant.getKyc())) {
                List<Kyc> kycList = applicant.getKyc();
                for (Kyc kyc : kycList) {
                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PAN, kyc.getKycName()) && kyc.getKycNumber() != null) {
                        basicInfo.setPan(kyc.getKycNumber());
                    }
                }
            } else {
                basicInfo.setPan(Constant.BLANK);
            }
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setRationCardNumber(Constant.BLANK);
            basicInfo.setPassPortNo(Constant.BLANK);
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setMiddleName(GngUtils.setValueForNull(applicant.getApplicantName().getMiddleName()));
            basicInfo.setLastName(GngUtils.setValueForNull(applicant.getApplicantName().getLastName()));
            if (applicant.getFatherName() != null) {
                basicInfo.setFatherName(GngUtils.setValueForNull(applicant.getFatherName().getFirstName()));
            } else {
                basicInfo.setFatherName(Constant.BLANK);
            }
            if (applicant.getMotherName() != null) {
                basicInfo.setMotherName(GngUtils.setValueForNull(applicant.getMotherName().getFirstName()));
            } else {
                basicInfo.setMotherName(Constant.BLANK);
            }
            if (StringUtils.isNotEmpty(applicant.getDateOfBirth())) {
                String dt = applicant.getDateOfBirth();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                Date date = dateFormat.parse(dt);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = formatter.format(date);
                basicInfo.setDob(dateStr);
            } else {
                basicInfo.setDob(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(applicant.getEmail())) {
                List<Email> emailList = applicant.getEmail();
                for (Email email : emailList) {

                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PERSONAL, email.getEmailType())
                            && email.getEmailAddress() != null ||
                            StringUtils.equalsIgnoreCase(MiFinHelper.INDIVIDUAL, applicant.getApplicantType())) {
                        basicInfo.setEmail(email.getEmailAddress());

                    } else {

                        basicInfo.setCompanyEmail(email.getEmailAddress());
                    }

                }
            } else {
                basicInfo.setCompanyEmail(Constant.BLANK);
            }
            basicInfo.setGender(GngUtils.setValueForNull(applicant.getGender()));
            if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
                List<Phone> phoneList = applicant.getPhone();
                for (Phone phone : phoneList) {
                    if (phone.getPhoneType() != null && StringUtils.equalsIgnoreCase(phone.getPhoneType(), MiFinHelper.PERSONAL_MOBILE)) {
                        basicInfo.setCell(phone.getPhoneNumber());
                    } else {
                        basicInfo.setLandLine1(Constant.BLANK);
                        basicInfo.setLandLine2(Constant.BLANK);
                    }

                }

            } else {
                basicInfo.setCell(Constant.BLANK);
                basicInfo.setLandLine1(Constant.BLANK);
                basicInfo.setLandLine2(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                List<CustomerAddress> addressList = applicant.getAddress();
                for (CustomerAddress customerAddress : addressList) {
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name(), customerAddress.getAddressType())) {
                        basicInfo.setAddressType(GngUtils.setValueForNull(customerAddress.getAddressType()));
                        basicInfo.setCurrentCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setLandMark(GngUtils.setValueForNull(customerAddress.getLandMark()));
                        basicInfo.setCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setState(GngUtils.setValueForNull(customerAddress.getState()));
                        basicInfo.setCountry(GngUtils.setValueForNull(customerAddress.getCountry()));
                        if (customerAddress.getPin() != 0) {
                            basicInfo.setZip(Long.toString(customerAddress.getPin()));
                        } else {
                            basicInfo.setZip(Constant.BLANK);
                        }
                    }
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name(), customerAddress.getAddressType())) {
                        basicInfo.setCompanyAddress(GngUtils.setValueForNull(customerAddress.getAddressLine1()));
                        basicInfo.setCompanyCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setLocality(GngUtils.setValueForNull(customerAddress.getLandMark()));
                        basicInfo.setCompanyZip(GngUtils.setValueForNull(Long.toString(customerAddress.getPin())));
                        basicInfo.setCompanyState(GngUtils.setValueForNull(customerAddress.getCity()));
                    } else if (StringUtils.isNotEmpty(basicInfo.getCompanyZip())) {
                        basicInfo.setCompanyAddress(Constant.BLANK);
                        basicInfo.setCompanyCity(Constant.BLANK);
                        basicInfo.setCompanyZip(Constant.BLANK);
                        basicInfo.setCompanyState(Constant.BLANK);
                    }
                    basicInfo.setFloor(Constant.BLANK);
                }
            } else {
                basicInfo.setAddressType(Constant.BLANK);
                basicInfo.setLandMark(Constant.BLANK);
                basicInfo.setCity(Constant.BLANK);
                basicInfo.setState(Constant.BLANK);
                basicInfo.setCurrentCity(Constant.BLANK);
                basicInfo.setCountry(Constant.BLANK);
            }
            basicInfo.setCompanyEmail(Constant.BLANK);
            basicInfo.setCompnay(Constant.BLANK);
            basicInfo.setCompanyNameCorporate(Constant.BLANK);
            basicInfo.setDoi(Constant.BLANK);
            basicInfo.setGender(GngUtils.setValueForNull(applicant.getGender()));
            basicInfo.setLocality(Constant.BLANK);
            basicInfo.setCompanyPhone1(Constant.BLANK);
        }
        basicInfo.setCompanyFAx(Constant.BLANK);
        basicInfo.setAllCompany(Constant.BLANK);
        basicInfo.setBatchID(Constant.BLANK);
        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
        basicInfo.setBuildingName(Constant.BLANK);
        basicInfo.setCell2(Constant.BLANK);
        basicInfo.setCell3(Constant.BLANK);
        basicInfo.setDl(Constant.BLANK);
        basicInfo.setFlat(Constant.BLANK);
        basicInfo.setFloor(Constant.BLANK);
        miFinRequest.setBasicInfo(basicInfo);
        return miFinRequest;
    }
    //for applicant data
    public TopUpMifinDedupeRequest buildMiFinDedupeRequest(ApplicationRequest applicationRequest, Applicant applicant) throws ParseException {

        TopUpMifinDedupeRequest miFinRequest = TopUpMifinDedupeRequest.builder().build();
        // miFinRequest.setRequest(new MifinReqData());
        com.softcell.gonogo.model.mifin.topup.BasicInfo basicInfo = com.softcell.gonogo.model.mifin.topup.BasicInfo.builder().build();
        logger.info("MiFin buildMiFinDedupeRequest : {} for refId: {}", applicationRequest.getRefID());
        if (applicationRequest.getRequest().getApplicant() != null) {
            applicant = applicationRequest.getRequest().getApplicant();
        }

        if (GngUtils.isIndividual(applicant.getApplicantType())) {
            basicInfo.setCustType(MiFinHelper.getCustomerEntityType(applicant.getApplicantType()));
            basicInfo.setApplicantCode(Constant.BLANK);
           /* if(StringUtils.isNotEmpty(applicant.getApplicantId())) {
                basicInfo.setApplicantId(applicant.getApplicantId());
            }*/
            if (StringUtils.isNotEmpty(applicant.getApplicantName().getFirstName())) {
                basicInfo.setFirstName(applicant.getApplicantName().getFirstName());
            }

            if (CollectionUtils.isNotEmpty(applicant.getKyc())) {
                List<Kyc> kycList = applicant.getKyc();
                for (Kyc kyc : kycList) {
                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PAN, kyc.getKycName()) && kyc.getKycNumber() != null) {
                        basicInfo.setPan(kyc.getKycNumber());
                    }
                }
            } else {
                basicInfo.setPan(Constant.BLANK);
            }
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setRationCardNumber(Constant.BLANK);
            basicInfo.setPassPortNo(Constant.BLANK);
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setMiddleName(GngUtils.setValueForNull(applicant.getApplicantName().getMiddleName()));
            basicInfo.setLastName(GngUtils.setValueForNull(applicant.getApplicantName().getLastName()));
            if (applicant.getFatherName() != null) {
                basicInfo.setFatherName(GngUtils.setValueForNull(applicant.getFatherName().getFirstName()));
            } else {
                basicInfo.setFatherName(Constant.BLANK);
            }
            if (applicant.getMotherName() != null) {
                basicInfo.setMotherName(GngUtils.setValueForNull(applicant.getMotherName().getFirstName()));
            } else {
                basicInfo.setMotherName(Constant.BLANK);
            }
            if (StringUtils.isNotEmpty(applicant.getDateOfBirth())) {
                String dt = applicant.getDateOfBirth();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                Date date = dateFormat.parse(dt);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = formatter.format(date);
                basicInfo.setDob(dateStr);
            } else {
                basicInfo.setDob(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(applicant.getEmail())) {
                List<Email> emailList = applicant.getEmail();
                for (Email email : emailList) {

                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PERSONAL, email.getEmailType())
                            && email.getEmailAddress() != null ||
                            StringUtils.equalsIgnoreCase(MiFinHelper.INDIVIDUAL, applicant.getApplicantType())) {
                        basicInfo.setEmail(email.getEmailAddress());

                    } else {

                        basicInfo.setCompanyEmail(email.getEmailAddress());
                    }

                }
            } else {
                basicInfo.setCompanyEmail(Constant.BLANK);
            }
            basicInfo.setGender(GngUtils.setValueForNull(applicant.getGender()));
            if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
                List<Phone> phoneList = applicant.getPhone();
                for (Phone phone : phoneList) {
                    if (phone.getPhoneType() != null && StringUtils.equalsIgnoreCase(phone.getPhoneType(), MiFinHelper.PERSONAL_MOBILE)) {
                        basicInfo.setCell(phone.getPhoneNumber());
                    } else {
                        basicInfo.setLandLine1(Constant.BLANK);
                        basicInfo.setLandLine2(Constant.BLANK);
                    }

                }

            } else {
                basicInfo.setCell(Constant.BLANK);
                basicInfo.setLandLine1(Constant.BLANK);
                basicInfo.setLandLine2(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                List<CustomerAddress> addressList = applicant.getAddress();
                for (CustomerAddress customerAddress : addressList) {
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name(), customerAddress.getAddressType())) {
                        basicInfo.setAddressType(GngUtils.setValueForNull(customerAddress.getAddressType()));
                        basicInfo.setCurrentCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setLandMark(GngUtils.setValueForNull(customerAddress.getLandMark()));
                        basicInfo.setCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setState(GngUtils.setValueForNull(customerAddress.getState()));
                        basicInfo.setCountry(GngUtils.setValueForNull(customerAddress.getCountry()));
                        if (customerAddress.getPin() != 0) {
                            basicInfo.setZip(Long.toString(customerAddress.getPin()));
                        } else {
                            basicInfo.setZip(Constant.BLANK);
                        }
                    }
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name(), customerAddress.getAddressType())) {
                        basicInfo.setCompanyAddress(GngUtils.setValueForNull(customerAddress.getAddressLine1()));
                        basicInfo.setCompanyCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setLocality(GngUtils.setValueForNull(customerAddress.getLandMark()));
                        basicInfo.setCompanyZip(GngUtils.setValueForNull(Long.toString(customerAddress.getPin())));
                        basicInfo.setCompanyState(GngUtils.setValueForNull(customerAddress.getCity()));
                    } else if (StringUtils.isNotEmpty(basicInfo.getCompanyZip())) {
                        basicInfo.setCompanyAddress(Constant.BLANK);
                        basicInfo.setCompanyCity(Constant.BLANK);
                        basicInfo.setCompanyZip(Constant.BLANK);
                        basicInfo.setCompanyState(Constant.BLANK);
                    }
                    basicInfo.setFloor(Constant.BLANK);
                }
            } else {
                basicInfo.setAddressType(Constant.BLANK);
                basicInfo.setLandMark(Constant.BLANK);
                basicInfo.setCity(Constant.BLANK);
                basicInfo.setState(Constant.BLANK);
                basicInfo.setCurrentCity(Constant.BLANK);
                basicInfo.setCountry(Constant.BLANK);
            }
            basicInfo.setCompanyEmail(Constant.BLANK);
            basicInfo.setCompnay(Constant.BLANK);
            basicInfo.setCompanyNameCorporate(Constant.BLANK);
            basicInfo.setDoi(Constant.BLANK);
            basicInfo.setGender(GngUtils.setValueForNull(applicant.getGender()));
            basicInfo.setLocality(Constant.BLANK);
            basicInfo.setCompanyPhone1(Constant.BLANK);
        } else {
            MiFinHelper.getCustomerEntityType(applicant.getApplicantType());
            basicInfo.setApplicantCode(Constant.BLANK);
            if (applicant.getApplicantId() != null) {
                basicInfo.setApplicantId(applicant.getApplicantId());
            }
            if (CollectionUtils.isNotEmpty(applicant.getEmail())) {
                List<Email> emailList = applicant.getEmail();
                for (Email email : emailList) {
                    basicInfo.setCompanyEmail(email.getEmailAddress());
                }
            }
            basicInfo.setCompanyNameCorporate(GngUtils.setValueForNull(applicant.getApplicantName().getFirstName()));
            basicInfo.setCompnay(GngUtils.setValueForNull(applicant.getApplicantName().getFirstName()));
            if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
                List<Phone> phoneList = applicant.getPhone();
                for (Phone phone : phoneList) {
                    if (phone.getPhoneType() != null && StringUtils.equalsIgnoreCase(phone.getPhoneType(), MiFinHelper.PERSONAL_MOBILE)) {
                        basicInfo.setCompanyPhone1(phone.getPhoneNumber());
                    }

                }

            } else {
                basicInfo.setCompanyPhone1(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(applicant.getAddress())) {
                List<CustomerAddress> addressList = applicant.getAddress();
                for (CustomerAddress customerAddress : addressList) {
                    basicInfo.setCompanyCity(GngUtils.setValueForNull(customerAddress.getCity()));
                    basicInfo.setState(GngUtils.setValueForNull(customerAddress.getState()));
                    if (customerAddress.getPin() != 0L) {
                        basicInfo.setCompanyZip(Long.toString(customerAddress.getPin()));
                    }
                }

            }
            //add company Type
            if (CollectionUtils.isNotEmpty(applicant.getEmployment())) {
                List<Employment> employmentList = applicant.getEmployment();
                for (Employment employmet : employmentList) {
                    basicInfo.setDoi(GngUtils.setValueForNull(employmet.getDateOfJoining()));
                }
            } else {
                basicInfo.setDoi("");
            }
            if (CollectionUtils.isNotEmpty(applicant.getKyc())) {
                List<Kyc> kycList = applicant.getKyc();
                for (Kyc kyc : kycList) {
                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PAN, kyc.getKycName()) && kyc.getKycNumber() != null) {
                        basicInfo.setPan(kyc.getKycNumber());
                    }
                }
            }
            basicInfo.setDob(Constant.BLANK);
        }
        basicInfo.setCompanyFAx(Constant.BLANK);
        basicInfo.setAllCompany(Constant.BLANK);
        basicInfo.setBatchID(Constant.BLANK);
        basicInfo.setBranch(GngUtils.setValueForNull(applicationRequest.getAppMetaData().getBranchV2().getBranchName()));
        basicInfo.setBuildingName(Constant.BLANK);
        basicInfo.setCell2(Constant.BLANK);
        basicInfo.setCell3(Constant.BLANK);
        basicInfo.setDl(Constant.BLANK);
        basicInfo.setFlat(Constant.BLANK);
        basicInfo.setFloor(Constant.BLANK);
        miFinRequest.setBasicInfo(basicInfo);
        return miFinRequest;
    }

    public TopUpMifinDedupeRequest buildMiFinDedupeRequestForCoApplicant(CoApplicant coApplicant) throws ParseException {
        logger.info("MiFin buildMiFinDedupeRequestForCoApplicant : {} for refId: {}");
        System.out.println("buildMiFinDedupeRequestForCoApplicant");
        TopUpMifinDedupeRequest miFinRequest = TopUpMifinDedupeRequest.builder().build();
        //miFinRequest.setRequest(new MifinReqData());
        com.softcell.gonogo.model.mifin.topup.BasicInfo basicInfo = com.softcell.gonogo.model.mifin.topup.BasicInfo.builder().build();

        if (GngUtils.isIndividual(coApplicant.getApplicantType())) {
            basicInfo.setCustType(MiFinHelper.getCustomerEntityType(coApplicant.getApplicantType()));
            basicInfo.setApplicantCode(Constant.BLANK);
            if (StringUtils.isNotEmpty(coApplicant.getApplicantName().getFirstName())) {
                basicInfo.setFirstName(coApplicant.getApplicantName().getFirstName());
            }

            if (CollectionUtils.isNotEmpty(coApplicant.getKyc())) {
                List<Kyc> kycList = coApplicant.getKyc();
                for (Kyc kyc : kycList) {
                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PAN, kyc.getKycName()) && kyc.getKycNumber() != null) {
                        basicInfo.setPan(kyc.getKycNumber());
                    }
                }
            } else {
                basicInfo.setPan(Constant.BLANK);
            }
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setRationCardNumber(Constant.BLANK);
            basicInfo.setPassPortNo(Constant.BLANK);
            basicInfo.setVoterCardNo(Constant.BLANK);
            basicInfo.setMiddleName(GngUtils.setValueForNull(coApplicant.getApplicantName().getMiddleName()));
            basicInfo.setLastName(GngUtils.setValueForNull(coApplicant.getApplicantName().getLastName()));
            if (coApplicant.getFatherName() != null) {
                basicInfo.setFatherName(GngUtils.setValueForNull(coApplicant.getFatherName().getFirstName()));
            } else {
                basicInfo.setFatherName(Constant.BLANK);
            }
            if (coApplicant.getMotherName() != null) {
                basicInfo.setMotherName(GngUtils.setValueForNull(coApplicant.getMotherName().getFirstName()));
            } else {
                basicInfo.setMotherName(Constant.BLANK);
            }
            if (StringUtils.isNotEmpty(coApplicant.getDateOfBirth())) {
                String dt = coApplicant.getDateOfBirth();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                Date date = dateFormat.parse(dt);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = formatter.format(date);
                basicInfo.setDob(dateStr);
            } else {
                basicInfo.setDob(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(coApplicant.getEmail())) {
                List<Email> emailList = coApplicant.getEmail();
                for (Email email : emailList) {

                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PERSONAL, email.getEmailType())
                            && email.getEmailAddress() != null ||
                            StringUtils.equalsIgnoreCase(MiFinHelper.INDIVIDUAL, coApplicant.getApplicantType())) {
                        basicInfo.setEmail(email.getEmailAddress());

                    } else {

                        basicInfo.setCompanyEmail(email.getEmailAddress());
                    }

                }
            } else {
                basicInfo.setCompanyEmail(Constant.BLANK);
            }
            basicInfo.setGender(GngUtils.setValueForNull(coApplicant.getGender()));
            if (CollectionUtils.isNotEmpty(coApplicant.getPhone())) {
                List<Phone> phoneList = coApplicant.getPhone();
                for (Phone phone : phoneList) {
                    if (phone.getPhoneType() != null && StringUtils.equalsIgnoreCase(phone.getPhoneType(), MiFinHelper.PERSONAL_MOBILE)) {
                        basicInfo.setCell(phone.getPhoneNumber());
                    } else {
                        basicInfo.setLandLine1(Constant.BLANK);
                        basicInfo.setLandLine2(Constant.BLANK);
                    }

                }

            } else {
                basicInfo.setCell(Constant.BLANK);
                basicInfo.setLandLine1(Constant.BLANK);
                basicInfo.setLandLine2(Constant.BLANK);
            }
            if (CollectionUtils.isNotEmpty(coApplicant.getAddress())) {
                List<CustomerAddress> addressList = coApplicant.getAddress();
                for (CustomerAddress customerAddress : addressList) {
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name(), customerAddress.getAddressType()) &&
                            StringUtils.isNotBlank(customerAddress.getCity())) {
                        basicInfo.setCurrentCity(customerAddress.getCity());
                    } else {
                        basicInfo.setCurrentCity(Constant.BLANK);
                    }
                    basicInfo.setAddressType(GngUtils.setValueForNull(customerAddress.getAddressType()));
                    basicInfo.setLandMark(GngUtils.setValueForNull(customerAddress.getLandMark()));
                    basicInfo.setCity(GngUtils.setValueForNull(customerAddress.getCity()));
                    basicInfo.setState(GngUtils.setValueForNull(customerAddress.getState()));
                    basicInfo.setCountry(GngUtils.setValueForNull(customerAddress.getCountry()));
                    basicInfo.setFlat(GngUtils.setValueForNull(customerAddress.getFlatNo()));
                    if (customerAddress.getPin() != 0) {
                        basicInfo.setZip(Long.toString(customerAddress.getPin()));
                    } else {
                        basicInfo.setZip(Constant.BLANK);
                    }
                    if (StringUtils.equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name(), customerAddress.getAddressType())) {
                        basicInfo.setCompanyAddress(GngUtils.setValueForNull(customerAddress.getAddressLine1()));
                        basicInfo.setCompanyCity(GngUtils.setValueForNull(customerAddress.getCity()));
                        basicInfo.setLocality(Constant.BLANK);
                        basicInfo.setCompanyZip(GngUtils.setValueForNull(Long.toString(customerAddress.getPin())));
                        basicInfo.setCompanyState(Constant.BLANK);
                    }
                    basicInfo.setFloor(Constant.BLANK);
                }
            } else {
                basicInfo.setAddressType(Constant.BLANK);
                basicInfo.setLandMark(Constant.BLANK);
                basicInfo.setCity(Constant.BLANK);
                basicInfo.setState(Constant.BLANK);
                basicInfo.setCurrentCity(Constant.BLANK);
                basicInfo.setCountry(Constant.BLANK);
            }
            basicInfo.setCompanyEmail(Constant.BLANK);
            basicInfo.setCompnay(Constant.BLANK);
            basicInfo.setCompanyNameCorporate(Constant.BLANK);
            basicInfo.setDoi(Constant.BLANK);
            basicInfo.setGender(GngUtils.setValueForNull(coApplicant.getGender()));
            basicInfo.setLocality(Constant.BLANK);
            basicInfo.setCompanyPhone1(Constant.BLANK);
            basicInfo.setCompanyZip(Constant.BLANK);
            basicInfo.setCompanyState(Constant.BLANK);
        } else {
            MiFinHelper.getCustomerEntityType(coApplicant.getApplicantType());
            basicInfo.setApplicantCode(Constant.BLANK);
            if (coApplicant.getApplicantId() != null) {
                basicInfo.setApplicantId(coApplicant.getApplicantId());
            }
            if (CollectionUtils.isNotEmpty(coApplicant.getEmail())) {
                List<Email> emailList = coApplicant.getEmail();
                for (Email email : emailList) {
                    basicInfo.setCompanyEmail(email.getEmailAddress());
                }
            }
            basicInfo.setCompanyNameCorporate(GngUtils.setValueForNull(coApplicant.getApplicantName().getFirstName()));
            basicInfo.setCompnay(GngUtils.setValueForNull(coApplicant.getApplicantName().getFirstName()));
            if (CollectionUtils.isNotEmpty(coApplicant.getPhone())) {
                List<Phone> phoneList = coApplicant.getPhone();
                for (Phone phone : phoneList) {
                    if (phone.getPhoneType() != null && StringUtils.equalsIgnoreCase(phone.getPhoneType(), "PERSONAL_MOBILE")) {
                        basicInfo.setCompanyPhone1(phone.getPhoneNumber());
                    }

                }

            } else {
                basicInfo.setCompanyPhone1(MiFinHelper.BLANK_VALUE);
            }
            if (CollectionUtils.isNotEmpty(coApplicant.getAddress())) {
                List<CustomerAddress> addressList = coApplicant.getAddress();
                for (CustomerAddress customerAddress : addressList) {
                    basicInfo.setCompanyCity(GngUtils.setValueForNull(customerAddress.getCity()));
                    basicInfo.setState(GngUtils.setValueForNull(customerAddress.getState()));
                    if (customerAddress.getPin() != 0L) {
                        basicInfo.setCompanyZip(Long.toString(customerAddress.getPin()));
                    }
                }

            } else {
                basicInfo.setCompanyState(MiFinHelper.BLANK_VALUE);
                basicInfo.setCompanyCity(MiFinHelper.BLANK_VALUE);
                basicInfo.setCompanyZip(MiFinHelper.BLANK_VALUE);
            }

            if (CollectionUtils.isNotEmpty(coApplicant.getEmployment())) {
                List<Employment> employmentList = coApplicant.getEmployment();
                for (Employment employmet : employmentList) {
                    basicInfo.setDoi(GngUtils.setValueForNull(employmet.getDateOfJoining()));
                }
            } else {
                basicInfo.setDoi(MiFinHelper.BLANK_VALUE);
            }
            if (CollectionUtils.isNotEmpty(coApplicant.getKyc())) {
                List<Kyc> kycList = coApplicant.getKyc();
                for (Kyc kyc : kycList) {
                    if (StringUtils.equalsIgnoreCase(MiFinHelper.PAN, kyc.getKycName()) && kyc.getKycNumber() != null) {
                        basicInfo.setPan(kyc.getKycNumber());
                    }
                }
            } else {
                basicInfo.setPan(MiFinHelper.BLANK_VALUE);
            }
            basicInfo.setDob(MiFinHelper.BLANK_VALUE);
        }
        basicInfo.setCompanyFAx(MiFinHelper.BLANK_VALUE);
        basicInfo.setAllCompany(MiFinHelper.BLANK_VALUE);
        basicInfo.setBatchID(MiFinHelper.BLANK_VALUE);
        basicInfo.setBuildingName(MiFinHelper.BLANK_VALUE);
        basicInfo.setCell2(MiFinHelper.BLANK_VALUE);
        basicInfo.setCell3(MiFinHelper.BLANK_VALUE);
        basicInfo.setDl(MiFinHelper.BLANK_VALUE);
        basicInfo.setFlat(MiFinHelper.BLANK_VALUE);
        basicInfo.setFloor(MiFinHelper.BLANK_VALUE);
        miFinRequest.setBasicInfo(basicInfo);
        return miFinRequest;
    }

    @Override
    public List<TopUpDedupeResponse> sendMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest)  {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String urlType = UrlType.TOP_UP_DEDUPE.toValue();
        List<TopUpDedupeResponse> topUpDedupeMifinResponseList = new ArrayList<TopUpDedupeResponse>();
        TopUpDedupeResponse topUpDedupeMifinResponse = null;
        MifinReciveResponse mifinReciveResponse = null;
        TopUpMifinDedupeRequest dedupeRequest =null;

        MiFinCallLog apiLog = new MiFinCallLog();
        MifinCallRequest mifinCallRequest;

        List<TopUpDedupeResponse> topUpDedupeResponsesList = new ArrayList<>();
        try{
            WFJobCommDomain urlConfig = getServiceConfiguration(topUpDedupeRequest.getHeader().getInstitutionId(), urlType);

            String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            if (topUpDedupeRequest.getLoanDetailTypeValue() != null && !StringUtils.equalsIgnoreCase(topUpDedupeRequest.getLoanDetailType(),"B")) {
             dedupeRequest = buildMiFinLoanDetail(topUpDedupeRequest, MiFinHelper.LOAN_TYPE_A);
            mifinCallRequest = getMifinCallRequest(dedupeRequest, MIFINLOANDETAIL, urlConfig);


             mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                    url, MifinReciveResponse.class);
            logger.info("mifinReciveResponse {}", mifinReciveResponse);
            if (mifinReciveResponse != null) {
                topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
            }
           /* for hardcoded
           topUpDedupeMifinResponse = miFinHelper.getTempResponseForLoandetail();*/

            if (topUpDedupeMifinResponse != null && StringUtils.equalsIgnoreCase(topUpDedupeMifinResponse.getStatus(), "S")) {
                topUpDedupeMifinResponse.setTopUpType(topUpDedupeRequest.getLoanDetailTypeValue());
                topUpDedupeMifinResponse.apiHit = true;
                topUpDedupeResponsesList.add(topUpDedupeMifinResponse);
            }
            if (StringUtils.equalsIgnoreCase(topUpDedupeRequest.getLoanDetailTypeValue(), TOPUP_GROSS)) {
                dedupeRequest = buildMiFinLoanDetail(topUpDedupeRequest, MiFinHelper.LOAN_TYPE_B);
                mifinCallRequest = getMifinCallRequest(dedupeRequest, MIFINLOANDETAIL, urlConfig);
                logger.debug("MiFin request for MIFINLOANDETAIL refid {} : {}", topUpDedupeRequest.getRefId(), mifinCallRequest);
                mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                        url, MifinReciveResponse.class);

                /* for hardcoded
           topUpDedupeMifinResponse = miFinHelper.getTempResponseForLoandetail();*/
                if (topUpDedupeMifinResponse != null) {
                    topUpDedupeMifinResponse.setTopUpType(topUpDedupeRequest.getLoanDetailTypeValue());
                    topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                    topUpDedupeResponsesList.add(topUpDedupeMifinResponse);
                }
            }/*else{
                dedupeRequest = buildMiFinLoanDetail(topUpDedupeRequest, MiFinHelper.LOAN_TYPE_B);
                mifinCallRequest = getMifinCallRequest(dedupeRequest, MIFINLOANDETAIL, urlConfig);
                logger.debug("MiFin request for MIFINLOANDETAIL refid {} : {}", topUpDedupeRequest.getRefId(), mifinCallRequest);
                mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                        url, MifinReciveResponse.class);
                if (mifinReciveResponse != null) {
                    topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                }
                *//* for hardcoded
           topUpDedupeMifinResponse = miFinHelper.getTempResponseForLoandetail();*//*
                if (topUpDedupeMifinResponse != null) {
                    topUpDedupeMifinResponse.setTopUpType(topUpDedupeRequest.getLoanDetailTypeValue());
                    topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                    topUpDedupeResponsesList.add(topUpDedupeMifinResponse);
                }

            }*/
            if (CollectionUtils.isNotEmpty(topUpDedupeResponsesList)) {
                checkData(topUpDedupeResponsesList, topUpDedupeRequest.getRefId(), MIFINLOANDETAIL, dedupeRequest, urlType, null);
            }
        }else{
            dedupeRequest = buildMiFinLoanDetail(topUpDedupeRequest, MiFinHelper.LOAN_TYPE_B);
            mifinCallRequest = getMifinCallRequest(dedupeRequest, MIFINLOANDETAIL, urlConfig);
            logger.debug("MiFin request for MIFINLOANDETAIL refid {} : {}", topUpDedupeRequest.getRefId(), mifinCallRequest);
            mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest,
                    url, MifinReciveResponse.class);
            if (mifinReciveResponse != null) {
                topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
            }
                /* for hardcoded
           topUpDedupeMifinResponse = miFinHelper.getTempResponseForLoandetail();*/
            if (topUpDedupeMifinResponse != null) {
                topUpDedupeMifinResponse.setTopUpType(topUpDedupeRequest.getLoanDetailTypeValue());
                topUpDedupeMifinResponse = getTopupResponse(mifinReciveResponse);
                topUpDedupeResponsesList.add(topUpDedupeMifinResponse);
            }
            if (CollectionUtils.isNotEmpty(topUpDedupeResponsesList)) {
                checkData(topUpDedupeResponsesList, topUpDedupeRequest.getRefId(), MIFINLMSDETAIL, dedupeRequest, urlType, null);
            }
        }

        }catch (Exception e){
            logger.error("{} in top up thrid Api error", e.getStackTrace());
        }
        return topUpDedupeResponsesList;
    }

    private MifinCallRequest getMifinCallRequest(TopUpMifinDedupeRequest dedupeRequest, String requestType,
                                                 WFJobCommDomain urlConfig) throws JsonProcessingException {
        MifinCallRequest mifinCallRequest = new MifinCallRequest();
        Authentication authentication = getAuthenticationDetails(urlConfig, requestType);
        dedupeRequest.setAuthentication(authentication);
        mifinCallRequest.setReqType(requestType);
        String requestParams = JsonUtil.ObjectToString(dedupeRequest);
        mifinCallRequest.setRequest(requestParams);
        return mifinCallRequest;
    }

    public TopUpMifinDedupeRequest buildMiFinLoanDetail(TopUpDedupeRequest topUpDedupeRequest, String loanType) {

        TopUpMifinDedupeRequest topUpMifinDedupeRequest = TopUpMifinDedupeRequest.builder().build();
        com.softcell.gonogo.model.mifin.topup.BasicInfo basicInfo = com.softcell.gonogo.model.mifin.topup.BasicInfo.builder().build();
        if (CollectionUtils.isNotEmpty(topUpDedupeRequest.getProspectCodeList())) {
            basicInfo.setProspectCodeList(topUpDedupeRequest.getProspectCodeList());
        }
        if (StringUtils.isNotEmpty(loanType)) {
            basicInfo.setLoanDetailType(loanType);
        }
        topUpMifinDedupeRequest.setBasicInfo(basicInfo);
        return topUpMifinDedupeRequest;
    }

    private TopUpDedupeResponse getTopupResponse(MifinReciveResponse mifinReciveResponse) throws IOException, ParseException {
        logger.info("mifinReciveResponse {}", mifinReciveResponse);
        if (mifinReciveResponse != null) {
            String mifinRespnseString = mifinReciveResponse.getResponse();
            MifinTopUpResponse mifinTopUpresponse = JsonUtil.StringToObject(mifinRespnseString, MifinTopUpResponse.class);
            TopUpDedupeResponse topUpDedupeResponse = mifinTopUpresponse.getResponseMifin();
            List<LmsDetail> list = new ArrayList<LmsDetail>();
            List<LoanSummaryDetail> LoansummaryList = new ArrayList<LoanSummaryDetail>();
            if (topUpDedupeResponse != null) {
                if (topUpDedupeResponse.getObject() != null && topUpDedupeResponse.getObject() instanceof List<?>) {
                    list = (List<LmsDetail>) topUpDedupeResponse.getObject();
                    topUpDedupeResponse.setLmsDetailsList(list);
                    logger.info("lmsDetailList as object  {}", list);
                } else if (topUpDedupeResponse.getObject() != null) {
                    topUpDedupeResponse.setLmsDetail(JsonUtil.MapToObject((Map<String, Object>) topUpDedupeResponse.getObject(), LmsDetail.class));
                    list.add(topUpDedupeResponse.getLmsDetail());
                }
                if (topUpDedupeResponse.getLoanSummaryObject() != null && topUpDedupeResponse.getLoanSummaryObject() instanceof List<?>) {
                    LoansummaryList = (List<LoanSummaryDetail>) topUpDedupeResponse.getLoanSummaryObject();
                    topUpDedupeResponse.setLoanSummaryDeatilList(LoansummaryList);

                    logger.info("LoanSummaryDetail as object  {}", list);
                } else if (topUpDedupeResponse.getLoanSummaryObject() != null) {
                    topUpDedupeResponse.setLoanSummaryDeatil(JsonUtil.MapToObject((Map<String, Object>) topUpDedupeResponse.getLoanSummaryObject(), LoanSummaryDetail.class));
                    LoansummaryList.add(topUpDedupeResponse.getLoanSummaryDeatil());
                }
                if (topUpDedupeResponse.getApplicantDetailsList() != null ||
                        CollectionUtils.isNotEmpty(topUpDedupeResponse.getMatchApplicantList())) {
                    DedupeInfo dedupeInfo = new DedupeInfo();
                    dedupeInfo.setApplicantDetailsList(topUpDedupeResponse.getApplicantDetailsList());
                    dedupeInfo.setMatchApplicantList(topUpDedupeResponse.getMatchApplicantList());
                    topUpDedupeResponse.setDedupeInfo(dedupeInfo);
                }
                if (topUpDedupeResponse.getDedupeInfo() != null) {
                    if (CollectionUtils.isNotEmpty(topUpDedupeResponse.getDedupeInfo().getMatchApplicantList())) {
                        List<MatchApplicantDetails> matchApplicantDetailsList = topUpDedupeResponse.getDedupeInfo().getMatchApplicantList();
                        for (MatchApplicantDetails matchApplicantDetails : matchApplicantDetailsList) {
                            // Date come in 01-FEB-2019 format from mifin and convert in 01-02-2019
                            if(StringUtils.isNotEmpty(matchApplicantDetails.getDob())) {
                                String dateStr = GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                                (GngDateUtil.ddMMMyyyy_WITH_HYPHEN,
                                                        matchApplicantDetails.getDob()),
                                        GngDateUtil.ddMMyyyy);

                                matchApplicantDetails.setDob(dateStr);
                            }else{
                                matchApplicantDetails.setDob(Constant.BLANK);
                            }
                        }

                    }
                }
                if(CollectionUtils.isNotEmpty(topUpDedupeResponse.getApplicantlist())){
                    List<com.softcell.gonogo.model.mifin.topup.Applicant> applicantList = topUpDedupeResponse.getApplicantlist() ;
                    for(com.softcell.gonogo.model.mifin.topup.Applicant applicant : applicantList){
                        if(CollectionUtils.isNotEmpty(applicant.getProspectList())){
                            List<ProspectDetails> prospectDetailsList = applicant.getProspectList();
                            for(ProspectDetails  prospectDetails : prospectDetailsList){
                                if(StringUtils.isNotEmpty(prospectDetails.getDisbursalDate())){
                                    String dateStr = GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                                    (GngDateUtil.YyyyMmddTHhMmSs,
                                                            prospectDetails.getDisbursalDate()),
                                            GngDateUtil.ddMMyyyy);

                                    prospectDetails.setDisbursalDate(dateStr);
                                }
                            }
                        }
                    }
                }
            }
            miFinHelper.calTotalExposureAmt(topUpDedupeResponse);
            mifinTopUpresponse.setResponseMifin(topUpDedupeResponse);
            logger.info("Mifin Response {} ", mifinTopUpresponse.getResponseMifin());
            logger.info("mifinTopUpresponse {}", mifinTopUpresponse);
            logger.info("TopUpDedupeResponse {}", mifinTopUpresponse.getResponseMifin());
            return mifinTopUpresponse.getResponseMifin();
        } else {
            return null;
        }
    }

    public boolean checkData(List<TopUpDedupeResponse> topUpDedupeResponseList, String refId, String requestType, TopUpMifinDedupeRequest topUpMifinDedupeRequest, String urlType, String appId) {
        MiFinCallLog mifinApiLog = extenalApiMongoRepository.fetchMiFinCallLogByRequestType(refId, requestType);
        List<TopUpDedupeResponse> topUpDedupeResponsedbList = new ArrayList<TopUpDedupeResponse>();
         boolean isUpdate = false;
      if (mifinApiLog != null && CollectionUtils.isNotEmpty(topUpDedupeResponseList)) {
            mifinApiLog.setDedupeResponseList(topUpDedupeResponseList);
            isUpdate = applicationRepository.updateMifinCallLog(mifinApiLog, refId, requestType, appId);
        } else {
            mifinApiLog = new MiFinCallLog();
            logger.info("saving new condition");
            mifinApiLog.setApi(urlType);
            mifinApiLog.setAppId(appId);
            mifinApiLog.setRefId(refId);
            mifinApiLog.setRequestType(requestType);
            mifinApiLog.setMiFinRequest(topUpMifinDedupeRequest);
            mifinApiLog.setDedupeResponseList(topUpDedupeResponseList);
            externalAPILogRepository.saveMiFinCallLog(mifinApiLog);
        }
        return isUpdate;
    }

    @Override
    public IciciPremiumCalculationResponse calculatePremiumICICICall(String refId, String institutionId,
                                                                     InsuranceRequest insuranceRequest, String initiationPoint, String changedAmount, Applicant applicant) {
        IciciPremiumCalculationResponse response = null;
        try {
            WFJobCommDomain commDomain = getServiceConfiguration(institutionId, insuranceRequest.getHeader().getProduct().name(),
                    PolicyHelper.InsuranceCompany.ICICI.name(), UrlType.INSURANCE_PREMIUM_CALCULATION.toValue());
            DMZResponse connectorResponse = null;
            // Set call log fields
            String requestString = null, responseString = null;
            IciciPremiumCalculationRequest request = null;

            //applicant = applicationRequest.getRequest().getApplicant();
            if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())) {
                for(InsurancePolicy policy : applicant.getInsurancePolicyList()){
                    if(policy.isPremiumHit()){
                        if(StringUtils.equalsIgnoreCase(policy.getCompany(), IciciHelper.ICICI_COMPANY_NAME)) {
                            ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                            // build request
                            request = iciciHelper.buildIciciPremiumCalculationRequest(insuranceRequest, policy, initiationPoint, changedAmount, applicant);
                            requestString = JsonUtil.ObjectToString(request);
                            logger.info("Premium calculation request for {} on action {} is {}", refId, "Demographic Save", requestString);
                            apiLog.setRequestString(requestString);
                            // Call ICICI for insurance premium amount
                            responseString = callICICI(refId, institutionId, commDomain, request, apiLog, requestString);
                            logger.info("Premium calculation response for {} on action {} is {}", refId, "Demographic Save", responseString);
                            apiLog.setResponseString(responseString);
                            if (responseString != null &&  StringUtils.isNotEmpty(responseString)) {
                                try {
                                        connectorResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
                                        if(connectorResponse.getResponseString() != null) {
                                          apiLog.setResponse(JsonUtil.StringToObject(connectorResponse.getResponseString(),
                                                IciciPremiumCalculationResponse.class));
                                        }
                                } catch (Exception e) {
                                    apiLog.setResponse(connectorResponse);
                                     logger.error("{}",e.getStackTrace());
                                    logger.error("Error occurred while response string to object conversion {}", e.getMessage());
                                }
                                // check status
                                if (apiLog.getResponse() instanceof IciciPremiumCalculationResponse) {
                                    if (responseString.contains(IciciHelper.SUCCESS_CODE)) {
                                        response = (IciciPremiumCalculationResponse) apiLog.getResponse();
                                        policy.setPremium(GngCalculationUtils.roundValue(
                                                Double.valueOf(response.getPremiumSummary().getTotalFirstPremium()),0)
                                               );
                                        policy.setSumAssured((GngCalculationUtils.roundValue(
                                                Double.parseDouble(changedAmount),0)
                                        ));
                                        logger.info("{} Successfully uploaded policy to ICICI", refId);
                                        logger.info("Premium calculation response for {} on action {} is {}", refId, "Demographic Save", response);
                                    }else{
                                        policy.setPremium(Double.valueOf("0"));
                                        policy.setSumAssured((GngCalculationUtils.roundValue(
                                                Double.parseDouble(changedAmount),0)
                                        ));
                                        logger.info("{} Successfully uploaded policy to ICICI", refId);
                                        logger.info("Premium calculation response for {} on action {} is {}", refId, "Demographic Save", response);
                                    }
                                    policy.setPremiumHit(false);
                                }else{
                                    policy.setPremium(Double.valueOf("0"));
                                    policy.setSumAssured((GngCalculationUtils.roundValue(
                                            Double.parseDouble(changedAmount),0)
                                    ));
                                }
                                // save req-response in db
                            }else{
                                policy.setPremium(Double.valueOf("0"));
                                policy.setSumAssured((GngCalculationUtils.roundValue(
                                        Double.parseDouble(changedAmount),0)
                                ));
                            }
                            externalAPILogRepository.saveIciciCallLog(apiLog);
                        }
                    }

                }
            }
            //}
        } catch (Exception e) {
            logger.info("Config not found for inst {}, {}, product {}", insuranceRequest.getHeader().getInstitutionId(),
                    UrlType.INSURANCE.toValue(), insuranceRequest.getHeader().getProduct().name()); logger.error("{}",e.getStackTrace());
        }
        return response;
    }

    private String callICICI(String refId, String institution, WFJobCommDomain commDomain, Object request, ThirdPartyApiLog apiLog, String requestString)  {
        String responseString;
        String url = commDomain.getBaseUrl();
        DMZRequest connectorRequest = new DMZRequest();
        connectorRequest.setRequestType("icici_insurance");
        apiLog.setRefId(refId);
        apiLog.setInstitutionId(institution);
        //apiLog.setApplicantId(applicant.getApplicantId());
        apiLog.setRequestType(UrlType.INSURANCE.toValue());

        apiLog.setCallDate(new Date());
        apiLog.setRequestString(requestString);
        apiLog.setRequest(request);

        connectorRequest.setRequestString(requestString);
        List<HeaderKey> headerKeyList = commDomain.getKeys();
        HeaderKey tempKey = new HeaderKey();

        tempKey.setKeyName(IciciHelper.RequestField.URL.getFieldName());
        int index = headerKeyList.indexOf(tempKey);
        if(CollectionUtils.isNotEmpty(headerKeyList))
            connectorRequest.setRequestURL(headerKeyList.get(0).getKeyValue());
        logger.info("{} connecting to url {} with request string {}", refId, url, requestString);
        try{
            responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);
        }catch (Exception e){
            logger.error("{} while response comming");
            responseString = null;
        }
        logger.info("{} received response {} from {}", refId, responseString, url);
        apiLog.setResponseString(responseString);
        return responseString;
    }

    @Override
    public void pushPolicy(InsuranceRequest insuranceRequest, LoanCharges loanCharges) {

        String refId = insuranceRequest.getRefId();
        String institution = insuranceRequest.getHeader().getInstitutionId();

        try{
            Map<String ,WFJobCommDomain> commDomainMap = new HashMap<String, WFJobCommDomain>() ;
            try{
                WFJobCommDomain commDomainICICI = getServiceConfiguration(institution, insuranceRequest.getHeader().getProduct().name(),
                    PolicyHelper.InsuranceCompany.ICICI.name(), UrlType.INSURANCE.toValue());
                commDomainMap.put( PolicyHelper.InsuranceCompany.ICICI.name(), commDomainICICI);
                logger.info("Config : {}", commDomainICICI);
            } catch (Exception e){
                logger.info("Could not find config for ICICI refId : {} : Post Disbursal", insuranceRequest.getRefId());
            }
            try {
                WFJobCommDomain commDomainReligare = getServiceConfiguration(institution, insuranceRequest.getHeader().getProduct().name(),
                        PolicyHelper.InsuranceCompany.RELIGARE.name(), UrlType.RELIGARE.toValue());
                commDomainMap.put( PolicyHelper.InsuranceCompany.RELIGARE.name(), commDomainReligare);
                logger.info("Config : {}", commDomainReligare);
            } catch (Exception e){
                logger.info("Could not find config for RELIGARE refId : {} : Post Disbursal", insuranceRequest.getRefId());
            }
            pushPolicyForApplicant(insuranceRequest, commDomainMap, loanCharges);
        }
        catch (Exception ex){
            ex.printStackTrace();
            logger.info("exception {}" , ex);
        }

    }

     private void pushPolicyForApplicant(InsuranceRequest insuranceRequest, Map<String, WFJobCommDomain> commDomainMap, LoanCharges loanCharges) throws Exception {

        logger.info("In push policy for Applicant for refId : {}", insuranceRequest.getRefId());
        List<Applicant>  applicantList = insuranceRequest.getInsuranceData().getApplicantList();
        if(CollectionUtils.isNotEmpty(applicantList)){
            for(Applicant applicant : applicantList ){
                if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())){
                    List<InsurancePolicy> insurancePolicyList = applicant.getInsurancePolicyList();
                    for(InsurancePolicy insurancePolicy : insurancePolicyList  ){
                        if(PolicyHelper.InsuranceCompany.isInsuranceCompany(insurancePolicy.getCompany(),PolicyHelper.InsuranceCompany.ICICI) &&
                                commDomainMap.get(PolicyHelper.InsuranceCompany.ICICI.name()) != null){
                            callToIciciInsurance(insuranceRequest, loanCharges ,applicant , commDomainMap.get(PolicyHelper.InsuranceCompany.ICICI.name()));
                        }
                        if((PolicyHelper.InsuranceCompany.isInsuranceCompany(insurancePolicy.getCompany(),PolicyHelper.InsuranceCompany.RELIGARE)
                        || StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), ReligareHelper.RELIGARE_LOAN_PROTECTION)) &&
                                commDomainMap.get(PolicyHelper.InsuranceCompany.RELIGARE.name()) != null){
                           callToReligareInsurance(insuranceRequest, commDomainMap.get(PolicyHelper.InsuranceCompany.RELIGARE.name()), loanCharges);
                        }
                    }

                }
            }
        }


    }

    private List<CoApplicant> collectCoapplicantWithInsurance(List<CoApplicant> coAppList) {
       List<CoApplicant> coApplicantList = new ArrayList<CoApplicant>();
        for(CoApplicant coApplicant : coAppList ){
            if(CollectionUtils.isNotEmpty(coApplicant.getInsurancePolicyList())){
                coApplicantList.add(coApplicant);
            }
        }
        return coApplicantList;
    }

    private void callToReligareInsurance(InsuranceRequest insuranceRequest, WFJobCommDomain commDomain, LoanCharges loanCharges) {

        String refId = insuranceRequest.getRefId();
        String institution = insuranceRequest.getHeader().getInstitutionId();
        List<Applicant> applicantList = null;

        try{
            if (commDomain.getBaseUrl() == null)
                throw new GoNoGoException(String.format("%s Configuration not found for %s", refId, UrlType.INSURANCE));

            if(CollectionUtils.isNotEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
              applicantList = insuranceRequest.getInsuranceData().getApplicantList();
            }
            // Check policy and send request per Religare policy
            for(Applicant applicant : applicantList){
                creteRequestForReligareInsurance(insuranceRequest, applicant, commDomain, loanCharges);
            }
            applicationRepository.saveInsuranceData(insuranceRequest);
        }catch (Exception e) {
            logger.info("Config not found for inst {}, {}, product {}\nException is {}",
                    insuranceRequest.getHeader().getInstitutionId(), UrlType.INSURANCE.toValue(),
                    insuranceRequest.getHeader().getProduct(), e.getStackTrace());
        }
    }

    private void creteRequestForReligareInsurance(InsuranceRequest insuranceRequest, Applicant applicant, WFJobCommDomain commDomain, LoanCharges loanCharges) {
        String refId = insuranceRequest.getRefId();
        String institution = insuranceRequest.getHeader().getInstitutionId();
        if (CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())){
            List<InsurancePolicy> appliedInsurances = applicant
                    .getInsurancePolicyList()
                    .stream()
                    .filter(policy ->
                            PolicyHelper.InsuranceCompany.isInsuranceCompany(
                                    policy.getCompany(), PolicyHelper.InsuranceCompany.RELIGARE)
                  || StringUtils.equalsIgnoreCase(policy.getCompany(), ReligareHelper.RELIGARE_LOAN_PROTECTION))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(appliedInsurances)) {
                try{
                    CreatePolicySecure createPolicySecure = null;
                    String url = commDomain.getBaseUrl();
                    DMZRequest connectorRequest = new DMZRequest();
                    DMZResponse connectorResponse = null;
                    connectorRequest.setRequestType("religare_insurance");
                    String requestString = null, responseString = null;

                    List<HeaderKey> headerKeyList = commDomain.getKeys();
                    HeaderKey tempKey = new HeaderKey();

                    tempKey.setKeyName(IciciHelper.RequestField.URL.getFieldName());
                    int index = headerKeyList.indexOf(tempKey);
                    if( CollectionUtils.isNotEmpty(headerKeyList))
                        connectorRequest.setRequestURL(headerKeyList.get(0).getKeyValue());
                    connectorRequest.setHeaderKeyList(headerKeyList);
                    ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                    apiLog.setRefId(refId);
                    apiLog.setInstitutionId(institution);
                    apiLog.setApplicantId(applicant.getApplicantId());
                    apiLog.setRequestType(UrlType.RELIGARE.toValue());

                    for(InsurancePolicy insurancePolicy : appliedInsurances ){
                        connectorResponse = null;
                         createPolicySecure = religareHelper.buildReligareRequest(insuranceRequest, applicant, insurancePolicy ,  loanCharges );
                        requestString = JsonUtil.ObjectToString(createPolicySecure);
                        logger.info("{} connecting to url {} with request string {}", refId, url, requestString);
                        connectorRequest.setRequestString(requestString);
                        apiLog.setCallDate(new Date());
                        apiLog.setRequestString(requestString);
                        apiLog.setRequest(createPolicySecure);
                        responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);
                        logger.info("{} received response {} from {}", refId, responseString, url);
                        apiLog.setResponseString(responseString);
                        if (responseString != null) {
                            try {
                                Religare religare = null;
                                callReligarePushApi(responseString, religare,connectorResponse, insurancePolicy, apiLog);
                                /********If it Fails for first time then as per requirement call
                                 one more time************************************/
                                if(StringUtils.equalsIgnoreCase(insurancePolicy.getStatus(),InsurancePolicyEvaluator.STATUS_FAILED)){
                                    responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);
                                    callReligarePushApi(responseString, religare,connectorResponse, insurancePolicy, apiLog);
                                }
                            } catch (Exception e) {
                                apiLog.setResponse(connectorResponse);
                                logger.error("Error occurred while response string to object conversion {}", e.getStackTrace());
                            }
                        }
                    }
                } catch(Exception ex){

                }
            }
        }else {
            logger.info("{} No insurance opted.", refId);
        }
    }

    private void callReligarePushApi(String responseString, Religare religare, DMZResponse connectorResponse,
                                     InsurancePolicy insurancePolicy, ThirdPartyApiLog apiLog) {
        try{
            logger.debug("in religare push ");
            connectorResponse = (DMZResponse) JsonUtil.StringToObject(responseString, DMZResponse.class);
            religare = JsonUtil.StringToObject(connectorResponse.getResponseString(),
                    Religare.class);
            apiLog.setResponse(JsonUtil.StringToObject(connectorResponse.getResponseString(),
                    Religare.class));
            externalAPILogRepository.saveIciciCallLog(apiLog);
            if(religare.getIntPolicyDataIO() != null && religare.getIntPolicyDataIO().getPolicy() != null &&
                    CollectionUtils.isEmpty(religare.getIntPolicyDataIO().getErrorLists()) &&
                    CollectionUtils.isEmpty(religare.getIntPolicyDataIO().getListErrorListList())){
                if(religare.getIntPolicyDataIO().getPolicy().getProposalNum() != null){
                    logger.info("set refNo {}", religare.getIntPolicyDataIO().getPolicy().getProposalNum());
                    insurancePolicy.setPolicyNumber(religare.getIntPolicyDataIO().getPolicy().getProposalNum());
                    insurancePolicy.setStatus(InsurancePolicyEvaluator.STATUS_REGISTERED);
                }
            }else{
                insurancePolicy.setStatus(InsurancePolicyEvaluator.STATUS_FAILED);
            }
        }
        catch (Exception ex ){
            logger.error("{} religare Error ", ExceptionUtils.getStackTrace(ex));
        }
    }
    public void callToIciciInsurance(InsuranceRequest insuranceRequest, LoanCharges loanCharges , Applicant applicant, WFJobCommDomain commDomain) throws Exception {
        String refId = insuranceRequest.getRefId();
        String institution = insuranceRequest.getHeader().getInstitutionId();

        try {
            logger.info("{} Calls started for {}, ICICI Config : {}", insuranceRequest.getRefId(), UrlType.INSURANCE.toValue(), commDomain);
            if(commDomain.getBaseUrl() == null) {
                commDomain = getServiceConfiguration(institution, insuranceRequest.getHeader().getProduct().name(),
                        PolicyHelper.InsuranceCompany.ICICI.name(), UrlType.INSURANCE.toValue());
                logger.info("Again fetched ICICI Config : {}",commDomain);
            }
            if (commDomain.getBaseUrl() == null)
                throw new GoNoGoException(String.format("%s Configuration not found for %s", refId, UrlType.INSURANCE));

            // Check policy and send request per icici policy
            callToIciciInsurance(insuranceRequest, applicant, loanCharges, commDomain);
            applicationRepository.saveInsuranceData(insuranceRequest);

            logger.info("{} Calls ended for {}", refId, UrlType.INSURANCE.toValue());
        } catch (Exception e) {
            logger.info("Config not found for inst {}, {}, product {}\nException is {}",
                    insuranceRequest.getHeader().getInstitutionId(), UrlType.INSURANCE.toValue(),
                    insuranceRequest.getHeader().getProduct(), e.getStackTrace());
        }
    }

    private void callToIciciInsurance(InsuranceRequest insuranceRequest , Applicant applicant, LoanCharges loanCharges,
                                      WFJobCommDomain commDomain) {
        String refId = insuranceRequest.getRefId();
        String institution = insuranceRequest.getHeader().getInstitutionId();
        String policyNo = null;
        if (CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())) {
            /* Iterate through the list
            *       For ICICI policy
                *       build request
                *       call connector api
                *       save request-response in db
            *       */
            List<InsurancePolicy> appliedInsurances = applicant
                    .getInsurancePolicyList()
                    .stream()
                    .filter(policy ->
                            PolicyHelper.InsuranceCompany.isInsuranceCompany(
                                    policy.getCompany(), PolicyHelper.InsuranceCompany.ICICI))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(appliedInsurances)) {
                try {
                    // Iterate list TODO use async calls
                    IciciRequest request = null;
                    String url = commDomain.getBaseUrl();
                    DMZRequest connectorRequest = new DMZRequest();
                    DMZResponse connectorResponse = null;
                    connectorRequest.setRequestType("icici_insurance");
                    if(CollectionUtils.isNotEmpty(commDomain.getKeys())){
                        connectorRequest.setRequestURL(commDomain.getKeys().get(0).getKeyValue());
                    }
                    String requestString = null, responseString = null;
                    // Set call log fields
                    ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                    apiLog.setRefId(refId);
                    apiLog.setInstitutionId(institution);
                    apiLog.setApplicantId(applicant.getApplicantId());
                    apiLog.setRequestType(UrlType.INSURANCE.toValue());
                    for (InsurancePolicy policy : appliedInsurances) {
                        connectorResponse = null;
                        // build request
                        if(policy.getPremium() > 0){
                            request = (IciciRequest) policyHelper.buildInsuranceRequest(insuranceRequest, applicant,
                                    loanCharges.getDisbursedDate(), policy, commDomain);
                            requestString = JsonUtil.ObjectToString(request);
                            logger.info("{} connecting to url {} with request string {}", refId, url, requestString);
                            connectorRequest.setRequestString(requestString);

                            apiLog.setCallDate(new Date());
                            apiLog.setRequestString(requestString);
                            apiLog.setRequest(request);
                            // call api
                            connectorResponse = (DMZResponse)  TransportUtils.postJsonRequest(connectorRequest,
                                    commDomain.getBaseUrl(), DMZResponse.class);
                            //ToDo remove after testing
                            //responseString ="{\"sResponse\":\"{\\\"responseCode\\\":\\\"E00\\\",\\\"BREDecision\\\":\\\"J2\\\",\\\"responseRemarks\\\":\\\"LC\\\",\\\"modalPremium\\\":\\\"6090\\\",\\\"BREAction\\\":\\\"Refer to CUW\\\",\\\"baseCounterOffer\\\":\\\"0\\\",\\\"lifeOption\\\":\\\"\\\",\\\"transID\\\":\\\"OS90171703\\\",\\\"annualPremiumWithTax\\\":\\\"null\\\",\\\"ciCounterOffer\\\":\\\"0\\\",\\\"URL\\\":\\\"https://iprusalesbeta.com/digiuatnew/apptrackerhomeNew.htm?appNumber=elZhZGN5bmVVc3hFYkpXdXZHUUM0QT09&dob=c1FGZXhSZ2NSejhObVdaSGJXUWJLZz09&source=1\\\",\\\"adbrCounterOffer\\\":\\\"0\\\",\\\"isMedical\\\":\\\"false\\\"}\",\"sErrorCode\":null,\"sErrorMsg\":null}";
                            responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);
                            logger.info("{} received response {} from {}", refId, responseString, url);
                            apiLog.setResponseString(responseString);
                            if (responseString != null) {
                                try {
                                    connectorResponse = (DMZResponse) JsonUtil.StringToObject(responseString, DMZResponse.class);
                                    apiLog.setResponse(JsonUtil.StringToObject(connectorResponse.getResponseString(),
                                            IciciResponse.class));
                                } catch (Exception e) {
                                    apiLog.setResponse(connectorResponse);
                                     logger.error("{}",e.getStackTrace());
                                    logger.error("Error occurred while response string to object conversion {}", e.getStackTrace());
                                }
                                // check status
                                if (apiLog.getResponse() instanceof IciciResponse) {
                                    if (responseString.contains(IciciHelper.SUCCESS_CODE)) {
                                        IciciResponse iciciResponse = (IciciResponse) apiLog.getResponse();
                                        policy.setStatus(InsurancePolicyEvaluator.STATUS_REGISTERED);
                                        policy.setPolicyNumber(iciciResponse.transID);
                                        policy.setUrl(iciciResponse.uRL);
                                        logger.info("{} Successfully uploaded policy to ICICI", refId);
                                    }
                                }
                            }
                            // save req-response in db
                            externalAPILogRepository.saveIciciCallLog(apiLog);
                        }else{
                            //If policy no is less then 0
                            policy.setStatus(InsurancePolicyEvaluator.STATUS_FAILED);
                        }
                    }

                } catch (Exception e) {
                    logger.error("{} Exception occoured while generating insurance  message is {} stacktrace {}",
                            insuranceRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
                }
            } else {
                logger.info("{} Not opted ICICI insurance.", refId);
            }
        } else {
            logger.info("{} No insurance opted.", refId);
        }
    }

    @Override
    public MiFinCallLog evaluateMifinDedup(ApplicationRequest applicationRequest){
        MiFinCallLog miFinCallLog = null;
        try {
            Applicant applicant =  applicationRequest.getRequest().getApplicant();
            String urlType = UrlType.TOP_UP_DEDUPE.toValue();
            WFJobCommDomain mifinDedupeConfiguaration = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);

            Authentication authentication = getAuthenticationDetails(mifinDedupeConfiguaration, MIFIN_DEDUPE_DETAIL);
            List<TopUpDedupeResponse> topUpDedupeMifinResponseList = new ArrayList<TopUpDedupeResponse>();

            String url = Arrays.asList(mifinDedupeConfiguaration.getBaseUrl(), mifinDedupeConfiguaration.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            MifinCallRequest mifinCallRequest = null;
            TopUpMifinDedupeRequest miFinRequest = null;
            MifinReciveResponse mifinReciveResponse = null;
            String requestParams = null;

            if (applicant != null) {
                miFinRequest = buildMiFinDedupeRequest(applicationRequest);
                miFinRequest.setAuthentication(authentication);
                requestParams = JsonUtil.ObjectToString(miFinRequest);
                logger.info("request sending to mifin {}", requestParams);

                if (miFinRequest != null) {
                    mifinCallRequest = new MifinCallRequest();
                    mifinCallRequest.setReqType(MIFIN_DEDUPE_DETAIL);
                    mifinCallRequest.setRequest(requestParams);
                    logger.info("request after String {}", mifinCallRequest);
                    logger.info("MiFin request in applicant : {} for refId: {}", mifinCallRequest, applicationRequest.getRefID());

                    mifinReciveResponse = (MifinReciveResponse) TransportUtils.postJsonRequest(mifinCallRequest, url, MifinReciveResponse.class);
                    topUpDedupeMifinResponseList.add(getTopupResponse(mifinReciveResponse));
                }
            }
            logger.info("responseList {}", topUpDedupeMifinResponseList);

            if(CollectionUtils.isNotEmpty(topUpDedupeMifinResponseList)) {
                miFinCallLog = new MiFinCallLog();
                miFinCallLog.setApi(urlType);
                miFinCallLog.setRequestType(MIFIN_DEDUPE_DETAIL);
                miFinCallLog.setMiFinRequest(miFinRequest);
                miFinCallLog.setDedupeResponseList(topUpDedupeMifinResponseList);
            }
        }catch (Exception e){
            logger.error("Error while getting dedup from Mifin {}", e.getMessage());
        }

        return miFinCallLog;
    }

    @Override
    public CrmResponse fetchCRMDedupeUsingHeaders(LoginDataRequest loginDataRequest){
        CrmResponse crmResponse = null;
        try {
            String urlType = UrlType.CRM_DEDUPE.toValue();
            WFJobCommDomain crmDedupeConfiguaration = getServiceConfiguration(loginDataRequest.getHeader().getInstitutionId(), urlType);
            String connectorUrl = crmDedupeConfiguaration.getBaseUrl();

            Map<String, String> keyMap = new HashMap<>();
            if (CollectionUtils.isNotEmpty(crmDedupeConfiguaration.getKeys())) {
                for (HeaderKey key : crmDedupeConfiguaration.getKeys()) {
                    keyMap.put(key.getKeyName(), key.getKeyValue());
                }
            }

            DMZRequest dmzRequest = new DMZRequest();
            dmzRequest.setRequestType(urlType);
            dmzRequest.setRequestURL(keyMap.get("URL"));

            Map<String, String> headerMap = new HashMap<>();
            getAuthenticationDetails(headerMap);
            getCustomerDetails(headerMap, loginDataRequest.getRegistrationData().getPhoneNumber());

            List<HeaderKey> headerKeyList = headerMap.entrySet().stream()
                    .map(header -> {
                        HeaderKey headerKey = new HeaderKey();
                        headerKey.setKeyName(header.getKey());
                        headerKey.setKeyValue(header.getValue());
                        return headerKey;
                    }).collect(Collectors.toList());

            dmzRequest.setHeaderKeyList(headerKeyList);

            logger.info("sending request to crm for redId{}", loginDataRequest.getRefId());
            String responseString = new HttpTransportationService().postRequest(connectorUrl, JsonUtil.ObjectToString(dmzRequest), MediaType.APPLICATION_JSON_VALUE);
            logger.debug("{} response {}", loginDataRequest.getRefId(), responseString);

            DMZResponse dmzResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
            crmResponse = JsonUtil.StringToObject(dmzResponse.getResponseString(), CrmResponse.class);

        } catch (Exception e) {
            logger.error("{ } refId, Exception occurred while fetchCRMDedupe Details. Exception {}", loginDataRequest.getRefId(), ExceptionUtils.getStackTrace(e));
        }
        return crmResponse;
    }

    private void getAuthenticationDetails(Map<String, String> headerMap){
        headerMap.put("APP_NAME", "MIFIN");
        headerMap.put("APP_PASS", "PASS@123");
        headerMap.put("IP_ADDRESS", "10.1.1.3");
        headerMap.put("DEVICE_ID", "DIGIPL_CRM_2");
        headerMap.put("LONGITUDE", "77.3877269");
        headerMap.put("LATITUDE", "28.6127356");
    }

    private void getCustomerDetails(Map<String, String> headerMap, String personalPhoneNum){
        logger.info("CRM getCustomerDetails");
        headerMap.put("Customer_Full_Name", Constant.BLANK);
        headerMap.put("Mobile_Number",personalPhoneNum);
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId, String product, String serviceId, String urlType) throws GoNoGoException {
        logger.info("fetching config against instId {} , utlType {}, product {}", institutionId, urlType, product);
        WFJobCommDomain connectionConfig = null;
        if (serviceId != null) {
            connectionConfig = workFlowCommunicationManager.getWfCommDomainJob(
                    institutionId, product, serviceId, urlType);
        } else {
            connectionConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    institutionId, product, urlType);
        }

        if (null == connectionConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return connectionConfig;
    }

    @Override
    public void callToMiFin(ApplicationRequest applicationRequest, String loggedInUserRole, String loggedInUserId) throws Exception {
        String userId = ambitMifinManager.getUser(applicationRequest.getHeader(), loggedInUserId);

        if (StringUtils.isEmpty(applicationRequest.getMiFinProspectCode())) {
            ambitMifinManager.createLoan(applicationRequest, userId);
        }
        if (StringUtils.isNotEmpty(applicationRequest.getMiFinProspectCode()) && !(applicationRequest.getMiFinProspectCode().contains("null"))) {
            ambitMifinManager.saveApplicantDetails(applicationRequest, userId);
            if (!(applicationRequest.getRequest().getCoApplicant().isEmpty()) && null != applicationRequest.getRequest().getCoApplicant()) {
                ambitMifinManager.saveCoApplicantAddress(applicationRequest, userId);
                ambitMifinManager.saveApplicantDetails(applicationRequest, userId);
            }
            if (null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                if (StringUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMiFinApplicantCode()) && !(applicationRequest.getMiFinProspectCode().contains("null"))) {
                    ambitMifinManager.saveApplicantAddress(applicationRequest, userId);
                }
            }
        }
        if(applicationRequest.getRequest().getMifinDedupeReinitiateFlag()){
            ambitMifinManager.processDedupe(applicationRequest, userId);
        }
    }

    @Override
    public void callToMiFinSaveData(GoNoGoCustomerApplication goNoGoCustomerApplication,  String loggedInUserRole, String loggedInUserId) throws Exception {
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        String currentStage = applicationRequest.getCurrentStageId();
        String refId = goNoGoCustomerApplication.getGngRefId();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
        String executionPoint = null;
        String userId = ambitMifinManager.getUser(applicationRequest.getHeader(), loggedInUserId);

        ActivityLogs activityLogs = auditHelper.createActivityLog(null,applicationRequest.getHeader());
        activityLogs.setAction("MIFIN_SAVE_DATA");
        activityLogs.setStage(currentStage);
        activityLogs.setStep(ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_SAVE_DATA);
        activityLogs.setRefId(refId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        if (StringUtils.equalsIgnoreCase(currentStage, GNGWorkflowConstant.HOPS.toFaceValue())) {
            executionPoint = "submit-by-" + loggedInUserRole;
            applicationRepository.updateCompletedInfo(refId, institutionId, executionPoint, loggedInUserId, loggedInUserRole);
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
        }
        if (StringUtils.isNotEmpty(applicationRequest.getMiFinProspectCode())) {
            if (StringUtils.isEmpty(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMifinCRDecisionId())
                    && ambitMifinManager.savePersonalInsurance(applicationRequest, userId)
                    && ambitMifinManager.updateLoanDetail(applicationRequest, userId)
                    && ambitMifinManager.saveVerification(applicationRequest, userId)
                    && ambitMifinManager.crDecision(applicationRequest, userId)) {
                logger.info("Mifin Apis SaveVerification , UpdateLoanDetail ,SaveCrDecision are successfull");
            }
            if(StringUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMifinCRDecisionId())
                    && ambitMifinManager.saveDisbursalMaker(applicationRequest, userId)) {
                logger.info("Mifin Apis SaveVerification , UpdateLoanDetail ,SaveCrDecision , SavePersonalInsurance and SaveDisbursalMaker are successfull");
                applicationRequest.setCurrentStageId(GNGWorkflowConstant.DISB.toFaceValue());
                applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISB.toFaceValue(),
                        applicationRequest.getCurrentStageId())) {
                    applicationStatus = GNGWorkflowConstant.DISBURSED.toFaceValue();
                }
            } else {
                if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(),
                        applicationRequest.getCurrentStageId())) {
                    logger.info("Attention {} ...Revering to stage HOPS and status Approved!!", applicationRequest.getRefID());
                    applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                }
            }
        } else {
            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(),
                    applicationRequest.getCurrentStageId())) {
                logger.info("Attention {} ...Revering to stage HOPS and status Approved!!", applicationRequest.getRefID());
                applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
        }
        applicationRepository.updateStageId(applicationRequest, applicationStatus);
        applicationRepository.updateCompletedInfo(refId, institutionId, executionPoint, loggedInUserId, loggedInUserRole);
        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
    }

    @Override
    public void callToMiFinDeleteApplicant(ApplicationRequest applicationRequest, List<String> coApplicantIds, String loggedInUserRole, String loggedInUserId) throws Exception {
        String userId = ambitMifinManager.getUser(applicationRequest.getHeader(), loggedInUserId);
        ambitMifinManager.deleteMifinApplicant(applicationRequest , coApplicantIds, userId);
    }

    @Override
    public void pushIMDDataToMifin(GoNoGoCustomerApplication gngCustApplication, InitialMoneyDeposit imd2) throws Exception {
        logger.debug("in pushIMDDataToMifin for refId {}",gngCustApplication.getGngRefId());
        String institutionId = gngCustApplication.getApplicationRequest().getHeader().getInstitutionId();
        String urlType = UrlType.MIFINPUSHIMD.toValue();
        String refId = gngCustApplication.getGngRefId();
        DMZRequest connectorRequest = new DMZRequest();
        WFJobCommDomain wfJobCommDomain;
        MiFinResponse miFinResponse = null;
        DMZResponse dmzResponse = null;

        IMDMiFinRequest miFinRequest = miFinHelper.builIMDRequest(gngCustApplication, imd2);
        wfJobCommDomain = getServiceConfiguration(institutionId, urlType);
        if (wfJobCommDomain != null) {
            String url = Arrays.asList(wfJobCommDomain.getBaseUrl(), wfJobCommDomain.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            Authentication authentication = getAuthenticationDetails(wfJobCommDomain, "");
            miFinRequest.setAuthentication(authentication);
            String valueJson = JsonUtil.ObjectToString(miFinRequest);
            logger.info("mifin request for mifin_push_imd - {} ",valueJson);
            connectorRequest.setRequestString(valueJson);
            connectorRequest.setRequestURL(wfJobCommDomain.getUrl());
            connectorRequest.setRequestType(urlType);
            logger.info("requesType {}, url {}, final connector request for mifin_push_imd - {} " ,connectorRequest.getRequestType(),
                    url, JsonUtil.ObjectToString(connectorRequest));
            String responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest),
                    null, MediaType.APPLICATION_JSON_VALUE);
            logger.info("MiFin connector response of mifin_push_imd : {} for refId: {}", responseString, refId);
            try {
                if(StringUtils.isNotEmpty(responseString)) {
                    dmzResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
                    miFinResponse = JsonUtil.StringToObject(dmzResponse != null ? dmzResponse.getResponseString() : null, MiFinResponse.class);
                } else {
                    logger.error("empty response from mifin_push_imd api for refId {}",refId);
                    throw new Exception("empty response from mifin");
                }
            } catch (Exception e) {
                logger.error("error while parcing mifin response of mifin_push_imd api for refId {}, ex : {}",refId, e.getStackTrace());
                throw new Exception("inappropriate response from mifin, failed while deserialize mifin response");
            }
            logger.info("MiFin response of mifin_push_imd : {} for refId: {}", JsonUtil.ObjectToString(miFinResponse), refId);

            // Save to external API log
            MiFinCallLog apiLog = new MiFinCallLog();
            apiLog.setApi(urlType);
            apiLog.setRefId(refId);
            apiLog.setMiFinRequest(miFinRequest);
            apiLog.setMiFinResponse(miFinResponse);
            apiLog.setRequest(valueJson);
            externalAPILogRepository.saveMiFinCallLog(apiLog);
            if(miFinResponse != null && miFinResponse.getMiFinBaseResponse() != null ) {
                if (StringUtils.equals(miFinResponse.getMiFinBaseResponse().getStatus(), MiFinHelper.STATUS_SUCCESS)) {
                    gngCustApplication.getApplicationRequest().setMifinImd2Status(miFinResponse.getMiFinBaseResponse().getStatus());
                    gngCustApplication.getApplicationRequest().setMifinImd2Message(miFinResponse.getMiFinBaseResponse().getMessage());
                } else {
                    gngCustApplication.getApplicationRequest().setMifinImd2Status(MiFinHelper.STATUS_FAILED);
                    gngCustApplication.getApplicationRequest().setMifinImd2Message(miFinResponse.getMiFinBaseResponse().getMessage());
                    imd2.setStatus("Pending");
                    imd2.setCleared(false);
                    logger.error("Mifin response - {} refId {}",miFinResponse.getMiFinBaseResponse().getMessage(), refId);
                }
            } else {
                gngCustApplication.getApplicationRequest().setMifinImd2Status(MiFinHelper.STATUS_FAILED);
                gngCustApplication.getApplicationRequest().setMifinImd2Message("Got null/empty response from Mifin");
                imd2.setStatus("Pending");
                imd2.setCleared(false);
                logger.error("Got null/empty response from Mifin for mifin_push_imd for refId {}", refId);
            }
        } else {
            throw new Exception("no wf_communication_settings found for mifin_push_imd call for refId {}"+refId);
        }
    }
}




