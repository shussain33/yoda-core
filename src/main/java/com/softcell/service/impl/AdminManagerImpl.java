package com.softcell.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.ActionName;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.AdminLogRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.kyc.TotalKycReqResLog;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.GenericMsgResponse;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.security.ApplicationStatusAndStage;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.ModifyAppStageAndStatusRequest;
import com.softcell.gonogo.model.sms.SmsServReqResLogRequest;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.model.tclos.log.TcLosReqReslog;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.service.AdminManager;
import com.softcell.service.CroManager;
import com.softcell.service.DocumentStoreManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.CDL;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by yogeshb on 10/2/17.
 */

@Service
public class AdminManagerImpl implements AdminManager {

    private static final Logger logger = LoggerFactory.getLogger(AdminManagerImpl.class);

    private static final String MESSAGE ="message";
    private static final String DECISION_MESSAGE_FORMAT_ERROR ="File not proper; <Status>\n<Message>\n<refid>\n<refId>\n...";

    @Autowired
    private CroManager croManager;

    @Autowired
    private AdminLogRepository adminLogRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Override
    public BaseResponse getPanLog(String refID) throws Exception {
        PanRequest panLog = adminLogRepository.getPanLog(refID);
        if (null != panLog) {
            return GngUtils.getBaseResponse(HttpStatus.OK, panLog);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

    }

    @Override
    public BaseResponse getAadharLog(String refID) throws Exception {
        AadhaarRequest aadharLog = adminLogRepository.getAadharLog(refID);

        if (null != aadharLog) {
            return GngUtils.getBaseResponse(HttpStatus.OK, aadharLog);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getMbLog(String refID) throws Exception {
        RequestJsonDomain mbLog = adminLogRepository.getMbLog(refID);
        if (null != mbLog) {
            return GngUtils.getBaseResponse(HttpStatus.OK, mbLog);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getMbLogWithCoApplicant(String refID) throws Exception {
        List<RequestJsonDomain> mbLogWithCoapplicant = adminLogRepository.getMbLogWithCoapplicant(refID);

        if (null != mbLogWithCoapplicant && !mbLogWithCoapplicant.isEmpty()) {
            return GngUtils.getBaseResponse(HttpStatus.OK, mbLogWithCoapplicant);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getScoringLog(String refID) throws Exception {
        ScoringApplicationRequest scoringLog = adminLogRepository.getScoringLog(refID);

        if (null != scoringLog ) {
            return GngUtils.getBaseResponse(HttpStatus.OK, scoringLog);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getMainLog(String refID) throws Exception {
        ApplicationRequest mainLog = adminLogRepository.getMainLog(refID);
        if (null != mainLog ) {
            return GngUtils.getBaseResponse(HttpStatus.OK, mainLog);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse doCacheRefresh(LoginRequest loginRequest) throws Exception {

        logger.debug("doCacheRefresh service is started");

        String userID = loginRequest.getUserName();
        String password = loginRequest.getPassword();

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(Status.FAILED.name());

        if (StringUtils.equals(userID, "yogesh") && StringUtils.equals(password, "1234554321")) {

            if (Cache.serviceCallForRefreshCache()) {

                genericResponse.setStatus(Status.OK.name());

            }
        }

        logger.debug("doCacheRefresh service is completed");

        return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
    }

    @Override
    public BaseResponse getCsvFromJson(List<OtpLog> json) throws JsonProcessingException {

        logger.debug("getCsvFromJson service started ");

        String jsonArrayString = JsonUtil.ObjectToString(json);

        JSONArray jsonArray = new JSONArray(jsonArrayString);

        logger.debug("getCsvFromJson service completed ");

        return GngUtils.getBaseResponse(HttpStatus.OK, CDL.toString(jsonArray));
    }

    @Override
    public BaseResponse updateDealerName(LoginRequest loginRequest) throws Exception {

        logger.debug("updateDealerName service started ");

        BaseResponse baseResponse;

        if (StringUtils.equals(loginRequest.getUserName(), "james_@_bond_@_123")
                && StringUtils.equals(loginRequest.getPassword(), "itsacrimetoupdate@Password123")
                && StringUtils.isNotBlank(loginRequest.getRefId())) {

            if (adminLogRepository.updateDealerName(loginRequest.getRefId())) {

                GenericResponse genericResponse = new GenericResponse();

                genericResponse.setStatus(Status.OK.name());

                baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);

            }
            else {
                baseResponse= GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
            }
        }else {
            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse= GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors);
        }

        logger.debug("updateDealerName service completed");

        return baseResponse;


    }



    @Override
    public BaseResponse getSingleBucketData(String refID) throws Exception {
        GoNoGoCroApplicationResponse singleBucketData = adminLogRepository.getSingleBucketData(refID);
        if (null != singleBucketData) {
            return GngUtils.getBaseResponse(HttpStatus.OK, singleBucketData);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public BaseResponse getLogData() throws Exception {

        logger.debug("getLogData service started ");

        List<OtpLog> workFlowLogList = adminLogRepository.getLogData();

        if (workFlowLogList != null && !workFlowLogList.isEmpty()) {

            logger.debug("getLogData service workFlowLogList size {}", workFlowLogList.size());

            for (OtpLog workFlowLog : workFlowLogList) {

                workFlowLog.setStartTimeToString(new DateTime(workFlowLog.getStartTime()).toString());

                workFlowLog.setEndTimeToString(new DateTime(workFlowLog.getEndTime()).toString());

                workFlowLog
                        .setTurnAroundTime(new DateTime(workFlowLog.getEndTime()).getMillis()
                                - new DateTime(workFlowLog.getStartTime()).getMillis());
            }

        }

        logger.debug("getLogData service completed ");

        return GngUtils.getBaseResponse(HttpStatus.OK, workFlowLogList);

    }

    @Override
    public BaseResponse updateStageAndStatus(LoginRequest loginRequest) throws Exception {

        logger.debug("updateStageAndStatus service started ");

        BaseResponse baseResponse ;

        if (StringUtils.equals(loginRequest.getUserName(), "james_@_bond_@_123")
                && StringUtils.equals(loginRequest.getPassword(), "itsacrimetoupdate@Password123")
                && StringUtils.isNotBlank(loginRequest.getRefId())) {


            if (adminLogRepository.updateStageAndStatus(loginRequest.getRefId(), GNGWorkflowConstant.APPROVED.toFaceValue(), GNGWorkflowConstant.APRV.toFaceValue())) {

                GenericResponse genericResponse = new GenericResponse();

                genericResponse.setStatus(Status.OK.name());

               baseResponse= GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);

            }else {
                baseResponse= GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors);
        }

        logger.debug("updateStageAndStatus service completed");

        return baseResponse;
    }

    @Override
    public BaseResponse getSalesForceLog(SalesForceLog salesForceLog) throws Exception {
        logger.debug("getSalesForceLog service started");
        BaseResponse baseResponse = null;
        SalesForceLog salesForceLogData = null;
            salesForceLogData = adminLogRepository.getSalesForceLog(salesForceLog);
            if (salesForceLogData != null) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, salesForceLogData);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        return baseResponse;
    }


    public BaseResponse getRawResponse (AdminLogRequest adminLogRequest){

        logger.debug("getRawResponse service started");

        BaseResponse baseResponse = null;

        List<RawResponseLog> rawResponseLogList = adminLogRepository.getRawResponse(adminLogRequest);

        if (!CollectionUtils.isEmpty(rawResponseLogList)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, rawResponseLogList);

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }

    @Override
    public List<GetUserProfileCallLog> getCreditVidyaResponse(CreditVidyaLogRequest creditVidyaLogRequest) {
        return externalAPILogRepository.fetchCreditVidyaResponse(creditVidyaLogRequest);
    }

    @Override

    public List<SaathiCallLog> getSaathiResponse(SaathiLogRequest saathiLogRequest) {
        return externalAPILogRepository.fetchSaathiResponse(saathiLogRequest);
    }

    @Override
    public BaseResponse getTkycLog(TKycLogRequest tKycLogRequest) {
        BaseResponse baseResponse = null;
        TotalKycReqResLog totalKycReqResLog = externalAPILogRepository.getTKycLog(tKycLogRequest);
        if (totalKycReqResLog != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, totalKycReqResLog);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse cancelDoNotRaisedApplication(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception{
        BaseResponse baseResponse = null;
        int breakCount = 1;
        int skip = 0;
        int limit;


        if (cancelDoNotRaisedApplicationRequest.getBatchCount() > 0 && cancelDoNotRaisedApplicationRequest.getBatchCount() < 100) {
            limit = cancelDoNotRaisedApplicationRequest.getBatchCount();
        } else {
            limit = 100;
        }

        List<String> cancelledApplicationList = null;

        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationsList = null;


        if (cancelDoNotRaisedApplicationRequest.getPastNoOfDays() > 0) {

            logger.info("cancelling {} no of days before cases",cancelDoNotRaisedApplicationRequest.getPastNoOfDays());

            cancelledApplicationList = new ArrayList<>();

            Date todaysZeroTmDt =GngDateUtil.getZeroTimeFromDate(new Date());

            Date noOfDaysBeforeDt = new Date(todaysZeroTmDt.getTime() - cancelDoNotRaisedApplicationRequest.getPastNoOfDays() * 24 * 3600 * 1000l);

            // get formatted date
            Date formattedDate = GngDateUtil.getZeroTimeFromDate(noOfDaysBeforeDt);

            logger.info("{} no of days before date to cancel case is {}",cancelDoNotRaisedApplicationRequest.getPastNoOfDays(),formattedDate);

            while (breakCount > 0) {
                goNoGoCustomerApplicationsList = adminLogRepository.getApplicationLessThanDateForCancelPurpose(formattedDate, cancelDoNotRaisedApplicationRequest.getHeader().getInstitutionId(), limit, skip);

                startCancelApplicationProcess(goNoGoCustomerApplicationsList, cancelDoNotRaisedApplicationRequest, cancelledApplicationList);
                breakCount = goNoGoCustomerApplicationsList.size();
            }

        } else if (null != cancelDoNotRaisedApplicationRequest.getStartDate() && null != cancelDoNotRaisedApplicationRequest.getEndDate()) {

            cancelledApplicationList = new ArrayList<>();

            while (breakCount > 0) {
                goNoGoCustomerApplicationsList = adminLogRepository.getApplicationBetweenToAndFromDate(cancelDoNotRaisedApplicationRequest, limit, skip);

                startCancelApplicationProcess(goNoGoCustomerApplicationsList, cancelDoNotRaisedApplicationRequest, cancelledApplicationList);
                breakCount = goNoGoCustomerApplicationsList.size();
            }
        }

        if (!CollectionUtils.isEmpty(cancelledApplicationList)) {

            CancelApplicationResponse cancelApplicationResponse =new CancelApplicationResponse();
            cancelApplicationResponse.setCancelAppCount(cancelledApplicationList.size());
            cancelApplicationResponse.setCancelAppList(cancelledApplicationList);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, cancelApplicationResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    private void startCancelApplicationProcess(List<GoNoGoCustomerApplication> goNoGoCustomerApplicationsList, CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest,List<String> cancelledApplicationList) throws Exception{

        if (!CollectionUtils.isEmpty(goNoGoCustomerApplicationsList)) {
            for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplicationsList) {

                String gngRefId = goNoGoCustomerApplication.getGngRefId();
                String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
                if (!cancelledApplicationList.contains(gngRefId)) {
                    cancelledApplicationList.add(gngRefId);
                    String croJustificationRemark = String.format(ErrorCode.BATCH_CANCEL_AFTER_X_DAYS_DESCRIPTION, cancelDoNotRaisedApplicationRequest.getPastNoOfDays());
                    // cancel delivery order not raised application
                    CancelApplicationDetails cancelApplicationDetails = buildCancelApplicationDetails(goNoGoCustomerApplication, cancelDoNotRaisedApplicationRequest);
                    // save cancelled applicationLog
                    applicationRepository.updateDedupeApplicationStatus(institutionId, gngRefId,
                            cancelDoNotRaisedApplicationRequest.getAppStatus(), cancelDoNotRaisedApplicationRequest.getAppStage(), goNoGoCustomerApplication, croJustificationRemark);

                    adminLogRepository.saveCancelledApplicationLog(cancelApplicationDetails);

                }

            }
        }
    }

    private CancelApplicationDetails buildCancelApplicationDetails(GoNoGoCustomerApplication goNoGoCustomerApplication, CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) {

        CancelApplicationDetails cancelApplicationDetails = new CancelApplicationDetails();

        cancelApplicationDetails.setRefId(goNoGoCustomerApplication.getGngRefId());
        cancelApplicationDetails.setPreviousStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
        cancelApplicationDetails.setPreviousStatus(goNoGoCustomerApplication.getApplicationStatus());
        cancelApplicationDetails.setCurrentStage(cancelDoNotRaisedApplicationRequest.getAppStage());
        cancelApplicationDetails.setCurrentStatus(cancelDoNotRaisedApplicationRequest.getAppStatus());
        cancelApplicationDetails.setStatusChangeReason(Status.DO_NOT_RAISE_BATCH_CANCEL_APP.name());
        cancelApplicationDetails.setStatusUpdateDate(new Date());
        return cancelApplicationDetails;
    }


    @Override
    public BaseResponse getDoNotRaisedApplicationCountForCancel(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception{


        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationsList = null;
        BaseResponse baseResponse = null;
        int limit = cancelDoNotRaisedApplicationRequest.getLimit();
        int skip = cancelDoNotRaisedApplicationRequest.getSkip();


        DoNotRaisedApplicationCountResponse doNotRaisedApplicationCountResponse = null;

        if (cancelDoNotRaisedApplicationRequest.getPastNoOfDays() > 0) {

            Date todaysZeroTmDt =GngDateUtil.getZeroTimeFromDate(new Date());

            Date noOfDaysBeforeDt = new Date(todaysZeroTmDt.getTime() - cancelDoNotRaisedApplicationRequest.getPastNoOfDays() * 24 * 3600 * 1000l);

            // get formatted date
            Date formattedDate = GngDateUtil.getZeroTimeFromDate(noOfDaysBeforeDt);

            goNoGoCustomerApplicationsList = adminLogRepository.getApplicationLessThanDateForCancelPurpose(formattedDate, cancelDoNotRaisedApplicationRequest.getHeader().getInstitutionId(), limit, skip);

            doNotRaisedApplicationCountResponse = buildCancelCasesCount(goNoGoCustomerApplicationsList);

        } else if (null != cancelDoNotRaisedApplicationRequest.getStartDate() && null != cancelDoNotRaisedApplicationRequest.getEndDate()) {

            goNoGoCustomerApplicationsList = adminLogRepository.getApplicationBetweenToAndFromDate(cancelDoNotRaisedApplicationRequest, limit, skip);

            doNotRaisedApplicationCountResponse = buildCancelCasesCount(goNoGoCustomerApplicationsList);

        }


        if (null != doNotRaisedApplicationCountResponse) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doNotRaisedApplicationCountResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    private DoNotRaisedApplicationCountResponse buildCancelCasesCount(List<GoNoGoCustomerApplication> goNoGoCustomerApplicationsList) throws Exception{

        DoNotRaisedApplicationCountResponse doNotRaisedApplicationCountResponse = null;

       List<String> applicationList = new ArrayList<>();


        if (!CollectionUtils.isEmpty(goNoGoCustomerApplicationsList)) {
            doNotRaisedApplicationCountResponse = new DoNotRaisedApplicationCountResponse();

            int applicationCount = 0;

            for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplicationsList) {
                if (!applicationList.contains(goNoGoCustomerApplication.getGngRefId())) {
                    applicationList.add(goNoGoCustomerApplication.getGngRefId());
                    applicationCount++;
                }

            }
            doNotRaisedApplicationCountResponse.setApplicationCount(applicationCount);
            doNotRaisedApplicationCountResponse.setAppplicationRefId(applicationList);
        }
        return doNotRaisedApplicationCountResponse;
    }
    @Override
    public BaseResponse getTcLosLog(TcLosLogRequest tcLosLogRequest) {
        BaseResponse baseResponse = null;
        TcLosReqReslog tcLosReqReslog = externalAPILogRepository.getTcLosLog(tcLosLogRequest);
        if (tcLosReqReslog != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tcLosReqReslog);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getSmsServiceReqResLog(SmsServReqResLogRequest smsServReqResLogRequest) {
        BaseResponse baseResponse = null;
       List<SmsServiceReqResLog> smsServiceReqResLogs = externalAPILogRepository.getSmsServiceReqResLog(smsServReqResLogRequest);
        if (smsServiceReqResLogs != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, smsServiceReqResLogs);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse getAadharLogByAadharNoAndInstId(String aadharNo, String institutionId) {
        BaseResponse baseResponse = null;
        List<AadharLog> aadharLogByAadharNoAndInstId = externalAPILogRepository.getAadharLogByAadharNoAndInstId(aadharNo, institutionId);
        if (!CollectionUtils.isEmpty(aadharLogByAadharNoAndInstId)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, aadharLogByAadharNoAndInstId);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse getApplicationStatusAndStage(String refId, String institutionId) throws Exception {

        ApplicationStatusAndStage statusAndStageResponse = new ApplicationStatusAndStage();
        GoNoGoCroApplicationResponse gonogoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, institutionId);

        if (null != gonogoCustomerApplication && StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationStatus())
                && null != gonogoCustomerApplication.getApplicationRequest()) {

            String applicationStatus = gonogoCustomerApplication.getApplicationStatus();
            String currentStageId = gonogoCustomerApplication.getApplicationRequest().getCurrentStageId();

            statusAndStageResponse.setStatus(Status.SUCCESS.name());
            statusAndStageResponse.setAppStage(currentStageId);
            statusAndStageResponse.setAppStatus(applicationStatus);
            statusAndStageResponse.setMessage(String.format(ErrorCode.APPLICATION_STAGE_AND_STATUS, applicationStatus, currentStageId));
        } else {
            statusAndStageResponse.setStatus(Status.FAILED.name());
            statusAndStageResponse.setMessage(String.format(ErrorCode.APP_NT_FND, refId));
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, statusAndStageResponse);
    }

    @Override
    public BaseResponse modifyAppStageAndStatus(ModifyAppStageAndStatusRequest stageAndStatusRequest) throws Exception {

        String refId = stageAndStatusRequest.getRefId();
        String institutionId = stageAndStatusRequest.getHeader().getInstitutionId();
        String oldAppStatus = null;
        String oldStageId = null;

        GenericMsgResponse genericMsgResponse = new GenericMsgResponse();

        GoNoGoCroApplicationResponse gonogoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, institutionId);

        if (null != gonogoCustomerApplication && StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationStatus())
                && null != gonogoCustomerApplication.getApplicationRequest()
                && StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId())) {

            oldAppStatus = gonogoCustomerApplication.getApplicationStatus();
            oldStageId = gonogoCustomerApplication.getApplicationRequest().getCurrentStageId();

                if (stageAndStatusRequest.getActionName() == ActionName.DO_RESET) {

                    if (StringUtils.equals(GNGWorkflowConstant.APPROVED.toFaceValue(), oldAppStatus)
                            && StringUtils.equals(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue(), oldStageId)) {

                        genericMsgResponse = updateApplicationStatusAndStage(stageAndStatusRequest, oldAppStatus, oldStageId);

                    } else {
                        genericMsgResponse.setStatus(Status.FAILED.name());
                        genericMsgResponse.setMessage(ErrorCode.ONLY_PDDE_STAGE_ALLOWED_TO_RESET_DO);

                    }
                }else{
                    genericMsgResponse = updateApplicationStatusAndStage(stageAndStatusRequest ,oldAppStatus, oldStageId);

                }

        } else {

            genericMsgResponse.setStatus(Status.FAILED.name());
            genericMsgResponse.setMessage(String.format(ErrorCode.APP_NT_FND,refId));

        }
        return GngUtils.getBaseResponse(HttpStatus.OK,genericMsgResponse);
    }

    private GenericMsgResponse updateApplicationStatusAndStage(ModifyAppStageAndStatusRequest stageAndStatusRequest, String oldAppStatus, String oldAppStage) throws Exception {
        GenericMsgResponse genericMsgResponse = new GenericMsgResponse();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLogs = auditHelper.createActivityLog(null, stageAndStatusRequest.getHeader());
        activityLogs.setInstitutionId(stageAndStatusRequest.getHeader().getInstitutionId());
        activityLogs.setRefId(stageAndStatusRequest.getRefId());
        activityLogs.setAction(ActionName.MODIFY_APP_STAGE_STATUS.name());
        activityLogs.setOldAppStatus(oldAppStatus);
        activityLogs.setOldAppStage(oldAppStage);
        activityLogs.setCurrentAppStatus(stageAndStatusRequest.getAppStatus());
        activityLogs.setCurrentAppStage(stageAndStatusRequest.getAppStage());
        activityLogs.setActionDate(new Date());

        if (adminLogRepository.updateStageAndStatus(stageAndStatusRequest.getRefId(), stageAndStatusRequest.getAppStatus(), stageAndStatusRequest.getAppStage())) {
            activityLogs.setStatus(Status.SUCCESS.name());
            genericMsgResponse.setStatus(Status.SUCCESS.name());
            activityLogs.setCustomMsg(ErrorCode.APP_STAGE_SUCCESSFULLY_UPDATE);
            genericMsgResponse.setMessage(ErrorCode.APP_STAGE_SUCCESSFULLY_UPDATE);
        } else {
            activityLogs.setStatus(Status.FAILED.name());
            genericMsgResponse.setStatus(Status.FAILED.name());
            genericMsgResponse.setMessage(ErrorCode.PROBLEM_OCCUR_DURING_STAGE_UPDATE);
            genericMsgResponse.setMessage(ErrorCode.PROBLEM_OCCUR_DURING_STAGE_UPDATE);
        }

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        // save activity log
        eventPublisher.publishEvent(activityLogs);

        return genericMsgResponse;
    }

    /*
    * A File is providing decision, remarks and the refIds to be changed to
    * Here is file format
    * -------------
    * decision => Approved / Declined / Queue / OnHold/ Cancelled / Disbursed / LMS-Initiated
    * remark => This wll be populated in CRO Justification
    * refId1
    * refId2
    * ...
    * refIdn
    * -------------
    * */
    @Override
    public BaseResponse bulkDeclineCases(File uploadedFile, String institutionId) throws Exception {
        // Activity logs
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setInstitutionId(institutionId);
        activityLogs.setAction(ActionName.BULK_UPDATE_DECISION.name());
        activityLogs.setActionDate(new Date());


        Map<String, Object> result = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(uploadedFile));
        List<String> lines = reader.lines().collect(Collectors.toList());
        if(CollectionUtils.isNotEmpty(lines) && lines.size() > 2){

            // First line : Status
            // second line Remark
            String status = lines.get(0);
            String remark = lines.get(1);
            activityLogs.setCurrentAppStatus(status);

            CroJustification caseJustification = new CroJustification();
            caseJustification.setRemark(remark);
            List<CroJustification> justificationList = new ArrayList<>();
            justificationList.add(caseJustification);

            Header header = new Header();
            header.setInstitutionId(institutionId);
            header.setCroId(GNGWorkflowConstant.USER_SYSTEM_ADMIN.toFaceValue());
            CroApprovalRequest decisionRequest = new CroApprovalRequest();
            decisionRequest.setHeader(header);
            decisionRequest.setApplicationStatus(status);
            decisionRequest.setCroJustification(justificationList);

            String refId = null;
            int updatedCount = 0;
            List<String> failedRefIds = new ArrayList<>();

            for (int i = 2; i < lines.size(); i++) {
                try {
                refId = lines.get(i);
                decisionRequest.setReferenceId(refId);
                croManager.updateDecision(decisionRequest, null);
                updatedCount++;
                } catch (Exception e){
                    logger.error("Failed to update status of {} due to {}", refId, e.getMessage());
                    failedRefIds.add(refId);
                }
            }
            result.put("updateCount", lines.size()-2);
            result.put("updatedCount", updatedCount);
            result.put("failedRefIds", failedRefIds);

        } else {
            result.put(MESSAGE, DECISION_MESSAGE_FORMAT_ERROR);
        }
        stopWatch.stop();
        logger.info("Result of bulk status update {}", result);
        activityLogs.setCustomMsg(result.toString());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        // save activity log
        eventPublisher.publishEvent(activityLogs);

        return GngUtils.getBaseResponse(HttpStatus.OK, result);
    }
}
