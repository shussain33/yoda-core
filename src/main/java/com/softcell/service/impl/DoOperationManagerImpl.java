package com.softcell.service.impl;

import com.softcell.constants.DoStatusEnum;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.AdminLogRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.DoOperationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dooperation.DeliveryOrderOperationRequest;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.dooperation.DeliveryOrderOperationResponse;
import com.softcell.gonogo.service.factory.DoOperationBuilder;
import com.softcell.service.DigitizationStoreManager;
import com.softcell.service.DoOperationManager;
import com.softcell.service.DocumentStoreManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by mahesh on 30/8/17.
 */
@Service
public class DoOperationManagerImpl implements DoOperationManager {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private DigitizationStoreManager digitizationStoreManager;

    @Autowired
    private DoOperationBuilder doOperationBuilder;

    @Autowired
    private DoOperationRepository doOperationRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private AdminLogRepository adminLogRepository;

    @Autowired
    private DocumentStoreManager documentStoreManager;


    @Override
    public BaseResponse cancelDeliveryOrder(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        String refId = deliveryOrderOperationRequest.getRefID();
        String institutionId = deliveryOrderOperationRequest.getHeader().getInstitutionId();

        PostIPA postIPA = applicationRepository.getPostIPA(refId, institutionId);

        DeliveryOrderOperationResponse deliveryOrderOperationResponse = new DeliveryOrderOperationResponse();

        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_CANCELLATION_FAIL);
        deliveryOrderOperationResponse.setStatus(Status.FAILED.name());

        if (null != postIPA) {

            if (null != postIPA.getDeliveryOrderStatus()) {

                if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.CREATED.name())
                        || StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.RESTORED.name())
                        || StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.MAIL_FAILED.name())) {

                    if (startCancelOrRestoredDeliveryOrderProcess(deliveryOrderOperationRequest.getHeader(), deliveryOrderOperationRequest.getRefID(), postIPA, DoStatusEnum.CANCELLED.name())) {
                        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_CANCELLATION_SUCCESS);
                        deliveryOrderOperationResponse.setStatus(Status.SUCCESS.name());
                    }

                } else if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.MAILED.name())
                        || StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.MAIL_IN_PROGRESS.name())) {

                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_ALREADY_SUBMITTED_TO_DEALER);

                } else if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.CANCELLED.name())) {

                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_ALREADY_CANCELLED);
                } else if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.NOT_CREATED.name())) {

                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_NOT_CREATED_FOR_THIS_APP);
                }
            } else {
                //logic for previous application i.e post ipa details available but deliveryOrder status  field is blank
                //  if delivery order status is MAILED ,then provide error message
                if (checkDeliveryOrderPdfExistance(refId, institutionId)) {
                    if (getDeliveryOrderMailStatus(deliveryOrderOperationRequest.getRefID())) {

                        deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                        deliveryOrderOperationResponse.setResponseMsg("Cannot cancel DO ," +ErrorCode.DO_ALREADY_SUBMITTED_TO_DEALER);

                        // if delivery order status is not MAILED ,then cancel delivery order
                    } else if (startCancelOrRestoredDeliveryOrderProcess(deliveryOrderOperationRequest.getHeader(), deliveryOrderOperationRequest.getRefID(), postIPA, DoStatusEnum.CANCELLED.name())) {

                        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_CANCELLATION_SUCCESS);
                        deliveryOrderOperationResponse.setStatus(Status.SUCCESS.name());

                    }
                } else {
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_NOT_CREATED_FOR_THIS_APP);
                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());

                }
            }

        } else {
            deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
            deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_NOT_CREATED_FOR_THIS_APP);

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, deliveryOrderOperationResponse);
    }

    @Override
    public BaseResponse restoreDeliveryOrder(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        String refId =deliveryOrderOperationRequest.getRefID();
        String institutionId = deliveryOrderOperationRequest.getHeader().getInstitutionId();

        PostIPA postIPA = applicationRepository.getPostIPA(refId, institutionId);

        DeliveryOrderOperationResponse deliveryOrderOperationResponse = new DeliveryOrderOperationResponse();

        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_RESTORATION_FAIL);
        deliveryOrderOperationResponse.setStatus(Status.FAILED.name());

        if (null != postIPA ) {

            if (null != postIPA.getDeliveryOrderStatus()) {

                if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.CANCELLED.name())) {

                    if (startCancelOrRestoredDeliveryOrderProcess(deliveryOrderOperationRequest.getHeader(), deliveryOrderOperationRequest.getRefID(), postIPA, DoStatusEnum.RESTORED.name())) {
                        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_RESTORATION_SUCCESS);
                        deliveryOrderOperationResponse.setStatus(Status.SUCCESS.name());
                    }

                } else if (GngUtils.checkDeliveryOrderRestoreStatus(postIPA.getDeliveryOrderStatus())) {

                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(String.format(ErrorCode.CAN_NOT_RESTORE_DO ,postIPA.getDeliveryOrderStatus()));

                } else if (StringUtils.equals(postIPA.getDeliveryOrderStatus(), DoStatusEnum.RESTORED.name())) {

                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_ALREADY_RESTORED);
                }
            } else {
                //logic for previous application i.e post ipa details available but deliveryOrder status  field is blank
                //  if delivery order status is MAILED ,then provide error message
                if (checkDeliveryOrderPdfExistance(refId, institutionId)) {
                    if (getDeliveryOrderMailStatus(deliveryOrderOperationRequest.getRefID())) {

                        deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                        deliveryOrderOperationResponse.setResponseMsg("Cannot restore DO ," + ErrorCode.DO_ALREADY_SUBMITTED_TO_DEALER);

                        // if delivery order status is not MAILED ,then cancel delivery order
                    } else if (startCancelOrRestoredDeliveryOrderProcess(deliveryOrderOperationRequest.getHeader(), deliveryOrderOperationRequest.getRefID(), postIPA, DoStatusEnum.RESTORED.name())) {

                        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_RESTORATION_SUCCESS);
                        deliveryOrderOperationResponse.setStatus(Status.SUCCESS.name());

                    }
                } else {
                    deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
                    deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_NOT_CREATED_FOR_THIS_APP);
                }

            }

        } else {
            deliveryOrderOperationResponse.setStatus(Status.FAILED.name());
            deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_NOT_CREATED_FOR_THIS_APP);

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, deliveryOrderOperationResponse);

    }


    @Override
    public BaseResponse getDeliveryOrderStatus(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception {

        String refId = deliveryOrderOperationRequest.getRefID();
        String institutionId = deliveryOrderOperationRequest.getHeader().getInstitutionId();

        PostIPA postIPA = applicationRepository.getPostIPA(refId,institutionId);

        DeliveryOrderOperationResponse deliveryOrderOperationResponse = new DeliveryOrderOperationResponse();

        deliveryOrderOperationResponse.setStatus(Status.SUCCESS.name());
        deliveryOrderOperationResponse.setResponseMsg(ErrorCode.DO_STATUS_FOUND);

        //if post ipa is null ,do is not created for this application.
        if (null != postIPA) {

            // if post ipa is not null ,but delivery order status is null ,then it is the old application i.e need to check EmailLog
            if (null != postIPA.getDeliveryOrderStatus()) {

                deliveryOrderOperationResponse.setDeliveryOrderStatus(postIPA.getDeliveryOrderStatus());

            } else {
                //logic for previous application i.e post ipa details available but deliveryOrder status  field is blank
                if (checkDeliveryOrderPdfExistance(refId, institutionId)) {
                    if (getDeliveryOrderMailStatus(deliveryOrderOperationRequest.getRefID())) {

                        deliveryOrderOperationResponse.setDeliveryOrderStatus(DoStatusEnum.MAILED.name());

                    } else {

                        deliveryOrderOperationResponse.setDeliveryOrderStatus(DoStatusEnum.CREATED.name());
                    }
                } else {
                    deliveryOrderOperationResponse.setDeliveryOrderStatus(DoStatusEnum.NOT_CREATED.name());
                }

            }

        } else {
            deliveryOrderOperationResponse.setDeliveryOrderStatus(DoStatusEnum.NOT_CREATED.name());

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, deliveryOrderOperationResponse);
    }

    private boolean startCancelOrRestoredDeliveryOrderProcess(Header header, String refID, PostIPA postIPA, String cancelOrRestored) throws Exception {

        PostIpaRequest postIpaRequest = doOperationBuilder.buildPostIpaRequest(header, refID);

        String deliveryOrderFileId = digitizationStoreManager.saveDeliveryOrderForm(postIpaRequest, postIPA, cancelOrRestored);

        if (StringUtils.isNotBlank(deliveryOrderFileId)) {

            boolean updateDoStatusResult = doOperationRepository.updateDoStatusInPostIpa(refID,
                    header.getInstitutionId(), cancelOrRestored);

            DoOperationLog doOperationLog = doOperationBuilder.buildDoOperationLog(postIpaRequest.getRefID()
                    , postIpaRequest.getHeader(), cancelOrRestored);

            //save doOperationLog
            doOperationRepository.saveDoOperationLog(doOperationLog);

            if (updateDoStatusResult) {
                return true;
            }

        }

        return false;
    }


    private boolean getDeliveryOrderMailStatus(String gngRefId) throws Exception {

        //form Delivery Order Name using RefId and the model No
        String deliveryOrderName = GngUtils.formDeliveryOrderName(gngRefId);

        // get Image Id using RefId and DeliveryOrderName
        String DOImageId = uploadFileRepository.getImageIdByRefIdAndFileName(gngRefId, deliveryOrderName);

        // get Delivery Order Image Status using DO Image Id

        if (StringUtils.isNotBlank(DOImageId)) {

            GetFileRequest getImageRequest = new GetFileRequest();

            getImageRequest.setImageFileID(DOImageId);
            getImageRequest.setRefID(gngRefId);

            // check mail status in the mail log.
            if (adminLogRepository.getEmailStatus(getImageRequest)) {
                return true;
            }

        }
        return false;
    }


    private boolean checkDeliveryOrderPdfExistance(String refId, String institutionId) throws Exception {

        BaseResponse deliveryOrderPdf = documentStoreManager.getDeliveryOrderPdf(refId, institutionId);

        Document document = GngUtils.convertBaseResponsePayload(deliveryOrderPdf, Document.class);

        if (null != document && null != document.getByteCode()) {
            return true;
        }
        return false;
    }

    @Override
    public BaseResponse getDoOperationLog(String refId, String institutionId) {

        BaseResponse baseResponse;
        List<DoOperationLog> doOperationLog = doOperationRepository.getDoOperationLog(refId, institutionId);

        if (!CollectionUtils.isEmpty(doOperationLog)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doOperationLog);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }
}
