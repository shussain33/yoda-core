package com.softcell.service.impl;

import com.softcell.config.AuthenticationConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.TemplateConstants;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.configuration.admin.*;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.IntimationConfigResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.security.v2.LoginBaseResponseV2;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.rest.controllers.ApplicationConfigurationController;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.service.AppConfigurationManager;
import com.softcell.utils.GngUtils;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by amit on 28/5/18.
 */
@Service
public class AppConfigurationManagerImpl implements AppConfigurationManager {

    private Logger logger = LoggerFactory.getLogger(AppConfigurationManagerImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    AppConfigurationHelper appConfigurationHelper;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MasterDataViewRepository masterDataViewRepository;

    @Autowired
    @Qualifier("redis")
    private RedisAsyncCommands<String,String> stringRedisAsyncCommands;

    @Autowired
    @Qualifier("redisCluster")
    private RedisAdvancedClusterAsyncCommands<String,String> strinClusterAsyncCommands;

    @Override
    public BaseResponse synchUsers(AdminConfigRequest adminConfigRequest, HttpServletRequest httpRequest) {

        List<LoginServiceResponse> usersResponse = getUsersDetailsFromUAM(adminConfigRequest.getInstitutionId(), httpRequest);
        createUniqueIds(usersResponse);
        List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List = new ArrayList<>();
        boolean status = false;
        if (CollectionUtils.isNotEmpty(usersResponse)) {

            logger.debug("Removing existing users {}", adminConfigRequest.getInstitutionId());
            appConfigurationRepository.removeAllUsers(LoginServiceResponse.class, adminConfigRequest.getInstitutionId());
            logger.debug("Inserting new users {}", adminConfigRequest.getInstitutionId());
            appConfigurationRepository.saveAll(usersResponse, LoginServiceResponse.class);

            Map<Integer, Map<String, Map<Branch, Map<String, List<LoginServiceResponse>>>>> resultMap =
                    appConfigurationHelper.calculateOrganisationalHierarchyForUsers(usersResponse);

            if (!resultMap.isEmpty()) {
                Map<String, Set<String>> productWiseRoleMap = new HashMap<>();
                hierarchyMasterV2List = appConfigurationHelper.createHierarchyMasterData(resultMap, productWiseRoleMap);
                status = appConfigurationRepository.saveHierarchyMaster(adminConfigRequest.getHeader().getInstitutionId(),
                        hierarchyMasterV2List);
                //Cache.initInstitutionWiseUserIdNameMap();
                //add all productwise roles in config
                GenerateGroupRequest generateGroupRequest = new GenerateGroupRequest();
                generateGroupRequest.setHeader(adminConfigRequest.getHeader());
                productWiseRoleMap.keySet().forEach((key) -> {
                    generateGroupRequest.setProduct(key);
                    generateBranchGroups(generateGroupRequest, true);
                });

                appConfigurationHelper.addProductWiseRolesInConfig(productWiseRoleMap, adminConfigRequest.getHeader().getInstitutionId());
            }
        }
        BaseResponse response = new BaseResponse();
        if (status) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, hierarchyMasterV2List);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    private void createUniqueIds(List<LoginServiceResponse> usersResponse) {
        usersResponse.forEach(user -> {
            user.setId(user.getInstitutionId() + "_" + user.getId());
            user.setUserName(StringUtils.isNotEmpty(user.getFullName()) ? user.getFullName()
                    :getUserFullName ( user.getFirstName(), user.getMiddleName(), user.getLastName()));
        });
    }
    private List<LoginServiceResponse> optimizedUserResponse(List<LoginServiceResponse> usersResponse){
        List<LoginServiceResponse> miniUserResponse = usersResponse.stream()
                .map(userResponse -> {
                    LoginServiceResponse loginServiceResponse = new LoginServiceResponse();
                    loginServiceResponse.setId(userResponse.getId());
                    loginServiceResponse.setLoginId(userResponse.getLoginId());
                    loginServiceResponse.setInstitutionId(userResponse.getInstitutionId());
                    loginServiceResponse.setFirstName(userResponse.getFirstName());
                    loginServiceResponse.setLastName(userResponse.getLastName());
                    loginServiceResponse.setUserName(
                            StringUtils.isNotEmpty(userResponse.getFullName()) ? userResponse.getFullName()
                                    :getUserFullName ( userResponse.getFirstName(), userResponse.getMiddleName(), userResponse.getLastName()));
                    loginServiceResponse.setEmail(userResponse.getEmail());
                    loginServiceResponse.setMobile(userResponse.getMobile());
                    loginServiceResponse.setActive(userResponse.getActive());
                    loginServiceResponse.setRoles(userResponse.getRoles());
                    loginServiceResponse.setHierarchy(userResponse.getHierarchy());
                    return loginServiceResponse;
                })
                .collect(Collectors.toList());
        return miniUserResponse;
    }

    private String getUserFullName(String fName, String mName, String lName){
        StringBuilder fullName = new StringBuilder();

        return fullName.append(fName)
                .append(" ")
                .append(StringUtils.isNotEmpty(mName)? (mName +" ") :"")
                .append(lName).toString();
    }

    @Override
    public BaseResponse synchUser(SynchUserRequest synchUserRequest) {

        LoginServiceResponse loginServiceResponse = synchUserRequest.getUserData();
        loginServiceResponse.setId(loginServiceResponse.getInstitutionId() + "_" + loginServiceResponse.getId());
        appConfigurationRepository.update(loginServiceResponse);
        String institutionId = synchUserRequest.getInstitutionId();
        // update changed users info in organisational hierarchy
        appConfigurationHelper.updateUserInOrganisationalHierarchy(institutionId, loginServiceResponse);

        BaseResponse response;
        if (loginServiceResponse != null && institutionId != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.CONFLICT);
        }
        return response;
    }

    @Override
    public List<LoginServiceResponse> getUsersDetailsFromUAM(String institutionId, HttpServletRequest httpRequest) {
        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY_V3.toFaceValue());
        List<LoginServiceResponse> usersResponse = new ArrayList<>();


        int pageNo = 0;
        int size = 100;
        int totalUsers = 0;
        List<LoginServiceResponse> usersList = null;
        HttpHeaders headers = new HttpHeaders();
        headers.set("institutionID", institutionId);
        headers.set("Authorization", StringUtils.isNotEmpty(httpRequest.getHeader("Authorization")) ? httpRequest.getHeader("Authorization") : "Bearer luam-api-key");
        logger.debug(" getUsersDetailsFromUAM service headers {}", headers.toString());

        do {
            String url = config.getFetchUsersUrl() + "?institutionId=" + institutionId +
                    "&page=" + pageNo + "&size=" + size + "&sort=id,Desc";
            try {
                LoginBaseResponseV2 loginBaseResponse = (LoginBaseResponseV2) restTemplate.exchange
                        (url, HttpMethod.GET,new HttpEntity<Object>(headers),LoginBaseResponseV2.class).getBody();
                usersList = (List<LoginServiceResponse>) loginBaseResponse.getBody().getLoginServiceResponse();

                if (CollectionUtils.isNotEmpty(usersList)) {
                    usersResponse.addAll(usersList);
                    totalUsers += usersList.size();
                }
            } catch (Exception e) {
                logger.info("Error while fetching users data from UAM for URL {}, Exception {} ", url, ExceptionUtils.getStackTrace(e));
            }
            pageNo++;
            if(CollectionUtils.isEmpty(usersList))
                usersList = new ArrayList<>();
        } while (usersList.size() == size);
        logger.debug("total {} users are ready for synch", totalUsers);
        return usersResponse;
    }

    @Override
    public BaseResponse getUsersHierarchy(AdminConfigRequest adminConfigRequest) {

        List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List =
                appConfigurationRepository.fetchHierarchyMaster(adminConfigRequest.getHeader().getInstitutionId());
        BaseResponse response = new BaseResponse();
        if (CollectionUtils.isNotEmpty(organizationalHierarchyMasterV2List)) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, organizationalHierarchyMasterV2List);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse getInstitutionConfiguration(AdminConfigRequest adminConfigRequest) {
        String institutionId = adminConfigRequest.getHeader().getInstitutionId();
        String configType = adminConfigRequest.getConfigType();
        InstitutionConfig institutionConfig = appConfigurationRepository.fetchInstitutionConfiguration(institutionId,configType);
        BaseResponse response;
        if (institutionConfig != null) {
            if(StringUtils.equalsIgnoreCase(EndPointReferrer.INTIMATION_CONFIG, configType)) {
                appConfigurationHelper.mapIntimationToInstitution(institutionConfig, institutionId);
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, institutionConfig);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse getIntimationConfiguration(AdminConfigRequest adminConfigRequest) {
        String institutionId = adminConfigRequest.getHeader().getInstitutionId();
        IntimationConfigResponse intimationConfigResponse = new IntimationConfigResponse();
        InstitutionConfig institutionConfig =
                appConfigurationRepository.fetchInstitutionConfiguration(institutionId, adminConfigRequest.getConfigType());
        appConfigurationHelper.mapIntimationToInstitution(institutionConfig, institutionId);
        intimationConfigResponse.setInstitutionConfig(institutionConfig);
        intimationConfigResponse.setActions(appConfigurationHelper.getProductActions());
        intimationConfigResponse.setTemplateDetails(appConfigurationRepository.findTemplateDetailsByInstituteId(
                adminConfigRequest.getHeader().getInstitutionId()));

        return GngUtils.getBaseResponse(HttpStatus.OK, intimationConfigResponse);
    }

    @Override
    public BaseResponse saveProductModuleConfig(InstitutionModuleRequest institutionModuleRequest, String configType) {
        BaseResponse baseResponse;
        ProductConfig productConfig = institutionModuleRequest.getProductConfig();
        if (productConfig != null) {
            InstitutionConfig institutionConfig =
                    appConfigurationRepository.fetchInstitutionConfiguration(institutionModuleRequest.getHeader().getInstitutionId());
            if (institutionConfig != null) {
                appConfigurationHelper.filterAndUpdateModuleConfig(institutionConfig, productConfig, configType);
            } else {
                institutionConfig = new InstitutionConfig();
                institutionConfig.setInstitutionId(institutionModuleRequest.getHeader().getInstitutionId());
                List<String> products = new ArrayList<>();
                products.add(productConfig.getProductName());
                institutionConfig.setProducts(products);
                List<ProductConfig> productConfigList = new ArrayList<>();
                productConfigList.add(productConfig);
                institutionConfig.setProductConfigs(productConfigList);
            }
            appConfigurationRepository.save(institutionConfig);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteProductModuleConfig(InstitutionModuleRequest institutionModuleRequest, String configType) {
        BaseResponse baseResponse;
        String productName = institutionModuleRequest.getProductName();
        if (productName != null) {
            InstitutionConfig institutionConfig =
                    appConfigurationRepository.fetchInstitutionConfiguration(institutionModuleRequest.getHeader().getInstitutionId());
            if (institutionConfig != null) {
                for (ProductConfig productConfig : institutionConfig.getProductConfigs()) {
                    if (StringUtils.equals(productConfig.getProductName(), productName)) {
                        if (StringUtils.equals(configType, EndPointReferrer.SAVE_MODULE_CONFIG))
                            productConfig.setModuleConfigList(null);
                        if (StringUtils.equals(configType, EndPointReferrer.SAVE_MODULE_CONFIG))
                            productConfig.setScreenValueMasterConfigs(null);
                        break;
                    }
                }
                appConfigurationRepository.save(institutionConfig);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse saveIntimationConfig(IntimationConfigRequest intimationConfigRequest, String configType) {
        BaseResponse baseResponse;
        String productName = intimationConfigRequest.getProductName();
        ActionGroup config = intimationConfigRequest.getConfig();
        IntimationGroup group = intimationConfigRequest.getGroup();

        if (config != null) {
            IntimationConfiguration intimationConfiguration =
                    appConfigurationRepository.fetchIntimitionConfiguration(intimationConfigRequest.getHeader().getInstitutionId(),
                            intimationConfigRequest.getProductName());
            if (intimationConfiguration != null) {
                intimationConfiguration =  appConfigurationHelper.filterAndUpdateIntimationConfig(intimationConfiguration, intimationConfigRequest, configType);
            } else {
                intimationConfiguration = new IntimationConfiguration();
                intimationConfiguration.setInstitutionId(intimationConfigRequest.getHeader().getInstitutionId());
                intimationConfiguration.setProduct(productName);
                intimationConfiguration.setIntimationConfig(new IntimationConfig());
                intimationConfiguration.getIntimationConfig().setActionGroupList(new ArrayList<>());
                intimationConfiguration.getIntimationConfig().getActionGroupList().add(config);
                intimationConfiguration.getIntimationConfig().setGroups(new ArrayList<>());
                intimationConfiguration.getIntimationConfig().getGroups().add(group);
            }
            appConfigurationRepository.updateIntimation(intimationConfiguration);
            //Cache.initInstitutionProductWiseIntimationMap();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, intimationConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteIntimationConfig(IntimationConfigRequest intimationConfigRequest, String configType) {
        BaseResponse baseResponse;
        String productName = intimationConfigRequest.getProductName();
        String institutionId = intimationConfigRequest.getHeader().getInstitutionId();
        if (productName != null) {
            IntimationConfiguration intimationConfiguration = appConfigurationRepository.fetchIntimitionConfiguration(institutionId, productName);

            if (intimationConfiguration != null) {

                intimationConfiguration.getIntimationConfig().setActionGroupList(null);
                appConfigurationRepository.save(intimationConfiguration);
                //Cache.initInstitutionProductWiseIntimationMap();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse generateBranchGroups(GenerateGroupRequest generateGroupRequest, boolean isSynchCall){
        BaseResponse baseResponse;
        String institutionId = generateGroupRequest.getHeader().getInstitutionId();
        String productName = generateGroupRequest.getProduct();
        List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List = appConfigurationRepository.fetchHierarchyMaster(
                institutionId, productName);
        boolean isGenerate = true;
        /*if(groupNameGroupMap.isEmpty()){
            isGenerate = false;
        }*/

        if (CollectionUtils.isNotEmpty(hierarchyMasterV2List) && isGenerate) {
            List<IntimationGroup> groups = new ArrayList<>();
            Set<String> groupTypeValues = new HashSet<>();
            appConfigurationHelper.createApplicationSpecificGroup(groupTypeValues);
            appConfigurationHelper.createRoleWiseGroups(hierarchyMasterV2List, groupTypeValues);
            appConfigurationHelper.createBranchWiseGroups(groupTypeValues);
            appConfigurationHelper.createProductWiseGroups(productName, groupTypeValues);
            appConfigurationHelper.createBranchRoleWiseGroups(hierarchyMasterV2List, groupTypeValues);
            appConfigurationHelper.createZoneWiseGroups(groupTypeValues);
            appConfigurationHelper.createZoneRoleWiseGroups(hierarchyMasterV2List, groupTypeValues);
            appConfigurationHelper.createLocationRoleWiseGroups(hierarchyMasterV2List, groupTypeValues);
            appConfigurationHelper.createRegionRoleWiseGroups(hierarchyMasterV2List, groupTypeValues);
            saveGeneratedIntimationGroups(institutionId, productName, groups, groupTypeValues);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, groupTypeValues);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    private void saveGeneratedIntimationGroups(String institutionId, String productName, List<IntimationGroup> groups, Set<String> groupTypeValues) {
        IntimationConfiguration intimationConfiguration = appConfigurationRepository.fetchIntimitionConfiguration(institutionId, productName);
        if(Objects.isNull(intimationConfiguration)){
            intimationConfiguration = new IntimationConfiguration();
            intimationConfiguration.setInstitutionId(institutionId);
            intimationConfiguration.setProduct(productName);
            IntimationConfig intimationConfig = new IntimationConfig();
            intimationConfig.setDefaultGroupCreated(true);
            intimationConfig.setGroups(groups);
            intimationConfig.setDefaultGroups(groupTypeValues);
            intimationConfiguration.setIntimationConfig(intimationConfig);
        }else{
            IntimationConfig intimationConfig = intimationConfiguration.getIntimationConfig();
            if(Objects.isNull(intimationConfig)){
                intimationConfig = new IntimationConfig();
            }
            intimationConfig.setDefaultGroupCreated(true);
            intimationConfig.setGroups(groups);
            intimationConfig.setDefaultGroups(groupTypeValues);
            intimationConfiguration.setIntimationConfig(intimationConfig);
        }
        try {
            appConfigurationRepository.removeIntimationConfiguration(IntimationConfiguration.class,institutionId,productName);
            appConfigurationRepository.save(intimationConfiguration);
        } catch (Exception e) {
            logger.error("Error occoured during updation intimation groups! : ", ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public BaseResponse createWorkflow(WorkflowRequest workflowRequest) {
        BaseResponse response = null;
        try {
            InstitutionConfig institutionConfig =
                    appConfigurationRepository.fetchInstitutionConfiguration(workflowRequest.getHeader().getInstitutionId());
            institutionConfig = appConfigurationHelper.createOrUpdateWorkflowRequest(workflowRequest, institutionConfig);
            appConfigurationRepository.save(institutionConfig);
            response = GngUtils.getBaseResponse(HttpStatus.OK, workflowRequest.getWorkflowMaster());
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return response;
    }

    @Override
    public WorkflowMaster getActiveWorkFlow(String institutionId, String product) {
        InstitutionConfig institutionConfig =
                appConfigurationRepository.fetchInstitutionConfiguration(institutionId);

        WorkflowMaster workflowMaster = null;
        if(institutionConfig != null && CollectionUtils.isNotEmpty(institutionConfig.getProducts())){
            for(ProductConfig productConfig : institutionConfig.getProductConfigs()) {
                if(StringUtils.equals(productConfig.getProductName(), product)){
                    if(CollectionUtils.isNotEmpty(productConfig.getWorkflowMasterList())) {
                        for(WorkflowMaster workflow : productConfig.getWorkflowMasterList()){
                            if(workflow.isActive()){
                                workflowMaster = workflow;
                                break;
                            }
                        };
                    }
                }
            };
        }
        return workflowMaster;
    }

    @Override
    public BaseResponse getTemplateDetails(String instituteId) {
        BaseResponse response = null;
        List<TemplateDetails> templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteId(instituteId);
        TemplateRequest templateRequest = new TemplateRequest();
        if (templateDetails != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, templateDetails);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return response;
    }

    @Override
    public BaseResponse save(TemplateRequest templateRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        try {
            // logger.debug("{} : saving Verification Details refId {}", templateRequest.getRefId());
            boolean save = appConfigurationRepository.saveTemplateDetail(templateRequest);
            if (save) {
                response = GngUtils.getBaseResponse(HttpStatus.OK,"Submitted successfully");
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());

        }
        return response;
    }

    @Override
    public BaseResponse updateTemplateInfo(TemplateRequest templateRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        Map<String,String> clientInfo=null;
        String refId = templateRequest.getRefId();
        double aprvLoan = 0 ;
        try {
            Header header = templateRequest.getHeader();
            TemplateDetails templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteIdAndTemplateName(
                    header.getInstitutionId(), header.getProduct().name(), templateRequest.getTemplateDetails().getTemplateName());
            clientInfo = new HashMap<>();

            if(templateDetails != null && clientInfo !=null){
                response=GngUtils.getBaseResponse(HttpStatus.OK,templateDetails);
            }else{
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());

        }
        return response;
    }

    @Override
    public BaseResponse createTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        try{
            java.lang.reflect.Field[] allFields = TemplateConstants.class.getDeclaredFields();
            if(allFields != null){
                response = GngUtils.getBaseResponse(HttpStatus.OK, allFields);
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return response;
    }


    @Override
    public BaseResponse deleteTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        boolean deleteTemplate=false;
        try {
            Header header = templateRequest.getHeader();
            TemplateDetails templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteIdAndTemplateName(
                    header.getInstitutionId(), header.getProduct().name(), templateRequest.getTemplateDetails().getTemplateName());
            if(templateDetails !=null){
                deleteTemplate = appConfigurationRepository.deleteTemplate(templateDetails);
            }
            if(deleteTemplate){
                response = GngUtils.getBaseResponse(HttpStatus.OK, "Deleted successfully");
            }else{
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());

        }
        return response;
    }

    @Override
    public BaseResponse getAllTemplateDetails(String instituteId) {
        BaseResponse response = null;
        List<TemplateDetails> templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteId(instituteId);
        List<TemplateConfiguration> templateConfigurations = appConfigurationRepository.findVelocityTemplateByInstitution(instituteId);
        TemplateInfo templateInfo = new TemplateInfo();
        templateInfo.setTemplateDetailsList(templateDetails);
        templateInfo.setTemplateConfigurationList(templateConfigurations);
        if (templateDetails != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, templateInfo);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return response;
    }

    @Override
    public BaseResponse genrateTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        Map<String,String> clientInfo=null;
        String refId = templateRequest.getRefId();
        String instituteId = templateRequest.getHeader().getInstitutionId();
        Header header = templateRequest.getHeader();

        try{
            TemplateDetails templateDetails = appConfigurationHelper.generateTemplateUsingApplicationData(refId, instituteId,
                    header.getProduct().name(), templateRequest.getTemplateDetails().getTemplateName(), header.getLoggedInUserRole(),null, null);

            if(templateDetails != null){
                templateRequest.setTemplateDetails(templateDetails);
                response = GngUtils.getBaseResponse(HttpStatus.OK, templateRequest);
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return response;
    }

    @Override
    public BaseResponse intimateUser(IntimationRequest intimationRequest){

        String refId = intimationRequest.getRefId();
        String action = intimationRequest.getAction();
        BaseResponse response;
        try {
            appConfigurationHelper.intimateUsers(refId, action);
            response = GngUtils.getBaseResponse(HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error during call intimateUsers()");
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    @Override
    public List<HierarchyMaster> getActiveHierarchyFlow(String instituteId, String product) {
        List<HierarchyMaster> hierarchyList =  appConfigurationHelper.getActiveHierarchyFlow(instituteId, product);
        return hierarchyList;
    }
}
