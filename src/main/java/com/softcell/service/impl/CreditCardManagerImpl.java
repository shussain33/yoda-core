package com.softcell.service.impl;


import com.softcell.config.CreditCardSurrogateConfig;
import com.softcell.constants.CustomHttpStatus;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.surrogate.CreditCardSurrogateRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogatEntries;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateResponse;
import com.softcell.gonogo.service.CreditCardSurrogateCaller;
import com.softcell.service.CreditCardManager;
import com.softcell.utils.GngUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author yogeshb
 */
@Service
public class CreditCardManagerImpl implements CreditCardManager {

    @Autowired
    private CreditCardSurrogateCaller creditCardSurrogateCaller;

    @Autowired
    private CreditCardSurrogateRepository creditCardSurrogateRepository;

    @Override
    public BaseResponse checkCreditCard(
            CreditCardSurrogateRequest creditCardSurrogateRequest) {

        BaseResponse baseResponse;
        creditCardSurrogateRequest.getHeader().setDateTime(new Date());

        CreditCardSurrogateConfig creditCardSurrogateConfig = (CreditCardSurrogateConfig) Cache.URL_CONFIGURATION
                .getCreditCardSurrogateConfig().get(
                        creditCardSurrogateRequest.getHeader()
                                .getInstitutionId());


        if (null == creditCardSurrogateConfig) {

            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                     .build());

            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        /**
         * Following condition check dedupe is enabled from app or not
         * and also check this credit card response available in database or not
         */
        if (creditCardSurrogateConfig.isDedupe()
                && creditCardSurrogateRepository
                .isExist(creditCardSurrogateRequest)) {

            return GngUtils.getBaseResponse(HttpStatus.OK, creditCardSurrogateRepository
                    .getCreditCardSurrogateDetails(creditCardSurrogateRequest));
        }

        /**
         * Api call
         */
        CreditCardSurrogateResponse creditCardSurrogateResponse = creditCardSurrogateCaller
                .callToCreditCardSurrogateApi(creditCardSurrogateRequest,
                        creditCardSurrogateConfig);

        /**
         * Store CreditCardSurrogate request and response in database.
         */

        CreditCardSurrogatEntries creditCardSurrogatEntries = new CreditCardSurrogatEntries();
        creditCardSurrogatEntries
                .setCreditCardSurrogateRequest(creditCardSurrogateRequest);
        creditCardSurrogatEntries
                .setCreditCardSurrogateResponse(creditCardSurrogateResponse);

        creditCardSurrogateRepository
                .saveCreditCardSurrogateTransaction(creditCardSurrogatEntries);
        if (null != creditCardSurrogateResponse) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, creditCardSurrogateResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

}
