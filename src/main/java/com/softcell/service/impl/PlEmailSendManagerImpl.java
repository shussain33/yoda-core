package com.softcell.service.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.EmailServiceConfiguration;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.MailRequest;
import com.softcell.gonogo.model.security.MessagingServiceResponse;
import com.softcell.service.PlEmailSendManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author yogeshb
 */
//TODO cleanup http
@Service
public class PlEmailSendManagerImpl implements PlEmailSendManager {
    private static final Logger logger = LoggerFactory.getLogger(PlEmailSendManagerImpl.class);

    @Override
    public boolean sendEmailToClient(
            MailRequest attachementDOMailRequest) {
        EmailServiceConfiguration emailUrlConfig = Cache.URL_CONFIGURATION.getEmailServiceConfigurationForPlAndCcbt().get(attachementDOMailRequest.getInstitutionId());
        String jsonForMail = getRequestJson(attachementDOMailRequest);

        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response;
        HttpPost post = null;

        try {
            post = new HttpPost(emailUrlConfig.getEmailUrl());
            StringEntity se = new StringEntity(jsonForMail);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            if (response != null) {
                MessagingServiceResponse plMailServiceResponse = getAadharObject(getResponseJson(new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()))));

                if (null != plMailServiceResponse && plMailServiceResponse.getStatus().equals("OK")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (null != post) {
                post.releaseConnection();
            }
        }

    }

    private MessagingServiceResponse getAadharObject(String tempResponse) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(tempResponse, MessagingServiceResponse.class);
        } catch (JsonParseException e) {
            logger.error("Error in JsonParseException: " + e);
            return null;
        } catch (JsonMappingException e) {
            logger.error("Error in JsonMappingException: " + e);
            return null;
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }


    private String getRequestJson(MailRequest attachementDOMailRequest) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(attachementDOMailRequest);
        } catch (JsonGenerationException e1) {
            logger.error("Error in JsonGenerationException: " + e1);
            return null;
        } catch (JsonMappingException e1) {
            logger.error("Error in JsonMappingException: " + e1);
            return null;
        } catch (IOException e1) {
            logger.error("Error in IOException: " + e1);
            return null;
        }
    }

    private String getResponseJson(BufferedReader rd) {

        StringBuilder tempResponse = new StringBuilder();
        String line;
        try {
            if (null != rd) {
                while ((line = rd.readLine()) != null) {
                    if (StringUtils.isBlank(tempResponse)) {
                        tempResponse.append(line);
                    } else {
                        tempResponse.append(tempResponse).append(" ")
                                .append(line);
                    }
                }
                return tempResponse.toString();
            } else {
                return null;
            }
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }

}
