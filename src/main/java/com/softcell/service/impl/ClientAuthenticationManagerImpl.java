package com.softcell.service.impl;


import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.config.*;
import com.softcell.constants.*;
import com.softcell.constants.aadhar.KycRequestType;
import com.softcell.constants.aadhar.KycRequestVersion;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.LoggerRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.UrlConfigurationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.OtpVerificationData;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.DefaultBaseResponse;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarHeader;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequestNew;
import com.softcell.gonogo.model.core.kyc.request.aadhar.ClientAadhaarRequest;
import com.softcell.gonogo.model.core.kyc.response.Errors;
import com.softcell.gonogo.model.core.kyc.response.aadhar.*;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.request.emudra.*;
import com.softcell.gonogo.model.request.smsservice.SmsServiceBaseRequest;
import com.softcell.gonogo.model.response.GenericMsgResponse;
import com.softcell.gonogo.model.response.SmsResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.smsservice.SmsServiceBaseResponse;
import com.softcell.gonogo.model.security.GetOtpRequest;
import com.softcell.gonogo.model.security.GetOtpResponse;
import com.softcell.gonogo.model.security.MessagingServiceResponse;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.service.AadharServiceCaller;
import com.softcell.gonogo.service.SmsServiceCaller;
import com.softcell.gonogo.service.factory.AadharEntityBuilder;
import com.softcell.gonogo.service.factory.MessagingServicesBuilder;
import com.softcell.gonogo.service.factory.SMSReqBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.RandomUniqueIdGenerate;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.*;
import com.softcell.utils.aadharpidblock.AadharResponseReader;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * This class is service layer for ClientAuthenticationController
 *
 * @author yogeshb
 */
@EnableAsync
@Service
public class ClientAuthenticationManagerImpl implements
        ClientAuthenticationManager {

    private static final Logger logger = LoggerFactory.getLogger(ClientAuthenticationManagerImpl.class);

    @Autowired
    private LoggerRepository loggerRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    private URLConfiguration urlConfiguration = Cache.URL_CONFIGURATION;

    @Autowired
    private AadharEntityBuilder aadharEntityBuilder;

    @Autowired
    private AadharServiceCaller serviceCaller;

    @Autowired
    private SmsServiceCaller smsServiceCaller;

    @Autowired
    private MessagingServicesBuilder messagingServicesBuilder;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private SMSReqBuilder sMSReqBuilder;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private AsyncJob asyncJob;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private UrlConfigurationRepository urlConfigurationRepository;

    @Override
    public BaseResponse sendOtp(GetOtpRequest getOtpRequest) throws Exception {

        logger.info("inside sendOtp service :" + getOtpRequest);
        BaseResponse baseResponse;
        getOtpRequest.getHeader().setDateTime(new Date());
        OtpLog workFlowLog = new OtpLog();
        workFlowLog.setStartTime(new Date());

        GetOtpResponse getOtpResponse = new GetOtpResponse();
        String mobileNumber = getOtpRequest.getOtpDetails().getMobileNumber();
        String otp = GngUtils.generateOtp();

        OtpConfiguration config = Cache.URL_CONFIGURATION.getOtpConfiguration().
                get(getOtpRequest.getHeader().getInstitutionId());

        if (null != config) {
            MessagingServiceResponse smsServiceResponse = (MessagingServiceResponse) TransportUtils.
                    postJsonRequest(messagingServicesBuilder.buildSmsRequest(mobileNumber, otp), config.getOtpServiceURL(),
                            MessagingServiceResponse.class);


            if (smsServiceResponse != null) {
                if (smsServiceResponse.getStatus().equals(Status.OK.name())) {
                    getOtpResponse.setOtp(otp);
                    getOtpResponse.setStatus(Status.SUCCESS);
                } else {
                    getOtpResponse.setStatus(Status.FAILED);
                    getOtpResponse.setOtp("");
                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, getOtpResponse);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);
        }

        workFlowLog.setDsaID(getOtpRequest.getHeader().getDsaId());
        workFlowLog.setInstitutionID(getOtpRequest.getHeader().getInstitutionId());
        workFlowLog.setMobileNumber(mobileNumber);
        workFlowLog.setOtp(otp);
        workFlowLog.setEndTime(new Date());
        loggerRepository.setLogDetails(workFlowLog);

        return baseResponse;
    }

    @Override
    public BaseResponse getOtpByAadhar(
            ClientAadhaarRequest aadharOtpRequest) throws Exception {

        try {
            aadharOtpRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }
        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildOtpRequest(aadharOtpRequest);

        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = serviceCaller
                .callAadharOtpService(aadhaarRequest, aadharOtpRequest
                        .getHeader().getInstitutionId());


        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);

        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getOtpResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient.setTransactionId(aadhaarRequest.getKycRequest().getOtpDetails().getTransactionIdentifier());

                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getOtpResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    @Override
    public BaseResponse verifyOtpByAadharAndGetEkycDeatils(
            ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception {
        try {
            aadharOtpWithOtherDeatilsRequest.getHeader()
                    .setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */

        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildEkycUsingOtpRequestV2(aadharOtpWithOtherDeatilsRequest);

        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = null;

        aadharMainResponse = serviceCaller.callAadharEkycService(
                aadhaarRequest, aadharOtpWithOtherDeatilsRequest.getHeader()
                        .getInstitutionId());


        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpWithOtherDeatilsRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);

                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);

            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    //This method uses OLD AADHAR request without HSM.
    @Override
    public BaseResponse verifyBioByAadharAndGetEkycDetails(
            ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {
        try {
            aadharEkycwithBioRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildBiometricRequest(aadharEkycwithBioRequest);

        /**
         * Call to Aadhar ekyc Service
         */
        AadharMainResponse aadharMainResponse = null;

        aadharMainResponse = serviceCaller.callAadharBiometricService(
                aadhaarRequest, aadharEkycwithBioRequest.getHeader()
                        .getInstitutionId());

        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharEkycwithBioRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        //        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved." + aadharLog);
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    //This method uses NEW AADHAR request with HSM.
    @Override
    public BaseResponse verifyBioByAadharAndGetEkycDetailsV2_2(
            ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {
        try {
            aadharEkycwithBioRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildBiometricRequest(aadharEkycwithBioRequest);
        /*
        * Create Aadhaar XML
        * */

        String xml = aadharEntityBuilder.createAadhaarXml(aadhaarRequest, aadharEkycwithBioRequest.getHeader()
                .getInstitutionId());
        logger.debug("XML created->" + xml);
        /*String signXML= DigitallySignRequest.signRequestString(xml, true);*/
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();
        AadhaarRequestNew aadhaarRequestNew = null;
        AadharMainResponse aadharMainResponse = null;
        if (xml != null && !"failure".equalsIgnoreCase(xml)) {
            aadhaarRequestNew = AadhaarRequestNew.builder()
                    .aadhaarXml(xml)
                    .header(AadhaarHeader.builder()
                            .applicationId(aadhaarRequest.getHeader().getApplicationId())
                            .aadhaarNumber0(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(0)))
                            .aadhaarNumber1(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(1)))
                            .asaSignReqd("Y")
                            .auaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAuaCode())
                            .kycType(aadhaarRequest.getKycRequest().getAadharHolderDetails().getKycType())
                            .requestTime(GngDateUtil.toAadhaarRequest(new Date().getTime()))
                            .requestType(aadhaarRequest.getHeader().getRequestType())
                            .subAuaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getSubAuaCode())
                            .terminalId(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTerminalId())
                            .transactionIdentifier(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTransactionIdentifier())
                            .version(aadhaarRequest.getKycRequest().getAadharHolderDetails().getVersion()).build()
                    ).build();
            /**
             * Call to Aadhar Otp Service
             */

            try {
                aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2(
                        aadhaarRequestNew, aadharEkycwithBioRequest.getHeader()
                                .getInstitutionId());
            } catch (Exception e) {
                logger.error("Exception occured with message: {}", e.getMessage());
            }
            logger.debug("Response fetched->" + aadharMainResponse);

        /*aadharMainResponse = serviceCaller.callAadharBiometricService(
                aadhaarRequest, aadharEkycwithBioRequest.getHeader()
                        .getInstitutionId());*/


            AadharLog aadharLog = new AadharLog();
            aadharLog.setAadharClientRequest(aadharEkycwithBioRequest);
            aadharLog.setAadhaarRequest(aadhaarRequest);
            aadharLog.setAadharMainResponse(aadharMainResponse);
            boolean status = false;
            //        status = loggerRepository.setAadharLogDeatails(aadharLog);
            if (status) {
                logger.info("aadhar log saved." + aadharLog);
            } else {
                logger.info("aadhar log not saved.");
            }
            Resp kycResponse = null;
            KycResp kycDecryptedResponse = null;
            AadharResponseReader parseResponse = new AadharResponseReader();

            if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
                DataDecryptor dataDecryptor = new DataDecryptor();
                kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
                if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                    if (kycResponse.getKycResponse().length() == 0) {
                        throw new Exception(
                                "KYC response xml retured a status of -1, no content found.");
                    }
                }
                String kycRes = kycResponse.getKycResponse();

                String dcrpxml = "";
                if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

                } else {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(decode);
                }

                if (dataDecryptor.verify(dcrpxml)) {
                    kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
                } else {
                    logger.error("KYC response xml signature cam failed.");
                }
                aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

                aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

                if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getTxnStatus())) {
                    if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())) {
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient
                                .setKycResponseDetails(aadharMainResponse
                                        .getKycResponse().getKycResponseDetails());
                    } else if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.ERROR,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())
                            && CollectionUtils.isNotEmpty(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors())) {

                        List<Errors> errorList = aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getErrors();
                        Errors error = errorList.get(0);
                        AadharError aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient.setResult(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getKycResponse().getResult());
                        aadharResponseToClient.setError(aadharError);
                        aadharLog.setGNGstatus(Constant.FAILED);
                        aadharLog.setGNGStatusDtl(aadharError.toString());

                    }
                } else {

                    AadharError aadharError = null;
                    if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                        Errors error = aadharMainResponse.getErrors().get(0);
                        aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharLog.setGNGStatusDtl(aadharError.toString());
                    }

                    if (aadharMainResponse.getKycResponse() != null) {
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                    }
                    aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                            .getAcknowledgementId());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);


                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXP_AADHAAR_XML)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    //This method uses OLD AADHAR request with HSM.
    public BaseResponse verifyBioByAadharAndGetEkycDetailsV2_2_NEW(
            ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {

        try {
            aadharEkycwithBioRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildBiometricRequest(aadharEkycwithBioRequest);

        /**
         * Call to Aadhar ekyc Service
         */
        AadharMainResponse aadharMainResponse = null;

        aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2_NEW(
                aadhaarRequest, aadharEkycwithBioRequest.getHeader()
                        .getInstitutionId());

        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharEkycwithBioRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        //        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved." + aadharLog);
        } else {
            logger.info("aadhar log not saved.");
        }

        Resp kycResponse = null;
        KycResp kycDecryptedResponse = null;
        AadharResponseReader parseResponse = new AadharResponseReader();

        if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
            DataDecryptor dataDecryptor = new DataDecryptor();
            kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
            if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                if (kycResponse.getKycResponse().length() == 0) {
                    throw new Exception(
                            "KYC response xml retured a status of -1, no content found.");
                }
            }
            String kycRes = kycResponse.getKycResponse();

            String dcrpxml = "";
            if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

            } else {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(decode);
            }

            if (dataDecryptor.verify(dcrpxml)) {
                kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
            } else {
                logger.error("KYC response xml signature cam failed.");
            }
            aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())
                        && CollectionUtils.isNotEmpty(aadharMainResponse
                        .getKycResponse().getKycResponseDetails()
                        .getErrors())) {

                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());

                }
            } else {

                AadharError aadharError = null;
                if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                    Errors error = aadharMainResponse.getErrors().get(0);
                    aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }

                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
                aadharLog.setGNGstatus(Constant.FAILED);


            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);

    }

    @Override
    public BaseResponse verifyIrisByAadharAndGetEkycDeatils(
            ClientAadhaarRequest clientAadhaarRequest) {
        try {
            clientAadhaarRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * build pid request json
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildIrisRequest(clientAadhaarRequest);
        /**
         * Call to Aadhar ekyc Service
         */
        AadharMainResponse aadharMainResponse = null;
        try {
            aadharMainResponse = serviceCaller.callAadharEkycService(
                    aadhaarRequest, clientAadhaarRequest.getHeader()
                            .getInstitutionId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(clientAadhaarRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    @Override
    public BaseResponse sendSms(SmsRequest smsRequest) {

        SmsResponse smsResponse = null;
        String institutionId = smsRequest.getHeader().getInstitutionId();
        String otpMessage = RandomUniqueIdGenerate.generateUniqueId(6).toUpperCase();
        SmsServiceReqResLog smsServiceReqResLog = null;
        if(Institute.isInstitute(institutionId,Institute.SBFC)){
            otpMessage = RandomUniqueIdGenerate.generateNumericOTP(6);
        }

        List<SmsServiceReqResLog> smsServiceLogList = applicationRepository.fetchSmsServiceLog(smsRequest.getIdentifier());

        if(CollectionUtils.isNotEmpty(smsServiceLogList) && !checkMaxTry(institutionId, smsServiceLogList)){

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.LIMIT_EXCEEDED)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.FORBIDDEN, errors);
        }else{

            smsServiceReqResLog = createSmsServiceLog(smsRequest, smsServiceLogList);
        }
        SmsServiceConfiguration smsServiceConfiguration = new SmsServiceConfiguration();

        ActivityLogs activityLog = auditHelper.createActivityLog(smsRequest.getHeader(),smsRequest.getRefId(),null ,null, EndPointReferrer.SEND_SMS, null, smsRequest.getHeader().getLoggedInUserId());
        activityLog.setAction("SEND_SMS");

        ServiceCommunication serviceCommunication = urlConfigurationRepository.getServiceCommunication(smsRequest.getHeader().getInstitutionId(),ConfigType.SMSSERVICE );
        if (serviceCommunication == null &&
                (Cache.URL_CONFIGURATION.getSmsServiceConfiguration() == null
                        || Cache.URL_CONFIGURATION.getSmsServiceConfiguration().get(
                        smsRequest.getHeader().getInstitutionId()) == null)) {

            logger.error("Sms service app not found on server.");

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);

        } else {
            try {
                SmsServiceBaseRequest smsServiceBaseRequest = new SmsServiceBaseRequest();
                smsServiceBaseRequest.map(smsRequest);
                if(serviceCommunication != null){
                    ConfigType.convertDataToSmsServiceConfiguration(serviceCommunication,smsServiceConfiguration );
                }else{
                    smsServiceConfiguration = Cache.URL_CONFIGURATION.getSmsServiceConfiguration().get(
                            smsRequest.getHeader().getInstitutionId());
                }
                if (smsRequest.isOtpSms()) {
                    //Get the SMS contents
                    SmsTemplateConfiguration smsTemplateConfiguration =
                            lookupService.getSmsTemplateConfiguration(
                                    smsRequest.getHeader().getInstitutionId()
                                    , smsRequest.getHeader().getProduct().toProductId()
                                    , GNGWorkflowConstant.OTP_SMS.name());
                    String smsMsg = null;
                    if (smsTemplateConfiguration != null) {
                        if(getPlaceHolderCount(smsTemplateConfiguration.getSmsText())>1) {
                            smsMsg = smsTemplateConfiguration.getFormattedSMSText(smsRequest.getRefId(), otpMessage);
                        } else {
                            smsMsg = smsTemplateConfiguration.getFormattedSMSText(otpMessage);
                        }
                    } else {
                        if (StringUtils.isNotBlank(smsRequest.getRefId())) {
                            smsMsg = "Your OTP for GoNoGo Application with RefId:" + smsRequest.getRefId() + " is " + otpMessage;
                        } else {
                            smsMsg = "Your OTP for GoNoGo Application is " + otpMessage;
                        }
                    }
                    smsServiceBaseRequest.setMessage(smsMsg);
                    logger.debug("OUT OF templateDetails LOOP {} " ,smsRequest.getRefId());
                    if(Institute.isInstitute(smsRequest.getHeader().getInstitutionId(), Institute.SBFC)) {

                        logger.debug("In templateDetails LOOP {} " ,smsRequest.getRefId());
                        TemplateDetails templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteIdAndProduct(
                                smsRequest.getHeader().getInstitutionId(), smsRequest.getHeader().getProduct().name() );
                        if (templateDetails != null) {
                            Matcher matcher;
                            Pattern pattern = Pattern.compile("\\{\\{([^}]+)\\}\\}");

                            if (templateDetails.getTemplateContent() != null) {
                                // create pattern to find application fields for template content
                                String content = templateDetails.getTemplateContent();
                                matcher = pattern.matcher(content);

                                // Get application fields configured in template
                                List<String> fields = new ArrayList<>();
                                while (matcher.find()) {
                                    String key = matcher.group(1);
                                    fields.add(key);
                                }
                                if (CollectionUtils.isNotEmpty(fields)) {
                                    for (String temp : fields) {
                                        switch (temp) {
                                            case "REF_ID":
                                                content = content.replaceAll("\\{\\{" + temp + "\\}\\}"
                                                        , smsRequest.getRefId());
                                                break;

                                            case "OTP":
                                                content = content.replaceAll("\\{\\{" + temp + "\\}\\}"
                                                        , otpMessage);
                                                break;
                                        }
                                    }
                                }
                                content = content.replaceAll("\\{\\{", " ");
                                content = content.replaceAll("\\}\\}", " ");
                                content = content.replaceAll("&nbsp;", "");
                                content = content.replaceAll("\\<[^>]*>", "");

                                logger.debug("In templateDetails refid {} {}" ,smsRequest.getRefId(), content);
                                smsServiceBaseRequest.setMessage(content);
                                logger.debug("In templateDetails--2  refid {} {}" , smsRequest.getRefId(), smsServiceBaseRequest.getMessage());
                            }
                        }
                    }

                    smsServiceBaseRequest.setByteStringMsg(smsMsg.getBytes(StandardCharsets.UTF_8));
                    logger.info("SmsRequest for OTP:- " + JsonUtil.ObjectToString(smsServiceBaseRequest));
                }
                //smsServiceBaseRequest.setOtpMsg(!smsRequest.isOtpSms());  //sending false to smsservice
                smsServiceBaseRequest.setOtpMsg(false);  //sending false to smsservice
                smsServiceReqResLog.setOrgReq(smsServiceBaseRequest.toString());
                // Call Sms service and then take further action based on
                // response from Sms Service.
                SmsServiceBaseResponse smsServiceBaseResposne = smsServiceCaller.callSmsService(smsServiceBaseRequest,smsServiceConfiguration.getUrl());
                if (smsServiceBaseResposne == null) {
                    smsResponse = SmsUtils.getSmsErrorResponse(
                            "Sms sending failed, try again", null);
                    logger.error("SmsServiceBase Response was null or empty");

                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
                } else {
                    if (smsServiceBaseResposne.getErrors() != null
                            && smsServiceBaseResposne.getErrors().length > 0) {
                        smsResponse = SmsUtils.getSmsErrorResponse(
                                "Sms sending failed, try again",
                                smsServiceBaseResposne.getSmsStatus());
                        for (com.softcell.gonogo.model.response.smsservice.Error error : smsServiceBaseResposne.getErrors()) {
                            logger.error(error.toString());
                        }
                    } else {
                        smsResponse = SmsUtils.getSmsSuccessResponse(otpMessage,smsServiceBaseResposne.getSmsStatus());
                        if(Cache.isSentOtpDisable(smsRequest.getHeader().getInstitutionId())){
                            removeOtpFromResponse(institutionId,smsResponse);
                        }
                        smsResponse.setIdentifier(smsServiceReqResLog.getIdentifier());

                        smsServiceReqResLog.setOrgRes(smsResponse.toString());
                        externalAPILogRepository.saveSmsServiceReqResLog(smsServiceReqResLog);
                    }
                }
            } catch (IOException e) {
                smsResponse = SmsUtils.getSmsErrorResponse(
                        "Sms sending failed, try again", null);
                logger.error(e.toString());
            }
        }

        logger.debug("Send SMS service publishing activity logs");
        activityEventPublisher.publishEvent(activityLog);
        return GngUtils.getBaseResponse(HttpStatus.OK, smsResponse);
    }

    private boolean checkMaxTry(String institutionId, List<SmsServiceReqResLog> smsServiceLogList){
        if(smsServiceLogList.size() >= 3)
            return false;

        return true;
    }

    private SmsServiceReqResLog createSmsServiceLog(SmsRequest smsRequest, List<SmsServiceReqResLog> smsServiceLogList){
        SmsServiceReqResLog smsServiceReqResLog = null;

        if(CollectionUtils.isNotEmpty(smsServiceLogList)){

            smsServiceReqResLog = SmsServiceReqResLog.builder()
                    .identifier(smsRequest.getIdentifier())
                    .institutionId(smsRequest.getHeader().getInstitutionId())
                    .mobileNumber(smsRequest.getMobileNumber()[0]).build();

        }else{
            smsServiceReqResLog = SmsServiceReqResLog.builder()
                    .identifier(smsRequest.getMobileNumber()[0] +"-"+ UUID.randomUUID().toString())
                    .institutionId(smsRequest.getHeader().getInstitutionId())
                    .mobileNumber(smsRequest.getMobileNumber()[0]).build();
        }

        return smsServiceReqResLog;
    }

    private void removeOtpFromResponse(String institutionId, SmsResponse smsResponse) {
        if(StringUtils.isNotEmpty(smsResponse.getOtp())){
            OtpVerificationData otpVerificationData = OtpVerificationData.builder()
                    .id(UUID.randomUUID().toString())
                    .institutionId(institutionId)
                    .otp(smsResponse.getOtp())
                    .retry(3)
                    .purpose(GNGWorkflowConstant.MOBILE.toFaceValue())
                    .build();
            applicationRepository.saveVerificationOtp(otpVerificationData);
            smsResponse.setOtp(null);
            smsResponse.setUuid(otpVerificationData.getId());
        }
    }

    private int getPlaceHolderCount(String smsText) {
        return org.springframework.util.StringUtils.countOccurrencesOf(smsText,"%s");
    }

    @Override
    public BaseResponse getOtpByAadharV2_1(ClientAadhaarRequest aadharOtpRequest) throws Exception {


        try {
            aadharOtpRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }
        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildOtpRequestV2_1(aadharOtpRequest);


        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = serviceCaller
                .callAadharOtpServiceV2_1(aadhaarRequest, aadharOtpRequest
                        .getHeader().getInstitutionId());


        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);

        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS, aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getOtpResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient.setTransactionId(aadhaarRequest.getKycRequest().getOtpDetails().getTransactionIdentifier());

                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse().getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR, aadharMainResponse.getKycResponse().getOtpResponseDetails().getUidaiStatus())
                        && !CollectionUtils.isEmpty(aadharMainResponse.getKycResponse().getOtpResponseDetails().getErrors())) {

                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);

                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());

                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    //set aadhar error
                    aadharResponseToClient.setError(aadharError);
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                }
            } else {
                AadharError aadharError = null;

                if (!CollectionUtils.isEmpty(aadharMainResponse.getErrors())) {

                    aadharError = new AadharError();
                    Errors error = aadharMainResponse.getErrors().get(0);
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                }
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse().getOtpResponseDetails() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails().getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    @Override
    public BaseResponse getOtpByAadharV2_2(ClientAadhaarRequest aadharOtpRequest) throws Exception {


        try {
            aadharOtpRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }
        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildOtpRequestV2_1(aadharOtpRequest);

        String xml = aadharEntityBuilder.createOTPXml(aadhaarRequest, aadharOtpRequest.getHeader()
                .getInstitutionId());

        AadhaarRequestNew aadhaarRequestNew = AadhaarRequestNew.builder()
                .aadhaarXml(xml)
                .header(AadhaarHeader.builder()
                        .applicationId(aadhaarRequest.getHeader().getApplicationId())
                        .requestType(aadhaarRequest.getHeader().getRequestType())
                        .aadhaarNumber0(String.valueOf(aadhaarRequest.getKycRequest().getOtpDetails().getAadharNumber().charAt(0)))
                        .aadhaarNumber1(String.valueOf(aadhaarRequest.getKycRequest().getOtpDetails().getAadharNumber().charAt(1)))
                        .asaSignReqd("Y")
                        .auaCode(aadhaarRequest.getKycRequest().getOtpDetails().getAuaCode())
                        .kycType("otp")
                        .requestTime(GngDateUtil.toAadhaarRequest(new Date().getTime()))
                        .subAuaCode(aadhaarRequest.getKycRequest().getOtpDetails().getSubAuaCode())
                        .terminalId(aadhaarRequest.getKycRequest().getOtpDetails().getTerminalId())
                        .transactionIdentifier(aadhaarRequest.getKycRequest().getOtpDetails().getTransactionIdentifier())
                        .version(aadhaarRequest.getKycRequest().getOtpDetails().getVersion()).build()
                ).build();

        AadharMainResponse aadharMainResponse = null;
        logger.debug("Request created->" + aadhaarRequestNew);
            /*aadharMainResponse = (AadharMainResponse) TransportUtils.postJsonRequest(aadhaarRequestNew, "http://172.30.155.85:90/SoftcellKSA/kyc/ksaRequest.action", AadharMainResponse.class);*/
        try {
            aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2(
                    aadhaarRequestNew, aadharOtpRequest.getHeader()
                            .getInstitutionId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.debug("Response created->" + aadharMainResponse);

        OtpResponseReader otpResponseParser = new OtpResponseReader();
        XMLOtpResponse parseOtpResponse = null;
        AsaServiceErrorResponse asaErrorResponse = null;
        if (aadharMainResponse != null && aadharMainResponse.getResponseXMl() != null) {
            if (StringUtils.startsWith(aadharMainResponse.getResponseXMl().trim(), "<?xml")) {
                if (StringUtils.contains(aadharMainResponse.getResponseXMl(), "<ERROR_RESPONSE>")) {
                    asaErrorResponse = otpResponseParser.parseAsaErrorResponse_(aadharMainResponse.getResponseXMl());
                    aadharMainResponse.setErrors(asaErrorResponse.getErrors());
                    aadharMainResponse.setTxnStatus(Constant.ERROR);
                } else {
                    parseOtpResponse = otpResponseParser.parseOtpResponse(aadharMainResponse.getResponseXMl());
                    aadharMainResponse.setKycResponse(otpResponseParser.getOtpResponseDetailsObject(parseOtpResponse));
                }
            } else {
                aadharMainResponse.setTxnStatus(Constant.ERROR);
                List<Errors> listError = new ArrayList<Errors>();
                Errors errors = new Errors();
                errors.setCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));
                errors.setDescription("Error in Aadhaar OTP XML response");
                listError.add(errors);
                aadharMainResponse.setErrors(listError);
                logger.error("Error in Aadhaar OTP XML response");
            }
        }
        logger.debug("Parsed OTP resp------------------->" + parseOtpResponse);

        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);

        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getOtpResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient.setTransactionId(aadhaarRequest.getKycRequest().getOtpDetails().getTransactionIdentifier());

                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getOtpResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getOtpResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getOtpResponseDetails()
                            .getUidaiStatus());
                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getOtpResponseDetails() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse.getKycResponse()
                            .getOtpResponseDetails().getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
        }
        logger.debug("Resp to client--------------------->" + aadharResponseToClient);
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    /*New method with aadharLog Changes.*/
    @Override
    public BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_1_1(ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception {
        boolean status = false;
        try {
            aadharOtpWithOtherDeatilsRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder.buildEkycUsingOtpRequestV2_1(aadharOtpWithOtherDeatilsRequest);

        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = null;
        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpWithOtherDeatilsRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        DefaultBaseResponse defaultBaseResponse = null;

        BaseResponse aadharServBaseResponse = serviceCaller.callAadharEkycServiceV2_1_1(aadhaarRequest
                , aadharOtpWithOtherDeatilsRequest.getHeader().getInstitutionId());

        if (aadharServBaseResponse != null && aadharServBaseResponse.getStatus().getStatusCode() == HttpStatus.OK.value() && aadharServBaseResponse.getPayload() != null) {
            aadharLog.setGNGstatus(Constant.SUCCESS);
            aadharLog.setGNGStatusDtl(ErrorCode.GNG_SUCCESS_RESPONSE);
            aadharMainResponse = (AadharMainResponse) aadharServBaseResponse.getPayload().getT();
        } else {
            defaultBaseResponse = (DefaultBaseResponse) aadharServBaseResponse.getPayload().getT();
        }

        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();
        aadharLog.setAadharMainResponse(aadharMainResponse);
        BaseResponse baseResponse = null;

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS, aadharMainResponse.getTxnStatus())) {

                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse().getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse.getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus());
                    aadharResponseToClient.setAcknowledgementId(aadharMainResponse.getAcknowledgementId());
                    aadharResponseToClient.setKycResponseDetails(aadharMainResponse.getKycResponse().getKycResponseDetails());

                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(ErrorCode.GNG_SUCCESS_RESPONSE);
                    aadharLog.setEKycStatus(Constant.SUCCESS);
                    aadharLog.setEKycStatusDtl(ErrorCode.KYC_SUCCESS_RESPONSE);
                    aadharLog.setUIDAIStatus(Constant.SUCCESS);
                    aadharLog.setUIDAIStatusDtl(ErrorCode.UIDAI_SUCCESS_RESPONSE);

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
                } else if (aadharMainResponse.getKycResponse() != null && aadharMainResponse.getKycResponse().getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR, aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse.getKycResponse().getKycResponseDetails().getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse.getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus());
                    aadharResponseToClient.setAcknowledgementId(aadharMainResponse.getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse.getKycResponse().getKycResponseDetails().getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);

                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(ErrorCode.GNG_SUCCESS_RESPONSE);
                    aadharLog.setEKycStatus(Constant.SUCCESS);
                    aadharLog.setEKycStatusDtl(ErrorCode.KYC_SUCCESS_RESPONSE);
                    aadharLog.setUIDAIStatus(Constant.ERROR);
                    aadharLog.setUIDAIStatusDtl(errorList.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining(",")));

                    List<String> errList = new ArrayList<>();
                    errList.add(ErrorCode.GNG_SUCCESS_RESPONSE);
                    errList.add((ErrorCode.KYC_SUCCESS_RESPONSE));
                    errList.add(errorList.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining(",")));

                    aadharResponseToClient.setErrorList(errList);

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
                }
            } else {
                List<Errors> errorList = aadharMainResponse.getErrors();
                Errors error = errorList.get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse.getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null)
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus());

                aadharResponseToClient.setAcknowledgementId(aadharMainResponse.getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);

                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(ErrorCode.GNG_TRANSACTION_FAILURE);
                aadharLog.setEKycStatus(Constant.FAILED);
                aadharLog.setEKycStatusDtl(ErrorCode.KYC_TRANSACTION_FAILURE);
                aadharLog.setUIDAIStatus(Constant.FAILED);
                aadharLog.setUIDAIStatusDtl(ErrorCode.UIDAI_TRANSACTION_FAILURE);

                List<String> errList = new ArrayList<>();
                errList.add(ErrorCode.GNG_TRANSACTION_FAILURE);
                errList.add(ErrorCode.KYC_TRANSACTION_FAILURE);
                errList.add(ErrorCode.UIDAI_TRANSACTION_FAILURE);

                aadharResponseToClient.setErrorList(errList);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            if (defaultBaseResponse != null) {
                aadharLog.setGNGstatus(Constant.ERROR);
                aadharLog.setGNGStatusDtl(defaultBaseResponse.getResMessage());
            } else {
                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(ErrorCode.GNG_ERROR_RESPONSE);
            }
            aadharLog.setEKycStatus(ErrorCode.NOT_AVAILABLE);
            aadharLog.setEKycStatusDtl(ErrorCode.KYC_RESPONSE);
            aadharLog.setUIDAIStatus(ErrorCode.NOT_AVAILABLE);
            aadharLog.setUIDAIStatusDtl(ErrorCode.UIDAI_RESPONSE);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);
        }

        status = loggerRepository.setAadharLogDeatails(aadharLog);

        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }
        return baseResponse;
    }

    @Override
    public BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_1(ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception {
        boolean status = false;
        try {

            aadharOtpWithOtherDeatilsRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */

        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildEkycUsingOtpRequestV2_1(aadharOtpWithOtherDeatilsRequest);

        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = null;
        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpWithOtherDeatilsRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);

        try {
            aadharMainResponse = serviceCaller.callAadharEkycServiceV2_1(
                    aadhaarRequest, aadharOtpWithOtherDeatilsRequest.getHeader()
                            .getInstitutionId());

        } catch (Exception e) {
            logger.error(e.getMessage());
            aadharLog.setGNGStatusDtl(e.getMessage());
            aadharLog.setGNGstatus(Constant.FAILED);
        }


        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();
        aadharLog.setAadharMainResponse(aadharMainResponse);
        status = loggerRepository.setAadharLogDeatails(aadharLog);

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(Constant.SUCCESS);
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse().getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse().getKycResponseDetails().getUidaiStatus())
                        && CollectionUtils.isNotEmpty(aadharMainResponse.getKycResponse()
                        .getKycResponseDetails().getErrors())) {

                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }
            } else {
                AadharError aadharError = null;

                if (!CollectionUtils.isEmpty(aadharMainResponse.getErrors())) {

                    Errors error = aadharMainResponse.getErrors().get(0);

                    aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                }

                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                // set aadharError
                aadharResponseToClient.setError(aadharError);
                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(aadharError.toString());
            }
            loggerRepository.setAadharLogDeatails(aadharLog);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            aadharLog.setGNGstatus(Constant.FAILED);
            aadharLog.setGNGStatusDtl(errors.toString());
            loggerRepository.setAadharLogDeatails(aadharLog);

            return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }


    //This method uses OLD AADHAR request with HSM.
    @Override
    public BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_2_NEW(ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception {

        try {

            aadharOtpWithOtherDeatilsRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * Create request for Aadhar Request
         */

        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildEkycUsingOtpRequestV2_1(aadharOtpWithOtherDeatilsRequest);

        logger.debug("Input Request->-====================----------------\n\n " + aadhaarRequest.toString());
        /**
         * Call to Aadhar Otp Service
         */
        AadharMainResponse aadharMainResponse = null;

        aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2_NEW(
                aadhaarRequest, aadharOtpWithOtherDeatilsRequest.getHeader()
                        .getInstitutionId());


        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(aadharOtpWithOtherDeatilsRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        Resp kycResponse = null;
        KycResp kycDecryptedResponse = null;
        AadharResponseReader parseResponse = new AadharResponseReader();

        if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
            logger.debug("Input Response->-====================----------------\n\n " + aadharMainResponse.toString());

            DataDecryptor dataDecryptor = new DataDecryptor();
            kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
            if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                if (kycResponse.getKycResponse().length() == 0) {
                    throw new Exception(
                            "KYC response xml retured a status of -1, no content found.");
                }
            }
            String kycRes = kycResponse.getKycResponse();

            String dcrpxml = "";
            if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

            } else {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(decode);
            }

            if (dataDecryptor.verify(dcrpxml)) {
                kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
            } else {
                logger.error("KYC response xml signature cam failed.");
            }
            aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(Constant.SUCCESS);
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())
                        && CollectionUtils.isNotEmpty(aadharMainResponse
                        .getKycResponse().getKycResponseDetails()
                        .getErrors())) {

                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }
            } else {

                AadharError aadharError = null;
                if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                    Errors error = aadharMainResponse.getErrors().get(0);
                    aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                }

                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(aadharError.toString());
            }
            loggerRepository.setAadharLogDeatails(aadharLog);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            aadharLog.setGNGstatus(Constant.FAILED);
            aadharLog.setGNGStatusDtl(errors.toString());
            loggerRepository.setAadharLogDeatails(aadharLog);

            return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }


    //This method uses OLD AADHAR request with HSM.
    @Override
    public BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_2(ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception {

        try {

            aadharOtpWithOtherDeatilsRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }
        /**
         * Create request for Aadhar Request
         */

        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildEkycUsingOtpRequestV2_1(aadharOtpWithOtherDeatilsRequest);

        /*
        * Create Aadhaar XML
        * */

        String xml = aadharEntityBuilder.createAadhaarXml(aadhaarRequest, aadharOtpWithOtherDeatilsRequest.getHeader()
                .getInstitutionId());

        logger.debug("XML created->" + xml);
        /*String signXML= DigitallySignRequest.signRequestString(xml, true);*/
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();
        AadhaarRequestNew aadhaarRequestNew = null;
        AadharMainResponse aadharMainResponse = null;
        if (xml != null && !"failure".equalsIgnoreCase(xml)) {
            aadhaarRequestNew = AadhaarRequestNew.builder()
                    .aadhaarXml(xml)
                    .header(AadhaarHeader.builder()
                            .applicationId(aadhaarRequest.getHeader().getApplicationId())
                            .aadhaarNumber0(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(0)))
                            .aadhaarNumber1(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(1)))
                            .asaSignReqd("Y")
                            .auaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAuaCode())
                            .kycType(aadhaarRequest.getKycRequest().getAadharHolderDetails().getKycType())
                            .requestTime(GngDateUtil.toAadhaarRequest(new Date().getTime()))
                            .requestType(aadhaarRequest.getHeader().getRequestType())
                            .subAuaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getSubAuaCode())
                            .terminalId(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTerminalId())
                            .transactionIdentifier(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTransactionIdentifier())
                            .version(aadhaarRequest.getKycRequest().getAadharHolderDetails().getVersion()).build()
                    ).build();

            /**
             * Call to Aadhar Otp Service
             */
            logger.debug("New Request--------------------->\n\n----------------->" + aadhaarRequestNew);
            try {
                aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2(
                        aadhaarRequestNew, aadharOtpWithOtherDeatilsRequest.getHeader()
                                .getInstitutionId());
            } catch (Exception e) {
                logger.error("Exception occured with message: {}", e.getMessage());
            }
            logger.debug("Response fetched->" + aadharMainResponse);


            AadharLog aadharLog = new AadharLog();
            aadharLog.setAadharClientRequest(aadharOtpWithOtherDeatilsRequest);
            aadharLog.setAadhaarRequest(aadhaarRequest);
            aadharLog.setAadhaarRequestNew(aadhaarRequestNew);
            aadharLog.setAadharMainResponse(aadharMainResponse);
            boolean status = false;
            status = loggerRepository.setAadharLogDeatails(aadharLog);
            if (status) {
                logger.info("aadhar log saved.");
            } else {
                logger.info("aadhar log not saved.");
            }
            Resp kycResponse = null;
            KycResp kycDecryptedResponse = null;
            AadharResponseReader parseResponse = new AadharResponseReader();

            if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
                DataDecryptor dataDecryptor = new DataDecryptor();
                kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
                if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                    if (kycResponse.getKycResponse().length() == 0) {
                        throw new Exception(
                                "KYC response xml retured a status of -1, no content found.");
                    }
                }
                String kycRes = kycResponse.getKycResponse();

                String dcrpxml = "";
                if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

                } else {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(decode);
                }

                if (dataDecryptor.verify(dcrpxml)) {
                    kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
                } else {
                    logger.error("KYC response xml signature cam failed.");
                }
                aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

                aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

                if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getTxnStatus())) {
                    if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())) {
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient
                                .setKycResponseDetails(aadharMainResponse
                                        .getKycResponse().getKycResponseDetails());
                    } else if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.ERROR,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())
                            && CollectionUtils.isNotEmpty(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors())) {

                        List<Errors> errorList = aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getErrors();
                        Errors error = errorList.get(0);
                        AadharError aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient.setResult(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getKycResponse().getResult());
                        aadharResponseToClient.setError(aadharError);
                        aadharLog.setGNGstatus(Constant.FAILED);
                        aadharLog.setGNGStatusDtl(aadharError.toString());
                    }
                } else {

                    AadharError aadharError = null;
                    if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                        Errors error = aadharMainResponse.getErrors().get(0);
                        aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                    }

                    if (aadharMainResponse.getKycResponse() != null) {
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                    }
                    aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                            .getAcknowledgementId());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXP_AADHAAR_XML)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    //This method uses OLD AADHAR request without HSM
    @Override
    public BaseResponse verifyIrisByAadharAndGetEkycDeatilsV2_1(ClientAadhaarRequest clientAadhaarRequest) {

        try {
            clientAadhaarRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * build pid request json
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildIrisRequestV2_1(clientAadhaarRequest);
        /**
         * Call to Aadhar ekyc Service
         */
        AadharMainResponse aadharMainResponse = null;
        try {
            aadharMainResponse = serviceCaller.callAadharEkycServiceV2_1(
                    aadhaarRequest, clientAadhaarRequest.getHeader()
                            .getInstitutionId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(clientAadhaarRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        if (aadharMainResponse != null) {
            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());
            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(Constant.SUCCESS);
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }
            } else {
                Errors error = aadharMainResponse.getErrors().get(0);
                AadharError aadharError = new AadharError();
                aadharError.setCode(error.getCode());
                aadharError.setDescription(error.getDescription());
                aadharResponseToClient.setTxnStatus(aadharMainResponse
                        .getTxnStatus());
                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(aadharError.toString());
            }
            loggerRepository.setAadharLogDeatails(aadharLog);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            aadharLog.setGNGstatus(Constant.FAILED);
            aadharLog.setGNGStatusDtl(errors.toString());
            loggerRepository.setAadharLogDeatails(aadharLog);

            return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }


    //This method uses NEW AADHAR request with HSM.
    @Override
    public BaseResponse verifyIrisByAadharAndGetEkycDeatilsV2_2(ClientAadhaarRequest clientAadhaarRequest) throws Exception {

        try {
            clientAadhaarRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * build pid request json
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildIrisRequestV2_1(clientAadhaarRequest);


        /*
        * Create Aadhaar XML
        * */

        String xml = aadharEntityBuilder.createAadhaarXml(aadhaarRequest, clientAadhaarRequest.getHeader()
                .getInstitutionId());
        logger.debug("XML created->" + xml);
        /*String signXML= DigitallySignRequest.signRequestString(xml, true);*/
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();
        AadhaarRequestNew aadhaarRequestNew = null;
        AadharMainResponse aadharMainResponse = null;

        if (xml != null && !"failure".equalsIgnoreCase(xml)) {
            aadhaarRequestNew = AadhaarRequestNew.builder()
                    .aadhaarXml(xml)
                    .header(AadhaarHeader.builder()
                            .applicationId(aadhaarRequest.getHeader().getApplicationId())
                            .aadhaarNumber0(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(0)))
                            .aadhaarNumber1(String.valueOf(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber().charAt(1)))
                            .asaSignReqd("Y")
                            .auaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getAuaCode())
                            .kycType(aadhaarRequest.getKycRequest().getAadharHolderDetails().getKycType())
                            .requestTime(GngDateUtil.toAadhaarRequest(new Date().getTime()))
                            .requestType(aadhaarRequest.getHeader().getRequestType())
                            .subAuaCode(aadhaarRequest.getKycRequest().getAadharHolderDetails().getSubAuaCode())
                            .terminalId(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTerminalId())
                            .transactionIdentifier(aadhaarRequest.getKycRequest().getAadharHolderDetails().getTransactionIdentifier())
                            .version(aadhaarRequest.getKycRequest().getAadharHolderDetails().getVersion()).build()
                    ).build();
            /**
             * Call to Aadhar Otp Service
             */

            try {
                aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2(
                        aadhaarRequestNew, clientAadhaarRequest.getHeader()
                                .getInstitutionId());
            } catch (Exception e) {
                logger.error("Exception occured with message: {}", e.getMessage());
            }
            logger.debug("Response fetched->" + aadharMainResponse);

            AadharLog aadharLog = new AadharLog();
            aadharLog.setAadharClientRequest(clientAadhaarRequest);
            aadharLog.setAadhaarRequest(aadhaarRequest);
            aadharLog.setAadharMainResponse(aadharMainResponse);
            boolean status = false;
            status = loggerRepository.setAadharLogDeatails(aadharLog);
            if (status) {
                logger.info("aadhar log saved.");
            } else {
                logger.info("aadhar log not saved.");
            }


            Resp kycResponse = null;
            KycResp kycDecryptedResponse = null;
            AadharResponseReader parseResponse = new AadharResponseReader();

            if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
                DataDecryptor dataDecryptor = new DataDecryptor();
                kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
                if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                    if (kycResponse.getKycResponse().length() == 0) {
                        throw new Exception(
                                "KYC response xml retured a status of -1, no content found.");
                    }
                }
                String kycRes = kycResponse.getKycResponse();

                String dcrpxml = "";
                if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

                } else {
                    byte[] decode = Base64.decode(kycRes.getBytes());
                    dcrpxml = new String(decode);
                }

                if (dataDecryptor.verify(dcrpxml)) {
                    kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
                } else {
                    logger.error("KYC response xml signature cam failed.");
                }
                aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

                aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

                if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getTxnStatus())) {
                    if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())) {
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient
                                .setKycResponseDetails(aadharMainResponse
                                        .getKycResponse().getKycResponseDetails());
                    } else if (aadharMainResponse.getKycResponse() != null
                            && aadharMainResponse.getKycResponse()
                            .getKycResponseDetails() != null
                            && StringUtils.equalsIgnoreCase(Constant.ERROR,
                            aadharMainResponse.getKycResponse()
                                    .getKycResponseDetails()
                                    .getUidaiStatus())
                            && CollectionUtils.isNotEmpty(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors())) {

                        List<Errors> errorList = aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getErrors();
                        Errors error = errorList.get(0);
                        AadharError aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                        aadharResponseToClient
                                .setAcknowledgementId(aadharMainResponse
                                        .getAcknowledgementId());
                        aadharResponseToClient.setResult(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getKycResponse().getResult());
                        aadharResponseToClient.setError(aadharError);
                        aadharLog.setGNGstatus(Constant.FAILED);
                        aadharLog.setGNGStatusDtl(aadharError.toString());
                    }
                } else {

                    AadharError aadharError = null;
                    if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                        Errors error = aadharMainResponse.getErrors().get(0);
                        aadharError = new AadharError();
                        aadharError.setCode(error.getCode());
                        aadharError.setDescription(error.getDescription());
                        aadharResponseToClient.setTxnStatus(aadharMainResponse
                                .getTxnStatus());
                    }

                    if (aadharMainResponse.getKycResponse() != null) {
                        aadharResponseToClient.setUidaiStatus(aadharMainResponse
                                .getKycResponse().getKycResponseDetails()
                                .getUidaiStatus());
                    }
                    aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                            .getAcknowledgementId());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());

                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXP_AADHAAR_XML)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, errors);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);
    }

    //This method uses OLD AADHAR request with HSM.
    public BaseResponse verifyIrisByAadharAndGetEkycDeatilsV2_2_NEW(ClientAadhaarRequest clientAadhaarRequest) throws Exception {

        try {
            clientAadhaarRequest.getHeader().setDateTime(new Date());
        } catch (Exception exp) {
            logger.error("Exception Header Null : " + exp);
        }

        /**
         * build pid request json
         */
        AadhaarRequest aadhaarRequest = aadharEntityBuilder
                .buildIrisRequestV2_1(clientAadhaarRequest);
        /**
         * Call to Aadhar ekyc Service
         */
        AadharMainResponse aadharMainResponse = null;
        try {
            aadharMainResponse = serviceCaller.callAadharEkycServiceV2_2_NEW(
                    aadhaarRequest, clientAadhaarRequest.getHeader()
                            .getInstitutionId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        AadharResponseToClient aadharResponseToClient = new AadharResponseToClient();

        AadharLog aadharLog = new AadharLog();
        aadharLog.setAadharClientRequest(clientAadhaarRequest);
        aadharLog.setAadhaarRequest(aadhaarRequest);
        aadharLog.setAadharMainResponse(aadharMainResponse);
        boolean status = false;
        status = loggerRepository.setAadharLogDeatails(aadharLog);
        if (status) {
            logger.info("aadhar log saved.");
        } else {
            logger.info("aadhar log not saved.");
        }

        Resp kycResponse = null;
        KycResp kycDecryptedResponse = null;
        AadharResponseReader parseResponse = new AadharResponseReader();

        if (null != aadharMainResponse && null != aadharMainResponse.getResponseXMl()) {
            DataDecryptor dataDecryptor = new DataDecryptor();
            kycResponse = parseResponse.kycRespElementReader(aadharMainResponse.getResponseXMl());
            if (kycResponse.getStatus().equalsIgnoreCase("-1")) {
                if (kycResponse.getKycResponse().length() == 0) {
                    throw new Exception(
                            "KYC response xml retured a status of -1, no content found.");
                }
            }
            String kycRes = kycResponse.getKycResponse();

            String dcrpxml = "";
            if (kycResponse.getStatus().equalsIgnoreCase("0")) {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(dataDecryptor.decryptWithHSM_PS(decode));

            } else {
                byte[] decode = Base64.decode(kycRes.getBytes());
                dcrpxml = new String(decode);
            }

            if (dataDecryptor.verify(dcrpxml)) {
                kycDecryptedResponse = parseResponse.kycResponseReader(dcrpxml);
            } else {
                logger.error("KYC response xml signature cam failed.");
            }
            aadharMainResponse.setKycResponse(parseResponse.getKycResponseDetailsObject(kycDecryptedResponse));

            aadharResponseToClient.setRawXmlResponse(aadharMainResponse.getRawXmlResponse());

            if (StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                    aadharMainResponse.getTxnStatus())) {
                if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())) {
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient
                            .setKycResponseDetails(aadharMainResponse
                                    .getKycResponse().getKycResponseDetails());
                    aadharLog.setGNGstatus(Constant.SUCCESS);
                    aadharLog.setGNGStatusDtl(Constant.SUCCESS);
                } else if (aadharMainResponse.getKycResponse() != null
                        && aadharMainResponse.getKycResponse()
                        .getKycResponseDetails() != null
                        && StringUtils.equalsIgnoreCase(Constant.ERROR,
                        aadharMainResponse.getKycResponse()
                                .getKycResponseDetails()
                                .getUidaiStatus())
                        && CollectionUtils.isNotEmpty(aadharMainResponse
                        .getKycResponse().getKycResponseDetails()
                        .getErrors())) {

                    List<Errors> errorList = aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getErrors();
                    Errors error = errorList.get(0);
                    AadharError aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                    aadharResponseToClient
                            .setAcknowledgementId(aadharMainResponse
                                    .getAcknowledgementId());
                    aadharResponseToClient.setResult(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getKycResponse().getResult());
                    aadharResponseToClient.setError(aadharError);
                    aadharLog.setGNGstatus(Constant.FAILED);
                    aadharLog.setGNGStatusDtl(aadharError.toString());
                }
            } else {

                AadharError aadharError = null;
                if (CollectionUtils.isNotEmpty(aadharMainResponse.getErrors())) {
                    Errors error = aadharMainResponse.getErrors().get(0);
                    aadharError = new AadharError();
                    aadharError.setCode(error.getCode());
                    aadharError.setDescription(error.getDescription());
                    aadharResponseToClient.setTxnStatus(aadharMainResponse
                            .getTxnStatus());
                }

                if (aadharMainResponse.getKycResponse() != null) {
                    aadharResponseToClient.setUidaiStatus(aadharMainResponse
                            .getKycResponse().getKycResponseDetails()
                            .getUidaiStatus());
                }
                aadharResponseToClient.setAcknowledgementId(aadharMainResponse
                        .getAcknowledgementId());
                aadharResponseToClient.setError(aadharError);
                aadharLog.setGNGstatus(Constant.FAILED);
                aadharLog.setGNGStatusDtl(aadharError.toString());
            }
            loggerRepository.setAadharLogDeatails(aadharLog);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            aadharLog.setGNGstatus(Constant.FAILED);
            aadharLog.setGNGStatusDtl(errors.toString());
            loggerRepository.setAadharLogDeatails(aadharLog);

            return GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, aadharResponseToClient);

    }

    /**
     * Send SMS to Personal Phone
     *
     * @param applicationRequest
     * @param smsMessage
     * @return
     */
    public BaseResponse sendSMS(ApplicationRequest applicationRequest, String smsMessage) {
        logger.info("Inside Sending SMS");

        BaseResponse baseResponse = null;
        GenericMsgResponse genericMsgResponse = new GenericMsgResponse();

        String personalPhoneNum = GngUtils.getPhoneNumber(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue()
                , applicationRequest.getRequest().getApplicant().getPhone());

        BaseResponse smsResponse = null;
        if (personalPhoneNum != null) {
            SmsRequest smsRequest = sMSReqBuilder.buildSmsRequest(
                    applicationRequest.getHeader()
                    , personalPhoneNum
                    , smsMessage);
            smsResponse = sendSms(smsRequest);
            if (null != smsResponse
                    && smsResponse.getStatus().getStatusCode() == HttpStatus.OK.value()
                    && null != smsResponse.getPayload()) {
                //Read the SMS response
                SmsResponse smsOriginalResponse = (SmsResponse) smsResponse.getPayload().getT();
                if (smsOriginalResponse.isSuccess()) {
                    logger.info("SMS sent.");
                    genericMsgResponse.setStatus(Status.SUCCESS.name());
                    genericMsgResponse.setMessage("SMS sent.");
                } else {
                    logger.error("SMS failed:" + smsOriginalResponse.getErrors());
                    genericMsgResponse.setStatus(Status.FAILED.name());
                    genericMsgResponse.setMessage(ErrorCode.EXTERNAL_SERVICE_FAILURE);
                }
            } else {
                genericMsgResponse.setStatus(Status.FAILED.name());
                genericMsgResponse.setMessage(ErrorCode.EXTERNAL_SERVICE_FAILURE);
            }
        } else {
            genericMsgResponse.setStatus(Status.FAILED.name());
            genericMsgResponse.setMessage(ErrorCode.PERSONAL_MOBILE_UNAVAILABLE);
        }
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
        return baseResponse;
    }

    @Override
    public BaseResponse doESign(ESignRequest eSignRequest) throws Exception {

        logger.debug("Inside do Esign");

        BaseResponse baseResponse = null;

        ESignPayload eSignPayload = null;
        String fileType = "";
        int numberOfDocSend = 0;
        int numberOfDocSigned = 0;

        /**
         * Getting url configuration
         */
        WFJobCommDomain eSignConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                eSignRequest.getHeader().getInstitutionId(), UrlType.EMUDRA_ESIGN.toValue());
        ESignedLog eSignedLog = ESignedLog.builder()
                .requestJson(eSignRequest.toString())
                .requestDate(new Date())
                .build();

        ESignResponse eSignResponse = null;

        if (null != eSignConfig) {
            String eSignUrl = Arrays.asList(eSignConfig.getBaseUrl(), eSignConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

            /**
             * Prepare documents for e-signing
             */
            List<Document> eSignInputList = getESignInputList(eSignRequest.getHeader().getInstitutionId(),
                    eSignRequest.getRefId(),
                    eSignRequest.getLocation(),
                    eSignRequest.getDocNumberList());

            logger.debug("eSignInputList {} ", eSignInputList);
            if (eSignInputList != null && !eSignInputList.isEmpty()) {
                fileType = eSignInputList.get(0).getFileType();
            }
            numberOfDocSend = null != eSignInputList ? eSignInputList.size() : 0;

            if (!CollectionUtils.isEmpty(eSignInputList)) {

                /**
                 * create a request for e-sign api
                 */
                eSignPayload = ESignPayload.builder()
                        .eKycChannel(eSignRequest.getEkycChannel())
                        .institutionId(eSignRequest.getHeader().getInstitutionId())
                        .kycXml(eSignRequest.getKycResponseXml())
                        .uniqueTransactionID(StringUtils.join(eSignRequest.getRefId(), new Date().getTime()))
                        .documents(eSignInputList)
                        .build();

                logger.debug("esign_emudra request:  " + new Gson().toJson(eSignPayload));
                /**
                 * calling e-sign api
                 */
                eSignResponse = (ESignResponse) TransportUtils.postJsonRequest(eSignPayload, eSignUrl, ESignResponse.class);
                logger.debug("esign_emudra response:  " + new Gson().toJson(eSignResponse));

                /**
                 * doing process on response
                 */
                if (null != eSignResponse) {
                    if (null == eSignResponse.getError()) {
                        /**
                         * Persist documents in database
                         */
                        List<String> docIds = prepareDocToPersist(eSignResponse.getSignedDocuments(), eSignRequest.getRefId(), eSignInputList);
                        numberOfDocSigned = null != docIds ? docIds.size() : 0;
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, docIds);
                    } else {
                        Collection<Error> errors = new ArrayList<>();
                        errors.add(Error.builder()
                                .message(eSignResponse.getError().getMessage())
                                .errorCode(eSignResponse.getError().getType())
                                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                .level(Error.SEVERITY.CRITICAL.name())
                                .build());
                        baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_ERROR, errors);
                    }
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                }

            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.DOCUMENTS_NOT_FOUND_FOR_ESIGN)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
        }

        eSignedLog.setRefId(eSignRequest.getRefId());
        eSignedLog.setAadhaarNumber(eSignRequest.getAadhaarNumber());
        eSignedLog.setAuthMode(eSignRequest.getEkycChannel().name());
        eSignedLog.setResponseDate(new Date());
        eSignedLog.setInstitutionId(eSignRequest.getHeader().getInstitutionId());
        eSignedLog.setTransactionId(null != eSignPayload ? eSignPayload.getUniqueTransactionID() : null);
        eSignedLog.setResponseJson(JsonUtil.ObjectToString(eSignResponse));
        eSignedLog.setStatus(null != eSignResponse ? eSignResponse.getStatus() : ReqResHelperConstants.failure.name());
        eSignedLog.setNumberOfDocSend(numberOfDocSend);
        eSignedLog.setNumberOfDocSigned(numberOfDocSigned);
        eSignedLog.setConsentToESigned(eSignRequest.isConsentToESign());
        eSignedLog.setFileType(fileType);
        eSignedLog.setRequestType(RequestType.ESIGN.name());

        /**
         * Save log in db
         */
        loggerRepository.saveESignedLog(eSignedLog);

        return baseResponse;
    }


    @Override
    public BaseResponse doEMandate(ESignRequest eSignRequest) throws Exception {
        logger.debug("Inside do eMandate");
        BaseResponse baseResponse = null;

        ESignPayload eSignPayload = null;
        List<Document> eMandateInputList = new ArrayList<>();
        int numberOfDocSend = 0;
        int numberOfDocSigned = 0;
        String responseXml = null;
        String fileType = null;
        String requestXml = null;

        /**
         * Getting url configuration
         */
        WFJobCommDomain eSignConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                eSignRequest.getHeader().getInstitutionId(), UrlType.EMUDRA_ESIGN.toValue());
        ESignedLog eSignedLog = ESignedLog.builder()
                .requestJson(eSignRequest.toString())
                .requestDate(new Date())
                .build();

        ESignResponse eSignResponse = null;

        if (null != eSignConfig) {
            String eSignUrl = Arrays.asList(eSignConfig.getBaseUrl(), eSignConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));


            /**
             * Prepare xml for e-signing
             */

            XMLDocument xmlDocument = getXMlDocumentModel(eSignRequest.getHeader().getInstitutionId(),
                    eSignRequest.getRefId());

            if (null != xmlDocument && CollectionUtils.isEmpty(xmlDocument.getErrorMessage())) {
                eMandateInputList = getXMLESignInputList(xmlDocument, eSignRequest.getLocation());
                if (eMandateInputList != null && !eMandateInputList.isEmpty()) {
                    logger.debug("emandate xml created successfully");
                    fileType = eMandateInputList.get(0).getFileType();
                    requestXml = eMandateInputList.get(0).getFile();
                    numberOfDocSend = eMandateInputList.size();
                }


                if (!CollectionUtils.isEmpty(eMandateInputList)) {

                    /**
                     * create a request for e-sign api
                     */
                    eSignPayload = ESignPayload.builder()
                            .eKycChannel(eSignRequest.getEkycChannel())
                            .institutionId(eSignRequest.getHeader().getInstitutionId())
                            .kycXml(eSignRequest.getKycResponseXml())
                            .uniqueTransactionID(StringUtils.join(eSignRequest.getRefId(), new Date().getTime()))
                            .documents(eMandateInputList)
                            .build();
                    logger.debug("esign_emudra request:  " + new Gson().toJson(eSignPayload));

                    /**
                     * calling e-sign api
                     */
                    logger.debug(eSignPayload.toString());
                    eSignResponse = (ESignResponse) TransportUtils.postJsonRequest(eSignPayload, eSignUrl, ESignResponse.class);
                    logger.debug("esign_emudra response:  " + new Gson().toJson(eSignResponse));

                    /**
                     * doing process on response
                     */
                    if (null != eSignResponse) {
                        if (null == eSignResponse.getError() && !CollectionUtils.isEmpty(eSignResponse.getSignedDocuments())) {
                            /**
                             * Persist documents in database
                             */

                            responseXml = GngUtils.decodeBase64EncodedString(eSignResponse.getSignedDocuments().get(0).getFile());

                            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "eMandate XML successfully signed from eMudra");

                        } else {
                            Collection<Error> errors = new ArrayList<>();
                            errors.add(Error.builder()
                                    .message(null != eSignResponse.getError() ? eSignResponse.getError().getMessage() : "Response XML is null")
                                    .errorCode(null != eSignResponse.getError() ? eSignResponse.getError().getType() : "responseEmpty")
                                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                    .level(Error.SEVERITY.CRITICAL.name())
                                    .build());
                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_ERROR, errors);
                        }
                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                    }

                } else {
                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.DOCUMENTS_NOT_FOUND_FOR_ESIGN)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                if (xmlDocument == null || CollectionUtils.isEmpty(xmlDocument.getErrorMessage())) {
                    errors.add(Error.builder()
                            .message(ErrorCode.DETAILS_NOT_FOUND_REF_ID)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                } else {
                    for (String str : xmlDocument.getErrorMessage()) {
                        errors.add(Error.builder()
                                .message(str)
                                .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                                .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                                .level(Error.SEVERITY.CRITICAL.name())
                                .build());
                    }
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
        }

        eSignedLog.setRefId(eSignRequest.getRefId());
        eSignedLog.setAadhaarNumber(eSignRequest.getAadhaarNumber());
        eSignedLog.setAuthMode(eSignRequest.getEkycChannel().name());
        eSignedLog.setResponseDate(new Date());
        eSignedLog.setInstitutionId(eSignRequest.getHeader().getInstitutionId());
        eSignedLog.setTransactionId(null != eSignPayload ? eSignPayload.getUniqueTransactionID() : null);
        eSignedLog.setResponseJson(JsonUtil.ObjectToString(eSignResponse));
        eSignedLog.setStatus(null != eSignResponse ? eSignResponse.getStatus() : ReqResHelperConstants.failure.name());
        eSignedLog.setNumberOfDocSend(numberOfDocSend);
        eSignedLog.setNumberOfDocSigned(numberOfDocSigned);
        eSignedLog.setConsentToESigned(eSignRequest.isConsentToESign());
        eSignedLog.setFileType(fileType);
        eSignedLog.setRequestXml(requestXml);
        eSignedLog.setResponseXml(responseXml);
        eSignedLog.setRequestType(RequestType.ENACH.name());
        loggerRepository.saveESignedLog(eSignedLog);

        if (responseXml != null) {
            asyncJob.sftpUploadAsync(eSignRequest, responseXml);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse doESignAndEMandate(ESignRequest eSignRequest) throws Exception {
        logger.debug("Inside do eSign and eMandate");
        BaseResponse baseResponse = null;

        ESignPayload eSignPayload = null;
        Document eMandateInputXML = new Document();
        int numberOfDocSend = 0;
        int numberOfDocSigned = 0;
        String responseXml = null;
        String fileType = null;
        String requestXml = null;

        /**
         * Getting url configuration
         */
        WFJobCommDomain eSignConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                eSignRequest.getHeader().getInstitutionId(), UrlType.EMUDRA_ESIGN.toValue());

        ESignedLog eSignedLog = ESignedLog.builder()
                .requestJson(eSignRequest.toString())
                .requestDate(new Date())
                .build();

        ESignResponse eSignResponse = null;

        if (null != eSignConfig) {
            String eSignUrl = Arrays.asList(eSignConfig.getBaseUrl(), eSignConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

            /**
             * Prepare documents for e-signing
             */
            List<Document> eSignInputList = getESignInputList(eSignRequest.getHeader().getInstitutionId(),
                    eSignRequest.getRefId(),
                    eSignRequest.getLocation(),
                    eSignRequest.getDocNumberList());

            XMLDocument xmlDocument = getXMlDocumentModel(eSignRequest.getHeader().getInstitutionId(),
                    eSignRequest.getRefId());

            if (null != xmlDocument && CollectionUtils.isEmpty(xmlDocument.getErrorMessage())) {
                eMandateInputXML = getXMLESignInputList(xmlDocument, eSignRequest.getLocation()).get(0);
                if (eMandateInputXML != null) {
                    if (eSignInputList == null) {
                        eSignInputList = new ArrayList<>();
                    }
                    eSignInputList.add(eMandateInputXML);
                    logger.debug("emandate xml created successfully");
                    /*fileType = eMandateInputList.get(0).getFileType();*/
                    requestXml = eMandateInputXML.getFile();
                }
                /*if (eSignInputList != null && !eSignInputList.isEmpty()) {
                    fileType = eSignInputList.get(0).getFileType();
                }*/
                numberOfDocSend = null != eSignInputList ? eSignInputList.size() : 0;

                if (!CollectionUtils.isEmpty(eSignInputList)) {

                    /**
                     * create a request for e-sign api
                     */
                    eSignPayload = ESignPayload.builder()
                            .eKycChannel(eSignRequest.getEkycChannel())
                            .institutionId(eSignRequest.getHeader().getInstitutionId())
                            .kycXml(eSignRequest.getKycResponseXml())
                            .uniqueTransactionID(StringUtils.join(eSignRequest.getRefId(), new Date().getTime()))
                            .documents(eSignInputList)
                            .build();
                    logger.debug("esign_emudra request:  " + new Gson().toJson(eSignPayload));
                    /**
                     * calling e-sign api
                     */
                    eSignResponse = (ESignResponse) TransportUtils.postJsonRequest(eSignPayload, eSignUrl, ESignResponse.class);
                    logger.debug("esign_emudra response:  " + new Gson().toJson(eSignPayload));

                    /**
                     * doing process on response
                     */
                    if (null != eSignResponse) {
                        if (null == eSignResponse.getError()) {
                            /**
                             * Persist documents in database
                             */
                            List<String> docIds = prepareDocToPersist(eSignResponse.getSignedDocuments().stream().filter(p -> GNGWorkflowConstant.PDF.name().equalsIgnoreCase(p.getFileType())).collect(Collectors.toList()), eSignRequest.getRefId(), eSignInputList);
                            numberOfDocSigned = null != docIds ? docIds.size() : 0;
                            SignedDocument xmlSignedDoc = eSignResponse.getSignedDocuments().stream().filter(p -> GNGWorkflowConstant.NPCIXML.name().equalsIgnoreCase(p.getFileType())).findFirst().get();
                            if (xmlSignedDoc != null && xmlSignedDoc.getFile() != null) {
                                responseXml = GngUtils.decodeBase64EncodedString(xmlSignedDoc.getFile());
                                numberOfDocSigned++;
                            }
                            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, docIds);
                        } else {
                            Collection<Error> errors = new ArrayList<>();
                            errors.add(Error.builder()
                                    .message(eSignResponse.getError().getMessage())
                                    .errorCode(eSignResponse.getError().getType())
                                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                    .level(Error.SEVERITY.CRITICAL.name())
                                    .build());
                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_ERROR, errors);
                        }
                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                    }

                } else {
                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.DOCUMENTS_NOT_FOUND_FOR_ESIGN)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            } else {
                Collection<Error> errors = new ArrayList<>();
                if (xmlDocument == null || CollectionUtils.isEmpty(xmlDocument.getErrorMessage())) {
                    errors.add(Error.builder()
                            .message(ErrorCode.DETAILS_NOT_FOUND_REF_ID)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                } else {
                    for (String str : xmlDocument.getErrorMessage()) {
                        errors.add(Error.builder()
                                .message(str)
                                .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                                .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                                .level(Error.SEVERITY.CRITICAL.name())
                                .build());
                    }
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
        }


        eSignedLog.setRefId(eSignRequest.getRefId());
        eSignedLog.setAadhaarNumber(eSignRequest.getAadhaarNumber());
        eSignedLog.setAuthMode(eSignRequest.getEkycChannel().name());
        eSignedLog.setResponseDate(new Date());
        eSignedLog.setInstitutionId(eSignRequest.getHeader().getInstitutionId());
        eSignedLog.setTransactionId(null != eSignPayload ? eSignPayload.getUniqueTransactionID() : null);
        eSignedLog.setResponseJson(JsonUtil.ObjectToString(eSignResponse));
        eSignedLog.setStatus(null != eSignResponse ? eSignResponse.getStatus() : ReqResHelperConstants.failure.name());
        eSignedLog.setNumberOfDocSend(numberOfDocSend);
        eSignedLog.setNumberOfDocSigned(numberOfDocSigned);
        eSignedLog.setConsentToESigned(eSignRequest.isConsentToESign());
        eSignedLog.setFileType(fileType);
        eSignedLog.setRequestXml(requestXml);
        eSignedLog.setResponseXml(responseXml);
        /**
         * Save log in db
         */
        loggerRepository.saveESignedLog(eSignedLog);

        if (responseXml != null) {
            asyncJob.sftpUploadAsync(eSignRequest, responseXml);
        }


        return baseResponse;
    }

    private List<String> prepareDocToPersist(List<SignedDocument> signedDocuments, String refId, List<Document> eSignInputList) {
        List<String> docIds = new ArrayList<>();
        int index = 0;
        for (SignedDocument signedDocument : signedDocuments) {

            byte[] fileByte = org.apache.commons.codec.binary.Base64.decodeBase64(signedDocument.getFile());

            Object metaData = eSignInputList.get(index).getMetaData();

            DBObject modifiedMetadata = getMetadataModifiedObject((DBObject) metaData);

            try (InputStream inputStream = new ByteArrayInputStream(fileByte)) {

                String id = uploadFileRepository.store(inputStream,
                        signedDocument.getFileName(),
                        MediaType.APPLICATION_PDF_VALUE,
                        modifiedMetadata);

                docIds.add(id);

            } catch (IOException e) {
                logger.error("Error occurred while inserting document of {} ref Id in database with probable cause:{}", refId, e.getMessage());
            }
            index++;
        }
        return docIds;
    }


    private DBObject getMetadataModifiedObject(DBObject metaData) {
        DBObject uploadFileDetails = new BasicDBObject();
        if (null != metaData) {
            uploadFileDetails = (DBObject) metaData.get("uploadFileDetails");
            if (null == uploadFileDetails) {
                uploadFileDetails = new BasicDBObject();
            }
            uploadFileDetails.put("eSigned", true);
            metaData.put("uploadFileDetails", uploadFileDetails);
            return metaData;
        } else {
            uploadFileDetails.put("eSigned", true);
            return uploadFileDetails;
        }
    }

    private List<Document> getESignInputList(String institutionId, String refId, String location, Set<String> docNumberList) throws IOException {

        logger.debug("getting eSign Input List having institutionId {} , refId {} , location {} , docNumberList {} ", institutionId, refId, location, docNumberList);

        List<Document> eSignInputList = new ArrayList<>();
        for (String docId : docNumberList) {
            GridFSDBFile document = uploadFileRepository.getDocument(docId, institutionId, refId);
            if (null != document) {
                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(IOUtils.toByteArray(document.getInputStream()));
                Document esInputsPDF = new Document();
                esInputsPDF.setFile(new String(encodeBase64));
                esInputsPDF.setFileName(document.getFilename());
                esInputsPDF.setLocation(location);
                esInputsPDF.setMetaData(document.getMetaData());
                esInputsPDF.setFileType(document.getContentType());
                if (StringUtils.equals(MediaType.APPLICATION_PDF_VALUE, document.getContentType())) {
                    eSignInputList.add(esInputsPDF);
                }
            }
        }
        return eSignInputList;
    }


    private List<Document> getXMLESignInputList(XMLDocument xmlDocument, String location) throws IOException {
        logger.debug("Inside get XML eSignInputList");
        List<Document> eSignInputList = new ArrayList<>();
        StringBuilder xmlDocStr = new StringBuilder();
        xmlDocStr.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xmlDocStr.append("<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.009.001.04\">");
        xmlDocStr.append("<MndtInitnReq><GrpHdr>");
        xmlDocStr.append("<MsgId>" + xmlDocument.getMsgId() + "</MsgId>");
        xmlDocStr.append("<CreDtTm>" + xmlDocument.getCreDtTme() + "</CreDtTm>");
        xmlDocStr.append("<InstgAgt><FinInstnId>");
        xmlDocStr.append("<ClrSysMmbId><MmbId>" + xmlDocument.getInstructingAgentMmbId() + "</MmbId></ClrSysMmbId>");
        if (!"".equalsIgnoreCase(xmlDocument.getInstructingBankName())) {
            xmlDocStr.append("<Nm>" + xmlDocument.getInstructingBankName() + "</Nm>");
        }
        xmlDocStr.append("</FinInstnId></InstgAgt>");
        xmlDocStr.append("<InstdAgt><FinInstnId>");
        xmlDocStr.append("<ClrSysMmbId><MmbId>" + xmlDocument.getInstructedAgentMmbId() + "</MmbId></ClrSysMmbId>");
        if (!"".equalsIgnoreCase(xmlDocument.getInstructedBankName())) {
            xmlDocStr.append("<Nm>" + xmlDocument.getInstructedBankName() + "</Nm>");
        }
        xmlDocStr.append("</FinInstnId></InstdAgt>");
        xmlDocStr.append("</GrpHdr>");
        xmlDocStr.append("<Mndt>");
        xmlDocStr.append("<MndtReqId>" + xmlDocument.getMndtReqId() + "</MndtReqId>");
        xmlDocStr.append("<Tp><SvcLvl><Prtry>" + xmlDocument.getMndtSvcLvlPrtry() + "</Prtry></SvcLvl>");
        xmlDocStr.append("<LclInstrm><Prtry>" + xmlDocument.getMndtLclInstrmPrtry() + "</Prtry></LclInstrm></Tp>");
        xmlDocStr.append("<Ocrncs><SeqTp>" + xmlDocument.getMndtSeqTp() + "</SeqTp>");
        xmlDocStr.append("<Frqcy><Tp>" + xmlDocument.getMdthFreqTp() + "</Tp></Frqcy>");
        if (!"".equalsIgnoreCase(xmlDocument.getMndtFirstColltnDt())) {
            xmlDocStr.append("<FrstColltnDt>" + xmlDocument.getMndtFirstColltnDt() + "</FrstColltnDt>");
        }
        if (!"".equalsIgnoreCase(xmlDocument.getMndtFinalColltnDt())) {
            xmlDocStr.append("<FnlColltnDt>" + xmlDocument.getMndtFinalColltnDt() + "</FnlColltnDt>");
        }
        xmlDocStr.append("</Ocrncs>");
        xmlDocStr.append("<MaxAmt Ccy=\"INR\">" + xmlDocument.getMaxAmt() + "</MaxAmt>");
        xmlDocStr.append("<Cdtr><Nm>" + xmlDocument.getCdtrName() + "</Nm></Cdtr>");
        xmlDocStr.append("<CdtrAcct><Id><Othr><Id>" + xmlDocument.getCdtrAccId() + "</Id></Othr></Id>");
        xmlDocStr.append("</CdtrAcct>");
        xmlDocStr.append("<CdtrAgt><FinInstnId><ClrSysMmbId>");
        xmlDocStr.append("<MmbId>" + xmlDocument.getCdtrAgntMemId() + "</MmbId>");
        xmlDocStr.append("</ClrSysMmbId><Nm>" + xmlDocument.getCdtrAgntMemBankName() + "</Nm>");
        xmlDocStr.append("</FinInstnId></CdtrAgt>");
        xmlDocStr.append("<Dbtr><Nm>" + xmlDocument.getDbtrName() + "</Nm><Id><PrvtId><Othr>");
        if (!"".equalsIgnoreCase(xmlDocument.getDbtrId())) {
            xmlDocStr.append("<Id>" + xmlDocument.getDbtrId() + "</Id>");
        }
        if (!"".equalsIgnoreCase(xmlDocument.getDbtrPrtry())) {
            xmlDocStr.append("<SchmeNm><Prtry>" + xmlDocument.getDbtrPrtry() + "</Prtry></SchmeNm>");
        }
        xmlDocStr.append("</Othr></PrvtId></Id>");
        xmlDocStr.append("<CtctDtls><Nm>" + xmlDocument.getAadhaarNo() + "</Nm>");
        if (!"".equalsIgnoreCase(xmlDocument.getPhNo())) {
            xmlDocStr.append("<PhneNb>" + xmlDocument.getPhNo() + "</PhneNb>");
        }
        if (!"".equalsIgnoreCase(xmlDocument.getMblNo())) {
            xmlDocStr.append("<MobNb>" + xmlDocument.getMblNo() + "</MobNb>");
        }
        if (!"".equalsIgnoreCase(xmlDocument.getEmail())) {
            xmlDocStr.append("<EmailAdr>" + xmlDocument.getEmail() + "</EmailAdr>");
        }
        if (!"".equalsIgnoreCase(xmlDocument.getPan())) {
            xmlDocStr.append("<Othr>" + xmlDocument.getPan() + "</Othr>");
        }
        xmlDocStr.append("</CtctDtls>");
        xmlDocStr.append("</Dbtr>");
        xmlDocStr.append("<DbtrAcct>");
        xmlDocStr.append("<Id>");
        xmlDocStr.append("<Othr>");
        xmlDocStr.append("<Id>" + xmlDocument.getDbtrAccntId() + "</Id>");
        xmlDocStr.append("</Othr>");
        xmlDocStr.append("</Id>");
        xmlDocStr.append("<Tp>");
        xmlDocStr.append("<Prtry>" + xmlDocument.getDbtrAccntPrtry() + "</Prtry>");
        xmlDocStr.append("</Tp>");
        xmlDocStr.append("</DbtrAcct>");
        xmlDocStr.append("<DbtrAgt>");
        xmlDocStr.append("<FinInstnId>");
        xmlDocStr.append("<ClrSysMmbId>");
        xmlDocStr.append("<MmbId>" + xmlDocument.getDbtrAccntMemId() + "</MmbId>");
        xmlDocStr.append("</ClrSysMmbId>");
        if (!"".equalsIgnoreCase(xmlDocument.getDbtrAccntMemName())) {
            xmlDocStr.append("<Nm>" + xmlDocument.getDbtrAccntMemName() + "</Nm>");
        }
        xmlDocStr.append("</FinInstnId>");
        xmlDocStr.append("</DbtrAgt>");
        xmlDocStr.append("</Mndt>");
        xmlDocStr.append("</MndtInitnReq>");
        xmlDocStr.append("</Document>");
        Document esInputsPDF = new Document();
        esInputsPDF.setFile(xmlDocStr.toString());
        esInputsPDF.setFileName(xmlDocument.getMsgId() + ".xml");
        esInputsPDF.setLocation(location);
        esInputsPDF.setFileType(MediaType.APPLICATION_XML_VALUE);
        eSignInputList.add(esInputsPDF);
        return eSignInputList;
    }


    private XMLDocument getXMlDocumentModel(String institutionId, String refId) throws Exception {
        logger.debug("Inside getXMlDocumentModel - ENACH initiated");
        PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(refId, institutionId);

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);

        EmandateCredentials emandateCredentials = urlConfiguration.getEmandateCredentialsConfig().get(institutionId);

        ApplicationRequest applicationRequest = null;//goNoGoCustomerApplication.getApplicationRequest();
        Applicant applicant = null;
        Name applicantName = null;
        com.softcell.gonogo.model.core.Application application = null;

        String aadhaarNumber = null;
        String panNumber = null;
        String passportNumber = null;
        String voterId = null;
        String drivingLicense = null;
        String otherId = null;
        String panName = null;

        long oPhoneNumber = 0;
        int oAreaCode = 0;
        String oCountryCode;
        String oExtension = null;
        String countryCode;
        long pPhoneNumber = 0;
        long pAreaCode = 0;
        String pCountryCode = null;
        String pExtension;
        long rPhoneNumber = 0;
        int rAreaCode = 0;
        String rCountryCode = null;
        String rExtension = null;
        int rZipCode = 0;
        String pEmailAddress = null;
        String oEmailAddress = null;
        String rEmailAddress = null;

        String accountNo = null;
        String accountType = null;
        String bankName = null;
        String ifsc = null;
        PostIPA postIPA = null;
        double revisedEmi = 0;
        String loanStartDate = null;
        String loanEndDate = null;
        String gngRefId = null;
        String fullName = null;
        String personalPhone = "";
        String residentialPhone = "";
        List<String> errorMsg = new ArrayList<>();

        if (null != postIpaRequest) {
            postIPA = postIpaRequest.getPostIPA();
            if (null != postIPA) {
                if (postIPA.getRevisedEmi() > 0.0) {
                    revisedEmi = postIPA.getRevisedEmi();
                } else if (postIPA.getEmi() > 0.0) {
                    revisedEmi = postIPA.getEmi();
                } else {
                    errorMsg.add("EMI amount for this reference id not found");
                }

                if (postIPA.getEmiStartDate() != null)
                    loanStartDate = GngDateUtil.getYYYYMMDDFormat(postIPA.getEmiStartDate());
                if (postIPA.getEmiEndDate() != null)
                    loanEndDate = GngDateUtil.getYYYYMMDDFormat(postIPA.getEmiEndDate());
            } else {
                errorMsg.add("PostIpa of PostIpaRequest for this reference id not found");
            }
        } else {
            errorMsg.add("PostIpaRequest for this reference id not found");
        }
        if (null != goNoGoCustomerApplication) {
            applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            gngRefId = goNoGoCustomerApplication.getGngRefId();
            if (null != applicationRequest) {
            /*header = applicationRequest.getHeader();*/
                if (null != applicationRequest.getRequest()) {
                    applicant = applicationRequest.getRequest().getApplicant();
                    application = applicationRequest.getRequest().getApplication();
                    if (null != applicant) {
                        applicantName = applicant.getApplicantName();
                        if (!CollectionUtils.isEmpty(applicant.getPhone())) {
                            for (Phone phone : applicant.getPhone()) {
                                switch (phone.getPhoneType()) {
                                    case PhoneJsonKeys.PERSONAL_MOBILE_TYPE: {
                                        try {
                                            pPhoneNumber = Long.parseLong(phone.getPhoneNumber());
                                        } catch (Exception e) {
                                            //ignore
                                        }
                                        try {
                                            pAreaCode = Long.parseLong(phone.getAreaCode());
                                        } catch (Exception e) {
                                            //ignore
                                        }
                                        pCountryCode = phone.getCountryCode();
                                        pExtension = phone.getExtension();
                                    }
                                    break;
                                    case PhoneJsonKeys.RESIDENCE_PHONE: {
                                        try {
                                            rPhoneNumber = Long.parseLong(phone.getPhoneNumber());
                                        } catch (Exception e) {
                                            //ignore
                                        }
                                        if (rAreaCode == 0) {
                                            try {
                                                rAreaCode = Integer.parseInt(phone.getAreaCode());
                                            } catch (Exception e) {
                                                //ignore
                                            }
                                        }

                                        rCountryCode = phone.getCountryCode();
                                        rExtension = phone.getExtension();
                                    }
                                    break;
                                }
                            }
                        }

                        if (!CollectionUtils.isEmpty(applicant.getEmail())) {
                            List<Email> emails = applicant.getEmail();
                            for (Email email : emails) {
                                switch (email.getEmailType()) {
                                    case "PERSONAL": {
                                        pEmailAddress = email.getEmailAddress();
                                    }
                                    break;
                                    case "WORK": {
                                        oEmailAddress = email.getEmailAddress();
                                    }
                                    break;
                                    case "PERMANENT": {
                                        rEmailAddress = email.getEmailAddress();
                                    }
                                    break;
                                }
                            }
                        }

                        if (!CollectionUtils.isEmpty(applicant.getKyc())) {
                            List<Kyc> kycs = applicant.getKyc();
                            for (Kyc kyc : kycs) {
                                switch (kyc.getKycName()) {
                                    case "AADHAAR": {
                                        if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                                            aadhaarNumber = kyc.getKycNumber();
                                        }
                                    }
                                    break;
                                    case "PAN": {
                                        panNumber = kyc.getKycNumber();
                                    }
                                    break;
                                }
                            }
                        }
                        if (aadhaarNumber == null || "".equalsIgnoreCase(aadhaarNumber)) {
                            errorMsg.add("Aadhaar for this reference id not found");
                        }
                        if (!CollectionUtils.isEmpty(applicant.getBankingDetails())) {
                            BankingDetails bankingDetails = applicant.getBankingDetails().get(0);
                            if (null != bankingDetails) {
                                accountNo = bankingDetails.getAccountNumber();
                                if (null == accountNo) {
                                    errorMsg.add("Account No for this reference id not found");
                                }
                                if (null != bankingDetails.getAccountType()) {
                                    if (AccountType.SAVINGS.name().equalsIgnoreCase(bankingDetails.getAccountType()) || AccountType.CURRENT.name().equalsIgnoreCase(bankingDetails.getAccountType())) {
                                        accountType = bankingDetails.getAccountType().toUpperCase();
                                    } else {
                                        accountType = AccountType.OTHERS.name();
                                    }
                                } else {
                                    errorMsg.add("Account Type for this reference id not found");
                                }
                                bankName = bankingDetails.getBankName();
                                if (bankName != null && emandateCredentials != null) {
                                    if ((!emandateCredentials.getAllowedBanks().contains(bankName))
                                            || ((bankingDetails.getRepaymentType().equals(GNGWorkflowConstant.REPAYMENT_TYPE_ADM.name()))
                                            || (bankingDetails.getRepaymentType().equals(GNGWorkflowConstant.REPAYMENT_TYPE_ADM.toFaceValue()))
                                            || (bankingDetails.getRepaymentType().equals(GNGWorkflowConstant.REPAYMENT_TYPE_ECS.toFaceValue()))
                                            || (bankingDetails.getRepaymentType().equals(GNGWorkflowConstant.REPAYMENT_TYPE_ECS.name())))) {
                                        errorMsg.add(ErrorCode.BANK_REPAYMENT_TYPE_NOT_ELIGIBLE);
                                        return XMLDocument.builder()
                                                .errorMessage(errorMsg)
                                                .build();
                                    }
                                }
                                if (bankName != null && bankName.length() > 50) {
                                    bankName = bankName.substring(0, 49);
                                }
                                ifsc = bankingDetails.getIfscCode();
                                if (null == ifsc) {
                                    errorMsg.add("IFSC for this reference id not found");
                                }
                            } else {
                                errorMsg.add("Banking Details for this reference id not found");
                            }
                        } else {
                            errorMsg.add("Banking Details for this reference id not found");
                        }

                        if (null != applicantName.getMiddleName() && !"".equalsIgnoreCase(applicantName.getMiddleName())) {
                            fullName = applicantName.getFirstName() + " " + applicantName.getMiddleName() + " " + applicantName.getLastName();
                            if (fullName != null && fullName.length() > 40) {
                                fullName = applicantName.getFirstName() + " " + applicantName.getLastName();
                            }
                        } else {
                            fullName = applicantName.getFirstName() + " " + applicantName.getLastName();
                        }
                        if (fullName != null && fullName.length() > 40) {
                            fullName = fullName.substring(0, 39);
                        }
                        if (pPhoneNumber != 0l) {
                            personalPhone = pCountryCode + "-" + pPhoneNumber;
                        }

                        if (rPhoneNumber != 0l) {
                            residentialPhone = rCountryCode + "-" + rAreaCode + "-" + pPhoneNumber;
                        }

                    } else {
                        errorMsg.add("Applicant Details for this reference id not found");
                    }
                } else {
                    errorMsg.add("Application Request for this reference id not found");

                }
            } else {
                errorMsg.add("Application Request for this reference id not found");
            }
        } else {
            errorMsg.add("GoNoGo Customer details for this reference id not found");

        }
        String dateTime = GngDateUtil.toAadharTimeStamp(new Date());
        String msgId = StringUtils.join(gngRefId, (int) (new Date().getTime() / 1000));
        if (emandateCredentials == null) {
            errorMsg.add("Emandate Config Not Found");
        }
        if (!CollectionUtils.isEmpty(errorMsg)) {
            return XMLDocument.builder()
                    .errorMessage(errorMsg)
                    .build();
        }
        return XMLDocument.builder()
                .initiatingPartyId(emandateCredentials.getInitiatingPartyId())
                .cdtrAccId(emandateCredentials.getCdtrAccId())
                .cdtrAgntMemBankName(emandateCredentials.getCdtrAgntMemBankName())
                .cdtrAgntMemId(emandateCredentials.getCdtrAgntMemId())
                .cdtrName(emandateCredentials.getCdtrName())
                .instructingAgentMmbId(emandateCredentials.getInstructingAgentMmbId())
                .instructingBankName(emandateCredentials.getInstructingBankName())
                .mdthFreqTp(emandateCredentials.getMdthFreqTp())
                .mndtSeqTp(emandateCredentials.getMndtSeqTp())
                .mndtLclInstrmPrtry(emandateCredentials.getMndtLclInstrmPrtry())
                .mndtSvcLvlPrtry(emandateCredentials.getMndtSvcLvlPrtry())
                .aadhaarNo(null != aadhaarNumber ? aadhaarNumber : "")
                .pan(null != panNumber ? panNumber : "")
                .dbtrAccntId(null != accountNo ? accountNo : "")
                .dbtrPrtry("")
                .dbtrAccntMemId(null != ifsc ? ifsc : "")
                .dbtrAccntPrtry(null != accountType ? accountType : "")
                .dbtrAccntMemName(null != bankName ? bankName : "")
                .dbtrId(null != gngRefId ? gngRefId : "")
                .dbtrName(null != fullName ? fullName : "")
                .email(null != rEmailAddress ? rEmailAddress : null != pEmailAddress ? pEmailAddress : null != oEmailAddress ? oEmailAddress : "")
                .instructedAgentMmbId(null != ifsc ? ifsc : "")
                .instructedBankName(null != bankName ? bankName : "")
                .mblNo(null != personalPhone ? personalPhone : "")
                .maxAmt(String.valueOf(revisedEmi))
                .mndtFinalColltnDt(null != loanEndDate ? loanEndDate : "")
                .mndtFirstColltnDt(null != loanStartDate ? loanStartDate : "")
                .mndtReqId(null != msgId ? msgId : "")
                .msgId(null != msgId ? msgId : "")
                .creDtTme(null != dateTime ? dateTime : "")
                .phNo(null != residentialPhone ? residentialPhone : "")
                .build();
    }


    @Override
    public BaseResponse getOtpByAadharServiceVersionDecider(ClientAadhaarRequest aadharOtpRequest) throws Exception {

        logger.info("inside getOtpByAadharServiceVersionDecide... ");
        logger.info("getting aadhar version configuration for {}", KycRequestType.GET_OTP.name());

        KycRequestVersion aadharVersionConfiguration = lookupService.getAadharVersionConfiguration(aadharOtpRequest.getHeader().getInstitutionId(),
                KycRequestType.GET_OTP.name());

        if (null != aadharVersionConfiguration) {
            logger.info("Aadhar version configuration found successfully for {} is {} ", KycRequestType.GET_OTP.name(), aadharVersionConfiguration.name());
        } else {
            logger.error("Aadhar version configuration not found for {} ", KycRequestType.GET_OTP.name());
        }
        if (KycRequestVersion.HSM == aadharVersionConfiguration) {
            //call hsm service
            return getOtpByAadharV2_2(aadharOtpRequest);
        } else {
            //call non hsm service
            return getOtpByAadharV2_1(aadharOtpRequest);
        }
    }

    @Override
    public BaseResponse verifyOtpByAadharServiceVersionDecider(ClientAadhaarRequest clientAadhaarRequest) throws Exception {


        logger.info("inside getOtpByAadharServiceVersionDecide... ");
        logger.info("getting aadhar version configuration for {}", KycRequestType.VERIFY_OTP.name());

        KycRequestVersion aadharVersionConfiguration = lookupService.getAadharVersionConfiguration(clientAadhaarRequest.getHeader().getInstitutionId(),
                KycRequestType.VERIFY_OTP.name());

        if (null != aadharVersionConfiguration) {
            logger.info("Aadhar version configuration found successfully for {} is {} ", KycRequestType.VERIFY_OTP.name(), aadharVersionConfiguration.name());
        } else {
            logger.error("Aadhar version configuration not found for {} ", KycRequestType.GET_OTP.name());
        }

        if (KycRequestVersion.HSM == aadharVersionConfiguration) {
            //call hsm service
            return verifyOtpByAadharAndGetEkycDeatilsV2_2_NEW(clientAadhaarRequest);
        } else {
            //call non hsm service
            return verifyOtpByAadharAndGetEkycDeatilsV2_1(clientAadhaarRequest);
        }
    }

    @Override
    public BaseResponse verifyOtpByAadharServiceVersionDeciderV2_1_1(ClientAadhaarRequest clientAadhaarRequest) throws Exception {

        logger.info("inside getOtpByAadharServiceVersionDecide... ");
        logger.info("getting aadhar version configuration for {}", KycRequestType.VERIFY_OTP.name());

        KycRequestVersion aadharVersionConfiguration = lookupService.getAadharVersionConfiguration(clientAadhaarRequest.getHeader().getInstitutionId(),
                KycRequestType.VERIFY_OTP.name());

        if (null != aadharVersionConfiguration) {
            logger.info("Aadhar version configuration found successfully for {} is {} ", KycRequestType.VERIFY_OTP.name(), aadharVersionConfiguration.name());
        } else {
            logger.error("Aadhar version configuration not found for {} ", KycRequestType.GET_OTP.name());
        }
        if (KycRequestVersion.HSM == aadharVersionConfiguration) {
            //call hsm service
            return verifyOtpByAadharAndGetEkycDeatilsV2_2_NEW(clientAadhaarRequest);
        } else {
            //call non hsm service
            return verifyOtpByAadharAndGetEkycDeatilsV2_1_1(clientAadhaarRequest);
        }
    }

    @Override
    public BaseResponse verifyIrisByAadharServiceVersionDecider(ClientAadhaarRequest clientAadhaarRequest) throws Exception {

        logger.info("inside verifyIrisByAadharServiceVersionDecider... ");
        logger.info("getting aadhar version configuration for {}", KycRequestType.VERIFY_IRIS.name());

        KycRequestVersion aadharVersionConfiguration = lookupService.getAadharVersionConfiguration(clientAadhaarRequest.getHeader().getInstitutionId(),
                KycRequestType.VERIFY_IRIS.name());

        if (null != aadharVersionConfiguration) {
            logger.info("Aadhar version configuration found successfully for {} is {} ", KycRequestType.VERIFY_IRIS.name(), aadharVersionConfiguration.name());
        } else {
            logger.error("Aadhar version configuration not found for {} ", KycRequestType.VERIFY_IRIS.name());
        }

        if (KycRequestVersion.HSM == aadharVersionConfiguration) {
            //call hsm service
            return verifyIrisByAadharAndGetEkycDeatilsV2_2_NEW(clientAadhaarRequest);
        } else {
            //call non hsm service
            return verifyIrisByAadharAndGetEkycDeatilsV2_1(clientAadhaarRequest);

        }
    }

    @Override
    public BaseResponse verifyBioByAadharServiceVersionDecider(ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception {


        logger.info("inside verifyIrisByAadharServiceVersionDecider... ");
        logger.info("getting aadhar version configuration for {}", KycRequestType.VERIFY_BIO.name());

        KycRequestVersion aadharVersionConfiguration = lookupService.getAadharVersionConfiguration(aadharEkycwithBioRequest.getHeader().getInstitutionId(),
                KycRequestType.VERIFY_BIO.name());

        if (null != aadharVersionConfiguration) {
            logger.info("Aadhar version configuration found successfully for {} is {} ", KycRequestType.VERIFY_BIO.name(), aadharVersionConfiguration.name());
        } else {
            logger.error("Aadhar version configuration not found for {} ", KycRequestType.VERIFY_BIO.name());
        }

        if (KycRequestVersion.HSM == aadharVersionConfiguration) {

            // call hsm service
            return verifyBioByAadharAndGetEkycDetailsV2_2_NEW(aadharEkycwithBioRequest);
        } else {

            // call non hsm service
            return verifyBioByAadharAndGetEkycDetails(aadharEkycwithBioRequest);
        }
    }

    @Override
    public BaseResponse verifyOtp(SmsRequest smsRequest) {
        String result = applicationRepository.isValidOtp(smsRequest.getHeader().getInstitutionId(), smsRequest.getUuid(), smsRequest.getOtp());
        BaseResponse baseResponse = null;
        if(StringUtils.equalsIgnoreCase(result, Constant.VERIFIED)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, result);
        }else if(StringUtils.equalsIgnoreCase(result, ErrorCode.OTP_VERIFICATION_LIMIT_EXCEEDED)){
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(result)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            baseResponse =  GngUtils.getBaseResponse(HttpStatus.FORBIDDEN, errors);
        }
        else{
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(result)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            baseResponse =  GngUtils.getBaseResponse(HttpStatus.CONFLICT, errors);
        }
        return  baseResponse;
    }

}
