package com.softcell.service.impl;


import com.softcell.config.templates.TemplateName;
import com.softcell.constants.*;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.*;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.dao.mongodb.repository.multiproduct.MultiProductRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.*;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.CoApplicantResponse;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.core.perfios.BankData;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.HierarchyUser;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.request.dedupe.AdditionalDedupeFieldsRequest;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.response.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.queue.management.QueueEventHandler;
import com.softcell.gonogo.service.factory.SMSReqBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.*;
import com.softcell.utils.DMSUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.utils.MultiBureauUtility;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author yogeshb
 */
@Service
public class CroManagerImpl implements CroManager {

    private static final Logger logger = LoggerFactory.getLogger(CroManagerImpl.class);

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private MultiProductRepository multiProductRepository;

    @Autowired
    private MultibureauRepository multibureauRepository;

    @Autowired
    private SalesForceRepository salesForceRepository;

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private DMZConnector dmzConnector;

    @Autowired
    private AuditDataManager auditDataManager;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private DMSFeedManager dmsFeedManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private AdminLogRepository adminLogRepository;

    @Autowired
    private DigitizationStoreManager digitizationStoreManager;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    @Autowired
    private SMSReqBuilder sMSReqBuilder;

    @Autowired
    private QueueEventHandler queueManager;

    @Autowired
    private ModuleManager moduleManager;

    @Autowired
    private ModuleHelper moduleHelper;

    @Autowired
    private MasterDataViewRepository masterDataViewRepository;

    @Autowired
    private DataEntryManager dataEntryManager;

    @Autowired
    AppConfigurationHelper appConfigurationHelper;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Override
    public BaseResponse getApplicationData(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("getApplicationData service started");
        BaseResponse baseResponse;
        String refId = checkApplicationStatus.getGonogoRefId();
        String institutionId = checkApplicationStatus.getHeader().getInstitutionId();
        String userId = checkApplicationStatus.getHeader().getLoggedInUserId();
        String loggedInUserRole = checkApplicationStatus.getHeader().getLoggedInUserRole();
        String product = checkApplicationStatus.getHeader().getProduct().name();
        boolean isCredit = false;
        if(checkApplicationStatus.getHeader().getCredit() != null) {
            isCredit = checkApplicationStatus.getHeader().getCredit();
        }

        /**
         * TODO @Amit Role and stage based Editable check for FOS & CPA
         * On dashboard validate that same application must not accessed by multiple users
         * so set the application isEditable flag to false if in another users bucket.
         */

        if (Cache.INSTITUTION_QUEUE_CONFIG.get(institutionId) != null && Cache.INSTITUTION_QUEUE_CONFIG.get(institutionId)) {
            // Check whether the requested application case is still assigned to the user
            if (!queueManager.isAssignedRefId(refId, userId, institutionId)) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,
                        String.format("User %s is offline / RefId %s not assigned to user of institution %s",
                                userId, refId, institutionId));
                return baseResponse;
            }
        }

        GoNoGoCroApplicationResponse applicationData = applicationRepository
                .getApplicationByRefId(refId, CacheConstant.WEB.toString(), institutionId);

        if (!product.equals(applicationData.getApplicationRequest().getHeader().getProduct().name())){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,
                    String.format(" RefId %s is not of Product %s",
                            refId, product));
            return baseResponse;
        }
        AllocationInfo allocationInfo = applicationData.getAllocationInfo();

        boolean isEditable = moduleManager.checkApplicationIsEditable(refId, userId, loggedInUserRole, applicationData.getApplicationRequest().getCurrentStageId(),
                institutionId, allocationInfo, isCredit);

        if (applicationData != null) {

            logger.debug("getApplicationData service got success from application repository for id {} ", refId);

            // Set editiable mode for application data
            applicationData.setEditable(isEditable);

            Set<String> refSet = getAllRefId(applicationData, refId);

            // Check propertyValuation - there are images uploaded by third party
            if(StringUtils.equals(product, Product.LAP.name())) {
                checkPropertyValuation(refId, checkApplicationStatus.getHeader());
            }
            setApplicationPostIpaDetails(refId, refSet, checkApplicationStatus.getHeader().getInstitutionId(), applicationData);
            // Set hierarchyData to applicationData for CREDIT stage
            //setHierarchyData(applicationData, userId, checkApplicationStatus.getHeader().getInstitutionId(), isEditable);
            setHierarchyData(applicationData, userId, checkApplicationStatus.getHeader(), isEditable);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationData);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        logger.debug("getApplicationData service completed successfully ");

        return baseResponse;
    }

    @Override
    public BaseResponse getApplicationDataByMob(
            GetGngByMobRequest getGngByMobRequest) throws Exception {

        logger.debug("getApplicationData by mobile number service started");
        BaseResponse baseResponse;
        GoNoGoCustomerApplication applicationData = applicationRepository
                .fetchGNGDataByMob(getGngByMobRequest);



        if (applicationData != null) {

            logger.debug("getApplicationDataByMob service got success from application repository");

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationData);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        logger.debug("getApplicationDataByMob service completed successfully ");

        return baseResponse;
    }

    private void removeOldMBResponse(GoNoGoCroApplicationResponse applicationData) {
        Request request = applicationData.getApplicationRequest().getRequest();
        // For Primary applicant
        boolean isIndividual = GngUtils.isIndividual(request.getApplicant()) ;
        if( isIndividual ) {
            if( applicationData.getApplicantComponentResponse().getMbCorpJsonResponse() != null ) {
                applicationData.getApplicantComponentResponse().setMbCorpJsonResponse(null) ;
            }
        } else { // Non Individual
            if( applicationData.getApplicantComponentResponse().getMultiBureauJsonRespose() != null ) {
                applicationData.getApplicantComponentResponse().setMultiBureauJsonRespose(null) ;
            }
        }
        // For Co-applicants
        List<ComponentResponse> coAppComponentResponses = applicationData.getApplicantComponentResponseList();
        if( CollectionUtils.isNotEmpty(request.getCoApplicant()) && CollectionUtils.isNotEmpty(coAppComponentResponses)) {
            String applicantId ;
            ComponentResponse componentResponse;
            for( CoApplicant coApplicant : request.getCoApplicant()) {
                isIndividual = GngUtils.isIndividual(coApplicant) ;
                componentResponse = coAppComponentResponses
                                    .stream()
                                    .filter(compResponse -> StringUtils.equalsIgnoreCase(
                                                            compResponse.getApplicantId(), coApplicant.getApplicantId() ))
                                    .findFirst().orElse(null);
                if( componentResponse != null ){
                    if( isIndividual ) {
                        if( componentResponse.getMbCorpJsonResponse() != null ) {
                            componentResponse.setMbCorpJsonResponse(null) ;
                        }
                    } else { // Non Individual
                        if( componentResponse.getMultiBureauJsonRespose() != null ) {
                            componentResponse.setMultiBureauJsonRespose(null) ;
                        }
                    }
                }
            }
        }
    }

    private void checkPropertyValuation(String refId, Header header){
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setRefId(refId);
        valuationRequest.setHeader(header);
        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        // If there is any submitted valuation then try to get agency report
        if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList()) ) {
            int submittedCount = (int)valuation.getValuationDetailsList().stream()
                    .filter(valuationDetails -> (StringUtils.equalsIgnoreCase(valuationDetails.getStatus(),
                            ThirdPartyVerification.Status.SUBMITTED.name())) )
                    .count();
            // If there is any submitted valuation then try to get agency report
            if (submittedCount > 0) {
                // Fetch agency's responses for triggered valuations
                logger.debug("{} Checking for valuation agency reports agency : ...", valuationRequest.getRefId());
                moduleHelper.getAgencyValuationReport(valuationRequest, valuation);
            }
        }
    }

    private void setHierarchyData(GoNoGoCroApplicationResponse applicationData, String userId, Header header,
                                  boolean isEditable) {
        String stageId = applicationData.getApplicationRequest().getCurrentStageId();
        boolean coOrigination = applicationData.getApplicationRequest().getRequest().getApplication().isCoOrigination();
        if(isEditable && (Stages.isCpaStage(stageId) || Stages.isCreditStage(stageId) || Stages.isOpsStage(stageId))) {
            logger.debug("Fetching hierarchy data for {} refID" + applicationData.getGngRefId());
            Integer branchCode = applicationData.getApplicationRequest().getAppMetaData().getBranchV2().getBranchId();
            List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasters = masterDataViewRepository
                    .fetchOrganizationalHierarchyMasters(header.getInstitutionId(), branchCode, userId, header.getProduct().name());

            List<String> rolesToFetch = new ArrayList<>();
            rolesToFetch.addAll(Roles.getCpaRoles());
            rolesToFetch.add(Roles.Role.CREDIT.name());
            rolesToFetch.addAll(Roles.getCreditRoles());
            if (Stages.isOpsStage(stageId)) {
                rolesToFetch.addAll(Roles.getOpsRoles());
            }
            // adding roles of co_origination if the case is of co_origination i.e. coorigination is true
            if(coOrigination){
                rolesToFetch.add(Roles.Role.EXTERNAL_CREDIT.name());
                rolesToFetch.add(Roles.Role.EXTERNAL_OPS.name());
            }
            List<HierarchyUser> usersList = getHierarchyUsers(organizationalHierarchyMasters, rolesToFetch, userId);
            applicationData.setHierarchyDetails(usersList);
        }
    }

    private List<HierarchyUser> getHierarchyUsers(List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasters, List<String> rolesToFetch, String userId) {
        List<HierarchyUser> users = new ArrayList<>();
        organizationalHierarchyMasters.forEach(master -> {
            master.getRoleWiseUsersList().forEach(roleWiseUsers -> {
                if(rolesToFetch.contains(roleWiseUsers.getRole())){
                    users.addAll(roleWiseUsers.getUsers().stream().filter(user -> user.isActive() &&
                            !StringUtils.equalsIgnoreCase(userId, user.getUserId())).collect(Collectors.toList()));
                }
            });
        });
        List<HierarchyUser> hierarchyUsers = users.stream()
                .sorted(Comparator.comparing(HierarchyUser::getUserName)) //comparator - how you want to sort it
                .collect(Collectors.toList());
        return hierarchyUsers;
    }

    /*private void setHierarchyData(GoNoGoCroApplicationResponse applicationData, String userId, String institutionId,
                                  boolean isEditable) {
        String stageId = applicationData.getApplicationRequest().getCurrentStageId();
        if(isEditable && (Stages.isCpaStage(stageId) || Stages.isCreditStage(stageId) || Stages.isOpsStage(stageId))){
            logger.debug("Fetching hierarchy data for {} refID" + applicationData.getGngRefId());
            //TODO Amit change getBranchCode logic to commented code after android fixes the problem
            Integer branchCode = applicationData.getApplicationRequest().getAppMetaData().getBranchV2().getBranchId();
            String product = applicationData.getApplicationRequest().getHeader().getProduct().name();
            //applicationData.getApplicationRequest().getHeader().getBranchCode();
            List<OrganizationalHierarchyMaster> organizationalHierarchyMasters = masterDataViewRepository
                    .fetchOrganizationalHierarchyMasters(institutionId, branchCode, userId, product);

            Set<HierarchyNode> hierarchyNodes = new HashSet<>();
            organizationalHierarchyMasters.forEach( hierarchyMaster -> {
                List<HierarchyNode> allHierarchyNodeList = hierarchyMaster.getHierarchyNodes().stream()
                            .filter(hierarchyNode -> ! StringUtils.equalsIgnoreCase(hierarchyNode.getUserId(), userId))
                            .collect(Collectors.toList());
                if(Stages.isCpaStage(stageId) || Stages.isCreditStage(stageId)){
                    List<HierarchyNode> cpaAndCreditHierarchyNodes = allHierarchyNodeList.stream()
                            .filter(hierarchyNode -> (StringUtils.equals(Roles.Role.CREDIT.name(), hierarchyNode.getRole()) ||
                                    Roles.isCpaRole(hierarchyNode.getRole()) || Roles.isCreditRole(hierarchyNode.getRole()))).collect(Collectors.toList());
                    hierarchyNodes.addAll(cpaAndCreditHierarchyNodes);
                }else {
                    hierarchyNodes.addAll(allHierarchyNodeList);
                }
            });
            List<HierarchyNode> hierarchyNodeList = new ArrayList<>(hierarchyNodes.stream()
                    .sorted(Comparator.comparing(HierarchyNode::getLevel)) //comparator - how you want to sort it
                    .collect(Collectors.toList()));
            applicationData.setHierarchyDetails(hierarchyNodeList);
        }
    }*/

    @Override
    public BaseResponse getApplicationImages(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        BaseResponse baseResponse;
        logger.debug("getApplicationImages service started ");

        String refId = checkApplicationStatus.getGonogoRefId();

        logger.debug("getApplicationImages service calling getParentApplication repo ");

        GoNoGoCroApplicationResponse applicationData = multiProductRepository
                .getParentApplication(refId);

        logger.debug("getApplicationImages service call to getParentApplication repo completed");

        Set<String> refSet = null;

        if (applicationData != null) {

            refSet = getAllRefId(applicationData, refId);

        } else {

            refSet = new HashSet<>();

            if (applicationRepository.isApplicationRequestExist(refId, checkApplicationStatus.getHeader().getInstitutionId())) {

                refSet.add(refId);

            }
        }

        if (null != refSet && !refSet.isEmpty()) {

            logger.debug("getApplicationImages service calling getKycImageDetails repo ");

            List<KycImageDetails> appImgDetails = uploadFileRepository
                    .getKycImageDetails(refSet, checkApplicationStatus.getHeader()
                            .getInstitutionId(), Status.IMAGES.name());

            logger.debug("getApplicationImages service completed");

            if (appImgDetails != null && !appImgDetails.isEmpty()) {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, appImgDetails);

            } else {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }


    @Override
    public BaseResponse getDocumentsForDmsPush(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        BaseResponse baseResponse;
        logger.debug("getApplicationImages service started ");

        String refId = checkApplicationStatus.getGonogoRefId();


        logger.debug("getApplicationImages service calling getKycImageDetails repo ");

        List<ImagesDetails> documentsForDmsPush = uploadFileRepository.getDocumentsForDmsPush(refId, checkApplicationStatus.getHeader().getInstitutionId());

        logger.debug("getApplicationImages service completed with document size {}",documentsForDmsPush.size());

        if (!CollectionUtils.isEmpty(documentsForDmsPush)) {

            /**
             * add cibil report image details
             */
            List<ImagesDetails> imagesDetailsList = DMSUtils.addCibilReportObjectId(documentsForDmsPush);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, imagesDetailsList);

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }


        return baseResponse;
    }

    @Override
    public BaseResponse getPosidexDedupeApplicationDetails(ApplicationStatusLogRequest applicationStatusLogRequest) {
        BaseResponse baseResponse;
        PosidexResponse posidexResponse = applicationRepository.getPosidexResponse(applicationStatusLogRequest);
        if (null != posidexResponse) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, posidexResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse updateDedupeParameter(AdditionalDedupeFieldsRequest additionalDedupeFieldsRequest) {

        BaseResponse baseResponse;

        boolean updateFlagForGoNoGODoc = applicationRepository.updateDedupeFields(additionalDedupeFieldsRequest);
        boolean updateFlagForApplicationRequestDoc = applicationRepository.updateDedupeFieldsInApplicationRequest(additionalDedupeFieldsRequest);
        if (updateFlagForGoNoGODoc && updateFlagForApplicationRequestDoc) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, updateFlagForGoNoGODoc);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }


    @Override
    public BaseResponse getDashboardData(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        BaseResponse baseResponse;
        logger.debug("getDashboardData service started");

        if (!configurationManager.isValidVersion(checkApplicationStatus
                .getHeader().getInstitutionId(), checkApplicationStatus
                .getHeader().getApplicationSource())) {

            logger.debug("getDashboardData service finished due to outdated version");

            Collection<com.softcell.gonogo.model.response.core.Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
        }


        String refId = checkApplicationStatus.getGonogoRefId();


        logger.debug("getDashboardData service calling getAppByRefId repo for completed request");

        GoNoGoCroApplicationResponse applicationData = applicationRepository
                .getApplicationByRefId(refId, CacheConstant.WEB.toString(),
                        checkApplicationStatus.getHeader().getInstitutionId());

        if (applicationData != null) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationData.getApplicationRequest());

        } else {

            logger.debug("getDashboardData service calling getAppByRefId repo for partial request");

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationRepository
                    .getApplicationRequestByRefId(refId, checkApplicationStatus
                            .getHeader().getInstitutionId()));

        }

        return baseResponse;
    }

    @Override
    public BaseResponse getPartialSavedApplicationData(
            CheckApplicationStatus checkApplicationStatus) throws Exception {


        logger.debug("getPartialSavedApplicationData service started");
        BaseResponse baseResponse;

        GoNoGoCustomerApplication gngCA = new GoNoGoCustomerApplication();

        String refId = checkApplicationStatus.getGonogoRefId();

        logger.debug("getPartialSavedApplicationData service calling appByRefId repo");

        ApplicationRequest applicationRequest = applicationRepository
                .getApplicationRequestByRefId(refId, checkApplicationStatus
                        .getHeader().getInstitutionId());


        if (applicationRequest != null) {

            gngCA.setApplicationRequest(applicationRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, gngCA);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        logger.debug("getPartialSavedApplicationData service completed");

        return baseResponse;
    }

    @Override
    public BaseResponse getApplicationDataForCroTwoScreen(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        BaseResponse baseResponse;
        logger.debug("getApplicationDataForCroTwoScreen service started");

        String refId = checkApplicationStatus.getGonogoRefId();

        logger.debug("getApplicationDataForCroTwoScreen service calling AppbyRefId repo");

        GoNoGoCroApplicationResponse applicationData = applicationRepository
                .getApplicationByRefId(refId, CacheConstant.WEB.toString(),
                        checkApplicationStatus.getHeader().getInstitutionId());

        logger.debug("getApplicationDataForCroTwoScreen service call to AppbyRefId repo completed");

        if (applicationData != null) {

            Set<String> refSet = getAllRefId(applicationData, refId);

            setApplicationPostIpaDetails(refId, refSet, checkApplicationStatus.getHeader().getInstitutionId(), applicationData);

            logger.debug("getApplicationDataForCroTwoScreen service completed with response");

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationData);

        } else {

            logger.debug("getApplicationDataForCroTwoScreen service completed with no response");

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }

    @Override
    public BaseResponse setCroDecesion(
            CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("setCroDecesion service started calling updateCroDec repo");

        BaseResponse response;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String status = GNGWorkflowConstant.APPROVED.toFaceValue();
        String stage = GNGWorkflowConstant.APRV.toFaceValue();

        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, croApprovalRequest.getHeader());
        if( StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.SUBJ_APRV.toFaceValue())){
            activityLog.setAction(GNGWorkflowConstant.SUBJECTIVE_APPROVAL.toFaceValue());
            status = GNGWorkflowConstant.SUBJECTIVE_APPROVAL.toFaceValue();
            stage = GNGWorkflowConstant.SUBJ_APRV.toFaceValue();
        } else if(StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(),GNGWorkflowConstant.EXT_APPROVED.toFaceValue())) {
            activityLog.setAction(GNGWorkflowConstant.EXT_APPROVAL.toFaceValue());
            status = GNGWorkflowConstant.EXT_APPROVED.toFaceValue();
            stage = GNGWorkflowConstant.EXT_APRV.toFaceValue();
        } else {
            activityLog.setAction(GNGWorkflowConstant.CRO_APPROVAL.toFaceValue());
        }

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(croApprovalRequest.getReferenceId(), croApprovalRequest.getHeader().getInstitutionId());

        String applicationStatus = goNoGoCroApplicationResponse.getApplicationStatus();
        String currentStageId = goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId();

        boolean stageCheckForDeclineCase = StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.DECLINED.toFaceValue()) &&
                StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.DCLN.toFaceValue());

        boolean stageCheckForQueuedCase = StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.QUEUED.toFaceValue()) &&
                StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.CR_Q.toFaceValue());

        //Approval status of case can be updated if application current status is not in OPS stages
        boolean stageCheckForCreditCase = !StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.BOPS.toFaceValue()) &&
                        !StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.HOPS.toFaceValue());

        /**
         * to Approve any application first check if its previous status is DECLINED ,stage is DCLN or status is QUEUED ,stage is CR_Q otherwise provide error message.
         */
        if (stageCheckForDeclineCase || stageCheckForQueuedCase || stageCheckForCreditCase ) {

            boolean updateStatus = applicationRepository
                    .updateCroDecision(croApprovalRequest);
            logger.debug("setCroDecesion service update cro decision with udpateStatus,", updateStatus);
            if(!Institute.isInstitute(croApprovalRequest.getHeader().getInstitutionId(), Institute.SBFC))
                appConfigurationHelper.intimateUsers(croApprovalRequest.getReferenceId(), IntimationConstants.CREDIT_DECISION);

            if(stageCheckForCreditCase) {
                if( updateStatus ) {
                    updateCamSummary(croApprovalRequest, httpRequest);
                    updateDM(croApprovalRequest);
                    updateApplicationBucket(croApprovalRequest, goNoGoCroApplicationResponse);
                }
            }

            // Save Activity log
            populateActivityLog(activityLog, croApprovalRequest.getReferenceId(), status, stage);
            activityLog.setUserId(croApprovalRequest.getHeader().getLoggedInUserId());
            activityLog.setUserRole(croApprovalRequest.getHeader().getLoggedInUserRole());

            GenericResponse genericResponse = new GenericResponse();
            if (updateStatus) {
                if(Institute.isInstitute(croApprovalRequest.getHeader().getInstitutionId(), Institute.SBFC)
                        && StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.SUBJ_APRV.toFaceValue()))
                    appConfigurationHelper.intimateUsers(croApprovalRequest.getReferenceId(), IntimationConstants.CREDIT_DECISION);

                genericResponse.setStatus("OK UPDATE SUCCESSFULLY");
                try {
                    updateReProcessCountAfterDecision(croApprovalRequest);
                    // update CRO decision in Sales-Force currently for DMI only.
                    //salesForceRepository.updateCroDecision(croApprovalRequest);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    throw e;
                }

                logger.debug("setCroDecision service completed with successful update");

                /**
                 * TVR call
                 */
                if (lookupService.checkActionsAccess(croApprovalRequest.getHeader().getInstitutionId(),
                        croApprovalRequest.getHeader().getProduct().toProductId(),
                        ActionName.TVR_DATA_PUSH)) {
                    logger.debug("Going to call TVR data push service for refId: " +
                                    "{},InstitutionId: {},ProductId: {}", croApprovalRequest.getReferenceId(),
                            croApprovalRequest.getHeader().getInstitutionId(),
                            croApprovalRequest.getHeader().getProduct().toProductId());
                    dmzConnector.pushTvr(croApprovalRequest.getReferenceId(), croApprovalRequest.getHeader().getInstitutionId(), null);
                } else {
                    logger.debug("Criteria not match to call TVR data push service for refId: {},InstitutionId: {},ProductId: {}", croApprovalRequest.getReferenceId(),
                            croApprovalRequest.getHeader().getInstitutionId(),
                            croApprovalRequest.getHeader().getProduct().toProductId());
                }

                /**
                 * Send Approval SMS
                 */
                if (lookupService.checkActionsAccess(
                        croApprovalRequest.getHeader().getInstitutionId()
                        , croApprovalRequest.getHeader().getProduct().toProductId()
                        , ActionName.SEND_APPROVAL_SMS)) {

                    //Get the SMS contents
                    SmsTemplateConfiguration smsTemplateConfiguration =
                            lookupService.getSmsTemplateConfiguration(
                                    croApprovalRequest.getHeader().getInstitutionId()
                                    , croApprovalRequest.getHeader().getProduct().toProductId()
                                    , GNGWorkflowConstant.APPROVAL_SMS.name());
                    String smsMessage = null;
                    if (smsTemplateConfiguration != null) {
                        smsMessage = smsTemplateConfiguration.getFormattedSMSText(
                                goNoGoCroApplicationResponse.getGngRefId());
                    } else {
                        smsMessage = "TVS Loan approved. For Ref Id : " + croApprovalRequest.getReferenceId();
                    }
                    //Send SMS
                    BaseResponse smsResponse = clientAuthenticationManager.sendSMS(
                            goNoGoCroApplicationResponse.getApplicationRequest(), smsMessage);
                }
            } else {
                logger.debug("setCroDecision service completed with failed status");
                genericResponse.setStatus(Constant.FAILED);
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
        } else {
            response = GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug("setCroDecision service publishing activity logs");
        activityEventPublisher.publishEvent(activityLog);

        return response;
    }

    private void updateDM(CroApprovalRequest croApprovalRequest) {
        DMRequest dmRequest = new DMRequest();
        dmRequest.setRefId(croApprovalRequest.getReferenceId());
        dmRequest.setHeader(croApprovalRequest.getHeader());
        DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(dmRequest);
        if(disbursementMemo != null) {
            if(disbursementMemo.getCamSummary() != null) {
                disbursementMemo.getCamSummary().setFinalLoanApprovedBy(croApprovalRequest.getHeader().getLoggedInUserId());
                disbursementMemo.getCamSummary().setApprovalDate(new Date());
            }
            dmRequest.setDisbursementMemo(disbursementMemo);
            applicationRepository.saveDMDetails(dmRequest);
        }
    }

    private void updateApplicationBucket(CroApprovalRequest croApprovalRequest, GoNoGoCroApplicationResponse goNoGoCroApplicationResponse){
        if(StringUtils.equalsIgnoreCase(croApprovalRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.name())) {
            try {
                String refId = croApprovalRequest.getReferenceId();
                String instId = croApprovalRequest.getHeader().getInstitutionId();
                PerfiosData perfiosData = applicationRepository.fetchPerfiosData(refId, instId);
                if (Buckets.isCreditBucket(goNoGoCroApplicationResponse.getApplicationBucket())) {
                    if (perfiosData != null) {
                        Optional<BankData> bankData = perfiosData.getSaveDataList().stream().filter(obj -> !StringUtils.equalsIgnoreCase(obj.getStatus(), Status.FAILED.name())).findAny();
                        if (!bankData.isPresent())
                            applicationRepository.updateApplicationBucket(refId, instId);
                    } else {
                        applicationRepository.updateApplicationBucket(refId, instId);
                    }
                }
            } catch (Exception e) {
                logger.error("Error while updating the ApplicationBucket {}", e.getStackTrace());
            }
        }
    }

    private void updateCamSummary(CroApprovalRequest croApprovalRequest, HttpServletRequest httpRequest) throws Exception {
        CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
        camDetailsRequest.setRefId(croApprovalRequest.getReferenceId());
        camDetailsRequest.setHeader(croApprovalRequest.getHeader());
        camDetailsRequest.setCamType(EndPointReferrer.CAM_SUMMARY);
        CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
        if(camDetails == null) {
            camDetails = new CamDetails();
        }

        CamSummary camSummary = camDetails.getSummary();
        if(camSummary != null) {
            camSummary.setApprovalDate(new Date());
            camSummary.setFinalLoanApprovedBy(croApprovalRequest.getHeader().getLoggedInUserId());
            List<Remark> remarkList = camSummary.getRemarkList();
            if(remarkList == null) {
                remarkList = new ArrayList<>();
                camSummary.setRemarkList(remarkList);
            }
            for (CroJustification croJustification : croApprovalRequest.getCroJustification()) {
                Remark remark = new Remark();
                remark.setBy(croJustification.getCroID());
                remark.setComment(croJustification.getRemark());
                remark.setRemarkDate(croJustification.getCroJustificationUpdateDate());
                remarkList.add(remark);
            }
            camDetails.setSummary(camSummary);
            camDetailsRequest.setCamDetails(camDetails);
            dataEntryManager.saveCamDetails(camDetailsRequest, camDetailsRequest.getCamType(), false, httpRequest);
        }
    }

    @Override
    public BaseResponse resetStatus(CroApprovalRequest croApprovalRequest) throws Exception {

        logger.debug("resetStatus service started ");


        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, croApprovalRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.RESET_APP_STATUS.toFaceValue());
        String applicationID = croApprovalRequest.getHeader()
                .getApplicationId();

        String institutionID = croApprovalRequest.getHeader()
                .getInstitutionId();

        String croID = croApprovalRequest.getHeader().getCroId();

        String referenceId = croApprovalRequest.getReferenceId();

        String status = croApprovalRequest.getApplicationStatus();

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(Status.FAILED.name());

        if (StringUtils.equalsIgnoreCase(status, GNGWorkflowConstant.QUEUED.toFaceValue())) {

            logger.debug("resetStatus service setting postIpaStage for stage {}", status);

            applicationRepository.setPostIpaStage(referenceId, GNGWorkflowConstant.CR_Q.toFaceValue(),
                    croApprovalRequest.getHeader().getInstitutionId());

        }

        logger.debug("resetStatus service reset status of application to {}", status);

        boolean updateStatus = applicationRepository.resetStatus(applicationID,
                institutionID, croID, referenceId, status);
        String stage = GngUtils.getCurrentStageBasedOnApplicationStatus(status);
        if (updateStatus) {

            genericResponse.setStatus(Status.OK.name());

        }
        populateActivityLog(activityLog, croApprovalRequest.getReferenceId(), status,stage);

        logger.debug("resetStatus service reset status competed ");
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
    }

    @Override
    public BaseResponse setCroDecesionOnHold(
            CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("setCroDecisionOnHold service started ");

        BaseResponse response;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, croApprovalRequest.getHeader());
        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(croApprovalRequest.getReferenceId(), croApprovalRequest.getHeader().getInstitutionId());

        String applicationStatus = goNoGoCroApplicationResponse.getApplicationStatus();
        String currentStageId = goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId();

        //form Delivery Order Name using RefId and DO_
        String deliveryOrderName = GngUtils.formDeliveryOrderName(croApprovalRequest.getReferenceId());

        // get doImageId using RefId and DeliveryOrderName
        String dOImageId = uploadFileRepository.getImageIdByRefIdAndFileName(croApprovalRequest.getReferenceId(), deliveryOrderName);

        boolean approveStageCheckForDeclineCase = StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.APPROVED.toFaceValue())
                && StringUtils.isBlank(dOImageId)
                && StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.DECLINED.toFaceValue());

        boolean queuedStageCheckForDeclineCase = StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.QUEUED.toFaceValue())
                && StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.CR_Q.toFaceValue())
                && StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.DECLINED.toFaceValue());

        boolean queuedStageCheckForOnHoldCase = StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.QUEUED.toFaceValue())
                && StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.CR_Q.toFaceValue())
                && StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(), GNGWorkflowConstant.ON_HOLD.toFaceValue());

        boolean dedupeFoundStageForDedupeQueueCase = StringUtils.equalsIgnoreCase(applicationStatus,GNGWorkflowConstant.DEDUPE_QUEUE.toFaceValue())
                && StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.DEDUPE_FOUND.toFaceValue());


        //Approval status of case can be updated if application current status is not in OPS stages
        boolean stageCheckForCreditCase = !StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.BOPS.toFaceValue()) &&
                !StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.HOPS.toFaceValue());

        /**
         * to decline application check if its previous status is APPROVED ,stage is APRV or status is QUEUED , stage is CR_Q otherwise provide error message.
         * to On Hold application check if its previous status is QUEUED ,stage is CR_Q otherwise provide error message.
         */

        if ((approveStageCheckForDeclineCase || queuedStageCheckForDeclineCase)
                || queuedStageCheckForOnHoldCase || dedupeFoundStageForDedupeQueueCase || stageCheckForCreditCase ) {

            boolean updateStatus = applicationRepository
                    .updateOnHoldCroDecision(croApprovalRequest);
            //appConfigurationHelper.intimateUsers(croApprovalRequest.getReferenceId(), IntimationConstants.CREDIT_DECISION);
            if (updateStatus) {
                updateCamSummary(croApprovalRequest, httpRequest);
            }

            // Save activity log
            String status = GNGWorkflowConstant.DECLINED.toFaceValue();
            String stage = GNGWorkflowConstant.DCLN.toFaceValue();
            String action = GNGWorkflowConstant.CRO_DECLINE.toFaceValue();
            if (StringUtils.equalsIgnoreCase(croApprovalRequest.getApplicationStatus(),
                    GNGWorkflowConstant.ON_HOLD.toFaceValue())) {
                status = GNGWorkflowConstant.ON_HOLD.toFaceValue();
                stage = GNGWorkflowConstant.CR_H.toFaceValue();
                action = GNGWorkflowConstant.CRO_ONHOLD.toFaceValue();
            }

            populateActivityLog(activityLog, croApprovalRequest.getReferenceId(), status, stage);
            activityLog.setAction(action);

            GenericResponse genericResponse = new GenericResponse();
            genericResponse.setStatus(Status.FAILED.name());

            if (updateStatus) {
                try {
                    updateReProcessCountAfterDecision(croApprovalRequest);
                    // Update CRO decision in sales force currently only for DMI.
                    // salesForceRepository.updateCroDecision(croApprovalRequest);
                    if(!Institute.isInstitute(croApprovalRequest.getHeader().getInstitutionId(), Institute.SBFC))
                        appConfigurationHelper.intimateUsers(croApprovalRequest.getReferenceId(), IntimationConstants.CREDIT_DECISION);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    throw e;
                }
                genericResponse.setStatus("OK UPDATE SUCCESSFULLY");
                logger.debug("saveActivityLog repo completed with updated status");
                /**
                 * TVR call
                 */
                if (lookupService.checkActionsAccess(croApprovalRequest.getHeader().getInstitutionId(),
                        croApprovalRequest.getHeader().getProduct().toProductId(),
                        ActionName.TVR_DATA_PUSH)) {
                    logger.debug("Going to call TVR data push service for refId: " +
                                    "{},InstitutionId: {},ProductId: {}", croApprovalRequest.getReferenceId(),
                            croApprovalRequest.getHeader().getInstitutionId(),
                            croApprovalRequest.getHeader().getProduct().toProductId());
                    dmzConnector.pushTvr(croApprovalRequest.getReferenceId(), croApprovalRequest.getHeader().getInstitutionId(), null);
                } else {
                    logger.debug("Criteria not match to call TVR data push service for refId: {},InstitutionId: {},ProductId: {}", croApprovalRequest.getReferenceId(),
                            croApprovalRequest.getHeader().getInstitutionId(),
                            croApprovalRequest.getHeader().getProduct().toProductId());
                }
            } else {
                logger.debug("saveActivityLog repo completed ");
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
        } else {
            response = GngUtils.getBaseResponse(CustomHttpStatus.UNABLE_TO_CHANGE_APPLICATION_STATUS, GngUtils.getChangeApplicationStatusErrorList());
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug("setCroDecision service publishing activity logs");
        activityEventPublisher.publishEvent(activityLog);
        return response;
    }

    @Override
    public BaseResponse getCroQueue(CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("getCroQueue service started");
        BaseResponse baseResponse;

        List<CroQueue> croQueue = applicationRepository.getCroQueue(croQueueRequest).collect(Collectors.toList());

        if (null != croQueue && !croQueue.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croQueue);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCroTwoQueue(CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("getCroTwoQueue service started");

        BaseResponse baseResponse;

        List<CroQueue> croQueue = applicationRepository.getCro2Queue(croQueueRequest);

        if (null != croQueue && !croQueue.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croQueue);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCroQueueCriteria(CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("getCroQueueCriteria service started");

        BaseResponse baseResponse;

        List<CroQueue> croQueue = applicationRepository.getCroQueueCriteria(croQueueRequest);

        if (null != croQueue && !croQueue.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croQueue);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCro3Queue(CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("getCro3Queue service started");

        BaseResponse baseResponse;

        List<CroQueue> croQueue = applicationRepository.getCro3Queue(croQueueRequest);

        if (null != croQueue && !croQueue.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croQueue);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getCro4Queue(CroQueueRequest croQueueRequest) throws Exception {

        logger.debug("getCro4Queue service started");

        BaseResponse baseResponse;

        List<CroQueue> croQueue = applicationRepository.getCro4Queue(croQueueRequest);

        if (null != croQueue && !croQueue.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croQueue);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getpostIpo(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("getpostIpo service started");

        BaseResponse baseResponse;

        PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(checkApplicationStatus.getGonogoRefId(),
                checkApplicationStatus.getHeader().getInstitutionId());

        if (null != postIpaRequest) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, postIpaRequest);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse updateLosDetails(LOSDetailsRequest lOSDetailsRequest) throws Exception {

        logger.debug("updateLosDetails service started");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, lOSDetailsRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.LOS_STATUS_UPDATE.toFaceValue());
        populateActivityLog(activityLog, lOSDetailsRequest.getRefID(), GNGWorkflowConstant.APPROVED.toFaceValue(),
                lOSDetailsRequest.getlOSDetails().getStatus());

        GenericResponse genericResponse = new GenericResponse();

        genericResponse.setStatus(Status.FAILED.name());

        boolean isApplicationStatusCancelled = applicationRepository.isApplicationStatusCancelled(lOSDetailsRequest.getRefID(), lOSDetailsRequest.getHeader().getInstitutionId());

        //check application status here
        if (isApplicationStatusCancelled) {

            genericResponse.setStatus(Status.FAILED.name());
            activityLog.setStatus(Status.FAILED.name());
            activityLog.setCustomMsg("Can Not updateLos details because application has been cancelled.");

            populateActivityLog(activityLog, lOSDetailsRequest.getRefID(), GNGWorkflowConstant.CANCELLED.toFaceValue(),
                    GNGWorkflowConstant.CNCLD.toFaceValue());

            logger.error("application status found cancelled at time updating Los Details for ref id {}", lOSDetailsRequest.getRefID());

        } else {

            boolean status = applicationRepository.updateLosDetails(lOSDetailsRequest.getRefID(),
                    lOSDetailsRequest.getlOSDetails(), lOSDetailsRequest.getHeader().getInstitutionId());

            logger.debug("updateLosDetails service completed with update status {}", status);

            if (status) {
                genericResponse.setStatus(Status.SUCCESS.name());
                activityLog.setStatus(Status.SUCCESS.name());
                activityLog.setCustomMsg("Los Details Update successfully");
            }

        }


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
    }

    @Override
    public BaseResponse updateInvoiceDetails(
            InvoiceDetailsRequest invoiceDetailsRequest) throws Exception {

        logger.debug("updateInvoiceDetails service started");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, invoiceDetailsRequest.getHeader());
        populateActivityLog(activityLog, invoiceDetailsRequest.getRefID(), GNGWorkflowConstant.APPROVED.toFaceValue(),
                GNGWorkflowConstant.INV_GNR.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.INVOICE_GENERATION.toFaceValue());


        GenericResponse genericMsgResponse=new GenericResponse();
        genericMsgResponse.setStatus(Status.FAILED.name());

        boolean updateStatus = applicationRepository.updateInvoiceDetails(invoiceDetailsRequest.getRefID(),
                invoiceDetailsRequest.getInvoiceDetails(), invoiceDetailsRequest.getHeader().getInstitutionId());

        DmzRequest dmzRequest = new DmzRequest();
        dmzRequest.setHeader(invoiceDetailsRequest.getHeader());
        dmzRequest.setGonogoRefId(invoiceDetailsRequest.getRefID());

        if (updateStatus) {
            genericMsgResponse.setStatus(Status.SUCCESS.name());

            boolean digitizationFormAction = lookupService.checkActionsAccess(invoiceDetailsRequest.getHeader().getInstitutionId(), invoiceDetailsRequest.getHeader().getProduct().toProductId(),
                    ActionName.DIGI_FORM_SAVE_AT_INVOICE_TIME);

            // this action added for save digitization form purpose(application form ,agreement form )
            if (digitizationFormAction) {

                //store application form bytes in gridFs
                saveApplicationForm(invoiceDetailsRequest, GNGWorkflowConstant.APPLICATION_FORM.toFaceValue());

                //store agreement form bytes in gridFs
                saveAgreementForm(invoiceDetailsRequest, GNGWorkflowConstant.AGREEMENT_FORM.toFaceValue());

                //store ach mandate form
                saveAchMandateForm(invoiceDetailsRequest, TemplateName.ACH_MANDATE_FORM.name());

            }

            //push data to los
            pushDataToLos(invoiceDetailsRequest);

            //push data to DMS
            pushDataToDMS(invoiceDetailsRequest);

            //push mb data to los
            //method added in pushDataToLos
            //dmzConnector.postMBDataToLos(dmzRequest);

                /**
                 * Send SMS
                 */
               if (lookupService.checkActionsAccess(
                        invoiceDetailsRequest.getHeader().getInstitutionId()
                        , invoiceDetailsRequest.getHeader().getProduct().toProductId()
                        , ActionName.SEND_EW_SMS)) {
                   ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());
                   if (extendedWarrantyDetails != null) {
                       try {
                           //Get Customer Application
                           GoNoGoCroApplicationResponse goNoGoCroApplicationResponse =
                                   applicationRepository.getGoNoGoCustomerApplicationByRefId(
                                           invoiceDetailsRequest.getRefID()
                                           , invoiceDetailsRequest.getHeader().getInstitutionId());
                           if (goNoGoCroApplicationResponse != null) {
                               PostIPA postIpa = applicationRepository.getPostIPA(invoiceDetailsRequest.getRefID(),
                                       invoiceDetailsRequest.getHeader().getInstitutionId());
                               if (postIpa != null) {
                                   //Get the SMS contents
                                   SmsTemplateConfiguration smsTemplateConfiguration =
                                           lookupService.getSmsTemplateConfiguration(
                                                   invoiceDetailsRequest.getHeader().getInstitutionId()
                                                   , invoiceDetailsRequest.getHeader().getProduct().toProductId()
                                                   , GNGWorkflowConstant.EXTENDED_WARRANTY_SMS.name());
                                   if (smsTemplateConfiguration != null) {
                                       String modelNo = GngUtils.getModelNo(postIpa);
                                       String smsMessage = null;
                                       Date startDate=null;
                                       Date endDate=null;
                                       if (modelNo != null && goNoGoCroApplicationResponse.getInvoiceDetails().getInvoiceDate() != null ) {

                                           //startDate=getInvoiceDate() + extendedWarrantyDetails.getOemWarranty()
                                           startDate = GngDateUtil.addYearsToDate(goNoGoCroApplicationResponse.getInvoiceDetails().getInvoiceDate(), (int) extendedWarrantyDetails.getOemWarranty());
                                           //startDate + extendedWarrantyDetails.getEwWarrantyTenure()
                                           endDate = GngDateUtil.addYearsToDate(startDate, (int) extendedWarrantyDetails.getEwWarrantyTenure());
                                           smsMessage = smsTemplateConfiguration.getFormattedSMSText(
                                                   invoiceDetailsRequest.getHeader().getProduct().toProductName()
                                                   , modelNo
                                                   , "" + GngDateUtil.changeDateFormat(startDate, GngDateUtil.DD_MMM_YY)
                                                   , "" + GngDateUtil.changeDateFormat(endDate
                                                           , GngDateUtil.DD_MMM_YY)
                                           );

                                          if (smsMessage != null) {
                                               BaseResponse smsResponse = clientAuthenticationManager.sendSMS(goNoGoCroApplicationResponse.getApplicationRequest()
                                                       , smsMessage);
                                               if (null != smsResponse && null != smsResponse.getPayload()) {
                                                   //genericMsgResponse = (GenericMsgResponse) smsResponse.getPayload().getT();
                                                   genericMsgResponse.setStatus(Status.SUCCESS.name());
                                                   //genericMsgResponse.setMessage(MsgConstants.EW_SMS_SUCCESS_MSG);
                                               }
                                           } else {
                                               logger.error(ErrorCode.ERR_MSG_NT_CONFIGURED);
                                               genericMsgResponse.setStatus(Status.FAILED.name());
                                               //genericMsgResponse.setMessage(ErrorCode.ERR_MSG_NT_CONFIGURED);
                                           }
                                       } else {
                                           logger.error(ErrorCode.ERROR_SENDING_SMS);
                                           logger.error(String.format(ErrorCode.ERROR_SENDING_SMS_FIELD_VALUES, smsMessage, modelNo
                                                   , startDate
                                                   ,endDate));
                                           genericMsgResponse.setStatus(Status.ERROR.name());
                                           /*
                                           genericMsgResponse.setMessage(String.format(ErrorCode.ERROR_SENDING_SMS_FIELD_VALUES, smsMessage, modelNo
                                                   , startDate
                                                   ,endDate));
                                                   */
                                       }
                                   } else {
                                       //smsTemplateConfiguration is Null
                                       logger.error(ErrorCode.ERROR_SENDING_SMS_TEMPLATE_NT_FOUND);
                                       genericMsgResponse.setStatus(Status.ERROR.name());
                                       //genericMsgResponse.setMessage(ErrorCode.ERROR_SENDING_SMS_TEMPLATE_NT_FOUND);
                                   }
                               } else {
                                   logger.error(ErrorCode.ERROR_SENDING_SMS_POSTIPA_DTLS_NTFND);
                                   genericMsgResponse.setStatus(Status.ERROR.name());
                                   //genericMsgResponse.setMessage(ErrorCode.ERROR_SENDING_SMS_POSTIPA_DTLS_NTFND);
                               }
                           } else {
                               logger.error(String.format(ErrorCode.ERROR_SENDING_SMS_APP_NTFND, extendedWarrantyDetails.getRefId()));
                               genericMsgResponse.setStatus(Status.ERROR.name());
                               //genericMsgResponse.setMessage(String.format(ErrorCode.ERROR_SENDING_SMS_APP_NTFND, extendedWarrantyDetails.getRefId()));
                           }
                       } catch (Exception e) {
                           logger.error(String.format(ErrorCode.ERROR_DATA_RETRIEVAL, extendedWarrantyDetails.getRefId(), e.getMessage()));
                           genericMsgResponse.setStatus(Status.ERROR.name());
                           //genericMsgResponse.setMessage(String.format(ErrorCode.ERROR_DATA_RETRIEVAL, extendedWarrantyDetails.getRefId(), e.getMessage()));
                       }
                   } else {
                       logger.info(String.format(ErrorCode.ERROR_EW_DETAILS_NTFND, invoiceDetailsRequest.getRefID()));

                   }
                   logger.debug("updateInvoiceDetails service completed with update status {}", genericMsgResponse.getStatus());
               }
        } else {
            logger.debug("updateInvoiceDetails service completed with update status {}", genericMsgResponse.getStatus());
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        // Save activity log
        activityEventPublisher.publishEvent(activityLog);

        return GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
    }

    private void saveAchMandateForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {
        if (null != digitizationStoreManager.saveAchMandateForm(invoiceDetailsRequest, fileName)) {
            logger.info("AchMandate form stored successfully in the database");
        } else {
            logger.error("Problem occur at the time of saving AchMandate form in the database");
        }

    }

    private void saveAgreementForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {

        if (null != digitizationStoreManager.saveAgreementForm(invoiceDetailsRequest, fileName)) {
            logger.info("Application form stored successfully in the database");
        } else {
            logger.error("Problem occur at the time of saving Application form in the database");
        }

    }

    private void saveApplicationForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {

        if (null != digitizationStoreManager.saveApplicationForm(invoiceDetailsRequest, fileName)) {
            logger.info("Agreement form stored successfully in the database");
        } else {
            logger.error("Problem occur at the time of saving Agreement form in the database");
        }

    }


    private void pushDataToDMS(InvoiceDetailsRequest invoiceDetailsRequest) {
        try {
            logger.debug("Inside push data to DMS");

            PushDMSDocumentsRequest pushDMSDocumentsRequest = new PushDMSDocumentsRequest();
            //build the pushDmsDocumentRequest object.
            pushDMSDocumentsRequest.setHeader(invoiceDetailsRequest.getHeader());
            pushDMSDocumentsRequest.setRefId(invoiceDetailsRequest.getRefID());

            dmsFeedManager.pushDocumentsToDms(pushDMSDocumentsRequest);

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occur at time of pushing documents to DMS with probable cause {}", e.getMessage());

        }
    }

    private void updateReProcessCountAfterDecision(
            CroApprovalRequest croApprovalRequest) throws Exception {
        logger.debug("updateReProcessCountAfterDecision service started");

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository
                .getGoNoGoCustomerApplicationByRefId(croApprovalRequest
                        .getReferenceId(), croApprovalRequest.getHeader()
                        .getInstitutionId());

        if ((goNoGoCustomerApplication.getApplicationRequest()
                .getReAppraiseDetails() != null && goNoGoCustomerApplication
                .getApplicationRequest().getReAppraiseDetails()
                .getReAppraiseCount() > 0)
                || goNoGoCustomerApplication.getReInitiateCount() > 0) {

            goNoGoCustomerApplication
                    .setReProcessCount(goNoGoCustomerApplication
                            .getReProcessCount() + 1);

            FinalApplicationMongoRepository finalAppRepo = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
            finalAppRepo
                    .updateGoNoGoCustomerApplication(goNoGoCustomerApplication);
        }
    }

    @Override
    public BaseResponse getCoApplicationData(
            CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("getCoApplicationData service started");

        String refId = checkApplicationStatus.getGonogoRefId();

        String applicantId = checkApplicationStatus.getApplicantId();

        CoApplicantResponse coAppResponse = new CoApplicantResponse();

        String institutionId = checkApplicationStatus.getHeader()
                .getInstitutionId();

        logger.debug("getCoApplicationData calling getCoApplicantBycoApplicantId repo to fetch coApplicant data");

        CoApplicant coApplicant = applicationRepository
                .getCoApplicantBycoApplicantId(refId, applicantId,
                        institutionId);

        logger.debug("getCoApplicationData call to getCoApplicantBycoApplicantId repo completed");

        coAppResponse.setApplicant(coApplicant);

        logger.debug("getCoApplicationData calling getKycImageDetailsByApplicant repo to fetch image details");

        List<KycImageDetails> appImgDetails = uploadFileRepository
                .getKycImageDetailsByApplicant(refId, applicantId,
                        institutionId);

        logger.debug("getCoApplicationData call to getKycImageDetailsByApplicant repo competed");

        coAppResponse.setImageMap(appImgDetails);

        MultiBureauDetails multiBureauDetails = new MultiBureauDetails();

        logger.debug("getCoApplicationData service calling getCoApplicantMBResponse repo to fetch multibureau data");

        MultiBureauCoApplicantResponse multiBureauResponse = multibureauRepository
                .getCoApplicantMBResponse(refId, applicantId);

        logger.debug("getCoApplicationData service call to getCoApplicantMBResponse repo completed");

        if (multiBureauResponse != null
                && multiBureauResponse.getMultiBureauJsonRespose() != null) {

            ResponseMultiJsonDomain responseMultiJson = multiBureauResponse.getMultiBureauJsonRespose();

            coAppResponse.setMultiJsonDomain(responseMultiJson);


            logger.debug("getCoApplicationData service calling multibureau getScore utility ");

            Map<String, String> score = MultiBureauUtility
                    .getScore(responseMultiJson);

            logger.debug("getCoApplicationData service call to multibureau getScore completed ");

            multiBureauDetails.setChmScore(score.get(GNGWorkflowConstant.HIGHMARK
                    .name()));
            multiBureauDetails.setCibilScore(score
                    .get(GNGWorkflowConstant.CIBIL.name()));
            multiBureauDetails.setEquifaxlScore(score
                    .get(GNGWorkflowConstant.EQUIFAX.name()));
            multiBureauDetails.setExperianScore(score
                    .get(GNGWorkflowConstant.EXPERIAN.name()));

            coAppResponse.setMultiBeauroDetails(multiBureauDetails);

        }

        logger.debug("getCoApplicationData service completed");

        return GngUtils.getBaseResponse(HttpStatus.OK, coAppResponse);
    }

    @Override
    public BaseResponse getMergeApplicationRequest(CheckApplicationStatus checkApplicationStatus) throws Exception {

        logger.debug("getMergeApplicationRequest service is started");
        BaseResponse baseResponse;
        List<ApplicationRequest> dashboardAppData = new ArrayList<>();

        dashboardAppData.add(GngUtils.convertBaseResponsePayload(getDashboardData(checkApplicationStatus), ApplicationRequest.class));

        List<ApplicationRequest> previousAppRequests = auditDataManager
                .getAppRequestFromAuditData(checkApplicationStatus
                                .getGonogoRefId(), checkApplicationStatus
                                .getHeader().getInstitutionId(),
                        AuditType.RE_APPRAISE_RE_INITIATE);

        if (previousAppRequests != null) {

            dashboardAppData.addAll(previousAppRequests);
            dashboardAppData.sort(Comparator.comparing(o -> o.getHeader().getDateTime()));
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dashboardAppData);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    /**
     * It is use to fetch all the reference id hierarchy
     * Like if child id is present all the parent ref id will be return vice versa
     *
     * @param applicationData
     * @param refId
     * @return
     */
    private Set<String> getAllRefId(GoNoGoCroApplicationResponse applicationData, String refId) {

        logger.debug("getAllRefId called to fetch all refIds for refId {}", refId);

        Set<String> refIds = new HashSet<>();

        if (StringUtils.isNotBlank(refId) && (refId.equals(applicationData.getRootID())
                || StringUtils.isBlank(applicationData.getRootID()))) {

            refIds.add(refId);

        } else {

            logger.debug("getAllRefId service fetching refIds from multiProduct repo started");

            refIds = multiProductRepository
                    .getAllRefenceIDAgainstRootApplication(applicationData
                            .getRootID());
        }

        logger.debug("getAllRefId service fetched competed successfully ");

        return refIds;
    }

    /**
     * It is used to populate post ipa detail of application to gonogo customer response
     *
     * @param refId
     * @param refSet
     * @param intitutionId
     * @param applicationData
     */
    private void setApplicationPostIpaDetails(String refId, Set<String> refSet, String intitutionId, GoNoGoCroApplicationResponse applicationData) throws Exception {

        logger.debug("setApplicationPostIpaDetails method called ");

        List<KycImageDetails> appImgDetails = uploadFileRepository
                .getKycImageDetails(refSet, intitutionId, Status.ALL.name());

        PostIpaRequest postIpaRequest = applicationRepository
                .fetchPostIpaDetails(refId, intitutionId);

        applicationData.setAppImgDetail(appImgDetails);

        if (postIpaRequest != null) {
            applicationData.setPostIPA(postIpaRequest.getPostIPA());
        }

        logger.debug("setApplicationPostIpaDetails method completed");
    }

    private void populateActivityLog(ActivityLogs activityLogs, String refId, String appStatus, String stage) {
        if (StringUtils.isNotBlank(refId)) {
            activityLogs.setRefId(refId);
        }
        activityLogs.setStatus(appStatus);
        activityLogs.setStage(stage);
    }


    /**
     * This method is used to push gonogo data to los only after invoice data is generated successfully
     *
     * @param invoiceDetailsRequest
     * @throws Exception
     */
    private void pushDataToLos(InvoiceDetailsRequest invoiceDetailsRequest) throws Exception {
        logger.debug("pushDataToLos service started");

        DmzRequest dmzRequest = new DmzRequest();

        dmzRequest.setGonogoRefId(invoiceDetailsRequest.getRefID());
        dmzRequest.setHeader(invoiceDetailsRequest.getHeader());

        try {

            /*Cache.checkActionsAccess(ActionName.LOS_DATA_ENTRY_INTERFACE, invoiceDetailsRequest.getHeader().getInstitutionId(),
                    invoiceDetailsRequest.getHeader().getProduct().toProductId());*/

            if (lookupService.checkActionsAccess(invoiceDetailsRequest.getHeader().getInstitutionId(),
                    invoiceDetailsRequest.getHeader().getProduct().toProductId(), ActionName.LOS_DATA_ENTRY_INTERFACE)) {

                logger.debug("Calling postGNGDataToLos serivce ");

                DMZResponse dmzResponse = GngUtils.convertBaseResponsePayload(dmzConnector.postGNGDataToLos(dmzRequest), DMZResponse.class);
                if (dmzResponse != null) {
                    logger.debug("postGNGDataToLos service completed with status {}", dmzResponse.getErrorMsg());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }

    }

    @Override
    public BaseResponse getApplicationStatusLog(ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception {

        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationStatusLogRequest.getRefId(), applicationStatusLogRequest.getHeader().getInstitutionId());
        BaseResponse baseResponse;

        if (null != goNoGoCroApplicationResponse) {

            List<ApplicationStatusLogResponse> applicationStatusLogList = buildApplicationStatusLogRequest(goNoGoCroApplicationResponse);

            if (null != applicationStatusLogList && !applicationStatusLogList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationStatusLogList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    private List<ApplicationStatusLogResponse> buildApplicationStatusLogRequest(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse) {

        ApplicationStatusLogResponse applicationStatusLogResponse;
        List<ApplicationStatusLogResponse> applicationStatusLogResponseList = new ArrayList<>();
        int approvedCaseCount = 0;
        List<CroJustification> croJustificationList = goNoGoCroApplicationResponse.getCroJustification();
        List<CroDecision> croDecisionList = goNoGoCroApplicationResponse.getCroDecisions();

        if (null != croJustificationList && !croJustificationList.isEmpty()) {

            for (CroJustification croJustification : croJustificationList) {
                applicationStatusLogResponse = new ApplicationStatusLogResponse();
                applicationStatusLogResponse.setRefId(goNoGoCroApplicationResponse.getGngRefId());

                if (StringUtils.equals(croJustification.getDecisionCase(), GNGWorkflowConstant.APPROVED.toFaceValue())) {
                    if (null != croDecisionList && !croDecisionList.isEmpty() && approvedCaseCount < croDecisionList.size()) {
                        applicationStatusLogResponse.setCroDecision(croDecisionList.get(approvedCaseCount));
                        approvedCaseCount++;
                    }
                }
                applicationStatusLogResponse.setCroJustification(croJustification);
                applicationStatusLogResponseList.add(applicationStatusLogResponse);
            }
        }

        return applicationStatusLogResponseList;
    }

    @Override
    public BaseResponse getDedupeApplicationDetails(ApplicationStatusLogRequest applicationStatusLogRequest) throws Exception {

        FinalApplicationMongoRepository finalApplicationMongoRepository = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());

        return finalApplicationMongoRepository.getDedupeApplicationDetails(applicationStatusLogRequest ,false);

    }

    @Override
    public BaseResponse setTVRStatus(UpdateTvrStatusRequest updateTvrStatusRequest, HttpHeaders httpHeaders,
                                     HttpServletRequest httpRequest) throws Exception {
        logger.debug("setTvrStatus service started calling updateTvr repo");
        BaseResponse response;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, updateTvrStatusRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.TVR_UPDATE.toFaceValue());
        activityLog.setStep(GNGWorkflowConstant.TVR_STATUS_UPDATE.toFaceValue());
        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository
                .getGoNoGoCustomerApplicationByRefId(updateTvrStatusRequest.getReferenceId()
                        , updateTvrStatusRequest.getHeader().getInstitutionId());

        if (goNoGoCroApplicationResponse != null) {
            boolean updateStatus = applicationRepository.updateTvrStatus(updateTvrStatusRequest);
            logger.debug("setTvrStatus service update with udpateStatus,", updateStatus);

            // Save Activity log
            populateActivityLog(activityLog, updateTvrStatusRequest.getReferenceId(), null, null);

            GenericResponse genericResponse = new GenericResponse();
            if (updateStatus) {
                genericResponse.setStatus(Constant.SUCCESS);
            } else {
                logger.debug("setTvrStatus service completed with failed status");
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg("setTvrStatus service completed with failed status");
                genericResponse.setStatus(Constant.FAILED);
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.INTERNAL_SERVER_ERROR.name());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug("setTvrStatus service publishing activity logs");
        activityEventPublisher.publishEvent(activityLog);
        return response;
    }

    @Override
    public BaseResponse getTVRStatus(GetTvrStatusRequest getTvrStatusRequest, HttpHeaders httpHeaders,
                                     HttpServletRequest httpRequest) throws Exception {
        BaseResponse response;
        logger.debug("getTvrStatus service started");
        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository
                .getGoNoGoCustomerApplicationByRefId(getTvrStatusRequest.getReferenceId()
                        , getTvrStatusRequest.getHeader().getInstitutionId());

        if (goNoGoCroApplicationResponse != null) {
            TvrStatusResponse tvrStatusResponse = new TvrStatusResponse();
            tvrStatusResponse.setTvrDetails(goNoGoCroApplicationResponse.getTvrDetails());
            response = GngUtils.getBaseResponse(HttpStatus.OK, tvrStatusResponse);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse reassignApplication(ReassignApplicationRequest reassignApplicationRequest,
                                            HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(reassignApplicationRequest.getHeader(), reassignApplicationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.CRDT.toFaceValue(), EndPointReferrer.REASSIGN_APPLICATION,
                GNGWorkflowConstant.QUEUED.toFaceValue(), reassignApplicationRequest.getHeader().getLoggedInUserId());
        activityLogs.setAction("REASSIGN_APPLICATION");

        String refId = reassignApplicationRequest.getRefId();
        String institutionId = reassignApplicationRequest.getHeader().getInstitutionId();
        String assignedUser = reassignApplicationRequest.getUserName();
        String assignedRole = reassignApplicationRequest.getRole();
        String assignedBy = reassignApplicationRequest.getHeader().getLoggedInUserId();
        boolean isCredit = false;
        if(Institute.isInstitute(reassignApplicationRequest.getHeader().getInstitutionId(),Institute.SBFC)) {
            if(reassignApplicationRequest.getHeader().getCredit() != null) {
                isCredit = reassignApplicationRequest.getHeader().getCredit();
            }
        }

        String currentStage = applicationRepository.getApplicationStage(institutionId, refId);
        activityLogs.setStage(currentStage);

        AllocationInfo allocationInfo = new AllocationInfo();
        allocationInfo.setOperator(assignedUser);
        allocationInfo.setRole(assignedRole);
        allocationInfo.setLocked(true);
        allocationInfo.setAssignedBy(assignedBy);

        boolean status = applicationRepository.updateGoNoGoCustomerApplication(refId, allocationInfo, institutionId);

        if (status) {
            List<String> userIds = new ArrayList<>();
            userIds.add(assignedUser);
            appConfigurationHelper.intimateUsers(refId, IntimationConstants.CASE_RECOMMENDATION, userIds);

            boolean isStageChanged = false;
            String stageName = null;
            if(Roles.isCpaRole(assignedRole)){
                stageName = Stages.Stage.DDE.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.CREDIT.name(), assignedRole) || Roles.isCreditRole(assignedRole) || isCredit){
                stageName = Stages.Stage.CRDT.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.BOPS.name(), assignedRole)){
                stageName = Stages.Stage.BOPS.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.HOPS.name(), assignedRole)){
                stageName = Stages.Stage.HOPS.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.EXTERNAL_CREDIT.name(), assignedRole)){
                stageName = Stages.Stage.EXTERNAL_CRDT.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.EXTERNAL_OPS.name(), assignedRole)){
                stageName = Stages.Stage.EXTERNAL_OPS.name();
            }
            isStageChanged = applicationRepository.updateApplicationStage(refId, institutionId, stageName,null);
            activityLogs.setChangedStage(stageName);

                response = GngUtils.getBaseResponse(HttpStatus.OK, Constant.SUCCESS);
                StringBuilder activityMessage = new StringBuilder();
                activityMessage.append("Successfully reassigned " + refId + " refID to " + assignedUser + " role "
                        + assignedRole + " by " + assignedBy);
                if (isStageChanged) {
                    activityMessage.append("Also stage changed to " + stageName);
                }
                activityLogs.setCustomMsg(activityMessage.toString());
                logger.debug("Successfully reassigned {} refID to {} role {} by {}" + refId, assignedUser, assignedRole, assignedBy);
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.CONFLICT, Constant.ERROR);
                logger.debug("Unable to reassign {} refID." + refId);
            }
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                    Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLogs);
            return response;
    }

    @Override
    public BaseResponse resetAllocationInfo(ChangeAllocationInfoRequest reassignApplicationRequest,
                                            HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(reassignApplicationRequest.getHeader(), reassignApplicationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.CRDT.toFaceValue(), EndPointReferrer.RESET_ALLOCATION_INFO,
                GNGWorkflowConstant.QUEUED.toFaceValue(), reassignApplicationRequest.getHeader().getLoggedInUserId());
        activityLogs.setAction("RESET_ALLOCATION_INFO");

        String refId = reassignApplicationRequest.getRefId();
        String institutionId = reassignApplicationRequest.getHeader().getInstitutionId();
        String assignedUser = reassignApplicationRequest.getUserName();
        String assignedRole = reassignApplicationRequest.getRole();
        String assignedBy = reassignApplicationRequest.getHeader().getLoggedInUserId();
        boolean isSetNull = reassignApplicationRequest.isSetToNull();
        boolean isManualLock = reassignApplicationRequest.isManualLock();
        String stageName = null;
        boolean isCredit = false;
        if(Institute.isInstitute(reassignApplicationRequest.getHeader().getInstitutionId(),Institute.SBFC)) {
            if(reassignApplicationRequest.getHeader().getCredit() != null) {
                isCredit = reassignApplicationRequest.getHeader().getCredit();
            }
        }

        AllocationInfo allocationInfo;
        if (isSetNull) {
            allocationInfo = null;
            stageName = reassignApplicationRequest.getStageName();
        } else{
            allocationInfo = new AllocationInfo();
            allocationInfo.setOperator(assignedUser);
            allocationInfo.setRole(assignedRole);
            allocationInfo.setLocked(isManualLock);
            allocationInfo.setAssignedBy(assignedBy);
            if(Roles.isCpaRole(assignedRole)){
                stageName = Stages.Stage.DDE.name();
            }else if(StringUtils.equalsIgnoreCase(Roles.Role.CREDIT.name(), assignedRole) || Roles.isCreditRole(assignedRole) || isCredit){
                stageName = Stages.Stage.CRDT.name();
            }
        }

        String currentStage = applicationRepository.getApplicationStage(institutionId, refId);
        activityLogs.setStage(currentStage);
        boolean status = applicationRepository.updateApplicationForAllocationInfo(refId, allocationInfo, institutionId);

        if(status){
            boolean isStageChanged = false;
            if(!StringUtils.isEmpty(stageName)){
                isStageChanged = applicationRepository.updateApplicationStage(refId, institutionId, stageName,
                        GNGWorkflowConstant.QUEUED.toFaceValue());
                activityLogs.setChangedStage(stageName);
            }

            response = GngUtils.getBaseResponse(HttpStatus.OK, Constant.SUCCESS);
            StringBuilder activityMessage = new StringBuilder();
            activityMessage.append("Successfully reassigned " + refId + " refID to " + assignedUser + " role "
                    + assignedRole + " by " + assignedBy);
            if(isStageChanged){
                activityMessage.append("Also stage changed to " + stageName);
            }
            activityLogs.setCustomMsg(activityMessage.toString());
            logger.debug("Successfully reassigned {} refID to {} role {} by {}" + refId, assignedUser, assignedRole, assignedBy);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, Constant.ERROR);
            logger.debug("Unable to reassign {} refID." + refId);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public void updateDecision(CroApprovalRequest croApprovalRequest, HttpServletRequest httpRequest) throws Exception{
        boolean updateStatus = applicationRepository.updateOnHoldCroDecision(croApprovalRequest);
        if (updateStatus) {
            updateCamSummary(croApprovalRequest, httpRequest);
        }
    }

    @Override
    public BaseResponse fosAddRemark(CroApprovalRequest croApprovalRequest, HttpHeaders httpHeaders, HttpServletRequest httpRequest) throws Exception {
        logger.debug("fosAddRemark CroManagerImpl started");

        BaseResponse response= null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(croApprovalRequest.getHeader(),croApprovalRequest.getReferenceId(),httpRequest,null,EndPointReferrer.FOS_REMARK,"",croApprovalRequest.getHeader().getLoggedInUserId());
        GenericResponse genericResponse = new GenericResponse();

        if(Institute.isInstitute(croApprovalRequest.getHeader().getInstitutionId(),Institute.SBFC)){
            boolean updateStatus = applicationRepository.saveFosRemark(croApprovalRequest);
            if (updateStatus){
                populateActivityLog(activityLog, croApprovalRequest.getReferenceId(), null, null);
                activityLog.setAction(GNGWorkflowConstant.FOS_ADD_REMARK.toFaceValue());
                genericResponse.setStatus("OK UPDATE SUCCESSFULLY");
                response = GngUtils.getBaseResponse(HttpStatus.OK,genericResponse);
            }else{
                genericResponse.setStatus("FAILED TO UPDATE");
                response = GngUtils.getBaseResponse(HttpStatus.OK,genericResponse);
            }
        }else{
            genericResponse.setStatus("NOT A SBFC INSTITUTE");
            response = GngUtils.getBaseResponse(HttpStatus.OK,genericResponse);
        }

        if(CollectionUtils.isNotEmpty(croApprovalRequest.getCroJustification())){
            int justificationListSize = croApprovalRequest.getCroJustification().size();
            activityLog.setRemarks(croApprovalRequest.getCroJustification().get(justificationListSize-1).getRemark() + FieldSeparator.FORWARD_SLASH +
                    croApprovalRequest.getCroJustification().get(justificationListSize-1).getMasterRemark());
            activityLog.setCurrentUserId(croApprovalRequest.getCroJustification().get(justificationListSize-1).getSubjectTo());
            activityLog.setCurrentUserName(StringUtils.isNotBlank(activityLog.getCurrentUserId()) ?
                    Cache.getUserName(croApprovalRequest.getHeader().getInstitutionId(),activityLog.getCurrentUserId()) : null);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug("fosAddRemark service publishing activity logs");
        activityEventPublisher.publishEvent(activityLog);
        logger.debug("fosAddRemark CroManagerImpl Ended");
        return response;
    }


}

