package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.workflow.WorkFlowCommunicationRepo;
import com.softcell.dao.mongodb.repository.workflow.WorkflowJobCommunication;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by prateek on 13/2/17.
 */
@Service
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class WorkFlowCommunicationManagerImpl implements WorkFlowCommunicationManager {

    private static final Logger logger = LoggerFactory.getLogger(WorkFlowCommunicationManagerImpl.class);

    @Autowired
    private WorkflowJobCommunication workflowJobCommunication;

    public WorkFlowCommunicationManagerImpl(){
        if(null == workflowJobCommunication){
            workflowJobCommunication = new WorkFlowCommunicationRepo();
        }

    }

    @Override
    public BaseResponse getWfJobComm(String institutionId) throws SystemException {

        return GngUtils.getBaseResponse(HttpStatus.OK ,workflowJobCommunication.findByInstitutionId(institutionId));

    }

    @Override
    public WFJobCommDomain getWfCommDomainJobByType(String institutionId, String type) {
        return workflowJobCommunication.getMultibureauCommDomain(institutionId, type);
    }

    @Override
    public WFJobCommDomain getWfCommDomainJobByType(String institutionId, String product, String type) {
        return workflowJobCommunication.getMultibureauCommDomain(institutionId,product, type);
    }

    @Override
    public  WFJobCommDomain getWfCommDomainJob(final String institutionId , final String product, final String serviceId, final String type  ){
        return workflowJobCommunication.getMultibureauCommDomain(institutionId, product, serviceId, type);
    }

	@Override
    public List<WFJobCommDomain> getServices(String institutionId, String type, String product) {
        return workflowJobCommunication.getServices(institutionId, type, product);
    }
    
    @Override
    public WFJobCommDomain getWfCommDomainJobByMemberId(String institutionId, String product, String memberId, String type) {
        return workflowJobCommunication.getMultibureauCommDomainByMemberId(institutionId, product, memberId, type);
    }


    @Override
    public void saveWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException {
        workflowJobCommunication.saveByInstitutionId(wfJobCommDomain);
    }

    @Override
    public void deleteWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException {
        workflowJobCommunication.deleteWfJobComm(wfJobCommDomain);
    }

    @Override
    public void updateWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException {
        workflowJobCommunication.findAndUpdate(wfJobCommDomain);
    }

    @Override
    public WFJobCommDomain getConfigFromIsEnabledFlag(String institutionId, String type) {
        return workflowJobCommunication.getConfigFromIsEnabledFlag(institutionId, type);
    }


    @Override
    public WFJobCommDomain getConfigForProduct(String institutionId, String type, String product) {
        return workflowJobCommunication.getConfigForProduct(institutionId, type,product);
    }
}
