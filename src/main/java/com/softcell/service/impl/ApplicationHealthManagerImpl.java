package com.softcell.service.impl;

import com.softcell.config.*;
import com.softcell.dao.mongodb.repository.health.ApplicationHealthRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.multibureau.pickup.MbCommunicationDomain;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.service.ApplicationHealthManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;


@Service
public class ApplicationHealthManagerImpl implements ApplicationHealthManager {

    URLConfiguration urlConfiguration = Cache.URL_CONFIGURATION;

    @Autowired
    private ApplicationHealthRepository applicationHealthRepository;

    @Override
    public Object getDBStat() throws Exception {

        return applicationHealthRepository.getStats();

    }

    @Override
    public Object getCollectionNames() throws Exception {

        return applicationHealthRepository.getCollectionNames();

    }

    @Override
    public Object getServerStatus() throws Exception {

        return applicationHealthRepository.getServerStatusInfo();

    }

    @Override
    public Object gethostInfo() throws Exception {

        return applicationHealthRepository.getHostInfo();
    }

    @Override
    public Object getExternalServicesStatus(Header header) {
        String institutionID = header.getInstitutionId();
        Map<String, String> urlStatus = new HashMap<String, String>();
        urlStatus.put("MULTIBUREAU", getMbStatus(institutionID));
        urlStatus.put("PAN", getPanStatus(institutionID));
        urlStatus.put("AADHAR", getAadhaarStatus(institutionID));
        urlStatus.put("BRE", getBREStatus(institutionID));
        urlStatus.put("NTC", getNTCStatus(institutionID));
        urlStatus.put("CREDIT_CARD", getCreditCardStatus(institutionID));
        urlStatus.put("AUTHENTICATION", getAuthServiceStatus());
        urlStatus.put("OTP_ACL", getOtpServiceStatus(institutionID));
        urlStatus.put("EMAIL_ACL", getEmailServiceStatus(institutionID));
        urlStatus.put("RELIANCE_DIGITAL", getRelianceDigitalServiceStatus(institutionID));
        // EsConnectionConfig elasticService = urlConfiguration
        // .getEsConnectionConfig().get(institutionID);
        return urlStatus;
    }

    private String getRelianceDigitalServiceStatus(String institutionID) {
        RelianceConfiguration relianceDigitalService = urlConfiguration
                .getRelianceConfiguration().get(institutionID);
        if (null != relianceDigitalService
                && StringUtils.isNotBlank(relianceDigitalService.getBaseUrl())) {
            return isAvailable(relianceDigitalService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getEmailServiceStatus(String institutionID) {
        EmailServiceConfiguration emailService = urlConfiguration
                .getEmailServiceConfiguration().get(institutionID);
        if (null != emailService
                && StringUtils.isNotBlank(emailService.getBaseUrl())) {
            return isAvailable(emailService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getOtpServiceStatus(String institutionID) {
        OtpConfiguration otpService = urlConfiguration.getOtpConfiguration()
                .get(institutionID);
        if (null != otpService
                && StringUtils.isNotBlank(otpService.getBaseUrl())) {
            return isAvailable(otpService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getAuthServiceStatus() {
        AuthenticationConfiguration authService = urlConfiguration
                .getAuthenticationConfiguration().get("AUTHENTICATION");
        if (null != authService
                && StringUtils.isNotBlank(authService.getBaseUrl())) {
            return isAvailable(authService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getCreditCardStatus(String institutionID) {
        CreditCardSurrogateConfig creditCardService = urlConfiguration
                .getCreditCardSurrogateConfig().get(institutionID);
        if (null != creditCardService
                && StringUtils.isNotBlank(creditCardService.getBaseUrl())) {
            return isAvailable(creditCardService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getNTCStatus(String institutionID) {
        NTCConfiguration ntcService = urlConfiguration.getNtcConfiguration()
                .get(institutionID);
        if (null != ntcService
                && StringUtils.isNotBlank(ntcService.getBaseUrl())) {
            return isAvailable(ntcService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getBREStatus(String institutionID) {
        ScoringCommunication scoringService = urlConfiguration
                .getScoringCommunicationConfig().get(institutionID);
        if (null != scoringService
                && StringUtils.isNotBlank(scoringService.getBaseUrl())) {
            return isAvailable(scoringService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getAadhaarStatus(String institutionID) {
        AadharConfiguration aadharService = urlConfiguration
                .getAadharConfiguration().get(institutionID);
        if (null != aadharService
                && StringUtils.isNotBlank(aadharService.getBaseUrl())) {
            return isAvailable(aadharService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getPanStatus(String institutionID) {
        PanCommunicationDomain panService = urlConfiguration
                .getPanCommunicationDomainConfig().get(institutionID);
        if (null != panService
                && StringUtils.isNotBlank(panService.getBaseUrl())) {
            return isAvailable(panService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String getMbStatus(String institutionID) {
        MbCommunicationDomain mbService = urlConfiguration
                .getMbCommunicationDomainConfig().get(institutionID);
        if (null != mbService && StringUtils.isNotBlank(mbService.getBaseUrl())) {
            return isAvailable(mbService.getBaseUrl());
        } else {
            return "401";
        }
    }

    private String isAvailable(String url) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<?> responseEntity = restTemplate.getForEntity(url,
                    String.class);
            return responseEntity.getStatusCode().toString();
        } catch (Exception exception) {
            return "404";
        }

    }

}
