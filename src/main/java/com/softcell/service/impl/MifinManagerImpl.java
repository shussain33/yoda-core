package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.mifin.topup.Applicant;
import com.softcell.gonogo.model.mifin.topup.*;
import com.softcell.gonogo.model.mifin.topup.Applicant;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.TopUpInfo;
import com.softcell.gonogo.model.response.MifinDedupeResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.ExternalApiManager;
import com.softcell.service.MifinManager;
import com.softcell.service.utils.MiFinHelper;
import com.softcell.service.utils.MifinDedupeDetails;
import com.softcell.service.utils.OriginationHelper;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static com.softcell.service.utils.MiFinHelper.*;

@Service
public class MifinManagerImpl implements MifinManager
{
    private static final Logger logger = LoggerFactory.getLogger(MifinManagerImpl.class);

    @Autowired
    private ExternalApiManager externalApiManager;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private MiFinHelper miFinHelper;

    @Autowired
    private OriginationHelper originationHelper;

    private static final int DATA_COUNT = 7;

    private static final int PROSPECT_LIST_SIZE = 10;

    //1stapi mifin applicant details
    @Override
    public BaseResponse getTopupDedupeResponse(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) {
        TopUpDedupeResponse topUpDedupeResponse = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(topUpDedupeRequest.getRefId());
        BaseResponse baseResponse = null;

        if (goNoGoCustomerApplication.getApplicationRequest().getRequest().dedupeParamsChange) {
            try {
                ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                //send data to mifin
                topUpDedupeResponse = externalApiManager.sendDataToMIFINApplicantDetail(topUpDedupeRequest, topUpDedupeRequest.getRefId());
                if (topUpDedupeResponse != null) {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, topUpDedupeResponse);
                }
                applicationRequest.getRequest().dedupeParamsChange = false;
                boolean isUpdate = applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
            } catch (Exception ex) {
                logger.error("{}",ex.getStackTrace());

                Collection<Error> errors = new ArrayList<Error>();
                errors.add(Error.builder()
                        .message(ex.getMessage())

                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
            }
        } else {
            MiFinCallLog mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(topUpDedupeRequest.getRefId(), EXISTINGLOANDETAILS);
            if(mifinApiLog != null && CollectionUtils.isNotEmpty(mifinApiLog.getDedupeResponseList()) && mifinApiLog.getDedupeResponseList().get(0) != null){
                List<TopUpDedupeResponse> topUpDedupeResponsesList = (List<TopUpDedupeResponse>) mifinApiLog.getDedupeResponseList();
                if(CollectionUtils.isNotEmpty(topUpDedupeResponsesList)){
                    topUpDedupeResponsesList.get(0).setStatus("S");
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, topUpDedupeResponsesList.get(0));
                }
            }else{
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }

        }
        return baseResponse;
    }



    @Override
    public BaseResponse getMifinApplicantData(String refId, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;

        MiFinCallLog miFinCallLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(refId, EXISTINGLOANDETAILS);
        if (miFinCallLog != null && CollectionUtils.isNotEmpty(miFinCallLog.getDedupeResponseList())) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog.getDedupeResponseList().get(0));
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse getDedupeData(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {
        return getDedupe(applicationRequest, applicationRequest.getRequest().getApplicant(), applicationRequest.getRequest().getCoApplicant(),
                httpRequest);
    }

    @Override
    public BaseResponse getDedupe(ApplicationRequest applicationRequest, com.softcell.gonogo.model.core.Applicant applicant,
                                  List<CoApplicant> coApplicantList, HttpServletRequest httpRequest) throws Exception {
        BaseResponse baseResponse = null;
        logger.info("in goNoGoCustomerApplication provided request", applicationRequest.toString());
        List<TopUpDedupeResponse> topUpDedupeResponseList = null ;
        topUpDedupeResponseList = externalApiManager.sendDataToMifinDedupe(applicationRequest, applicant, coApplicantList);
        List<MiFinCallLog> miFinCallLog = externalAPILogRepository.fetchMiFinCallLogForDedupe(applicationRequest.getRefID(), MIFIN_DEDUPE_DETAIL);

        if (CollectionUtils.isNotEmpty(topUpDedupeResponseList)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }
    @Override
    public BaseResponse sendDatatoMifinDedupe(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) throws Exception {
        String refId = topUpDedupeRequest.getRefId();
        BaseResponse baseResponse = null;
        logger.info("MifinManager sendDatatoMifinDedupe() started for RefId : " + refId);

        ApplicationRequest requestedObject = topUpDedupeRequest.getApplicationRequest();
        String updatedUserId = topUpDedupeRequest.getUpdatedUserId();
        applicationRepository.updateGoNoGoCustomerApplication(requestedObject);
        if (StringUtils.isNotEmpty(updatedUserId)) {
            com.softcell.gonogo.model.core.Applicant applicant = null;
            List<CoApplicant> coApplicantList = null;
            if (requestedObject.getRequest() != null) {
                Request request = requestedObject.getRequest();
                if (StringUtils.equalsIgnoreCase(updatedUserId, request.getApplicant().getApplicantId())) {
                    applicant = request.getApplicant();
                } else {
                    coApplicantList = request.getCoApplicant().stream().filter(coApp ->
                            StringUtils.equalsIgnoreCase(updatedUserId, coApp.getApplicantId())).collect(Collectors.toList());
                }
            }
            baseResponse = getDedupe(requestedObject, applicant, coApplicantList, httpRequest);
        } else {
            logger.info("No DAT found Against provided request");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getMifinDedupeDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;
        List<MiFinCallLog>   miFinCallLog = new ArrayList<>();
        MiFinCallLog apiLog = new MiFinCallLog();

        try {
            miFinCallLog = externalAPILogRepository.fetchMiFinCallLogForDedupe(
                    topUpDedupeRequest.getRefId(), MIFIN_DEDUPE_DETAIL);
            GoNoGoCustomerApplication goNoGoCustomerApplication =  applicationRepository.getGoNoGoCustomerApplicationByRefId(topUpDedupeRequest.getRefId());
            if ( CollectionUtils.isNotEmpty(miFinCallLog)) {
                if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo() != null) {
                    if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo().apiHit){
                        miFinCallLog.get(0).apiHit = true;
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog);
                }else{
                    apiLog.apiHit = true;
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog);
                }

            }else if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo() != null){
                if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo().apiHit){
                    apiLog.apiHit = true;
                    miFinCallLog.add(apiLog);
                }else{
                    miFinCallLog.add(apiLog);
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,miFinCallLog);
            }else{
                /*miFinCallLog.add(apiLog);*/
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,miFinCallLog);
            }

            return baseResponse;
        } catch (Exception e) {
            logger.error("Exception During fetch mifin detail");
        }
        return baseResponse;
    }


    @Override
    public BaseResponse sendMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) throws Exception {
        TopUpDedupeResponse topUpDedupeResponse = null;
        BaseResponse baseResponse = null;
        List<String> prospectIds = new ArrayList<>();
        boolean isUpdate = false;
        Map<String, List<String>> custIdProspectCodesMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(topUpDedupeRequest.getDedupeResponseList()) &&
                CollectionUtils.isNotEmpty(topUpDedupeRequest.getDedupeResponseList().get(0).getDedupeInfo().getMatchApplicantList())) {
            List<MatchApplicantDetails> matchedAplicantdetailLis = topUpDedupeRequest.getDedupeResponseList().get(0).getDedupeInfo().getMatchApplicantList();
            prospectIds = fetchPropectCodeFromApplicantloanInfo(matchedAplicantdetailLis, prospectIds);
        }
        /*prospectIds = createMifinRequestForDataFetch(topUpDedupeRequest.getRefId(),
                topUpDedupeRequest.getCustomerIdList(), custIdProspectCodesMap);*/
        topUpDedupeRequest.setProspectCodeList(prospectIds);
        logger.info("topUpDedupeRequest :: ", topUpDedupeRequest);
        try {
            //send data to mifin
            List<TopUpDedupeResponse> topUpDedupeResponsesdbList = new ArrayList<TopUpDedupeResponse>();
            MiFinCallLog mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(topUpDedupeRequest.getRefId(), MIFIN_DEDUPE_DETAIL);
            if (mifinApiLog != null) {
                topUpDedupeResponsesdbList = topUpDedupeRequest.getDedupeResponseList();
                mifinApiLog.setDedupeResponseList(topUpDedupeResponsesdbList);
                isUpdate = applicationRepository.updateMifinCallLog(mifinApiLog, topUpDedupeRequest.getRefId(), MIFIN_DEDUPE_DETAIL);
            }
            logger.info("mifincalllog updated {}", isUpdate);
            List<TopUpDedupeResponse> topUpDedupeResponseList = externalApiManager.sendMiFINLoanDetail(topUpDedupeRequest);
            // Populate Data In Application
            if(CollectionUtils.isNotEmpty(topUpDedupeResponseList)){
                for (TopUpDedupeResponse response : topUpDedupeResponseList) {
                    if(CollectionUtils.isNotEmpty(response.getLoanSummaryDeatilList()) && StringUtils.equalsIgnoreCase(response.getStatus(),"S")){
                        changeFlag(mifinApiLog,topUpDedupeRequest.getRefId());
                        baseResponse = populateMifinDedupeData(response, topUpDedupeRequest.getRefId(),
                                topUpDedupeRequest.getHeader().getInstitutionId(),topUpDedupeRequest);
                    }/*else{
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Data is Not Present on  mifin side");
                }*/
                }
            }


        } catch (Exception ex) {
           logger.error("{}",ex.getStackTrace());
        }
        return baseResponse;
    }

    private void changeFlag(MiFinCallLog mifinApiLog, String refId) {
        mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(refId, EXISTINGLOANDETAILS);
        if(mifinApiLog != null && CollectionUtils.isNotEmpty(mifinApiLog.getDedupeResponseList())){
            if(CollectionUtils.isNotEmpty( mifinApiLog.getDedupeResponseList().get(0).getApplicantlist())){
                List<Applicant> applicantList =  mifinApiLog.getDedupeResponseList().get(0).getApplicantlist();
                for(Applicant applicant: applicantList){
                    if(CollectionUtils.isNotEmpty(applicant.getProspectList())){
                        List<ProspectDetails> prospectList = applicant.getProspectList();
                        for(ProspectDetails prospectDetail : prospectList){
                            if(prospectDetail.dedupeFlag){
                                prospectDetail.dedupeFlag = false;
                            }
                        }
                    }
                }
            }
        }
        applicationRepository.updateMifinCallLog(mifinApiLog, refId, EXISTINGLOANDETAILS, null);
    }

    private List<String> createMifinRequestForDataFetch(String refId, List<String> customerIdList,
                                                        Map<String, List<String>> custIdProspectCodesMap) throws Exception {
        List<String> prospect = new ArrayList<String>();
        MiFinCallLog miFinCallLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(refId, EXISTINGLOANDETAILS);
        if (miFinCallLog != null) {
            List<TopUpDedupeResponse> topUpDedupeResponseList = miFinCallLog.getDedupeResponseList();
            logger.info("getTopUpDedupeResponselist() from database {}", topUpDedupeResponseList);
            for (TopUpDedupeResponse topupdedupe : topUpDedupeResponseList) {
                if (CollectionUtils.isNotEmpty(topupdedupe.getApplicantlist())) {
                    List<Applicant> applicantList = topupdedupe.getApplicantlist();
                    for (Applicant applicant : applicantList) {
                        if (StringUtils.isNotBlank(applicant.getCustomerCode()) && CollectionUtils.isNotEmpty(applicant.getProspectList())
                                && customerIdList.contains(applicant.getCustomerCode())) {
                            List<ProspectDetails> propectList = applicant.getProspectList();
                            custIdProspectCodesMap.put(applicant.getCustomerCode(), new ArrayList<>());
                            for (ProspectDetails prospectDetail : propectList) {
                                if (prospectDetail.getProspectCode() != null && prospect.size() < PROSPECT_LIST_SIZE) {
                                    prospect.add(prospectDetail.getProspectCode());
                                    custIdProspectCodesMap.get(applicant.getCustomerCode()).add(prospectDetail.getProspectCode());
                                }
                            }
                        }
                    }
                }
            }
        }
        return prospect;
    }

    @Override
    public BaseResponse getMiFinLoanDetail(String refId, HttpServletRequest httpRequest) throws Exception {
        TopUpDedupeResponse topUpDedupeResponse = null;
        BaseResponse baseResponse = null;
        MiFinCallLog miFinCallLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(refId, MIFINLOANDETAIL);
        if (miFinCallLog != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog.getMiFinResponse());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }



    public BaseResponse getMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) throws ParseException {
        BaseResponse baseResponse = null;
        topUpDedupeRequest = createTopUpDedupeRequest(topUpDedupeRequest);
        double totalPos = calculateTotalPos(topUpDedupeRequest);
        List<TopUpDedupeResponse> topUpDedupeResponsedbList = new ArrayList<TopUpDedupeResponse>();
        TopUpDedupeResponse topUpDedupeResponsedb = new TopUpDedupeResponse();
        try {
            List<TopUpDedupeResponse> topUpDedupeResponseList = externalApiManager.sendMiFINLoanDetail(topUpDedupeRequest);
            MiFinCallLog mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(topUpDedupeRequest.getRefId(), EXISTINGLOANDETAILS);
            if (mifinApiLog != null) {
                List<Applicant> applicantList = topUpDedupeRequest.getApplicantList();
                if (CollectionUtils.isNotEmpty(applicantList)) {
                    topUpDedupeResponsedb.setApplicantlist(applicantList);
                    if(CollectionUtils.isNotEmpty(mifinApiLog.getDedupeResponseList()))
                        topUpDedupeResponsedb.setTotalExposure(mifinApiLog.getDedupeResponseList().get(0).getTotalExposure());
                    topUpDedupeResponsedbList.add(topUpDedupeResponsedb);
                }
                mifinApiLog.setDedupeResponseList(topUpDedupeResponsedbList);
                boolean isUpdate = applicationRepository.updateMifinCallLog(mifinApiLog, topUpDedupeRequest.getRefId(), EXISTINGLOANDETAILS);
                logger.info("mifincalllog updated {}", isUpdate);
            }
            // Populate Data In Application
            if(CollectionUtils.isNotEmpty(topUpDedupeResponseList)){
                for (TopUpDedupeResponse response : topUpDedupeResponseList) {
                    if(CollectionUtils.isNotEmpty(response.getLoanSummaryDeatilList()) && StringUtils.equalsIgnoreCase("S",response.getStatus())){
                        if(CollectionUtils.isNotEmpty(mifinApiLog.getDedupeResponseList())){
                            response.setTotalExposure(mifinApiLog.getDedupeResponseList().get(0).getTotalExposure());
                            response.setTotalValueToBeConsider(Math.round(totalPos));
                            baseResponse = populateMifinDedupeData(response, topUpDedupeRequest.getRefId(),
                                    topUpDedupeRequest.getHeader().getInstitutionId(),topUpDedupeRequest);
                        }
                    }
                }
            }

            logger.info("Response of populateMifinDedupeDetails:{}", baseResponse);
        } catch (Exception ex) {
           logger.error("while calling nififn 3rd APi {} " ,ex.getStackTrace());
        }
        return baseResponse;
    }

    private double calculateTotalPos(TopUpDedupeRequest topUpDedupeRequest) {
        double pos =0 ;
        try{
            if (CollectionUtils.isNotEmpty(topUpDedupeRequest.getApplicantList())) {
                logger.info("in pos calculation");
                List<Applicant> applicantList = topUpDedupeRequest.getApplicantList();
                miFinHelper.sortDisbursalDate(topUpDedupeRequest.getRefId(), applicantList);
                for (Applicant applicant : applicantList) {
                    if (CollectionUtils.isNotEmpty(applicant.getProspectList())){
                        for (ProspectDetails prospectDetails : applicant.getProspectList()) {
                            if (prospectDetails.valueToBeConsider) {
                                pos = pos + Double.parseDouble(GngUtils.setValueForZero(prospectDetails.getProncipalOutstanding()));
                            }
                        }
                    }
                }
            }
        }catch (Exception ex){
            logger.error("{} in calculation of pos value", ExceptionUtils.getStackTrace(ex));
        }
        return pos;
    }

    private TopUpDedupeRequest createTopUpDedupeRequest(TopUpDedupeRequest topUpDedupeRequest) {
        List<String> prospectCodeList = new ArrayList<String>();

        if (CollectionUtils.isNotEmpty(topUpDedupeRequest.getApplicantList())) {
            List<Applicant> applicantList = topUpDedupeRequest.getApplicantList();
            miFinHelper.sortDisbursalDate(topUpDedupeRequest.getRefId(), applicantList);
            for (Applicant applicant : applicantList) {
                if (CollectionUtils.isNotEmpty(applicant.getProspectList()) &&
                        prospectCodeList.size() < PROSPECT_LIST_SIZE) {
                    prospectCodeList = addProspectCode(applicant.getProspectList(), prospectCodeList);

                }
            }
        }
        topUpDedupeRequest.setProspectCodeList(prospectCodeList);
        return topUpDedupeRequest;
    }

    private List<String> addProspectCode(List<ProspectDetails> prospectList, List<String> prospectCodeList) {
        if (CollectionUtils.isNotEmpty(prospectList)) {
            for (ProspectDetails prospectDetails : prospectList) {
                if (prospectDetails.dedupeFlag) {
                    prospectCodeList.add(prospectDetails.getProspectCode());
                }
            }
        }
        return prospectCodeList;
    }


    public BaseResponse populateMifinDedupeData(TopUpDedupeResponse topUpDedupeResponse, String refId, String institutionId,TopUpDedupeRequest topUpDedupeRequest) throws ParseException {
        MifinDedupeResponse mifinDedupeResponse = new MifinDedupeResponse();
        BaseResponse baseResponse = null;
        int pageCount = 0;
        try {
            ApplicationRequest applicationRequest = applicationRepository.getApplicantData(refId, institutionId);
            MifinDedupeDetails mifinDedupeDetails = new MifinDedupeDetails();
            List<String> successRecords = new ArrayList<String>();
            // Fetch all collection to which populate data from dedupe response
            miFinHelper.fetchAllObjectsForRefId(refId, institutionId, mifinDedupeDetails, applicationRequest);
            // Fill dedupe details to those collections
            mifinDedupeDetails = miFinHelper.fillDedupeData(topUpDedupeResponse, applicationRequest, mifinDedupeDetails,
                    successRecords,topUpDedupeRequest);

            if (mifinDedupeDetails != null) {
                if (mifinDedupeDetails.getApplicationRequest() != null) {
                    try {
                        applicationRepository.updateGoNoGoCustomerApplication(mifinDedupeDetails.getApplicationRequest());
                        cahngeApiHitFlag(mifinDedupeDetails.getApplicationRequest());
                        pageCount++;
                    } catch (Exception e) {
                       logger.error("application request {} " ,e.getStackTrace());
                    }
                }
                /*if (mifinDedupeDetails.getLoanCharges() != null) {
                    try {
                        applicationRepository.saveLoanChargesDetails(refId, institutionId, mifinDedupeDetails.getLoanCharges());
                        pageCount++;

                    } catch (Exception e) {
                        logger.error("{}",e.getStackTrace());
                    }
                }*/
                if (mifinDedupeDetails.getCamDetails() != null) {

                    try {
                        applicationRepository.saveCamSummaryDetails(mifinDedupeDetails.getApplicationRequest(), mifinDedupeDetails.getCamDetails());
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("while populating camSummary data" , e.getStackTrace());
                    }
                }
               /* if (mifinDedupeDetails.getEligibility() != null) {
                    try {
                        applicationRepository.saveEligibilityDetails(mifinDedupeDetails.getEligibility());
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("{}",e.getStackTrace());
                    }

                }*/
                if (mifinDedupeDetails.getListOfDocsRequest() != null) {
                    try {
                        applicationRepository.saveListOfDocs(mifinDedupeDetails.getListOfDocsRequest());
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("while populating document data {} ", e.getStackTrace() );
                    }
                }
                if (mifinDedupeDetails.getRepayment() != null) {
                    try {
                        applicationRepository.saveRepaymentDetails(mifinDedupeDetails.getRepayment());
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("while populating repaymentData {} ", e.getStackTrace());
                    }
                }
                if (mifinDedupeDetails.getValuation() != null) {
                    try {
                        applicationRepository.saveValuationDetails(mifinDedupeDetails.getValuation(), mifinDedupeDetails.getApplicationRequest().getRefID(),
                                mifinDedupeDetails.getApplicationRequest().getHeader().getInstitutionId());
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("while populating Valuation Data {}" ,e.getStackTrace() );
                    }
                }
                if (mifinDedupeDetails.getDisbursementMemo() != null) {

                    try {
                        DMRequest dmRequest = new DMRequest();
                        dmRequest.setDisbursementMemo(mifinDedupeDetails.getDisbursementMemo());
                        dmRequest.setRefId(refId);
                        dmRequest.setHeader(applicationRequest.getHeader());
                        applicationRepository.saveDMDetails(dmRequest);
                        pageCount++;
                    } catch (Exception e) {
                        logger.error("while saving DM details {} ", e.getStackTrace());
                    }
                }
                   /* if (mifinDedupeDetails.getVerificationDetails() != null){
                        try {
                            applicationRepository.saveVerificationDetails(mifinDedupeDetails.getVerificationDetails());
                        }  catch (Exception e) {
                            logger.error("{}",e.getStackTrace());
                        }
                    }*/
                logger.info("No of pages saved:{}", pageCount);
                MiFinCallLog mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(refId, EXISTINGLOANDETAILS);
                if (mifinApiLog != null) {
                    List<TopUpDedupeResponse> topUpDedupeResponseList = (List<TopUpDedupeResponse>) mifinApiLog.getDedupeResponseList();
                    if (CollectionUtils.isNotEmpty(topUpDedupeResponseList)) {
                        topUpDedupeResponseList.get(0).setStatus("S");
                        mifinDedupeResponse.setApplicantList(topUpDedupeResponseList.get(0).getApplicantlist());
                    }
                    if (mifinDedupeDetails.getLegalVerification() != null) {
                        LegalVerificationRequest legalVerificationRequest = new LegalVerificationRequest();
                        try {
                            legalVerificationRequest.setLegalVerification(mifinDedupeDetails.getLegalVerification());
                            applicationRepository.saveLegalVerificationDetailsForTopUp(legalVerificationRequest, mifinDedupeDetails.getApplicationRequest().getRefID(),
                                    mifinDedupeDetails.getApplicationRequest().getHeader().getInstitutionId());
                            pageCount++;
                        } catch (Exception e) {
                           logger.error("while populating legalVerification {} ", e.getStackTrace());
                        }
                    }
                }
                String message = null;
                if (pageCount == DATA_COUNT) {
                    message = "Successfully Updated Pages : " + successRecords;
                    mifinDedupeResponse.setMessage(message);
                    mifinDedupeResponse.setApplicationRequest(mifinDedupeDetails.getApplicationRequest());
                } else {
                    message = "Partial Updated Pages : " + successRecords;
                    mifinDedupeResponse.setMessage(message);
                    mifinDedupeResponse.setApplicationRequest(mifinDedupeDetails.getApplicationRequest());
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, mifinDedupeResponse);
            } else {
                String errorMsg = "No Response from mifin";
                Collection<Error> errors = new ArrayList<Error>();
                errors.add(Error.builder().errorCode(errorMsg).build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, errors);

            }


        } catch (Exception e) {
            logger.error("while populating data {}" , e.getStackTrace());
        }
        return baseResponse;

    }

    private void cahngeApiHitFlag( ApplicationRequest applicationRequest) {

        try {
            if(applicationRequest.getRequest().getTopupInfo() != null){
                applicationRequest.getRequest().getTopupInfo().apiHit = true;
            }else{
                TopUpInfo topUpInfo = new TopUpInfo();
                topUpInfo.apiHit = true;
                applicationRequest.getRequest().setTopupInfo(topUpInfo);
            }
            applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
    }

    private List<String> createMifinRequestForLoanDetail(List<MatchApplicantDetails> matchedAplicantdetailList, List<String> prospectIds) {
        for (MatchApplicantDetails matchedApplicant : matchedAplicantdetailList) {
            if (matchedApplicant.selected && prospectIds.size() < 11) {
                if (matchedApplicant.getPropectList() != null) {
                    if (CollectionUtils.isNotEmpty(matchedApplicant.getPropectList().getPropectCodes())) {
                        List<PropectCodes> prospectCodeList = matchedApplicant.getPropectList().getPropectCodes();
                        for (PropectCodes propectCodes : prospectCodeList) {
                            if (StringUtils.isNotEmpty(propectCodes.getProspectCode())) {
                                prospectIds.add(propectCodes.getProspectCode());
                            }
                        }
                    }
                }

            }

        }
        return prospectIds;
    }

    private List<String> fetchPropectCodeFromApplicantloanInfo(List<MatchApplicantDetails> matchedAplicantdetailList, List<String> prospectIds) {
        for (MatchApplicantDetails matchedApplicant : matchedAplicantdetailList) {
            if (matchedApplicant.selected ) {
                if (matchedApplicant.getApplicantLoanInfoList() != null) {
                    if (CollectionUtils.isNotEmpty(matchedApplicant.getApplicantLoanInfoList())) {
                        List<ApplicantLoanInfo> applicantLoanInfoList = matchedApplicant.getApplicantLoanInfoList();
                        for (ApplicantLoanInfo applicantLoanInfo : applicantLoanInfoList) {
                            if (StringUtils.isNotEmpty(applicantLoanInfo.getProspectCode()) && prospectIds.size() < PROSPECT_LIST_SIZE) {
                                prospectIds.add(applicantLoanInfo.getProspectCode());
                            }
                        }
                    }
                }
            }
        }
        return prospectIds;
    }

    @Override
    public BaseResponse getMifinLmsDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse baseResponse = null;
        boolean isGross = false;
        List<String> propsectList = new ArrayList<>();
        double existingAmt = 0;
        double principalOutStanding = 0;
        double  interestDueOutStanding = 0;
        double totalPrincipalOs = 0;
        double totalInteresetOs = 0;
        double otherCharges = 0;
        /*if(StringUtils.equalsIgnoreCase(topUpDedupeRequest.getLoanDetailTypeValue(), TOPUP_GROSS)){
            isGross = true;
        }*/
        MiFinCallLog mifinApiLog = externalAPILogRepository.fetchMiFinCallLogByRequestType(topUpDedupeRequest.getRefId(), MIFINLOANDETAIL);
        if(CollectionUtils.isNotEmpty(mifinApiLog.getDedupeResponseList())){
             List<TopUpDedupeResponse> dedupeResponseList = mifinApiLog.getDedupeResponseList();
            if(CollectionUtils.isNotEmpty(dedupeResponseList.get(0).getLoanSummaryDeatilList())) {
                     List<LoanSummaryDetail> loanSummaryDeatilList = dedupeResponseList.get(0).getLoanSummaryDeatilList();
                     for (LoanSummaryDetail loanSummaryDetail : loanSummaryDeatilList) {
                         propsectList.add(loanSummaryDetail.getPROSPECTCODE());
                     }
            }
        }
        topUpDedupeRequest.setProspectCodeList(propsectList);
      topUpDedupeRequest.setLoanDetailType("B");

        List<TopUpDedupeResponse> topUpDedupeResponseList = externalApiManager.sendMiFINLoanDetail(topUpDedupeRequest);

            LmsDetail lmsDetail = null;
            if(CollectionUtils.isNotEmpty(topUpDedupeResponseList)){
                TopUpDedupeResponse  topUpDedupeResponse =  topUpDedupeResponseList.get(0);
                if(CollectionUtils.isNotEmpty(topUpDedupeResponse.getLmsDetailsList())){
                    List<LmsDetail> lmsDetailsList = (List<LmsDetail>)topUpDedupeResponse.getLmsDetailsList();
                    for(Object lmsDetails : lmsDetailsList){
                        lmsDetail = JsonUtil.MapToObject((Map<String, Object>)  lmsDetails, LmsDetail.class);
                         principalOutStanding = Double.parseDouble(GngUtils.setValueForZero(lmsDetail.getPrincipalOutstanding()));
                         interestDueOutStanding = Double.parseDouble(GngUtils.setValueForZero(lmsDetail.getInterestdueOutstanding()));
                         otherCharges = Double.parseDouble(GngUtils.setValueForZero(lmsDetail.getOtherCharges()));
                        existingAmt = principalOutStanding + interestDueOutStanding+existingAmt + otherCharges ;
                        totalPrincipalOs = principalOutStanding + totalPrincipalOs ;
                        totalInteresetOs = interestDueOutStanding + totalInteresetOs ;
                        logger.info("{} existingAmt {}" , topUpDedupeRequest.getRefId(),existingAmt);
                    }
                }
                existingAmt = Math.round(existingAmt);
            LoanChargesRequest loanChargesRequest = new LoanChargesRequest();
            loanChargesRequest.setRefId(topUpDedupeRequest.getRefId());
            loanChargesRequest.setHeader(topUpDedupeRequest.getHeader());
            LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetails(loanChargesRequest);
                DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(topUpDedupeRequest.getRefId()
                        , topUpDedupeRequest.getHeader().getInstitutionId());
            if(loanCharges == null){
                loanCharges = new LoanCharges();
                TenureDetails tenureDetails =  new TenureDetails();
                tenureDetails.setRemarks(new Remark());
                InterestDetails interestDetails = new InterestDetails();
                interestDetails.setRemarks(new Remark());
                loanCharges.setInterestDetails(interestDetails);
                loanCharges.setTenureDetails(tenureDetails);

                loanCharges.setRefId(topUpDedupeRequest.getRefId());
                loanCharges.setInstitutionId(topUpDedupeRequest.getHeader().getInstitutionId());
                LoanAmtDetails loanAmtDetails = new LoanAmtDetails();
                loanAmtDetails.setExistingLoanAmt(existingAmt);
                loanAmtDetails.setRemarks(new Remark());
                loanAmtDetails.setPrincipalOsAmt(Math.round(totalPrincipalOs));
                loanAmtDetails.setInterestOsAmt(Math.round(totalInteresetOs));
                loanCharges.setLoanDetails(loanAmtDetails);

            }else if(loanCharges.getLoanDetails() != null){
                LoanAmtDetails loanAmtDetails = loanCharges.getLoanDetails();
                loanAmtDetails.setExistingLoanAmt(existingAmt);
                loanAmtDetails.setPrincipalOsAmt(Math.round(totalPrincipalOs));
                loanCharges.setLoanDetails(loanAmtDetails);
            }
            applicationRepository.saveLoanChargesDetails(topUpDedupeRequest.getRefId(), topUpDedupeRequest.getHeader().getInstitutionId(),  loanCharges);
        }
        if (CollectionUtils.isNotEmpty(topUpDedupeResponseList)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, topUpDedupeResponseList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
}



    @Override
    public BaseResponse sendEmiCalRequest(AdminLogRequest adminLogRequest, HttpServletRequest httpRequest){
        BaseResponse baseResponse = null;
        //EmiCalResponse emiCalResponse = sendEmiCalRequest(adminLogRequest.getRefID(), adminLogRequest.getHeader());
        originationHelper.populateMisReleatedData(adminLogRequest.getRefID(), adminLogRequest.getHeader().getInstitutionId());
        /*if (emiCalResponse != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emiCalResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }*/

        return baseResponse;
    }


}
