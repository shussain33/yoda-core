package com.softcell.service.impl;

import com.softcell.constants.ApplicantType;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceData;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.InsuranceManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ssg0268 on 9/7/19.
 */

@Service
public class InsuranceManagerImpl implements InsuranceManager {

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    private static final Logger logger = LoggerFactory.getLogger(InsuranceManagerImpl.class);

    @Override
    public BaseResponse saveInsuranceData(InsuranceRequest insuranceRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(insuranceRequest, httpRequest,
                insuranceRequest.getHeader().getLoggedInUserId());
        activityEventPublisher.publishEvent(activityLogs);
        try{
            if(insuranceRequest.getInsuranceData() != null){
                applicationRepository.saveInsuranceData(insuranceRequest);
                applicationRepository.updateCompletedInfo(insuranceRequest.getRefId(), insuranceRequest.getHeader().getInstitutionId(),
                        EndPointReferrer.SAVE_INSURANCE_DATA, insuranceRequest.getHeader().getLoggedInUserId(), insuranceRequest.getHeader().getLoggedInUserRole());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insuranceRequest.getInsuranceData());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getInsuranceData(InsuranceRequest insuranceRequest, HttpServletRequest httpRequest){
        BaseResponse baseResponse = null;
        String refId = insuranceRequest.getRefId();
        String institutionId = insuranceRequest.getHeader().getInstitutionId();
        InsuranceData calculatePremiumAmountRequestdb = null;
        List<Applicant> applicantList = new ArrayList<Applicant>();
        InsurancePolicy insurance = new InsurancePolicy();
        List<InsurancePolicy> insuranceList = new ArrayList<>();
        //insuranceList.add(insurance);
        Integer amount = 0;
        String tenor = null;
        try {
            ApplicationRequest applicationRequest = applicationRepository.getApplicationRequest(refId,
                    institutionId);
            amount = (((Double) applicationRequest.getRequest().getApplication().getLoanAmount()).intValue());
            tenor = applicationRequest.getRequest().getApplication().getTenorRequested();
            calculatePremiumAmountRequestdb = applicationRepository.fetchInsuranceData(refId);
            if (calculatePremiumAmountRequestdb != null && CollectionUtils.isNotEmpty(calculatePremiumAmountRequestdb.getApplicantList())) {
                if(insuranceRequest.getInsuranceData() == null ){
                    insuranceRequest.setInsuranceData(calculatePremiumAmountRequestdb);
                    insuranceRequest.getInsuranceData().setChangedLoanAmount(String.valueOf(amount));
                    insuranceRequest.getInsuranceData().setChangedTenorAmount(String.valueOf(tenor));
                }
                if(CollectionUtils.isEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
                    insuranceRequest.getInsuranceData().setApplicantList(calculatePremiumAmountRequestdb.getApplicantList());
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insuranceRequest.getInsuranceData());
            }else {
                if(insuranceRequest.getInsuranceData() == null){
                    insuranceRequest.setInsuranceData(new InsuranceData());
                }

                if(CollectionUtils.isEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
                    if (applicationRequest != null && applicationRequest.getRequest() != null) {
                        if (applicationRequest.getRequest().getApplicant() != null &&
                                ApplicantType.isIndividual(applicationRequest.getRequest().getApplicant().getApplicantType())) {
                            applicationRequest.getRequest().getApplicant().setInsurancePolicyList(insuranceList);
                            applicantList.add(applicationRequest.getRequest().getApplicant());
                        }
                        if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
                            for (CoApplicant caApp : applicationRequest.getRequest().getCoApplicant()) {
                                if(ApplicantType.isIndividual(caApp.getApplicantType())){
                                    caApp.setInsurancePolicyList(insuranceList);
                                    applicantList.add(caApp);
                                }
                            }
                        }
                    }
                    insuranceRequest.getInsuranceData().setApplicantList(applicantList);
                }
                for(Applicant applicant :  insuranceRequest.getInsuranceData().getApplicantList()){
                    for(InsurancePolicy insurancepolicy : applicant.getInsurancePolicyList()){
                        if(!insurancepolicy.isException()){
                            tenor = applicationRequest.getRequest().getApplication().getTenorRequested();
                            insurancepolicy.setTenorRequested(tenor);
                            amount = (((Double) applicationRequest.getRequest().getApplication().getLoanAmount()).intValue());
                            EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(refId, institutionId);
                            if (eligibility != null && eligibility.getAprvLoan() != 0) {
                                amount = (((Double) eligibility.getAprvLoan()).intValue());
                                tenor = String.valueOf(eligibility.getTenor());
                            }
                            insurancepolicy.setProposedAmount(Integer.valueOf(amount).toString());
                            insurancepolicy.setTenorRequested(tenor);
                        }
                    }
                    applicant.setInsurancePolicyList(applicant.getInsurancePolicyList());
                }
            }

            insuranceRequest.getInsuranceData().setChangedLoanAmount(String.valueOf(amount));
            insuranceRequest.getInsuranceData().setChangedTenorAmount(tenor);
            applicationRepository.saveInsuranceData(insuranceRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, insuranceRequest.getInsuranceData());
        }catch (Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

}
