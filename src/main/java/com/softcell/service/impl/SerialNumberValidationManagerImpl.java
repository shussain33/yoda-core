package com.softcell.service.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ApplicationStageRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.*;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleRequest;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleResponse;
import com.softcell.gonogo.serialnumbervalidation.apple.RollbackRequestServiceDetails;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioRequest;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeRequest;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeResponse;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleRequest;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleResponse;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexRequest;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexResponse;
import com.softcell.gonogo.serialnumbervalidation.kent.KentRequest;
import com.softcell.gonogo.serialnumbervalidation.kent.KentResponse;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaRequest;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoRequest;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoResponse;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicRequest;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicResponse;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungRequest;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.RollBackResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyRequest;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyResponse;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconRequest;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconResponse;
import com.softcell.gonogo.service.factory.SerialNumberValidationBuilder;
import com.softcell.gonogo.service.factory.serialnumbervalidation.CarrierSerialNumberValidationBuilder;
import com.softcell.gonogo.service.factory.serialnumbervalidation.ComioSerialNumberValidationBuilder;
import com.softcell.gonogo.service.factory.serialnumbervalidation.NokiaSerialNumberValidationBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.SerialNumberValidationManager;
import com.softcell.service.serialnumbervalidation.CarrierSerialNumberValidationManager;
import com.softcell.service.serialnumbervalidation.ComioSerialNumberValidationManager;
import com.softcell.service.serialnumbervalidation.NokiaSerialNumberValidationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.SerialNumberValidationEngine;
import com.softcell.service.validator.serialnumbervalidation.CarrierSerialNumberValidationEngine;
import com.softcell.service.validator.serialnumbervalidation.ComioSerialNumberValidationEngine;
import com.softcell.service.validator.serialnumbervalidation.NokiaSerialNumberValidationEngine;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mahesh
 */
@Service
public class SerialNumberValidationManagerImpl implements SerialNumberValidationManager {

    private static final Logger logger = LoggerFactory.getLogger(SerialNumberValidationManagerImpl.class);

    @Autowired
    private ApplicationStageRepository applicationStagerepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private SerialNumberValidationBuilder serialNumberValidationBuilder;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private SerialNumberValidationEngine serialNumberValidationEngine;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private ComioSerialNumberValidationEngine comioSerialNumberValidationEngine;

    @Autowired
    private ComioSerialNumberValidationBuilder comioSerialNumberValidationBuilder;

    @Autowired
    private ComioSerialNumberValidationManager comioSerialNumberValidationManager;

    @Autowired
    private NokiaSerialNumberValidationEngine nokiaSerialNumberValidationEngine;

    @Autowired
    private NokiaSerialNumberValidationBuilder nokiaSerialNumberValidationBuilder;

    @Autowired
    private NokiaSerialNumberValidationManager nokiaSerialNumberValidationManager;

    @Autowired
    private CarrierSerialNumberValidationManager carrierSerialNumberValidationManager;

    @Autowired
    private CarrierSerialNumberValidationEngine carrierSerialNumberValidationEngine;

    @Autowired
    private CarrierSerialNumberValidationBuilder carrierSerialNumberValidationBuilder;

    @Override
    public BaseResponse serialSaleConfirmation(
            SerialSaleConfirmationRequest serialsaleconfirmationrequest) throws Exception {

        BaseResponse baseResponse = null;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();

        serialNumberInfoBuilder.institutionId(serialsaleconfirmationrequest.getHeader().getInstitutionId());
        serialNumberInfoBuilder.product(serialsaleconfirmationrequest.getHeader().getProduct());
        serialNumberInfoBuilder.refID(serialsaleconfirmationrequest.getReferenceID());
        serialNumberInfoBuilder.vendor(serialsaleconfirmationrequest.getVendor());
        serialNumberInfoBuilder.serialNumber(serialsaleconfirmationrequest.getSerialNumber());
        serialNumberInfoBuilder.imeiNumber(serialsaleconfirmationrequest.getImeiNumber());
        serialNumberInfoBuilder.skuCode(serialsaleconfirmationrequest.getSkuCode());
        serialNumberInfoBuilder.dealerId(serialsaleconfirmationrequest.getHeader().getDealerId());
        serialNumberInfoBuilder.channelCode(serialsaleconfirmationrequest.getChannelCode());

        if (Product.DPL == serialsaleconfirmationrequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialsaleconfirmationrequest.getSerialNumber())
                    && StringUtils.isBlank(serialsaleconfirmationrequest.getImeiNumber())) {
                baseResponse = goForSamsungSerialNumberValidation(serialsaleconfirmationrequest, serialNumberInfoBuilder);
            } else if (StringUtils.isNotBlank(serialsaleconfirmationrequest.getImeiNumber())
                    && StringUtils.isBlank(serialsaleconfirmationrequest.getSerialNumber())) {
                baseResponse = goForSamsungImeiValidation(serialsaleconfirmationrequest, serialNumberInfoBuilder);
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else if (Product.CDL == serialsaleconfirmationrequest.getHeader().getProduct()) {
            baseResponse = goForSamsungSerialNumberValidation(serialsaleconfirmationrequest, serialNumberInfoBuilder);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }
        return baseResponse;
    }

    private BaseResponse goForSamsungImeiValidation(SerialSaleConfirmationRequest serialsaleconfirmationrequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {
        BaseResponse baseResponse;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialsaleconfirmationrequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForSamsungImie(serialsaleconfirmationrequest);

        if (errors.isEmpty()) {
            WFJobCommDomain samsungConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialsaleconfirmationrequest.getHeader().getInstitutionId(), UrlType.SAMSUNG_IMEI.toValue());

            if (null != samsungConfig) {
                SamsungRequest samsungRequest = serialNumberValidationBuilder.buildSamsungImeiRequest(serialsaleconfirmationrequest, samsungConfig);
                SamsungResponse samsungResponse = callToSamsung(samsungRequest, samsungConfig);

                if (null != samsungResponse && null == samsungResponse.getError()) {

                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildSamsungImeiResponse(samsungResponse);
                    serialNumberInfoBuilder.originalResponse(samsungResponse.getOriginalResponse());
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        serialNumberInfoBuilder.status(Status.VALID.name());

                        // *  update application stage
                        applicationStagerepository
                                .updateApplicationStage(serialsaleconfirmationrequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        logger.debug("Saving serial number info for {}", UrlType.SAMSUNG_IMEI.name());
                    } else {
                        serialNumberInfoBuilder.status(Status.INVALID.name());
                        activityLog.setCustomMsg("Invalid number");
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                } else {
                    logger.warn("get error from Connector api for {}", UrlType.SAMSUNG_IMEI.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(samsungResponse.getError().getType())
                            .message(samsungResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(samsungResponse.getError().getMessage());
                }
            } else {
                logger.warn("Configuration not found for {}", UrlType.SAMSUNG_IMEI.name());
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
            }
        } else {
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        //  save samsung imei number information to the database.
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private BaseResponse goForSamsungSerialNumberValidation(SerialSaleConfirmationRequest serialsaleconfirmationrequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {
        BaseResponse baseResponse = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialsaleconfirmationrequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForSamsung(serialsaleconfirmationrequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialsaleconfirmationrequest.getVendor());
        if (errors.isEmpty()) {
            WFJobCommDomain samsungConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialsaleconfirmationrequest.getHeader().getInstitutionId(), UrlType.SAMSUNG.toValue());

            if (null != samsungConfig) {
                SamsungRequest samsungRequest = serialNumberValidationBuilder.buildSamsungRequest(serialsaleconfirmationrequest, samsungConfig);
                SamsungResponse samsungResponse = callToSamsung(samsungRequest, samsungConfig);
                if (null != samsungResponse && null == samsungResponse.getError()) {
                    String originalResponse = samsungResponse.getOriginalResponse();
                    serialNumberInfoBuilder.originalResponse(originalResponse);

                    String[] output = originalResponse.split(FieldSeparator.FORWARD_SLASH);
                    String status = output[0];
                    String responseMsg = output[1];

                    if (StringUtils.equals("false", status)) {

                        serialNumberResponseBuilder.status(Status.INVALID.name());
                        serialNumberResponseBuilder.message(responseMsg);
                        serialNumberInfoBuilder.status(Status.INVALID.name());

                    } else if (StringUtils.equals("true", status)) {
                        serialNumberResponseBuilder.status(Status.VALID.name());
                        serialNumberResponseBuilder.message(responseMsg);
                        serialNumberInfoBuilder.status(Status.VALID.name());

                        // *  update application stage
                        applicationStagerepository
                                .updateApplicationStage(serialsaleconfirmationrequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponseBuilder.build());
                } else {
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(samsungResponse.getError().getType())
                            .message(samsungResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());
                    activityLog.setCustomMsg(samsungResponse.getError().getMessage());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }
                //  save samsung serial number information into the database.
                externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());
            } else {
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
            }
        } else {
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        //  save samsung serial number information into the database.
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }


    @Override
    public BaseResponse checkSerialNumberFlag(
            SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest) {

        SerialNumberApplicableVendorLog.SerialNumberApplicableVendorLogBuilder logBuilder = SerialNumberApplicableVendorLog.builder();

        logBuilder.institutionId(serialNumberApplicableCheckRequest.getHeader().getInstitutionId());
        logBuilder.referenceId(serialNumberApplicableCheckRequest.getRefId());
        logBuilder.dealerId(serialNumberApplicableCheckRequest.getHeader().getDealerId());
        logBuilder.product(serialNumberApplicableCheckRequest.getHeader().getProduct().name());
        logBuilder.dateTime(new Date());

        PostIpaRequest postIpaRequest = externalAPILogRepository
                .checkSerialNumberValidationFlag(serialNumberApplicableCheckRequest);
        String vendor;
        String make;
        BaseResponse baseResponse;


        if (null != postIpaRequest && null != postIpaRequest.getPostIPA()) {
            vendor = GngUtils.getManufaturer(postIpaRequest.getPostIPA().getAssetDetails());
            make = GngUtils.getMake(postIpaRequest.getPostIPA().getAssetDetails());
            logBuilder.vendor(vendor);
        } else {
            logBuilder.status(HttpStatus.NO_CONTENT.name());
            logBuilder.customMsg(ErrorCode.NO_DATA_FOUND);
            externalAPILogRepository.saveSerialNumberApplicableVendorLog(logBuilder.build());
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        Set<String> applicableVendors = lookupService.getApplicableVendors(serialNumberApplicableCheckRequest.getHeader().getInstitutionId(),
                serialNumberApplicableCheckRequest.getHeader().getProduct().toProductId());

        if (Product.DPL == serialNumberApplicableCheckRequest.getHeader().getProduct()) {

            boolean isDealerSkipApplicable = applicationRepository.isSerialOrImeiValidationSkipApplicableToDealer(serialNumberApplicableCheckRequest.getHeader().getInstitutionId(),
                    serialNumberApplicableCheckRequest.getHeader().getDealerId(),
                    serialNumberApplicableCheckRequest.getHeader().getProduct().name());

            baseResponse = getSerialNumberIsApplicableResponse(isDealerSkipApplicable, applicableVendors, vendor, make);

            logBuilder.dealerSkipApplicable(isDealerSkipApplicable);

        } else if (Product.CDL == serialNumberApplicableCheckRequest.getHeader().getProduct()) {

            String product = "CD";

            boolean isDealerSkipApplicable = applicationRepository.isSerialOrImeiValidationSkipApplicableToDealer(serialNumberApplicableCheckRequest.getHeader().getInstitutionId(),
                    serialNumberApplicableCheckRequest.getHeader().getDealerId(),
                    product);
            baseResponse = getSerialNumberIsApplicableResponse(isDealerSkipApplicable, applicableVendors, vendor, make);

            logBuilder.dealerSkipApplicable(isDealerSkipApplicable);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

            logBuilder.status(HttpStatus.UNPROCESSABLE_ENTITY.name());
            logBuilder.customMsg(ErrorCode.INVALID_PRODUCT);
        }
        externalAPILogRepository.saveSerialNumberApplicableVendorLog(logBuilder.build());
        return baseResponse;

    }

    private BaseResponse getSerialNumberIsApplicableResponse(boolean isDealerSkipApplicable, Set<String> applicableVendors, String vendor, String make) {

        BaseResponse baseResponse;

        if (isDealerSkipApplicable) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, SerialNumberResponse.builder()
                    .status(Constant.FAILED)
                    .message(ResponseConstants.NOT_APPLICABLE_VANDOR)
                    .vendor(vendor).build());
        } else {
            if (null != applicableVendors && applicableVendors.contains(vendor)
                    && !StringUtils.contains(make, GNGWorkflowConstant.VANILLA.toFaceValue())) {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, SerialNumberResponse.builder()
                        .status(Constant.SUCCESS)
                        .message(ResponseConstants.APPLICABLE_VANDOR)
                        .vendor(vendor).build());
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, SerialNumberResponse.builder()
                        .status(Constant.FAILED)
                        .message(ResponseConstants.NOT_APPLICABLE_VANDOR)
                        .vendor(vendor).build());
            }
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getUpdatedSRNumberDetails(String referenceId) {

        BaseResponse baseResponse;
        SerialNumberInfo serialNumberInfo = externalAPILogRepository.viewUpdatedSRNumberDetails(referenceId);

        if (serialNumberInfo != null) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberInfo);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;

    }

    @Override
    public BaseResponse getSerialNumberDetails(String referenceId) {

        BaseResponse baseResponse;
        SerialNumberInfo serialNumberInfo = externalAPILogRepository.getSerialNumberDetails(referenceId);

        if (serialNumberInfo != null) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberInfo);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;

    }


    @Override
    public BaseResponse validateSony(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.materialName(serialSaleConfirmationRequest.getMaterialName());
        builder.productCode(serialSaleConfirmationRequest.getProductCode());
        builder.storeCode(serialSaleConfirmationRequest.getStoreCode());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());

        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
                baseResponse = goForSonySerialNumberValidation(serialSaleConfirmationRequest, builder);
            } else if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForSonyImeiValidation(serialSaleConfirmationRequest, builder);
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            baseResponse = goForSonySerialNumberValidation(serialSaleConfirmationRequest, builder);
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        SerialNumberInfo serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);


        return baseResponse;
    }

    private BaseResponse goForSonyImeiValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder builder) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForMobile(serialSaleConfirmationRequest);
        BaseResponse baseResponse;
        if (errors.isEmpty()) {

            WFJobCommDomain sonyMobConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.SONY_MOBILE.toValue());

            if (null != sonyMobConfig) {
                SonyRequest sonyRequest = serialNumberValidationBuilder.buildSonyMobileRequest(serialSaleConfirmationRequest, sonyMobConfig);

                SonyResponse sonyResponse = callToSony(sonyMobConfig, sonyRequest);

                if (null != sonyResponse && null == sonyResponse.getError()) {
                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildSonyResponse(sonyResponse);

                    builder.originalResponse(sonyResponse.getComments());
                    builder.status(serialNumberResponse.getStatus());
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    builder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(sonyResponse.getError().getType())
                            .message(sonyResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());
                    activityLog.setCustomMsg(sonyResponse.getError().getMessage());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }
            } else {
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
        } else {
            builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;

    }

    private BaseResponse goForSonySerialNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder builder) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForCAV(serialSaleConfirmationRequest);
        BaseResponse baseResponse;
        if (errors.isEmpty()) {

            WFJobCommDomain sonyCavConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.SONY_CAV.toValue());
            if (null != sonyCavConfig) {
                SonyRequest sonyRequest = serialNumberValidationBuilder.buildSonyCAVRequest(serialSaleConfirmationRequest, sonyCavConfig);
                SonyResponse sonyResponse = callToSony(sonyCavConfig, sonyRequest);
                if (null != sonyResponse && null == sonyResponse.getError()) {
                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildSonyResponse(sonyResponse);

                    builder.originalResponse(sonyResponse.getComments());
                    builder.status(serialNumberResponse.getStatus());

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    builder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(sonyResponse.getError().getType())
                            .message(sonyResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());
                    activityLog.setCustomMsg(sonyResponse.getError().getMessage());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }
            } else {
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse validateIntex(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {
        BaseResponse baseResponse;
        SerialNumberResponse serialNumberResponse;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set ststus to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.materialCode(serialSaleConfirmationRequest.getMaterialCode());

        Collection<Error> errors = serialNumberValidationEngine.validationForIntex(serialSaleConfirmationRequest);
        if (errors.isEmpty()) {
            WFJobCommDomain intexConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.INTEX_SERIAL_NUMBER.toValue());

            if (null != intexConfig) {
                IntexRequest intexRequest = serialNumberValidationBuilder.buildIntexSerialNumberRequest(serialSaleConfirmationRequest, intexConfig);
                IntexResponse intexResponse = callToIntex(intexConfig, intexRequest);
                if (null != intexResponse && null == intexResponse.getError()) {
                    serialNumberResponse = serialNumberValidationBuilder.buildIntexResponse(intexResponse);

                    builder.originalResponse(String.valueOf(intexResponse.getResponseCode()));
                    builder.status(serialNumberResponse.getStatus());

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    builder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(intexResponse.getError().getType())
                            .message(intexResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(intexResponse.getError().getMessage());
                }
            } else {
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
            }

            serialNumberInfo = builder.build();


            if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {
                // update application stage to SRNV
                applicationStagerepository
                        .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
            }
        } else {
            builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        /**
         * saving logs
         */
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse validatePanasonic(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode());


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
                baseResponse = goForPanasonicSerialNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForPanasonicImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = GngUtils.getInvalidSerialOrIMEIErrorList();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {

            baseResponse = goForPanasonicSerialNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);

        } else {
            errors = GngUtils.getInvalidProductErrorList();

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse validateGioneeImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {
        BaseResponse baseResponse;
        Collection<Error> errors;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.skuCode(serialSaleConfirmationRequest.getSkuCode());
        builder.storeCode(serialSaleConfirmationRequest.getStoreCode());

        WFJobCommDomain gioneeConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.GIONEE_IMEI.toValue());

        if (null == gioneeConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = serialNumberValidationEngine.validationForGioneeImei(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {
                GioneeRequest gioneeRequest = serialNumberValidationBuilder.buildGioneeImeiRequest(serialSaleConfirmationRequest, gioneeConfig);
                GioneeResponse gioneeResponse = callToGionee(gioneeConfig, gioneeRequest);

                if (null != gioneeResponse && null == gioneeResponse.getError()) {

                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildGioneeImeiResponse(gioneeResponse);
                    builder.status(serialNumberResponse.getStatus());
                    builder.originalResponse(serialNumberResponse.getStatus() + ":" + serialNumberResponse.getMessage());
                    serialNumberInfo = builder.build();

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {

                        // update application stage to IMEIV
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    errors = new ArrayList<>();
                    String message = gioneeResponse.getError().getType() + " : " + gioneeResponse.getError().getMessage();
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(message);
                }
            } else {
                builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        /**
         * Saving logs
         */
        serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse validateApple(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse;
        Collection<Error> errors;

        SerialNumberResponse serialNumberResponse = null;

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.bankName(serialSaleConfirmationRequest.getBankName());
        builder.mpn(serialSaleConfirmationRequest.getMpn());
        builder.tenure(serialSaleConfirmationRequest.getTenure());
        builder.storeAppleId(serialSaleConfirmationRequest.getStoreAppleId());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.scheme(serialSaleConfirmationRequest.getScheme());

        WFJobCommDomain appleConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.APPLE.toValue());

        if (null == appleConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {

            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
                baseResponse = goForAppleSerialNumberValidation(serialSaleConfirmationRequest, appleConfig, serialNumberResponse, builder);
            } else if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForAppleImeiValidation(serialSaleConfirmationRequest, appleConfig, serialNumberResponse, builder);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }

        } else if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            baseResponse = goForAppleSerialNumberValidation(serialSaleConfirmationRequest, appleConfig, serialNumberResponse, builder);
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }

        /**
         * Saving serial Number details in database.
         */
        externalAPILogRepository.saveSerialNumberInfo(builder.build());
        return baseResponse;

    }

    private WFJobCommDomain getWFJobCommDomain(RollbackRequest rollbackRequest, String urlType) {
        return workFlowCommunicationManager.getWfCommDomainJobByType(
                rollbackRequest.getHeader().getInstitutionId(), urlType);

    }

    @Override
    public BaseResponse doRollback(RollbackRequest rollbackRequest) throws Exception {

        Collection<Error> errors;
        BaseResponse baseResponse = null;

        if (rollbackRequest.getVendor().equalsIgnoreCase(GNGWorkflowConstant.APPLE.toFaceValue())) {

            WFJobCommDomain appleConfig = getWFJobCommDomain(rollbackRequest, UrlType.APPLE_ROLLBACK.toValue());
            if (null == appleConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }

            AppleRequest appleRequest;
            if (StringUtils.isNotBlank(rollbackRequest.getImeiNumber()) && StringUtils.isBlank(rollbackRequest.getSerialNumber())) {
                errors = serialNumberValidationEngine.validationForImeiAppleRollback(rollbackRequest);

                if (errors.isEmpty()) {
                    appleRequest = serialNumberValidationBuilder.buildAppleImeiRollbackRequest(rollbackRequest, appleConfig);
                    baseResponse = goForAppleIMEIorSerialRollback(rollbackRequest, appleRequest, appleConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
            else if (StringUtils.isNotBlank(rollbackRequest.getSerialNumber()) && StringUtils.isBlank(rollbackRequest.getImeiNumber())) {
                errors = serialNumberValidationEngine.validationForSerialNumberAppleRollback(rollbackRequest);

                if (errors.isEmpty()) {
                    appleRequest = serialNumberValidationBuilder.buildAppleSerialNumberRollbackRequest(rollbackRequest, appleConfig);
                    baseResponse = goForAppleIMEIorSerialRollback(rollbackRequest, appleRequest, appleConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
            else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else if (StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.VIDEOCON.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.KENTSTAR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.KELVINATOR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.SANSUI.toFaceValue())) {

            WFJobCommDomain videoconConfig = getWFJobCommDomain(rollbackRequest, UrlType.VIDEOCON_ROLLBACK.toValue());
            if (null == videoconConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = serialNumberValidationEngine.validationForSerialVideoconRollback(rollbackRequest);
                if (errors.isEmpty()) {
                    VideoconRequest videoconRequest = serialNumberValidationBuilder.buildVideoconSerialNumberRollbackRequest(rollbackRequest);
                    videoconRequest.setFType(Constant.RB);
                    baseResponse = goForVideoconRollback(rollbackRequest, videoconRequest, videoconConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else if (StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.KENT.toFaceValue())) {

            WFJobCommDomain kentConfig = getWFJobCommDomain(rollbackRequest, UrlType.KENT.toValue());
            if (null == kentConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = serialNumberValidationEngine.validationForKentRollback(rollbackRequest);
                if (errors.isEmpty()) {
                    KentRequest kentRequest = serialNumberValidationBuilder.buildKentSerialRollbackRequest(rollbackRequest, kentConfig);

                    baseResponse = callToKentRollback(rollbackRequest, kentRequest, kentConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else if (StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.GOOGLE.toFaceValue())) {

            WFJobCommDomain googleConfig = getWFJobCommDomain(rollbackRequest, UrlType.GOOGLE_IMEI_ROLLBACK.toValue());
            if (null == googleConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = serialNumberValidationEngine.validationForGoogleRollback(rollbackRequest);
                if (errors.isEmpty()) {
                GoogleRequest googleRequest = serialNumberValidationBuilder.buildGoogleImeiRollbackRequest(rollbackRequest);

                    baseResponse = callToGoogleRollback(rollbackRequest, googleRequest, googleConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else if (StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.COMIO.toFaceValue())) {

            WFJobCommDomain comioConfig = getWFJobCommDomain(rollbackRequest, UrlType.COMIO_IMEI_ROLLBACK.toValue());

            if (null == comioConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = comioSerialNumberValidationEngine.validationForComioRollback(rollbackRequest);
                if (CollectionUtils.isEmpty(errors)) {
                    ComioRequest comioRequest = comioSerialNumberValidationBuilder.buildComioImeiRollbackRequest(rollbackRequest,comioConfig);

                    baseResponse = comioSerialNumberValidationManager.callToComioRollback(rollbackRequest, comioRequest, comioConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else if (StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(), Vendor.NOKIA.toFaceValue())) {

            WFJobCommDomain nokiaConfig = getWFJobCommDomain(rollbackRequest, UrlType.NOKIA_IMEI_ROLLBACK.toValue());
            if (null == nokiaConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = nokiaSerialNumberValidationEngine.validationForNokiaImeiRollback(rollbackRequest);
                if (errors.isEmpty()) {
                    NokiaRequest nokiaRequest = nokiaSerialNumberValidationBuilder.buildNokiaImeiRollbackRequest(rollbackRequest);

                    baseResponse = nokiaSerialNumberValidationManager.getNokiaRollback(rollbackRequest, nokiaRequest, nokiaConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        }else if(StringUtils.equalsIgnoreCase(rollbackRequest.getVendor(),Vendor.CARRIER.toFaceValue())) {

            WFJobCommDomain carrierConfig = getWFJobCommDomain(rollbackRequest, UrlType.CARRIER_SERIAL_ROLLBACK.toValue());
            if (null == carrierConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = carrierSerialNumberValidationEngine.validationForCarrierRollback(rollbackRequest);
                if (errors.isEmpty()) {

                    CarrierRequest carrierRequest = carrierSerialNumberValidationBuilder.buildCarrierSerialRollbackRequest(rollbackRequest, carrierConfig);

                    baseResponse = carrierSerialNumberValidationManager.goForCarrierRollback(rollbackRequest, carrierRequest, carrierConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VENDOR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getRollbackServiceRequestDetails(GetRollbackDetailsRequest getRollbackDetailsRequest) {
        BaseResponse baseResponse;
        SerialNumberInfo serialNumberInfo = externalAPILogRepository.getValidatedImeiOrSerialNumber(getRollbackDetailsRequest);

        if (serialNumberInfo != null && StringUtils.equalsIgnoreCase(GNGWorkflowConstant.APPLE.toFaceValue(), serialNumberInfo.getVendor())) {

            RollbackRequestServiceDetails rollbackRequestServiceDetails = serialNumberValidationBuilder.buildAppleRollbackPreRequisiteServiceResponse(serialNumberInfo);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, rollbackRequestServiceDetails);

        } else if (serialNumberInfo != null) {
            if (StringUtils.equalsIgnoreCase(Vendor.VIDEOCON.toFaceValue(), serialNumberInfo.getVendor()) ||
                    StringUtils.equalsIgnoreCase(Vendor.KENTSTAR.toFaceValue(), serialNumberInfo.getVendor()) ||
                    StringUtils.equalsIgnoreCase(Vendor.KELVINATOR.toFaceValue(), serialNumberInfo.getVendor()) ||
                    StringUtils.equalsIgnoreCase(Vendor.SANSUI.toFaceValue(), serialNumberInfo.getVendor())) {

                RollbackRequestServiceDetails rollbackRequestServiceDetails = serialNumberValidationBuilder.buildAppleRollbackPreRequisiteServiceResponse(serialNumberInfo);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, rollbackRequestServiceDetails);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else if (serialNumberInfo != null && StringUtils.equalsIgnoreCase(Vendor.KENT.toFaceValue(), serialNumberInfo.getVendor())) {

            RollbackRequestServiceDetails rollbackRequestServiceDetails = serialNumberValidationBuilder.buildAppleRollbackPreRequisiteServiceResponse(serialNumberInfo);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, rollbackRequestServiceDetails);
        } else if (serialNumberInfo != null && StringUtils.equalsIgnoreCase(Vendor.GOOGLE.toFaceValue(), serialNumberInfo.getVendor())) {

            RollbackRequestServiceDetails rollbackRequestServiceDetails = serialNumberValidationBuilder.buildAppleRollbackPreRequisiteServiceResponse(serialNumberInfo);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, rollbackRequestServiceDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    private BaseResponse goForAppleIMEIorSerialRollback(RollbackRequest rollbackRequest, AppleRequest appleRequest, WFJobCommDomain appleConfig) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());


        if(StringUtils.isNotBlank(rollbackRequest.getSerialNumber()) && Product.DPL == rollbackRequest.getHeader().getProduct()){

            activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
            activityLog.setAction(GNGWorkflowConstant.SR_NR_ROLLBACK.toFaceValue());

        }else if (StringUtils.isNotBlank(rollbackRequest.getImeiNumber())){
            activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
            activityLog.setAction(ActionName.IMEI_ROLLBACK.toString());

        }else {
            activityLog.setStage(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
            activityLog.setAction(GNGWorkflowConstant.SR_NR_ROLLBACK.toFaceValue());
        }

        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        RollBackResponse rollBackResponse = callToAppleRollback(appleRequest, appleConfig);
        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.imeiNumber(rollbackRequest.getImeiNumber());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());


        if (null != rollBackResponse && null == rollBackResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildAppleRollbackResponse(rollBackResponse);
            if (rollBackResponse.isSaleRollback()) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

                /**
                 * update serial-number valid to rollback,because we rollback this serial number or imei number.
                 */
                externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());

                if(rollbackRequest.isResetStage()){

                    if (StringUtils.isNotBlank(rollbackRequest.getImeiNumber()) || StringUtils.isNotBlank(rollbackRequest.getSerialNumber()) && Product.DPL == rollbackRequest.getHeader().getProduct()) {
                        applicationStagerepository
                                .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.APRV.toFaceValue());

                    }else {
                        applicationStagerepository
                                .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
                    }

                    activityLog.setStatus(Status.SUCCESS.toString());
                }


            } else {
                rollbackFeatureLogbuilder.status(Status.FAILED.name());
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != rollBackResponse ? rollBackResponse.getError().getMessage() : null)
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            activityLog.setCustomMsg(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));
        }
        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;
    }


    private BaseResponse goForAppleSerialNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig, SerialNumberResponse serialNumberResponse, SerialNumberInfo.Builder builder) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForAppleCD(serialSaleConfirmationRequest);
        BaseResponse baseResponse;
        if (errors.isEmpty()) {
            AppleRequest appleRequest = serialNumberValidationBuilder.buildAppleCDRequest(serialSaleConfirmationRequest, appleConfig);
            AppleResponse appleResponse = callToApple(appleRequest, appleConfig);
            if (null != appleResponse) {

                serialNumberResponse = serialNumberValidationBuilder.buildAppleResponse(appleResponse);

                builder.originalResponse(serialNumberResponse.getMessage());
                builder.status(serialNumberResponse.getStatus());
                builder.distributorId(null != appleResponse.getRegisteredResponse() ? appleResponse.getRegisteredResponse().getDistributorId() : null);

                if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                    applicationStagerepository
                            .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                    activityLog.setStatus(Status.SUCCESS.toString());
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
            } else {
                errors.add(Error.builder()
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .errorCode(Error.SEVERITY.CRITICAL.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setCustomMsg(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private BaseResponse goForAppleImeiValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig, SerialNumberResponse serialNumberResponse, SerialNumberInfo.Builder builder) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        Collection<Error> errors = serialNumberValidationEngine.validationForAppleDPL(serialSaleConfirmationRequest);
        BaseResponse baseResponse;

        if (errors.isEmpty()) {
            //Fetch postIPA for creating custom scheme for Apple IMEI validation.
            PostIPA postIPA = applicationRepository.getPostIPA(serialSaleConfirmationRequest.getReferenceID(),
                    serialSaleConfirmationRequest.getHeader().getInstitutionId());
            //Set scheme for apple like  "MaxTenure" | "MinTenure"
            if (null != postIPA) {
                serialSaleConfirmationRequest.setScheme(GngUtils.getSchemeForAppleIMEI(postIPA));
                logger.info("Custom scheme for Apple is {}", serialSaleConfirmationRequest.getScheme());
            }

            AppleRequest appleRequest = serialNumberValidationBuilder.buildAppleDPLRequest(serialSaleConfirmationRequest, appleConfig);
            AppleResponse appleResponse = callToApple(appleRequest, appleConfig);
            if (null != appleResponse) {

                serialNumberResponse = serialNumberValidationBuilder.buildAppleResponse(appleResponse);
                builder.originalResponse(serialNumberResponse.getMessage());
                builder.status(serialNumberResponse.getStatus());
                builder.distributorId(null != appleResponse.getRegisteredResponse() ? appleResponse.getRegisteredResponse().getDistributorId() : null);
                if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                    applicationStagerepository
                            .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                    activityLog.setStatus(Status.SUCCESS.toString());
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
            } else {
                errors.add(Error.builder()
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .errorCode(Error.SEVERITY.CRITICAL.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setCustomMsg(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()));
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private GioneeResponse callToGionee(WFJobCommDomain gioneeConfig, GioneeRequest gioneeRequest) throws Exception {
        String url = Arrays.asList(gioneeConfig.getBaseUrl(), gioneeConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (GioneeResponse) TransportUtils.postJsonRequest(gioneeRequest, url, GioneeResponse.class);

    }

    private VideoconResponse callToVideocon(WFJobCommDomain videoconConfig, VideoconRequest videoconRequest) throws Exception {
        String url = Arrays.asList(videoconConfig.getBaseUrl(), videoconConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        return (VideoconResponse) TransportUtils.postJsonRequest(videoconRequest, url, VideoconResponse.class);

    }

    private SonyResponse callToSony(WFJobCommDomain sonyConfig, SonyRequest sonyRequest)
            throws Exception {
        String url = Arrays.asList(sonyConfig.getBaseUrl(), sonyConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (SonyResponse) TransportUtils.postJsonRequest(sonyRequest, url, SonyResponse.class);

    }

    private IntexResponse callToIntex(WFJobCommDomain intexConfig, IntexRequest intexRequest) throws Exception {
        String url = Arrays.asList(intexConfig.getBaseUrl(), intexConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (IntexResponse) TransportUtils.postJsonRequest(intexRequest, url, IntexResponse.class);

    }

    private PanasonicResponse callToPanasonic(WFJobCommDomain panasonicConfig, PanasonicRequest panasonicRequest) throws Exception {
        String url = Arrays.asList(panasonicConfig.getBaseUrl(), panasonicConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (PanasonicResponse) TransportUtils.postJsonRequest(panasonicRequest, url, PanasonicResponse.class);

    }

    private AppleResponse callToApple(AppleRequest appleRequest, WFJobCommDomain appleConfig) throws Exception {
        String url = Arrays.asList(appleConfig.getBaseUrl(), appleConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (AppleResponse) TransportUtils.postJsonRequest(appleRequest, url, AppleResponse.class);
    }

    private RollBackResponse callToAppleRollback(AppleRequest appleRequest, WFJobCommDomain appleConfig) throws Exception {

        String url = Arrays.asList(appleConfig.getBaseUrl(), appleConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (RollBackResponse) TransportUtils.postJsonRequest(appleRequest, url, RollBackResponse.class);

    }

    private RollBackResponse callToVideoconRollback(VideoconRequest videoconRequest, WFJobCommDomain videoconConfig) throws Exception {

        String url = Arrays.asList(videoconConfig.getBaseUrl(), videoconConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (RollBackResponse) TransportUtils.postJsonRequest(videoconRequest, url, RollBackResponse.class);

    }

    private DisbursementResponse callToVideoconDisbursement(DisbursementRequest disbursementRequest, WFJobCommDomain videoconConfig) throws Exception {

        String url = Arrays.asList(videoconConfig.getBaseUrl(), videoconConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (DisbursementResponse) TransportUtils.postJsonRequest(disbursementRequest, url, DisbursementResponse.class);

    }


    private SamsungResponse callToSamsung(SamsungRequest samsungRequest, WFJobCommDomain samsungConfig) throws Exception {
        String url = Arrays.asList(samsungConfig.getBaseUrl(), samsungConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (SamsungResponse) TransportUtils.postJsonRequest(samsungRequest, url, SamsungResponse.class);

    }


    @Override
    public BaseResponse validateVideoconSansuiKelvinatorKentstar(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(serialSaleConfirmationRequest.getVendor());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        serialNumberInfoBuilder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        serialNumberInfoBuilder.refID(serialSaleConfirmationRequest.getReferenceID());
        serialNumberInfoBuilder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        serialNumberInfoBuilder.vendor(serialSaleConfirmationRequest.getVendor());
        serialNumberInfoBuilder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        serialNumberInfoBuilder.skuCode(serialSaleConfirmationRequest.getSkuCode());

        Collection<Error> errors = serialNumberValidationEngine.validationForVideocon(serialSaleConfirmationRequest);
        BaseResponse baseResponse;
        if (errors.isEmpty()) {

            WFJobCommDomain videoconConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.VIDEOCON_SERIAL_NUMBER.toValue());

            if (null != videoconConfig) {
                VideoconRequest videoconRequest = serialNumberValidationBuilder.buildVideoconRequest(serialSaleConfirmationRequest);
                VideoconResponse videoconResponse = callToVideocon(videoconConfig, videoconRequest);
                if (null != videoconResponse && null == videoconResponse.getError()) {
                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildVideoconResponse(videoconResponse);

                    serialNumberInfoBuilder.originalResponse(String.valueOf(videoconResponse.getResponseCode()));
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    errors.add(Error.builder()
                            .id(videoconResponse.getError().getType())
                            .message(videoconResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());
                    activityLog.setCustomMsg(videoconResponse.getError().getMessage());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }
            } else {
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
        } else {
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }
        //  save videocon serial number information to the database.
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private BaseResponse goForVideoconRollback(RollbackRequest rollbackRequest, VideoconRequest videoconRequest, WFJobCommDomain videoconConfig) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_ROLLBACK.toFaceValue());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        RollBackResponse rollBackResponse = callToVideoconRollback(videoconRequest, videoconConfig);
        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.serialNumber(videoconRequest.getSerialNumber());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != rollBackResponse && null == rollBackResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildVideoconSerialRollbackResponse(rollBackResponse);

            if (rollbackRequest.isResetStage() && StringUtils.equalsIgnoreCase(Status.VALID.name(),serialNumberResponse.getStatus())) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

            /**
             * update serial-number valid to rollback,because we rollback this serial number.
             */
            externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());

                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {

                rollbackFeatureLogbuilder.status(Status.FAILED.name());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            activityLog.setCustomMsg(null != rollBackResponse ? rollBackResponse.getError().getMessage() : null);
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != rollBackResponse ? rollBackResponse.getError().getMessage() : null)
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse doDisbursement(DisbursementRequest disbursementRequest) throws Exception {

        Collection<Error> errors;
        BaseResponse baseResponse = null;
        String vendor = disbursementRequest.getVendor();
        if (StringUtils.equalsIgnoreCase(vendor, Vendor.VIDEOCON.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(vendor, Vendor.KENTSTAR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(vendor, Vendor.KELVINATOR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(vendor, Vendor.SANSUI.toFaceValue())) {

            WFJobCommDomain videoconConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    disbursementRequest.getHeader().getInstitutionId(), UrlType.VIDEOCON_ROLLBACK.toValue());

            if (null == videoconConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = serialNumberValidationEngine.validationForSerialVideoconDisbursement(disbursementRequest);
                if (errors.isEmpty()) {
                    VideoconRequest videoconRequest = serialNumberValidationBuilder.buildVideoconSerialNumberDisbursementRequest(disbursementRequest);
                    videoconRequest.setFType(Constant.DM);
                    baseResponse = goForVideoconDisbursement(disbursementRequest, videoconRequest, videoconConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else if (StringUtils.equalsIgnoreCase(vendor, Vendor.KENT.toFaceValue())) {

            WFJobCommDomain kentConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    disbursementRequest.getHeader().getInstitutionId(), UrlType.KENT.toValue());

            if (null == kentConfig) {
                errors = GngUtils.getConfigurationNotFoundErrorList();
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            } else {
                errors = serialNumberValidationEngine.validationForKentDisburse(disbursementRequest);
                if (errors.isEmpty()) {
                    KentRequest kentRequest = serialNumberValidationBuilder.buildKentSerialDisburseRequest(disbursementRequest, kentConfig);

                    baseResponse = goForKentDisbursement(disbursementRequest, kentRequest, kentConfig);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                }
            }
        } else {

            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VENDOR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

        }
        return baseResponse;
    }

    private BaseResponse goForVideoconDisbursement(DisbursementRequest disbursementRequest, VideoconRequest videoconRequest, WFJobCommDomain videoconConfig) throws Exception {

        DisbursementResponse disbursementResponse = callToVideoconDisbursement(disbursementRequest, videoconConfig);
        BaseResponse baseResponse;
        DisbursementFeatureLog.Builder disbursementFeatureLogBuilder = new DisbursementFeatureLog.Builder();
        disbursementFeatureLogBuilder.serialNumber(disbursementRequest.getSerialNumber());
        disbursementFeatureLogBuilder.product(disbursementRequest.getHeader().getProduct());
        disbursementFeatureLogBuilder.skuCode(disbursementRequest.getSkuCode());
        disbursementFeatureLogBuilder.dealerId(disbursementRequest.getHeader().getDealerId());
        disbursementFeatureLogBuilder.refID(disbursementRequest.getReferenceID());
        disbursementFeatureLogBuilder.institutionId(disbursementRequest.getHeader().getInstitutionId());
        disbursementFeatureLogBuilder.vendor(disbursementRequest.getVendor());


        if (null != disbursementResponse && null == disbursementResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildVideoconSerialDisbursementResponse(disbursementResponse);

            if(StringUtils.equalsIgnoreCase(Status.VALID.name(),serialNumberResponse.getStatus())){

                disbursementFeatureLogBuilder.status(Status.DISBURSEMENT.name());
            } else {

                disbursementFeatureLogBuilder.status(Status.FAILED.name());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            disbursementFeatureLogBuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != disbursementResponse ? disbursementResponse.getError().getMessage() : null)
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        externalAPILogRepository.saveDisbursementLog(disbursementFeatureLogBuilder.build());
        return baseResponse;
    }


    @Override
    public BaseResponse validateKent(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {
        BaseResponse baseResponse;
        Collection<Error> errors;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.serialNumber(serialSaleConfirmationRequest.getSerialNumber());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.skuCode(serialSaleConfirmationRequest.getSkuCode());
        builder.storeCode(serialSaleConfirmationRequest.getStoreCode());
        builder.scheme(serialSaleConfirmationRequest.getScheme());

        WFJobCommDomain kentConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.KENT.toValue());

        if (null == kentConfig) {
            errors = GngUtils.getConfigurationNotFoundErrorList();
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = serialNumberValidationEngine.validationForKent(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {
                KentRequest kentRequest = serialNumberValidationBuilder.buildKentRequest(serialSaleConfirmationRequest, kentConfig);

                KentResponse kentResponse = callToKent(kentConfig, kentRequest);

                if (null != kentResponse && null == kentResponse.getError()) {

                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildKentResponse(kentResponse);
                    builder.status(serialNumberResponse.getStatus());
                    builder.originalResponse(serialNumberResponse.getStatus() + ":" + serialNumberResponse.getMessage());
                    serialNumberInfo = builder.build();

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {

                        // update application stage to SRNV
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    errors = new ArrayList<>();
                    String message = kentResponse.getError().getType() + " : " + kentResponse.getError().getMessage();
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(message);
                }
            } else {
                builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        /**
         * Saving logs
         */
        serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private KentResponse callToKent(WFJobCommDomain kentConfig, KentRequest kentRequest) throws Exception {

        String url = Arrays.asList(kentConfig.getBaseUrl(), kentConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (KentResponse) TransportUtils.postJsonRequest(kentRequest, url, KentResponse.class);

    }

    private BaseResponse callToKentRollback(RollbackRequest rollbackRequest, KentRequest kentRequest, WFJobCommDomain kentConfig) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_ROLLBACK.toFaceValue());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        KentResponse kentResponse = callToKent(kentConfig, kentRequest);
        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSkuCode());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != kentResponse && null == kentResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildKentResponse(kentResponse);

            if (rollbackRequest.isResetStage() && StringUtils.equalsIgnoreCase(Status.VALID.name(),serialNumberResponse.getStatus())) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

            /**
             * update serial-number valid to rollback,because we rollback this serial number.
             */
            externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());

                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {

                rollbackFeatureLogbuilder.status(Status.FAILED.name());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            activityLog.setCustomMsg(kentResponse.getError().getMessage());
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != kentResponse ? kentResponse.getError().getMessage() : null)
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    private BaseResponse goForKentDisbursement(DisbursementRequest disbursementRequest, KentRequest kentRequest, WFJobCommDomain kentConfig) throws Exception {

        KentResponse kentResponse = callToKent(kentConfig, kentRequest);
        BaseResponse baseResponse = null;
        DisbursementFeatureLog.Builder disbursementFeatureLogBuilder = new DisbursementFeatureLog.Builder();
        disbursementFeatureLogBuilder.serialNumber(disbursementRequest.getSerialNumber());
        disbursementFeatureLogBuilder.product(disbursementRequest.getHeader().getProduct());
        disbursementFeatureLogBuilder.skuCode(disbursementRequest.getSkuCode());
        disbursementFeatureLogBuilder.dealerId(disbursementRequest.getHeader().getDealerId());
        disbursementFeatureLogBuilder.refID(disbursementRequest.getReferenceID());
        disbursementFeatureLogBuilder.institutionId(disbursementRequest.getHeader().getInstitutionId());
        disbursementFeatureLogBuilder.vendor(disbursementRequest.getVendor());


        if (null != kentResponse && null == kentResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildKentResponse(kentResponse);

            disbursementFeatureLogBuilder.status(Status.DISBURSEMENT.name());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

        } else {
            disbursementFeatureLogBuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != kentResponse ? kentResponse.getError().getMessage() : null)
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }
        externalAPILogRepository.saveDisbursementLog(disbursementFeatureLogBuilder.build());
        return baseResponse;
    }


    private BaseResponse goForPanasonicSerialNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());

        activityLog.setStage(GNGWorkflowConstant.SRNV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.SR_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid` number
        activityLog.setStatus(Status.FAIL.toString());

        //call to panasonic validation
        Collection<Error> errors = serialNumberValidationEngine.validationForPanasonicSerial(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {
            //getting Panasonic config from WFJobCommDomain
            WFJobCommDomain panasonicConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.PANASONIC_SERIAL_NUMBER.toValue());

            if (null != panasonicConfig) {

                PanasonicRequest panasonicRequest = serialNumberValidationBuilder.buildPanasonicSerialNumberRequest(serialSaleConfirmationRequest, panasonicConfig);
                PanasonicResponse panasonicResponse = callToPanasonic(panasonicConfig, panasonicRequest);

                if (null != panasonicResponse && null == panasonicResponse.getError()) {

                    serialNumberResponse = serialNumberValidationBuilder.buildPanasonicSerialResponse(panasonicResponse);

                    serialNumberInfoBuilder.originalResponse(panasonicResponse.getValidateDataResponse());
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    // update application stage to SRNV
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.SRNV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                } else {
                    //no getting response from panasonic
                    logger.warn("get error from Connector api for {}", UrlType.PANASONIC_SERIAL_NUMBER.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(panasonicResponse.getError().getType())
                            .message(panasonicResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(panasonicResponse.getError().getMessage());

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.PANASONIC_SERIAL_NUMBER.name());

                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {

            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.FAILED.name());
            activityLog.setCustomMsg("ERROR");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for panasonic serial number information to the database.
         */
        logger.debug("Saving serial number info for {}", UrlType.PANASONIC_SERIAL_NUMBER.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;

    }


    private BaseResponse goForPanasonicImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {
        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only for valid` number
        activityLog.setStatus(Status.FAIL.toString());

        //call to panasonic validation
        Collection<Error> errors = serialNumberValidationEngine.validationForPanasonicImei(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {
            //getting Panasonic config from WFJobCommDomain
            WFJobCommDomain panasonicConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.PANASONIC_IMEI.toValue());

            if (null != panasonicConfig) {

                PanasonicRequest panasonicRequest = serialNumberValidationBuilder.buildPanasonicImeiNumberRequest(serialSaleConfirmationRequest, panasonicConfig);
                PanasonicResponse panasonicResponse = callToPanasonic(panasonicConfig, panasonicRequest);

                if (null != panasonicResponse && null == panasonicResponse.getError()) {

                    serialNumberResponse = serialNumberValidationBuilder.buildPanasonicImeiResponse(panasonicResponse);

                    serialNumberInfoBuilder.originalResponse(panasonicResponse.getValidateDataResponse());
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    // update application stage to IMEIV
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository.
                                updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        activityLog.setCustomMsg("Valid IMEI number");

                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg("Invalid IMEI number");
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                } else {
                    //no getting response from panasonic
                    logger.warn("get error from Connector api for {}", UrlType.PANASONIC_IMEI.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .id(panasonicResponse.getError().getType())
                            .message(panasonicResponse.getError().getMessage())
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(panasonicResponse.getError().getMessage());

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.PANASONIC_IMEI.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {

            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.FAILED.name());
            activityLog.setCustomMsg("ERROR");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for panasonic serial number information to the database.
         */
        logger.debug("Saving serial number info for {}", UrlType.PANASONIC_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }


    @Override
    public BaseResponse validateOppoImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse;
        Collection<Error> errors;
        SerialNumberInfo serialNumberInfo = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        // Set status to success only if the number is valid one
        activityLog.setStatus(Status.FAIL.toString());

        SerialNumberInfo.Builder builder = SerialNumberInfo.builder();
        builder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId());
        builder.product(serialSaleConfirmationRequest.getHeader().getProduct());
        builder.refID(serialSaleConfirmationRequest.getReferenceID());
        builder.vendor(serialSaleConfirmationRequest.getVendor());
        builder.imeiNumber(serialSaleConfirmationRequest.getImeiNumber());
        builder.dealerId(serialSaleConfirmationRequest.getHeader().getDealerId());
        builder.skuCode(serialSaleConfirmationRequest.getSkuCode());
        builder.oppoDealerId(serialSaleConfirmationRequest.getDealerId());
        /*Channelcode contains OppoAgent information */
        builder.channelCode(serialSaleConfirmationRequest.getChannelCode());

        WFJobCommDomain oppoConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.OPPO_IMEI.toValue());

        if (null == oppoConfig) {
            errors = GngUtils.getConfigurationNotFoundErrorList();
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            errors = serialNumberValidationEngine.validationForOppoImei(serialSaleConfirmationRequest);

            if (CollectionUtils.isEmpty(errors)) {

                OppoRequest oppoRequest = serialNumberValidationBuilder.buildOppoImeiRequest(serialSaleConfirmationRequest, oppoConfig);
                OppoResponse oppoResponse = callToOppo(oppoConfig, oppoRequest);

                if (null != oppoResponse && null == oppoResponse.getError()) {

                    SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder.buildOppoImeiResponse(oppoResponse);
                    builder.status(serialNumberResponse.getStatus());
                    builder.originalResponse(serialNumberResponse.getStatus() + ":" + serialNumberResponse.getMessage());
                    serialNumberInfo = builder.build();

                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberInfo.getStatus())) {

                        // update application stage to IMEIV
                        applicationStagerepository
                                .updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);
                } else {
                    builder.status(Status.NO_RESPONSE.name());
                    errors = new ArrayList<>();
                    String message = oppoResponse.getError().getType() + " : " + oppoResponse.getError().getMessage();
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setCustomMsg(message);
                }
            } else {
                builder.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            builder.status(Status.PRODUCT_ALLOWED_FAILED.name());
            errors = GngUtils.getInvalidProductErrorList();

            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(ErrorCode.INVALID_PRODUCT);
        }

        /**
         * Saving logs
         */
        serialNumberInfo = builder.build();
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfo);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;

    }


    private OppoResponse callToOppo(WFJobCommDomain oppoConfig, OppoRequest oppoRequest) throws Exception {

        String url = Arrays.asList(oppoConfig.getBaseUrl(), oppoConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (OppoResponse) TransportUtils.postJsonRequest(oppoRequest, url, OppoResponse.class);

    }


    @Override
    public BaseResponse validateGoogle(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;

        SerialNumberInfo.Builder serialNumberInfoBuilder = SerialNumberInfo.builder();
        serialNumberInfoBuilder.institutionId(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .product(serialSaleConfirmationRequest.getHeader().getProduct())
                .refID(serialSaleConfirmationRequest.getReferenceID())
                .vendor(serialSaleConfirmationRequest.getVendor())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .rdsCode(serialSaleConfirmationRequest.getRdsCode());


        if (Product.DPL == serialSaleConfirmationRequest.getHeader().getProduct()) {
            if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber())
                    && StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
                baseResponse = goForGoogleImeiNumberValidation(serialSaleConfirmationRequest, serialNumberInfoBuilder);
            } else {
                errors = GngUtils.getInvalidSerialOrIMEIErrorList();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
            }
        } else {
            errors = GngUtils.getInvalidProductErrorList();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;

    }

    private BaseResponse goForGoogleImeiNumberValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest, SerialNumberInfo.Builder serialNumberInfoBuilder) throws Exception {

        BaseResponse baseResponse = null;
        SerialNumberResponse serialNumberResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, serialSaleConfirmationRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.IMEIV.toFaceValue());
        activityLog.setAction(GNGWorkflowConstant.IMEI_VALIDATION.toFaceValue());
        activityLog.setRefId(serialSaleConfirmationRequest.getReferenceID());
        // Set status to success only for valid` number
        activityLog.setStatus(Status.FAIL.toString());

        //call to Google validation
        Collection<Error> errors = serialNumberValidationEngine.validationForGoogleImei(serialSaleConfirmationRequest);
        SerialNumberResponse.Builder serialNumberResponseBuilder = SerialNumberResponse.builder();
        serialNumberResponseBuilder.vendor(serialSaleConfirmationRequest.getVendor());


        if (errors.isEmpty()) {
            //getting Google config from WFJobCommDomain
            WFJobCommDomain googleConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    serialSaleConfirmationRequest.getHeader().getInstitutionId(), UrlType.GOOGLE_IMEI.toValue());

            if (null != googleConfig) {

                GoogleRequest googleRequest = serialNumberValidationBuilder.buildGoogleImeiNumberRequest(serialSaleConfirmationRequest);
                GoogleResponse googleResponse = callToGoogle(googleConfig, googleRequest);

                if (null != googleResponse && null == googleResponse.getError()) {

                    serialNumberResponse = serialNumberValidationBuilder.buildGoogleImeiResponse(googleResponse);

                    serialNumberInfoBuilder.originalResponse(serialNumberResponse.getMessage());
                    serialNumberInfoBuilder.status(serialNumberResponse.getStatus());

                    // update application stage to IMEIV
                    if (StringUtils.equalsIgnoreCase(Status.VALID.name(), serialNumberResponse.getStatus())) {
                        applicationStagerepository.
                                updateApplicationStage(serialSaleConfirmationRequest, GNGWorkflowConstant.IMEIV.toFaceValue());
                        activityLog.setStatus(Status.SUCCESS.toString());
                        activityLog.setCustomMsg("Valid IMEI number");

                    } else {
                        activityLog.setStatus(Status.FAILED.name());
                        activityLog.setCustomMsg("Invalid IMEI number");
                    }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);

                } else {
                    //no getting response from Google API
                    logger.warn("get error from Connector api for {}", UrlType.GOOGLE_IMEI.name());
                    serialNumberInfoBuilder.status(Status.ERROR.name());
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .level(Error.SEVERITY.HIGH.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .build());

                    activityLog.setStatus(Status.FAILED.name());
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, "Google"));

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);

                }

            } else {
                //configuration not found.
                logger.warn("Configuration not found for {}", UrlType.GOOGLE_IMEI.name());
                errors = GngUtils.getConfigurationNotFoundErrorList();

                activityLog.setStatus(Status.FAILED.name());
                activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);

                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }


        } else {
            StringBuilder validationErrorMsg = new StringBuilder();
            for(Error error : errors){
                validationErrorMsg.append(error.getMessage()).append(", ");
                activityLog.setCustomMsg(validationErrorMsg.toString());
                serialNumberInfoBuilder.customMsg(validationErrorMsg.toString());
            }
            serialNumberInfoBuilder.status(Status.BUSINESS_VALIDATION_FAILED.name());
            activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        /**
         * Saving logs for Google IMEI number information to the database.
         */
        logger.debug("Saving IMEI number info for {}", UrlType.GOOGLE_IMEI.name());
        externalAPILogRepository.saveSerialNumberInfo(serialNumberInfoBuilder.build());


        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;


    }


    private GoogleResponse callToGoogle(WFJobCommDomain googleConfig, GoogleRequest googleRequest) throws Exception {
        String url = Arrays.asList(googleConfig.getBaseUrl(), googleConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (GoogleResponse) TransportUtils.postJsonRequest(googleRequest, url, GoogleResponse.class);

    }


    private BaseResponse callToGoogleRollback(RollbackRequest rollbackRequest, GoogleRequest googleRequest, WFJobCommDomain googleConfig) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, rollbackRequest.getHeader());
        activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
        activityLog.setAction(ActionName.IMEI_ROLLBACK.name());
        activityLog.setRefId(rollbackRequest.getReferenceID());
        activityLog.setInstitutionId(rollbackRequest.getHeader().getInstitutionId());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());


        GoogleResponse googleResponse = callToGoogle(googleConfig, googleRequest);

        BaseResponse baseResponse;
        RollbackFeatureLog.Builder rollbackFeatureLogbuilder = RollbackFeatureLog.builder();
        rollbackFeatureLogbuilder.imeiNumber(rollbackRequest.getImeiNumber());
        rollbackFeatureLogbuilder.serialNumber(rollbackRequest.getSerialNumber());
        rollbackFeatureLogbuilder.dealerId(rollbackRequest.getHeader().getDealerId());
        rollbackFeatureLogbuilder.refID(rollbackRequest.getReferenceID());
        rollbackFeatureLogbuilder.vendor(rollbackRequest.getVendor());

        if (null != googleResponse && null == googleResponse.getError()) {

            SerialNumberResponse serialNumberResponse = serialNumberValidationBuilder
                    .buildGoogleImeiRollbackResponse(googleResponse);

            if (rollbackRequest.isResetStage() && StringUtils.equalsIgnoreCase(Status.SUCCESS.name(),serialNumberResponse.getStatus()) ) {

                rollbackFeatureLogbuilder.status(Status.ROLL_BACK.name());

                /**
                 * update IMEI-number valid to rollback,because we rollback this IMEI number.
                 */
                externalAPILogRepository.updateSerialNumberInfoLog(rollbackRequest, Status.ROLL_BACK.name());


                applicationStagerepository
                        .updateApplicationStage(rollbackRequest.getReferenceID(), GNGWorkflowConstant.APRV.toFaceValue());
                activityLog.setStatus(Status.SUCCESS.toString());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            } else {

                rollbackFeatureLogbuilder.status(Status.FAILED.name());
                activityLog.setCustomMsg(serialNumberResponse.getMessage());
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serialNumberResponse);


        } else {
            activityLog.setCustomMsg(null != googleResponse ? googleResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, "Google"));
            rollbackFeatureLogbuilder.status(Status.ERROR.name());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .errorCode(Error.SEVERITY.CRITICAL.name())
                    .message(null != googleResponse ? googleResponse.getError().getMessage() : String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, "Google"))
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }

        externalAPILogRepository.saveRollbackLog(rollbackFeatureLogbuilder.build());
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);
        return baseResponse;

    }

    @Override
    public BaseResponse saveSerialNumberApplicableResponse(SerialNumberApplicableVendorResultRequest serialNumberApplicableVendorResultRequest) throws Exception {

        SerialNumberApplicableVendorLog.SerialNumberApplicableVendorLogBuilder logBuilder = SerialNumberApplicableVendorLog.builder();
        logBuilder.institutionId(serialNumberApplicableVendorResultRequest.getHeader().getInstitutionId());
        logBuilder.dealerId(serialNumberApplicableVendorResultRequest.getHeader().getDealerId());
        logBuilder.product(serialNumberApplicableVendorResultRequest.getHeader().getProduct().name());
        logBuilder.referenceId(serialNumberApplicableVendorResultRequest.getReferenceId());
        logBuilder.vendor(serialNumberApplicableVendorResultRequest.getVendor());
        logBuilder.dateTime(new Date());

        logBuilder.status(serialNumberApplicableVendorResultRequest.getStatus());
        logBuilder.customMsg(serialNumberApplicableVendorResultRequest.getMessage());
        externalAPILogRepository.saveSerialNumberApplicableVendorLog(logBuilder.build());

        return GngUtils.getBaseResponse(HttpStatus.OK,logBuilder.build());
    }

    }