package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.factory.DigitizationBuilder;
import com.softcell.service.DigitizationManager;
import com.softcell.service.DigitizationStoreManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Created by mahesh on 27/7/17.
 */
@Service
public class DigitizationStoreManagerImpl implements DigitizationStoreManager {

    private static final Logger logger = LoggerFactory.getLogger(DigitizationStoreManagerImpl.class);

    @Autowired
    private DigitizationManager digitizationManager;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private DigitizationBuilder digitizationBuilder;

    @Autowired
    private ApplicationRepository applicationRepository;


    @Override
    public String saveApplicationForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {

        String applicationFormFileId = null;
        logger.info("Inside the save agreement form");

        try {
            BaseResponse applicationForm = digitizationManager.getApplicationForm(invoiceDetailsRequest.getHeader().getInstitutionId(),
                    invoiceDetailsRequest.getHeader().getProduct().toProductId(),
                    invoiceDetailsRequest.getRefID());

            Document applicationFormDocument = GngUtils.convertBaseResponsePayload(applicationForm, Document.class);

            if (null != applicationFormDocument
                    && StringUtils.isNotBlank(applicationFormDocument.getByteCode())) {

                logger.info("Application form bytecode successfully fetched from digitization manager");

                FileUploadRequest fileUploadRequest = digitizationBuilder.buildDigitizationFormUploadRequest(invoiceDetailsRequest, fileName);


                // convert base64 to bytes and store in the db.

                byte[] bytes = Base64.decodeBase64(applicationFormDocument.getByteCode().getBytes());

                try (InputStream inputStream = new ByteArrayInputStream(bytes)) {
                    applicationFormFileId = uploadFileRepository.store(inputStream,
                            fileName, MediaType.APPLICATION_PDF_VALUE,
                            fileUploadRequest);
                }
                if (null == applicationFormFileId) {
                    logger.error("Problem occur at the time saving application form for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());

                } else {
                    logger.info("Application form successfully saved for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());
                }

            }

        } catch (Exception e) {

            logger.error("problem occur at the time of saving ApplicationForm in the db with probable cause {}", e.getMessage());

        }
        return applicationFormFileId;

    }

    @Override
    public String  saveAgreementForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {

        logger.info("Inside the save agreement form");

        String agreementFormFileId = null;

        try {
            BaseResponse agreementForm = digitizationManager.getAgreementForm(invoiceDetailsRequest.getHeader().getInstitutionId(),
                    invoiceDetailsRequest.getHeader().getProduct().toProductId(),
                    invoiceDetailsRequest.getRefID());

            Document agreementFormDocument = GngUtils.convertBaseResponsePayload(agreementForm, Document.class);

            if (null != agreementFormDocument
                    && StringUtils.isNotBlank(agreementFormDocument.getByteCode())) {

                logger.info("Agreement form bytecode  successfully fetched from digitization manager");

                FileUploadRequest fileUploadRequest = digitizationBuilder.buildDigitizationFormUploadRequest(invoiceDetailsRequest, fileName);

                // convert base64 to bytes and store in the db.

                byte[] bytes = Base64.decodeBase64(agreementFormDocument.getByteCode().getBytes());

                try (InputStream inputStream = new ByteArrayInputStream(bytes)) {

                    agreementFormFileId = uploadFileRepository.store(inputStream,
                            fileName, MediaType.APPLICATION_PDF_VALUE,
                            fileUploadRequest);

                }
                if (null == agreementFormFileId) {
                    logger.error("Problem occur at the time saving agreement form for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());

                } else {
                    logger.info("Agreement form successfully saved for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());
                }

            }


        } catch (Exception e) {

            logger.error("problem occur at the time of saving Agreement Form in the db with probable cause {}", e.getMessage());

        }

        return agreementFormFileId;

    }

    @Override
    public String saveDeliveryOrderForm(PostIpaRequest postIpaRequest, PostIPA postIPA ,String fileOperationType) {

        //fileOperationType may be CREATE ,CANCEL ,RESTORE etc.
        String deliveryOrderFileId = null;
        String modelNo = null;

        try {

            BaseResponse deliveryOrder = digitizationManager.getDeliveryOrder(
                    postIpaRequest.getHeader().getInstitutionId(),
                    postIpaRequest.getHeader().getProduct().toProductId(), postIpaRequest.getRefID() ,fileOperationType);


            Document deliveryOrderDocument = GngUtils.convertBaseResponsePayload(deliveryOrder, Document.class);

            if (null != deliveryOrderDocument && StringUtils.isNotBlank(deliveryOrderDocument.getByteCode())
                    && !CollectionUtils.isEmpty(postIPA.getAssetDetails())
                    && StringUtils.isNotBlank(postIPA.getAssetDetails().get(0).getModelNo())) {

                modelNo = postIPA.getAssetDetails().get(0).getModelNo();

                /**
                 *  decode base64 to bytes and store in the db.
                 */
                byte[] bytes = Base64.decodeBase64(deliveryOrderDocument.getByteCode().getBytes());

                try (InputStream inputStream = new ByteArrayInputStream(bytes)) {

                    FileUploadRequest fileUploadRequest = digitizationBuilder.buildDeliveryOrderUploadRequest(postIpaRequest, modelNo);

                    applicationRepository.softDeleteDOReport(postIpaRequest
                            .getRefID(), postIpaRequest.getHeader()
                            .getInstitutionId(), fileUploadRequest.getUploadFileDetails().getFileName());

                    deliveryOrderFileId = uploadFileRepository.store(inputStream,
                            fileUploadRequest.getUploadFileDetails().getFileName(), MediaType.APPLICATION_PDF_VALUE,
                            fileUploadRequest);

                }
            }
        } catch (Exception e) {
            logger.error("problem occur at the time of saving Delivery Order in the db with probable cause {}",e.getMessage());
        }
        return deliveryOrderFileId;
    }

    @Override
    public String saveAchMandateForm(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {
        logger.info("Inside the save achMandate form");

        String achMandateFormFileId = null;

        try {
            BaseResponse achMandateForm = digitizationManager.downloadAchMandateForm(invoiceDetailsRequest.getHeader().getInstitutionId(),
                    invoiceDetailsRequest.getHeader().getProduct().toProductId(),
                    invoiceDetailsRequest.getRefID());

            Document achFormDocument = GngUtils.convertBaseResponsePayload(achMandateForm, Document.class);

            if (null != achFormDocument
                    && StringUtils.isNotBlank(achFormDocument.getByteCode())) {

                logger.info("AchMandate form bytecode  successfully fetched from digitization manager");

                FileUploadRequest fileUploadRequest = digitizationBuilder.buildDigitizationFormUploadRequest(invoiceDetailsRequest, fileName);

                // convert base64 to bytes and store in the db.

                byte[] bytes = Base64.decodeBase64(achFormDocument.getByteCode().getBytes());

                try (InputStream inputStream = new ByteArrayInputStream(bytes)) {

                    achMandateFormFileId = uploadFileRepository.store(inputStream,
                            fileName, MediaType.APPLICATION_PDF_VALUE,
                            fileUploadRequest);

                }
                if (StringUtils.isBlank(achMandateFormFileId)) {
                    logger.error("Problem occur at the time saving achMandate form for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());

                } else {
                    logger.info("AchMandate form successfully saved for ref Id {} and inst id {}", invoiceDetailsRequest.getRefID(), invoiceDetailsRequest.getHeader().getInstitutionId());
                }

            }


        } catch (Exception e) {

            logger.error("problem occur at the time of saving Ach Mandate Form in the db with probable cause {}", e.getMessage());

        }

        return achMandateFormFileId;
    }
}
