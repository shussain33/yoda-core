package com.softcell.service.impl;

import com.softcell.constants.Institute;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.DashboardDataHelper;
import com.softcell.dao.mongodb.repository.DashboardDataRepository;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.dashboard.DashboardDetails;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.dashboard.DashboardResponse;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.ConfigurationManager;
import com.softcell.service.DashboardManger;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by suraj on 11/9/20.
 */
@Service
public class DashboardMangerImpl implements DashboardManger{

    private static final Logger logger = LoggerFactory.getLogger(DashboardMangerImpl.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private DashboardDataHelper dashboardDataHelper;

    @Autowired
    private DashboardDataRepository dashboardDataRepository;

    @Override
    public BaseResponse getDashBoardData(DashboardRequest dashboardRequest) throws Exception {
        logger.debug("getDashBoardData service started");

        BaseResponse baseResponse = null;

        if (configurationManager.isValidVersion(dashboardRequest.getHeader().getInstitutionId(),
                dashboardRequest.getHeader().getApplicationSource())) {

            DashboardResponse dashboardResponse = getDashboardDetails(dashboardRequest);

            if (!CollectionUtils.isEmpty(dashboardResponse.getDashboardDetailsList())) {
                if(dashboardRequest.getHeader().getApplicationSource().contains("WEB")){
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dashboardResponse);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dashboardResponse.getDashboardDetailsList());
                }
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
        }
        return baseResponse;
    }

    private DashboardResponse getDashboardDetails(DashboardRequest dashboardRequest) {
        StopWatch timer = new StopWatch();
        timer.start();
        logger.info("DashboardDetails started for userId : " + dashboardRequest.getHeader().getLoggedInUserId());
        DashboardResponse dashboardResponse = new DashboardResponse();
        List<DashboardDetails> detailsList = null;
        long totalRecordCount = 0;
        boolean refCriteria = false, filterCriteria = false;

        Header header = dashboardRequest.getHeader();
        boolean showMyRecords = dashboardRequest.isShowMyRecords();
        int skip = dashboardRequest.getSkip();
        int limit = dashboardRequest.getLimit();
        String userId = header.getLoggedInUserId();
        String role = header.getLoggedInUserRole();
        Map<String, String> filterMap = dashboardRequest.getFilters();
        if(Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)) {
            if(header.getCredit() != null){
                if(header.getCredit()) {
                    role = Roles.Role.CREDIT.name();
                }
            }
        }

        try {
            StopWatch queryTimer = new StopWatch();
            queryTimer.start();

            if(!CollectionUtils.isEmpty(filterMap)){
                refCriteria = filterMap.entrySet().stream().anyMatch(obj -> StringUtils.equals(obj.getKey(),"refId") && StringUtils.length(obj.getValue()) == 16);
                filterCriteria = true;
            }

            Criteria criteria = new Criteria();
            //adding default Criteria for institutionId, lanType, sales criteria if any, date range
            dashboardDataHelper.addDefaultCriteria(dashboardRequest, filterMap, criteria, refCriteria);
            //   Hierarchy criteria creation
            dashboardDataHelper.addHierarchyCriteria(dashboardRequest, criteria);
            // Add loggedIn role specific criteria in query
            dashboardDataHelper.addRoleSpecificCriteria(userId, role, criteria, header, showMyRecords);
            // Internal External Verifier sepcific criteria
            Map<String, String> appIdStatusMap = null;
            if(Cache.VERIFICATION_ROLE_URL_MAP.containsKey(role)) {
                appIdStatusMap = dashboardDataHelper.addCriteraForVerifierAndGetData(criteria, header, role, showMyRecords);
            }
            //Addin filter Criteria
            if(filterCriteria){
                dashboardDataHelper.addFilterSpecificCriteria(criteria, filterMap, refCriteria);
            }

            //Fetching total records count from DB
            totalRecordCount = dashboardDataRepository.fetchDashboardRecordCount(criteria);
            queryTimer.stop();
            logger.info("Query builder executed in ms : " + queryTimer.getLastTaskTimeMillis());
            //logger.info("Query by userId {} of role {} for dashboard request {}", userId, role, new Query(criteria));
            //Fetching actual records count from DB
            List<GoNoGoCustomerApplication> applications;
            if(totalRecordCount > 0) {
                applications = dashboardDataRepository.fetchDashboardRecords(criteria, skip, limit);
                List<DashboardDetails> finalGngResponseList = dashboardDataHelper.buildDashboardDetail(applications, appIdStatusMap);
                detailsList = finalGngResponseList.stream().sorted(Comparator.comparing(DashboardDetails::getDate).reversed())
                        .collect(Collectors.toList());
                logger.debug("getDashboardData repo completed with record count {}", detailsList.size());
            }else {
                detailsList = new ArrayList<>();
                logger.debug("getDashboardData repo completed with record count {}", detailsList.size());
            }
        }catch (DataAccessException e) {
            logger.error("Exception fetching records for dashboard details {}", e.getMessage());
        }

        dashboardResponse.setDashboardDetailsList(detailsList);
        dashboardResponse.setRecordCount(totalRecordCount);
        timer.stop();
        logger.info("DashboardDetails ended for userId : {}, In-ms : {}" + dashboardRequest.getHeader().getLoggedInUserId(), timer.getLastTaskTimeMillis());
        return dashboardResponse;
    }
}
