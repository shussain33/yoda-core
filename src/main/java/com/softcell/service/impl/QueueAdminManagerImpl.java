package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.queue.management.QueueRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.core.AllocationInfo;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.gonogo.model.queueMgmt.CroQueueTracking;
import com.softcell.gonogo.model.request.queue.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.queue.CroAuditResponse;
import com.softcell.gonogo.model.response.queue.QueueStatusResponse;
import com.softcell.gonogo.model.response.queue.SchedulerInfoResponse;
import com.softcell.gonogo.model.response.queue.UserStatus;
import com.softcell.gonogo.queue.management.CaseLoadBalancer;
import com.softcell.gonogo.queue.management.QueueEventHandler;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;
import com.softcell.gonogo.queue.management.Roles;
import com.softcell.service.QueueAdminManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by archana on 18/9/17.
 */
@Service
public class QueueAdminManagerImpl implements QueueAdminManager {

    private static final Logger logger = LoggerFactory.getLogger(QueueAdminManagerImpl.class);
    private static final String SCHEDULER_RUN_SUCCESSFUL = "Scheduler run successful";
    private static final String CASE_ALLOCATION_DONE = "Case allocation done";
    private static final String  USER_NOT_LOGGED_IN = "User not logged in";
    private static final String  PERMISSION_DENIED = "Permission denied";
    private static final String  PROVIDE_DATES = "Provide date(s)";
    private static final String  WRONG_PERIOD= "Wrong period";
    private static final String  CAN_NOT_ASSIGN_CASE_TO_USER = "Can not assign case to user";
    private static final String  CAN_NOT_ASSIGN_CASE_TO_SAME_USER = "Can not assign case to same user";
    private static final String  USERS_ARE_OF_DIFFERENT_ROLES = "Users are of different roles";
    private static final String  NOT_A_SINGLE_CASE_IS_AVAILABLE = "Not a single case is available";

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private QueueEventHandler queueEventHandler;

    @Autowired
    private CaseLoadBalancer caseLoadBalancer;

    /**
     * Returns online and idle users
     *
     * @param queueStatusRequest
     * @return
     */
    @Override
    public BaseResponse getStatus(QueueStatusRequest queueStatusRequest) {
        BaseResponse baseResponse;

        String institutionId = queueStatusRequest.getHeader().getInstitutionId();
        QueueStatusResponse response = new QueueStatusResponse();
        response.setOnlineUsers(getOnlineUsersList(institutionId));
        response.setIdleUsers(getIdleUsersList(institutionId));
        response.setOfflineUsers(getOfflineUsersList(institutionId));
        if (queueStatusRequest.isDbStatus()) {
            List<GoNoGoCustomerApplication> list = queueRepository.getAssignedCases(institutionId);
            Map<AllocationInfo, List<String>> resultMap =
                    list.stream().collect(
                            Collectors.groupingBy(GoNoGoCustomerApplication::getAllocationInfo,
                                    Collectors.mapping(GoNoGoCustomerApplication::getGngRefId, Collectors.toList())
                            )
                    );

            Map<String, List<String>> result = new HashMap<>();
            resultMap.forEach((k, v) -> {
               result.put(k.getOperator(), v);
            });
            response.setOperatorIdMap(result);
        }
        logger.debug("status : Online users : {} ", response.getOnlineUsers());
        logger.debug("status : Idle users : {} ", response.getIdleUsers());
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, response);
        return baseResponse;
    }

    @Override
    public BaseResponse getCroAuditData(CroAuditRequest croAuditRequest) {
        BaseResponse baseResponse;
        CroAuditResponse croAuditResponse;

        try {
            validateCroAuditRequest(croAuditRequest);

            String userIdToAudit = croAuditRequest.getUserIdToAudit();
            String institutionId = croAuditRequest.getHeader().getInstitutionId();
            boolean forToday = croAuditRequest.isTodayFlag();
            boolean isLoggedIn = queueEventHandler.isUserLoggedIn(userIdToAudit, institutionId);
            Date fromDate = croAuditRequest.getFromDate();
            Date toDate = croAuditRequest.getToDate();

            if (!isLoggedIn) {
                // User not logged in ! throw exception
                throw new GoNoGoException(USER_NOT_LOGGED_IN);
            }

            if (forToday) { // Admin requesting a CRO's today's data
                croAuditResponse = getCroAudit(userIdToAudit, institutionId, null, null);

            } else { // Admin requesting a CRO's stats for a period
                croAuditResponse = getCroAudit(userIdToAudit, institutionId, fromDate, toDate);
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, croAuditResponse);
            logger.debug("CRO Audit list : {}", croAuditResponse.getAuditList());
        } catch (Exception e) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, e.getMessage());
        }
        return baseResponse;
    }

    private CroAuditResponse getCroAudit(String userIdToAudit, String institutionId, Date fromDate, Date toDate)
            throws Exception {

        CroAuditResponse croAuditResponse = new CroAuditResponse();
        croAuditResponse.setUserId(userIdToAudit);
        croAuditResponse.setInstitutionId(institutionId);

        List<CroAudit> croAuditList = queueRepository.getCroAudit(userIdToAudit, institutionId, fromDate, toDate);
        Map<String, List<CroAudit>> result =
                croAuditList.stream().collect(
                        Collectors.groupingBy(
                                CroAudit::getDecision, Collectors.toList()
                        )
                );
        croAuditResponse.setAuditList(result);
        return croAuditResponse;
    }

    @Override
    public BaseResponse getCroStatistics(CroAuditRequest croAuditRequest) throws Exception {
        BaseResponse baseResponse;
        try {
        /*
            Check Role - QueueAdmin can view data of other user
                        If role is CRO then the user can view only his data
            Check login status - QueueAdmin can view offline user's data as well
                            A CRO can view his own dat only if he is logged in
                            ( Phew! - for an offline user it is impossible to send request for his own
                            But to make the code foolproof we have cater for this condition. )
            Check whether requested for today
                    Check for today's flag.
                    If it is set then get realtimeusercache for the user
                    If cache has the user and the audit data, return from cache
                    else query db to get audit data ; populate this data in cache if today flag is set.
         */

            Map<String, Long> statistics = new HashMap<>();

            validateCroAuditRequest(croAuditRequest);

            String userIdToAudit = croAuditRequest.getUserIdToAudit();
            String institutionId = croAuditRequest.getHeader().getInstitutionId();
            boolean forToday = croAuditRequest.isTodayFlag();
            boolean isAdmin = StringUtils.equalsIgnoreCase(croAuditRequest.getHeader().getLoggedInUserRole(), Roles.Role.QUEUE_ADMIN.name());
            boolean isLoggedIn = queueEventHandler.isUserLoggedIn(userIdToAudit, institutionId);
            Date fromDate = croAuditRequest.getFromDate();
            Date toDate = croAuditRequest.getToDate();

            if (isAdmin) { // Admin user
                if (isLoggedIn) { // CRO online
                    if (forToday) { // Admin requesting a CRO's today's stats
                        statistics = queueEventHandler.getCroStatistics(croAuditRequest);
                    } else { // Admin requesting a CRO's stats for a period
                        statistics = queueRepository.fetchCroStatistics(userIdToAudit,
                                institutionId, fromDate, toDate);
                    }
                } else { // CRO is not online; Admin wants to check his stats
                    if (forToday) {
                        // get from DB
                        statistics = queueRepository.fetchCroStatistics(userIdToAudit, institutionId);
                    } else {
                        statistics = queueRepository.fetchCroStatistics(userIdToAudit,
                                institutionId, fromDate, toDate);
                    }
                }
            } else { // Fetching data for self
                if (isLoggedIn) {
                    if (forToday) {
                        statistics = queueEventHandler.getCroStatistics(croAuditRequest);
                    } else {
                        statistics = queueRepository.fetchCroStatistics(userIdToAudit,
                                institutionId, fromDate, toDate);
                    }
                } else {
                    // User not logged in ! throw exception
                    throw new GoNoGoException(USER_NOT_LOGGED_IN);
                }
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, statistics);
        } catch (Exception e) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, e.getMessage());
        }
        return baseResponse;
    }

    private void validateCroAuditRequest(CroAuditRequest croAuditRequest) throws Exception {

        // Validate users
        validateUsers(croAuditRequest.getHeader().getLoggedInUserId(), croAuditRequest.getHeader().getLoggedInUserRole(),
                croAuditRequest.getUserIdToAudit(), croAuditRequest.getHeader().getInstitutionId());

        // Validate Dates
        if (!croAuditRequest.isTodayFlag()) {
            validateDates(croAuditRequest.getFromDate(), croAuditRequest.getToDate());
        }
    }

    private void validateUsers(String loggedInUserId, String role, String userIdToAudit, String institutionId) throws GoNoGoException {
        boolean isAdmin = StringUtils.equalsIgnoreCase(role, Roles.Role.QUEUE_ADMIN.name());
        boolean isLoggedIn = queueEventHandler.isUserLoggedIn(userIdToAudit, institutionId);

        /*
            Valid cases : 1) An admin user(=loggedInuserId) can request any user's(= userIdToAudit) data for any period
                irrespective of user(= userIdToAudit) logged in or not.
                  2) A user can request for his own data once he logs in.
         */
        // So the exceptional scenarios are as follows :
        // A nonAdmin user requesting for another user's data !! Security breach!!!
        if (!isAdmin && !StringUtils.equalsIgnoreCase(loggedInUserId, userIdToAudit)) {
            throw new GoNoGoException(PERMISSION_DENIED);
        }

        // A nonAdmn user requesting data for himself though not logged in ! May be somebody has tweaked teh system ??
        if (!isAdmin && !isLoggedIn) {
            throw new GoNoGoException(USER_NOT_LOGGED_IN);
        }
    }

    private void validateDates(Date fromDate, Date toDate) throws GoNoGoException {
        // validate dates
        if (fromDate == null && toDate == null) {
            throw new GoNoGoException(PROVIDE_DATES);
        }
        if (fromDate != null && toDate != null) {
            // Todate must be later of from date
            if (!toDate.after(fromDate)) {
                throw new GoNoGoException(WRONG_PERIOD);
            }
        }
    }

    @Override
    public BaseResponse assignCases(CaseAllocationRequest caseAllocationRequest) throws Exception {
        BaseResponse baseResponse;
        try {
            validateAllocationRequest(caseAllocationRequest);
            queueEventHandler.assignCases(caseAllocationRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, CASE_ALLOCATION_DONE);
        } catch (Exception e) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, e.getMessage());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getSchedulerInfo(SchedulerInfoRequest schedulerInfoRequest) {
        BaseResponse baseResponse;
        SchedulerInfoResponse response = new SchedulerInfoResponse();
        response.setIdleTimeInSec(caseLoadBalancer.getMAX_IDLE_TIME_IN_MILLIS() / 1000);
        response.setOfflineTimeSec(caseLoadBalancer.getOFFLINE_TIME_IN_MILLIS() / 1000);
        response.setLastSchedulerRun(caseLoadBalancer.getLastSchedulerRun());
        response.setSchedulerFrequency(caseLoadBalancer.getDELAY_IN_MILLIS() / 1000);

        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, response);

        return baseResponse;
    }

    @Override
    public BaseResponse runScheduler(RunSchedulerRequest runSchedulerRequest) {
        BaseResponse baseResponse;
        try {
            caseLoadBalancer.scheduleCaseLoadBalancing();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, SCHEDULER_RUN_SUCCESSFUL);
        } catch (Exception e) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, e.getMessage());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse changeAvailability(ChangeAvailabilityRequest changeAvailabilityRequest) {
        BaseResponse baseResponse;
        try {
            queueEventHandler.changeAvailability(changeAvailabilityRequest.getUserId(), changeAvailabilityRequest.getHeader().getInstitutionId(),
                    changeAvailabilityRequest.isAvailabilityFlag());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK);
        } catch (Exception e) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, e.getMessage());
        }

        return baseResponse;
    }

    private void validateAllocationRequest(CaseAllocationRequest caseAllocationRequest) throws GoNoGoException {
        /*  Check there is atleast from/to user
                        From != to
                        if from is present then the case must be present in his queue if he is online/idle
            */
        // From and to both are not provided
        if (StringUtils.isBlank(caseAllocationRequest.getAllocateFromUserId()) &&
                StringUtils.isBlank(caseAllocationRequest.getAllocateFromUserId())) {
            throw new GoNoGoException(CAN_NOT_ASSIGN_CASE_TO_USER);
        }
        // From and to both are same
        if (StringUtils.equalsIgnoreCase(caseAllocationRequest.getAllocateFromUserId(),
                caseAllocationRequest.getAllocateToUserId())) {
            throw new GoNoGoException(CAN_NOT_ASSIGN_CASE_TO_SAME_USER);
        }
        // From and to both are provided - they are of different roles
        if (StringUtils.isNotBlank(caseAllocationRequest.getAllocateFromUserId()) &&
                StringUtils.isNotBlank(caseAllocationRequest.getAllocateFromUserId())) {
            // Check roles
            // TODO : Currently only a single role is stored. When all roles are stored change this code to check in the list of roles
            String fromRole = queueEventHandler.getRealtimeUserStatus(caseAllocationRequest.getAllocateFromUserId(),
                    caseAllocationRequest.getHeader().getInstitutionId()).getRole();

            String toRole = queueEventHandler.getRealtimeUserStatus(caseAllocationRequest.getAllocateToUserId(),
                    caseAllocationRequest.getHeader().getInstitutionId()).getRole();
            if( ! StringUtils.equalsIgnoreCase( fromRole, toRole) ) {
                throw new GoNoGoException(USERS_ARE_OF_DIFFERENT_ROLES);
            }
        }

        if (StringUtils.isNotBlank(caseAllocationRequest.getAllocateFromUserId())) {
            // case must be present in his queue if he is online/idle
            if (Collections.disjoint(caseAllocationRequest.getCaseIds(),
                    queueEventHandler.getRealtimeUserStatus(caseAllocationRequest.getAllocateFromUserId(),
                            caseAllocationRequest.getHeader().getInstitutionId())
                            .getAssignedCaseIds())) {
                throw new GoNoGoException(NOT_A_SINGLE_CASE_IS_AVAILABLE);
            }
        }
    }

    private List<UserStatus> getOnlineUsersList(String institutionId) {
        // Get online users for this instIds
        List<RealtimeUserStatus> realtimeUserStatusList = queueEventHandler.getOnlineUsers(institutionId, true);
        List<UserStatus> userStatusList = new ArrayList<>();
        UserStatus userStatus = null;
        for (RealtimeUserStatus realtimeUserStatus : realtimeUserStatusList) {
            userStatus = new UserStatus();
            try {
                BeanUtils.copyProperties(userStatus, realtimeUserStatus);
                logger.debug(" {} \n copied to : \n{} ", realtimeUserStatus.toString(), userStatus.toString());
                userStatusList.add(userStatus);
            } catch (Exception ie) {
                logger.error("Error while copying properties of {} : {}", realtimeUserStatus, ie.getMessage());
            }
        }
        return userStatusList;
    }

    private List<UserStatus> getIdleUsersList(String institutionId) {
        // Get online users for this instIds
        List<RealtimeUserStatus> realtimeUserStatusList = queueEventHandler.getIdleUsers(institutionId, true);
        List<UserStatus> userStatusList = new ArrayList<>();
        UserStatus userStatus = null;
        for (RealtimeUserStatus realtimeUserStatus : realtimeUserStatusList) {
            userStatus = new UserStatus();
            try {
                BeanUtils.copyProperties(userStatus, realtimeUserStatus);
                userStatusList.add(userStatus);
            } catch (Exception ie) {
                logger.error("Error while copying properties of {} : {}", realtimeUserStatus, ie.getMessage());
            }
        }
        return userStatusList;
    }

    private List<UserStatus> getOfflineUsersList(String institutionId) {

        // Get online users for this instIds
        List<String> excludeUsers = queueEventHandler.getOnlineUsers(institutionId, false).stream()
                .map(status -> status.getUserId()).collect(Collectors.toList());
        // add idle users to exclude list
        excludeUsers.addAll(queueEventHandler.getIdleUsers(institutionId, false).stream()
                .map(status -> status.getUserId()).collect(Collectors.toList()));

        // Get rest of the users from database
        List<CroQueueTracking> croQueueTrackingList = queueRepository.getCroQueueTrackingList(institutionId,
                excludeUsers, false);

        List<UserStatus> userStatusList = new ArrayList<>();
        UserStatus userStatus = null;
        for (CroQueueTracking croQueueTracking : croQueueTrackingList) {
            userStatus = new UserStatus();
            try {
                BeanUtils.copyProperties(userStatus, croQueueTracking);
                userStatusList.add(userStatus);
            } catch (Exception ie) {
                logger.error("Error while copying properties of {} : {}", croQueueTracking, ie.getMessage());
            }
        }
        return userStatusList;
    }

    @Override
    public BaseResponse getAllUnassignedApplicationIds() {
        BaseResponse baseResponse;

        List<GoNoGoCustomerApplication> applicationList = queueRepository.getAllUnassignedApplications();
        //Sorting CustomerAapplications by their dates
        Collections.sort(applicationList, new Comparator<GoNoGoCustomerApplication>() {
            public int compare(GoNoGoCustomerApplication o1, GoNoGoCustomerApplication o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });
        List<String> applicationIds = new ArrayList<>();
        applicationList.forEach(tempObj -> {
            applicationIds.add(tempObj.getGngRefId());
        });
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicationIds);
        return baseResponse;
    }
}
