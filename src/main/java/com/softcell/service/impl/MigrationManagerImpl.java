package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.MigrationRepository;
import com.softcell.gonogo.model.core.BankingStatement;
import com.softcell.gonogo.model.core.CompletionInfo;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.multibureau.commercialCibil.cibilCommresponse.Base;
import com.softcell.gonogo.model.request.DsaIdData;
import com.softcell.gonogo.model.request.MigrationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.MigrationResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.queue.management.Roles;
import com.softcell.service.AppConfigurationManager;
import com.softcell.service.MigrationManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by amit on 11/3/19.
 */
@Service
public class MigrationManagerImpl implements MigrationManager {
    private Logger logger = LoggerFactory.getLogger(MigrationManagerImpl.class);

    @Autowired
    MigrationRepository migrationRepository;

    @Autowired
    AppConfigurationManager appConfigurationManager;


    @Override
    public BaseResponse updateRegistrationInfo(MigrationRequest migrationRequest) {
        BaseResponse response = null;
        logger.info("Registration Info migration started!");
        Header header = migrationRequest.getHeader();
        List<String> refIds = migrationRequest.getRefIds();
        Map<String, Object> criteriaConditions = new HashMap<>();
        criteriaConditions.put("completed.registration", null);

        List<String> includes = new ArrayList<>();
        includes.add("completed");
        includes.add("dateTime");
        includes.add("applicationRequest.header.dsaId");

        List<GoNoGoCustomerApplication> applications = migrationRepository.fetchApplication(header.getInstitutionId(),
                null, refIds, criteriaConditions, includes);
        List<String> appIds = applications.stream().map(GoNoGoCustomerApplication :: getGngRefId).collect(Collectors.toList());
        logger.info("Impacted Application Ids : " + appIds);

        for(GoNoGoCustomerApplication application : applications){
            try{
                CompletionInfo info = new CompletionInfo();
                info.setRole(Roles.Role.FOS.name());
                info.setUserId(application.getApplicationRequest().getHeader().getDsaId());
                info.setCompletedOn(application.getDateTime());
                info.setUpdatedOn(application.getDateTime());
                if(application.getCompleted() == null)
                    application.setCompleted(new HashMap<>());
                Map<String, Object> updateMap = new HashedMap();
                updateMap.put("completed.registration", info);
                migrationRepository.updateApplication(application.getGngRefId(), updateMap);
            } catch(Exception e) {
                logger.info("Not updated : " + application.getGngRefId());
                appIds.remove(application.getGngRefId());
            }
        }
        logger.info("Registration Info migration ended!");
        response = GngUtils.getBaseResponse(HttpStatus.OK, "Successfully updated application Ids : " + appIds);
        return response;
    }

    @Override
    public BaseResponse updateSourcingChannel(MigrationRequest migrationRequest) {
        BaseResponse response = null;
        logger.info("Sourcing Channel migration started!");
        Header header = migrationRequest.getHeader();
        List<String> refIds = migrationRequest.getRefIds();
        Map<String, Object> criteriaConditions = new HashMap<>();
        criteriaConditions.put("applicationRequest.request.application.channel", null);

        List<String> includes = new ArrayList<>();
            includes.add("applicationRequest.request.application.channel");

        List<GoNoGoCustomerApplication> applications = migrationRepository.fetchApplication(header.getInstitutionId(),
                null, refIds, criteriaConditions, includes);
        List<String> appIds = applications.stream().map(GoNoGoCustomerApplication :: getGngRefId).collect(Collectors.toList());
        logger.info("Impacted Application Ids : " + appIds);

        for(GoNoGoCustomerApplication application : applications){
            try{
                Map<String, Object> updateMap = new HashedMap();
                updateMap.put("applicationRequest.request.application.channel", "D2C");
                migrationRepository.updateApplication(application.getGngRefId(), updateMap);
            } catch(Exception e) {
                logger.info("Not updated : " + application.getGngRefId());
                appIds.remove(application.getGngRefId());
            }
        }
        logger.info("Sourcing Channel migration ended!");
        response = GngUtils.getBaseResponse(HttpStatus.OK, "Successfully updated application Ids : " + appIds);
        return response;
    }

    @Override
    public BaseResponse updateBranchMangerName(MigrationRequest migrationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        logger.info("BranchManger Name migration started!");
        Header header = migrationRequest.getHeader();
        List<String> refIds = migrationRequest.getRefIds();
        Map<String, Object> criteriaConditions = new HashMap<>();
        criteriaConditions.put("applicationRequest.header.institutionId", header.getInstitutionId());
        if(CollectionUtils.isNotEmpty(refIds))
            criteriaConditions.put("_id$", refIds);
        criteriaConditions.put("applicationRequest.appMetaData.branchV2.branchManagerName", "");
        criteriaConditions.put("applicationRequest.header.applicationSource!", null);

        List<String> includes = new ArrayList<>();
        includes.add("applicationRequest.request.application.channel");

        List<GoNoGoCustomerApplication> applications = (List<GoNoGoCustomerApplication>)migrationRepository.fetch(
                GoNoGoCustomerApplication.class, criteriaConditions, includes);

        List<String> appIds = applications.stream().map(GoNoGoCustomerApplication :: getGngRefId).collect(Collectors.toList());
        List<Integer> branchIds =  new ArrayList<>();
        for(GoNoGoCustomerApplication application : applications){
            branchIds.add(application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchId());
        }
        Map<Integer, String> branchIdManagerNameMap = fetchBranchIdMangerName(header.getInstitutionId(), branchIds, httpRequest);

        logger.info("Impacted Application Ids : " + appIds);
        for(GoNoGoCustomerApplication application : applications){
            try{
                Integer branchId = application.getApplicationRequest().getAppMetaData().getBranchV2().getBranchId();
                if(branchIdManagerNameMap.containsKey(branchId)) {
                    Map<String, Object> updateMap = new HashedMap();
                    updateMap.put("applicationRequest.appMetaData.branchV2.branchManagerName", branchIdManagerNameMap.get(branchId));
                    migrationRepository.updateApplication(application.getGngRefId(), updateMap);
                }
            } catch(Exception e) {
                logger.info("Not updated : " + application.getGngRefId());
                appIds.remove(application.getGngRefId());
            }
        }
        logger.info("BranchManger Name migration ended!");
        response = GngUtils.getBaseResponse(HttpStatus.OK, "Successfully updated application Ids : " + appIds);
        return response;
    }


    public BaseResponse updateDsaId(MigrationRequest migrationRequest) {
        BaseResponse baseResponse = null;
        try {
            boolean flag = false;
            int cnt = 0 ;
            MigrationResponse migrationResponse = new MigrationResponse();
             HashMap<String,Integer> UpdateSecId = new HashMap<>();
            migrationResponse.setUpdateSecId(UpdateSecId);
            if (migrationRequest != null) {
                if (CollectionUtils.isNotEmpty(migrationRequest.getDsaIdDataList())) {
                    for (DsaIdData dsaIdData : migrationRequest.getDsaIdDataList()) {
                        GoNoGoCustomerApplication gngCustomerApplication = migrationRepository.fetchGonogoCustomerByDsaId(migrationRequest.getHeader().getInstitutionId(), dsaIdData.getPrimaryId());
                        if (StringUtils.isNotEmpty(dsaIdData.getPrimaryId()) && CollectionUtils.isNotEmpty(dsaIdData.getSecondaryIdList())) {
                            if(gngCustomerApplication != null && gngCustomerApplication.getApplicationRequest().getAppMetaData().getDsaName() != null){
                               cnt = migrationRepository.updateDsaId(migrationRequest, dsaIdData.getSecondaryIdList(), dsaIdData.getPrimaryId(),
                                        gngCustomerApplication.getApplicationRequest().getAppMetaData().getDsaName(),
                                        gngCustomerApplication.getApplicationRequest().getAppMetaData().getDsaEmailId());
                            }else{
                                cnt = migrationRepository.updateDsaId(migrationRequest, dsaIdData.getSecondaryIdList(), dsaIdData.getPrimaryId(), null,null);
                            }
                            UpdateSecId.put(dsaIdData.getPrimaryId(), cnt);
                        }
                        cnt = 0;
                    }
                }
            }

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, migrationResponse);
        } catch (Exception ex) {
            logger.error("{} in updateDsaId ", ex.getStackTrace());
        }
        return baseResponse;
    }


    /* To change secondaryId by Primary Id for cases */
    @Override
    public BaseResponse bulkDsaIdCases(File uploadedFile, String institutionId) {
        BaseResponse baseResponse = null;
        try {
            MigrationRequest migrationRequest = new MigrationRequest();
            generateData( uploadedFile,  institutionId, migrationRequest);
            baseResponse = updateDsaId(migrationRequest);
            //updateAllocationInfo(migrationRequest);
        } catch (Exception e) {
            logger.error("{} stackStrace  , {} detail meassage", e.getStackTrace(), e.getMessage());
        }
        return baseResponse;
    }

    private void generateData(File uploadedFile, String institutionId,MigrationRequest migrationRequest) {
        try{
            BufferedReader reader = new BufferedReader(new FileReader(uploadedFile));
            List<DsaIdData> dsaIdDataList = new ArrayList<>();

            List<String> lines = reader.lines().collect(Collectors.toList());
            lines.remove(0); // this is heaer of csv
            String[] values ;
            DsaIdData dsaIdData = null;
            List<String> dsaIdList = null;
            for (String s : lines) {
                dsaIdData = new DsaIdData();
                values = s.split(",");
                dsaIdList = new ArrayList<>(Arrays.asList(values));
                dsaIdData.setPrimaryId(dsaIdList.get(0));
                dsaIdList.remove(0);
                dsaIdData.setSecondaryIdList(dsaIdList) ;
                dsaIdDataList.add(dsaIdData);
            }
            Header header = new Header();
            header.setInstitutionId(institutionId);
            migrationRequest.setHeader(header);
            migrationRequest.setDsaIdDataList(dsaIdDataList);
        }catch (Exception ex){
        logger.error("{}",ex.getStackTrace());
        }
    }

    @Override
    public BaseResponse bulkDsaIdCasesCount(File uploadedFile, String institutionId) {
        BaseResponse baseResponse = null;
        try {
            MigrationRequest migrationRequest = new MigrationRequest();
            generateData( uploadedFile,  institutionId, migrationRequest);
            baseResponse = toBeupdateDsaId(migrationRequest);
            //updateAllocationInfo(migrationRequest);
        } catch (Exception e) {
            logger.error("{} stackStrace  , {} detail meassage", e.getStackTrace(), e.getMessage());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse allocateInfoChange(File uploadedFile, String institutionId) {
        BaseResponse baseResponse = null;
        try {
            MigrationRequest migrationRequest = new MigrationRequest();
            generateData( uploadedFile,  institutionId, migrationRequest);
            baseResponse = updateAllocationInfo(migrationRequest);
            //updateAllocationInfo(migrationRequest);
        } catch (Exception e) {
            logger.error("{} stackStrace  , {} detail meassage", e.getStackTrace(), e.getMessage());
        }
        return baseResponse;
    }

    private BaseResponse toBeupdateDsaId(MigrationRequest migrationRequest) {
        BaseResponse baseResponse = null;
        try{
            MigrationResponse migrationResponse = new MigrationResponse();
            HashMap<String,Long> secIdCount =  new HashMap<>();
            migrationResponse.setSecIdCount(secIdCount);
            long cnt = 0 ;
            if (migrationRequest != null) {
                if (CollectionUtils.isNotEmpty(migrationRequest.getDsaIdDataList())) {
                    for (DsaIdData dsaIdData : migrationRequest.getDsaIdDataList()) {
                        if (StringUtils.isNotEmpty(dsaIdData.getPrimaryId()) && CollectionUtils.isNotEmpty(dsaIdData.getSecondaryIdList())) {
                            for (String secId : dsaIdData.getSecondaryIdList()){
                                cnt = migrationRepository.countDsaId(migrationRequest, secId);
                                if(cnt != 0){
                                    secIdCount.put(secId, cnt);
                                }
                                cnt = 0;
                            }
                        }
                    }
                }

            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, migrationResponse);
        }catch (Exception ex){
            logger.error("while counting {}", ex.getStackTrace());
        }
        return baseResponse;
    }

    private BaseResponse updateAllocationInfo(MigrationRequest migrationRequest) {
        BaseResponse baseResponse = null;
        try{
            int cnt = 0;
            MigrationResponse migrationResponse = new MigrationResponse();
            HashMap<String,Integer> UpdateSecId = new HashMap<>();
            migrationResponse.setUpdateSecId(UpdateSecId);
            if (migrationRequest != null) {
                if (CollectionUtils.isNotEmpty(migrationRequest.getDsaIdDataList())) {
                    for (DsaIdData dsaIdData : migrationRequest.getDsaIdDataList()) {
                        if (StringUtils.isNotEmpty(dsaIdData.getPrimaryId()) && CollectionUtils.isNotEmpty(dsaIdData.getSecondaryIdList())) {
                            for (String secId : dsaIdData.getSecondaryIdList()){
                                cnt = migrationRepository.updateAllocationInfo(migrationRequest, secId, dsaIdData.getPrimaryId());
                                UpdateSecId.put(dsaIdData.getPrimaryId(), cnt);
                            }
                        }
                    }
                }

            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, migrationResponse);
        }catch (Exception ex ){
            logger.error("stacktrace while updating allocationInfo {}",ex.getStackTrace());
            logger.error("message while updating allocationInfo {} ", ex.getMessage());
        }
        return baseResponse;
    }


    private Map<Integer,String> fetchBranchIdMangerName(String institutionId, List<Integer> branchIds, HttpServletRequest httpRequest) {
        Map<Integer,String> branchIdManagerNameMap = new HashMap<>();
        List<LoginServiceResponse> usersResponse = appConfigurationManager.getUsersDetailsFromUAM(institutionId, httpRequest);
        usersResponse.forEach(userDtls -> {
            if(CollectionUtils.isNotEmpty(userDtls.getBranches()))
                userDtls.getBranches().forEach(branch -> {
                    if(StringUtils.isNotEmpty(branch.getBranchManagerName()))
                        branchIdManagerNameMap.put(branch.getBranchId(), branch.getBranchManagerName());
                });
        });
        if(!branchIdManagerNameMap.keySet().containsAll(branchIds)){
            branchIds.removeAll(branchIdManagerNameMap.keySet());
        }
        return branchIdManagerNameMap;
    }


    @Override
    public BaseResponse changeInBankingDetail(String institutionId, boolean hit) {
        BaseResponse baseResponse = null;
        MigrationResponse migrationResponse = new MigrationResponse();
        int abbCount = 0;
        int avgSummationCount = 0;
        HashMap<String,Integer> UpdateSecId = new HashMap<>();

        List<CamDetails> camDetailList = migrationRepository.fetchCamDetails(institutionId);
        Iterator itr = camDetailList.iterator();
        CamDetails camDetail = null;
        while (itr.hasNext()){
            camDetail =  (CamDetails)itr.next();
            double sum =0;
            double totalSum = 0 ;
            if(CollectionUtils.isNotEmpty(camDetail.getBankingStatements())){
               for(BankingStatement bankingStatement :camDetail.getBankingStatements()){
                  if(bankingStatement.getAverageAbb() != 0){
                     sum = bankingStatement.getAverageAbb()+sum;
                     abbCount ++;
                  }
                  if(CollectionUtils.isNotEmpty(bankingStatement.getBankingTransactions())){
                     totalSum = (bankingStatement.getNetBusinessCreditTransctionsAmt()
                     / bankingStatement.getBankingTransactions().size())+ totalSum;
                     avgSummationCount ++;
                  }
               }
               camDetail.setAvgCreditSummation(totalSum/camDetail.getBankingStatements().size());
               camDetail.setTotalAbb(sum);
            }
            if(hit){
                  camDetail.setSbfc(true);
                  migrationRepository.updateCamDetail(camDetail.getRefId(),camDetail);
            }
        }
        UpdateSecId.put("Abb_cnt", abbCount);
        UpdateSecId.put("AvgSummation_Cnt", avgSummationCount);
        migrationResponse.setUpdateSecId(UpdateSecId);
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, migrationResponse);

        return baseResponse;
    }
}
