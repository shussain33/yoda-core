package com.softcell.service.impl;


import com.softcell.constants.FieldSeparator;
import com.softcell.constants.ReqResHelperConstants;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.LoggerRepository;
import com.softcell.gonogo.model.request.emudra.ESignRequest;
import com.softcell.gonogo.model.request.emudra.FTPRequest;
import com.softcell.gonogo.model.request.emudra.FTPResponse;
import com.softcell.gonogo.model.request.emudra.SFTPLog;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;


/**
 * @author shyamk
 */
@EnableAsync
@Service
public class AsyncJob {

    private static final Logger logger = LoggerFactory.getLogger(AsyncJob.class);

    @Autowired
    private LoggerRepository loggerRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Async
    public void sftpUploadAsync(ESignRequest eSignRequest, String responseXml) throws Exception {
        WFJobCommDomain sftpConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                eSignRequest.getHeader().getInstitutionId(), UrlType.SFTP_FILE.toValue());
        FTPRequest ftpRequest = new FTPRequest();
        FTPResponse ftpResponse = null;
        if (null != sftpConfig) {
            ftpRequest.setFile(responseXml);
            ftpRequest.setInstitutionId(eSignRequest.getHeader().getInstitutionId());
            String ftpUrl = Arrays.asList(sftpConfig.getBaseUrl(), sftpConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
            String emandateCount = applicationRepository.getUpdatedCountForEMandate(eSignRequest.getHeader().getInstitutionId());
            ftpRequest.setFileName("MMS-CREATE-HSBC-TVSCREDIT-" + GngDateUtil.toddMMyyyyTimeStamp(new Date()) + "-Esign" + emandateCount + "-INP.xml");

            ftpResponse = (FTPResponse) TransportUtils.postJsonRequest(ftpRequest, ftpUrl, FTPResponse.class);
        }

        SFTPLog sftpLog = SFTPLog.builder()
                .refId(eSignRequest.getRefId())
                .date(new Date())
                .institutionId(eSignRequest.getHeader().getInstitutionId())
                .status(null != ftpResponse ? ftpResponse.getStatus() : ReqResHelperConstants.failure.name())
                .file(ftpRequest.getFile())
                .fileName(ftpRequest.getFileName())
                .build();
        loggerRepository.saveSFTFLog(sftpLog);
    }

}
