package com.softcell.service.impl;


import com.softcell.constants.Constant;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.master.DealerRankingRequest;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.master.DealerRankingResponse;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.service.MasterManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author yogeshb
 */
@Service
public class MasterManagerImpl implements MasterManager {

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private LookupService lookupService;

    @Override
    public BaseResponse getAssetModelMasterList(ApplicationMetadataRequest applicationMetadataRequest) {

        List<AssetModelMaster> assetModelMasterList;
        BaseResponse baseResponse;

        String institutionId = applicationMetadataRequest.getHeader().getInstitutionId();
        String productName = applicationMetadataRequest.getHeader().getProduct().name();

        String modelNo = applicationMetadataRequest.getModelNo();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            assetModelMasterList = masterRepository.getAssetModelMasterDetails(modelNo, institutionId, productAliasName);

            if (null != assetModelMasterList && !assetModelMasterList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assetModelMasterList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAssetModelMasterListByTextSearch(
            String query) {

        List<AssetModelMaster> assetModelMasterList;
        BaseResponse baseResponse;

        assetModelMasterList = masterRepository.getAssetModelMasterByFullTextSearch(query);

        if (null != assetModelMasterList && !assetModelMasterList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assetModelMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getManufacturerList(
            ApplicationMetadataRequest metadataRequest) {

        metadataRequest.getHeader().setDateTime(new Date());
        BaseResponse baseResponse;
        Set<String> manufacturerList;

        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            manufacturerList = masterRepository.getManufacturer(metadataRequest, productAliasName);

            if (null != manufacturerList && !manufacturerList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, manufacturerList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getManufacturerDetailsList(ApplicationMetadataRequest metadataRequest) {
        metadataRequest.getHeader().setDateTime(new Date());
        BaseResponse baseResponse;
        List<ManufacturerMaster> manufacturerList;
        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        manufacturerList = masterRepository.getManufacturerDetailsList(metadataRequest);
        if (null != manufacturerList && !manufacturerList.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, manufacturerList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCategoryList(
            ApplicationMetadataRequest metadataRequest) {

        metadataRequest.getHeader().setDateTime(new Date());
        BaseResponse baseResponse;
        Set<String> categoryList;

        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            categoryList = masterRepository.getCategories(metadataRequest, productAliasName);

            if (null != categoryList && !categoryList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, categoryList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getMakeList(ApplicationMetadataRequest metadataRequest) {

        metadataRequest.getHeader().setDateTime(new Date());
        Set<String> makeList;
        BaseResponse baseResponse;

        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            makeList = masterRepository.getMakes(metadataRequest, productAliasName);

            if (null != makeList && !makeList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, makeList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getModelNumberList(
            ApplicationMetadataRequest metadataRequest) {

        metadataRequest.getHeader().setDateTime(new Date());
        Set<String> modelNoList;
        BaseResponse baseResponse;
        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            modelNoList = masterRepository.getModelNumbers(metadataRequest, productAliasName);

            if (null != modelNoList && !modelNoList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, modelNoList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getDealerRanking(
            DealerRankingRequest dealerRankingRequest) {
        DealerRankingResponse dealerRankingResponse = new DealerRankingResponse();


        DealerEmailMaster dealerEmailMaster = masterRepository
                .getDealerRanking(dealerRankingRequest.getHeader().getDealerId(), dealerRankingRequest.getHeader().getInstitutionId());
        if (null != dealerEmailMaster) {

            dealerRankingResponse.setResponseMsg("Dealer Ranking Found");
            dealerRankingResponse.setStatus(Status.SUCCESS.name());

            dealerRankingResponse.setDealerRanking(dealerEmailMaster
                    .getDealerComm());

        } else {
            dealerRankingResponse
                    .setResponseMsg("Dealer Record Not Found Against Dealer Id");
            dealerRankingResponse.setStatus(Status.FAILED.name());
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, dealerRankingResponse);
    }

    @Override
    public BaseResponse getListOfModelsWithOtherDetails(ApplicationMetadataRequest metadataRequest) {
        BaseResponse baseResponse;

        String institutionId = metadataRequest.getHeader().getInstitutionId();
        String productName = metadataRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            List<AssetModelMaster> assets = masterRepository.getModelsWithOtherDetails(metadataRequest, productAliasName);

            if (null != assets && !assets.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assets);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse saveDropDownMaster(DropdownMaster dropdownMaster) {
        BaseResponse baseResponse;
        List<HashMap<String, String>> dropDownValueDetailsList = dropdownMaster.getDropDownValueDetailsList();
        GenericResponse genericResponse = new GenericResponse();
        if (CollectionUtils.isNotEmpty(dropDownValueDetailsList)) {
            boolean result = masterRepository.saveDropDownMaster(dropdownMaster);
            if(result) {
                genericResponse.setStatus(Constant.SUCCESS);
            } else {
                genericResponse.setStatus(Constant.FAILED);
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
        } else {
            genericResponse.setStatus("Data fields are Empty");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, genericResponse);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getDropDownMaster(DropdownMasterRequest dropdownMaster) {
        BaseResponse baseResponse = null;
        List<DropdownMaster> response = masterRepository.fetchDropDownMaster(dropdownMaster);
        if(dropdownMaster.isProductWise()){
            separateDropDownMaster(response, dropdownMaster.getHeader().getProduct());
        }
        if (CollectionUtils.isNotEmpty(response)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, response);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    private void separateDropDownMaster(List<DropdownMaster> response, Product product) {
        List<HashMap<String, String>> dropdownMasterList = new ArrayList<>();
        for(DropdownMaster dropdownMaster : response){
            dropdownMasterList = new ArrayList<>();
            for(HashMap<String, String> map : dropdownMaster.getDropDownValueDetailsList()){
                if(StringUtils.equalsIgnoreCase(map.get("Product"),product.name()) ||
                        StringUtils.equalsIgnoreCase(map.get("sProduct"),product.name())){
                    if(!StringUtils.equalsIgnoreCase(map.get("dropDownType"), "Loan_CHARGES_MASTER")||
                            !StringUtils.equalsIgnoreCase(map.get("dropDownType"), "EMI_DUE_DATE") ){
                        dropdownMasterList.add(map);
                    }
                }
            }
            dropdownMaster.setDropDownValueDetailsList(dropdownMasterList);
        }
    }


}
