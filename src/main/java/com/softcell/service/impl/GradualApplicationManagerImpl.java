package com.softcell.service.impl;

import com.softcell.constants.CustomHttpStatus;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.service.ConfigurationManager;
import com.softcell.service.GradualApplicationManager;
import com.softcell.utils.DeviceTraceUtility;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.PartialWorkFlow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


/**
 * @author yogeshb
 */
@Service
public class GradualApplicationManagerImpl implements GradualApplicationManager {

    private static Logger logger = LoggerFactory.getLogger(GradualApplicationManagerImpl.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private PartialWorkFlow partialWorkFlow;

    @Override
    public BaseResponse initApplicationLifeCycle(
            ApplicationRequest applicationRequest, String stepId,
            HttpServletRequest httpRequest) throws Exception {

        BaseResponse response = null;
        logger.debug("initApplicationLifeCycle service called for setp {}", stepId);
        applicationRequest.getHeader().setDateTime(new Date());

        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());

        /* version check point  */
        if (!configurationManager.isValidVersion(applicationRequest
                .getHeader().getInstitutionId(), applicationRequest
                .getHeader().getApplicationSource())) {

            activityLog.setCustomMsg(
                    new StringBuilder().append(ErrorCode.INVALID_VERSION_DSCR).append(" | ")
                            .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                            .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                            .append(Error.SEVERITY.CRITICAL.name()).toString());

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            // Persist activityLog in DB
            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLog);

        } else {
            switch (stepId) {

                /**
                 * Step1 is used for create unique reference id, here Header is compulsory object.
                 */
                case "step1": {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, partialWorkFlow.workFlowStepOne(applicationRequest,
                            httpRequest));
                    break;
                }

                /**
                 * step2 and step3 we are just updating the application's remaining
                 * data and receiving whole object in the request hence same thing
                 * happens in both the cases.
                 */
                case "step2": {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, partialWorkFlow.workFlowStepTwo(applicationRequest,
                            httpRequest));
                    break;
                }
                case "step3": {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, partialWorkFlow.workFlowStepThree(applicationRequest,
                            httpRequest));
                    break;
                }

                /**
                 * Actual workflow starts from this step,all external module services call
                 * initiate from this step.
                 */
                case "step4": {
                    applicationRequest.getHeader().setDateTime(new Date());
                    applicationRequest.setQdeDecision(false);
                     /* IMEI and client details put into DeviceInfo domain under Header.
                     */
                    DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
                    deviceTraceUtility.populateDeviceInfo(applicationRequest.getHeader(), httpRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK, partialWorkFlow.workFlowStartStep(applicationRequest,
                            httpRequest));
                    break;
                }

                /**
                 * Dedupe Check flow for TVS
                 */
                case "step5": {
                    DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
                    deviceTraceUtility.populateDeviceInfo(applicationRequest.getHeader(), httpRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK,partialWorkFlow.workFlowDedupeStep(applicationRequest, httpRequest));
                    break;
                }

                /**
                 * remaining workflow execution step for TVS
                 */
                case "step6": {
                    DeviceTraceUtility deviceTraceUtility = new DeviceTraceUtility();
                    deviceTraceUtility.populateDeviceInfo(applicationRequest.getHeader(), httpRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK, partialWorkFlow.startRemainingWorkflow(applicationRequest, httpRequest));
                    break;
                }


                /**
                 * Wrong step
                 */
                default: {
                    logger.warn("Ref Id  STEP is not define {}", applicationRequest.getRefID());
                    activityLog.setCustomMsg(
                            new StringBuilder().append(ErrorCode.INVALID_REQUEST_DSCR).append(" | ")
                                    .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                                    .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                                    .append(Error.SEVERITY.CRITICAL.name()).toString());

                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.INVALID_REQUEST_DSCR)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
                    // Persist activityLog in DB
                    logger.debug(String.format(" Publishing activity log from PartialWorkflow in thread %s",
                            Thread.currentThread().getName()));
                    this.activityEventPublisher.publishEvent(activityLog);
                }
            }
        }
        return response;
    }
}