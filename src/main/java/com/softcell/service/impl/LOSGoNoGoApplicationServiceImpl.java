
package com.softcell.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.los.LOSGoNoGoApplicationDao;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.factory.tvscdlosintegration.*;
import com.softcell.gonogo.utils.LosTvsHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.LOSGoNoGoApplicationService;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service("gonogoCustomerApplicationService")
public class LOSGoNoGoApplicationServiceImpl implements LOSGoNoGoApplicationService {
	private final Logger logger = LoggerFactory.getLogger(LOSGoNoGoApplicationServiceImpl.class);

	@Autowired
	LOSGoNoGoApplicationDao gonogoApplicationDao;

	@Autowired
	private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

	@Autowired
	private LosTvsInitiateBuilder losTvsInitiateBuilder;

	@Autowired
    private LosTvsHelper losTvsHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
	private LosTvsGroupThreeReqBuilder losTvsGroupThreeReqBuilder;

    @Autowired
	private LosTvsGroupOneReqBuilder losTvsGroupOneReqBuilder;

	@Autowired
	private LosTvsGroupTwoReqBuilder losTvsGroupTwoReqBuilder;

	@Autowired
	LosTvsGroupFourReqBuilder losTvsGroupFourReqBuilder;

    @Autowired
	LosTvsGroupFiveReqBuilder losTvsGroupFiveReqBuilder;

    @Autowired
	LosTvsGroupStatusBuilder losTvsGroupStatusBuilder;
	/*@Autowired
	Group2Builder group2Builder;*/

	Gson gson = new Gson();

	@Override
	public List<GoNoGoCustomerApplication> getAllgoNoGoCustomerApplicationslistRecord() {
		try {
			logger.debug("in getAllgoNoGoCustomerApplicationslistRecord serviceIMPL");
			List<GoNoGoCustomerApplication> goCustomerApplications = gonogoApplicationDao
					.getAllgoNoGoCustomerApplicationslistRecord();
			return goCustomerApplications;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured etAllgoNoGoCustomerApplicationslistRecord serviceIMPL",e);
			return null;
		}
	}

	@Override
	public GoNoGoCustomerApplication getGONOGOApplicationAsPerCaseId(String gngRefId) {
		logger.debug("in getGONOGOApplicationAsPerCaseId serviceIMPL:::" + gngRefId);
		AuditingInfo.AuditingInfoBuilder gonogoAuditingInfo = AuditingInfo.builder();
		try {
			String groupName = LosTvsCollectionNameEnums.GoNoGoCustomerApplication_query.name();
			String referenceId = gngRefId;
			Date requestDt = new Date();

			GoNoGoCustomerApplication goNoGoCustomerApplication = gonogoApplicationDao
					.getGONOGOApplicationAsPerCaseId(gngRefId);
			auditCollectionLog(groupName,requestDt,referenceId,goNoGoCustomerApplication,referenceId);
			return goNoGoCustomerApplication;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getGONOGOApplicationAsPerCaseId serviceIMPL::",e);
			return null;
		}
	}

	@Override
	public List<GoNoGoCustomerApplication> getsearchRecord(String date, String stage) {
		// TODO Auto-generated method stub
		logger.debug("in getsearchRecord serviceIMPL");
		try {
			List<GoNoGoCustomerApplication> alllist = gonogoApplicationDao.getsearchRecord(date, stage);
			// serviceImpl:::");
			return alllist;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error occured in getsearchRecord serviceiMPL",e);
			return null;
		}
	}



    private InsertOrUpdateTvsRecordGroupThree getLosTvsRequestGroupThreeJson(GoNoGoCustomerApplication gonogoCustomerApplication, PostIpaRequest postIpaRequest, SerialNumberInfo serialNumberInfo, InsurancePremiumDetails insurancePremiumDetails, SchemeMasterData schemeMasterData, ExtendedWarrantyDetails extendedWarrantyDetails) {

	    return losTvsGroupThreeReqBuilder.buildLosTvsGroupThreeRequest(gonogoCustomerApplication, postIpaRequest, serialNumberInfo, insurancePremiumDetails, schemeMasterData, extendedWarrantyDetails);
	}

	@Override
	public PostIpaRequest getpostIpaRequestBysRefID(String sRefID) {
		logger.debug("in getpostIpaRequestBysRefID"+sRefID);
		try {
			String groupName = LosTvsCollectionNameEnums.PostIpaRequest_query.name();
			String referenceId = sRefID;
			Date requestDt = new Date();

			PostIpaRequest postIpaRequest = gonogoApplicationDao.getpostIpaRequestBysRefID(sRefID);
			auditCollectionLog(groupName,requestDt,referenceId,postIpaRequest,referenceId);

			return postIpaRequest;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getpostIpaRequestBysRefID serviceImpl",e);
			return null;
		}
	}

	@Override
	public SerialNumberInfo getSerialNumberInfoBysRefID(String refID) {
		logger.debug("in getSerialNumberInfoBysRefID serviceIMPL:::" + refID);
		try {
			String groupName = LosTvsCollectionNameEnums.SerialNumberInfo_query.name();
			String referenceId = refID;
			Date requestDt = new Date();

			SerialNumberInfo serialNumberInfo = gonogoApplicationDao.getSerialNumberInfoBysRefID(refID);
			auditCollectionLog(groupName,requestDt,referenceId,serialNumberInfo,referenceId);
			return serialNumberInfo;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getSerialNumberInfoBysRefID serviceIMPL",e);
			return null;
		}
	}

	@Override
	public InsurancePremiumDetails getinsurancePremiumDetailsBysRefID(String refID) {
		logger.debug("in getinsurancePremiumDetailsBysRefID serviceIMPL:::" + refID);
		try {
			String groupName = LosTvsCollectionNameEnums.InsurancePremiumDetails_query.name();
			String referenceId = refID;
			Date requestDt = new Date();

			InsurancePremiumDetails insurancePremiumDetails = gonogoApplicationDao
					.getinsurancePremiumDetailsBysRefID(refID);
			auditCollectionLog(groupName,requestDt,referenceId,insurancePremiumDetails,referenceId);
			return insurancePremiumDetails;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getinsurancePremiumDetailsBysRefID:",e);
			return null;
		}
	}

	@Override
	public ExtendedWarrantyDetails getextendedWarrantyDetailsBysRefID(String refID) {
		logger.debug("in getextendedWarrantyDetailsBysRefID serviceIMPL:::" + refID);
		String groupName = LosTvsCollectionNameEnums.ExtendedWarrantyDetails_query.name();
		String referenceId = refID;
		Date requestDt = new Date();

		try {
			ExtendedWarrantyDetails ExtendedWarrantyDetails = gonogoApplicationDao
					.getextendedWarrantyDetailsBysRefID(refID);
			auditCollectionLog(groupName,requestDt,referenceId,ExtendedWarrantyDetails,referenceId);
			return ExtendedWarrantyDetails;
		} catch (Exception e) {
			logger.error("Error occured in getextendedWarrantyDetailsBysRefID:",e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public SchemeMasterData getschemeMasterDataBysRefID(String schemeID) {
		return null;
	}


	public SchemeMasterData getschemeMasterDataBysRefID(PostIpaRequest postIPARequest) {
		SchemeMasterData schemeMasterData=null;
		String groupName = LosTvsCollectionNameEnums.SchemeMasterData_query.name();
		String referenceId = postIPARequest.getRefID();
		Date requestDt = new Date();
		try {
			if (postIPARequest != null) {
				if (postIPARequest.getPostIPA() != null) {
					if (postIPARequest.getPostIPA().getScheme() != null
							&& StringUtils.isNotBlank(postIPARequest.getPostIPA().getScheme())) {
						logger.debug("before fetching schemeId::" + postIPARequest.getPostIPA().getScheme());


						schemeMasterData = gonogoApplicationDao.getschemeMasterDataBysRefID(postIPARequest.getPostIPA().getScheme());
						auditCollectionLog(groupName,requestDt,referenceId,schemeMasterData,postIPARequest.getPostIPA().getScheme());
					}else{
						auditCollectionLog(groupName,requestDt,referenceId,null,null);
					}
				}
			}
			return schemeMasterData;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getschemeMasterDataBysRefID:",e);
			return null;
		}
	}

    private InsertOrUpdateTvsRecordGroupOne getLosTvsRequestGroupOneJson(GoNoGoCustomerApplication gonogoCustomerApplication, SerialNumberInfo serialNumberInfo) {

		logger.debug("in InsertOrUpdateTvsRecordGroupOne serviceIMPL");
		return losTvsGroupOneReqBuilder.buildLosTvsGroupOneRequest(gonogoCustomerApplication,serialNumberInfo);
	}


    private InsertOrUpdateTvsRecordGroupFour getLosTvsRequestGroupFourJson(ExtendedWarrantyDetails extendedWarrantyDetails, List<ESignedLog> eSignedLog, GoNoGoCustomerApplication gonogoCustomerApplication, InsurancePremiumDetails insurancePremiumDetails, SerialNumberInfo serialNumberInfo) {

		logger.debug("in InsertOrUpdateTvsRecordForGroup004 serviceIMPL");

		return losTvsGroupFourReqBuilder.buildLosTvsGroupFourRequest(extendedWarrantyDetails,eSignedLog,gonogoCustomerApplication,insurancePremiumDetails,serialNumberInfo);
	}

    private InsertOrUpdateTvsRecordGroupFive getLosTvsRequestGroupFiveJson(GoNoGoCustomerApplication gonogoCustomerApplication, DocumentListForGroupFive documentListForGroupFive) {

		logger.debug("in InsertOrUpdateTvsRecordGroupFive serviceIMPL");

		return losTvsGroupFiveReqBuilder.buildTvsLosGroupFiveRequest(gonogoCustomerApplication,documentListForGroupFive);
	}

	private InsertOrUpdateTvsRecordGroupTwo getLosTvsRequestGroupTwoJson(GoNoGoCustomerApplication gonogoCustomerApplication) {
		logger.debug("in InsertOrUpdateTvsRecordGroupFive serviceIMPL");
		return losTvsGroupTwoReqBuilder.buildLosTvsGroupTwoRequest(gonogoCustomerApplication);
	}


	public List<ESignedLog> getesignedLogByrefIdBysRefID(String refID) {
		logger.debug("in getesignedLogByrefIdBysRefID serviceIPL:::" + refID);
        List<ESignedLog> eSignedLogList = new ArrayList<ESignedLog>();
		String groupName = LosTvsCollectionNameEnums.ESignedLog_query.name();
		String referenceId = refID;
		Date requestDt = new Date();
		try {
            eSignedLogList = gonogoApplicationDao.getesignedLogByrefIdBysRefID(refID);
			auditCollectionLog(groupName,requestDt,referenceId,eSignedLogList,referenceId);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getesignedLogByrefIdBysRefID",e);

		}

		return eSignedLogList;
	}


	@Override
	public DocumentListForGroupFive getUploadFileDetailsAsPerCaseId(String gngRefId) {
		String groupName = LosTvsCollectionNameEnums.DocumentListForGroupFive_query.name();
		String referenceId = gngRefId;
		Date requestDt = new Date();

		try {
			logger.debug("in getUploadFileDetailsAsPerCaseId serviceIMPL");
			DocumentListForGroupFive DocumentListForGroupFive = gonogoApplicationDao
					.getUploadFileDetailsAsPerCaseId(gngRefId);
			auditCollectionLog(groupName,requestDt,referenceId,DocumentListForGroupFive,referenceId);
			for (int i = 0; i < DocumentListForGroupFive.getDmsDocList().size(); i++) {
				logger.debug(
						"filename in getDmsDocList" + DocumentListForGroupFive.getDmsDocList().get(i).getFilename());
			}
			for (int i = 0; i < DocumentListForGroupFive.getDmsDocList().size(); i++) {
				logger.debug("filename in getPdfsDocList"
						+ DocumentListForGroupFive.getPdfsDocList().get(i).getFilename());
			}
			return DocumentListForGroupFive;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Error occured in getUploadFileDetailsAsPerCaseId serviceIMPL",e);
			return null;
		}

	}

	@Override
	public FileUploadRequest getfileUploadRequestAsPerCaseId(String gngRefId) {
		logger.debug("in getUploadFileDetailsAsPerCaseId");
		try {
			FileUploadRequest fileUploadRequest = gonogoApplicationDao.getfileUploadRequestAsPerCaseId(gngRefId);

			return fileUploadRequest;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error occured in getfileUploadRequestAsPerCaseId",e);
			return null;
		}

	}


	@Override
	public TvsGroupServiceStatus getTvsGroupServiceStatus(String gngRefI) {
		try {
			TvsGroupServiceStatus TvsGroupServiceStatus = gonogoApplicationDao.getTvsGroupServiceStatus(gngRefI);

			return TvsGroupServiceStatus;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void saveTvsGroupServicStatus(TvsGroupServiceStatus groupService_Status) {
		gonogoApplicationDao.saveTvsGroupServicStatus(groupService_Status);
	}


	@Override
	public List<AuditingInfo> getAuditLogById(String _id) {

		List<AuditingInfo> AuditingInfo = gonogoApplicationDao.getAuditLogById(_id);

		return AuditingInfo;
	}

	@Override
	public List<AuditingInfo> getCompleteAuditLog() {

		List<AuditingInfo> AuditList =  new ArrayList<AuditingInfo>();
		AuditList = gonogoApplicationDao.getCompleteAuditLog();
		return AuditList;
	}

	@Override
	public void postAuditingInfo(AuditingInfo PostAuditingInfo)
	{
		gonogoApplicationDao.postAuditingInfo(PostAuditingInfo);
	}

	public void constructAuditInfoLog(BaseResponse baseResponse, Object object, String groupName,Date RequestDateTime, String action) throws Exception{


		AuditingInfo.AuditingInfoBuilder auditingInfo = AuditingInfo.builder();
		String caseId = "";
		String requestJson="";

		if(StringUtils.equalsIgnoreCase(LosTvsGroupEnums.Initiate.name(),groupName)){
			auditingInfo.requestDateTime(RequestDateTime)
					.endPointService(LosTvsEndPointServiceEnums.invokeTVSIntiateService.name())
					.groupName(LosTvsGroupEnums.Initiate.name())
					.action(action);

			TvsIntiateData intiateData = (TvsIntiateData) object;

			caseId = intiateData.getProspectid();
			requestJson = JsonUtil.ObjectToString(intiateData);
			auditingInfo.responseDateTime(new Date())
					.prospectId(caseId)
					.requestJson(requestJson);
		}

		if(StringUtils.equalsIgnoreCase(LosTvsGroupEnums.GroupThree.name(),groupName)){
			auditingInfo.requestDateTime(RequestDateTime)
					.endPointService(LosTvsEndPointServiceEnums.invokeTvsGroupThreeCreateService.name())
					.groupName(LosTvsGroupEnums.GroupThree.name())
					.action(action);

			InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordGroupThree = (InsertOrUpdateTvsRecordGroupThree)object;

			caseId = insertOrUpdateTvsRecordGroupThree.getCDLos().getProspectid();
			requestJson = JsonUtil.ObjectToString(insertOrUpdateTvsRecordGroupThree);
			auditingInfo.responseDateTime(new Date())
					.prospectId(caseId)
					.requestJson(requestJson);
		}

		if(StringUtils.equalsIgnoreCase(LosTvsGroupEnums.GroupOne.name(),groupName)){
			auditingInfo.requestDateTime(RequestDateTime)
					.endPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupOne.name())
					.groupName(LosTvsGroupEnums.GroupOne.name())
					.action(action);

			InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOne = (InsertOrUpdateTvsRecordGroupOne)object;

			caseId = insertOrUpdateTvsRecordGroupOne.getCDLos().getProspectid();
			requestJson = JsonUtil.ObjectToString(insertOrUpdateTvsRecordGroupOne);

			auditingInfo.responseDateTime(new Date())
					.prospectId(caseId)
					.requestJson(requestJson);
		}

		if(StringUtils.equalsIgnoreCase(LosTvsGroupEnums.GroupFour.name(),groupName)){
			auditingInfo.requestDateTime(RequestDateTime)
					.endPointService(LosTvsEndPointServiceEnums.invokeTvsGroupFourCreateService.name())
					.groupName(LosTvsGroupEnums.GroupFour.name())
					.action(action);

			InsertOrUpdateTvsRecordGroupFour insertOrUpdateTvsRecordGroupFour = (InsertOrUpdateTvsRecordGroupFour)object;
			caseId = insertOrUpdateTvsRecordGroupFour.getCDLos().getProspectid();
			requestJson = JsonUtil.ObjectToString(insertOrUpdateTvsRecordGroupFour);

			auditingInfo.responseDateTime(new Date())
					.prospectId(caseId)
					.requestJson(requestJson);
		}

		if(StringUtils.equalsIgnoreCase(LosTvsGroupEnums.GroupFive.name(),groupName)){
			auditingInfo.requestDateTime(RequestDateTime)
					.endPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupFive.name())
					.groupName(LosTvsGroupEnums.GroupFive.name())
					.action(action);

			InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFive = (InsertOrUpdateTvsRecordGroupFive)object;
			caseId = insertOrUpdateTvsRecordGroupFive.getCDLos().getProspectid();
			requestJson = JsonUtil.ObjectToString(insertOrUpdateTvsRecordGroupFive);

			auditingInfo.responseDateTime(new Date())
					.prospectId(caseId)
					.requestJson(requestJson);
		}
		TVSCdLosResponse tvsCdLosGroupResponse = (TVSCdLosResponse)baseResponse.getPayload().getT();
		auditingInfo
				.responseJson(gson.toJson(tvsCdLosGroupResponse))
				.statusCode(tvsCdLosGroupResponse.getStatusCode())
				.statusMessage(tvsCdLosGroupResponse.getStatusMessage())
				.startPointService(LosTvsStartPointService.tvscdlos.name())
				.status(gson.toJson(tvsCdLosGroupResponse))
				//if ((AuditingInfoGroupThreeAuditingInfoGroupThree.getSource() != null) || (!(AuditingInfoGroupThree.getSource() == "Dashboard")))
				//{
				.reprocess(LosTvsGroupStatusLogEnums.N.name());
		//}
		//	else{AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());}
		gonogoApplicationDao.postAuditingInfo(auditingInfo.build());
	}

	public void auditCollectionLog(String groupName,Date RequestDateTime, String prospectId,Object object,String requestJson) throws Exception {
		AuditingInfo.AuditingInfoBuilder auditingInfo = AuditingInfo.builder();

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.GoNoGoCustomerApplication_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));

		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.DocumentListForGroupFive_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));


		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.ESignedLog_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));


		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.ExtendedWarrantyDetails_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));


		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.InsurancePremiumDetails_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));


		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.PostIpaRequest_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));

		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.SchemeMasterData_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));

		}

		if (StringUtils.equalsIgnoreCase(LosTvsCollectionNameEnums.SerialNumberInfo_query.name(), groupName)) {
			auditingInfo.prospectId(prospectId)
					.requestDateTime(RequestDateTime)
					.responseDateTime(new Date())
					.requestJson(requestJson)
					.responseJson(gson.toJson(object));
		}
		auditingInfo.action("")
				.groupName(groupName)
				.reprocess("")
				.source("")
				.status("")
				.statusCode("")
				.statusMessage("")
				.user("")
				.startPointService("")
				.endPointService("");

		gonogoApplicationDao.postAuditingInfo(auditingInfo.build());

	}


	@Override
	public BaseResponse invokeTVSIntiateService(LosTvsRequest losTvsRequest, TvsIntiateData tvsIntiateData) throws Exception{
		    BaseResponse baseResponse =null;
            Collection<Error> errors = new ArrayList<Error>();

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

			WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
					, UrlType.LOSTVS_INITIATE.toValue());

			if (null != lostvsconfig) {

				InitiateRequest initiateRequest = losTvsInitiateBuilder.buildLosTvsInitiateRequest(lostvsconfig,tvsIntiateData);
				TVSCdLosResponse tvsCdLosResponse = callToLosTvsInitiate(initiateRequest, lostvsconfig);

				if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
					baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
				}else{
					logger.warn("get error from Connector api for {}", UrlType.LOSTVS_INITIATE.name());
					errors.add(Error.builder()
							//.id(tvsCdLosResponse.getError().getType())
							.message(tvsCdLosResponse.getError().getMessage())
							.level(Error.SEVERITY.HIGH.name())
							.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
							.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
							.build());

					baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                    activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                    activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
				}
			}else{
				logger.warn("Configuration not found for {}", UrlType.LOSTVS_INITIATE.name());
				errors.add(Error.builder()
						.message(ErrorCode.CONFIGURATION_NOT_FOUND)
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.level(Error.SEVERITY.CRITICAL.name())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
                activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
			}
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);

		return baseResponse;

	}

	@Override
	public BaseResponse invokeTVSGroupOneService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneResponse) throws Exception{
		BaseResponse baseResponse =null;
		Collection<Error> errors = new ArrayList<Error>();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

        WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
				, UrlType.LOSTVS_GROUP_ONE.toValue());

		if (null != lostvsconfig) {

			TVSGroupOneRequest tvsGroupOneRequest = losTvsInitiateBuilder.buildLosTvsGroupOneRequest(lostvsconfig,insertOrUpdateTvsRecordGroupOneResponse);
			TVSCdLosResponse tvsCdLosResponse = callToLosTvsGroupOne(tvsGroupOneRequest, lostvsconfig);

			if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
				baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
			}else{
				logger.warn("get error from Connector api for {}", UrlType.LOSTVS_GROUP_ONE.name());
				errors.add(Error.builder()
						.id(tvsCdLosResponse.getError().getType())
						.message(tvsCdLosResponse.getError().getMessage())
						.level(Error.SEVERITY.HIGH.name())
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
			}
		}else{
			logger.warn("Configuration not found for {}", UrlType.LOSTVS_GROUP_ONE.name());
			errors.add(Error.builder()
					.message(ErrorCode.CONFIGURATION_NOT_FOUND)
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.CRITICAL.name())
					.build());

			baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
		}

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
		return baseResponse;

	}

	@Override
	public BaseResponse invokeTVSGroupTwoService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoResponse) throws Exception {
		BaseResponse baseResponse =null;
		Collection<Error> errors = new ArrayList<Error>();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

		WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
				, UrlType.LOSTVS_GROUP_TWO.toValue());

		if (null != lostvsconfig) {

			TVSGroupTwoRequest tvsGroupTwoRequest = losTvsInitiateBuilder.buildLosTvsGroupTwoRequest(lostvsconfig,insertOrUpdateTvsRecordGroupTwoResponse);
			TVSCdLosResponse tvsCdLosResponse = callToLosTvsGroupTwo(tvsGroupTwoRequest, lostvsconfig);

			if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
				baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
			}else{
				logger.warn("get error from Connector api for {}", UrlType.LOSTVS_GROUP_TWO.name());
				errors.add(Error.builder()
						.id(tvsCdLosResponse.getError().getType())
						.message(tvsCdLosResponse.getError().getMessage())
						.level(Error.SEVERITY.HIGH.name())
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
				activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
				activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
			}
		}else{
			logger.warn("Configuration not found for {}", UrlType.LOSTVS_GROUP_TWO.name());
			errors.add(Error.builder()
					.message(ErrorCode.CONFIGURATION_NOT_FOUND)
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.CRITICAL.name())
					.build());

			baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
			activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
			activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
		}

		stopWatch.stop();
		activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
		activityEventPublisher.publishEvent(activityLog);
		return baseResponse;

	}

	@Override
	public BaseResponse invokeTVSGroupThreeService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordResponse) throws Exception{
		BaseResponse baseResponse =null;
		Collection<Error> errors = new ArrayList<Error>();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

        WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
				, UrlType.LOSTVS_GROUP_THREE.toValue());

		if (null != lostvsconfig) {

			TVSGroupThreeRequest tvsGroupThreeRequest = losTvsInitiateBuilder.buildLosTvsGroupThreeRequest(lostvsconfig,insertOrUpdateTvsRecordResponse);
			TVSCdLosResponse tvsCdLosResponse = callToLosTvsGroupThree(tvsGroupThreeRequest, lostvsconfig);

			if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
				baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
			}else{
				logger.warn("get error from Connector api for {}", UrlType.LOSTVS_GROUP_THREE.name());
				errors.add(Error.builder()
						.id(tvsCdLosResponse.getError().getType())
						.message(tvsCdLosResponse.getError().getMessage())
						.level(Error.SEVERITY.HIGH.name())
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
			}
		}else{
			logger.warn("Configuration not found for {}", UrlType.LOSTVS_GROUP_THREE.name());
			errors.add(Error.builder()
					.message(ErrorCode.CONFIGURATION_NOT_FOUND)
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.CRITICAL.name())
					.build());

			baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
		}

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
		return baseResponse;
	}

	@Override
	public BaseResponse invokeTVSGroupFourService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupFour groupFourReturn) throws Exception{
		BaseResponse baseResponse =null;
		Collection<Error> errors = new ArrayList<Error>();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

		WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
				, UrlType.LOSTVS_GROUP_FOUR.toValue());

		if (null != lostvsconfig) {

			TVSGroupFourRequest tvsGroupFourRequest = losTvsInitiateBuilder.buildLosTvsGroupFourRequest(lostvsconfig,groupFourReturn);
			TVSCdLosResponse tvsCdLosResponse = callToLosTvsGroupFour(tvsGroupFourRequest, lostvsconfig);

			if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
				baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
			}else{
				logger.warn("get error from Connector api for {}", UrlType.LOSTVS_GROUP_FOUR.name());
				errors.add(Error.builder()
						.id(tvsCdLosResponse.getError().getType())
						.message(tvsCdLosResponse.getError().getMessage())
						.level(Error.SEVERITY.HIGH.name())
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
			}
		}else{
			logger.warn("Configuration not found for {}", UrlType.LOSTVS_GROUP_FOUR.name());
			errors.add(Error.builder()
					.message(ErrorCode.CONFIGURATION_NOT_FOUND)
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.CRITICAL.name())
					.build());

			baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
		}

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
		return baseResponse;
	}

	@Override
	public BaseResponse invokeTVSGroupFiveService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveResponse) throws Exception{
		BaseResponse baseResponse =null;
		Collection<Error> errors = new ArrayList<Error>();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = losTvsHelper.createActivityLog(null, losTvsRequest.getHeader());

		WFJobCommDomain lostvsconfig = workFlowCommunicationManager.getWfCommDomainJobByType(losTvsRequest.getHeader().getInstitutionId()
				, UrlType.LOSTVS_GROUP_FIVE.toValue());

		if (null != lostvsconfig) {

			TVSGroupFiveRequest tvsGroupFiveRequest = losTvsInitiateBuilder.buildLosTvsGroupFiveRequest(lostvsconfig,insertOrUpdateTvsRecordGroupFiveResponse);
			TVSCdLosResponse tvsCdLosResponse = callToLosTvsGroupFive(tvsGroupFiveRequest, lostvsconfig);

			if (null != tvsCdLosResponse && null == tvsCdLosResponse.getError()) {
				baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosResponse);
			}else{
				logger.warn("get error from Connector api for {}", UrlType.LOSTVS_GROUP_FIVE.name());
				errors.add(Error.builder()
						.id(tvsCdLosResponse.getError().getType())
						.message(tvsCdLosResponse.getError().getMessage())
						.level(Error.SEVERITY.HIGH.name())
						.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
						.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
						.build());

				baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());

            }
		}else{
			logger.warn("Configuration not found for {}", UrlType.LOSTVS_GROUP_FIVE.name());
			errors.add(Error.builder()
					.message(ErrorCode.CONFIGURATION_NOT_FOUND)
					.errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
					.errorType(Error.ERROR_TYPE.SYSTEM.toValue())
					.level(Error.SEVERITY.CRITICAL.name())
					.build());

			baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
		}

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
		return baseResponse;
	}

	@Override
	public BaseResponse pullAndTranformDataByCaseID(LosTvsRequest losTvsRequest) throws Exception{
		BaseResponse baseResponsePullAndTransform = null;
		String caseId = losTvsRequest.getRefID();
		List<TVSCdLosGroupResponse> tvsCdLosGroupResponseList = new ArrayList<TVSCdLosGroupResponse>();
		if(StringUtils.isNotBlank(caseId)){
		TvsIntiateData tvsIntiateData =  losTvsInitiateBuilder.buildLosTvsInitiateInputRequest(losTvsRequest);

			Date requestDT = new Date();
			//initiate connector call
			BaseResponse baseResponse = invokeTVSIntiateService(losTvsRequest,tvsIntiateData);
			constructAuditInfoLog(baseResponse,tvsIntiateData,LosTvsGroupEnums.Initiate.name(),requestDT,"");//Audit Call
			if(baseResponse!=null) {

                GoNoGoCustomerApplication gonogoCustomerApplication = getGONOGOApplicationAsPerCaseId(caseId);
                PostIpaRequest postIpaRequest = getpostIpaRequestBysRefID(caseId);
                SerialNumberInfo serialNumberInfo = getSerialNumberInfoBysRefID(caseId);
                InsurancePremiumDetails insurancePremiumDetails = getinsurancePremiumDetailsBysRefID(caseId);
                //getting schememaster by postipa
                SchemeMasterData schemeMasterData = getschemeMasterDataBysRefID(postIpaRequest);
                ExtendedWarrantyDetails extendedWarrantyDetails = getextendedWarrantyDetailsBysRefID(caseId);
                DocumentListForGroupFive documentListForGroupFive = getUploadFileDetailsAsPerCaseId(caseId);
                List<ESignedLog> eSignedLog = getesignedLogByrefIdBysRefID(caseId);


                InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordGroupThree = getLosTvsRequestGroupThreeJson(gonogoCustomerApplication,postIpaRequest,
                        serialNumberInfo,insurancePremiumDetails,schemeMasterData,extendedWarrantyDetails);
				InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneRequestJson = getLosTvsRequestGroupOneJson(gonogoCustomerApplication,serialNumberInfo);
				InsertOrUpdateTvsRecordGroupFour insertOrUpdateTvsRecordGroupFourRequestJson = getLosTvsRequestGroupFourJson(extendedWarrantyDetails,eSignedLog,gonogoCustomerApplication,insurancePremiumDetails,serialNumberInfo);
				InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveRequestJson = getLosTvsRequestGroupFiveJson(gonogoCustomerApplication,documentListForGroupFive);
				InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoRequestJson = null;
						//getLosTvsRequestGroupTwoJson(gonogoCustomerApplication);

				TvsGroupServiceStatus groupServiceStatus = getTvsGroupServiceStatus(caseId);

				if (groupServiceStatus == null) {

					groupServiceStatus = losTvsGroupStatusBuilder.setDefaultGroupStatus();

				}

				//insert or update for group three
				if(insertOrUpdateTvsRecordGroupThree!=null) {
					if (groupServiceStatus.getTVSGroup3Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupThree.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						String action = LosTvsActionEnums.I.name();
						Date requestDt = new Date();

						//group three connector call
						BaseResponse baseResponseGrpThree = invokeTVSGroupThreeService(losTvsRequest,insertOrUpdateTvsRecordGroupThree);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupThree,LosTvsGroupEnums.GroupThree.name(),requestDt,action);

						if(baseResponseGrpThree!=null){
						TVSCdLosResponse tvsCdLosGroupThreeCreateResponse = (TVSCdLosResponse)baseResponseGrpThree.getPayload().getT();
							logger.debug("baseResponseGrpThree insert"+ JsonUtil.ObjectToString(tvsCdLosGroupThreeCreateResponse));
							if(tvsCdLosGroupThreeCreateResponse!=null){
								logger.debug("tvsCdLosGroupThreeResponse::" + tvsCdLosGroupThreeCreateResponse.getStatusMessage());
								if (tvsCdLosGroupThreeCreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
									groupServiceStatus.setTVSGroup3Status(LosTvsGroupStatusLogEnums.Y.name());
								}

								TVSCdLosGroupResponse tvsCdLosGroupThreeResponse =createGroupThreeResponse(LosTvsGroupEnums.GroupThree.name(),LosTvsActionEnums.I.name(),
										tvsCdLosGroupThreeCreateResponse.getStatusCode(),tvsCdLosGroupThreeCreateResponse.getStatusMessage(),insertOrUpdateTvsRecordGroupThree);
								tvsCdLosGroupResponseList.add(tvsCdLosGroupThreeResponse);
							}
						}

					}else{
						//changing update action status
						CDLos cdLos = insertOrUpdateTvsRecordGroupThree.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupThree.setCDLos(cdLos);

						//group three connector call
						String action = LosTvsActionEnums.U.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpThree = invokeTVSGroupThreeService(losTvsRequest,insertOrUpdateTvsRecordGroupThree);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupThree,LosTvsGroupEnums.GroupThree.name(),requestDt,action);
						if(baseResponseGrpThree!=null){
						TVSCdLosResponse tvsCdLosGroupThreeUpdateResponse = (TVSCdLosResponse)baseResponseGrpThree.getPayload().getT();
                            logger.debug("baseResponseGrpThree update"+ JsonUtil.ObjectToString(tvsCdLosGroupThreeUpdateResponse));
                            if(tvsCdLosGroupThreeUpdateResponse!=null) {
                                TVSCdLosGroupResponse tvsCdLosGroupThreeResponse =createGroupThreeResponse(LosTvsGroupEnums.GroupThree.name(),LosTvsActionEnums.U.name(),
                                        tvsCdLosGroupThreeUpdateResponse.getStatusCode(),tvsCdLosGroupThreeUpdateResponse.getStatusMessage(),insertOrUpdateTvsRecordGroupThree);
                                tvsCdLosGroupResponseList.add(tvsCdLosGroupThreeResponse);
                            }
                        }
					}
				}//End of group three

				//insert or update group one
				if(insertOrUpdateTvsRecordGroupOneRequestJson!=null){
					if (groupServiceStatus.getTVSGroup1Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupOneRequestJson.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr001 insert" + caseId);

						//group one connector call
						String action = LosTvsActionEnums.I.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpOne = invokeTVSGroupOneService(losTvsRequest,insertOrUpdateTvsRecordGroupOneRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupOneRequestJson,LosTvsGroupEnums.GroupOne.name(),requestDt,action);

						if(baseResponseGrpOne!=null){
                            //castng response
                            TVSCdLosResponse tvsCdLosGroupOneResponse = (TVSCdLosResponse)baseResponseGrpOne.getPayload().getT();
                            logger.debug("baseResponseGrpOne Insert"+ JsonUtil.ObjectToString(tvsCdLosGroupOneResponse));
                            if(tvsCdLosGroupOneResponse!=null){
                                //changing group status to Y
                                if (tvsCdLosGroupOneResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
                                    groupServiceStatus.setTVSGroup1Status(LosTvsGroupStatusLogEnums.Y.name());
                                }
                                //populating json response for case id
                                TVSCdLosGroupResponse tvsGroupOneResponse = losTvsGroupStatusBuilder.createGroupOneResponse(LosTvsGroupEnums.GroupOne.name(),LosTvsActionEnums.I.name(),tvsCdLosGroupOneResponse.getStatusCode(),
                                        null,insertOrUpdateTvsRecordGroupOneRequestJson);
                                tvsCdLosGroupResponseList.add(tvsGroupOneResponse);
                            }

                        }
					}
					else{
						//changing update action status
						CDLos cdLos = insertOrUpdateTvsRecordGroupOneRequestJson.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupOneRequestJson.setCDLos(cdLos);

						//group one connector call
						String action = LosTvsActionEnums.U.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpOne = invokeTVSGroupOneService(losTvsRequest,insertOrUpdateTvsRecordGroupOneRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupOneRequestJson,LosTvsGroupEnums.GroupOne.name(),requestDt,action);
						if(baseResponseGrpOne!=null){
                            //castng response
                            TVSCdLosResponse tvsCdLosGroupOneResponse = (TVSCdLosResponse)baseResponseGrpOne.getPayload().getT();
                            logger.debug("baseResponseGrpOne update"+ JsonUtil.ObjectToString(tvsCdLosGroupOneResponse));
                            //populating json response for case id
                            TVSCdLosGroupResponse tvsGroupOneResponse = losTvsGroupStatusBuilder.createGroupOneResponse(LosTvsGroupEnums.GroupOne.name(),LosTvsActionEnums.U.name(),tvsCdLosGroupOneResponse.getStatusCode(),
                                    null,insertOrUpdateTvsRecordGroupOneRequestJson);
                            tvsCdLosGroupResponseList.add(tvsGroupOneResponse);
                        }
					}
				}//End of group one


				//insert or update group four
				if(insertOrUpdateTvsRecordGroupFourRequestJson!=null){

					if (groupServiceStatus.getTVSGroup4Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupFourRequestJson.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr004 insert" + caseId);

						//group four connector call
						String action = LosTvsActionEnums.I.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpFour = invokeTVSGroupFourService(losTvsRequest,insertOrUpdateTvsRecordGroupFourRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupFourRequestJson,LosTvsGroupEnums.GroupFour.name(),requestDt,action);

                        if(baseResponseGrpFour!=null){
                            //castng response
                            TVSCdLosResponse tvsCdLosGroupFourResponse = (TVSCdLosResponse)baseResponseGrpFour.getPayload().getT();
                            logger.debug("baseResponseGrpFour insert"+ JsonUtil.ObjectToString(tvsCdLosGroupFourResponse));
                            if(tvsCdLosGroupFourResponse!=null) {
                                logger.debug("tvsCdLosGroupFourCreateResponse response::"
                                        + tvsCdLosGroupFourResponse.getStatusMessage());
                                if (tvsCdLosGroupFourResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
                                    groupServiceStatus.setTVSGroup4Status(LosTvsGroupStatusLogEnums.Y.name());
                                }
                                //populating json response for case id
                                TVSCdLosGroupResponse tvsGroupFourResponse = losTvsGroupStatusBuilder.createGroupFourResponse(LosTvsGroupEnums.GroupFour.name(), LosTvsActionEnums.I.name(), tvsCdLosGroupFourResponse.getStatusCode(),
                                        tvsCdLosGroupFourResponse.getStatusMessage(), insertOrUpdateTvsRecordGroupFourRequestJson);
                                tvsCdLosGroupResponseList.add(tvsGroupFourResponse);
                            }
                        }
					}
					else{

						//changing update action status
						CDLos cdLos = insertOrUpdateTvsRecordGroupFourRequestJson.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupFourRequestJson.setCDLos(cdLos);
						logger.debug("in tvscdlos gr004 Update" + caseId);

						//group four connector call
						String action = LosTvsActionEnums.U.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpFour = invokeTVSGroupFourService(losTvsRequest,insertOrUpdateTvsRecordGroupFourRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupFourRequestJson,LosTvsGroupEnums.GroupFour.name(),requestDt,action);

						if(baseResponseGrpFour!=null){
                            TVSCdLosResponse tvsCdLosGroupFourResponse = (TVSCdLosResponse)baseResponseGrpFour.getPayload().getT();
                            logger.debug("baseResponseGrpFour update"+ JsonUtil.ObjectToString(tvsCdLosGroupFourResponse));
                            if(tvsCdLosGroupFourResponse!=null){
                                //populating json response for case id
                                TVSCdLosGroupResponse tvsGroupFourResponse = losTvsGroupStatusBuilder.createGroupFourResponse(LosTvsGroupEnums.GroupFour.name(), LosTvsActionEnums.U.name(), tvsCdLosGroupFourResponse.getStatusCode(),
                                        tvsCdLosGroupFourResponse.getStatusMessage(), insertOrUpdateTvsRecordGroupFourRequestJson);
                                tvsCdLosGroupResponseList.add(tvsGroupFourResponse);
                            }
                        }
					}

				}//End of group four

				if(insertOrUpdateTvsRecordGroupFiveRequestJson!=null){
					if (groupServiceStatus.getTVSGroup5Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupFiveRequestJson.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr005 insert"+ caseId);

						//Group five connector call
						String action = LosTvsActionEnums.I.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpFive = invokeTVSGroupFiveService(losTvsRequest,insertOrUpdateTvsRecordGroupFiveRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupFiveRequestJson,LosTvsGroupEnums.GroupFive.name(),requestDt,action);

                        if(baseResponseGrpFive!=null){
                            //cast response
                            TVSCdLosResponse tvsCdLosGroupFiveResponse = (TVSCdLosResponse)baseResponseGrpFive.getPayload().getT();
                            logger.debug("baseResponseGrpFive insert"+ JsonUtil.ObjectToString(tvsCdLosGroupFiveResponse));
                            //group status update
                            if(tvsCdLosGroupFiveResponse!=null){
                                if (tvsCdLosGroupFiveResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
                                    groupServiceStatus.setTVSGroup5Status(LosTvsGroupStatusLogEnums.Y.name());
                                }
                            }

                            //populating json response for case id
                            TVSCdLosGroupResponse tvsGroupFiveResponse = losTvsGroupStatusBuilder.createGroupFiveResponse(LosTvsGroupEnums.GroupFive.name(), LosTvsActionEnums.I.name(), tvsCdLosGroupFiveResponse.getStatusCode(),
                                    tvsCdLosGroupFiveResponse.getStatusMessage(), insertOrUpdateTvsRecordGroupFiveRequestJson);
                            tvsCdLosGroupResponseList.add(tvsGroupFiveResponse);
                        }
					}
					else{
						CDLos cdLos = insertOrUpdateTvsRecordGroupFiveRequestJson.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupFiveRequestJson.setCDLos(cdLos);

						//Group five connector call
						String action = LosTvsActionEnums.I.name();
						Date requestDt = new Date();
						BaseResponse baseResponseGrpFive = invokeTVSGroupFiveService(losTvsRequest,insertOrUpdateTvsRecordGroupFiveRequestJson);
						constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupFiveRequestJson,LosTvsGroupEnums.GroupFive.name(),requestDt,action);

                        if(baseResponseGrpFive!=null){
                            //cast response
                            TVSCdLosResponse tvsCdLosGroupFiveResponse = (TVSCdLosResponse)baseResponseGrpFive.getPayload().getT();
                            logger.debug("baseResponseGrpFive update"+ JsonUtil.ObjectToString(tvsCdLosGroupFiveResponse));
                            //populating json response for case id
                            TVSCdLosGroupResponse tvsGroupFiveResponse = losTvsGroupStatusBuilder.createGroupFiveResponse(LosTvsGroupEnums.GroupFive.name(), LosTvsActionEnums.U.name(), tvsCdLosGroupFiveResponse.getStatusCode(),
                                    tvsCdLosGroupFiveResponse.getStatusMessage(), insertOrUpdateTvsRecordGroupFiveRequestJson);
                            tvsCdLosGroupResponseList.add(tvsGroupFiveResponse);
                        }
					}
				}//End of group five


				//insert or update group Two
				if(insertOrUpdateTvsRecordGroupTwoRequestJson!=null){
					if (groupServiceStatus.getTVSGroup2Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupTwoRequestJson.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr002 insert" + caseId);

						//group two connector call
                        String action = LosTvsActionEnums.I.name();
                        Date requestDt = new Date();
						BaseResponse baseResponseGrpTwo = invokeTVSGroupTwoService(losTvsRequest,insertOrUpdateTvsRecordGroupTwoRequestJson);
                        constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupTwoRequestJson,LosTvsGroupEnums.GroupTwo.name(),requestDt,action);
						if(baseResponseGrpTwo!=null){
							//castng response
							TVSCdLosResponse tvsCdLosGroupTwoResponse = (TVSCdLosResponse)baseResponseGrpTwo.getPayload().getT();
							logger.debug("baseResponseGrpTwo insert"+ JsonUtil.ObjectToString(tvsCdLosGroupTwoResponse));
							if(tvsCdLosGroupTwoResponse!=null){
								//changing group status to Y
								if (tvsCdLosGroupTwoResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
									groupServiceStatus.setTVSGroup2Status(LosTvsGroupStatusLogEnums.Y.name());
								}
								//populating json response for case id
								TVSCdLosGroupResponse tvsGroupTwoResponse = losTvsGroupStatusBuilder.createGroupTwoResponse(LosTvsGroupEnums.GroupTwo.name(),LosTvsActionEnums.I.name(),tvsCdLosGroupTwoResponse.getStatusCode(),
										tvsCdLosGroupTwoResponse.getStatusMessage(),insertOrUpdateTvsRecordGroupTwoRequestJson);
								tvsCdLosGroupResponseList.add(tvsGroupTwoResponse);
							}
						}
					}
					else{
						//changing update action status
						CDLos cdLos = insertOrUpdateTvsRecordGroupTwoRequestJson.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupTwoRequestJson.setCDLos(cdLos);

						//group one connector call
                        String action = LosTvsActionEnums.U.name();
                        Date requestDt = new Date();
						BaseResponse baseResponseGrpTwo = invokeTVSGroupTwoService(losTvsRequest,insertOrUpdateTvsRecordGroupTwoRequestJson);
                        constructAuditInfoLog(baseResponse,insertOrUpdateTvsRecordGroupTwoRequestJson,LosTvsGroupEnums.GroupTwo.name(),requestDt,action);
						if(baseResponseGrpTwo!=null){
							//castng response
							TVSCdLosResponse tvsCdLosGroupTwoResponse = (TVSCdLosResponse)baseResponseGrpTwo.getPayload().getT();
							logger.debug("baseResponseGrpTwo update"+ JsonUtil.ObjectToString(tvsCdLosGroupTwoResponse));
							//populating json response for case id
							TVSCdLosGroupResponse tvsGroupTwoResponse = losTvsGroupStatusBuilder.createGroupTwoResponse(LosTvsGroupEnums.GroupTwo.name(),LosTvsActionEnums.U.name(),tvsCdLosGroupTwoResponse.getStatusCode(),
									tvsCdLosGroupTwoResponse.getStatusMessage(),insertOrUpdateTvsRecordGroupTwoRequestJson);
							tvsCdLosGroupResponseList.add(tvsGroupTwoResponse);
						}
					}
				}//End of group Two


				groupServiceStatus.setCaseId(caseId);
				groupServiceStatus.setUpdateDateTime(new Date());
				logger.debug("before sending to save groupServiceStatus::" + gson.toJson(groupServiceStatus));
				saveTvsGroupServicStatus(groupServiceStatus);

			}

			if (CollectionUtils.isNotEmpty(tvsCdLosGroupResponseList)) {
				baseResponsePullAndTransform = GngUtils.getBaseResponse(HttpStatus.OK, tvsCdLosGroupResponseList);
			}else{
				baseResponsePullAndTransform = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,GngUtils.getNoContentErrorList());
			}
		}//End caseId check
		else{
			logger.debug("case id is not found");
		}

		return baseResponsePullAndTransform;
	}


	public List<TVSCdLosGroupResponse> pullAndPopulateDataByOnlyCaseId(String caseId){
		TvsIntiateData tvsIntiateData = new TvsIntiateData();
		TVSCdLosResponse tvsCdLosIntiateResponse = new TVSCdLosResponse();
		TvsGroupServiceStatus groupServiceStatus = new TvsGroupServiceStatus();
		List<TVSCdLosGroupResponse> tvsCdLosGroupResponseList = new ArrayList<TVSCdLosGroupResponse>();

		try {
			logger.debug("in tvscdlos api");

			tvsIntiateData =  losTvsInitiateBuilder.buildLosTvsInitiateInputRequestForCaseId(caseId);

			tvsCdLosIntiateResponse = this.invokeTVSIntiateServiceByCaseID(tvsIntiateData);

			if(tvsCdLosIntiateResponse!=null) {
				logger.debug("intiate service response in case by id"
						+ tvsCdLosIntiateResponse.getStatusMessage());
                GoNoGoCustomerApplication gonogoCustomerApplication = getGONOGOApplicationAsPerCaseId(caseId);
                PostIpaRequest postIpaRequest = getpostIpaRequestBysRefID(caseId);
                SerialNumberInfo serialNumberInfo = getSerialNumberInfoBysRefID(caseId);
                InsurancePremiumDetails insurancePremiumDetails = getinsurancePremiumDetailsBysRefID(caseId);
                //getting schememaster by postipa
                SchemeMasterData schemeMasterData = getschemeMasterDataBysRefID(postIpaRequest);
                ExtendedWarrantyDetails extendedWarrantyDetails = getextendedWarrantyDetailsBysRefID(caseId);
                DocumentListForGroupFive documentListForGroupFive = getUploadFileDetailsAsPerCaseId(caseId);
                List<ESignedLog> eSignedLog = getesignedLogByrefIdBysRefID(caseId);

				//populate group3
				InsertOrUpdateTvsRecordGroupThree	insertOrUpdateTvsRecordResponse = getLosTvsRequestGroupThreeJson(gonogoCustomerApplication,postIpaRequest,serialNumberInfo,
                        insurancePremiumDetails,schemeMasterData,extendedWarrantyDetails);
				//populate groupone--------
				InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneResponse = getLosTvsRequestGroupOneJson(gonogoCustomerApplication,serialNumberInfo);

				//populate group two
				InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoResponse = getLosTvsRequestGroupTwoJson(gonogoCustomerApplication);

				//populate group four
				InsertOrUpdateTvsRecordGroupFour GroupFourReturn = getLosTvsRequestGroupFourJson(extendedWarrantyDetails,eSignedLog,gonogoCustomerApplication,insurancePremiumDetails,serialNumberInfo);

				//populate group five
				InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveResponse = getLosTvsRequestGroupFiveJson(gonogoCustomerApplication,documentListForGroupFive);

				TvsGroupServiceStatus groupService_Status_return = getTvsGroupServiceStatus(caseId);
				// .tvsGroupService_StatusBycaseId(caseId);
				if (groupService_Status_return == null) {
					groupServiceStatus.setTVSGroup1Status(LosTvsGroupStatusLogEnums.N.name());
					groupServiceStatus.setTVSGroup2Status(LosTvsGroupStatusLogEnums.N.name());
					groupServiceStatus.setTVSGroup3Status(LosTvsGroupStatusLogEnums.N.name());
					groupServiceStatus.setTVSGroup4Status(LosTvsGroupStatusLogEnums.N.name());
					groupServiceStatus.setTVSGroup5Status(LosTvsGroupStatusLogEnums.N.name());
				} else {
					groupServiceStatus = groupService_Status_return;
				}

				if(insertOrUpdateTvsRecordResponse!=null) {
					if (groupServiceStatus.getTVSGroup3Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordResponse.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr003 insert" + caseId);

						AuditingInfo AuditingInfoGroupThree = new AuditingInfo();

						AuditingInfoGroupThree.setRequestDateTime(new Date());

						TVSCdLosResponse tvsCdLosGroupThreeCreateResponse = this
								.invokeTvsGroupThreeCreateService(insertOrUpdateTvsRecordResponse);

						AuditingInfoGroupThree.setResponseDateTime(new Date());

						if (tvsCdLosGroupThreeCreateResponse != null)
						{
							logger.debug("tvsCdLosGroupThreeCreateResponse response::" + tvsCdLosGroupThreeCreateResponse.getStatusMessage());
							if (tvsCdLosGroupThreeCreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
								groupServiceStatus.setTVSGroup3Status(LosTvsGroupStatusLogEnums.Y.name());
							}
							TVSCdLosGroupResponse TVSCdLosGroupThreeResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupThreeResponse.setGroupName(LosTvsGroupEnums.GroupThree.name());
							TVSCdLosGroupThreeResponse.setGroupAction(LosTvsActionEnums.I.name());
							TVSCdLosGroupThreeResponse.setStatusCode(tvsCdLosGroupThreeCreateResponse.getStatusCode());
							TVSCdLosGroupThreeResponse.setStatusMessage(tvsCdLosGroupThreeCreateResponse.getStatusMessage());
							TVSCdLosGroupThreeResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupThreeResponse);

							AuditingInfoGroupThree.setProspectId(insertOrUpdateTvsRecordResponse.getCDLos().getProspectid());
							AuditingInfoGroupThree.setRequestJson(gson.toJson(insertOrUpdateTvsRecordResponse));
							AuditingInfoGroupThree.setResponseJson(gson.toJson(tvsIntiateData));
							AuditingInfoGroupThree.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupThree.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupThree.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupThree.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsGroupThreeCreateService.name());
							AuditingInfoGroupThree.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupThree.setGroupName(LosTvsGroupEnums.GroupThree.name());
							AuditingInfoGroupThree.setAction(LosTvsActionEnums.I.name());
							//if ((AuditingInfoGroupThreeAuditingInfoGroupThree.getSource() != null) || (!(AuditingInfoGroupThree.getSource() == "Dashboard")))
							//{
							AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							//}
							//	else{AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());}
							this.postAuditingInfo(AuditingInfoGroupThree);


						}

					} else {
						CDLos cdLos = insertOrUpdateTvsRecordResponse.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordResponse.setCDLos(cdLos);
						logger.debug("in tvscdlos gr003 Update" + caseId);
						logger.debug("in tvscdlos GR003 update json ::" + gson.toJson(insertOrUpdateTvsRecordResponse));

						AuditingInfo AuditingInfoGroupThree = new AuditingInfo();

						AuditingInfoGroupThree.setRequestDateTime(new Date());


						TVSCdLosResponse tvsCdLosGroupThreeUpdateResponse = this
								.invokeTvsGroupThreeCreateService(insertOrUpdateTvsRecordResponse);


						AuditingInfoGroupThree.setResponseDateTime(new Date());

						if(tvsCdLosGroupThreeUpdateResponse!=null) {
							logger.debug("in tvscdlos GR003 update status::" + gson.toJson(tvsCdLosGroupThreeUpdateResponse));
							TVSCdLosGroupResponse TVSCdLosGroupThreeResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupThreeResponse.setGroupName(LosTvsGroupEnums.GroupThree.name());
							TVSCdLosGroupThreeResponse.setGroupAction(LosTvsActionEnums.U.name());
							TVSCdLosGroupThreeResponse.setStatusCode(tvsCdLosGroupThreeUpdateResponse.getStatusCode());
							TVSCdLosGroupThreeResponse.setStatusMessage(tvsCdLosGroupThreeUpdateResponse.getStatusMessage());
							TVSCdLosGroupThreeResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupThreeResponse);

							AuditingInfoGroupThree.setProspectId(insertOrUpdateTvsRecordResponse.getCDLos().getProspectid());
							AuditingInfoGroupThree.setRequestJson(gson.toJson(insertOrUpdateTvsRecordResponse));
							AuditingInfoGroupThree.setResponseJson(gson.toJson(tvsIntiateData));
							AuditingInfoGroupThree.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupThree.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupThree.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupThree.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsGroupThreeCreateService.name());
							AuditingInfoGroupThree.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupThree.setGroupName(LosTvsGroupEnums.GroupThree.name());
							AuditingInfoGroupThree.setAction(LosTvsActionEnums.U.name());
							//if ((AuditingInfoGroupThree.getSource() != null) || (!(AuditingInfoGroupThree.getSource() == "Dashboard")))
							//{
							AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							//}
							//	else{AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());}
							this.postAuditingInfo(AuditingInfoGroupThree);

						}

					}
				}

				if(insertOrUpdateTvsRecordGroupOneResponse!=null) {
					if (groupServiceStatus.getTVSGroup1Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupOneResponse.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr001 insert" + caseId);


						AuditingInfo AuditingInfoGroupOne = new AuditingInfo();

						AuditingInfoGroupOne.setRequestDateTime(new Date());
						TVSCdLosResponse tvsCdLosGroupOneCreateResponse = this
								.invokeTvsCreateServiceGroupOne(insertOrUpdateTvsRecordGroupOneResponse);

						AuditingInfoGroupOne.setResponseDateTime(new Date());


						if(tvsCdLosGroupOneCreateResponse!=null) {
							logger.debug("tvsCdLosGroupOneCreateResponse response::"
									+ tvsCdLosGroupOneCreateResponse.getStatusMessage());
							if (tvsCdLosGroupOneCreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
								groupServiceStatus.setTVSGroup1Status(LosTvsGroupStatusLogEnums.Y.name());
							}

							TVSCdLosGroupResponse TVSCdLosGroupOneResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupOneResponse.setGroupName(LosTvsGroupEnums.GroupOne.name());
							TVSCdLosGroupOneResponse.setGroupAction(LosTvsActionEnums.I.name());
							TVSCdLosGroupOneResponse.setStatusCode(tvsCdLosGroupOneCreateResponse.getStatusCode());
							TVSCdLosGroupOneResponse.setStatusMessage(tvsCdLosGroupOneCreateResponse.getStatusMessage());
							TVSCdLosGroupOneResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupOneResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupOneResponse);


							AuditingInfoGroupOne.setProspectId(insertOrUpdateTvsRecordGroupOneResponse.getCDLos().getProspectid());
							AuditingInfoGroupOne.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupOneResponse));
							AuditingInfoGroupOne.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupOne.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupOne.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupOne.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupOne.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupOne.name());
							AuditingInfoGroupOne.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupOne.setGroupName(LosTvsGroupEnums.GroupOne.name());

							AuditingInfoGroupOne.setAction(LosTvsActionEnums.I.name());
							AuditingInfoGroupOne.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupOne);

						}

					} else {
						CDLos cdLos = insertOrUpdateTvsRecordGroupOneResponse.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupOneResponse.setCDLos(cdLos);

						logger.debug("in tvscdlos gr001 Update" + caseId);
						logger.debug("in GR001 update json ::" + gson.toJson(insertOrUpdateTvsRecordGroupOneResponse));
						AuditingInfo AuditingInfoGroupOne = new AuditingInfo();

						AuditingInfoGroupOne.setRequestDateTime(new Date());
						TVSCdLosResponse tvsCdLosGroupOneCreateResponse = this
								.invokeTvsCreateServiceGroupOne(insertOrUpdateTvsRecordGroupOneResponse);

						AuditingInfoGroupOne.setResponseDateTime(new Date());


						if(tvsCdLosGroupOneCreateResponse!=null) {
							logger.debug("in GR001 update status::" + gson.toJson(tvsCdLosGroupOneCreateResponse));
							TVSCdLosGroupResponse TVSCdLosGroupOneResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupOneResponse.setGroupName(LosTvsGroupEnums.GroupOne.name());
							TVSCdLosGroupOneResponse.setGroupAction(LosTvsActionEnums.U.name());
							TVSCdLosGroupOneResponse.setStatusCode(tvsCdLosGroupOneCreateResponse.getStatusCode());
							TVSCdLosGroupOneResponse.setStatusMessage(tvsCdLosGroupOneCreateResponse.getStatusMessage());
							TVSCdLosGroupOneResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupOneResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupOneResponse);


							AuditingInfoGroupOne.setProspectId(insertOrUpdateTvsRecordGroupOneResponse.getCDLos().getProspectid());
							AuditingInfoGroupOne.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupOneResponse));
							AuditingInfoGroupOne.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupOne.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupOne.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupOne.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupOne.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupOne.name());
							AuditingInfoGroupOne.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupOne.setGroupName(LosTvsGroupEnums.GroupOne.name());
							AuditingInfoGroupOne.setAction(LosTvsActionEnums.U.name());
							AuditingInfoGroupOne.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupOne);

						}

					}
				}

				if(insertOrUpdateTvsRecordGroupTwoResponse!=null) {
					if (groupServiceStatus.getTVSGroup2Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupTwoResponse.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr002 insert" + caseId);


						AuditingInfo AuditingInfoGroupTwo = new AuditingInfo();

						AuditingInfoGroupTwo.setRequestDateTime(new Date());
						TVSCdLosResponse tvsCdLosGroupTwoCreateResponse = this
								.invokeTvsCreateServiceGroupTwo(insertOrUpdateTvsRecordGroupTwoResponse);

						AuditingInfoGroupTwo.setResponseDateTime(new Date());


						if(tvsCdLosGroupTwoCreateResponse!=null) {
							logger.debug("tvsCdLosGroupTwoCreateResponse response::"
									+ tvsCdLosGroupTwoCreateResponse.getStatusMessage());
							if (tvsCdLosGroupTwoCreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
								groupServiceStatus.setTVSGroup2Status(LosTvsGroupStatusLogEnums.Y.name());
							}

							TVSCdLosGroupResponse TVSCdLosGroupTwoResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupTwoResponse.setGroupName(LosTvsGroupEnums.GroupTwo.name());
							TVSCdLosGroupTwoResponse.setGroupAction(LosTvsActionEnums.I.name());
							TVSCdLosGroupTwoResponse.setStatusCode(tvsCdLosGroupTwoCreateResponse.getStatusCode());
							TVSCdLosGroupTwoResponse.setStatusMessage(tvsCdLosGroupTwoCreateResponse.getStatusMessage());
							TVSCdLosGroupTwoResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupTwoResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupTwoResponse);


							AuditingInfoGroupTwo.setProspectId(insertOrUpdateTvsRecordGroupTwoResponse.getCDLos().getProspectid());
							AuditingInfoGroupTwo.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupTwoResponse));
							AuditingInfoGroupTwo.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupTwo.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupTwo.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupTwo.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupTwo.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupTwo.name());
							AuditingInfoGroupTwo.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupTwo.setGroupName(LosTvsGroupEnums.GroupTwo.name());

							AuditingInfoGroupTwo.setAction(LosTvsActionEnums.I.name());
							AuditingInfoGroupTwo.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupTwo);

						}

					} else {
						CDLos cdLos = insertOrUpdateTvsRecordGroupTwoResponse.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupTwoResponse.setCDLos(cdLos);

						logger.debug("in tvscdlos gr002 Update" + caseId);
						logger.debug("in GR002 update json ::" + gson.toJson(insertOrUpdateTvsRecordGroupTwoResponse));
						AuditingInfo AuditingInfoGroupTwo = new AuditingInfo();

						AuditingInfoGroupTwo.setRequestDateTime(new Date());
						TVSCdLosResponse tvsCdLosGroupTwoCreateResponse = this
								.invokeTvsCreateServiceGroupTwo(insertOrUpdateTvsRecordGroupTwoResponse);

						AuditingInfoGroupTwo.setResponseDateTime(new Date());


						if(tvsCdLosGroupTwoCreateResponse!=null) {
							logger.debug("in GR001 update status::" + gson.toJson(tvsCdLosGroupTwoCreateResponse));
							TVSCdLosGroupResponse TVSCdLosGroupTwoResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupTwoResponse.setGroupName(LosTvsGroupEnums.GroupTwo.name());
							TVSCdLosGroupTwoResponse.setGroupAction(LosTvsActionEnums.U.name());
							TVSCdLosGroupTwoResponse.setStatusCode(tvsCdLosGroupTwoCreateResponse.getStatusCode());
							TVSCdLosGroupTwoResponse.setStatusMessage(tvsCdLosGroupTwoCreateResponse.getStatusMessage());
							TVSCdLosGroupTwoResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupTwoResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupTwoResponse);


							AuditingInfoGroupTwo.setProspectId(insertOrUpdateTvsRecordGroupTwoResponse.getCDLos().getProspectid());
							AuditingInfoGroupTwo.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupTwoResponse));
							AuditingInfoGroupTwo.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupTwo.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupTwo.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupTwo.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupTwo.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupTwo.name());
							AuditingInfoGroupTwo.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupTwo.setGroupName(LosTvsGroupEnums.GroupTwo.name());
							AuditingInfoGroupTwo.setAction(LosTvsActionEnums.U.name());
							AuditingInfoGroupTwo.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupTwo);

						}

					}
				}


				if(GroupFourReturn!=null) {
					if (groupServiceStatus.getTVSGroup4Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& GroupFourReturn.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr004 insert" + caseId);


						AuditingInfo AuditingInfoGroupFour =new AuditingInfo();

						AuditingInfoGroupFour.setRequestDateTime(new Date());

						TVSCdLosResponse tvsCdLosGroupFourCreateResponse = this
								.invokeTvsGroupFourCreateService(GroupFourReturn);

						AuditingInfoGroupFour.setResponseDateTime(new Date());


						if(tvsCdLosGroupFourCreateResponse!=null)
						{
							logger.debug("tvsCdLosGroupFourCreateResponse response::"
									+ tvsCdLosGroupFourCreateResponse.getStatusMessage());
							if (tvsCdLosGroupFourCreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {
								groupServiceStatus.setTVSGroup4Status(LosTvsGroupStatusLogEnums.Y.name());
							}

							TVSCdLosGroupResponse TVSCdLosGroupFourResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupFourResponse.setGroupName(LosTvsGroupEnums.GroupFour.name());
							TVSCdLosGroupFourResponse.setGroupAction(LosTvsActionEnums.I.name());
							TVSCdLosGroupFourResponse.setStatusCode(tvsCdLosGroupFourCreateResponse.getStatusCode());
							TVSCdLosGroupFourResponse.setStatusMessage(tvsCdLosGroupFourCreateResponse.getStatusMessage());
							TVSCdLosGroupFourResponse.setJsonRequest(gson.toJson(GroupFourReturn));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupFourResponse);

							AuditingInfoGroupFour.setProspectId(GroupFourReturn.getCDLos().getProspectid());
							AuditingInfoGroupFour.setRequestJson(gson.toJson(GroupFourReturn));
							AuditingInfoGroupFour.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFour.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupFour.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupFour.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupFour.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsGroupFourCreateService.name());
							AuditingInfoGroupFour.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFour.setGroupName(LosTvsGroupEnums.GroupFour.name());
							AuditingInfoGroupFour.setAction(LosTvsActionEnums.I.name());
							AuditingInfoGroupFour.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupFour);

						}
					} else {
						CDLos cdLos = GroupFourReturn.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						GroupFourReturn.setCDLos(cdLos);
						logger.debug("in tvscdlos gr004 Update" + caseId);
						logger.debug("in GR004 update json ::" + gson.toJson(GroupFourReturn));

						AuditingInfo AuditingInfoGroupFour =new AuditingInfo();

						AuditingInfoGroupFour.setRequestDateTime(new Date());


						TVSCdLosResponse tvsCdLosGroupFourCreateResponse = this
								.invokeTvsGroupFourCreateService(GroupFourReturn);


						AuditingInfoGroupFour.setResponseDateTime(new Date());

						if(tvsCdLosGroupFourCreateResponse!=null) {
							logger.debug("in GR004 update status::" + gson.toJson(tvsCdLosGroupFourCreateResponse));

							TVSCdLosGroupResponse TVSCdLosGroupFourResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupFourResponse.setGroupName(LosTvsGroupEnums.GroupFour.name());
							TVSCdLosGroupFourResponse.setGroupAction(LosTvsActionEnums.U.name());
							TVSCdLosGroupFourResponse.setStatusCode(tvsCdLosGroupFourCreateResponse.getStatusCode());
							TVSCdLosGroupFourResponse.setStatusMessage(tvsCdLosGroupFourCreateResponse.getStatusMessage());
							TVSCdLosGroupFourResponse.setJsonRequest(gson.toJson(GroupFourReturn));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupFourResponse);



							AuditingInfoGroupFour.setProspectId(GroupFourReturn.getCDLos().getProspectid());
							AuditingInfoGroupFour.setRequestJson(gson.toJson(GroupFourReturn));
							AuditingInfoGroupFour.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFour.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupFour.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupFour.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupFour.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsGroupFourCreateService.name());
							AuditingInfoGroupFour.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFour.setGroupName(LosTvsGroupEnums.GroupFour.name());
							AuditingInfoGroupFour.setAction(LosTvsActionEnums.U.name());
							AuditingInfoGroupFour.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupFour);
						}

					}
				}

				if(insertOrUpdateTvsRecordGroupFiveResponse!=null) {
					if (groupServiceStatus.getTVSGroup5Status().equalsIgnoreCase(LosTvsGroupStatusLogEnums.N.name())
							&& insertOrUpdateTvsRecordGroupFiveResponse.getCDLos().getAction().equalsIgnoreCase(LosTvsActionEnums.I.name())) {
						logger.debug("in tvscdlos gr005 insert"+ caseId);
						if (insertOrUpdateTvsRecordGroupFiveResponse != null) {
							AuditingInfo AuditingInfoGroupFive =new AuditingInfo();

							AuditingInfoGroupFive.setRequestDateTime(new Date());


							TVSCdLosResponse tvsCdLosGR005CreateResponse = this
									.invokeTvsCreateServiceGroupFive(insertOrUpdateTvsRecordGroupFiveResponse);


							AuditingInfoGroupFive.setResponseDateTime(new Date());


							if(tvsCdLosGR005CreateResponse!=null) {
								logger.debug("tvsCdLosGR005CreateResponse response::"
										+ tvsCdLosGR005CreateResponse.getStatusMessage());
								if (tvsCdLosGR005CreateResponse.getStatusCode().equalsIgnoreCase(LosTvsGroupStatusLogEnums.SR.name())) {

									groupServiceStatus.setTVSGroup5Status(LosTvsGroupStatusLogEnums.Y.name());
								}
								TVSCdLosGroupResponse TVSCdLosGroupFiveResponse = new TVSCdLosGroupResponse();
								TVSCdLosGroupFiveResponse.setGroupName(LosTvsGroupEnums.GroupFive.name());
								TVSCdLosGroupFiveResponse.setGroupAction(LosTvsActionEnums.I.name());
								TVSCdLosGroupFiveResponse.setStatusCode(tvsCdLosGR005CreateResponse.getStatusCode());
								TVSCdLosGroupFiveResponse.setStatusMessage(tvsCdLosGR005CreateResponse.getStatusMessage());
								TVSCdLosGroupFiveResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupFiveResponse));
								tvsCdLosGroupResponseList.add(TVSCdLosGroupFiveResponse);

								AuditingInfoGroupFive.setProspectId(insertOrUpdateTvsRecordGroupFiveResponse.getCDLos().getProspectid());
								AuditingInfoGroupFive.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupFiveResponse));
								AuditingInfoGroupFive.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
								AuditingInfoGroupFive.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
								AuditingInfoGroupFive.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
								AuditingInfoGroupFive.setStartPointService(LosTvsStartPointService.tvscdlos.name());
								AuditingInfoGroupFive.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupFive.name());
								AuditingInfoGroupFive.setStatus(gson.toJson(tvsCdLosIntiateResponse));
								AuditingInfoGroupFive.setGroupName(LosTvsGroupEnums.GroupFive.name());
								AuditingInfoGroupFive.setAction(LosTvsActionEnums.I.name());
								AuditingInfoGroupFive.setReprocess(LosTvsGroupStatusLogEnums.N.name());
								this.postAuditingInfo(AuditingInfoGroupFive);
							}
						}

					} else {
						CDLos cdLos = insertOrUpdateTvsRecordGroupFiveResponse.getCDLos();
						cdLos.setAction(LosTvsActionEnums.U.name());
						insertOrUpdateTvsRecordGroupFiveResponse.setCDLos(cdLos);
						logger.debug("in tvscdlos gr005 Update" + caseId);
						logger.debug("in GR005 update json ::" + gson.toJson(insertOrUpdateTvsRecordGroupFiveResponse));

						AuditingInfo AuditingInfoGroupFive =new AuditingInfo();

						AuditingInfoGroupFive.setRequestDateTime(new Date());

						TVSCdLosResponse tvsCdLosGR005CreateResponse = this
								.invokeTvsCreateServiceGroupFive(insertOrUpdateTvsRecordGroupFiveResponse);

						AuditingInfoGroupFive.setResponseDateTime(new Date());


						if(tvsCdLosGR005CreateResponse!=null) {
							logger.debug("in GR005 update status::" + gson.toJson(tvsCdLosGR005CreateResponse));
							TVSCdLosGroupResponse TVSCdLosGroupFiveResponse = new TVSCdLosGroupResponse();
							TVSCdLosGroupFiveResponse.setGroupName(LosTvsGroupEnums.GroupFive.name());
							TVSCdLosGroupFiveResponse.setGroupAction(LosTvsActionEnums.U.name());
							TVSCdLosGroupFiveResponse.setStatusCode(tvsCdLosGR005CreateResponse.getStatusCode());
							TVSCdLosGroupFiveResponse.setStatusMessage(tvsCdLosGR005CreateResponse.getStatusMessage());
							TVSCdLosGroupFiveResponse.setJsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupFiveResponse));
							tvsCdLosGroupResponseList.add(TVSCdLosGroupFiveResponse);

							AuditingInfoGroupFive.setProspectId(insertOrUpdateTvsRecordGroupFiveResponse.getCDLos().getProspectid());
							AuditingInfoGroupFive.setRequestJson(gson.toJson(insertOrUpdateTvsRecordGroupFiveResponse));
							AuditingInfoGroupFive.setResponseJson(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFive.setStatusCode(tvsCdLosIntiateResponse.getStatusCode());
							AuditingInfoGroupFive.setStatusMessage(tvsCdLosIntiateResponse.getStatusMessage());
							AuditingInfoGroupFive.setStartPointService(LosTvsStartPointService.tvscdlos.name());
							AuditingInfoGroupFive.setEndPointService(LosTvsEndPointServiceEnums.invokeTvsCreateServiceGroupFive.name());
							AuditingInfoGroupFive.setStatus(gson.toJson(tvsCdLosIntiateResponse));
							AuditingInfoGroupFive.setGroupName(LosTvsGroupEnums.GroupFive.name());
							AuditingInfoGroupFive.setAction(LosTvsActionEnums.I.name());
							AuditingInfoGroupFive.setReprocess(LosTvsGroupStatusLogEnums.N.name());
							this.postAuditingInfo(AuditingInfoGroupFive);
						}
					}
				}
				groupServiceStatus.setCaseId(caseId);
				groupServiceStatus.setUpdateDateTime(new Date());
				logger.debug("before sending to save groupServiceStatus::" + gson.toJson(groupServiceStatus));
				saveTvsGroupServicStatus(groupServiceStatus);

			}else
			{
				logger.debug("Error occured while getting intitate service status for case id:::");
			}

		} catch (Exception e) {
			logger.error("Error occured in tvscdlos case by id",e);
		}
		return tvsCdLosGroupResponseList;
	}

	private TVSCdLosGroupResponse createGroupThreeResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordGroupThree) {
		return losTvsGroupStatusBuilder.createGroupThreeResponse(groupName,action,statusCode,statusMessage,insertOrUpdateTvsRecordGroupThree);
	}

	private TVSCdLosResponse callToLosTvsGroupFive(TVSGroupFiveRequest tvsGroupFiveRequest, WFJobCommDomain lostvsconfig) throws Exception{
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(tvsGroupFiveRequest, url, TVSCdLosResponse.class);
	}

	private TVSCdLosResponse callToLosTvsGroupFour(TVSGroupFourRequest tvsGroupFourRequest, WFJobCommDomain lostvsconfig) throws Exception{
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(tvsGroupFourRequest, url, TVSCdLosResponse.class);
	}

	private TVSCdLosResponse callToLosTvsGroupThree(TVSGroupThreeRequest tvsGroupThreeRequest, WFJobCommDomain lostvsconfig) throws Exception{
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(tvsGroupThreeRequest, url, TVSCdLosResponse.class);
	}

	private TVSCdLosResponse callToLosTvsGroupOne(TVSGroupOneRequest tvsGroupOneRequest, WFJobCommDomain lostvsconfig) throws Exception{
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(tvsGroupOneRequest, url, TVSCdLosResponse.class);
	}

	private TVSCdLosResponse callToLosTvsGroupTwo(TVSGroupTwoRequest tvsGroupTwoRequest, WFJobCommDomain lostvsconfig) throws Exception{
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(tvsGroupTwoRequest, url, TVSCdLosResponse.class);
	}

	private TVSCdLosResponse  callToLosTvsInitiate(InitiateRequest initiateRequest, WFJobCommDomain lostvsconfig) throws Exception {
		String url = Arrays.asList(lostvsconfig.getBaseUrl(), lostvsconfig.getEndpoint())
				.parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
		return (TVSCdLosResponse) TransportUtils.postJsonRequest(initiateRequest, url, TVSCdLosResponse.class);

	}

	// invoke TVS create service

	public
	TVSCdLosResponse invokeTvsCreateServiceGroupFive(
			InsertOrUpdateTvsRecordGroupFive tvsInsertData) throws Exception {
		AuditingInfo AuditingInfoGroupFive = new AuditingInfo();

		logger.debug("in invokeTvsCreateServiceGroupFive JSON:"+gson.toJson(tvsInsertData));
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();

		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			HttpHeaders headers = new HttpHeaders();

			logger.debug("into tvscdlosCreateGroupFive json:::" + gson.toJson(tvsInsertData));

			/*MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("name", "insertGR005");
			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
			map.add("data", gson.toJson(tvsInsertData));
			headers = this.createHeaders();

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);


			ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);*/

			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsInsertData));
			tvsCdLosRequest.setName("insertGR005");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			AuditingInfoGroupFive.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			AuditingInfoGroupFive.setResponseDateTime(new Date());




			logger.debug("in tvscdlos tvscdlosCreateGroupFive response::" + response);

			ObjectMapper objMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = objMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);
			logger.debug("in tvscdlosCreateGroupFive controller:::" + gson.toJson(tvsCdLosResponse));

			AuditingInfoGroupFive.setProspectId(tvsInsertData.getCDLos().getProspectid());
			AuditingInfoGroupFive.setRequestJson(gson.toJson(tvsInsertData));
			AuditingInfoGroupFive.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupFive.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfoGroupFive.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfoGroupFive.setStartPointService(LosTvsStartPointService.tvscdlosCreateGroupFive.name());
			AuditingInfoGroupFive.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfoGroupFive.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupFive.setGroupName(LosTvsGroupEnums.GroupFive.name());
			AuditingInfoGroupFive.setAction(LosTvsActionEnums.I.name());
			AuditingInfoGroupFive.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfoGroupFive);
		} catch (Exception e) {
			logger.error("in invokeTvsCreateServiceGroupFive controller",e);
		}
		return tvsCdLosResponse;
	}

	// invoke TVS create GR003 service

	public  TVSCdLosResponse invokeTvsGroupThreeCreateService(
			InsertOrUpdateTvsRecordGroupThree tvsInsertData) throws Exception {
		AuditingInfo AuditingInfoGroupThree = new AuditingInfo();

		logger.debug("in invokeTvsGroupThreeCreateService  JSON:"+gson.toJson(tvsInsertData));
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();

		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			//HttpHeaders headers = new HttpHeaders();

			logger.debug("into json:::" + gson.toJson(tvsInsertData));

			/*MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("name", "insertGR003");
			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
			map.add("data", gson.toJson(tvsInsertData));
			headers = this.createHeaders();*/

			/*HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);*/

			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsInsertData));
			tvsCdLosRequest.setName("insertGR003");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			AuditingInfoGroupThree.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			AuditingInfoGroupThree.setResponseDateTime(new Date());


			//System.out.println("in invokeTvsCreateService response::==" + response);

			ObjectMapper objMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = objMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);
			logger.debug("in tvscdlosCreateGroupThree controller response JSON :::" + gson.toJson(tvsCdLosResponse));

			AuditingInfoGroupThree.setProspectId(tvsInsertData.getCDLos().getProspectid());
			AuditingInfoGroupThree.setRequestJson(gson.toJson(tvsInsertData));
			AuditingInfoGroupThree.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupThree.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfoGroupThree.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfoGroupThree.setStartPointService(LosTvsStartPointService.tvscdlosCreateGroupThree.name());
			AuditingInfoGroupThree.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfoGroupThree.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupThree.setGroupName(LosTvsGroupEnums.GroupThree.name());
			AuditingInfoGroupThree.setAction(LosTvsActionEnums.I.name());
			AuditingInfoGroupThree.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfoGroupThree);


		} catch (Exception e) {
			logger.error("in tvscdlosCreateGroupThree controller",e);
		}
		return tvsCdLosResponse;
	}



	public TVSCdLosResponse invokeTvsGroupFourCreateService(
			InsertOrUpdateTvsRecordGroupFour tvsInsertData) throws Exception {
		AuditingInfo AuditingInfoGroupFour = new AuditingInfo();
		logger.debug("in invokeTvsGroupFourCreateService "+gson.toJson(tvsInsertData));
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();

		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			HttpHeaders headers = new HttpHeaders();

			logger.debug("into tvscdlosCreateForGr04 json:::" + gson.toJson(tvsInsertData));

			/*MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("name", "insertGR004");
			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
			map.add("data", gson.toJson(tvsInsertData));
			headers = this.createHeaders();

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);*/

			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsInsertData));
			tvsCdLosRequest.setName("insertGR004");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			AuditingInfoGroupFour.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			AuditingInfoGroupFour.setResponseDateTime(new Date());

			logger.debug("in invokeTvsCreateService response::==" + response);

			ObjectMapper objMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = objMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);
			logger.debug("in tvscdlosCreateForGr04  response:::" + gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupFour.setProspectId(tvsInsertData.getCDLos().getProspectid());
			AuditingInfoGroupFour.setRequestJson(gson.toJson(tvsInsertData));
			AuditingInfoGroupFour.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupFour.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfoGroupFour.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfoGroupFour.setStartPointService(LosTvsStartPointService.tvscdlosCreateGroupFour.name());
			AuditingInfoGroupFour.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfoGroupFour.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupFour.setGroupName(LosTvsGroupEnums.GroupFour.name());
			AuditingInfoGroupFour.setAction(LosTvsActionEnums.I.name());
			AuditingInfoGroupFour.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfoGroupFour);

		} catch (Exception e) {
			logger.error("in tvscdlosCreateForGr04 controller",e);
		}
		return tvsCdLosResponse;
	}

	// invoke TVS create GR001 service
	public TVSCdLosResponse invokeTvsCreateServiceGroupOne(
			InsertOrUpdateTvsRecordGroupOne tvsInsertData) throws Exception {
		logger.debug("in invokeTvsCreateServiceGroupOne JSON:"+gson.toJson(tvsInsertData));
		AuditingInfo AuditingInfoGroupOne = new AuditingInfo();
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();
		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//			HttpHeaders headers = new HttpHeaders();

//			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//			map.add("name", "insertGR001");
//			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
//			map.add("data", gson.toJson(tvsInsertData));
//			headers = this.createHeaders();

/*			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers)*/;

			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsInsertData));
			tvsCdLosRequest.setName("insertGR001");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			AuditingInfoGroupOne.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			logger.debug("in tvscdlosCreateGroupOne response::==" + response);
			AuditingInfoGroupOne.setResponseDateTime(new Date());

			ObjectMapper objMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = objMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);
			logger.debug("in invokeTvsCreateServiceGroupOne response JSON:::" + gson.toJson(tvsCdLosResponse));

			AuditingInfoGroupOne.setProspectId(tvsInsertData.getCDLos().getProspectid());
			AuditingInfoGroupOne.setRequestJson(gson.toJson(tvsInsertData));
			AuditingInfoGroupOne.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupOne.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfoGroupOne.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfoGroupOne.setStartPointService(LosTvsStartPointService.tvscdlosCreateGroupOne.name());
			AuditingInfoGroupOne.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfoGroupOne.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupOne.setGroupName(LosTvsGroupEnums.GroupOne.name());
			AuditingInfoGroupOne.setAction(LosTvsActionEnums.I.name());
			AuditingInfoGroupOne.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfoGroupOne);


		} catch (Exception e) {
			logger.error("Error occured in tvscdlosCreateGroupOne controller:",e);
		}
		return tvsCdLosResponse;
	}

	// invoke TVS create GR002 service
	public TVSCdLosResponse invokeTvsCreateServiceGroupTwo(
			InsertOrUpdateTvsRecordGroupTwo tvsInsertData) throws Exception {
		logger.debug("in invokeTvsCreateServiceGroupTwo JSON:"+gson.toJson(tvsInsertData));
		AuditingInfo AuditingInfoGroupTwo = new AuditingInfo();
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();
		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//			HttpHeaders headers = new HttpHeaders();

//			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//			map.add("name", "insertGR001");
//			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
//			map.add("data", gson.toJson(tvsInsertData));
//			headers = this.createHeaders();

/*			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers)*/;

			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsInsertData));
			tvsCdLosRequest.setName("insertGR002");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			AuditingInfoGroupTwo.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			logger.debug("in tvscdlosCreateGroupTwo response::==" + response);
			AuditingInfoGroupTwo.setResponseDateTime(new Date());

			ObjectMapper objMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = objMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);
			logger.debug("in invokeTvsCreateServiceGroupTwo response JSON:::" + gson.toJson(tvsCdLosResponse));

			AuditingInfoGroupTwo.setProspectId(tvsInsertData.getCDLos().getProspectid());
			AuditingInfoGroupTwo.setRequestJson(gson.toJson(tvsInsertData));
			AuditingInfoGroupTwo.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupTwo.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfoGroupTwo.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfoGroupTwo.setStartPointService(LosTvsStartPointService.tvscdlosCreateGroupTwo.name());
			//TODO: Remove hard coding.
			AuditingInfoGroupTwo.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfoGroupTwo.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfoGroupTwo.setGroupName(LosTvsGroupEnums.GroupTwo.name());
			AuditingInfoGroupTwo.setAction(LosTvsActionEnums.I.name());
			AuditingInfoGroupTwo.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfoGroupTwo);


		} catch (Exception e) {
			logger.error("Error occured in tvscdlosCreateGroupOne controller:",e);
		}
		return tvsCdLosResponse;
	}


	public TVSCdLosResponse invokeTVSIntiateServiceByCaseID(TvsIntiateData tvsIntiateData) throws Exception {
		TvsIntiateData tvsIntiateRequest = new TvsIntiateData();
		TVSCdLosResponse tvsCdLosResponse = new TVSCdLosResponse();
		AuditingInfo AuditingInfo = new AuditingInfo();
		logger.debug("in intiateTvsService controller");
		try {
			String url = "http://172.30.155.42/connector/losconnect/los-push-tvs";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			HttpHeaders headers = new HttpHeaders();


	/*MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			System.out.println("in initiateCdLos refID:::" + tvsIntiateData.getProspectid());

			map.add("name", "initiateCdLos");
			map.add("key", "5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");
			map.add("data", );
*/


			TVSCdLosRequest tvsCdLosRequest = new TVSCdLosRequest();
			tvsCdLosRequest.setData(gson.toJson(tvsIntiateData));
			tvsCdLosRequest.setName("initiateCdLos");
			tvsCdLosRequest.setKey("5dfd585c8f4264333a6b7a3df9afc3fd324bd1c5042533abaf865454caa8bb6e");

			//headers = this.createHeaders();


/*HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);*/
			AuditingInfo.setRequestDateTime(new Date());
			ResponseEntity<String> response = restTemplate.postForEntity(url, tvsCdLosRequest, String.class);
			AuditingInfo.setResponseDateTime(new Date());

			// System.out.println("tvscdlos response::==" + response);
			ObjectMapper ObjectMapperForTVSCdLosResponse = new ObjectMapper();
			tvsCdLosResponse = ObjectMapperForTVSCdLosResponse.readValue(response.getBody(), TVSCdLosResponse.class);


			AuditingInfo.setProspectId(tvsIntiateData.getProspectid());
			AuditingInfo.setRequestJson(gson.toJson(tvsIntiateData));
			AuditingInfo.setResponseJson(gson.toJson(tvsCdLosResponse));
			AuditingInfo.setStatusCode(tvsCdLosResponse.getStatusCode());
			AuditingInfo.setStatusMessage(tvsCdLosResponse.getStatusMessage());
			AuditingInfo.setStartPointService(LosTvsStartPointService.intiateTvsService.name());
			AuditingInfo.setEndPointService("https://mobileportaluat.tvscs.co.in/tvs/vs/cdlos");
			AuditingInfo.setStatus(gson.toJson(tvsCdLosResponse));
			AuditingInfo.setGroupName("");
			AuditingInfo.setAction(LosTvsActionEnums.I.name());
			AuditingInfo.setReprocess(LosTvsGroupStatusLogEnums.N.name());
			this.postAuditingInfo(AuditingInfo);

		} catch (Exception e) {
			logger.error("Error occured in intiateTvsService controller",e);
		}
		return tvsCdLosResponse;

	}



}
