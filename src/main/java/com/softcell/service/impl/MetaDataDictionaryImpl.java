package com.softcell.service.impl;

import com.softcell.config.templates.TemplateName;
import com.softcell.constants.ActionName;
import com.softcell.constants.LogoType;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.repository.MetadataRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.metadata.MetadataEntity;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.rest.domains.MetadataDictRequest;
import com.softcell.service.MetaDataDictionaryService;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by prateek on 10/3/17.
 */
@Service
public class MetaDataDictionaryImpl  implements MetaDataDictionaryService {

    private static final Logger logger = LoggerFactory.getLogger(MetaDataDictionaryImpl.class);

    @Autowired
    private MetadataRepository metadataRepository;

    /**
     *
     * @param institutionId
     * @param product
     * @param type
     * @return
     * @throws SystemException
     */
    @Override
    public BaseResponse findTypeValues(String institutionId ,String product ,String type) throws SystemException {

        logger.debug(" getting metadata dictionary of type  [{}]", type);

        Collection<String> typeValues = metadataRepository.findTypeValues(institutionId,
                type,
                product);

        return BaseResponse.builder()
                .payload(new Payload(typeValues))
                .build();

    }

    /**
     *
     * @param institutionId
     * @param product
     * @return BaseResponse
     * @throws SystemException
     */
    @Override
    public BaseResponse findByInstitutionIdNProduct(String institutionId, String product) throws SystemException {

        Optional<Collection<MetadataEntity>> allByInstitutionNProduct = metadataRepository.findAllByInstitutionNProduct(
                institutionId, product
        );

        return BaseResponse.builder().payload(new Payload<>(allByInstitutionNProduct.get())).build();

    }

    /**
     *
     * @param institutionId
     * @return
     * @throws SystemException
     */
    @Override
    public BaseResponse findByInstitutionId(String institutionId) throws SystemException {
        Optional<Collection<MetadataEntity>> allByInstitutionNProduct = metadataRepository.findAllByInstitution(institutionId);

        return BaseResponse.builder().payload(new Payload<>(allByInstitutionNProduct.get())).build();
    }

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    @Override
    public BaseResponse createTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException {

        metadataRepository.createTypeValues(metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().toValue(),
                metadataDictRequest.getProduct().name() ,
                metadataDictRequest.getValues());

        return BaseResponse.builder().payload(new Payload<>(Collections.emptyMap())).build();
    }

    /**
     *
     * @param metadataDictRequest
     * @return
     * @throws SystemException
     */
    @Override
    public BaseResponse replaceTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException {

        metadataRepository.replaceTypeValues(metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().toValue(),
                metadataDictRequest.getProduct().name(),
                metadataDictRequest.getValues()
                );

        return BaseResponse.builder().payload(new Payload<>(Collections.emptyMap())).build();
    }

    /**
     *
     * @param metadataDictRequest
     * @return
     * @throws SystemException
     */
    @Override
    public BaseResponse renameTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException {

        MetadataEntity metadataEntity = metadataRepository.renameTypeValues(
                metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().name()
                , metadataDictRequest.getProduct().name()
                , metadataDictRequest.getRenameValueMap());

        return BaseResponse.builder().payload(new Payload<>(metadataEntity)).build();
    }

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    @Override
    public BaseResponse deleteType(MetadataDictRequest metadataDictRequest) throws SystemException {

        MetadataEntity metadataEntity = metadataRepository.deleteType(metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().toValue(),
                metadataDictRequest.getProduct().name()
        );

        return BaseResponse.builder().payload(new Payload<>(metadataEntity != null ? metadataEntity : Collections.emptyMap() )).build();
    }

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    @Override
    public void factoryDefaultTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException {

        MetadataEntity metadataEntity = metadataRepository.factoryDefaultTypeValues(metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().toValue(),
                metadataDictRequest.getProduct().name()
        );

    }

    @Override
    public void disableTypeValue(MetadataDictRequest metadataDictRequest) throws SystemException {
        metadataRepository.disableTypeValue(
                metadataDictRequest.getInstitutionId(),
                metadataDictRequest.getType().name(),
                metadataDictRequest.getProduct().name());
    }

    @Override
    public BaseResponse findByType(String type) {
        Collection<String> valueList;

        switch (type){
            case "action":{

                valueList = GngUtils.getEnumValueList(ActionName.class);
            }
            break;
            case "attachment":{

                valueList = GngUtils.getEnumValueList(TemplateName.class);
            }
            break;
            case "product":{

                valueList = GngUtils.getEnumValueList(Product.class);
            }
            break;
            case "urltype": {

                valueList = GngUtils.getEnumValueList(UrlType.class);
            }
            break;
            case "logo": {

                valueList = GngUtils.getEnumValueList(LogoType.class);
            }
            break;
            default:{
                return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,GngUtils.getNoContentErrorList());
            }

        }

        return GngUtils.getBaseResponse(HttpStatus.OK,valueList);
    }
}
