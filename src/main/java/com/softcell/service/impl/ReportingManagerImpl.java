package com.softcell.service.impl;

import com.google.common.base.CharMatcher;
import com.mongodb.BasicDBObject;
import com.softcell.config.reporting.ColumConfigurationMap;
import com.softcell.config.reporting.GoNoGoSystemFields;
import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.KYC_TYPES;
import com.softcell.constants.dbconstants.GoNoGoCustomerApplicationKeyConstants;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.dao.mongodb.repository.ReportingRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.dao.mongodb.utils.GngCollectionUtils;
import com.softcell.gonogo.exceptions.ReportConfigNotInDb;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLog;
import com.softcell.gonogo.model.logger.AadharLog;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.LosUtrDetailsMaster;
import com.softcell.gonogo.model.reports.AssetModelTreeView;
import com.softcell.gonogo.model.reports.ChannelRptDomain;
import com.softcell.gonogo.model.reports.DeviceInfoRptDomain;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.SerialNumberInfoLogRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorLog;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.parse.*;
import com.softcell.reporting.builder.*;
import com.softcell.reporting.domains.*;
import com.softcell.reporting.request.LosUtrUpdateReportRequest;
import com.softcell.reporting.request.ReportRequest;
import com.softcell.reporting.request.ReportSearchRequest;
import com.softcell.reporting.utils.ReportCompressionUtility;
import com.softcell.service.ReportingManager;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.SequenceGenerator;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author kishor
 */
@Service
public class ReportingManagerImpl implements ReportingManager {

    private static Logger logger = LoggerFactory.getLogger(ReportingManagerImpl.class);

    //TODO remove this from clearing as member variable
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Autowired
    private ReportingRepository reportingRepository;

    @Autowired
    private Report report;

    @Autowired
    private ReportMongoPipeLines reportingPipelineHelper;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    private LookupService lookupService;


    @Override
    public BaseResponse getReportDimensions(ReportRequest reportRequest) throws ReportConfigNotInDb {

        GoNoGoSystemFields goNoGoSystemFields = null;

        goNoGoSystemFields = reportingRepository.getGoNoGoDimensions(QueryBuilder.buildDimensionQuery(reportRequest));

        if (null != goNoGoSystemFields) {

            goNoGoSystemFields.setReportingConfigurations(reportingRepository.reportSystemConfiguration(QueryBuilder
                    .buildReportConfigurationQuery(reportRequest)));
        } else {

            logger.info("Report Configuration not available in db ");

            throw new ReportConfigNotInDb("Report Configuration not available in db");
        }


        return GngUtils.getBaseResponse(HttpStatus.OK, goNoGoSystemFields);
    }

    @Override
    public BaseResponse getCustomReports(ReportingConfigurations reportingConfigurations) {

        FlatReportConfiguration flatReportConfiguration = reportingRepository
                .getFlatReportConfiguration(QueryBuilder
                        .buildFlatReportConfigQuery(reportingConfigurations));

        CustomReport customReport = null;
        if (flatReportConfiguration != null) {
            Query query = QueryBuilder
                    .buildReportingConfigurationQuery(reportingConfigurations);
            customReport = reportingRepository.getCustomReport(
                    flatReportConfiguration, query);
        }

        if (customReport != null) {

            return GngUtils.getBaseResponse(HttpStatus.OK, customReport);

        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse saveCustomReports(
            ReportingConfigurations reportingConfigurations) {

        CustomReport customReport = null;

        if (null != reportingConfigurations) {

            String reportId = reportingConfigurations.getReportId();

            if (StringUtils.isBlank(reportId)) {

                reportingConfigurations.setReportId(SequenceGenerator
                        .getReportId());

            }

            reportingRepository
                    .saveCustomReportConfiguration(reportingConfigurations);

            FlatReportConfiguration flatReportConfiguration = reportingRepository
                    .getFlatReportConfiguration(QueryBuilder
                            .buildFlatReportConfigQuery(reportingConfigurations));

            if (null != flatReportConfiguration) {

                Query query = QueryBuilder
                        .buildReportingConfigurationQuery(reportingConfigurations);
                customReport = reportingRepository.getCustomReport(
                        flatReportConfiguration, query);

            }

        }

        if (customReport != null) {
            return GngUtils.getBaseResponse(HttpStatus.OK, customReport);
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse getReportNames(ReportSearchRequest reportSearchRequest) {

        List<ReportDetail> reportDetails = reportingRepository.searchReportByName(QueryBuilder
                .buildReportSearchQuery(reportSearchRequest));

        if (reportDetails != null && !reportDetails.isEmpty()) {

            return GngUtils.getBaseResponse(HttpStatus.OK, reportDetails);

        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse getReportConfiguration(ReportRequest reportRequest) {

        ReportingConfigurations reportingConfigurations = reportingRepository.reportConfiguration(QueryBuilder
                .buildReportConfigByIdQuery(reportRequest));

        if (null != reportingConfigurations) {

            GoNoGoSystemFields goNoGoSystemFields = reportingRepository
                    .getGoNoGoDimensions(QueryBuilder
                            .buildDimensionQuery(reportRequest));

            Set<ColumnConfiguration> unselectedColumn = GngCollectionUtils
                    .getUnSelectedColumns(
                            goNoGoSystemFields.getFieldsDetails(),
                            reportingConfigurations
                                    .getFlatReportConfiguration());

            reportingConfigurations.setUnSelectedColumn(unselectedColumn);

            return GngUtils.getBaseResponse(HttpStatus.OK, reportingConfigurations);
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

    }

    @Override
    @Deprecated
    public BaseResponse getCsvReport(ReportingConfigurations reportingConfigurations) throws Exception {


        FlatReportConfiguration flatReportConfiguration = reportingRepository.getFlatReportConfiguration(
                QueryBuilder.buildFlatReportConfigQuery(reportingConfigurations));

        List<GoNoGoCustomerApplication> customerApplications = null;//= reportingRepository.getCustomerApplicationOld(new Query());

        ReportWriter reportWriter = new ReportWriter();

        if (null == flatReportConfiguration) {

            flatReportConfiguration = ColumConfigurationMap.getConFigurationV2CSV();

        }

        try {

            byte[] bytes = reportWriter.writeZipFile(customerApplications, flatReportConfiguration);

            if (bytes != null) {

                return GngUtils.getBaseResponse(HttpStatus.OK, bytes);

            }

            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        } catch (Exception e) {

            e.printStackTrace();

            throw new Exception();

        }


    }

    @Override
    public BaseResponse previewCustomReports(ReportingConfigurations reportingConfigurations) {

        CustomReport customReport = null;

        if (null != reportingConfigurations && null != reportingConfigurations.getFlatReportConfiguration()) {

            Query query = QueryBuilder.buildReportingPreviewConfigQuery(reportingConfigurations);

            customReport = reportingRepository.getCustomReport(reportingConfigurations
                    .getFlatReportConfiguration(), query);

        }

        if (customReport != null) {
            return GngUtils.getBaseResponse(HttpStatus.OK, customReport);
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public BaseResponse saveDefaultReports(
            ReportingConfigurations reportingConfigurations) {

        if (null != reportingConfigurations) {

            String reportId = reportingConfigurations.getReportId();

            if (StringUtils.isBlank(reportId)) {

                reportingConfigurations.setReportId(SequenceGenerator.getReportId());
            }

            reportingRepository.saveCustomReportConfiguration(reportingConfigurations);

            ReportRequest reportRequest = new ReportRequest();

            reportRequest.setHeader(reportingConfigurations.getHeader());
            reportRequest.setProductTypes(reportingConfigurations.getProducts());
            reportRequest.setUserId(reportingConfigurations.getUserId());

            GoNoGoSystemFields goNoGoSystemFields = reportingRepository.getGoNoGoDimensions(QueryBuilder
                    .buildDimensionQuery(reportRequest));

            reportingConfigurations.setUnSelectedColumn(goNoGoSystemFields
                    .getFieldsDetails());

            return GngUtils.getBaseResponse(HttpStatus.OK, reportingConfigurations);

        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public byte[] getChannelWiseReport(Date startDate, Date endDate, String product,
                                       String institutionId) throws Exception {
        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.INSTITUTION_ID).is(institutionId),
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.LOAN_TYPE).is(product),
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SUBMIT_DATE_TIME).lte(endDate).gte(startDate)
        ));

        List<String> includeFields = getIncludeFields();

        for (String includeField : includeFields) {
            query.fields().include(includeField);
        }

        Stream<GoNoGoCustomerApplication> customerApplication = reportingRepository.getCustomerApplication(query);

        List<ChannelRptDomain> channelReportList = customerApplication.map(application -> {

            ChannelRptDomain channelReport = new ChannelRptDomain();

            channelReport.setRefID(application.getGngRefId());

            channelReport.setDate(application.getDateTime());

            channelReport.setTime(application.getDateTime());

            try {

                channelReport.setDsaID(application.getApplicationRequest().getHeader().getDsaId());

            } catch (Exception exp) {
                exp.printStackTrace();
                logger.error(" error occurred while setting dsaId in  channel wise report with probable cause [{}] ", exp.getMessage());

            }

            try {

                if (null != application.getApplicationRequest()
                        .getHeader()
                        && null != application
                        .getApplicationRequest().getHeader()
                        .getApplicationSource()) {

                    channelReport.setChannel(application.getApplicationRequest().getHeader()
                            .getApplicationSource());

                } else {

                    channelReport.setChannel("Android");

                }

            } catch (Exception exp) {
                exp.printStackTrace();
                logger.error(" error occurred while setting channel in channel wise report with probable cause [{}] ", exp.getMessage());
            }

            return channelReport;

        }).collect(Collectors.toList());


        JsonFlattener parser = new JsonFlattener();

        CSVWriter writer = new CSVWriter();

        List<Map<String, String>> flatJson;

        byte[] bytes;

        try {


            flatJson = parser.parseJson(JsonUtil.ObjectToString(channelReportList));


            bytes = GnGFileUtils.compressBytes(
                    "CHANNEL_WISE_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    writer.writeAsCSV(flatJson));

        } catch (Exception e) {

            logger.error(" error occurred  while fetching channel wise report [{}] ", e.getMessage());

            throw new Exception(String.format("Exception while fetching channel wise report [%s]", e.getMessage()));

        }

        return bytes;
    }

    /**
     * Ref No, Date , DSA & Channel (Web / Android)
     */

    private List<String> getIncludeFields() {
        List<String> includeField = new ArrayList<String>();
        includeField.add(GoNoGoCustomerApplicationKeyConstants.REF_ID);
        includeField
                .add(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SUBMIT_DATE_TIME);
        includeField.add(GoNoGoCustomerApplicationKeyConstants.DSA_ID);
        includeField
                .add(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SOURCE);
        return includeField;
    }

    @Override
    public BaseResponse generateOtpReport(
            OtpReportRequest otpReportRequest) throws Exception {

        // FIXME institutionId missing in query

        Aggregation otpAggregation = reportingPipelineHelper.otpReport(
                otpReportRequest.getStartDate(), otpReportRequest.getEndDate());

        List<OtpReportResponse> list = new ArrayList<OtpReportResponse>();

        AggregationResults<BasicDBObject> otpReport = reportingRepository
                .otpReport(otpAggregation);

        for (BasicDBObject basicDBObject : otpReport) {

            OtpReportResponse otpReportResponse = new OtpReportResponse();
            String day = basicDBObject.getString("day");
            String month = basicDBObject.getString("month");
            String year = basicDBObject.getString("year");

            String date = dateFormat.format(dateFormat.parse(day + "/" + month
                    + "/" + year));

            otpReportResponse.setDate(date);
            otpReportResponse.setMobileNumber(basicDBObject
                    .getString("mobileNumber"));
            otpReportResponse.setCount(basicDBObject.getInt("count"));

            list.add(otpReportResponse);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, list);

    }

    @Override
    public byte[] generateZippedOtpReport(OtpReportRequest otpReportRequest)
            throws Exception {

        Aggregation otpAggregation = reportingPipelineHelper.otpReport(
                otpReportRequest.getStartDate(), otpReportRequest.getEndDate());

        AggregationResults<BasicDBObject> otpReport = reportingRepository
                .otpReport(otpAggregation);

        List<BasicDBObject> mappedResults = otpReport.getMappedResults();

        BasicDBObject basicDBObject = mappedResults.get(0);

        Set<String> keySet = basicDBObject.keySet();

        StringBuilder response = new StringBuilder();

        for (String string : keySet) {
            if (StringUtils.isBlank(response)) {
                if (StringUtils.equalsIgnoreCase(string, "day")
                        || StringUtils.equalsIgnoreCase(string, "month")
                        || StringUtils.equalsIgnoreCase(string, "year")) {
                    if (response.indexOf("date") == -1) {
                        response.append("date");
                    }
                }
            } else {

                if (StringUtils.equalsIgnoreCase(string, "day")
                        || StringUtils.equalsIgnoreCase(string, "month")
                        || StringUtils.equalsIgnoreCase(string, "year")) {
                    if (response.indexOf("date") == -1) {
                        response.append("date");
                    }
                } else {
                    response.append("," + string);
                }
            }
        }

        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream1 = null;

        outputStream1 = new ByteArrayOutputStream();
        reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                StandardCharsets.UTF_8);
        reportOutputStreamWriter.write(response.toString());
        reportOutputStreamWriter.append('\n');

        for (BasicDBObject object : mappedResults) {

            response = new StringBuilder();

            String date;

            date = null != object.getString("day") ? object.getString("day")
                    : "";
            date += "/";
            date += null != object.getString("month") ? object
                    .getString("month") : "";
            date += "/";
            date += null != object.getString("year") ? object.getString("year")
                    : "";

            response.append(dateFormat.format(dateFormat.parse(date)));

            response.append(",");

            response.append(object.get("mobileNumber"));

            response.append(",");

            response.append(object.get("count"));

            reportOutputStreamWriter.write(response.toString());

            reportOutputStreamWriter.append('\n');

        }

        reportOutputStreamWriter.close();

        return GnGFileUtils.compressBytes("Otp_Report" + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray()
        );

    }

    @Override
    public BaseResponse generateFirstLastLoginReport(FirstLastLoginReportRequest firstLastLoginReportRequest)
            throws Exception {

        Assert.notNull(firstLastLoginReportRequest, "FirstLastLoginReportRequest object must not be null");

        //List<FirstLastLoginReportResponse> firstLastLoginReportResponses = new ArrayList<FirstLastLoginReportResponse>();

        TypedAggregation firstLastLoginAggregation = reportingPipelineHelper
                .firstLastLoginReportAggregaterBuilder(firstLastLoginReportRequest);

        Stream<BasicDBObject> basicDBObjectStream = reportingRepository
                .firstLastLoginReport(firstLastLoginAggregation);


        List<FirstLastLoginReportResponse> firstLastLoginReportResponses = basicDBObjectStream.map(object -> {
            FirstLastLoginReportResponse response = new FirstLastLoginReportResponse();

            response.setDay(null != object.getString("day") ? object
                    .getString("day") : "");
            response.setMonth(null != object.getString("month") ? object
                    .getString("month") : "");
            response.setYear(null != object.getString("year") ? object
                    .getString("year") : "");
            response.setDealerId(null != object.getString("dealerId") ? object
                    .getString("dealerId") : "");
            response.setCount(-1 != object.getInt("count") ? object
                    .getInt("count") : 0);
            response.setFirstLogin(null != object.getDate("firstLogin") ? GngDateUtil
                    .getDdMmYyyyHhMmSsDateFormat(new DateTime(object
                            .getDate("firstLogin"))) : null);
            response.setLastLogin(null != object.getDate("lastLogin") ? GngDateUtil
                    .getDdMmYyyyHhMmSsDateFormat(new DateTime(object
                            .getDate("lastLogin"))) : null);

            return response;
        }).collect(Collectors.toList());


        /*for (BasicDBObject object : firstLastLoginReport) {
            FirstLastLoginReportResponse response = new FirstLastLoginReportResponse();

            response.setDay(null != object.getString("day") ? object
                    .getString("day") : "");
            response.setMonth(null != object.getString("month") ? object
                    .getString("month") : "");
            response.setYear(null != object.getString("year") ? object
                    .getString("year") : "");
            response.setDealerId(null != object.getString("dealerId") ? object
                    .getString("dealerId") : "");
            response.setCount(-1 != object.getInt("count") ? object
                    .getInt("count") : 0);
            response.setFirstLogin(null != object.getDate("firstLogin") ? GngDateUtil
                    .getDdMmYyyyHhMmSsDateFormat(new DateTime(object
                            .getDate("firstLogin"))) : null);
            response.setLastLogin(null != object.getDate("lastLogin") ? GngDateUtil
                    .getDdMmYyyyHhMmSsDateFormat(new DateTime(object
                            .getDate("lastLogin"))) : null);

            firstLastLoginReportResponses.add(response);
        }*/

        return GngUtils.getBaseResponse(HttpStatus.OK, firstLastLoginReportResponses);

    }

    @Override
    public byte[] generateZippedFirstLastLoginReport(FirstLastLoginReportRequest firstLastLoginReportRequest) throws Exception {

        Assert.notNull(firstLastLoginReportRequest, "FirstLastLoginReportRequest object must not be null");

        TypedAggregation firstLastLoginAggregation = reportingPipelineHelper
                .firstLastLoginReportAggregaterBuilder(firstLastLoginReportRequest);

        Stream<BasicDBObject> basicDBObjectStream = reportingRepository
                .firstLastLoginReport(firstLastLoginAggregation);

        StringBuilder response = new StringBuilder();

        /*BasicDBObject basicDBObject = firstLastLoginReport.getMappedResults()
                .get(0);

        Set<String> keySet2 = basicDBObject.keySet();

        for (String string : keySet2) {
            if (StringUtils.isBlank(response)) {
                if (StringUtils.equalsIgnoreCase(string, "day")
                        || StringUtils.equalsIgnoreCase(string, "month")
                        || StringUtils.equalsIgnoreCase(string, "year")) {
                    if (response.indexOf("date") == -1) {
                        response.append("date");
                    }
                }
            } else {

                if (StringUtils.equalsIgnoreCase(string, "day")
                        || StringUtils.equalsIgnoreCase(string, "month")
                        || StringUtils.equalsIgnoreCase(string, "year")) {
                    if (response.indexOf("date") == -1) {
                        response.append("date");
                    }
                } else {
                    response.append("," + string);
                }
            }
        }*/

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        outputStream1 = new ByteArrayOutputStream();

        reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

        reportOutputStreamWriter.write(response.toString());

        reportOutputStreamWriter.append('\n');

        basicDBObjectStream.forEach(object -> {
            System.out.println(object);
        });


        /*for (BasicDBObject object : firstLastLoginReport) {

            response = new StringBuilder();

            String date;

            date = null != object.getString("day") ? object.getString("day") : "";
            date += "/";

            date += null != object.getString("month") ? object.getString("month") : "";

            date += "/";

            date += null != object.getString("year") ? object.getString("year") : "";

            response.append(dateFormat.format(dateFormat.parse(date)));

            response.append(",");

            response.append(null != object.getString("dsaId") ? object.getString("dsaId") : "");

            response.append(",");

            response.append(-1 != object.getInt("count") ? object.getInt("count") : 0);

            response.append(",");

            response.append(null != object.getDate("firstLogin") ? GngDateUtil.getTimeStamp(new DateTime(object.getDate("firstLogin")))
                    : null);
            response.append(",");
            response.append(null != object.getDate("lastLogin") ? GngDateUtil
                    .getTimeStamp(new DateTime(object.getDate("lastLogin")))
                    : null);

            reportOutputStreamWriter.write(response.toString());
            reportOutputStreamWriter.append('\n');

        }*/

        reportOutputStreamWriter.close();


        return GnGFileUtils.compressBytes("Login_Report"
                        + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());

    }

    @Override
    public BaseResponse generateSalesIncentiveReport(SalesReportRequest reportRequest) throws Exception {


        Stream<SalesReportResponse> salesReportResponseStream = null;

        //reportingRepository.salesIncentiveReport(reportingPipelineHelper.generateSalesReport(reportRequest));

        List<SalesReportResponse> salesReportResponses = salesReportResponseStream.map(salesReportResponse -> {

            List<Kyc> kycs = salesReportResponse.getKycs();

            kycs.parallelStream().forEach(kyc -> {

                String kycNumber = kyc.getKycNumber();

                if (KYC_TYPES.PAN.toString().equalsIgnoreCase(kyc.getKycName())) {

                    salesReportResponse.setPan(kycNumber);

                } else if (KYC_TYPES.AADHAR.toString().equalsIgnoreCase(kyc.getKycName())) {

                    salesReportResponse.setAadhar(kycNumber);

                } else if (KYC_TYPES.VOTER_ID.toString().equalsIgnoreCase(kyc.getKycName())) {

                    salesReportResponse.setVoterId(kycNumber);

                } else if (KYC_TYPES.DRIVING_LICENSE.toString().equalsIgnoreCase(kyc.getKycName())) {

                    salesReportResponse.setDrivingLicense(kycNumber);

                } else if (KYC_TYPES.PASSPORT.toString().equalsIgnoreCase(kyc.getKycName())) {

                    salesReportResponse.setPassport(kycNumber);
                }

            });

            salesReportResponse.setKycs(null);

            return salesReportResponse;

        }).collect(Collectors.toList());

        return GngUtils.getBaseResponse(HttpStatus.OK, salesReportResponses);

    }

    @Override
    public byte[] generateZippedSalesIncentiveReport(SalesReportRequest reportRequest) throws Exception {

        Stream salesReportResponseStream = reportingRepository.salesIncentiveReport(reportingPipelineHelper
                .generateSalesReport(reportRequest));

        ReportCompressionUtility compressionUtility = new ReportCompressionUtility();

        return compressionUtility.salesReport(salesReportResponseStream);

    }

    @Override
    public byte[] generateZippedSalesReport(SalesReportRequest reportRequest)
            throws Exception {

        Assert.notNull(reportRequest, "Sales report request must not be blank or null !!");

        Optional<String> product = reportRequest.getProducts().parallelStream().findFirst();

        Query gonogoCustomerQuery = QueryBuilder.buildGoNoGoApplicationQuery(
                reportRequest.getInstitutionId(), reportRequest.getStartDate(),
                reportRequest.getEndDate(), product.get());


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        final OutputStreamWriter reportOutputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(ReportCustomConfiguration.buildSalesReportConfiguration());

        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

        flatReprtConfiguration.setReportName("GNGVersion2");

        flatReprtConfiguration.setReportType("CSV");

        flatReprtConfiguration.setReportFormat("CSV");


        try {

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

        } catch (IOException e3) {

            e3.printStackTrace();

            logger.error("error occurred while preparing stream for zipped sales report with probable cause [{}] ", e3.getMessage());

        }

        Stream<GoNoGoCustomerApplication> goNoGoCustomerApplicationStream = reportingRepository.zippedSalesReport(gonogoCustomerQuery);

        if (null != goNoGoCustomerApplicationStream) {

            goNoGoCustomerApplicationStream.forEach(goNoGoCustomerApplication -> {

                try {

                    GoNoGoApplicationToJsonParser applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);

                    PostIpaRequest postIpaRequest = reportingRepository
                            .getPostIpaRequest(QueryBuilder
                                    .buildPostIpaRequest(goNoGoCustomerApplication
                                            .getGngRefId()));

                    FileHandoverDetails fileHandoverDetails = reportingRepository
                            .getFilehandOverDetails(QueryBuilder
                                    .buildFilehandOverDetails(goNoGoCustomerApplication
                                            .getGngRefId()));

                    DealerBranchMaster dealerBranchMaster = reportingRepository
                            .getBranchMaster(goNoGoCustomerApplication.getApplicationRequest()
                                    .getHeader().getDealerId(), goNoGoCustomerApplication.getApplicationRequest()
                                    .getHeader().getInstitutionId());

                    if (null != dealerBranchMaster) {
                        applicationToJsonParser
                                .build(dealerBranchMaster);
                    }

                    if (null != fileHandoverDetails) {

                        applicationToJsonParser
                                .build(fileHandoverDetails);
                    }

                    if (null != postIpaRequest) {

                        applicationToJsonParser
                                .build(postIpaRequest);
                    }

                    StringBuffer row = new StringBuffer();

                    row.append(CharMatcher.anyOf("\n\r\t").removeFrom(applicationToJsonParser.build()
                            .enrichJson(flatReprtConfiguration).toString()));


                    reportOutputStreamWriter.write(row.toString() + '\n');

                } catch (Exception e) {

                    e.printStackTrace();

                    logger.error(" error occurred while generating zipped credit report with probable cause [{}] ", e.getMessage());

                }

            });

        }

        reportOutputStreamWriter.close();

        return GnGFileUtils.compressBytes("Sales_Offered" + GngDateUtil.getMmDdYyyySlash(new DateTime()) + ".csv",
                outputStream.toByteArray());

    }

    @Override
    public byte[] generateZippedCreditReport(
            CreditReportRequest creditReportRequest) throws Exception {

        Assert.notNull(creditReportRequest,
                "Credit report request must not be blank or null !!");


        Optional<String> product = creditReportRequest.getProducts().parallelStream().findFirst();

        Query gonogoCustomerQuery = QueryBuilder.buildGoNoGoApplicationQuery(
                creditReportRequest.getInstitutionId(),
                creditReportRequest.getStartDate(),
                creditReportRequest.getEndDate(), product.get());

        List<GoNoGoCustomerApplication> creditReportRecords = reportingRepository
                .zippedCreditReport(gonogoCustomerQuery);

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(ReportCustomConfiguration
                .buildCreditReportConfiguration());

        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

        flatReprtConfiguration.setReportName("GNGVersion2");

        flatReprtConfiguration.setReportType("CSV");

        flatReprtConfiguration.setReportFormat("CSV");

        GoNoGoApplicationToJsonParser applicationToJsonParser;

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        try {
            outputStream1 = new ByteArrayOutputStream();
            reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                    StandardCharsets.UTF_8);
            reportOutputStreamWriter.write(flatReprtConfiguration
                    .getFileHeader());
            reportOutputStreamWriter.append('\n');

        } catch (IOException e3) {
            e3.printStackTrace();
        }

        for (GoNoGoCustomerApplication goNoGoCustomerApplication : creditReportRecords) {

            applicationToJsonParser = new GoNoGoApplicationToJsonParser(
                    goNoGoCustomerApplication);

            Query buildSerialNumberInfoQuery = QueryBuilder.buildSerialNumberInfoQuery(goNoGoCustomerApplication);

            SerialNumberInfo serialNumberInfo = reportingRepository.fetchSerialNumberByRefIdNStatus(buildSerialNumberInfoQuery);

            if (null != serialNumberInfo) {
                applicationToJsonParser.build(serialNumberInfo);
            }


            PostIpaRequest postIpaRequest = reportingRepository
                    .getPostIpaRequest(QueryBuilder
                            .buildPostIpaRequest(goNoGoCustomerApplication
                                    .getGngRefId()));


            addMetaData(goNoGoCustomerApplication, applicationToJsonParser);

            StringBuffer row = new StringBuffer();

            if (null != postIpaRequest) {
                applicationToJsonParser.build();
                row.append(applicationToJsonParser
                        .build(postIpaRequest.getPostIPA())
                        .enrichJson(flatReprtConfiguration).toString()
                        .replaceAll("[\\\n|\\\r|\\\t]", ""));
            } else {
                row.append(applicationToJsonParser.build()
                        .enrichJson(flatReprtConfiguration).toString()
                        .replaceAll("[\\\n|\\\r|\\\t]", ""));
            }

            reportOutputStreamWriter.write(row.toString());
            reportOutputStreamWriter.write('\n');
        }

        reportOutputStreamWriter.close();

        return GnGFileUtils.compressBytes("CREDIT_REPORT_"
                        + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());

    }

    private void addMetaData(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            GoNoGoApplicationToJsonParser applicationToJsonParser) {

        Query query = new Query();

        query.addCriteria(Criteria.where("dealerID").is(goNoGoCustomerApplication.getApplicationRequest().
                getHeader().getDealerId()).and("active").is(true));

        DealerEmailMaster dealerEmailMaster = reportingRepository.fetchDealerEmailMasterBasedOnDealerId(query);

        if (null != dealerEmailMaster) {
            applicationToJsonParser.populateMetaData(dealerEmailMaster);
        }
    }

    @Override
    @Deprecated
    public BaseResponse createTreeViewForAssetModelReport() {

        List<AssetModelTreeView> assetModelTreeViews = new ArrayList<>();

        AssetModelTreeView assetModelTreeView = new AssetModelTreeView();

        Set<String> manufacturerSet = reportingRepository.getAssetModelMasterManufacturer("");

        assetModelTreeView.setParent("null");

        assetModelTreeView.setName("Manufactuters");

        List<AssetModelTreeView> manufacturerList = manufacturerSet.parallelStream().map(manufacturerObj -> {

            AssetModelTreeView manufacturer = new AssetModelTreeView();

            manufacturer.setParent("Manufactuters");

            manufacturer.setName(manufacturerObj);

            return manufacturer;

        }).collect(Collectors.toList());


        assetModelTreeView.setChildren(manufacturerList);

        List<AssetModelTreeView> categoryList = new ArrayList<AssetModelTreeView>();
        Map<String, Set<String>> categoryMap = reportingRepository.getCategoryAgainstManufacturer(manufacturerSet);

        for (AssetModelTreeView manufacurer : manufacturerList) {
            for (Map.Entry<String, Set<String>> entry : categoryMap.entrySet()) {
                if (entry.getKey().equals(manufacurer.getName())) {
                    manufacurer.setChildren(setCategoriesInManifacturer(
                            manufacurer, categoryList, entry.getValue()));

                }
            }
        }


        assetModelTreeViews.add(assetModelTreeView);

        return GngUtils.getBaseResponse(HttpStatus.OK, assetModelTreeViews);
    }

    private List<AssetModelTreeView> setCategoriesInManifacturer(
            AssetModelTreeView manufacurer,
            List<AssetModelTreeView> categoryList, Set<String> categories) {

        return categories.parallelStream().map(ctg -> {

            AssetModelTreeView categoryObj = new AssetModelTreeView();

            categoryObj.setName(ctg);

            categoryObj.setParent(manufacurer.getName());

            Set<String> makeSet = reportingRepository.getMake(ctg, manufacurer.getName());

            categoryObj.setChildren(setMakeInCategory(manufacurer.getName(), categoryObj, makeSet));

            return categoryObj;

        }).collect(Collectors.toList());

    }

    private List<AssetModelTreeView> setMakeInCategory(String manufacturer,
                                                       AssetModelTreeView categoryObj, Set<String> makeSet) {

        return makeSet.parallelStream().map(make -> {

            AssetModelTreeView makeObj = new AssetModelTreeView();

            makeObj.setName(make);

            makeObj.setParent(categoryObj.getName());

            Set<String> modelSet = reportingRepository.getModels(categoryObj.getName(), make, manufacturer);

            makeObj.setChildren(setModelInMake(makeObj, modelSet));

            return makeObj;

        }).collect(Collectors.toList());

    }

    private List<AssetModelTreeView> setModelInMake(AssetModelTreeView makeObj,
                                                    Set<String> modelSet) {

        List<AssetModelTreeView> modelList = new ArrayList<AssetModelTreeView>();

        for (String model : modelSet) {

            AssetModelTreeView modelObj = new AssetModelTreeView();

            modelObj.setName(model);

            modelObj.setParent(makeObj.getName());

            modelList.add(modelObj);

        }

        return modelList;
    }


    private Map<String, Integer> getManufacturerMap(StackRequest stackRequest) {

        ApplicationMetadataRequest appMetaDataRequest = new ApplicationMetadataRequest() {
            {
                setHeader(stackRequest.getHeader());
            }
        };
        String productName = stackRequest.getHeader().getProduct().toProductName();
        String institutionId = stackRequest.getHeader().getInstitutionId();
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        Set<String> manufacturers = masterRepository
                .getManufacturer(appMetaDataRequest, productAliasName);

        Map<String, Integer> manuFacturerMap = new HashMap<String, Integer>();

        for (String manfact : manufacturers) {
            manuFacturerMap.put(manfact, 0);
        }
        return manuFacturerMap;
    }

    @Override
    public BaseResponse getSerialLog() {
        return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getSerialNumberLog());
    }

    @Override
    public byte[] getSerialNumberLogZipReportByVendor(String institutionId, String vendor, Date startDate, Date endDate) throws SystemException {

        try {

            Query query = new Query();

            query.addCriteria(new Criteria().andOperator(
                    Criteria.where("institutionId").is(institutionId),
                    Criteria.where("vendor").is(vendor),
                    Criteria.where("dateTime").lte(endDate).gte(startDate)
            ));

            List<SerialNumberInfo> serialNumberInfoList = reportingRepository.getSerialNumberByIdOrVendorLog(query);

            return getSerialNumberCsvZipReport(serialNumberInfoList);


        } catch (Exception e) {

            e.printStackTrace();

            logger.error("error occurred while generating serial number log zip report by vendor with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while generating serial number log zip report by vendor with probable cause [%s] ", e.getMessage()));

        }
    }

    private byte[] getSerialNumberCsvZipReport(List<SerialNumberInfo> serialNumberInfoList) throws SystemException {

        ByteArrayOutputStream outputStream1;

        try {

            OutputStreamWriter reportOutputStreamWriter;


            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(SerialNumberReportConfiguration.serialLogConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("SerialNumberLogReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            SerialNumberReportParser serialNumberReportParser;

            if (!CollectionUtils.isEmpty(serialNumberInfoList)) {

                for (SerialNumberInfo serialNumberInfo : serialNumberInfoList) {

                    serialNumberReportParser = new SerialNumberReportParser(serialNumberInfo);

                    StringBuffer row = new StringBuffer();

                    row.append(serialNumberReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", ""));

                    reportOutputStreamWriter.write(row.toString());
                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("SERIAL_NUMBER_LOG_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream1.toByteArray());

        } catch (Exception e) {

            e.printStackTrace();

            logger.error(" error occurred while preparing serial number csv zip report with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while preparing serial number csv zip report with probable cause [%s]",
                    e.getMessage()));

        }

    }

    @Override
    public byte[] getLosUtrUpdateReport(LosUtrUpdateReportRequest losUtrUpdateReportRequest) throws SystemException {

        List<LosUtrDetailsMaster> losUtrDetailsMasterList = reportingRepository.getLosUtrMaster(losUtrUpdateReportRequest.getHeader().getInstitutionId());

        try {

            OutputStreamWriter reportOutputStreamWriter;

            ByteArrayOutputStream outputStream1;

            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(LosUtrUpdateReportConfiguration.losUtrUpdateConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("LosUtrUpdateDetailsReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            LosUtrUpdateReportParser losUtrUpdateReportParser;

            if (!CollectionUtils.isEmpty(losUtrDetailsMasterList)) {

                for (LosUtrDetailsMaster losUtrDetailsMaster : losUtrDetailsMasterList) {

                    losUtrUpdateReportParser = new LosUtrUpdateReportParser(losUtrDetailsMaster);

                    StringBuffer row = new StringBuffer();

                    row.append(losUtrUpdateReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", ""));

                    reportOutputStreamWriter.write(row.toString());

                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("LOS_UTR_UPDATE_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream1.toByteArray());

        } catch (Exception e) {

            e.printStackTrace();

            logger.error(" error occurred while  fetching getLosUtrUpdateReport with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format(" Exception while fetching getLosUtrUpdateReport with cause [%s]", e.getMessage()));
        }
    }


    @Override
    public BaseResponse getSerialByIDLog(String refID, String vendor) throws Exception {

        try {

            Criteria[] andCriterias = new Criteria[2];

            if (null != refID) {
                andCriterias[0] = Criteria.where("refID").is(refID);
            }

            if (null != vendor) {
                andCriterias[1] = Criteria.where("vendor").is(vendor);
            }

            Query query = new Query();

            query.addCriteria(new Criteria().andOperator(andCriterias));

            logger.debug("getSerialByIDLog report formed query {}", query);


            return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getSerialNumberByIdOrVendorLog(query));

        } catch (Exception e) {

            e.printStackTrace();

            logger.error(" error occurred while fetching getSerialIdbyLog with cause probable cause [{}] ", e.getMessage());

            throw new Exception(String.format("Exception while fetching getSerialIdbyLog with cause [%s]", e.getMessage()));
        }
    }

    @Override
    public byte[] getDeviceReport(Date startDate, Date endDate, String product,
                                  String institutionId) throws Exception {
        byte[] csvReport = null;

        Query query = new Query();

        query.addCriteria(new Criteria().andOperator(
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.INSTITUTION_ID).is(institutionId),
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.LOAN_TYPE).is(product),
                Criteria.where(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SUBMIT_DATE_TIME).lte(endDate).gte(startDate)
        ));

        getDeviceReportIncludeFields().parallelStream().forEach(a -> query.fields().include(a));


        Stream<GoNoGoCustomerApplication> customerApplicationStream = reportingRepository.getCustomerApplication(query);

        List<DeviceInfoRptDomain> deviceInformationReportList = customerApplicationStream.map(customerApplication -> {


            DeviceInfoRptDomain deviceInformationReport = new DeviceInfoRptDomain();

            deviceInformationReport.setReferenceID(customerApplication.getGngRefId());

            deviceInformationReport.setDate(customerApplication.getDateTime());

            deviceInformationReport.setTime(customerApplication.getDateTime());

            if (null != customerApplication.getApplicationRequest()
                    && null != customerApplication
                    .getApplicationRequest().getHeader()) {

                deviceInformationReport.setAppSource(customerApplication.getApplicationRequest().getHeader()
                        .getApplicationSource());

                deviceInformationReport.setDsaID(customerApplication
                        .getApplicationRequest().getHeader().getDsaId());

                deviceInformationReport
                        .setInstitutionID(customerApplication
                                .getApplicationRequest().getHeader()
                                .getInstitutionId());

                if (null != customerApplication.getApplicationRequest()
                        .getHeader().getDeviceInfo()) {

                    deviceInformationReport
                            .setIpAddress(customerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getIpAddress());

                    deviceInformationReport
                            .setImeiNumber(customerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getImeiNumber());

                    deviceInformationReport
                            .setDeviceID(customerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getDeviceID());


                    deviceInformationReport.setDeviceManufacturer(customerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getDeviceManufacturer());


                    deviceInformationReport.setDeviceModel(customerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getDeviceModel());


                    if (null != customerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getBrowserDetails()) {
                        deviceInformationReport
                                .setBrowserName(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getBrowserDetails()
                                        .getBrowserName());
                        deviceInformationReport
                                .setBrowserVersion(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getBrowserDetails()
                                        .getVersion());
                    }

                    if (null != customerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getGeoLocation()) {
                        deviceInformationReport.setLattitude(String
                                .valueOf(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getGeoLocation()
                                        .getLatitude()));
                        deviceInformationReport.setLongitude(String
                                .valueOf(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getGeoLocation()
                                        .getLongitude()));
                    }

                    if (null != customerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getOsDetails()) {
                        deviceInformationReport
                                .setMacAddress(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getOsDetails()
                                        .getMacAddress());
                        deviceInformationReport
                                .setOsName(customerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getOsDetails()
                                        .getOsName());
                    }

                }
            }

            return deviceInformationReport;

        }).collect(Collectors.toList());


        //List<GoNoGoCustomerApplication> goNoGoCustomerApplications = null; //reportingRepository.getCustomerApplicationOld(query);


        JsonFlattener parser = new JsonFlattener();

        CSVWriter writer = new CSVWriter();

        List<Map<String, String>> flatJson;

        try {

            String jasksonJson = JsonUtil.ObjectToString(deviceInformationReportList);

            flatJson = parser.parseJson(jasksonJson);

            csvReport = writer.writeAsCSV(flatJson);


        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Exception while fetching device report with cause {}", e.getMessage());

            throw new Exception(String.format("Exception while fetching device report with cause [%s]", e.getMessage()));
        }

        return csvReport;

    }

    @Override
    public BaseResponse getSerialNumberRequestLog(String refID, String vendor) {

        return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getSerialNumberRequestLog(
                refID, vendor));
    }

    @Override
    public BaseResponse getSerialNumberRequestLog() {
        return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getSerialNumberRequestLog());
    }

    @Override
    public BaseResponse getMailLogReport(String refID) {
        return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getMailLogReport(refID));
    }

    @Override
    public BaseResponse getMailLogReport() {

        return GngUtils.getBaseResponse(HttpStatus.OK, reportingRepository.getMailLogReport());
    }

    @Override
    public byte[] getSerialNumberRollBackReportByVendor(SerialNumberInfoLogRequest serialNumberInfoLogRequest) throws SystemException {
        try {

            List<RollbackFeatureLog> rollbackFeatureLogList = reportingRepository.getSerialNumberRollBackLogByVendor(serialNumberInfoLogRequest);

            return getSerialNumberRollBackCsvZipReport(rollbackFeatureLogList);


        } catch (Exception e) {

            e.printStackTrace();

            logger.error("error occurred while generating serial number rollback log zip report by vendor with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while generating serial number rollback log zip report by vendor with probable cause [%s] ", e.getMessage()));

        }
    }

    @Override
    public BaseResponse getAadhaarLogReport(AadhaarLogRequest aadhaarLogRequest) {
        List<AadharLog> aadhaarLogs = reportingRepository.findAadhaarLogs(aadhaarLogRequest).collect(Collectors.toList());
        BaseResponse baseResponse;
        if (null != aadhaarLogs && !aadhaarLogs.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, aadhaarLogs);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getMbPushLogs(DmzRequest dmzRequest) {

        List<MbPushResponse> mbPushResponses = reportingRepository.getMbPushResponseLogs(dmzRequest);

        BaseResponse baseResponse;

        if (!org.apache.commons.collections.CollectionUtils.isEmpty(mbPushResponses)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, mbPushResponses);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getTvrLogs(String refId) {

        BaseResponse baseResponse;

        List<TVRLogs> tvrLogs = reportingRepository.getTvrLogs(refId);

        if (!CollectionUtils.isEmpty(tvrLogs)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tvrLogs);

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    private byte[] getSerialNumberRollBackCsvZipReport(List<RollbackFeatureLog> rollbackFeatureLogList) {


        try {

            OutputStreamWriter reportOutputStreamWriter;

            ByteArrayOutputStream outputStream1;

            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(RollbackFeatureReportConfiguration.serialRollbackConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("SerialNumberRollbackReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            RollbackFeatureReportParser rollbackFeatureReportParser;

            if (!CollectionUtils.isEmpty(rollbackFeatureLogList)) {

                for (RollbackFeatureLog rollbackFeatureLog : rollbackFeatureLogList) {

                    rollbackFeatureReportParser = new RollbackFeatureReportParser(rollbackFeatureLog);

                    StringBuffer row = new StringBuffer();

                    row.append(rollbackFeatureReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\t\\r\\n]", ""));

                    reportOutputStreamWriter.write(row.toString());

                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("SERIAL_NUMBER_ROLLBACK_REPORT"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream1.toByteArray());

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Problem occurred while generating serial number rollback report with probable cause [{}]", e.getMessage());

            throw new SystemException(String.format("Problem occurred while generating serial number rollback report with probable cause [{%s}]", e.getMessage()));
        }
    }

    /**
     * @return
     */
    private List<String> getDeviceReportIncludeFields() {

        List<String> includeField = new ArrayList<String>();
        includeField.add(GoNoGoCustomerApplicationKeyConstants.REF_ID);
        includeField
                .add(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SUBMIT_DATE_TIME);
        includeField.add(GoNoGoCustomerApplicationKeyConstants.DSA_ID);
        includeField
                .add(GoNoGoCustomerApplicationKeyConstants.APPLICATION_SOURCE);
        includeField
                .add(GoNoGoCustomerApplicationKeyConstants.INSTITUTION_ID);
        includeField.add(GoNoGoCustomerApplicationKeyConstants.DEVICE_INFO);

        return includeField;
    }


    @Override
    public byte[] getAccountNumberLogZipReport(String institutionId, String product, Date startDate, Date endDate) {

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("product").is(product)
                    .and("dateTime").lte(GngDateUtil.getEndTimeToDate(endDate)).gte(GngDateUtil.getZeroTimeFromDate(startDate)));

            List<AccountNumberInfo> accountNumberInfoList = reportingRepository.getAccountNumberLog(query);

            return getAccountNumberCsvZipReport(accountNumberInfoList);


        } catch (Exception e) {

            logger.error("error occurred while generating account number log zip with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while generating account number log zip report with probable cause [%s] ", e.getMessage()));

        }
    }

    private byte[] getAccountNumberCsvZipReport(List<AccountNumberInfo> accountNumberInfoList) throws SystemException {

        ByteArrayOutputStream outputStream1;

        try {

            OutputStreamWriter reportOutputStreamWriter;

            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(AccountNumberReportConfiguration.accountNumberLogConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("AccountNumberLogReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            AccountNumberReportParser accountNumberReportParser;

            if (!CollectionUtils.isEmpty(accountNumberInfoList)) {

                for (AccountNumberInfo accountNumberInfo : accountNumberInfoList) {

                    accountNumberReportParser = new AccountNumberReportParser(accountNumberInfo);

                    StringBuffer row = new StringBuffer();

                    row.append(accountNumberReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", ""));

                    reportOutputStreamWriter.write(row.toString());
                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("ACCOUNT_NUMBER_LOG_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream1.toByteArray());

        } catch (Exception e) {

            logger.error(" error occurred while preparing account number csv zip report with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while preparing account number csv zip report with probable cause [%s]",
                    e.getMessage()));

        }

    }

    @Override
    public byte[] getSerialNumberApplicableVendorLogZipReport(String institutionId, Date startDate, Date endDate) {

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("institutionId").is(institutionId)
                    .and("dateTime").lte(GngDateUtil.getEndTimeToDate(endDate)).gte(GngDateUtil.getZeroTimeFromDate(startDate)));

            List<SerialNumberApplicableVendorLog> serialNumberApplicableCheckLogList = reportingRepository.getSerialNumberApplicableVendorLog(query);

            return getSerialNumberApplicableVendorCsvZipReport(serialNumberApplicableCheckLogList);


        } catch (Exception e) {

            logger.error("error occurred while generating serial number applicable log zip with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while generating serial number applicable log zip report with probable cause [%s] ", e.getMessage()));

        }
    }

    private byte[] getSerialNumberApplicableVendorCsvZipReport(List<SerialNumberApplicableVendorLog> serialNumberApplicableVendorLogList) throws SystemException {

        ByteArrayOutputStream outputStream1;

        try {

            OutputStreamWriter reportOutputStreamWriter;

            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(SerialNumberApplicableVendorReportConfiguration.serialNumberApplicableVendorLogConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("SerialNumberApplicableVendorLogReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            SerialNumberApplicableVendorReportParser serialNumberApplicableVendorReportParser;

            if (!CollectionUtils.isEmpty(serialNumberApplicableVendorLogList)) {

                for (SerialNumberApplicableVendorLog serialNumberApplicableVendorLog : serialNumberApplicableVendorLogList) {

                    serialNumberApplicableVendorReportParser = new SerialNumberApplicableVendorReportParser(serialNumberApplicableVendorLog);

                    StringBuffer row = new StringBuffer();

                    row.append(serialNumberApplicableVendorReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", ""));

                    reportOutputStreamWriter.write(row.toString());
                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("SERIAL_NUMBER_APPLICABLE_VENDOR_LOG_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream1.toByteArray());

        } catch (Exception e) {

            logger.error(" error occurred while preparing serial number applicable vendor csv zip report with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while preparing serial number applicable vendor csv zip report with probable cause [%s]",
                    e.getMessage()));

        }

    }

    @Override
    public BaseResponse getSBFCLMSLogByRefId(String refId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("sbfclmsIntegrationRequest.refID").is(refId));

        return getBaseResponse(HttpStatus.OK, reportingRepository.getSBFCLMSLogByRefId(query));
    }

    @Override
    public byte[] getSBFCLMSLogZipReport(String instId, Date startDate, Date endDate) {

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("instId").is(instId)
                    .and("dateTime").lte(GngDateUtil.getEndTimeToDate(endDate)).gte(GngDateUtil.getZeroTimeFromDate(startDate)));

            List<SBFCLMSIntegrationLog> sbfclmsIntegrationLogs = reportingRepository.getSBFCLMSLog(query);

            return getSBFCLMSCsvZipReport(sbfclmsIntegrationLogs);


        } catch (Exception e) {

            logger.error("error occurred while generating account number log zip with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while generating account number log zip report with probable cause [%s] ", e.getMessage()));

        }
    }

    private byte[] getSBFCLMSCsvZipReport(List<SBFCLMSIntegrationLog> sbfclmsIntegrationLogs) throws SystemException {

        ByteArrayOutputStream outputStream;

        try {

            OutputStreamWriter reportOutputStreamWriter;

            FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

            flatReprtConfiguration.setHeaderMap(SBFCLMSReportConfiguration.SBFCLMSLogConfigMap);

            flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

            flatReprtConfiguration.setReportName("SBFCLMSLogReport");

            flatReprtConfiguration.setReportType("CSV");

            flatReprtConfiguration.setReportFormat("CSV");

            outputStream = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            SBFCLMSReportParser sbfclmsReportParser;

            if (!CollectionUtils.isEmpty(sbfclmsIntegrationLogs)) {

                for (SBFCLMSIntegrationLog sbfclmsIntegrationLog : sbfclmsIntegrationLogs) {

                    sbfclmsReportParser = new SBFCLMSReportParser(sbfclmsIntegrationLog);

                    StringBuffer row = new StringBuffer();

                    row.append(sbfclmsReportParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", ""));

                    reportOutputStreamWriter.write(row.toString());
                    reportOutputStreamWriter.write('\n');
                }
            }

            reportOutputStreamWriter.close();

            return GnGFileUtils.compressBytes("SBFC_LMS_LOG_REPORT_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                    outputStream.toByteArray());

        } catch (Exception e) {

            logger.error(" error occurred while preparing SBFC-LMS csv zip report with probable cause [{}] ", e.getMessage());

            throw new SystemException(String.format("error occurred while preparing SBFC-LMS csv zip report with probable cause [%s]",
                    e.getMessage()));

        }
    }

    private BaseResponse getBaseResponse(HttpStatus httpStatus, Object buzResponse) {

        if (null == buzResponse)
            buzResponse = Collections.emptyMap();

        return BaseResponse.builder()
                .payload(new Payload<>(buzResponse))
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .build();
    }

}
