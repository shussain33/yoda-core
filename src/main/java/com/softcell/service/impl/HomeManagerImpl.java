package com.softcell.service.impl;


import com.softcell.config.AuthenticationConfiguration;
import com.softcell.config.ServiceCommunication;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UrlConfigurationRepository;
import com.softcell.dao.mongodb.repository.UrlConfigurationRepositoryImpl;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.cache.services.RedisService;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.security.*;
import com.softcell.gonogo.model.security.v2.LogoutBaseResponse;
import com.softcell.gonogo.model.security.v2.LogoutRequestV2;
import com.softcell.gonogo.model.security.v2.Product;
import com.softcell.gonogo.model.security.v2.RoleDetail;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.service.ConfigurationManager;
import com.softcell.service.HomeManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * @author yogeshb
 */
@Service
public class HomeManagerImpl implements HomeManager {

    private static final Logger logger = LoggerFactory.getLogger(HomeManagerImpl.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private RedisService redisService;

    @Autowired
    private LookupService lookupService;

    @Override
    public BaseResponse login(
            LoginRequest loginRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("login service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(loginRequest.getHeader(), httpRequest,
                GNGWorkflowConstant.LOGIN.toFaceValue());
        if (StringUtils.isNotBlank(loginRequest.getUserName())) {
            activityLog.setUserName(loginRequest.getUserName());
        }
        activityLog.setRefId(loginRequest.getRefId());
        activityLog.setInstitutionId(loginRequest.getInstId());

        com.softcell.gonogo.model.security.LoginServiceRequest loginServiceRequest = new com.softcell.gonogo.model.security.LoginServiceRequest();
        loginServiceRequest.setUsername(loginRequest.getUserName());
        loginServiceRequest.setPassword(loginRequest.getPassword());
        loginServiceRequest.setMode(ReqResHelperConstants.W.name());
        loginServiceRequest.setApplication(ReqResHelperConstants.GNG.name());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY.toFaceValue());

        logger.debug("login service calling postAuthRequest service  ");

        com.softcell.gonogo.model.security.LoginServiceResponse loginServiceResponse = (com.softcell.gonogo.model.security.LoginServiceResponse) TransportUtils.
                postAuthRequest(loginServiceRequest, config.getUrl(), com.softcell.gonogo.model.security.LoginServiceResponse.class);

        BaseResponse baseResponse ;

        if (loginServiceResponse != null) {
            if (StringUtils.equalsIgnoreCase(Status.ERROR.name(),
                    loginServiceResponse.getStatus())) {
                logger.debug("login service received error {} from postAuth service", loginServiceResponse.getError());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,loginServiceResponse);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", loginServiceResponse.getError()));

            } else if (null != loginRequest.getHeader()
                    && !configurationManager.isValidVersion(loginServiceResponse.getUserDetails().get(0).getINSTITUTIONID(), loginRequest
                    .getHeader().getApplicationSource())) {

                logger.debug("login service call failed due to unsupported version");

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_VERSION_DSCR)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", ErrorCode.INVALID_VERSION_DSCR));

            } else {
                logger.info("login service completed successfully with response {}",
                        loginServiceResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,loginServiceResponse);
                activityLog.setStatus((GNGWorkflowConstant.SUCCESS.toFaceValue()));
            }
       } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
            activityLog.setCustomMsg(String.format("Error : %s", HttpStatus.INTERNAL_SERVER_ERROR.name()));
        }

        // Persist activityLog in DB
        logger.debug(String.format("Publishing login activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse logout(LogoutRequest logoutRequest, HttpServletRequest httpRequest) throws Exception {
        logger.debug("logout service started");

        GenericResponse genericResponse = new GenericResponse();
        AuthenticationConfiguration config = null;
        LogoutBaseResponse logoutBaseResponse = null;
        try {
            Header header1 = new Header();
            header1.setInstitutionId(logoutRequest.getHeader().getInstitutionId());
            header1.setCroId(logoutRequest.getHeader().getCroId());
            header1.setDsaId(logoutRequest.getHeader().getDsaId());
            header1.setApplicationSource(logoutRequest.getHeader().getApplicationSource());
            header1.setLoggedInUserRole(logoutRequest.getHeader().getLoggedInUserRole());
            header1.setLoggedInUserId(logoutRequest.getHeader().getLoggedInUserId());
            header1.setDealerId(logoutRequest.getHeader().getDealerId());

            genericResponse.setStatus(Constant.SUCCESS);

            // Save activity log
            ActivityLogs activityLog = getActivityLog(header1, httpRequest,
                    GNGWorkflowConstant.LOGOUT.toFaceValue());
            activityLog.setInstitutionId(logoutRequest.getInstitutionId());
            activityLog.setUserName(logoutRequest.getUserId());
            activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());

            // Persist activityLog in DB
            logger.debug(String.format("Publishing logout activity from thread %s", Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLog);
            // Set AllocationStatus to null of all applications who has operator as currently logged-in userId
            boolean autoLockstatus = true;
            boolean preFlight = false;
            if (StringUtils.isNotEmpty(logoutRequest.getHeader().getInstitutionId())) {
                config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.LOGOUT_V2.toFaceValue());
                if (config != null) {
                    LogoutRequestV2 logoutRequestV2 = new LogoutRequestV2().builder()
                            .loginId(logoutRequest.getUserId())
                            .oldPassword("-").newPassword("-")
                            .newLoginId("-")
                            .institutionId(Integer.parseInt(logoutRequest.getHeader().getInstitutionId()))
                            .instituteName(logoutRequest.getHeader().getInstituteName())
                            .application("GNG-SME").build();
                    logger.info("logout request ..." + JsonUtil.ObjectToString(logoutRequestV2));
                    logger.info("request url..." + config.getUrl());
                    Map<String, String> header = new HashMap<>();
                    header.put("Authorization", httpRequest.getHeader("Authorization"));
                    String logoutResponse = new HttpTransportationService().
                            postRequest(config.getUrl(), JsonUtil.ObjectToString(logoutRequestV2), header, MediaType.APPLICATION_JSON_VALUE);
                    if (logoutResponse != null) {
                        logoutBaseResponse = JsonUtil.StringToObject(logoutResponse, LogoutBaseResponse.class);
                        if (logoutBaseResponse != null && logoutBaseResponse.getError() == null) {
                            preFlight = true;
                            if(StringUtils.isNotEmpty(httpRequest.getHeader(Constant.SOURCE_ID))){
                                redisService.removeKeysFromCacheBasedPattern(logoutRequest.getUserId().concat(FieldSeparator.ASTERISK));
                            }
                        } else {
                            return GngUtils.getBaseResponse(HttpStatus.NOT_ACCEPTABLE, logoutBaseResponse.getError());
                        }
                    } else {
                        return GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR, GngUtils.getThirdPartyException());
                    }
                } else {
                    logger.info("logout url for uam not found");
                }
            } else {
                preFlight = true;
            }
            if (preFlight) {
                if (autoLockstatus) {
                    applicationRepository.releaseAllocatedApplications(logoutRequest.getUserId(), logoutRequest.getInstitutionId());
                    /**
                     * Added for Los version 2.0
                     */
                    try {
                        applicationRepository.releaseAllocatedApplications(logoutRequest.getUserId(), logoutRequest.getInstitutionId());
                    } catch (Exception ex) {
                    }
                }
                return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
            } else {
                return GngUtils.getBaseResponse(HttpStatus.NOT_FOUND, "Institution Not Found");
            }
        } catch (Exception e) {
            genericResponse.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
        }
    }

    @Override
    public BaseResponse changePassword(
            ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug(" changePassword service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.CHANGE_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomain changePassService = new ChangePasswordServiceDomain();
        changePassService.setUserName(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY.toFaceValue());

        ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) TransportUtils.
                postAuthRequest(changePassService, config.getChangePassUrl(), ChangePasswordResponse.class);

        logger.info("changePassword service completed with response {} ", changePasswordResponse);
        // Persist activityLog in DB
        activityLog.setStatus((GNGWorkflowConstant.SUCCESS.toFaceValue()));
        logger.debug(String.format("Publishing pwd change activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return GngUtils.getBaseResponse(HttpStatus.OK, changePasswordResponse);
    }

    @Override
    public BaseResponse resetPassword(
            ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug("resetPassword service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.RESET_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomain changePassService = new ChangePasswordServiceDomain();
        changePassService.setUserName(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader()
                .getInstitutionId());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY.toFaceValue());

        logger.debug("resetPassword service calling postAuthRequest service for authentication ");

        ChangePasswordResponse changePassResponse = (ChangePasswordResponse) TransportUtils.
                postAuthRequest(changePassService, config.getResetPassUrl(), ChangePasswordResponse.class);

        logger.info("resetPassword service completed with  response {}", changePassResponse);
        // Persist activityLog in DB
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        logger.debug(String.format("Publishing pwd reset activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return GngUtils.getBaseResponse(HttpStatus.OK,changePassResponse);
    }

    @Override
    public BaseResponse loginV2(LoginRequest loginRequest, HttpServletRequest httpRequest) throws Exception {

        logger.debug("loginV2 service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(loginRequest.getHeader(), httpRequest,
                GNGWorkflowConstant.LOGIN.toFaceValue());
        if (StringUtils.isNotBlank(loginRequest.getUserName())) {
            activityLog.setUserName(loginRequest.getUserName());
        }
        activityLog.setRefId(loginRequest.getRefId());
        activityLog.setInstitutionId(loginRequest.getInstId());

        com.softcell.gonogo.model.security.v2.LoginServiceRequest loginServiceRequest = new com.softcell.gonogo.model.security.v2.LoginServiceRequest();

        loginServiceRequest.setLoginId(loginRequest.getUserName());
        loginServiceRequest.setPassword(loginRequest.getPassword());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY_V2.toFaceValue());

        logger.debug("loginv2 service calling postAuthRequest service  ");

        String loginResponseString = new HttpTransportationService().
                postRequest(config.getUrl(), JsonUtil.ObjectToString(loginServiceRequest), MediaType.APPLICATION_JSON_VALUE);

        com.softcell.gonogo.model.security.v2.LoginBaseResponse loginBaseResponse = null;

        BaseResponse baseResponse;

        com.softcell.gonogo.model.security.v2.LoginServiceResponse loginServiceResponse = null;

        if (loginResponseString != null) {
            loginBaseResponse = JsonUtil.StringToObject(loginResponseString,  com.softcell.gonogo.model.security.v2.LoginBaseResponse.class);
            if (loginBaseResponse.getBody()!= null && loginBaseResponse.getBody().getLoginServiceResponse() !=null ) {
                loginServiceResponse = loginBaseResponse.getBody().getLoginServiceResponse();
            }

            if (loginBaseResponse != null && loginBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), loginBaseResponse.getError().getStatus())) {

                logger.debug("login service received error {} from postAuth service", loginBaseResponse.getError().getMessage());

                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .message(loginBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.FORBIDDEN,errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", loginBaseResponse.getError()));

            } else if (null != loginServiceResponse && null != loginRequest.getHeader()
                    && !configurationManager.isValidVersion(loginServiceResponse.getInstitutionId().toString(),
                    loginRequest.getHeader().getApplicationSource())) {

                logger.debug("login service call failed due to unsupported version");

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_VERSION_DSCR)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", ErrorCode.INVALID_VERSION_DSCR));

            } else {
                logger.info("login service completed successfully with response {}",
                        loginServiceResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,loginServiceResponse);
                activityLog.setStatus((GNGWorkflowConstant.SUCCESS.toFaceValue()));
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
            activityLog.setCustomMsg(String.format("Error : %s", HttpStatus.INTERNAL_SERVER_ERROR.name()));
        }

        // Persist activityLog in DB
        logger.debug(String.format("Publishing login activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse changePasswordV2(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug(" changePasswordV2 service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.CHANGE_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomainV2 changePassService = new ChangePasswordServiceDomainV2();
        changePassService.setLoginId(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader()
                .getInstitutionId());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY_V2.toFaceValue());

        Map<String,String> headerParams = new HashMap<>();
        headerParams.put("Authorization", StringUtils.isNotEmpty(httpRequest.getHeader("Authorization")) ? httpRequest.getHeader("Authorization") : "Bearer luam-api-key");
        headerParams.put("institutionID",chngPassRqst.getHeader().getInstitutionId());
        logger.debug(" changePasswordV2 service headers {}", headerParams.toString());

        String changePasswordResponseString = new HttpTransportationService().
                postRequest(config.getChangePassUrl(), JsonUtil.ObjectToString(changePassService), headerParams,  MediaType.APPLICATION_JSON_VALUE);

        BaseResponse baseResponse;

        if (changePasswordResponseString != null) {
            PasswordBaseResponse passwordBaseResponse = JsonUtil
                    .StringToObject(changePasswordResponseString, PasswordBaseResponse.class);
            ChangePasswordResponse changePasswordResponse = null;

            if (passwordBaseResponse.getBody()!= null && passwordBaseResponse.getBody().getChangePasswordResponse() !=null ) {
                changePasswordResponse = passwordBaseResponse.getBody().getChangePasswordResponse();
            }

            if (passwordBaseResponse != null && passwordBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), passwordBaseResponse.getError().getStatus())) {

                logger.debug("change password  service received error {} ", passwordBaseResponse.getError().getMessage());

                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .message(passwordBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", passwordBaseResponse.getError().getMessage()));
            } else {
                logger.info("change password service completed successfully with response {}",
                        changePasswordResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,changePasswordResponse);
                // Persist activityLog in DB
                activityLog.setStatus((GNGWorkflowConstant.SUCCESS.toFaceValue()));

            }

        } else {

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
            activityLog.setCustomMsg(String.format("Error : %s", HttpStatus.INTERNAL_SERVER_ERROR.name()));
        }
        logger.info("changePasswordV2 service completed with response {} ", baseResponse);
        logger.debug(String.format("Publishing pwd change activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse resetPasswordV2(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug("resetPasswordV2 service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.RESET_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomainV2 changePassService = new ChangePasswordServiceDomainV2();
        changePassService.setLoginId(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader()
                .getInstitutionId());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY_V2.toFaceValue());

        logger.debug("resetPasswordV2 service calling postAuthRequest service for authentication ");

        String resetResponseString = new HttpTransportationService().
                postRequest(config.getResetPassUrl(), JsonUtil.ObjectToString(changePassService), MediaType.APPLICATION_JSON_VALUE);


        BaseResponse baseResponse;

        if (resetResponseString != null) {
            PasswordBaseResponse passwordBaseResponse = JsonUtil
                    .StringToObject(resetResponseString, PasswordBaseResponse.class);

            ChangePasswordResponse changePasswordResponse = null;

            if (passwordBaseResponse.getBody()!= null && passwordBaseResponse.getBody().getChangePasswordResponse() !=null ) {
                changePasswordResponse = passwordBaseResponse.getBody().getChangePasswordResponse();
            }

            if (passwordBaseResponse != null && passwordBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), passwordBaseResponse.getError().getStatus())) {

                logger.debug("reset password received error {} from postAuth service", passwordBaseResponse.getError().getMessage());
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(passwordBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(passwordBaseResponse.getError().getMessage());
            } else {
                logger.info("reset password service completed successfully with response {}",
                        changePasswordResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,changePasswordResponse);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.INTERNAL_SERVER_ERROR.name());
        }

        logger.info("resetpasswordV2 service completed with response {} ", baseResponse);
        logger.debug(String.format("Publishing pwd reset activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse loginV3(LoginRequest loginRequest, HttpServletRequest httpRequest)  {
        BaseResponse baseResponse = null;
        try{

        logger.debug("loginV3 service started ");

        // Save activity log
        UrlConfigurationRepository urlConfigurationRepository = new UrlConfigurationRepositoryImpl();

        ActivityLogs activityLog = getActivityLog(loginRequest.getHeader(), httpRequest,
                GNGWorkflowConstant.LOGIN.toFaceValue());

        if (StringUtils.isNotBlank(loginRequest.getUserName())) {
            activityLog.setUserName(loginRequest.getUserName());
        }
        activityLog.setRefId(loginRequest.getRefId());
        activityLog.setInstitutionId(loginRequest.getInstId());

        com.softcell.gonogo.model.security.v2.LoginServiceRequest loginServiceRequest =
                new com.softcell.gonogo.model.security.v2.LoginServiceRequest();

        loginServiceRequest.setLoginId(loginRequest.getUserName());
        loginServiceRequest.setPassword(loginRequest.getPassword());
        loginServiceRequest.setInstituteName(loginRequest.getInstituteName());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration()
                .get(GNGWorkflowConstant.AUTH_KEY_V3.toFaceValue());
        logger.debug("loginv3 service calling postAuthRequest service: url: {}",config.getUrl());
        logger.debug("loginv3 service calling postAuthRequest service request {}",JsonUtil.ObjectToString(loginServiceRequest));

        String loginResponseString = new HttpTransportationService().
                postRequest(config.getUrl(), JsonUtil.ObjectToString(loginServiceRequest), MediaType.APPLICATION_JSON_VALUE);

        com.softcell.gonogo.model.security.v2.LoginBaseResponse loginBaseResponse = null;

        com.softcell.gonogo.model.security.v2.LoginServiceResponse loginServiceResponse = null;

        if (loginResponseString != null) {
            loginBaseResponse = JsonUtil.StringToObject(loginResponseString,  com.softcell.gonogo.model.security.v2.LoginBaseResponse.class);

            if (loginBaseResponse.getBody() != null && loginBaseResponse.getBody().getLoginServiceResponse() != null
                    && loginBaseResponse.getBody().getLoginServiceResponse().getStatusCode() == 409) {
                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .message(loginBaseResponse.getBody().getLoginServiceResponse().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.CONFLICTS.toCode()))
                        .errorType(Error.ERROR_TYPE.CONFLICTS.toValue())
                        .level(Error.SEVERITY.LOW.name())
                        .errors(loginBaseResponse.getError())
                        .build());
                return GngUtils.getBaseResponse(HttpStatus.CONFLICT, errors);
            }

            if (loginBaseResponse.getBody()!= null && loginBaseResponse.getBody().getLoginServiceResponse() !=null ) {
                loginServiceResponse = loginBaseResponse.getBody().getLoginServiceResponse();
            }

            if (loginBaseResponse != null && loginBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), loginBaseResponse.getError().getStatus())) {

                logger.debug("login service received error {} from postAuth service", loginBaseResponse.getError().getMessage());

                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .message(loginBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.FORBIDDEN,errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", loginBaseResponse.getError()));

            } else if (null != loginServiceResponse && null != loginRequest.getHeader()
                    && !configurationManager.isValidVersion(loginServiceResponse.getInstitutionId().toString(),
                    loginRequest.getHeader().getApplicationSource())) {

                logger.debug("login service call failed due to unsupported version {} for institute {}",
                        loginRequest.getHeader().getApplicationSource(), loginServiceResponse.getInstitutionId());

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.INVALID_VERSION_DSCR)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.UPGRADE_REQUIRED, errors);
                activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
                activityLog.setCustomMsg(String.format("Error : %s", ErrorCode.INVALID_VERSION_DSCR));

            } else {
                if(loginServiceResponse != null){
                    if(loginServiceResponse.getRolesInfo()!=null){
                        List<String> actionNames=new ArrayList<String>();
                        for (RoleDetail roleDetail : loginServiceResponse.getRolesInfo()) {
                            roleDetail.getActions().forEach(obj -> {
                                actionNames.add(obj.getActionName());
                            });
                            roleDetail.getActionNames().addAll(actionNames);
                            actionNames.clear();
                        }
                    }
                    ActionConfiguration actionConfiguration = lookupService.getActionConfig(String.valueOf(loginServiceResponse.getInstitutionId()), ActionName.API_ROLE_AUTHORISATION_ENABLE);
                    logger.info("actionConfiguration: {}",actionConfiguration);
                    if(actionConfiguration != null && StringUtils.equalsIgnoreCase(actionConfiguration.getAuthenticationMode(),GNGWorkflowConstant.ON.toFaceValue())){
                        logger.info("login-web-v3: inside redis add: 1");
                        setDataInRedis(loginServiceResponse,actionConfiguration);
                    }
                }

                logger.info("login service completed successfully with response {}",
                        loginServiceResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,loginServiceResponse);
                activityLog.setStatus((GNGWorkflowConstant.SUCCESS.toFaceValue()));
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus((GNGWorkflowConstant.FAILED.toFaceValue()));
            activityLog.setCustomMsg(String.format("Error : %s", HttpStatus.INTERNAL_SERVER_ERROR.name()));
        }

        // Persist activityLog in DB
        logger.debug(String.format("Publishing login activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        }catch (Exception e){
            logger.error("Login Error {}", e.getStackTrace());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse resetPasswordV3(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug("resetPasswordV3 service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.RESET_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomainV2 changePassService = new ChangePasswordServiceDomainV2();
        changePassService.setLoginId(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader()
                .getInstitutionId());
        changePassService.setInstituteName(chngPassRqst.getInstituteName());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration()
                .get(GNGWorkflowConstant.AUTH_KEY_V3.toFaceValue());

        logger.debug("resetPasswordV3 service calling postAuthRequest service for authentication ");

        String resetResponseString = new HttpTransportationService().
                postRequest(config.getResetPassUrl(), JsonUtil.ObjectToString(changePassService), MediaType.APPLICATION_JSON_VALUE);


        BaseResponse baseResponse;

        if (resetResponseString != null) {
            PasswordBaseResponse passwordBaseResponse = JsonUtil
                    .StringToObject(resetResponseString, PasswordBaseResponse.class);

            ChangePasswordResponse changePasswordResponse = null;

            if (passwordBaseResponse.getBody()!= null && passwordBaseResponse.getBody().getChangePasswordResponse() !=null ) {
                changePasswordResponse = passwordBaseResponse.getBody().getChangePasswordResponse();
            }

            if (passwordBaseResponse != null && passwordBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), passwordBaseResponse.getError().getStatus())) {

                logger.debug("reset password received error {} from postAuth service", passwordBaseResponse.getError().getMessage());
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(passwordBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(passwordBaseResponse.getError().getMessage());
            } else {
                logger.info("reset password service completed successfully with response {}",
                        changePasswordResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,changePasswordResponse);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.INTERNAL_SERVER_ERROR.name());
        }

        logger.info("resetpasswordV2 service completed with response {} ", baseResponse);
        logger.debug(String.format("Publishing pwd reset activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    private ActivityLogs getActivityLog(Header header, HttpServletRequest httpRequest, String action) {
        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, header);
        activityLog.setAction(action);
        return activityLog;
    }

    @Override
    public com.softcell.gonogo.model.security.v2.LoginBaseResponse getLoginDetails(LoginRequest loginRequest) throws IOException {

        com.softcell.gonogo.model.security.v2.LoginServiceRequest loginServiceRequest =
                new com.softcell.gonogo.model.security.v2.LoginServiceRequest();

        loginServiceRequest.setLoginId(loginRequest.getUserName());
        loginServiceRequest.setPassword(loginRequest.getPassword());
        loginServiceRequest.setInstituteName(loginRequest.getInstituteName());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration()
                .get(GNGWorkflowConstant.AUTH_KEY_V3.toFaceValue());

        String loginResponseString = new HttpTransportationService().
                postRequest(config.getUrl(), JsonUtil.ObjectToString(loginServiceRequest), MediaType.APPLICATION_JSON_VALUE);

        com.softcell.gonogo.model.security.v2.LoginBaseResponse loginBaseResponse = null;


        if (loginResponseString != null) {
            loginBaseResponse = JsonUtil.StringToObject(loginResponseString, com.softcell.gonogo.model.security.v2.LoginBaseResponse.class);
        }
        return loginBaseResponse;
    }

    @Override
    public BaseResponse changeUserPassword(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug("changeUserPassword service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.CHANGE_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());
        activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        ChangePasswordServiceDomainV2 changePassService = new ChangePasswordServiceDomainV2();
        changePassService.setLoginId(chngPassRqst.getUserName());
        changePassService.setOldPassword(chngPassRqst.getOldpassword());
        changePassService.setNewPassword(chngPassRqst.getNewPassword());
        changePassService.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.AUTH_KEY.toFaceValue());

        logger.debug("changeUserPassword service calling postAuthRequest service for authentication ");
        logger.info("reset pwd url {}, request - {}",config.getChangeUserPassUrl(), JsonUtil.ObjectToString(changePassService));

        String resetResponseString = new HttpTransportationService().
                postRequest(config.getChangeUserPassUrl(), JsonUtil.ObjectToString(changePassService), MediaType.APPLICATION_JSON_VALUE);

        logger.info("reset pwd response - {}",resetResponseString);
        BaseResponse baseResponse;

        if (resetResponseString != null) {
            PasswordBaseResponse passwordBaseResponse = JsonUtil
                    .StringToObject(resetResponseString, PasswordBaseResponse.class);
            logger.info("reset pwd response object {}", passwordBaseResponse.toString());
            ChangePasswordResponse changePasswordResponse = null;

            if (passwordBaseResponse.getBody()!= null && passwordBaseResponse.getBody().getChangePasswordResponse() !=null ) {
                changePasswordResponse = passwordBaseResponse.getBody().getChangePasswordResponse();
            }

            if (passwordBaseResponse != null && passwordBaseResponse.getError() != null
                    && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), passwordBaseResponse.getError().getStatus())) {

                logger.debug("change user password received error {} from postAuth service", passwordBaseResponse.getError().getMessage());
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(passwordBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(passwordBaseResponse.getError().getMessage());
            } else {
                logger.info("change user password service completed successfully with response {}",
                        changePasswordResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,changePasswordResponse);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.INTERNAL_SERVER_ERROR.name());
        }

        logger.info("changeUserPassword service completed with response {} ", baseResponse);
        logger.debug(String.format("Publishing pwd change activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse forgotPassword(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception {

        logger.debug("forgotPassword service started ");

        // Save activity log
        ActivityLogs activityLog = getActivityLog(chngPassRqst.getHeader(), httpRequest,
                GNGWorkflowConstant.RESET_PASSWORD.toFaceValue());
        activityLog.setUserName(chngPassRqst.getUserName());

        if (StringUtils.isNotBlank(chngPassRqst.getHeader().getInstitutionId())) {
            activityLog.setInstitutionId(chngPassRqst.getHeader().getInstitutionId());
        }

        ChangePasswordServiceDomainV2 changePassService = new ChangePasswordServiceDomainV2();
        changePassService.setLoginId(chngPassRqst.getUserName());
        changePassService.setInstituteName(chngPassRqst.getInstituteName());
        changePassService.setInstitutionId("0");

        AuthenticationConfiguration config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration()
                .get(GNGWorkflowConstant.AUTH_KEY_V3.toFaceValue());

        logger.debug("forgotPassword service calling postAuthRequest service for authentication ");

        String resetResponseString = new HttpTransportationService().
                postRequest(config.getResetPassUam(), JsonUtil.ObjectToString(changePassService), MediaType.APPLICATION_JSON_VALUE);


        BaseResponse baseResponse;

        if (resetResponseString != null) {
            PasswordBaseResponse passwordBaseResponse = JsonUtil
                    .StringToObject(resetResponseString, PasswordBaseResponse.class);

            ChangePasswordResponse changePasswordResponse = null;

            if (passwordBaseResponse.getBody()!= null && passwordBaseResponse.getBody().getChangePasswordResponse() !=null ) {
                changePasswordResponse = passwordBaseResponse.getBody().getChangePasswordResponse();
            }

            if (passwordBaseResponse.getError() != null && StringUtils.equalsIgnoreCase(HttpStatus.OK.name(), passwordBaseResponse.getError().getStatus())) {

                logger.debug("forgotPassword received error {} from postAuth service", passwordBaseResponse.getError().getMessage());
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(passwordBaseResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(passwordBaseResponse.getError().getMessage());
            } else {
                logger.info("forgotPassword service completed successfully with response {}",
                        changePasswordResponse);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,changePasswordResponse);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            }
        } else {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.INTERNAL_SERVER_ERROR.name());
        }

        logger.info("forgotPassword service completed with response {} ", baseResponse);
        logger.debug(String.format("Publishing forgotPassword activity from thread %s", Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    public void setDataInRedis(com.softcell.gonogo.model.security.v2.LoginServiceResponse loginServiceResponse, ActionConfiguration actionConfiguration){
        long exp = actionConfiguration.getExpiry();
        setRolesinRedis(loginServiceResponse.getRoles(),loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), exp);
        setHierarchyinRedis(loginServiceResponse.getHierarchy(),loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), exp);
        setBranchesinRedis(loginServiceResponse.getBranches(),loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), exp);
        setDealersinRedis(loginServiceResponse.getDealers(),loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), exp);
        setProductsinRedis(loginServiceResponse.getProduct(),loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), exp);
        setUserDetails(loginServiceResponse.getLoginId()
                ,loginServiceResponse.getInstitutionId().toString(), loginServiceResponse,exp);
    }

    public boolean setRolesinRedis(Set<String> roles,String userId,String institutionId,long exp){
        try{
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.ROLES
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,roles, exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting roles data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean setHierarchyinRedis(com.softcell.gonogo.model.security.v2.Hierarchy hierarchy, String userId, String institutionId, long exp){
        try{
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.HIERARCHY
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,hierarchy, exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting hierarchy data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean setBranchesinRedis(Set<com.softcell.gonogo.model.security.v2.Branch> branches, String userId, String institutionId, long exp){
        try{
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.BRANCHES
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,branches, exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting branches data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean setDealersinRedis(Set<com.softcell.gonogo.model.security.v2.Dealer> dealers, String userId, String institutionId, long exp){
        try{
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.DEALERS
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,dealers, exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting dealers data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean setProductsinRedis(Set<Product> products, String userId, String institutionId, long exp){
        try{
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.PRODUCTS
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,products, exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting dealers data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean expireDataOnLogout(String userId){
        try{
            redisService.removeKeysFromCacheBasedPattern(userId);
        }catch(Exception e){
            logger.error("Exception occurred while removing data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    public boolean setUserDetails(String userId, String institutionId, com.softcell.gonogo.model.security.v2.LoginServiceResponse loginServiceResponse, long exp){
        try{
            com.softcell.gonogo.model.security.v2.LoginServiceResponse impDetails = com.softcell.gonogo.model.security.v2.LoginServiceResponse.builder()
                    .mobile(loginServiceResponse.getMobile())
                    .creditAmount(loginServiceResponse.getCreditAmount())
                    .loginId(loginServiceResponse.getLoginId())
                    .sapCode(loginServiceResponse.getSapCode())
                    .instituteName(loginServiceResponse.getInstituteName())
                    .email(loginServiceResponse.getEmail())
                    .authorizationToken(loginServiceResponse.getAuthorizationToken())
                    .roles(loginServiceResponse.getRoles())
                    .build();
            String redisKey = StringUtils.join(userId, FieldSeparator.UNDER_SCORE_STR,institutionId,FieldSeparator.UNDER_SCORE_STR,Constant.USER_DETAILS
                    ,FieldSeparator.UNDER_SCORE_STR,Constant.SME);
            redisService.addToSetCache(redisKey,impDetails,exp);
        }catch(Exception e){
            logger.error("Exception occurred while setting user details data in redis with probable cause: " +e);
            return false;
        }
        return true;
    }

    @Override
    public LogoutBaseResponse logoutUserWithoutAuthToken(LogoutRequest logoutRequest) throws Exception {
        logger.info("logoutUserWithoutAuthToken method started");

        LogoutBaseResponse logoutBaseResponse = null;
        AuthenticationConfiguration config = null;
        try{
            config = Cache.URL_CONFIGURATION.getAuthenticationConfiguration().get(GNGWorkflowConstant.LOGOUT_V2.toFaceValue());
            if (config != null) {
                LogoutRequestV2 logoutRequestV2 = new LogoutRequestV2().builder()
                        .loginId(logoutRequest.getUserId())
                        .oldPassword("-").newPassword("-").newLoginId("-")
                        .institutionId(Integer.parseInt(logoutRequest.getInstitutionId()))
                        .instituteName("SBFC").application("GNG-SME").build();

                logger.info("logoutUserWithoutAuthToken: logout request: {}",JsonUtil.ObjectToString(logoutRequestV2));
                logger.info("logoutUserWithoutAuthToken: request url: {}",config.getUrl());

                String logoutResponseString = new HttpTransportationService().
                        postRequest(config.getUrl(), JsonUtil.ObjectToString(logoutRequestV2), MediaType.APPLICATION_JSON_VALUE);
                if (logoutResponseString != null) {
                    logoutBaseResponse = JsonUtil.StringToObject(logoutResponseString, LogoutBaseResponse.class);
                }
            }else{
                logger.info("logoutUserWithoutAuthToken: logout url for uam not found");
            }
        }catch (Exception e){
            logger.error("Error while logoutUserWithoutAuthToken: {}",e.getMessage());
            e.printStackTrace();
        }
        return logoutBaseResponse;
    }
}
