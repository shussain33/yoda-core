package com.softcell.service.impl;

import com.softcell.constants.AuditType;
import com.softcell.constants.CacheConstant;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.AuditRepository;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.response.ApplicationResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.factory.GngResponseEntityBuilder;
import com.softcell.service.AuditDataManager;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.ReInitiateApplicationRequest;
import com.softcell.workflow.WorkFlowManager;
import com.softcell.workflow.WorkFlowRestartConfiguration;
import com.softcell.workflow.component.finished.AuditData;
import com.softcell.workflow.utils.ReAppraiseReInitiateUtils;
import com.softcell.workflow.utils.Utility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author vinodk
 *         <p/>
 *         Implements AuditDataManager for fetching required data from AuditData.
 */
@Service
public class AuditDataManagerImpl implements AuditDataManager {

    private static final Logger logger = LoggerFactory.getLogger(AuditDataManagerImpl.class);

    private static final String collectionName = MongoRepositoryUtil.getAuditCollectionName(GoNoGoCroApplicationResponse.class);

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private WorkFlowManager workFlowManager;


    @Override
    public List<ApplicationRequest> getAppRequestFromAuditData(String refID,
                                                               String institutionId, AuditType auditType) throws Exception {

        logger.debug("getAppRequestFromAuditData service started");

        //Get all the applications from the DB.
        List<AuditData> reAppraisedApplications = auditRepository
                .getAuditData(refID, institutionId, auditType,
                        collectionName);

        List<ApplicationRequest> applicationRequests = null;

        //If no applications are found in the DB then return null array.
        if (reAppraisedApplications != null
                && !reAppraisedApplications.isEmpty()) {

            logger.debug("getAppRequestFromAuditData service reappraiseApplication count {}", reAppraisedApplications.size());

            applicationRequests = new ArrayList<>(
                    reAppraisedApplications.size());

            //Add all the application requests in the list.
            for (int i = 0; i < reAppraisedApplications.size(); i++) {

                if (reAppraisedApplications.get(i) != null) {

                    GoNoGoCustomerApplication gngCustApp = (GoNoGoCustomerApplication) reAppraisedApplications
                            .get(i).getAuditData();

                    applicationRequests.add(gngCustApp.getApplicationRequest());
                }
            }
        }

        logger.debug("getAppRequestFromAuditData service completed");

        return applicationRequests;
    }

    private List<ApplicationResponse> getAuditedApplicationStatus(
            CheckApplicationStatus checkApplicationStatus, AuditType auditType) throws Exception {
        //AuditData list which contains only ApplicationResponse related fields.
        List<AuditData> auditDataList = auditRepository.getAuditData(
                checkApplicationStatus.getGonogoRefId(), checkApplicationStatus
                        .getHeader().getInstitutionId(), auditType,
                collectionName);

        List<ApplicationResponse> auditedAppResponseList = null;

        if (auditDataList != null && auditDataList.size() > 0) {
            auditedAppResponseList = new ArrayList<>(
                    auditDataList.size());

            for (AuditData auditData : auditDataList) {

                GoNoGoCustomerApplication goNoGoCustomerApplication = null;

                try {
                    goNoGoCustomerApplication = (GoNoGoCustomerApplication) auditData
                            .getAuditData();
                } catch (ClassCastException e) {
                    logger.error("Class cast exception from AuditData to GoNoGoCustomerApplication" + e.getMessage());
                    continue;
                }

                if (goNoGoCustomerApplication != null) {
                    ApplicationResponse auditedAppResponse = new ApplicationResponse();

                    auditedAppResponse
                            .setInterimStatus(goNoGoCustomerApplication
                                    .getIntrimStatus());
                    auditedAppResponse
                            .setApplicationStatus(goNoGoCustomerApplication
                                    .getApplicationStatus());
                    auditedAppResponse
                            .setModuleOutcomes(goNoGoCustomerApplication
                                    .getApplScoreVector());
                    auditedAppResponse
                            .setCroJustification(goNoGoCustomerApplication
                                    .getCroJustification());

                    /**
                     * Field added for ReAppraisal Comparison
                     */
                    auditedAppResponse.setSurrogate(goNoGoCustomerApplication
                            .getApplicationRequest().getRequest().getApplicant()
                            .getSurrogate());
                    auditedAppResponse.setSubmitDate(goNoGoCustomerApplication
                            .getApplicationRequest().getHeader().getDateTime());
                    /**
                     * Field added for ReAppraisal Comparison
                     */
                    auditedAppResponse.setAppliedAmount(goNoGoCustomerApplication
                            .getApplicationRequest().getRequest().getApplication()
                            .getLoanAmount());

                    if (goNoGoCustomerApplication.getApplicationRequest()
                            .getReAppraiseDetails() != null) {
                        auditedAppResponse
                                .setReAppraiseReason(goNoGoCustomerApplication
                                        .getApplicationRequest()
                                        .getReAppraiseDetails()
                                        .getReappraiseReason());

                        auditedAppResponse
                                .setReAppraiseRemark(goNoGoCustomerApplication
                                        .getApplicationRequest()
                                        .getReAppraiseDetails()
                                        .getReappraiseRemark());
                    }

                    if (goNoGoCustomerApplication.getCroDecisions() != null) {
                        auditedAppResponse
                                .setCroDecisions(goNoGoCustomerApplication
                                        .getCroDecisions());
                    }
                    auditedAppResponseList.add(auditedAppResponse);
                }
            }
        }

        return auditedAppResponseList;
    }


    @Override
    public BaseResponse getReappraisalApplications(
            AuditDataRequest auditDataRequest) throws Exception {
        List<GoNoGoCustomerApplication> reAppraisalAppList = null;
        BaseResponse baseResponse = null;
        if (StringUtils.isNotBlank(auditDataRequest.getRefId())) {

            List<AuditData> auditDataList = auditRepository.getAuditData(
                    auditDataRequest.getRefId(), auditDataRequest.getHeader()
                            .getInstitutionId(),
                    AuditType.RE_APPRAISE_RE_INITIATE, collectionName);
            if (auditDataList != null && auditDataList.size() > 0) {

                reAppraisalAppList = new ArrayList<>();

                for (AuditData auditData : auditDataList) {

                    GoNoGoCustomerApplication reAppraiseApplication = null;
                    try {
                        reAppraiseApplication = (GoNoGoCustomerApplication) auditData
                                .getAuditData();
                    } catch (ClassCastException e) {
                        logger.error("Class cast exception from AuditData to GoNoGoCustomerApplication" + e.getMessage());
                        continue;
                    }

                    if (reAppraiseApplication != null)
                        reAppraisalAppList.add(reAppraiseApplication);

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, reAppraisalAppList);
                }
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        }
        return baseResponse;
    }


    @Override
    public BaseResponse getApplicationData(WorkFlowRestartConfiguration workFlowRestartConfiguration) throws Exception {
        GngResponseEntityBuilder gngResponseEntityBuilder = new GngResponseEntityBuilder();

        GoNoGoCroApplicationResponse goNoGoCustomerApplication = applicationRepository
                .getApplicationByRefIdForAuditLog(workFlowRestartConfiguration.getGngRefId(), CacheConstant.WEB.toString(),
                        workFlowRestartConfiguration.getHeader().getInstitutionId());

        if (goNoGoCustomerApplication != null) {

            if (goNoGoCustomerApplication.getReInitiateCount() >= ReAppraiseReInitiateUtils.getNumberOfReTry(goNoGoCustomerApplication)) {
                /**
                 * if number of retry is capped then return invalid request.
                 */
                return GngUtils.getBaseResponse(HttpStatus.OK, gngResponseEntityBuilder.getInvalidRequestResponse());
            }

            workFlowManager.reStartWorkFlow(goNoGoCustomerApplication, workFlowRestartConfiguration);

            return GngUtils.getBaseResponse(HttpStatus.OK, gngResponseEntityBuilder
                    .getSuccesAcknowledgement(goNoGoCustomerApplication.
                            getApplicationRequest()));
        } else {

            return GngUtils.getBaseResponse(HttpStatus.OK, gngResponseEntityBuilder.getInvalidRequestResponse());

        }

    }

    @Override
    public BaseResponse getBreAuditData(AuditDataRequest auditDataRequest) throws Exception{

        List<AuditData> auditData = auditRepository.getAuditData(auditDataRequest.getRefId(),
                auditDataRequest.getHeader().getInstitutionId(), AuditType.RE_INITIATE, collectionName);

        if (auditData != null) {

            return GngUtils.getBaseResponse(HttpStatus.OK, auditData);

        } else {

            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    @Override
    public ApplicationResponse checkStatus(String refId, String institutionId) throws Exception {
        return applicationRepository
                .checkStatus(refId, institutionId);
    }

    @Override
    public BaseResponse reInitiateApplication(ReInitiateApplicationRequest reInitiateApplicationRequest) throws Exception {
        /**
         * Get restart work-flow app(Which modules should be run for
         * re-processing or re-initiating)
         */

        /**
         * Re-Initiate the application.
         */
        return GngUtils.getBaseResponse(HttpStatus.OK,
                workFlowManager.reInitiateApplication(reInitiateApplicationRequest));
    }

    @Override
    public BaseResponse doReAppraiseApplication(ApplicationRequest applicationRequest) throws Exception {
        // TODO: reset reappraise count to 0 if it fails.

        return GngUtils.getBaseResponse(HttpStatus.OK, workFlowManager.reappraiseApplication(applicationRequest));
    }

    @Override
    public BaseResponse getReAppraiseStatus(CheckApplicationStatus checkApplicationStatus) throws Exception {

        List<ApplicationResponse> auditedAppResponseList = null;
        /**
         * Get the current application status response.
         */
        ApplicationResponse applicationResponse = applicationRepository
                .checkStatus(checkApplicationStatus.getGonogoRefId(),
                        checkApplicationStatus.getHeader()
                                .getInstitutionId());
        applicationResponse.setGonogoRefId(checkApplicationStatus
                .getGonogoRefId());

        /**
         * Get all previous reappraised applications status response from audit Data.
         */
        auditedAppResponseList = getAuditedApplicationStatus(checkApplicationStatus,
                AuditType.RE_APPRAISE_RE_INITIATE);
        if (auditedAppResponseList != null
                && auditedAppResponseList.size() >= 0) {
            auditedAppResponseList.add(applicationResponse);
        } else if (applicationResponse != null) {
            auditedAppResponseList = Collections
                    .singletonList(applicationResponse);
        }

        if (auditedAppResponseList != null)
            Collections.sort(auditedAppResponseList);

        return GngUtils.getBaseResponse(HttpStatus.OK, auditedAppResponseList);
    }

    @Override
    public BaseResponse getQuickDataEntryStatus(AuditDataRequest auditDataRequest)
         throws Exception {

        /**
         * Get qde data from audit Data.
         */
        ApplicationRequest applicationRequest = applicationRepository.getApplicationRequestByRefId(
                auditDataRequest.getRefId(), auditDataRequest.getHeader().getInstitutionId());
        String applicantType = applicationRequest.getApplicantType();
        AuditType auditType = Utility.getAuditType(applicantType);
        List<AuditData> auditData = auditRepository.getAuditData(auditDataRequest.getRefId(),
                auditDataRequest.getHeader().getInstitutionId(), auditType, collectionName);

        if (auditData != null) {
            return GngUtils.getBaseResponse(HttpStatus.OK, auditData);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    private List<ApplicationResponse> getAuditedApplicationStatus(CheckApplicationStatus checkApplicationStatus)
            throws Exception {
        // get applicant type to know audit type

        ApplicationRequest applicationRequest = applicationRepository.getApplicationRequestByRefId(
                                                        checkApplicationStatus.getGonogoRefId(),
                                                        checkApplicationStatus.getHeader().getInstitutionId());
        String applicantType = applicationRequest.getApplicantType();
        return getAuditedApplicationStatus(checkApplicationStatus , Utility.getAuditType(applicantType));
    }
}
