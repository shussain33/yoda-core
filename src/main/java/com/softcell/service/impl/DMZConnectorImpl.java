package com.softcell.service.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.softcell.config.LosConfiguration;
import com.softcell.config.RelianceConfiguration;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.*;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.los.*;
import com.softcell.gonogo.model.los.LosLogging.LosException;
import com.softcell.gonogo.model.los.tvr.TVRLogs;
import com.softcell.gonogo.model.los.tvr.TvrRequestDTO;
import com.softcell.gonogo.model.los.tvr.TvrResponse;
import com.softcell.gonogo.model.reliance.RelianceDigitalLogging;
import com.softcell.gonogo.model.reliance.RelianceDigitalLogging.RelianceDigitalException;
import com.softcell.gonogo.model.reliance.RelianceDigitalRequest;
import com.softcell.gonogo.model.reliance.RelianceDigitalResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.LOSDetailsRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.dmz.DmzServiceCheckRequest;
import com.softcell.gonogo.model.response.los.MbPushResponse;
import com.softcell.gonogo.model.tkil.TKILLogging;
import com.softcell.gonogo.model.tkil.TKILRequest;
import com.softcell.gonogo.model.tkil.TKILResponse;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import com.softcell.gonogo.service.factory.*;
import com.softcell.gonogo.service.factory.impl.LosGnGRequestBuilderImplV2;
import com.softcell.gonogo.service.factory.impl.LosMBRequestBuilderImpl;
import com.softcell.gonogo.service.factory.impl.RelianceDigitalRequestBuilderImpl;
import com.softcell.gonogo.service.factory.impl.TvrRequestBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.CroManager;
import com.softcell.service.DMSFeedManager;
import com.softcell.service.DMZConnector;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.SerialNumberValidationEngine;
import com.softcell.soap.mb.nucleus.ws.los.types.LosCustomerResponse;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class is use to connect dmz server to access
 * Reliance Digital service
 *
 * @author bhuvneshk
 */
@Service
public class DMZConnectorImpl implements DMZConnector {


    private static final Logger logger = LoggerFactory.getLogger(DMZConnectorImpl.class);

    @Autowired
    private DMZRepository dmzRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private CroManager croManager;

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Autowired
    private LosRequestBuilder losRequestBuilder;

    @Autowired
    private TvrRequestBuilder tvrRequestBuilder;

    @Autowired
    private TkilRequestBuilder tkilRequestBuilder;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private DMSFeedManager dmsFeedManager;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private SerialNumberValidationEngine serialNumberValidationEngine;

    @Autowired
    private LosRepository losRepository;

    public DMZConnectorImpl() {
        if (null == workFlowCommunicationManager) {
            workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();
        }
        if (null == losRequestBuilder) {
            losRequestBuilder = new LosGnGRequestBuilderImplV2();
        }
        if (null == dmzRepository) {
            dmzRepository = new DMZMongoRepository();
        }
        if (null == applicationRepository) {
            applicationRepository = new ApplicationMongoRepository();
        }
        if(null == tvrRequestBuilder){
            tvrRequestBuilder = new TvrRequestBuilderImpl();
        }
    }

    /**
     * This method calls the DMZ service for reliance
     *
     * @return
     */
    @Override
    public BaseResponse connectRelianceDigitalService(DmzRequest dmzRequest) throws Exception {

        DMZResponse dmzResponse = new DMZResponse();
        logger.debug("DMZS initiated reliance service for ref id {}", dmzRequest.getGonogoRefId());

        if (dmzRepository.checkRelianceStatus(dmzRequest.getGonogoRefId())) {

            dmzResponse.setErrorCode(Status.SUCCESS.name());
            dmzResponse.setErrorMsg("Record is punched successfully");
            return GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
        }

        RelianceDigitalRequestBuilder builder = new RelianceDigitalRequestBuilderImpl();
        PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(dmzRequest.getGonogoRefId(), dmzRequest.getHeader().getInstitutionId());

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(dmzRequest.getGonogoRefId(), dmzRequest.getHeader().getInstitutionId());
        ApplicationRequest appRequest = goNoGoCustomerApplication != null ? goNoGoCustomerApplication.getApplicationRequest() : null;

        if (postIpaRequest == null || appRequest == null) {

            dmzResponse.setErrorCode(Status.ERROR.name());
            dmzResponse.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);
            return GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
        }

        //Cache.checkActionsAccess(ActionName.ALL_RELIANCE_DEALER, institutionId, dmzRequest.getHeader().getProduct().toProductId());

        if (lookupService.checkActionsAccess(dmzRequest.getHeader().getInstitutionId(), dmzRequest.getHeader().getProduct().toProductId(), ActionName.ALL_RELIANCE_DEALER)) {

            if (postIpaRequest != null && postIpaRequest.getPostIPA() != null
                    && postIpaRequest.getPostIPA().getAssetDetails() != null
                    && !postIpaRequest.getPostIPA().getAssetDetails().isEmpty()
                    && StringUtils.isNotBlank(postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName())) {

                String dealerName = postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName();

                if (!dmzRepository.checkRelianceDealerAccess(dealerName)) {

                    dmzResponse.setErrorCode(Status.SUCCESS.name());
                    dmzResponse.setErrorMsg(dealerName + " doesn't have access");

                    return GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
                }
            }
        }

        RelianceDigitalRequest relianceRequest = builder.buildRelianceDigitalRequest(postIpaRequest, appRequest);
        RelianceDigitalLogging relianceLog = new RelianceDigitalLogging();
        RelianceDigitalLogging.RelianceDigitalException relianceException = relianceLog.new RelianceDigitalException();
        relianceLog.setRequest(relianceRequest);

        if (relianceRequest != null) {

            RelianceConfiguration config = Cache.URL_CONFIGURATION.getRelianceConfiguration().get(dmzRequest.getHeader().getInstitutionId());

            if (null == config) {
                logger.error("Configuration not setup for institution {} ", dmzRequest.getHeader().getInstitutionId());
                throw new Exception("Configuration not setup for institution " + dmzRequest.getHeader().getInstitutionId());
            }
            try {

                String url = config.getUrl();
                String inputJson = JsonUtil.ObjectToString(relianceRequest);
                String response = httpTransportationService.postRequest(url, inputJson, MediaType.APPLICATION_JSON);
                relianceLog.setRawResponse(response);
                dmzResponse.setErrorMsg(response);

                logger.debug("DMZS received response {} reliance service ", response);

                if (StringUtils.isNotBlank(response)) {
                    RelianceDigitalResponse relianceResponse = JsonUtil.StringToObject(response, RelianceDigitalResponse.class);
                    if (relianceResponse != null) {
                        if (StringUtils.endsWithIgnoreCase(relianceResponse.getErrorCode(), relianceErrorCode.SUCCESS.getFaceValue())) {
                            dmzResponse.setErrorCode(Status.SUCCESS.name());
                        } else {
                            dmzResponse.setErrorCode(Status.ERROR.name());
                        }
                        dmzResponse.setErrorMsg(relianceResponse.getErrorMsg());
                    }
                    relianceLog.setResponse(dmzResponse);
                }
            } catch (UnsupportedEncodingException | ClientProtocolException | IllegalStateException
                    | JsonGenerationException | JsonParseException | JsonMappingException e) {
                populateRelianceException(relianceException, e);

            } catch (IOException e) {
                populateRelianceException(relianceException, e);
            }
            relianceLog.setException(relianceException);
            dmzRepository.saveRelianceDigitalObj(relianceLog);
        }
        if (relianceLog.getResponse() != null && StringUtils.equalsIgnoreCase(relianceLog.getResponse().getErrorCode(), Status.SUCCESS.name())) {
            return GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
        }
        logger.debug("DMZS reliance service completed ");
        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

    }


    @Override
    public BaseResponse connectDealer(DmzRequest dmzRequest) throws Exception {

        BaseResponse baseResponse=null;

         if (dmzRequest.getActionName().equals(ActionName.RELIANCE_INTERFACE)){
            baseResponse = connectRelianceDigitalService(dmzRequest);
        }else if(dmzRequest.getActionName().equals(ActionName.TKIL_INTERFACE)){
             baseResponse = pushTkil(dmzRequest);
        }else {
             Collection<Error> errors = new ArrayList<>();
             errors.add(Error.builder()
                     .message(ErrorCode.INVALID_INTERFACE)
                     .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                     .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                     .level(Error.SEVERITY.LOW.name())
                     .build());
             baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

         }
        return baseResponse;
    }

    @Override
    public BaseResponse postGNGDataToLos(DmzRequest dmzRequest) throws Exception {

        logger.debug("DMZS initiated los service for ref id {}", dmzRequest.getGonogoRefId());

        DMZResponse losResponse = new DMZResponse();

        if (dmzRepository.checkLosStatus(dmzRequest.getGonogoRefId())) {

            logger.debug("DMZS los service already hit successfully ");

            losResponse.setErrorCode(Status.SUCCESS.name());
            losResponse.setErrorMsg("Record is punched successfully");

            return GngUtils.getBaseResponse(HttpStatus.OK, losResponse);
        }

        LosLogging losLog = new LosLogging();
        LosLogging.LosException losException = losLog.new LosException();
        LOSDetailsRequest losDetailsRequest = new LOSDetailsRequest();

        String refId = dmzRequest.getGonogoRefId();
        String institutionId = dmzRequest.getHeader().getInstitutionId();

        PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(refId, institutionId);
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, institutionId);
        ApplicationRequest applicationRequest = goNoGoCustomerApplication != null ? goNoGoCustomerApplication.getApplicationRequest() : null;
        if (postIpaRequest == null || (applicationRequest == null ||
                !(StringUtils.equalsIgnoreCase(applicationRequest.getCurrentStageId(),GNGWorkflowConstant.INV_GNR.toFaceValue())
                        || (StringUtils.equalsIgnoreCase(applicationRequest.getCurrentStageId(),
                                                  GNGWorkflowConstant.LOS_ERROR.toFaceValue())
           )))) {

            losResponse.setErrorCode(Status.ERROR.name());
            losResponse.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);
            return GngUtils.getBaseResponse(HttpStatus.OK, losResponse);

        }

        try {

            long mbPushSequenceNumber = losRepository.getMbPushSequenceNumber(institutionId);
            LosMbData losMbData = GngUtils.getLosMbData(applicationRequest, mbPushSequenceNumber);

            String losRequestXml = losRequestBuilder.buildLosCustomerRequest(goNoGoCustomerApplication, postIpaRequest,losMbData);

            if (losRequestXml != null && losRequestXml.contains("<losCustomerRequest>")) {

                losRequestXml = losRequestXml.replace("<losCustomerRequest>", "<losCustomerRequest xmlns=\"http://ws.nucleus.com/los/types\">");
            }


            losLog.setRequest(losRequestXml);
            losLog.setInstitutionId(institutionId);
            losLog.setInsertDate(new Date());
            losLog.setRefId(refId);

            LosFeedRequest losFeedRequest = new LosFeedRequest();
            losFeedRequest.setRequest(losRequestXml);
            losFeedRequest.setInstitutionId(institutionId);

            if (losRequestXml != null) {

                LosConfiguration losConfiguration = Cache.URL_CONFIGURATION.getLosConfiguration().get(institutionId);
                logger.info("LOs COnfig" +losConfiguration);
                LosFeedResponse losFeedResponse = (LosFeedResponse) TransportUtils.postJsonRequest(losFeedRequest, losConfiguration.getGngPostUrl(), LosFeedResponse.class);

                logger.debug("DMZS received response {} los service ", losFeedResponse);

                if (losFeedResponse != null) {

                    String responseXml = losFeedResponse.getResponseXml();

                    losLog.setResponse(losFeedResponse.getResponseXml());

                    losLog.setRawResponse(responseXml);

                    if (responseXml != null && StringUtils.contains(responseXml, "xmlns")) {

                        JAXBContext jaxbContext = JAXBContext.newInstance(LosCustomerResponse.class);

                        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

                        StringReader reader = new StringReader(responseXml);

                        LosCustomerResponse losCustomerResponse = null;


                        try {

                             losCustomerResponse = (LosCustomerResponse) unmarshaller.unmarshal(reader);


                            losLog.setLosCustomerResponse(losCustomerResponse);

                        }catch (JAXBException e){

                            logger.error("{}",e.getStackTrace());

                            losException.setDevMessage(e.getMessage());
                            losLog.setException(losException);

                        }

                        if (losCustomerResponse != null && StringUtils.isNotBlank(losCustomerResponse.getApplicationId())) {

                            if (postIpaRequest != null && postIpaRequest.getPostIPA() != null) {

                                losLog.setLosStage(GNGWorkflowConstant.LOS_BDE.toFaceValue());

                            } else {

                                losLog.setLosStage(GNGWorkflowConstant.LOS_CAN.toFaceValue());

                            }
                            if (losMbData != null) {
                                losMbData.setLosId(losCustomerResponse.getApplicationId());
                                losRepository.saveLosMbData(losMbData);
                            }
                            if (lookupService.checkActionsAccess(dmzRequest.getHeader().getInstitutionId(),
                                    dmzRequest.getHeader().getProduct().toProductId(), ActionName.LOS_MB_DATA_PUSH)) {
                                postMBDataToLos(dmzRequest);
                            }

                        } else {

                            losLog.setLosStage(GNGWorkflowConstant.LOS_ERROR.toFaceValue());

                            losException.setCause(losCustomerResponse.getErrorDesc());

                            losLog.setException(losException);
                        }
                    } else {

                        losException.setCause(responseXml);

                        losLog.setException(losException);

                        losLog.setLosStage(GNGWorkflowConstant.LOS_ERROR.toFaceValue());
                    }

                    losDetailsRequest.setRefID(refId);

                    losDetailsRequest.setHeader(dmzRequest.getHeader());

                    LOSDetails losDetails = new LOSDetails();

                    if (losLog.getLosCustomerResponse() != null) {

                        losDetails.setLosID(losLog.getLosCustomerResponse().getApplicationId());

                    }

                    losDetails.setStatus(losLog.getLosStage());

                    losDetails.setError(losException.getCause());

                    losDetailsRequest.setlOSDetails(losDetails);


                }else{

                    losResponse.setErrorCode(Status.ERROR.name());
                    losResponse.setErrorMsg(ErrorCode.LOS_CONNECT_ERROR);

                    return GngUtils.getBaseResponse(HttpStatus.OK, losResponse);
                }
            }
        } catch (UnsupportedEncodingException | ClientProtocolException | IllegalStateException
                | JsonGenerationException | JsonParseException | JsonMappingException | JAXBException e) {

            populateLosException(losException, e);

        } catch (IOException e) {

            populateLosException(losException, e);

        } catch (Exception e) {

            populateLosException(losException, e);


        }

        losLog.setException(losException);
        dmzRepository.saveLosLogObj(losLog);

        if (losDetailsRequest.getlOSDetails() != null && StringUtils.isNotBlank(losDetailsRequest.getlOSDetails().getError())) {
            losDetailsRequest.getlOSDetails().setError(losLog.getException().getDevMessage() + ":" + losLog.getException().getCause());
        }

        if (losLog.getLosCustomerResponse() != null &&
                !StringUtils.containsIgnoreCase(losLog.getLosCustomerResponse().getErrorDesc(), "duplicate")) {

            croManager.updateLosDetails(losDetailsRequest);

        }

        if (losLog.getException() != null
                && (StringUtils.isNotBlank(losLog.getException().getCause()) || StringUtils
                .isNotBlank(losLog.getException().getDevMessage()))) {

            losResponse.setErrorCode(Status.ERROR.name());

            StringBuffer error = new StringBuffer();

            if (StringUtils.isNotBlank(losLog.getException().getCause())) {

                error.append(losLog.getException().getCause());

            }
            if (StringUtils.isNotBlank(losLog.getException().getDevMessage())) {

                error.append(losLog.getException().getDevMessage());

            }

            losResponse.setErrorCode(Status.ERROR.name());
            losResponse.setErrorMsg(error.toString());

            return GngUtils.getBaseResponse(HttpStatus.OK, losResponse);

        }

        if (losLog.getLosCustomerResponse() != null && StringUtils.equalsIgnoreCase(losLog.getLosCustomerResponse().getErrorDesc(), Status.SUCCESS.name())) {
            return GngUtils.getBaseResponse(HttpStatus.OK, losResponse);
        }

        logger.debug("DMZS los service completed ");

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

    }

    private void pushDataToDMS(DmzRequest dmzRequest) {
        try {
            logger.debug("Inside push DMS data");

            //build DMS push object
            PushDMSDocumentsRequest pushDMSDocumentsRequest = new PushDMSDocumentsRequest();

            pushDMSDocumentsRequest.setRefId(dmzRequest.getGonogoRefId());
            pushDMSDocumentsRequest.setHeader(dmzRequest.getHeader());
            //call to DMS push service
            dmsFeedManager.pushDocumentsToDms(pushDMSDocumentsRequest);

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occur at the time of pushing data to the DMS with probable cause {}", e.getMessage());
        }
    }

    @Override
    public BaseResponse postMBDataToLos(DmzRequest dmzRequest) throws Exception {

        MbPushResponse mbPushResponse = new MbPushResponse();

        mbPushResponse.setRefId(dmzRequest.getGonogoRefId());

        String refId = dmzRequest.getGonogoRefId();

        String institutionId = dmzRequest.getHeader().getInstitutionId();

        LosMBRequestBuilder builder = new LosMBRequestBuilderImpl();

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, institutionId);
        LosMbData losMbData = losRepository.getLosMbData(refId);

        List<LosMbPostRequest> losMBPostRequests = null;

        try {

            losMBPostRequests = builder.buildLosMbRequest(goNoGoCustomerApplication,losMbData);

        } catch (JAXBException e1) {

            logger.error("Error occurred while building losmb request with probable cause [{}]", e1);

            throw new Exception(String.format("Error occurred while building losmb request with probable cause [%s]", e1));


        }


        if (losMBPostRequests != null && !losMBPostRequests.isEmpty()) {

            for (LosMbPostRequest losMbPostRequest : losMBPostRequests) {

                LosConfiguration losConfiguration = Cache.URL_CONFIGURATION.getLosConfiguration().get(institutionId);

                if (null == losConfiguration) {

                    logger.error("Configuration not setup for institution {} ", institutionId);

                    throw new Exception(String.format("Configuration not setup for institution %s", institutionId));
                }

                String url = losConfiguration.getMbPostUrl();

                String inputJson = JsonUtil.ObjectToString(losMbPostRequest);

                try {
                    String response = httpTransportationService.postRequest(url, inputJson, MediaType.APPLICATION_JSON);

                    if (StringUtils.isNotBlank(response)) {

                        MbPushResponse mbPushResponseForSingleRequest = JsonUtil.StringToObject(response, MbPushResponse.class);

                        if (null != mbPushResponseForSingleRequest.getMbResponseAck() && null == mbPushResponseForSingleRequest.getMbEoTAck()) {

                            mbPushResponse.setMbResponseAck(mbPushResponseForSingleRequest.getMbResponseAck());

                        } else if (null != mbPushResponseForSingleRequest.getMbEoTAck() && null == mbPushResponseForSingleRequest.getMbResponseAck()) {

                            mbPushResponse.setMbEoTAck(mbPushResponseForSingleRequest.getMbEoTAck());
                        }
                    }
                } catch (UnsupportedEncodingException | ClientProtocolException | IllegalStateException
                        | JsonGenerationException | JsonParseException | JsonMappingException e) {

                    logger.error("Error occurred while sending mb data push to los with probable cause [{}]", e);

                    throw new SystemException(String.format("Error occurred while sending mb data push to los with probable cause [%s]", e));

                } catch (IOException e) {

                    logger.error("Error occurred while sending mb data push to los with probable cause [{}]", e);

                    throw new SystemException(String.format("Error occurred while sending mb data push to los with probable cause [%s]", e));
                }

            }
        }

        dmzRepository.losMbPushLogs(mbPushResponse);

        return GngUtils.getBaseResponse(HttpStatus.OK, mbPushResponse);

    }

    @Override
    public BaseResponse getServiceStatus(
            DmzServiceCheckRequest dmzServiceCheckReq) throws Exception {


        logger.debug("DMZ service to check service status is initiated");

        DMZResponse dmzServiceStatus = new DMZResponse();

        dmzServiceStatus.setErrorCode(Status.SUCCESS.name());

        if (dmzServiceCheckReq.getActionName() == ActionName.LOS_DATA_ENTRY_INTERFACE &&
                dmzRepository.checkInvoiceObject(dmzServiceCheckReq)) {

            logger.debug("DMZ service invoice object is not populated");

            dmzServiceStatus.setErrorCode(Status.ERROR.name());
            dmzServiceStatus.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);

            return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);

        } else if (dmzServiceCheckReq.getActionName() == ActionName.RELIANCE_INTERFACE) {


            PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(dmzServiceCheckReq.getGonogoRefId(),
                    dmzServiceCheckReq.getHeader().getInstitutionId());

            if (postIpaRequest == null) {

                logger.debug("DMZ service  postipa not populated");

                dmzServiceStatus.setErrorCode(Status.ERROR.name());
                dmzServiceStatus.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);

                return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);

            } else if (postIpaRequest.getPostIPA() != null
                    && postIpaRequest.getPostIPA().getAssetDetails() != null
                    && !postIpaRequest.getPostIPA().getAssetDetails().isEmpty()
                    && (lookupService.checkActionsAccess(
                    dmzServiceCheckReq.getHeader().getInstitutionId(), dmzServiceCheckReq.getHeader().getProduct().toProductId(), ActionName.ALL_RELIANCE_DEALER) &&
                    !dmzRepository.checkRelianceDealerAccess(postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName()))) {

                dmzServiceStatus.setErrorCode(Status.SUCCESS.name());
                dmzServiceStatus.setErrorMsg(postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName() + " doesn't have access");

                return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);
            }

        } else if (dmzServiceCheckReq.getActionName() == ActionName.TKIL_INTERFACE) {

            PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(dmzServiceCheckReq.getGonogoRefId(),
                    dmzServiceCheckReq.getHeader().getInstitutionId());

            if (postIpaRequest == null) {

                logger.debug("DMZ service postipa not populated");

                dmzServiceStatus.setErrorCode(Status.ERROR.name());
                dmzServiceStatus.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);

                return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);
            }else if (postIpaRequest.getPostIPA() != null
                    && !CollectionUtils.isEmpty(postIpaRequest.getPostIPA().getAssetDetails())
                    && (lookupService.checkActionsAccess(
                    dmzServiceCheckReq.getHeader().getInstitutionId(), dmzServiceCheckReq.getHeader().getProduct().toProductId(), ActionName.ALL_TKIL_DEALER) &&
                    !dmzRepository.checkTkilDealerAccess( dmzServiceCheckReq.getHeader().getInstitutionId(),postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName()))) {

                dmzServiceStatus.setErrorCode(Status.SUCCESS.name());
                dmzServiceStatus.setErrorMsg(postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName() + " doesn't have access");

                return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);
            }

        }

        Object object = dmzRepository.getServiceStatus(dmzServiceCheckReq);

        if (object == null) {

            dmzServiceStatus.setErrorCode(Status.ERROR.name());
            dmzServiceStatus.setErrorMsg("Service was not successful. Please try again");

            return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);

        } else {

            return GngUtils.getBaseResponse(HttpStatus.OK, dmzServiceStatus);
        }

    }

    @Override
    public boolean pushTvr(String referenceId, String institutionId, GoNoGoCustomerApplication goNoGoCustomerApplicationWf) {

        boolean tvrStatus = false;

        try {

            WFJobCommDomain tvrConfig = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, UrlType.TVR.toValue());

            GoNoGoCustomerApplication goNoGoCustomerApplication;

            if (null == goNoGoCustomerApplicationWf) {
                //
                goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(referenceId, institutionId);

            } else {
                //Call from Workflow
                goNoGoCustomerApplication = goNoGoCustomerApplicationWf;
            }

            if (null != goNoGoCustomerApplication && null != tvrConfig) {
                TvrRequestDTO tvrRequestDTO = tvrRequestBuilder.buildTvrRequest(goNoGoCustomerApplication);

                String url = Arrays.asList(tvrConfig.getBaseUrl(), tvrConfig.getEndpoint())
                        .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

                TvrResponse tvrResponse = (TvrResponse) TransportUtils.postJsonRequest(tvrRequestDTO, url, TvrResponse.class);

                if (null != tvrResponse) {
                    tvrResponse.setResponseDate(new Date());
                    dmzRepository.saveTvrLog(TVRLogs.builder()
                            .refId(referenceId)
                            .tvrRequest(tvrRequestDTO)
                            .tvrResponse(tvrResponse)
                            .build());

                    tvrStatus = true;

                    logger.info("Getting response from Tvr and and save for referenceId {}", referenceId);

                } else {

                    logger.debug("Not getting response from Tvr for referenceId {}", referenceId);

                }
            } else {
                logger.warn("Tvr not call due to lack of configuration or data to build request object for referenceId {}", referenceId);
                tvrStatus = false;
            }

        } catch (Exception e) {
            logger.error("Exception occurred while pushing data to TVR with probable cause:{}", e.getStackTrace());
            tvrStatus = false;
        }

        return tvrStatus;
    }

    @Override
    public BaseResponse pushTkil(DmzRequest dmzRequest) throws Exception {

        BaseResponse baseResponse=null;
        TKILLogging tkilLogging = new TKILLogging();
        DMZResponse dmzResponse = new DMZResponse();
        logger.debug("DMZS initiated tkil service for ref id {}", dmzRequest.getGonogoRefId());

        if (dmzRepository.checkTkilStatus(dmzRequest.getGonogoRefId())) {

            dmzResponse.setErrorCode(Status.SUCCESS.name());
            dmzResponse.setErrorMsg("Record is punched successfully");

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
        }else {

            PostIpaRequest postIpaRequest = applicationRepository.fetchPostIpaDetails(dmzRequest.getGonogoRefId(), dmzRequest.getHeader().getInstitutionId());

            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(dmzRequest.getGonogoRefId(), dmzRequest.getHeader().getInstitutionId());

            ApplicationRequest appRequest = goNoGoCustomerApplication != null ? goNoGoCustomerApplication.getApplicationRequest():null;

            if (postIpaRequest == null || appRequest == null) {

                dmzResponse.setErrorCode(Status.ERROR.name());
                dmzResponse.setErrorMsg(ErrorCode.INKVOKE_SERVICE_ERROR);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
                tkilLogging.setResponse(dmzResponse);
            }else {

                //Cache.checkActionsAccess(ActionName.ALL_TKIL_DEALER, institutionId, dmzRequest.getHeader().getProduct().toProductId());

                if (lookupService.checkActionsAccess(dmzRequest.getHeader().getInstitutionId(), dmzRequest.getHeader().getProduct().toProductId(), ActionName.ALL_TKIL_DEALER)) {

                    if (postIpaRequest.getPostIPA() != null
                            && !CollectionUtils.isEmpty(postIpaRequest.getPostIPA().getAssetDetails())
                            && StringUtils.isNotBlank(postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName())) {

                        String dealerName = postIpaRequest.getPostIPA().getAssetDetails().get(0).getDlrName();

                        if (!dmzRepository.checkTkilDealerAccess(dmzRequest.getHeader().getInstitutionId(),dealerName)) {

                            dmzResponse.setErrorCode(Status.SUCCESS.name());
                            dmzResponse.setErrorMsg(dealerName + " doesn't have access");

                            return GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
                        }
                    }
                }

                TKILRequest tkilRequest = tkilRequestBuilder.buildTkilRequest(postIpaRequest, appRequest);

                Collection<Error> errors = serialNumberValidationEngine.validationForTkil(tkilRequest);

                ThirdPartyException thirdPartyException = new ThirdPartyException();

                tkilLogging.setRequest(tkilRequest);

                if (errors.isEmpty()) {

                        WFJobCommDomain config = workFlowCommunicationManager.getWfCommDomainJobByType(dmzRequest.getHeader().getInstitutionId(), UrlType.TKIL.toValue());
                        if (null == config) {

                            logger.error("Configuration not setup for institution {} ", dmzRequest.getHeader().getInstitutionId());

                            errors.add(Error.builder()
                                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                    .level(Error.SEVERITY.CRITICAL.name())
                                    .build());
                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
                        }else {

                        try {

                            String url = StringUtils.join(config.getBaseUrl(), config.getEndpoint());
                            String inputJson = JsonUtil.ObjectToString(tkilRequest);
                            String response = httpTransportationService.postRequest(url, inputJson, MediaType.APPLICATION_JSON);
                            tkilLogging.setRawResponse(response);

                            logger.debug("DMZS received response {} tkil service ", response);

                            if (StringUtils.isNotBlank(response)) {

                                TKILResponse tkilResponse = JsonUtil.StringToObject(response, TKILResponse.class);

                                if (tkilResponse != null && tkilResponse.isOriginalResponse()) {

                                    dmzResponse.setErrorCode(Status.SUCCESS.name());
                                    dmzResponse.setErrorMsg(GngUtils.convertTKILResponseCodeToMessage(tkilResponse.isOriginalResponse()));
                                } else {
                                    dmzResponse.setErrorCode(Status.ERROR.name());
                                    dmzResponse.setErrorMsg(GngUtils.convertTKILResponseCodeToMessage(tkilResponse.isOriginalResponse()));
                                }
                                tkilLogging.setResponse(dmzResponse);
                                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,dmzResponse);
                            }else {
                                dmzResponse.setErrorCode(Status.ERROR.name());
                                dmzResponse.setErrorMsg(ErrorCode.EXTERNAL_SERVICE_FAILURE);
                                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,dmzResponse);
                            }
                        } catch (UnsupportedEncodingException | ClientProtocolException | IllegalStateException
                                | JsonGenerationException | JsonParseException | JsonMappingException e) {

                            populateTkilException(thirdPartyException, e);

                        } catch (IOException e) {
                            populateTkilException(thirdPartyException, e);
                        }
                    }
                        tkilLogging.setThirdPartyException(thirdPartyException);

                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
                    }

                if (tkilLogging.getResponse() != null && StringUtils.equalsIgnoreCase(tkilLogging.getResponse().getErrorCode(), Status.SUCCESS.name())) {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmzResponse);
                }
            }
            dmzRepository.saveTkilLog(tkilLogging);
            logger.debug("DMZS tkil service completed ");
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getTkilLogs(String refID) throws Exception{
        BaseResponse baseResponse=null;
        try {
            logger.debug("DMZS initiated tkil log service for ref id {}", refID);
            List<TKILLogging> tkilLoggingList =  dmzRepository.getTkilLogData(refID);
            if(CollectionUtils.isEmpty(tkilLoggingList)){
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, tkilLoggingList);
            }
        }catch (Exception e){
            logger.error("Error while fetching tkil log with possible cause {}", e.getMessage());

            throw new SystemException(String.format("Error while fetching tkil log with possible cause {%s}", e.getMessage()));
        }
        return baseResponse;
    }

    /**
     * Save exception
     *
     * @param relianceException
     * @param e
     */
    private void populateRelianceException(RelianceDigitalException relianceException, Throwable e) {
        if (e.getCause() != null)
            relianceException.setCause(e.getCause().toString());
        relianceException.setDevMessage(e.getMessage());
    }

    private void populateLosException(LosException losException, Throwable e) {
        if (e.getCause() != null)
            losException.setCause(e.getCause().toString());
        losException.setDevMessage(e.getMessage());
    }

    enum relianceErrorCode {

        SUCCESS("00");

        private String value;

        relianceErrorCode(String value) {
            this.value = value;
        }

        public String getFaceValue() {
            return value;
        }
    }

    /**
     * Save exception
     *
     * @param thirdPartyException
     * @param e
     */
    private void populateTkilException(ThirdPartyException thirdPartyException, Throwable e) {
        if (e.getCause() != null)
            thirdPartyException.setType(e.getCause().toString());
        thirdPartyException.setMessage(e.getMessage());
    }

    enum tkilSuccessCode {

        SUCCESS("00");

        private String value;

        tkilSuccessCode(String value) {
            this.value = value;
        }

        public String getFaceValue() {
            return value;
        }
    }

}
