package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.ApplicationTrackingRepository;
import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.service.ApplicationTrackerManager;
import com.softcell.service.EventDataStandardization;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationTrackerManagerImpl implements ApplicationTrackerManager {

    @Autowired
    private ApplicationTrackingRepository applicationTrackingRepository;

    @Autowired
    private EventDataStandardization eventDataStandardization;

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTrackerManagerImpl.class);



    @Override
    public BaseResponse getApplicationCaseHistory(
            CheckApplicationStatus checkApplicationStatus) throws Exception {
        try {

            List<?> events = applicationTrackingRepository.getApplicationCaseHistory(checkApplicationStatus.getHeader().getInstitutionId(),
                    checkApplicationStatus.getGonogoRefId(), checkApplicationStatus.getHeader().getApplicationSource());
            if (null != events && events.size() > 0) {

                return GngUtils.getBaseResponse(HttpStatus.OK, eventDataStandardization.getApplicationCaseHistory(checkApplicationStatus.getHeader().getInstitutionId(),
                        checkApplicationStatus.getGonogoRefId(), events));
            }

            return GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        } catch (Exception e) {

            logger.error("{}",e.getStackTrace());

            throw new Exception(String.format("Exception while fetching application case history with cause [%s]",e.getMessage()));
        }

    }

    @Override
    public BaseResponse getApplicationCSV(ReportingModuleConfiguration configuration) throws Exception {

        List<ApplicationTracking> applicationTrackings = applicationTrackingRepository.getApplicationHistory(configuration);

        EventDataStandardization dataStandardization = new EventDataStandardizationImpl();

        if (null != applicationTrackings) {

            return GngUtils.getBaseResponse(HttpStatus.OK ,dataStandardization.getApplicationHistoryReport(applicationTrackings));

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
    }


}
