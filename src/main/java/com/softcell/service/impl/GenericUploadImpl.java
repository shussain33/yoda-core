package com.softcell.service.impl;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.softcell.constants.Constant;
import com.softcell.constants.Status;
import com.softcell.constants.SystemErrorConstant;
import com.softcell.constants.error.ErrorCode;
import com.softcell.constants.master.MasterDetails;
import com.softcell.dao.mongodb.repository.FileUploadRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.response.MasterRecords;
import com.softcell.gonogo.model.response.MasterValidationStatus;
import com.softcell.service.GenericUpload;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by bhuvneshk on 27/9/17.
 */
@Service
public class GenericUploadImpl implements GenericUpload{


    private static final Logger logger = LoggerFactory.getLogger(GenericUploadImpl.class);

    @Autowired
    FileUploadRepository fileUploadRepository;

    @Autowired
    ConfigurationRepository configurationRepository;

    MasterValidationStatus masterValidationStatus;

    @Override
    public FileUploadStatus uploadPincode(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (PincodeStateMasterFields field : PincodeStateMasterFields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<PinCodeMaster> pinCodeMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(pinCodeMasterList)){
            //insert institution id against every record
            pinCodeMasterList.forEach(pinCodeMaster ->{
                pinCodeMaster.setInstitutionId(institutionId);
                pinCodeMaster.setActive(true);
            });
            boolean dbStatus=fileUploadRepository.insertPinCodeMasterList(pinCodeMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(pinCodeMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

    return fileUploadStatus;
    }

    @Override
    public FileUploadStatus uploadBranch(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (BranchMasterFields field : BranchMasterFields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<BranchMaster> branchMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(branchMasterList)){
            //insert institution id against every record
            branchMasterList.forEach(branchMaster ->{
                branchMaster.setInstitutionId(institutionId);
                branchMaster.setActive(true);
            });

            boolean dbStatus=fileUploadRepository.insertBranchMasterList(branchMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(branchMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;
    }

    @Override
    public FileUploadStatus uploadDealerEmail(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (DealerEmailFields field : DealerEmailFields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<DealerEmailMaster> dealerEmailMasters =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(dealerEmailMasters)){
            //insert institution id against every record
            dealerEmailMasters.forEach(dealerEmailMaster ->{
                dealerEmailMaster.setInstitutionID(institutionId);
                dealerEmailMaster.setDealerID(dealerEmailMaster.getSupplierID());
                dealerEmailMaster.setRecipientsType("TO");
                dealerEmailMaster.setActive(true);
            });
            boolean dbStatus=fileUploadRepository.insertDealerEmailMasterList(dealerEmailMasters);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(dealerEmailMasters);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;
    }

    @Override
    public FileUploadStatus uploadInsurancePremium(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (InsurancePremiumFields field : InsurancePremiumFields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<InsurancePremiumMaster> insurancePremiumMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(insurancePremiumMasterList)){
            //insert institution id against every record
            insurancePremiumMasterList.forEach(insurancePremiumMaster ->{

                insurancePremiumMaster.setInstitutionId(institutionId);
                insurancePremiumMaster.setInsuranceTypeDesc(insurancePremiumMaster.getInsuranceType());
                insurancePremiumMaster.setActive(true);

            });

            boolean dbStatus=fileUploadRepository.insertInsurancePremiumMaster(insurancePremiumMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(insurancePremiumMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;
    }

    @Override
    public FileUploadStatus uploadState(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (StateMasterfields field : StateMasterfields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<StateMaster> stateMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(stateMasterList)){
            //insert institution id against every record
            stateMasterList.forEach(stateMaster ->{

                stateMaster.setInstitutionId(institutionId);
                stateMaster.setActive(true);

            });

            boolean dbStatus=fileUploadRepository.insertStateMasterList(stateMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(stateMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;

    }

    @Override
    public FileUploadStatus uploadCityState(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (CityStateMasterFields field : CityStateMasterFields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<CityStateMappingMaster> cityStateMappingMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(cityStateMappingMasterList)){
            //insert institution id against every record
            cityStateMappingMasterList.forEach(cityStateMaster ->{

                cityStateMaster.setInstitutionId(institutionId);
                cityStateMaster.setActive(true);

            });

            boolean dbStatus=fileUploadRepository.insertCityStateMasterList(cityStateMappingMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(cityStateMappingMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;

    }

    @Override
    public FileUploadStatus uploadScheme(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (SchemeMasterFields field : SchemeMasterFields.values()) {
            columnMapping.put(field.name(), field.getStringValue());
        }

        List<SchemeMasterData> schemeMasterDataList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(schemeMasterDataList)){
            //insert institution id against every record
            schemeMasterDataList.forEach(schemeMasterData ->{
                schemeMasterData.setInstitutionId(institutionId);
                schemeMasterData.setTenure(schemeMasterData.getMaxTenure());
                schemeMasterData.setActive(true);

            });

            boolean dbStatus=fileUploadRepository.insertSchemeMaster(schemeMasterDataList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(schemeMasterDataList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;

    }

    @Override
    public FileUploadStatus uploadAssetModel(File file, MasterDetails masterName, String institutionId) throws IOException {

        HashMap<String, String> columnMapping = new HashMap();
        MasterRecords masterRecords = null;
        FileUploadStatus fileUploadStatus=new FileUploadStatus();

        MasterMappingConfiguration masterMappingConfiguration = configurationRepository
                .getMasterMappingConfiguration(institutionId, masterName.getMasterName());

        for (AssetModelMasterfields field : AssetModelMasterfields.values()) {
            columnMapping.put(field.name(), field.getValue());
        }

        List<AssetModelMaster> assetModelMasterList =
                transformCsvToList(file, columnMapping, masterName.getMasterClass(),fileUploadStatus, masterMappingConfiguration);

        if(!CollectionUtils.isEmpty(assetModelMasterList)){
            //insert institution id against every record
            assetModelMasterList.forEach(assetModelMaster ->{

                assetModelMaster.setInstitutionId(institutionId);
                assetModelMaster.setBlockDesc(assetModelMaster.getProductFlag());

                // MO default for now
                assetModelMaster.setMakeModelFlag("MO");
                assetModelMaster.setActive(true);

            });

            boolean dbStatus=fileUploadRepository.insertAssetModelMasterList(assetModelMasterList);
            masterRecords = new MasterRecords();

            masterRecords.setMasterList(assetModelMasterList);
            fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);
        }

        return fileUploadStatus;

    }

    private List transformCsvToList(File file, HashMap<String,String> destinationMapping, Class classname, FileUploadStatus fileUploadStatus,
                                     MasterMappingConfiguration masterMappingConfiguration) throws IOException {

        CsvToBean csv = new CsvToBean();

        /**
         * TO fetch header details from source
         */
        CSVReader headerReader = new CSVReader(new FileReader(file));

        /**
         * Source file reader
         */
        CSVReader fileReader = new CSVReader(new FileReader(file));

        /**
         * Header list
         */
        String[] header=headerReader.readNext();
        List headerlist= Arrays.asList(header);

        /**
         * Destination header
         */
        Object[] key= destinationMapping.keySet().toArray();
        List keyList=Arrays.asList(key);

        /**
         * Matching field for source to destination
         */
        List<String>matchFields=new ArrayList<String>(keyList);
        List<String> aditionalFields=new ArrayList<String>(headerlist);
        matchFields.retainAll(headerlist);
        aditionalFields.removeAll(keyList);
        List<String>missingFields=new ArrayList<String>(keyList);
        missingFields.removeAll( matchFields );

        fileUploadStatus.setAdditionalFields(String.valueOf(aditionalFields));
        logger.debug("matching fields in coloum and csv file"+matchFields);
        logger.debug("additional fields is present in csv file"+aditionalFields);
        logger.debug("missing fields in csv file which are present in bean file"+missingFields);


        Map<String, String> columnMappingMap = masterMappingConfiguration.getColumnMapping();
        Map<String, String> sourceColumnToBeanMapping = new HashMap<>();

        columnMappingMap.keySet().forEach(sourceColumnName -> {
            String destinationColumnName = columnMappingMap.get(sourceColumnName);
            if(destinationMapping.containsKey(destinationColumnName)){
                String beanName = destinationMapping.get(destinationColumnName);
                sourceColumnToBeanMapping.put(sourceColumnName, beanName);
            }
        });
        if (columnMappingMap.size() == sourceColumnToBeanMapping.size()){


            HeaderColumnNameTranslateMappingStrategy strategy = new HeaderColumnNameTranslateMappingStrategy();
            strategy.setType(classname);
            strategy.setColumnMapping(sourceColumnToBeanMapping);

            //Parse the file
            return csv.parse(strategy, fileReader);}
        else{
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("column mapping is incorrect");
            return null;
        }

    }

    private FileUploadStatus setRecordStatus(boolean dbStatus, MasterRecords masterRecords, FileUploadStatus fileUploadStatus) {
        // FileUploadStatus fileUploadStatus=new FileUploadStatus();
        if(dbStatus==true){
            fileUploadStatus.setStatus(Constant.SUCCESS);
            fileUploadStatus.setNumberOfRecords(masterRecords.getMasterList().size());
            fileUploadStatus.setNumberOfRecordSuccess(masterRecords.getMasterList().size());


            masterRecords.setFileUploadStatus(fileUploadStatus);
        }else{
            fileUploadStatus.setStatus(Status.ERROR.name());
            fileUploadStatus.setErrorDesc(SystemErrorConstant.CURRUPT_RECORD);
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        if (masterRecords != null
                && masterRecords.getMasterList() != null
                && !masterRecords.getMasterList().isEmpty()
                && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
            fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);}
        else {
            fileUploadStatus = masterRecords.getFileUploadStatus();
        }
        return fileUploadStatus;
    }

    private FileUploadStatus getDbFileUploadStatus(boolean dbStatus, MasterRecords masterRecords) {
        FileUploadStatus fileUploadStatus;
        if (dbStatus) {
            logger.info("Database operation success");
            fileUploadStatus = masterRecords.getFileUploadStatus();

        } else {
            logger.error("Database operation failure");
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc("Database operation failure");
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return fileUploadStatus;
    }

    @Override
    public FileUploadStatus uploadApiRoleAuthorisationMaster(File file, MasterDetails masterName, String institutionId,String sourceId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if(file != null && masterName != null && StringUtils.isNotBlank(institutionId)){
            MasterMappingConfiguration masterMappingConfiguration = configurationRepository.getMasterMappingConfiguration(institutionId, masterName.getMasterName(),null);

            if(masterMappingConfiguration != null){
                masterValidationStatus = columnMappingCheck(file, masterMappingConfiguration);

                if (masterValidationStatus.isValid()) {
                    HashMap<String, String> columnMapping = new HashMap();
                    MasterRecords masterRecords = null;
                    for (ApiRoleAuthorisationFields field : ApiRoleAuthorisationFields.values()) {
                        columnMapping.put(field.name(), field.getValue());
                    }

                    if(!columnMapping.isEmpty()){
                        List<ApiRoleAuthorisationMaster> apiRoleAuthorisationMasters = transformCsvToList(file, columnMapping, masterName.getMasterClass(), fileUploadStatus, masterMappingConfiguration);

                        if (!CollectionUtils.isEmpty(apiRoleAuthorisationMasters)) {
                            apiRoleAuthorisationMasters.forEach( apiRoleAuthorisationMaster -> {
                                apiRoleAuthorisationMaster.setInstitutionId(institutionId);
                                apiRoleAuthorisationMaster.setSourceId(sourceId);
                                apiRoleAuthorisationMaster.setActive(true);
                                apiRoleAuthorisationMaster.setRolesList(Arrays.asList(apiRoleAuthorisationMaster.getRoles().split(",")));
                                apiRoleAuthorisationMaster.setRoles(null);
                            });
                        }

                        boolean dbStatus = fileUploadRepository.insertApiRoleAuthorisationMaster(apiRoleAuthorisationMasters,institutionId,sourceId);
                        masterRecords = new MasterRecords();
                        masterRecords.setMasterList(apiRoleAuthorisationMasters);
                        fileUploadStatus = setRecordStatus(dbStatus, masterRecords, fileUploadStatus);
                    }
                }else {
                    fileUploadStatus.setErrorDesc(String.format(ErrorCode.WRONG_FILE_CONTENT, masterName, masterValidationStatus.getMissingHeader()));
                }
            }else{
                fileUploadStatus.setErrorDesc(String.format(ErrorCode.MST_CONFIG_NOT_FOUND));
            }
        }else{
            fileUploadStatus.setErrorDesc(String.format(ErrorCode.REQ_VALIDATION_FAILED));
        }
        return fileUploadStatus;
    }

    public MasterValidationStatus columnMappingCheck(File file, MasterMappingConfiguration masterMappingConfiguration) {
        MasterValidationStatus masterValidationStatus = new MasterValidationStatus();
        boolean valid = false;
        CSVReader headerReader = null;
        List<String> missingHeader = new ArrayList<>();
        try {
            headerReader = new CSVReader(new FileReader(file));
            String[] header = headerReader.readNext();
            List<String> headerlist = new ArrayList<>();
            if (headerlist != null) {
                for (String s : header) {
                    headerlist.add(s.toUpperCase().trim());
                }
                Map<String, String> columnMapping = masterMappingConfiguration.getColumnMapping();
                List<String> columnlist = new ArrayList<>();
                String[] map = columnMapping.keySet().toArray(new String[0]);
                for (String s : map) {
                    columnlist.add(s.toUpperCase().trim());
                }
                List<String> columnlisttemp = new ArrayList<>();
                columnlisttemp.addAll(columnlist);
                columnlisttemp.retainAll(headerlist);
                columnlist.removeAll(columnlisttemp);
                missingHeader.addAll(columnlist);
            }
            if (missingHeader.isEmpty()) {
                valid = true;
            } else {
                valid = false;
            }

            masterValidationStatus.setMissingHeader(missingHeader);
            masterValidationStatus.setValid(valid);

        } catch (Exception e) {
            logger.error("Error in columnMappingCheck" + e);
        }
        return masterValidationStatus;
    }

}
