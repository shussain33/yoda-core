package com.softcell.service.impl;

import com.softcell.dao.mongodb.repository.GitRepository;
import com.softcell.gonogo.model.git.GitRepositoryState;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by prateek on 13/3/17.
 */
@Service
public class GitPropertiesService {

    @Autowired
    private GitRepository gitRepository;


    public BaseResponse getGitProprty() {
        GitRepositoryState gitRepositoryState = gitRepository.getGitRepositoryState();
        return BaseResponse.builder().payload(new Payload<>(gitRepositoryState)).build();
    }

}
