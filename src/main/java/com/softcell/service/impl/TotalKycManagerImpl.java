package com.softcell.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.kyc.request.KPanRequest;
import com.softcell.gonogo.model.kyc.KycVerificationRequest;
import com.softcell.gonogo.model.kyc.KycVerificationResponse;
import com.softcell.gonogo.model.kyc.TotalKycReqResLog;
import com.softcell.gonogo.model.kyc.karza.drivlic.request.KDrivLicResult;
import com.softcell.gonogo.model.kyc.request.*;
import com.softcell.gonogo.model.kyc.request.VoterIdResponses.KVoterIdResult;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstIdentityRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstAuthDetailRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstIdentyRequest;
import com.softcell.gonogo.model.kyc.request.gstResponses.KGstAuthResponse;
import com.softcell.gonogo.model.kyc.request.gstResponses.KGstAuthResult;
import com.softcell.gonogo.model.kyc.request.gstResponses.KGstIdentityResponse;
import com.softcell.gonogo.model.kyc.request.gstResponses.KGstIdentityResult;
import com.softcell.gonogo.model.kyc.request.tanRequests.KTanRequest;
import com.softcell.gonogo.model.kyc.request.tanResponses.KTanAuthResult;
import com.softcell.gonogo.model.kyc.request.tanResponses.KTanDetailResult;
import com.softcell.gonogo.model.kyc.response.*;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.factory.TotalKycBuilder;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.TotalKycManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.utils.Utility;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by anupamad on 13/7/17.
 */
@Service
public class TotalKycManagerImpl implements TotalKycManager{

    private static final Logger logger = LoggerFactory.getLogger(TotalKycManagerImpl.class);

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private TotalKycBuilder totalKycBuilder;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationMongoRepository applicationMongoRepository;

    // TODO : Move to appropriate constant class later
    public static final String KYC_SERVICE_KARZA = "KARZA";

    private ObjectMapper mapper = new ObjectMapper();


    /**
     * Get LPG Details
     * @param totalKycLpgDetailRequest
     * @return
     * @throws Exception
     */
    @Override
    public BaseResponse getLpgDetail(TotalKycLpgDetailRequest totalKycLpgDetailRequest) throws Exception {
        BaseResponse baseResponse;
        Collection<Error> errors;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycLpgDetailRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_LPG_DETAIL.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                        .action(UrlType.TOTAL_KYC_KLPG.toValue())
                        .orgReq(totalKycLpgDetailRequest.toString()).build();
        //Read the configuration.
        WFJobCommDomain totalKycConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycLpgDetailRequest.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KLPG.toValue());

        if (null == totalKycConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycLpgDetailRequest.getHeader().getProduct()) {
            //Build request for the connector call
            KLpgIdRequest kLpgIdRequest = totalKycBuilder.buildLpgDetailRequest(totalKycLpgDetailRequest, totalKycConfig);

            String url = Arrays.asList(totalKycConfig.getBaseUrl(), totalKycConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            //Make the API call
            KLpgDetailResponse kLpgDetailResponse = (KLpgDetailResponse) TransportUtils.postJsonRequest(kLpgIdRequest,
                    url, KLpgDetailResponse.class);

            if (null != kLpgDetailResponse && null == kLpgDetailResponse.getError()) {
                totalKycReqResLog.setKycId(kLpgIdRequest.getMobileNumber());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KLPG.name());
                totalKycReqResLog.setStatus(kLpgDetailResponse.getStatus());
                totalKycReqResLog.setOrgRes(kLpgDetailResponse.getOrgRes());
                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kLpgDetailResponse);
            }else if(kLpgDetailResponse != null && kLpgDetailResponse.getError() != null) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kLpgDetailResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kLpgDetailResponse.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get LPG details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    /**
     * Get Driving License Details.
     * @param totalKycDlDetailRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getDlDetail(TotalKycDlDetailRequest totalKycDlDetailRequest) throws Exception
    {
        BaseResponse baseResponse;
        Collection<Error> errors;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycDlDetailRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_DRIV_LIC_DETAIL.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KDL.toValue())
                .orgReq(totalKycDlDetailRequest.toString()).build();

        if (Product.CDL == totalKycDlDetailRequest.getHeader().getProduct()) {
            //Read the configuration details
            WFJobCommDomain totalKycConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    totalKycDlDetailRequest.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KDL.toValue());

            if (null == totalKycConfig) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }

            //Build request for the connector call
            KDrivLicRequest kDrivLicRequest = totalKycBuilder.buildDlDetailRequest(totalKycDlDetailRequest,
                            totalKycConfig);
            String url = Arrays.asList(totalKycConfig.getBaseUrl(), totalKycConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            KDrivLicResponse kDrivLicResponse  = (KDrivLicResponse)
                        TransportUtils.postJsonRequest(kDrivLicRequest, url, KDrivLicResponse.class);

            if (null != kDrivLicResponse && null == kDrivLicResponse.getError()) {
                totalKycReqResLog.setKycId(kDrivLicRequest.getDlNo());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KDL.name());
                totalKycReqResLog.setStatus(kDrivLicResponse.getStatus());
                totalKycReqResLog.setOrgRes(kDrivLicResponse.getOrgRes());
                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kDrivLicResponse);
            }else if (kDrivLicResponse != null && kDrivLicResponse.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kDrivLicResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kDrivLicResponse.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            }else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Driving License details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());

        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    /**
     * Get Electricity Detail Request
     * @param totalKycElecDtlRequest
     * @return
     * @throws Exception
     */
    public BaseResponse getElecDetail(TotalKycElecDtlRequest totalKycElecDtlRequest) throws Exception
    {
        BaseResponse baseResponse;
        Collection<Error> errors;
        KElectrBillResponse kElectrBillResponse = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycElecDtlRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_ELEC_BILL_DETAIL.toFaceValue());
        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KELEC.toValue())
                .orgReq(totalKycElecDtlRequest.toString()).build();

        //Get the configuration
        WFJobCommDomain totalKycConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycElecDtlRequest.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KELEC.toValue());

        if (null == totalKycConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycElecDtlRequest.getHeader().getProduct()) {
            KElecRequest kElecRequest = totalKycBuilder.buildElecDetailRequest(totalKycElecDtlRequest, totalKycConfig);

            String url = Arrays.asList(totalKycConfig.getBaseUrl(), totalKycConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            kElectrBillResponse = (KElectrBillResponse)
                    TransportUtils.postJsonRequest(kElecRequest, url, KElectrBillResponse.class);

            if (null != kElectrBillResponse && null == kElectrBillResponse.getError()) {
                totalKycReqResLog.setKycId(kElecRequest.getConsumerId());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KELEC.name());
                totalKycReqResLog.setStatus(kElectrBillResponse.getStatus());
                totalKycReqResLog.setOrgRes(kElectrBillResponse.getOrgRes());
                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kElectrBillResponse);
            }else if (kElectrBillResponse != null && kElectrBillResponse.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kElectrBillResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kElectrBillResponse.getError().getType())
                        .build());

                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Electricity details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    /**
     * Get Voter ID details.
     * @param totalKycVoterDetailRequest
     * @return
     * @throws Exception
     */
    @Override
    public BaseResponse getVoterDetail(TotalKycVoterDetailRequest totalKycVoterDetailRequest) throws Exception {
        BaseResponse baseResponse;
        Collection<Error> errors;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycVoterDetailRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_VOTERID_DETAIL.toFaceValue());
        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KVOTERID.toValue())
                .orgReq(totalKycVoterDetailRequest.toString()).build();

        WFJobCommDomain totalKycConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycVoterDetailRequest.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KVOTERID.toValue());

        if (null == totalKycConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycVoterDetailRequest.getHeader().getProduct()) {
            //Build request for the connector call
            KVoterIdRequest kVoterIdRequest = totalKycBuilder.buildVoterDetailRequest(totalKycVoterDetailRequest, totalKycConfig);

            String url = Arrays.asList(totalKycConfig.getBaseUrl(), totalKycConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
            KVoterDetailResponse kVoterDetailResponse = (KVoterDetailResponse) TransportUtils.postJsonRequest(kVoterIdRequest
                    , url, KVoterDetailResponse.class);

            if (null != kVoterDetailResponse && null == kVoterDetailResponse.getError()) {
                totalKycReqResLog.setKycId(kVoterIdRequest.getEpicNo());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KVOTERID.name());
                totalKycReqResLog.setStatus(kVoterDetailResponse.getStatus());
                totalKycReqResLog.setOrgRes(kVoterDetailResponse.getOrgRes());
                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kVoterDetailResponse);
            }else if (kVoterDetailResponse != null && kVoterDetailResponse.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kVoterDetailResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kVoterDetailResponse.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Voter details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getLpgDetailV2(TotalKycLpgDetailRequestV2 totalKycLpgDetailRequestV2) throws Exception
    {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycLpgDetailRequestV2.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_LPG_DETAIL_V2.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KLPG_V2.toValue())
                .orgReq(totalKycLpgDetailRequestV2.toString()).build();

        WFJobCommDomain lpgconfigV2 = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycLpgDetailRequestV2.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KLPG_V2.toValue());

        if (null == lpgconfigV2) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycLpgDetailRequestV2.getHeader().getProduct()) {
            //Build request for the connector call
            KLpgIdRequestV2 kLpgIdRequestV2 = totalKycBuilder.buildLpgDetailRequestV2(totalKycLpgDetailRequestV2);

            String url = Arrays.asList(lpgconfigV2.getBaseUrl(), lpgconfigV2.getEndpoint())
                   .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            //Make the API call
            KLpgDetailResponseV2 kLpgDetailResponseV2 = (KLpgDetailResponseV2) TransportUtils.postJsonRequest(
                    kLpgIdRequestV2, url, KLpgDetailResponseV2.class);

            if (null != kLpgDetailResponseV2 && null == kLpgDetailResponseV2.getError()) {
                totalKycReqResLog.setKycId(kLpgIdRequestV2.getMobileNumber());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KLPG_V2.name());
                totalKycReqResLog.setStatus(kLpgDetailResponseV2.getStatus());
                totalKycReqResLog.setOrgRes(kLpgDetailResponseV2.getOrgRes());
                totalKycReqResLog.setKycVersion( KVersion.KVERSION2.toValue() );
                totalKycReqResLog.setRequestId(kLpgDetailResponseV2.getRequest_id());
                totalKycReqResLog.setStatusCode(kLpgDetailResponseV2.getStatus_code());

                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kLpgDetailResponseV2);
            }else if(kLpgDetailResponseV2 != null && kLpgDetailResponseV2.getError() != null) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kLpgDetailResponseV2.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kLpgDetailResponseV2.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get LPG details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        }else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getDlDetailV2(TotalKycDlDetailRequestV2 totalKycDlDetailRequestV2) throws Exception
    {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycDlDetailRequestV2.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_DRIV_LIC_DETAIL_V2.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KDL_V2.toValue())
                .orgReq(totalKycDlDetailRequestV2.toString()).build();

        if (Product.CDL == totalKycDlDetailRequestV2.getHeader().getProduct()) {
            //Read the configuration details
            WFJobCommDomain dlConfigV2 = workFlowCommunicationManager.getWfCommDomainJobByType(
                    totalKycDlDetailRequestV2.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KDL_V2.toValue());

            if (null == dlConfigV2) {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }

            //Build request for the connector call
            KDrivLicRequestV2 kDrivLicRequestV2 = totalKycBuilder.buildDlDetailRequestV2(totalKycDlDetailRequestV2);

            String url = Arrays.asList(dlConfigV2.getBaseUrl(), dlConfigV2.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            KDrivLicResponseV2 kDrivLicResponseV2 = (KDrivLicResponseV2)
                    TransportUtils.postJsonRequest(kDrivLicRequestV2, url, KDrivLicResponseV2.class);

            if (null != kDrivLicResponseV2 && null == kDrivLicResponseV2.getError()) {
                totalKycReqResLog.setKycId(kDrivLicRequestV2.getDlNo());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KDL_V2.name());
                totalKycReqResLog.setStatus(kDrivLicResponseV2.getStatus());
                totalKycReqResLog.setOrgRes(kDrivLicResponseV2.getOrgRes());
                totalKycReqResLog.setKycVersion( KVersion.KVERSION2.toValue() );
                totalKycReqResLog.setRequestId(kDrivLicResponseV2.getRequest_id());
                totalKycReqResLog.setStatusCode(kDrivLicResponseV2.getStatus_code());

                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kDrivLicResponseV2);
            }else if (kDrivLicResponseV2 != null && kDrivLicResponseV2.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kDrivLicResponseV2.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kDrivLicResponseV2.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            }else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Driving License details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());

        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getVoterDetailV2(TotalKycVoterDetailRequestV2 totalKycVoterDetailRequestV2) throws Exception {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycVoterDetailRequestV2.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_VOTERID_DETAIL_V2.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KVOTERID_V2.toValue())
                .orgReq(totalKycVoterDetailRequestV2.toString()).build();

        WFJobCommDomain voterConfigV2 = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycVoterDetailRequestV2.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KVOTERID_V2.toValue());

        if (null == voterConfigV2) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycVoterDetailRequestV2.getHeader().getProduct()) {
            //Build request for the connector call
            KVoterIdRequestV2 kVoterIdRequestV2 = totalKycBuilder.buildVoterDetailRequestV2(totalKycVoterDetailRequestV2);

            String url = Arrays.asList(voterConfigV2.getBaseUrl(), voterConfigV2.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            KVoterDetailResponseV2 kVoterDetailResponseV2 = (KVoterDetailResponseV2) TransportUtils.postJsonRequest(
                    kVoterIdRequestV2, url, KVoterDetailResponseV2.class);

            if (null != kVoterDetailResponseV2 && null == kVoterDetailResponseV2.getError()) {
                totalKycReqResLog.setKycId(kVoterIdRequestV2.getEpicNo());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KVOTERID_V2.name());
                totalKycReqResLog.setStatus(kVoterDetailResponseV2.getStatus());
                totalKycReqResLog.setOrgRes(kVoterDetailResponseV2.getOrgRes());
                totalKycReqResLog.setKycVersion( KVersion.KVERSION2.toValue() );
                totalKycReqResLog.setRequestId(kVoterDetailResponseV2.getRequest_id());
                totalKycReqResLog.setStatusCode(kVoterDetailResponseV2.getStatus_code());

                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kVoterDetailResponseV2);
            }else if (kVoterDetailResponseV2 != null && kVoterDetailResponseV2.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kVoterDetailResponseV2.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kVoterDetailResponseV2.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Voter details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getElecDetailV2(TotalKycElecDetailRequestV2 totalKycElecDetailRequestV2) throws Exception {

        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycElecDetailRequestV2.getHeader());
        activityLog.setAction(GNGWorkflowConstant.GET_ELEC_BILL_DETAIL_V2.toFaceValue());

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_KELEC_V2.toValue())
                .orgReq(totalKycElecDetailRequestV2.toString()).build();

        //Get the configuration
        WFJobCommDomain elecConfigV2 = workFlowCommunicationManager.getWfCommDomainJobByType(
                totalKycElecDetailRequestV2.getHeader().getInstitutionId(), UrlType.TOTAL_KYC_KELEC_V2.toValue());

        if (null == elecConfigV2) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        if (Product.CDL == totalKycElecDetailRequestV2.getHeader().getProduct()) {
            KElecRequestV2 kElecRequestV2 = totalKycBuilder.buildElecDetailRequestV2(totalKycElecDetailRequestV2);

            String url = Arrays.asList(elecConfigV2.getBaseUrl(), elecConfigV2.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

            KElectrBillResponseV2 kElectrBillResponseV2 = (KElectrBillResponseV2)
                    TransportUtils.postJsonRequest(kElecRequestV2, url, KElectrBillResponseV2.class);


            if (null != kElectrBillResponseV2 && null == kElectrBillResponseV2.getError()) {
                totalKycReqResLog.setKycId(kElecRequestV2.getConsumerId());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_KELEC_V2.name());
                totalKycReqResLog.setStatus(kElectrBillResponseV2.getStatus());
                totalKycReqResLog.setOrgRes(kElectrBillResponseV2.getOrgRes());
                totalKycReqResLog.setKycVersion(KVersion.KVERSION2.toValue());
                totalKycReqResLog.setRequestId(kElectrBillResponseV2.getRequest_id());
                totalKycReqResLog.setStatusCode(kElectrBillResponseV2.getStatus_code());
                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);

                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kElectrBillResponseV2);
            }else if (kElectrBillResponseV2 != null && kElectrBillResponseV2.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kElectrBillResponseV2.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kElectrBillResponseV2.getError().getType())
                        .build());

                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Electricity details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_PRODUCT)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.LOW.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(HttpStatus.BAD_REQUEST.name());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getGstAuthDetail(TotalKycGstAuthDetailRequest totalKycGstAuthDetailRequest) throws Exception {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycGstAuthDetailRequest.getHeader());
        activityLog.setAction( EndPointReferrer.TOTAL_KYC_GST_AUTH);

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_GSTAUTH.toValue())
                .orgReq(totalKycGstAuthDetailRequest.toString()).build();

        WFJobCommDomain gstAuthConfig = workFlowCommunicationManager.getConfigForProduct(
                totalKycGstAuthDetailRequest.getHeader().getInstitutionId(),
                UrlType.TOTAL_KYC_GSTAUTH.toValue(),
                totalKycGstAuthDetailRequest.getHeader().getProduct().toProductName());

        if (null == gstAuthConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }
        //Build request for the connector call
        KGstRequest kGstRequest = totalKycBuilder.buildGstAuthRequest(totalKycGstAuthDetailRequest);

            String url = Utility.getJoinedUrl(gstAuthConfig.getBaseUrl(), gstAuthConfig.getEndpoint());

            KGstAuthResponse kGstAuthResponse = (KGstAuthResponse) TransportUtils.postJsonRequest(
                    kGstRequest, url, KGstAuthResponse.class);

            if (null != kGstAuthResponse && null == kGstAuthResponse.getError()) {
                totalKycReqResLog.setKycId(kGstRequest.getGstin());
                totalKycReqResLog.setAction(UrlType.TOTAL_KYC_GSTAUTH.name());
                totalKycReqResLog.setStatus(kGstAuthResponse.getStatus());
                totalKycReqResLog.setOrgRes(kGstAuthResponse.getOrgRes().toString());
                totalKycReqResLog.setRequestId(kGstAuthResponse.getOrgRes().getRequestId());
                totalKycReqResLog.setStatusCode(kGstAuthResponse.getOrgRes().getStatusCode());

                externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
                activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kGstAuthResponse);
            }else if (kGstAuthResponse != null && kGstAuthResponse.getError() != null){
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(kGstAuthResponse.getError().getMessage())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(kGstAuthResponse.getError().getType())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            } else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Failed to get Gst authentication details response.")
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getGstIdentityDetails(TotalKycGstIdentyRequest totalKycGstIdentyRequest) throws Exception {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycGstIdentyRequest.getHeader());
        activityLog.setAction( EndPointReferrer.TOTAL_KYC_GST_AUTH);

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_GSTIDENTITY.toValue())
                .orgReq(totalKycGstIdentyRequest.toString()).build();

        WFJobCommDomain gstIdentityConfig = workFlowCommunicationManager.getConfigForProduct(
                totalKycGstIdentyRequest.getHeader().getInstitutionId(),
                UrlType.TOTAL_KYC_GSTIDENTITY.toValue(),
                totalKycGstIdentyRequest.getHeader().getProduct().toProductName());

        if (null == gstIdentityConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        //Build request for the connector call
        KGstIdentityRequest kGstIdentityRequest = totalKycBuilder.buildGstIdentityRequest(totalKycGstIdentyRequest);

        String url = Utility.getJoinedUrl(gstIdentityConfig.getBaseUrl(), gstIdentityConfig.getEndpoint());

        KGstIdentityResponse kGstIdentityResponse = (KGstIdentityResponse) TransportUtils.postJsonRequest(
                kGstIdentityRequest, url, KGstIdentityResponse.class);

        if (null != kGstIdentityResponse && null == kGstIdentityResponse.getError()) {
            if(kGstIdentityRequest.isGstinAvailable()) {
                totalKycReqResLog.setKycId(kGstIdentityRequest.getGstin());
            } else {
                totalKycReqResLog.setKycId(kGstIdentityRequest.getKycNumber());
            }
            totalKycReqResLog.setAction(UrlType.TOTAL_KYC_GSTIDENTITY.name());
            totalKycReqResLog.setStatus(kGstIdentityResponse.getStatus());
            totalKycReqResLog.setOrgRes(kGstIdentityResponse.getOrgRes().toString());
            totalKycReqResLog.setRequestId(kGstIdentityResponse.getOrgRes().getRequestId());
            totalKycReqResLog.setStatusCode(kGstIdentityResponse.getOrgRes().getStatusCode());

            externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
            activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kGstIdentityResponse);
        }else if (kGstIdentityResponse != null && kGstIdentityResponse.getError() != null){
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(kGstIdentityResponse.getError().getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(kGstIdentityResponse.getError().getType())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        } else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message("Failed to get Gst authentication details response.")
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    @Override
    public BaseResponse getPanAuthDetail(TotalKycPanAuthDetailRequest totalKycPanAuthDetailRequest) throws Exception {
        BaseResponse baseResponse=null;
        Collection<Error> errors=null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, totalKycPanAuthDetailRequest.getHeader());
        activityLog.setAction( EndPointReferrer.TOTAL_KYC_PAN_AUTH);

        TotalKycReqResLog totalKycReqResLog = TotalKycReqResLog.builder()
                .action(UrlType.TOTAL_KYC_PANAUTH.toValue())
                .orgReq(totalKycPanAuthDetailRequest.toString()).build();

        WFJobCommDomain panAuthConfig = workFlowCommunicationManager.getConfigForProduct(
                totalKycPanAuthDetailRequest.getHeader().getInstitutionId(),
                UrlType.TOTAL_KYC_PANAUTH.toValue(),
                totalKycPanAuthDetailRequest.getHeader().getProduct().toProductName());

        if (null == panAuthConfig) {

            errors=totalKycErrorMessage(ErrorCode.CONFIGURATION_NOT_FOUND,Error.ERROR_TYPE.SYSTEM.toCode(),Error.ERROR_TYPE.SYSTEM.toValue(),Error.SEVERITY.CRITICAL.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.CONFIGURATION_NOT_FOUND.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }
        //Build request for the connector call
        KPanRequest kPanRequest = totalKycBuilder.buildPanAuthRequest(totalKycPanAuthDetailRequest);

        String url = Utility.getJoinedUrl(panAuthConfig.getBaseUrl(), panAuthConfig.getEndpoint());

        KPanAuthResponse kPanAuthResponse = (KPanAuthResponse) TransportUtils.postJsonRequest(
                kPanRequest, url, KPanAuthResponse.class);

        if ( kPanAuthResponse != null ) {
            if( null == kPanAuthResponse.getError()) {
            totalKycReqResLog.setKycId(kPanRequest.getPan());
            totalKycReqResLog.setAction(UrlType.TOTAL_KYC_PANAUTH.name());
            totalKycReqResLog.setStatus(kPanAuthResponse.getStatus());
            totalKycReqResLog.setOrgRes(kPanAuthResponse.getOrgRes().toString());
            totalKycReqResLog.setRequestId(kPanAuthResponse.getOrgRes().getRequestId());
            totalKycReqResLog.setStatusCode(kPanAuthResponse.getOrgRes().getStatusCode());

            externalAPILogRepository.saveTotalKycReqResLog(totalKycReqResLog);
            activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, kPanAuthResponse);
        }else {
                   errors=totalKycErrorMessage(kPanAuthResponse.getError().getMessage(),Error.ERROR_TYPE.SYSTEM.toCode(),Error.ERROR_TYPE.SYSTEM.toValue(),Error.SEVERITY.CRITICAL.name());
                  activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                 activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
                 baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }
        }else{
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message("Failed to get Pan authentication details response.")
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR.name());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
             }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    public Collection totalKycErrorMessage(String message,int errorCode,String errorType,String errorLevel) {
        Collection<Error> err=null;
        err = new ArrayList<>();
        err.add(Error.builder()
                .message(message)
                .errorCode(String.valueOf(errorCode))
                .errorType(errorType)
                .level(errorLevel)
                .build());
       return err;
    }

    /*public void KARZA(){
        //TODO chk mandetory parasm for kycTtype.
//        validateRequestFor(applicationReq, kycType)
//        Map result = connectKarza();
        // TODO check whether success

        // proceed if success
        populateApplication(applicationReq, result.get(), kyc);

    }
    connectKarza() {

        *//*  pickUp field from appRequest and populate request for connector with licenseKey

        * *//*
    }
*/

    @Override
    public BaseResponse verifyKYC(KycVerificationRequest kycVerificationRequest) throws Exception {
        BaseResponse baseResponse = null;
        Collection<Error> errors = null;
        //getting config from db.
        String institutionId = kycVerificationRequest.getHeader().getInstitutionId();
        String product = kycVerificationRequest.getHeader().getProduct().toProductName();
        String refId = kycVerificationRequest.getRefId();
        WFJobCommDomain kycConfig = getConfiguration(institutionId,  UrlType.TOTAL_KYC.toValue(), product );
        if( kycConfig != null) {
            if (StringUtils.equalsIgnoreCase(kycConfig.getServiceId(), KYC_SERVICE_KARZA)) {
                // overloaded following method
                KarzaRequest karzaRequest = buildKycRequest(kycVerificationRequest, kycConfig);
                RawBaseResponse rawBaseResponse = getKycDetail(karzaRequest,kycConfig);

                if(rawBaseResponse.getError() == null && rawBaseResponse.getKarzaResponse() !=null ){
                    String karzaStatusCode = rawBaseResponse.getKarzaResponse().getStatusCode();
                    if( StringUtils.endsWithIgnoreCase(karzaStatusCode, KarzaHelper.KARZA_STATUS_101) ) {
                        // setting verified flag to true and other fields
                KycVerificationResponse kycVerificationResponse = KycVerificationResponse.builder()
                                                                        .refId(kycVerificationRequest.getRefId())
                                                                        .kycNumber(kycVerificationRequest.getKycNumber())
                                                                        .kycType(kycVerificationRequest.getKycType())
                                                                        .verified(true).build();
                      //set response in db.
//                         setKycStatus(refId,institutionId, kycVerificationRequest.getKycType());
                        // setting to kycVerificationResponse the base response
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,kycVerificationResponse);
                    } else {
                        logger.info("{} Karza response for {} is {} : {} ", kycVerificationRequest.getRefId(),
                                kycVerificationRequest.getKycType(),
                                karzaStatusCode, KarzaHelper.KARZA_STATUS_MESSAGES.get(karzaStatusCode));
                        //// not setting flag and setting other fields and returning.
                        KycVerificationResponse kycVerificationResponse = KycVerificationResponse.builder()
                                .refId(kycVerificationRequest.getRefId())
                                .kycNumber(kycVerificationRequest.getKycNumber())
                                .kycType(kycVerificationRequest.getKycType())
                                .verified(false).build();
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,kycVerificationResponse);
                    }
                } else {
                    logger.info("Error occured :{}", rawBaseResponse.getError());
                    ErrorDesc errorDesc = rawBaseResponse.getError();
                    errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(errorDesc.getMessage())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(errorDesc.getType())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_ERROR,errors);
                }
            }
            else {
                errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
            return  baseResponse;
        }else {
              logger.info("KYC Config not found for InstituionId {}, product {}", institutionId,
                                                            kycVerificationRequest.getHeader().getProduct());

            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }
        return baseResponse;
    }

    private void setKycStatus(String refId, String institutionId, String kycType) throws Exception {

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationMongoRepository.getGoNoGoCustomerApplicationByRefId(refId);

        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

        if(StringUtils.equalsIgnoreCase(KarzaHelper.PAN,kycType)){
            List<Kyc> kycList = applicationRequest.getRequest().getApplicant().getKyc();
            kycList.forEach(obj -> {
               if(obj.getKycName().equalsIgnoreCase(KarzaHelper.PAN))  {
                   obj.setKycStatus(true);
               }
            });
        } else if(StringUtils.equalsIgnoreCase(KarzaHelper.GST,kycType)){
            Applicant applicant =  applicationRequest.getRequest().getApplicant();
            applicant.setGstStatus(true);
        }
        applicationMongoRepository.saveApplication(applicationRequest);
    }

    private KarzaRequest buildKycRequest(KycVerificationRequest kycVerificationRequest, WFJobCommDomain kycConfig) throws Exception{
        String requestJson = null;
        KarzaRequest karzaRequest = null;
        if(kycVerificationRequest.getKycType() != null){
            if(kycVerificationRequest.getKycNumber() != null){

                switch (kycVerificationRequest.getKycType()) {
                    case KarzaHelper.GST :
                    case KarzaHelper.GSTAUTH :
                        KGstRequest kGstRequest = KGstRequest.builder().gstin(kycVerificationRequest.getKycNumber()).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kGstRequest);
                        logger.info("Request Json for GST or GSTAUTH :{}", requestJson);
                        break;

                    case KarzaHelper.PAN :
                        KPanRequest kPanRequest = KPanRequest.builder().pan(kycVerificationRequest.getKycNumber()).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kPanRequest);
                        logger.info("Request Json for PAN :{}", requestJson);
                        break;
                    default:
                        throw new GoNoGoException("kycType " + kycVerificationRequest.getKycType() + " is not configured in the system");

                }
                karzaRequest = KarzaRequest.builder()
                        .kycRequest(requestJson)
                        .karzaKey(kycConfig.getLicenseKey())
                        .kycType(kycVerificationRequest.getKycType())
                        .build();
            } else{
                logger.info("KycNumber is null");
                throw new GoNoGoException("Please specify kycNumber");
            }
        } else{
            logger.info("KycType is null");
            throw new GoNoGoException("Please specify kycType");
        }

        return karzaRequest;

    }

    @Override
    public BaseResponse getKycDetailByKarza(ApplicationRequest applicationRequest, String kycType) throws Exception {
        BaseResponse baseResponse = null;
        Collection<Error> errors = null;

        //getting config from db.
        WFJobCommDomain karzaConfig = workFlowCommunicationManager.getConfigForProduct(
                applicationRequest.getHeader().getInstitutionId(),
                UrlType.KARZA.toValue(),
                applicationRequest.getHeader().getProduct().toProductName());
        if(karzaConfig != null){
            //building requestJson for Connector
            // if the kyc type is empty then call all APIs
            // as per applicant type - INDIVIDUAl / Non-Individual
            if( kycType == null ){
                List<String> kycSequence = KarzaHelper.nonIndividualKYCSeq;
                if( GngUtils.isIndividual(applicationRequest.getRequest().getApplicant() ) ) {
                    kycSequence = KarzaHelper.individualKYCSeq;
                }
                String name = null;
                for( String kyc : kycSequence ){
                    populateFromKarza(applicationRequest, kyc, karzaConfig) ;
                    if( StringUtils.endsWithIgnoreCase(kyc, KarzaHelper.PAN )){
                        name = applicationRequest.getRequest().getApplicant().getApplicantName().getFirstName();
                    }
                }
                if( StringUtils.isNotEmpty(name)){
                    applicationRequest.getRequest().getApplicant().getApplicantName().setFirstName(name);
                }
            } else {
                populateFromKarza(applicationRequest, kycType, karzaConfig) ;
            }
        } else {
            logger.info("Config not found for InstituionId {}, product {}",
                    applicationRequest.getHeader().getInstitutionId(), applicationRequest.getHeader().getProduct().toProductName());
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,applicationRequest);
        return baseResponse;
    }

    private void populateFromKarza(ApplicationRequest applicationRequest, String kycType, WFJobCommDomain karzaConfig)
            throws Exception  {
        Collection<Error> errors = null;
        KarzaRequest karzaRequest = buildKycRequest(applicationRequest, kycType, karzaConfig);
        RawBaseResponse rawBaseResponse = getKycDetail(karzaRequest,karzaConfig);

        if(rawBaseResponse.getError() == null && rawBaseResponse.getKarzaResponse() !=null ){
            String karzaStatusCode = rawBaseResponse.getKarzaResponse().getStatusCode();
            if( StringUtils.endsWithIgnoreCase(karzaStatusCode, KarzaHelper.KARZA_STATUS_101) ) {
                populateApplication(rawBaseResponse, kycType, applicationRequest);
            } else {
                logger.info("{} Karza response for {} is {} : {} ", applicationRequest.getRefID(), kycType,
                        karzaStatusCode, KarzaHelper.KARZA_STATUS_MESSAGES.get(karzaStatusCode));
            }
        } else {
            logger.info("Error occured :{}", rawBaseResponse.getError());
            ErrorDesc errorDesc = rawBaseResponse.getError();
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(errorDesc.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(errorDesc.getType())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            BaseResponse baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_ERROR,errors);
        }
    }

    private ApplicationRequest populateApplication(RawBaseResponse rawBaseResponse, String kycType, ApplicationRequest applicationRequest) throws Exception{
        KarzaResponse karzaResponse = rawBaseResponse.getKarzaResponse();

        String name;
        switch (kycType){
            case KarzaHelper.PAN :
                KPanAuthResult kPanAuthResult =
                        JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(), KPanAuthResult.class);
                name = kPanAuthResult.getName();
                applicationRequest = fillApplicantName(name,applicationRequest);
                break;
            case KarzaHelper.VOTERID :
                KVoterIdResult kVoterIdResult = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KVoterIdResult.class);
                fillApplicationData(kVoterIdResult,applicationRequest);
                break;

            case KarzaHelper.DRIVINGLICENSE :
                // TODO
                KDrivLicResult kDrivLicResult = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KDrivLicResult.class);
                fillApplicationData(kDrivLicResult,applicationRequest);
                break;

            case KarzaHelper.GST :
                String jsonStr = JsonUtil.ObjectToString(karzaResponse.getResult());
                List<KGstIdentityResult> kGstIdentityResult = mapper.readValue(jsonStr, new TypeReference<List<KGstIdentityResult>>(){});
                fillApplicationData(kGstIdentityResult,applicationRequest);
                break;
            case KarzaHelper.GSTAUTH :
                KGstAuthResult kGstAuthResult = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KGstAuthResult.class);
                fillApplicationData(kGstAuthResult,applicationRequest);
                break;
            case KarzaHelper.TAN :
                KTanDetailResult kTanDetailResult = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KTanDetailResult.class);
                fillApplicationData(kTanDetailResult,applicationRequest);
                break;
            case KarzaHelper.TANAUTH :
                KTanAuthResult kTanAuthResult = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KTanAuthResult.class);
                name = kTanAuthResult.getName();
                fillApplicantName(name,applicationRequest);
                break;
             //TODO passport
            case KarzaHelper.PASSPORT :
                KVoterIdResult result = JsonUtil.MapToObject((Map<String, Object>)karzaResponse.getResult(),
                        KVoterIdResult.class);
                break;
            default: throw new GoNoGoException("No matching kycTYpe");
        }
        return applicationRequest;
    }

    private void fillApplicationData(KDrivLicResult kDrivLicResult, ApplicationRequest applicationRequest) {
        fillApplicantName(kDrivLicResult.getName(), applicationRequest);
        String dob = kDrivLicResult.getDob().replace("-","") ;
        fillApplicantDob( dob, applicationRequest);

        //settiing adress fields.
        CustomerAddress address = new CustomerAddress();
        address.setAddressLine1(kDrivLicResult.getAddress());

        Applicant applicant = applicationRequest.getRequest().getApplicant();
        if( CollectionUtils.isNotEmpty(applicant.getAddress())){
            // get permanent address
            List<CustomerAddress> addrList =  applicant.getAddress()
                    .stream()
                    .filter(addr -> StringUtils.endsWithIgnoreCase(addr.getAddressType(),
                            GNGWorkflowConstant.PERMANENT.toFaceValue() ))
                    .collect(Collectors.toList());
            if( CollectionUtils.isNotEmpty(addrList)){
                address = addrList.get(0);
                address.setAddressLine1(kDrivLicResult.getAddress());
            }else {
                applicationRequest.getRequest().getApplicant().getAddress().add(address);
            }
        } else {
            List<CustomerAddress> addrList = new ArrayList<>();
            addrList.add(address);
            applicationRequest.getRequest().getApplicant().setAddress(addrList);
        }
    }

    private void fillApplicantDob(String dob, ApplicationRequest applicationRequest) {
        applicationRequest.getRequest().getApplicant().setDateOfBirth(dob);
    }

    private void fillApplicationData(List<KGstIdentityResult> kGstIdentityResults, ApplicationRequest applicationRequest) {
        if( CollectionUtils.isNotEmpty(kGstIdentityResults) ) {
            Applicant applicant = applicationRequest.getRequest().getApplicant();
            // Filter on the basis of GST no. provided
            // and set email id, mobile and company name in the form data.

            List<KGstIdentityResult> gstInfoList = kGstIdentityResults
                                                .stream()
                                                .filter(result -> StringUtils.endsWithIgnoreCase(result.getGstinId(),
                                                        applicant.getGstNo()))
                                                .collect(Collectors.toList());
            if( CollectionUtils.isNotEmpty(gstInfoList)){
                KGstIdentityResult gstInfo = gstInfoList.get(0);
                // Set registration name as first name
                applicant.getApplicantName().setFirstName(gstInfo.getRegistrationName());
                //  Set OFFICE_MOBILE mobile
                if( StringUtils.isNotEmpty(gstInfo.getMobNum())) {
                    setPhone(applicant, GNGWorkflowConstant.OFFICE_MOBILE.toFaceValue(), gstInfo.getMobNum());
                }
                // Set email to office email
                if( StringUtils.isNotEmpty(gstInfo.getEmailId())) {
                    setEmail(applicant, GNGWorkflowConstant.OFFICE_EMAIL.toFaceValue(), gstInfo.getEmailId());
                }
            }

        }
    }

    private void fillApplicationData(KGstAuthResult kGstAuthResult, ApplicationRequest applicationRequest) {
        // TODO

    }
    private void fillApplicationData(KTanDetailResult kTanDetailResult, ApplicationRequest applicationRequest) {
        // TODO
    }

    private void setPhone(Applicant applicant, String phoneType, String mobNum) {
        Phone phone = new Phone();
        phone.setPhoneType(phoneType);

        if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
            List<Phone> phoneList = applicant.getPhone()
                    .stream()
                    .filter(p -> StringUtils.endsWithIgnoreCase(p.getPhoneType(), phoneType))
                    .collect(Collectors.toList());

            if (CollectionUtils.isEmpty(phoneList)) {
                applicant.getPhone().add(phone);
            } else {
                phone = phoneList.get(0);
            }
        } else {
            // Add to applicant phone
            List<Phone> phoneList = new ArrayList<>();
            applicant.setPhone(phoneList);
            phoneList.add(phone);
        }
        phone.setPhoneNumber(mobNum);
    }

    private void setEmail(Applicant applicant, String emailType, String emailId){
        Email email = new Email();
        email.setEmailType(emailType);

        if (CollectionUtils.isNotEmpty(applicant.getEmail())) {
            List<Email> emailList = applicant.getEmail()
                    .stream()
                    .filter(e -> StringUtils.endsWithIgnoreCase(e.getEmailType(),emailType))
                    .collect(Collectors.toList());

            if (CollectionUtils.isEmpty(emailList)) {
                applicant.getEmail().add(email);
            } else {
                email = emailList.get(0);
            }
        } else {
            // Add to applicant email
            List<Email> emailList = new ArrayList<>();
            applicant.setEmail(emailList);
            emailList.add(email);
        }
        email.setEmailAddress(emailId);
    }
    //setting filed from VOTERID result.
    private void fillApplicationData(KVoterIdResult kVoterIdResult,ApplicationRequest applicationRequest) {

        String name = kVoterIdResult.getName();
        String state = kVoterIdResult.getState();
        String dist = kVoterIdResult.getDistrict();
        String gender = kVoterIdResult.getGender();
        String house_no =kVoterIdResult.getHouseNo();

        Applicant applicant = applicationRequest.getRequest().getApplicant();
        fillApplicantName(name, applicationRequest);
        applicant.setGender(gender);

        //settiing adress fields.
        CustomerAddress address = new CustomerAddress();
        address.setDistrict(dist);
        address.setState(state);
        address.setFlatNo(house_no);

        if( CollectionUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAddress())){
            // get permanent address
            List<CustomerAddress> addrList =  applicant.getAddress()
                    .stream()
                    .filter(addr -> StringUtils.endsWithIgnoreCase(addr.getAddressType(),
                            GNGWorkflowConstant.PERMANENT.toFaceValue() ))
                    .collect(Collectors.toList());
            if( CollectionUtils.isNotEmpty(addrList)){
                address = addrList.get(0);
                address.setDistrict(dist);
                address.setState(state);
                address.setFlatNo(house_no);
            }else {
                applicationRequest.getRequest().getApplicant().getAddress().add(address);
            }
        } else {
            List<CustomerAddress> addrList = new ArrayList<>();
            addrList.add(address);
            applicationRequest.getRequest().getApplicant().setAddress(addrList);
        }
    }

    private ApplicationRequest setAddress(CustomerAddress address, ApplicationRequest applicationRequest) {
        if (address != null){
            List<CustomerAddress> addresses = new ArrayList<>();
            addresses.add(address);
            applicationRequest.getRequest().getApplicant().setAddress(addresses);
        }
        return applicationRequest;
    }


    // Common method for API's that returns only name in result
    private ApplicationRequest fillApplicantName(String name, ApplicationRequest applicationRequest) {

        if(StringUtils.isNotBlank(name)){
        applicationRequest.getRequest().getApplicant().getApplicantName().setFirstName(name);
        }
        return applicationRequest;
    }

    private RawBaseResponse getKycDetail(KarzaRequest karzaRequest,WFJobCommDomain karzaConfig) {

        RawBaseResponse rawBaseResponse = null;
        String url = Utility.getJoinedUrl(karzaConfig.getBaseUrl(), karzaConfig.getEndpoint());
        try {
           //call to connector.
           RawResponse rawResponse= (RawResponse) TransportUtils.postJsonRequest( karzaRequest,url,RawResponse.class);
            logger.info("RawResponse from connector : {}",rawBaseResponse);
               if (rawResponse!=null && null != rawResponse.getResponse()) {
                   Map<Integer,String> responseBody = rawResponse.getResponse();
                   logger.info("responsebody = " + responseBody);
                   if (responseBody.containsKey(HttpStatus.OK.value())) {
                       String originalResString = responseBody.get(HttpStatus.OK.value());
                       KarzaResponse karzaResponse = JsonUtil.StringToObject(originalResString,KarzaResponse.class);
                       rawBaseResponse = RawBaseResponse.builder()
                               .status(HttpStatus.OK.name())
                               .karzaResponse(karzaResponse)
                               .build();
                   } else{
                       rawBaseResponse = RawBaseResponse.builder()
                                        .status(CustomHttpStatus.THIRD_PARTY_ERROR.name())
                                        .error(GngUtils.getOtherResponse(responseBody))
                                        .build();
                   }
               } else {
                   rawBaseResponse = RawBaseResponse.builder()
                           .status(GNGWorkflowConstant.FAILED.toFaceValue())
                           .error(ErrorDesc.builder()
                                   .type(Error.ERROR_TYPE.SYSTEM.toValue())
                                   .message("Failed to get Kyc Details").build())
                           .build();
               }
        } catch (Exception e){
            logger.info(e.getMessage());
            logger.error("{}",e.getStackTrace());
        }
    return rawBaseResponse;
    }

    private KarzaRequest buildKycRequest(ApplicationRequest applicationRequest, String kycType, WFJobCommDomain karzaConfig) throws Exception {
        KarzaRequest karzaRequest = null;
        String requestJson = null;
        String kycNumber = null;

        if (karzaConfig != null) {
            if (applicationRequest != null && applicationRequest.getRequest() != null
                    && applicationRequest.getRequest().getApplicant() != null) {
                ApplicationRequest appRequest = applicationRequest;
                Request request = appRequest.getRequest();
                Applicant applicant = request.getApplicant();
                String dob = applicant.getDateOfBirth();
                // GST, TAN, TIN are not KYC types ; take from specific fields by checking @Param kycType passed
                if (! KYC_TYPES.isKYCType(kycType) ) {
                    switch (kycType) {
                        case KarzaHelper.GST:
                        case KarzaHelper.GSTAUTH:
                            kycNumber = applicant.getGstNo(); break;
                        case KarzaHelper.TAN:
                        case KarzaHelper.TANAUTH:
                            kycNumber = applicant.getTanNo(); break;
                        default : break;
                    }
                } else {
                    List<Kyc> applicantKycList = applicant.getKyc();
                    List<Kyc> kycList = applicantKycList
                            .stream()
                            .filter(kyc -> kycType.equals(kyc.getKycName()) && kyc.getKycNumber() != null)
                            .collect(Collectors.toList());

                    if (CollectionUtils.isNotEmpty(kycList)) {
                        //build kycType Specific request.
                        kycNumber = kycList.get(0).getKycNumber();
                    }
                }
                if (kycNumber == null) {
                    logger.info("kyc number not found ");
                    throw new GoNoGoException("kyc number not found");
                }
                // Set consent as 'y' always as currently we are not capturing on UI
                switch (kycType) {
                    case KarzaHelper.PAN:
                        KPanRequest kPanRequest = KPanRequest.builder().pan(kycNumber).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kPanRequest);
                        logger.info("Request Json for PAN :{}", requestJson);
                        break;
                    case KarzaHelper.GST:
                    case KarzaHelper.GSTAUTH:
                        KGstRequest kGstRequest = KGstRequest.builder().gstin(kycNumber).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kGstRequest);
                        logger.info("Request Json for GST or GSTAUTH :{}", requestJson);
                        break;
                    case KarzaHelper.TAN:
                    case KarzaHelper.TANAUTH:
                        KTanRequest kTanRequest = KTanRequest.builder().tan(kycNumber).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kTanRequest);
                        logger.info("Request Json for TAN or TANAUTH :{}", requestJson);
                        break;
                    case KarzaHelper.VOTERID:
                        KVoterIdRequestV2 kVoterIdRequest = KVoterIdRequestV2.builder().epicNo(kycNumber).consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kVoterIdRequest);
                        logger.info("Request Json for VOTERID :{}", requestJson);
                        break;
                    //DOB is required for DL verification.
                    case KarzaHelper.DRIVINGLICENSE:
                        // DOB to be sent in pattern="dd-MM-yyyy"
                        KDrivLicRequestV2 kDrivLicRequest = KDrivLicRequestV2.builder()
                                .dlNo(kycNumber)
                                .dob(GngDateUtil.getDdMmYyyy(GngDateUtil.getDate(dob)))
                                .consent(KConsentEnum.y).build();
                        requestJson = JsonUtil.ObjectToString(kDrivLicRequest);
                        logger.info("Request Json for Driving License :{}", requestJson);
                        break;
                    default:
                        throw new GoNoGoException("kycType " + kycType + " is not configured in the system");
                }
                karzaRequest = KarzaRequest.builder()
                        .kycType(kycType)
                        .kycRequest(requestJson)
                        .karzaKey(karzaConfig.getLicenseKey()).build();
            } else {
                logger.info("ApplicationRequest is empty");
                throw new GoNoGoException("Precondition failed");
            }

        } else {
            logger.info("Karza Config not found "  );
            throw new GoNoGoException("Configuration not found");
        }
        return karzaRequest;
    }

    private WFJobCommDomain getConfiguration(String institutionId, String urlType, String toProductName) throws GoNoGoException{
        return workFlowCommunicationManager.getConfigForProduct(institutionId, urlType, toProductName);

    }
}
