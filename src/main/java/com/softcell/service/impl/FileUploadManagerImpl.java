package com.softcell.service.impl;


import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.softcell.constants.*;
import com.softcell.constants.csv.CSVHeaderConstant;
import com.softcell.constants.error.ErrorCode;
import com.softcell.constants.master.*;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.FileUploadRepository;
import com.softcell.dao.mongodb.repository.digitization.DigitizationRepository;
import com.softcell.dao.mongodb.repository.security.SecurityRepository;
import com.softcell.gonogo.model.configuration.VersionMaster;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.MasterActivityDetails;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.MasterRecords;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.service.FileUploadManager;
import com.softcell.service.GenericUpload;
import com.softcell.service.parser.MasterFileCSVParser;
import com.softcell.utils.GngUtils;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.io.*;
import java.nio.file.Files;
import java.util.*;

import static com.softcell.constants.Constant.NON_TRANSACTIONAL;
import static com.softcell.constants.Constant.TRANSACTIONAL;
import static com.softcell.constants.error.ErrorCode.CSV_FILE_VALID_ERROR_MSG;


/**
 * @author yogeshb
 */
@Service
public class FileUploadManagerImpl implements FileUploadManager {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadManagerImpl.class);

    private final String SUPPORTED_FILE_TYPE = "text/csv";

    private final String SUPPORTED_XSL_TYPE = "application/vnd.ms-excel";

    private static String DEFAULT_BLOCKED_FROMTIME="00:30:00";
    private static String DEFAULT_BLOCKED_TOTIME="03:00:00";

    @Autowired
    private FileUploadRepository fileUploadRepository;

    @Autowired
    private SecurityRepository securityRepository;

    @Autowired
    private MasterFileCSVParser masterFileCSVParser;

    @Autowired
    private DigitizationRepository digitizationRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private GenericUpload genericUpload;

    @Override
    public BaseResponse uploadCommonGeneralParameterMaster(File uploadedFile, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.COMMON_GENERAL_MASTER_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseGeneralParameterMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CommonGeneralParameterMaster> commonGeneralParameterMasterList = (List<CommonGeneralParameterMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository
                            .insertCommonGeneralMaster(commonGeneralParameterMasterList);
                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadSchemesMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;
            try {

                HashMap columnMapping = new HashMap();
                for (SchemeMasterFields field : SchemeMasterFields.values()) {
                    columnMapping.put(field.name(), field.getStringValue());
                }
                List<SchemeMasterData> schemeMasterList =
                        transformCsvToList(file, columnMapping, SchemeMasterData.class,fileUploadStatus);
                //insert institution id and date against every record
                if(!CollectionUtils.isEmpty(schemeMasterList)) {
                    schemeMasterList.forEach(scheme -> {
                        scheme.setInstitutionId(institutionId);
                        scheme.setInsertDate(new Date());
                    });
                    boolean dbStatus = fileUploadRepository.insertSchemeMaster(schemeMasterList);
                    masterRecords = new MasterRecords();
                    masterRecords.setMasterList(schemeMasterList);

                    fileUploadStatus = setRecordStatus(dbStatus, masterRecords, fileUploadStatus);
                }
            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    private FileUploadStatus setRecordStatus(boolean dbStatus, MasterRecords masterRecords, FileUploadStatus fileUploadStatus) {
       // FileUploadStatus fileUploadStatus=new FileUploadStatus();
        if(dbStatus==true){
            fileUploadStatus.setStatus(Constant.SUCCESS);
            fileUploadStatus.setNumberOfRecords(masterRecords.getMasterList().size());
            fileUploadStatus.setNumberOfRecordSuccess(masterRecords.getMasterList().size());


            masterRecords.setFileUploadStatus(fileUploadStatus);
        }else{
            fileUploadStatus.setStatus(Status.ERROR.name());
            fileUploadStatus.setErrorDesc(SystemErrorConstant.CURRUPT_RECORD);
            masterRecords.setFileUploadStatus(fileUploadStatus);
        }
        if (masterRecords != null
                && masterRecords.getMasterList() != null
                && !masterRecords.getMasterList().isEmpty()
                && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
            fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);}
        else {
            fileUploadStatus = masterRecords.getFileUploadStatus();
        }
        return fileUploadStatus;
    }

    private List transformCsvToList(File file, HashMap columnMapping, Class classname,FileUploadStatus fileUploadStatus) throws IOException {
        CsvToBean csv = new CsvToBean();
        CSVReader headerReader = new CSVReader(new FileReader(file));
        CSVReader fileReader = new CSVReader(new FileReader(file));
        String[] header=headerReader.readNext();
        List headerlist=Arrays.asList(header);
        Object[] key=columnMapping.keySet().toArray();
        List keyList=Arrays.asList(key);

        List<String>matchFields=new ArrayList<String>(keyList);
        List<String> aditionalFields=new ArrayList<String>(headerlist);
        matchFields.retainAll(headerlist);
        aditionalFields.addAll(keyList);
        aditionalFields.removeAll(keyList);
        List<String>missingFields=new ArrayList<String>(keyList);
        missingFields.addAll( matchFields );
        missingFields.removeAll( matchFields );

        fileUploadStatus.setAdditionalFields(String.valueOf(aditionalFields));
        logger.debug("matching fields in coloum and csv file"+matchFields);
        logger.debug("additional fields is present in csv file"+aditionalFields);
        logger.debug("missing fields in csv file which are present in bean file"+missingFields);
        if (!CollectionUtils.isEmpty(matchFields)){
            HeaderColumnNameTranslateMappingStrategy strategy = new HeaderColumnNameTranslateMappingStrategy();
            strategy.setType(classname);
            strategy.setColumnMapping(columnMapping);
            //Parse the file
            return csv.parse(strategy, fileReader);}
        else{
            fileUploadStatus.setStatus(Constant.FAILED);
            return null;
        }

    }

    @Override
    public BaseResponse uploadReferenceDetailsMaster(File file, String institutionId, String product) throws IOException {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.REFERENCE_DETAILS_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(file)), format);

                masterRecords = masterFileCSVParser.parseReferenceDetailsMaster(parser, institutionId, product);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<ReferenceDetailsMaster> referenceDetailsMasters = (List<ReferenceDetailsMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertReferenceDetailsMasters(referenceDetailsMasters);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    logger.info("Wrong csv file uploaded, please upload valid csv file");
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("Error in uploadnig scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    @Override
    public BaseResponse uploadBankingMaster(File file, String institutionId) throws IOException {
        FileUploadStatus fileUploadStatus;
        MasterRecords masterRecords;
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_XSL_TYPE, Files.probeContentType(file.toPath()))) {
            InputStream targetStream = new FileInputStream(file);
            POIFSFileSystem fs = new POIFSFileSystem(targetStream);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            masterRecords = masterFileCSVParser.parseBankingMaster(wb, institutionId);
            fileUploadStatus = getBankMasterUploadStatus(masterRecords);

        } else if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            CSVFormat format = CSVFormat.RFC4180.withHeader(
                    CSVHeaderConstant.RBI_BANK_DETAILS_MASTER_HEADER).withSkipHeaderRecord();
            CSVParser parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(file)), format);
            masterRecords = masterFileCSVParser.parseBankDetailsCsvMaster(parser, institutionId);
            fileUploadStatus = getBankMasterUploadStatus(masterRecords);
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    private FileUploadStatus getBankMasterUploadStatus(MasterRecords masterRecords) {

        FileUploadStatus fileUploadStatus;

        if (masterRecords != null
                && masterRecords.getMasterList() != null
                && !masterRecords.getMasterList().isEmpty()
                && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

            @SuppressWarnings("unchecked")
            List<BankDetailsMaster> bankingDetailsMasterList = (List<BankDetailsMaster>) masterRecords
                    .getMasterList();
            boolean dbStatus = fileUploadRepository
                    .insertBankingDetailsMasterList(bankingDetailsMasterList);

            fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

        } else {
            logger.info("Wrong csv file uploaded, please upload valid csv file");
            fileUploadStatus = masterRecords.getFileUploadStatus();
        }
        return fileUploadStatus;
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadAssetModelMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {

                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.ASSET_MODEL_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        new FileInputStream(file)), format);


                masterRecords = masterFileCSVParser
                        .parseAssetModelMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<AssetModelMaster> assetModelMasterList = (List<AssetModelMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertAssetModelMasterList(assetModelMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    logger.info("Wrong csv file uploaded, please upload valid csv file");
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("Error in uploadnig scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadPincodeMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.PIN_CODE_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);


                masterRecords = masterFileCSVParser.parsePinCodeMaster(parser,
                        institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<PinCodeMaster> pinCodeMasterList = (List<PinCodeMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertPinCodeMasterList(pinCodeMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    logger.error("Wrong csv file uploaded, please upload valid csv file");
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadEmployerMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                /**
                 * Create the CSVFormat object
                 */
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.EMPLOYER_MASTER_HEADER)
                        .withSkipHeaderRecord();

                /**
                 * initialize the CSVParser object
                 */
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);


                masterRecords = masterFileCSVParser.parseEmployerMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<EmployerMaster> employerMasterList = (List<EmployerMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertEmployerMasterList(employerMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadDealerEmailMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DEALER_EMAIL_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseDealerEmailMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<DealerEmailMaster> dealerEmailMasterList = (List<DealerEmailMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertDealerEmailMasterList(dealerEmailMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadGNGDealerEmailMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {

                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.GNG_DEALER_EMAIL_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseGNGDealerEmailMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<GNGDealerEmailMaster> gngDealerEmailMasterList = (List<GNGDealerEmailMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertGNGDealerEmailMasterList(gngDealerEmailMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadSchemeModelDealerCityMappingMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180
                        .withHeader(
                                CSVHeaderConstant.SCHEME_MODEL_DEALER_CITY_MAPPING_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser
                        .parseSchemeModelDealerCityMapping(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<SchemeModelDealerCityMapping> schemeModelDealerCityMappingMasterList = (List<SchemeModelDealerCityMapping>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertSchemeModelDealerCityMappingMasterList(schemeModelDealerCityMappingMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadSchemeDateMappingMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_DATE_MAPPING_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser
                        .parseSchemeDateMapping(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<SchemeDateMapping> schemeDateMappingList = (List<SchemeDateMapping>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertSchemeDateMappingMasterList(schemeDateMappingList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }


    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadCarSurrogateMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.CAR_SURROGATE_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseCarSurrogateMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CarSurrogateMaster> carSurrogateMasterList = (List<CarSurrogateMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertCarSurrogateMasterList(carSurrogateMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadCityStateMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.CITY_STATE_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                // masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseCityStateMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CityStateMappingMaster> cityStateMappingMasterList = (List<CityStateMappingMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertCityStateMasterList(cityStateMappingMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadPlPincodeEmailMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.PL_PINCODE_EMAIL_MASTER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parsePlPincodeEmailMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<PlPincodeEmailMaster> plPincodeEmailMasterList = (List<PlPincodeEmailMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertPlPincodeEmailMasterList(plPincodeEmailMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file          to be saved or uploaded.
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadBankDetailsMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.BANK_DETAILS_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseBankDetailsMaster(
                        parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<BankDetailsMaster> bankDetailsMasterList = (List<BankDetailsMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertBankDetailsMasterList(bankDetailsMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file          to be saved or uploaded.
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadHierarchyMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.HIERARCHY_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                // masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseHierarchyMaster(
                        parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<HierarchyMaster> hierarchyMasterList = (List<HierarchyMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertHierarchyMasterList(hierarchyMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file          to be saved or uploaded.
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadCDLHierarchyMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DEALER_BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseCDLHierarchyMaster(
                        parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CDLHierarchyMaster> cdlHierarchyMasterList = (List<CDLHierarchyMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertCDLHierarchyMasterList(cdlHierarchyMasterList);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     * @throws Exception
     */
    @Override
    public BaseResponse uploadBranchMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseBranchMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<BranchMaster> branchMasterList = (List<BranchMaster>) masterRecords.getMasterList();

                    boolean dbStatus = fileUploadRepository.insertBranchMasterList(branchMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }


    /**
     * @param file
     * @return
     */
    @Override
    public BaseResponse uploadVersionMaster(File file) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {

                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.VERSION_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser
                        .parseVersionCofigurationMaster(parser);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<VersionMaster> versionMasterList = (List<VersionMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = doDatabaseOperations(versionMasterList, masterRecords);
                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param versionMasterList
     * @param masterRecords
     * @return
     */

    private boolean doDatabaseOperations(List<VersionMaster> versionMasterList, MasterRecords masterRecords) {
        try {
            int addOrUpdateCount = 0;
            int removeCount = 0;
            int failedCount = 0;
            for (VersionMaster versionMaster : versionMasterList) {
                if (StringUtils.equalsIgnoreCase("AN",
                        versionMaster.getAction())) {
                    if (securityRepository.addAllowedVersion(
                            versionMaster.getInstition(),
                            versionMaster.getVersion())) {
                        addOrUpdateCount++;
                    } else {
                        failedCount++;
                    }

                } else if (StringUtils.equalsIgnoreCase("RM",
                        versionMaster.getAction())) {
                    if (securityRepository.removeOldVersion(
                            versionMaster.getInstition(),
                            versionMaster.getVersion())) {
                        removeCount++;
                    } else {
                        failedCount++;
                    }
                } else {
                    failedCount++;
                }
            }
            masterRecords.getFileUploadStatus().setNumberOfRecordsDbAddOrUpdate(addOrUpdateCount);
            masterRecords.getFileUploadStatus().setNumberOfRecordsDbRemoved(removeCount);
            masterRecords.getFileUploadStatus().setNumberOfRecordsDbFailed(failedCount);

            return true;
        } catch (Exception exp) {
            return false;
        }
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadModelVariantMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.MODEL_VARIANT_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseModelVariantMaster(
                        parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<ModelVariantMaster> modelVariantMasterList = (List<ModelVariantMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertModelVariantMasterList(modelVariantMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                e.printStackTrace();
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadAssetCategoryMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = null;
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.ASSET_CATEGORY_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseAssetCategoryMaster(
                        parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<AssetCategoryMaster> assetCatgMasterList = (List<AssetCategoryMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertAssetCategoryMasterList(assetCatgMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadStateMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;
            fileUploadStatus=new FileUploadStatus();
            try {
                HashMap columnMapping = new HashMap();
                for (StateMasterfields field : StateMasterfields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<StateMaster> stateMasters =
                        transformCsvToList(file, columnMapping, StateMaster.class,fileUploadStatus);
                if(!CollectionUtils.isEmpty(stateMasters)){
                    //insert institution id against every record
                stateMasters.forEach(state ->{state.setInstitutionId(institutionId);});
                boolean dbStatus=fileUploadRepository.insertStateMasterList(stateMasters);
                masterRecords = new MasterRecords();

                masterRecords.setMasterList(stateMasters);
                fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}

            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_STATE_MASTER);
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadCityMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.CITY_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseCityMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CityMaster> cityMasterList = (List<CityMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertCityMasterList(cityMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadCreditPromotionMaster(File file, String institutionId) throws Exception {


        FileUploadStatus fileUploadStatus = null;
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.CREDIT_PROMOTION_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseCreditPromotionMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<CreditPromotionMaster> creditPromoMasterList = (List<CreditPromotionMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertCreditPromoMasterList(creditPromoMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadDsaBranchMaster(File file, String institutionId) throws Exception {


        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords = null;

            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DSA_BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseDsaBranchMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<DSABranchMaster> creditPromoMasterList = (List<DSABranchMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertDsaBranchMasterList(creditPromoMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadDsaProductMaster(File file, String institutionId) throws Exception {


        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DSA_PRODUCT_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseDsaProductMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<DSAProductMaster> dsaProductMasterList = (List<DSAProductMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertDsaProductMasterList(dsaProductMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * @param file
     * @param institutionId
     * @return
     */
    @Override
    public BaseResponse uploadDealerBranchMaster(File file, String institutionId) throws Exception {


        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DEALER_BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();

                masterRecords = masterFileCSVParser.parseDealerBranchMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<DealerBranchMaster> dealerBranchMasterList = (List<DealerBranchMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertDealerBranchMasterList(dealerBranchMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadNegativeAreaGeoLimitMaster(File file, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.NEGATIVE_AREA_GEO_LIMIT_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseNegativeAreaGeoLimitMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasterList = (List<NegativeAreaGeoLimitMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertNegativeAreaFundingMaster(negativeAreaGeoLimitMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setStatus(Status.ERROR.name());
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }


    @Override
    public BaseResponse uploadLosMasterDetails(File file, String institutionId) throws Exception {
        LOSMasterUploadDetailsResponse losMasterUploadDetailsResponse = new LOSMasterUploadDetailsResponse();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.LOS_MASTER_UPLOAD_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseLosDetailsMaster(parser, institutionId);
                if (StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    if (masterRecords != null
                            && masterRecords.getMasterList() != null
                            && !masterRecords.getMasterList().isEmpty()) {

                        List<LosUtrDetailsMaster> losUtrDetailsMasterList = (List<LosUtrDetailsMaster>) masterRecords
                                .getMasterList();

                        return GngUtils.getBaseResponse(HttpStatus.OK, checkLosDetailsMappingWithDatabase(losUtrDetailsMasterList));

                    } else {
                        logger.error("Wrong csv provided");
                        losMasterUploadDetailsResponse
                                .setCsvFileErrorDesc(SystemErrorConstant.WRONG_CSV);
                        losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
                    }
                } else {
                    losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
                    losMasterUploadDetailsResponse.setCsvFileErrorDesc(masterRecords.getFileUploadStatus().getErrorDesc());
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("error in uploading los master csv");
                losMasterUploadDetailsResponse
                        .setCsvFileErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                                + e.getMessage());
                losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
            }
        } else {
            losMasterUploadDetailsResponse
                    .setCsvFileErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, losMasterUploadDetailsResponse);

    }


    @Override
    public BaseResponse updateNetDisbursementAmount(File uploadedFile, String institutionId) throws Exception{
        LOSMasterUploadDetailsResponse losMasterUploadDetailsResponse = new LOSMasterUploadDetailsResponse();
        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.LOS_MASTER_UPLOAD_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseNetDisbursalMaster(parser, institutionId);
                if (StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    if (masterRecords != null
                            && masterRecords.getMasterList() != null
                            && !masterRecords.getMasterList().isEmpty()) {

                        List<NetDisbursalDetailsMaster> netDisbursalMasterList = (List<NetDisbursalDetailsMaster>) masterRecords
                                .getMasterList();

                        return GngUtils.getBaseResponse(HttpStatus.OK, checkNetDisbursalMappingWithDatabase(netDisbursalMasterList));

                    } else {
                        logger.error("Wrong csv provided");
                        losMasterUploadDetailsResponse
                                .setCsvFileErrorDesc(SystemErrorConstant.WRONG_CSV);
                        losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
                    }
                } else {
                    losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
                    losMasterUploadDetailsResponse.setCsvFileErrorDesc(masterRecords.getFileUploadStatus().getErrorDesc());
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("error in uploading los master csv");
                losMasterUploadDetailsResponse
                        .setCsvFileErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                                + e.getMessage());
                losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
            }
        } else {
            losMasterUploadDetailsResponse
                    .setCsvFileErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            losMasterUploadDetailsResponse.setCsvFileStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, losMasterUploadDetailsResponse);
    }

    @Override
    public BaseResponse uploadSupplierLocationMaster(File file, String institutionId) throws IOException {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (SupplierLocationFields field : SupplierLocationFields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<SupplierLocationMaster> supplierLocationMasters = transformCsvToList(file, columnMapping, SupplierLocationMaster.class,fileUploadStatus);;

                if(!CollectionUtils.isEmpty(supplierLocationMasters)){
                    //insert institution id and against every record

                    supplierLocationMasters.forEach(supplierLocationDoc -> {
                        supplierLocationDoc.setInstitutionId(institutionId);
                        supplierLocationDoc.setActive(true);
                    });

                    boolean dbStatus=fileUploadRepository.insertSupplierLocationMaster(supplierLocationMasters);
                    masterRecords = new MasterRecords();
                    masterRecords.setMasterList(supplierLocationMasters);
                    fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_ACCHEAD_MASTER);
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadRelianceDealerBranchMaster(
            File file, String institutionId) throws IOException {


        FileUploadStatus fileUploadStatus;
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.RELIANCE_DEALER_BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseRelianceDealerBranchMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<RelianceDealerBranchMaster> dealerBranchMasterList = (List<RelianceDealerBranchMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertRelianceDealerBranchMasterList(dealerBranchMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    private LOSMasterUploadDetailsResponse checkLosDetailsMappingWithDatabase(List<LosUtrDetailsMaster> losUtrDetailsMasterList) throws Exception {

        LOSMasterUploadDetailsResponse losMasterUploadDetailsResponse = new LOSMasterUploadDetailsResponse();
        List<LosUtrDetailsMaster> losRecordUpdateResponseList = new ArrayList<>();
        for (LosUtrDetailsMaster losUtrDetailsMasterRequest : losUtrDetailsMasterList) {

            GoNoGoCroApplicationResponse goNoGoCroApplication = applicationRepository
                    .getGoNoGoCustomerApplicationByRefId(losUtrDetailsMasterRequest.getRefID(), losUtrDetailsMasterRequest.getInstitutionID());

            if (goNoGoCroApplication != null) {
                verifyLosDetails(goNoGoCroApplication, losUtrDetailsMasterRequest, losRecordUpdateResponseList);

            } else {
                losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
                losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.APPLICATION_NOT_FOUND);
                losRecordUpdateResponseList.add(losUtrDetailsMasterRequest);
            }
        }
        getSuccessAndFailedRecordCount(losRecordUpdateResponseList, losMasterUploadDetailsResponse);
        losMasterUploadDetailsResponse.setLosDetailsList(losRecordUpdateResponseList);
        losMasterUploadDetailsResponse.setCsvFileStatus(Status.SUCCESS.name());

        return losMasterUploadDetailsResponse;
    }

    private NetDisbursalAmtUpdateResponse checkNetDisbursalMappingWithDatabase(List<NetDisbursalDetailsMaster> netDisbursalDetailsMasters) throws Exception {

        NetDisbursalAmtUpdateResponse netDisbursalAmtUpdateResponse = new NetDisbursalAmtUpdateResponse();
        List<NetDisbursalDetailsMaster> netDisbursalUpdateResponseList = new ArrayList<>();
        for (NetDisbursalDetailsMaster netDisbursalDetailsRequest : netDisbursalDetailsMasters) {

            GoNoGoCroApplicationResponse goNoGoCroApplication = applicationRepository
                    .getGoNoGoCustomerApplicationByRefId(netDisbursalDetailsRequest.getRefID(), netDisbursalDetailsRequest.getInstitutionID());

            if (goNoGoCroApplication != null) {
                verifyNetDisbursalDetails(netDisbursalDetailsRequest, netDisbursalUpdateResponseList);

            } else {
                netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
                netDisbursalDetailsRequest.setResponseMessage(ErrorCode.APPLICATION_NOT_FOUND);
                netDisbursalUpdateResponseList.add(netDisbursalDetailsRequest);
            }
        }
        getSuccessAndFailedNetDisbursalCount(netDisbursalUpdateResponseList, netDisbursalAmtUpdateResponse);
        netDisbursalAmtUpdateResponse.setNetDisbursalDetailsList(netDisbursalUpdateResponseList);
        netDisbursalAmtUpdateResponse.setCsvFileStatus(Status.SUCCESS.name());

        return netDisbursalAmtUpdateResponse;
    }



    private void verifyNetDisbursalDetails(NetDisbursalDetailsMaster netDisbursalDetailsRequest, List<NetDisbursalDetailsMaster> netDisbursalDetailsMastersList) throws Exception {

        PostIPA postIpa = digitizationRepository.getPostIpa(netDisbursalDetailsRequest.getInstitutionID(), netDisbursalDetailsRequest.getRefID());

        double netDisbursalAmount = 0;
        double totalDeductionByHdbfs = 0;
        double financeAmount=0;

        if (null != postIpa) {

            // totalDeductionByHdbfs
            totalDeductionByHdbfs = postIpa
                    .getProcessingFees()
                    + postIpa.getAdvanceEmi()
                    + postIpa.getManufSubBorneByDealer()
                    + postIpa.getDealerSubvention()
                    + postIpa.getOtherChargesIfAny();

            if (postIpa.getFinanceAmount() != 0) {
                financeAmount = postIpa.getFinanceAmount();
            } else {
                financeAmount = postIpa.getTotalAssetCost() - postIpa.getMarginMoney();
            }


            // calculate netDisbursalAmount
            netDisbursalAmount = financeAmount - totalDeductionByHdbfs;

            // cross verify calculated netDisbursal amount with netDisbursal Amount which is provided in the csv file
            if (netDisbursalDetailsRequest.getNewNetDisbursalAmt() > 0 && netDisbursalDetailsRequest.getNewNetDisbursalAmt() == netDisbursalAmount) {

                // if post ipa and csv file net disbursal amount not match then update in the post ipa netdisbursal amount
                if(netDisbursalDetailsRequest.getNewNetDisbursalAmt() != postIpa.getNetDisbursalAmount()){

                    netDisbursalDetailsRequest.setOldNetDisbursalAmt(postIpa.getNetDisbursalAmount());
                    updateNetDisbursalAmountInDatabase(netDisbursalDetailsRequest, netDisbursalDetailsMastersList);

                }else{
                    netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
                    netDisbursalDetailsRequest.setResponseMessage(ErrorCode.NET_DISBURSAL_AMT_ALREADY_UPDATED);
                    netDisbursalDetailsMastersList.add(netDisbursalDetailsRequest);
                }

            } else {

                netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
                netDisbursalDetailsRequest.setResponseMessage(ErrorCode.NET_DISBURSAL_NOT_MATCH);
                netDisbursalDetailsMastersList.add(netDisbursalDetailsRequest);
            }
        }else{
            netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
            netDisbursalDetailsRequest.setResponseMessage(ErrorCode.POST_IPA_DETAILS_NOT_FOUND);
            netDisbursalDetailsMastersList.add(netDisbursalDetailsRequest);
        }

    }

    private void verifyLosDetails(GoNoGoCroApplicationResponse goNoGoCroApplication, LosUtrDetailsMaster losUtrDetailsMasterRequest, List<LosUtrDetailsMaster> losMasterResponseList) throws Exception {

        String applicationStatus = goNoGoCroApplication.getApplicationStatus();

        LOSDetails losDetails = null;

        if (null != goNoGoCroApplication.getLosDetails()) {

            losDetails = goNoGoCroApplication.getLosDetails();
        }

        PostIPA postIpa = digitizationRepository.getPostIpa(losUtrDetailsMasterRequest.getInstitutionID(), losUtrDetailsMasterRequest.getRefID());

        double netDisbursalAmount = 0;

        if (null != postIpa) {

            netDisbursalAmount = postIpa.getNetDisbursalAmount();
        }

        if (losUtrDetailsMasterRequest.getNetDisbursalAmt() == netDisbursalAmount && null != postIpa) {

            if (checkApplicationStage(goNoGoCroApplication.getApplicationRequest().getCurrentStageId(), applicationStatus)) {
                /**
                 *  if los details are null or los id is blank and stage is matching then update all los details
                 *  i.e. los id ,utr no and status
                 */
                if (losDetails == null || StringUtils.isBlank(losDetails.getLosID())) {

                    updateAndUploadLosDetailsInDatabase(losUtrDetailsMasterRequest, losMasterResponseList);

                } else {
                    /**
                     *  if los id already present in db and match with provided los id  then check utr no also else reject the record and
                     *  provide los id not match message.
                     */
                    if (StringUtils.equals(losDetails.getLosID(), losUtrDetailsMasterRequest.getLosID())) {
                        /**
                         * if los id and utr no both match then reject the record and provide message to user else
                         * update the utr no.
                         */
                        if (StringUtils.equals(losDetails.getUtrNumber(), losUtrDetailsMasterRequest.getUtrNumber())) {

                            losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
                            losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.LOSID_UTRNO_ALREADY_EXIST);
                            losMasterResponseList.add(losUtrDetailsMasterRequest);

                        } else {
                            /**
                             * if los id are same then update the utr no and status
                             */
                            updateAndUploadLosDetailsInDatabase(losUtrDetailsMasterRequest, losMasterResponseList);
                        }

                    } else {
                        losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
                        losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.LOS_ID_NOT_MATCH);
                        losMasterResponseList.add(losUtrDetailsMasterRequest);
                    }
                }
            } else {
                losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
                losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.NOT_VALID_APPLICATION_STAGE_FOUND);
                losMasterResponseList.add(losUtrDetailsMasterRequest);
            }
        } else {

            losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
            losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.NET_DISBURSAL_NOT_MATCH);
            losMasterResponseList.add(losUtrDetailsMasterRequest);
        }

    }

    private boolean checkApplicationStage(String applicationStage, String applicationStatus) {

        Set<String> applicationStagegForLosUpload = GngUtils.getApplicationStagesForUtrUpload();

        if (applicationStagegForLosUpload.contains(applicationStage) && StringUtils.equals(applicationStatus,
                GNGWorkflowConstant.APPROVED.toFaceValue())) {
            return true;
        } else {
            return false;
        }

    }

    private void updateAndUploadLosDetailsInDatabase(LosUtrDetailsMaster losUtrDetailsMasterRequest, List<LosUtrDetailsMaster> losMasterResponseList) {

        if (fileUploadRepository.updateLosDetails(losUtrDetailsMasterRequest)) {
            if (fileUploadRepository.insertLosMasterDetails(losUtrDetailsMasterRequest)) {
                losUtrDetailsMasterRequest.setStatus(Status.SUCCESS.name());
                losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.SUCCESSFULLY_RECORD_INSERTED);
                losMasterResponseList.add(losUtrDetailsMasterRequest);
            } else {
                losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
                losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.PROBLEM_TO_INSERT_RECORD);
                losMasterResponseList.add(losUtrDetailsMasterRequest);
            }
        } else {
            losUtrDetailsMasterRequest.setStatus(Status.FAILED.name());
            losUtrDetailsMasterRequest.setResponseMessage(ErrorCode.PROBLEM_TO_UPDATE_RECORD);
            losMasterResponseList.add(losUtrDetailsMasterRequest);
        }
    }

    private void updateNetDisbursalAmountInDatabase(NetDisbursalDetailsMaster netDisbursalDetailsRequest, List<NetDisbursalDetailsMaster> netDisbursalResponseList) {

        if (fileUploadRepository.updateNetDisbursalAmount(netDisbursalDetailsRequest)) {
            if (fileUploadRepository.insertNetDisbursalDetails(netDisbursalDetailsRequest)) {
                netDisbursalDetailsRequest.setStatus(Status.SUCCESS.name());
                netDisbursalDetailsRequest.setResponseMessage(ErrorCode.SUCCESSFULLY_RECORD_INSERTED);
                netDisbursalResponseList.add(netDisbursalDetailsRequest);
            } else {
                netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
                netDisbursalDetailsRequest.setResponseMessage(ErrorCode.PROBLEM_TO_INSERT_RECORD);
                netDisbursalResponseList.add(netDisbursalDetailsRequest);
            }
        } else {
            netDisbursalDetailsRequest.setStatus(Status.FAILED.name());
            netDisbursalDetailsRequest.setResponseMessage(ErrorCode.PROBLEM_TO_UPDATE_RECORD);
            netDisbursalResponseList.add(netDisbursalDetailsRequest);
        }
    }

    private void getSuccessAndFailedRecordCount(List<LosUtrDetailsMaster> losRecordUpdateResponseList, LOSMasterUploadDetailsResponse losMasterUploadDetailsResponse) {
        if (losRecordUpdateResponseList != null && !losRecordUpdateResponseList.isEmpty()) {
            int failedRecord = 0;
            int successRecord = 0;
            for (LosUtrDetailsMaster losUtrDetailsMaster : losRecordUpdateResponseList) {
                if (StringUtils.equals(losUtrDetailsMaster.getStatus(), Status.SUCCESS.name())) {
                    successRecord++;
                } else {
                    failedRecord++;
                }

            }
            losMasterUploadDetailsResponse.setNoOfFailedRecord(failedRecord);
            losMasterUploadDetailsResponse.setNoOfSucceedRecord(successRecord);
        }
    }

    private void getSuccessAndFailedNetDisbursalCount(List<NetDisbursalDetailsMaster> netDisbursalUpdateResponseList, NetDisbursalAmtUpdateResponse netDisbursalAmtUpdateResponse) {

        if (netDisbursalUpdateResponseList != null && !netDisbursalUpdateResponseList.isEmpty()) {
            int failedRecord = 0;
            int successRecord = 0;
            for (NetDisbursalDetailsMaster netDisbursalDetailsMaster : netDisbursalUpdateResponseList) {
                if (StringUtils.equals(netDisbursalDetailsMaster.getStatus(), Status.SUCCESS.name())) {
                    successRecord++;
                } else {
                    failedRecord++;
                }

            }
            netDisbursalAmtUpdateResponse.setNoOfFailedRecord(failedRecord);
            netDisbursalAmtUpdateResponse.setNoOfSucceedRecord(successRecord);
        }
    }

    @Override
    public BaseResponse uploadLoyaltyCardMaster(File uploadFile, String institutionId) throws IOException {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadFile.toPath()))) {
            MasterRecords masterRecords;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object

                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.LOYALTY_CARD_MASTER_HEADER)
                        .withSkipHeaderRecord();

                parser = new CSVParser(new InputStreamReader(new FileInputStream(uploadFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser
                        .parseLoyaltyCardMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<LoyaltyCardMaster> loyaltyCardMastersList = (List<LoyaltyCardMaster>) masterRecords
                            .getMasterList();
                    boolean dbStatus = fileUploadRepository
                            .insertLoyaltyCardMasterList(loyaltyCardMastersList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading loyalty card master csv");
                fileUploadStatus.setStatus(Status.ERROR.name());
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadLosBankingMaster(File uploadedFile, String institutionId) throws IOException {
        FileUploadStatus fileUploadStatus;

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.LOS_BANK_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(uploadedFile)), format);

                masterRecords = masterFileCSVParser.parseLosBankMaster(parser, institutionId);

                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<LosBankMaster> losBankMasters = (List<LosBankMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertLosBankMasterList(losBankMasters);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);


                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    private FileUploadStatus getDbFileUploadStatus(boolean dbStatus, MasterRecords masterRecords) {
        FileUploadStatus fileUploadStatus;
        if (dbStatus) {
            logger.info("Database operation success");
            fileUploadStatus = masterRecords.getFileUploadStatus();

        } else {
            logger.error("Database operation failure");
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc("Database operation failure");
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return fileUploadStatus;
    }

    @Override
    public BaseResponse uploadEwInsGngAssetCategoryMaster(File uploadedFile, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.EW_INS_GNG_ASSET_CAT_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseEwInsGngAssetCategoryMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<EwInsGngAssetCategoryMaster> ewInsGngAssetCategoryMasterList = (List<EwInsGngAssetCategoryMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository
                            .insertEwInsGngAssetCategoryMaster(ewInsGngAssetCategoryMasterList);
                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    @Override
    public BaseResponse uploadEwPremiumDataMaster(File uploadedFile, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.EW_PREMIUM_MASTER_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseEwPremiumDataMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<EWPremiumMaster> ewPremiumMasterList = (List<EWPremiumMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository
                            .insertEwPremiumDataMaster(ewPremiumMasterList);
                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);


    }

    @Override
    public BaseResponse uploadInsurancePremiumMaster(File uploadedFile, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.INSURANCE_PREMIUM_MASTER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseInsurancePremiumMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<InsurancePremiumMaster> insurancePremiumMasterList = (List<InsurancePremiumMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository
                            .insertInsurancePremiumMaster(insurancePremiumMasterList);
                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);


    }

    @Override
    public BaseResponse uploadElecServProvMaster(File uploadedFile, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE,
                Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.ELEC_SERV_PROV_MASTER_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                masterRecords = masterFileCSVParser.parseElecServProvMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<ElecServProvMaster> elecServProvMasterList = (List<ElecServProvMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository
                            .insertElecServProvMaster(elecServProvMasterList);
                    if (dbStatus) {
                        logger.info("Inserted data into Electricity service Provide Master.");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Failed insertion into Electricity service Provide Master.");
                        fileUploadStatus.setErrorDesc("Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }
            } catch (Exception e) {
                logger.error("Error in uploading elctricity service provider master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("Error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadTkilDealerBranchMaster(File file, String institutionId) throws IOException {

        FileUploadStatus fileUploadStatus;
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {

            MasterRecords masterRecords;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.TKIL_DEALER_BRANCH_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(file)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseTkilDealerBranchMaster(parser, institutionId);

                if (masterRecords != null
                        && !CollectionUtils.isEmpty(masterRecords.getMasterList())
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<TkilDealerBranchMaster> dealerBranchMasterList = (List<TkilDealerBranchMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertTkilDealerBranchMasterList(dealerBranchMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading tkil dealer branch master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadTaxCodeMaster(File file, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (TaxCodeFields taxCodeField : TaxCodeFields.values()) {
                    columnMapping.put(taxCodeField.name(), taxCodeField.getValue());
                }
                List<TaxCodeMaster> taxCodeMasterList = transformCsvToList(file, columnMapping, TaxCodeMaster.class,fileUploadStatus);
                if(!CollectionUtils.isEmpty(taxCodeMasterList)){
                    //insert institution id and against every record
                taxCodeMasterList.forEach(taxCode ->taxCode.setInstitutionId(institutionId));
                boolean dbStatus=fileUploadRepository.insertTaxCodeMaster(taxCodeMasterList);
                masterRecords = new MasterRecords();
                masterRecords.setMasterList(taxCodeMasterList);
                fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_TAXCODES_MASTER);
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadAccountsHeadMaster(File file, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (AccountsHeadFields field : AccountsHeadFields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<AccountsHeadMaster> accountsHeadMasterList = transformCsvToList(file, columnMapping, AccountsHeadMaster.class,fileUploadStatus);;

                if(!CollectionUtils.isEmpty(accountsHeadMasterList)){
                    //insert institution id and against every record
                accountsHeadMasterList.forEach(accHead -> accHead.setInstitutionId(institutionId));
                boolean dbStatus=fileUploadRepository.insertAccountsHeadMaster(accountsHeadMasterList);
                masterRecords = new MasterRecords();
                masterRecords.setMasterList(accountsHeadMasterList);
                fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_ACCHEAD_MASTER);
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    /**
     * Upload StateBranchMaster records from CSV file.
     * @param file
     * @param institutionId
     * @return
     * @throws Exception
     */
    @Override
    public BaseResponse uploadStateBranchMaster(File file, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (StateBranchMasterFields field : StateBranchMasterFields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<StateBranchMaster> stateBranchMasterList =
                        transformCsvToList(file, columnMapping, StateBranchMaster.class,fileUploadStatus);

                if(!CollectionUtils.isEmpty(stateBranchMasterList)){
                    //insert institution id and against every record
                stateBranchMasterList.forEach(stateBranch -> stateBranch.setInstitutionId(institutionId));
                boolean dbStatus=fileUploadRepository.insertStateBranchMaster(stateBranchMasterList);
                masterRecords = new MasterRecords();
                masterRecords.setMasterList(stateBranchMasterList);
                fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_STATEBRANCH_MASTER);
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadTCLosGngMappingMaster(File file, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
       if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (TCLosGngMappingFields field : TCLosGngMappingFields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<TCLosGngMappingMaster> tcLosGngMappings =
                        transformCsvToList(file, columnMapping, TCLosGngMappingMaster.class,fileUploadStatus);

                if(!CollectionUtils.isEmpty(tcLosGngMappings)){
                    //insert institution id and against every record    
                   tcLosGngMappings.forEach(tcLosGngMapping -> tcLosGngMapping.setInstitutionId(institutionId));
                    boolean dbStatus=fileUploadRepository.insertTCLosGngMappingMaster(tcLosGngMappings);
                    masterRecords = new MasterRecords();
                    masterRecords.setMasterList(tcLosGngMappings);
                    fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error(ErrorCode. ERROR_UPLOADING_TCLOSGNGMAPPING);
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
      } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadSourcingDetailMaster(File uploadedFile, String institutionId) throws IOException {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SOURCING_DETAIL_MASTER_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseSourcingDetailMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<SourcingDetailMaster> sourcingDetailMasterList = (List<SourcingDetailMaster>) masterRecords
                            .getMasterList();

                    dbStatus = fileUploadRepository.insertSourcingDetailMaster(sourcingDetailMasterList);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadPromotionalSchemeMaster(File uploadedFile, String institutionId) throws IOException {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.PROMOTIONAL_SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parsePromotionalSchemeMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<PromotionalSchemeMaster> promotionalSchemeMasterList = (List<PromotionalSchemeMaster>) masterRecords
                            .getMasterList();

                    dbStatus = fileUploadRepository.insertPromotionSchemeMaster(promotionalSchemeMasterList);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }


    @Override
    public BaseResponse uploadBankMaster(File uploadedFile, String institutionId) throws IOException {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.BANK_MASTER_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseBankMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<BankMaster> bankMasterList = (List<BankMaster>) masterRecords
                            .getMasterList();

                    dbStatus = fileUploadRepository.insertBankMaster(bankMasterList);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading scheme master csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    @Override
    public BaseResponse uploadGenericMaster(File file, String institutionId, MasterDetails masterName) throws Exception {

        FileUploadStatus fileUploadStatus = null;

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            try {

                if(masterName == MasterDetails.PINCODE_MASTER) {

                    fileUploadStatus = genericUpload.uploadPincode(file, masterName, institutionId);

                }else if(masterName == MasterDetails.BRANCH_MASTER) {

                    fileUploadStatus = genericUpload.uploadBranch(file, masterName, institutionId);

                }else if(masterName == MasterDetails.DEALER_EMAIL_MASTER) {

                    fileUploadStatus = genericUpload.uploadDealerEmail(file, masterName, institutionId);

                }else if(masterName == MasterDetails.INSURANCE_PREMIUM_MASTER) {

                    fileUploadStatus = genericUpload.uploadInsurancePremium(file, masterName, institutionId);

                }else if(masterName == MasterDetails.STATE_MASTER) {

                    fileUploadStatus = genericUpload.uploadState(file, masterName, institutionId);

                }else if(masterName == MasterDetails.CITY_STATE_MASTER) {

                    fileUploadStatus = genericUpload.uploadCityState(file, masterName, institutionId);

                }else if(masterName == MasterDetails.SCHEME_MASTER) {

                    fileUploadStatus = genericUpload.uploadScheme(file, masterName, institutionId);

                }else if(masterName == MasterDetails.ASSET_MASTER) {

                    fileUploadStatus = genericUpload.uploadAssetModel(file, masterName, institutionId);

                }

            } catch (Exception e) {
                logger.error(ErrorCode.ERROR_UPLOADING_STATE_MASTER);
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);

    }

    @Override
    public BaseResponse uploadManufacturers(File file, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();
        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            MasterRecords masterRecords = null;
            try {
                HashMap columnMapping = new HashMap();
                for (ManufacturerMasterFields field : ManufacturerMasterFields.values()) {
                    columnMapping.put(field.name(), field.getValue());
                }
                List<ManufacturerMaster> manufacturers =
                        transformCsvToList(file, columnMapping, ManufacturerMaster.class,fileUploadStatus);

                if(!CollectionUtils.isEmpty(manufacturers)){
                    //insert institution id and against every record    
                    manufacturers.forEach(manufacturer -> manufacturer.setInstitutionId(institutionId));
                    boolean dbStatus=fileUploadRepository.insertManufacturers(manufacturers);
                    masterRecords = new MasterRecords();
                    masterRecords.setMasterList(manufacturers);
                    fileUploadStatus= setRecordStatus(dbStatus,masterRecords,fileUploadStatus);}
            } catch (Exception e) {
                logger.error("Error uploading ManufacturerMaster.");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadPaynimoBankMaster(File uploadedFile, String institutionId) throws IOException {
        FileUploadStatus fileUploadStatus;
        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.PAYNIMO_BNNK_MASTER_HEADER)
                        .withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(new FileInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parsePaynimoBankMaster(parser, institutionId);

                if (masterRecords != null
                        && !CollectionUtils.isEmpty(masterRecords.getMasterList())
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")
                    List<PaynimoBankMaster> paynimoBankMasterList = (List<PaynimoBankMaster>) masterRecords
                            .getMasterList();

                    boolean dbStatus = fileUploadRepository
                            .insertPaynimoBankMasterList(paynimoBankMasterList);

                    fileUploadStatus = getDbFileUploadStatus(dbStatus, masterRecords);

                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }

            } catch (Exception e) {
                logger.error("error in uploading paynimo bank master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());

            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }


    @Override
    public BaseResponse uploadDigitizationEmailMaster(File uploadedFile, String institutionId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {

            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseDigitizationEmailMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {

                    @SuppressWarnings("unchecked")

                    List<DigitizationDealerEmailMaster> digitizationEmailMaster = (List<DigitizationDealerEmailMaster>) masterRecords
                            .getMasterList();

                    dbStatus = fileUploadRepository.insertDigitizationEmailMaster(digitizationEmailMaster);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());

                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();

                }

            } catch (Exception e) {
                logger.error("error in uploading digitizationEmailMaster csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadDeviationMaster(File uploadedFile, String institutionId, String product) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER).withSkipHeaderRecord();
                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);
                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseDeviationMaster(parser, institutionId, product);
                boolean dbStatus = false;
                if (masterRecords != null
                        && masterRecords.getMasterList() != null
                        && !masterRecords.getMasterList().isEmpty()
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
                    List<DeviationMaster> deviationMasterList = (List<DeviationMaster>) masterRecords
                            .getMasterList();
                    dbStatus = fileUploadRepository.insertDeviationMaster(deviationMasterList);
                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());
                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }
            } catch (Exception e) {
                logger.error("error in uploading deviationMaster csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadOrganizationalHierarchyMaster(File uploadedFile, String institutionId) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER).withSkipHeaderRecord();
                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);
                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseOrganizationalHierarchyMaster(parser, institutionId);
                boolean dbStatus = false;
                if (masterRecords != null
                        && CollectionUtils.isNotEmpty(masterRecords.getMasterList())
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
                    List<OrganizationalHierarchyMaster> organizationalHierarchyMasters = (List<OrganizationalHierarchyMaster>)
                            masterRecords.getMasterList();
                    dbStatus = fileUploadRepository.insertOrganizationalHierarchyMaster(organizationalHierarchyMasters, institutionId);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());
                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }
            } catch (Exception e) {
                logger.error("error in uploading deviationMaster csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    if(parser != null)
                        parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadPotentialCustomer(File uploadedFile, String institutionId, String product) throws Exception {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_XSL_TYPE, Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER).withSkipHeaderRecord();
                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);

                InputStream targetStream = new FileInputStream(uploadedFile);
                OPCPackage opcPackage = OPCPackage.open(targetStream);
                XSSFWorkbook wb = new XSSFWorkbook(opcPackage);
                XSSFSheet sheet = wb.getSheetAt(0);

                masterRecords = masterFileCSVParser.parseCustomerDetailsMaster(sheet, institutionId, product);
                if (masterRecords != null
                        && CollectionUtils.isNotEmpty(masterRecords.getMasterList())
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getMasterList());
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }
            } catch (Exception e) {
                logger.error("error in uploading potential customer xls");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    if(parser != null)
                        parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public BaseResponse uploadRoiSchemeMaster(File uploadedFile, String institutionId, String product) throws IOException {
        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (uploadedFile.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(uploadedFile.toPath()))) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;
            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.DIGITIZATION_EMAIL_MASTER).withSkipHeaderRecord();
                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(FileUtils.openInputStream(uploadedFile)), format);
                //masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseRoiSchemeMasterDetails(parser, institutionId, product);
                boolean dbStatus = false;
                if (masterRecords != null
                        && CollectionUtils.isNotEmpty(masterRecords.getMasterList())
                        && StringUtils.equals(masterRecords.getFileUploadStatus().getStatus(), Status.SUCCESS.name())) {
                    List<RoiSchemeMaster> roiSchemeMasters = (List<RoiSchemeMaster>)masterRecords.getMasterList();
                    dbStatus = fileUploadRepository.insertRoiSchemeMaster(roiSchemeMasters, institutionId, product);

                    if (dbStatus) {
                        logger.info("Database operation success");
                        return GngUtils.getBaseResponse(HttpStatus.OK, masterRecords.getFileUploadStatus());
                    } else {
                        logger.error("Database operation failure");
                        fileUploadStatus.setErrorDesc("Master already found in the database. Database operation failure");
                        fileUploadStatus.setStatus(Status.ERROR.name());
                    }
                } else {
                    fileUploadStatus = masterRecords.getFileUploadStatus();
                }
            } catch (Exception e) {
                logger.error("error in uploading RoiSchemaMaster csv");
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION
                        + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            } finally {
                try {
                    if(parser != null)
                        parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }

    @Override
    public boolean checkMasterUploadRestriction(String institutionId,String masterName, FileUploadStatus fileUploadStatus) {
        boolean isRestricted =false;
        try {
            MasterSchedulerConfiguration masterSchedulerConfiguration = null;
            /**
             * get data from cache or db
             */
            masterSchedulerConfiguration=fetchMasterSchedulerConfigurations(institutionId,masterSchedulerConfiguration);
            MasterSchedulerConfiguration masterConfig = applicationRepository.findMasterSchedularConfiguration(institutionId, masterName);
            logger.info("{}  masterConfigDetails",masterConfig);
            if (masterSchedulerConfiguration!=null&& masterSchedulerConfiguration.isActive()){
                /**
                 * will return true if current time is in between from and TO else false
                 */
                isRestricted = GngDateUtil.checkScheduleTime(masterSchedulerConfiguration.getFromTime(), masterSchedulerConfiguration.getToTime());
                logger.info("{}  RestrictedData1",isRestricted);
                if (isRestricted && masterConfig!=null){
                    logger.info("{}  RestrictedData2",isRestricted);
                    /**
                     * will return true if current time is in between from and TO else false
                     */
                    isRestricted = !GngDateUtil.checkScheduleTime(masterConfig.getFromTime(), masterConfig.getToTime());
                    logger.info("{}  RestrictedData3",isRestricted);
                }
            }
            if (isRestricted && null!= masterSchedulerConfiguration){
                logger.info("{}  RestrictedData4",isRestricted);
                logger.info("{}  masterSchedulerConfigurationDetails",masterSchedulerConfiguration);
                fileUploadStatus.setErrorDesc(String.format(ErrorCode.MASTER_UPLOAD_RESTRICTION, masterName, masterSchedulerConfiguration.getFromTime(), masterSchedulerConfiguration.getToTime()));
            }
        }catch (Exception e){
            logger.error("Exception occured while checking master restriction details and exception is:{}",e);
        }
        return isRestricted;
    }


    private MasterSchedulerConfiguration fetchMasterSchedulerConfigurations(String institutionId, MasterSchedulerConfiguration masterSchedulerConfiguration) {
        masterSchedulerConfiguration = Cache.masterSchedularConfigurationMap.get(institutionId);
        logger.info("{}  masterSchedulerConfiguration1",masterSchedulerConfiguration);
        if (masterSchedulerConfiguration == null) {
            masterSchedulerConfiguration = applicationRepository.findMasterSchedularConfigurationBasisOnInstId(institutionId, Constant.DEFAULT);
            logger.info("{}  masterSchedulerConfiguration2",masterSchedulerConfiguration);
            if (masterSchedulerConfiguration == null) {
                masterSchedulerConfiguration = Cache.masterSchedularConfigurationMap.get(Constant.DEFAULT_INSTID);
                logger.info("{}  masterSchedulerConfiguration3",masterSchedulerConfiguration);
                if (masterSchedulerConfiguration == null) {
                    masterSchedulerConfiguration = applicationRepository.findMasterSchedularConfiguration(Constant.DEFAULT_INSTID, Constant.DEFAULT);
                    logger.info("{}  masterSchedulerConfiguration4",masterSchedulerConfiguration);
                    if (masterSchedulerConfiguration != null){
                        Cache.masterSchedularConfigurationMap.put(masterSchedulerConfiguration.getInstitutionId(),masterSchedulerConfiguration);
                        logger.info("{}  masterSchedulerConfiguration5",masterSchedulerConfiguration);
                    }
                }
            }else{
                Cache.masterSchedularConfigurationMap.put(masterSchedulerConfiguration.getInstitutionId(),masterSchedulerConfiguration);
                logger.info("{}  masterSchedulerConfiguration6",masterSchedulerConfiguration);
            }
        }
        return masterSchedulerConfiguration;
    }

    @Override
    public MasterActivityDetails masterActivityDetailsInitializer(String institutionId, String masterName, String emailId, String userName, String userRole, String deviceInfo) {
        MasterActivityDetails.MasterActivityDetailsBuilder builder = MasterActivityDetails.builder();
        MasterActivityDetails activityDetails = builder.userRole(userRole).userName(userName).deviceInfo(deviceInfo).emailId(emailId)
                .status(Status.INITIATED.name()).masterCategory(getMasterCategory(masterName))
                .institutionId(institutionId).uuid(UUID.randomUUID().toString()).createdDate(new Date()).masterName(masterName).build();
        return  activityDetails;
    }

    private String getMasterCategory(String masterName){
        String status= NON_TRANSACTIONAL;
        for (TransactionalMasterEnum masters : TransactionalMasterEnum.values()) {
            if (masters.name().equalsIgnoreCase(masterName)) {
                status= TRANSACTIONAL; break;
            }
        }
        return status;
    }

    @Override
    public ResponseEntity<BaseResponse> isInvalidUpload(String institutionId, File uploadedFile, MasterActivityDetails masterActivityDetails) {
        ResponseEntity<BaseResponse> responseEntity=null;
        try {
            if (!FilenameUtils.getExtension(uploadedFile.getName()).equalsIgnoreCase(GNGWorkflowConstant.CSV.toFaceValue())) {
                masterActivityDetails.setStatus(Status.FAILED.name());
                masterActivityDetails.setError(CSV_FILE_VALID_ERROR_MSG);
                responseEntity=new ResponseEntity<>(GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, CSV_FILE_VALID_ERROR_MSG), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception ex){
            masterActivityDetails.setStatus(Status.FAILED.name());
            masterActivityDetails.setError(ex.getMessage());
        }
        return responseEntity;
    }

    @Override
    public BaseResponse uploadApiRoleAuthorisationMaster(File file, String institutionId, MasterActivityDetails masterActivityDetails, String sourceId) throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        if (file.exists() && StringUtils.equalsIgnoreCase(SUPPORTED_FILE_TYPE, Files.probeContentType(file.toPath()))) {
            try {
                fileUploadStatus = genericUpload.uploadApiRoleAuthorisationMaster(file, MasterDetails.API_ROLE_AUTHORISATION_MASTER, institutionId,sourceId);
            }catch (Exception e) {
                logger.error("Error occured while uploading master with probable cause {}",e);
                logger.error(String.format(  ErrorCode.ERROR_UPLOADING_MASTER,MasterDetails.API_ROLE_AUTHORISATION_MASTER));
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setErrorDesc(SystemErrorConstant.RUNTIME_EXCEPTION + e.getMessage());
                fileUploadStatus.setStatus(Status.ERROR.name());
            }
        }else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setErrorDesc(SystemErrorConstant.FORMAT_NOT_SUPPORTED);
            fileUploadStatus.setStatus(Status.ERROR.name());
        }

        if (StringUtils.equalsIgnoreCase(fileUploadStatus.getStatus(),Status.SUCCESS.name())||StringUtils.equalsIgnoreCase(fileUploadStatus.getErrorDesc(),ErrorCode.DB_OPERATION_FAILED)||StringUtils.equalsIgnoreCase(fileUploadStatus.getErrorDesc(),SystemErrorConstant.CURRUPT_RECORD)){
            masterActivityDetails.setStatus(Status.PENDING.name());
        }else{
            masterActivityDetails.setStatus(Status.FAILED.name());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadStatus);
    }
}
