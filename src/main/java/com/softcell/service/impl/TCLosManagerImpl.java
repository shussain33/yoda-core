package com.softcell.service.impl;

import com.opencsv.CSVWriter;
import com.softcell.constants.*;
import com.softcell.constants.csv.CSVHeaderConstant;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.exceptions.TcLosException;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.tclos.log.TcLosReqReslog;
import com.softcell.gonogo.model.tclos.request.TCLosInputRequest;
import com.softcell.gonogo.model.tclos.response.TCLosResponse;
import com.softcell.gonogo.service.TCLosManager;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Softcell on 21/09/17.
 */
@Service
public class TCLosManagerImpl implements TCLosManager{
    private static final Logger logger = LoggerFactory.getLogger(TCLosManagerImpl.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MasterRepository masterRepository;

    @Autowired
    UploadFileRepository uploadFileRepository;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Override
    public void buildTCLosData(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse
                                , boolean generateCSV
                                , String path
                                , String branchCodeVal
                                ) throws TcLosException {
        if (goNoGoCroApplicationResponse==null)
        {
            logger.error("Application not found for refId {}.", goNoGoCroApplicationResponse.getGngRefId());
            throw new TcLosException(String.format(ErrorCode.APP_NT_FND, goNoGoCroApplicationResponse.getGngRefId()));
        }

        PostIPA postIPA = null;
        ExtendedWarrantyDetails extendedWarrantyDetails = null;
        CreditVidyaDetails creditVidyaDetails = null;
        StateBranchMaster stateBranchMaster = null;
        InsurancePremiumDetails insurancePremiumDetails=null;
        DealerEmailMaster dealerEmailMaster = null;
        List<TCLosGngMappingMaster> tcLosGngMappingList = null;
        String branchCode = null;
        String refId = goNoGoCroApplicationResponse.getGngRefId();
        if(generateCSV) {
            branchCode = branchCodeVal;
        } else {
            branchCode = goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getBranchCode();
        }

        try {
            tcLosGngMappingList = masterRepository.getTCLosGngMappingMaster(goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            stateBranchMaster = masterRepository.getStateBranchMaster(branchCode
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            postIPA = applicationRepository.getPostIPA(refId, goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(refId
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            creditVidyaDetails = applicationRepository.fetchCreditVidyaDetails(refId
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            insurancePremiumDetails =applicationRepository.fetchInsuranceDetails(refId,goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
            dealerEmailMaster =applicationRepository.getDealerEmailMaster(goNoGoCroApplicationResponse.getApplicationRequest().getHeader());
        } catch (Exception e) {
            logger.error("Error in retrieving pre-requisite data for RefId {} . Probable Cause: {} ", refId, e.getMessage());
            throw new TcLosException(String.format(ErrorCode.MASTER_RETRIEVE_ERROR, refId, e.getMessage()));
        }

        List tcLosDataList = new ArrayList();
        AccountsHeadMaster accountsHeadMaster = null;
        TaxCodeMaster taxCodeMaster = null;

        long incrementalId= applicationRepository.getIncrIdBySeqType(goNoGoCroApplicationResponse
                        .getApplicationRequest().getHeader().getInstitutionId()
                , branchCode
                , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getProduct()
                , SequenceType.TVSCSAMT_SEQUENCE,3);

        if(CollectionUtils.isEmpty(tcLosGngMappingList)) {
            logger.error("TCLOS_GNG_MAPPING_MASTER is empty.");
            throw new TcLosException(ErrorCode.MAPPING_MASTER_EMPTY);
        }

        if(postIPA == null) {
            logger.error("postIPA is empty for RefId : {} .", refId);
            throw new TcLosException(String.format(ErrorCode.POSTIPA_NFND, refId));
        }

        //Read all the field mapping details from enum.
        for (TCLosGngMappingMaster tcLosGngMapping: tcLosGngMappingList) {
            accountsHeadMaster = masterRepository.getAccountHeadMaster(tcLosGngMapping.getSapField()
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());

            if(accountsHeadMaster == null) {
                logger.error("ACCOUNTS_HEAD_MASTER entry not found for field {} & Institute {} ."
                        , tcLosGngMapping.getSapField()
                        , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
                throw new TcLosException(String.format(ErrorCode.ACCHEADER_NFND
                        , tcLosGngMapping.getSapField()
                        , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId()));
            }

            TCLosData tCLosData = new TCLosData();

            //******* DEAFAULT FIELDS********
            initializeDefaultFields(tCLosData);

            setAmount(tCLosData, tcLosGngMapping.getGngField(), postIPA, extendedWarrantyDetails
                    , creditVidyaDetails, insurancePremiumDetails);

            if(!generateCSV && tCLosData.getAmount() <= 0) {
                continue;
            }

            //Populate Payment Mode from accountsHeaderMaster.
            tCLosData.setPaymentMode(accountsHeadMaster.getPaymentMode());

            tCLosData.setInterfaceId(goNoGoCroApplicationResponse.getGngRefId());
            tCLosData.setReferenceNo(goNoGoCroApplicationResponse.getAgreementNum());

            //Set COST_CENTER only for INCOME
            if (TCLosFieldType.INCOME.name().equals(tcLosGngMapping.getFieldType())) {
                tCLosData.setCostCenter(branchCode);
            }

            tCLosData.setDocumentHead(tcLosGngMapping.getAccEntrytype());
            tCLosData.setTokenCode(tcLosGngMapping.getAccEntryNumber());
            tCLosData.setAccType(tcLosGngMapping.getAccType());
            tCLosData.setSpecialGl(accountsHeadMaster.getSpecialGlIndicator());

            if ( CreditOrDebit.CREDIT.name().equals(tcLosGngMapping.getAccFieldType())) {
                tCLosData.setDebitCreditIndicator(CreditDebitIndicator.H.getValue());
                tCLosData.setPostingKey(accountsHeadMaster.getPostingKeyCr());
            }
            else {
                tCLosData.setDebitCreditIndicator(CreditDebitIndicator.S.getValue());
                tCLosData.setPostingKey(accountsHeadMaster.getPostingKeyDb());
            }

            tCLosData.setItemText("" + tcLosGngMapping.getAccEntrytype() + "-"
                    + goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId()
                    + "-" + goNoGoCroApplicationResponse.getAgreementNum());

            tCLosData.setGlAccountNo(accountsHeadMaster.getTvsmAccountCode());

            if (AhmPandalFlag.Y.name().equals(accountsHeadMaster.getPandlFlag())) {
                tCLosData.setPandlFlag(TcLosPandalFlag.X.name());
                //Below fields are set when tCLosData.PanDlFlag is 'X'
                tCLosData.setCostCode(null!=accountsHeadMaster.getTvsmAccountCode()?accountsHeadMaster.getTvsmAccountCode():"0");// This is a non-mandatory field but TVS API expects some value for this.
                tCLosData.setProfitCenter(branchCode);
                tCLosData.setBranchCode(branchCode);
                tCLosData.setProduct("CD");
                if(stateBranchMaster != null) {
                    taxCodeMaster = masterRepository.getTaxCodeMaster(stateBranchMaster.getStateCode()
                            , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
                    if(taxCodeMaster !=  null) {
                        tCLosData.setTaxCode(taxCodeMaster.getNewTaxCode());
                        tCLosData.setSacCode(taxCodeMaster.getSacCode());
                        tCLosData.setBusinessPlace(taxCodeMaster.getState());
                    } else {
                        throw new TcLosException(String.format(ErrorCode.TAXCODE_MASTER_NFND
                                , stateBranchMaster.getStateCode()));
                    }
                } else {
                    throw new TcLosException(String.format(ErrorCode.STATE_BRANCH_MASTER_NFND
                            , branchCode
                            , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId()));
                }
            }

            // This also checks inside the PNDL flag value
            setCustomerNoAndCustType(tCLosData
                    , tcLosGngMapping
                    , postIPA
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader()
                    , dealerEmailMaster
                    , accountsHeadMaster);


            if( TokenCode.DP.getValue().equals(tcLosGngMapping.getAccEntryNumber())) {
                tCLosData.setIncrTranNo(GNGWorkflowConstant.CD.toFaceValue() + (incrementalId-2));
            }else if (TokenCode.DIS.getValue().equals(tcLosGngMapping.getAccEntryNumber())){
                tCLosData.setIncrTranNo(GNGWorkflowConstant.CD.toFaceValue() + (incrementalId-1));
            }else {
                tCLosData.setIncrTranNo(GNGWorkflowConstant.CD.toFaceValue() + incrementalId);
            }


            tcLosDataList.add(tCLosData);
        }

        if (CollectionUtils.isNotEmpty(tcLosDataList)) {
            if(generateCSV) {
                generateCsv(tcLosDataList, refId, path);
            }
            else {
                saveDocumentToDB(tcLosDataList, refId,goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId());
                //sending tcloslist to sap
                sendToSapApi(tcLosDataList, goNoGoCroApplicationResponse);
            }

        }else {
            logger.error("No records found. CSV not written for refID : ", goNoGoCroApplicationResponse.getGngRefId());
            throw new TcLosException(String.format(ErrorCode.NO_RECORDS_ERR, goNoGoCroApplicationResponse.getGngRefId()));
        }
    }

    private void initializeDefaultFields(TCLosData tCLosData) {
        Date date = new Date();

        tCLosData.setDocumentType("DA");
        tCLosData.setTaxAutoFlag("X");
        tCLosData.setBusinessArea("TVSC");
        tCLosData.setCurrencyKey("INR");
        tCLosData.setTransferFlag("N");
        tCLosData.setVendorId("Softcell");

        tCLosData.setDtupDate("");
        tCLosData.setBaseLineDate("");
        tCLosData.setUserName("");
        tCLosData.setBusTran("");
        tCLosData.setGlItemNo("");
        tCLosData.setValueDate("");
        tCLosData.setVendorNo("1");//This is a non-mandatory field but TVS API expects some value for this.
        tCLosData.setAssSplGl("");
        tCLosData.setPanDlNo("0");//This is a non-mandatory field but TVS API expects some value for this.

        tCLosData.setFavour("");
        tCLosData.setChqCity("");
        tCLosData.setAccNo("");
        tCLosData.setIfscCode("");
        tCLosData.setChequeNo("0");//This is a non-mandatory field but TVS API expects some value for this.
        tCLosData.setBrsRef1("");
        tCLosData.setBrsRef2("");
        tCLosData.setBrsRef3("");
        tCLosData.setPayoutTaxFlag("");
        tCLosData.setAssignmentNo("");
        tCLosData.setGlDesc("GLDESC");// This is a non-mandatory field but TVS API expects some value for this.
        tCLosData.setDocumentNo("1");//This is a non-mandatory field but TVS API expects some value for this.
        tCLosData.setInterfaceRemarks("");
        tCLosData.setDocumentYear(new SimpleDateFormat(GngDateUtil.yyyy).format(new Date()));


        //Initialize date fields.

        String strDate = GngDateUtil.getFormattedDate(date, "dd/MM/yyyy");
        tCLosData.setDocumentDate(strDate);
        tCLosData.setPostingDate(strDate);
        tCLosData.setTransactionDate(strDate);
    }

    private void setAmount(TCLosData tCLosData, String fieldName, PostIPA postIPA
            , ExtendedWarrantyDetails extendedWarrantyDetails, CreditVidyaDetails creditVidyaDetails
            , InsurancePremiumDetails insurancePremiumDetails) {
        //1
        if (TCLosGngMappingDetails.LOANDISBURSALACCOUNT_DP.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getTotalRecDwnAmt());
        } //2
        else if (TCLosGngMappingDetails.PROCESSING_FEES.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getProcessingFees());
        } //3
        else if (TCLosGngMappingDetails.EXTENDED_WARRANTY.getValue().equals(fieldName) && extendedWarrantyDetails != null) {
            tCLosData.setAmount(extendedWarrantyDetails.getEwWarrantyPremium());
        } //4
        else if (TCLosGngMappingDetails.INSURANCE_PREMIUM_EMI.getValue().equals(fieldName)  && insurancePremiumDetails != null) {
            if(PaymentMethod.EMI.equals(insurancePremiumDetails.getPaymentMethod())) {
                tCLosData.setAmount(insurancePremiumDetails.getInsurancePremium());
            }
        } //5
        else if (TCLosGngMappingDetails.INSURANCE_PREMIUM_DP.getValue().equals(fieldName)  && insurancePremiumDetails != null) {
            if(PaymentMethod.DOWN_PAYMENT.equals(insurancePremiumDetails.getPaymentMethod())) {
                tCLosData.setAmount(insurancePremiumDetails.getInsuranceDPAmt());
            }
        } //6
        else if (TCLosGngMappingDetails.ADVANCE_EMI.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getAdvanceEmi());
        } //7
        else if (TCLosGngMappingDetails.INITIALHIRE_DP.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount((postIPA.getTotalAssetCost()));
        } //8
        else if (TCLosGngMappingDetails.DEALER_BUYDOWN.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getDealerSubvention());
        } //9
        else if (TCLosGngMappingDetails.OTHER_CHARGES.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getOtherChargesIfAny());
        } //10
        else if (TCLosGngMappingDetails.LOANDISBURSALACCOUNT_DIS.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getLoanDisbursalAmt()); //Db
        } //11
        else if (TCLosGngMappingDetails.DEALER_ACCOUNT.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getNetDisbursalAmount()); //Cr
        } //12
        else if (TCLosGngMappingDetails.SUBVENTION_INCOME.getValue().equals(fieldName) && postIPA != null) {
             tCLosData.setAmount(postIPA.getManufSubventionMbd());
        } //13
        else if (TCLosGngMappingDetails.MANUFACTURER_BUYDOWN.getValue().equals(fieldName) && postIPA != null) {
             tCLosData.setAmount(postIPA.getManufSubventionMbd());
        } //14
        else if (TCLosGngMappingDetails.SOH.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getTotalRecvAmt());
        } //15
        else if (TCLosGngMappingDetails.INITIALHIRE_AGR.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getTotalAssetCost());
        } //16
        else if (TCLosGngMappingDetails.LOANDISBURSALACCOUNT_AGR.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getLoanAccAgrAmt());
        }//17
        else if (TCLosGngMappingDetails.UMFC.getValue().equals(fieldName) && postIPA != null) {
            tCLosData.setAmount(postIPA.getUmfc());
        }
    }

    /**
     * Set the customer_no and customer_type.
     * @param tCLosData
     * @param tcLosGngMapping
     * @param postIPA
     * @param header
     * @param dealerEmailMaster
     * @param accountsHeadMaster
     * @throws TcLosException
     */
    private void setCustomerNoAndCustType(TCLosData tCLosData, TCLosGngMappingMaster tcLosGngMapping
            , PostIPA postIPA, Header header, DealerEmailMaster dealerEmailMaster
            , AccountsHeadMaster accountsHeadMaster) throws TcLosException {
        if (dealerEmailMaster == null) {
            throw new TcLosException(String.format(ErrorCode.DEALERMASTER_NFND, header.getInstitutionId()
                    , header.getDealerId()));
        }
        if (TCLosGngMappingDetails.MANUFACTURER_BUYDOWN.getValue().equals(tcLosGngMapping.getGngField())) {
            if (TlgmAccType.D.name().equals(tcLosGngMapping.getAccType())) {
                String manufacturer = GngUtils.getManufaturerCode(postIPA.getAssetDetails());
                if (manufacturer != null) {
                    //Find master entry from manufacturer.
                    ManufacturerMaster manufacturerMaster = masterRepository.getManufacturer(manufacturer
                            , header.getInstitutionId());
                    if (manufacturerMaster != null) {
                        tCLosData.setCustomerNo(manufacturerMaster.getSapCode());
                        tCLosData.setCustomerType(manufacturerMaster.getSapCode());
                    } else {
                        throw new TcLosException(String.format(ErrorCode.MFR_MASTER_NFND, manufacturer
                                , header.getInstitutionId()));
                    }
                } else {
                    throw new TcLosException(String.format(ErrorCode.ASSET_DTL_NFND));
                }
            }
        } else {
            if (TlgmAccType.D.name().equals(tcLosGngMapping.getAccType())) {
                tCLosData.setCustomerNo(dealerEmailMaster.getDealerSapCode());
            }
            if (AhmPandalFlag.Y.name().equals(accountsHeadMaster.getPandlFlag())) {
                tCLosData.setCustomerType(dealerEmailMaster.getDealerSapCode());
            }
        }
    }

    private void generateCsv(List<TCLosData> tcLosDataList, String refId, String path) throws TcLosException{
        logger.info("Inside generate CSV.");
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter(path + refId + ".csv"));
            String[] columns = CSVHeaderConstant.SAP_FILE_HEADER.split(",");
            csvWriter.writeNext(columns);

            String csvLines[][] = readBeanToCSVLine(tcLosDataList, columns.length);
            for (int i = 0; i < tcLosDataList.size(); i++) {
                csvWriter.writeNext(csvLines[i]);
            }
            csvWriter.close();
        } catch (Exception e) {
            logger.error("Error in writing CSV file.");
            throw new TcLosException(String.format(ErrorCode.CSV_FILE_CREATION_ERR, refId, e.getMessage()));
        }
    }


    private String[][] readBeanToCSVLine(List tcLosDataList, int arrLength) {
        int size = tcLosDataList.size();
        String[][] csvLines = new String[size][arrLength];

        for (int i = 0; i < size; i++) {
            csvLines[i][0] = ((TCLosData) tcLosDataList.get(i)).getInterfaceId();
            csvLines[i][1] = ((TCLosData) tcLosDataList.get(i)).getTokenCode();
            csvLines[i][2] = ((TCLosData) tcLosDataList.get(i)).getDocumentType();
            csvLines[i][3] = ((TCLosData) tcLosDataList.get(i)).getDocumentDate();
            csvLines[i][4] = ((TCLosData) tcLosDataList.get(i)).getPostingDate();
            csvLines[i][5] = ((TCLosData) tcLosDataList.get(i)).getTransactionDate();
            csvLines[i][6] = ((TCLosData) tcLosDataList.get(i)).getUserName();
            csvLines[i][7] = ((TCLosData) tcLosDataList.get(i)).getReferenceNo();
            csvLines[i][8] = ((TCLosData) tcLosDataList.get(i)).getDocumentHead();
            csvLines[i][9] = ((TCLosData) tcLosDataList.get(i)).getBusTran();
            csvLines[i][10] = ((TCLosData) tcLosDataList.get(i)).getGlItemNo();
            csvLines[i][11] = ((TCLosData) tcLosDataList.get(i)).getTaxAutoFlag();
            csvLines[i][12] = ((TCLosData) tcLosDataList.get(i)).getPostingKey();
            csvLines[i][13] = ((TCLosData) tcLosDataList.get(i)).getAccType();
            csvLines[i][14] = ((TCLosData) tcLosDataList.get(i)).getDebitCreditIndicator();
            csvLines[i][15] = ((TCLosData) tcLosDataList.get(i)).getBusinessArea();
            csvLines[i][16] = ((TCLosData) tcLosDataList.get(i)).getTaxCode();
            csvLines[i][17] = ""+((TCLosData) tcLosDataList.get(i)).getAmount();
            csvLines[i][18] = ((TCLosData) tcLosDataList.get(i)).getCurrencyKey();
            csvLines[i][19] = ((TCLosData) tcLosDataList.get(i)).getValueDate();
            csvLines[i][20] = ((TCLosData) tcLosDataList.get(i)).getAssignmentNo();
            csvLines[i][21] = ((TCLosData) tcLosDataList.get(i)).getItemText();
            csvLines[i][22] = ((TCLosData) tcLosDataList.get(i)).getCostCenter();
            csvLines[i][23] = ((TCLosData) tcLosDataList.get(i)).getGlAccountNo();
            csvLines[i][24] = ((TCLosData) tcLosDataList.get(i)).getGlDesc();
            csvLines[i][25] = ((TCLosData) tcLosDataList.get(i)).getCustomerNo();
            csvLines[i][26] = ((TCLosData) tcLosDataList.get(i)).getVendorNo();
            csvLines[i][27] = ((TCLosData) tcLosDataList.get(i)).getSpecialGl();
            csvLines[i][28] = ((TCLosData) tcLosDataList.get(i)).getAssSplGl();
            csvLines[i][29] = ((TCLosData) tcLosDataList.get(i)).getBaseLineDate();
            csvLines[i][30] = ((TCLosData) tcLosDataList.get(i)).getPandlFlag();
            csvLines[i][31] = ((TCLosData) tcLosDataList.get(i)).getPanDlNo();
            csvLines[i][32] = ((TCLosData) tcLosDataList.get(i)).getCostCode();
            csvLines[i][33] = ((TCLosData) tcLosDataList.get(i)).getProfitCenter();
            csvLines[i][34] = ((TCLosData) tcLosDataList.get(i)).getProduct();
            csvLines[i][35] = ((TCLosData) tcLosDataList.get(i)).getBranchCode();
            csvLines[i][36] = ((TCLosData) tcLosDataList.get(i)).getTransferFlag();
            csvLines[i][37] = ((TCLosData) tcLosDataList.get(i)).getInterfaceRemarks();
            csvLines[i][38] = ((TCLosData) tcLosDataList.get(i)).getDtupDate();
            csvLines[i][39] = ((TCLosData) tcLosDataList.get(i)).getIncrTranNo();
            csvLines[i][40] = ((TCLosData) tcLosDataList.get(i)).getDocumentNo();
            csvLines[i][41] = ((TCLosData) tcLosDataList.get(i)).getDocumentYear();
            csvLines[i][42] = ((TCLosData) tcLosDataList.get(i)).getFavour();
            csvLines[i][43] = ((TCLosData) tcLosDataList.get(i)).getChqCity();
            csvLines[i][44] = ((TCLosData) tcLosDataList.get(i)).getAccNo();
            csvLines[i][45] = ((TCLosData) tcLosDataList.get(i)).getIfscCode();
            csvLines[i][46] = ((TCLosData) tcLosDataList.get(i)).getChequeNo();
            csvLines[i][47] = ((TCLosData) tcLosDataList.get(i)).getBrsRef1();
            csvLines[i][48] = ((TCLosData) tcLosDataList.get(i)).getBrsRef2();
            csvLines[i][49] = ((TCLosData) tcLosDataList.get(i)).getBrsRef3();
            csvLines[i][50] = ((TCLosData) tcLosDataList.get(i)).getCustomerType();
            csvLines[i][51] = ((TCLosData) tcLosDataList.get(i)).getPayoutTaxFlag();
            csvLines[i][52] = ((TCLosData) tcLosDataList.get(i)).getPaymentMode();
            csvLines[i][53] = ((TCLosData) tcLosDataList.get(i)).getVendorId();
            csvLines[i][54] = ((TCLosData) tcLosDataList.get(i)).getBusinessPlace();
            csvLines[i][55] = ((TCLosData) tcLosDataList.get(i)).getSacCode();
        }
        return csvLines;
    }

    private boolean saveDocumentToDB(List<TCLosData> tcLosDataList, String refId, String institutionId) {
        logger.info("Inside save document");

        FileHeader fileHeader = new FileHeader();
        fileHeader.setApplicantId("System_Admin");
        fileHeader.setApplicationId("1609162SOFTCELL");
        fileHeader.setInstitutionId(institutionId);
        fileHeader.setSourceId("System");
        fileHeader.setApplicationSource("WEB");
        fileHeader.setRequestType("JSON");
        fileHeader.setCroId("System");
        fileHeader.setDealerId("DEFAULT");
        fileHeader.setDsaId("TVS_DSA");

        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileId("01");
        uploadFileDetails.setFileName(refId + ".csv");
        uploadFileDetails.setFileType("application/csv");
        uploadFileDetails.setFileData("");
        uploadFileDetails.setDocumentType("SAPCSV");
        uploadFileDetails.setDocumentSubType("DISBURSEMENT");
        uploadFileDetails.setDeleted(false);

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setGonogoReferanceId("" + refId);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);

        fileUploadRequest.getFileHeader().setDateTime(new Date());

        String fileName = fileUploadRequest.getUploadFileDetails().getFileName();
        String fileType = fileUploadRequest.getUploadFileDetails().getFileType();

        String res = null;
        try {
            StringWriter writer = new StringWriter();
            CSVWriter csvWriter = new CSVWriter(writer);
            String[] columns = CSVHeaderConstant.SAP_FILE_HEADER.split(",");
            csvWriter.writeNext(columns);
            String csvLines[][] = readBeanToCSVLine(tcLosDataList, columns.length);
            for (int i = 0; i < tcLosDataList.size(); i++) {
                csvWriter.writeNext(csvLines[i]);
            }
            InputStream myInputStream = new ByteArrayInputStream(writer.getBuffer().toString().getBytes());
            res = uploadFileRepository.store(myInputStream, fileName, fileType, fileUploadRequest);
        } catch (Exception e) {
            logger.error(e.toString());
            e.printStackTrace();
        }
        if (res != null) {
            logger.info("File stored successfully.");
            return true;
        } else {
            logger.error("Problem in storing the file.");
            return false;
        }
    }

    //sending tcLosData to connector
    public void sendToSapApi(List tcLosData, GoNoGoCroApplicationResponse goNoGoCroApplicationResponse) throws TcLosException {
        Collection<Error> errors;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //create log
        ActivityLogs activityLog = auditHelper.createActivityLog(null, goNoGoCroApplicationResponse.getApplicationRequest().getHeader());
        activityLog.setAction(GNGWorkflowConstant.SAP_API_ENQUIRY.toFaceValue());


        //Read the configuration.
        WFJobCommDomain sapConfig = workFlowCommunicationManager.getWfCommDomainJobByType
                (goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId(), UrlType.TVS_SAP_ENTRY.toValue());

        //throw exception if configuration is not found in wf_communication_setting collection
        if (null == sapConfig) {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.TC_LOS_CONFIGURATION_NT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(ErrorCode.TC_LOS_CONFIGURATION_NT_FOUND);


            throw new TcLosException(String.format(ErrorCode.TC_LOS_CONFIGURATION_NT_FOUND, errors
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId()));
        }

        // if sapconfig is not null create request for sap enquiry
        TCLosInputRequest sapRequest = TCLosInputRequest.builder()
                .action(GNGWorkflowConstant.SAP_ACTION.toFaceValue()).name(GNGWorkflowConstant.INSERT_SAP_INTERFACE.toFaceValue()).key(sapConfig.getLicenseKey()).tcLosData(tcLosData)
                .build();

        TcLosReqReslog tcLosReqReslog = new TcLosReqReslog();
        tcLosReqReslog.setRefId(goNoGoCroApplicationResponse.getGngRefId());
        tcLosReqReslog.setRequest(sapRequest);
        tcLosReqReslog.setRequestDt(new Date());
        String url = Arrays.asList(sapConfig.getBaseUrl(), sapConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));

        //Make the API call
        TCLosResponse sapResponse = null;
        try {
            sapResponse = (TCLosResponse) TransportUtils.postJsonRequest(sapRequest,
                    url, TCLosResponse.class);
            tcLosReqReslog.setResponse(sapResponse);
            tcLosReqReslog.setResponseDt(new Date());
        } catch (Exception e) {
            tcLosReqReslog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            tcLosReqReslog.setFailureReason(e.getMessage());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(e.getMessage());
            activityLog.setRefId(goNoGoCroApplicationResponse.getGngRefId());
            externalAPILogRepository.savetcLosReqResLog(tcLosReqReslog);
            throw new TcLosException(String.format(ErrorCode.EXCEPTION_CALLING_API,
                            e.getMessage()));

        }



        //after getting success from sap api
        if (null != sapResponse && null == sapResponse.getError()) {
            sapRequest.setAction(UrlType.TVS_SAP_ENTRY.name());
            activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            activityLog.setRefId(goNoGoCroApplicationResponse.getGngRefId());
            tcLosReqReslog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
            //save logs to collection
            externalAPILogRepository.savetcLosReqResLog(tcLosReqReslog);
        }
        else if(sapResponse != null && sapResponse.getError() != null) {
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            sapRequest.setAction(UrlType.TVS_SAP_ENTRY.name());
            activityLog.setCustomMsg(sapResponse.getError().getMessage());
            activityLog.setRefId(goNoGoCroApplicationResponse.getGngRefId());
            tcLosReqReslog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            tcLosReqReslog.setFailureReason(sapResponse.getError().getMessage());
            //save logs to collection
            externalAPILogRepository.savetcLosReqResLog(tcLosReqReslog);
            throw new TcLosException(String.format(sapResponse.getError().getMessage()
                    , goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId()));

        }
        //Third Party Server Error
        else {
            errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.TC_LOS_FAILED)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(ErrorCode.EXTERNAL_SERVICE_FAILURE);
            tcLosReqReslog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            tcLosReqReslog.setFailureReason(ErrorCode.TC_LOS_FAILED);

            //save logs to collection
            externalAPILogRepository.savetcLosReqResLog(tcLosReqReslog);
            throw new TcLosException(String.format(ErrorCode.EXTERNAL_SERVICE_FAILURE,
                     goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getInstitutionId()));

        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        //create log
        activityEventPublisher.publishEvent(activityLog);
    }

}
