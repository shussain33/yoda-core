package com.softcell.service.impl;


import com.mongodb.WriteResult;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.HierarchyRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.gonogo.model.masters.*;
import com.softcell.gonogo.model.request.SchemeMetadataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.ApplicableSchemeReportDetails;
import com.softcell.gonogo.model.request.master.DeviationMasterRequest;
import com.softcell.gonogo.model.request.master.GetAllElecServProvRequest;
import com.softcell.gonogo.model.request.master.HierarchyMasterRequest;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.DeleteCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.InsertCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.SourcingDetailsRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.UpdateCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.GetSchemeZipReportRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.*;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.gonogo.model.response.Table;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.master.commongeneralmaster.CommonGeneralMasterResponse;
import com.softcell.gonogo.model.response.master.commongeneralmaster.SourcingDetails;
import com.softcell.gonogo.model.response.master.schemedatemapping.ManufacturerDescWithCcid;
import com.softcell.gonogo.model.response.master.schemedatemapping.SchemeApproveDeclineResponse;
import com.softcell.gonogo.model.response.master.schemedatemapping.SchemeInformationResponse;
import com.softcell.gonogo.model.response.master.schemedatemapping.UpdateSchemeDateMappingDetailsResponse;
import com.softcell.gonogo.service.factory.MasterDataViewBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.parse.SchemeReportParser;
import com.softcell.reporting.builder.SchemeReportConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.service.MasterDataViewManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author mahesh
 */
@Service
public class MasterDataViewManagerImpl implements MasterDataViewManager {

    @Autowired
    private MasterDataViewRepository masterDataViewRepository;

    @Autowired
    private MasterDataViewBuilder masterDataViewBuilder;

    @Autowired
    private LookupService lookupService;

    @Override
    public BaseResponse viewAssetModelMasterDetails(String institutionId, String product, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(product, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, product);

        if (StringUtils.isNotBlank(productAliasName)) {

            resultList = masterDataViewRepository.getAssetModelDetails(institutionId, limit, skip, productAliasName);

            if (null != resultList && null != resultList.getData()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllSchemeIds(SchemeMasterRequest schemeMasterRequest) {

        BaseResponse baseResponse;
        List<SchemeInformationResponse> schemeInformationResponseList;

        String institutionId = schemeMasterRequest.getHeader().getInstitutionId();
        String productName = schemeMasterRequest.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            schemeInformationResponseList = masterDataViewRepository.viewAllSchemeIds(institutionId, productAliasName);

            if (null != schemeInformationResponseList && !schemeInformationResponseList.isEmpty()) {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, schemeInformationResponseList);

            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getSchemeDateMappingDeatils(
            GetSchemeDateMappingDetailsRequest schemeDateMappingDetailsRequest) throws Exception {

        SchemeDetailsDateMapping schemeDetailsDateMapping;
        BaseResponse baseResponse;

        String institutionId = schemeDateMappingDetailsRequest.getHeader().getInstitutionId();
        String productName = schemeDateMappingDetailsRequest.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            schemeDetailsDateMapping = masterDataViewRepository
                    .viewSchemeDateMappingDeatils(schemeDateMappingDetailsRequest, productAliasName);

            if (null != schemeDetailsDateMapping) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, schemeDetailsDateMapping);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse validateAssetCost(
            AssetCostValidationRequest assetCostValidationRequest) {

        BaseResponse baseResponse;

        String institutionId = assetCostValidationRequest.getHeader().getInstitutionId();
        String productName = assetCostValidationRequest.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, masterDataViewRepository
                    .validateAssetCost(assetCostValidationRequest, productAliasName));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse viewPinCodeMasterDetails(String institutionId, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

        resultList = masterDataViewRepository.getPinCodeMasterDetails(institutionId, limit, skip);

        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse viewEmployerMasterDetails(String institutionId, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

        resultList = masterDataViewRepository.getEmployerMasterDetails(institutionId, limit, skip);

        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse viewDealerBranchMasterDetails(
            String institutionId, String product, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(product, institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, product);

        resultList = masterDataViewRepository
                .getDealerBranchMasterDetails(institutionId, productAliasName, limit, skip);

        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse viewDealerEmailMasterDetails(
            String institutionId, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

        resultList = masterDataViewRepository
                .getDealerEmailMasterDetails(institutionId, limit, skip);

        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse viewSchemeModelDealerCityMappingMaster(
            String institutionId, int limit, int skip) {

        BaseResponse baseResponse;
        Table resultList;

        resultList = masterDataViewRepository
                .getSchemeModelDealerCityMappingDetails(institutionId, limit, skip);

        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override

    public BaseResponse updateSchemeDateMappingDetails(
            UpdateSchemeDateMappingDetailsRequest updateSchemeDetailsRequest) throws Exception {
        UpdateSchemeDateMappingDetailsResponse updateSchemeDetailsResponse = new UpdateSchemeDateMappingDetailsResponse();
        /**
         * First check user role i.e.it is "maker" or "self checker" or
         * "Scheme-Admin"
         */

        String institutionId = updateSchemeDetailsRequest.getHeader().getInstitutionId();
        String productName = updateSchemeDetailsRequest.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotEmpty(productAliasName)) {

            String userRole = updateSchemeDetailsRequest
                    .getSchemeDetailsDateMapping().getUserRole();
            if (userRole.equals(Status.MAKER.name())
                    || userRole.equals(Status.SELF_CHECKER.name())
                    || userRole.equals(Status.SCHEME_ADMIN.name())) {

                if (masterDataViewRepository
                        .updateSchemeDateMappingDetails(updateSchemeDetailsRequest, productAliasName)) {
                    updateSchemeDetailsResponse.setStatus(Status.SUCCESS.name());
                    updateSchemeDetailsResponse
                            .setResMessage("Scheme Data Updated Successfully");
                } else {
                    updateSchemeDetailsResponse.setStatus(Status.FAILED.name());
                    updateSchemeDetailsResponse
                            .setResMessage("Problem In Updation Of Scheme Data");
                }

            } else {
                updateSchemeDetailsResponse.setStatus(Status.FAILED.name());
                updateSchemeDetailsResponse
                        .setResMessage("User Has No Authority To Make Changes.");
            }
        } else {
            return GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, updateSchemeDetailsResponse);
    }

    @Override
    public BaseResponse getManufacturerAgainstCcid(
            GetSchemeInformationRequest getSchemeInformationRequest) throws Exception {

        BaseResponse baseResponse;
        Set<String> manufacturerSet;
        String institutionId = getSchemeInformationRequest.getHeader().getInstitutionId();
        String productName = getSchemeInformationRequest.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            manufacturerSet = masterDataViewRepository
                    .getManufacturerAgainstCcid(getSchemeInformationRequest, productAliasName);

            if (null != manufacturerSet && !manufacturerSet.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, manufacturerSet);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllState(String institutionId) throws Exception {

        BaseResponse baseResponse;
        Set<String> allStateSet;

        allStateSet = masterDataViewRepository.getAllState(institutionId);

        if (null != allStateSet && !allStateSet.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, allStateSet);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllCityAgainstState(
            GetAllCityAgainstStateRequest getAllCityAgainstStateRequest) {

        BaseResponse baseResponse;
        Set<InclCityWithStateName> allCityWithStateSet;

        allCityWithStateSet = masterDataViewRepository
                .getAllCityAgainstState(getAllCityAgainstStateRequest);

        if (!CollectionUtils.isEmpty(allCityWithStateSet)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, allCityWithStateSet);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse updateSchemeDealerMappingDetails(
            SchemeDealerMappingDetails schemeDealerMappingDetails) throws Exception {

        UpdateSchemeDateMappingDetailsResponse updateSchemeDetailsResponse = new UpdateSchemeDateMappingDetailsResponse();


        String institutionId = schemeDealerMappingDetails.getHeader().getInstitutionId();
        String productName = schemeDealerMappingDetails.getHeader().getProduct().name();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        /**
         * check product configuration in the cache .if not found then throw the configuration not found error.
         */
        if (StringUtils.isNotBlank(productAliasName)) {

            String userRole = schemeDealerMappingDetails.getUserRole();
            /**
             * First check user role i.e. it is "self checker" or "maker" or
             * "Scheme_Admin"
             */
            if (userRole.equals(Status.MAKER.name())
                    || userRole.equals(Status.SELF_CHECKER.name())
                    || userRole.equals(Status.SCHEME_ADMIN.name())) {

                if (masterDataViewRepository
                        .updateSchemeDealerMappingDetails(schemeDealerMappingDetails, productAliasName)) {
                    updateSchemeDetailsResponse
                            .setResMessage("Data Updated Successfully");
                    updateSchemeDetailsResponse.setStatus(Status.SUCCESS.name());
                } else {
                    updateSchemeDetailsResponse
                            .setResMessage("Problem In Data Updation");
                    updateSchemeDetailsResponse.setStatus(Status.FAILED.name());
                }

            } else {
                updateSchemeDetailsResponse
                        .setResMessage("User Has No Rights To Make Changes.");
                updateSchemeDetailsResponse.setStatus(Status.FAILED.name());

            }
        } else {
            GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, updateSchemeDetailsResponse);
    }

    @Override
    public BaseResponse getSchemeDealerMappingDetails(
            GetSchemeDealerMappingRequest getSchemeDealerMappingRequest) throws Exception {

        SchemeDealerMappingDetails schemeDealerMappingDetails = null;

        String institutionId = getSchemeDealerMappingRequest.getHeader().getInstitutionId();
        String productName = getSchemeDealerMappingRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            schemeDealerMappingDetails = masterDataViewRepository
                    .getSchemeDealerMappingDataAgainstSchemeId(getSchemeDealerMappingRequest,
                            productAliasName);
        } else {
            GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, schemeDealerMappingDetails);
    }

    @Override
    public BaseResponse getAllAssetCategoryAgainstManfctr(
            GetAllAssetCtgrAgainstMnfcRequest assetCtgrAgainstMnfcRequest) {

        BaseResponse baseResponse;
        Set<IncludedAssetCtgWithManfc> assetCtgWithManfcList;

        String institutionId = assetCtgrAgainstMnfcRequest.getHeader().getInstitutionId();
        String productName = assetCtgrAgainstMnfcRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            assetCtgWithManfcList = masterDataViewRepository.viewAllAssetCtgrAgainstManfctr(assetCtgrAgainstMnfcRequest, productAliasName);

            if (!CollectionUtils.isEmpty(assetCtgWithManfcList)) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assetCtgWithManfcList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllModelAgainstAsstCtgMnfc(
            GetAllModelAgainstAsstCtgRequest getAllModelAgainstAsstCtgRequest) {

        BaseResponse baseResponse;
        Set<IncludedModelIdWithInformation> allModelNoList;

        String institutionId = getAllModelAgainstAsstCtgRequest.getHeader().getInstitutionId();
        String productName = getAllModelAgainstAsstCtgRequest.getHeader().getProduct().name();
        /**
         * check product configuration in the cache if not found then return the error message otherwise
         * proceed.
         */
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isNotBlank(productAliasName)) {

            allModelNoList = masterDataViewRepository
                    .viewAllModelAgainstAsstCtgMnfc(getAllModelAgainstAsstCtgRequest, productAliasName);

            if (!CollectionUtils.isEmpty(allModelNoList)) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, allModelNoList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllDealerAgainstCity(
            GetAllDealerAgainstCityRequest getAllDealerAgainstCityRequest) {

        Set<InclDealerIdWithStateCity> dealerDetails;
        BaseResponse baseResponse;

        dealerDetails = masterDataViewRepository
                .viewAllDealerAgainstCity(getAllDealerAgainstCityRequest);

        if (!CollectionUtils.isEmpty(dealerDetails)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dealerDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getFilteredSchemes(
            SchemeMetadataRequest schemeMetadataRequest) throws Exception {

        String modelNo = schemeMetadataRequest.getModelNo();
        String maufacturerDesc = schemeMetadataRequest.getManufacturerDesc();
        String catgDesc = schemeMetadataRequest.getCatgDesc();
        String dealerID = schemeMetadataRequest.getHeader().getDealerId();
        String assetMake = schemeMetadataRequest.getMake();

        String institutionId = schemeMetadataRequest.getHeader().getInstitutionId();
        String productName = schemeMetadataRequest.getHeader().getProduct().name();
        Set<String> nonVanillaSchemeIds = null;

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        if (StringUtils.isBlank(productAliasName)) {

            return GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        /**
         * to find modelId fetch asset model master.
         */
        AssetModelMaster assetModelMaster = getAssetModelMaster(modelNo,
                catgDesc, maufacturerDesc, assetMake, productAliasName, institutionId);

        if (null != assetModelMaster) {
            if (!StringUtils.contains(modelNo, "VANILLA") && StringUtils.isNotBlank(modelNo)) {

                nonVanillaSchemeIds = getNonVanillaSchemeIds(assetModelMaster.getManufacturerId(), productAliasName, institutionId);

            } else {
                // return all vanilla scheme from scheme master
                return GngUtils.checkBlankResponseForSchemeMaster(getVanilla(productAliasName, institutionId));
            }
        }

        if (null != nonVanillaSchemeIds && !nonVanillaSchemeIds.isEmpty()) {

            return GngUtils.checkBlankResponseForSchemeMaster(getFilteredSchemeIdsAccordingToAssetMappingAndDateMapping(nonVanillaSchemeIds,
                    assetModelMaster, dealerID, productAliasName, institutionId));
        }
        // return all General Scheme from scheme master
        return GngUtils.checkBlankResponseForSchemeMaster(getGeneralSchemes(productAliasName, institutionId));
    }


    private List<SchemeMasterData> getFilteredSchemeIdsAccordingToAssetMappingAndDateMapping(
            Set<String> nonVanillaSchemeIdsSet, AssetModelMaster assetModelMaster,
            String dealerID, String productAliasName, String institutionId) throws Exception {

        Set<String> schemeIdsSetAfterAssetMapping = new HashSet<>();
        Set<String> schemeIdsSetAfterDateMapping = null;

        for (String nonVanillaSchemeId : nonVanillaSchemeIdsSet) {
            if (checkAssetMappingOfSchemeIds(nonVanillaSchemeId, assetModelMaster, dealerID, productAliasName, institutionId)) {
                schemeIdsSetAfterAssetMapping.add(nonVanillaSchemeId);
            }
        }


        if (!schemeIdsSetAfterAssetMapping.isEmpty()) {
            schemeIdsSetAfterDateMapping = checkDateMappingOfSchemeIds(schemeIdsSetAfterAssetMapping, productAliasName, institutionId);
        }
        if (null != schemeIdsSetAfterDateMapping
                && !schemeIdsSetAfterDateMapping.isEmpty()) {

            List<SchemeMasterData> schemeMasterData = masterDataViewRepository
                    .getSchemeMasterData(schemeIdsSetAfterDateMapping, productAliasName, institutionId);

            if (schemeMasterData != null && !schemeMasterData.isEmpty()) {
                return schemeMasterData;
            }

        }
        return getGeneralSchemes(productAliasName, institutionId);

    }

    private boolean checkAssetMappingOfSchemeIds(String filterSchemeId,
                                                 AssetModelMaster assetModelMaster, String dealerID, String productAliasName, String institutionId) {

        if (masterDataViewRepository.checkModelSchemeMapping(filterSchemeId,
                assetModelMaster.getModelId(), productAliasName, institutionId)
                && masterDataViewRepository.checkDealerSchemeMapping(
                filterSchemeId, dealerID, productAliasName, institutionId)) {
            return true;
        } else {
            return false;
        }
    }

    private List<SchemeMasterData> getGeneralSchemes(String productAliasName, String institutionId) {
        return masterDataViewRepository
                .getGeneralSchemeMasterDetails(productAliasName, institutionId);

    }

    private Set<String> checkDateMappingOfSchemeIds(Set<String> schemeIdsSetAfterAssetMapping, String productAliasName, String institutionId) throws Exception {
        // 3. From Scheme-Validity Mapping, get the subset of schemeids
        // which are valid on the date of submission.

        Set<String> schemeIdsAfterDateMapping = new HashSet<>();
        List<SchemeDetailsDateMapping> schemeDetailsDateMappingList = masterDataViewRepository
                .getSchemeDetailsDateMapping(schemeIdsSetAfterAssetMapping, productAliasName, institutionId);

        if (null == schemeDetailsDateMappingList || schemeDetailsDateMappingList.isEmpty()) {
            return schemeIdsAfterDateMapping;
        }

        for (SchemeDetailsDateMapping schemeDetailsDateMapping : schemeDetailsDateMappingList) {
            Date validFrom = schemeDetailsDateMapping.getValidFromDate();
            Date validTo = schemeDetailsDateMapping.getValidToDate();
            // Check current date is between To and From date
            if (GngDateUtil.isDateWithinRange(validFrom, validTo)
                    && schemeDetailsDateMapping.getSchemeStatus().equals(
                    Status.APPROVED.name())
                    && !schemeDetailsDateMapping.isUpdateStatus()) {
                /**
                 *  add the schemeId in the finalDateFilteredschemeId and after remove it from
                 *  list according to condition.
                 */

                schemeIdsAfterDateMapping.add(schemeDetailsDateMapping
                        .getSchemeID());

                Set<Date> excludedDatesSet = schemeDetailsDateMapping
                        .getExcludedDate();
                Set<Date> includedDatesSet = schemeDetailsDateMapping
                        .getIncludedDate();
                // if excluded date is not empty ,check current date is in
                // excluded list.
                if (excludedDatesSet != null && !excludedDatesSet.isEmpty()) {
                    for (Date excludedDate : excludedDatesSet) {
                        if (GngDateUtil.isDateMatch(excludedDate)) {
                            // Check if the current date is in exclusion list
                            schemeIdsAfterDateMapping
                                    .remove(schemeDetailsDateMapping
                                            .getSchemeID());
                        }
                    }
                    // if included list is not empty
                } else if (includedDatesSet != null
                        && !includedDatesSet.isEmpty()) {
                    boolean flag = true;
                    for (Date includedDate : includedDatesSet) {
                        if (GngDateUtil.isDateMatch(includedDate)) {
                            // check if current date is in inclusion list
                            flag = false;
                            schemeIdsAfterDateMapping
                                    .add(schemeDetailsDateMapping.getSchemeID());
                            break;
                        }
                    }
                    if (flag) {
                        schemeIdsAfterDateMapping
                                .remove(schemeDetailsDateMapping.getSchemeID());
                    }
                }

            } else {
                // if current date is not between From and to Date.
                schemeIdsAfterDateMapping.remove(schemeDetailsDateMapping
                        .getSchemeID());
            }
        }
        return schemeIdsAfterDateMapping;
    }


    private AssetModelMaster getAssetModelMaster(String modelNo,
                                                 String catgDesc, String maufacturerDesc, String assetMake, String productAliasName, String institutionId) throws Exception {

        return masterDataViewRepository.getAssetModelMaster(modelNo, catgDesc,
                maufacturerDesc, assetMake, productAliasName, institutionId);

    }


    private Set<String> getNonVanillaSchemeIds(String manufacturerId, String productAliasName, String institutionId) throws Exception {

        return masterDataViewRepository
                .getNonVanillaSchemeIds(manufacturerId, productAliasName, institutionId);

    }

    private List<SchemeMasterData> getVanilla(String productAliasName, String institutionId) throws Exception {

        return masterDataViewRepository.getVanillaSchemeMasterDetails("", productAliasName, institutionId);

    }


    @Override
    public BaseResponse getAssetMakeAgainstMnfcAndCtg(
            GetAllAssetMakeAgainstMnfCtgRequest getAllAssetMakeAgainstMnfCtgRequest) throws Exception {

        Set<IncludedMakeWithCtgMnfcr> includedMakeWithCtgMnfcrs;
        BaseResponse baseResponse;

        String institutionId = getAllAssetMakeAgainstMnfCtgRequest.getHeader().getInstitutionId();
        String productName = getAllAssetMakeAgainstMnfCtgRequest.getHeader().getProduct().name();
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            includedMakeWithCtgMnfcrs = masterDataViewRepository
                    .getAssetMakeAgainstMnfcCtg(getAllAssetMakeAgainstMnfCtgRequest, productAliasName);

            if (!CollectionUtils.isEmpty(includedMakeWithCtgMnfcrs)) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, includedMakeWithCtgMnfcrs);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;

    }


    @Override
    public BaseResponse getConditionalScheme(
            GetConditionalSchemeRequest getFilteredSchemeRequest) {

        String typeOfScheme = getFilteredSchemeRequest.getTypeOfScheme();
        List<SchemeInformationResponse> schemeInformationResponseList;
        String productName = getFilteredSchemeRequest.getHeader().getProduct().name();
        String institutionId = getFilteredSchemeRequest.getHeader().getInstitutionId();
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        BaseResponse baseResponse;

        if (StringUtils.isNotBlank(productAliasName)) {
            if (typeOfScheme.equals(Status.ALL.name())) {
                schemeInformationResponseList = masterDataViewRepository.viewAllSchemeIds(institutionId, productAliasName);
            } else if (typeOfScheme.equals(Status.GENERAL.name())) {
                schemeInformationResponseList = masterDataViewRepository.getGeneralScheme(institutionId, productAliasName);
            } else if (typeOfScheme.equals(Status.VANILLA.name())) {
                schemeInformationResponseList = masterDataViewRepository.getVanillaScheme(institutionId, productAliasName);
            } else {
                schemeInformationResponseList = masterDataViewRepository
                        .getSchemeAgainstManufacturer(typeOfScheme, institutionId, productAliasName);
            }

            if (null != schemeInformationResponseList && !schemeInformationResponseList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, schemeInformationResponseList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getAllManufacturer(
            GetAllManufacturerRequest getAllManufacturerRequest) {

        List<ManufacturerDescWithCcid> manufacturerDescWithCcidsList;
        BaseResponse baseResponse;

        String productName = getAllManufacturerRequest.getHeader().getProduct().name();
        String institutionId = getAllManufacturerRequest.getHeader().getInstitutionId();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {

            manufacturerDescWithCcidsList = masterDataViewRepository.getAllManufacturer(getAllManufacturerRequest, productAliasName);

            if (null != manufacturerDescWithCcidsList && !manufacturerDescWithCcidsList.isEmpty()) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, manufacturerDescWithCcidsList);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return baseResponse;
    }

    /**
     *
     * @param getAllElecServProvRequest
     * @return
     */
    @Override
    public BaseResponse getAllElecServProviders(GetAllElecServProvRequest getAllElecServProvRequest) {
        BaseResponse baseResponse;
        String institutionId = getAllElecServProvRequest.getHeader().getInstitutionId();
        Table resultList = masterDataViewRepository.getAllElecServProviders(institutionId);
        if (null != resultList && null != resultList.getData()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public List<ApplicableSchemeDetails> getAllApplicableScheme(
            GetAllApplicableSchemeRequest getAllApplicableSchemeRequest) {

        List<ApplicableSchemeDetails> applicableSchemeDetailsList = new ArrayList<>();
        String typeOfScheme = getAllApplicableSchemeRequest.getTypeOfScheme();

        String productName = getAllApplicableSchemeRequest.getHeader().getProduct().name();
        String institutionId = getAllApplicableSchemeRequest.getHeader().getInstitutionId();

//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);

        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);


        List<SchemeDetailsDateMapping> schemeDateMappingList = masterDataViewRepository
                .getAllSchemeDateMappingScheme(typeOfScheme, productAliasName, institutionId);

        if (null != schemeDateMappingList) {
            for (SchemeDetailsDateMapping schemeDetailsDateMapping : schemeDateMappingList) {

                populateSchemeDateMapping(schemeDetailsDateMapping,
                        applicableSchemeDetailsList, typeOfScheme, productAliasName, institutionId);
            }

        }
        /**
         * get List of schemeId which having partial data i.e date mapping done but dealer mapping not done.
         */
        List<String> partialSchemeList = masterDataViewRepository
                .getAllPartialDealerMappingSchemeId(typeOfScheme, productAliasName, institutionId);

        /**
         * if partial data found , then populate the partial data
         */
        if (null != partialSchemeList && !partialSchemeList.isEmpty()) {

            List<SchemeDealerMappingDetails> schemeDealremappingList = masterDataViewRepository
                    .getAllPartialDealerMappingSchemeData(partialSchemeList,
                            typeOfScheme, productAliasName, institutionId);
            if (null != schemeDealremappingList
                    && !schemeDealremappingList.isEmpty()) {
                for (SchemeDealerMappingDetails schemeDealerMappingDetails : schemeDealremappingList) {
                    ApplicableSchemeDetails applicableSchemeDetails = new ApplicableSchemeDetails();
                    populateSchemeDealerMapping(applicableSchemeDetails,
                            schemeDealerMappingDetails);
                    applicableSchemeDetails
                            .setSchemeID(schemeDealerMappingDetails
                                    .getSchemeID());
                    applicableSchemeDetails
                            .setSchemeStatus(schemeDealerMappingDetails
                                    .getSchemeStatus());
                    applicableSchemeDetails
                            .setSchemeDesc(schemeDealerMappingDetails
                                    .getSchemeDesc());
                    /**
                     * set partial data blank or null
                     */
                    applicableSchemeDetails.setValidFromDate(null);
                    applicableSchemeDetails.setValidToDate(null);
                    applicableSchemeDetails
                            .setIncludedDate(new TreeSet<Date>());
                    applicableSchemeDetails
                            .setExcludedDate(new TreeSet<Date>());
                    applicableSchemeDetails
                            .setMakerId(schemeDealerMappingDetails.getMakerID());
                    applicableSchemeDetailsList.add(applicableSchemeDetails);
                }

            }
        }

        return applicableSchemeDetailsList;
    }

    private void populateSchemeDateMapping(
            SchemeDetailsDateMapping schemeDetailsDateMapping,
            List<ApplicableSchemeDetails> applicableSchemeDetailsList,
            String typeOfScheme, String productAliasName, String institutionId) {

        ApplicableSchemeDetails applicableSchemeDetails = new ApplicableSchemeDetails();
        applicableSchemeDetails.setSchemeID(schemeDetailsDateMapping
                .getSchemeID());
        applicableSchemeDetails.setSchemeDesc(schemeDetailsDateMapping
                .getSchemeDesc());
        applicableSchemeDetails.setValidFromDate(schemeDetailsDateMapping
                .getValidFromDate());
        applicableSchemeDetails.setValidToDate(schemeDetailsDateMapping
                .getValidToDate());
        applicableSchemeDetails.setIncludedDate(schemeDetailsDateMapping
                .getIncludedDate());
        applicableSchemeDetails.setExcludedDate(schemeDetailsDateMapping
                .getExcludedDate());
        applicableSchemeDetails.setMakerId(schemeDetailsDateMapping
                .getMakerID());
        applicableSchemeDetails.setMakeDate(schemeDetailsDateMapping
                .getMakeDate());
        applicableSchemeDetails.setCheckerId(schemeDetailsDateMapping
                .getCheckerId());
        applicableSchemeDetails.setCheckDate(schemeDetailsDateMapping
                .getCheckDate());
        applicableSchemeDetails.setSchemeStatus(schemeDetailsDateMapping
                .getSchemeStatus());
        applicableSchemeDetails.setProductFlag(productAliasName);

        SchemeDealerMappingDetails schemeDealerMappingDetails = masterDataViewRepository
                .getApplicableAssetDetails(
                        schemeDetailsDateMapping.getSchemeID(), typeOfScheme, productAliasName, institutionId);
        /**
         * check schemeDealerMapping if not empty then populate
         */
        if (null != schemeDealerMappingDetails) {
            populateSchemeDealerMapping(applicableSchemeDetails,
                    schemeDealerMappingDetails);

        } else {
            // else set empty set.
            applicableSchemeDetails.setAssetMake(new HashSet<>());
            applicableSchemeDetails.setAssetCategory(new HashSet<>());
            applicableSchemeDetails.setMaufacturer(new HashSet<>());
        }
        applicableSchemeDetailsList.add(applicableSchemeDetails);

    }

    private void populateSchemeDealerMapping(
            ApplicableSchemeDetails applicableSchemeDetails,
            SchemeDealerMappingDetails schemeDealerMappingDetails) {
        Set<String> assetCtgWithManfcSet = new TreeSet<>();
        Set<String> makeWithCtgMnfcrSet = new TreeSet<>();
        Set<String> modelNoSet = new TreeSet<>();
        Set<String> stateSet = new TreeSet<>();
        Set<String> citySet = new TreeSet<>();
        Set<String> dealerSet = new TreeSet<>();
        applicableSchemeDetails
                .setAllStateCityDealer(schemeDealerMappingDetails
                        .isAllStateCityDealer());
        applicableSchemeDetails.setMaufacturer(schemeDealerMappingDetails
                .getIncludedMnfcr());

        for (IncludedAssetCtgWithManfc ctgWithManfc : schemeDealerMappingDetails
                .getInclAsstCtgWithManf()) {
            assetCtgWithManfcSet.add(ctgWithManfc.getIncludedACtgr());
        }
        applicableSchemeDetails.setAssetCategory(assetCtgWithManfcSet);

        for (IncludedMakeWithCtgMnfcr includedMakeWithCtgMnfcr : schemeDealerMappingDetails
                .getIncludedMakeWithCtgMnfcr()) {
            makeWithCtgMnfcrSet.add(includedMakeWithCtgMnfcr
                    .getIncludeAsstMake());
        }
        applicableSchemeDetails.setAssetMake(makeWithCtgMnfcrSet);
        for (IncludedModelIdWithInformation includedModelIdWithInformation : schemeDealerMappingDetails
                .getIncludedModelIdWithInformation()) {
            modelNoSet.add(includedModelIdWithInformation.getIncludedModel());
        }
        applicableSchemeDetails.setModelNo(modelNoSet);
        if (applicableSchemeDetails.isAllStateCityDealer()) {
            citySet.add(Status.ALL.name());
            dealerSet.add(Status.ALL.name());
            stateSet.add(Status.ALL.name());
            applicableSchemeDetails.setState(stateSet);

        } else {

            for (InclCityWithStateName inclCityWithStateName : schemeDealerMappingDetails
                    .getInclCityWithStateName()) {
                citySet.add(inclCityWithStateName.getCityName());
            }

            for (InclDealerIdWithStateCity inclDealerIdWithStateCity : schemeDealerMappingDetails
                    .getInclDealerIdWithStateCity()) {
                dealerSet.add(inclDealerIdWithStateCity.getSupplierDesc());
            }
            applicableSchemeDetails.setState(schemeDealerMappingDetails
                    .getIncludedState());

        }
        applicableSchemeDetails.setDealerName(dealerSet);
        applicableSchemeDetails.setCity(citySet);
    }


    @Override
    public BaseResponse approveDeclineSchemeStatus(
            SchemeApproveDeclineRequest schemeApproveDeclineRequest) throws Exception {

        SchemeApproveDeclineResponse schemeApproveDeclineResponse = new SchemeApproveDeclineResponse();

        String productName = schemeApproveDeclineRequest.getHeader().getProduct().name();
        String institutionId = schemeApproveDeclineRequest.getHeader().getInstitutionId();
//        String productAliasName = Cache.getAliasNameByProductNameAndInstitutionId(productName, institutionId);
        String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

        if (StringUtils.isNotBlank(productAliasName)) {
            /**
             * check scheme details are partial or complete . if partial the
             * restrict the user to update scheme status.
             */
            if (masterDataViewRepository
                    .checkPartialSchemeDetails(schemeApproveDeclineRequest, productAliasName)) {
                /**
                 * check user role i.e. checker if it is self_checker ,then check
                 * the maker of scheme. the self checker should not be (maker and
                 * checker)
                 */
                if (masterDataViewRepository
                        .checkCheckerDetails(schemeApproveDeclineRequest, productAliasName)) {
                    if (masterDataViewRepository
                            .changeSchemeStatus(schemeApproveDeclineRequest, productAliasName)) {
                        schemeApproveDeclineResponse
                                .setMessage("Scheme Status Updated Successfully.");
                        schemeApproveDeclineResponse.setStatus(Status.SUCCESS
                                .name());
                    } else {
                        schemeApproveDeclineResponse
                                .setMessage("Problem In Updation Of Scheme Status.");
                        schemeApproveDeclineResponse
                                .setStatus(Status.FAILED.name());
                    }
                } else {

                    schemeApproveDeclineResponse
                            .setMessage("User Has No Rights To Change Scheme Status.");
                    schemeApproveDeclineResponse.setStatus(Status.FAILED.name());
                }
            } else {
                schemeApproveDeclineResponse
                        .setMessage("Unable to Change Scheme Status Because of Partial Scheme Data.. ");
                schemeApproveDeclineResponse.setStatus(Status.FAILED.name());
            }
        } else {
            return GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, GngUtils.getConfigurationNotFoundErrorList());
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, schemeApproveDeclineResponse);

    }

    @Override
    public byte[] getCsvZipReport(GetSchemeZipReportRequest getSchemeZipReportRequest) throws IOException {

        List<ApplicableSchemeReportDetails> applicableSchemeReportDetailsList = new ArrayList<>();

        GetAllApplicableSchemeRequest getAllApplicableSchemeRequest = buildGetAllApplicableSchemeRequest(getSchemeZipReportRequest);

        applicableSchemeReportDetailsList = masterDataViewBuilder.buildApplicableSchemeReportResponse(getAllApplicableScheme(getAllApplicableSchemeRequest));

        String encoding = "UTF8";

        OutputStreamWriter reportOutputStreamWriter;
        ByteArrayOutputStream outputStream1;

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();
        flatReprtConfiguration
                .setHeaderMap(SchemeReportConfiguration.schemeConfigMap);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("ApplicableSchemeReport");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");

        outputStream1 = new ByteArrayOutputStream();
        reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                encoding);
        reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());
        reportOutputStreamWriter.append('\n');

        SchemeReportParser schemeReportParser;
        if (null != applicableSchemeReportDetailsList && !applicableSchemeReportDetailsList.isEmpty()) {
            for (ApplicableSchemeReportDetails applicableSchemeReportDetails : applicableSchemeReportDetailsList) {

                schemeReportParser = new SchemeReportParser(
                        applicableSchemeReportDetails);
                String row;
                row = schemeReportParser.build()
                        .enrichJson(flatReprtConfiguration).toString()
                        .replaceAll("[\\\n|\\\r|\\\t]", "");

                reportOutputStreamWriter.write(row);
                reportOutputStreamWriter.write('\n');
            }
        }

        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry("SCHEME_REPORT_"
                + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv");
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        zout.setComment("Scheme_Report");
        zout.finish();
        zout.closeEntry();

        return baos.toByteArray();

    }


    private GetAllApplicableSchemeRequest buildGetAllApplicableSchemeRequest(GetSchemeZipReportRequest getSchemeZipReportRequest) {

        GetAllApplicableSchemeRequest getAllApplicableSchemeRequest = new GetAllApplicableSchemeRequest();
        Header header = new Header();

        header.setInstitutionId(getSchemeZipReportRequest.getHeader().getInstitutionId());
        header.setProduct(getSchemeZipReportRequest.getHeader().getProduct());

        getAllApplicableSchemeRequest.setHeader(header);
        getAllApplicableSchemeRequest.setTypeOfScheme(Status.APPROVED.name());

        return getAllApplicableSchemeRequest;
    }

    @Override
    public BaseResponse deleteCommonGeneralParameterMaster(DeleteCommonGeneralMasterRequest deleteCommonGeneralMasterRequest) throws Exception {

            BaseResponse  baseResponse;
            CommonGeneralMasterResponse commonGeneralMasterResponse = new CommonGeneralMasterResponse();

            WriteResult writeResult = masterDataViewRepository
                    .deleteCommonGeneralMaster(deleteCommonGeneralMasterRequest.getUniqueId()
                            , deleteCommonGeneralMasterRequest.getHeader().getInstitutionId());

            if (null != writeResult && writeResult.isUpdateOfExisting()) {

                commonGeneralMasterResponse.setStatus(Status.SUCCESS.name());
                commonGeneralMasterResponse.setMessage("Record Successfully Deleted");
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, commonGeneralMasterResponse);

            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
            }

        return baseResponse;

    }


    @Override
    public BaseResponse updateCommonGeneralParameterMaster(UpdateCommonGeneralMasterRequest updateCommonGeneralMasterRequest) throws Exception {

        BaseResponse baseResponse;
        CommonGeneralMasterResponse commonGeneralMasterResponse =new CommonGeneralMasterResponse();

        WriteResult writeResult = masterDataViewRepository
                .updateCommonGeneralParameterMaster(updateCommonGeneralMasterRequest);

        if (null != writeResult && writeResult.isUpdateOfExisting()) {

            commonGeneralMasterResponse.setStatus(Status.SUCCESS.name());
            commonGeneralMasterResponse.setMessage("Record Successfully Updated");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, commonGeneralMasterResponse);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }



    @Override
    public BaseResponse insertCommonGeneralParameterMaster(InsertCommonGeneralMasterRequest insertCommonGeneralMasterRequest) {

         masterDataViewRepository.insertCommonGeneralMaster(insertCommonGeneralMasterRequest.getCommonGeneralParameterMaster());

        CommonGeneralMasterResponse commonGeneralMasterResponse = new CommonGeneralMasterResponse();
            commonGeneralMasterResponse.setStatus(Status.SUCCESS.name());
            commonGeneralMasterResponse.setMessage("Record Successfully Inserted");

        return GngUtils.getBaseResponse(HttpStatus.OK, commonGeneralMasterResponse);
    }

    @Override
    public BaseResponse getCommonGeneralMasterRecord(String institutionId, Integer limit, Integer skip) {

        if (limit == 0 && skip == 0) {
            limit = 100;
            skip = 0;
        }
        BaseResponse baseResponse;
        Table resultList = masterDataViewRepository.getCommonGeneralMasterRecord(institutionId,
                limit, skip);

        if (null != resultList &&
                null != resultList.getData()
                && !resultList.getData().isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, resultList);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getSourcingDetails(SourcingDetailsRequest sourcingDetailsRequest) {

        BaseResponse baseResponse;
        SourcingDetails sourcingDetails = masterDataViewRepository.getSourcingDetails(sourcingDetailsRequest);

        if (null != sourcingDetails && null != sourcingDetails.getS4()) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, sourcingDetails);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
       return baseResponse;
    }

    @Override
    public BaseResponse getAllDeviationMasters(DeviationMasterRequest deviationMasterRequest) {
        BaseResponse baseResponse;
        String institutionId = deviationMasterRequest.getHeader().getInstitutionId();
        Product product = deviationMasterRequest.getHeader().getProduct();

        List<DeviationMaster> deviationMasterList = masterDataViewRepository.fetchAllDeviationMasters(institutionId, product);
        if (!CollectionUtils.isEmpty(deviationMasterList)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, deviationMasterList);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllHierarchyMasters(HierarchyMasterRequest hierarchyMasterRequest) {
        BaseResponse baseResponse;
        String institutionId = hierarchyMasterRequest.getHeader().getInstitutionId();

        List<OrganizationalHierarchyMaster> organizationalHierarchyMasters = masterDataViewRepository.fetchAllHierarchyMasters(institutionId);
        if (!CollectionUtils.isEmpty(organizationalHierarchyMasters)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, organizationalHierarchyMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getRoiSchemeMasterData(HierarchyMasterRequest hierarchyMasterRequest) {
        BaseResponse baseResponse;
        String institutionId = hierarchyMasterRequest.getHeader().getInstitutionId();
        String product = hierarchyMasterRequest.getHeader().getProduct().name();
        List<RoiSchemeMaster> schemeMasters = masterDataViewRepository.getRoiSchemeMasterDetails(institutionId, product);

        if (!CollectionUtils.isEmpty(schemeMasters)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, schemeMasters);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;    }
}
