package com.softcell.service.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.DMSRepository;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.dms.DMSLogResponse;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.dms.PushDMSDocumentsResponse;
import com.softcell.gonogo.model.dms.addfolder.AddFolderRequest;
import com.softcell.gonogo.model.dms.addfolder.AddFolderResult;
import com.softcell.gonogo.model.dms.additionaldoc.DmsAdditionalInformation;
import com.softcell.gonogo.model.dms.collection.DMSCreatedFolderInformation;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.dms.connectcabinet.ConnectCabinetRequest;
import com.softcell.gonogo.model.dms.connectcabinet.ConnectCabinetResponse;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentRequest;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentResponse;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.factory.DMSBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.*;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.DMSUtils;
import com.softcell.utils.GngUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mahesh on 24/6/17.
 */
@Service
public class DMSFeedManagerImpl implements DMSFeedManager {


    private static final Logger logger = LoggerFactory.getLogger(DMSFeedManagerImpl.class);

    @Autowired
    private DMSRepository dmsRepository;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private DMSBuilder dmsBuilder;

    @Autowired
    private CroManager croManager;

    @Autowired
    private DocumentStoreManager documentStoreManager;

    @Autowired
    private DigitizationStoreManager digitizationStoreManager;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private LookupService lookupService;

    @Override
    public BaseResponse pushDocumentsToDms(PushDMSDocumentsRequest pushDMSDocumentsRequest) throws Exception {

        BaseResponse baseResponse = null;

        int userDbId = 0;

        String losId = null;

        String refId = pushDMSDocumentsRequest.getRefId();

        WFJobCommDomain connectDisconnectDomain = null;

        String institutionId = pushDMSDocumentsRequest.getHeader().getInstitutionId();

        LOSDetails losDetails = dmsRepository.getLOSInformation(refId, institutionId);

        try {

            boolean actionFlag = lookupService.checkActionsAccess(institutionId, pushDMSDocumentsRequest.getHeader().getProduct().toProductId(), ActionName.DMS_DOC_PUSH);

            if (!actionFlag) {

                PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest,
                        ErrorCode.DMS_PUSH_DOC_ACTION_NOT_FOUND, null);
                // save dms error log
                dmsRepository.saveDmsErrorLog(dmsErrorLog);

                return GngUtils.getNonAuthoritativeInformationErrorList();
            }

            if (null != losDetails
                    && StringUtils.isNotBlank(losDetails.getLosID())) {

                losId = losDetails.getLosID();

                connectDisconnectDomain = workFlowCommunicationManager.getWfCommDomainJobByType(
                        institutionId, UrlType.DMS_CONNECT_DIS.toValue());

                boolean otherConfigurationParameter = dmsRepository.checkWFJobCommDomainConfiguration(institutionId);


                if (null != connectDisconnectDomain && otherConfigurationParameter) {

                    ConnectCabinetRequest connectCabinetRequest = dmsBuilder.buildConnectCabinetObject(losId, institutionId, Status.CONNECT.name());

                    ConnectCabinetResponse connectCabinetResponse = callToConnectCabinet(connectCabinetRequest, connectDisconnectDomain);

                    if (null != connectCabinetResponse) {

                        if (StringUtils.equals(connectCabinetResponse.getStatus(), Status.SUCCESS.name())) {

                            if (StringUtils.equals(connectCabinetResponse.getConnectCabinetOriginalResponse().getStatusCode(), "0")) {

                                userDbId = connectCabinetResponse.getConnectCabinetOriginalResponse().getUserDbId();

                                logger.info("Connection created with DMS cabinet successfully with userDbId {}", userDbId);

                                baseResponse = startDMSPushDocumentProcess(pushDMSDocumentsRequest, losId, userDbId);

                            } else {
                                logger.error(connectCabinetResponse.getConnectCabinetOriginalResponse().getErrorMessage());

                                PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest,
                                        connectCabinetResponse.getConnectCabinetOriginalResponse().getErrorMessage(),
                                        connectCabinetResponse.getConnectCabinetOriginalResponse().getStatusCode());
                                // save dms error log
                                dmsRepository.saveDmsErrorLog(dmsErrorLog);

                                baseResponse = DMSUtils.getDMSErrorResponse(connectCabinetResponse.getConnectCabinetOriginalResponse().getErrorMessage());
                            }
                        } else {
                            logger.error(connectCabinetResponse.getError().getMessage());

                            PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest,
                                    connectCabinetResponse.getError().getMessage(),
                                    connectCabinetResponse.getError().getType());
                            // save dms error log
                            dmsRepository.saveDmsErrorLog(dmsErrorLog);

                            baseResponse = DMSUtils.getDMSErrorResponse(connectCabinetResponse.getError().getMessage());
                        }

                    } else {

                        logger.error(ErrorCode.PROBLEM_WITH_CABINET_CONNECTION);

                        // save dms error log
                        PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest, ErrorCode.PROBLEM_WITH_CABINET_CONNECTION, null);
                        dmsRepository.saveDmsErrorLog(dmsErrorLog);

                        baseResponse = DMSUtils.getDMSErrorResponse(ErrorCode.PROBLEM_WITH_CABINET_CONNECTION);
                    }

                } else {

                    logger.error("Configuration not found for {}", UrlType.DMS_CONNECT_DIS);

                    // save dms error log
                    PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest, UrlType.DMS_CONNECT_DIS.toValue() + "configuration not found", null);
                    dmsRepository.saveDmsErrorLog(dmsErrorLog);

                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
                }

            } else {

                logger.info("Los information not found for this application {}", refId);

                // save dms error log
                PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest, ErrorCode.LOS_INFO_NOT_FOUND, null);
                dmsRepository.saveDmsErrorLog(dmsErrorLog);

                baseResponse = DMSUtils.getDMSErrorResponse(ErrorCode.LOS_INFO_NOT_FOUND);
            }
        } catch (Exception e) {

            e.printStackTrace();
            logger.error(ErrorCode.DMS_EXCEPTION_MESSAGE + e.getMessage());

            // save dms error log
            PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest, ErrorCode.DMS_EXCEPTION_MESSAGE + e.getMessage(), null);
            dmsRepository.saveDmsErrorLog(dmsErrorLog);

            baseResponse = DMSUtils.getDMSErrorResponse(ErrorCode.DMS_EXCEPTION_MESSAGE + e.getMessage());


        } finally {

            logger.info("Inside the close DMS cabinet connection for application Id {}", refId);

            if (null != connectDisconnectDomain && null != losId && userDbId != 0) {
                ConnectCabinetRequest connectCabinetRequest = dmsBuilder.buildDisConnectCabinetObject(losId, institutionId, Status.DISCONNECT.name(), userDbId);

                callToConnectCabinet(connectCabinetRequest, connectDisconnectDomain);
                logger.info("DMS cabinet connection close successfully with userDbId {} and ", userDbId);
            }
        }

        return baseResponse;
    }

    private BaseResponse startDMSPushDocumentProcess(PushDMSDocumentsRequest pushDMSDocumentsRequest, String losId, int userDbId) throws Exception {

        String institutionId = pushDMSDocumentsRequest.getHeader().getInstitutionId();
        String refId = pushDMSDocumentsRequest.getRefId();

        if (!dmsRepository.checkFolderAvailability(refId, institutionId)) {

            logger.info("First time calling ...Folders are not created in dms against the application {}", refId);

            PushDMSDocumentsResponse pushDMSDocumentsResponse = createFolderInDMSDatabase(losId, refId, institutionId, userDbId);

            if (StringUtils.equals(pushDMSDocumentsResponse.getStatus(), Status.ERROR.name())) {

                // save dms error log
                PushDMSDocumentsRequest dmsErrorLog = dmsBuilder.buildDmsErrorLogObject(pushDMSDocumentsRequest, pushDMSDocumentsResponse.getResponseMsg(), null);
                dmsRepository.saveDmsErrorLog(dmsErrorLog);

                return GngUtils.getBaseResponse(HttpStatus.OK, pushDMSDocumentsResponse);
            }
        } else {
            logger.info("Folders are already created in dms against the application {}", refId);
        }


        List<ImagesDetails> documents = getDocuments(refId, institutionId, pushDMSDocumentsRequest.getHeader().getProduct());

        Set<String> alreadyPushedDocumentNames = dmsRepository.alreadyPushedDocumentNames(refId, institutionId);

        return checkDocumentExistance(documents, alreadyPushedDocumentNames, refId, institutionId, losId, userDbId);


    }


    private List<ImagesDetails> getDocuments(String refId, String institutionId, Product product) throws Exception {

        List<ImagesDetails> documentsForDmsList = null;
        CheckApplicationStatus checkApplicationStatus = dmsBuilder.buildGetApplicationImagesRequest(refId, institutionId);

        // check application and agreement form existance in the database if not found then create manually.
        if (dmsRepository.checkApplicationAgreementFormExistance(refId, institutionId)) {

            logger.info("Application form and Agreement form not found against referenceId {}", refId);
            logger.info("Manually generating application and agreement form for referenceId {}", refId);

            InvoiceDetailsRequest invoiceDetailsRequest = DMSUtils.buildAgreementApplicationFormRequest(refId, institutionId, product);

            digitizationStoreManager.saveApplicationForm(invoiceDetailsRequest, GNGWorkflowConstant.APPLICATION_FORM.toFaceValue());
            digitizationStoreManager.saveAgreementForm(invoiceDetailsRequest, GNGWorkflowConstant.AGREEMENT_FORM.toFaceValue());
        }

        BaseResponse baseResponse = croManager.getDocumentsForDmsPush(checkApplicationStatus);

        if (null != baseResponse && null != baseResponse.getStatus()
                && StringUtils.equals(baseResponse.getStatus().getStatusValue(), Status.OK.name())) {

            documentsForDmsList = (List<ImagesDetails>) baseResponse.getPayload().getT();

        }

        return documentsForDmsList;
    }

    private BaseResponse checkDocumentExistance(List<ImagesDetails> dmsDocumentsDetailsList, Set<String> alreadyPushedDocumentDetails, String refId, String institutionId, String losId, int userDbId) throws Exception {

        if (CollectionUtils.isEmpty(dmsDocumentsDetailsList)) {

            return DMSUtils.getDMSErrorResponse(ErrorCode.DOCUMENTS_NOT_FOUND);
        }

        for (ImagesDetails imagesDetails : dmsDocumentsDetailsList) {

            if (!alreadyPushedDocumentDetails.contains(imagesDetails.getImageId())) {

                String documentBase64Code = null;
                Date createDate =null;
                DMSPushDocumentInformation dmsPushDocumentInformation = null;
                long folderId = 0;

                String folderName = DMSUtils.getFolderNameAccordingToImageType(imagesDetails.getImageName());

                if (StringUtils.isNotBlank(folderName)) {

                    folderId = dmsRepository.getFolderIdAgainstFolderName(refId, institutionId, folderName);

                    documentBase64Code = getDocumentBase64Code(imagesDetails.getImageId(), institutionId, refId);


                    if (StringUtils.isNotBlank(documentBase64Code) && folderId != 0) {

                        logger.info("call to get additional Information method for document {}", imagesDetails.getImageName());

                        Set<DmsAdditionalInformation> dmsAdditionalInformations = getDmsAdditionalInformation(folderName, refId, institutionId, losId);

                        PostDocumentRequest postDocumentRequest = dmsBuilder.buildDocumentPushRequest(imagesDetails, institutionId, losId, documentBase64Code, folderId, userDbId, dmsAdditionalInformations);

                        WFJobCommDomain dmsPostDocumentWs_domain = workFlowCommunicationManager.getWfCommDomainJobByType(
                                institutionId, UrlType.DMS_POST_DOCUMENT.toValue());

                        // the date before the dms service call
                         createDate = new Date();

                        PostDocumentResponse postDocumentResponse = callToPostDocument(postDocumentRequest, dmsPostDocumentWs_domain);

                        if (null == postDocumentResponse) {

                            imagesDetails.setStatus(Status.FAILED.name());
                            imagesDetails.setReason(ErrorCode.BLANK_RESPONSE_FROM_DMZ);

                            dmsPushDocumentInformation = dmsBuilder.buildPushDocumentInformationFailedObject(imagesDetails, refId, institutionId,createDate);

                        } else if (null != postDocumentResponse
                                && null != postDocumentResponse.getDmsOriginalResponse()
                                && StringUtils.equals(postDocumentResponse.getStatus(), Status.SUCCESS.name())) {

                            if (postDocumentResponse.getDmsOriginalResponse().getStatusCode() == 0) {

                                logger.info("{} document successfully pushed in dms", imagesDetails.getImageName());

                                dmsPushDocumentInformation = dmsBuilder.buildPushDocumentInformationSuccessObject(postDocumentResponse, refId, institutionId, imagesDetails.getImageId(), folderName ,createDate);

                            } else {

                                logger.error("{} document Failed to push in dms with probable cause {}", imagesDetails.getImageName(), postDocumentResponse.getDmsOriginalResponse().getDmsMessage());

                                imagesDetails.setStatus(Status.FAILED.name());
                                imagesDetails.setReason(postDocumentResponse.getDmsOriginalResponse().getDmsMessage());
                                imagesDetails.setBlockName(String.valueOf(postDocumentResponse.getDmsOriginalResponse().getStatusCode()));

                                dmsPushDocumentInformation = dmsBuilder.buildPushDocumentInformationFailedObject(imagesDetails, refId, institutionId,createDate);
                            }
                        } else {

                            logger.error("{} document Failed to push in dms with probable cause {}", imagesDetails.getImageName(), postDocumentResponse.getError().getMessage());

                            imagesDetails.setStatus(Status.FAILED.name());
                            imagesDetails.setReason(postDocumentResponse.getError().getMessage());

                            dmsPushDocumentInformation = dmsBuilder.buildPushDocumentInformationFailedObject(imagesDetails, refId, institutionId,createDate);

                        }
                    } else {
                        if (folderId == 0) {
                            logger.error("{} document Failed to push in dms with probable cause {}", imagesDetails.getImageName(), ErrorCode.FOLDER_NOT_FOUND);

                            imagesDetails.setReason(ErrorCode.FOLDER_NOT_FOUND);

                        } else {
                            logger.error("{} document Failed to push in dms with probable cause {}", imagesDetails.getImageName(), ErrorCode.IMAGE_NOT_FOUND);

                            if (StringUtils.equals(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue(), imagesDetails.getImageName())) {

                                imagesDetails.setReason(ErrorCode.CIBIL_REPORT_NOT_FOUND);
                            } else {
                                imagesDetails.setReason(ErrorCode.IMAGE_NOT_FOUND);
                            }

                        }
                        imagesDetails.setStatus(Status.FAILED.name());

                        dmsPushDocumentInformation = dmsBuilder.buildPushDocumentInformationFailedObject(imagesDetails, refId, institutionId,createDate);

                    }

                    savePushDocumentInformation(dmsPushDocumentInformation);
                }
            }
        }
        return DMSUtils.getDMSPushDocumentsResponse(dmsRepository.getSuccessFailureCountOfPushDocumentsWithCause(institutionId, refId));
    }

    private Set<DmsAdditionalInformation> getDmsAdditionalInformation(String folderName, String refId, String institutionId, String losId) throws Exception {

        Set<DmsAdditionalInformation> dmsAdditionalFinalInformationSet = new HashSet<>();
        DmsAdditionalInformation dmsAdditionalInformation;

        GoNoGoCroApplicationResponse goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId, institutionId);

        List<DmsFolderConfiguration> dmsFolderConfigurations = lookupService.getDmsIndexInformation(institutionId);

        if (null != goNoGoCustomerApplication && !CollectionUtils.isEmpty(dmsFolderConfigurations)) {

            for (DmsFolderConfiguration dmsFolderConfiguration : dmsFolderConfigurations) {

                dmsAdditionalInformation = new DmsAdditionalInformation();
                dmsAdditionalInformation.setIndexId(dmsFolderConfiguration.getIndexId());
                dmsAdditionalInformation.setIndexType(dmsFolderConfiguration.getIndexType());
                dmsAdditionalInformation.setInformationType(dmsFolderConfiguration.getIndexName());

                if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Application_No.name())) {

                    dmsAdditionalInformation.setIndexValue(goNoGoCustomerApplication.getGngRefId());
                    dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);

                }

                if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.LOS_ID.name())) {
                    dmsAdditionalInformation.setIndexValue(losId);
                    dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);

                }
                if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Doc_Related.name())) {
                    dmsAdditionalInformation.setIndexValue(DmsEnum.APPLICANT.name());
                    dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                }

                if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages.name())) {
                    if (StringUtils.equals(folderName, GNGWorkflowConstant.UNDERWRITING.toFaceValue())) {
                        dmsAdditionalInformation.setIndexValue(GNGWorkflowConstant.UNDERWRITING.toFaceValue());
                    }
                    if (StringUtils.equals(folderName, GNGWorkflowConstant.DISBURSAL.toFaceValue())) {
                        dmsAdditionalInformation.setIndexValue(GNGWorkflowConstant.DISBURSAL.toFaceValue());

                    }
                    if (StringUtils.equals(folderName, GNGWorkflowConstant.SOURCING.toFaceValue())) {
                        dmsAdditionalInformation.setIndexValue(GNGWorkflowConstant.SOURCING.toFaceValue());
                    }
                    dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);

                }
                if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Product.name())) {
                    if (null != goNoGoCustomerApplication.getApplicationRequest()
                            && null != goNoGoCustomerApplication.getApplicationRequest().getHeader()
                            && null != goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct()) {

                        String productNameForDms = DMSUtils.getProductNameForDms(goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().name());

                        dmsAdditionalInformation.setIndexValue(productNameForDms);
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }

                }

                if (StringUtils.equals(folderName, GNGWorkflowConstant.SOURCING.toFaceValue())) {

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Sourcing.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Disbursal.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.N.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Underwriting.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.N.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }


                }

                if (StringUtils.equals(folderName, GNGWorkflowConstant.UNDERWRITING.toFaceValue())) {

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Sourcing.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Underwriting.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Disbursal.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.N.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }


                }

                if (StringUtils.equals(folderName, GNGWorkflowConstant.DISBURSAL.toFaceValue())) {

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Disbursal.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Sourcing.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Stages_Underwriting.name())) {
                        dmsAdditionalInformation.setIndexValue(Status.Y.name());
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                    }
                }

                if (StringUtils.equals(folderName, GNGWorkflowConstant.DISBURSAL.toFaceValue())) {

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.DO_Number.name())) {
                        dmsAdditionalInformation.setIndexValue(refId);
                        dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);

                    }
                }

                if (StringUtils.equals(folderName, GNGWorkflowConstant.SOURCING.toFaceValue())) {

                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Pan_Card_Number.name())) {

                        String panCardNo = DMSUtils.getKycDocument(goNoGoCustomerApplication
                                , GNGWorkflowConstant.PAN.toFaceValue());

                        if (null != panCardNo) {
                            dmsAdditionalInformation.setIndexValue(panCardNo);
                            dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);

                        }
                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Aadhar_Card_Number.name())) {

                        String aadharCardNo = DMSUtils.getKycDocument(goNoGoCustomerApplication
                                , GNGWorkflowConstant.AADHAAR.toFaceValue());

                        if (null != aadharCardNo) {
                            dmsAdditionalInformation.setIndexValue(aadharCardNo);
                            dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                        }

                    }
                    if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), DmsEnum.Driving_Licence_Number.name())) {

                        String drivingLicenceNo = DMSUtils.getKycDocument(goNoGoCustomerApplication
                                , GNGWorkflowConstant.DRIVING_LICENSE.toFaceValue());
                        if (null != drivingLicenceNo) {
                            dmsAdditionalInformation.setIndexValue(drivingLicenceNo);
                            dmsAdditionalFinalInformationSet.add(dmsAdditionalInformation);
                        }

                    }
                }
            }
            logger.info("Additional information for folder {} found successfully", folderName);

        } else {
            logger.error("error occur at the time of getting additional doc information with probable cause{}", ErrorCode.ADDITIONAL_DOC_CONFIG_NOT_FOUNT);
        }

        return dmsAdditionalFinalInformationSet;
    }


    private String getDocumentBase64Code(String imageObjectId, String institutionId, String refId) throws Exception {

        String byteCode = null;
        //if imageId is CIBIL then get cibil report bytecode
        if (StringUtils.equals(imageObjectId, GNGWorkflowConstant.CIBIL.toFaceValue())) {

            byte[] byteArray = dmsRepository.getCibilReportBase64Code(institutionId, refId);
            if (null != byteArray) {

                byte[] encodeBase64 = Base64.encodeBase64(byteArray);
                byteCode = new String(encodeBase64);
            }

            //otherwise get other image bytecode i.e .PAN ,APPLICANT-PHOTO,AGREEMENT FORM ,APPLICATION FORM ,AADHAR
        } else {
            GetFileRequest getImageRequest = dmsBuilder.buildGetDocumentBase64ImageRequest(imageObjectId, institutionId);

            BaseResponse baseResponse = documentStoreManager.getDocumentBase64ImageByRefId(getImageRequest);

            Document document = GngUtils.convertBaseResponsePayload(baseResponse, Document.class);

            if (null != document) {

                byteCode = document.getByteCode();
            }

        }
        return byteCode;
    }

    // save push document information in the database
    private void savePushDocumentInformation(DMSPushDocumentInformation dmsPushDocumentInformation) {

        if (null != dmsPushDocumentInformation) {
            dmsRepository.savePushDocumentInformation(dmsPushDocumentInformation);
        }
    }


    private PushDMSDocumentsResponse createFolderInDMSDatabase(String losId, String refId, String institutionId, int userDbId) throws Exception {

        AddFolderRequest addFolderRequest;
        AddFolderResult addFolderResult;
        Date createDate = null;

        PushDMSDocumentsResponse pushDMSDocumentsResponse = new PushDMSDocumentsResponse();
        pushDMSDocumentsResponse.setStatus(Status.SUCCESS.name());


        List<DmsFolderConfiguration> dmsFolderConfiguration = lookupService.getDmsIndexInformation(institutionId);

        //  123 is the parent folderId in the dms i.e business folder id
        long parentFolderId = DMSUtils.getBusinessFolderId(dmsFolderConfiguration, Status.BUSINESS.name());


        //get folder name list from DMS util
        Set<String> folderNameSet = DMSUtils.getFolderNameSet(losId);

        for (String folderName : folderNameSet) {


            addFolderRequest = dmsBuilder.buildAddFolderRequest(losId, refId, institutionId, folderName, parentFolderId, userDbId);

            WFJobCommDomain dmsAddFolderConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    institutionId, UrlType.DMS_ADD_FOLDER.toValue());
            //the time before service call
            createDate =new Date();

            addFolderResult = callToAddFolder(addFolderRequest, dmsAddFolderConfig);


            if (null != addFolderResult && StringUtils.equals(addFolderResult.getStatus(), Status.SUCCESS.name())
                    && null != addFolderResult.getAddFolderDmsOriginalResponse()
                    && StringUtils.equals(addFolderResult.getAddFolderDmsOriginalResponse().getStatusCode(), "0")) {

                //parent folder id will be same for three folders i.e
                // Sourcing ,UnderWriter ,Disbursal
                if (StringUtils.equals(folderName, losId)) {
                    parentFolderId = addFolderResult.getAddFolderDmsOriginalResponse().getFolderIndex();
                }

                //build dmsCreatedFolderInformation object and save in the database
                DMSCreatedFolderInformation dmsCreatedFolderInformation = dmsBuilder.buildCreatedFolderInformationObject(refId, institutionId, addFolderResult.getAddFolderDmsOriginalResponse());

                dmsCreatedFolderInformation.getFolderInformation().get(0).setInsertDate(createDate);

                logger.info("DMSCreatedFolderInformation object build successfully for saving purpose");

                dmsRepository.saveCreatedFolderInformation(dmsCreatedFolderInformation);

                logger.info("{} folder created in dms under parentFolder {} and userDbId {}", folderName, parentFolderId, userDbId);

            } else if (null != addFolderResult && StringUtils.equals(addFolderResult.getStatus(), Status.SUCCESS.name())
                    && null != addFolderResult.getAddFolderDmsOriginalResponse()
                    && !StringUtils.equals(addFolderResult.getAddFolderDmsOriginalResponse().getStatusCode(), "0")) {

                logger.info("problem occur at the time creating folder {} with status code {} from dms", folderName,
                        addFolderResult.getAddFolderDmsOriginalResponse().getStatusCode());

                pushDMSDocumentsResponse.setResponseMsg(addFolderResult.getAddFolderDmsOriginalResponse().getErrorMessage());
                pushDMSDocumentsResponse.setStatus(Status.ERROR.name());

                return pushDMSDocumentsResponse;

            } else {

                pushDMSDocumentsResponse.setResponseMsg(addFolderResult.getError().getMessage());
                pushDMSDocumentsResponse.setStatus(addFolderResult.getStatus());

                return pushDMSDocumentsResponse;
            }

        }

        return pushDMSDocumentsResponse;

    }


    private AddFolderResult callToAddFolder(AddFolderRequest addFolderRequest, WFJobCommDomain dmsConfig) throws Exception {

        String url = Arrays.asList(dmsConfig.getBaseUrl(), dmsConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        logger.info("Inside callToAddFolder with url {}", url);

        return (AddFolderResult) TransportUtils.postJsonRequest(addFolderRequest, url, AddFolderResult.class);
    }

    private PostDocumentResponse callToPostDocument(PostDocumentRequest postDocumentRequest, WFJobCommDomain dmsConfig) throws Exception {
        String url = Arrays.asList(dmsConfig.getBaseUrl(), dmsConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        logger.info("Inside callToPostDocument with url {}", url);

        return (PostDocumentResponse) TransportUtils.postJsonRequest(postDocumentRequest, url, PostDocumentResponse.class);

    }

    private ConnectCabinetResponse callToConnectCabinet(ConnectCabinetRequest connectCabinetRequest, WFJobCommDomain dmsConfig) throws Exception {
        String url = Arrays.asList(dmsConfig.getBaseUrl(), dmsConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        logger.info("Inside callToConnectCabinet with url {}", url);

        return (ConnectCabinetResponse) TransportUtils.postJsonRequest(connectCabinetRequest, url, ConnectCabinetResponse.class);

    }

    @Override
    public BaseResponse getPushDocumentsInfo(String refId, String institutionId) {

        BaseResponse baseResponse;
        DMSLogResponse dmsLog = dmsRepository.getPushDocumentsInfo(refId, institutionId);

        if (null == dmsLog.getDmsPushDocumentInformation()
                && CollectionUtils.isEmpty(dmsLog.getFolderInformationList())) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        } else {
            dmsLog.setRefId(refId);
            dmsLog.setInstitutionId(institutionId);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmsLog);

        }
        return baseResponse;

    }

    @Override
    public BaseResponse getDMSErrorLog(String refId, String institutionId) {

        BaseResponse baseResponse;
        List<PushDMSDocumentsRequest> dmsErrorLog = dmsRepository.getDMSErrorLog(refId, institutionId);

        if (!CollectionUtils.isEmpty(dmsErrorLog)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmsErrorLog);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }
}
