package com.softcell.service.impl;

import com.softcell.gonogo.model.casehistory.DataUnit;
import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.logger.ApplicationCaseHistory;
import com.softcell.gonogo.model.logger.Stage;
import com.softcell.gonogo.model.logger.Statastics;
import com.softcell.service.EventDataStandardization;
import com.softcell.utils.GngDateUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author kishorp
 */
@Service
public class EventDataStandardizationImpl implements EventDataStandardization {

    private static HashMap<String, Integer> eventGroup = new HashMap<>();
    private static HashMap<String, String> eventGroupDescr = new HashMap<>();
    private static HashMap<Integer, String> stageDescr = new HashMap<>();
    private static HashMap<String, Integer> stageDescrById = new HashMap<>();
    private static List<String> header = new ArrayList<>();

    static {

        header.add("Stage");
        header.add("Step");
        header.add("TimeStamp");
        header.add("Duration");
        header.add("Cumulative Duration");
        eventGroup.put("101", 0);
        eventGroup.put("102", 0);
        eventGroup.put("103", 0);
        eventGroup.put("104", 0);

        eventGroup.put("601", 1);
        eventGroup.put("602", 1);
        eventGroup.put("603", 1);
        eventGroup.put("604", 1);
        eventGroup.put("605", 1);
        eventGroup.put("606", 1);
        eventGroup.put("607", 1);
        eventGroup.put("608", 1);
        eventGroup.put("609", 1);

        eventGroup.put("201", 2);
        eventGroup.put("202", 2);
        eventGroup.put("203", 2);
        eventGroup.put("204", 2);
        eventGroup.put("205", 2);

        eventGroup.put("206", 3);
        eventGroup.put("301", 3);
        eventGroup.put("302", 3);
        eventGroup.put("303", 3);

        eventGroup.put("304", 4);
        eventGroup.put("305", 4);
        eventGroup.put("306", 4);
        eventGroup.put("307", 4);

        eventGroupDescr.put("101", "Login");
        eventGroupDescr.put("102", "Application DE Start");
        eventGroupDescr.put("103", "Application DE Start");
        eventGroupDescr.put("104", "Application Submited");

        eventGroupDescr.put("601", "Negative Dedupe");
        eventGroupDescr.put("602", "MB");
        eventGroupDescr.put("603", "PAN");
        eventGroupDescr.put("604", "Scoring");
        eventGroupDescr.put("605", "Address Scoring");
        eventGroupDescr.put("606", "Aadhaar");
        eventGroupDescr.put("607", "NTC");
        eventGroupDescr.put("608", "Salesforce");
        eventGroupDescr.put("609", "AWS");

        eventGroupDescr.put("201", "STP Declined");
        eventGroupDescr.put("202", "STP Approved");
        eventGroupDescr.put("203", "Approved");
        eventGroupDescr.put("204", "Declined");
        eventGroupDescr.put("205", "Queue");

        eventGroupDescr.put("206", "PDDE");
        eventGroupDescr.put("301", "DO Generated");
        eventGroupDescr.put("302", "Serial Number Validation");
        eventGroupDescr.put("303", "Invoice Generated");

        eventGroupDescr.put("304", "LOS_DCLN");
        eventGroupDescr.put("305", "LOS_DISB");
        eventGroupDescr.put("306", "LOS_APRV");
        eventGroupDescr.put("307", "LOS_QDE");

        stageDescr.put(0, "DE");
        stageDescr.put(1, "BRE");
        stageDescr.put(2, "IPA");
        stageDescr.put(3, "PDDE");
        stageDescr.put(4, "PDDV");

        stageDescrById.put("DE", 0);
        stageDescrById.put("BRE", 1);
        stageDescrById.put("IPA", 2);
        stageDescrById.put("PDDE", 3);
        stageDescrById.put("PDDV", 4);

    }

    @Override
    public ApplicationCaseHistory getApplicationCaseHistory(
            String institutionId, String refId, Set<String> products,
            List<?> events) {

        return null;

    }

    private Stage getStage(List<ApplicationTracking> keyValue, int stageId,
                           long lastStageDuration, DateTimeFormatter timeTampFormat) {
        Stage stage = new Stage();
        stage.setStepHeader(header);
        stage.setName(stageDescr.get(stageId));

        List<List<String>> data = new ArrayList<>();
        stage.setData(data);
        long duration = 0;
        String timeStamp = null;
        long startTime = 0;
        long endTime = 0;
        boolean isFirst = true;
        boolean isLast = true;
        Date endDate = null;
        for (ApplicationTracking event : keyValue) {
            duration = duration + event.getNodeEdge();
            List<String> row = new ArrayList<>(stage.getStepHeader().size());
            row.add(0, stage.getName());
            row.add(1, eventGroupDescr.get(event.getStepId()));
            timeStamp = GngDateUtil.getTimeStamp12H(
                    new DateTime(event.getEndDate()), timeTampFormat);
            row.add(2, timeStamp);
            DataUnit dataUnit = GngDateUtil.getTimeDataUnit(event
                    .getNodeEdge());
            row.add(3, event.getNodeEdge() + "");

            row.add(4, "");
            data.add(row);
            if (isFirst) {
                startTime = event.getStartDate().getTime();
                isFirst = false;
            } else {
                endTime = event.getEndDate().getTime();
                isLast = false;
            }
            endDate = event.getEndDate();
        }

        List<String> row = new ArrayList<>(stage.getStepHeader().size());
        row.add(0, stage.getName());
        row.add(1, "Stage Complete");
        row.add(2, timeStamp);

        if (!isLast) {
            stage.setTimeStamp(GngDateUtil.getTimeStamp12H(new DateTime(
                    endTime), timeTampFormat));
        } else {
            stage.setTimeStamp(GngDateUtil.getTimeStamp12H(new DateTime(
                    startTime), timeTampFormat));
        }
        stage.setDate(GngDateUtil.getddMMMYyyy(new DateTime(endDate.getTime())));
        Statastics statastics = new Statastics();
        DataUnit dataUnit = GngDateUtil.getTimeDataUnit(duration);
        if (isFirst || isLast) {
            row.add(3, Long.toString(duration));
            statastics.setDuration(duration);
        } else {
            duration = Math.abs(startTime - endTime);
            row.add(3, Long.toString(duration));
            statastics.setDuration(duration);
        }
        row.add(4, Double.toString((duration + lastStageDuration)));

        statastics.setUnit(dataUnit.getUnit());
        statastics.setText(dataUnit.getValue());
        statastics.setCummulativeDuration(duration + lastStageDuration);

        stage.setBucketId(getbucketNumber(stageId, duration));
        stage.setStatastics(statastics);

        data.add(row);
        return stage;
    }

    @Override
    public ApplicationCaseHistory getApplicationCaseHistory(
            String institutionId, String refId, List events) {
        processNodeAge(events);
        ApplicationCaseHistory applicationCaseHistory = standardizeEvents(
                events, GngDateUtil.timeStamp24H);
        return applicationCaseHistory;
    }

    private ApplicationCaseHistory standardizeEvents(List events,
                                                     DateTimeFormatter dateformat) {
        ApplicationCaseHistory applicationCaseHistory = new ApplicationCaseHistory();
        TreeMap<Integer, List<ApplicationTracking>> eventMap = new TreeMap<>();
        for (Object event : events) {
            ApplicationTracking applicationTracking = (ApplicationTracking) event;
            if (null != applicationTracking.getStepId()
                    && eventGroup.containsKey(applicationTracking.getStepId())) {
                Integer groupId = eventGroup.get(applicationTracking
                        .getStepId());
                if (eventMap.containsKey(groupId)) {
                    eventMap.get(groupId).add(applicationTracking);
                } else {
                    List<ApplicationTracking> applicationTrackings = new ArrayList<ApplicationTracking>();
                    eventMap.put(groupId, applicationTrackings);
                    applicationTrackings.add(applicationTracking);
                }
            }
        }
        if (!eventMap.isEmpty()) {
            List<Stage> stages = new ArrayList<Stage>();
            applicationCaseHistory.setStages(stages);
            long cummulativeDuration = 0;

            for (int index = 0; index < 5; index++) {
                List<ApplicationTracking> stageEvents = eventMap.get(index);
                if (null == stageEvents) {
                    Stage stage = new Stage();
                    stage.setName(stageDescr.get(index));
                    stage.setData(new ArrayList<>());
                    stage.setStepHeader(header);
                    stage.setPending(true);
                    stage.setStageId(index);
                    applicationCaseHistory.getStages().add(stage);
                } else {
                    Stage stage = getStage(stageEvents, index,
                            cummulativeDuration, dateformat);
                    stages.add(stage);
                    cummulativeDuration = stage.getStatastics()
                            .getCummulativeDuration();
                }
            }
            // for (Entry<Integer, List<ApplicationTracking>> keyValue :
            // eventMap
            // // .entrySet()) {
            // // Stage stage = getStage(keyValue, cummulativeDuration,
            // dateformat);
            // // stages.add(stage);
            // // cummulativeDuration = stage.getStatastics()
            // // .getCummulativeDuration();
            // // }
            //
            // addPendingStageDetails(applicationCaseHistory);
            // reOrderStages(applicationCaseHistory);
        }
        return applicationCaseHistory;
    }

    private void reOrderStages(ApplicationCaseHistory applicationCaseHistory) {
        TreeMap<Integer, Stage> stages = new TreeMap<>();
        for (Stage stage : applicationCaseHistory.getStages()) {
            stages.putIfAbsent(stage.getStageId(), stage);
        }
        ArrayList<Stage> orderStages = new ArrayList<>();
        for (Entry<Integer, Stage> stage : stages.entrySet()) {
            orderStages.add(stage.getValue());
        }
        applicationCaseHistory.setStages(orderStages);
    }

    private void addPendingStageDetails(
            ApplicationCaseHistory applicationCaseHistory) {
        int stageLength = applicationCaseHistory.getStages().size();
        Set<Integer> keySet = new HashSet<Integer>();
        for (Stage stages : applicationCaseHistory.getStages()) {
            String stageName = stages.getName();
            if (null != stageName && stageDescrById.containsKey(stageName)) {
                keySet.add(stageDescrById.get(stageName));
            }
        }
        if (stageLength > 0)
            for (int i = 0; i < 5; i++) {
                if (!keySet.contains(i)) {
                    Stage stage = new Stage();
                    stage.setName(stageDescr.get(i));
                    stage.setData(new ArrayList<>());
                    stage.setStepHeader(header);
                    stage.setPending(true);
                    stage.setStageId(i);
                    applicationCaseHistory.getStages().add(stage);
                }
            }
        else {
            for (int i = 0; i < 5; i++) {
                Stage stage = new Stage();
                stage.setName(stageDescr.get(i));
                stage.setData(new ArrayList<>());
                stage.setStepHeader(header);
                stage.setPending(true);
                stage.setStageId(i);
                applicationCaseHistory.getStages().add(stage);
            }
        }

    }

    private void processNodeAge(List<ApplicationTracking> appTrackingLog) {
        long edgeCumulative = 0l;
        for (int i = 1; i < appTrackingLog.size(); i++) {
            int prev = i - 1;
            ApplicationTracking previous = appTrackingLog.get(prev);
            ApplicationTracking current = appTrackingLog.get(i);
            if (prev == 0) {
                previous.setTimeDiffrence(0);
            }
            if (current != null && previous != null
                    && current.getStartDate() != null
                    && previous.getStartDate() != null) {
                long nodeEdge = Math
                        .abs((current.getEndDate().getTime() - previous
                                .getEndDate().getTime()));
                current.setNodeEdge(nodeEdge);
                current.setNodeEdgeReadable(GngDateUtil
                        .readableTime(nodeEdge));
                edgeCumulative = edgeCumulative + nodeEdge;
                current.setEdgeCumulativeReadable(GngDateUtil
                        .readableTime(edgeCumulative));
                current.setEdgeCumulative(edgeCumulative);

            }
        }
    }

    @Override
    public byte[] getApplicationHistoryReport(
            List<ApplicationTracking> applicationTrackings) throws IOException {
        String encoding = "UTF8";
        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream1 = null;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                new File("D:\\data" + "\\CaseHistory"
                        + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime())
                        + ".csv")));
        try {
            StringBuilder header = new StringBuilder();
            header.append('"').append("InstitutionId").append('"').append(',');
            header.append('"').append("DSAId").append('"').append(',');
            header.append('"').append("ApplicationId").append('"').append(',');
            header.append('"').append("Stage").append('"').append(',');
            header.append('"').append("Step").append('"').append(',');
            header.append('"').append("TimeStamp").append('"').append(',');
            header.append('"').append("Duration").append('"').append(',');
            header.append('"').append("Cumulative Duration").append('"');

            outputStream1 = new ByteArrayOutputStream();
            reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                    encoding);
            reportOutputStreamWriter.append('\n');
            reportOutputStreamWriter.append(header);
            reportOutputStreamWriter.append('\n');
            bufferedWriter.append(header);
            bufferedWriter.newLine();
        } catch (IOException e3) {
            e3.printStackTrace();
        }

        Map<String, List<ApplicationTracking>> eventTreeMap = new TreeMap<String, List<ApplicationTracking>>();
        for (ApplicationTracking applicationTracking : applicationTrackings) {
            if (null == applicationTracking.getGngRefId())
                continue;
            if (eventTreeMap.containsKey(applicationTracking.getGngRefId())) {
                eventTreeMap.get(applicationTracking.getGngRefId()).add(
                        applicationTracking);
            } else {
                List<ApplicationTracking> events = new ArrayList<ApplicationTracking>();
                events.add(applicationTracking);
                eventTreeMap.put(applicationTracking.getGngRefId(), events);

            }
        }

        for (Entry<String, List<ApplicationTracking>> event : eventTreeMap
                .entrySet()) {
            processNodeAge(event.getValue());
            ApplicationCaseHistory applicationCaseHistory = standardizeEvents(
                    event.getValue(), GngDateUtil.timeStamp);
            if (null != applicationCaseHistory
                    && applicationCaseHistory.getStages() != null) {
                for (Stage stage : applicationCaseHistory.getStages()) {

                    if (null != stage.getData()) {
                        for (List<String> steps : stage.getData()) {
                            if (null != event.getValue().get(0)
                                    .getInstitutionId()
                                    && null != event.getValue().get(0)
                                    .getDsaId()) {
                                StringBuilder row = new StringBuilder();
                                row.append("\"")
                                        .append(event.getValue().get(0)
                                                .getInstitutionId())
                                        .append("\"").append(',');
                                row.append("\"")
                                        .append(event.getValue().get(0)
                                                .getDsaId())
                                        .append("\"")
                                        .append(',')
                                        .append("\"")
                                        .append(event.getValue().get(0)
                                                .getGngRefId()).append("\"")
                                        .append(',');

                                for (String step : steps) {
                                    row.append("\"").append(step).append("\"")
                                            .append(',');
                                }

                                bufferedWriter.write(row.toString());
                                bufferedWriter.newLine();
                                reportOutputStreamWriter.write(row.toString());
                                reportOutputStreamWriter.write('\n');
                            }
                        }

                    }

                }
            }
        }

        bufferedWriter.flush();
        bufferedWriter.close();
        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry("CaseHistory_"
                + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv");
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        zout.setComment("CaseHistory");
        zout.finish();
        zout.closeEntry();

        return baos.toByteArray();

    }

    private int getbucketNumber(int stage, long duration) {
        switch (stage) {
            case 0: {
                return getDEBucket(duration);
            }

            case 1: {
                return getBREBucket(duration);
            }

            case 2: {
                return getIPABucket(duration);
            }

            case 3: {
                return getPDDEBucket(duration);
            }

            case 4: {
                return getPDDVBucket(duration);
            }

            default:
                break;
        }
        return 0;
    }

    private int getDEBucket(long duration) {
        if (duration <= 600000)
            return 0;
        if (600000 < duration && duration <= 1200000)
            return 1;
        else
            return 2;
    }

    private int getPDDEBucket(long duration) {
        if (duration <= 3600000)
            return 0;
        if (43200000 < duration && duration <= 86400000)
            return 1;
        else
            return 2;
    }

    private int getPDDVBucket(long duration) {
        if (duration <= 335600000)
            return 0;
        if (335600000 < duration && duration <= 864000000)
            return 1;
        else
            return 2;
    }

    private int getBREBucket(long duration) {
        if (duration <= 9000)
            return 0;
        if (9000 < duration && duration <= 1500)
            return 1;
        else
            return 2;
    }

    private int getIPABucket(long duration) {
        if (duration <= 240000)
            return 0;
        if (240000 < duration && duration <= 600000)
            return 1;
        else
            return 2;
    }

}
