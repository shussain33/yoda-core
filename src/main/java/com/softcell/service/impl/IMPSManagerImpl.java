package com.softcell.service.impl;

import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.lookup.LookupDao;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.imps.IMPSRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.imps.*;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.request.imps.IMPSResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.factory.AccountNumberValidationBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.IMPSManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.AccountNumberValidationEngine;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.NameMatchEngine;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sampat on 3/11/17.
 */

@Service
public class IMPSManagerImpl implements IMPSManager {

    private static final Logger logger = LoggerFactory.getLogger(IMPSManagerImpl.class);

    @Autowired
    AccountNumberValidationEngine accountNumberValidationEngine;

    @Autowired
    AccountNumberValidationBuilder accountNumberValidationBuilder;

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private IMPSRepository impsRepository;

    @Autowired
    FinalApplicationMongoRepository finalApplicationMongoRepository;

    @Autowired
    LookupService lookupService;

    @Autowired
    LookupDao lookupDao;

    @Autowired
    ConfigurationRepository configurationRepository;

    @Override
    public BaseResponse validateAccountNumber(IMPSRequest impsRequest) throws Exception {

        BaseResponse baseResponse;
        IMPSResponse impsResponse = new IMPSResponse();

        impsResponse = checkAccountNumberDedupe(impsRequest);

        if (StringUtils.equalsIgnoreCase(IMPSStatus.DEDUPE.name(), impsResponse.getStatus())) {
            logger.info("Account number validation request in dedupe.");
            return GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);
        }

        Integer totalAttempt = impsRepository.getAccountValidationAttempt(impsRequest.getHeader().getInstitutionId());

        if(totalAttempt>0 && null!=totalAttempt){

            if (impsRepository.checkAttemptStatus(StringUtils.join(totalAttempt,GNGWorkflowConstant.IMPS_ATTEMPT_PREFIX.toFaceValue(),impsRequest.getReferenceID()),impsRequest.getHeader().getInstitutionId())) {
                logger.info("Account number validation attempts are over.");
                impsResponse.setStatus(IMPSStatus.VALID.name());
                impsResponse.setMessage(ErrorCode.ATTEMPT_OVER);
                baseResponse =  GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);

            }else{
                baseResponse = goForAccountNumberValidation(impsRequest);
            }
        }else {

            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, GngUtils.getConfigurationNotFoundErrorList());
        }

        return baseResponse;

    }

    private BaseResponse goForAccountNumberValidation(IMPSRequest impsRequest) throws Exception {

        BaseResponse baseResponse;
        IMPSResponse impsResponse = new IMPSResponse();

        long validationCount = impsRepository.getAccountValidationCount(impsRequest.getHeader().getInstitutionId(), impsRequest.getReferenceID());
        validationCount = ++validationCount;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog = auditHelper.createActivityLog(null, impsRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.ACC_NR_VALIDATION.toFaceValue());
        // Set status to success only for valid number
        activityLog.setStatus(Status.FAIL.toString());

        AccountNumberInfo.AccountNumberInfoBuilder accountNumberInfo = accountNumberValidationBuilder.buildAccountNumberInfo(impsRequest);

        BankingDetails bankingDetails = accountNumberValidationBuilder.buildBankingDetails(impsRequest);


        if(impsRepository.checkBankInPaynimoBankMaster(impsRequest.getHeader().getInstitutionId(), impsRequest.getBankName())){

        Collection<Error> errors = accountNumberValidationEngine.validationForIMPS(impsRequest);

        if (CollectionUtils.isEmpty(errors)) {

            WFJobCommDomain impsUrlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    impsRequest.getHeader().getInstitutionId(), UrlType.IMPS.toValue());

            IMPSConfigDomain impsConfig = configurationRepository.findIMPSConfigByInstitutionId(impsRequest.getHeader().getInstitutionId());

            if (null != impsConfig) {
                AccountNumberValidationRequest accountNumberValidationRequest = accountNumberValidationBuilder.buildAccountNumberValidationRequest(impsRequest, impsConfig);

                accountNumberValidationRequest.setInstrumentHolderName(getAccountHolderFullName(impsRequest));

                accountNumberValidationRequest = getPersonalMobileNumberAndEmail(impsRequest, accountNumberValidationRequest);

                accountNumberValidationRequest.setTxnMerchantRefNo(StringUtils.join(validationCount, GNGWorkflowConstant.IMPS_ATTEMPT_PREFIX.toFaceValue(), impsRequest.getReferenceID()));

                AccountNumberValidationResponse accountNumberValidationResponse = callToIMPS(impsUrlConfig, accountNumberValidationRequest);

                if (null == accountNumberValidationResponse || StringUtils.isBlank(accountNumberValidationResponse.getTxnErrorCode())
                        || StringUtils.equalsIgnoreCase(IMPSConstants.PNY_TECHNICAL_ERROR,accountNumberValidationResponse.getTxnErrorCode())) {

                    impsResponse.setStatus(IMPSStatus.IMPS_ERROR.name());
                    impsResponse.setMessage(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    impsResponse.setTimerValue(impsConfig.getTimerValue());
                    accountNumberInfo.status(IMPSStatus.IMPS_ERROR.name());
                    bankingDetails.setStatus(IMPSStatus.IMPS_ERROR.name());
                    bankingDetails.setRemarks(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    impsResponse.setBankingDetails(bankingDetails);
                    accountNumberInfo.customMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);

                    } else if (null == accountNumberValidationResponse.getError()) {

                        impsResponse = accountNumberValidationBuilder.buildIMPSResponse(accountNumberValidationResponse);
                        accountNumberInfo.originalResponseCode(accountNumberValidationResponse.getTxnErrorCode());
                        accountNumberInfo.originalResponseMsg(accountNumberValidationResponse.getTxnErrorDesc());
                        accountNumberInfo.attemptID(StringUtils.join(validationCount, GNGWorkflowConstant.IMPS_ATTEMPT_PREFIX.toFaceValue(), impsRequest.getReferenceID()));
                        accountNumberInfo.status(IMPSStatus.INVALID.name());
                        accountNumberInfo.customMsg(accountNumberValidationResponse.getTxnErrorDesc());
                        bankingDetails.setStatus(IMPSStatus.INVALID.name());
                        bankingDetails.setRemarks(accountNumberValidationResponse.getTxnErrorDesc());
                            if (StringUtils.equalsIgnoreCase(IMPSStatus.VALID.name(), impsResponse.getStatus())) {

                                accountNumberInfo.responseAccHolderName(accountNumberValidationResponse.getAccountHolderName());
                                float nameMatchingResult = matchAccountHolderName(accountNumberValidationRequest.getInstrumentHolderName(), accountNumberValidationResponse.getAccountHolderName());
                                accountNumberInfo.nameMatchingResult(GngUtils.getDecimalValue(nameMatchingResult));
                                if (impsConfig.getNameMatchingValue() > 0 && nameMatchingResult >= impsConfig.getNameMatchingValue()) {

                                    activityLog.setStatus(IMPSStatus.VALID.name());
                                    accountNumberInfo.status(IMPSStatus.VALID.name());
                                    accountNumberInfo.customMsg(ResponseConstants.IMPS_SUCCESS);
                                    bankingDetails.setStatus(IMPSStatus.VALID.name());
                                    bankingDetails.setRemarks(ResponseConstants.IMPS_SUCCESS);
                                    impsResponse.setMessage(ResponseConstants.IMPS_SUCCESS);
                                    impsResponse.setStatus(IMPSStatus.VALID.name());

                                } else {
                                    activityLog.setCustomMsg(ErrorCode.ACC_VALID_NAME_INVALID);
                                    activityLog.setStatus(IMPSStatus.INVALID.toString());
                                    accountNumberInfo.customMsg(ErrorCode.ACC_VALID_NAME_INVALID);
                                    accountNumberInfo.status(IMPSStatus.INVALID.name());
                                    bankingDetails.setStatus(IMPSStatus.INVALID.name());
                                    bankingDetails.setRemarks(StringUtils.join(ErrorCode.ACC_VALID_NAME_INVALID, accountNumberValidationResponse.getAccountHolderName()));

                                    impsResponse.setStatus(IMPSStatus.INVALID.name());
                                    impsResponse.setMessage(ErrorCode.ACC_VALID_NAME_INVALID);
                                }

                            }
                            impsResponse.setBankingDetails(bankingDetails);
                            impsRepository.saveBankingDetails(bankingDetails,impsRequest.getHeader().getInstitutionId(),impsRequest.getReferenceID());
                            impsRepository.saveAccountNumberInfo(accountNumberInfo.build());
                            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);
                        } else {

                    impsResponse.setStatus(IMPSStatus.IMPS_ERROR.name());
                    impsResponse.setMessage(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    impsResponse.setTimerValue(impsConfig.getTimerValue());
                    accountNumberInfo.status(IMPSStatus.IMPS_ERROR.name());
                    bankingDetails.setStatus(IMPSStatus.IMPS_ERROR.name());
                    bankingDetails.setRemarks(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    impsResponse.setBankingDetails(bankingDetails);
                    accountNumberInfo.customMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    activityLog.setCustomMsg(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, Vendor.PAYNIMO.toFaceValue()));
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);

                    }
                } else {

                    errors = GngUtils.getConfigurationNotFoundErrorList();
                    activityLog.setCustomMsg(ErrorCode.CONFIGURATION_NOT_FOUND);
                    baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);

                }
            } else {
                activityLog.setStatus(Status.BUSINESS_VALIDATION_FAILED.name());
                accountNumberInfo.status(Status.BUSINESS_VALIDATION_FAILED.name());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

            }
        }else {

            accountNumberInfo.attemptID(StringUtils.join(validationCount, GNGWorkflowConstant.IMPS_ATTEMPT_PREFIX.toFaceValue(), impsRequest.getReferenceID()));
            accountNumberInfo.status(IMPSStatus.NOT_SUPPORTED.name());
            accountNumberInfo.customMsg(ErrorCode.INVALID_BANK_DETAILS);
            bankingDetails.setStatus(IMPSStatus.NOT_SUPPORTED.name());
            bankingDetails.setRemarks(ErrorCode.INVALID_BANK_DETAILS);
            activityLog.setStatus(IMPSStatus.NOT_SUPPORTED.name());
            activityLog.setCustomMsg(ErrorCode.INVALID_BANK_DETAILS);
            impsResponse.setStatus(IMPSStatus.NOT_SUPPORTED.name());
            impsResponse.setMessage(ErrorCode.INVALID_BANK_DETAILS);
            impsResponse.setBankingDetails(bankingDetails);
            baseResponse =  GngUtils.getBaseResponse(HttpStatus.OK, impsResponse);
            impsRepository.saveBankingDetails(bankingDetails,impsRequest.getHeader().getInstitutionId(),impsRequest.getReferenceID());
            impsRepository.saveAccountNumberInfo(accountNumberInfo.build());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        eventPublisher.publishEvent(activityLog);

        return baseResponse;
    }

    @Override
    public BaseResponse getAccountValidationAttempt(String institutionId, String refID) {

        int validationAttempt = impsRepository.getAccountValidationAttempt(institutionId);
        long validationCount = impsRepository.getAccountValidationCount(institutionId, refID);

        AccountValidationAttemptResponse accountValidationAttemptResponse = accountNumberValidationBuilder.buildAccountValidationAttemptResponse(validationAttempt, validationCount);

        return GngUtils.getBaseResponse(HttpStatus.OK,accountValidationAttemptResponse);

    }

    @Override
    public BaseResponse getValidatedBanks(String institutionId, String refID) {

        List<AccountNumberInfo> accountNumberInfos = impsRepository.getAccountValidatedBanks(institutionId, refID);
        List<ValidatedBanksResponse> validatedBanksResponses = new ArrayList<>();
        if (!CollectionUtils.isEmpty(accountNumberInfos)) {
            for (AccountNumberInfo accountNumberInfo : accountNumberInfos) {
                ValidatedBanksResponse validatedBanksResponse =  accountNumberValidationBuilder.buildValidatedBanksResponse(accountNumberInfo);
                validatedBanksResponses.add(validatedBanksResponse);
            }
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, validatedBanksResponses);
    }

    @Override
    public BaseResponse checkAccountNumberValidationFlag(String institutionId) {
        BaseResponse baseResponse=null;
        IMPSResponse impsResponse = new IMPSResponse();
            if(null!=lookupService.getIMPSDomainConfig(institutionId)){

                IMPSConfigDomain impsConfigDomain = lookupService.getIMPSDomainConfig(institutionId);
                if(impsConfigDomain.getIsEnabled() || impsConfigDomain.getNoOfRetry()!=0){

                    impsResponse.setStatus(Status.VALID.name());
                    impsResponse.setMessage(ErrorCode.ACC_NO_APPLICABLE);

                }else {
                    impsResponse.setStatus(Status.INVALID.name());
                    impsResponse.setMessage(ErrorCode.ACC_NO_NOT_APPLICABLE);
                }

            }else {
                impsResponse.setStatus(Status.INVALID.name());
                impsResponse.setMessage(ErrorCode.CONFIGURATION_NOT_FOUND);
            }
        return GngUtils.getBaseResponse(HttpStatus.OK,impsResponse);
    }

    @Override
    public BaseResponse updateBankingDetailsImps(BankingDetails bankingDetails, String institutionId, String refID) {

       return GngUtils.getBaseResponse(HttpStatus.OK,impsRepository.updateAccountNumberValidStatus(institutionId,refID,bankingDetails));

    }

    private AccountNumberValidationResponse callToIMPS(WFJobCommDomain impsUrlConfig, AccountNumberValidationRequest accountNumberValidationRequest) throws Exception {
        String url = Arrays.asList(impsUrlConfig.getBaseUrl(), impsUrlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));
        return (AccountNumberValidationResponse) TransportUtils.postJsonRequest(accountNumberValidationRequest, url, AccountNumberValidationResponse.class);

    }

    private String getAccountHolderFullName(IMPSRequest impsRequest){
        StringBuilder fullName = new StringBuilder();

        return fullName.append(impsRequest.getAccountHolderFName())
                .append(FieldSeparator.SPACE_STR)
                .append(StringUtils.isNotEmpty(impsRequest.getAccountHolderMName())?impsRequest.getAccountHolderMName():FieldSeparator.BLANK)
                .append(FieldSeparator.SPACE_STR)
                .append(impsRequest.getAccountHolderLName()).toString();

    }

    private AccountNumberValidationRequest getPersonalMobileNumberAndEmail(IMPSRequest impsRequest, AccountNumberValidationRequest accountNumberValidationRequest){

       GoNoGoCustomerApplication customerApplication =  finalApplicationMongoRepository.getGoNoGoApplication(impsRequest.getReferenceID());

       if(null!= customerApplication &&
               null!=customerApplication.getApplicationRequest() &&
               null!=customerApplication.getApplicationRequest().getRequest() &&
               null!=customerApplication.getApplicationRequest().getRequest().getApplicant()){

           Applicant applicant = customerApplication.getApplicationRequest().getRequest().getApplicant();

           accountNumberValidationRequest.setConsMobileNumber(GngUtils.getPhoneNumber(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(),applicant.getPhone()));
           accountNumberValidationRequest.setConsEmailID(GngUtils.getEmailId(GNGWorkflowConstant.PERSONAL.toFaceValue(),applicant.getEmail()));
       }else {
           accountNumberValidationRequest.setConsMobileNumber(FieldSeparator.BLANK);
           accountNumberValidationRequest.setConsEmailID(FieldSeparator.BLANK);
       }

        return accountNumberValidationRequest;
    }

    private IMPSResponse checkAccountNumberDedupe(IMPSRequest impsRequest) {

        IMPSResponse impsResponse = new IMPSResponse();

        if (impsRepository.checkAccountNumberInDedup(impsRequest)) {

            impsResponse.setMessage(ErrorCode.ACCOUNT_NO_DEDUPE);
            impsResponse.setStatus(IMPSStatus.DEDUPE.name());
        }
        return impsResponse;
    }

    private float matchAccountHolderName(String dsaEnteredName, String paynimoResponseName){

        NameMatchEngine nameMatchEngine = new NameMatchEngine();

        return nameMatchEngine.match(paynimoResponseName.toString(),dsaEnteredName.toString())*100;

    }
}
