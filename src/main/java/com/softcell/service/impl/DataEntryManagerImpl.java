package com.softcell.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.*;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.*;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.*;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.admin.WorkflowMaster;
import com.softcell.gonogo.model.configuration.admin.WorkflowNode;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.BalanceSheet;
import com.softcell.gonogo.model.core.ProfitAndLoss;
import com.softcell.gonogo.model.core.Ratios;
import com.softcell.gonogo.model.core.cam.*;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.eligibility.EligibleOffers;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.perfios.BankData;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.perfios.RetriveFileData;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.request.DeviationRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.*;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.BankDetailsMaster;
import com.softcell.gonogo.model.masters.DeviationMaster;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.mifin.crm.CrmResponse;
import com.softcell.gonogo.model.mifin.topup.AddressDetail;
import com.softcell.gonogo.model.mifin.topup.MatchApplicantDetails;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.ssl.perfios.PerfiosRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.request.digitalcore.*;
import com.softcell.gonogo.model.response.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.*;
import com.softcell.service.thirdparty.utils.IciciHelper;
import com.softcell.service.thirdparty.utils.ReligareHelper;
import com.softcell.service.utils.MiFinHelper;
import com.softcell.service.utils.OriginationHelper;
import com.softcell.service.validator.ApplicationValidationEngine;
import com.softcell.utils.*;
import com.softcell.workflow.RequestAggregator;
import com.softcell.workflow.SequenceGenerator;
import com.softcell.workflow.executors.extapi.ThirdPartyExecutor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * @author yogeshb
 */
@Service
public class DataEntryManagerImpl implements DataEntryManager {

    private static Logger logger = LoggerFactory.getLogger(DataEntryManagerImpl.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationValidationEngine applicationValidationEngine;

    @Autowired
    private RequestAggregator requestAggregator;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ModuleManager moduleManager;

    @Autowired
    private ModuleHelper moduleHelper;

    @Autowired
    private MiFinHelper miFinHelper;

    @Autowired
    private FinalApplicationRepository finalApplicationRepository;

    @Autowired
    private ExternalApiManager externalApiManager;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private MasterDataViewRepository masterDataViewRepository;

    @Autowired
    AppConfigurationManager appConfigurationManager;

    @Autowired
    AppConfigurationHelper appConfigurationHelper;

    @Autowired
    private CroManager croManager;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    ReligareHelper  religareHelper;

    @Autowired
    private OriginationHelper originationHelper;

    @Autowired
    private MasterDataMongoRepository masterDataRepository;

    @Override
    public BaseResponse save(
            ApplicationRequest applicationRequest, String stepId, HttpServletRequest httpRequest)  {
        BaseResponse response = null;
        logger.debug("save() service called for step {}", stepId);
        applicationRequest.getHeader().setDateTime(new Date());

        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest, httpRequest,
                null, stepId, null, applicationRequest.getHeader().getLoggedInUserId());
        if (applicationRequest.getRefID() != null) {
            activityLogs.setRefId(applicationRequest.getRefID());
        }

        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        String action = null;
        try {
            // version check point
            Object versionError = validateVersion(applicationRequest, httpRequest);
            if (versionError != null) {
                response = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, versionError);
            } else {
                String refId = applicationRequest.getRefID();
                Header header = applicationRequest.getHeader();
                StopWatch stopWatch = new StopWatch();
                stopWatch.start();
                logger.debug("Starting with step {} for refId {}", stepId, refId);
                switch (stepId) {
                    case EndPointReferrer.STEP_THIRD_PARTY_DATA:
                        setAge(applicationRequest);
                    case EndPointReferrer.STEP_REGISTRATION: {
                        // Validations
                        logger.debug("{} :Validation for refId {}", stepId, refId);
                        applicationValidationEngine.validate(applicationRequest, stepId);
                        setmisFieldData(applicationRequest);

                        Boolean saveResult;
                        // ID generation
                        if (StringUtils.isBlank(applicationRequest.getRefID())) {

                            // This is start of application punching
                            // SO stage is DE and status is NEW
                            // Set Stage to DE
                            applicationRequest.setCurrentStageId(GNGWorkflowConstant.DE.name());
                            activityLogs.setStage(GNGWorkflowConstant.DE.name());
                            activityLogs.setStatus(Status.NEW.toString());

                            logger.debug("Generating refId ");

                            Object object = generateAndSetRefId(applicationRequest);

                            if (object instanceof Acknowledgement) {
                                logger.info("Error in refId generation: {}", JsonUtil.ObjectToString(object));
                                activityLogs.setCustomMsg("Error in refId generation:" + JsonUtil.ObjectToString(object));
                                return GngUtils.getBaseResponse(HttpStatus.OK, object);
                            }
                            refId = applicationRequest.getRefID();
                            action = IntimationConstants.REGISTRATION;
                            // Set refId for activity log
                            activityLogs.setRefId(applicationRequest.getRefID());
                            // Set applicationId
                            applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());
                            //set source (as of now leviosa for digi-pl)
                            if(StringUtils.equalsIgnoreCase(header.getProduct().name(),Product.DIGI_PL.name())){
                                applicationRequest.getHeader().setSource("Leviosa");
                            }

                            // Save AppRequest
                            logger.debug("{} : inserting applicationRequest refId {}", stepId, refId);
                            applicationRepository.saveApplication(applicationRequest);

                            //in case of personal loan for prepoulate
                            if (applicationRequest.getHeader().getProduct() == Product.PL) {
                                moduleHelper.presaveEligibilityDetails(applicationRequest, stepId);
                            }
                            // For Co origination SBFC
                            moduleManager.checkForCoOrigination(applicationRequest);

                            goNoGoCustomerApplication = RequestAggregator.getGoNoGo(applicationRequest);
                            // Save GnGCustomerApplication
                            logger.debug("{} : inserting goNoGoCustomerApplication refId {}", stepId, refId);
                            saveResult = applicationRepository.saveGoNoGoCustomerApplication(goNoGoCustomerApplication);

                            // Add this step in completed-set of execution items
                            applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                                    EndPointReferrer.STEP_REGISTRATION, header.getLoggedInUserId(), header.getLoggedInUserRole());
                        } else {
                            // Update application
                            logger.debug("{} : updating goNoGoCustomerApplication refId {}", stepId, refId);
                            saveResult = applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
                            //update eligibility details
                            //in case of personal loan for prepoulate
                            if (applicationRequest.getHeader().getProduct() == Product.PL) {
                                moduleHelper.presaveEligibilityDetails(applicationRequest, stepId);
                            }

                            // For Co origination SBFC
                            moduleManager.checkForCoOrigination(applicationRequest);

                            // Fetch GogogoApplication
                            logger.debug("{} : fetching goNoGoCustomerApplication refId {}", stepId, refId);
                            goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
                        }
                        // After-save module check
                        logger.debug("{} : after-save module check refId {}", stepId, refId);

                        moduleManager.executeBre(goNoGoCustomerApplication, stepId, header.getLoggedInUserRole(), header,
                                ConfigurationConstants.WF_NODE_TYPE_SCREEN);

                        //Acknowledgement based on db operation.
                        if (saveResult) {
                            response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
                        } else {
                            response = GngUtils.getBaseResponse(HttpStatus.OK, RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                                    ErrorCode.INVALID_REQUEST_DSCR, null, null));
                        }

                        break;
                    }// STEP_REGISTRATION
                    case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:
                        logger.info("step 1 : logs for kyc check, step {}  refId {} oReq {}", stepId, refId, applicationRequest.getRequest());
                        action = (action == null) ? IntimationConstants.DEMOGRAPHIC_DETAILS : action;
                        setAge(applicationRequest);
                        moduleHelper.setcity(applicationRequest);
                        if(Institute.isInstitute(header.getInstitutionId(),Institute.SBFC) && applicationRequest.getRequest() != null){
                            moduleHelper.makeNameCapital(applicationRequest.getRequest());
                        }
                    case EndPointReferrer.STEP_PROFESSION_INCOME_DETAILS:
                        action = (action == null) ? IntimationConstants.MCP_DETAILS : action;
                    case EndPointReferrer.STEP_COLLATERAL_DETAILS:
                        action = (action == null) ? IntimationConstants.COLLATERALS_DETAILS : action;
                    case EndPointReferrer.STEP_IMD_DETAILS:
                        action = (action == null) ? IntimationConstants.IMD_DETAILS : action;
                        //in case of personal loan for prepoulate
                        if (applicationRequest.getHeader().getProduct() == Product.PL) {
                            moduleHelper.presaveEligibilityDetails(applicationRequest, stepId);
                            moduleHelper.preSaveCamSummary(applicationRequest);
                        }
                        goNoGoCustomerApplication = process(applicationRequest, stepId, header.getLoggedInUserId(), header.getLoggedInUserRole());
                        logger.info("step 3 : logs for kyc check, step {}  refId {} oReq {}", stepId, refId, goNoGoCustomerApplication.getApplicationRequest().getRequest());
                        activityLogs.setStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                        activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());

                        response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
                        logger.debug(" FINISHED step {}  refId {} ", stepId, refId);

                        break;

                    case EndPointReferrer.STEP_BANKING_DETAILS:
                        // in case of pl only
                        moduleHelper.preSaveRepaymentDetails(applicationRequest);

                        applicationValidationEngine.validate(applicationRequest, stepId);
                        // Save request
                        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

                        // Add this step in completed-set of execution items
                        applicationRepository.updateCompletedInfo(refId, applicationRequest.getHeader().getInstitutionId(), stepId, header.getLoggedInUserId(), header.getLoggedInUserRole());
                        // Get CustomerApplication from DB
                        goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
                        /*logger.debug("{} : fetched GoNoGoCustomerApplication refId {} :\n {}", stepId, refId,
                                goNoGoCustomerApplication.getApplicationRequest());*/

                        activityLogs.setStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                        activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());

                        response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
                        logger.debug(" FINISHED step {}  refId {} ", stepId, refId);
                        break;

                    default:
                        String errorMsg = String.format("STEP %s is not defined : refid %s", stepId, applicationRequest.getRefID());
                        logger.warn("STEP {} is not defined : refid {}", stepId, applicationRequest.getRefID());
                        activityLogs.setCustomMsg(
                                new StringBuilder().append(errorMsg).append(" | ")
                                        .append(ErrorCode.INVALID_REQUEST_DSCR).append(" | ")
                                        .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                                        .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                                        .append(Error.SEVERITY.CRITICAL.name()).toString());

                        Collection<Error> errors = new ArrayList<>();
                        errors.add(Error.builder()
                                .message(errorMsg)
                                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                .level(Error.SEVERITY.CRITICAL.name())
                                .build());
                        response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
                        break;

                }// switch
                // Intimate Users
                if (action != null) {
                    appConfigurationHelper.intimateUsers(goNoGoCustomerApplication.getGngRefId(), action);
                }
                // Save activity log
                stopWatch.stop();
                activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());
                activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
                logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                        Thread.currentThread().getName()));
                activityEventPublisher.publishEvent(activityLogs);
            } // else
        } catch (GoNoGoException gngException) {
            logger.error(gngException.getStackTrace().toString());
            //gngException.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(gngException.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }catch (Exception e){
            logger.error("Error while saving", e.getStackTrace());
        }
        return response;
    }


    @Override
    public BaseResponse submit(ApplicationRequest applicationRequest, String stepId, HttpServletRequest httpRequest) {

        BaseResponse response = null;
        Header header = applicationRequest.getHeader();
        String insttId = header.getInstitutionId();
        String userId = header.getLoggedInUserId();
        String role = header.getLoggedInUserRole();
        boolean isCredit = false;
        if (Institute.isInstitute(header.getInstitutionId(), Institute.SBFC)) {
            if (header.getCredit() != null) {
                isCredit = header.getCredit();
            }
        }
        if(isCredit){
            role = Roles.Role.CREDIT.name();
        }

        logger.debug("submit() service called for step {}, {} by {} of role {} ", stepId, applicationRequest.getRefID(), userId, role);
        applicationRequest.getHeader().setDateTime(new Date());

        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest, httpRequest,
                null, stepId, null, applicationRequest.getHeader().getLoggedInUserId());
        if (applicationRequest.getRefID() != null) {
            activityLogs.setRefId(applicationRequest.getRefID());
        }

        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        GoNoGoCustomerApplication goNoGoCustApp = null;
        try {
            // version check point
            Object versionError = validateVersion(applicationRequest, httpRequest);
            if (versionError != null) {
                response = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, versionError);
            } else {
                String refId = applicationRequest.getRefID();
                StopWatch stopWatch = new StopWatch();
                stopWatch.start();

                String loggedInRole = applicationRequest.getHeader().getLoggedInUserRole();
                if(isCredit){
                    loggedInRole = Roles.Role.CREDIT.name();
                }
                String currentStage = applicationRepository.getApplicationStage(insttId, refId);

                activityLogs.setStage(currentStage);

                boolean updateStage = true;
                String applicationStatus = null;
                // Application Request data is sent only in case of FOS submit !!

                // Validate IMD is cleared or not on HOPS submit
                goNoGoCustApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                if (Institute.isInstitute(insttId, Institute.SBFC) && StringUtils.equalsIgnoreCase(role, Roles.Role.HOPS.name())
                        && goNoGoCustApp.getApplicationRequest().getHeader().getProduct() == Product.LAP) {
                    Error error = verifyIMDCleared(goNoGoCustApp);
                    if (error != null) {
                        throw new GoNoGoException(error.getMessage());
                    }
                }

                // Validate Stage Change action
                moduleManager.validateStageChange(applicationRequest.getRefID(), header, currentStage);

                String product = applicationRequest.getHeader().getProduct().name();
                WorkflowMaster workflowMaster = appConfigurationManager.getActiveWorkFlow(insttId, product);
                String action = null;
                String changedStage = null;
                if (workflowMaster != null) {
                    WorkflowNode node = appConfigurationHelper.getWorkflowNode(currentStage, ConfigurationConstants.WF_NODE_TYPE_STAGE,
                            ConfigurationConstants.WF_EVENT_SUBMIT, workflowMaster);
                    if (node != null) {
                        goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                        Application application = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication();
                        String flowType = ConfigurationConstants.APPLICATION_FLOW_TYPE_NORMAL;
                        if (application.isCoOrigination()) {
                            flowType = ConfigurationConstants.APPLICATION_FLOW_TYPE_COORIGINATION;
                        }
                        String nextStage = currentStage;
                        if (Cache.ROLE_ENABLED_STAGES_MAP.get(loggedInRole).contains(currentStage)) {
                            nextStage = appConfigurationHelper.getNextStage(node, currentStage,
                                    goNoGoCustomerApplication.getApplicationStatus(), loggedInRole, flowType);
                            applicationRequest.setCurrentStageId(nextStage);
                            changedStage = nextStage;
                            stepId = "submit-by-" + loggedInRole;
                            applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);
                            // After-submit module check
                            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                            goNoGoCustomerApplication.getApplicationRequest().setFinalTranchCancelled(applicationRequest.isFinalTranchCancelled());
                            goNoGoCustomerApplication.getApplicationRequest().setFinalTranchCancelledRemarks(applicationRequest.getFinalTranchCancelledRemarks());
                            moduleManager.executeBre(goNoGoCustomerApplication, currentStage, role, applicationRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_STAGE);
                            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                        }

                        String bucket = "";
                        if(StringUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationBucket())) bucket = goNoGoCustomerApplication.getApplicationBucket();

                        if ( !Stages.isBopsStage(currentStage)
                                && (StringUtils.equalsIgnoreCase(nextStage, GNGWorkflowConstant.STAGE_CHANGE_VIA_CODE.toFaceValue()) || Buckets.isSTPBucket(bucket))) {
                            // Already changed via executeBRE and updated in DB
                            updateStage = false;
                        } else {
                            goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(nextStage);
                        }
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                    } else {
                        applicationRequest.setCurrentStageId(currentStage);
                    }
                } else {
                    // Set Stage according to role
                    if (StringUtils.equalsIgnoreCase(currentStage, GNGWorkflowConstant.DE.name())
                            || StringUtils.equalsIgnoreCase(currentStage, GNGWorkflowConstant.CR_Q.name())) {
                        // TODO : rectify the check a bit :( Remove hardcoded literals.
                        // There are different steps for FOS
                        if (StringUtils.equalsIgnoreCase(stepId, "stepId")) {
                            // update step id
                            applicationRepository.updateCompletedInfo(refId, insttId, "submit-by-fos", userId, role);
                        } else {
                            process(applicationRequest, stepId, userId, role);
                        }
                        applicationRequest.setCurrentStageId(GNGWorkflowConstant.DDE.name());
                        changedStage = GNGWorkflowConstant.DDE.name();
                    }
                    if (Roles.Role.valueOf(loggedInRole) == Roles.Role.CPA) {
                        // There are different steps for CPA
                        if (StringUtils.equalsIgnoreCase(stepId, "stepId")) {
                            // update step id
                            stepId = "submit-by-cpa";
                        }
                        applicationRequest.setCurrentStageId(GNGWorkflowConstant.CRDT.name());
                        changedStage = GNGWorkflowConstant.CRDT.name();
                        // update step id
                        applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);

                    } else if (StringUtils.equalsIgnoreCase(loggedInRole,Roles.Role.CREDIT.name()) || Roles.isCreditRole(loggedInRole) || isCredit) {
                        //checking applicationStatus
                        if (currentStage.equalsIgnoreCase(GNGWorkflowConstant.APRV.toFaceValue())) {
                            applicationRequest.setCurrentStageId(GNGWorkflowConstant.BOPS.name());
                            changedStage = GNGWorkflowConstant.BOPS.name();
                        } else {
                            // TODO Add ref id, role, stage
                            throw new GoNoGoException("Submitted at wrong stage");
                        }
                        // update step id
                        stepId = "submit-by-" + loggedInRole;
                        applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);

                    } else if (Roles.Role.valueOf(loggedInRole) == Roles.Role.BOPS) {
                        applicationRequest.setCurrentStageId(GNGWorkflowConstant.HOPS.name());
                        changedStage = GNGWorkflowConstant.HOPS.name();
                        // update step id
                        stepId = "submit-by-BOPS";
                        applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);

                    } else if (Roles.Role.valueOf(loggedInRole) == Roles.Role.HOPS) {
                    /* HOPS submits DISB case to miFin.
                        DISB is last stage of the application.
                    */
                        if (currentStage.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue())
                                || currentStage.equalsIgnoreCase(GNGWorkflowConstant.TRANCH_HOPS.toFaceValue())) {
                            applicationRequest.setCurrentStageId(GNGWorkflowConstant.PUSH_TO_LMS.name());
                            changedStage = GNGWorkflowConstant.PUSH_TO_LMS.name();
                            // update DM details
                            updateDM(applicationRequest);
                            // update step id
                            stepId = "submit-by-HOPS";
                            applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);
                            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                            // Case has reached to end of processing; push data to LMS system.
                            externalAPICall(goNoGoCustomerApplication, applicationRequest);
                            //reverting to previous stage if Mifin push failed.
                            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(),
                                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                                logger.info("Attention {} ...Revering to stage HOPS and status Approved!!", applicationRequest.getRefID());
                                applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                                changedStage = GNGWorkflowConstant.HOPS.name();
                                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                            }
                            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(),
                                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                                applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                                changedStage = GNGWorkflowConstant.TRANCH_HOPS.name();
                                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                            }
                            // update application status to Disbursed if pushed to LMS
                            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISB.toFaceValue(),
                                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                                changedStage = GNGWorkflowConstant.DISB.name();
                                applicationStatus = GNGWorkflowConstant.DISBURSED.toFaceValue();
                                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                            }

                        } else {
                            throw new GoNoGoException("Submitted at wrong stage");
                        }
                        // update step id
                        applicationRepository.updateCompletedInfo(refId, insttId, stepId, userId, role);

                    }
                }/*else {
                    throw new GoNoGoException(String.format("Role %s not configured for SUBMIT action", loggedInRole));
                }*/

                if (updateStage) {
                    // update stage in db
                    updateStageId(applicationRequest, applicationStatus);
                }

                // Remove allocation status of application as stage of application is changed
                applicationRepository.updateGoNoGoCustomerApplication(refId, null, insttId);

                // Intimate Users
                if (changedStage != null)
                    action = appConfigurationHelper.getActionBasesOnRole(insttId, product, role, changedStage, isCredit);
                //For SBFC
                if(StringUtils.isNotEmpty(action) && Institute.isInstitute(insttId, Institute.SBFC) && StringUtils.equalsIgnoreCase(action, IntimationConstants.HOPS_SUBMIT)
                        && !(StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISB.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())
                        || StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISBURSED.toFaceValue(), goNoGoCustomerApplication.getApplicationStatus()))){
                    //In case if action is HOPS Submit and case is disbursed then only intimation should happen for SBFC.
                    logger.debug("setting action {} to null where currentStageId is {} and applicationStatus is",
                            action, goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(), goNoGoCustomerApplication.getApplicationStatus());
                    action = null;
                }
                if (action != null) {
                    appConfigurationHelper.intimateUsers(refId, action);
                }

                // fetch updated doc
                goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
                goNoGoCustomerApplication.setEditable(false);

                activityLogs.setChangedStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());
                activityLogs.setAction(EndPointReferrer.SUBMIT_APPLICATION);
                activityLogs.setCustomMsg("Stage changed from " + currentStage + " to " + goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
                logger.debug(" FINISHED step {}  refId {} ", stepId, refId);

                // Save activity log
                stopWatch.stop();
                activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
                logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                        Thread.currentThread().getName()));
                activityEventPublisher.publishEvent(activityLogs);
            }
        } catch (GoNoGoException gngException) {
            logger.error("error while submit {}",gngException.getMessage());
            gngException.printStackTrace();
            response = GngUtils.getBaseResponse(HttpStatus.OK, gngException.getMessage());
        } catch (Exception gngException) {
            logger.error("error while submit {}",gngException.getMessage());
            gngException.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(gngException.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }

        return response;
    }

    @Override
    public void updateDM(ApplicationRequest applicationRequest) {
        DMRequest dmRequest = new DMRequest();
        dmRequest.setRefId(applicationRequest.getRefID());
        dmRequest.setHeader(applicationRequest.getHeader());
        DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(dmRequest);
        if (disbursementMemo != null) {
            disbursementMemo.setDmName(applicationRequest.getHeader().getLoggedInUserId());
            dmRequest.setDisbursementMemo(disbursementMemo);
            applicationRepository.saveDMDetails(dmRequest);
        }
    }

    @Override
    public BaseResponse savePropertyVisitDetails(PropertyVisitRequest propertyVisitRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        String stepId = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(propertyVisitRequest.getHeader(), propertyVisitRequest.getRefId(),
                httpRequest, GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_PROPERTY_VISIT_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), propertyVisitRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving PropertyVisit Details refId {}", propertyVisitRequest.getRefId());

            boolean save = applicationRepository.savePropertyVisitDetails(propertyVisitRequest);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = propertyVisitRequest.getHeader();
                // intimate users
                //appConfigurationHelper.intimateUsers(propertyVisitRequest.getRefId(), IntimationConstants.PROPERTY_VISIT);
                if (Institute.isInstitute(propertyVisitRequest.getHeader().getInstitutionId(), Institute.FVSG)) {
                    stepId = EndPointReferrer.SAVE_PROPERTY_VISIT_DETAILS + "by" + propertyVisitRequest.getHeader().getLoggedInUserRole();
                } else {
                    stepId = EndPointReferrer.SAVE_PROPERTY_VISIT_DETAILS;
                }
                applicationRepository.updateCompletedInfo(propertyVisitRequest.getRefId(), header.getInstitutionId(),
                        stepId, header.getLoggedInUserId(), header.getLoggedInUserRole());

                //appConfigurationHelper.intimateUsers(propertyVisitRequest.getRefId(), IntimationConstants.PROPERTY_VISIT);

                PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, propertyVisit);
            }
        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getPropertyVisitDetails(PropertyVisitRequest propertyVisitRequest) {
        BaseResponse response = new BaseResponse();
        PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);
        if (propertyVisit != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, propertyVisit);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse savePersonalDiscussion(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID(),applicationRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(), applicationRequest.getRefID(), httpRequest,
                applicationStage, EndPointReferrer.SAVE_PERSONAL_DISCUSSION_DETAILS,
                applicationStatus, applicationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving PersonalDiscussion Details refId {}", applicationRequest.getRefID());
            boolean save = applicationRepository.savePersonalDiscussionDetails(applicationRequest);
            if (save) {
                // Add this step in completed-set of execution items
                String stepId = EndPointReferrer.SAVE_PERSONAL_DISCUSSION_DETAILS;
                Header header = applicationRequest.getHeader();
                applicationRepository.updateCompletedInfo(applicationRequest.getRefID(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_PERSONAL_DISCUSSION_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
                appConfigurationHelper.intimateUsers(goNoGoCustomerApplication.getGngRefId(), IntimationConstants.PERSONAL_DETAILS);
                response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
            }
        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getRepaymentDetails(RepaymentRequest repaymentRequest) {
        BaseResponse response = new BaseResponse();
        Repayment repayment = applicationRepository.fetchRepaymentDetails(repaymentRequest);
        if (repayment != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, repayment);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse saveRepaymentDetails(RepaymentRequest repaymentRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(repaymentRequest.getRefId(),repaymentRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(repaymentRequest.getHeader(), repaymentRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.SAVE_REPAYMENT_DETAILS,
                applicationStatus, repaymentRequest.getHeader().getLoggedInUserId());

        Repayment repayment = repaymentRequest.getRepayment();
        repayment.setRefId(repaymentRequest.getRefId());
        repayment.setInstitutionId(repaymentRequest.getHeader().getInstitutionId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving RepaymentDetails Details refId {}", repaymentRequest.getRefId());
            boolean save = applicationRepository.saveRepaymentDetails(repayment);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = repaymentRequest.getHeader();
                applicationRepository.updateCompletedInfo(repaymentRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_REPAYMENT_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                repayment = applicationRepository.fetchRepaymentDetails(repaymentRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, repayment);
            }
        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse saveLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(legalVerificationRequest.getHeader(), legalVerificationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_LEGAL_VERIFICATION_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), legalVerificationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
            logger.debug("{} : saving LegalVerification Details refId {}", legalVerificationRequest.getRefId());
            boolean save = applicationRepository.saveLegalVerificationMultiDetails(legalVerificationRequest, verificationStatusDealersMap);

            if (save) {
                if (!verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).isEmpty() ||
                        !verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).isEmpty()) {
                    String verificationAction = IntimationConstants.VERIFICATION_TRIGGER;
                    if (verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).isEmpty()) {
                        verificationAction = IntimationConstants.VERIFICATION_DONE;
                    }
                    appConfigurationHelper.intimateUsers(legalVerificationRequest.getRefId(),
                            verificationAction, IntimationConstants.VER_TYPE_LEGAL, verificationStatusDealersMap);
                }
                // Add this step in completed-set of execution items
                Header header = legalVerificationRequest.getHeader();
                applicationRepository.updateCompletedInfo(legalVerificationRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_LEGAL_VERIFICATION_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, legalVerification);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(legalVerificationRequest.getHeader(), legalVerificationRequest.getRefId(), httpRequest,
                null, EndPointReferrer.GET_LEGAL_VERIFICATION_DETAILS,
                null, legalVerificationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
        Name name = applicationRepository.fetchApplicantName(legalVerificationRequest.getRefId());


        if (legalVerification != null && legalVerification.getLegalVerificationDetailsList() == null) {
            LegalVerificationDetails legalVerificatioDetailsByRefId = applicationRepository.fetchOldLegalVerificatioDetailsByRefId(legalVerificationRequest);
            if (legalVerificatioDetailsByRefId != null) {
                legalVerificatioDetailsByRefId.setCollateralId("0");
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                if (StringUtils.equalsIgnoreCase(legalVerificatioDetailsByRefId.getStatus(), ThirdPartyVerification.Status.PENDING.name()) && name != null) {
                    legalVerificatioDetailsByRefId.getLegalVerificationInput().setApplicantName(name);
                    legalVerificatioDetailsByRefId.setApplicantName(name);
                }
                legalVerificationDetailsList.add(legalVerificatioDetailsByRefId);
                legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            }
        }
        if (legalVerification != null) {
            List<LegalVerificationDetails> legalVerificationDetailsList = legalVerification.getLegalVerificationDetailsList();
            for (LegalVerificationDetails legalVerficationdetail : legalVerificationDetailsList) {
                if (StringUtils.equalsIgnoreCase(legalVerficationdetail.getStatus(), ThirdPartyVerification.Status.PENDING.name()) && name != null) {
                    legalVerficationdetail.getLegalVerificationInput().setApplicantName(name);
                    legalVerficationdetail.setApplicantName(name);
                }
            }

            response = GngUtils.getBaseResponse(HttpStatus.OK, legalVerification);
        } else {
            LegalVerification legalVerification1 = new LegalVerification();
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(legalVerificationRequest.getRefId());
            if (goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getCollateral() != null) {
                List<Collateral> collateralList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getCollateral();
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                for (Collateral collateral : collateralList) {
                    LegalVerificationDetails legalVerificationDetails = new LegalVerificationDetails();
                    legalVerificationDetails.setCollateralId(collateral.getCollateralId());
                    legalVerificationDetails.setLegalVerificationOutput(new LegalVerificationOutput());
                    legalVerificationDetails.setLegalVerificationInput(new LegalVerificationInput());

                    legalVerificationDetails.getLegalVerificationInput().setPropertyAddress(collateral.getAddress());
                    legalVerificationDetails.getLegalVerificationInput().setPropOwnerName(collateral.getOwnerNames());
                    legalVerificationDetails.getLegalVerificationInput().setPropType(collateral.getType());
                    legalVerificationDetails.getLegalVerificationOutput().setOwnerNameOnPapers(collateral.getOwnerNames());
                    legalVerificationDetails.setApplicantName(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName());
                    legalVerificationDetails.setStatus(ThirdPartyVerification.Status.PENDING.name());
                    legalVerificationDetailsList.add(legalVerificationDetails);
                }
                legalVerification1.setLegalVerificationDetailsList(legalVerificationDetailsList);
                legalVerificationRequest.setLegalVerification(legalVerification1);
                Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
                verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
                verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
                Boolean save = applicationRepository.saveLegalVerificationMultiDetails(legalVerificationRequest, verificationStatusDealersMap);
                if (save != null) {
                   /* appConfigurationHelper.intimateUsers(legalVerificationRequest.getRefId(),
                            IntimationConstants.VERIFICATION_TRIGGER, IntimationConstants.VER_TYPE_LEGAL, verificationStatusDealersMap);
                    */
                    response = GngUtils.getBaseResponse(HttpStatus.OK, legalVerification1);
                }
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        }

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse saveLoanChargesDetails(LoanChargesRequest loanChargesRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        boolean flag = false;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(loanChargesRequest.getRefId(),loanChargesRequest.getHeader().getInstitutionId());
        if (null != goNoGoCustomerApplication && null != goNoGoCustomerApplication.getApplicationRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId() && goNoGoCustomerApplication.getApplicationStatus() != null) {
            applicationStage = goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId();
            applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(loanChargesRequest.getHeader(), loanChargesRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS,
                applicationStatus, loanChargesRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("saving LoanCharges Details refId {}", loanChargesRequest.getRefId());
            String quarter = GngDateUtil.getQuaterFromDate(loanChargesRequest.getLoanCharges().getDisbursedDate());
            loanChargesRequest.getLoanCharges().setQuarter(quarter);
            boolean save = applicationRepository.saveLoanChargesDetails(loanChargesRequest);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = loanChargesRequest.getHeader();
                applicationRepository.updateCompletedInfo(loanChargesRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetails(loanChargesRequest);
                try{
                    if (Institute.isInstitute(loanChargesRequest.getHeader().getInstitutionId(), Institute.SBFC)) {
                        flag = moduleHelper.calculatePremium(loanChargesRequest.getRefId(),
                                loanChargesRequest.getHeader(), loanCharges.getLoanDetails().getFinalDisbAmt(),
                                loanCharges.getTenureDetails().getDisbTenureMonths(), EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS);
                        if(flag){
                            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(loanChargesRequest.getRefId());
                            loanCharges = moduleHelper.populateInsuranceInLoanCharges(loanCharges,insuranceData);
                            loanCharges.setInsuranceMsg("Re-computing Calculate premium amount");
                        }
                    }
                }catch (Exception ex){
                    logger.error("while calculating insurance in LoanCharges {}", ex.getStackTrace());
                }
                response = GngUtils.getBaseResponse(HttpStatus.OK, loanCharges);
                try{
                    /*  Even After co-origination save if user changes value in loan charges  then Co-origination value will distribute according to master
                Existing value will be override*/
                    originationHelper.saveOriginationForUpdateValues(loanChargesRequest.getRefId(), header, loanCharges);
                }catch (Exception ex){
                    logger.error("Exception occurs while updating {}" , ExceptionUtils.getStackTrace(ex));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getLoanChargesDetails(LoanChargesRequest loanChargesRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(loanChargesRequest.getRefId(),loanChargesRequest.getHeader().getInstitutionId());
        if (null != goNoGoCustomerApplication && null != goNoGoCustomerApplication.getApplicationRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId() && goNoGoCustomerApplication.getApplicationStatus() != null) {
            applicationStage = goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId();
            applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(loanChargesRequest.getHeader(), loanChargesRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.GET_LOAN_CHARGES_DETAILS,
                applicationStatus, loanChargesRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetails(loanChargesRequest);
        InsuranceData insuranceData = applicationRepository.fetchInsuranceData(loanChargesRequest.getRefId());
        if(Institute.isInstitute(loanChargesRequest.getHeader().getInstitutionId(),Institute.SBFC)
                || Institute.isInstitute(loanChargesRequest.getHeader().getInstitutionId(),Institute.AMBIT)) {
            if (!StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),
                    GNGWorkflowConstant.DISB.toFaceValue()) || !StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationStatus(),
                    GNGWorkflowConstant.DISBURSED.toFaceValue())) {
                loanCharges = moduleHelper.populateInsuranceInLoanCharges(loanCharges, insuranceData);
                if(loanCharges != null){
                    loanCharges.setImdCollectedAmt(buildIMDCollectedAmt(goNoGoCustomerApplication));
                }
            }
        }
        if(loanCharges != null){
            if(Institute.isInstitute(loanChargesRequest.getHeader().getInstitutionId(),Institute.SBFC) ||
                    Institute.isInstitute(loanChargesRequest.getHeader().getInstitutionId(),Institute.AMBIT)) {
                applicationRepository.saveLoanChargesDetails(loanChargesRequest.getRefId(), loanChargesRequest.getHeader().getInstitutionId(), loanCharges);
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, loanCharges);
        }else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    private double buildIMDCollectedAmt(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        return goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getImd2()
                .stream()
                .filter(imd -> StringUtils.equalsIgnoreCase(imd.getStatus(), "Cleared"))
                .findFirst()
                .orElse(new InitialMoneyDeposit())
                .getAmount();
    }

    @Override
    public BaseResponse saveVerificationDetails(VerificationRequest verificationRequest, String verificationType, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(verificationRequest.getRefId(),verificationRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(verificationRequest.getHeader(), verificationRequest.getRefId(), httpRequest,
                applicationStage, verificationType,
                applicationStatus, verificationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving Verification Details refId {}", verificationRequest.getRefId());

            Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());

            boolean save = applicationRepository.saveVerificationDetails(verificationRequest, verificationType, verificationStatusDealersMap);
            if (save) {
                if (!verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).isEmpty() ||
                        !verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED).isEmpty()) {
                    String verificationAction = IntimationConstants.VERIFICATION_TRIGGER;
                    if (verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED).isEmpty()) {
                        verificationAction = IntimationConstants.VERIFICATION_DONE;
                    }
                    appConfigurationHelper.intimateUsers(verificationRequest.getRefId(),
                            verificationAction, verificationType, verificationStatusDealersMap);
                }

                // pre-populating banking verification in repayment page in case of pl
                if (verificationRequest.getHeader().getProduct().toProductName() ==
                        Product.PL.toProductName()) {
                    VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);
                    if (verificationDetails != null && verificationDetails.getBankingVerification() != null) {
                        List<BankingVerification> bankingVerification = verificationDetails.getBankingVerification();
                        Repayment repayment = new Repayment();
                        List<RepaymentDetails> repaymentDetailList = new ArrayList<>();
                        RepaymentDetails repaymentDetails = new RepaymentDetails();
                        repaymentDetails.setBankName(bankingVerification.get(0).getBankName());
                        repaymentDetails.setIfscCode(bankingVerification.get(0).getIfscCode());
                        repaymentDetails.setBranchName(bankingVerification.get(0).getBranchName());
                        repaymentDetails.setAccountNumber(bankingVerification.get(0).getAccountNumber());
                        repaymentDetailList.add(repaymentDetails);
                        repayment.setRepaymentDetailsList(repaymentDetailList);
                        repayment.setRefId(verificationRequest.getRefId());
                        repayment.setInstitutionId(verificationRequest.getHeader().getInstitutionId());
                        applicationRepository.saveRepaymentDetails(repayment);
                    }
                }
                // Add this step in completed-set of execution items
                Header header = verificationRequest.getHeader();
                applicationRepository.updateCompletedInfo(verificationRequest.getRefId(), header.getInstitutionId(),
                        verificationType, header.getLoggedInUserId(), header.getLoggedInUserRole());

                VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        logger.debug("finished saving Verification Details refId {}, response is {}",verificationRequest.getRefId(),response);
        return response;
    }

    @Override
    public BaseResponse getVerificationDetails(VerificationRequest verificationRequest)  {
        BaseResponse response = new BaseResponse();
        logger.debug("Indside getVerificationDetails():1 for refId: {}",verificationRequest.getRefId());
        try{
             VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);

        String role = verificationRequest.getHeader().getLoggedInUserRole();
        boolean isVerifier = Cache.VERIFICATION_ROLE_URL_MAP.containsKey(role);

        if (verificationDetails != null) {
            if (StringUtils.isNotEmpty(verificationRequest.getVerificationType())) {
                switch (verificationRequest.getVerificationType()) {
                    case EndPointReferrer.OFFICE_VERIFICATION:
                        response = getBaseResponseForOV(verificationRequest, verificationDetails, isVerifier);
                        break;
                    case EndPointReferrer.RESIDENCE_VERIFICATION:
                        response = getBaseResponseForRV(verificationRequest, verificationDetails, isVerifier);
                        break;
                    case EndPointReferrer.ITR_VERIFICATION:
                        response = getBaseResponseForITR(verificationRequest, verificationDetails, isVerifier);
                        break;
                    case EndPointReferrer.BANK_VERIFICATION:
                        response = getBaseResponseForBanking(verificationRequest, verificationDetails, isVerifier);
                        break;
                    case EndPointReferrer.REFERENCE_DETAILS:
                        response = getBaseResponseForReferenceDetails(verificationRequest, verificationDetails);
                        break;
                }
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails);
            }
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        }
            logger.debug("finished getVerificationDetails(): 2");
        }catch (Exception e){
            logger.error("verification save error {}" ,e.getStackTrace() );
        }
        return response;
    }

    private BaseResponse getBaseResponseForReferenceDetails(VerificationRequest verificationRequest, VerificationDetails verificationDetails) {
        BaseResponse response = null;

        //No need to filter out Reference details as this is done by CPA
        if (verificationDetails.getReferenceDetails() != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails.getReferenceDetails());
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        }
        return response;
    }

    private BaseResponse getBaseResponseForBanking(VerificationRequest verificationRequest, VerificationDetails verificationDetails, boolean isVerifier) {
        BaseResponse response = null;

        String agencyCode = verificationRequest.getHeader().getDealerId();
        //FilterOut Banking verification list which is pending with verifiers agency
        if (isVerifier && verificationDetails.getBankingVerification() != null) {
            List<BankingVerification> bankingVerifications = null;
            if(CollectionUtils.isNotEmpty(verificationRequest.getHeader().getDealerIds()) && Institute.isInstitute(verificationRequest.getHeader().getInstitutionId(),Institute.SBFC)){
                bankingVerifications = verificationDetails.getBankingVerification().stream()
                        .filter(bankDetail -> StringUtils.isNotEmpty(bankDetail.getAgencyCode()) && verificationRequest.getHeader().getDealerIds().contains(bankDetail.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(bankDetail.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(bankDetail.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }else {
                bankingVerifications = verificationDetails.getBankingVerification().stream()
                        .filter(bankDetail -> StringUtils.equalsIgnoreCase(agencyCode, bankDetail.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(bankDetail.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(bankDetail.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }
            verificationDetails.setBankingVerification(bankingVerifications);
        }
        if (CollectionUtils.isEmpty(verificationDetails.getBankingVerification())) {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails.getBankingVerification());
        }

        return response;
    }

    private BaseResponse getBaseResponseForITR(VerificationRequest verificationRequest, VerificationDetails verificationDetails, boolean isVerifier) {
        BaseResponse response = null;

        String agencyCode = verificationRequest.getHeader().getDealerId();
        //FilterOut ITR verification list which is pending with verifiers agency
        if (isVerifier && verificationDetails.getItrVerification() != null) {
            List<ItrVerification> itrVerifications = null;
            if(CollectionUtils.isNotEmpty(verificationRequest.getHeader().getDealerIds()) && Institute.isInstitute(verificationRequest.getHeader().getInstitutionId(),Institute.SBFC)){
                itrVerifications = verificationDetails.getItrVerification().stream()
                        .filter(itrDetail ->  StringUtils.isNotEmpty(itrDetail.getAgencyCode()) && verificationRequest.getHeader().getDealerIds().contains(itrDetail.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(itrDetail.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(itrDetail.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }else {
                itrVerifications = verificationDetails.getItrVerification().stream()
                        .filter(itrDetail -> StringUtils.equalsIgnoreCase(agencyCode, itrDetail.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(itrDetail.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(itrDetail.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }
            verificationDetails.setItrVerification(itrVerifications);
        }
        if (CollectionUtils.isEmpty(verificationDetails.getItrVerification())) {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails.getItrVerification());
        }

        return response;
    }


    private BaseResponse getBaseResponseForRV(VerificationRequest verificationRequest, VerificationDetails verificationDetails, boolean isVerifier) {
        logger.debug("Inside getBaseResponseForRV():1 for refId: {}",verificationRequest.getRefId());
        BaseResponse response = null;

        String agencyCode = verificationRequest.getHeader().getDealerId();
        //FilterOut Residence verification list which is pending with verifiers agency
        if (isVerifier && verificationDetails.getResidenceVerification() != null) {
            List<PropertyVerification> propertyVerifications = null;
            if(CollectionUtils.isNotEmpty(verificationRequest.getHeader().getDealerIds()) && Institute.isInstitute(verificationRequest.getHeader().getInstitutionId(),Institute.SBFC)){
                propertyVerifications = verificationDetails.getResidenceVerification().stream()
                        .filter(property -> StringUtils.isNotEmpty(property.getAgencyCode()) && verificationRequest.getHeader().getDealerIds().contains(property.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }else{
                propertyVerifications = verificationDetails.getResidenceVerification().stream()
                        .filter(property -> StringUtils.equalsIgnoreCase(agencyCode, property.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }
            verificationDetails.setResidenceVerification(propertyVerifications);
        }
        if (CollectionUtils.isEmpty(verificationDetails.getResidenceVerification())) {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails.getResidenceVerification());
        }
        logger.debug("finished getBaseResponseForRV():2");
        return response;
    }

    private BaseResponse getBaseResponseForOV(VerificationRequest verificationRequest, VerificationDetails verificationDetails, boolean isVerifier) {
        logger.debug("Inside getBaseResponseForOV():1 for refId: {}",verificationRequest.getRefId());
        BaseResponse response = null;
        String refId = verificationRequest.getRefId();
        String agencyCode = verificationRequest.getHeader().getDealerId();
        //FilterOut Office verification list which is pending with verifiers agency
        if (isVerifier && verificationDetails.getOfficeVerification() != null) {
            List<PropertyVerification> propertyVerifications = null;
            if(CollectionUtils.isNotEmpty(verificationRequest.getHeader().getDealerIds()) && Institute.isInstitute(verificationRequest.getHeader().getInstitutionId(),Institute.SBFC)){
                propertyVerifications = verificationDetails.getOfficeVerification().stream()
                        .filter(property -> StringUtils.isNotEmpty(property.getAgencyCode()) && verificationRequest.getHeader().getDealerIds().contains(property.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }else{
                propertyVerifications = verificationDetails.getOfficeVerification().stream()
                        .filter(property -> StringUtils.equalsIgnoreCase(agencyCode, property.getAgencyCode())
                                && (StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                                || StringUtils.equalsIgnoreCase(property.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                        .collect(Collectors.toList());
            }
            verificationDetails.setOfficeVerification(propertyVerifications);
        }
        if (CollectionUtils.isEmpty(verificationDetails.getOfficeVerification())) {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(verificationRequest.getVerificationType()));
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails.getOfficeVerification());
        }
        logger.debug("finished getBaseResponseForOV():2");
        return response;
    }

    @Override
    public BaseResponse saveCamDetails(CamDetailsRequest camDetailsRequest, String camType, boolean isGetCamApi, HttpServletRequest httpRequest)  {
        BaseResponse response = null;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(camDetailsRequest.getRefId(),camDetailsRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(camDetailsRequest.getHeader(), camDetailsRequest.getRefId(), httpRequest,
                applicationStage, camType,
                applicationStatus, camDetailsRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving CamDetails Details refId {}", camDetailsRequest.getRefId());
            if (camDetailsRequest.getHeader().getProduct() == Product.PL
                    && StringUtils.equalsIgnoreCase(camType, EndPointReferrer.CAM_RTR_DETAILS)) {

                applicationRepository.saveEligibilityDetails(camDetailsRequest);
            }
            if(Institute.isInstitute(camDetailsRequest.getHeader().getInstitutionId(),Institute.AMBIT)) {

                setConsolidatedBankStatements(camDetailsRequest);
            }
            boolean save = applicationRepository.saveCamDetails(camDetailsRequest, camType);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = camDetailsRequest.getHeader();
                if (!isGetCamApi) {
                    applicationRepository.updateCompletedInfo(camDetailsRequest.getRefId(), header.getInstitutionId(),
                            camType, header.getLoggedInUserId(), header.getLoggedInUserRole());
                }
                CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        //for cro-onhold end-point (action)
        if(!httpRequest.getRequestURI().contains("cro-onhold")){
            activityEventPublisher.publishEvent(activityLogs);
        }
        return response;
    }

    public BaseResponse getCamDetails(CamDetailsRequest camDetailsRequest) throws Exception {
        BaseResponse response = null;
        try{
        CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
        if (camDetails != null) {
            if (StringUtils.isNotEmpty(camDetailsRequest.getCamType())) {
                if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_RTR_DETAILS)) {
                    if (camDetails.getRtrDetailList() != null) {
                        response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getRtrDetailList());
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                } else if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_PL_BS_DETAILS)) {
                    if (camDetails.getPlAndBSAnalysis() != null) {
                        List<PLAndBSAnalysis> plAndBSAnalysisList = camDetails.getPlAndBSAnalysis();
                        //for old data migration and setting Values in YearList
                        migrateOldDataProfitAndLoss(plAndBSAnalysisList);
                        migrateOldDataBalenceSheet(plAndBSAnalysisList);
                        migrateOldDataRatio(plAndBSAnalysisList);

                        response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getPlAndBSAnalysis());
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                } else if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_BANKING_DETAILS)) {
                    if (camDetails.getBankingStatements() != null) {
                        if(camDetails.isSbfc()){
                            response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails);
                        }else{
                            response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getBankingStatements());
                        }
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                } else if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_SUMMARY)) {
                    if (camDetails.getSummary() != null) {
                        response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getSummary());
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                }
                else if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_GST_DETAILS)){
                    if (CollectionUtils.isNotEmpty(camDetails.getGstDetails())) {
                        response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getGstDetails());
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                } else if (StringUtils.equals(camDetailsRequest.getCamType(), EndPointReferrer.ALL_CAM_DETAILS)) {
                    if(camDetails != null) {
                        response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails);
                    } else {
                        response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
                    }
                }
            } else {
                response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails);
            }
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList(camDetailsRequest.getCamType()));
        }
        }catch (Exception e){
        logger.error("while saving cam details  {}" , e.getStackTrace());
        }
        return response;
    }

    private void setAge(ApplicationRequest applicationRequest) {

        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();

        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }
    }

    private GoNoGoCustomerApplication fetchgoNoGoCustomerApplication(ApplicationRequest applicationRequest) throws Exception {
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        // Set logged in userid and his role
        goNoGoCustomerApplication.getApplicationRequest().getHeader().setLoggedInUserId(applicationRequest.getHeader().getLoggedInUserId());
        goNoGoCustomerApplication.getApplicationRequest().getHeader().setLoggedInUserRole(applicationRequest.getHeader().getLoggedInUserRole());

        return goNoGoCustomerApplication;
    }

    private void setEditable(ApplicationRequest inputRequest, GoNoGoCustomerApplication persistedApplication) {
       /* If logged in user is of role FOS and he has submitted the application then send
            the application in uneditable mode.
        */
        if (Roles.Role.valueOf(inputRequest.getHeader().getLoggedInUserRole()) == Roles.Role.FOS) {
            persistedApplication.setEditable(false);
        }
    }

    private Collection<Error> validateVersion(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) {
        boolean validVersion = true;
        Collection<Error> errors = null;
        if (!configurationManager.isValidVersion(applicationRequest
                .getHeader().getInstitutionId(), applicationRequest
                .getHeader().getApplicationSource())) {
            validVersion = false;
            ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());

            activityLog.setCustomMsg(
                    new StringBuilder().append(ErrorCode.INVALID_VERSION_DSCR).append(" | ")
                            .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                            .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                            .append(Error.SEVERITY.CRITICAL.name()).toString());

            errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            // Persist activityLog in DB
            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLog);

        }
        return errors;
    }

    @Override
    public Object generateAndSetRefId(ApplicationRequest applicationRequest) {
        Object object = requestAggregator.generateReferenceId(applicationRequest);
        if (object instanceof Acknowledgement) {
            return object;
        } else {
            String refID = (String) object;
            applicationRequest.setRefID(refID);
            return applicationRequest;
        }
    }

    private GoNoGoCustomerApplication process(ApplicationRequest applicationRequest, String stepId, String userId, String role) throws Exception {
        String refId = applicationRequest.getRefID();
        logger.debug("{} : validating refId {}", stepId, refId);
        // Validations
        applicationValidationEngine.validate(applicationRequest, stepId);
        // FIX : We need to set certain values which are supposed to be set in the incoming request.
        moduleHelper.fixValues(applicationRequest);

        logger.debug("{} : updating GoNoGoCustomerApplication refId {}: \n {}", stepId, refId, applicationRequest);


        // Update applicationRequest related data before updating GoNoGoCustomerApplication
        moduleManager.preSave(applicationRequest, stepId);
        logger.info("step 2 : logs for kyc check, step {}  refId {} oReq {}", stepId, refId, applicationRequest.getRequest());
        // Save request
        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

        // Add this step in completed-set of execution items
        applicationRepository.updateCompletedInfo(refId, applicationRequest.getHeader().getInstitutionId(), stepId, userId, role);

        logger.debug("{} : fetching GoNoGoCustomerApplication refId {}", stepId, refId);
        // Get CustomerApplication from DB
        GoNoGoCustomerApplication goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
        logger.debug("{} : fetched GoNoGoCustomerApplication refId {} :\n {}", stepId, refId,
                goNoGoCustomerApplication.getApplicationRequest());

        moduleManager.afterSave(goNoGoCustomerApplication, stepId);

        logger.debug("{} : invoking BRE components for refId {} ", stepId, refId);
        // After-save module check
        moduleManager.executeBre(goNoGoCustomerApplication, stepId, role, applicationRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);

        return goNoGoCustomerApplication;
    }

    @Override
    public BaseResponse saveListOfDocs(ListOfDocsRequest listOfDocsRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(listOfDocsRequest.getHeader(), listOfDocsRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_LIST_OF_DOCUMENTS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), listOfDocsRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving ListOfDocs Details refId {}", listOfDocsRequest.getRefId());
            if (listOfDocsRequest.getListOfDocs() != null && listOfDocsRequest.getListOfDocs().getDisbursedDate() == null) {
                LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(listOfDocsRequest.getRefId(), listOfDocsRequest.getHeader().getInstitutionId());
                ListOfDocs listOfDocs = listOfDocsRequest.getListOfDocs();
                if (loanCharges != null && loanCharges.getDisbursedDate() != null) {
                    listOfDocs.setDisbursedDate(loanCharges.getDisbursedDate());
                } else {
                    listOfDocs.setDisbursedDate(new Date());
                }
                listOfDocsRequest.setListOfDocs(listOfDocs);
            }
            boolean save = applicationRepository.saveListOfDocs(listOfDocsRequest);
            if (save) {
                applicationRepository.updateCompletedInfo(listOfDocsRequest.getRefId(), listOfDocsRequest.getHeader().getInstitutionId(),
                        EndPointReferrer.SAVE_LIST_OF_DOCUMENTS, listOfDocsRequest.getHeader().getLoggedInUserId(),
                        listOfDocsRequest.getHeader().getLoggedInUserRole());
                ListOfDocs listOfDocs = applicationRepository.fetchListOfDocs(listOfDocsRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, listOfDocs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getListOfDocs(ListOfDocsRequest listOfDocsRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(listOfDocsRequest.getRefId(),listOfDocsRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus()!=null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(listOfDocsRequest.getHeader(), listOfDocsRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.GET_LIST_OF_DOCUMENTS,
                applicationStatus, listOfDocsRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ListOfDocs listOfDocs = applicationRepository.fetchListOfDocs(listOfDocsRequest);

        if (listOfDocs != null) {
            LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(listOfDocsRequest.getRefId(), listOfDocsRequest.getHeader().getInstitutionId());
            if (loanCharges != null && loanCharges.getDisbursedDate() != null) {
                listOfDocs.setDisbursedDate(loanCharges.getDisbursedDate());
            } else {
                listOfDocs.setDisbursedDate(new Date());
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, listOfDocs);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return response;
    }

    @Override
    public BaseResponse saveCustomerCreditDocs(CustomerCreditRequest customerCreditRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;

        ActivityLogs activityLogs = auditHelper.createActivityLog(customerCreditRequest.getHeader(), customerCreditRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_CUSTOMER_CREDIT_DOCS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), customerCreditRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving CustomerCredit Details for refId {}", customerCreditRequest.getRefId());
            boolean save = applicationRepository.saveCustomerCreditDocs(customerCreditRequest);
            if (save) {
                applicationRepository.updateCompletedInfo(customerCreditRequest.getRefId(), customerCreditRequest.getHeader().getInstitutionId(),
                        EndPointReferrer.SAVE_CUSTOMER_CREDIT_DOCS, customerCreditRequest.getHeader().getLoggedInUserId(), customerCreditRequest.getHeader().getLoggedInUserRole());
                CustomerCreditDocs customerCreditDocs = applicationRepository.fetchCustomerCreditDocs(customerCreditRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, customerCreditDocs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getCustomerCreditDocs(CustomerCreditRequest customerCreditRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(customerCreditRequest.getRefId(),customerCreditRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus()!=null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(customerCreditRequest.getHeader(), customerCreditRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.GET_CUSTOMER_CREDIT_DOCS,
                applicationStatus, customerCreditRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        CustomerCreditDocs customerCreditDocs = applicationRepository.fetchCustomerCreditDocs(customerCreditRequest);
        if (customerCreditDocs != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, customerCreditDocs);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse triggerVerification(VerificationRequest verificationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        // TODO Stage,Status
        ActivityLogs activityLogs = auditHelper.createActivityLog(verificationRequest.getHeader(), verificationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.TRIGGER_VERIFICATION,
                null, verificationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving TriggerVerification Details refId {}", verificationRequest.getVerificationDetails().getRefId());
            boolean save = false;
            if (verificationRequest.getVerificationType().equals(EndPointReferrer.LEGAL_VERIFICATION)) {
                save = applicationRepository.triggerVerificationForLegal(verificationRequest);
                if (save) {
                    LegalVerificationRequest legalVerificationRequest = new LegalVerificationRequest();
                    legalVerificationRequest.setHeader(verificationRequest.getHeader());
                    legalVerificationRequest.setRefId(verificationRequest.getRefId());
                    LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK, legalVerification);
                }
            } else if (verificationRequest.getVerificationType().equals(EndPointReferrer.VALUATION_VERIFICATION)) {
                save = applicationRepository.triggerVerificationForValuation(verificationRequest);
                if (save) {
                    ValuationRequest valuationRequest = new ValuationRequest();
                    valuationRequest.setHeader(verificationRequest.getHeader());
                    valuationRequest.setRefId(verificationRequest.getRefId());
                    Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK, valuation);
                }
            } else {
                save = applicationRepository.triggerVerification(verificationRequest);
                if (save) {
                    VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);
                    response = GngUtils.getBaseResponse(HttpStatus.OK, verificationDetails);
                }
            }
            if (response == null) {
                response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse saveValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(valuationRequest.getHeader(), valuationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_VALUATION_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), valuationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving Valuation Details refId {}", valuationRequest.getRefId());
            Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
            Map<String, Valuation> triggerToAgencies = new HashMap<>();
            boolean save = applicationRepository.saveValuationDetails(valuationRequest, verificationStatusDealersMap, triggerToAgencies);
            if (save) {
                appConfigurationHelper.intimateUsers(valuationRequest.getRefId(),
                        IntimationConstants.PROPERTY_VALUATION, IntimationConstants.VER_TYPE_VALUATION, verificationStatusDealersMap);
                // Send message to valuation agency
                logger.debug("{} triggering to agencies {}", valuationRequest.getRefId(), triggerToAgencies);
                moduleHelper.triggerToAgencies(valuationRequest.getRefId(), valuationRequest.getHeader().getInstitutionId(), triggerToAgencies);
                // Add this step in completed-set of execution items
                Header header = valuationRequest.getHeader();
                applicationRepository.updateCompletedInfo(valuationRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_VALUATION_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, valuation);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse deleteValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(valuationRequest.getHeader(), valuationRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.DELETE_VALUATION_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), valuationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving Valuation Details refId {}", valuationRequest.getRefId());
            boolean save = applicationRepository.deleteValuationDetails(valuationRequest);
            if (save) {
                Header header = valuationRequest.getHeader();
                applicationRepository.updateCompletedInfo(valuationRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.DELETE_VALUATION_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, valuation);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        ActivityLogs activityLogs = auditHelper.createActivityLog(valuationRequest.getHeader(), valuationRequest.getRefId(), httpRequest,
                null, EndPointReferrer.GET_VALUATION_DETAILS,
                null, valuationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        boolean isVerifier = Cache.VERIFICATION_ROLE_URL_MAP.containsKey(valuationRequest.getHeader().getLoggedInUserRole());

        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        if (valuation != null) {
            response = getBaseResponseForValuation(valuationRequest, valuation, isVerifier);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    private BaseResponse getBaseResponseForValuation(ValuationRequest valuationRequest, Valuation valuation, boolean isVerifier) {
        BaseResponse response = null;

        String agencyCode = valuationRequest.getHeader().getDealerId();
        //FilterOut Banking verification list which is pending with verifiers agency
        if (isVerifier && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
            List<ValuationDetails> valuationDetailsList = valuation.getValuationDetailsList().stream()
                    .filter(valuationDetails -> StringUtils.equalsIgnoreCase(agencyCode, valuationDetails.getAgencyCode())
                            && (StringUtils.equalsIgnoreCase(valuationDetails.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name())
                            || StringUtils.equalsIgnoreCase(valuationDetails.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())))
                    .collect(Collectors.toList());
            valuation.setValuationDetailsList(valuationDetailsList);
        }
        if (CollectionUtils.isEmpty(valuation.getValuationDetailsList())) {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.OK, valuation.getValuationDetailsList());
        }

        return response;
    }

    @Override
    public BaseResponse saveEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;
        String stepId = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(eligibilityRequest.getHeader(), eligibilityRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_ELIGIBILITY_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), eligibilityRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        EligibilityDetails eligibility = eligibilityRequest.getEligibilityDetails();
        eligibility.setInstitutionId(eligibilityRequest.getHeader().getInstitutionId());
        eligibility.setRefId(eligibilityRequest.getRefId());
        boolean flag = false;
        try {
            boolean save = applicationRepository.saveEligibilityDetails(eligibility);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = eligibilityRequest.getHeader();
                applicationRepository.updateCompletedInfo(eligibilityRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_ELIGIBILITY_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());
                if (Institute.isInstitute(eligibilityRequest.getHeader().getInstitutionId(), Institute.SBFC)) {
                    flag = moduleHelper.calculatePremium(eligibilityRequest.getRefId(), eligibilityRequest.getHeader(), eligibility.getAprvLoan(),
                            eligibility.getTenor(),EndPointReferrer.SAVE_ELIGIBILITY_DETAILS);
                    if(flag){
                        eligibility.setInsuranceMessage("Re-computing Calculate premium amount");
                    }
                }
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, eligibility);
                if(eligibilityRequest.getEligibilityDetails().isTriggerSobreDeviation() || Institute.isInstitute(eligibilityRequest.getHeader().getInstitutionId(),Institute.FVSG)) {
                    moduleManager.executeBre(applicationRepository.getGoNoGoCustomerApplicationByRefId(eligibilityRequest.getRefId()), EndPointReferrer.SAVE_ELIGIBILITY_DETAILS, header.getLoggedInUserRole(), header, ConfigurationConstants.WF_NODE_TYPE_SCREEN);
                    GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.fetchScoringData(eligibilityRequest.getRefId());
                    EligibilityDetails eligibilityData = applicationRepository.fetchEligibilityDetails(eligibilityRequest);
                    responseToEligibility(goNoGoCustomerApplication,eligibilityData,eligibilityRequest.getHeader().getLoggedInUserRole());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, eligibilityData);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);

        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    @Override
    public BaseResponse getEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;
        EligibilityDetails eligibilityDetails = applicationRepository.fetchEligibilityDetails(eligibilityRequest);
        if (eligibilityDetails != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, eligibilityDetails);
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public boolean updateStageId(ApplicationRequest applicationRequest, String applicationStatus) throws Exception {
        return applicationRepository.updateStageId(applicationRequest, applicationStatus);
    }

    /**
     * Manually lock/unlock the application (i.e make editable/uneditable to other users of application)
     *
     * @param applicationLockRequest
     * @param httpRequest
     * @return
     */
    @Override
    public BaseResponse changeApplicationLockStatus(ApplicationLockRequest applicationLockRequest, HttpServletRequest httpRequest) {
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationLockRequest.getHeader(), applicationLockRequest.getRefId(), httpRequest,
                null, EndPointReferrer.CHANGE_APP_LOCK_STATUS,
                null, applicationLockRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String userId = applicationLockRequest.getHeader().getLoggedInUserId();
        String institutionId = applicationLockRequest.getHeader().getInstitutionId();
        String refId = applicationLockRequest.getRefId();
        boolean isLocked = applicationLockRequest.isLocked();

        logger.debug("{} : Changing application manual lock status to {} for refId {}", applicationLockRequest.isLocked(), applicationLockRequest.getRefId());
        applicationRepository.changeApplicationLockStatus(userId, institutionId, refId, isLocked);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return GngUtils.getBaseResponse(HttpStatus.OK, Constant.SUCCESS);
    }

    @Override
    public BaseResponse saveDeviation(DeviationRequest deviationRequest) {
        BaseResponse baseResponse = null;
        try {
            boolean save = applicationRepository.saveDeviation(deviationRequest.getRefId(),
                    deviationRequest.getHeader().getInstitutionId(), deviationRequest.getDeviationDetails());
            if (save) {
                applicationRepository.updateCompletedInfo(deviationRequest.getRefId(), deviationRequest.getHeader().getInstitutionId(),
                        EndPointReferrer.SAVE_DEVIATION, deviationRequest.getHeader().getLoggedInUserId(),
                        deviationRequest.getHeader().getLoggedInUserRole());
                return GngUtils.getBaseResponse(HttpStatus.OK, deviationRequest);
            }
        } catch (Exception e) {
            logger.info("Exception occured while saving deviation");
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse approveAllDeviation(DeviationRequest deviationRequest) {
        BaseResponse baseResponse = null;
        boolean save = false;
        String refId = deviationRequest.getRefId();
        String instId = deviationRequest.getHeader().getInstitutionId();
        String role = deviationRequest.getHeader().getLoggedInUserRole();
        String mitigationRemark = deviationRequest.getMitigationRemark();
        String resolvedBy = deviationRequest.getResolvedBy();

        DeviationDetails deviationDetails = applicationRepository.fetchDeviationDetails(refId, instId);
        if (deviationDetails != null && CollectionUtils.isNotEmpty(deviationDetails.getDeviationList())) {
            for (Deviation deviation : deviationDetails.getDeviationList()) {
                if (CollectionUtils.isEmpty(deviation.getAuthorities())) {
                    DeviationMaster devMaster = masterDataViewRepository.fetchDeviationMastersRecord(instId, deviationRequest.getHeader().getProduct(),
                            deviation.getDeviation(), deviation.getCategory());
                    deviation.setAuthorities(devMaster.getAuthorities());
                }
                if (StringUtils.equalsIgnoreCase("Pending", deviation.getStatus()) && (StringUtils.equalsIgnoreCase(
                        deviation.getAuthority(), role) || (CollectionUtils.isNotEmpty(deviation.getAuthorities()) &&
                        deviation.getAuthorities().contains(role)))) {
                    deviation.setMitigationRemarks(mitigationRemark);
                    deviation.setStatus("Resolved");
                    deviation.setResolvedBy(resolvedBy);
                    deviation.setResolvedDate(new Date());
                }
            }
            deviationRequest.setDeviationDetails(deviationDetails);
            save = applicationRepository.saveDeviation(deviationRequest.getRefId(),
                    deviationRequest.getHeader().getInstitutionId(), deviationDetails);
        }
        if (save) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, deviationDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.CONFLICT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getDeviation(DeviationRequest deviationRequest) {
        String refId = deviationRequest.getRefId();
        String instId = deviationRequest.getHeader().getInstitutionId();
        Product product = deviationRequest.getHeader().getProduct();
        BaseResponse baseResponse = null;
        DeviationDetails deviationDetails = applicationRepository.fetchDeviationDetails(refId, instId);
        if (deviationDetails != null && CollectionUtils.isNotEmpty(deviationDetails.getDeviationList())) {
            boolean updatedDeviationDtls = false;
            for (Deviation deviation : deviationDetails.getDeviationList()) {
                if (CollectionUtils.isEmpty(deviation.getAuthorities())) {
                    DeviationMaster deviationMaster = masterDataViewRepository.fetchDeviationMastersRecord(instId, product,
                            deviation.getDeviation(), deviation.getCategory());
                    deviation.setAuthorities(deviationMaster.getAuthorities());
                    updatedDeviationDtls = true;
                }
            }
            if (updatedDeviationDtls) {
                applicationRepository.updateDeviation(refId,
                        instId, deviationDetails);
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, deviationDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCamDetailsByCamType(CamDetailsRequest camDetailsRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        if (StringUtils.equals(camDetailsRequest.getCamType(), EndPointReferrer.CAM_SUMMARY)
                || StringUtils.equals(camDetailsRequest.getCamType(), EndPointReferrer.ALL_CAM_DETAILS)) {
            GoNoGoCustomerApplication gngAppObj = applicationRepository.getGoNoGoCustomerApplicationByRefId
                    (camDetailsRequest.getRefId());
            if (gngAppObj != null && gngAppObj.getApplicationRequest() != null) {
                CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
                if (camDetails == null) {
                    camDetails = new CamDetails();
                }
                getCamSummary(camDetails, camDetailsRequest, httpRequest, gngAppObj);
                if(Institute.isInstitute(camDetailsRequest.getHeader().getInstitutionId(), Institute.AMBIT)) {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails);
                } else {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, camDetails.getSummary());
                }
            }
        } else {
            response = getCamDetails(camDetailsRequest);
        }
        return response;
    }

    public String getCamDetailsByCamTypeTemp(CamDetailsRequest camDetailsRequest, HttpServletRequest httpRequest) throws Exception {
        String response;
        if (camDetailsRequest.getCamType().equals(EndPointReferrer.CAM_SUMMARY) ||
                camDetailsRequest.getCamType().equals(EndPointReferrer.ALL_CAM_DETAILS)) {
            GoNoGoCustomerApplication gngAppObj = applicationRepository.getGoNoGoCustomerApplicationByRefId
                    (camDetailsRequest.getRefId());
            if (gngAppObj != null && gngAppObj.getApplicationRequest() != null) {
                CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
                if (camDetails == null) {
                    camDetails = new CamDetails();
                }
                getCamSummary(camDetails, camDetailsRequest, httpRequest, gngAppObj);
                response = "camDetails successfully updated for RefId - "+camDetails.getRefId();
            } else {
                response = "not updated, gng collection is empty for RefId"+camDetailsRequest.getRefId();
            }
        } else {
            response = "not updated, sCamType is wrong in the request for RefId"+camDetailsRequest.getRefId();
        }
        return response;
    }

    @Override
    public BaseResponse updateCamDetails(UpdateCamRequest updateCamRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        List<String> responseList = new ArrayList<>();
        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = applicationRepository.findGNGRefIdListByLastUpdatedDate(
                updateCamRequest.header.getInstitutionId(), updateCamRequest.startDate, updateCamRequest.endDate);
        if (CollectionUtils.isNotEmpty(goNoGoCustomerApplications)) {
            goNoGoCustomerApplications.forEach(gngApplicantion -> {
                CamDetailsRequest camDetailsRequest = CamDetailsRequest.builder()
                        .header(updateCamRequest.getHeader())
                        .refId(gngApplicantion.getGngRefId())
                        .camType(updateCamRequest.camType)
                        .build();
                try {
                    responseList.add(getCamDetailsByCamTypeTemp(camDetailsRequest, httpRequest));
                } catch (Exception e) {
                    responseList.add("failed to call get-cam-details for ref - "+gngApplicantion.getGngRefId());
                    logger.error("Error occurred while calling getCamDetailsByCamType for refId {} , Exception {} ", gngApplicantion.getGngRefId(),  ExceptionUtils.getStackTrace(e));
                }
            });
        } else {
            responseList.add("No RefIds found in between provided dates");
        }
        response = GngUtils.getBaseResponse(HttpStatus.OK, responseList);
        return response;
    }

    private void getCamSummary(CamDetails camDetails, CamDetailsRequest camDetailsRequest, HttpServletRequest httpRequest, GoNoGoCustomerApplication gngAppObj) throws Exception {
        CamSummary camSummary = populateCamSummary(camDetails, camDetailsRequest, gngAppObj);
        camDetails.setSummary(camSummary);
        camDetailsRequest.setCamDetails(camDetails);
        saveCamDetails(camDetailsRequest, camDetailsRequest.getCamType(), true, httpRequest);
    }

    private CamSummary populateCamSummary(CamDetails camDetails, CamDetailsRequest camDetailsRequest, GoNoGoCustomerApplication gngAppObj) throws Exception {
        CamSummary camSummary = new CamSummary();
        if (camDetails != null && camDetails.getSummary() != null) {
            camSummary.setRoiType(camDetails.getSummary().getRoiType());
            camSummary.setInstallmentType(camDetails.getSummary().getInstallmentType());
            camSummary.setPropertyvalueConsidr(camDetails.getSummary().getPropertyvalueConsidr());
            camSummary.setPfPercentage(camDetails.getSummary().getPfPercentage());
            camSummary.setProcessingFees(camDetails.getSummary().getProcessingFees());
            camSummary.setInsuranceOfferedAsLoan(camDetails.getSummary().isInsuranceOfferedAsLoan());
            camSummary.setPslSubClassification(camDetails.getSummary().getPslSubClassification());
            camSummary.setInsuranceOfferedAmt(camDetails.getSummary().getInsuranceOfferedAmt());
            camSummary.setRemarkList(camDetails.getSummary().getRemarkList());
            camSummary.setCollateralList(camDetails.getSummary().getCollateralList());
            camSummary.setAddress(gngAppObj.getApplicationRequest().getRequest().getApplicant().getAddress());
            camSummary.setSpouseWorking(camDetails.getSummary().getSpouseWorking());
            camSummary.setApprovalDate(camDetails.getSummary().getApprovalDate());
            camSummary.setSubProgram(camDetails.getSummary().getSubProgram());
            camSummary.setRoi(camDetails.getSummary().getRoi());
            camSummary.setFoir(camDetails.getSummary().getFoir());
            camSummary.setFinalFoir(camDetails.getSummary().getFinalFoir());
            camSummary.setAdvEmi(camDetails.getSummary().getAdvEmi());
            camSummary.setRiskProfile(camDetails.getSummary().getRiskProfile());
            camSummary.setDisbAmt(camDetails.getSummary().getDisbAmt());
            camSummary.setCustomerCategory(camDetails.getSummary().getCustomerCategory());
        }
        camSummary.setLan(camDetailsRequest.getRefId());
        setApplicationDetails(camSummary, gngAppObj);
        setEligibilityDetails(camDetailsRequest, camSummary, gngAppObj);
        setValuationDetails(camDetailsRequest, camSummary);
        setDeviationDetails(camDetailsRequest, camSummary);
        setSanctionConditionDetails(camDetailsRequest, camSummary);
        if (Institute.isInstitute(camDetailsRequest.getHeader().getInstitutionId(), Institute.AMBIT)) {
            if(gngAppObj.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails() != null) {
                camSummary.setIndustry(gngAppObj.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails().getIndustry());
            }
            if(ApplicantType.isIndividual(gngAppObj.getApplicationRequest().getRequest().getApplicant().getApplicantType())) {
                camSummary.setCompanyName(Constant.BLANK);
            }
            if(StringUtils.equals(gngAppObj.getApplicationRequest().getApplicantType(), ApplicantType.NORMAL.value())) {
                camSummary.setCustomerType("NEW");
            }
            if(StringUtils.equalsIgnoreCase(camDetailsRequest.getHeader().getProduct().toProductName(), Product.BL.toProductName())) {
                camSummary.setLan(gngAppObj.getApplicationRequest().getMiFinProspectCode());
            }
        }
        return camSummary;
    }

    private void setDeviationDetails(CamDetailsRequest camDetailsRequest, CamSummary camSummary) {
        String refId = camDetailsRequest.getRefId();
        String instId = camDetailsRequest.getHeader().getInstitutionId();
        Product product = camDetailsRequest.getHeader().getProduct();

        DeviationDetails deviationDetails = applicationRepository.fetchDeviationDetails(refId, instId);
        if (deviationDetails != null && CollectionUtils.isNotEmpty(deviationDetails.getDeviationList())) {
            boolean updatedDeviationDtls = false;
            for (Deviation deviation : deviationDetails.getDeviationList()) {
                if (CollectionUtils.isEmpty(deviation.getAuthorities())) {
                    DeviationMaster deviationMaster = masterDataViewRepository.fetchDeviationMastersRecord(instId, product,
                            deviation.getDeviation(), deviation.getCategory());
                    if(deviationMaster != null) {
                        deviation.setAuthorities(deviationMaster.getAuthorities());
                        updatedDeviationDtls = true;
                    }
                }
            }
            if (updatedDeviationDtls) {
                applicationRepository.updateDeviation(refId,
                        instId, deviationDetails);
            }
            camSummary.setDeviationDetails(deviationDetails);
        }
    }

    private void setSanctionConditionDetails(CamDetailsRequest camDetailsRequest, CamSummary camSummary) {
        String refId = camDetailsRequest.getRefId();
        String instId = camDetailsRequest.getHeader().getInstitutionId();

        SanctionConditions sanctionConditions = applicationRepository.fetchSanctionConditions(refId, instId);

        if (sanctionConditions != null) {
            camSummary.setSanctionConditions(sanctionConditions);
        }
    }

    private void setValuationDetails(CamDetailsRequest camDetailsRequest, CamSummary camSummary) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setHeader(camDetailsRequest.getHeader());
        valuationRequest.setRefId(camDetailsRequest.getRefId());
        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        List<Collateral> collateralList = camSummary.getCollateralList();
        if (valuation != null) {
            List<ValuationDetails> valuationDetailsList = valuation.getValuationDetailsList();
            if (CollectionUtils.isNotEmpty(valuationDetailsList) && CollectionUtils.isNotEmpty(collateralList)) {
                for (Collateral collateral : collateralList) {
                    List<ValuationDetails> tempList = valuationDetailsList.stream().filter(valuationDetails ->
                            StringUtils.equals(collateral.getCollateralId(), valuationDetails.getCollateralId())
                    ).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(tempList)) {
                        ValuationDetails valuationDetails1 = tempList.get(0);
                        setValuationDataInCollateral(valuationDetails1, collateral, 1);
                        if (tempList.size() > 1) {
                            ValuationDetails valuationDetails2 = tempList.get(1);
                            setValuationDataInCollateral(valuationDetails2, collateral, 2);
                        }
                    }
                }
            }
        }
    }

    private void setValuationDataInCollateral(ValuationDetails valuationDetails, Collateral collateral, int valIndex) {
        if (valuationDetails != null) {
            if (valuationDetails.getValuationOutput() != null
                    && ThirdPartyVerification.Status.get(valuationDetails.getStatus()) == ThirdPartyVerification.Status.VERIFIED) {

                if(valuationDetails.getValuationOutput().getValueToConsider() > 0)
                    collateral.setValueToConsider(valuationDetails.getValuationOutput().getValueToConsider());

                if (valIndex == 1) {
                    if (valuationDetails.getValuationOutput().getCurrentMarketValue() != null)
                        collateral.setPropertyValue1(Double.parseDouble(valuationDetails.getValuationOutput().getCurrentMarketValue()));
                }
                if (valIndex == 2) {
                    if (valuationDetails.getValuationOutput().getCurrentMarketValue() != null)
                        collateral.setPropertyValue2(Double.parseDouble(valuationDetails.getValuationOutput().getCurrentMarketValue()));
                }
                if (collateral.getPropertyArea() == null) {
                    collateral.setPropertyArea(valuationDetails.getValuationOutput().getAdoptedArea());
                    collateral.setPropertyAreaUnit(valuationDetails.getValuationOutput().getAdoptedAreaUnit());
                } else {
                    if (StringUtils.isNotEmpty(collateral.getPropertyArea()) && Integer.parseInt(collateral.getPropertyArea()) > Integer.parseInt(valuationDetails.getValuationOutput().getAdoptedArea())) {
                        collateral.setPropertyArea(valuationDetails.getValuationOutput().getAdoptedArea());
                        collateral.setPropertyAreaUnit(valuationDetails.getValuationOutput().getAdoptedAreaUnit());
                    }
                }
            }
        }
    }

    private void setEligibilityDetails(CamDetailsRequest camDetailsRequest, CamSummary camSummary, GoNoGoCustomerApplication gngAppObj) {
        EligibilityRequest eligibilityRequest = new EligibilityRequest();
        eligibilityRequest.setRefId(camDetailsRequest.getRefId());
        eligibilityRequest.setHeader(camDetailsRequest.getHeader());

        EligibilityDetails eligibilityDetails;
        if(StringUtils.equalsIgnoreCase(camDetailsRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName()))
            eligibilityDetails = gngAppObj.getApplicationRequest().getRequest().getApplicant().getDigiPLEligibility();
        else
            eligibilityDetails = applicationRepository.fetchEligibilityDetails(eligibilityRequest);

        if (eligibilityDetails != null) {
            if (eligibilityDetails.getFoirDetail() != null) {
                if (eligibilityDetails.getFoirDetail().getEligibleAmount() != null)
                    camSummary.setLtvForLoanApproved(Double.parseDouble(eligibilityDetails.getFoirDetail().getEligibleAmount()));
                camSummary.setMaxLTV(eligibilityDetails.getFoirDetail().getLtv());
                camSummary.setCombinedLtv(eligibilityDetails.getFoirDetail().getCombinedLtv());
            }
            camSummary.setFinalLoanApproved(eligibilityDetails.getAprvLoan());
            if (camDetailsRequest.getHeader().getProduct() == Product.PL) {
                camSummary.setFinalLoanApproved(eligibilityDetails.getFinalAmtLacks());
            }

            camSummary.setEmi(eligibilityDetails.getEmi());
            //setting Pf from eligibility only for PL
            if (camDetailsRequest.getHeader().getProduct() == Product.PL) {
                camSummary.setPfPercentage(eligibilityDetails.getPF());
            }
            camSummary.setTenure(String.valueOf(eligibilityDetails.getTenor()));
            if (CollectionUtils.isNotEmpty(eligibilityDetails.getIncomeDetailList())) {
                if (eligibilityDetails.getIncomeDetailList().get(0).getRtr() != null) {
                    camSummary.setTotalTenure(eligibilityDetails.getIncomeDetailList().get(0).getRtr().getTotalTenor());
                    camSummary.setBalanceTenor(String.valueOf(eligibilityDetails.getIncomeDetailList().get(0).getRtr().getBalanceTenor()));
                }
                if (eligibilityDetails.getIncomeDetailList().get(0).getTopUp() != null) {
                    if (camSummary.getTotalTenure() != 0) {
                        camSummary.setTotalTenure(eligibilityDetails.getIncomeDetailList().get(0).getTopUp().getTotalTenor());
                    }
                    if (camSummary.getBalanceTenor() != null) {
                        camSummary.setBalanceTenor(String.valueOf(eligibilityDetails.getIncomeDetailList().get(0).getTopUp().getBalanceTenor()));
                    }
                }
            }
            if(!StringUtils.equalsIgnoreCase(camDetailsRequest.getHeader().getProduct().toProductName(),
                    Product.DIGI_PL.toProductName())) {
                camSummary.setFoir(eligibilityDetails.getFoir());
                camSummary.setRoi(eligibilityDetails.getEligibilityRoi());
            }
            if(StringUtils.equalsIgnoreCase(camDetailsRequest.getHeader().getProduct().toProductName(),
                    Product.DIGI_PL.toProductName())) {
                camSummary.setTenure(eligibilityDetails.getAppliedTenor());
            }
            camSummary.setFinalFoir(eligibilityDetails.getFinalFoir());
            if (Institute.isInstitute(camDetailsRequest.getHeader().getInstitutionId(), Institute.AMBIT)) {
                camSummary.setGrossIncome(eligibilityDetails.getGrossIncome());
                camSummary.setFixedOblgation(eligibilityDetails.getFixedOblgation());
                if (CollectionUtils.isNotEmpty(eligibilityDetails.getIncomeDetailList())) {
                    camSummary.setIncomeDetailList(eligibilityDetails.getIncomeDetailList());
                }
                if (eligibilityDetails.getDerivedFields() != null) {
                    camSummary.setDerivedFields(eligibilityDetails.getDerivedFields());
                }
            }
        }
    }

    private void setApplicationDetails(CamSummary camSummary, GoNoGoCustomerApplication gngAppObj) throws Exception {
        if (gngAppObj != null) {
            Application application = gngAppObj.getApplicationRequest().getRequest().getApplication();
            List<CustomerAddress> addr = gngAppObj.getApplicationRequest().getRequest().getApplicant().getAddress();

            if (application != null) {
                camSummary.setLoanAmount(application.getLoanAmount());
                camSummary.setLoanType(application.getLoanType());
                camSummary.setProduct(application.getLoanType());
                camSummary.setSourcingChannel(application.getChannel());
                camSummary.setTenorRequested(application.getTenorRequested());
                List<Collateral> collateralList = application.getCollateral();
                if (CollectionUtils.isNotEmpty(collateralList)) {
                    if(CollectionUtils.isNotEmpty(camSummary.getCollateralList())){
                        collateralList.stream().forEach(oApplCollateral ->{
                            camSummary.getCollateralList().forEach(oCamCollateral ->{
                                if(StringUtils.equalsIgnoreCase(oCamCollateral.getCollateralId(),oApplCollateral.getCollateralId()))
                                    oApplCollateral.setValueToConsider(oCamCollateral.getValueToConsider());
                            });
                        });
                    }
                    camSummary.setCollateralList(collateralList);
                }
            }
            camSummary.setCustomerType(gngAppObj.getApplicationRequest().getApplicantType());
            Applicant applicant = gngAppObj.getApplicationRequest().getRequest().getApplicant();
            List<CoApplicant> coApplicantList = gngAppObj.getApplicationRequest().getRequest().getCoApplicant();

            camSummary.setAddress(gngAppObj.getApplicationRequest().getRequest().getApplicant().getAddress());
            List<CustomerAddress> camAddr = camSummary.getAddress();
            if(!StringUtils.equalsIgnoreCase(gngAppObj.getApplicationRequest().getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName())) {
                int timeAtAddr = addr.get(0).getTimeAtAddress();
                for (CustomerAddress temp : camAddr) {
                    temp.setTimeAtAddress(timeAtAddr);
                }
            }
            List<ApplicantDetails> applicantList = new ArrayList<>();
            if (applicant != null) {
                camSummary.setApplicantName(applicant.getApplicantName());
                if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
                    camSummary.setMobileNumber(applicant.getPhone().get(0).getPhoneNumber());
                }
                camSummary.setDateOfBirth(applicant.getDateOfBirth());
                if (CollectionUtils.isNotEmpty(applicant.getEmployment())) {
                    camSummary.setTotalWorkExperience(applicant.getEmployment().get(0).getTotalYearsOfExperience());
                    camSummary.setWorkingSince(applicant.getEmployment().get(0).getWorkExps());
                    if (StringUtils.equalsIgnoreCase(applicant.getApplicantType(), EndPointReferrer.INDIVIDUAL)) {
                        camSummary.setCompanyName(applicant.getEmployment().get(0).getEmploymentName());
                    } else {
                        if (applicant.getApplicantName() != null)
                            camSummary.setCompanyName(applicant.getApplicantName().getFirstName());
                    }
                    camSummary.setBusinessVintage(applicant.getEmployment().get(0).getDateOfJoining());
                }

                if(Institute.isInstitute(gngAppObj.getApplicationRequest().getHeader().getInstitutionId(),Institute.SBFC)){
                    camSummary.setTimeWithEmplr(applicant.getEmployment().get(0).getTimeWithEmployer());
                }
                if (CollectionUtils.isNotEmpty(applicant.getLoanDetails())) {
                    camSummary.setBtBankOrNBFCName(applicant.getLoanDetails().get(0).getRepaymentBankName());
                    camSummary.setOriginalLoanAmount(applicant.getLoanDetails().get(0).getLoanAmt());
                    camSummary.setMob(applicant.getLoanDetails().get(0).getMob());
                    camSummary.setPos(applicant.getLoanDetails().get(0).getPos());
                    camSummary.setBtOrExistingLoanAmt(applicant.getLoanDetails().get(0).getLoanAmt());
                }
                if (applicant.getProfessionIncomeDetails() != null) {
                    camSummary.setPsl(applicant.getProfessionIncomeDetails().isPsl());
                    camSummary.setScheme(applicant.getProfessionIncomeDetails().getLoanScheme());
                    camSummary.setIndustry(applicant.getProfessionIncomeDetails().getBusiness());
                    camSummary.setBusinessNature(applicant.getProfessionIncomeDetails().getBusiness());
                }
                if (gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion() != null) {
                    camSummary.setPdStatus(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().isPositive());
                    camSummary.setPdDoneDate(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getDiscussiondate());
                    //For Ambit
                    camSummary.setCreditDiscussionDate(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getCreditDiscussion());
                    camSummary.setSector(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getIndustry());
                    camSummary.setPdDoneBy(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getPdDoneBy());
                    camSummary.setPdIndustry(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getIndustry());
                    camSummary.setBusinessType(gngAppObj.getApplicationRequest().getRequest().getPersonalDiscussion().getBusinessType());
                }


                    if(Institute.isInstitute(gngAppObj.getApplicationRequest().getHeader().getInstitutionId(),Institute.SBFC) ||
                            Institute.isInstitute(gngAppObj.getApplicationRequest().getHeader().getInstitutionId(),Institute.AMBIT)) {
                        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(gngAppObj.getApplicationRequest().getRefID(),gngAppObj.getApplicationRequest().getHeader().getInstitutionId());
                        ApplicantDetails applicantDetails = new ApplicantDetails();
                        applicantDetails.setApplicantId(applicant.getApplicantId());
                        applicantDetails.setApplicantName(applicant.getApplicantName());
                        applicantDetails.setAge(applicant.getAge());
                        applicantDetails.setEmploymentType(applicant.getEmployment().get(0).getEmploymentType());
                        applicantDetails.setPhoneNumber(applicant.getPhone().get(0).getPhoneNumber());
                        applicantDetails.setApplicantType(applicant.getApplicantType());
                        applicantDetails.setDtJoin(applicant.getEmployment().get(0).getDateOfJoining());

                        if(Institute.isInstitute(gngAppObj.getApplicationRequest().getHeader().getInstitutionId(),Institute.AMBIT)){
                            List<CustomerAddress> customerAddressList = gngAppObj.getApplicationRequest().getRequest().getApplicant().getAddress();
                            for(CustomerAddress customer:customerAddressList){
                                if(StringUtils.equalsIgnoreCase(applicantDetails.getApplicantType(),ApplicantType.INDIVIDUAL.value())) {
                                    if(StringUtils.equalsIgnoreCase(customer.getAddressType(),GNGWorkflowConstant.RESIDENCE.toFaceValue())) {
                                        applicantDetails.setTimeAtAddrResi(customer.getTimeAtAddress());
                                        applicantDetails.setAddressType(customer.getResidenceAddressType());
                                        break;
                                    }else{
                                        applicantDetails.setTimeAtAddrOfc(customer.getTimeAtAddress());
                                        applicantDetails.setAddressType(customer.getResidenceAddressType());
                                        break;
                                    }
                                }else{
                                    if(StringUtils.equalsIgnoreCase(customer.getAddressType(),GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                        applicantDetails.setTimeAtAddrOfc(customer.getTimeAtAddress());
                                        applicantDetails.setAddressType(customer.getResidenceAddressType());
                                        applicantDetails.setAge(applicant.getEmployment().get(0).getTimeWithEmployer());
                                        break;
                                    }
                                }
                            }
                            if(verificationDetails != null ){
                                List<PropertyVerification> propertyVerifications = verificationDetails.getOfficeVerification();
                                List<PropertyVerification> residenceVerification = verificationDetails.getResidenceVerification();
                                if(CollectionUtils.isNotEmpty(propertyVerifications)) {
                                    for (PropertyVerification propertyVerification : propertyVerifications) {
                                        if(propertyVerification.getApplicantId().equals("0")){
                                            if(propertyVerification.getOutput() != null) {
                                                applicantDetails.setOvStatus(propertyVerification.getOutput().getStatus());
                                            }
                                        }
                                    }
                                }
                                if(CollectionUtils.isNotEmpty(residenceVerification)) {
                                    for (PropertyVerification propertyVerification : residenceVerification) {
                                        if(propertyVerification.getApplicantId().equals("0")){
                                            if(propertyVerification.getOutput() != null) {
                                                applicantDetails.setRvStatus(propertyVerification.getOutput().getStatus());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (gngAppObj.getApplicantComponentResponse() != null && gngAppObj.getApplicantComponentResponse().getMultiBureauJsonRespose() != null &&
                                CollectionUtils.isNotEmpty(gngAppObj.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList()) &&
                                gngAppObj.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList().get(0).getResponseJsonObject() != null) {
                            applicantDetails.setResponseJsonObject(gngAppObj.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList().get(0).getResponseJsonObject());
                        }

                        applicantList.add(applicantDetails);

                        if(CollectionUtils.isNotEmpty(coApplicantList)) {
                            for (CoApplicant coApplicant : coApplicantList) {
                                ApplicantDetails coApplicantDetails = new ApplicantDetails();
                                coApplicantDetails.setApplicantId(coApplicant.getApplicantId());
                                coApplicantDetails.setApplicantName(coApplicant.getApplicantName());
                                coApplicantDetails.setAge(coApplicant.getAge());
                                coApplicantDetails.setEmploymentType(coApplicant.getEmployment().get(0).getEmploymentType());
                                coApplicantDetails.setPhoneNumber(coApplicant.getPhone().get(0).getPhoneNumber());
                                coApplicantDetails.setApplicantType(coApplicant.getApplicantType());
                                coApplicantDetails.setRelationWithApplicant(coApplicant.getRelationWithApplicant());
                                coApplicantDetails.setDtJoin(coApplicant.getEmployment().get(0).getDateOfJoining());

                                if (CollectionUtils.isNotEmpty(gngAppObj.getApplicantComponentResponseList())){
                                    for(ComponentResponse componentResponse:gngAppObj.getApplicantComponentResponseList()) {
                                        if (componentResponse.getMultiBureauJsonRespose() != null && componentResponse.getApplicantId() != null){
                                            if (StringUtils.equalsIgnoreCase(componentResponse.getApplicantId(), coApplicant.getApplicantId())) {
                                                if (CollectionUtils.isNotEmpty(componentResponse.getMultiBureauJsonRespose().getFinishedList())){
                                                        if(componentResponse.getMultiBureauJsonRespose().getFinishedList().get(0).getResponseJsonObject() != null) {
                                                            coApplicantDetails.setResponseJsonObject(componentResponse.getMultiBureauJsonRespose().getFinishedList().get(0).getResponseJsonObject());
                                                        }  }
                                            }
                                    }
                                    }
                            }

                                if(Institute.isInstitute(gngAppObj.getApplicationRequest().getHeader().getInstitutionId(),Institute.AMBIT)){
                                    for(CustomerAddress customer:coApplicant.getAddress()){
                                        if(StringUtils.equalsIgnoreCase(coApplicant.getApplicantType(),ApplicantType.INDIVIDUAL.value())) {
                                            if(StringUtils.equalsIgnoreCase(customer.getAddressType(),GNGWorkflowConstant.RESIDENCE.toFaceValue())) {
                                                coApplicantDetails.setTimeAtAddrResi(customer.getTimeAtAddress());
                                                coApplicantDetails.setAddressType(customer.getResidenceAddressType());
                                                break;
                                            }else{
                                                coApplicantDetails.setTimeAtAddrOfc(customer.getTimeAtAddress());
                                                coApplicantDetails.setAddressType(customer.getResidenceAddressType());
                                                break;
                                            }
                                        }else{
                                            if(StringUtils.equalsIgnoreCase(customer.getAddressType(),GNGWorkflowConstant.OFFICE.toFaceValue())) {
                                                coApplicantDetails.setTimeAtAddrOfc(customer.getTimeAtAddress());
                                                coApplicantDetails.setAddressType(customer.getResidenceAddressType());
                                                coApplicantDetails.setAge(coApplicant.getEmployment().get(0).getTimeWithEmployer());
                                                break;
                                            }
                                        }
                                    }
                                    if(verificationDetails != null ){
                                        List<PropertyVerification> propertyVerifications = verificationDetails.getOfficeVerification();
                                        List<PropertyVerification> residenceVerification = verificationDetails.getResidenceVerification();
                                        if(CollectionUtils.isNotEmpty(propertyVerifications)) {
                                            for (PropertyVerification propertyVerification : propertyVerifications) {
                                                if(StringUtils.equalsIgnoreCase(propertyVerification.getApplicantId(),coApplicant.getApplicantId())){
                                                    if(propertyVerification.getOutput() != null) {
                                                        coApplicantDetails.setOvStatus(propertyVerification.getOutput().getStatus());
                                                    }
                                                }
                                            }
                                        } if(CollectionUtils.isNotEmpty(residenceVerification)) {
                                            for (PropertyVerification propertyVerification : residenceVerification) {
                                                if(StringUtils.equalsIgnoreCase(propertyVerification.getApplicantId(),coApplicant.getApplicantId())){
                                                    if(propertyVerification.getOutput() != null) {
                                                        coApplicantDetails.setRvStatus(propertyVerification.getOutput().getStatus());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                applicantList.add(coApplicantDetails);

                            }
                        }
                        camSummary.setApplicantList(applicantList);
                    }
                }

            if (gngAppObj.getApplicationRequest().getAppMetaData().getBranchV2() != null) {
                camSummary.setRmName(gngAppObj.getApplicationRequest().getAppMetaData().getBranchV2().getBranchManagerName());
                camSummary.setLocation(gngAppObj.getApplicationRequest().getAppMetaData().getBranchV2().getBranchName());
            }
            if (CollectionUtils.isNotEmpty(applicant.getEmployment()) && applicant.getEmployment().get(0) != null) {
                camSummary.setDateOfJoining(GngUtils.setValueForNull(applicant.getEmployment().get(0).getDateOfJoining()));
            }
            if (gngAppObj.getIntrimStatus() != null)
                camSummary.setLoginDate(gngAppObj.getIntrimStatus().getStartTime());
            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(gngAppObj.getGngRefId());
            if(insuranceData != null){
                camSummary.setInsuranceOfferedAmt(setInsuranceValue(insuranceData));
            }
        }
    }

    @Override
    public BaseResponse saveDMDetails(DMRequest dmRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(dmRequest.getRefId(),dmRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(dmRequest.getHeader(), dmRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.SAVE_DM_DETAILS,
                applicationStatus, dmRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving Disbursement Memo Details for refId {}", dmRequest.getRefId());
            boolean save = applicationRepository.saveDMDetails(dmRequest);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = dmRequest.getHeader();
                applicationRepository.updateCompletedInfo(dmRequest.getRefId(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_DM_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

                DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(dmRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, disbursementMemo);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getDMDetails(DMRequest dmRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = new BaseResponse();
        DisbursementMemo disbursementMemo = chkStageForDM(dmRequest, httpRequest);
        if (disbursementMemo != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, disbursementMemo);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    private DisbursementMemo chkStageForDM(DMRequest dmRequest, HttpServletRequest httpRequest) throws Exception {
        DisbursementMemo disbursementMemo = new DisbursementMemo();
        GoNoGoCustomerApplication gngAppObj = applicationRepository.getGoNoGoCustomerApplicationByRefId
                (dmRequest.getRefId());
        if (gngAppObj != null && gngAppObj.getApplicationRequest() != null) {
            CamDetails camDetails = camDetails(dmRequest);
            LoanCharges loanCharges = loanChargesDetails(dmRequest);
            if(StringUtils.equalsIgnoreCase(gngAppObj.getApplicationRequest().getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName())
                    && null != loanCharges
                    && Buckets.isSTPBucket(gngAppObj.getApplicationBucket())){
                logger.debug("Getting tranch-wise loan charges.");
                loanCharges =  miFinHelper.getTranchWiseLoanCharges(gngAppObj.getApplicationRequest() , loanCharges.getLoanChargesList());
            }

            Repayment repayment = repaymentDetails(dmRequest);
            DisbursementMemo dmDbObj = applicationRepository.fetchDMDetails(dmRequest);
            if(null != gngAppObj.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails())
                camDetails.getSummary().setScheme(gngAppObj.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme());
            if (camDetails != null) {
                disbursementMemo.setCamSummary(camDetails.getSummary());
            }
            disbursementMemo.setLoanCharges(loanCharges);
            disbursementMemo.setRepayment(repayment);
            ApplicationRequest applicationRequest = gngAppObj.getApplicationRequest();
            // An application always has appmetadata; but following check is for fail-safe
            if (applicationRequest.getAppMetaData() != null && applicationRequest.getAppMetaData().getBranchV2() != null) {
                if (StringUtils.equalsIgnoreCase("Spoke", applicationRequest.getAppMetaData().getBranchV2().getBranchType())) {
                    disbursementMemo.setBranchcode(applicationRequest.getAppMetaData().getBranchV2().getHubBranchId().toString());
                    disbursementMemo.setBranchName(applicationRequest.getAppMetaData().getBranchV2().getHubBranchName());
                    disbursementMemo.setSpokeCode(applicationRequest.getAppMetaData().getBranchV2().getBranchId().toString());
                    disbursementMemo.setSpokeName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
                } else if (StringUtils.equalsIgnoreCase("LoanStation", applicationRequest.getAppMetaData().getBranchV2().getBranchType())) {
                    disbursementMemo.setBranchcode(applicationRequest.getAppMetaData().getBranchV2().getHubBranchId().toString());
                    disbursementMemo.setBranchName(applicationRequest.getAppMetaData().getBranchV2().getHubBranchName());
                    disbursementMemo.setLoanStationCode(applicationRequest.getAppMetaData().getBranchV2().getBranchId().toString());
                    disbursementMemo.setLoanStationName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
                } else {
                    disbursementMemo.setBranchcode(String.valueOf(applicationRequest.getAppMetaData().getBranchV2().getBranchId()));
                    disbursementMemo.setBranchName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
                }
            }
            if (applicationRequest.getRequest() != null && applicationRequest.getRequest().getApplication() != null) {
                disbursementMemo.setCoApplicant(applicationRequest.getRequest().getCoApplicant());
                if (applicationRequest.getRequest().getTopupInfo() != null &&
                        dmDbObj != null && CollectionUtils.isNotEmpty(applicationRequest.getRequest().getTopupInfo().getMiFinOldProspectCode())) {
                    disbursementMemo.setLinkLoan(dmDbObj.getLinkLoan());
                    disbursementMemo.setCustomerId(dmDbObj.getCustomerId());
                }
            }
            setValuationDetails(dmRequest, disbursementMemo);
            if (dmDbObj != null) {
                disbursementMemo.setDmInputList(dmDbObj.getDmInputList());
                disbursementMemo.setFinalConsiderValueType(dmDbObj.getFinalConsiderValueType());
                disbursementMemo.setRemarkList(dmDbObj.getRemarkList());
            }
            dmRequest.setDisbursementMemo(disbursementMemo);
            applicationRepository.saveDMDetails(dmRequest);
            disbursementMemo = applicationRepository.fetchDMDetails(dmRequest);
        }
        return disbursementMemo;
    }

    private Repayment repaymentDetails(DMRequest dmRequest) {
        RepaymentRequest repaymentRequest = new RepaymentRequest();
        repaymentRequest.setHeader(dmRequest.getHeader());
        repaymentRequest.setRefId(dmRequest.getRefId());
        Repayment repayment = applicationRepository.fetchRepaymentDetails(repaymentRequest);
        return repayment;
    }

    private LoanCharges loanChargesDetails(DMRequest dmRequest) {
        LoanChargesRequest loanChargesRequest = new LoanChargesRequest();
        loanChargesRequest.setHeader(dmRequest.getHeader());
        loanChargesRequest.setRefId(dmRequest.getRefId());
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetails(loanChargesRequest);
        return loanCharges;
    }

    private CamDetails camDetails(DMRequest dmRequest) {
        CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
        camDetailsRequest.setHeader(dmRequest.getHeader());
        camDetailsRequest.setRefId(dmRequest.getRefId());
        camDetailsRequest.setCamType(EndPointReferrer.CAM_SUMMARY);
        CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
        return camDetails;
    }

    private void setValuationDetails(DMRequest dmRequest, DisbursementMemo disbursementMemo) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setHeader(dmRequest.getHeader());
        valuationRequest.setRefId(dmRequest.getRefId());
        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        if (valuation != null) {
            List<ValuationDetails> valuationDetailsList = valuation.getValuationDetailsList();
            if (CollectionUtils.isNotEmpty(valuationDetailsList)) {
                for (ValuationDetails valuationDetails : valuationDetailsList) {
                    if (disbursementMemo.getValuer1() == null) {
                        disbursementMemo.setValuer1(valuationDetails.getAgencyCode());
                    } else {
                        disbursementMemo.setValuer2(valuationDetails.getAgencyCode());
                    }
                    if (valuationDetails != null && valuationDetails.getValuationInput() != null) {
                        disbursementMemo.setPropertyAddress(valuationDetails.getValuationInput().getPropertyAddress());
                    }
                }
            }
        }
    }

    @Override
    public BaseResponse saveDedupeRemarks(DedupeRemarkRequest dedupeRemarkRequest) {
        BaseResponse baseResponse = null;
        DedupeMatch dedupeMatch = dedupeRemarkRequest.getDedupeMatch();
        dedupeMatch.setRefId(dedupeRemarkRequest.getRefId());
        dedupeMatch.setInstitutionId(dedupeRemarkRequest.getHeader().getInstitutionId());
        try {
            boolean save = finalApplicationRepository.saveDedupeInfo(dedupeMatch);
            if (save) {
                return GngUtils.getBaseResponse(HttpStatus.OK, dedupeRemarkRequest.getDedupeMatch());
            }
        } catch (Exception e) {
            logger.error("Exception occured while saving deviation" + e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getDedupeRemarks(DedupeRemarkRequest dedupeRemarkRequest) {
        String refId = dedupeRemarkRequest.getRefId();
        String instId = dedupeRemarkRequest.getHeader().getInstitutionId();
        BaseResponse baseResponse = null;
        DedupeMatch dedupeMatch = applicationRepository.fetchDedupeInfo(refId, instId);
        if (dedupeMatch != null && CollectionUtils.isNotEmpty(dedupeMatch.getDedupeMatchDetails())) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dedupeMatch);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse saveSanctionConditions(SanctionConditionRequest sanctionConditionRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(sanctionConditionRequest.getHeader(), sanctionConditionRequest.getRefId(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_SANCTION_CONDITIONS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), sanctionConditionRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving SanctionConditions Details refId {}", sanctionConditionRequest.getRefId());
            boolean save = applicationRepository.saveSanctionConditions(sanctionConditionRequest);
            if (save) {
                applicationRepository.updateCompletedInfo(sanctionConditionRequest.getRefId(), sanctionConditionRequest.getHeader().getInstitutionId(),
                        EndPointReferrer.SAVE_SANCTION_CONDITIONS, sanctionConditionRequest.getHeader().getLoggedInUserId(),
                        sanctionConditionRequest.getHeader().getLoggedInUserRole());
                SanctionConditions sanctionConditions = applicationRepository.fetchSanctionConditions(sanctionConditionRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, sanctionConditions);
            }
        } catch (Exception e) {
            logger.error("Exception occured while saving SanctionConditions" + e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getSanctionConditions(SanctionConditionRequest sanctionConditionRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(sanctionConditionRequest.getRefId(),sanctionConditionRequest.getHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() &&
                null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus()!=null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(sanctionConditionRequest.getHeader(), sanctionConditionRequest.getRefId(), httpRequest,
                applicationStage, EndPointReferrer.GET_SANCTION_CONDITIONS,
                applicationStatus, sanctionConditionRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SanctionConditions sanctionConditions = applicationRepository.fetchSanctionConditions(sanctionConditionRequest);

        if (sanctionConditions != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, sanctionConditions);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return response;
    }

    @Override
    public BaseResponse getDocumentByFileName(DocumentRequest documentRequest, HttpServletRequest httpRequest) {
        BaseResponse response;
        ActivityLogs activityLogs = auditHelper.createActivityLog(documentRequest.getHeader(), documentRequest.getRefId(), httpRequest,
                null, EndPointReferrer.GET_DOCUMENT_BY_FILE_NAME,
                null, documentRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<KycImageDetails> appImgDetails = uploadFileRepository
                .getDocumentDetailsByFileName(documentRequest.getRefId(), documentRequest.getHeader().getInstitutionId(),
                        documentRequest.getFileName());
        if (appImgDetails != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, appImgDetails);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from getImageByFileName in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse updateCompletedInfoGonogo(UpdateGNGRequest updateGNGRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response;
        String result;
        ActivityLogs activityLogs = auditHelper.createActivityLog(updateGNGRequest.getHeader(), updateGNGRequest.getRefId(), httpRequest,
                null, EndPointReferrer.UPDATE_COMPLETED_INFO_GNG,
                null, updateGNGRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList = applicationRepository
                .fetchGNGData(updateGNGRequest);
        if (CollectionUtils.isNotEmpty(goNoGoCustomerApplicationList)) {
            logger.debug(" CompletedInfo count  {} ", goNoGoCustomerApplicationList.size());
            int count = applicationRepository.updateCompletedInfoGonogo(updateGNGRequest, goNoGoCustomerApplicationList);
            result = "Fetched " + goNoGoCustomerApplicationList.size() + " and Updated " + count;
            response = GngUtils.getBaseResponse(HttpStatus.OK, result);
        } else {
            result = "No result found to update";
            response = GngUtils.getBaseResponse(HttpStatus.OK, result);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log for update completed Info GoNoGo in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public void externalAPICall(GoNoGoCustomerApplication goNoGoCustomerApplication, ApplicationRequest applicationRequest) {
        // Whether to push to external system
        if (lookupService.checkActionsAccess(
                applicationRequest.getHeader().getInstitutionId()
                , applicationRequest.getHeader().getProduct().toProductId()
                , ActionName.PUSH_DATA_TO_MIFIN)) {
            try {
                logger.info("Pushing data to mifin for refId: {}", goNoGoCustomerApplication.getGngRefId());
                externalApiManager.pushDataToMiFin(goNoGoCustomerApplication);
            } catch (Exception e) {
                logger.debug("Exception {} occurred while {} for refid {}", e.getMessage(),
                        ActionName.PUSH_DATA_TO_MIFIN.name(), applicationRequest.getRefID());
                //e.printStackTrace();
            }
        }

        /*// Whether to push to external system
        if (lookupService.checkActionsAccess(
                applicationRequest.getHeader().getInstitutionId()
                , applicationRequest.getHeader().getProduct().toProductId()
                , ActionName.PUSH_DATA_TO_LMS)) {
            try {
                logger.info("Pushing data to LMS for refId: {}",goNoGoCustomerApplication.getGngRefId());
                externalApiManager.pushDataToLMS(goNoGoCustomerApplication.getApplicationRequest());
            } catch(Exception e){
                logger.debug("Exception {} occurred while {} for refid {}", e.getMessage(),
                        ActionName.PUSH_DATA_TO_LMS.name(), applicationRequest.getRefID());
            }
        }*/
    }

    @Override
    public BaseResponse verifyApproval(ApprovalVerificationRequest amountVerificationRequest) {

        double amount = amountVerificationRequest.getUserMaxAmount();
        String refId = amountVerificationRequest.getRefId();
        String instId = amountVerificationRequest.getHeader().getInstitutionId();
        String product = amountVerificationRequest.getHeader().getProduct().name();
        boolean amountVerified = verifyAprvAmount(refId, instId, amount, product);
        boolean allDmResolved = validateDeviations(refId, instId);


        BaseResponse baseResponse = new BaseResponse();

        List<Error> errorList = new ArrayList<>();
        if (!amountVerified) {
            Error error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message("Credit Approval Limit Exceed")
                    .build();
            errorList.add(error);
        }
        //Check where insurance proposed amount and eligibility loan  amount for insurance case is same.
        Error error = verifyInsuranceAmount(refId, instId, product);
        errorList = new ArrayList<>();

        if (error != null) {
            errorList.add(error);
        }
        if (!allDmResolved) {
             error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message("Some of the raised deviations are pending, needs to be resolved before approval")
                    .build();
            errorList.add(error);
        }

        // for validating Legal,RV,OV and valuation is verified by verifiers or not
        verifyValidation(amountVerificationRequest, refId, errorList);

        if(Institute.isInstitute(instId, Institute.SBFC)){
            creditApproveValidationForSBFC(error,errorList,refId,instId,product);
        }

        if (CollectionUtils.isNotEmpty(errorList)) {
            logger.info("Errorlist size is{} for refId {}", errorList.size(), refId);
            logger.info(errorList.toString());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.PRECONDITION_FAILED, errorList);
        } else {
            GenericMsgResponse genericMsgResponse = GenericMsgResponse.builder()
                    .status(GNGWorkflowConstant.SUCCESS.name())
                    .message("Success")
                    .build();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
        }
        return baseResponse;
    }

    private void creditApproveValidationForSBFC(Error error,List<Error> errorList,String refId,String instId,String product){
        List<String> creditStages = new ArrayList<>();
        creditStages.add(Stages.Stage.CRDT.name());creditStages.add(Stages.Stage.CR_H.name());creditStages.add(Stages.Stage.APRV.name());
        creditStages.add(Stages.Stage.DCLN.name());creditStages.add(Stages.Stage.SUBJ_APRV.name());
        try{
            GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId,instId);
            String currentStage = gngApp.getApplicationRequest().getCurrentStageId();

            if(StringUtils.isNotEmpty(currentStage) && creditStages.contains(currentStage)){
                if(StringUtils.equalsIgnoreCase(product, Product.LAP.name())){
                    //customerCategory field is now mandatory, on the cam-summary screen SBFC
                    error = customerCategoryHelper(refId,instId);
                    if(error != null){
                        errorList.add(error);
                    }

                    //Disbursal amount validation at cam summary screen
                    error = camSummaryDisbAmtValidation(refId,instId,gngApp);
                    if(error != null){
                        errorList.add(error);
                    }
                }

                //personal discussion screen validation
                if(StringUtils.equalsIgnoreCase(product, Product.LAP.name()) ||
                        StringUtils.equalsIgnoreCase(product, Product.BL.name())){
                    error = pdScreenValidationHelper(gngApp);
                    if(error != null){
                        errorList.add(error);
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error while fetching gngApplication {}",e.getMessage());
            e.printStackTrace();
        }
    }

    private Error customerCategoryHelper(String refId, String instId){
        Error error = null;
        try{
            CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId,instId);
            if(camDetails != null && camDetails.getSummary() != null && StringUtils.isEmpty(camDetails.getSummary().getCustomerCategory())){
                error = Error.builder().errorType(MsgConstants.BUSINESS_ERROR).message("Please select customer category at cam summary screen").build();
            }
        }catch (Exception e){
            logger.error("Error while customerCategoryHelper {}", e.getMessage());
            e.printStackTrace();
        }
        return error;
    }

    private Error pdScreenValidationHelper(GoNoGoCustomerApplication gngApp){
        Error error = null;
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null &&
                    gngApp.getApplicationRequest().getRequest() != null &&
                    gngApp.getApplicationRequest().getRequest().getPersonalDiscussion() != null){
                PersonalDiscussion personalDiscussion = gngApp.getApplicationRequest().getRequest().getPersonalDiscussion();
                String businessType = personalDiscussion.getBusinessType();
                String industryType = personalDiscussion.getIndustry();
                String sector = personalDiscussion.getSector();
                if(StringUtils.isEmpty(businessType) || StringUtils.isEmpty(industryType) || StringUtils.isEmpty(sector)){
                    error = Error.builder()
                            .errorType(MsgConstants.BUSINESS_ERROR)
                            .message("Business Type and Sector is mandatory at Personal Discussion screen!")
                            .build();
                }
            }
        }catch (Exception e){
            logger.error("Error while pdScreenValidationHelper {}",e.getMessage());
            e.printStackTrace();
        }
        return error;
    }

    private Error camSummaryDisbAmtValidation(String refId,String instId,GoNoGoCustomerApplication gngApp){
        Error error = null;
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getRequest() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplication() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplicant() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails() != null){
                String topUpType = gngApp.getApplicationRequest().getRequest().getApplication().getTopupType();
                String loanScheme = gngApp.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme();
                if(StringUtils.containsIgnoreCase(loanScheme,"BT") &&
                        !(StringUtils.equalsIgnoreCase(topUpType,"Top Up Parallel") || StringUtils.equalsIgnoreCase(topUpType,"Top Up Gross"))){
                    CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId,instId);
                    if(camDetails != null && camDetails.getSummary() != null &&
                            camDetails.getSummary().getDisbAmt() <= 0){
                        error = Error.builder().
                                errorType(MsgConstants.BUSINESS_ERROR).
                                message("Please enter Disbursal amount at Cam Summary screen").
                                build();
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error while camSummaryDisbAmtValidation {}", e.getMessage());
            e.printStackTrace();
        }
        return error;
    }

    private Error verifyInsuranceAmount(String refId, String instId, String product) {
        InsuranceData insuranceData = applicationRepository.fetchInsuranceData(refId);
        EligibilityDetails eligibiltyDetails = applicationRepository.fetchEligibilityDetails(refId, instId);
        //if flag is true the It will not allowed to Approve credit
        boolean flag = false;
        String msg = null;
        String appType = null;
        Error error = null;
        Map<Boolean, String> resultWithMsg = new HashedMap();

        /*******
         1) For Credit user there is check on Insurance proposed  Amount, tenor
         and Eligibility Approved Loan Amount, tenor respectively. If it is Not same then throws Error
         but in case when Insurance policy is 'Exceptional' case or waived then It will not throw Error
         2)For PL case insurance premium will calculate on submit so there is check on Credit to add
         NomineeInfor compulsory
         *********/
        if(insuranceData != null){
            if(CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
                for(Applicant applicant : insuranceData.getApplicantList()){
                    if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList()) && !applicant.isWaived()){
                        for(InsurancePolicy insurancePolicy  : applicant.getInsurancePolicyList()){
                            int applicantId = Integer.parseInt(applicant.getApplicantId());
                            if(applicantId == 0){
                                appType = "Applicant";
                            }else{
                                appType = "Co-Applicant";
                            }
                            if(StringUtils.isNotEmpty(insurancePolicy.getProposedAmount()) && !insurancePolicy.isException()
                                    && eligibiltyDetails != null
                                    && StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(),IciciHelper.ICICI_COMPANY_NAME) &&
                                    ApplicantType.isIndividual(applicant.getApplicantType())
                                    && StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(),ReligareHelper.RELIGARE_LOAN_PROTECTION)){
                                double proposedAmt = Double.parseDouble(insurancePolicy.getProposedAmount());
                                int tenor = Integer.parseInt(insurancePolicy.getTenorRequested());
                                if(!GngNumUtil.compareDoubleValue(eligibiltyDetails.getAprvLoan(),proposedAmt) ||
                                        tenor != eligibiltyDetails.getTenor() && insurancePolicy.getPremium() == 0){
                                    flag = true;
                                    if(applicantId == 0){
                                        msg = "please retrigger premium amount of "+appType;
                                    }else{
                                        msg = "please retrigger premium amount of   "+appType+" " +applicantId;
                                    }
                                    break;
                                }
                            }
                            if(StringUtils.equalsIgnoreCase(product,Product.PL.name()) ||
                                    StringUtils.equalsIgnoreCase(product,Product.BL.name())){
                                if(insurancePolicy.getNomineeInfo() == null ||
                                        StringUtils.isEmpty(insurancePolicy.getNomineeInfo().getName().getFirstName())){
                                    flag = true;
                                    msg = "please add NomineeInfo For  "+appType+" " +applicantId;
                                    break;
                                }
                            }
                        }
                    }

                    if(flag){
                        resultWithMsg.put(flag, msg);
                        break ;
                    }
                }
            }
        }

        if(MapUtils.isNotEmpty(resultWithMsg)){
            error = buildErrorList(resultWithMsg.get(flag));
        }
        return error;
    }


    private void verifyValidation(ApprovalVerificationRequest amountVerificationRequest, String refId, List<Error> errorList) {
        if(!StringUtils.equalsIgnoreCase(amountVerificationRequest.getHeader().getLoggedInUserRole(),Roles.Role.EXTERNAL_CREDIT.name())){
            //checking valuation is verify or not
            Error error = verifyValuation(refId, amountVerificationRequest.getHeader());
            if (error != null) {
                errorList.add(error);
            }
        }

        //checking in only case of role is BOPS
        if (StringUtils.equalsIgnoreCase(amountVerificationRequest.getHeader().getLoggedInUserRole(), Roles.Role.BOPS.name())) {
            //checking Legal is verify or not
            Error error1 = verifyLegal(refId, amountVerificationRequest.getHeader());
            if (error1 != null) {
                errorList.add(error1);
            }
            //checking residence is verify or not
            Error error2 = verifyResidenceVerification(refId, amountVerificationRequest.getHeader());
            if (error2 != null) {
                errorList.add(error2);
            }

            //checking office is verify or not
            Error error3 = verifyOfficeVerification(refId, amountVerificationRequest.getHeader());
            if (error3 != null) {
                errorList.add(error3);
            }
        }
    }

    private Error verifyValuation(String refId, Header header) {
        Boolean result = false;
        String msg = "Success";
        Error error = null;
        Map<Boolean, String> resultWithMsg = new HashedMap();
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setHeader(header);
        valuationRequest.setRefId(refId);
        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        if (valuation != null) {
            if (CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
                for (ValuationDetails valList : valuation.getValuationDetailsList()) {
                    if (StringUtils.equalsIgnoreCase(valList.getStatus().trim(), ThirdPartyVerification.Status.VERIFIED.name())) {
                        result = true;
                    } else {
                        result = false;
                        int collateralId = Integer.valueOf(valList.getCollateralId()) + 1;
                        msg = "Property Valuation of Collateral" + collateralId + "is Pending. Please fill them before further proceeding";
                        break;
                    }
                }
            }else{
                msg = "Please fill Collateral details in Collateral Screen";
            }
            resultWithMsg.put(result, msg);
        }
        if (resultWithMsg != null) {
            for (Map.Entry<Boolean, String> temp : resultWithMsg.entrySet()) {
                if (temp.getKey() == false) {
                    error = buildErrorList(temp.getValue());
                }
            }
        }
        return error;
    }

    private Error verifyOfficeVerification(String refId, Header header) {
        Boolean result = false;
        String msg = "Success";
        Error error = null;
        Map<Boolean, String> resultWithMsg = new HashedMap();
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, header.getInstitutionId());
        if (verificationDetails != null) {
            if (CollectionUtils.isNotEmpty(verificationDetails.getOfficeVerification())) {
                for (PropertyVerification officeList : verificationDetails.getOfficeVerification()) {
                    if (StringUtils.equalsIgnoreCase(officeList.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                        result = true;
                    } else {
                        msg = "Office Verification is Pending. Please fill them before further proceeding";
                        result = false;
                        break;
                    }
                }
                resultWithMsg.put(result, msg);
            }
        }
        if (resultWithMsg != null) {
            for (Map.Entry<Boolean, String> temp : resultWithMsg.entrySet()) {
                if (temp.getKey() == false) {
                    error = buildErrorList(temp.getValue());
                }
            }
        }
        return error;
    }

    private Error buildErrorList(String msg) {
        Error error = Error.builder()
                .errorType(MsgConstants.BUSINESS_ERROR)
                .message(msg)
                .build();
        return error;
    }


    private Error verifyResidenceVerification(String refId, Header header) {
        Boolean result = false;
        String msg = "Success";
        Error error = null;
        Map<Boolean, String> resultWithMsg = new HashedMap();
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, header.getInstitutionId());
        if (verificationDetails != null) {
            if (CollectionUtils.isNotEmpty(verificationDetails.getResidenceVerification())) {
                for (PropertyVerification resiList : verificationDetails.getResidenceVerification()) {
                    if (StringUtils.equalsIgnoreCase(resiList.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                        result = true;
                    } else {
                        msg = "Residence Verification is Pending. Please fill them before further proceeding";
                        result = false;
                        break;
                    }
                }
                resultWithMsg.put(result, msg);
            }

        }
        if (resultWithMsg != null) {
            for (Map.Entry<Boolean, String> temp : resultWithMsg.entrySet()) {
                if (temp.getKey() == false) {
                    error = buildErrorList(temp.getValue());
                }
            }
        }
        return error;
    }

    private Error verifyLegal(String refId, Header header) {
        Boolean result = true;
        String msg = "Success";
        Error error = null;
        Map<Boolean, String> resultWithMsg = new HashedMap();
        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetails(refId, header.getInstitutionId());
        ApplicationRequest applicantionRequest = applicationRepository.getApplicantData(refId, header.getInstitutionId());
        if (legalVerification != null) {
            if (CollectionUtils.isNotEmpty(legalVerification.getLegalVerificationDetailsList())) {
                for (LegalVerificationDetails tempList : legalVerification.getLegalVerificationDetailsList()) {
                    if (!StringUtils.equalsIgnoreCase(tempList.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                         ProfessionIncomeDetails professionIncomeDetails = applicantionRequest.getRequest().getApplicant().getProfessionIncomeDetails();
                         if(!GngUtils.verifiedSchemeList.contains(professionIncomeDetails.getLoanScheme())){
                            result = false;
                            int collateralId = Integer.valueOf(tempList.getCollateralId()) + 1;
                            msg = "Legal Verification of Collateral " + collateralId + " is Pending. Please fill them before further proceeding";
                            break;
                          }
                    }
                }
            }
            resultWithMsg.put(result, msg);
        }
        if (resultWithMsg != null) {
            for (Map.Entry<Boolean, String> temp : resultWithMsg.entrySet()) {
                if (temp.getKey() == false) {
                    error = buildErrorList(temp.getValue());
                }
            }
        }
        return error;
    }


    private boolean verifyAprvAmount(String refId, String instId, double amount, String product) {

        boolean result = false;
        EligibilityDetails eligibilityDetails = null;
        if (StringUtils.equalsIgnoreCase(Product.DIGI_PL.name(), product)) {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.fetchApplicantEligibility(refId);
            if(null != goNoGoCustomerApplication){
                eligibilityDetails = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getDigiPLEligibility();
            }
        }else
            eligibilityDetails = applicationRepository.fetchEligibilityDetails(refId, instId);

        if (eligibilityDetails != null) {
            double aprvAmt = eligibilityDetails.getAprvLoan();
            logger.info("Loan amount you are approving is ", eligibilityDetails.getAprvLoan());
            logger.info("Max limit of amount to be approved is ", amount);
            if (amount >= aprvAmt) {
                result = true;
            }
        } else {
            logger.info("Eligibility details are not available for refId {} with institutionId {}", refId, instId);
        }
        return result;
    }

    private boolean validateDeviations(String refID, String insttId) {
        boolean result = true;
        try {
            moduleManager.validateDeviations(refID, insttId);
        } catch (GoNoGoException e) {
            result = false;
        }
        return result;
    }

    @Override
    public BaseResponse deleteCollateral(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(), applicationRequest.getRefID(), httpRequest,
                applicationRequest.getCurrentStageId(), EndPointReferrer.DELETE_COLLATERAL,
                goNoGoCustomerApplication.getApplicationStatus(), applicationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("deleteCollateral for refId {}", applicationRequest.getRefID());
            deleteCollateralForPropertyVerification(applicationRequest);
            deleteCollateralForPropertyValuation(applicationRequest);
            deleteCollateralForLegalVerificaion(applicationRequest);
            deleteCollateralImage(applicationRequest);
            GenericMsgResponse genericMsgResponse = GenericMsgResponse.builder()
                    .status(GNGWorkflowConstant.SUCCESS.name())
                    .message("Success")
                    .build();
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    private void deleteCollateralImage(ApplicationRequest applicationRequest) {

        List<Collateral> collaterallList = applicationRequest.getRequest().getApplication().getCollateral();
        Set<String> refSet = new HashSet<>();
        List<String> Ids = new ArrayList<>();
        refSet.add(applicationRequest.getRefID());
        for (Collateral collateral : collaterallList) {
            Ids.add(collateral.getCollateralId());
        }

        List<KycImageDetails> kycImageDetailsList = uploadFileRepository
                .getKycImageDetails(refSet, applicationRequest.getHeader().getInstitutionId(), Status.ALL.name());
        if (CollectionUtils.isNotEmpty(kycImageDetailsList)) {
            logger.debug("Images found for refid {} , id {}", applicationRequest.getRefID(), kycImageDetailsList.size());
            //For deleting propertyVisit LegalVerification Images images using image types
            for (KycImageDetails kycImageDetails : kycImageDetailsList) {
                if (kycImageDetails != null) {
                    List<ImagesDetails> imagesDetailsList = kycImageDetails.getImageMap();
                    for (ImagesDetails imageDetail : imagesDetailsList) {
                        for (String id : Ids) {
                            if (imageDetail != null && StringUtils.isNotEmpty(imageDetail.getImageType())
                                    && StringUtils.equalsIgnoreCase(imageDetail.getImageType(), "Property Visit" + id)) {
                                logger.debug("deteImage for {} and  imageId{}", imageDetail.getImageType(), imageDetail.getImageId());
                                uploadFileRepository.deletedocument(imageDetail.getImageId());
                            }
                            if (imageDetail != null && StringUtils.isNotEmpty(imageDetail.getImageType())
                                    && StringUtils.equalsIgnoreCase(imageDetail.getImageType(), "Legal Varification" + id)) {
                                logger.debug("deteImage for {} and  imageId{}", imageDetail.getImageType(), imageDetail.getImageId());
                                uploadFileRepository.deletedocument(imageDetail.getImageId());
                            }
                        }

                    }
                }
            }
        }
    }


    private void deleteCollateralForPropertyValuation(ApplicationRequest applicationRequest) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setHeader(applicationRequest.getHeader());
        valuationRequest.setRefId(applicationRequest.getRefID());
        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        List<ValuationDetails> valuationDetailsList = new ArrayList<>();
        if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
            valuationDetailsList = valuation.getValuationDetailsList();
            Iterator<ValuationDetails> it = valuationDetailsList.iterator();
            while (it.hasNext()) {
                for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
                    if (StringUtils.equals(colletralObj.getCollateralId(), it.next().getCollateralId())) {
                        it.remove();
                    }
                }
            }
            valuation.setValuationDetailsList(valuationDetailsList);
            valuationRequest.setValuation(valuation);
            Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
            Map<String, Valuation> triggerToAgencies = new HashMap<>();
            applicationRepository.deleteValuationDetails(valuationRequest);
            //applicationRepository.saveValuationDetails(valuationRequest, verificationStatusDealersMap, triggerToAgencies);
            /*appConfigurationHelper.intimateUsers(valuationRequest.getValuation().getRefId(),
                    IntimationConstants.VERIFICATION_TRIGGER, IntimationConstants.VER_TYPE_VALUATION, verificationStatusDealersMap);
*/
        }
    }

    private void deleteCollateralForPropertyVerification(ApplicationRequest applicationRequest) {
        PropertyVisitRequest propertyVisitRequest = new PropertyVisitRequest();
        propertyVisitRequest.setHeader(applicationRequest.getHeader());
        propertyVisitRequest.setRefId(applicationRequest.getRefID());
        PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);
        List<PropertyVisitDetails> propertyVisitDetails = new ArrayList<>();
        if (propertyVisit != null && CollectionUtils.isNotEmpty(propertyVisit.getPropertyVisitDetailsList())) {
            propertyVisitDetails = propertyVisit.getPropertyVisitDetailsList();
            Iterator<PropertyVisitDetails> it = propertyVisitDetails.iterator();
            while (it.hasNext()) {
                for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
                    if (StringUtils.equals(colletralObj.getCollateralId(), it.next().getCollateralId())) {
                        it.remove();
                    }
                }
            }
            propertyVisit.setPropertyVisitDetailsList(propertyVisitDetails);
            propertyVisitRequest.setPropertyVisit(propertyVisit);
            applicationRepository.savePropertyVisitDetails(propertyVisitRequest);
        }
    }

    private void deleteCollateralForLegalVerificaion(ApplicationRequest applicationRequest) {
        LegalVerificationRequest legalVerificationRequest = new LegalVerificationRequest();
        legalVerificationRequest.setHeader(applicationRequest.getHeader());
        legalVerificationRequest.setRefId(applicationRequest.getRefID());
        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
        if (legalVerification != null && legalVerification.getLegalVerificationDetailsList() == null) {
            LegalVerificationDetails legalVerificatioDetailsByRefId = applicationRepository.fetchOldLegalVerificatioDetailsByRefId(legalVerificationRequest);
            if (legalVerificatioDetailsByRefId != null) {
                legalVerificatioDetailsByRefId.setCollateralId("0");
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                legalVerificationDetailsList.add(legalVerificatioDetailsByRefId);
                legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            }
        }
        List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
        if (legalVerification != null && CollectionUtils.isNotEmpty(legalVerification.getLegalVerificationDetailsList())) {
            legalVerificationDetailsList = legalVerification.getLegalVerificationDetailsList();
            Iterator<LegalVerificationDetails> it = legalVerificationDetailsList.iterator();
            while (it.hasNext()) {
                for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
                    if (StringUtils.equals(colletralObj.getCollateralId(), it.next().getCollateralId())) {
                        it.remove();
                    }
                }
            }
            legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            legalVerificationRequest.setLegalVerification(legalVerification);
            applicationRepository.updateLegalVerificationMultiDetails(legalVerificationRequest);
        }
    }

    @Override
    public BaseResponse deleteCoApplicant(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse response = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(), applicationRequest.getRefID(), httpRequest,
                applicationRequest.getCurrentStageId(), EndPointReferrer.DELETE_COLLATERAL,
                goNoGoCustomerApplication.getApplicationStatus(), applicationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : deleteCoApplicant for refId {}", applicationRequest.getRefID());
            deleteCoApplicantForRVOV(applicationRequest);
            GenericMsgResponse genericMsgResponse = GenericMsgResponse.builder()
                    .status(GNGWorkflowConstant.SUCCESS.name())
                    .message("Success")
                    .build();
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse deleteCoApplicantData(DeleteCoApplicantRequest request, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        logger.debug("{} : deleteCoApplicant for refId {}", request.getRefId());
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId
                    (request.getRefId());
            ActivityLogs activityLogs = auditHelper.createActivityLog(request.getHeader(), request.getRefId(), httpRequest,
                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(), EndPointReferrer.DELETE_COLLATERAL,
                    goNoGoCustomerApplication.getApplicationStatus(), request.getHeader().getLoggedInUserId());
            // Delete RV OV Details
            deleteCoApplicantDataForRVOV(request);
            // Delete RTRDetails
            deleteCoApplicantDataForRTR(request);
            //Delete Images from rv/ov
            deleteImages(request);
            // Delete Applicant from MIFIN( for Ambit)
            if(Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),Institute.AMBIT)
                    && StringUtils.equals(Product.BL.toProductName(), request.getHeader().getProduct().toProductName())
                    && null != goNoGoCustomerApplication && null != goNoGoCustomerApplication.getApplicationRequest()){
                externalApiManager.callToMiFinDeleteApplicant(goNoGoCustomerApplication.getApplicationRequest(), request.getCoApplicantIds(),
                        request.getHeader().getLoggedInUserRole(), request.getHeader().getLoggedInUserId());
            }
            //Delete Co applicants from case details
            deleteCoApplicantsFromApplication(goNoGoCustomerApplication, request);
            //delete InsuranceData
            deleteInsuranceData(request);

            applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            activityEventPublisher.publishEvent(activityLogs);
            GenericMsgResponse genericMsgResponse = GenericMsgResponse.builder()
                    .status(GNGWorkflowConstant.SUCCESS.name())
                    .message("Success")
                    .build();
            response = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder().message(e.getMessage()).errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue()).level(Error.SEVERITY.MEDIUM.name()).build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        return response;
    }

    private void deleteInsuranceData(DeleteCoApplicantRequest request) {
        try{
            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(request.getRefId());
            Iterator itr = null;
            if(insuranceData != null && CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
                Applicant applicant = null;
                for(String deletedId : request.getCoApplicantIds()){
                     itr = insuranceData.getApplicantList().iterator();
                    if(itr != null){
                        while(itr.hasNext()) {
                            applicant = (Applicant)itr.next();
                            if( StringUtils.equalsIgnoreCase(applicant.getApplicantId(), deletedId) ){
                               itr.remove();
                            }
                        }
                    }
                }
                InsuranceRequest insuranceRequest = new InsuranceRequest();
                insuranceRequest.setRefId(request.getRefId());
                insuranceRequest.setHeader(request.getHeader());
                insuranceRequest.setInsuranceData(insuranceData);
                applicationRepository.saveInsuranceData(insuranceRequest);
            }
        }catch (Exception ex){
            logger.error("{} refId  Exception {}",request.getRefId(), ExceptionUtils.getStackTrace(ex));
        }
    }


    private void deleteCoApplicantsFromApplication(GoNoGoCustomerApplication goNoGoCustomerApplication, DeleteCoApplicantRequest request) {
        List<String> coApplicantIdsToDelete = request.getCoApplicantIds();
        List<CoApplicant> coApplicants = new ArrayList<>();
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
            applicationRequest.getRequest().getCoApplicant().forEach(obj -> {
                if (!coApplicantIdsToDelete.contains(obj.getApplicantId()))
                    coApplicants.add(obj);
            });
            goNoGoCustomerApplication.getApplicationRequest().getRequest().setCoApplicant(coApplicants);
        }
    }

    private void deleteCoApplicantDataForRVOV(DeleteCoApplicantRequest request) {
        List<String> coApplicantIdsToDelete = request.getCoApplicantIds();
        VerificationRequest verificationRequest = new VerificationRequest();
        verificationRequest.setRefId(request.getRefId());
        verificationRequest.setHeader(request.getHeader());
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);
        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
        if (verificationDetails != null) {
            List<PropertyVerification> residenceVerification = verificationDetails.getResidenceVerification();
            if (CollectionUtils.isNotEmpty(residenceVerification)) {
                List<PropertyVerification> tempList = new ArrayList<>();
                residenceVerification.forEach(obj -> {
                    if (!coApplicantIdsToDelete.contains(obj.getApplicantId()))
                        tempList.add(obj);
                });
                verificationDetails.setResidenceVerification(tempList);
                verificationRequest.setVerificationDetails(verificationDetails);
                /*applicationRepository.saveVerificationDetails(verificationRequest, EndPointReferrer.RESIDENCE_VERIFICATION,
                        verificationStatusDealersMap);*/
                applicationRepository.deleteVerificationDetails(verificationRequest, EndPointReferrer.RESIDENCE_VERIFICATION);
            }
            List<PropertyVerification> officeVerification = verificationDetails.getOfficeVerification();
            if (CollectionUtils.isNotEmpty(officeVerification)) {
                List<PropertyVerification> tempList = new ArrayList<>();
                officeVerification.forEach(obj -> {
                    if (!coApplicantIdsToDelete.contains(obj.getApplicantId()))
                        tempList.add(obj);
                });
                verificationDetails.setOfficeVerification(tempList);
                verificationRequest.setVerificationDetails(verificationDetails);
                applicationRepository.deleteVerificationDetails(verificationRequest, EndPointReferrer.OFFICE_VERIFICATION);
            }
        }
    }

    private void deleteCoApplicantDataForRTR(DeleteCoApplicantRequest request){
        List<String> coApplicantIdsToDelete = request.getCoApplicantIds();
        CamDetails camDetails = applicationRepository.fetchCamDetails(request.getRefId(), request.getHeader().getInstitutionId(), EndPointReferrer.CAM_RTR_DETAILS);
        if(null != camDetails && CollectionUtils.isNotEmpty(camDetails.getRtrDetailList())){
            List<RTRDetail> tempList = new ArrayList<>();
            camDetails.getRtrDetailList().forEach(obj -> {
                if(!coApplicantIdsToDelete.contains(obj.getApplicantId()))
                    tempList.add(obj);
            });
            camDetails.setRtrDetailList(tempList);

            CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
            camDetailsRequest.setRefId(request.getRefId());
            camDetailsRequest.setHeader(request.getHeader());
            camDetailsRequest.setCamDetails(camDetails);
            applicationRepository.saveCamDetails(camDetailsRequest, EndPointReferrer.CAM_RTR_DETAILS);
        }
    }

    private void deleteCoApplicantForRVOV(ApplicationRequest applicationRequest) {
        VerificationRequest verificationRequest = new VerificationRequest();
        verificationRequest.setRefId(applicationRequest.getRefID());
        verificationRequest.setHeader(applicationRequest.getHeader());
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);

        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());

        if (verificationDetails != null) {
            List<PropertyVerification> residenceVerification = verificationDetails.getResidenceVerification();
            if (CollectionUtils.isNotEmpty(residenceVerification)) {
                Iterator<PropertyVerification> it = residenceVerification.iterator();
                while (it.hasNext()) {
                    for (CoApplicant coApplicant : applicationRequest.getRequest().getCoApplicant()) {
                        if (StringUtils.equals(coApplicant.getApplicantId(), it.next().getApplicantId())) {
                            it.remove();
                        }
                    }
                }
                verificationDetails.setResidenceVerification(residenceVerification);
                verificationRequest.setVerificationDetails(verificationDetails);
                applicationRepository.saveVerificationDetails(verificationRequest, EndPointReferrer.RESIDENCE_VERIFICATION,
                        verificationStatusDealersMap);
            }
            List<PropertyVerification> officeVerification = verificationDetails.getOfficeVerification();
            if (CollectionUtils.isNotEmpty(officeVerification)) {
                Iterator<PropertyVerification> it = officeVerification.iterator();
                while (it.hasNext()) {
                    for (CoApplicant coApplicant : applicationRequest.getRequest().getCoApplicant()) {
                        if (StringUtils.equals(coApplicant.getApplicantId(), it.next().getApplicantId())) {
                            it.remove();
                        }
                    }
                }
                verificationDetails.setOfficeVerification(officeVerification);
                verificationRequest.setVerificationDetails(verificationDetails);
                applicationRepository.saveVerificationDetails(verificationRequest, EndPointReferrer.OFFICE_VERIFICATION,
                        verificationStatusDealersMap);
            }
        }
    }

    private void migrateOldDataProfitAndLoss(List<PLAndBSAnalysis> plAndBSAnalysisList) {
        for (PLAndBSAnalysis objPLAndBSAnalysis : plAndBSAnalysisList) {
            //setting year String list for old data of profit and loss according to new structure
            if (objPLAndBSAnalysis.getProfitAndLoss().getYearsList() == null) {
                List<String> yearList = new ArrayList<>();
                yearList.add(objPLAndBSAnalysis.getProfitAndLoss().getCurrentYear());
                yearList.add(objPLAndBSAnalysis.getProfitAndLoss().getPreviousYear());
                objPLAndBSAnalysis.getProfitAndLoss().setYearsList(yearList);
            }
            if (objPLAndBSAnalysis.getProfitAndLoss() != null) {
                // Setting years with their values of each field of StatementFieldDetails type in profitAndLoss class
                forOldDataCompatibilityOfProfitAndLoss(objPLAndBSAnalysis);
            }


        }
    }

    private void migrateOldDataRatio(List<PLAndBSAnalysis> plAndBSAnalysisList) {
        if (CollectionUtils.isNotEmpty(plAndBSAnalysisList)) {
            for (PLAndBSAnalysis objPLAndBSAnalysis : plAndBSAnalysisList) {
                List<Ratios> ratioList = objPLAndBSAnalysis.getRatios();
                if (CollectionUtils.isNotEmpty(ratioList)) {
                    for (Ratios ratios : ratioList) {
                        List<RatioFieldDetail> ratioFi = ratios.getRatioFieldDetails();
                        if (CollectionUtils.isNotEmpty(ratioFi)) {
                            for (RatioFieldDetail obj : ratioFi) {
                                if (obj.getYears() == null) {
                                    // for backward compatibility of profit and loss
                                    forOldDataCompatibilityOfRatio(obj);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void forOldDataCompatibilityOfRatio(RatioFieldDetail ratioFieldDetail) {
        try {

            if (ratioFieldDetail.getYears() == null) {
                List<YearValue> yearList = new ArrayList<>();
                YearValue tempObj = new YearValue();
                tempObj.setYear("2018");
                tempObj.setValue(ratioFieldDetail.getCurrentYear());
                yearList.add(tempObj);
                tempObj = new YearValue();
                tempObj.setYear("2017");
                tempObj.setValue(ratioFieldDetail.getPreviousYear());
                yearList.add(tempObj);
                ratioFieldDetail.setYears(yearList);
            }
        } catch (Exception ex) {
            ////e.printStackTrace();
        }
    }


    private void migrateOldDataBalenceSheet(List<PLAndBSAnalysis> plAndBSAnalysisList) {
        for (PLAndBSAnalysis objPLAndBSAnalysis : plAndBSAnalysisList) {

            //setting year String list for old data of profit and loss according to new structure
            if (objPLAndBSAnalysis.getBalanceSheet().getYearsList() == null) {
                List<String> yearList = new ArrayList<>();
                yearList.add(objPLAndBSAnalysis.getBalanceSheet().getCurrentYear());
                yearList.add(objPLAndBSAnalysis.getBalanceSheet().getPreviousYear());
                objPLAndBSAnalysis.getBalanceSheet().setYearsList(yearList);
            }
            // Setting years with their values of each field of StatementFieldDetails type in BalenceSheet class
            if (objPLAndBSAnalysis.getBalanceSheet() != null) {
                forOldDataCompatibilityOfBalenceSheet(objPLAndBSAnalysis);
            }
        }
    }

    private void forOldDataCompatibilityOfBalenceSheet(PLAndBSAnalysis objPLAndBSAnalysis) {
        try {
            StatementFieldDetails stmtDetls;
            BalanceSheet balanceSheet = objPLAndBSAnalysis.getBalanceSheet();
            Field[] fields = BalanceSheet.class.getDeclaredFields();
            for (Field f : fields) {
                if (f.getType().equals(StatementFieldDetails.class)) {
                    //for accessing private fields
                    f.setAccessible(true);
                    stmtDetls = (StatementFieldDetails) f.get(balanceSheet);
                    //checking if yearlist is null i.e migrating old data
                    if (stmtDetls.getYears() == null) {
                        List<YearValue> yearList = new ArrayList<>();
                        YearValue tempObj = new YearValue();
                        tempObj.setYear(objPLAndBSAnalysis.getBalanceSheet().getCurrentYear());
                        tempObj.setValue(stmtDetls.getCurrentYear());
                        yearList.add(tempObj);
                        tempObj = new YearValue();
                        tempObj.setYear(objPLAndBSAnalysis.getBalanceSheet().getPreviousYear());
                        tempObj.setValue(stmtDetls.getPreviousYear());
                        yearList.add(tempObj);
                        stmtDetls.setYears(yearList);
                    }
                    f.setAccessible(false);
                }
            }

        } catch (Exception ex) {
            ////e.printStackTrace();
        }

    }

    private void forOldDataCompatibilityOfProfitAndLoss(PLAndBSAnalysis objPLAndBSAnalysis) {
        try {
            StatementFieldDetails stmtDetls;
            ProfitAndLoss profitAndLoss = objPLAndBSAnalysis.getProfitAndLoss();
            Field[] fields = ProfitAndLoss.class.getDeclaredFields();
            for (Field f : fields) {
                if (f.getType().equals(StatementFieldDetails.class)) {
                    //for accessing private fields
                    f.setAccessible(true);
                    stmtDetls = (StatementFieldDetails) f.get(profitAndLoss);
                    //checking if yearlist is null i.e migrating old data
                    if (stmtDetls.getYears() == null) {
                        List<YearValue> yearList = new ArrayList<>();
                        YearValue tempObj = new YearValue();
                        tempObj.setYear(objPLAndBSAnalysis.getProfitAndLoss().getCurrentYear());
                        tempObj.setValue(stmtDetls.getCurrentYear());
                        yearList.add(tempObj);
                        tempObj = new YearValue();
                        tempObj.setYear(objPLAndBSAnalysis.getProfitAndLoss().getPreviousYear());
                        tempObj.setValue(stmtDetls.getPreviousYear());
                        yearList.add(tempObj);
                        stmtDetls.setYears(yearList);
                    }
                    f.setAccessible(false);
                }
            }
        } catch (Exception ex) {
            ////e.printStackTrace();
        }
    }

    @Override
    public BaseResponse savePerfiosData(PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse baseResponse;
        boolean save = false;
        List<BankData> saveDataList = null;
        BankData saveData = null;
        long MaxLIMIT = 10;
        long cnt = 0;
        List<BankData> countList = new ArrayList<BankData>();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // Set Activity Log
        ActivityLogs activityLogs = auditHelper.createActivityLog(httpRequest, perfiosRequest.getHeader());
        activityLogs.setRefId(perfiosRequest.getRefId());
        activityLogs.setCustomMsg("Perfios ackId " + perfiosRequest.getAcknowledgeId());

        logger.debug("savePerfiosData method started for refId {}", perfiosRequest.getRefId());
        PerfiosData perfiosData = applicationRepository.fetchPerfiosData(perfiosRequest.getRefId(), perfiosRequest.getHeader().getInstitutionId());

        if (perfiosData == null) {
            perfiosData = new PerfiosData().builder().build();
            perfiosData.setRefId(perfiosRequest.getRefId());
            perfiosData.setInstitutionId(perfiosRequest.getHeader().getInstitutionId());
            perfiosData.setProduct(perfiosRequest.getHeader().getProduct());
        }
        if (perfiosData != null && CollectionUtils.isNotEmpty(perfiosData
                .getSaveDataList())) {
            cnt = perfiosData
                    .getSaveDataList()
                    .stream()
                    .filter(bankData ->
                            StringUtils.isNotEmpty(bankData.getAppId()) && StringUtils.equalsIgnoreCase(bankData.getAppId(), perfiosRequest.getAppId()))
                    .count();
            logger.debug("Already {} statements are uploaded for refId {}", cnt, perfiosRequest.getRefId());
        }
        if (CollectionUtils.isEmpty(perfiosData.getSaveDataList())) {
            saveDataList = new ArrayList<BankData>();
        } else {
            saveDataList = perfiosData.getSaveDataList();
            saveData = saveDataList.stream()
                    .filter(bankData -> StringUtils.equals(bankData.getAppId(), perfiosRequest.getAppId())
                            && StringUtils.equals(bankData.getAcknowledgeId(), perfiosRequest.getAcknowledgeId())
                            && StringUtils.equalsIgnoreCase(bankData.getApplIcantName(), perfiosRequest.getApplIcantName()))
                    .findAny().orElse(null);

        }
        if(null == saveData) {
            saveData = new BankData().builder().build();
            saveDataList.add(saveData);
        }

        saveData.setAppId(perfiosRequest.getAppId());
        saveData.setApplIcantName(perfiosRequest.getApplIcantName());
        saveData.setAcknowledgeId(perfiosRequest.getAcknowledgeId());
        saveData.setCallDate(new Date());
        saveData.setGenerateFileResponse(perfiosRequest.getData());
        saveData.setFromDate(perfiosRequest.getFromDate());
        saveData.setToDate(perfiosRequest.getToDate());

        perfiosData.setSaveDataList(saveDataList);
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(perfiosRequest.getRefId());
        boolean digiWebSite = false;
        if(perfiosRequest.getHeader().getProduct() == Product.DIGI_PL) {

            if(null != goNoGoCustomerApplication) {
                if(StringUtils.isNotEmpty(perfiosRequest.getStatement())
                        && StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getChannel(), "Online")) {
                    try {
                        Object res = JsonUtil.StringToObject(XML.toJSONObject(perfiosRequest.getStatement()).toString(), LinkedHashMap.class);
                        if (null != res) {
                            saveData.setData(res);
                            digiWebSite = true;
                            logger.debug("It is Online Case and statement is ready for upload for refId {}", perfiosRequest.getRefId());
                        }
                    } catch (Exception e) {
                        logger.debug("Exception occured while converting perfios xml to JSON. XML: {}", perfiosRequest.getStatement());
                        logger.error("Perfios XML Exception :", e.getStackTrace());
                    }
                }
                applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                        EndPointReferrer.SAVE_PERFIOS_DATA, perfiosRequest.getHeader().getLoggedInUserId(), perfiosRequest.getHeader().getLoggedInUserRole());
            }
        }

        try {
            if (cnt < MaxLIMIT) {
                save = applicationRepository.savePerfiosData(perfiosData);

                if(save && digiWebSite  && StringUtils.equals(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),Stages.Stage.DE.name()))
                    moduleManager.executeBre(applicationRepository.getGoNoGoCustomerApplicationByRefId(perfiosRequest.getRefId()),
                            EndPointReferrer.SAVE_PERFIOS_DATA, perfiosRequest.getHeader().getLoggedInUserRole(), perfiosRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);
            }
            if (save) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Success");
            } else {
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message("Limit Exceeded")
                        .level(Error.SEVERITY.LOW.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Error while saving perfios data " + e.getStackTrace());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        // Save activity log
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    @Override
    public BaseResponse getPerfiosData(PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws JsonProcessingException {
        BaseResponse baseResponse = null;
        PerfiosData perfiosData = applicationRepository.fetchPerfiosData(perfiosRequest.getRefId(), perfiosRequest.getHeader().getInstitutionId());
        if (perfiosData != null) {
            logger.info("Perfios call started for RefId : " + perfiosRequest.getRefId());
            List<BankData> saveDataList = perfiosData.getSaveDataList();
            boolean execBRE = false;
            if (CollectionUtils.isNotEmpty(saveDataList)) {
                boolean isDigiPl = StringUtils.equalsIgnoreCase(perfiosRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName());
                for (BankData saveData : saveDataList) {
                    if (saveData != null) {
                        boolean isValid = false;
                        if (saveData.getData() == null) {
                            isValid = true;
                        } else {
                            int statusCode = 0;
                            try{
                                statusCode = ((Double) ((LinkedHashMap) saveData.getData()).get("statusCode")).intValue();
                            }catch (Exception e){
                                try {
                                    statusCode = ((Integer) ((LinkedHashMap) saveData.getData()).get("statusCode"));
                                }catch(Exception ex){}
                            }

                            if (statusCode == 400) isValid = true;

                            if(isDigiPl && !isValid) {
                                try {
                                    if (statusCode == 404 ||
                                            StringUtils.equalsIgnoreCase(((String) ((LinkedHashMap) saveData.getData()).get("error")),
                                                    GNGWorkflowConstant.NO_PERFIOS_TRANSACTION_REGISTERD_FOR_REQ_ACK.toFaceValue())) {
                                        isValid = true;
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                        if (isValid) {
                            logger.info("Perfios called for AcknowledgementId : " + saveData.getAcknowledgeId());
                            RetriveFileData retriveData = new RetriveFileData().builder().build();
                            if (StringUtils.isNotEmpty(saveData.getAcknowledgeId())) {
                                retriveData.setAcknowledgeId(saveData.getAcknowledgeId());
                                retriveData.setReportType("json");
                                ThirdPartyExecutor thirdPartyExecutor = new ThirdPartyExecutor();
                                retriveData = thirdPartyExecutor.callPerfios(retriveData, perfiosRequest.getHeader(), perfiosRequest.getRefId());
                                if (retriveData != null) {
                                    saveData.setData(retriveData.getData());
                                    saveData.setResponseReciveDate(new Date());
                                    saveData.setStatus(retriveData.getStatus());
                                    if(StringUtils.equals(Constant.SUCCESS, retriveData.getStatus())) execBRE = true;
                                }
                            }
                        }
                    }
                }
                perfiosData.setSaveDataList(saveDataList);
                applicationRepository.savePerfiosData(perfiosData);
                try{
                    GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(perfiosRequest.getRefId());
                    if(isDigiPl && execBRE && StringUtils.equals(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),Stages.Stage.DE.name())){
                        moduleManager.executeBre(goNoGoCustomerApplication,
                                EndPointReferrer.GET_PERFIOS_DATA, perfiosRequest.getHeader().getLoggedInUserRole(), perfiosRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);
                    }
                }catch(Exception e){
                    logger.error("Exception Occured while executing BRE Rules for method getPerfiosData. Exception : {}"+ e.getStackTrace());
                }
            }
            logger.info("Perfios call ended for RefId : " + perfiosRequest.getRefId());
        }
        if (perfiosData != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, perfiosData);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getMifinLogData(PerfiosRequest mifinRequest, HttpServletRequest httpRequest) {
        BaseResponse baseResponse = null;
        MiFinCallLog miFinCallLog = externalAPILogRepository.fetchMiFinCallLog(mifinRequest.getRefId());
        if (miFinCallLog != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, miFinCallLog);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getCoApplicantCibilScore(String institutionId, List<String> refIds) {
        BaseResponse response = null;
        try {
            logger.debug("Cibil Score migration started for " + institutionId + ", Ids : " + refIds);
            long allRecordsCount = applicationRepository.fetchCountAllApplications(institutionId, refIds);
            List<String> updatedIds = new ArrayList<>();
            for (int skip = 0; skip < allRecordsCount; skip += 500) {
                List<GoNoGoCustomerApplication> goNoGoCustomerApplication = applicationRepository.fetchAllApplications(institutionId, refIds, skip);
                List<String> tempIds = migrateCibilDataForApplications(goNoGoCustomerApplication);
                if (CollectionUtils.isNotEmpty(tempIds)) {
                    updatedIds.addAll(tempIds);
                    logger.debug("Cibil score updated for applications IDs : " + tempIds);
                }
            }
            logger.debug("Cibil Score migration completed for " + institutionId + ", Ids : " + refIds);
            response = GngUtils.getBaseResponse(HttpStatus.OK, updatedIds);
        } catch (Exception e) {
            //e.printStackTrace();
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public List<String> migrateCibilDataForApplications(List<GoNoGoCustomerApplication> goNoGoCustomerApplication) {
        List<String> updatedIds = new ArrayList<>();
        for (GoNoGoCustomerApplication application : goNoGoCustomerApplication) {
            Map<String, CustomData> applicantCoApplicantData = (application.getApplicantCoApplicantData() != null &&
                    application.getApplicantCoApplicantData().isEmpty()) ? new HashMap<>() : application.getApplicantCoApplicantData();
            // if it is null
            if (application.getApplicantCoApplicantData() == null) {
                applicantCoApplicantData = new HashMap<>();
            }
            try {
                if (application.getApplicantComponentResponse() != null) {
                    Object obj = application.getApplicantComponentResponse().getMultiBureauJsonRespose().getMergedResponse();
                    String cibilScore = null;
                    if (obj != null && ((LinkedHashMap) obj).get("bureauFeeds") != null &&
                            CollectionUtils.isNotEmpty((List) (((LinkedHashMap) ((LinkedHashMap) obj).get("bureauFeeds")).get("cibilScore")))) {
                        cibilScore = (String) ((LinkedHashMap) ((List) ((LinkedHashMap) ((LinkedHashMap) obj).get("bureauFeeds"))
                                .get("cibilScore")).get(0)).get("score");
                    }
                    if (cibilScore != null) {
                        CustomData data = new CustomData();
                        data.setCibilScore(cibilScore);
                        applicantCoApplicantData.put("applicant", data);
                    }
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
            try {
                if (CollectionUtils.isNotEmpty(application.getApplicantComponentResponseList())) {
                    for (ComponentResponse compResp : application.getApplicantComponentResponseList()) {
                        Object obj = compResp.getMultiBureauJsonRespose().getMergedResponse();
                        String cibilScore = null;
                        if (obj != null && ((LinkedHashMap) obj).get("bureauFeeds") != null &&
                                CollectionUtils.isNotEmpty((List) (((LinkedHashMap) ((LinkedHashMap) obj).get("bureauFeeds")).get("cibilScore")))) {
                            Class<?> objClass = obj.getClass();
                            cibilScore = (String) ((LinkedHashMap) ((List) ((LinkedHashMap) ((LinkedHashMap) obj).get("bureauFeeds")).get("cibilScore")).get(0)).get("score");
                        }
                        if (cibilScore != null) {
                            CustomData data = new CustomData();
                            data.setCibilScore(cibilScore);
                            applicantCoApplicantData.put("coApplicant" + compResp.getApplicantId(), data);
                        }
                    }
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }

            if (applicantCoApplicantData != null && !applicantCoApplicantData.isEmpty()) {
                application.setApplicantCoApplicantData(applicantCoApplicantData);
                if (applicationRepository == null) {
                    applicationRepository = new ApplicationMongoRepository();
                }
                applicationRepository.updateApplicantCoApplicantData(application.getGngRefId(), applicantCoApplicantData);
                updatedIds.add(application.getGngRefId());
            }
        }
        return updatedIds;
    }

    @Override
    public BaseResponse migrateCoApplicantPLBSData(String institutionId, List<String> refIds) {
        BaseResponse response = null;
        try {
            logger.debug("PL/BS data migration started for " + institutionId + ", Ids : " + refIds);
            long recordCount = applicationRepository.fetchCountOfAllCamDetails(institutionId, refIds);
            List<String> updatedIds = new ArrayList<>();
            for (int skip = 0; skip < recordCount; skip += 1000) {
                List<CamDetails> camDetails = applicationRepository.fetchAllCamDetails(institutionId, refIds, skip);
                List<String> tempIds = migratePLBSDataForApplications(camDetails);
                if (CollectionUtils.isNotEmpty(tempIds)) {
                    updatedIds.addAll(tempIds);
                    logger.debug("PL/BS data updated for applications IDs : " + tempIds);
                }
            }
            logger.debug("PL/BS data migration completed for " + institutionId + ", Ids : " + refIds);
            response = GngUtils.getBaseResponse(HttpStatus.OK, updatedIds);
        } catch (Exception e) {
            //e.printStackTrace();
            response = GngUtils.getBaseResponse(HttpStatus.EXPECTATION_FAILED, GngUtils.getNoContentErrorList());
        }
        return response;
    }

    @Override
    public BaseResponse saveOriginationDetails(OriginationRequest originationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(originationRequest.getHeader(), originationRequest.getRefID(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_ORIGINATION,
                null, originationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving origination Details refId {}", originationRequest.getRefID());
            boolean save = applicationRepository.saveCoOriginationDetails(originationRequest);
            if (save) {
                // Add this step in completed-set of execution items
                Header header = originationRequest.getHeader();
                applicationRepository.updateCompletedInfo(originationRequest.getRefID(), header.getInstitutionId(),
                        EndPointReferrer.SAVE_ORIGINATION, header.getLoggedInUserId(), header.getLoggedInUserRole());

                CoOrigination coOriginationDetails = applicationRepository.fetchCoOriginationDetails(originationRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, coOriginationDetails);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse fetchOriginationDetails(OriginationRequest originationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(originationRequest.getHeader(), originationRequest.getRefID(), httpRequest,
                null, EndPointReferrer.GET_ORIGINATION,
                null, originationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        CoOrigination coOriginationDetails = applicationRepository.fetchCoOriginationDetails(originationRequest);
        if (coOriginationDetails != null && !coOriginationDetails.isPopulate()) {
           if(CollectionUtils.isNotEmpty(coOriginationDetails.getCoOriginationDetailsList())){
                try{
                    coOriginationDetails =  originationHelper.setOriginationChargesData(originationRequest, coOriginationDetails);
                }catch (Exception ex){
                    logger.error("{} refId for originationDetail {} ",originationRequest.getRefID(),ExceptionUtils.getStackTrace(ex));
                }
               response = GngUtils.getBaseResponse(HttpStatus.OK, coOriginationDetails);
           }
        } else if(coOriginationDetails != null){
            response = GngUtils.getBaseResponse(HttpStatus.OK, coOriginationDetails);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }


    public List<String> migratePLBSDataForApplications(List<CamDetails> camDetails) {
        List<String> updatedIds = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(camDetails)) {

            for (CamDetails camDetail : camDetails) {
                Map<String, CustomData> applicantCoApplicantData = new HashMap<>();
                if (CollectionUtils.isNotEmpty(camDetail.getPlAndBSAnalysis())) {
                    camDetail.getPlAndBSAnalysis().forEach(plbs -> {
                        CustomData data = null;
                        if (plbs.getProfitAndLoss() != null && plbs.getProfitAndLoss().getSales() != null) {
                            data = new CustomData();
                            data.setYearValue(plbs.getProfitAndLoss().getSales().getPreviousYear());
                            applicantCoApplicantData.put("applicant-" + plbs.getApplicantId() + "-2017", data);
                            data = new CustomData();
                            data.setYearValue(plbs.getProfitAndLoss().getSales().getCurrentYear());
                            applicantCoApplicantData.put("applicant-" + plbs.getApplicantId() + "-2018", data);

                            if (CollectionUtils.isNotEmpty(plbs.getProfitAndLoss().getSales().getYears())) {
                                for (YearValue yearValue : plbs.getProfitAndLoss().getSales().getYears()) {
                                    data = new CustomData();
                                    data.setYearValue(yearValue.getValue());
                                    applicantCoApplicantData.put("applicant-" + plbs.getApplicantId() + "-" + yearValue.getYear(), data);
                                }
                            }
                        }
                    });
                }
                if (applicantCoApplicantData != null && !applicantCoApplicantData.isEmpty()) {
                    camDetail.setApplicantCoApplicantData(applicantCoApplicantData);
                    applicationRepository.updateApplicantCoApplicantCamData(camDetail.getRefId(), applicantCoApplicantData);
                    updatedIds.add(camDetail.getRefId());
                }
            }
        }
        return updatedIds;
    }

    @Override
    public BaseResponse migrateApplicationBranches(DataMigrationRequest dataMigrationRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        boolean updateCases = dataMigrationRequest.isUpdateCases();
        String institutionId = dataMigrationRequest.getHeader().getInstitutionId();
        Date startDate = dataMigrationRequest.getStartDate();
        Date endDate = dataMigrationRequest.getEndDate();
        logger.debug("Migration function for application branch synch with UAM started for instId : {} for period {} to {}",
                institutionId, startDate, endDate);
        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationList = applicationRepository.fetchGoNoGoCustomerApplication(
                institutionId, startDate, endDate);
        String userId;
        Branch branch;
        Branch uamBranch;
        if (CollectionUtils.isNotEmpty(goNoGoCustomerApplicationList)) {
            logger.debug("Total " + goNoGoCustomerApplicationList.size() + " new punched cases found for period {} to {}",
                    startDate, endDate);
            List<LoginServiceResponse> usersResponse = appConfigurationManager.getUsersDetailsFromUAM(institutionId, httpRequest);
            Map<String, Branch> getFosIdHomeBranchMap = extractFosIdHomeBranchName(usersResponse);
            // Logic to fetch all cases which are not synch with uam
            Map<String, Branch> caseIdBranchToUpdate = new HashMap<>();
            for (GoNoGoCustomerApplication application : goNoGoCustomerApplicationList) {
                userId = application.getApplicationRequest().getHeader().getDsaId();
                if (StringUtils.isNotEmpty(userId) && getFosIdHomeBranchMap.containsKey(userId)) {
                    branch = application.getApplicationRequest().getAppMetaData().getBranchV2();
                    uamBranch = getFosIdHomeBranchMap.get(userId);
                    // inconstant may be id or name i.e both must be same as uam branch
                    if (!uamBranch.getBranchId().equals(branch.getBranchId()) || !StringUtils.equalsIgnoreCase(uamBranch.getBranchName(),
                            branch.getBranchName())) {
                        caseIdBranchToUpdate.put(application.getGngRefId(), uamBranch);
                    }
                }
            }
            logger.debug("Cases whose branch is not same as FOS branch in UAM are " + caseIdBranchToUpdate.keySet());
            if (updateCases && !caseIdBranchToUpdate.isEmpty()) {
                logger.debug("Application update activity started for institutionId : " + institutionId);
                caseIdBranchToUpdate.entrySet().forEach(entry -> {
                    applicationRepository.updateApplicationWithBranch(entry.getKey(), entry.getValue());
                    logger.debug("Application id : " + entry.getKey() + " updated successfully!");
                });
                logger.debug("Application update activity finfishes!");
            }
            response = GngUtils.getBaseResponse(HttpStatus.OK, caseIdBranchToUpdate.entrySet());
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No data found for request");
        }
        return response;
    }

    private Map<String, Branch> extractFosIdHomeBranchName(List<LoginServiceResponse> usersResponse) {
        Map<String, Branch> fosIdHomeBranchMap = new HashMap<>();
        usersResponse.stream().forEach(userData -> {
            if (CollectionUtils.isNotEmpty(userData.getRoles())) {
                String role = userData.getRoles().iterator().next();
                if (StringUtils.equalsIgnoreCase(role, Roles.Role.FOS.name()) &&
                        CollectionUtils.isNotEmpty(userData.getBranches())) {
                    fosIdHomeBranchMap.put(userData.getLoginId(), userData.getBranches().iterator().next());
                }
            }
        });
        return fosIdHomeBranchMap;
    }

    @Override
    public BaseResponse verifyStageSubmition(StageSubmitVerificationRequest submitVerificationRequest) {
        BaseResponse baseResponse = new BaseResponse();
        try{
            double amount = 0;//amountVerificationRequest.getUserMaxAmount();
            String refId = submitVerificationRequest.getRefId();
            String instId = submitVerificationRequest.getHeader().getInstitutionId();
            String action = submitVerificationRequest.getAction();
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            String applicationBucket = goNoGoCustomerApplication.getApplicationBucket();
            String role = submitVerificationRequest.getHeader().getLoggedInUserRole();
            String product = submitVerificationRequest.getHeader().getProduct().name();
            boolean isCredit = false;
            if(submitVerificationRequest.getHeader().getCredit() != null){
                isCredit = submitVerificationRequest.getHeader().getCredit();
            }

            List<Error> errorList = new ArrayList<>();
            Error error = null;
            boolean docDateVerified = false;

            docDateVerified = isDocDateVerified(goNoGoCustomerApplication, action,instId,product);
            boolean formNumberVerified = isFormNumberValidated(submitVerificationRequest, goNoGoCustomerApplication);

            //checking in only case of role is BOPS
            if (StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(), Roles.Role.BOPS.name())) {
                if (goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct() == Product.LAP) {
                    error = verifyLegal(submitVerificationRequest.getRefId(), submitVerificationRequest.getHeader());
                }

                if(goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct() == Product.DIGI_PL) {
                    if(Buckets.isVerificationBucket(applicationBucket)) {
                        Error error2 = verifyResidenceVerification(refId, submitVerificationRequest.getHeader());
                        if (error2 != null) {
                            errorList.add(error2);
                        }
                    }

                    //checking office is verify or not
                    if(StringUtils.equalsIgnoreCase(applicationBucket,Buckets.Bucket.B6.name())) {
                        Error error3 = verifyOfficeVerification(refId, submitVerificationRequest.getHeader());
                        if (error3 != null) {
                            errorList.add(error3);
                        }
                    }
                }else {
                    //checking residence is verify or not
                    Error error2 = verifyResidenceVerification(refId, submitVerificationRequest.getHeader());
                    if (error2 != null) {
                        errorList.add(error2);
                    }

                    //checking office is verify or not
                    Error error3 = verifyOfficeVerification(refId, submitVerificationRequest.getHeader());
                    if (error3 != null) {
                        errorList.add(error3);
                    }
                }
                if(Institute.isInstitute(submitVerificationRequest.getHeader().getInstitutionId(),Institute.SBFC)) {
                    Error error4 = verifyInsuranceData(refId, submitVerificationRequest.getHeader());
                    if (error4 != null) {
                        errorList.add(error4);
                    }
                }
            }
            if(Institute.isInstitute(submitVerificationRequest.getHeader().getInstitutionId(), Institute.AMBIT)
                    && Stages.isCreditStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())){
                Error error5 = verifyResidenceVerification(refId, submitVerificationRequest.getHeader());
                if (error5 != null) {
                    errorList.add(error5);
                }
                error5 = verifyOfficeVerification(refId, submitVerificationRequest.getHeader());
                if (error5 != null) {
                    errorList.add(error5);
                }
            }
            if (Institute.isInstitute(instId, Institute.SBFC) && StringUtils.equalsIgnoreCase(role, Roles.Role.BOPS.name())
                    && goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct() == Product.LAP) {
                Error error6 = verifyIMDCleared(goNoGoCustomerApplication);
                if (error6 != null) {
                    errorList.add(error6);
                }
            }
            if (error != null) {
                errorList.add(error);
            }
            if (!docDateVerified) {
                error = Error.builder()
                        .errorType(MsgConstants.BUSINESS_ERROR)
                        .message("Unable to submit, Document received date is mandatory!")
                        .build();
                errorList.add(error);
            }
            if (!formNumberVerified) {
                error = Error.builder()
                        .errorType(MsgConstants.BUSINESS_ERROR)
                        .message("Unable to submit, Form number is empty or not valid!")
                        .build();
                errorList.add(error);
            }

            if(Institute.isInstitute(instId, Institute.SBFC)){
                fieldValidationHelper(error,errorList,refId,instId,product,goNoGoCustomerApplication,role,isCredit);
            }

            if (CollectionUtils.isNotEmpty(errorList)) {
                logger.info("Errorlist size is{} for refId {}", errorList.size(), refId);
                logger.info(errorList.toString());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.PRECONDITION_FAILED, errorList);
            } else {
                GenericMsgResponse genericMsgResponse = GenericMsgResponse.builder()
                        .status(GNGWorkflowConstant.SUCCESS.name())
                        .message("Success")
                        .build();
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, genericMsgResponse);
            }
        }catch (Exception ex){
            logger.error("error in verifyStageSubmition {}", ex.getMessage());
            ex.printStackTrace();
            baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR, "error in verifyStageSubmition : "+ex.getMessage());
        }
        return baseResponse;
    }

    private void fieldValidationHelper(Error error,List<Error> errorList,String refId,String instId,String product,GoNoGoCustomerApplication gngApp,String loggedInUserRole,boolean isCredit){
        List<String> creditStages = new ArrayList<>();
        creditStages.add(Stages.Stage.CRDT.name());creditStages.add(Stages.Stage.CR_H.name());creditStages.add(Stages.Stage.APRV.name());
        creditStages.add(Stages.Stage.DCLN.name());creditStages.add(Stages.Stage.SUBJ_APRV.name());
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getCurrentStageId() != null){
                String currentStage = gngApp.getApplicationRequest().getCurrentStageId();
                if(Stages.isBopsStage(currentStage)){
                    error = customerCreditDocsIsPending(refId,instId,currentStage);
                    if(error != null){
                        errorList.add(error);
                    }
                }

                boolean stageAndRoleIsCPA = Stages.isCpaStage(currentStage) && Roles.isCpaRole(loggedInUserRole);
                boolean stageAndRoleIsCredit = creditStages.contains(currentStage) &&
                        (Roles.isCreditRole(loggedInUserRole) || StringUtils.equalsIgnoreCase(loggedInUserRole,Roles.Role.CREDIT.name()) || isCredit);

                if(stageAndRoleIsCPA || stageAndRoleIsCredit){
                    //address line length validation
                    List<Error> addressErrorList = addressValidationHelper(gngApp,refId,instId,product);
                    if(CollectionUtils.isNotEmpty(addressErrorList)){
                        errorList.addAll(addressErrorList);
                    }

                    //otp verification for applicant
                    error = otpIsVerified(gngApp);
                    if(error != null){
                        errorList.add(error);
                    }

                    //applicant age validation
                    List<Error> ageErrorList = applicantAgeValidation(gngApp);
                    if(CollectionUtils.isNotEmpty(ageErrorList)){
                        errorList.addAll(ageErrorList);
                    }

                    if(StringUtils.equalsIgnoreCase(product,Product.LAP.name())){
                        //mcp screen validations
                        List<Error> mcpScreenErrors = mcpScreenValidationHelper(gngApp);
                        if(CollectionUtils.isNotEmpty(mcpScreenErrors)){
                            errorList.addAll(mcpScreenErrors);
                        }
                    }
                }
            }
        }catch (Exception e){
            logger.error("error while fieldValidationHelper {}",e.getMessage());
            e.printStackTrace();
        }
    }

    private Error customerCreditDocsIsPending(String refId, String instId, String stage){
        Error error = null;
        boolean customerDocsIsPending = false;

        CustomerCreditDocs customerCreditDocs = applicationRepository.fetchCustomerCreditDocs(refId, instId);
        if(customerCreditDocs != null){
            if(customerCreditDocs.getCustomerCreditDetails() != null && CollectionUtils.isNotEmpty(customerCreditDocs.getCustomerCreditDetails())){
                for(CustomerCreditDetail customerCreditDetail : customerCreditDocs.getCustomerCreditDetails()){
                    if(StringUtils.equalsIgnoreCase(customerCreditDetail.getCreditAssoStatus(),"Pending")){
                        customerDocsIsPending = true;
                        break;
                    }
                }
            }
        }
        if(customerDocsIsPending){
            error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message("Case can't be submitted with document branch status pending at customer document screen!")
                    .build();
        }
        return error;
    }

    private List<Error> addressValidationHelper(GoNoGoCustomerApplication gngApp,String refId,String instId,String product){
        List<Error> errorList = new ArrayList<>();
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getRequest() != null){
                Request request = gngApp.getApplicationRequest().getRequest();
                if(request.getApplicant() != null){
                    Applicant applicant = request.getApplicant();
                    if(CollectionUtils.isNotEmpty(applicant.getAddress())){
                        List<CustomerAddress> addressList = applicant.getAddress();
                        List<Error> applicantAddressErrorList = addressLengthHelperForList(addressList,"Demographics screen!");
                        errorList.addAll(applicantAddressErrorList);
                    }
                }
                if(request.getCoApplicant() != null && CollectionUtils.isNotEmpty(request.getCoApplicant())){
                    List<CoApplicant> coApplicantList = request.getCoApplicant();
                    for(CoApplicant coApplicant:coApplicantList){
                        if(CollectionUtils.isNotEmpty(coApplicant.getAddress())){
                            List<CustomerAddress> addressList = coApplicant.getAddress();
                            List<Error> coApplicantAddressErrorList = addressLengthHelperForList(addressList,"Demographics screen!");
                            errorList.addAll(coApplicantAddressErrorList);
                        }
                    }
                }
                if(StringUtils.equalsIgnoreCase(product,Product.LAP.name()) && request.getApplication() != null){
                    if(CollectionUtils.isNotEmpty(request.getApplication().getCollateral())){
                        List<Collateral> collateralList = request.getApplication().getCollateral();
                        for(Collateral collateral : collateralList){
                            if(collateral.getAddress() != null){
                                CustomerAddress customerAddress = collateral.getAddress();
                                List<Error> collateralListError = addressLengthHelperForObject(customerAddress,"Collateral screen!",100);
                                errorList.addAll(collateralListError);
                            }
                        }
                    }
                }
            }
            if(StringUtils.equalsIgnoreCase(product,Product.LAP.name()) || StringUtils.equalsIgnoreCase(product,Product.BL.name())){
                VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, instId);
                if(verificationDetails != null){
                    if(CollectionUtils.isNotEmpty(verificationDetails.getReferenceDetails())){
                        for(ReferenceDetails referenceDetails : verificationDetails.getReferenceDetails()){
                            if(referenceDetails.getAddress() != null){
                                CustomerAddress customerAddress = referenceDetails.getAddress();
                                List<Error> referenceErrorList = addressLengthHelperForObject(customerAddress,"Reference Tab at Verification screen!",30);
                                errorList.addAll(referenceErrorList);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error while validating the address length {}", e.getMessage());
            e.printStackTrace();
        }
        return errorList;
    }

    private List<Error> addressLengthHelperForList(List<CustomerAddress> customerAddressList,String screenName){
        List<Error> errorList = new ArrayList<>();
        for(CustomerAddress customerAddress : customerAddressList){
            List<Error> addressErrorList = addressLengthHelperForObject(customerAddress,screenName,100);
            errorList.addAll(addressErrorList);
        }
        return errorList;
    }

    private List<Error> addressLengthHelperForObject(CustomerAddress customerAddress,String screenName,int length){
        List<Error> errorList = new ArrayList<>();
        Error error = null;
        String errForLine1 = "Please enter valid address for Address Line 1 at ";
        String errForLine2 = "Please enter valid address for Address Line 2 at ";

        String addressLine1 = customerAddress.getAddressLine1();
        String addressLine2 = customerAddress.getAddressLine2();
        if(addressLine1.length() == 0 || addressLine1.length()>length){
            error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message(errForLine1 + screenName)
                    .build();
            errorList.add(error);
        }
        if(addressLine2.length() == 0 || addressLine2.length()>length){
            error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message(errForLine2 + screenName)
                    .build();
            errorList.add(error);
        }
        return errorList;
    }

    private Error otpIsVerified(GoNoGoCustomerApplication gngApp){
        boolean otpIsVerified = true;
        Error error = null;
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null &&
                    gngApp.getApplicationRequest().getRequest() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplicant() != null) {
                Applicant applicant = gngApp.getApplicationRequest().getRequest().getApplicant();
                if(CollectionUtils.isNotEmpty(applicant.getPhone())){
                    Phone phone = applicant.getPhone().get(0);
                    if(phone != null){
                        if(!phone.isVerified()){
                            otpIsVerified = false;
                        }
                    }
                }else{
                    otpIsVerified = false;
                }
            }else{
                otpIsVerified = false;
            }
        }catch (Exception e){
            logger.error("Error while otpIsVerified {}",e.getMessage());
            e.printStackTrace();
        }
        if(!otpIsVerified){
            error = Error.builder()
                    .errorType(MsgConstants.BUSINESS_ERROR)
                    .message("Mobile OTP verification for applicant is mandatory at Demographic screen")
                    .build();
        }
        return error;
    }

    private List<Error> applicantAgeValidation(GoNoGoCustomerApplication gngApp){
        List<Error> errorList = new ArrayList<>();
        Error error = null;
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getRequest() != null) {
                if(gngApp.getApplicationRequest().getRequest().getApplicant() != null){
                    String applicantDOB = gngApp.getApplicationRequest().getRequest().getApplicant().getDateOfBirth();
                    if(StringUtils.isNotEmpty(applicantDOB)){
                        long age = calculateAgeHelper(applicantDOB);
                        if(age<18 || age>75){
                            error = Error.builder()
                                    .errorType(MsgConstants.BUSINESS_ERROR)
                                    .message("Applicant age must be between 18 to 75 years")
                                    .build();
                            errorList.add(error);
                        }
                    }
                }
                if(CollectionUtils.isNotEmpty(gngApp.getApplicationRequest().getRequest().getCoApplicant())){
                    int i=1;
                    for(CoApplicant coApplicant:gngApp.getApplicationRequest().getRequest().getCoApplicant()){
                        String coApplicantDOB = coApplicant.getDateOfBirth();
                        if(StringUtils.isNotEmpty(coApplicantDOB)){
                            long age = calculateAgeHelper(coApplicantDOB);
                            if(age<18 || age>75){
                                error = Error.builder()
                                        .errorType(MsgConstants.BUSINESS_ERROR)
                                        .message("CoApplicant "+i+" age must be between 18 to 75 years")
                                        .build();
                                errorList.add(error);
                            }
                        }
                        i++;
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error while applicantAgeValidation {}",e.getMessage());
            e.printStackTrace();
        }
        return errorList;
    }

    private long calculateAgeHelper(String dob) {
        int year = Integer.parseInt(dob.substring(4,8));
        int day = Integer.parseInt(dob.substring(0,2));
        int month = Integer.parseInt(dob.substring(2,4));
        LocalDate birthDate = LocalDate.of(year,month,day);
        LocalDate currentDate = LocalDate.now(ZoneId.systemDefault());
        return ChronoUnit.YEARS.between(birthDate,currentDate);
    }

    private List<Error> mcpScreenValidationHelper(GoNoGoCustomerApplication gngApp){
        List<Error> errorList = new ArrayList<>();
        Error error = null;
        try{
            if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getRequest() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplicant() != null &&
                    gngApp.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails() != null){
                String loanScheme = gngApp.getApplicationRequest().getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme();
                List<com.softcell.gonogo.model.core.LoanDetails> loanDetailsList = gngApp.getApplicationRequest().getRequest().getApplicant().getLoanDetails();
                if(CollectionUtils.isNotEmpty(loanDetailsList)){
                    com.softcell.gonogo.model.core.LoanDetails loanDetails = loanDetailsList.get(0);
                    if(loanDetails != null){
                        int mob = loanDetails.getMob();
                        String bankName = loanDetails.getRepaymentBankName();
                        double originalLoanAmount = loanDetails.getLoanAmt();
                        boolean checkIfCaseIsBT = StringUtils.containsIgnoreCase(loanScheme,"BT") ||
                                StringUtils.equalsIgnoreCase(loanScheme,"Top Up Parallel") || StringUtils.equalsIgnoreCase(loanScheme,"Top Up Gross");
                        if(checkIfCaseIsBT){
                            if(StringUtils.isEmpty(bankName)){
                                error = Error.builder()
                                        .errorType(MsgConstants.BUSINESS_ERROR)
                                        .message("Bank Name is mandatory MCP screen!")
                                        .build();
                                errorList.add(error);
                            }
                            if(mob<=0){
                                error = Error.builder()
                                        .errorType(MsgConstants.BUSINESS_ERROR)
                                        .message("MOB must be greater than 0 at MCP screen!")
                                        .build();
                                errorList.add(error);
                            }
                            if(originalLoanAmount<=0){
                                error = Error.builder()
                                        .errorType(MsgConstants.BUSINESS_ERROR)
                                        .message("Original Loan Amount must be greater than 0 at MCP screen!")
                                        .build();
                                errorList.add(error);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            logger.error("Error while mcpScreenValidationHelper {}",e.getMessage());
            e.printStackTrace();
        }
        return errorList;
    }

    private Error verifyInsuranceData(String refId, Header header) {
        List<Applicant> applicantList = new ArrayList<>();
        boolean flag = false;
        String appType = null;
        String msg = null;
        Error  error = null;
        Map<Boolean, String> resultWithMsg = new HashMap<>();
        InsuranceData insuranceData = applicationRepository.fetchInsuranceData(refId);
        ApplicationRequest applicationRequest = applicationRepository.getApplicationRequest(refId, header.getInstitutionId());
        if(applicationRequest.getRequest().getApplicant() != null){
            applicantList.add(applicationRequest.getRequest().getApplicant());
        }
        if(CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())){
            for( CoApplicant coApp: applicationRequest.getRequest().getCoApplicant()){
                applicantList.add(coApp);
            }
        }
        if (insuranceData != null) {
            if(CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
                for(Applicant applicant : insuranceData.getApplicantList()){
                    for(Applicant applicationApplicant : applicantList){
                        if(StringUtils.equalsIgnoreCase(applicant.getApplicantId(),applicationApplicant.getApplicantId())){
                            if(!applicant.isWaived() ){
                                if(CollectionUtils.isEmpty(applicant.getInsurancePolicyList())){
                                   if(ApplicantType.isIndividual(applicant.getApplicantType())){
                                       if(StringUtils.equalsIgnoreCase(applicant.getApplicantId(), "0")){
                                           flag = true ;
                                           appType = "applicant";
                                           msg = "Please hit insurance for  " +appType;
                                           resultWithMsg.put(flag, msg);
                                           break ;
                                       }else {
                                           flag =  moduleHelper.validateInsuranceCnt(insuranceData.getApplicantList());
                                           if(flag){
                                               msg = "Insurance is Mandatory for one of the Individual";
                                               resultWithMsg.put(flag, msg);
                                               break ;
                                           }
                                       }
                                   }
                                }else{
                                    flag = moduleHelper.calculatePremiumAmountCheck(applicant.getInsurancePolicyList()) ;
                                    if(flag){
                                        if(Integer.parseInt(applicant.getApplicantId()) == 0){
                                            appType = "applicant";
                                        }else{
                                            appType = "Co-Applicant "+(Integer.parseInt(applicant.getApplicantId()));
                                        }
                                        msg = "premium  Amount of Insurance is Zero for " +appType;
                                        resultWithMsg.put(flag, msg);
                                    }
                                }
                            }
                        }
                    }
                    if(flag){
                        break;
                    }
                }
            }
        }else{
            flag = true ;
            appType = "applicant";
            msg = "Please hit insurance for  " +appType;
            resultWithMsg.put(flag, msg);
        }
        if(MapUtils.isNotEmpty(resultWithMsg)){
            error = buildErrorList(resultWithMsg.get(flag));
        }
        return error;
    }

    private Error verifyIMDCleared(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        Error error = null;
        Boolean flag = false;
        List<InitialMoneyDeposit> imd2 = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getImd2();
        if (CollectionUtils.isNotEmpty(imd2)) {
            for (InitialMoneyDeposit imd : imd2) {
                if (imd.isCleared()) {
                    flag = true;
                    break;
                } else if (imd.isWaiverState()) {
                    flag = true;
                    break;
                }
            }
        }
        if (!flag) {
            error = buildErrorList("Unable to submit, IMD is not cleared!");
        }
        return error;
    }

    private boolean isFormNumberValidated(StageSubmitVerificationRequest stageSubmitVerificationRequest, GoNoGoCustomerApplication goNoGoCustomerApplication){
        boolean isValidFormNumber = true;
        Header header = stageSubmitVerificationRequest.getHeader();
        boolean isCredit = false;
        if (Institute.isInstitute(header.getInstitutionId(), Institute.SBFC)) {
            if (header.getCredit() != null) {
                isCredit = header.getCredit();
            }
        }

        if(Product.LAP == header.getProduct() && (StringUtils.equalsIgnoreCase(Roles.Role.CREDIT.name(),header.getLoggedInUserRole())
                || Roles.isCreditRole(header.getLoggedInUserRole()) || isCredit)){
            if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().isCoOrigination()){
                if(StringUtils.isEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getIciciFormNumber())){
                    isValidFormNumber = false;
                }else if(StringUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getIciciFormNumber())
                        && goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getIciciFormNumber().length() != 10){
                    isValidFormNumber = false;
                }
            }
        }
        return isValidFormNumber;
    }

    private boolean isDocDateVerified(GoNoGoCustomerApplication goNoGoCustomerApplication, String action, String institutionId, String product) {
        boolean docDateVerified = true;
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        Application application = applicationRequest.getRequest().getApplication();

        //for SBFC DIGI_PL, document receive date is not mandatory
        if(Institute.isInstitute(institutionId,Institute.SBFC) && StringUtils.equalsIgnoreCase(product, Product.DIGI_PL.name())){
            return true;
        }

        String currentStage = applicationRequest.getCurrentStageId();
        if (StringUtils.equalsIgnoreCase(currentStage, Stages.Stage.DDE.name()) || Stages.isCreditStage(currentStage) ||
                (StringUtils.isNotEmpty(action) && StringUtils.equalsIgnoreCase(action, Stages.Stage.APRV.name())) || Stages.isCpaStage(currentStage)) {
            if (application.getDocReceivedDate() == null) {
                docDateVerified = false;
            }
        }
        return docDateVerified;
    }

    public void deleteImages(DeleteCoApplicantRequest request) {
        try {
            logger.debug("in delete Images");
            // GoNoGoCroApplicationResponse gonogoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(request.getRefId(), request.getHeader().getInstitutionId());
            Set<String> refSet = new HashSet<>();
            refSet.add(request.getRefId());
            List<KycImageDetails> kycImageDetailsList = uploadFileRepository
                    .getKycImageDetails(refSet, request.getHeader().getInstitutionId(), Status.ALL.name());
            logger.debug("kycImageDetailsList size {}", kycImageDetailsList.size());

            List<String> Ids = request.getCoApplicantIds();
            if (CollectionUtils.isNotEmpty(kycImageDetailsList)) {
                logger.debug("Images found for refid {} , id {}", request.getRefId(), kycImageDetailsList.size());
                //For deleting rv/ov images using image types
                for (KycImageDetails kycImageDetails : kycImageDetailsList) {
                    if (kycImageDetails != null) {
                        List<ImagesDetails> imagesDetailsList = kycImageDetails.getImageMap();
                        for (ImagesDetails imageDetail : imagesDetailsList) {

                            for (String id : Ids) {
                                if (imageDetail != null && StringUtils.isNotEmpty(imageDetail.getImageType())
                                        && StringUtils.equalsIgnoreCase(imageDetail.getImageType(), "Residence Varification" + id)) {
                                    logger.debug("deteImage for {} and  imageId{}", imageDetail.getImageType(), imageDetail.getImageId());
                                    uploadFileRepository.deletedocument(imageDetail.getImageId());
                                }
                                if (imageDetail != null && StringUtils.isNotEmpty(imageDetail.getImageType())
                                        && StringUtils.equalsIgnoreCase(imageDetail.getImageType(), "Office Varification" + id)) {
                                    logger.debug("deteImage for {} and  imageId{}", imageDetail.getImageType(), imageDetail.getImageId());
                                    uploadFileRepository.deletedocument(imageDetail.getImageId());
                                }
                            }

                        }
                    }
                }
            }

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setmisFieldData(ApplicationRequest applicationRequest) {
        //required for mis releated mapping
        //In case of Branch Type NonSpoke  Branch Id and BranchName will set to hubBranchid and hubBranchName
        if (StringUtils.isEmpty(applicationRequest.getAppMetaData().getBranchV2().getHubBranchName())) {
            applicationRequest.getAppMetaData().getBranchV2().setHubBranchId(applicationRequest.getAppMetaData().getBranchV2().getBranchId());
            applicationRequest.getAppMetaData().getBranchV2().setHubBranchName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
        }
        if (StringUtils.equalsIgnoreCase("Spoke", applicationRequest.getAppMetaData().getBranchV2().getBranchType())) {
            applicationRequest.getAppMetaData().getBranchV2().setSpokeBranchId(applicationRequest.getAppMetaData().getBranchV2().getBranchId());
            applicationRequest.getAppMetaData().getBranchV2().setSpokeBranchName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
        }
    }
    @Override
    public BaseResponse calculatePremiumAmount(InsuranceRequest insuranceRequest) throws Exception {

        String refId = insuranceRequest.getRefId();
        String institutionId = insuranceRequest.getHeader().getInstitutionId();
        ApplicationRequest applicationRequest = null;
        Applicant applicant = null;
        String initiationPoint = insuranceRequest.getInsuranceData().getInitiationPoint();
        String changedAmount = null;
        String changedTenor = null;
        String product = insuranceRequest.getHeader().getProduct().name();

        GoNoGoCustomerApplication application = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
        applicationRequest = application.getApplicationRequest();

        if(CollectionUtils.isNotEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
            for(Applicant app : insuranceRequest.getInsuranceData().getApplicantList()){
                for(InsurancePolicy insurancePolicy : app.getInsurancePolicyList()){
                    if(!insurancePolicy.isException()){
                        Integer amount = (((Double) applicationRequest.getRequest().getApplication().getLoanAmount()).intValue());
                        changedAmount = Integer.valueOf(amount).toString();
                        changedTenor = applicationRequest.getRequest().getApplication().getTenorRequested();

                        EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(refId, institutionId);
                        if (eligibility != null && eligibility.getAprvLoan() != 0 ) {
                            amount = (((Double) eligibility.getAprvLoan()).intValue());
                            changedAmount = amount.toString();
                            changedTenor = String.valueOf(eligibility.getTenor());
                        }
                        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
                        if(loanCharges != null  && loanCharges.getLoanDetails()!= null && insuranceRequest.getHeader().getProduct() != Product.LAP){
                            if(loanCharges.getLoanDetails().getFinalDisbAmt() >0 && loanCharges.getTenureDetails().getDisbTenureMonths() >0){
                                changedAmount = String.valueOf(loanCharges.getLoanDetails().getFinalDisbAmt());
                                changedTenor = String.valueOf(loanCharges.getTenureDetails().getDisbTenureMonths());
                            }
                        }
                    }else {
                        changedAmount = insurancePolicy.getProposedAmount();
                        changedTenor = insurancePolicy.getTenorRequested();
                    }
                    insurancePolicy.setTenorRequested(changedTenor);
                    insurancePolicy.setProposedAmount(changedAmount);
                }
            }
        }

        if(CollectionUtils.isNotEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
            List<Applicant> applicantList= insuranceRequest.getInsuranceData().getApplicantList();
            for(Applicant  applicant1 : applicantList ){
                for(InsurancePolicy insurancePolicy : applicant1.getInsurancePolicyList()){
                    if(insurancePolicy.isPremiumHit()){
                        applicant = applicant1;
                        if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), IciciHelper.ICICI_COMPANY_NAME)){
                            try{
                                externalApiManager.calculatePremiumICICICall(
                                        refId, institutionId, insuranceRequest, initiationPoint, insurancePolicy.getProposedAmount(), applicant);
                                insurancePolicy.setPremiumHit(false);
                                if(insurancePolicy.getPremium() == null || insurancePolicy.getPremium() == 0){
                                    insurancePolicy.setMsg("Premium Amount is Zero");
                                }else{
                                    insurancePolicy.setMsg(MiFinHelper.STATUS_SUCCESS);
                                }
                            }catch(Exception ex){
                                logger.error("{} refId stackTrace {} while calculate Icici premium ", refId, ExceptionUtils.getStackTrace(ex));
                            }
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), ReligareHelper.RELIGARE_LOAN_PROTECTION)){
                            try{

                                religareHelper.calculateReligarePremium(institutionId,product,insurancePolicy.getProposedAmount(),
                                        insurancePolicy.getTenorRequested(), applicant, insurancePolicy);
                                insurancePolicy.setPremiumHit(false);
                            }catch(Exception ex){
                                logger.error("{} refId stacktrace while calculate religare premium {}" ,ExceptionUtils.getStackTrace(ex));
                            }
                        }
                        if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())){
                            insuranceRequest.getInsuranceData().getApplicantList().get(applicantList.indexOf(applicant)).setInsurancePolicyList(applicant.getInsurancePolicyList());
                        }
                    }
                }
            }
        }
        applicationRepository.saveInsuranceData(insuranceRequest);
        List<InsurancePolicy> policyList = null;
        if(applicant != null){
            policyList = applicant.getInsurancePolicyList();
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, policyList);
    }

    @Override
    public BaseResponse insuranceDataPush(String  refId, Header header){

        InsuranceData insuranceData = null;
        GoNoGoCustomerApplication application = null;
        String msg = null;
        try {
            application = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
        } catch (Exception e) {
            logger.error("insurance data push {}",ExceptionUtils.getStackTrace(e));
        }
        ApplicationRequest applicationRequest = application.getApplicationRequest();
        insuranceData = applicationRepository.fetchInsuranceData(refId);
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId
                , header.getInstitutionId());
        InsuranceRequest insuranceRequest = new InsuranceRequest();
        insuranceRequest.setRefId(refId);
        insuranceRequest.setHeader(header);
        insuranceRequest.setInsuranceData(insuranceData);
            insuranceRequest.getInsuranceData().setCurrentCnt(insuranceRequest.getInsuranceData().getCurrentCnt() + 1);
            applicationRepository.saveInsuranceData(insuranceRequest);
            logger.info("Insurance hit count" ,insuranceRequest.getInsuranceData().getCurrentCnt());
            externalApiManager.pushPolicy(insuranceRequest, loanCharges);
            msg = "You have hitted Insurance"+insuranceRequest.getInsuranceData().getCurrentCnt()+ "no of times";
        return GngUtils.getBaseResponse(HttpStatus.OK, msg);
    }

    public double setInsuranceValue(InsuranceData insuranceData) {
        double totalInsuranceAmt = 0 ;
        if(CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
            for(Applicant applicant : insuranceData.getApplicantList()){
                if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList())){
                    for(InsurancePolicy insurancePolicy : applicant.getInsurancePolicyList()){
                        if(insurancePolicy != null && insurancePolicy.getPremium() != null){
                            totalInsuranceAmt = totalInsuranceAmt + insurancePolicy.getPremium();
                        }
                    }
                }
            }
        }
        return totalInsuranceAmt;
    }

    public void setConsolidatedBankStatements(CamDetailsRequest camDetailsRequest){

        try {

            List<BankingStatement> bankingStatements = camDetailsRequest.getCamDetails().getBankingStatements();
            CamDetails camDetails = applicationRepository.fetchCamDetails(camDetailsRequest);
            if (null == camDetails) {
                camDetails = new CamDetails();
            }
            List<BankingStatement> bankingData = camDetails.getBankingStatements();

            BankingStatement bankingStatement1 = new BankingStatement();
            List<BankingStatement> bk = new ArrayList<>();

            double nonBusinessCreditTransctionsAmt = 0;
            double nonBusinessDebitTransctionsAmt = 0;
            double netBusinessCreditTransctionsAmt = 0;
            double netBusinessDebitTransctionsAmt = 0;
            double averageAbb = 0;
            double averageCreditSum = 0;
            int totalNoOfInwardTransaction = 0;
            int totalNoOfOutwardTransaction = 0;
            double percentOfInwardReturns = 0;
            double percentOfOutwardReturns = 0;
            double totalCashCredit = 0;
            double totalCredit = 0;
            double totalGST = 0;
            double totalIntColl = 0;
            double totalOtherCredit = 0;
            double totalCCUtilisation = 0;
            int ccodLimit = 0;

            if(CollectionUtils.isNotEmpty(bankingStatements)) {
                for (BankingStatement bankingStatement : bankingStatements) {
                    if (StringUtils.equalsIgnoreCase(bankingStatement.getAccountType(), Constant.CURRENT) && !bankingStatement.isConsolidated()) {

                        nonBusinessCreditTransctionsAmt = bankingStatement.getNonBusinessCreditTransctionsAmt() + nonBusinessCreditTransctionsAmt;
                        nonBusinessDebitTransctionsAmt = bankingStatement.getNonBusinessDebitTransctionsAmt() + nonBusinessDebitTransctionsAmt;
                        netBusinessCreditTransctionsAmt = bankingStatement.getNetBusinessCreditTransctionsAmt() + netBusinessCreditTransctionsAmt;
                        netBusinessDebitTransctionsAmt = bankingStatement.getNetBusinessDebitTransctionsAmt() + netBusinessDebitTransctionsAmt;
                        averageAbb = bankingStatement.getAverageAbb() + averageAbb;
                        averageCreditSum = bankingStatement.getAverageCreditSum() + averageCreditSum;
                        totalNoOfInwardTransaction = bankingStatement.getTotalNoOfInwardTransaction() + totalNoOfInwardTransaction;
                        totalNoOfOutwardTransaction = bankingStatement.getTotalNoOfOutwardTransaction() + totalNoOfOutwardTransaction;
                        percentOfInwardReturns = bankingStatement.getPercentOfInwardReturns() + percentOfInwardReturns;
                        percentOfOutwardReturns = bankingStatement.getPercentOfOutwardReturns() + percentOfOutwardReturns;
                        totalCashCredit = bankingStatement.getTotalCashCredit() + totalCashCredit;
                        totalCredit = bankingStatement.getTotalCredit() + totalCredit;
                        totalGST = bankingStatement.getTotalGST() + totalGST;
                        totalIntColl = bankingStatement.getTotalIntColl() + totalIntColl;
                        totalOtherCredit = bankingStatement.getTotalOtherCredit() + totalOtherCredit;
                        totalCCUtilisation = bankingStatement.getTotalCCUtilisation() + totalCCUtilisation;
                        ccodLimit = bankingStatement.getCcodLimit() + ccodLimit;
                    }
                }
            }

            bankingStatement1.setNonBusinessCreditTransctionsAmt(nonBusinessCreditTransctionsAmt);
            bankingStatement1.setNonBusinessDebitTransctionsAmt(nonBusinessDebitTransctionsAmt);
            bankingStatement1.setNetBusinessDebitTransctionsAmt(netBusinessDebitTransctionsAmt);
            bankingStatement1.setNetBusinessCreditTransctionsAmt(netBusinessCreditTransctionsAmt);
            bankingStatement1.setAverageAbb(averageAbb);
            bankingStatement1.setAverageCreditSum(averageCreditSum);
            bankingStatement1.setTotalNoOfInwardTransaction(totalNoOfInwardTransaction);
            bankingStatement1.setTotalNoOfOutwardTransaction(totalNoOfOutwardTransaction);
            bankingStatement1.setPercentOfInwardReturns(percentOfInwardReturns);
            bankingStatement1.setPercentOfOutwardReturns(percentOfOutwardReturns);
            bankingStatement1.setTotalCashCredit(totalCashCredit);
            bankingStatement1.setTotalCredit(totalCredit);
            bankingStatement1.setTotalGST(totalGST);
            bankingStatement1.setTotalIntColl(totalIntColl);
            bankingStatement1.setTotalOtherCredit(totalOtherCredit);
            bankingStatement1.setTotalCCUtilisation(totalCCUtilisation);
            bankingStatement1.setCcodLimit(ccodLimit);

            if(CollectionUtils.isNotEmpty(bankingStatements)){
                BankingStatement bankingStatement = bankingStatements.get(0);
                if(bankingStatement.isConsolidated()) {
                    bankingStatement = bankingStatements.get(1);
                }
                if (bankingStatement.getAccountType().equalsIgnoreCase("Current")) {
                    List<BankingTransaction> bankingTransactions = bankingStatement.getBankingTransactions();
                    List<BankingTransaction> transactions = new ArrayList<>();

                    for (BankingTransaction bankingTransaction : bankingTransactions) {
                        BankingTransaction bankingTransaction1 = new BankingTransaction();
                        double abbAmount = 0;
                        double interestDebited = 0;
                        double chqAndEmiReturnsInward = 0;
                        double chqAndEmiReturnsOutward = 0;
                        double gst = 0;
                        double intColl = 0;
                        double cashCredit = 0;
                        double otherCredit = 0;
                        double creditTotal = 0;
                        double ccUtilisation = 0;
                        List<Double> businessDebitList = new ArrayList<>();
                        List<Double> businessCreditList = new ArrayList<>();

                        List<AmountForDate> amountForDateList = bankingTransaction.getInMonthBalance().stream()
                                .map(obj -> {
                                    AmountForDate amountForDateData = new AmountForDate();
                                    amountForDateData.setMonthDate(obj.getMonthDate());
                                    amountForDateData.setBalanceAmt(0);
                                    return amountForDateData;
                                }).collect(Collectors.toList());


                        if (CollectionUtils.isNotEmpty(bankingTransaction.getBusinessDebit())) {
                            businessDebitList = bankingTransaction.getBusinessDebit().stream()
                                    .map(obj -> {
                                        return new Double(0.0);
                                    }).collect(Collectors.toList());
                        }

                        if (CollectionUtils.isNotEmpty(bankingTransaction.getBusinessCredit())) {
                            businessCreditList = bankingTransaction.getBusinessCredit().stream()
                                    .map(obj -> {
                                        return new Double(0.0);
                                    }).collect(Collectors.toList());
                        }

                        int i = bankingTransaction.getDate().indexOf("-");
                        if (i != -1) {
                            String Bankingdate = bankingTransaction.getDate().substring(0, i);
                            for (BankingStatement bankingStatementt : bankingStatements) {
                                if (StringUtils.equalsIgnoreCase(bankingStatementt.getAccountType(), Constant.CURRENT) && !bankingStatementt.isConsolidated()) {
                                    List<BankingTransaction> data = bankingStatementt.getBankingTransactions();
                                    for (BankingTransaction transaction : data) {
                                        int j = transaction.getDate().indexOf("-");
                                        if (j != -1) {
                                            List<AmountForDate> inMonthBalance = transaction.getInMonthBalance();
                                            List<Double> businessCredit = transaction.getBusinessCredit();
                                            List<Double> businessDebit = transaction.getBusinessDebit();
                                            double debitAmount = 0;
                                            double creditAmount = 0;
                                            String transactionDate = transaction.getDate().substring(0, j);
                                            if (Bankingdate.equalsIgnoreCase(transactionDate)
                                                    && bankingTransaction.getDate().substring(i + 1, bankingTransaction.getDate().length()).equalsIgnoreCase(transaction.getDate().substring(j + 1, transaction.getDate().length()))) {
                                                for (int k = 0; k < inMonthBalance.size(); k++) {
                                                    double amount = amountForDateList.get(k).getBalanceAmt();
                                                    amount = inMonthBalance.get(k).getBalanceAmt() + amount;
                                                    amountForDateList.get(k).setBalanceAmt(amount);
                                                }
                                                if (CollectionUtils.isNotEmpty(businessDebit)) {
                                                    for (int n = 0; n < businessDebit.size(); n++) {
                                                        if (CollectionUtils.isNotEmpty(businessDebitList) && n < businessDebitList.size()) {
                                                            debitAmount = businessDebitList.get(n);
                                                        }
                                                        debitAmount = businessDebit.get(n) + debitAmount;
                                                        if (n >= businessDebitList.size()) {
                                                            businessDebitList.add(debitAmount);
                                                        } else {
                                                            businessDebitList.set(n, debitAmount);
                                                        }
                                                    }
                                                }
                                                if (CollectionUtils.isNotEmpty(businessCredit)) {
                                                    for (int m = 0; m < businessCredit.size(); m++) {
                                                        if (CollectionUtils.isNotEmpty(businessCreditList) && m < businessCreditList.size()) {
                                                            creditAmount = businessCreditList.get(m);
                                                        }
                                                        creditAmount = businessCredit.get(m) + creditAmount;
                                                        if (m >= businessCreditList.size()) {
                                                            businessCreditList.add(creditAmount);
                                                        } else {
                                                            businessCreditList.set(m, creditAmount);
                                                        }
                                                    }
                                                }
                                                abbAmount = transaction.getAbbAmount() + abbAmount;
                                                interestDebited = transaction.getInterestDebited() + interestDebited;
                                                chqAndEmiReturnsInward = transaction.getChqAndEmiReturnsInward() + chqAndEmiReturnsInward;
                                                chqAndEmiReturnsOutward = transaction.getChqAndEmiReturnsOutward() + chqAndEmiReturnsOutward;
                                                gst = transaction.getGst() + gst;
                                                intColl = transaction.getIntColl() + intColl;
                                                cashCredit = transaction.getCashCredit() + cashCredit;
                                                otherCredit = transaction.getOtherCredit() + otherCredit;
                                                creditTotal = transaction.getCreditTotal() + creditTotal;
                                                ccUtilisation = transaction.getCcUtilisation() + ccUtilisation;
                                            }
                                        }
                                    }
                                }
                            }
                            bankingTransaction1.setDate(bankingTransaction.getDate());
                            bankingTransaction1.setInMonthBalance(amountForDateList);
                            bankingTransaction1.setAbbAmount(abbAmount);
                            bankingTransaction1.setBusinessCredit(businessCreditList);
                            bankingTransaction1.setBusinessDebit(businessDebitList);
                            bankingTransaction1.setInterestDebited(interestDebited);
                            bankingTransaction1.setChqAndEmiReturnsOutward(chqAndEmiReturnsOutward);
                            bankingTransaction1.setChqAndEmiReturnsInward(chqAndEmiReturnsInward);
                            bankingTransaction1.setGst(gst);
                            bankingTransaction1.setIntColl(intColl);
                            bankingTransaction1.setCashCredit(cashCredit);
                            bankingTransaction1.setOtherCredit(otherCredit);
                            bankingTransaction1.setCreditTotal(creditTotal);
                            bankingTransaction1.setCcUtilisation(ccUtilisation);

                            transactions.add(bankingTransaction1);
                        }
                    }
                    int k = 1;
                    Collections.sort(transactions, BankingTransaction.AbbComparator);
                    for (BankingTransaction transaction : transactions) {
                        transaction.setRank(k);
                        k++;
                    }
                    bankingStatement1.setBankingTransactions(transactions);
                    bankingStatement1.setAccountType(bankingStatement.getAccountType());
                    bankingStatement1.setApplicantId(bankingStatement.getApplicantId());
                    bankingStatement1.setApplicantName(bankingStatement.getApplicantName());
                    bankingStatement1.setConsolidated(true);
                }

                for (BankingStatement data : bankingStatements) {
                    if (!data.isConsolidated()) {
                        bk.add(data);
                    }
                }
                List<BankingStatement> bankStatements = bankingData.stream().filter(dbBankData -> (
                        dbBankData.isConsolidated())).collect(Collectors.toList());
                bankStatements.clear();
                if(StringUtils.equalsIgnoreCase(bankingStatement.getAccountType(), Constant.CURRENT)) {
                     bankStatements.add(bankingStatement1);
                }
                bankStatements.addAll(bk);

                camDetailsRequest.getCamDetails().setBankingStatements(bankStatements);
            }
        }catch(Exception e){
            logger.error("Exception occurred while adding bankStatements data {} for refId {} ",
                    camDetailsRequest.getRefId() , ExceptionUtils.getStackTrace(e));
        }
    }

    public void responseToEligibility(GoNoGoCustomerApplication goNoGoCustomerApplication,EligibilityDetails eligibilityData,String role) {

        Object derivedFieldsObject = null;
        double eligibiltyResponseObject = 0;
        double maxTenor = 0;
        try{
            if (goNoGoCustomerApplication != null && goNoGoCustomerApplication.getApplicantComponentResponse() != null
                    && goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse() != null) {
                ScoringApplicantResponse scoringApplicantResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse().getApplicantResponse();
                if (scoringApplicantResponse != null && CollectionUtils.isNotEmpty(scoringApplicantResponse.getApplicantResult())) {
                    derivedFieldsObject = scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields();
                    eligibiltyResponseObject = scoringApplicantResponse.getApplicantResult().get(0).getEligibilityResponse().getEligibilityAmount();
                    maxTenor = scoringApplicantResponse.getApplicantResult().get(0).getEligibilityResponse().getMaxTenor();

                    if (Institute.isInstitute(eligibilityData.getInstitutionId(),Institute.AMBIT)) {
                        eligibilityData.setAprvLoan(GngUtils.setValueZero(eligibiltyResponseObject));
                        if(maxTenor>0) {
                            eligibilityData.setTenor((int) (maxTenor));
                        }
                    }
                    if (derivedFieldsObject != null) {
                        HashMap<String, Double> eligibleLoanAmt = (HashMap<String, Double>) derivedFieldsObject;
                        HashMap<String, String> eligibleAmt = (HashMap<String, String>) derivedFieldsObject;
                        eligibilityData.setDerivedFields(derivedFieldsObject);
                        if (Institute.isInstitute(eligibilityData.getInstitutionId(),Institute.AMBIT)) {
                            if(eligibleLoanAmt.get("CALCULATED_FIELDS$GST_GROSS_PROFIT") != null) {
                                eligibilityData.setGrossIncome(GngUtils.setValueZero(eligibleLoanAmt.get("CALCULATED_FIELDS$GST_GROSS_PROFIT")));
                            }
                            if(eligibleLoanAmt.get("CUSTOM_FIELDS$INCOME_ROI") != null) {
                                if (String.class.isInstance(eligibleLoanAmt.get("CUSTOM_FIELDS$INCOME_ROI"))) {
                                    eligibilityData.setEligibilityRoi(Double.parseDouble(GngUtils.setValueForZero(eligibleAmt.get("CUSTOM_FIELDS$INCOME_ROI"))));
                                } else {
                                    eligibilityData.setEligibilityRoi((GngUtils.setValueZero(eligibleLoanAmt.get("CUSTOM_FIELDS$INCOME_ROI"))));
                                }
                            }
                        }
                    }
                }
            }
            applicationRepository.saveEligibilityDetails(eligibilityData);
        }catch(Exception e){
            logger.error("{ } refId, Exception occurred while saving eligibility details {}",
                    goNoGoCustomerApplication.getApplicationRequest().getRefID() , ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public BaseResponse populateRepaymentDetails(CamDetailsRequest camDetailsRequest){
        BaseResponse baseResponse = null;
        String refId = camDetailsRequest.getRefId();
        try{
            logger.debug("method populateRepayment started for { }", refId);
            if (StringUtils.isNotEmpty(refId) && camDetailsRequest.getCamDetails() != null) {
                GoNoGoCustomerApplication  goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                CamDetails camDetails = camDetailsRequest.getCamDetails();
                if(null != goNoGoCustomerApplication){

                    Repayment repayment = new Repayment();
                    List<RepaymentDetails> repaymentDetailsList = new ArrayList<>();
                    List<DMInput> dmInputList = new ArrayList<>();
                    List<BankingStatement> bankingDetails =  camDetails.getBankingStatements().stream()
                            .filter(dbBankData -> dbBankData.isRepayBank())
                            .collect(Collectors.toList());


                    bankingDetails.forEach(obj -> {
                        RepaymentDetails repaymentDetails = new RepaymentDetails();
                        DMInput dmInput = new DMInput();
                        dmInput.setDmBankName(obj.getBankName());
                        repaymentDetails.setIfscCode(obj.getIfscCode());
                        BankDetailsMaster bankingDetailsMaster = masterDataRepository.getBankMasterDetailsByIfscCode(camDetailsRequest.getHeader().getInstitutionId(),obj.getIfscCode());
                        if(bankingDetailsMaster != null) {
                            repaymentDetails.setBankName(bankingDetailsMaster.getBankName());
                            Address bankAddress = new Address();
                            bankAddress.setAddressLine1(bankingDetailsMaster.getBranch());
                            repaymentDetails.setBranchaddress(bankAddress);
                            repaymentDetails.setmICRCode(bankingDetailsMaster.getMicrCode());
                        }
                        repaymentDetails.setAccountNumber(obj.getAccountNumber());
                        Name name = new Name();
                        name.setFirstName(obj.getAccountHolder());
                        repaymentDetails.setAccountHolderName(name);
                        repaymentDetails.setAccountType(obj.getAccountType());
                        repaymentDetailsList.add(repaymentDetails);
                        dmInputList.add(dmInput);
                    });

                    RepaymentRequest repaymentRequest = new RepaymentRequest();
                    DMRequest dmRequest = new DMRequest();
                    DisbursementMemo disbursementMemo = new DisbursementMemo();
                    dmRequest.setRefId(refId);
                    dmRequest.setHeader(camDetailsRequest.getHeader());
                    disbursementMemo.setDmInputList(dmInputList);
                    dmRequest.setDisbursementMemo(disbursementMemo);
                    applicationRepository.saveDMDetails(dmRequest);
                    repaymentRequest.setRefId(refId);
                    repaymentRequest.setHeader(camDetailsRequest.getHeader());
                    repayment.setRepaymentDetailsList(repaymentDetailsList);
                    repayment.setRefId(repaymentRequest.getRefId());
                    repayment.setInstitutionId(repaymentRequest.getHeader().getInstitutionId());
                    repaymentRequest.setRepayment(repayment);
                    applicationRepository.saveRepaymentDetails(repayment);
                    Repayment repaymentData = applicationRepository.fetchRepaymentDetails(repaymentRequest);
                    if (repaymentData != null) {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, repaymentData);
                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                    }
                }else{
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                }
            }else{
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        }catch(Exception e){
            logger.error("{ } refId, Exception occurred while saving repayment Details. Exception {}", refId, ExceptionUtils.getStackTrace(e));
        }
        logger.debug("method populateRepayment ended for { }", refId);
        return baseResponse;
    }

    @Override
    public  BaseResponse saveLoginData(LoginDataRequest loginDataRequest,
                                       HttpServletRequest httpRequest)throws Exception {
        //This Method is used for case login with different steps like registration, eligibility, demographic details, personal details, employment details
        BaseResponse baseResponse = null;
        boolean stepMissed=false;
        String stepName = loginDataRequest.getStepName();
        logger.debug("saveLoginData() service called for step {}", stepName);

        ActivityLogs activityLogs = auditHelper.createActivityLog(loginDataRequest.getHeader(),
                loginDataRequest.getRefId(), null,
                null, EndPointReferrer.SAVE_LOGIN_DATA, null, loginDataRequest.getHeader().getLoggedInUserId());
        if (loginDataRequest.getRefId() != null) {
            activityLogs.setRefId(loginDataRequest.getRefId());
        }
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        String action = null;
        ApplicationRequest applicationRequest;
        LoginDataResponse loginDataResponse = new LoginDataResponse();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            Object versionError = validateVersion(loginDataRequest.getHeader(), httpRequest);
            if (versionError != null) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, versionError);
            }else{
                String refId = loginDataRequest.getRefId();
                Header header = loginDataRequest.getHeader();

                if (StringUtils.isNotEmpty(stepName)) {
                    switch (stepName) {
                        case EndPointReferrer.MCP_SAVE:
                        case EndPointReferrer.STEP_REGISTRATION:{
                            applicationRequest = mappingApplicationRequest(loginDataRequest);
                            if (null != applicationRequest) {

                                if(!StringUtils.equalsIgnoreCase(stepName, EndPointReferrer.MCP_SAVE)
                                        && !loginDataRequest.isExistingAppln()
                                        && dedupeFound(loginDataRequest, loginDataResponse, applicationRequest)){
                                    return GngUtils.getBaseResponse(HttpStatus.OK, loginDataResponse);
                                }

                                Object object = processRegistration(applicationRequest, header, stepName, httpRequest);
                                if (object instanceof Acknowledgement) {
                                    logger.info("Error in refId generation: {}", JsonUtil.ObjectToString(object));
                                    activityLogs.setCustomMsg("Error in refId generation:" + JsonUtil.ObjectToString(object));
                                    return GngUtils.getBaseResponse(HttpStatus.OK, object);
                                } else if (object instanceof GoNoGoCustomerApplication) {
                                    goNoGoCustomerApplication = ((GoNoGoCustomerApplication)object);
                                    action = stepName;
                                    activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());
                                    loginDataResponse.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
                                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loginDataResponse);
                                } else {
                                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                                            ErrorCode.INVALID_REQUEST_DSCR, null, null));
                                }
                            } else
                                stepMissed=true;
                        }
                        break;
                        case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:
                            action = (action == null) ? IntimationConstants.DEMOGRAPHIC_DETAILS : action;
                        case EndPointReferrer.SAVE_ELIGIBILITY_DETAILS:
                        case EndPointReferrer.STEP_PERSONAL_DETAILS:
                        case EndPointReferrer.STEP_EMPLOYMENT_DETAILS:
                            applicationRequest = mappingApplicationRequest(loginDataRequest);
                            if(StringUtils.isNotEmpty(refId) && null != applicationRequest){

                                goNoGoCustomerApplication = processApplicationDetails(applicationRequest, stepName, header.getLoggedInUserId(), header.getLoggedInUserRole(), httpRequest);
                                activityLogs.setStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                                activityLogs.setStatus(goNoGoCustomerApplication.getApplicationStatus());

                                loginDataResponse.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
                                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loginDataResponse);
                                logger.debug(" FINISHED step {}  refId {} ", stepName, refId);
                            }else
                                stepMissed=true;
                            break;
                        default:
                            String errorMsg = String.format("STEP %s is not defined ", stepName);
                            logger.warn("STEP {} is not defined", stepName);
                            activityLogs.setCustomMsg(
                                    new StringBuilder().append(errorMsg).append(" | ")
                                            .append(ErrorCode.INVALID_REQUEST_DSCR).append(" | ")
                                            .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                                            .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                                            .append(Error.SEVERITY.CRITICAL.name()).toString());
                            Collection<Error> errors = new ArrayList<>();
                            errors.add(Error.builder()
                                    .message(errorMsg)
                                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                    .level(Error.SEVERITY.CRITICAL.name())
                                    .build());
                            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
                            break;

                    }

                    if(stepMissed){
                        String errorMsg = String.format("STEP %s is not executed. Application Request or RefId is empty", stepName);
                        logger.warn("STEP {} is not executed. Application Request or RefId is empty", stepName);
                        activityLogs.setCustomMsg(
                                new StringBuilder().append(errorMsg).append(" | ")
                                        .append(ErrorCode.INVALID_REQUEST_DSCR).append(" | ")
                                        .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                                        .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                                        .append(Error.SEVERITY.CRITICAL.name()).toString());
                        Collection<Error> errors = new ArrayList<>();
                        errors.add(Error.builder()
                                .message(errorMsg)
                                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                .level(Error.SEVERITY.CRITICAL.name())
                                .build());
                        baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
                    }
                    if (action != null && !stepMissed) {
                        appConfigurationHelper.intimateUsers(goNoGoCustomerApplication.getGngRefId(), action);
                    }//*/
                }
            }
        }catch (GoNoGoException gngException) {
            logger.error(gngException.getMessage());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(gngException.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);
        }catch (Exception e)
        {
            logger.error(e.getMessage());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, errors);

        }finally {
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                    Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLogs);
        }
        return baseResponse;
    }

    @Override
    public ApplicationRequest mappingApplicationRequest(LoginDataRequest loginDataRequest) throws Exception{
        GoNoGoCustomerApplication goNoGoCustomerApplication=null;
        ApplicationRequest applicationRequest=null;

        String stepName=loginDataRequest.getStepName();
        String refId=loginDataRequest.getRefId();
        Boolean withDedupe = loginDataRequest.isWithDedupe();

        switch(stepName) {
            case EndPointReferrer.MCP_SAVE:
            case EndPointReferrer.STEP_REGISTRATION: {
                if(!withDedupe || StringUtils.isEmpty(loginDataRequest.getDedupeRefId())) {
                    //Dedup is not found and we are proceeding with fresh/existing case
                    if(StringUtils.isNotEmpty(refId)) {
                        goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                    }
                    applicationRequest = mapRegisterationData(loginDataRequest, applicationRequest);
                }else{
                    if(StringUtils.isNotEmpty(loginDataRequest.getDedupeRefId())) {
                        // When Internal Dedup is found
                        goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(loginDataRequest.getDedupeRefId());
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                    }else if(StringUtils.isNotEmpty(loginDataRequest.getProspectCode())){
                        // When Mifin Dedup is found
                        applicationRequest = mapRegisterationData(loginDataRequest, applicationRequest);
                        applicationRequest = mapMifinDedupData(loginDataRequest.getMatchApplicantDetails(), applicationRequest);
                    }
                }

                if(null != applicationRequest) {
                    applicationRequest.setAppMetaData(loginDataRequest.getAppMetaData());
                    applicationRequest.setAggregatorData(loginDataRequest.getAggregatorData());
                    applicationRequest.setRefID(refId);
                    setAge(applicationRequest);
                }
                break;
            }
            case EndPointReferrer.SAVE_ELIGIBILITY_DETAILS:{
                EligibilityData eligibilityData = loginDataRequest.getEligibilityData();
                if(null != eligibilityData){
                    goNoGoCustomerApplication=applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                    if(null != goNoGoCustomerApplication) {
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                        mapEligibilityData(eligibilityData, applicationRequest.getRequest().getApplication(), applicationRequest.getRequest().getApplicant());
                    }
                }
                break;
            }
            case EndPointReferrer.STEP_PERSONAL_DETAILS:{
                PersonalDetails personalDetails=loginDataRequest.getPersonalDetails();
                if(null!= personalDetails){
                    goNoGoCustomerApplication=applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                    if(null != goNoGoCustomerApplication) {
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                        mapPersonalDetails(personalDetails, applicationRequest.getRequest().getApplicant());
                    }
                }
                break;
            }
            case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:{
                DemographicData demographicData=loginDataRequest.getDemographicData();
                if(null!=demographicData){
                    goNoGoCustomerApplication= applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                    if(null != goNoGoCustomerApplication) {
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                        mapDemographicData(demographicData, applicationRequest.getRequest().getApplicant());
                    }
                }
                break;
            }
            case EndPointReferrer.STEP_EMPLOYMENT_DETAILS:{
                EmploymentData employmentData=loginDataRequest.getEmploymentData();
                if(null!=employmentData){
                    goNoGoCustomerApplication= applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
                    if(null != goNoGoCustomerApplication) {
                        applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                        mapEmploymentData(employmentData, applicationRequest.getRequest().getApplicant());
                    }
                }
            }
            break;
        }
        if(null != applicationRequest) applicationRequest.setHeader(loginDataRequest.getHeader());
        return applicationRequest;
    }

    private ApplicationRequest mapMifinDedupData(MatchApplicantDetails matchApplicantDetails, ApplicationRequest applicationRequest){
        ArrayList<Kyc> kycList=null;
        Kyc kyc=null;
        List<CustomerAddress> demographicAddressList = null;
        CustomerAddress customerAddress= null;
        Applicant applicant=null;

        if(null != matchApplicantDetails && null != applicationRequest){
            kycList=new ArrayList<>();
            demographicAddressList=new ArrayList<>();

            kyc=new Kyc();
            kyc.setKycName(KYC_TYPES.PAN.toString());
            kyc.setKycNumber(matchApplicantDetails.getPan());
            kyc.setKycStatus(false);
            kycList.add(kyc);

            applicant=applicationRequest.getRequest().getApplicant();
            applicant.setDateOfBirth(matchApplicantDetails.getDob());
            applicant.setKyc(kycList);

            for(AddressDetail addressDetail: matchApplicantDetails.getAddressDetailsList()){
                customerAddress=new CustomerAddress();
                customerAddress.setAddressType(addressDetail.getAddressType());
                customerAddress.setAddressLine1(addressDetail.getAddress());
                customerAddress.setPin(addressDetail.getPinCode() != null ? Long.valueOf(addressDetail.getPinCode()):null );
                demographicAddressList.add(customerAddress);
            }
            if(demographicAddressList.size()>0)
                applicant.setAddress(demographicAddressList);
        }
        return applicationRequest;
    }
    private ApplicationRequest mapRegisterationData(LoginDataRequest loginDataRequest, ApplicationRequest applicationRequest)throws Exception{
        RegistrationData registrationData = loginDataRequest.getRegistrationData();
        Name applicantName = null;
        Phone phone = null;
        Email email = null;
        Applicant applicant = null;
        Application application = null;
        Request request = null;
        if (null != registrationData) {
            List<Phone> phoneList = new ArrayList();
            List<Email> emailList = new ArrayList();

            if(null == applicationRequest) {
                applicationRequest = new ApplicationRequest();
                request = new Request();
                application = new Application();
                applicant = new Applicant();

                application.setLoanType(loginDataRequest.getHeader().getProduct().name());
                application.setProductID(loginDataRequest.getHeader().getProduct().toProductId());

                //Request
                request.setApplicant(applicant);
                request.setApplication(application);

                //Application Request
                applicationRequest.setRequest(request);
                applicationRequest.setApplicantType("NORMAL");
            }else{
                request = applicationRequest.getRequest();
                application = request.getApplication();
                applicant = request.getApplicant();
                if(null != applicant.getApplicantName())
                    applicantName = applicant.getApplicantName();
                if(CollectionUtils.isNotEmpty(applicant.getEmail())) {
                    emailList = applicant.getEmail();
                    email = emailList.stream()
                            .filter(obj -> StringUtils.equalsIgnoreCase("PERSONAL", obj.getEmailType()))
                            .findAny().orElse(null);
                }
                if(CollectionUtils.isNotEmpty(applicant.getPhone())) {
                    phoneList = applicant.getPhone();
                    phone = phoneList.stream()
                            .filter(obj -> StringUtils.equalsIgnoreCase("PERSONAL_MOBILE", obj.getPhoneType()))
                            .findAny().orElse(null);
                }
            }

            if(null == email) {
                email = new Email();
                email.setEmailType("PERSONAL");
                emailList.add(email);
            }
            if(null ==  phone) {
                phone = new Phone();
                phone.setPhoneType("PERSONAL_MOBILE");
                phoneList.add(phone);
            }

            if(null == applicantName)
                applicantName = new Name();
            //Applicant Name
            BeanUtils.copyProperties(applicantName, registrationData);

            //Applicant Phone
            phone.setPhoneNumber(registrationData.getPhoneNumber());
            phone.setCountryCode("+91");
            phone.setVerified(registrationData.isPhoneVerified());

            //Applicant Email
            email.setEmailAddress(registrationData.getEmailAddress());

            //Applicant
            applicant.setApplicantId("0");
            applicant.setApplicantName(applicantName);
            applicant.setPhone(phoneList);
            applicant.setEmail(emailList);
            applicant.setCity(registrationData.getCity());
            applicant.setApplicantType(ApplicantType.INDIVIDUAL.value());

            EligibilityData eligibilityData = loginDataRequest.getEligibilityData();
            if(null != eligibilityData){
                mapEligibilityData(eligibilityData, application, applicant);
            }

            PersonalDetails personalDetails=loginDataRequest.getPersonalDetails();
            if(null != personalDetails) {
                mapPersonalDetails(personalDetails, applicant);
            }

            DemographicData demographicData=loginDataRequest.getDemographicData();
            if(null!=demographicData) {
                mapDemographicData(demographicData, applicant);
            }

            EmploymentData employmentData = loginDataRequest.getEmploymentData();
            if(null != employmentData){
                mapEmploymentData(employmentData, applicant);
            }

            //Application
            BeanUtils.copyProperties(application, registrationData);
        }
        return applicationRequest;
    }

    private void mapEligibilityData(EligibilityData eligibilityData, Application application, Applicant applicant) throws Exception{
        String tenure = "0";
        if (StringUtils.isNotEmpty(eligibilityData.getAppliedTenor())) tenure = eligibilityData.getAppliedTenor();

        EligibilityDetails eligibilityDetails = new EligibilityDetails();
        BeanUtils.copyProperties(eligibilityDetails, eligibilityData);
        eligibilityDetails.setAppliedTenor(tenure);
        eligibilityDetails.setFixedOblgation(eligibilityData.getFixedOblgation());

        if (CollectionUtils.isNotEmpty(eligibilityData.getEligibleOffersList())) {
            setEligibleOffer(eligibilityDetails);
        }
        application.setRoi(eligibilityData.getRoi());
        application.setLoanAmount(eligibilityData.getAppliedLoan());
        application.setTenorRequested(tenure);
        applicant.setEligibilityDetails(eligibilityDetails);
    }

    private void mapPersonalDetails(PersonalDetails personalDetails, Applicant applicant) throws Exception{
        BeanUtils.copyProperties(applicant, personalDetails);
        applicant.setGender(personalDetails.getGender());
        applicant.setMaritalStatus(personalDetails.getMaritalStatus());

        Kyc kyc=new Kyc();
        BeanUtils.copyProperties(kyc, personalDetails);

        List<Kyc> kycs;
        if(CollectionUtils.isNotEmpty(applicant.getKyc())){
            kycs = applicant.getKyc().stream()
                    .filter(okyc -> !(StringUtils.equalsIgnoreCase(kyc.getKycRequestType(), okyc.getKycRequestType())))
                    .collect(Collectors.toList());
            kycs.add(kyc);
        }else{
            kycs = new ArrayList<>();
            kycs.add(kyc);
        }

        applicant.setKyc(kycs);
    }

    private void mapDemographicData(DemographicData demographicData, Applicant applicant)throws Exception{
        List<CustomerAddress> demographicAddressList  = new ArrayList<>();
        CustomerAddress customerAddress = null;
        for (DemographicAddress demographicAddress : demographicData.getAddress()) {
            customerAddress = new CustomerAddress();
            BeanUtils.copyProperties(customerAddress, demographicAddress);
            customerAddress.setAddressType(demographicAddress.getAddressType());
            customerAddress.setResidenceAddressType(demographicAddress.getOwnershipType());
            demographicAddressList.add(customerAddress);
        }
        if (demographicAddressList.size() > 0)
            applicant.setAddress(demographicAddressList);
    }

    private void mapEmploymentData(EmploymentData employmentData, Applicant applicant) throws Exception{
        List<Employment> employmentList=new ArrayList<>();
        Employment employment=new Employment();
        BeanUtils.copyProperties(employment, employmentData);
        employment.setEmploymentName(employmentData.getEmploymentName());
        employment.setEmploymentType(GNGWorkflowConstant.SALARIED.toFaceValue());
        employment.setTotalYearsOfExperience(employmentData.getTotalYearsOfExperience());
        employment.setTimeWithEmployer(employmentData.getTimeWithEmployer());
        employmentList.add(employment);

        applicant.setEmployment(employmentList);
    }

    private boolean dedupeFound(LoginDataRequest loginDataRequest, LoginDataResponse loginDataResponse, ApplicationRequest applicationRequest){
        /*First time when we are registering case, in first savelogindata request isWithDedupe and isSkipDeupe is always false.
                                  If any dedupe found, matchdetails list will be sent in response. In this case user either selects(isWithDedupe=true)
                                  OR deselects(isSkipDedupe=true) any existing dedupe application. So this check is for skipping unnecessary multiple internal dedupe check calls.
                                * */
        boolean dedupFound = false;

        if (!loginDataRequest.isWithDedupe() && !loginDataRequest.isSkipDedupe()) {
            //This block purpose is to check internal dedupe
            Set<DedupeDetails> dedupeResultSet = finalApplicationRepository.deDupeCustomerV2(applicationRequest);
            if (null != dedupeResultSet) {
                List<DedupeMatchDetail> matchDetailList = getDedupeMatchList(dedupeResultSet);
                if (CollectionUtils.isNotEmpty(matchDetailList)) {
                    logger.debug("Internal dedup found {}", matchDetailList);
                    loginDataResponse.setDedupeMatchDetails(matchDetailList);
                    dedupFound = true;
                }
            }

            //This block is to check mifin dedup
            if (lookupService.checkActionsAccess(
                    applicationRequest.getHeader().getInstitutionId()
                    , applicationRequest.getHeader().getProduct().toProductId()
                    , ActionName.GET_DEDUP_DATA_FROM_MIFIN)) {
                MiFinCallLog miFinCallLog = externalApiManager.evaluateMifinDedup(applicationRequest);
                if (null != miFinCallLog && CollectionUtils.isNotEmpty(miFinCallLog.getDedupeResponseList()) && (null != miFinCallLog.getDedupeResponseList().get(0) && CollectionUtils.isNotEmpty(miFinCallLog.getDedupeResponseList().get(0).getMatchApplicantList()))) {
                    logger.debug("Mifin dedup found {}", miFinCallLog.getDedupeResponseList().get(0));
                    loginDataResponse.setMifinDedupeResponse(miFinCallLog.getDedupeResponseList().get(0));
                    dedupFound = true;
                }
            }
        }
        return dedupFound;
    }

    @Override
    public BaseResponse fetchCRMDedupe(LoginDataRequest loginDataRequest){
        BaseResponse baseResponse;
        if (lookupService.checkActionsAccess(
                loginDataRequest.getHeader().getInstitutionId()
                , loginDataRequest.getHeader().getProduct().toProductId()
                , ActionName.GET_DEDUP_DATA_FROM_CRM)) {
            if(null != loginDataRequest.getRegistrationData() && StringUtils.isNotEmpty(loginDataRequest.getRegistrationData().getPhoneNumber())) {
                CrmResponse crmResponse = externalApiManager.fetchCRMDedupeUsingHeaders(loginDataRequest);
                if (null != crmResponse && null != crmResponse.getCrmResponseDetails()
                        && StringUtils.equalsIgnoreCase(crmResponse.getCrmResponseDetails().getStatus(), "S")) {
                    logger.debug("Crm dedup found {}", crmResponse);
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, crmResponse);
                } else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                }
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, "RegistrationData : PhoneNumber can not be empty");
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.FORBIDDEN, "Access to action is forbidden");
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getActivityLogsDetails(UserActivityRequest userActivityRequest) throws Exception {
        BaseResponse baseResponse = null;
        List<TimelineData> timelineDataList = new ArrayList<>();
        List<ActivityLogs> activityLogsList;
        Collection<Error> errors = new ArrayList<>();
        TimelineDataResponse timelineDataResponse = new TimelineDataResponse();
        try {
            activityLogsList = applicationRepository.fetchUserActivityLogsByRefId(userActivityRequest.getRefId(), userActivityRequest.getHeader().getInstitutionId());
            if (CollectionUtils.isNotEmpty(activityLogsList)) {
                moduleHelper.setLogsInTimelineDataList(userActivityRequest.getHeader().getInstitutionId(), activityLogsList, timelineDataList);
                if (CollectionUtils.isNotEmpty(timelineDataList)) {
                    timelineDataResponse.setRefId(userActivityRequest.getRefId());
                    timelineDataResponse.setTimelineDataList(timelineDataList);
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, timelineDataResponse);
                } else {

                    errors.add(Error.builder()
                            .message("Error while fetching timeline data")
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
                }
            } else {

                errors.add(Error.builder()
                        .message(ErrorCode.NO_DATA_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .build());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, errors);
            }
        } catch (Exception e) {
            logger.error("Error in method getActivityLogsDetails due to {}", e.getStackTrace());
        }
        return baseResponse;
    }

    Object processRegistration(ApplicationRequest applicationRequest, Header header, String stepId, HttpServletRequest httpRequest)throws Exception{

        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(), applicationRequest.getRefID(), httpRequest,
                EndPointReferrer.STEP_REGISTRATION, stepId,
                null, applicationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Boolean saveResult;
        Boolean updateFlag;

        if (StringUtils.isBlank(applicationRequest.getRefID())) {
            // This is start of application punching
            // SO stage is DE and status is NEW
            // Set Stage to DE
            updateFlag=false;
            applicationRequest.setCurrentStageId(GNGWorkflowConstant.DE.name());
            activityLogs.setStage(GNGWorkflowConstant.DE.name());
            activityLogs.setStatus(Status.NEW.toString());

            logger.debug("Generating refId ");
            Object object = generateAndSetRefId(applicationRequest);

            if (object instanceof Acknowledgement)
                return object;
        }else
            updateFlag=true;

        String refId = applicationRequest.getRefID();

        // Set refId for activity log
        activityLogs.setRefId(applicationRequest.getRefID());
        // Set applicationId
        applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());
        //set source (as of now leviosa for digi-pl)
        if(StringUtils.equalsIgnoreCase(header.getProduct().name(),Product.DIGI_PL.name())){
            applicationRequest.getHeader().setSource("Leviosa");
        }
        GoNoGoCustomerApplication goNoGoCustomerApplication;
        if(!updateFlag){
            logger.debug("{} : inserting applicationRequest refId {}", stepId, refId);
            applicationRepository.saveApplication(applicationRequest);
            goNoGoCustomerApplication = RequestAggregator.getGoNoGo(applicationRequest);
            // Save GnGCustomerApplication
            logger.debug("{} : inserting goNoGoCustomerApplication refId {}", stepId, refId);
            saveResult = applicationRepository.saveGoNoGoCustomerApplication(goNoGoCustomerApplication);

            // Add this step in completed-set of execution items
            applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                    EndPointReferrer.STEP_REGISTRATION, header.getLoggedInUserId(), header.getLoggedInUserRole());
        }else{
            logger.debug("{} : updating goNoGoCustomerApplication refId {}", stepId, refId);
            saveResult = applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);
            logger.debug("{} : fetching goNoGoCustomerApplication refId {}", stepId, refId);
            goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
        }
        if(saveResult) {
            goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
            if(StringUtils.equalsIgnoreCase(stepId, EndPointReferrer.MCP_SAVE)){
                try {
                    moduleManager.preSave(goNoGoCustomerApplication.getApplicationRequest(), EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS);
                }catch(Exception e){}
            }
        }

        moduleManager.executeBre(goNoGoCustomerApplication, stepId, header.getLoggedInUserRole(), header,
                ConfigurationConstants.WF_NODE_TYPE_SCREEN);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return goNoGoCustomerApplication;
    }

    private GoNoGoCustomerApplication processApplicationDetails(ApplicationRequest applicationRequest, String stepId, String userId, String role, HttpServletRequest httpRequest) throws Exception {

        ActivityLogs activityLogs = auditHelper.createActivityLog(applicationRequest.getHeader(), applicationRequest.getRefID(), httpRequest,
                stepId, stepId,
                null, applicationRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refId = applicationRequest.getRefID();
        logger.debug("{} : validating refId {}", stepId, refId);

        if(StringUtils.equalsIgnoreCase(stepId, EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS)) {
            // FIX : We need to set certain values which are supposed to be set in the incoming request.
            setAge(applicationRequest);
            moduleHelper.setcity(applicationRequest);

            // Update applicationRequest related data before updating GoNoGoCustomerApplication
            moduleManager.preSave(applicationRequest, stepId);
        }

        applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

        // Add this step in completed-set of execution items
        applicationRepository.updateCompletedInfo(refId, applicationRequest.getHeader().getInstitutionId(), stepId, userId, role);

        logger.debug("{} : fetching GoNoGoCustomerApplication refId {}", stepId, refId);
        // Get CustomerApplication from DB
        GoNoGoCustomerApplication goNoGoCustomerApplication = fetchgoNoGoCustomerApplication(applicationRequest);
        logger.debug("{} : fetched GoNoGoCustomerApplication refId {} :\n {}", stepId, refId,
                goNoGoCustomerApplication.getApplicationRequest());

        moduleManager.afterSave(goNoGoCustomerApplication, stepId);

        logger.debug("{} : invoking BRE components for refId {} ", stepId, refId);
        // After-save module check
        moduleManager.executeBre(goNoGoCustomerApplication, stepId, role, applicationRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);

        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return goNoGoCustomerApplication;
    }

    private Collection<Error> validateVersion(Header header, HttpServletRequest httpRequest) {
        Collection<Error> errors = null;
        if (!configurationManager.isValidVersion(header.getInstitutionId(), header.getApplicationSource())) {
            ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, header);

            activityLog.setCustomMsg(
                    new StringBuilder().append(ErrorCode.INVALID_VERSION_DSCR).append(" | ")
                            .append(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode())).append(" | ")
                            .append(Error.ERROR_TYPE.SYSTEM.toValue()).append(" | ")
                            .append(Error.SEVERITY.CRITICAL.name()).toString());

            errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VERSION_DSCR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            // Persist activityLog in DB
            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            activityEventPublisher.publishEvent(activityLog);

        }
        return errors;
    }

    @Override
    public BaseResponse persistRepayment(BankingDetailsRequest bankingDetailsRequest){
        BaseResponse baseResponse = null;
        String refId = bankingDetailsRequest.getRefId();
        Header header = bankingDetailsRequest.getHeader();
        try{
            logger.debug("method persistRepayment started for { }", refId);
            if (StringUtils.isNotEmpty(refId) && CollectionUtils.isNotEmpty(bankingDetailsRequest.getBankingDetails())) {
                GoNoGoCustomerApplication  goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationOnlyByRefId(refId);
                if(null != goNoGoCustomerApplication){
                    ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                    applicationRequest.getRequest().getApplicant().setBankingDetails(bankingDetailsRequest.getBankingDetails());
                    moduleHelper.preSaveRepaymentDetails(applicationRequest);
                    applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

                    RepaymentRequest repaymentRequest = new RepaymentRequest();
                    repaymentRequest.setRefId(applicationRequest.getRefID());
                    repaymentRequest.setHeader(applicationRequest.getHeader());
                    Repayment repayment = applicationRepository.fetchRepaymentDetails(repaymentRequest);
                    if (repayment != null) {
                        updateDM(applicationRequest, repayment);
                        applicationRepository.updateCompletedInfo(goNoGoCustomerApplication, EndPointReferrer.SAVE_REPAYMENT_DETAILS,
                                header.getLoggedInUserId(), header.getLoggedInUserRole());
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, repayment);
                    } else {
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                    }
                }else{
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
                }
            }else{
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        }catch(Exception e){
            logger.error("{ } refId, Exception occurred while saving repayment Details. Exception {}", refId, ExceptionUtils.getStackTrace(e));
        }
        logger.debug("method persistRepayment ended for { }", refId);
        return baseResponse;
    }

    @Override
    public void updateDM(ApplicationRequest applicationRequest, Repayment repayment){
        String refId = repayment.getRefId();
        String institutionId = repayment.getInstitutionId();
        try {
            RepaymentDetails dmRepayment = CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList()) ?
                    repayment.getRepaymentDetailsList().get(0) : null;
            LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
            List<DMInput> dmInputs = new ArrayList<>();
            if (null != loanCharges && CollectionUtils.isNotEmpty(loanCharges.getLoanChargesList())) {
                for (LoanCharges dmLoanCharges : loanCharges.getLoanChargesList()) {
                    logger.debug("Setting values for {} percent LoanCharges", dmLoanCharges.getLoanPercentage());
                    DMInput dmInput = new DMInput();
                    if (null != dmRepayment) {
                        dmInput.setDmBankName(dmRepayment.getBankName());
                        dmInput.setDmPaymentMode("IMPS");//HardCoded for Now
                        dmInput.setInstrumentNo(dmRepayment.getChqNumber());
                    }
                    dmInput.setDmDate(new Date());
                    dmInput.setDmFavourting(applicationRequest.getRequest().getApplicant().getApplicantName().getFullName());
                    dmInput.setDmAmt(String.valueOf(dmLoanCharges.getNetDisbAmt()));
                    dmInput.setLoanPercentage(dmLoanCharges.getLoanPercentage());
                    dmInputs.add(dmInput);
                }
            }
            CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId, institutionId);
            DMRequest dmRequest = new DMRequest();
            dmRequest.setRefId(refId);
            dmRequest.setHeader(applicationRequest.getHeader());
            dmRequest.setDisbursementMemo(DisbursementMemo.builder()
                    .refId(refId)
                    .institutionId(institutionId)
                    .repayment(repayment)
                    .loanCharges(loanCharges)
                    .camSummary((null != camDetails && null != camDetails.getSummary()) ? camDetails.getSummary() : null)
                    .dmInputList(dmInputs)
                    .build());
            logger.debug("saveLoanChargesAndRepaymentDetails: Saving DM Details for RefId: {}", refId);
            saveDMDetails(dmRequest, null);
        }catch(Exception e){
            logger.error("{ } refId, Exception occurred while saving digiPL dmDetails. Exception {}", refId, ExceptionUtils.getStackTrace(e));
        }
    }

    List<DedupeMatchDetail> getDedupeMatchList(Set<DedupeDetails> dedupeResultSet){
        DedupeMatchDetail matchDetail = null;
        List<DedupeMatchDetail> matchDetailList = new ArrayList();
        for(DedupeDetails dedupeDetails : dedupeResultSet){
            // create matchdetail obj
            matchDetail = new DedupeMatchDetail();
            // set dedupedeatil in matchdetail obj
            matchDetail.setDedupeDetails(dedupeDetails);
            //set applicantId for MatchDetial obj
            matchDetail.setApplicantId(dedupeDetails.getApplicantId());
            // add matchdetail obj in the list
            matchDetailList.add(matchDetail);
        }
        //sorting match details in descending order according to refId
        Collections.sort(matchDetailList, (o1,o2) -> o2.getDedupeDetails().getRefId().compareTo(o1.getDedupeDetails().getRefId()));
        return matchDetailList;
    }

    private void setEligibleOffer(EligibilityDetails eligibility){
        // We are setting here values of that offer which is selected offer.
        EligibleOffers eligibleOffer = eligibility.getEligibleOffersList().stream()
                .filter(offer -> offer.isSelectedOffer()).findAny().orElse(null);
        if(null != eligibleOffer){
            eligibility.setElgLnAmt(eligibleOffer.getEligibilityAmount());
            eligibility.setAppliedLoan(eligibleOffer.getEligibilityAmount());
            eligibility.setAprvLoan(eligibility.getElgLnAmt());
            eligibility.setMonthlySalary(eligibleOffer.getIncome());
            eligibility.setTenor(eligibleOffer.getEligibleTenor() == null? 0:Integer.valueOf(eligibleOffer.getEligibleTenor()));
            eligibility.setElgFixedOblg(eligibleOffer.getFixedObligations());
            eligibility.setEmi(eligibleOffer.getEmi());
        }
    }

    @Override
    public BaseResponse saveOCRData(OCRRequest ocrRequest) throws Exception {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(ocrRequest.getHeader(),
                ocrRequest.getRefId(), null,
                null, EndPointReferrer.SAVE_OCR_DATA, null, ocrRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        logger.debug("{} : saving OCR Details refId {}", ocrRequest.getRefId());
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(ocrRequest.getRefId());
        if (ocrRequest.getRcuSummary() != null) {
            goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setRcuSummary(ocrRequest.getRcuSummary());
        }

        goNoGoCustomerApplication.getApplicationRequest().getRequest().setCaseInitiated(ocrRequest.isCaseInitiated());
        goNoGoCustomerApplication.getApplicationRequest().getRequest().setInitiatedBy(ocrRequest.getInitiatedBy());
        goNoGoCustomerApplication.getApplicationRequest().getRequest().setInitiatedByRole(ocrRequest.getInitiatedByRole());

        if (StringUtils.isNotEmpty(ocrRequest.getStatus())) {
            if (StringUtils.equals(ocrRequest.getStatus(), GNGWorkflowConstant.SALES.toFaceValue())) {
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setBranchQuery(true);
                //goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.BRANCH_QUERY.toFaceValue());
            }
            // in ABFL we are using these two fields to trigger RCU
            goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setOcrStatus(ocrRequest.getStatus());
            goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setAgencyStatus(ocrRequest.getAgencyStatus());

            if (StringUtils.isNotEmpty(ocrRequest.getAgencyCode())) {
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setAgencyCode(ocrRequest.getAgencyCode());
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setAgencyName(ocrRequest.getAgencyName());
            }
        }
        if ((StringUtils.equals(ocrRequest.getStatus(), GNGWorkflowConstant.SAMPLED.toFaceValue()) ||
                StringUtils.equals(ocrRequest.getAgencyStatus(), GNGWorkflowConstant.SAMPLED.toFaceValue()))
                && Roles.isDocVerRole(ocrRequest.getHeader().getLoggedInUserRole())) {
            if (ocrRequest.getRcuSummary() != null) {
                ocrRequest.getRcuSummary().setRcuSamplingDate(new Date());
            }
        }

        moduleHelper.deleteImageFromFSFiles(ocrRequest);
        applicationRepository.saveOCRData(ocrRequest);
        applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());

        if (StringUtils.equalsIgnoreCase(ocrRequest.getStatus(), IntimationConstants.VERIFICATION_STATUS_VERIFIED)) {
            // intimate users
            appConfigurationHelper.intimateUsers(ocrRequest.getRefId(), IntimationConstants.RCU_SUBMIT);
        } else if(StringUtils.equalsIgnoreCase(ocrRequest.getStatus(), "SUBMITTED")) {
            appConfigurationHelper.intimateUsers(ocrRequest.getRefId(), IntimationConstants.RCU_INITIATED);
        }

        goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(goNoGoCustomerApplication.getGngRefId());

        applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                EndPointReferrer.SAVE_OCR_DATA, ocrRequest.getHeader().getLoggedInUserId(), ocrRequest.getHeader().getLoggedInUserRole());

        Request request = goNoGoCustomerApplication.getApplicationRequest().getRequest();
        if (null != request.getApplication() && null != request.getApplication().getChannel()
                && StringUtils.equalsIgnoreCase(request.getApplication().getChannel(), "Online")){
            moduleManager.executeBre(goNoGoCustomerApplication,
                    EndPointReferrer.SUBMIT_OCR_DATA, ocrRequest.getHeader().getLoggedInUserRole(), ocrRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);
        }

        if (goNoGoCustomerApplication != null) {
            response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse submitOCRData(OCRRequest ocrRequest) throws Exception {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(ocrRequest.getHeader(),
                ocrRequest.getRefId(), null,
                null, EndPointReferrer.SUBMIT_OCR_DATA, null, ocrRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(ocrRequest.getRefId());

        if (goNoGoCustomerApplication != null) {
            applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                    EndPointReferrer.SUBMIT_OCR_DATA, ocrRequest.getHeader().getLoggedInUserId(), ocrRequest.getHeader().getLoggedInUserRole());

            moduleManager.executeBre(goNoGoCustomerApplication,
                    EndPointReferrer.SUBMIT_OCR_DATA, ocrRequest.getHeader().getLoggedInUserRole(), ocrRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);

            response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
        } else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse resetOCRData(OCRRequest ocrRequest) throws Exception {

        applicationRepository.deleteOCRData(ocrRequest);

        return getOCRData(ocrRequest);
    }

    @Override
    public BaseResponse getOCRData(OCRRequest ocrRequest) throws Exception{
        BaseResponse baseResponse = null;
        OCRDetails ocrDetails = applicationRepository.getOCRData(ocrRequest);
        if (ocrDetails != null) {
            moduleHelper.setImageDetailsToOCR(ocrDetails, ocrRequest);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, ocrDetails);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse resetApplicationStatus(LoginDataRequest loginDataRequest){
        boolean updateStatus=false;
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(Status.FAILED.name());

        if(null != loginDataRequest)
            updateStatus = applicationRepository.resetApplicationStatus(loginDataRequest.getRefId(), loginDataRequest.getHeader().getInstitutionId(),
                    null, loginDataRequest.getApplnBucket(), loginDataRequest.getApplnStatus());

        if (updateStatus)
            genericResponse.setStatus(Status.OK.name());

        return GngUtils.getBaseResponse(HttpStatus.OK, genericResponse);
    }

    @Override
    public BaseResponse saveDigiPLEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest) {
        ActivityLogs activityLogs = auditHelper.createActivityLog(eligibilityRequest.getHeader(),eligibilityRequest.getRefId(),httpRequest,
                applicationRepository.getApplicationStage(eligibilityRequest.getHeader().getInstitutionId(),eligibilityRequest.getRefId()), EndPointReferrer.SAVE_ELIGIBILITY_DETAILS, null, eligibilityRequest.getHeader().getLoggedInUserId());

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        BaseResponse baseResponse;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.fetchApplicantEligibility(eligibilityRequest.getRefId());
        if (goNoGoCustomerApplication != null) {
            EligibilityDetails reqEligibility = eligibilityRequest.getEligibilityDetails();
            Header header = eligibilityRequest.getHeader();
            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

            if(null == applicant.getEligibilityDetails() && null == applicant.getPostEligibility())
                applicant.setEligibilityDetails(reqEligibility);
            else if(null != applicant.getEligibilityDetails() && null == applicant.getPostEligibility())
                applicant.setEligibilityDetails(reqEligibility);
            else
                applicant.setPostEligibility(reqEligibility);

            applicationRepository.saveApplicantEligibility(eligibilityRequest.getRefId(), applicant);

            applicationRepository.updateCompletedInfo(eligibilityRequest.getRefId(), header.getInstitutionId(),
                    EndPointReferrer.SAVE_ELIGIBILITY_DETAILS, header.getLoggedInUserId(), header.getLoggedInUserRole());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicant);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    @Override
    public BaseResponse getDigiPLEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest) {
        ActivityLogs activityLogs = auditHelper.createActivityLog(eligibilityRequest.getHeader(), eligibilityRequest.getRefId(),
                httpRequest, GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.GET_DIGIPL_ELIGIBILITY_DETAILS,
                GNGWorkflowConstant.QUEUED.toFaceValue(), eligibilityRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        BaseResponse baseResponse;
        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.fetchApplicantEligibility(eligibilityRequest.getRefId());
        if (goNoGoCustomerApplication != null) {
            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
            applicant.setApplicationBucket(goNoGoCustomerApplication.getApplicationBucket());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, applicant);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }

    @Override
    public BaseResponse saveMismatchDetails(MismatchRequest mismatchRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        boolean save;
        List<Mismatch> newList=new ArrayList<>();
        ActivityLogs activityLogs = auditHelper.createActivityLog(mismatchRequest.getHeader(), mismatchRequest.getRefID(), httpRequest,
                GNGWorkflowConstant.DDE.toFaceValue(), EndPointReferrer.SAVE_MISMATCH_DETAILS,
                null, mismatchRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            logger.debug("{} : saving Mismatch Details refId {}", mismatchRequest.getRefID());
            MismatchRequest mismatchDetails=applicationRepository.fetchMismatchDetails(mismatchRequest);
            if(mismatchDetails!=null) {
                List<Mismatch> dbList=mismatchDetails.getMismatchList();
                List<Mismatch> requestList=mismatchRequest.getMismatchList();
                for(Mismatch db:dbList) {
                    boolean found=false;
                    for(Mismatch temp:requestList) {
                        if((db.getReqType().equals(temp.getReqType()))) {
                            logger.debug("Found matched Mismatch data() - {}", db.getReqType());
                            found = true;
                            break;
                        }
                    }
                    if(!found){
                        newList.add(db);
                    }
                }
                newList.addAll(mismatchRequest.getMismatchList());
            } else {
                newList=mismatchRequest.getMismatchList();
            }
            logger.debug("Mismatch deatils: - {}", newList);
            save = applicationRepository.saveMismatchDetails(mismatchRequest,newList);
            if (save) {
                mismatchDetails = applicationRepository.fetchMismatchDetails(mismatchRequest);
                response = GngUtils.getBaseResponse(HttpStatus.OK, mismatchDetails);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(e.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.MEDIUM.name())
                    .build());
            response = GngUtils.getBaseResponse(HttpStatus.OK, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getMismatchDetails(MismatchRequest mismatchRequest, HttpServletRequest httpRequest) {
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(mismatchRequest.getHeader(), mismatchRequest.getRefID(), httpRequest,
                null, EndPointReferrer.GET_MISMATCH_DETAILS,
                null, mismatchRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        MismatchRequest mismatchDetails = applicationRepository.fetchMismatchDetails(mismatchRequest);
        if(mismatchDetails != null){
            response = GngUtils.getBaseResponse(HttpStatus.OK, mismatchDetails);
        }else {
            response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse updateKycDetails(KycUpdateRequest kycUpdateRequest){
        BaseResponse response = null;
        ActivityLogs activityLogs = auditHelper.createActivityLog(kycUpdateRequest.getHeader(),
                kycUpdateRequest.getRefId(), null,
                null, EndPointReferrer.UPDATE_KYC_DETAILS, null, kycUpdateRequest.getHeader().getLoggedInUserId());
        activityLogs.setAction("UPDATE_KYC_DETAILS");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(kycUpdateRequest.getRefId());

            applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                    EndPointReferrer.UPDATE_KYC_DETAILS, kycUpdateRequest.getHeader().getLoggedInUserId(), kycUpdateRequest.getHeader().getLoggedInUserRole());

            if (CollectionUtils.isNotEmpty(kycUpdateRequest.getKycList())){
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setKyc(kycUpdateRequest.getKycList());

                applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());

                moduleManager.executeBre(applicationRepository.getGoNoGoCustomerApplicationByRefId(kycUpdateRequest.getRefId()),
                        EndPointReferrer.UPDATE_KYC_DETAILS, kycUpdateRequest.getHeader().getLoggedInUserRole(), kycUpdateRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_SCREEN);

                if (goNoGoCustomerApplication != null) {
                    response = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc());
                } else {
                    response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
                }
            }else{
                String errorMsg = String.format("Nothing is there to update kycs for refId %s", kycUpdateRequest.getRefId());
                logger.warn("Nothing is there to update kyc's for refid {}", kycUpdateRequest.getRefId());
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(errorMsg)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
            }
        }catch(Exception e){
            String errorMsg = String.format("Error while updating Kyc's for refid %s", kycUpdateRequest.getRefId());
            logger.warn("Error while updating Kyc's for refid {}", kycUpdateRequest.getRefId());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(errorMsg)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DataEntryManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return response;
    }

    @Override
    public BaseResponse getKycDetails(KycUpdateRequest kycUpdateRequest){
        BaseResponse response = null;
        try{
            if(StringUtils.isNotEmpty(kycUpdateRequest.getRefId())){
                GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(kycUpdateRequest.getRefId());
                List<Kyc> applKycList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc();
                if(CollectionUtils.isNotEmpty(applKycList)){
                    response = GngUtils.getBaseResponse(HttpStatus.OK, applKycList);
                } else {
                    response = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
                }
            }else{
                String errorMsg = String.format("refId is empty to fetch records");
                logger.warn("refId is empty to fetch records");
                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(errorMsg)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());
                response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
            }
        }catch(Exception e){
            String errorMsg = String.format("Error while getting Kyc's for refid %s", kycUpdateRequest.getRefId());
            logger.warn("Error while getting Kyc's for refid {}", kycUpdateRequest.getRefId());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(errorMsg)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            response = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
        }
        return response;
    }

    @Override
    public BaseResponse getApplicationBucket(LoginDataRequest loginDataRequest){
        BaseResponse baseResponse = null;

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.fetchApplicationBucket(loginDataRequest.getRefId());
        if(null != goNoGoCustomerApplication)
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication.getApplicationBucket());
        else
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());


        return baseResponse;
    }

    @Override
    public Collection<Error> validateLoginDataRequest(LoginDataRequest loginDataRequest, String functionName){
        Collection<Error> errors = new ArrayList<>();
        String message = "";
        //Checking Header
        if(null == loginDataRequest.getHeader()){
            message = "Header details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oHeader")
                    .build());
        }else{
            Header header = loginDataRequest.getHeader();
            if(StringUtils.isEmpty(header.getRequestType()) || StringUtils.isEmpty(header.getApplicationSource()) || null == header.getProduct() || StringUtils.isEmpty(header.getInstitutionId())
                    || StringUtils.isEmpty(header.getLoggedInUserId()) || StringUtils.isEmpty(header.getLoggedInUserRole()) || StringUtils.isEmpty(header.getDealerId())){
                message = "Header details: Request Type || Application Source || Product || InstitutionID || LoginId || LoginRole || DealerId should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.MEDIUM.name())
                        .build());
            }
        }

        //Checking AggregatorData
        if(StringUtils.equalsIgnoreCase(functionName, "saveMcpData")) {
            if (null == loginDataRequest.getAggregatorData()) {
                message = "Aggregator details should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.HIGH.name())
                        .fieldName("OAggregatorData")
                        .build());
            } else {
                AggregatorData aggregatorData = loginDataRequest.getAggregatorData();
                if (StringUtils.isEmpty(aggregatorData.getLoginRole())) {
                    message = "Aggregator details: LoginRole should not be empty.";
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.MEDIUM.name())
                            .build());
                }
                if (StringUtils.isEmpty(aggregatorData.getEncryptPassword()) && StringUtils.isEmpty(aggregatorData.getPassword())) {
                    message = "Aggregator details: EncryptKey and Password both should not be empty. Atleast one of them is required.";
                    errors.add(Error.builder()
                            .message(message)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.MEDIUM.name())
                            .build());
                }
            }
        }

        //Checking Registration Data
        if(null == loginDataRequest.getRegistrationData()){
            message = "Registration details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oRegistrationData")
                    .build());
        }else{
            RegistrationData registrationData = loginDataRequest.getRegistrationData();
            if(StringUtils.isEmpty(registrationData.getFirstName()) || StringUtils.isEmpty(registrationData.getEmailAddress()) || StringUtils.isEmpty(registrationData.getCity()) || StringUtils.isEmpty(registrationData.getPhoneNumber())
                    || StringUtils.isEmpty(registrationData.getLoanPurpose()) || StringUtils.isEmpty(registrationData.getChannel())){
                message = "Registration details: FirstName || MiddleName || LastName || Email || Phone || City || LoanPurpose || Channel should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.MEDIUM.name())
                        .build());
            }
        }

        //Checking EligibilityData
        if(null == loginDataRequest.getEligibilityData()){
            message = "Eligibility details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oEligibilityData")
                    .build());
        }else{
            EligibilityData eligibilityData = loginDataRequest.getEligibilityData();
            if( null == eligibilityData.getAppliedLoan() || null == eligibilityData.getIncome() || null == eligibilityData.getFixedOblgation() || null == eligibilityData.getCreditCardOutStanding()
                    || StringUtils.isEmpty(eligibilityData.getAppliedTenor()) || null == eligibilityData.getRoi() || CollectionUtils.isEmpty(eligibilityData.getEligibleOffersList())){
                message = "Eligibility details: LoanAmount || Income || FixedObligations || CreditCardOutStanding || Tenor || ROI || EligibleOffers should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.MEDIUM.name())
                        .build());
            }
        }

        //Checking PersonalDetails
        if(null == loginDataRequest.getPersonalDetails()){
            message = "Personal details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oPersonalDetails")
                    .build());
        }else{
            PersonalDetails personalDetails = loginDataRequest.getPersonalDetails();
            if(StringUtils.isEmpty(personalDetails.getDateOfBirth()) || StringUtils.isEmpty(personalDetails.getGender()) || StringUtils.isEmpty(personalDetails.getKycNumber())
                    || StringUtils.isEmpty(personalDetails.getMaritalStatus())){
                message = "Personal details: Dob || Gender || KycNumber || MarritalStatus should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.MEDIUM.name())
                        .build());
            }
        }

        //Checking Demographic Details
        if(null == loginDataRequest.getDemographicData() || CollectionUtils.isEmpty(loginDataRequest.getDemographicData().getAddress())){
            message = "Demographic details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oDemographicData")
                    .build());
        }else{
            loginDataRequest.getDemographicData().getAddress().forEach(demographicAddress -> {
                if(StringUtils.isEmpty(demographicAddress.getAddressType()) || StringUtils.isEmpty(demographicAddress.getOwnershipType()) || StringUtils.isEmpty(demographicAddress.getAddressLine1())
                        || StringUtils.isEmpty(demographicAddress.getAddressLine2()) || StringUtils.isEmpty(demographicAddress.getState()) || StringUtils.isEmpty(demographicAddress.getCity())
                        || null == demographicAddress.getTimeAtAddress() || null == demographicAddress.getPin() ) {
                    errors.add(Error.builder()
                            .message("Demographic details ["+ demographicAddress.getAddressType() +"] : AddressType || OwnershipType || TimeWithAddress || " +
                                    "AddressLine1 || AddressLine2 || Pin || City || State  should not be empty.")
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .level(Error.SEVERITY.MEDIUM.name())
                            .build());
                }
            });
        }

        //Checking Employment Details
        if(null == loginDataRequest.getEmploymentData()){
            message = "Employment details should not be empty.";
            errors.add(Error.builder()
                    .message(message)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("oEmploymentData")
                    .build());
        }else{
            EmploymentData employmentData = loginDataRequest.getEmploymentData();
            if(StringUtils.isEmpty(employmentData.getEmploymentName()) || StringUtils.isEmpty(employmentData.getEmploymentCategory()) || StringUtils.isEmpty(employmentData.getEmailAddress())
                    || null == employmentData.getTimeWithEmployer() || null == employmentData.getTotalYearsOfExperience() || StringUtils.isEmpty(employmentData.getModePayment())){
                message = "Employment details: EmploymentName || EmploymentCategory || Email || TimeWithEmployer || TotalYrsOfExp || ModeOfPayment should not be empty.";
                errors.add(Error.builder()
                        .message(message)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .level(Error.SEVERITY.MEDIUM.name())
                        .build());
            }
        }
        return errors;
    }

}