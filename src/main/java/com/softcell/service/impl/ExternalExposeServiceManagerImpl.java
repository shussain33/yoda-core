package com.softcell.service.impl;

import com.mongodb.BasicDBObject;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.AnalyticsMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.request.LOSDetailsRequest;
import com.softcell.gonogo.model.response.LOSDetailsUpdateResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.reporting.builder.ReportMongoPipeLines;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.request.DailyDisbursalRequest;
import com.softcell.service.ExternalExposeServiceManager;
import com.softcell.service.validator.ExternalExposeServiceValidationEngine;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by yogeshb on 24/5/17.
 */
@Service
public class ExternalExposeServiceManagerImpl implements ExternalExposeServiceManager {

    private static Logger logger = LoggerFactory.getLogger(ExternalExposeServiceManagerImpl.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalExposeServiceValidationEngine externalExposeServiceValidationEngine;

    @Autowired
    private ReportMongoPipeLines reportMongoPipeLines;

    @Autowired
    private AnalyticsMongoRepository analyticsMongoRepository;

    private static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();

    static {

        int index = 0;

        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("_id");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Count");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("count");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("total");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
    }



    @Override
    public BaseResponse updateOpsData(LOSDetailsRequest lOSDetailsRequest) {

        Collection<Error> errors = externalExposeServiceValidationEngine.validateLosUpdateRequest(lOSDetailsRequest.getlOSDetails());

        BaseResponse baseResponse;

        if (errors.isEmpty()) {

            if (applicationRepository.postIpaExists(lOSDetailsRequest.getRefID(), lOSDetailsRequest.getHeader().getInstitutionId())) {

                if (applicationRepository.updateOpsData(lOSDetailsRequest)) {
                    LOSDetailsUpdateResponse losDetailsUpdateResponse = new LOSDetailsUpdateResponse();
                    losDetailsUpdateResponse.setAcknowledgement(Status.SUCCESS.name());
                    losDetailsUpdateResponse.setLosID(lOSDetailsRequest.getlOSDetails().getLosID());
                    losDetailsUpdateResponse.setRefID(lOSDetailsRequest.getRefID());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, losDetailsUpdateResponse);
                } else {
                    LOSDetailsUpdateResponse losDetailsUpdateResponse = new LOSDetailsUpdateResponse();
                    losDetailsUpdateResponse.setAcknowledgement(Status.FAILED.name());
                    losDetailsUpdateResponse.setLosID(lOSDetailsRequest.getlOSDetails().getLosID());
                    losDetailsUpdateResponse.setRefID(lOSDetailsRequest.getRefID());

                    errors.add(Error.builder()
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(Error.SEVERITY.HIGH.name())
                            .message(ErrorCode.OPS_ERROR_MESSAGE)
                            .build());

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, losDetailsUpdateResponse, errors);
                }
            } else {
                errors.add(Error.builder()
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .level(Error.SEVERITY.HIGH.name())
                        .message(ErrorCode.POST_IPA_NOT_EXIST)
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.PRECONDITION_FAILED, errors);
            }

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }

        return baseResponse;

    }

    @Override
    public byte[] getDailyDisbursal(DailyDisbursalRequest dailyDisbursalRequest) throws Exception {

        Assert.notNull(dailyDisbursalRequest, "Daily Disbursal Report request must not be null or blank !!");

        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream = null;
        List<String> referenceIds = new ArrayList<>();
        Map<String,String> map = new HashMap<String,String>();

        outputStream = new ByteArrayOutputStream();
        reportOutputStreamWriter = new OutputStreamWriter(outputStream,
                StandardCharsets.UTF_8);

        try {

            Aggregation aggregation = reportMongoPipeLines.dailyDisbursalReport(dailyDisbursalRequest);

            AggregationResults<BasicDBObject> aggregationResult = analyticsMongoRepository.getDailyDisbursalReport(aggregation);

            List<BasicDBObject> mappedResult = aggregationResult.getMappedResults();

            mappedResult.forEach(mappedResults->{
                referenceIds.add(mappedResults.get("_id").toString());

                if(StringUtils.isNotBlank(mappedResults.get("branchCode").toString()) ){
                map.put(mappedResults.get("branchCode").toString(),mappedResults.get("branchName").toString());
                }

            });

            Aggregation aggregationPostIPA = reportMongoPipeLines.buildPostIpaRequest(referenceIds);

            AggregationResults<BasicDBObject> aggregationPostIPAResults = analyticsMongoRepository.getPostIPARequest(aggregationPostIPA);

            List<BasicDBObject> mappedResultPostIPA = aggregationPostIPAResults.getMappedResults();

            StringBuilder response = new StringBuilder();

            if(CollectionUtils.isNotEmpty(mappedResultPostIPA)) {
                BasicDBObject basicDBObject = mappedResultPostIPA.get(0);

                Set<String> keySet = basicDBObject.keySet();

                response.append("BRANCH NAME");
                response.append(",");
                response.append("DISBURSAL AMOUNT");
                response.append(",");
                response.append("DISBURSAL COUNT");

                reportOutputStreamWriter.write(response.toString());
                reportOutputStreamWriter.append('\n');

                for (BasicDBObject object : mappedResultPostIPA) {

                    String value = map.get(object.get("_id"));

                    if(StringUtils.isBlank(value)){
                        value="";
                    }

                    response = new StringBuilder();

                    response.append(value);

                    response.append(",");

                    response.append(object.get("total"));

                    response.append(",");

                    response.append(object.get("count"));

                    reportOutputStreamWriter.write(response.toString());

                    reportOutputStreamWriter.append('\n');

                }

            }else {
                logger.debug("no content found while fetching daily disbursal report for request [{}]"+dailyDisbursalRequest);

                return GnGFileUtils.compressBytes("DAILY_DISBURSAL_REPORT_" + GngDateUtil.getDDMMYYYYFormat(dailyDisbursalRequest.getStartDate()) + ".csv",
                        new String("No content found").getBytes());
            }

        } catch (Exception e) {
            logger.error(" error occurred  while fetching fetching daily disbursal report [{}] ", e.getMessage());

            throw new Exception(String.format("Exception while fetching daily disbursal report [%s]", e.getMessage()));
        }
        finally {
            reportOutputStreamWriter.close();
        }


        return GnGFileUtils.compressBytes("DAILY_DISBURSAL_REPORT_" + GngDateUtil.getDDMMYYYYFormat(dailyDisbursalRequest.getStartDate()) + ".csv",
                outputStream.toByteArray()
        );

    }

}
