package com.softcell.service.impl;

import com.softcell.constants.Constant;
import com.softcell.constants.csv.CSVHeaderConstant;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.gonogo.model.response.MasterRecords;
import com.softcell.service.FileParserManager;
import com.softcell.service.parser.MasterFileCSVParser;
import com.softcell.service.parser.MasterFileCSVParserImpl;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;


/**
 * @author yogeshb
 */
@Service
public class FileParserManagerImpl implements FileParserManager {
    private Logger logger = LoggerFactory
            .getLogger(FileParserManagerImpl.class);
    private MasterFileCSVParser masterFileCSVParser;

    @Override
    public FileUploadStatus parseSchemesMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseSchemeMaster(parser,institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseAssetModelMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseAssetModelMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parsePincodeMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parsePinCodeMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseEmployerMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseEmployerMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseDealerEmailMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseDealerEmailMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseSchemeModelDealerCityMappingMaster(
            MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseSchemeModelDealerCityMapping(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseCarSurrogateMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseCarSurrogateMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseHitachiSchemeMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseHitachiSchemeMaster(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

    @Override
    public FileUploadStatus parseSchemeDateMappingMaster(MultipartFile file, String institutionId) {
        FileUploadStatus fileUploadStatus = null;
        if (!file.isEmpty()
                && StringUtils.equals("text/csv", file.getContentType())) {
            MasterRecords masterRecords = null;
            CSVParser parser = null;

            try {
                // Create the CSVFormat object
                CSVFormat format = CSVFormat.RFC4180.withHeader(
                        CSVHeaderConstant.SCHEME_HEADER).withSkipHeaderRecord();

                // initialize the CSVParser object
                parser = new CSVParser(new InputStreamReader(
                        file.getInputStream()), format);

                masterFileCSVParser = new MasterFileCSVParserImpl();
                masterRecords = masterFileCSVParser.parseSchemeDateMapping(parser, institutionId);
                if (masterRecords != null
                        && masterRecords.getFileUploadStatus() != null) {
                    return masterRecords.getFileUploadStatus();
                } else {
                    fileUploadStatus = new FileUploadStatus();
                    fileUploadStatus.setStatus(Constant.FAILED);
                    fileUploadStatus.setErrorDesc("Wrong csv file uploaded, please upload valid csv file");
                    return fileUploadStatus;
                }
            } catch (Exception e) {
                logger.error("error in parsing scheme master csv");
                fileUploadStatus = new FileUploadStatus();
                fileUploadStatus.setStatus(Constant.FAILED);
                fileUploadStatus.setErrorDesc("Run time Exception: " + e.getMessage());
                return fileUploadStatus;
            } finally {
                try {
                    masterRecords = null;
                    parser.close();
                } catch (IOException e) {
                    logger.error("error in fileReader closing.");
                }
            }
        } else {
            fileUploadStatus = new FileUploadStatus();
            fileUploadStatus.setStatus(Constant.FAILED);
            fileUploadStatus.setErrorDesc("File Format not supported");
            return fileUploadStatus;
        }
    }

}
