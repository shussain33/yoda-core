package com.softcell.service.impl;

import com.softcell.config.KarzaConfiguration;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAuthenticationRequest;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaRequestData;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharAddressFreeText;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharDemographics;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharDob;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharName;
import com.softcell.utils.GngDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Gunaratna on 16/02/2019.
 * This class creates the response objects after receiving the
 * response from Karza API.
 */
@Component
public class KarzaRequestHelper {

    private static final Logger logger = LoggerFactory.getLogger(KarzaRequestHelper.class);
    private static final String errorMessage = "Error occurred while creating ";
    private static final String responseMessage = "Request Json for ";


    /**
     * creates Aadhar Request in the following JSON format:
     * {
     "data":{
     "aadhaar-id": "705868506568",
     "consent": "Y",
     "demographics": {
     "name": {
     "matching-strategy": "exact",
     "name-value": ""
     },
     "dob": {
     "format": "19950310",
     "dob-value": ""
     },
     "phone": "",
     "email": "",
     "gender": "female",
     "address-format": "freetext",
     "address-freetext": {
     "matching-strategy": "exact",
     "address-value": ""
     }
     }
     },
     "requestType": "aadhar"
     }
     * @param kyc
     * @param applicant
     * @param karzaConfiguration
     * @return
     */
    public KarzaAuthenticationRequest addAadharRequest(final Kyc kyc, final Applicant applicant, final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().aadhaarid(kyc.getKycNumber()).consent(karzaConfiguration.getConsent())
                            .demographics(AadharDemographics.builder()
                                    .name(AadharName.builder().matchingStrategy("exact").nameValue(applicant.getApplicantName().getFullName()).build())
                                    .dob(AadharDob.builder().format("19950310").dobValue(GngDateUtil.getDateInYyyyMMddFormat(applicant.getDateOfBirth())).build())
                                    .phone(applicant.getPhone().get(0).getPhoneNumber())
                                    .email(applicant.getEmail().get(0).getEmailAddress())
                                    .gender(applicant.getGender()).addressFormat("freetext")
                                    .addressFreetext(
                                            AadharAddressFreeText.builder().matchingStrategy("exact")
                                                    .addressValue(getApplicantAddress(applicant.getAddress().get(0))).build())
                                    .build()).build()).requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addPANRequest(final Kyc kyc, final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .pan(kyc.getKycNumber()).build()).requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addDLRequest(final Kyc kyc, final Applicant applicant,
                                                   final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .dl_no(kyc.getKycNumber()).dob(kyc.getDob()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addVoterIdRequest(final Kyc kyc, final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .epic_no(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addNregaResponse(final Kyc kyc, final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .jobcardid(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addPassportRequest(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            final Name applicantName = applicant.getApplicantName();
            /*return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .name(applicantName.getFirstName()+ " " +applicantName.getMiddleName())
                            .lastName(applicantName.getLastName())
                            .dob(applicant.getDateOfBirth())
                            .doe(kyc.getExpiryDate())
                            .gender(applicant.getGender())
                            .passportNo(kyc.getKycNumber())
                            .type("P")
                            .country(kyc.getCountryOfIssue()).build())
                    .requestType(kyc.getKycRequestType()).build();*/

            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .name(kyc.getName())
                            .dob(kyc.getDob())
                            .doe(kyc.getDoe())
                            .gender(applicant.getGender())
                            .passportNo(kyc.getKycNumber())
                            .type("P")
                            .country(kyc.getCountryOfIssue()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addTanAuthRequest(final Kyc kyc, final Applicant applicant,
                                                        final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .tan(applicant.getTanNo()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addTanDetailRequest(final Kyc kyc, final Applicant applicant,
                                                          final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .tan(applicant.getTanNo()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addMcaSignatoriesRequest(final Kyc kyc, final Applicant applicant,
                                                               final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .cin(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addCompanySearchRequest(final Kyc kyc, final Applicant applicant,
                                                              final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .company_name(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addCinLookup(final Kyc kyc, final Applicant applicant,
                                                   final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .cin(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addIec(final Kyc kyc, final Applicant applicant,
                                             final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .iec(kyc.getKycNumber()).build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addIecDetailed(final Kyc kyc, final Applicant applicant,
                                                     final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().iec(kyc.getKycNumber())
                            .consent(karzaConfiguration.getConsent())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addUAN(final Kyc kyc, final Applicant applicant,
                                             final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .aadhar(kyc.getKycNumber())
                            .uan(applicant.getUdyogAdhaar())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addGST(final Kyc kyc, final Applicant applicant,
                                             final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .gstin(applicant.getGstNo())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addGSTIdentification(final Kyc kyc, final Applicant applicant,
                                                           final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .input(kyc.getGstInput())
                            .state(kyc.getState())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }
    /**
     * Get the shop establishment based on reg_no and area_code.
     * @param kyc
     * @param applicant
     * @param karzaConfiguration
     * @return
     */
    public KarzaAuthenticationRequest addShopEstablishment(final Kyc kyc, final Applicant applicant,
                                                           final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .reg_no(kyc.getKycNumber())
                            .area_code(kyc.getAreaCode())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    /**
     * Get the FSSAI License Auth based on reg_no.
     * @param kyc
     * @param applicant
     * @param karzaConfiguration
     * @return
     */
    public KarzaAuthenticationRequest addFssaiLicenseAuth(final Kyc kyc, final Applicant applicant,
                                                          final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .reg_no(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    /**
     * Get the FDA License Auth based on license_no and state code.
     * @param kyc
     * @param applicant
     * @param karzaConfiguration
     * @return
     */
    public KarzaAuthenticationRequest addFdaLicenseAuth(final Kyc kyc, final Applicant applicant,
                                                        final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .licence_no(applicant.getTinNo())
                            .area_code(kyc.getAreaCode())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addCaMembershipAuth(final Kyc kyc, final Applicant applicant,
                                                          final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .membership_no(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addIcsiMembershipAuth(final Kyc kyc, final Applicant applicant,
                                                            final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .membership_no(kyc.getKycNumber())
                            .cp_no(kyc.getCpNo())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addMciMembershipAuth(final Kyc kyc, final Applicant applicant,
                                                           final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .registration_no(kyc.getKycNumber())
                            .year_of_reg(kyc.getYearOfReg())
                            .medical_council(kyc.getMedicalCouncil())
                            .consent(karzaConfiguration.getConsent())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addPngMembershipAuth(final Kyc kyc, final Applicant applicant,
                                                           final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .consumer_id(kyc.getKycNumber())
                            .bp_no(kyc.getBpNo())
                            .service_provider(kyc.getServiceProvider())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addElectricityBill(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .consumer_id(kyc.getKycNumber())
                            .service_provider(kyc.getServiceProvider())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addTeleAuth(final Kyc kyc, final Applicant applicant,
                                                  final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .tel_no(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addMobileOtpAuth(final Kyc kyc, final Applicant applicant,
                                                       final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .mobile(kyc.getKycNumber())
                            .consent(karzaConfiguration.getConsent())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addMobileAuth(final Kyc kyc, final Applicant applicant,
                                                    final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .otp(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).acknowledgementId("").build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addLpgAuth(final Kyc kyc, final Applicant applicant,
                                                 final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .lpg_id(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addEpfOtpAuth(final Kyc kyc, final Applicant applicant,
                                                    final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .uan(applicant.getUdyogAdhaar())
                            .mobile(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addEpfUanAuth(final Kyc kyc, final Applicant applicant,
                                                    final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .mobile(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addEmployerLookup(final Kyc kyc, final Applicant applicant,
                                                        final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .uan(applicant.getUdyogAdhaar())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addEsicIdLookup(final Kyc kyc, final Applicant applicant,
                                                      final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .esic_id(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addForm16(final Kyc kyc, final Applicant applicant,
                                                final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .tan(applicant.getTanNo())
                            .pan(kyc.getKycNumber())
                            .cert_no(kyc.getCertNo())
                            .amount(kyc.getAmount())
                            .fiscal_year(kyc.getFiscalYear())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addForm16Quarterly(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .tan(applicant.getTanNo())
                            .pan(kyc.getKycNumber())
                            .fiscal_year(kyc.getFiscalYear())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addItr(final Kyc kyc, final Applicant applicant,
                                             final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .pan(kyc.getKycNumber())
                            .ack(kyc.getAck())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addAddressMatching(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .address1(applicant.getAddress().get(0).getAddressLine1())
                            .address2(applicant.getAddress().get(0).getAddressLine2())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addEmail(final Kyc kyc, final Applicant applicant,
                                               final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .email(applicant.getEmail().get(0).getEmailAddress())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addNameSimilarity(final Kyc kyc, final Applicant applicant,
                                                        final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .name1(kyc.getName1())
                            .name2(kyc.getName2())
                            .type(applicant.getApplicantType())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addIfsc(final Kyc kyc, final Applicant applicant,
                                              final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .ifsc(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addBankVerification(final Kyc kyc, final Applicant applicant,
                                                          final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .ifsc(kyc.getIfsc())
                            .accountNumber(kyc.getAccountNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addHsnCode(final Kyc kyc, final Applicant applicant,
                                                 final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .hsCode(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addWebDomain(final Kyc kyc, final Applicant applicant,
                                                   final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .domain(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addVehicleRCAuth(final Kyc kyc, final Applicant applicant,
                                                       final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .reg_no(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addVehicleRCSearch(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .engine_no(kyc.getKycNumber())
                            .chassis_no(kyc.getChassis_no())
                            .state(kyc.getState())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addDetailedProfile(final Kyc kyc, final Applicant applicant,
                                                         final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .entityId(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addIbbi(final Kyc kyc, final Applicant applicant,
                                              final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .entityId(kyc.getKycNumber())
                            .nameOfCorporateDebtor(kyc.getNameOfCorporateDebtor())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addSuitFilled(final Kyc kyc, final Applicant applicant,
                                                    final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .name(kyc.getName())
                            .type(kyc.getType())
                            .entityId(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addLitigationSearch(final Kyc kyc, final Applicant applicant,
                                                          final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().consent(karzaConfiguration.getConsent())
                            .name(kyc.getName())
                            .relation(kyc.getRelation())
                            .state(kyc.getState())
                            .court_complex(kyc.getCourtComplex())
                            .results_per_page(kyc.getResultsPerPage())
                            .page_no(kyc.getPageNo())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addLitigationDetails(final Kyc kyc, final Applicant applicant,
                                                           final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder()
                            .cin(kyc.getKycNumber())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addGSTTranAPI(final Kyc kyc, final Applicant applicant, final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().gstUserName(kyc.getGstUserName())
                            .gstPassword(kyc.getGstPassword())
                            .gstin(applicant.getGstNo())
                            .consent(karzaConfiguration.getConsent())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    public KarzaAuthenticationRequest addGSTTranOTP(final Kyc kyc, final Applicant applicant,
                                                    final KarzaConfiguration karzaConfiguration) {
        try {
            return KarzaAuthenticationRequest.builder()
                    .data(KarzaRequestData.builder().gstUserName(kyc.getGstUserName())
                            .gstin(applicant.getGstNo())
                            .consent(karzaConfiguration.getConsent())
                            .build())
                    .requestType(kyc.getKycRequestType()).build();
        } catch (final Exception exception) {
            logger.error(errorMessage + kyc.getKycRequestType()+ " response", exception);
        }
        return null;
    }

    /**
     * Method to get the applicant address for Aadhar verification in format
     * FlatNo Address Line 1 Address Line 2 City State Pincode
     * @param customerAddress
     * @return
     */
    public String getApplicantAddress(final CustomerAddress customerAddress){
        return new StringBuilder().append(customerAddress.getFlatNo())
                .append(" ").append(customerAddress.getAddressLine1())
                .append(" ").append(customerAddress.getAddressLine2())
                .append(" ").append(customerAddress.getCity())
                .append(" ").append(customerAddress.getState())
                .append(" ").append(customerAddress.getPin()).toString();

    }
}
