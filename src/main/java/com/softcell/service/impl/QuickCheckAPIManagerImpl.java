package com.softcell.service.impl;

import com.softcell.constants.ActionName;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Institute;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.quickcheck.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.service.QuickCheckAPIManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.executors.intimation.IntimationExecutor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by ssg0302 on 27/8/19.
 */
@Service
public class QuickCheckAPIManagerImpl implements QuickCheckAPIManager {

    private static final Logger logger = LoggerFactory.getLogger(QuickCheckAPIManagerImpl.class);

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    public QuickCheckAPIManagerImpl(){
        if(null == httpTransportationService)
            httpTransportationService = new HttpTransportationService();

        if(null == workFlowCommunicationManager)
            workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();

        if(null == auditHelper)
            auditHelper = new AuditHelper();

        if (null == activityEventPublisher)
            activityEventPublisher = GoNoGoEventContext.getApplicationEventPublisher();

        if(null == applicationRepository)
            applicationRepository = new ApplicationMongoRepository();

        if(null == lookupService)
            lookupService = new LookupServiceHandler();

    }

    @Override
    public BaseResponse createEMandate(QuickCheckAPIRequest quickCheckAPIRequest) {
        logger.debug("createEMandate started for {} refId", quickCheckAPIRequest.getRefId());
        BaseResponse baseResponse = null;
        try {
            baseResponse = processRequest(quickCheckAPIRequest, null, EMandateConstants.CREATE_MANDATE);
        }catch(Exception e){
            logger.error("refId:{} Exception caught in createEMandate method: msg:{} stacktrace:{} "
                    , quickCheckAPIRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("createEMandate ended for {} refId", quickCheckAPIRequest.getRefId());
        return baseResponse;
    }

    @Override
    public BaseResponse processEMandate(QuickCheckAPIRequest quickCheckAPIRequest) {
        logger.debug("processEMandate started for {} refId and {} acknowledgementId", quickCheckAPIRequest.getRefId(), quickCheckAPIRequest.getRequest().getAcknowledgementId());
        BaseResponse baseResponse = null;
        try {
            EMandateDetails eMandateDetails =  applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());
            baseResponse  = checkEMandateDetails(quickCheckAPIRequest, eMandateDetails);
            if (null != baseResponse) return baseResponse;

            baseResponse = processRequest(quickCheckAPIRequest, eMandateDetails, EMandateConstants.PROCESS_MANDATE);
        }catch(Exception e){
            logger.error("refId:{} Exception caught in processEMandate method: msg:{} stacktrace:{} "
                    , quickCheckAPIRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("processEMandate ended for {} refId and {} acknowledgementId", quickCheckAPIRequest.getRefId(), quickCheckAPIRequest.getRequest().getAcknowledgementId());
        return baseResponse;
    }

    @Override
    public BaseResponse callBackEMandate(EMandateCallBackRequest eMandateCallBackRequest) throws Exception{
        logger.debug("callBackEMandate started for {} acknowledgementId and {} status", eMandateCallBackRequest.getAcknowledgementId(), eMandateCallBackRequest.getStatus());
        try{
            EMandateDetails eMandateDetails =  applicationRepository.fetchEMandateDetailsByAcknowledgementId(eMandateCallBackRequest.getAcknowledgementId());
            if(null == eMandateDetails)
                return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No EMandateDetails found for requested acknowledgementId");

            String requestType = EMandateConstants.CALLBACK_MANDATE;
            ProcessedRequests processedRequest=eMandateDetails.getProcessedRequests().stream()
                    .filter(result -> StringUtils.equalsIgnoreCase(requestType, result.getRequestType()))
                    .findAny().orElse(null);

            if(null == processedRequest){
                processedRequest = new ProcessedRequests();
                processedRequest.setRequestType(requestType);
                processedRequest.setEmandateCallBackRequest(eMandateCallBackRequest);
                eMandateDetails.getProcessedRequests().add(processedRequest);
            }else{
                processedRequest.setEmandateCallBackRequest(eMandateCallBackRequest);
            }

            eMandateDetails.setEmandateCallBackResult(eMandateCallBackRequest.getStatus());
            applicationRepository.updateEMandateDetails(eMandateDetails);

            auditEMandateDetails(null, eMandateCallBackRequest, eMandateDetails, null, requestType);
        }catch(Exception e){
            logger.error("acknowledgementId:{} Exception caught in callBackEMandate method: msg:{} stacktrace:{} "
                    , eMandateCallBackRequest.getAcknowledgementId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("callBackEMandate ended for {} acknowledgementId and {} status", eMandateCallBackRequest.getAcknowledgementId(), eMandateCallBackRequest.getStatus());
        return GngUtils.getBaseResponse(HttpStatus.OK, eMandateCallBackRequest);
    }

    @Override
    public BaseResponse getEMandateStatus(QuickCheckAPIRequest quickCheckAPIRequest) throws Exception{
        logger.debug("checkEMandateStatus started for {} refId and {} acknowledgementId", quickCheckAPIRequest.getRefId(), quickCheckAPIRequest.getRequest().getAcknowledgementId());
        BaseResponse baseResponse = null;
        try {
            EMandateDetails eMandateDetails = applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());
            baseResponse  = checkEMandateDetails(quickCheckAPIRequest, eMandateDetails);
            if (null != baseResponse) return baseResponse;

            baseResponse = processRequest(quickCheckAPIRequest, eMandateDetails, EMandateConstants.GET_MANDATE_STATUS);
        }catch(Exception e){
            logger.error("refId:{} Exception caught in checkEMandateStatus method: msg:{} stacktrace:{} "
                    , quickCheckAPIRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("checkEMandateStatus ended for {} refId and {} acknowledgementId", quickCheckAPIRequest.getRefId(), quickCheckAPIRequest.getRequest().getAcknowledgementId());
        return baseResponse;
    }

    @Override
    public BaseResponse resendLink(QuickCheckAPIRequest quickCheckAPIRequest){
        logger.debug("resendLink started for refId {}", quickCheckAPIRequest.getRefId());
        BaseResponse baseResponse = null;
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(quickCheckAPIRequest.getRefId());

            if(null == goNoGoCustomerApplication)
                return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No goNoGoCustomer details found for requested refId");

            String requestType = EMandateConstants.RESEND_MANDATE_DETAILS;
            quickCheckAPIRequest.setRequestType(requestType);

            EMandateDetails eMandateDetails =  applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());
            baseResponse  = checkEMandateDetails(quickCheckAPIRequest, eMandateDetails);
            if (null != baseResponse) return baseResponse;

            ProcessedRequests processedRequests = eMandateDetails.getProcessedRequests().stream()
                    .filter(request -> StringUtils.equalsIgnoreCase(EMandateConstants.PROCESS_MANDATE, request.getRequestType()))
                    .findAny().orElse(null);

            intimateCustomer(quickCheckAPIRequest, goNoGoCustomerApplication, eMandateDetails, processedRequests);

            applicationRepository.updateEMandateDetails(eMandateDetails);
            auditEMandateDetails(quickCheckAPIRequest, null, eMandateDetails, null, requestType);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, "Link successfully sent to Customer for redId " + quickCheckAPIRequest.getRefId());
        }catch(Exception e){
            logger.error("refId:{} Exception caught in resendLink method: msg:{} stacktrace:{} "
                    , quickCheckAPIRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("resendLink ended for refId {}", quickCheckAPIRequest.getRefId());
        return baseResponse;
    }

    private BaseResponse checkEMandateDetails(QuickCheckAPIRequest quickCheckAPIRequest, EMandateDetails eMandateDetails) throws Exception{
        BaseResponse baseResponse = null;
        if(null == eMandateDetails)
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No EMandateDetails found for requested refId");
        else if (!StringUtils.equals(eMandateDetails.getAcknowledgementId(), quickCheckAPIRequest.getRequest().getAcknowledgementId()))
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No data found for requested AcknowledgementId");
        else if(StringUtils.equalsIgnoreCase(quickCheckAPIRequest.getRequestType(), EMandateConstants.RESEND_MANDATE_DETAILS)){
            String masterValue = lookupService.getMasterValue(quickCheckAPIRequest.getHeader().getInstitutionId(), ActionName.RESEND_MAX_RETRIES_COUNT);
            int retryCnt = StringUtils.isEmpty(masterValue) ? 2 : Integer.valueOf(masterValue);
            if(eMandateDetails.getResendCount() >= retryCnt)
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, ErrorCode.NO_OF_HITS_EXCEEDED);
        }
        return baseResponse;
    }

    private BaseResponse processRequest(QuickCheckAPIRequest quickCheckAPIRequest, EMandateDetails eMandateDetails, String requestType)throws Exception {
        BaseResponse baseResponse = null;
        WFJobCommDomain wfJobCommDomain = getCommunicationSettings(quickCheckAPIRequest);
        if(null == wfJobCommDomain){
            logger.info("institutionID {} and Product {} and requestType {} No Configuration Found against.",
                    quickCheckAPIRequest.getHeader().getInstitutionId(),
                    quickCheckAPIRequest.getHeader().getProduct().toProductName(),
                    requestType);
            return  GngUtils.getBaseResponse(HttpStatus.OK, "No Configuration Found against this institutionID and Product");
        }else {
            String response = callSmartInterface(quickCheckAPIRequest, wfJobCommDomain, requestType);
            EMandateResponse eMandateResponse = getEMandateResponse(response);
            saveEMandateDetails(quickCheckAPIRequest, eMandateDetails, eMandateResponse, requestType);

            eMandateDetails = applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());
            if(null != eMandateDetails)
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, eMandateDetails);
            else
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No data found for request");

            auditEMandateDetails(quickCheckAPIRequest, null, eMandateDetails, eMandateResponse, requestType);
        }
        return  baseResponse;
    }

    private WFJobCommDomain getCommunicationSettings(QuickCheckAPIRequest quickCheckAPIRequest){
        String institutionId = quickCheckAPIRequest.getHeader().getInstitutionId();
        String product = quickCheckAPIRequest.getHeader().getProduct().name();

        return  workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product, UrlType.QUICKCHECK.toValue());
    }

    private String callSmartInterface(QuickCheckAPIRequest quickCheckAPIRequest, WFJobCommDomain wfJobCommDomain, String requestType) throws Exception{
        String url = Arrays.asList(wfJobCommDomain.getBaseUrl(), requestType)
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        quickCheckAPIRequest.setCallbackUrl(wfJobCommDomain.getEndpoint());
        logger.debug("Calling request {} ", JsonUtil.ObjectToString(quickCheckAPIRequest));
        logger.debug("Calling to SSL via {} url ", url);

        HashMap<String, String> headerParams = getSSLHeader(wfJobCommDomain, quickCheckAPIRequest.getRefId());
        return httpTransportationService.postRequestSSL(url, JsonUtil.ObjectToString(quickCheckAPIRequest), headerParams, MediaType.APPLICATION_JSON_VALUE);
    }

    private HashMap<String, String> getSSLHeader(WFJobCommDomain wfJobCommDomain, String refId) {
        HashMap<String, String> headerParam = new HashMap<>();
        if(wfJobCommDomain != null){
            headerParam.put(QuickCheckAPIRequestKeyHeader.LOGIN_ID, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), QuickCheckAPIRequestKeyHeader.LOGIN_ID));
            headerParam.put(QuickCheckAPIRequestKeyHeader.PASSWORD, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), QuickCheckAPIRequestKeyHeader.PASSWORD));
            headerParam.put(QuickCheckAPIRequestKeyHeader.INSTITUTION_NAME, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), QuickCheckAPIRequestKeyHeader.INSTITUTION_NAME));
            headerParam.put(QuickCheckAPIRequestKeyHeader.PRODUCT, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), QuickCheckAPIRequestKeyHeader.PRODUCT));
            headerParam.put(QuickCheckAPIRequestKeyHeader.APPLICATION_ID, refId);
            headerParam.put(QuickCheckAPIRequestKeyHeader.SOURCE_SYSTEM, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), QuickCheckAPIRequestKeyHeader.SOURCE_SYSTEM));
        }
        return  headerParam;
    }

    private EMandateResponse getEMandateResponse(String response) throws Exception{
        logger.debug("Response String {}", response);
        EMandateResponse eMandateResponse = null;
        try {
            eMandateResponse = JsonUtil.StringToObject(response, EMandateResponse.class);
        }catch (Exception e){
            logger.error("Error while mapping EMandateResponse response {} Exception {}", response, ExceptionUtils.getStackTrace(e));
        }
        return eMandateResponse;
    }

    private void saveEMandateDetails(QuickCheckAPIRequest quickCheckAPIRequest, EMandateDetails eMandateDetails, EMandateResponse eMandateResponse, String requestType)throws Exception{
        logger.debug("saveEMandateDetails started for refId {} and RequestType {}", quickCheckAPIRequest.getRefId(), requestType);
        ActivityLogs activityLogs = auditHelper.createActivityLog(quickCheckAPIRequest.getHeader(),
                quickCheckAPIRequest.getRefId(), null,
                null, requestType, null, quickCheckAPIRequest.getHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(quickCheckAPIRequest.getRefId());
        if(null != goNoGoCustomerApplication) {
            if (null == eMandateDetails)
                eMandateDetails = applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());

            if (StringUtils.equalsIgnoreCase(requestType, EMandateConstants.CREATE_MANDATE)) {
                if (null == eMandateDetails) {
                    eMandateDetails = new EMandateDetails();
                    eMandateDetails.setRefId(quickCheckAPIRequest.getRefId());
                }

                eMandateDetails.setHeader(quickCheckAPIRequest.getHeader());
                eMandateDetails.getProcessedRequests().clear();
                eMandateDetails.setAcknowledgementId(null);
                eMandateDetails.setEmandateURL(null);
                eMandateDetails.setEmandateStatus(null);
                eMandateDetails.setEmandateCallBackResult(null);
                eMandateDetails.setResendCount(0);
                eMandateDetails.setBankName(quickCheckAPIRequest.getBankName());

                ProcessedRequests processedRequest = mapProcessedRequest(quickCheckAPIRequest, eMandateResponse, requestType);
                eMandateDetails.getProcessedRequests().add(processedRequest);

                if (null != eMandateResponse)
                    eMandateDetails.setAcknowledgementId(eMandateResponse.getAcknowledgementId());

                applicationRepository.updateEMandateDetails(eMandateDetails);
            } else if (StringUtils.equalsIgnoreCase(requestType, EMandateConstants.PROCESS_MANDATE)
                    || StringUtils.equalsIgnoreCase(requestType, EMandateConstants.GET_MANDATE_STATUS)) {
                ProcessedRequests processedRequests = eMandateDetails.getProcessedRequests().stream()
                        .filter(request -> StringUtils.equalsIgnoreCase(requestType, request.getRequestType()))
                        .findAny().orElse(null);

                if (null == processedRequests) {
                    processedRequests = mapProcessedRequest(quickCheckAPIRequest, eMandateResponse, requestType);
                    eMandateDetails.getProcessedRequests().add(processedRequests);
                } else {
                    processedRequests.setEmandateRequest(quickCheckAPIRequest.getRequest());
                    processedRequests.setEmandateResponse(eMandateResponse);
                }
                if(StringUtils.equalsIgnoreCase(requestType, EMandateConstants.PROCESS_MANDATE) && null != eMandateResponse && null != eMandateResponse.getPayload()) {
                    eMandateDetails.setEmandateURL(eMandateResponse.getPayload().getEmandateURL()); //in case of ProcessMandate API
                }
                if(StringUtils.equalsIgnoreCase(requestType, EMandateConstants.GET_MANDATE_STATUS) && null != eMandateResponse && null != eMandateResponse.getPayload()) {
                    eMandateDetails.setEmandateStatus(eMandateResponse.getPayload().getMandateStatus()); // in case of getMandateStatus API
                }
                applicationRepository.updateEMandateDetails(eMandateDetails);

                /*
                Because of Customer requirement below code is Commented.
                if(StringUtils.equalsIgnoreCase(requestType, EMandateConstants.PROCESS_MANDATE) && null != processedRequests.getEmandateResponse()
                        && null != processedRequests.getEmandateResponse().getPayload()
                        && StringUtils.equalsIgnoreCase(processedRequests.getEmandateResponse().getPayload().getStatus(), GNGWorkflowConstant.SUCCESS.name()))
                intimateCustomer(quickCheckAPIRequest, goNoGoCustomerApplication, eMandateDetails, processedRequests);*/

            }
            applicationRepository.updateCompletedInfo(goNoGoCustomerApplication,
                    EMandateConstants.SAVE_ESIGN_ENACH_DATA, quickCheckAPIRequest.getHeader().getLoggedInUserId(), quickCheckAPIRequest.getHeader().getLoggedInUserRole());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from QuickCheckManager in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        logger.debug("saveEMandateDetails ended for refId {} and RequestType {}", quickCheckAPIRequest.getRefId(), requestType);
    }

    private ProcessedRequests mapProcessedRequest(QuickCheckAPIRequest quickCheckAPIRequest, EMandateResponse eMandateResponse, String requestType){
        return new ProcessedRequests().builder()
                .requestType(requestType)
                .emandateRequest(quickCheckAPIRequest.getRequest())
                .emandateResponse(eMandateResponse)
                .bankName(quickCheckAPIRequest.getBankName())
                .institutionName(quickCheckAPIRequest.getInstitutionName())
                .transactionId(quickCheckAPIRequest.getTransactionId())
                .callDate(new Date())
                .build();
    }

    private void auditEMandateDetails(QuickCheckAPIRequest quickCheckAPIRequest, EMandateCallBackRequest eMandateCallBackRequest, EMandateDetails eMandateDetails, EMandateResponse eMandateResponse, String requestType){
        try {
            if(null == quickCheckAPIRequest) {
                quickCheckAPIRequest = new QuickCheckAPIRequest();
                quickCheckAPIRequest.setRefId(eMandateDetails.getRefId());
                quickCheckAPIRequest.setHeader(eMandateDetails.getHeader());
            }
            logger.debug("auditEMandateDetails started for refId {} and requestType {} ", quickCheckAPIRequest.getRefId(), requestType);
            EMandateDetailsCallLog eMandateDetailsCallLog = new EMandateDetailsCallLog();

            BeanUtils.copyProperties(eMandateDetailsCallLog, quickCheckAPIRequest);

            eMandateDetailsCallLog.setRequestType(requestType);
            eMandateDetailsCallLog.setEmandateRequest(quickCheckAPIRequest.getRequest());
            eMandateDetailsCallLog.setEmandateResponse(eMandateResponse);
            eMandateDetailsCallLog.setEmandateCallBackRequest(eMandateCallBackRequest);

            if (null != eMandateResponse) {
                eMandateDetailsCallLog.setAcknowledgementId(eMandateResponse.getAcknowledgementId());
                if(null != eMandateResponse.getPayload())
                    eMandateDetailsCallLog.setEmandateURL(eMandateResponse.getPayload().getEmandateURL());
            }
            if (null != eMandateCallBackRequest){
                eMandateDetailsCallLog.setAcknowledgementId(eMandateCallBackRequest.getAcknowledgementId());
                eMandateDetailsCallLog.setEmandateResult(eMandateCallBackRequest.getStatus());
            }

            applicationRepository.saveEMandateDetailsCallLog(eMandateDetailsCallLog);
        }catch (Exception e){
            logger.error("Error occurred while auditEMandateDetails for refId {} and requestType {}, Exception {} ", quickCheckAPIRequest.getRefId(),  requestType, ExceptionUtils.getStackTrace(e));
        }
        logger.debug("auditEMandateDetails ended for refId {} ", quickCheckAPIRequest.getRefId());
    }

    private void intimateCustomer(QuickCheckAPIRequest quickCheckAPIRequest, GoNoGoCustomerApplication goNoGoCustomerApplication, EMandateDetails eMandateDetails, ProcessedRequests processedRequests){
        logger.debug("intimateCustomer started for refId {} and AcknowledgementId {}", quickCheckAPIRequest.getRefId(), quickCheckAPIRequest.getRequest().getAcknowledgementId());
        boolean cntFlag = false;
        try{
            if (lookupService.checkActionsAccess(
                    quickCheckAPIRequest.getHeader().getInstitutionId()
                    , quickCheckAPIRequest.getHeader().getProduct().toProductId()
                    , ActionName.SEND_ESIGN_ENACH_SMS)) {

                SmsTemplateConfiguration smsTemplateConfiguration =
                        lookupService.getSmsTemplateConfiguration(
                                quickCheckAPIRequest.getHeader().getInstitutionId()
                                , quickCheckAPIRequest.getHeader().getProduct().toProductId()
                                , GNGWorkflowConstant.ESIGN_ENACH_SMS.name());
                String smsMessage = null;
                if (smsTemplateConfiguration != null) {

                    smsMessage = smsTemplateConfiguration.getFormattedSMSText(
                            quickCheckAPIRequest.getRefId(),
                            eMandateDetails.getEmandateURL());
                } else {
                    smsMessage = String.format("Dear Customer, Mandate reference No: %s issued to"
                                    + Institute.getInstitute(quickCheckAPIRequest.getHeader().getInstitutionId()).name() +"Bank with" +
                                    " reference Loan No : %s . Click on URL to proceed further %s .",
                            processedRequests.getEmandateResponse().getPayload().getMandateId(),
                            quickCheckAPIRequest.getRefId(),
                            eMandateDetails.getEmandateURL());
                }
                logger.debug("smsMessage for refId {} is : {}", quickCheckAPIRequest.getRefId(), smsMessage);
                BaseResponse smsResponse = clientAuthenticationManager.sendSMS(
                        goNoGoCustomerApplication.getApplicationRequest(), smsMessage);

                cntFlag = true;
                logger.debug("sms sending status for refId {}  is: {}", quickCheckAPIRequest.getRefId(), smsResponse.getStatus());
            }
        }catch (Exception e){
            logger.error("Error occurred while sending sms for refId {}, Exception {} ", quickCheckAPIRequest.getRefId(),  ExceptionUtils.getStackTrace(e));
        }
        try {
            if (lookupService.checkActionsAccess(
                    quickCheckAPIRequest.getHeader().getInstitutionId()
                    , quickCheckAPIRequest.getHeader().getProduct().toProductId()
                    , ActionName.SEND_ESIGN_ENACH_EMAIL)) {

                IntimationExecutor intimationExecutor = new IntimationExecutor();
                intimationExecutor.process(goNoGoCustomerApplication, null, null, null, null, null);
                intimationExecutor.intimateGoNoGoCustomer(GNGWorkflowConstant.ESIGN_ENACH_MAIL.name());
                cntFlag = true;
            }
        }catch (Exception e){
            logger.error("Error occurred while sending mail for refId {}, Exception {} ", quickCheckAPIRequest.getRefId(),  ExceptionUtils.getStackTrace(e));
        }
        if(cntFlag && StringUtils.equalsIgnoreCase(quickCheckAPIRequest.getRequestType(), EMandateConstants.RESEND_MANDATE_DETAILS))
            eMandateDetails.setResendCount(eMandateDetails.getResendCount()+1);

        logger.debug("intimateCustomer ended for refId {} ", quickCheckAPIRequest.getRefId());
    }

    @Override
    public BaseResponse fetchEMandateDetails(QuickCheckAPIRequest quickCheckAPIRequest) {
        logger.debug("fetchEMandateDetails started for {} refId", quickCheckAPIRequest.getRefId());
        BaseResponse baseResponse = null;
        try {
            EMandateDetails eMandateDetails = applicationRepository.fetchEMandateDetails(quickCheckAPIRequest.getRefId());
            if(null == eMandateDetails)
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "No EMandateDetails found for requested refId");
            else
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, eMandateDetails);

        }catch(Exception e){
            logger.error("refId:{} Exception caught in fetchEMandateDetails method: msg:{} stacktrace:{} "
                    , quickCheckAPIRequest.getRefId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("fetchEMandateDetails ended for {} refId", quickCheckAPIRequest.getRefId());
        return baseResponse;
    }
}
