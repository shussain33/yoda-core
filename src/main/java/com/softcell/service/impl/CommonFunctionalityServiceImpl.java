package com.softcell.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.constants.CustomHttpStatus;
import com.softcell.dao.mongodb.repository.CommonFunctionalityRepository;
import com.softcell.dao.mongodb.repository.CommonFunctionalityService;
import com.softcell.gonogo.model.core.CollectionConfig;
import com.softcell.gonogo.model.request.CollectionConfigRequest;
import com.softcell.gonogo.model.request.CollectionDataRequest;
import com.softcell.gonogo.model.response.CollectionDataResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.utils.GngUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Service
public class CommonFunctionalityServiceImpl implements CommonFunctionalityService {

    private static Logger logger = LoggerFactory.getLogger(CommonFunctionalityServiceImpl.class);

    @Autowired
    private CommonFunctionalityRepository commonFunctionalityRepository;

    @Override
    public BaseResponse getData(CollectionDataRequest collectionDataRequest) {
        CollectionDataResponse response = new CollectionDataResponse();
        try {
            Class <?> className = getClassName(collectionDataRequest);
            List<?> dataList = commonFunctionalityRepository.fetchCollectionData(className, collectionDataRequest);
            response.setDataList(dataList);
        } catch (ClassNotFoundException e) {
            logger.error("Error occurred while finding class of respective collection ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR, CustomHttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("Error occurred while getting data from collection ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,CustomHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, response);
    }

    @Override
    public BaseResponse saveData (CollectionDataRequest collectionDataRequest) {
        commonFunctionalityRepository.saveDataInCollection(collectionDataRequest.getData(), collectionDataRequest.getCollectionName());

        return GngUtils.getBaseResponse(HttpStatus.OK, "");
    }

    private Class<?> getClassName(CollectionDataRequest collectionDataRequest) throws ClassNotFoundException {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Document.class));
        Map<String, Object> annotationMap;

        for (BeanDefinition beanDef : scanner.findCandidateComponents("com.softcell.gonogo.model")) {
            annotationMap = ((ScannedGenericBeanDefinition)beanDef).getMetadata().getAnnotationAttributes("org.springframework.data.mongodb.core.mapping.Document");
            if (null != annotationMap && annotationMap.get("collection").toString().equalsIgnoreCase(collectionDataRequest.getCollectionName())) {
                return Class.forName(beanDef.getBeanClassName());
            }
        }
        throw new ClassNotFoundException();
    }

    @Override
    public BaseResponse saveCollectionConfiguration(CollectionConfigRequest request) {
        try {
            for (CollectionConfig config : request.getConfigList()) {
                if (commonFunctionalityRepository.insertCollectionConfig(config)) {
                    config.setUpdateStatus("Existing config of " + config.getCollectionName() + " updated!");
                } else {
                    config.setUpdateStatus("Config of " + config.getCollectionName() + " inserted!");
                }
            }
            return GngUtils.getBaseResponse(HttpStatus.OK, request);

        } catch (Exception e) {
            logger.error("Error occurred while saving collection configuration ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.OK,CustomHttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public BaseResponse getCollectionConfiguration() {
        try{
            List<CollectionConfig> configList = commonFunctionalityRepository.fetchCollectionConfig();
            return GngUtils.getBaseResponse(HttpStatus.OK,configList);
        } catch (Exception e) {
            logger.error("Error occurred while fetching collection configuration ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.OK,CustomHttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public BaseResponse getDownloadedJsonData(CollectionDataRequest collectionDataRequest) {

        com.softcell.gonogo.model.response.Document document=new com.softcell.gonogo.model.response.Document();
        CollectionDataResponse response = new CollectionDataResponse();
        String base64String=null;
        try {
            Class<?> className = getClassName(collectionDataRequest);
            List<?> dataList = commonFunctionalityRepository.fetchCollectionData(className, collectionDataRequest);
            if (CollectionUtils.isNotEmpty(dataList)) {
                response.setDataList(dataList);
                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);

                byte[] bytes = jsonString.getBytes(StandardCharsets.UTF_8);
                if (null != bytes) {
                    base64String = new String(Base64.encodeBase64(bytes));
                }
                document.setByteCode(base64String);
                document.setDocName(collectionDataRequest.getCollectionName() + ".json");

            }
        }catch (ClassNotFoundException e) {
            logger.error("Error occurred while finding class of respective collection ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,CustomHttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("Error occurred while getting data from collection ", e.getMessage());
            return GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR,CustomHttpStatus.INTERNAL_SERVER_ERROR);
        }
        return GngUtils.getBaseResponse(HttpStatus.OK, document);

    }
}
