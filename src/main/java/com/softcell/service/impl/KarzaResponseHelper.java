package com.softcell.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.softcell.gonogo.model.core.kyc.response.karza.*;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.UdyogAadharNumberResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.addressmappingauthentication.AddressMatchingResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.BankAccountVerificationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.HsnCodeCheckResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.IfscCodeCheckResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification.CompanyIdentificationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification.LlpIdentificationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication.EmailVerificationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.EpfUanLookupResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.ItrAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.employerlookup.EmployerLookupResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.esicidauthentication.EsicIdAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16authentication.Form16AuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16quaterlyauthentication.QuarterlyRecordsCountForNextFiscalResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.otpauthentication.OtpResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication.PassbookResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.*;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication.GspGstinAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication.GstTransactionApiAveragesResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication.GstTransactionApiBusinessSummryResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication.GstTransactionApiResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication.GstTransactionStateWiseResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.icsiuthentication.IcsiMembershipAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication.IcwaiFirmAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication.IcwaiMembershipAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication.IecDetailedProfileResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication.IecResultResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.kscan.*;
import com.softcell.gonogo.model.core.kyc.response.karza.mciauthentication.MciMembershipAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.nregaauthentication.NregaResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.LpgIdAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication.MobileDetailsResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication.MobileOtpResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.PngAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.TelephoneBillAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.vehicleauthentication.VehicleRCAuthenticationResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication.WebsiteDomainAuthenticationResponse;
import com.softcell.gonogo.model.kyc.request.PassportResponses.PassportResponse;
import com.softcell.gonogo.model.kyc.request.tanResponses.TanAuthResponse;
import com.softcell.gonogo.model.kyc.request.tanResponses.TanDetailResponse;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Gunaratna on 16/02/2019.
 * This class creates the response objects after receiving the
 * response from Karza API.
 */
@Component
public class KarzaResponseHelper {

    private static final Logger logger = LoggerFactory.getLogger(KarzaResponseHelper.class);
    private static final String errorMessage = "Error occurred while creating ";
    private static final String responseMessage = "Response Json for ";

    public KarzaClientResponse addAadharResponse(final KarzaClientResponse karzaClientResponse,
                                                 final String tempResponse) {
        try {
            AadharNumberResponse aadharNumberResponse = JsonUtil.StringToObject(tempResponse, AadharNumberResponse.class);
            if (aadharNumberResponse.getPayload() != null) {
                karzaClientResponse.setKarzaStatus(KarzaHelper.VERIFIED);
                karzaClientResponse.setAadharNumberResponseDetails(aadharNumberResponse.getPayload());
            } else if (aadharNumberResponse.getError() != null
                    || CollectionUtils.isNotEmpty(aadharNumberResponse.getErrors())) {
                karzaClientResponse.setKarzaStatus(KarzaHelper.NOT_VERIFIED);
                karzaClientResponse.setErrorList(aadharNumberResponse.getErrors());
                karzaClientResponse.setError(aadharNumberResponse.getError());
            }
            logger.info(responseMessage + "AADHAR :{}", aadharNumberResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "aadhar response", exception);
        }
        return karzaClientResponse;

    }

    public KarzaClientResponse addPANResponse(final KarzaClientResponse karzaClientResponse,
                                              final String tempResponse) {
        try {
            PanResponse panResponse = JsonUtil.StringToObject(tempResponse, PanResponse.class);
            if (panResponse.getPayload() != null) {
                karzaClientResponse.setPanAuthResponse(panResponse.getPayload());
            } else if (panResponse.getError() != null|| CollectionUtils.isNotEmpty(panResponse.getErrors())) {
                karzaClientResponse.setErrorList(panResponse.getErrors());
                karzaClientResponse.setError(panResponse.getError());
            }
            logger.info(responseMessage + "PAN :{}", panResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "pan response", exception);
        }
        return karzaClientResponse;

    }

    public KarzaClientResponse addDLResponse(final KarzaClientResponse karzaClientResponse,
                                             final String tempResponse) {
        try {
            DrivingLicenceResponse drivingLicenceResponse = JsonUtil.StringToObject(tempResponse, DrivingLicenceResponse.class);
            if (drivingLicenceResponse.getPayload() != null) {
                karzaClientResponse.setDrivingLicenceAuthResponse(drivingLicenceResponse.getPayload());
            } else if (drivingLicenceResponse.getError() != null || CollectionUtils.isNotEmpty(drivingLicenceResponse.getErrors())) {
                karzaClientResponse.setErrorList(drivingLicenceResponse.getErrors());
                karzaClientResponse.setError(drivingLicenceResponse.getError());
            }
            logger.info(responseMessage + "DRIVINGLICENSE :{}", drivingLicenceResponse);


        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "DrivingLicenceResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addVoterIdResponse(final KarzaClientResponse karzaClientResponse,
                                                  final String tempResponse) {
        try {
            VoterIdResponse voterIdResponse = JsonUtil.StringToObject(tempResponse, VoterIdResponse.class);
            if (voterIdResponse.getPayload() != null) {
                karzaClientResponse.setVoterIdAuthResponse(voterIdResponse.getPayload());
            } else if (voterIdResponse.getError() != null || CollectionUtils.isNotEmpty(voterIdResponse.getErrors())) {
                karzaClientResponse.setErrorList(voterIdResponse.getErrors());
                karzaClientResponse.setError(voterIdResponse.getError());
                String errors = null;
            }
            logger.info(responseMessage + "VOTERID :{}", voterIdResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "VoterIdResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addNregaResponse(final KarzaClientResponse karzaClientResponse,
                                                final String tempResponse) {
        try {
            NregaResponse nregaResponseDetails = JsonUtil.StringToObject(tempResponse, NregaResponse.class);
            if (nregaResponseDetails.getPayload() != null) {
                karzaClientResponse.setNregaResponseDetails(nregaResponseDetails.getPayload());
            } else if (nregaResponseDetails.getError() != null || CollectionUtils.isNotEmpty(nregaResponseDetails.getErrors())) {
                karzaClientResponse.setErrorList(nregaResponseDetails.getErrors());
                karzaClientResponse.setError(nregaResponseDetails.getError());
            }
            logger.info(responseMessage + "NREGA :{}", nregaResponseDetails);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "NregaResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addPassportResponse(final KarzaClientResponse karzaClientResponse,
                                                   final String tempResponse) {
        try {
            PassportResponse passportResponse = JsonUtil.StringToObject(tempResponse, PassportResponse.class);
            if (passportResponse.getPayload() != null) {
                karzaClientResponse.setKPassportResult(passportResponse.getPayload());
            } else if (passportResponse.getError() != null || CollectionUtils.isNotEmpty(passportResponse.getErrors())) {
                karzaClientResponse.setErrorList(passportResponse.getErrors());
                karzaClientResponse.setError(passportResponse.getError());
            }
            logger.info(responseMessage + "PASSPORT :{}", passportResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "PassportResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addTANAuthResponse(final KarzaClientResponse karzaClientResponse,
                                                  final String tempResponse) {
        try {
            TanAuthResponse tanAuthResponse = JsonUtil.StringToObject(tempResponse, TanAuthResponse.class);
            if (tanAuthResponse.getPayload() != null) {
                karzaClientResponse.setKTanAuthResult(tanAuthResponse.getPayload());
            } else if (tanAuthResponse.getError() != null || CollectionUtils.isNotEmpty(tanAuthResponse.getErrors())) {
                karzaClientResponse.setErrorList(tanAuthResponse.getErrors());
                karzaClientResponse.setError(tanAuthResponse.getError());
            }
            logger.info(responseMessage + "TANAUTH :{}", tanAuthResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "TanAuthResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addTANDetailResponse(final KarzaClientResponse karzaClientResponse,
                                                    final String tempResponse) {
        try {
            TanDetailResponse tanDetailResponse = JsonUtil.StringToObject(tempResponse, TanDetailResponse.class);
            if (tanDetailResponse.getPayload() != null) {
                karzaClientResponse.setKTanDetailResult(tanDetailResponse.getPayload());
            } else if (tanDetailResponse.getError() != null || CollectionUtils.isNotEmpty(tanDetailResponse.getErrors())) {
                karzaClientResponse.setErrorList(tanDetailResponse.getErrors());
                karzaClientResponse.setError(tanDetailResponse.getError());
            }
            logger.info(responseMessage + "TAN :{}", tanDetailResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "TanDetailResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIecResultResponse(final KarzaClientResponse karzaClientResponse,
                                                    final String tempResponse) {
        try {
            IecResultResponse iecResultResponse = JsonUtil.StringToObject(tempResponse, IecResultResponse.class);
            if (iecResultResponse.getPayload() != null) {
                karzaClientResponse.setIecResultResponseDetails(iecResultResponse.getPayload());
            } else if (iecResultResponse.getError() != null || CollectionUtils.isNotEmpty(iecResultResponse.getErrors())) {
                karzaClientResponse.setErrorList(iecResultResponse.getErrors());
                karzaClientResponse.setError(iecResultResponse.getError());
            }
            logger.info(responseMessage + "IEC_AUTHENTICATION :{}", iecResultResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IecResultResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIecDetailedProfileResponse(final KarzaClientResponse karzaClientResponse,
                                                             final String tempResponse) {
        try {
            IecDetailedProfileResponse iecDetailedProfileResponse = JsonUtil.StringToObject(tempResponse, IecDetailedProfileResponse.class);
            if (iecDetailedProfileResponse.getPayload() != null) {
                karzaClientResponse.setIecDetailedProfileResponseDetails(iecDetailedProfileResponse.getPayload());
            } else if (iecDetailedProfileResponse.getError() != null || CollectionUtils.isNotEmpty(iecDetailedProfileResponse.getErrors())) {
                karzaClientResponse.setErrorList(iecDetailedProfileResponse.getErrors());
                karzaClientResponse.setError(iecDetailedProfileResponse.getError());
            }
            logger.info(responseMessage + "IEC_DETAILED_PROFILE :{}", iecDetailedProfileResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IecDetailedProfileResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addCompanyIdentificationResponse(final KarzaClientResponse karzaClientResponse,
                                                                final String tempResponse) {
        try {
            CompanyIdentificationResponse companyIdentificationResponse
                    = JsonUtil.StringToObject(tempResponse, CompanyIdentificationResponse.class);
            if (companyIdentificationResponse.getPayload() != null) {
                karzaClientResponse.setCompanyIdentificationResponseDetails(companyIdentificationResponse.getPayload());
            } else if (companyIdentificationResponse.getError() != null || CollectionUtils.isNotEmpty(companyIdentificationResponse.getErrors())) {
                karzaClientResponse.setErrorList(companyIdentificationResponse.getErrors());
                karzaClientResponse.setError(companyIdentificationResponse.getError());
            }
            logger.info(responseMessage + "COMPANY_CIN_AUTHENTICATION :{}", companyIdentificationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "CompanyIdentificationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addLlpIdentificationResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            LlpIdentificationResponse llpIdentificationResponse
                    = JsonUtil.StringToObject(tempResponse, LlpIdentificationResponse.class);
            if (llpIdentificationResponse.getPayload() != null) {
                karzaClientResponse.setLlpIdentificationResponseDetails(llpIdentificationResponse.getPayload());
            } else if (llpIdentificationResponse.getError() != null || CollectionUtils.isNotEmpty(llpIdentificationResponse.getErrors())) {
                karzaClientResponse.setErrorList(llpIdentificationResponse.getErrors());
                karzaClientResponse.setError(llpIdentificationResponse.getError());
            }
            logger.info(responseMessage + "LLP_CIN_AUTHENTICATION :{}", llpIdentificationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "LlpIdentificationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addUdyogAadharNumberResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            UdyogAadharNumberResponse udyogAadharNumberResponse
                    = JsonUtil.StringToObject(tempResponse, UdyogAadharNumberResponse.class);
            if (udyogAadharNumberResponse.getPayload() != null) {
                karzaClientResponse.setUdyogAadharNumberResponseDetails(udyogAadharNumberResponse.getPayload());
            } else if (udyogAadharNumberResponse.getError() != null || CollectionUtils.isNotEmpty(udyogAadharNumberResponse.getErrors())) {
                karzaClientResponse.setErrorList(udyogAadharNumberResponse.getErrors());
                karzaClientResponse.setError(udyogAadharNumberResponse.getError());
            }
            logger.info(responseMessage + "UDYOG_AADHAR_NUMBER :{}", udyogAadharNumberResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "UdyogAadharNumberResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstIdentificationResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            GstIdentificationResponse gstIdentificationResponse = JsonUtil.StringToObject(tempResponse, GstIdentificationResponse.class);
            if (gstIdentificationResponse.getPayload() != null) {
                karzaClientResponse.setGstIdentificationResponseDetails(gstIdentificationResponse.getPayload());
            } else if (gstIdentificationResponse.getError() != null || CollectionUtils.isNotEmpty(gstIdentificationResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstIdentificationResponse.getErrors());
                karzaClientResponse.setError(gstIdentificationResponse.getError());
            }
            logger.info(responseMessage + "GST :{}", gstIdentificationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "GstIdentificationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            GstAuthenticationResponse gstAuthenticationResponse = JsonUtil.StringToObject(tempResponse, GstAuthenticationResponse.class);
            if (gstAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setGstAuthenticationResponseDetails(gstAuthenticationResponse.getPayload());
            } else if (gstAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(gstAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstAuthenticationResponse.getErrors());
                karzaClientResponse.setError(gstAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "GSTAUTH :{}", gstAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "GstAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstSearchBasisPanResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            GstSearchBasisPanResponse gstSearchBasisPanResponseDetails = JsonUtil.StringToObject(tempResponse, GstSearchBasisPanResponse.class);
            if (gstSearchBasisPanResponseDetails.getPayload() != null) {
                karzaClientResponse.setGstSearchBasisPanResponseDetails(gstSearchBasisPanResponseDetails.getPayload());
            } else if (gstSearchBasisPanResponseDetails.getError() != null || CollectionUtils.isNotEmpty(gstSearchBasisPanResponseDetails.getErrors())) {
                karzaClientResponse.setErrorList(gstSearchBasisPanResponseDetails.getErrors());
                karzaClientResponse.setError(gstSearchBasisPanResponseDetails.getError());
            }
            logger.info(responseMessage + "GST_SEARCH_BASIS_PAN :{}", gstSearchBasisPanResponseDetails);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "GstSearchBasisPanResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGspGstinAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                 final String tempResponse) {
        try {
            GspGstinAuthenticationResponse gspGstinAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    GspGstinAuthenticationResponse.class);
            if (gspGstinAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setGspGstinAuthenticationResponseDetails(gspGstinAuthenticationResponse.getPayload());
            } else if (gspGstinAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(gspGstinAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(gspGstinAuthenticationResponse.getErrors());
                karzaClientResponse.setError(gspGstinAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "GSP_GSTIN_AUTH :{}", gspGstinAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "GspGstinAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGspGstReturnFillingResponse(final KarzaClientResponse karzaClientResponse,
                                                              final String tempResponse) {
        try {
            GspGstReturnFillingResponse gspGstReturnFillingResponse = JsonUtil.StringToObject(tempResponse, GspGstReturnFillingResponse.class);
            if (gspGstReturnFillingResponse.getPayload() != null) {
                karzaClientResponse.setGspGstReturnFillingResponseDetails(gspGstReturnFillingResponse.getPayload());
            } else if (gspGstReturnFillingResponse.getError() != null || CollectionUtils.isNotEmpty(gspGstReturnFillingResponse.getErrors())) {
                karzaClientResponse.setErrorList(gspGstReturnFillingResponse.getErrors());
                karzaClientResponse.setError(gspGstReturnFillingResponse.getError());
            }
            logger.info(responseMessage + "GSP_GST_RETURN_FILLING :{}", gspGstReturnFillingResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "GspGstReturnFillingResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addShopAndEstablishmentResponse(final KarzaClientResponse karzaClientResponse,
                                                               final String tempResponse) {
        try {
            ShopAndEstablishmentResponse shopAndEstablishmentResponse = JsonUtil.StringToObject(tempResponse,
                    ShopAndEstablishmentResponse.class);
            if (shopAndEstablishmentResponse.getPayload() != null) {
                karzaClientResponse.setShopAndEstablishmentResponseDetails(shopAndEstablishmentResponse.getPayload());
            } else if (shopAndEstablishmentResponse.getError() != null || CollectionUtils.isNotEmpty(shopAndEstablishmentResponse.getErrors())) {
                karzaClientResponse.setErrorList(shopAndEstablishmentResponse.getErrors());
                karzaClientResponse.setError(shopAndEstablishmentResponse.getError());
            }
            logger.info(responseMessage + "SHOP_ESTABLISHMENT :{}", shopAndEstablishmentResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "ShopAndEstablishmentResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addFssaiLicenseResponse(final KarzaClientResponse karzaClientResponse,
                                                       final String tempResponse) {
        try {
            FssaiLicenseResponse fssaiLicenseResponse = JsonUtil.StringToObject(tempResponse, FssaiLicenseResponse.class);

            if (fssaiLicenseResponse.getPayload() != null) {
                karzaClientResponse.setFssaiLicenseResponseDetails(fssaiLicenseResponse.getPayload());
            } else if (fssaiLicenseResponse.getError() != null || CollectionUtils.isNotEmpty(fssaiLicenseResponse.getErrors())) {
                karzaClientResponse.setErrorList(fssaiLicenseResponse.getErrors());
                karzaClientResponse.setError(fssaiLicenseResponse.getError());
            }
            logger.info(responseMessage + "FSSAI_LICENSE_AUTH :{}", fssaiLicenseResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "FssaiLicenseResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addFdaLicenseAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                   final String tempResponse) {
        try {
            FdaLicenseAuthenticationResponse fdaLicenseAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    FdaLicenseAuthenticationResponse.class);
            if (fdaLicenseAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setFdaLicenseAuthenticationResponseDetails(fdaLicenseAuthenticationResponse.getPayload());
            } else if (fdaLicenseAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(fdaLicenseAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(fdaLicenseAuthenticationResponse.getErrors());
                karzaClientResponse.setError(fdaLicenseAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "FDA_LICENSE_AUTH :{}", fdaLicenseAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "FdaLicenseAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addCaMembershipAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                     final String tempResponse) {
        try {
            CaMembershipAuthenticationResponse caMembershipAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    CaMembershipAuthenticationResponse.class);
            if (caMembershipAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setCaMembershipAuthenticationResponseDetails(caMembershipAuthenticationResponse.getPayload());
            } else if (caMembershipAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(caMembershipAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(caMembershipAuthenticationResponse.getErrors());
                karzaClientResponse.setError(caMembershipAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "CA_MEMBERSHIP_AUTH :{}", caMembershipAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "CaMembershipAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIcsiMembershipAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                       final String tempResponse) {
        try {
            IcsiMembershipAuthenticationResponse icsiMembershipAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    IcsiMembershipAuthenticationResponse.class);
            if (icsiMembershipAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setIcsiMembershipAuthenticationResponseDetails(icsiMembershipAuthenticationResponse.getPayload());
            } else if (icsiMembershipAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(icsiMembershipAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(icsiMembershipAuthenticationResponse.getErrors());
                karzaClientResponse.setError(icsiMembershipAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "ICSI_MEMBERSHIP :{}", icsiMembershipAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IcsiMembershipAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIcwaiMembershipAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                        final String tempResponse) {
        try {
            IcwaiMembershipAuthenticationResponse icwaiMembershipAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    IcwaiMembershipAuthenticationResponse.class);

            if (icwaiMembershipAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setIcwaiMembershipAuthenticationResponseDetails(icwaiMembershipAuthenticationResponse.getPayload());
            } else if (icwaiMembershipAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(icwaiMembershipAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(icwaiMembershipAuthenticationResponse.getErrors());
                karzaClientResponse.setError(icwaiMembershipAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "ICWAI_MEMBERSHIP_AUTH :{}", icwaiMembershipAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IcwaiMembershipAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIcwaiFirmAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                  final String tempResponse) {
        try {
            IcwaiFirmAuthenticationResponse icwaiFirmAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    IcwaiFirmAuthenticationResponse.class);

            if (icwaiFirmAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setIcwaiFirmAuthenticationResponseDetails(icwaiFirmAuthenticationResponse.getPayload());
            } else if (icwaiFirmAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(icwaiFirmAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(icwaiFirmAuthenticationResponse.getErrors());
                karzaClientResponse.setError(icwaiFirmAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "ICWAI_FIRM_AUTH :{}", icwaiFirmAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IcwaiFirmAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addMciMembershipAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                      final String tempResponse) {
        try {
            MciMembershipAuthenticationResponse mciMembershipAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    MciMembershipAuthenticationResponse.class);
            if (mciMembershipAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setMciMembershipAuthenticationResponseDetails(mciMembershipAuthenticationResponse.getPayload());
            } else if (mciMembershipAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(mciMembershipAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(mciMembershipAuthenticationResponse.getErrors());
                karzaClientResponse.setError(mciMembershipAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "MCI_MEMBERSHIP_AUTH :{}", mciMembershipAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "MciMembershipAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addPngAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            PngAuthenticationResponse pngAuthenticationResponse = JsonUtil.StringToObject(tempResponse, PngAuthenticationResponse.class);
            if (pngAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setPngAuthenticationResponseDetails(pngAuthenticationResponse.getPayload());
            } else if (pngAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(pngAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(pngAuthenticationResponse.getErrors());
                karzaClientResponse.setError(pngAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "PNG_AUTH :{}", pngAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "PngAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addElectricityResponse(final KarzaClientResponse karzaClientResponse,
                                                      final String tempResponse) {
        try {
            ElectricityResponse electricityResponse = JsonUtil.StringToObject(tempResponse, ElectricityResponse.class);
            if (electricityResponse.getPayload() != null) {
                karzaClientResponse.setElectricityAuthResponse(electricityResponse.getPayload());
            } else if (electricityResponse.getError() != null || CollectionUtils.isNotEmpty(electricityResponse.getErrors())) {
                karzaClientResponse.setErrorList(electricityResponse.getErrors());
            }
            logger.info(responseMessage + "ELECTRICITY_BILL_AUTH :{}", electricityResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "ElectricityResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addTelephoneBillAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                      final String tempResponse) {
        try {
            TelephoneBillAuthenticationResponse telephoneBillAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    TelephoneBillAuthenticationResponse.class);
            if (telephoneBillAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setTelephoneBillAuthenticationResponseDetails(telephoneBillAuthenticationResponse.getPayload());
            } else if (telephoneBillAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(telephoneBillAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(telephoneBillAuthenticationResponse.getErrors());
                karzaClientResponse.setError(telephoneBillAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "TELEPHONE_BILL_AUTH :{}", telephoneBillAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "TelephoneBillAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addMobileOtpResponse(final KarzaClientResponse karzaClientResponse,
                                                    final String tempResponse) {
        try {
            MobileOtpResponse mobileOtpResponse = JsonUtil.StringToObject(tempResponse, MobileOtpResponse.class);
            if (mobileOtpResponse.getPayload() != null) {
                karzaClientResponse.setMobileOtpResponseDetails(mobileOtpResponse.getPayload());
                karzaClientResponse.setAcknowledgementId(mobileOtpResponse.getAcknowledgementId());
            } else if (mobileOtpResponse.getError() != null || CollectionUtils.isNotEmpty(mobileOtpResponse.getErrors())) {
                karzaClientResponse.setErrorList(mobileOtpResponse.getErrors());
                karzaClientResponse.setError(mobileOtpResponse.getError());
            }
            logger.info(responseMessage + "MOBILE_OTP_AUTH :{}", mobileOtpResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "MobileOtpResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addMobileDetailsResponse(final KarzaClientResponse karzaClientResponse,
                                                        final String tempResponse) {
        try {
            MobileDetailsResponse mobileDetailsResponse = JsonUtil.StringToObject(tempResponse, MobileDetailsResponse.class);
            if (mobileDetailsResponse.getPayload() != null) {
                karzaClientResponse.setMobileDetailsResponseDetails(mobileDetailsResponse.getPayload());
            } else if (mobileDetailsResponse.getError() != null || CollectionUtils.isNotEmpty(mobileDetailsResponse.getErrors())) {
                karzaClientResponse.setErrorList(mobileDetailsResponse.getErrors());
                karzaClientResponse.setError(mobileDetailsResponse.getError());
            }
            logger.info(responseMessage + "MOBILE_DETAILS_AUTH :{}", mobileDetailsResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "MobileDetailsResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addLpgIdAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                              final String tempResponse) {
        try {
            LpgIdAuthenticationResponse lpgIdAuthenticationResponse = JsonUtil.StringToObject(tempResponse, LpgIdAuthenticationResponse.class);
            if (lpgIdAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setLpgIdAuthenticationResponseDetails(lpgIdAuthenticationResponse.getPayload());
            } else if (lpgIdAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(lpgIdAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(lpgIdAuthenticationResponse.getErrors());
                karzaClientResponse.setError(lpgIdAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "LPG_ID_AUTH :{}", lpgIdAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "LpgIdAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addOtpResponse(final KarzaClientResponse karzaClientResponse,
                                              final String tempResponse) {
        try {
            OtpResponse otpResponse = JsonUtil.StringToObject(tempResponse, OtpResponse.class);
            if (otpResponse.getPayload() != null) {
                karzaClientResponse.setOtpResponseDetails(otpResponse.getPayload());
                karzaClientResponse.setAcknowledgementId(otpResponse.getAcknowledgementId());
            } else if (otpResponse.getError() != null || CollectionUtils.isNotEmpty(otpResponse.getErrors())) {
                karzaClientResponse.setErrorList(otpResponse.getErrors());
                karzaClientResponse.setError(otpResponse.getError());
            }
            logger.info(responseMessage + "EPF_OTP_AUTH :{}", otpResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "OtpResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addPassbookResponse(final KarzaClientResponse karzaClientResponse,
                                                   final String tempResponse) {
        try {
            PassbookResponse passbookResponse = JsonUtil.StringToObject(tempResponse, PassbookResponse.class);
            if (passbookResponse.getPayload() != null) {
                karzaClientResponse.setPassbookResponseDetails(passbookResponse.getPayload());
            } else if (passbookResponse.getError() != null || CollectionUtils.isNotEmpty(passbookResponse.getErrors())) {
                karzaClientResponse.setErrorList(passbookResponse.getErrors());
                karzaClientResponse.setError(passbookResponse.getError());
            }
            logger.info(responseMessage + "EPF_OTP_AUTH :{}", passbookResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "PassbookResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEpfUanLookupResponse(final KarzaClientResponse karzaClientResponse,
                                                       final String tempResponse) {
        try {
            EpfUanLookupResponse epfUanLookupResponse = JsonUtil.StringToObject(tempResponse, EpfUanLookupResponse.class);
            if (epfUanLookupResponse.getPayload() != null) {
                karzaClientResponse.setEpfUanLookupResponseDetails(epfUanLookupResponse.getPayload());
            } else if (epfUanLookupResponse.getError() != null || CollectionUtils.isNotEmpty(epfUanLookupResponse.getErrors())) {
                karzaClientResponse.setErrorList(epfUanLookupResponse.getErrors());
                karzaClientResponse.setError(epfUanLookupResponse.getError());
            }
            logger.info(responseMessage + "EPF_UAN_LOOKUP :{}", epfUanLookupResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "EpfUanLookupResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEmployerLookupResponse(final KarzaClientResponse karzaClientResponse,
                                                         final String tempResponse) {
        try {
            EmployerLookupResponse employerLookupResponse = JsonUtil.StringToObject(tempResponse, EmployerLookupResponse.class);
            if (employerLookupResponse.getPayload() != null) {
                karzaClientResponse.setEmployerLookupResponseDetails(employerLookupResponse.getPayload());
            } else if (employerLookupResponse.getError() != null || CollectionUtils.isNotEmpty(employerLookupResponse.getErrors())) {
                karzaClientResponse.setErrorList(employerLookupResponse.getErrors());
                karzaClientResponse.setError(employerLookupResponse.getError());
            }
            logger.info(responseMessage + "EMPLOYEER_LOOKUP :{}", employerLookupResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "EmployerLookupResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEsicIdAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                               final String tempResponse) {
        try {
            EsicIdAuthenticationResponse esicIdAuthenticationResponse = JsonUtil.StringToObject(tempResponse, EsicIdAuthenticationResponse.class);
            if (esicIdAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setEsicIdAuthenticationResponseDetails(esicIdAuthenticationResponse.getPayload());
            } else if (esicIdAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(esicIdAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(esicIdAuthenticationResponse.getErrors());
                karzaClientResponse.setError(esicIdAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "ESIC_ID_AUTH :{}", esicIdAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "EsicIdAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addForm16AuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                               final String tempResponse) {
        try {
            Form16AuthenticationResponse form16AuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    Form16AuthenticationResponse.class);
            if (form16AuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setForm16AuthenticationResponseDetails(form16AuthenticationResponse.getPayload());
            } else if (form16AuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(form16AuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(form16AuthenticationResponse.getErrors());
                karzaClientResponse.setError(form16AuthenticationResponse.getError());
            }
            logger.info(responseMessage + "FORM_16_AUTH :{}", form16AuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "Form16AuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addQuarterlyRecordsCountForNextFiscalResponse(final KarzaClientResponse karzaClientResponse,
                                                                             final String tempResponse) {
        try {
            QuarterlyRecordsCountForNextFiscalResponse quarterlyRecordsCountForNextFiscal = JsonUtil.StringToObject(tempResponse,
                    QuarterlyRecordsCountForNextFiscalResponse.class);
            if (quarterlyRecordsCountForNextFiscal.getPayload() != null) {
                karzaClientResponse.setQuarterlyRecordsCountForNextFiscal(quarterlyRecordsCountForNextFiscal.getPayload());
            } else if (quarterlyRecordsCountForNextFiscal.getError() != null || CollectionUtils.isNotEmpty(quarterlyRecordsCountForNextFiscal.getErrors())) {
                karzaClientResponse.setErrorList(quarterlyRecordsCountForNextFiscal.getErrors());
                karzaClientResponse.setError(quarterlyRecordsCountForNextFiscal.getError());
            }
            logger.info(responseMessage + "FORM_16_QUATERLY_AUTH :{}", quarterlyRecordsCountForNextFiscal);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "QuarterlyRecordsCountForNextFiscalResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addItrAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                            final String tempResponse) {
        try {
            ItrAuthenticationResponse itrAuthenticationResponse = JsonUtil.StringToObject(tempResponse, ItrAuthenticationResponse.class);
            if (itrAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setItrAuthenticationResponseDetails(itrAuthenticationResponse.getPayload());
            } else if (itrAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(itrAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(itrAuthenticationResponse.getErrors());
                karzaClientResponse.setError(itrAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "ITR_AUTH :{}", itrAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "ItrAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addAddressMatchingResponse(final KarzaClientResponse karzaClientResponse,
                                                          final String tempResponse) {
        try {
            AddressMatchingResponse addressMatchingResponse = JsonUtil.StringToObject(tempResponse, AddressMatchingResponse.class);
            if (addressMatchingResponse.getPayload() != null) {
                karzaClientResponse.setAddressMatchingResponseDetails(addressMatchingResponse.getPayload());
            } else if (addressMatchingResponse.getError() != null || CollectionUtils.isNotEmpty(addressMatchingResponse.getErrors())) {
                karzaClientResponse.setErrorList(addressMatchingResponse.getErrors());
                karzaClientResponse.setError(addressMatchingResponse.getError());
            }
            logger.info(responseMessage + "ADDRESS_MATCHING :{}", addressMatchingResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "AddressMatchingResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addNameSimilarityResponse(final KarzaClientResponse karzaClientResponse,
                                                         final String tempResponse) {
        try {
            NameSimilarityResponse nameSimilarityResponse = JsonUtil.StringToObject(tempResponse, NameSimilarityResponse.class);
            if (nameSimilarityResponse.getPayload() != null) {
                karzaClientResponse.setNameSimilarityResponseDetails(nameSimilarityResponse.getPayload());
            } else if (nameSimilarityResponse.getError() != null || CollectionUtils.isNotEmpty(nameSimilarityResponse.getErrors())) {
                karzaClientResponse.setErrorList(nameSimilarityResponse.getErrors());
                karzaClientResponse.setError(nameSimilarityResponse.getError());
            }
            logger.info(responseMessage + "NAME_SIMILARITY :{}", nameSimilarityResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "NameSimilarityResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIfscCodeCheckResponse(final KarzaClientResponse karzaClientResponse,
                                                        final String tempResponse) {
        try {
            IfscCodeCheckResponse ifscCodeCheckResponse = JsonUtil.StringToObject(tempResponse, IfscCodeCheckResponse.class);
            if (ifscCodeCheckResponse.getPayload() != null) {
                karzaClientResponse.setIfscCodeCheckResponseDetails(ifscCodeCheckResponse.getPayload());
            } else if (ifscCodeCheckResponse.getError() != null || CollectionUtils.isNotEmpty(ifscCodeCheckResponse.getErrors())) {
                karzaClientResponse.setErrorList(ifscCodeCheckResponse.getErrors());
                karzaClientResponse.setError(ifscCodeCheckResponse.getError());
            }
            logger.info(responseMessage + "IFSC_CODE_CHECK :{}", ifscCodeCheckResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "IfscCodeCheckResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addBankAccountVerificationResponse(final KarzaClientResponse karzaClientResponse,
                                                                  final String tempResponse) {
        try {
            BankAccountVerificationResponse bankAccountVerificationResponse = JsonUtil.StringToObject(tempResponse,
                    BankAccountVerificationResponse.class);
            if (bankAccountVerificationResponse.getPayload() != null) {
                karzaClientResponse.setBankAccountVerificationResponseDetails(bankAccountVerificationResponse.getPayload());
            } else if (bankAccountVerificationResponse.getError() != null || CollectionUtils.isNotEmpty(bankAccountVerificationResponse.getErrors())) {
                karzaClientResponse.setErrorList(bankAccountVerificationResponse.getErrors());
                karzaClientResponse.setError(bankAccountVerificationResponse.getError());
            }
            logger.info(responseMessage + "BANK_ACCOUNT_VERIFICATION :{}", bankAccountVerificationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "BankAccountVerificationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addHsnCodeCheckResponse(final KarzaClientResponse karzaClientResponse,
                                                       final String tempResponse) {
        try {
            HsnCodeCheckResponse hsnCodeCheckResponse = JsonUtil.StringToObject(tempResponse, HsnCodeCheckResponse.class);
            if (hsnCodeCheckResponse.getPayload() != null) {
                karzaClientResponse.setHsnCodeCheckResponseDetails(hsnCodeCheckResponse.getPayload());
            } else if (hsnCodeCheckResponse.getError() != null || CollectionUtils.isNotEmpty(hsnCodeCheckResponse.getErrors())) {
                karzaClientResponse.setErrorList(hsnCodeCheckResponse.getErrors());
                karzaClientResponse.setError(hsnCodeCheckResponse.getError());
            }
            logger.info(responseMessage + "HSN_CODE :{}", hsnCodeCheckResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "hsnCodeCheckResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addWebsiteDomainAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                      final String tempResponse) {
        try {
            WebsiteDomainAuthenticationResponse websiteDomainAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    WebsiteDomainAuthenticationResponse.class);
            if (websiteDomainAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setWebsiteDomainAuthenticationResponseDetails(websiteDomainAuthenticationResponse.getPayload());
            } else if (websiteDomainAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(websiteDomainAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(websiteDomainAuthenticationResponse.getErrors());
                karzaClientResponse.setError(websiteDomainAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "WEBSITE_DOMAIN_AUTH :{}", websiteDomainAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "websiteDomainAuthenticationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addVehicleRCAuthenticationResponse(final KarzaClientResponse karzaClientResponse,
                                                                  final String tempResponse) {
        try {
            VehicleRCAuthenticationResponse vehicleRCAuthenticationResponse = JsonUtil.StringToObject(tempResponse,
                    VehicleRCAuthenticationResponse.class);
            if (vehicleRCAuthenticationResponse.getPayload() != null) {
                karzaClientResponse.setVehicleRCAuthenticationResponseDetails(vehicleRCAuthenticationResponse.getPayload());
            } else if (vehicleRCAuthenticationResponse.getError() != null || CollectionUtils.isNotEmpty(vehicleRCAuthenticationResponse.getErrors())) {
                karzaClientResponse.setErrorList(vehicleRCAuthenticationResponse.getErrors());
                karzaClientResponse.setError(vehicleRCAuthenticationResponse.getError());
            }
            logger.info(responseMessage + "VEHICLE_RC_AUTH :{}", vehicleRCAuthenticationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "vehicleRCAuthenticationResponse ", exception);
        }
        return karzaClientResponse;
    }

    // need to confirm if can use same response class
    public KarzaClientResponse addVehicleRCAuthenticationSearchResponse(final KarzaClientResponse karzaClientResponse,
                                                                        final String tempResponse) {
        try {
            VehicleRCAuthenticationResponse vehicleRCAuthenticationSearchResponse = JsonUtil.StringToObject(tempResponse,
                    VehicleRCAuthenticationResponse.class);
            if (vehicleRCAuthenticationSearchResponse.getPayload() != null) {
                karzaClientResponse.setVehicleRCAuthenticationResponseDetails(vehicleRCAuthenticationSearchResponse.getPayload());
            } else if (vehicleRCAuthenticationSearchResponse.getError() != null || CollectionUtils.isNotEmpty(vehicleRCAuthenticationSearchResponse.getErrors())) {
                karzaClientResponse.setErrorList(vehicleRCAuthenticationSearchResponse.getErrors());
                karzaClientResponse.setError(vehicleRCAuthenticationSearchResponse.getError());
            }
            logger.info(responseMessage + "VEHICLE_RC_AUTH :{}", vehicleRCAuthenticationSearchResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "vehicleRCAuthenticationSearchResponse ", exception);
        }
        return karzaClientResponse;
    }


    public KarzaClientResponse addGstTrrnVerifyOtpResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            GstTransactionApiVerifyOtpResponse gstTransactionApiVerifyOtpResponse = JsonUtil.StringToObject(tempResponse, GstTransactionApiVerifyOtpResponse.class);
            if (gstTransactionApiVerifyOtpResponse.getPayload() != null) {
                karzaClientResponse.setGstTransactionApiVerifyOtpResponseDetails(gstTransactionApiVerifyOtpResponse.getPayload());
            } else if (gstTransactionApiVerifyOtpResponse.getError() != null || CollectionUtils.isNotEmpty(gstTransactionApiVerifyOtpResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstTransactionApiVerifyOtpResponse.getErrors());
                karzaClientResponse.setError(gstTransactionApiVerifyOtpResponse.getError());
            }
            logger.info(responseMessage + "GSTTRANSACTIONGETOTP :{}", gstTransactionApiVerifyOtpResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstTransactionApiVerifyOtpResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstCredentialLinkGenerationResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            GstCredentialLinkGenerationResponse gstCredentialLinkGenerationResponse = JsonUtil.StringToObject(tempResponse, GstCredentialLinkGenerationResponse.class);
            if (gstCredentialLinkGenerationResponse.getPayload() != null) {
                karzaClientResponse.setGstCredentialLinkGenerationResponseDetails(gstCredentialLinkGenerationResponse.getPayload());
            } else if (gstCredentialLinkGenerationResponse.getError() != null || CollectionUtils.isNotEmpty(gstCredentialLinkGenerationResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstCredentialLinkGenerationResponse.getErrors());
                karzaClientResponse.setError(gstCredentialLinkGenerationResponse.getError());
            }
            logger.info(responseMessage + "GSTCREDENTIALLINKGENERATION :{}", gstCredentialLinkGenerationResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstCredentialLinkGenerationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstCredentialLinkSendResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            GstCredentialLinkGenerationResponse gstCredentialLinkSendResponse = JsonUtil.StringToObject(tempResponse, GstCredentialLinkGenerationResponse.class);
            if (gstCredentialLinkSendResponse.getPayload() != null) {
                karzaClientResponse.setGstCredentialLinkSendResponseDetails(gstCredentialLinkSendResponse.getPayload());
            } else if (gstCredentialLinkSendResponse.getError() != null || CollectionUtils.isNotEmpty(gstCredentialLinkSendResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstCredentialLinkSendResponse.getErrors());
                karzaClientResponse.setError(gstCredentialLinkSendResponse.getError());
            }
            logger.info(responseMessage + "GSTCREDENTIALLINKSend :{}", gstCredentialLinkSendResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstCredentialLinkGenerationResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEntitySearchResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            EntitySearchResponse entitySearchResponse = JsonUtil.StringToObject(tempResponse, EntitySearchResponse.class);
            if (entitySearchResponse.getPayload() != null) {
                karzaClientResponse.setEntitySearchResponseDetails(entitySearchResponse.getPayload());
            } else if (entitySearchResponse.getError() != null || CollectionUtils.isNotEmpty(entitySearchResponse.getErrors())) {
                karzaClientResponse.setErrorList(entitySearchResponse.getErrors());
                karzaClientResponse.setError(entitySearchResponse.getError());
            }
            logger.info(responseMessage + "ENTITYSEARCH :{}", entitySearchResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "EntitySearchResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addDetailedprofileResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            DetailedProfileResponse detailedProfileResponse = JsonUtil.StringToObject(tempResponse, DetailedProfileResponse.class);
            if (detailedProfileResponse.getPayload() != null) {
                karzaClientResponse.setDetailedProfileResponseDetails(detailedProfileResponse.getPayload());
            } else if (detailedProfileResponse.getError() != null || CollectionUtils.isNotEmpty(detailedProfileResponse.getErrors())) {
                karzaClientResponse.setErrorList(detailedProfileResponse.getErrors());
                karzaClientResponse.setError(detailedProfileResponse.getError());
            }
            logger.info(responseMessage + "DETAILEDPROFILE :{}", detailedProfileResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "DetailedProfileResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstFilingStatusResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            GstFilingStatusResponse gstFilingStatusResponse = JsonUtil.StringToObject(tempResponse, GstFilingStatusResponse.class);
            if (gstFilingStatusResponse.getPayload() != null) {
                karzaClientResponse.setGstFilingStatusResponseDetails(gstFilingStatusResponse.getPayload());
            } else if (gstFilingStatusResponse.getError() != null || CollectionUtils.isNotEmpty(gstFilingStatusResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstFilingStatusResponse.getErrors());
                karzaClientResponse.setError(gstFilingStatusResponse.getError());
            }
            logger.info(responseMessage + "GSTFILINGSTATUS :{}", gstFilingStatusResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstFilingStatusResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addGstProfileResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            GstProfileResponse gstProfileResponse = JsonUtil.StringToObject(tempResponse, GstProfileResponse.class);
            if (gstProfileResponse.getPayload() != null) {
                karzaClientResponse.setGstProfileResponseDetails(gstProfileResponse.getPayload());
            } else if (gstProfileResponse.getError() != null || CollectionUtils.isNotEmpty(gstProfileResponse.getErrors())) {
                karzaClientResponse.setErrorList(gstProfileResponse.getErrors());
                karzaClientResponse.setError(gstProfileResponse.getError());
            }
            logger.info(responseMessage + "GSTPROFILE :{}", gstProfileResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstProfileResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addIBBIResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            IBBIResponse ibbiResponse = JsonUtil.StringToObject(tempResponse, IBBIResponse.class);
            if (ibbiResponse.getPayload() != null) {
                karzaClientResponse.setIbbiResponseDetails(ibbiResponse.getPayload());
            } else if (ibbiResponse.getError() != null || CollectionUtils.isNotEmpty(ibbiResponse.getErrors())) {
                karzaClientResponse.setErrorList(ibbiResponse.getErrors());
                karzaClientResponse.setError(ibbiResponse.getError());
            }
            logger.info(responseMessage + "IBBI :{}", ibbiResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "IBBIResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addBIFRResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            BIFRResponse bifrResponse = JsonUtil.StringToObject(tempResponse, BIFRResponse.class);
            if (bifrResponse.getPayload() != null) {
                karzaClientResponse.setBifrResponseDetails(bifrResponse.getPayload());
            } else if (bifrResponse.getError() != null || CollectionUtils.isNotEmpty(bifrResponse.getErrors())) {
                karzaClientResponse.setErrorList(bifrResponse.getErrors());
                karzaClientResponse.setError(bifrResponse.getError());
            }
            logger.info(responseMessage + "BIFR :{}", bifrResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "BIFRResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addDrtCheckResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            DrtCheckResponse drtCheckResponse = JsonUtil.StringToObject(tempResponse, DrtCheckResponse.class);
            if (drtCheckResponse.getPayload() != null) {
                karzaClientResponse.setDrtCheckResponseDetails(drtCheckResponse.getPayload());
            } else if (drtCheckResponse.getError() != null || CollectionUtils.isNotEmpty(drtCheckResponse.getErrors())) {
                karzaClientResponse.setErrorList(drtCheckResponse.getErrors());
                karzaClientResponse.setError(drtCheckResponse.getError());
            }
            logger.info(responseMessage + "DRTCHECK :{}", drtCheckResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "DrtCheckResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addSuitFiledSearchResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            SuitFiledSearchResponse suitFiledSearchResponse = JsonUtil.StringToObject(tempResponse, SuitFiledSearchResponse.class);
            if (suitFiledSearchResponse.getPayload() != null) {
                karzaClientResponse.setSuitFiledSearchResponseDetails(suitFiledSearchResponse.getPayload());
            } else if (suitFiledSearchResponse.getError() != null || CollectionUtils.isNotEmpty(suitFiledSearchResponse.getErrors())) {
                karzaClientResponse.setErrorList(suitFiledSearchResponse.getErrors());
                karzaClientResponse.setError(suitFiledSearchResponse.getError());
            }
            logger.info(responseMessage + "SUITFILEDSEARCH :{}", suitFiledSearchResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "SuitFiledSearchResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addLitigationsSearchResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            LitigationsSearchResponse litigationsSearchResponse = JsonUtil.StringToObject(tempResponse, LitigationsSearchResponse.class);
            if (litigationsSearchResponse.getPayload() != null) {
                karzaClientResponse.setLitigationsSearchResponseDetails(litigationsSearchResponse.getPayload());
            } else if (litigationsSearchResponse.getError() != null || CollectionUtils.isNotEmpty(litigationsSearchResponse.getErrors())) {
                karzaClientResponse.setErrorList(litigationsSearchResponse.getErrors());
                karzaClientResponse.setError(litigationsSearchResponse.getError());
            }
            logger.info(responseMessage + "LITIGATIONSEARCH :{}", litigationsSearchResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "LitigationsSearchResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addLitigationDetailsDistrictCourtResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            LitigationDetailsDistrictCourtResponse litigationDetailsDistrictCourtResponse = JsonUtil.StringToObject(tempResponse, LitigationDetailsDistrictCourtResponse.class);
            if (litigationDetailsDistrictCourtResponse.getPayload() != null) {
                karzaClientResponse.setLitigationDetailsDistrictCourtResponseDetails(litigationDetailsDistrictCourtResponse.getPayload());
            } else if (litigationDetailsDistrictCourtResponse.getError() != null || CollectionUtils.isNotEmpty(litigationDetailsDistrictCourtResponse.getErrors())) {
                karzaClientResponse.setErrorList(litigationDetailsDistrictCourtResponse.getErrors());
                karzaClientResponse.setError(litigationDetailsDistrictCourtResponse.getError());
            }
            logger.info(responseMessage + "LITIGATIONDETAILSDISTRICTCOURT :{}", litigationDetailsDistrictCourtResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "LitigationDetailsDistrictCourtResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addCompanySearchResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            CompanySearchByNameResponse companySearchByNameResponse = JsonUtil.StringToObject(tempResponse, CompanySearchByNameResponse.class);
            if (companySearchByNameResponse.getPayload() != null) {
                karzaClientResponse.setCompanySearchByNameResponseDetails(companySearchByNameResponse.getPayload());
            } else if (companySearchByNameResponse.getError() != null || CollectionUtils.isNotEmpty(companySearchByNameResponse.getErrors())) {
                karzaClientResponse.setErrorList(companySearchByNameResponse.getErrors());
                karzaClientResponse.setError(companySearchByNameResponse.getError());
            }
            logger.info(responseMessage + "companySearchByNameResponse :{}", companySearchByNameResponse);
        } catch (final Exception exception) {
            logger.error(errorMessage + "companySearchByNameResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEmploymentVerificationAdvancedResponse(final KarzaClientResponse karzaClientResponse, final  String tempResponse){
        try{
            EmploymentVerificationAdvancedResponse employmentVerificationAdvancedResponse = JsonUtil.StringToObject(tempResponse, EmploymentVerificationAdvancedResponse.class);
            if(employmentVerificationAdvancedResponse.getPayload() != null){
                karzaClientResponse.setEmploymentVerificationAdvancedResponseDetails(employmentVerificationAdvancedResponse.getPayload());
            }else if(employmentVerificationAdvancedResponse.getError() != null || CollectionUtils.isNotEmpty(employmentVerificationAdvancedResponse.getErrors())){
                karzaClientResponse.setErrorList(employmentVerificationAdvancedResponse.getErrors());
                karzaClientResponse.setError(employmentVerificationAdvancedResponse.getError());
            }
            logger.info(responseMessage + "employmentVerificationAdvancedResponse: {}", employmentVerificationAdvancedResponse);
        }catch(final Exception exception){
            logger.error(errorMessage + "employmentVerificationAdvancedResponse", exception);
        }

        return karzaClientResponse;
    }

    public KarzaClientResponse addGstTransAPIResponse(final KarzaClientResponse karzaClientResponse, final String tempResponse) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(tempResponse);
            if (jsonNode.has("result") && jsonNode.get("result").has("current")){
                final GstTransactionApiResponse gstTransactionApiResponse = new GstTransactionApiResponse();
                JsonNode currentGstInfo = jsonNode.get("result").get("current");
                if (currentGstInfo.has("averages")) {
                    GstTransactionApiAveragesResponse gstTransactionApiAveragesResponse =
                            mapper.convertValue(currentGstInfo.get("averages"), GstTransactionApiAveragesResponse.class);
                    gstTransactionApiResponse.setGstTransactionApiAveragesResponse(gstTransactionApiAveragesResponse);
                }

                if (currentGstInfo.has("business_summary")) {
                    GstTransactionApiBusinessSummryResponse gstTransactionApiBusinessSummryResponse =
                            mapper.convertValue(currentGstInfo.get("business_summary"), GstTransactionApiBusinessSummryResponse.class);
                    gstTransactionApiResponse.setGstTransactionApiBusinessSummryResponse(gstTransactionApiBusinessSummryResponse);
                }

                if (currentGstInfo.has("purchase_state_wise")) {
                    ArrayNode purchaseArray = (ArrayNode) currentGstInfo.get("purchase_state_wise");
                    List<GstTransactionStateWiseResponse> gstTransactionPurchaseStateWiseResponseList =
                            Arrays.asList(mapper.convertValue(purchaseArray, GstTransactionStateWiseResponse[].class));
                    gstTransactionApiResponse.setGstTransactionPurchaseStateWiseResponseList(gstTransactionPurchaseStateWiseResponseList);
                }

                if (currentGstInfo.has("sales_state_wise")) {
                    ArrayNode salesArray = (ArrayNode) currentGstInfo.get("sales_state_wise");
                    List<GstTransactionStateWiseResponse> gstTransactionSalesStateWiseResponseList =
                            Arrays.asList(mapper.convertValue(salesArray, GstTransactionStateWiseResponse[].class));
                    gstTransactionApiResponse.setGstTransactionSalesStateWiseResponseList(gstTransactionSalesStateWiseResponseList);
                }
                karzaClientResponse.setGstTransactionApiResponse(gstTransactionApiResponse);

            }
            logger.info(responseMessage + "GSTTRANSACTIONAPI Success :{}");
        } catch (final Exception exception) {
            logger.error(errorMessage + "GstTransactionApiResponse", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addPANStatus(KarzaClientResponse karzaClientResponse, String tempResponse) {
        try {
            PanStatusResponse panResponse = JsonUtil.StringToObject(tempResponse, PanStatusResponse.class);
            if (panResponse.getPayload() != null) {
                karzaClientResponse.setPanAuthenticationResponse(panResponse.getPayload());
            } else if (panResponse.getError() != null|| CollectionUtils.isNotEmpty(panResponse.getErrors())) {
                karzaClientResponse.setErrorList(panResponse.getErrors());
                karzaClientResponse.setError(panResponse.getError());
            }
            logger.info(responseMessage + "PAN :{}", panResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "pan response", exception);
        }
        return karzaClientResponse;
    }

    public KarzaClientResponse addEmailVerificationResponse(KarzaClientResponse karzaClientResponse, String tempResponse) {
        try {
            EmailVerificationResponse emailVerificationResponse = JsonUtil.StringToObject(tempResponse, EmailVerificationResponse.class);
            if (emailVerificationResponse.getPayload() != null) {
                karzaClientResponse.setEmailVerificationResponseDetails(emailVerificationResponse.getPayload());
                karzaClientResponse.setAcknowledgementId(emailVerificationResponse.getAcknowledgementId());
            } else if (emailVerificationResponse.getError() != null|| CollectionUtils.isNotEmpty(emailVerificationResponse.getErrors())) {
                karzaClientResponse.setErrorList(emailVerificationResponse.getErrors());
                karzaClientResponse.setError(emailVerificationResponse.getError());
            }
            logger.info(responseMessage + "Email Verification:{}", emailVerificationResponse);
        } catch (final Exception exception) {
            karzaClientResponse.setError(exception.getMessage());
            logger.error(errorMessage + "Email Verification", exception);
        }
        return karzaClientResponse;
    }
}
