package com.softcell.service.impl;

import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.config.AmazonS3Configuration;
import com.softcell.config.EmailServiceConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.*;
import com.softcell.dao.mongodb.repository.multiproduct.MultiProductRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadInputStreamRequest;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.DigitizationDealerEmailMaster;
import com.softcell.gonogo.model.masters.GNGDealerEmailMaster;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.FileUploadResponse;
import com.softcell.gonogo.model.response.KycImages;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.salesforce.SalesForceInfo;
import com.softcell.gonogo.model.security.MessagingServiceResponse;
import com.softcell.gonogo.service.factory.DoOperationBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AmazonS3CloudStorageService;
import com.softcell.service.DigitizationManager;
import com.softcell.service.DocumentStoreManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.EmailRequestDeduped;
import com.softcell.utils.GngUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author yogeshb
 */
@Service
public class DocumentStoreManagerImpl implements DocumentStoreManager {

    private static final Logger logger = LoggerFactory.getLogger(DocumentStoreManagerImpl.class);

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private AdminLogRepository adminLogRepository;

    @Autowired
    private SalesForceRepository salesForceRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private MultiProductRepository multiProductRepository;

    @Autowired
    private MultibureauRepository multibureauRepository;

    @Autowired
    private DigitizationManager digitizationManager;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private DoOperationRepository doOperationRepository;

    @Autowired
    private DoOperationBuilder doOperationBuilder;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    @Override
    public BaseResponse saveImageToDB(FileUploadRequest fileUploadRequest) throws Exception {
        String applicationStage = "";
        String applicationStatus = "";
        GoNoGoCustomerApplication gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(fileUploadRequest.getGonogoReferanceId(),fileUploadRequest.getFileHeader().getInstitutionId());
        if (null != gngApp && null != gngApp.getApplicationRequest() && null != gngApp.getApplicationRequest().getCurrentStageId() && gngApp.getApplicationStatus() != null) {
            applicationStage = gngApp.getApplicationRequest().getCurrentStageId();
            applicationStatus = gngApp.getApplicationStatus();
        }
        ActivityLogs activityLogs = auditHelper.createActivityLog(fileUploadRequest.getFileHeader(), fileUploadRequest.getGonogoReferanceId(), null,
                applicationStage, EndPointReferrer.UPLOAD_IMAGE,
                applicationStatus, fileUploadRequest.getFileHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        FileUploadResponse fileUploadResponse = new FileUploadResponse();
        fileUploadRequest.getFileHeader().setDateTime(new Date());

        logger.info("{} upload document BY user {} ", fileUploadRequest.getGonogoReferanceId(), fileUploadRequest.getFileHeader().getDsaId());
        logger.info("{} file name  {} file Type", fileUploadRequest.getUploadFileDetails().getFileName(), fileUploadRequest.getUploadFileDetails().getFileType());

        String fileName = fileUploadRequest.getUploadFileDetails().getFileName();

        String fileType = fileUploadRequest.getUploadFileDetails().getFileType();
        /**
         * Decode data on other side, by processing encoded data
         */

        //Added logic for SBFC webside cases
        if (fileUploadRequest.getUploadFileDetails().getFileData().startsWith("data:image/")){
            String fileData = fileUploadRequest.getUploadFileDetails().getFileData();
            int beginIndex = 7 + fileData.indexOf("base64,");
            fileUploadRequest.getUploadFileDetails().setFileData(fileData.substring(beginIndex));
        }

        byte[] imageByte = Base64.decodeBase64(fileUploadRequest.getUploadFileDetails().getFileData());

        try (InputStream inputStream = new ByteArrayInputStream(imageByte)) {

            String res = uploadFileRepository.store(inputStream, fileName, fileType, fileUploadRequest);

            if (res != null) {
                fileUploadResponse.setGonogoRefId(fileUploadRequest
                        .getGonogoReferanceId());
                fileUploadResponse.setFileId(res);
                fileUploadResponse.setStatus(Constant.SUCCESS);

                try {

                    saveImageToAmazonS3(fileUploadResponse, fileUploadRequest.getUploadFileDetails(),
                            fileUploadRequest.getFileHeader());

                } catch (Exception e) {

                    logger.error("{}",e.getStackTrace());

                    logger.error("Exception in document store manager in fileUpload with probable cause [{}]", e.getMessage());

                    throw new Exception(String.format("Exception in document store manager in fileUpload with probable cause [{}]", e));

                }

            } else {
                fileUploadResponse.setStatus(Constant.ERROR);
            }
        } catch (Exception e) {

            logger.error("Exception in fileUpload: " + e);
            fileUploadResponse.setStatus(Constant.ERROR);
            throw new Exception("Exception in fileUpload:", e);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DocumentStoreManagerImpl in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);

        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadResponse);


    }

    @Override
    public BaseResponse saveImageWithMetadataToDB(MultipartFile file,
                                                  FileUploadRequest fileUploadRequest) throws Exception {

        fileUploadRequest.getFileHeader().setDateTime(new Date());

        String fileName = fileUploadRequest.getUploadFileDetails().getFileName();

        String fileType = fileUploadRequest.getUploadFileDetails().getFileType();

        FileUploadResponse fileUploadResponse = new FileUploadResponse();

        try {

            if (file.getInputStream().available() != 0) {

                String res = uploadFileRepository.store(file.getInputStream(), fileName, fileType, fileUploadRequest);

                if (res != null) {

                    fileUploadResponse.setGonogoRefId(fileUploadRequest.getGonogoReferanceId());
                    fileUploadResponse.setFileId(res);
                    fileUploadResponse.setStatus(Constant.SUCCESS);

                    try {

                        saveImageToAmazonS3(fileUploadResponse, fileUploadRequest.getUploadFileDetails(), fileUploadRequest.getFileHeader());

                    } catch (Exception e) {

                        logger.error("{}",e.getStackTrace());

                        logger.error("Exception in fileUpload: " + e);

                        throw new Exception(String.format("Error in saveImageWithMetadataToDB possble cause [%s]", e.getMessage()));
                    }

                } else {
                    fileUploadResponse.setStatus(Constant.ERROR);
                }

            } else {
                fileUploadResponse.setStatus(Constant.ERROR);
            }

        } catch (Exception e) {

            logger.error("Exception in fileUpload: " + e);

            fileUploadResponse.setStatus(Constant.ERROR);

            throw new Exception(String.format("Error in saveImageWithMetadataToDB possble cause [%s]", e.getMessage()));
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadResponse);

    }


    @Override
    public BaseResponse updateImageStatus(FileUploadRequest fileUploadRequest) {

        FileUploadResponse fileUploadResponse = new FileUploadResponse();


        fileUploadRequest.getFileHeader().setDateTime(new Date());

        if (uploadFileRepository.updateStatus(fileUploadRequest)) {

            fileUploadResponse.setStatus(Constant.SUCCESS);

        } else {

            fileUploadResponse.setStatus(Constant.ERROR);

        }

        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadResponse);

    }


    @Override
    public byte[] getDocumentImage(DocImageFIleGetRequest docImageFIleGetRequest) {

        String fileName = docImageFIleGetRequest.getImageRetrieveFilter().getDocName();

        String refID = docImageFIleGetRequest.getImageRetrieveFilter().getReferenceID();

        String applicantID = docImageFIleGetRequest.getImageRetrieveFilter().getApplicantID();

        GridFSDBFile gridFSDBFile = uploadFileRepository.getImage(fileName, refID, applicantID);

        try (InputStream inputStream = gridFSDBFile.getInputStream(); ByteArrayOutputStream bao = new ByteArrayOutputStream()) {

            // Prepare buffered image.
            BufferedImage img = ImageIO.read(inputStream);
            ImageIO.write(img, "jpg", bao);
            return bao.toByteArray();

        } catch (Exception e) {
            logger.error("Exception in get Image: " + e);
            return new byte[1];
        }
    }

    @Override
    public KycImages getDocumentImages(
            DocImageFIleGetRequest docImageFIleGetRequest) {
        String refID = docImageFIleGetRequest.getImageRetrieveFilter()
                .getReferenceID();
        String applicantID = docImageFIleGetRequest.getImageRetrieveFilter()
                .getApplicantID();

        List<GridFSDBFile> gridFSDBFileList = uploadFileRepository.getImages(
                refID, applicantID);
        KycImages kycImages = new KycImages();
        List<Document> documents = new ArrayList<Document>();
        for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {
            try {

                Document document = new Document();
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                InputStream inputStream = gridFSDBFile.getInputStream();
                try {
                    // Prepare buffered image.
                    BufferedImage img = ImageIO.read(inputStream);
                    ImageIO.write(img, "jpg", bao);

                    byte[] encodeBase64 = Base64
                            .encodeBase64(bao.toByteArray());

                    document.setByteCode(new String(encodeBase64));
                    document.setDocName(gridFSDBFile.getFilename());
                    documents.add(document);
                } finally {
                    inputStream.close();
                    bao.close();
                }
            } catch (Exception e) {
                logger.info("Exception in get Images: " + e);
                return kycImages;
            }
        }
        return kycImages;
    }

    @Override
    public BaseResponse getDocumentImageDetails(CheckApplicationStatus checkApplicationStatus) throws Exception {

        String refId = checkApplicationStatus.getGonogoRefId();

        GoNoGoCroApplicationResponse response = multiProductRepository.getParentApplication(refId);

        Set<String> refSet = new HashSet<String>();

        if (response != null && StringUtils.isNotBlank(refId)) {

            if (refId.equals(response.getRootID()) || response.getRootID() == null) {

                refSet.add(refId);

            } else {

                refSet = multiProductRepository.getAllRefenceIDAgainstRootApplication(response.getRootID());
            }
        }

        List<KycImageDetails> kycImageDetailsList = uploadFileRepository.getKycImageDetails(refSet,
                checkApplicationStatus.getHeader().getInstitutionId(), Status.ALL.name());

        if (kycImageDetailsList != null && !kycImageDetailsList.isEmpty()) {
            return GngUtils.getBaseResponse(HttpStatus.OK, kycImageDetailsList);
        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }


    @Override
    public BaseResponse getDocumentImageDetailsByApplicant(CheckApplicationStatus checkApplicationStatus) {

        List<KycImageDetails> kycImageDetailsList = uploadFileRepository.getKycImageDetailsByApplicant(checkApplicationStatus.getGonogoRefId(), checkApplicationStatus
                .getApplicantId(), checkApplicationStatus.getHeader().getInstitutionId());

        if (kycImageDetailsList != null && !kycImageDetailsList.isEmpty()) {

            return GngUtils.getBaseResponse(HttpStatus.OK, kycImageDetailsList);

        }

        return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
    }

    @Override
    public boolean validateRequest(CheckApplicationStatus checkApplicationStatus) {

        if (null != checkApplicationStatus
                && null != checkApplicationStatus.getHeader()
                && StringUtils.isNotBlank(checkApplicationStatus.getHeader()
                .getInstitutionId())
                && StringUtils.isNotBlank(checkApplicationStatus
                .getGonogoRefId())
                && StringUtils.isNotBlank(checkApplicationStatus
                .getApplicantId())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public BaseResponse getDocumentImageByRefId(GetFileRequest getImageRequest) throws Exception {

        String fileID = getImageRequest.getImageFileID();
        GridFSDBFile gridFSDBFile = uploadFileRepository.getById(fileID, getImageRequest.getHeader().getInstitutionId());

        try (InputStream inputStream = gridFSDBFile.getInputStream(); ByteArrayOutputStream bao = new ByteArrayOutputStream()) {

            BufferedImage img = ImageIO.read(inputStream);
            ImageIO.write(img, "jpg", bao);

            return GngUtils.getBaseResponse(HttpStatus.OK, bao.toByteArray());

        } catch (Exception e) {

            logger.error("Exception in get Images: " + e);

            throw new Exception(String.format("Exception while fetching getDocumentImageByRefId with probable cause [%s]", e.getMessage()));
        }
    }

    @Override
    public BaseResponse getDocumentBase64ImageByRefId(GetFileRequest getImageRequest) throws Exception {


        logger.info("getDocumentBase64ImageByRefId service started for imageID {} and instId {} ",getImageRequest.getImageFileID(),getImageRequest.getHeader().getInstitutionId());

        GridFSDBFile gridFSDBFile = uploadFileRepository.getById(getImageRequest.getImageFileID(),
                getImageRequest.getHeader().getInstitutionId());

        BaseResponse baseResponse;

        if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {

            try {

                Document doc = new Document();

                byte[] encodeBase64 = Base64.encodeBase64(IOUtils.toByteArray(gridFSDBFile.getInputStream()));

                doc.setByteCode(new String(encodeBase64));

                doc.setStatus(Status.SUCCESS.name());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);

                logger.info("Document successfully found for imageID {} and instId {} ",getImageRequest.getImageFileID(),getImageRequest.getHeader().getInstitutionId());


            } catch (Exception exp) {

                logger.error("Exception occurred in while fetching image  for refId {} for probable cause [{}] ", getImageRequest.getRefID(), exp);

                throw new SystemException(String.format("Exception occurred in while fetching image  for refId {%s} for probable cause [{%s}] ", getImageRequest.getRefID(), exp));

            }
        } else {

            logger.info("Document not found for imageID {} and instId {} ",getImageRequest.getImageFileID(),getImageRequest.getHeader().getInstitutionId());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;


    }

    @Override
    public BaseResponse getPdfDocumentByRefId(GetFileRequest getImageRequest) throws Exception {

        String fileID = getImageRequest.getImageFileID();

        GridFSDBFile gridFSDBFile = uploadFileRepository.getById(fileID, getImageRequest.getHeader().getInstitutionId());

        try (InputStream inputStream = gridFSDBFile.getInputStream(); ByteArrayOutputStream bao = new ByteArrayOutputStream()) {

            BufferedImage img = ImageIO.read(inputStream);

            ImageIO.write(img, "jpg", bao);

            return GngUtils.getBaseResponse(HttpStatus.OK, bao.toByteArray());

        } catch (IOException e) {

            logger.error("Exception while fetching getPdfDocumentdByRefId with probable cause [{}] ", e.getMessage());

            throw new Exception(String.format("Exception while fetching getPdfDocumentdByRefId with probable cause [%s]", e.getMessage()));

        }
    }


    @Override
    public BaseResponse getDeliveryOrderPdf(String refId ,String institutionId) throws Exception {

        Document doc = new Document();

        PostIPA postIPA = applicationRepository.getPostIPA(refId, institutionId);

        String deliveryOrderName = null;
        if (null != postIPA) {

            deliveryOrderName = GngUtils.formDeliveryOrderName(refId);
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        GridFSDBFile fileBucket = uploadFileRepository.getPDFByFilenameNRefID(refId,institutionId,deliveryOrderName);

        if (null != fileBucket && null != fileBucket.getInputStream()) {

            try (InputStream inputStream = fileBucket.getInputStream()) {

                byte[] pdfBytes = IOUtils.toByteArray(inputStream);
                byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);

                doc.setDocName(GNGWorkflowConstant.DO_REPORT.toFaceValue());
                doc.setByteCode(new String(encodeBase64));
                doc.setDocID(fileBucket.getId().toString());
                doc.setStatus(Constant.SUCCESS);

                return GngUtils.getBaseResponse(HttpStatus.OK, doc);

            } catch (Exception e) {

                logger.error("Exception: " + e);

                throw new Exception(String.format("Exception while fetching getDeliveryOrderPdf with probable cause [%s]", e.getMessage()));

            }
        } else {
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }


    @Override
    public BaseResponse getCibilPdfReport(GetFileRequest getFileRequest) {

        BaseResponse baseResponse;

        byte[] pdfBytes = uploadFileRepository.getCibilReport(getFileRequest.getRefID(),
                getFileRequest.getHeader().getInstitutionId());

        if (pdfBytes != null) {

            byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);

            Document doc = new Document();

            doc.setDocName(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue());

            doc.setByteCode(new String(encodeBase64));

            doc.setStatus(Constant.SUCCESS);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public List<Document> getCibilPdfReportWithCoapplicant(GetFileRequest cibilPdfRequest) {
        List<Document> documents = new ArrayList<Document>();

        Document doc = new Document();
        Set<String> coapplicantIds = multibureauRepository.getCoApplicantIds(cibilPdfRequest.getRefID());

        //This will fetch data from gonogocustomer collection as it a primary applicant application data
        doc = GngUtils.convertBaseResponsePayload(getCibilPdfReport(cibilPdfRequest), Document.class);

        documents.add(doc);

        //This will fetch data from CoApplicantMbResponse
        if (coapplicantIds != null && !coapplicantIds.isEmpty()) {
            for (String coApplicantId : coapplicantIds) {
                doc = new Document();
                doc.setDocName(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue());

                if (StringUtils.isNotBlank(coApplicantId)) {
                    byte[] pdfBytes = uploadFileRepository.getCibilReportForCoApplicant(coApplicantId);

                    if (pdfBytes != null) {

                        byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);
                        doc.setByteCode(new String(encodeBase64));

                        doc.setStatus(Constant.SUCCESS);

                    } else {
                        doc.setStatus(Constant.FAILED);
                    }
                    documents.add(doc);
                } else {
                    doc.setStatus(Constant.FAILED);
                }
            }
        }

        return documents;
    }

    @Override
    public BaseResponse getCibilPdfReportForCoapplicant(GetFileRequest cibilPdfRequest) {

        Document doc = new Document();
        String coApplicantId = cibilPdfRequest.getRefID();

        doc = new Document();
        doc.setDocName(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue());

        byte[] pdfBytes = uploadFileRepository.getCibilReportForCoApplicant(coApplicantId);

        if (pdfBytes != null) {

            byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);
            doc.setByteCode(new String(encodeBase64));

            doc.setStatus(Constant.SUCCESS);

        } else {
            doc.setStatus(Constant.FAILED);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, doc);
    }

    /**
     * This method calls AmazonS3CloudStorageService and uploads the file to
     * amazon.
     *
     * @param fileUploadResponse
     * @param uploadFileDetails
     * @param fileHeader
     */
    private void saveImageToAmazonS3(FileUploadResponse fileUploadResponse,
                                     UploadFileDetails uploadFileDetails, FileHeader fileHeader) throws Exception {

        boolean isProfilePic = false;

        if (StringUtils.isBlank(fileHeader.getInstitutionId()))
            return;

        ApplicationRequest applicationRequest = applicationRepository
                .getApplicationRequestByRefId(
                        fileUploadResponse.getGonogoRefId(),
                        fileHeader.getInstitutionId());

        if (applicationRequest == null)
            return;

        /**
         * Get amazon S3 app and upload the image to S3.
         */

        Map<String, AmazonS3Configuration> s3ConfigMap = Cache.URL_CONFIGURATION
                .getAmazonS3Configuration();

        AmazonS3Configuration s3Config = s3ConfigMap != null ? s3ConfigMap
                .get(fileHeader.getInstitutionId()) : null;

        SalesForceInfo salesForceInfo = salesForceRepository
                .getByApplicationRefId(fileUploadResponse.getGonogoRefId());

        if (null != salesForceInfo && null != salesForceInfo.getLeadId()
                && null != s3Config) {

            /**
             * Create S3storageService instance by providing it accessKey and
             * secretKey details.
             */
            AmazonS3CloudStorageService s3StorageService = new AmazonS3CloudStorageService(
                    s3Config.getAccessKey(), s3Config.getSecretKey());

            /**
             * Get file from the mongo database by its id.
             */
            GridFSDBFile gridFsDbFile = uploadFileRepository.getById(
                    fileUploadResponse.getFileId(),
                    fileHeader.getInstitutionId());

            if (null != gridFsDbFile
                    && !uploadFileRepository
                    .isUploadedToAmazonS3(fileUploadResponse
                            .getFileId())) {

                /**
                 * Target file name will be leadId/filename eg. lead id is
                 * 1004567 and filename is upload.jpg then target file name will
                 * be "1004567/upload.jpg"
                 */

                // TODO: remove hardcoded .jpg from below line and add

                /**
                 * uploadFileDetails.getFileExtension() once UI side code is changed.
                 */

                String targetFileName = salesForceInfo.getLeadId() + "/"
                        + gridFsDbFile.getFilename() + ".jpg";

                AmazonS3UploadInputStreamRequest uploadRequest = new AmazonS3UploadInputStreamRequest();
                uploadRequest.mapObjectFromMongoGridFsFile(gridFsDbFile,
                        s3Config.getBucketName(), targetFileName);

                AmazonS3UploadFileResponse response = s3StorageService
                        .uploadFile(uploadRequest);

                uploadFileRepository.updateFileAmazonUrl(
                        fileUploadResponse.getFileId(), response.getS3Url());

                if (uploadFileDetails.getFileType().equals(
                        KycJsonKeys.APPLICANT_PHOTO)) {
                    isProfilePic = true;
                }
                salesForceRepository.updateAmazonS3UrlInSalesForce(
                        response.getS3Url(), salesForceInfo.getLeadId(),
                        fileHeader.getInstitutionId(), isProfilePic,
                        uploadFileDetails, salesForceInfo,
                        fileHeader.getApplicantId());

                logger.info("Amazon S3 upload response:- s"
                        + response.toString());
            }
        }
    }

    public BaseResponse saveImage(FileUploadRequest fileUploadRequest)
            throws Exception {

        FileUploadResponse fileUploadResponse = new FileUploadResponse();

        fileUploadRequest.getFileHeader().setDateTime(new Date());

        String fileName = fileUploadRequest.getUploadFileDetails().getFileName();

        String fileType = fileUploadRequest.getUploadFileDetails().getFileType();

        byte[] imageByte = Base64.decodeBase64(fileUploadRequest.getUploadFileDetails().getFileData());

        try (InputStream inputStream = new ByteArrayInputStream(imageByte)) {

            String refId = fileUploadRequest.getGonogoReferanceId();

            String parentRefId = applicationRepository.getParentRefId(refId);

            Set<String> appImageLinkage = processApplicationImageLinkage(parentRefId, refId);

            fileUploadRequest.setAppImageLinkage(appImageLinkage);

            String res = uploadFileRepository.store(inputStream, fileName, fileType, fileUploadRequest);

            uploadFileRepository.updateStatus(fileUploadRequest);

            if (res != null) {

                fileUploadResponse.setGonogoRefId(fileUploadRequest.getGonogoReferanceId());
                fileUploadResponse.setFileId(res);
                fileUploadResponse.setStatus(Constant.SUCCESS);

                if (parentRefId != null && !appImageLinkage.isEmpty()) {
                    updateParentAppImageLinkage(appImageLinkage);
                }

                try {

                    saveImageToAmazonS3(fileUploadResponse, fileUploadRequest.getUploadFileDetails(), fileUploadRequest.getFileHeader());

                } catch (Exception e) {

                    logger.error("{}",e.getStackTrace());

                    logger.error("Exception in fileUpload: " + e);

                    throw new Exception("Exception in fileUpload:", e);
                }

            } else {

                fileUploadResponse.setStatus(Constant.ERROR);

            }
        } catch (Exception e) {

            logger.error("Exception in fileUpload: " + e);
            fileUploadResponse.setStatus(Constant.ERROR);
            throw new Exception("Exception in fileUpload:", e);
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadResponse);

    }

    /**
     * It will update all the application image linkage for parents application
     *
     * @param appImageLinkage
     */
    private void updateParentAppImageLinkage(Set<String> appImageLinkage) {
        for (String refId : appImageLinkage) {
            uploadFileRepository.updateAppImageLinkage(refId, appImageLinkage);
        }
    }

    /**
     * This method will return application image linkage set of parent
     * application With current refrence id added to the same
     *
     * @param parentRefId
     * @param currentRefId
     * @return
     */
    private Set<String> processApplicationImageLinkage(String parentRefId,
                                                       String currentRefId) {
        Set<String> appImageLinkage = null;
        if (StringUtils.isNotBlank(parentRefId)) {
            appImageLinkage = uploadFileRepository
                    .getAppImageLinkageArray(parentRefId);
        }

        if (appImageLinkage == null) {
            appImageLinkage = new TreeSet<String>();
        }
        appImageLinkage.add(currentRefId);
        return appImageLinkage;
    }

    @Override
    public BaseResponse sendMailToCustomer(GetFileRequest getImageRequest) throws Exception {

        boolean actionFlag = lookupService.checkActionsAccess(getImageRequest.getHeader().getInstitutionId(),
                getImageRequest.getHeader().getProduct().toProductId(), ActionName.SENT_EMAIL);

        EmailConfiguration emailConfiguration;

        Document document = new Document();

        if (actionFlag) {

            emailConfiguration = lookupService.getEmailConfiguration(getImageRequest.getHeader().getInstitutionId(),
                    getImageRequest.getHeader().getProduct().toProductId());

            if (null == emailConfiguration) {

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());


                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
        } else {

            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors);

        }

        BaseResponse baseResponse;

        if (null != getImageRequest && null != getImageRequest.getHeader()
                && StringUtils.isNotBlank(getImageRequest.getHeader().getInstitutionId())) {
            getImageRequest.getHeader().setDateTime(new Date());

            if (adminLogRepository.checkEmailDedupe(getImageRequest)) {
                adminLogRepository.saveEmailLog(getImageRequest);
                document.setStatus(Constant.SUCCESS);
                logger.debug("case in dedupe.");

                return GngUtils.getBaseResponse(HttpStatus.OK, document);

            }
            if (EmailRequestDeduped.isInprocess(getImageRequest)) {
                if (adminLogRepository.checkEmailDedupe(getImageRequest)) {
                    EmailRequestDeduped.removeEmailRegistry(getImageRequest);
                    getImageRequest.setEmailProcessStatus(Status.DEDUPE_FOUND.toString());
                    adminLogRepository.saveEmailLog(getImageRequest);
                    document.setStatus(Constant.SUCCESS);
                    logger.debug("case in dedupe.");

                    return GngUtils.getBaseResponse(HttpStatus.OK, document);

                }

                getImageRequest.setEmailProcessStatus(Status.IN_PROCESS.toString());
                adminLogRepository.saveEmailLog(getImageRequest);
                document.setStatus(Constant.SUCCESS);

                saveDeliveryOrderMailLog(getImageRequest, DoStatusEnum.MAIL_IN_PROGRESS.name());

                return GngUtils.getBaseResponse(HttpStatus.OK, document);

            } else {
                if (adminLogRepository.checkEmailDedupe(getImageRequest)) {
                    getImageRequest.setEmailProcessStatus(Status.DEDUPE_FOUND.toString());
                    adminLogRepository.saveEmailLog(getImageRequest);
                    document.setStatus(Constant.SUCCESS);
                    logger.debug("case in dedupe.");

                    return GngUtils.getBaseResponse(HttpStatus.OK, document);

                }
            }

            List<String> recipentList = new ArrayList<>();
            Set<String> ccList = new HashSet<>();
            Set<String> bccList = new HashSet<>();

            // 1. Temporary Solution to find dealerID
            String dealerID = adminLogRepository.getProcessedApplication(getImageRequest.getRefID(),
                    getImageRequest.getHeader().getInstitutionId());
            // ***********************************

            /**
             * Following db call get permanent cc list and to list if and only
             * if UAT region from our custom collection.
             */
            List<GNGDealerEmailMaster> gngDealerEmailMastersList = adminLogRepository.getGngDealerEmailMapping(getImageRequest.getHeader().getProduct().name(),
                    getImageRequest.getHeader().getInstitutionId());
            if (gngDealerEmailMastersList != null) {
                for (GNGDealerEmailMaster gngDealerEmailMaster : gngDealerEmailMastersList) {
                    if ("TO".equalsIgnoreCase(gngDealerEmailMaster.getRecipientType())) {
                        recipentList.add(gngDealerEmailMaster.getEmail());
                    } else if ("CC".equalsIgnoreCase(gngDealerEmailMaster.getRecipientType())) {
                        ccList.add(gngDealerEmailMaster.getEmail());
                    } else if ("BCC".equalsIgnoreCase(gngDealerEmailMaster.getRecipientType())) {
                        bccList.add(gngDealerEmailMaster.getEmail());
                    } else {
                        logger.warn("Unknown recipient or email ID not valid ==>" + gngDealerEmailMaster.getEmail());
                    }
                }
            }

            if (StringUtils.isNotBlank(dealerID)) {

                List<DealerEmailMaster> dealerEmailMasters = null;
                // means its not uat or sit environment i.e.production environment
                DigitizationDealerEmailMaster digitizationDealerEmailMaster = null;
                if (CollectionUtils.isEmpty(recipentList)) {

                    dealerEmailMasters = adminLogRepository.getDealerEmailMapping(dealerID,
                            getImageRequest.getHeader().getInstitutionId());

                    if (!CollectionUtils.isEmpty(dealerEmailMasters)) {
                        for (DealerEmailMaster dealerEmailMaster : dealerEmailMasters) {
                            recipentList.add(dealerEmailMaster.getEmail());
                        }
                    }
                    digitizationDealerEmailMaster = adminLogRepository.getEmailCcIdFromDigiEmailMaster(dealerID, getImageRequest.getHeader().getInstitutionId());

                    if (null != digitizationDealerEmailMaster && !CollectionUtils.isEmpty(digitizationDealerEmailMaster.getDeliveryOrderCcEmailIds())) {
                        ccList.addAll(digitizationDealerEmailMaster.getDeliveryOrderCcEmailIds());
                    }else{
                        logger.debug("Email CC ids not found in the digitizationDealerEmailMaster against dealer Id {} ",dealerID);
                    }

                }

            } else {
                logger.error(String.format("Dealer Id not found against the application {%s} for send email purpose ", getImageRequest.getRefID()));
            }

            MailRequest mailRequest = new MailRequest();
            mailRequest.setInstitutionId(getImageRequest.getHeader().getInstitutionId());
            if (!ccList.isEmpty()) {
                String[] ccArray = ccList.toArray(new String[ccList.size()]);
                mailRequest.setCc(ccArray);
            }
            if (!recipentList.isEmpty()) {
                String[] recipentArray = recipentList.toArray(new String[recipentList.size()]);
                mailRequest.setTo(recipentArray);
            }
            mailRequest.setSubject(emailConfiguration.getEmailSubject() + " " + getImageRequest.getRefID());
            mailRequest.setContent(emailConfiguration.getCopyRight());
            logger.warn("email sent To ==> " + Arrays.toString(mailRequest.getTo()) + "email sent CC ==> "
                    + Arrays.toString(mailRequest.getCc()) + "For Reference Id : " + getImageRequest.getRefID());
            mailRequest.setRefID(getImageRequest.getRefID());
            List<Attachment> attachments = null;
            try {
                attachments = getAttachments(getImageRequest, emailConfiguration.getAttachments());
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
            if (null != attachments && attachments.size() > 0) {
                mailRequest.setAttachments(attachments);
            }

            EmailServiceConfiguration config = Cache.URL_CONFIGURATION.getEmailServiceConfiguration().get(getImageRequest.getHeader()
                    .getInstitutionId());

            MessagingServiceResponse smsServiceResponse = (MessagingServiceResponse) TransportUtils.
                    postJsonRequest(mailRequest, config.getEmailUrl(),
                            MessagingServiceResponse.class);

            if (smsServiceResponse != null && smsServiceResponse.getStatus().equals("OK")) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, getDocument(true, getImageRequest, document));

            } else {
                /**
                 * To register in dedup log.
                 */
                getDocument(false, getImageRequest, document);

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .id(smsServiceResponse.getError().getType())
                        .message(smsServiceResponse.getError().getMessage())
                        .level(Error.SEVERITY.HIGH.name())
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse sendDigitizeMailToCustomer(GetFileRequest getImageRequest) throws Exception {
        boolean actionFlag = lookupService.checkActionsAccess(getImageRequest.getHeader().getInstitutionId(),
                getImageRequest.getHeader().getProduct().toProductId(), ActionName.SENT_EMAIL);

        BaseResponse baseResponse;

        EmailConfiguration emailConfiguration;

        Document document = new Document();

        if (actionFlag) {

            emailConfiguration = lookupService.getEmailConfiguration(getImageRequest.getHeader().getInstitutionId(),
                    getImageRequest.getHeader().getProduct().toProductId());

            if (null == emailConfiguration) {

                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .level(Error.SEVERITY.CRITICAL.name())
                        .build());

                return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
            }
        } else {

            Collection<Error> errors = new ArrayList<>();

            errors.add(Error.builder()
                    .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors);

        }

        if (null != getImageRequest
                && null != getImageRequest.getHeader()
                && StringUtils.isNotBlank(getImageRequest.getHeader().getInstitutionId())) {

            getImageRequest.getHeader().setDateTime(new Date());

            if (adminLogRepository.checkEmailDedupe(getImageRequest)) {

                adminLogRepository.saveEmailLog(getImageRequest);

                document.setStatus(Constant.SUCCESS);

                logger.debug("case in dedupe.");

                return GngUtils.getBaseResponse(HttpStatus.OK, document);

            }
            if (EmailRequestDeduped.isInprocess(getImageRequest)) {

                if (adminLogRepository.checkEmailDedupe(getImageRequest)) {

                    EmailRequestDeduped.removeEmailRegistry(getImageRequest);

                    getImageRequest.setEmailProcessStatus(Status.DEDUPE_FOUND.toString());

                    adminLogRepository.saveEmailLog(getImageRequest);

                    document.setStatus(Constant.SUCCESS);

                    logger.debug("case in dedupe.");

                    return GngUtils.getBaseResponse(HttpStatus.OK, document);

                }

                getImageRequest.setEmailProcessStatus(Status.IN_PROCESS.toString());
                adminLogRepository.saveEmailLog(getImageRequest);
                document.setStatus(Constant.SUCCESS);

                return GngUtils.getBaseResponse(HttpStatus.OK, document);

            } else {
                if (adminLogRepository.checkEmailDedupe(getImageRequest)) {
                    getImageRequest.setEmailProcessStatus(Status.DEDUPE_FOUND.toString());
                    adminLogRepository.saveEmailLog(getImageRequest);
                    document.setStatus(Constant.SUCCESS);
                    logger.debug("case in dedupe.");

                    return GngUtils.getBaseResponse(HttpStatus.OK, document);

                }
            }

            Set<String> recipientList = new HashSet<>();
            Set<String> ccList = new HashSet<>();
            Set<String> bccList = new HashSet<>();

            String dealerID = adminLogRepository.getProcessedApplication(getImageRequest.getRefID(),
                    getImageRequest.getHeader().getInstitutionId());

            /**
             * Following db call get permanent cc list and to list if and only
             * if UAT region from our custom collection.
             */
            List<GNGDealerEmailMaster> gngDealerEmailMastersList = adminLogRepository.getGngDealerEmailMapping(getImageRequest.getHeader().getProduct().name(),
                    getImageRequest.getHeader().getInstitutionId());
            /**
             * Populate data in recipient Set form master
             */
            if (gngDealerEmailMastersList != null) {
                for (GNGDealerEmailMaster gngDealerEmailMaster : gngDealerEmailMastersList) {
                    if (StringUtils.equalsIgnoreCase("TO", gngDealerEmailMaster.getRecipientType())) {
                        recipientList.add(gngDealerEmailMaster.getEmail());
                    } else if (StringUtils.equalsIgnoreCase("CC", gngDealerEmailMaster.getRecipientType())) {
                        ccList.add(gngDealerEmailMaster.getEmail());
                    } else if (StringUtils.equalsIgnoreCase("BCC", gngDealerEmailMaster.getRecipientType())) {
                        bccList.add(gngDealerEmailMaster.getEmail());
                    } else {
                        logger.warn("Unknown recipient or email ID not valid ==>" + gngDealerEmailMaster.getEmail());
                    }
                }
            }

            if (dealerID != null) {
                List<DealerEmailMaster> dealerEmailMasters = null;
                if (recipientList.isEmpty()) {
                    dealerEmailMasters = adminLogRepository.getDealerEmailMapping(dealerID,
                            getImageRequest.getHeader().getInstitutionId());
                }
                if (dealerEmailMasters != null) {
                    for (DealerEmailMaster dealerEmailMaster : dealerEmailMasters) {
                        recipientList.add(dealerEmailMaster.getEmail());
                    }
                }
            }

            MailRequest mailRequest = new MailRequest();
            mailRequest.setInstitutionId(getImageRequest.getHeader().getInstitutionId());
            if (!ccList.isEmpty()) {
                mailRequest.setCc(ccList.toArray(new String[ccList.size()]));
            }
            if (!recipientList.isEmpty()) {
                mailRequest.setTo(recipientList.toArray(new String[recipientList.size()]));
            }

            mailRequest.setSubject(emailConfiguration.getEmailSubject() + " " + getImageRequest.getRefID());

            mailRequest.setContent(emailConfiguration.getCopyRight());

            logger.warn("email sent To ==> " + Arrays.toString(mailRequest.getTo()) + "email sent CC ==> "
                    + Arrays.toString(mailRequest.getCc()) + "For Reference Id : " + getImageRequest.getRefID());

            mailRequest.setRefID(getImageRequest.getRefID());

            List<Attachment> attachments = null;

            try {
                attachments = getAttachments(getImageRequest, emailConfiguration.getSecondaryAttachments());
            } catch (IOException e) {
                logger.error("Error occurred while generating attachments with probable cause [{}]", e.getMessage());
            }

            if (null != attachments && attachments.size() > 0) {
                mailRequest.setAttachments(attachments);
            }

            EmailServiceConfiguration config = Cache.URL_CONFIGURATION.getEmailServiceConfiguration().get(getImageRequest.getHeader()
                    .getInstitutionId());

            MessagingServiceResponse smsServiceResponse = (MessagingServiceResponse) TransportUtils.
                    postJsonRequest(mailRequest, config.getEmailUrl(),
                            MessagingServiceResponse.class);

            if (smsServiceResponse != null && smsServiceResponse.getStatus().equals(Status.OK.name())) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, getDocument(true, getImageRequest, document));

            } else {
                /**
                 * To register in dedup log.
                 */
                getDocument(false, getImageRequest, document);

                Collection<Error> errors = new ArrayList<>();

                errors.add(Error.builder()
                        .id(smsServiceResponse.getError().getType())
                        .message(smsServiceResponse.getError().getMessage())
                        .level(Error.SEVERITY.HIGH.name())
                        .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                        .build());
                baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.THIRD_PARTY_INTERNAL_SERVER_ERROR, errors);
            }

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getBureauPdfReport(GetFileRequest getFileRequest, String bureau) {
        BaseResponse baseResponse;

        byte[] pdfBytes = uploadFileRepository.getBureauReport(getFileRequest.getRefID(),
                getFileRequest.getHeader().getInstitutionId(),bureau);

        if (pdfBytes != null && pdfBytes.length>0) {

            byte[] base64Bytes = Base64.encodeBase64(pdfBytes);

            Document doc = new Document();

            doc.setDocName(GNGWorkflowConstant.PDF_REPORT.toFaceValue());

            doc.setByteCode(new String(base64Bytes));

            doc.setStatus(Constant.SUCCESS);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    private List<Attachment> getAttachments(GetFileRequest getImageRequest, Set<TemplateName> allowedAttachments)
            throws Exception {
        String fileID = getImageRequest.getImageFileID();
        String PDF_CONTENT_TYPE = MediaType.APPLICATION_PDF_VALUE;
        String FILE_EXTENSION_PDF = ".pdf";

        List<Attachment> attachments = new ArrayList<Attachment>();
        Attachment attachment;
        for (TemplateName templateName : allowedAttachments) {
            switch (templateName) {
                case DELIVERY_ORDER: {
                    if (StringUtils.isNotBlank(fileID)) {
                        GridFSDBFile gridFSDBFile = uploadFileRepository.getById(fileID,
                                getImageRequest.getHeader().getInstitutionId());
                        if (null != gridFSDBFile) {
                            try {
                                attachment = new Attachment();
                                attachment.setContentType(PDF_CONTENT_TYPE);
                                attachment.setFile(IOUtils.toByteArray(gridFSDBFile.getInputStream()));
                                attachment.setFileName(getImageRequest.getRefID() + FILE_EXTENSION_PDF);
                                attachments.add(attachment);
                            } catch (Exception e) {
                                logger.error("Error occurred while generating DO attachment with probable cause [{}]", e.getMessage());
                            }
                        }
                    }else {
                        logger.warn("Do generation call without fileId.");
                    }
                }
                break;

                case APPLICATION_FORM: {
                    Document applicationFormDocument = GngUtils.convertBaseResponsePayload(digitizationManager.getApplicationForm(
                            getImageRequest.getHeader().getInstitutionId(), getImageRequest.getHeader().getProduct().toProductId(), getImageRequest.getRefID()), Document.class);

                    if (null != applicationFormDocument && StringUtils.isNotBlank(applicationFormDocument.getByteCode())) {
                        attachment = new Attachment();
                        attachment.setContentType(PDF_CONTENT_TYPE);
                        attachment.setFileName(TemplateName.APPLICATION_FORM.name() + FieldSeparator.UNDER_SQURE + getImageRequest.getRefID() + FILE_EXTENSION_PDF);
                        attachment.setFile(Base64.decodeBase64(applicationFormDocument.getByteCode()));
                        attachments.add(attachment);
                    }
                }
                break;

                case AGREEMENT_FORM: {

                    Document agreementFormDocument = GngUtils.convertBaseResponsePayload(digitizationManager.getAgreementForm(
                            getImageRequest.getHeader().getInstitutionId(), getImageRequest.getHeader().getProduct().toProductId(), getImageRequest.getRefID()), Document.class);

                    if (null != agreementFormDocument && StringUtils.isNotBlank(agreementFormDocument.getByteCode())) {
                        attachment = new Attachment();
                        attachment.setContentType(PDF_CONTENT_TYPE);
                        attachment.setFileName(
                                TemplateName.AGREEMENT_FORM.name() + FieldSeparator.UNDER_SQURE + getImageRequest.getRefID() + FILE_EXTENSION_PDF);
                        attachment.setFile(Base64.decodeBase64(agreementFormDocument.getByteCode()));
                        attachments.add(attachment);
                    }
                }
                break;

                default:
                    break;
            }
        }

        return attachments;
    }

    Document getDocument(boolean status, GetFileRequest getImageRequest, Document doc) throws Exception {

        if (status) {
            getImageRequest.setEmailProcessStatus(Status.COMPLETE
                    .toString());
            adminLogRepository.saveEmailLog(getImageRequest);
            doc.setStatus(Constant.SUCCESS);
            EmailRequestDeduped
                    .removeEmailRegistry(getImageRequest);
            //save delivery order mail log
            saveDeliveryOrderMailLog(getImageRequest ,DoStatusEnum.MAILED.name());

            return doc;
        } else {
            getImageRequest.setEmailProcessStatus(Status.FAIL
                    .toString());
            adminLogRepository.saveEmailLog(getImageRequest);
            doc.setStatus(Constant.FAILED);
            EmailRequestDeduped
                    .removeEmailRegistry(getImageRequest);

            saveDeliveryOrderMailLog(getImageRequest, DoStatusEnum.MAIL_FAILED.name());
            return doc;
        }
    }

    private void saveDeliveryOrderMailLog(GetFileRequest getImageRequest ,String mailStatus) {

        doOperationRepository.updateDoStatusInPostIpa(getImageRequest.getRefID(),
                getImageRequest.getHeader().getInstitutionId(), mailStatus);

        DoOperationLog doOperationLog = doOperationBuilder.buildDoOperationLog(getImageRequest.getRefID()
                , GngUtils.getFileHeader(getImageRequest.getHeader()), mailStatus);

        //save doOperationLog
        doOperationRepository.saveDoOperationLog(doOperationLog);
    }

    public BaseResponse getDocument(GetFileRequest getFileRequest) {
        BaseResponse baseResponse;
        //Get the document
        GridFSDBFile gridFSDBFile = uploadFileRepository.getDocument(getFileRequest);
        if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {
            try (InputStream inputStream = gridFSDBFile.getInputStream()) {
                Document doc = new Document();
                byte[] pdfBytes = IOUtils.toByteArray(inputStream);
                byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);
                doc.setDocName(getFileRequest.getDocumentType()+getFileRequest.getDocumentSubType());
                doc.setByteCode(new String(encodeBase64));
                doc.setDocID(gridFSDBFile.getId().toString());
                doc.setStatus(Constant.SUCCESS);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);
            } catch (Exception e) {
                logger.error("Exception: " + e);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    public BaseResponse saveDocumentToDB(FileUploadRequest fileUploadRequest, MultipartFile file) {
        ActivityLogs activityLogs = auditHelper.createActivityLog(fileUploadRequest.getFileHeader(), fileUploadRequest.getGonogoReferanceId(), null,
                fileUploadRequest.getUploadFileDetails().getFileName(), EndPointReferrer.UPLOAD_DOCUMENT,
                fileUploadRequest.getUploadFileDetails().getFileType(), fileUploadRequest.getFileHeader().getLoggedInUserId());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        FileUploadResponse fileUploadResponse = new FileUploadResponse();
        fileUploadRequest.getFileHeader().setDateTime(new Date());

        String fileName = fileUploadRequest.getUploadFileDetails().getFileName();
        String fileType = fileUploadRequest.getUploadFileDetails().getFileType();

        try {
            if (file.getInputStream().available() != 0) {
                String res = uploadFileRepository.store(file.getInputStream(), fileName, fileType, fileUploadRequest);
                if (res != null) {
                    fileUploadResponse.setGonogoRefId(fileUploadRequest.getGonogoReferanceId());
                    fileUploadResponse.setFileId(res);
                    fileUploadResponse.setStatus(Constant.SUCCESS);
                } else {
                    fileUploadResponse.setStatus(Constant.ERROR);
                }
            } else {
                fileUploadResponse.setStatus(Constant.ERROR);
            }

        } catch (Exception e) {
            logger.error(ErrorCode.FILE_UPLOAD_EXCEPTION + e);
            fileUploadResponse.setStatus(Constant.ERROR);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DocumentStoreManagerImpl in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return GngUtils.getBaseResponse(HttpStatus.OK, fileUploadResponse);
    }

    public BaseResponse getApplicationDocument(GetFileRequest getFileRequest) {
        BaseResponse baseResponse;
        //Get the document
        GridFSDBFile gridFSDBFile = uploadFileRepository.getApplicationDocument(getFileRequest);
        if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {
            try (InputStream inputStream = gridFSDBFile.getInputStream()) {
                Document doc = new Document();
                byte[] pdfBytes = IOUtils.toByteArray(inputStream);
                byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);
                doc.setDocName(getFileRequest.getDocumentType()+getFileRequest.getDocumentSubType());
                doc.setByteCode(new String(encodeBase64));
                doc.setDocID(gridFSDBFile.getId().toString());
                doc.setStatus(Constant.SUCCESS);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);
            } catch (Exception e) {
                logger.error("Exception: " + e);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    public BaseResponse getLoanAppDocument(FileUploadRequest fileUploadRequest) {
        BaseResponse baseResponse;
        //Get the document
        GridFSDBFile gridFSDBFile = uploadFileRepository.getLoanAppDocument(fileUploadRequest);
        if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {
            try (InputStream inputStream = gridFSDBFile.getInputStream()) {
                Document doc = new Document();
                byte[] pdfBytes = IOUtils.toByteArray(inputStream);
                byte[] encodeBase64 = Base64.encodeBase64(pdfBytes);
                doc.setDocName(fileUploadRequest.getUploadFileDetails().getDocumentType()
                        + fileUploadRequest.getUploadFileDetails().getDocumentSubType());
                doc.setByteCode(new String(encodeBase64));
                doc.setDocID(gridFSDBFile.getId().toString());
                doc.setStatus(Constant.SUCCESS);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, doc);
            } catch (Exception e) {
                logger.error("Exception: " + e);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getDocumentImageDetailsByRefId(CheckApplicationStatus checkApplicationStatus) {
        BaseResponse baseResponse;
        List<ImagesDetails> fileDetails = uploadFileRepository.getSignedFileDetails(checkApplicationStatus);
        if(!CollectionUtils.isEmpty(fileDetails)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,fileDetails);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteDocument(FileUploadRequest fileUploadRequest) {
        ActivityLogs activityLogs = auditHelper.createActivityLog(fileUploadRequest.getFileHeader(), fileUploadRequest.getGonogoReferanceId(), null,
                fileUploadRequest.getUploadFileDetails().getFileName(), EndPointReferrer.DELETE_DOCUMENT,
                fileUploadRequest.getUploadFileDetails().getFileType(), fileUploadRequest.getFileHeader().getLoggedInUserId());
        activityLogs.setAction("DELETE_DOCUMENT");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        BaseResponse baseResponse;
        logger.info("{} delete document BY user {} ", fileUploadRequest.getGonogoReferanceId(), fileUploadRequest.getFileHeader().getDsaId());
        logger.info("{} file name  {} file Type", fileUploadRequest.getUploadFileDetails().getFileName(), fileUploadRequest.getUploadFileDetails().getFileType());
        boolean flag = uploadFileRepository.deleteDocument(fileUploadRequest);
        if(flag){
            FileUploadResponse response = new FileUploadResponse();
            response.setGonogoRefId(fileUploadRequest
                    .getGonogoReferanceId());
            response.setFileId(fileUploadRequest.getImageID());
            response.setStatus(Constant.SUCCESS);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, response);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from DocumentStoreManagerImpl in thread %s",
                Thread.currentThread().getName()));
        activityEventPublisher.publishEvent(activityLogs);
        return baseResponse;
    }
}
