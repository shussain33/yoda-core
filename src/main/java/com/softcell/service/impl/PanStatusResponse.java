package com.softcell.service.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.Error;
import com.softcell.gonogo.model.core.kyc.response.karza.PanStatusCheck;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by ssg222 on 13/6/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PanStatusResponse {

    @JsonProperty("path")
    private String path;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("payload")
    private PanStatusCheck payload;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("error")
    private String error;

    @JsonProperty("errors")
    private ArrayList<Error> errors;

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;
}
