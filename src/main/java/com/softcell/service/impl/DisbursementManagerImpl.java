package com.softcell.service.impl;

import com.softcell.constants.ActionName;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.exceptions.TcLosException;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.los.tvs.LosTvsRequest;
import com.softcell.gonogo.model.request.AppDisbursalRequest;
import com.softcell.gonogo.model.request.GenSapFileRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.service.TCLosManager;
import com.softcell.gonogo.service.factory.SMSReqBuilder;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosRequestBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.rest.controllers.ApplicationController;
import com.softcell.service.ApplicationManager;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.service.DisbursementManager;
import com.softcell.service.LOSGoNoGoApplicationService;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by anupamad on 10/8/17.
 */
@Service
public class DisbursementManagerImpl implements DisbursementManager {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @Autowired
    private ApplicationManager applicationManager;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    @Autowired
    private SMSReqBuilder sMSReqBuilder;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private TCLosManager tcLosManager;

    @Autowired
    private LosRequestBuilder losRequestBuilder;

    @Autowired
    private LOSGoNoGoApplicationService losGoNoGoApplicationService;

    public BaseResponse disburseLoan(AppDisbursalRequest appDisbursalRequest) throws Exception {
        logger.info("Inside disburseLoan");
        BaseResponse baseResponse = null;

        ActivityLogs activityLog = auditHelper.createActivityLog(null, appDisbursalRequest.getHeader());
        activityLog.setAction(GNGWorkflowConstant.DISBURSE_LOAN.toFaceValue());
        activityLog.setStage(GNGWorkflowConstant.DISB.toFaceValue());
        activityLog.setRefId(appDisbursalRequest.getRefID());
        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository
                .getGoNoGoCustomerApplicationByRefId(appDisbursalRequest.getRefID()
                        , appDisbursalRequest.getHeader().getInstitutionId());
        if (goNoGoCroApplicationResponse == null) {
            logger.info("No Application found with Ref ID {} ", appDisbursalRequest.getRefID());
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        logger.info("Application found with Ref ID {} ", appDisbursalRequest.getRefID());

        String currentStageId = goNoGoCroApplicationResponse.getApplicationRequest().getCurrentStageId();
        boolean isDisbursed = StringUtils.equalsIgnoreCase(currentStageId, GNGWorkflowConstant.DISB.toFaceValue());

        if(isDisbursed) {
            logger.error(ErrorCode.APPLICATION_ALREADY_DISBURSED);
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.APPLICATION_ALREADY_DISBURSED)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(ErrorCode.APPLICATION_ALREADY_DISBURSED);
            activityEventPublisher.publishEvent(activityLog);
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, errors);
        }

        //Generate and set reference Number
        BaseResponse generateRefNumResponse = applicationManager.generateRefNum(goNoGoCroApplicationResponse);

        if (null != generateRefNumResponse) {
            if( generateRefNumResponse.getStatus().getStatusCode() == HttpStatus.OK.value()) {
             try {
                    tcLosManager.buildTCLosData(goNoGoCroApplicationResponse
                            , false, "", "");
                } catch (Exception ex) {
                    logger.error("Error in SAP file generation : {} ", ex.getMessage());
                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ex.getMessage())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                    activityLog.setCustomMsg(ErrorCode.ERROR_SAP_FILE_NOTGENERATED);
                    activityEventPublisher.publishEvent(activityLog);
                    return GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,errors);
                }
                if (applicationRepository.updateDisbursementData(appDisbursalRequest.getRefID()
                        , goNoGoCroApplicationResponse.getAgreementNum()
                        , GNGWorkflowConstant.DISB.toFaceValue()
                        ,goNoGoCroApplicationResponse.getApplicationRequest().getHeader().getDealerId())) {
                    if (lookupService.checkActionsAccess(
                            appDisbursalRequest.getHeader().getInstitutionId()
                            , appDisbursalRequest.getHeader().getProduct().toProductId()
                            , ActionName.SEND_DISBURSEMENT_SMS)) {
                        //Get the SMS contents
                        SmsTemplateConfiguration smsTemplateConfiguration =
                                lookupService.getSmsTemplateConfiguration(
                                        appDisbursalRequest.getHeader().getInstitutionId()
                                        , appDisbursalRequest.getHeader().getProduct().toProductId()
                                        , GNGWorkflowConstant.DISBURSEMENT_SMS.name());
                        String smsMessage = null;
                        if (smsTemplateConfiguration != null) {
                            smsMessage = smsTemplateConfiguration.getFormattedSMSText(
                                    goNoGoCroApplicationResponse.getGngRefId()
                                    , goNoGoCroApplicationResponse.getAgreementNum());
                        } else {
                            smsMessage = "Loan disbursement Done. For Ref Id : " + appDisbursalRequest.getRefID();
                        }
                        //Send SMS
                        BaseResponse smsResponse = clientAuthenticationManager.sendSMS(
                                goNoGoCroApplicationResponse.getApplicationRequest(), smsMessage);
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, true);
                        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                        baseResponse.setPayload(smsResponse.getPayload());
                        LosTvsRequest losTvsRequest = losRequestBuilder.buildLosDisbursalRequest(appDisbursalRequest);

                        try {
                            losGoNoGoApplicationService.pullAndTranformDataByCaseID(losTvsRequest);
                        } catch (Exception ex) {
                            logger.error("Error in Los Tvs Request call : {} ", ex.getMessage());
                            Collection<Error> errors = new ArrayList<>();
                            errors.add(Error.builder()
                                    .message("Error in Los call"+ex.getMessage())
                                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                                    .level(Error.SEVERITY.HIGH.name())
                                    .build());
                            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                            activityLog.setCustomMsg(ErrorCode.FAILED_LOSTVS_PULLANDTRANSFORM);
                            activityEventPublisher.publishEvent(activityLog);
                            return GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,errors);
                        }
                    }
                } else {
                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(String.format(ErrorCode.DISB_UPDATE_DATA_FAILED
                                    , appDisbursalRequest.getRefID()
                                    , appDisbursalRequest.getHeader().getInstitutionId()))
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.HIGH.name())
                            .build());
                    activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                    activityLog.setCustomMsg(String.format(ErrorCode.DISB_UPDATE_DATA_FAILED
                                    , appDisbursalRequest.getRefID()
                                    , appDisbursalRequest.getHeader().getInstitutionId()));
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,errors);
                }
            }
            else {
                //Generate refNum/agreementNum returned with error return the same response.
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(ErrorCode.GENERATE_REF_NO_ERROR);
                baseResponse = generateRefNumResponse;
            }
        }
        else {
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,
                    GngUtils.getNoContentErrorList());
        }
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    public BaseResponse genSapFile(GenSapFileRequest genSapFileRequest) throws Exception {
        logger.info("Inside genSapFile");
        GoNoGoCroApplicationResponse goNoGoCroApplicationResponse = applicationRepository
                .getGoNoGoCustomerApplicationByRefId(genSapFileRequest.getRefID()
                        , genSapFileRequest.getHeader().getInstitutionId());
        if (goNoGoCroApplicationResponse == null) {
            logger.info("No Application found with Ref ID {}.", genSapFileRequest.getRefID());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(String.format(ErrorCode.APP_NT_FND, genSapFileRequest.getRefID()))
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, errors);
        }
        try {
            tcLosManager.buildTCLosData(goNoGoCroApplicationResponse
                    , genSapFileRequest.isGenCsvFlag()
                    , genSapFileRequest.getPath()
                    , genSapFileRequest.getBranchCode()
            );
            return GngUtils.getBaseResponse(HttpStatus.OK, true);
        } catch (TcLosException ex){
            logger.error("Error in SAP file generation : {} ", ex.getMessage());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ex.getMessage())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,errors);
        }
    }
}
