package com.softcell.service.impl;

import com.mongodb.BasicDBObject;
import com.softcell.constants.ApplicantType;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.lms.LMSMongoRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.lms.*;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.service.factory.LMSBuilder;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataEntryManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.service.validator.sbfclms.SBFCLMSIntegrationValidationManager;
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by kumar on 20/7/18.
 */
@Component
public class LMSIntegrationManager {

    private static final Logger logger = LoggerFactory.getLogger(LMSIntegrationManager.class);

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private SBFCLMSIntegrationValidationManager sbfclmsIntegrationValidationManager;

    @Autowired
    LMSMongoRepository lmsMongoRepository;

    @Autowired
    LMSBuilder lmsBuilder;

    @Autowired
    ModuleHelper moduleHelper;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    RequestsWithInstitutionId requestsWithInstitutionId;

    @Autowired
    DataEntryManager dataEntryManager;

  /*  @Autowired
    ApplicationRepository applicationRepository;*/

    Map lmsMasterDataMap = new HashMap();

    Map lmsMasterDataMapList = new HashMap();

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    public RetrieveClientResponse retrieveClientByIdentifier(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        RetrieveClientResponse retrieveClientResponse;

        WFJobCommDomain urlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    sbfclmsIntegrationRequest.getOHeader().getInstitutionId(), UrlType.RETRIEVE_CLIENT_BY_IDENTIFIER.toValue());

        if (urlConfig != null) {
            try {
                retrieveClientResponse = callToSBFCLMSRetrieveClient(urlConfig, sbfclmsIntegrationRequest.getRetrieveClientRequest(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (retrieveClientResponse == null) {
                    logger.info("Getting NULL Response from SBFC-LMS Retrieve Client");

                    retrieveClientResponse = RetrieveClientResponse.builder()
                            .error(HttpStatus.NO_CONTENT.name())
                            .error_description(String.format(ErrorCode.BLANK_RESPONSE, EndPointReferrer.RETRIEVE_CLIENT_BY_IDENTIFIER))
                            .build();
                    }
                } catch (Exception e) {
                    logger.debug("Exception while getting response from LMS Retrieve Client By Identifier {}", e.getMessage());

                    retrieveClientResponse = RetrieveClientResponse.builder()
                            .error(HttpStatus.REQUEST_TIMEOUT.name())
                            .error_description(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, EndPointReferrer.RETRIEVE_CLIENT_BY_IDENTIFIER))
                            .build();
            }
        } else {
            logger.debug("Url configuration not Found for LMS Retrieve Client By Identifier");

            retrieveClientResponse = RetrieveClientResponse.builder()
                    .error(HttpStatus.PRECONDITION_FAILED.name())
                    .error_description(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .build();
        }

        return retrieveClientResponse;
    }

    private RetrieveClientResponse callToSBFCLMSRetrieveClient(WFJobCommDomain urlConfig, RetrieveClientRequest retrieveClientRequest, String institutionId) throws Exception {

        String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        // RequestsWithInstitutionId requestsWithInstitutionId = new RequestsWithInstitutionId();
        requestsWithInstitutionId.setRetrieveClientRequest(retrieveClientRequest);
        requestsWithInstitutionId.setInstitutionId(institutionId);

        return (RetrieveClientResponse) TransportUtils.postJsonRequest(requestsWithInstitutionId, url, RetrieveClientResponse.class);
    }

    public ClientCreationResponse clientCreation(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        ClientCreationResponse clientCreationResponse;

        WFJobCommDomain urlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    sbfclmsIntegrationRequest.getOHeader().getInstitutionId(), UrlType.CLIENT_CREATION.toValue());

        if (urlConfig != null) {

            try {
                clientCreationResponse = callToSBFCLMSClientCreation(urlConfig, sbfclmsIntegrationRequest.getClientCreationRequest(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (clientCreationResponse == null) {
                    logger.info("Getting NULL Response from SBFC-LMS Client Creation");

                    clientCreationResponse = ClientCreationResponse.builder()
                            .error(HttpStatus.NO_CONTENT.name())
                            .error_description(String.format(ErrorCode.BLANK_RESPONSE, EndPointReferrer.CLIENT_CREATION))
                            .build();
                }
            } catch (Exception e) {
                logger.debug("Exception while getting response from LMS Client Creation {}", e.getMessage());

                clientCreationResponse = ClientCreationResponse.builder()
                        .error(HttpStatus.REQUEST_TIMEOUT.name())
                        .error_description(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, EndPointReferrer.CLIENT_CREATION))
                        .build();
            }
        } else {
            logger.debug("Url configuration not Found for LMS Client Creation");

            clientCreationResponse = ClientCreationResponse.builder()
                    .error(HttpStatus.PRECONDITION_FAILED.name())
                    .error_description(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .build();
        }

        return clientCreationResponse;
    }

    private ClientCreationResponse callToSBFCLMSClientCreation(WFJobCommDomain urlConfig, ClientCreationRequest clientCreationRequest,   String institutionId) throws Exception {

        String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        requestsWithInstitutionId.setInstitutionId(institutionId);
        requestsWithInstitutionId.setClientCreationRequest(clientCreationRequest);

        return (ClientCreationResponse) TransportUtils.postJsonRequest(requestsWithInstitutionId, url, ClientCreationResponse.class);
    }

    public CreateLoanResponse createLoanApi(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        CreateLoanResponse createLoanResponse;

        WFJobCommDomain urlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                sbfclmsIntegrationRequest.getOHeader().getInstitutionId(), UrlType.CREATE_LOAN.toValue());

        if (urlConfig != null) {

            try {
                createLoanResponse = callToSBFCLMSCreateLoan(urlConfig, sbfclmsIntegrationRequest.getCreateLoanRequest(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (createLoanResponse == null) {
                    logger.info("Getting NULL Response from SBFC-LMS Create Loan");

                    createLoanResponse = CreateLoanResponse.builder()
                            .error(HttpStatus.NO_CONTENT.name())
                            .error_description(String.format(ErrorCode.BLANK_RESPONSE, EndPointReferrer.CREATE_LOAN_API))
                            .build();
                }
            } catch (Exception e) {
                logger.debug("Exception while getting response from LMS Create Loan {}", e.getMessage());

                createLoanResponse = CreateLoanResponse.builder()
                        .error(HttpStatus.REQUEST_TIMEOUT.name())
                        .error_description(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, EndPointReferrer.CREATE_LOAN_API))
                        .build();
            }
        } else {
            logger.debug("Url configuration not Found for LMS Create Loan");

            createLoanResponse = CreateLoanResponse.builder()
                    .error(HttpStatus.PRECONDITION_FAILED.name())
                    .error_description(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .build();
        }

        return createLoanResponse;
    }

    private CreateLoanResponse callToSBFCLMSCreateLoan(WFJobCommDomain urlConfig, CreateLoanRequest createLoanRequest , String institutionId) throws Exception {

        String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        requestsWithInstitutionId.setInstitutionId(institutionId);
        requestsWithInstitutionId.setCreateLoanRequest(createLoanRequest);

        return (CreateLoanResponse) TransportUtils.postJsonRequest(requestsWithInstitutionId, url, CreateLoanResponse.class);
    }

    public DisburseResponse dataPush(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        DisburseResponse disburseResponse = null;
        SBFCLMSIntegrationResponse sbfclmsIntegrationResponse;
        SBFCLMSIntegrationLog sbfclmsIntegrationLog;
        ApplicationRequest applicationRequest = null;
        String applicationStatus = null;

        try{
            SBFCLMSIntegrationResponse.SBFCLMSIntegrationResponseBuilder sbfclmsIntegrationResponseBuilder = SBFCLMSIntegrationResponse.builder();
            sbfclmsIntegrationResponseBuilder.date(new Date());
            sbfclmsIntegrationResponseBuilder.sbfclmsIntegrationRequest(sbfclmsIntegrationRequest);

            SBFCLMSIntegrationLog.SBFCLMSIntegrationLogBuilder sbfclmsIntegrationLogBuilder = SBFCLMSIntegrationLog.builder();
            sbfclmsIntegrationLogBuilder.refId(sbfclmsIntegrationRequest.getRefID());
            sbfclmsIntegrationLogBuilder.instId(sbfclmsIntegrationRequest.getOHeader().getInstitutionId());
            sbfclmsIntegrationLogBuilder.dateTime(new Date());
            sbfclmsIntegrationLogBuilder.status("Fail");

            GoNoGoCustomerApplication goNoGoCustomerApplication = lmsMongoRepository.getClientInformation(sbfclmsIntegrationRequest.getRefID(),
                    sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

            applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

            if (sbfclmsIntegrationRequest != null &&
                    sbfclmsIntegrationRequest.getRefID() != null &&
                    sbfclmsIntegrationRequest.getOHeader() != null &&
                    sbfclmsIntegrationRequest.getOHeader().getInstitutionId() != null){


                CamDetails camDetails = lmsMongoRepository.fetchCamDetailsByRefId(sbfclmsIntegrationRequest.getRefID(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (null != goNoGoCustomerApplication && null != camDetails) {

                    // lmsMasterDataMap = getLMSMasterData();

                    lmsMasterDataMap = getLmsMasterDataByInstitutionId(sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                    if (!CollectionUtils.isEmpty(lmsMasterDataMap)){

                        LoanCharges loanCharges = lmsMongoRepository.fetchLoanChargesDetailsByRefId(sbfclmsIntegrationRequest.getRefID(), sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                        if (loanCharges != null){

                            DisbursementMemo disbursementMemo = lmsMongoRepository.fetchDMDetails(sbfclmsIntegrationRequest.getRefID(), sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                            if (disbursementMemo != null) {

                                disburseResponse = sbfclmsIntegrationValidationManager.validateRquest(goNoGoCustomerApplication, lmsMasterDataMap, loanCharges, disbursementMemo, camDetails);

                                if (StringUtils.isBlank(disburseResponse.getError_description())) {

                                    sbfclmsIntegrationRequest = lmsBuilder.buildRetrieveClientRequest(goNoGoCustomerApplication, sbfclmsIntegrationRequest, lmsMasterDataMap);

                                    RetrieveClientResponse retrieveClientResponse = retrieveClientByIdentifier(sbfclmsIntegrationRequest);

                                    sbfclmsIntegrationResponseBuilder.retrieveClientResponse(retrieveClientResponse);

                                    if (retrieveClientResponse != null &&
                                            retrieveClientResponse.getClientDetails() != null &&
                                            StringUtils.isNotBlank(retrieveClientResponse.getClientDetails().getId())) {

                                        logger.info("Client already exist in SBFC-LMS");

                                        sbfclmsIntegrationLogBuilder.clientId(retrieveClientResponse.getClientDetails().getId());

                                        disburseResponse.setClientId(retrieveClientResponse.getClientDetails().getId());

                                        sbfclmsIntegrationRequest = lmsBuilder.buildCreateLoanRequest(goNoGoCustomerApplication, retrieveClientResponse.getClientDetails().getId(), sbfclmsIntegrationRequest, lmsMasterDataMap, loanCharges);

                                        disburseResponse = callCommanCode(sbfclmsIntegrationRequest, sbfclmsIntegrationResponseBuilder, goNoGoCustomerApplication, disburseResponse, sbfclmsIntegrationLogBuilder, loanCharges, disbursementMemo);
                                    } else {
                                        logger.info("Creating new client in SBFC-LMS");
                                        if (StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest()
                                                .getRequest().getApplicant().getApplicantType(), ApplicantType.INDIVIDUAL.value())) {
                                            sbfclmsIntegrationRequest = lmsBuilder.buildClientCreationRequest(goNoGoCustomerApplication,
                                                    sbfclmsIntegrationRequest, lmsMasterDataMap, camDetails, loanCharges);
                                        } else {
                                            sbfclmsIntegrationRequest = lmsBuilder.buildClientCreationRequestForEntity(goNoGoCustomerApplication,
                                                    sbfclmsIntegrationRequest, lmsMasterDataMap, camDetails, loanCharges);
                                        }

//                                    sbfclmsIntegrationRequest = lmsBuilder.buildClientCreationRequest(goNoGoCustomerApplication, sbfclmsIntegrationRequest, lmsMasterDataMap, camDetails);

                                        ClientCreationResponse clientCreationResponse = clientCreation(sbfclmsIntegrationRequest);

                                        sbfclmsIntegrationResponseBuilder.clientCreationResponse(clientCreationResponse);

                                        if (clientCreationResponse != null) {

                                            if (StringUtils.isNotBlank(clientCreationResponse.getClientId())) {

                                                sbfclmsIntegrationLogBuilder.clientId(clientCreationResponse.getClientId());

                                                disburseResponse.setClientId(clientCreationResponse.getClientId());

                                                sbfclmsIntegrationRequest = lmsBuilder.buildCreateLoanRequest(goNoGoCustomerApplication, clientCreationResponse.getClientId(), sbfclmsIntegrationRequest, lmsMasterDataMap, loanCharges);

                                                disburseResponse = callCommanCode(sbfclmsIntegrationRequest, sbfclmsIntegrationResponseBuilder, goNoGoCustomerApplication, disburseResponse, sbfclmsIntegrationLogBuilder, loanCharges, disbursementMemo);
                                            } else {
                                                logger.info("Getting Invalid Response from SBFC-LMS client-creation ");

                                                disburseResponse = DisburseResponse.builder()
                                                        .defaultUserMessage(clientCreationResponse.getDefaultUserMessage())
                                                        .developerMessage(clientCreationResponse.getDeveloperMessage())
                                                        .userMessageGlobalisationCode(clientCreationResponse.getUserMessageGlobalisationCode())
                                                        .errors(clientCreationResponse.getErrors())
                                                        .error_description(CollectionUtils.isEmpty(clientCreationResponse.getErrors()) ? "Blank Response" : clientCreationResponse.getErrors().get(0).getDeveloperMessage())
                                                        .build();

                                                sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                                                sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                                            }
                                        } else {
                                            logger.info("Getting NULL Response from SBFC-LMS client-creation ");

                                            disburseResponse = DisburseResponse.builder()
                                                    .error_description("Blank Response from SBFC-LMS client-creation")
                                                    .build();

                                            sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                                            sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                                        }
                                    }
                                } else {
                                    logger.info("SBFC-LMS mandatory fields validation error found");

                                    sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                                    sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                                }
                            } else {
                                logger.info("DMDetails not found");

                                disburseResponse = DisburseResponse.builder()
                                        .error_description("DMDetails not found")
                                        .build();

                                sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                                sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                            }
                        } else {
                            logger.info("Loan Charges Data not found");

                            disburseResponse = DisburseResponse.builder()
                                    .error_description("Loan Charges Data not found")
                                    .build();

                            sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                            sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                        }
                    } else {
                        logger.info("lmsMasterData not found");

                        disburseResponse = DisburseResponse.builder()
                                .error_description("LMS Master Data not found")
                                .build();

                        sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                        sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                    }
                } else {
                    logger.info("goNoGoCustomerApplication not found");

                    disburseResponse = DisburseResponse.builder()
                            .error_description("GoNoGoCustomerApplication not found")
                            .build();

                    sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                    sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                }
            } else {
                logger.debug("Getting Blank request for SBFC-LMS Data Push");

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Request")
                        .build();

                sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
            }

            if (StringUtils.isBlank(disburseResponse.getError_description())){

                if (StringUtils.isNotBlank(disburseResponse.getLoanId()) &&
                        StringUtils.isNotBlank(disburseResponse.getClientId()) &&
                        disburseResponse.getErrors() == null){

                    disburseResponse.setError_description("Data Push Successful. Client Id = " + disburseResponse.getClientId()
                            + " and Loan Id = " +disburseResponse.getLoanId());

                    sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                    sbfclmsIntegrationLogBuilder.loanId(disburseResponse.getLoanId());
                    sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                    sbfclmsIntegrationLogBuilder.status("Disburse");

                    applicationRequest.setCurrentStageId(GNGWorkflowConstant.DISB.toFaceValue());

                    applicationStatus = GNGWorkflowConstant.DISBURSED.toFaceValue();

                    applicationRepository.updateGoNoGoCustomerApplication(applicationRequest);

                    dataEntryManager.updateStageId(applicationRequest, applicationStatus);
                } else {

                    disburseResponse.setError_description(disburseResponse.getErrors().get(0).getDeveloperMessage());

                    sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                    sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
                }
            }

            sbfclmsIntegrationResponse = sbfclmsIntegrationResponseBuilder.build();
            lmsMongoRepository.saveLog(sbfclmsIntegrationResponse);

            sbfclmsIntegrationLog = sbfclmsIntegrationLogBuilder.build();
            lmsMongoRepository.saveLog(sbfclmsIntegrationLog);

        }catch(Exception e){
            logger.error("{} error occured at the time of pushing LMS data {}", applicationRequest.getRefID(), ExceptionUtils.getStackTrace(e));

        }
        return disburseResponse;
    }


    private DisburseResponse callCommanCode(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, SBFCLMSIntegrationResponse.SBFCLMSIntegrationResponseBuilder sbfclmsIntegrationResponseBuilder, GoNoGoCustomerApplication goNoGoCustomerApplication, DisburseResponse disburseResponse, SBFCLMSIntegrationLog.SBFCLMSIntegrationLogBuilder sbfclmsIntegrationLogBuilder, LoanCharges loanCharges, DisbursementMemo disbursementMemo) {

        CreateLoanResponse createLoanResponse = createLoanApi(sbfclmsIntegrationRequest);

        sbfclmsIntegrationResponseBuilder.createLoanResponse(createLoanResponse);

        if (createLoanResponse != null){

            if (createLoanResponse.getLoanId() != null &&
                    createLoanResponse.getErrors() == null){

                logger.info("Loan created in SBFC-LMS");

                disburseResponse.setLoanId(createLoanResponse.getLoanId());

                sbfclmsIntegrationLogBuilder.loanId(createLoanResponse.getLoanId());
                sbfclmsIntegrationLogBuilder.status("Submit");

            } else {

                logger.info("Getting Invalid Response from SBFC-LMS create loan");

                disburseResponse = DisburseResponse.builder()
                        .defaultUserMessage(createLoanResponse.getDefaultUserMessage())
                        .developerMessage(createLoanResponse.getDeveloperMessage())
                        .userMessageGlobalisationCode(createLoanResponse.getUserMessageGlobalisationCode())
                        .errors(createLoanResponse.getErrors())
                        .error_description(CollectionUtils.isEmpty(createLoanResponse.getErrors()) ? "Blank Response" : createLoanResponse.getErrors().get(0).getDeveloperMessage())
                        .build();

                sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

                sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
            }
        } else {

            logger.info("Getting NULL Response from SBFC-LMS create loan ");

            disburseResponse = DisburseResponse.builder()
                    .error_description("Blank Response from SBFC-LMS create loan")
                    .build();

            sbfclmsIntegrationResponseBuilder.disburseResponse(disburseResponse);

            sbfclmsIntegrationLogBuilder.responseMsg(disburseResponse.getError_description());
        }

        return disburseResponse;
    }

    private Map getLmsMasterDataByInstitutionId(String institutionId){
        List lmsMasterDataList = lmsMongoRepository.getMasterDataByInstitutionId(institutionId);
        if (!CollectionUtils.isEmpty(lmsMasterDataList)) {
            for (Object lmsMasterData : lmsMasterDataList) {

                if (lmsMasterData instanceof BasicDBObject) {

                    BasicDBObject basicDBObject = (BasicDBObject) lmsMasterData;

                    Set<String> values = basicDBObject.keySet();

                    for (String value : values) {

                        lmsMasterDataMapList.put(value, String.valueOf(basicDBObject.get(value)));
                    }
                    break;
                }
            }
        }

        return lmsMasterDataMapList;
    }


    private Map getLMSMasterData() {

        List lmsMasterDataList = lmsMongoRepository.getMasterData();

        if (!CollectionUtils.isEmpty(lmsMasterDataList)) {

            for (Object lmsMasterData : lmsMasterDataList) {

                if (lmsMasterData instanceof BasicDBObject) {

                    BasicDBObject basicDBObject = (BasicDBObject) lmsMasterData;

                    Set<String> values = basicDBObject.keySet();

                    for (String value : values) {

                        lmsMasterDataMap.put(value, String.valueOf(basicDBObject.get(value)));
                    }
                    break;
                }
            }
        }

        return lmsMasterDataMap;
    }

    public String addMasterData(String requestJson) {

        String response = null;

        if (StringUtils.isNotBlank(requestJson)) {

            lmsMongoRepository.saveMasterData(requestJson, "SBFCLMSMasterData");

        } else {
            logger.debug("Getting null request for Inserting SBFC-LMS Master Data");
        }

        return response;
    }

    public BaseResponse getMasterData() {

        return getBaseResponse(HttpStatus.OK, lmsMongoRepository.getMasterData());
    }

    public BaseResponse getMasterDataByInstitutionId(String institutionId){
        return getBaseResponse(HttpStatus.OK, lmsMongoRepository.getMasterDataByInstitutionId(institutionId));
    }

    private BaseResponse getBaseResponse(HttpStatus httpStatus, Object buzResponse) {
        if (null == buzResponse)
            buzResponse = Collections.emptyMap();

        return BaseResponse.builder()
                .payload(new Payload<>(buzResponse))
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .build();
    }

    public void deleteMasterData() {

        lmsMongoRepository.deleteMasterData();
    }

    public ApproveResponse approve(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        ApproveResponse approveResponse;

        WFJobCommDomain urlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                sbfclmsIntegrationRequest.getOHeader().getInstitutionId(), UrlType.APPROVE.toValue());

        if (urlConfig != null) {
            try {
                approveResponse = callToSBFCLMSApprove(urlConfig, sbfclmsIntegrationRequest.getApproveCoreRequest(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (approveResponse == null) {
                    logger.info("Getting NULL Response from SBFC-LMS Approve");

                    approveResponse = ApproveResponse.builder()
                            .error(HttpStatus.NO_CONTENT.name())
                            .error_description(String.format(ErrorCode.BLANK_RESPONSE, EndPointReferrer.APPROVE))
                            .build();
                }
            } catch (Exception e) {
                logger.debug("Exception while getting response from LMS Approve {}", e.getMessage());

                approveResponse = ApproveResponse.builder()
                        .error(HttpStatus.REQUEST_TIMEOUT.name())
                        .error_description(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, EndPointReferrer.APPROVE))
                        .build();
            }
        } else {
            logger.debug("Url configuration not Found for LMS Approve");

            approveResponse = ApproveResponse.builder()
                    .error(HttpStatus.PRECONDITION_FAILED.name())
                    .error_description(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .build();
        }

        return approveResponse;
    }

    private ApproveResponse callToSBFCLMSApprove(WFJobCommDomain urlConfig, ApproveCoreRequest approveCoreRequest, String institutionId) throws Exception {

        String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        requestsWithInstitutionId.setInstitutionId(institutionId);
        requestsWithInstitutionId.setApproveCoreRequest(approveCoreRequest);

        return (ApproveResponse) TransportUtils.postJsonRequest(requestsWithInstitutionId, url, ApproveResponse.class);
    }

    public DisburseResponse disburse(SBFCLMSIntegrationRequest sbfclmsIntegrationRequest) {

        DisburseResponse disburseResponse;

        WFJobCommDomain urlConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                sbfclmsIntegrationRequest.getOHeader().getInstitutionId(), UrlType.DISBURSE.toValue());

        if (urlConfig != null) {
            try {
                disburseResponse = callToSBFCLMSDisburse(urlConfig, sbfclmsIntegrationRequest.getDisburseCoreRequest(),
                        sbfclmsIntegrationRequest.getOHeader().getInstitutionId());

                if (disburseResponse == null) {
                    logger.info("Getting NULL Response from SBFC-LMS Disburse");

                    disburseResponse = DisburseResponse.builder()
                            .error(HttpStatus.NO_CONTENT.name())
                            .error_description(String.format(ErrorCode.BLANK_RESPONSE, EndPointReferrer.DISBURSE))
                            .build();
                }
            } catch (Exception e) {
                logger.debug("Exception while getting response from LMS Disburse {}", e.getMessage());

                disburseResponse = DisburseResponse.builder()
                        .error(HttpStatus.REQUEST_TIMEOUT.name())
                        .error_description(String.format(ErrorCode.UNABLE_TO_GET_RESPONSE, EndPointReferrer.DISBURSE))
                        .build();
            }
        } else {
            logger.debug("Url configuration not Found for LMS Disburse");

            disburseResponse = DisburseResponse.builder()
                    .error(HttpStatus.PRECONDITION_FAILED.name())
                    .error_description(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .build();
        }

        return disburseResponse;
    }

    private DisburseResponse callToSBFCLMSDisburse(WFJobCommDomain urlConfig, DisburseCoreRequest disburseCoreRequest, String institutionId) throws Exception {

        String url = Arrays.asList(urlConfig.getBaseUrl(), urlConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

        requestsWithInstitutionId.setInstitutionId(institutionId);
        requestsWithInstitutionId.setDisburseCoreRequest(disburseCoreRequest);

        return (DisburseResponse) TransportUtils.postJsonRequest(requestsWithInstitutionId, url, DisburseResponse.class);
    }
}