package com.softcell.service.impl;


import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.service.SerialNumberManufacturerManager;
import com.softcell.service.SerialNumberValidationManager;
import com.softcell.service.serialnumbervalidation.*;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author yogeshb
 */
@Service
public class SerialNumberManufacturerManagerImpl implements SerialNumberManufacturerManager {

    private static final Logger logger = LoggerFactory.getLogger(SerialNumberManufacturerManagerImpl.class);

    @Autowired
    private SerialNumberValidationManager serialNumberValidationManager;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private YuhoSerialNumberValidationManager yuhoSerialNumberValidation;

    @Autowired
    private ComioSerialNumberValidationManager comioSerialNumberValidationManager;

    @Autowired
    private NokiaSerialNumberValidationManager nokiaSerialNumberValidationManager;

    @Autowired
    private OnidaSerialNumberValidation onidaSerialNumberValidation;

    @Autowired
    private VoltasServiceCodeRegistrationManager voltasServiceCodeRegistrationManager;

    @Autowired
    private LgSerialNumberValidationManager lgSerialNumberValidationManager;

    @Autowired
    private LavaSerialNumberValidationManager lavaSerialNumberValidationManager;

    @Autowired
    private CarrierSerialNumberValidationManager carrierSerialNumberValidationManager;

    @Override
    public BaseResponse getVendorResponse(
            SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception {

        BaseResponse baseResponse;
        SerialNumberResponse serialNumberResponse;

        SerialNumberResponse dedupRes = checkDedup(serialSaleConfirmationRequest);

        /**
         * Saving request .
         */
        externalAPILogRepository.saveSerialNumberRequest(serialSaleConfirmationRequest);

        if (StringUtils.equalsIgnoreCase(Status.INVALID.name(), dedupRes.getStatus())) {
            logger.info("Serial Number or Imei Number validation request in dedup.");
            return GngUtils.getBaseResponse(HttpStatus.OK, dedupRes);
        }


        if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.SAMSUNG.toFaceValue())) {

            baseResponse = serialNumberValidationManager
                    .serialSaleConfirmation(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.LG.toFaceValue())) {

            baseResponse = lgSerialNumberValidationManager
                    .validateLg(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.SONY.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateSony(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.INTEX.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateIntex(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.APPLE.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateApple(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.PANASONIC.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validatePanasonic(serialSaleConfirmationRequest);

        } else if (serialSaleConfirmationRequest.getVendor().equalsIgnoreCase(
                GNGWorkflowConstant.GIONEE.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateGioneeImei(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(),
                Vendor.OPPO.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateOppoImei(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(),
                        Vendor.VIDEOCON.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(),
                        Vendor.KENTSTAR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(),
                        Vendor.KELVINATOR.toFaceValue()) ||
                StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(),
                        Vendor.SANSUI.toFaceValue())) {

          baseResponse = serialNumberValidationManager.validateVideoconSansuiKelvinatorKentstar(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.KENT.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateKent(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.GOOGLE.toFaceValue())) {

            baseResponse = serialNumberValidationManager.validateGoogle(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.YUHO.toFaceValue())) {

            baseResponse = yuhoSerialNumberValidation.validateYuho(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.COMIO.toFaceValue())) {

            baseResponse = comioSerialNumberValidationManager.validateComioImei(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.NOKIA.toFaceValue())) {

            baseResponse = nokiaSerialNumberValidationManager.validateNokia(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.ONIDA.toFaceValue())) {

            baseResponse = onidaSerialNumberValidation.validateOnidaSerialNumber(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.VOLTAS.toFaceValue())) {

            baseResponse = voltasServiceCodeRegistrationManager.validateVoltas(serialSaleConfirmationRequest);

        }else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.LAVA.toFaceValue())) {

            baseResponse = lavaSerialNumberValidationManager.validateLava(serialSaleConfirmationRequest);

        } else if (StringUtils.equalsIgnoreCase(serialSaleConfirmationRequest.getVendor(), Vendor.CARRIER.toFaceValue())) {

            baseResponse = carrierSerialNumberValidationManager.validateCarrierSerialNumber(serialSaleConfirmationRequest);

        } else {
            serialNumberResponse = new SerialNumberResponse();
            serialNumberResponse.setMessage(ErrorCode.INVALID_VENDOR);
            serialNumberResponse.setStatus(Status.FAILED.name());

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.INVALID_VENDOR)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);

        }

        return baseResponse;
    }

    private SerialNumberResponse checkDedup(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        if (externalAPILogRepository
                .checkSerialNumberInDedupe(serialSaleConfirmationRequest)) {

            SerialNumberResponse serialNumberResponse = new SerialNumberResponse();
            serialNumberResponse.setStatus(Status.INVALID.name());
            serialNumberResponse
                    .setMessage(ErrorCode.SERIAL_NUMBER_DEDUP);
            serialNumberResponse.setVendor(serialSaleConfirmationRequest
                    .getVendor());
            return serialNumberResponse;
        } else {
            SerialNumberResponse serialNumberResponse = new SerialNumberResponse();
            serialNumberResponse.setStatus(Status.PASS.name());
            serialNumberResponse
                    .setMessage(ErrorCode.SERIAL_NUMBER_DEDUP);
            serialNumberResponse.setVendor(serialSaleConfirmationRequest
                    .getVendor());
            return serialNumberResponse;
        }
    }

}
