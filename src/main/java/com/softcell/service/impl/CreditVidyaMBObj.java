package com.softcell.service.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.multibureau.pickup.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 22/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditVidyaMBObj {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private Long acknowledgmentId;

    @JsonProperty("FINISHED")
    private List<Finished> finishedList;

}

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
 class Finished {

    @JsonProperty("TRACKING-ID")
    private Long trackingId;

    @JsonProperty("BUREAU")
    private String bureau;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("JSON-RESPONSE-OBJECT")
    private Object responseJsonObject;


}
