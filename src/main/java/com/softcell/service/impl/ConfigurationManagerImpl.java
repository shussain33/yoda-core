package com.softcell.service.impl;


import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.CacheName;
import com.softcell.constants.Constant;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.lookup.LookupDao;
import com.softcell.dao.mongodb.repository.queue.management.QueueRepository;
import com.softcell.dao.mongodb.repository.security.SecurityRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.cache.UpdatedCache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.MasterMappingDetails;
import com.softcell.gonogo.model.MasterSchedulerActivityLogs;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.los.LosChargeConfig;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.response.core.Status;
import com.softcell.gonogo.model.security.VersionRequest;
import com.softcell.gonogo.model.security.Versions;
import com.softcell.gonogo.queue.management.QueueEventHandler;
import com.softcell.gonogo.queue.management.QueueUnassignedCases;
import com.softcell.service.ConfigurationManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * @author yogeshb
 */
@Service
public class ConfigurationManagerImpl implements ConfigurationManager {

    private final Logger logger = LoggerFactory.getLogger(ConfigurationManagerImpl.class);

    static List<String> queueGroupIds = new ArrayList<>();

    @Autowired
    private SecurityRepository securityRepository;

    @Autowired
    private ConfigurationRepository configurationRepository;


    @Autowired
    private UpdatedCache updatedCache;

    @Autowired
    private LookupDao lookupDao;

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private QueueEventHandler queueHandler;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;


    /**
     * On server startup creating QueueGroups of unassigned application in Redis cache
     * @param event
     */
    /*@org.springframework.context.event.EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        createQueueGroupConfiguration();
    }*/


    @Override
    public GenericResponse setVersion(VersionRequest versionRequest) {
        GenericResponse genericResponse = new GenericResponse();

        if (StringUtils.isNotBlank(versionRequest.getVersion())) {
            if (securityRepository.addAllowedVersion(versionRequest.getHeader()
                    .getInstitutionId(), versionRequest.getVersion())) {
                genericResponse.setStatus(Constant.SUCCESS);
                return genericResponse;
            } else {
                genericResponse.setStatus(Constant.FAILED);
                return genericResponse;
            }
        } else {
            genericResponse.setStatus(Constant.FAILED);
            return genericResponse;
        }

    }

    @Override
    public GenericResponse removeVersion(VersionRequest versionRequest) {

        GenericResponse genericResponse = new GenericResponse();

        if (StringUtils.isNotBlank(versionRequest.getVersion())) {
            if (securityRepository.removeOldVersion(versionRequest.getHeader()
                    .getInstitutionId(), versionRequest.getVersion())) {

                genericResponse.setStatus(Constant.SUCCESS);
                return genericResponse;
            } else {

                genericResponse.setStatus(Constant.FAILED);
                return genericResponse;
            }
        } else {

            genericResponse.setStatus(Constant.FAILED);
            return genericResponse;
        }
    }

    @Override
    public List<Versions> getVersionConguration() {
        return securityRepository.showAllVersions();
    }

    @Override
    public boolean isValidVersion(String institutionId, String version) {
        String lookupKey = StringUtils.join(CacheName.ACTION.name(), institutionId, version);
        if (!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            boolean result = securityRepository.isValidVersion(institutionId, version);
            updatedCache.CACHE.put(lookupKey, result);
            return result;
        } else {
            logger.debug("Cache hit occur for Cache Name [{}]: for lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            return (boolean) updatedCache.CACHE.get(lookupKey);
        }
    }

    @Override
    public BaseResponse addInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration) {

        String lookupKey = StringUtils.join(CacheName.ALIAS_NAME.name(),
                institutionProductConfiguration.getInstitutionID(),
                institutionProductConfiguration.getProductName());

        BaseResponse baseResponse;

        if (configurationRepository.addInstitutionProductConfiguration(institutionProductConfiguration)) {

            List<InstitutionProductConfiguration> institutionProductConfigurations = lookupDao.aliasNameByProductNameAndInstitutionIdReferrer(
                    institutionProductConfiguration.getInstitutionID(),
                    institutionProductConfiguration.getProductID());

            /* update cache */
            updatedCache.CACHE.put(lookupKey, institutionProductConfigurations);

            logger.debug("Cache updated against key[{}].", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, institutionProductConfiguration);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, institutionProductConfiguration);

        }

        return baseResponse;
    }


    @Override
    public BaseResponse removeInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.ALIAS_NAME.name(),
                institutionProductConfiguration.getInstitutionID(),
                institutionProductConfiguration.getProductName());

        if (configurationRepository
                .removeInstitutionProductConfiguration(institutionProductConfiguration)) {
            /* update cache */
            updatedCache.CACHE.remove(lookupKey);

            logger.debug("Cache deleted against key[{}].", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, institutionProductConfiguration);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, institutionProductConfiguration);

        }

        return baseResponse;
    }

    @Override
    public BaseResponse updateInstitutionProductConfig(InstitutionProductConfiguration institutionProductConfiguration) {

        String lookupKey = StringUtils.join(CacheName.ALIAS_NAME.name(),
                institutionProductConfiguration.getInstitutionID(),
                institutionProductConfiguration.getProductName());

        BaseResponse baseResponse;

        try {

            InstitutionProductConfiguration institutionProductConfig = configurationRepository.updateInstitutionProductConfiguration(institutionProductConfiguration);

            if (null != institutionProductConfig) {

                List<InstitutionProductConfiguration> institutionProductConfigurations = (List<InstitutionProductConfiguration>) updatedCache.CACHE.get(lookupKey);

                institutionProductConfigurations.add(institutionProductConfig);

                /** cache update */
                updatedCache.CACHE.put(lookupKey, institutionProductConfigurations);

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, institutionProductConfig);

            } else {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, institutionProductConfig);
            }

            return baseResponse;

        } catch (SystemException e) {

            Object available_products = e.get("AVAILABLE_PRODUCTS");

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(String.format(ErrorCode.UNREGISTERED_PRODUCT, available_products))
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.UNPROCESSABLE_ENTITY, errors);
        }


        return baseResponse;

    }

    @Override
    public List<InstitutionProductConfiguration> getInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration) {

        if (StringUtils.isNotBlank(institutionProductConfiguration.getInstitutionID())) {

            return configurationRepository.findAllInstitutionProductConfig(institutionProductConfiguration);

        } else {

            List<InstitutionProductConfiguration> institutionProductConfigurations = new ArrayList<InstitutionProductConfiguration>();

            return institutionProductConfigurations;
        }
    }

    @Override
    public BaseResponse addTemplateConfiguration(
            TemplateConfiguration templateConfiguration) {

        String lookupKey = StringUtils.join(CacheName.TEMPLATE.name(), templateConfiguration.getInstitutionId(), templateConfiguration.getProductId());

        BaseResponse baseResponse;
        templateConfiguration.setCreateDate(new Date());
        templateConfiguration.setLastUpdateDate(new Date());
        if (configurationRepository.saveTemplateConfiguration(templateConfiguration)) {

            /** cache update */
            updatedCache.CACHE.put(lookupKey, templateConfiguration);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, templateConfiguration);
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, templateConfiguration);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getTemplateConfiguration(
            TemplateConfiguration templateConfiguration) {

        BaseResponse baseResponse = new BaseResponse();

        if (null != templateConfiguration
                && StringUtils.isNotBlank(templateConfiguration
                .getInstitutionId())
                && StringUtils.isNotBlank(templateConfiguration
                .getProductId())) {

            List<TemplateConfiguration> templateConfigurations = configurationRepository
                    .getTemplateConfiguration(templateConfiguration);
            if (null != templateConfigurations
                    && templateConfigurations.size() > 0) {
                Status status = new Status();
                status.setStatusCode(HttpStatus.OK.value());
                status.setStatusValue(HttpStatus.OK.name());
                baseResponse.setStatus(status);
                Payload<List<TemplateConfiguration>> payload = new Payload(templateConfigurations);
                baseResponse
                        .setPayload(payload);
            } else {
                Status status = new Status();
                status.setStatusCode(HttpStatus.NO_CONTENT.value());
                status.setStatusValue(HttpStatus.NO_CONTENT.name());
                baseResponse.setStatus(status);
            }
        } else {
            Status status = new Status();
            status.setStatusCode(HttpStatus.BAD_REQUEST.value());
            status.setStatusValue(HttpStatus.BAD_REQUEST.name());
            baseResponse.setStatus(status);
        }

        return baseResponse;
    }

    @Override
    public TemplateConfiguration getTemplateConfiguration(String institutionId,
                                                          String productId, String templateName) {
        TemplateConfiguration templateConfiguration = null;
        if (StringUtils.isNotBlank(institutionId)
                && StringUtils.isNotBlank(productId)
                && StringUtils.isNotBlank(templateName)) {
            templateConfiguration = configurationRepository.getTemplateConfiguration(institutionId, productId, templateName);
        }
        return templateConfiguration;
    }

    @Override
    public BaseResponse updateTemplatePath(
            TemplateConfiguration templateConfiguration) {
        BaseResponse baseResponse;
        String lookupKey = StringUtils.join(CacheName.TEMPLATE.name(), templateConfiguration.getInstitutionId(), templateConfiguration.getProductId());

        if (StringUtils.isNotBlank(templateConfiguration.getTemplatePath()) && configurationRepository
                .updateTemplatePath(templateConfiguration)) {

            List<TemplateConfiguration> templateConfigurations = lookupDao.templateConfigurationReferrer(templateConfiguration.getInstitutionId(),
                    templateConfiguration.getProductId(), templateConfiguration.getTemplateName());

            updatedCache.CACHE.put(lookupKey, templateConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, templateConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, templateConfiguration);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse updateTemplateLogo(
            TemplateConfiguration templateConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.TEMPLATE.name(), templateConfiguration.getInstitutionId(), templateConfiguration.getProductId());

        if (null != templateConfiguration
                && StringUtils.isNotBlank(templateConfiguration.getLogoId())
                && configurationRepository.updateTemplateLogo(templateConfiguration)) {

            List<TemplateConfiguration> templateConfigurations = lookupDao.templateConfigurationReferrer(templateConfiguration.getInstitutionId(),
                    templateConfiguration.getProductId(), templateConfiguration.getTemplateName());

            updatedCache.CACHE.put(lookupKey, templateConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, templateConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, templateConfiguration);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse enableTemplate(
            TemplateConfiguration templateConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.TEMPLATE.name(), templateConfiguration.getInstitutionId(), templateConfiguration.getProductId());

        if (configurationRepository.enableTemplate(templateConfiguration)) {

            List<TemplateConfiguration> templateConfigurations = lookupDao.templateConfigurationReferrer(templateConfiguration.getInstitutionId(),
                    templateConfiguration.getProductId(), templateConfiguration.getTemplateName());

            updatedCache.CACHE.put(lookupKey, templateConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, templateConfigurations.get(0));

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, templateConfiguration);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse disableTemplate(
            TemplateConfiguration templateConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.TEMPLATE.name(), templateConfiguration.getInstitutionId(), templateConfiguration.getProductId());

        if (configurationRepository.disableTemplate(templateConfiguration)) {

            List<TemplateConfiguration> templateConfigurations = lookupDao.templateConfigurationReferrer(templateConfiguration.getInstitutionId(),
                    templateConfiguration.getProductId(), templateConfiguration.getTemplateName());

            updatedCache.CACHE.put(lookupKey, templateConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, templateConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, templateConfiguration);

        }
        return baseResponse;
    }

    @Override
    public BaseResponse addEmailConfiguration(EmailConfiguration emailConfiguration) {
        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());
        BaseResponse baseResponse;
        emailConfiguration.setProductId(emailConfiguration.getProductName().toProductId());
        emailConfiguration.setCreateDate(new Date());
        emailConfiguration.setLastUpdateDate(new Date());
        if (configurationRepository.saveEmailConfiguration(emailConfiguration)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse updateEmailConfiguration(EmailConfiguration emailConfiguration) {

        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());

        BaseResponse baseResponse;

        if (configurationRepository.updateEmailConfiguration(emailConfiguration)) {
            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfiguration);

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse disableEmailConfiguration(EmailConfiguration emailConfiguration) {
        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());

        if (null != emailConfiguration
                && StringUtils.isNotBlank(emailConfiguration.getInstitutionId())
                && StringUtils.isNotBlank(emailConfiguration.getProductId())) {

            emailConfiguration.setLastUpdateDate(new Date());

            if (configurationRepository.disableEmailConfiguration(emailConfiguration)) {

                 /* Store data in cache */
                updatedCache.CACHE.remove(lookupKey);

                logger.debug("Cache updated for key[{}]", lookupKey);

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfiguration);
            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse enableEmailConfiguration(EmailConfiguration emailConfiguration) {
        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());

        BaseResponse baseResponse;

        if (null != emailConfiguration
                && StringUtils.isNotBlank(emailConfiguration.getInstitutionId())
                && StringUtils.isNotBlank(emailConfiguration.getProductId())) {

            emailConfiguration.setLastUpdateDate(new Date());

            if (configurationRepository.enableEmailConfiguration(emailConfiguration)) {

                /* Store data in cache */
                updatedCache.CACHE.put(lookupKey, lookupDao.emailConfigurationReferrer(
                        emailConfiguration.getInstitutionId(),
                        emailConfiguration.getProductName().toProductId()));

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfiguration);

            } else {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);
            }
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);

        }
        return baseResponse;
    }

    @Override
    public BaseResponse getAllEmailConfiguration(EmailConfiguration emailConfiguration) {
        BaseResponse baseResponse = new BaseResponse();
        List<EmailConfiguration> emailConfigurations = configurationRepository
                .getEmailConfiguration(emailConfiguration);
        if (null != emailConfigurations && emailConfigurations.size() > 0) {
            Status status = new Status();
            status.setStatusCode(HttpStatus.OK.value());
            status.setStatusValue(HttpStatus.OK.name());
            baseResponse.setStatus(status);
            baseResponse.setPayload(new Payload(emailConfigurations));
        } else {
            Status status = new Status();
            status.setStatusCode(HttpStatus.NO_CONTENT.value());
            status.setStatusValue(HttpStatus.NO_CONTENT.name());
            baseResponse.setStatus(status);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse addAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration) {
        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());
        BaseResponse baseResponse;
        emailConfiguration.setLastUpdateDate(new Date());
        if (null != emailConfiguration.getAttachments()
                && emailConfiguration.getAttachments().size() > 0
                && configurationRepository.addAttachmentInEmailConfiguration(emailConfiguration)) {

            List<EmailConfiguration> emailConfigurations = lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, emailConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfigurations.get(0));

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);

        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration) {
        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());
        BaseResponse baseResponse;
        emailConfiguration.setLastUpdateDate(new Date());
        if (null != emailConfiguration.getAttachments()
                && emailConfiguration.getAttachments().size() > 0
                && configurationRepository.removeAttachmentFromEmailConfiguration(emailConfiguration)) {

            List<EmailConfiguration> emailConfigurations = lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, emailConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);

        }

        return baseResponse;
    }

    @Override
    public BaseResponse addValidationVendors(ValidationVendorsConfiguration validationVendorsConfiguration) {
        validationVendorsConfiguration.setEnable(true);
        validationVendorsConfiguration.setProductId(validationVendorsConfiguration.getProductName().toProductId());
        ValidationVendorsConfiguration vendorsConfiguration = saveValidationVendorsConfigurationInCacheAndDB(validationVendorsConfiguration);
        BaseResponse baseResponse;

        if (null != vendorsConfiguration) {

            String lookupKey = StringUtils.join(CacheName.APPLICABLE_VENDOR.name(), vendorsConfiguration.getInstitutionId(), vendorsConfiguration.getProductId());

            Set<String> vendors = (Set<String>) updatedCache.CACHE.get(lookupKey);

            if (!CollectionUtils.isEmpty(vendors)) {

                if (vendors.contains(vendorsConfiguration.getVendor().toFaceValue())) {

                    logger.debug("value already in cache.");

                } else {
                    vendors.add(vendorsConfiguration.getVendor().toFaceValue());

                    updatedCache.CACHE.putIfAbsent(lookupKey, vendors);

                    logger.debug("Cache update against key[{}] with value[{}]", lookupKey, vendorsConfiguration.getVendor().toFaceValue());

                }
            } else {
                vendors = new HashSet<>();

                vendors.add(vendorsConfiguration.getVendor().toFaceValue());

                updatedCache.CACHE.putIfAbsent(lookupKey, vendors);

                logger.debug("Cache update against key[{}] with value[{}]", lookupKey, vendorsConfiguration.getVendor().toFaceValue());
            }
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, vendorsConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getValidationVendors(String institutionId) {
        List<ValidationVendorsConfiguration> validationVendorsConfigurations =
                configurationRepository.getApplicableVendorConfiguration(institutionId);
        BaseResponse baseResponse;
        if (null != validationVendorsConfigurations && !validationVendorsConfigurations.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, validationVendorsConfigurations);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteValidationVendors(ValidationVendorsConfiguration validationVendorsConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.APPLICABLE_VENDOR.name(), validationVendorsConfiguration.getInstitutionId(), validationVendorsConfiguration.getProductName().toProductId());

        if (configurationRepository.deleteValidationVendorConfiguration(validationVendorsConfiguration)) {

            Set<String> vendors = (Set<String>) updatedCache.CACHE.get(lookupKey);

            if (!CollectionUtils.isEmpty(vendors)) {

                if (vendors.contains(validationVendorsConfiguration.getVendor().toFaceValue())) {

                    vendors.remove(validationVendorsConfiguration.getVendor().toFaceValue());

                    updatedCache.CACHE.putIfAbsent(lookupKey, vendors);

                    logger.debug("Cache update against key[{}] with value[{}]", lookupKey, validationVendorsConfiguration.getVendor().toFaceValue());

                } else {

                    logger.debug("value not in cache.");
                }
            }

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, validationVendorsConfiguration);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, validationVendorsConfiguration);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getCacheData() {
        BaseResponse baseResponse;
        if (!updatedCache.CACHE.isEmpty()) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, updatedCache.CACHE);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;
    }

    @Override
    public BaseResponse dropCache() {
        BaseResponse baseResponse;
        if (!updatedCache.CACHE.isEmpty()) {

            updatedCache.CACHE.clear();

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,"Cache drop successfully.");
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, "Cache already Empty.");

        }
        return baseResponse;
    }

    @Override
    public BaseResponse dropCache(String key) {

        BaseResponse baseResponse;

        if (updatedCache.CACHE.containsKey(key)) {

            updatedCache.CACHE.remove(key);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK,String.format("Cache drop for key [%S] successfully.",key));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, String.format("Cache already empty against [%s].",key));

        }
        return baseResponse;
    }

    @Override
    public BaseResponse addSecondaryAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration) {

        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());

        BaseResponse baseResponse;

        if (null != emailConfiguration.getSecondaryAttachments()
                && emailConfiguration.getSecondaryAttachments().size() > 0
                && configurationRepository.addSecondaryAttachmentInEmailConfiguration(emailConfiguration)) {

            List<EmailConfiguration> emailConfigurations = lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, emailConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfigurations.get(0));

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);

        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteSecondaryAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.EMAIL.name(),
                emailConfiguration.getInstitutionId(),
                emailConfiguration.getProductName().toProductId());

        if (null != emailConfiguration.getSecondaryAttachments()
                && emailConfiguration.getSecondaryAttachments().size() > 0
                && configurationRepository.removeSecondaryAttachmentFromEmailConfiguration(emailConfiguration)) {

            List<EmailConfiguration> emailConfigurations = lookupDao.emailConfigurationReferrer(
                    emailConfiguration.getInstitutionId(),
                    emailConfiguration.getProductName().toProductId());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, emailConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, emailConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, emailConfiguration);

        }

        return baseResponse;
    }

    @CachePut(key = "#p0.institutionId+p0.productId")
    public ValidationVendorsConfiguration saveValidationVendorsConfigurationInCacheAndDB(ValidationVendorsConfiguration validationVendorsConfiguration) {
        return configurationRepository.saveAllowedVendors(validationVendorsConfiguration);
    }


    @Override
    public BaseResponse addLoyaltyCardConfig(LoyaltyCardConfiguration loyaltyCardConfiguration) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.LOYALTY_CARD.name(),
                loyaltyCardConfiguration.getInstitutionId(),
                loyaltyCardConfiguration.getProductId());

        loyaltyCardConfiguration.setCreateDate(new Date());
        loyaltyCardConfiguration.setLastUpdateDate(new Date());
        if (configurationRepository.saveLoyaltyCardConfiguration(loyaltyCardConfiguration)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.loyaltyCardConfigurationReferral(
                    loyaltyCardConfiguration.getInstitutionId(),
                    loyaltyCardConfiguration.getProductId(),loyaltyCardConfiguration.getLoyaltyCardType()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loyaltyCardConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, loyaltyCardConfiguration);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse findLoyaltyCardConfig(String institutionId ,String productId) {

        BaseResponse baseResponse;

        List<LoyaltyCardConfiguration> loyaltyCardConfigurations = configurationRepository
                .getLoyaltyCardConfiguration(institutionId ,productId);

        if (!CollectionUtils.isEmpty(loyaltyCardConfigurations)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loyaltyCardConfigurations);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse deleteLoyaltyCardConfig(String institutionId, String productId ,LoyaltyCardType loyaltyCardType) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.LOYALTY_CARD.name(), loyaltyCardType.toFaceValue(), institutionId, productId);

        if (configurationRepository.removeLoyaltyCardConfiguration(institutionId, productId ,loyaltyCardType)) {

            updatedCache.CACHE.remove(lookupKey);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }
        return baseResponse;
    }


    @Override
    public BaseResponse updateLoyaltyCardConfig(LoyaltyCardConfiguration loyaltyCardConfiguration) {
        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.LOYALTY_CARD.name(),loyaltyCardConfiguration.getLoyaltyCardType().toFaceValue(), loyaltyCardConfiguration.getInstitutionId(),
                loyaltyCardConfiguration.getProductId());

        if (configurationRepository.updateLoyaltyCardConfiguration(loyaltyCardConfiguration)) {

            List<LoyaltyCardConfiguration> loyaltyCardConfigurations = lookupDao.loyaltyCardConfigurationReferral( loyaltyCardConfiguration.getInstitutionId(),
                    loyaltyCardConfiguration.getProductId(),loyaltyCardConfiguration.getLoyaltyCardType());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, loyaltyCardConfigurations);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loyaltyCardConfigurations.get(0));

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }
        return baseResponse;
    }

    public BaseResponse updateSmsTemplateConfig(SmsTemplateConfiguration smsTemplateConfiguration) {
        BaseResponse baseResponse;
        String lookupKey = StringUtils.join(CacheName.SMS_TEMPLATE.name(), smsTemplateConfiguration.getInstitutionId(),
                smsTemplateConfiguration.getProductId());

        if (configurationRepository.updateSmsTemplateConfiguration(smsTemplateConfiguration)) {
           /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, smsTemplateConfiguration);
            logger.debug("Cache updated for key[{}]", lookupKey);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, smsTemplateConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getSmsTemplateConfig(String institutionId) {
        logger.info("Inside getSmsTemplateConfig");
        BaseResponse baseResponse;

        List<SmsTemplateConfiguration> smsTemplateConfigurations = configurationRepository
                .getSmsTemplateConfiguration(institutionId);

        if (!CollectionUtils.isEmpty(smsTemplateConfigurations)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, smsTemplateConfigurations);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteSmsTemplateConfig(String institutionId, String productId, String smsType) {
        logger.info("Inside deleteSmsTemplateConfig");
        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.SMS_TEMPLATE.name(), institutionId, productId, smsType);

        if (configurationRepository.removeSmsTemplateConfiguration(institutionId, productId, smsType)) {


            updatedCache.CACHE.remove(lookupKey);

            logger.debug("Cache updated for key[{}]", lookupKey);
            logger.info("SmsTemplateConfiguration removed successfully");
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }
        return baseResponse;
    }

    @Override
    public BaseResponse addSmsTemplateConfig(SmsTemplateConfiguration smsTemplateConfiguration) {
        logger.info("Inside addSmsTemplateConfig ");
        BaseResponse baseResponse;
        String lookupKey = StringUtils.join(CacheName.SMS_TEMPLATE.name(), smsTemplateConfiguration.getInstitutionId(),
                smsTemplateConfiguration.getProductId());

        if (configurationRepository.saveSmsTemplateConfiguration(smsTemplateConfiguration)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.getSmsTemplateConfiguration(smsTemplateConfiguration));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, smsTemplateConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, smsTemplateConfiguration);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse addDmsFolderConfiguration(List<DmsFolderConfiguration> dmsFolderConfigurationList) {


        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.DMS_FOLDER_CONFIG.name(),
                dmsFolderConfigurationList.get(0).getInstitutionId());


        if (configurationRepository.saveDmsFolderConfiguration(dmsFolderConfigurationList)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.getDmsFolderInformation(
                    dmsFolderConfigurationList.get(0).getInstitutionId()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.FAILED.name());
        }
        return baseResponse;

    }

    @Override
    public BaseResponse getDmsFolderConfiguration(String institutionId) {
        BaseResponse baseResponse;

        List<DmsFolderConfiguration> dmsFolderConfiguration = configurationRepository
                .getDmsFolderConfiguration(institutionId);

        if (!CollectionUtils.isEmpty(dmsFolderConfiguration)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, dmsFolderConfiguration);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;
    }


    @Override
    public BaseResponse updateDmsFolderConfiguration(DmsFolderConfiguration dmsFolderConfiguration) {
        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.DMS_FOLDER_CONFIG.name(), dmsFolderConfiguration.getInstitutionId());

        if (configurationRepository.updateDmsFolderConfiguration(dmsFolderConfiguration)) {

            List<DmsFolderConfiguration> dmsFolderInformation = lookupDao.getDmsFolderInformation(dmsFolderConfiguration.getInstitutionId());

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, dmsFolderInformation);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteDmsFolderConfiguration(String institutionId, String indexName) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.DMS_FOLDER_CONFIG.name(), institutionId);

        if (configurationRepository.removeDmsFolderConfiguration(institutionId, indexName)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey,lookupDao.getDmsFolderInformation(institutionId));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }

        return baseResponse;
    }

    @Override
    public BaseResponse addMasterMappingConfiguration(List<MasterMappingConfiguration> masterMappingConfigurationList) {

        logger.debug("Adding master mapping configuration.. ");
        BaseResponse baseResponse = null;

        if (configurationRepository.saveMasterMappingConfiguration(masterMappingConfigurationList)) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.FAILED.name());
        }

        return baseResponse;
    }

    @Override
    public BaseResponse getLosChargeConfiguration(String institutionId) {
        BaseResponse baseResponse = null;
        List<LosChargeConfig> losChargeConfiguration = configurationRepository.getLosChargeConfiguration(institutionId);

        if (!CollectionUtils.isEmpty(losChargeConfiguration)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, losChargeConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse createLosChargeConfig(LosChargeConfig losChargeConfig) {

        return GngUtils.getBaseResponse(HttpStatus.OK, configurationRepository.createLosChargeConfiguration(losChargeConfig));
    }

    @Override
    public BaseResponse addCaseCancellationConfig(CaseCancellationJobConfig caseCancellationJobConfig) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.CASE_CANCEL_CONFIG.name(),
                caseCancellationJobConfig.getInstitutionId());


        if (configurationRepository.saveCaseCancelConfig(caseCancellationJobConfig)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.getCaseCancellationJobConfig(caseCancellationJobConfig.getInstitutionId()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.FAILED.name());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse getCaseCancellationConfig(String institutionId) {

        BaseResponse baseResponse = null;

        CaseCancellationJobConfig caseCancellationJobConfig = configurationRepository.getCaseCancellationConfiguration(institutionId);

        if (null != caseCancellationJobConfig) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, caseCancellationJobConfig);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, GngUtils.getNoContentErrorList());
        }
        return baseResponse;
    }


    @Override
    public BaseResponse deleteCaseCancellationConfig(String institutionId) {

        BaseResponse baseResponse = null;
        if (configurationRepository.deleteCaseCancellationConfiguration(institutionId)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.FAILED.name());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse getIMPSDomain(String institutionID) {

        BaseResponse baseResponse;

        IMPSConfigDomain impsConfigDomain = configurationRepository
                .findIMPSConfigByInstitutionId(institutionID);

        if (null!= impsConfigDomain) {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, impsConfigDomain);

        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }

        return baseResponse;

    }

    @Override
    public BaseResponse addIMPSConfiguration(IMPSConfigDomain impsConfigDomain) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.IMPS.name(),
                impsConfigDomain.getInstitutionId());


        if (configurationRepository.addIMPSConfiguration(impsConfigDomain)) {

            /* Store data in cache */
            updatedCache.CACHE.put(lookupKey, lookupDao.getImpsConfiguration(impsConfigDomain.getInstitutionId()));

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.FAILED.name());
        }
        return baseResponse;
    }

    @Override
    public BaseResponse deleteIMPSDomain(IMPSConfigDomain impsConfigDomain) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.IMPS.name(), impsConfigDomain.getInstitutionId());

        if (configurationRepository.deleteIMPSConfig(impsConfigDomain)) {

            /* Store data in cache */
            updatedCache.CACHE.remove(lookupKey);

            logger.debug("Cache updated for key[{}]", lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, com.softcell.constants.Status.FAILED.name());

        }

        return baseResponse;
    }

    /**
     * Fetch all unassigned applications from DB and creates InstitutionGroup wise application ids map
     * and put it in Redis cache
     * @return
     */
    @Override
    public BaseResponse createQueueGroupConfiguration() {
        BaseResponse response;
        try {
            logger.debug("QueueManagement configuration started...");
            // Create institution wise QueueGroup  UnassignedApplication-ids map
            Map<String, Map<String, List<String>>> instituteWiseQueueGroupApplicationIdMap = createInstitutionWiseUnassignedApplicationIdMap();

            instituteWiseQueueGroupApplicationIdMap.keySet().forEach(institutionId -> {
                //create List of QueueUnassignedCases for each Institution
                List<QueueUnassignedCases> QueueUnassignedCasesList = createQueueUnassignedCasesList(instituteWiseQueueGroupApplicationIdMap.get(institutionId), institutionId);
                // put all QueueUnassignedCasess for an Institution in redis cache
                queueHandler.addQueueUnassignedCasesInCache(QueueUnassignedCasesList);
            });
            logger.debug("QueueManagement configuration successfully completed.");
            response = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.SUCCESS.name());
        }catch (Exception e){
            response = GngUtils.getBaseResponse(HttpStatus.OK, com.softcell.constants.Status.ERROR.name());
            logger.debug("QueueManagement configuration failed :Error occurred.");
        }
        return response;
    }

    /**
     * @return Map<institutionId, Map<queueGroupName, List<Unassigned-ApplicationIds>>>
     */
    private Map<String, Map<String, List<String>>> createInstitutionWiseUnassignedApplicationIdMap() {

        Map<String, Map<String, List<String>>> resultMap = new HashMap<>();
        // fetch all unassigned applications from db
        List<GoNoGoCustomerApplication> applicationList = queueRepository.getAllUnassignedApplications();
        // sort them in ascending of their time
        Collections.sort(applicationList, new Comparator<GoNoGoCustomerApplication>() {
            public int compare(GoNoGoCustomerApplication o1, GoNoGoCustomerApplication o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });

        //create institution wise application-ids map
        applicationList.forEach(application -> {
            if(application.getApplicationRequest() != null){
                String institutionId = application.getApplicationRequest().getHeader().getInstitutionId();
                List<String>  applicationIds = new ArrayList<>();
                if(!resultMap.containsKey(institutionId)){
                    resultMap.put(institutionId, new HashMap<>());
                }
                String groupName = application.getAssignedQueueGroupId();
                if(!resultMap.get(institutionId).containsKey(groupName)){
                    resultMap.get(institutionId).put(groupName, new ArrayList<>());
                }
                resultMap.get(institutionId).get(groupName).add(application.getGngRefId());
            }
        });

        return resultMap;
    }

    private List<QueueUnassignedCases> createQueueUnassignedCasesList(Map<String, List<String>> queueGroupApplicationIdMap, String institutionId) {
        List<QueueUnassignedCases> QueueUnassignedCasesList = new ArrayList<>();
        for(Map.Entry<String, List<String>> queGroup : queueGroupApplicationIdMap.entrySet()){
            String instituteGroupId = "PENDING_CASES_" + institutionId + "_" + queGroup.getKey();
            List<String> unassignedApplicationIds = queGroup.getValue();
            queueGroupIds.add(instituteGroupId);

            QueueUnassignedCases tempObj = new QueueUnassignedCases();
            tempObj.setInstituteGroupId(instituteGroupId);
            tempObj.setUnassignedCaseIds(unassignedApplicationIds);
            QueueUnassignedCasesList.add(tempObj);
        }
        return QueueUnassignedCasesList;
    }

    @Override
    public BaseResponse insertMasterSchedulerConfig(MasterSchedulerConfiguration masterSchedulerConfiguration, String userName, String reason, String masterType) {
        BaseResponse baseResponse = null;
        boolean institutionIdContains = Cache.INSTITUTION_QUEUE_CONFIG.containsKey(masterSchedulerConfiguration.getInstitutionId());
        boolean timeValidation = GngDateUtil.compareTime(masterSchedulerConfiguration.getFromTime(),masterSchedulerConfiguration.getToTime());
        boolean masterNameValidation = StringUtils.equals(masterType,MasterMappingDetails.DROP_DOWN_MASTER.getMasterName())?true:GngUtils.masterNameValidation(masterSchedulerConfiguration.getMasterName());

        if (institutionIdContains && timeValidation && masterNameValidation){
            MasterSchedulerActivityLogs activityLogs = new MasterSchedulerActivityLogs();
            activityLogs.setMasterSchedulerConfiguration(masterSchedulerConfiguration);
            activityLogs.setUserName(userName);
            activityLogs.setDate(new Date());
            activityLogs.setReason(reason);
            boolean status = configurationRepository.insertMasterSchedularConfig(masterSchedulerConfiguration.getInstitutionId(), masterSchedulerConfiguration.getMasterName(), masterSchedulerConfiguration.getFromTime(), masterSchedulerConfiguration.getToTime());
            if (status) {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, Constant.SUCCESS);
                activityLogs.setStatus(Constant.SUCCESS);
            }else {
                GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED,ErrorCode.MASTER_CONFIG_NOT_UPDATED);
                activityLogs.setStatus(Constant.FAILED);
            }
            configurationRepository.insterMasterActivityLogs(activityLogs);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST,
                    GngUtils.getInternalServerErrorList(String.format(ErrorCode.MASTER_CONFIG_RESTRICTION,masterSchedulerConfiguration.getInstitutionId(),masterSchedulerConfiguration.getMasterName())));

        }
        return baseResponse;
    }

    @Override
    public BaseResponse fetchMasterSchedulerConfig(String institutionId) {
        BaseResponse baseResponse;
        List<MasterSchedulerConfiguration> masterSchedulerConfigurations = configurationRepository.fetchMasterSchedulerConfig(institutionId);
        if (CollectionUtils.isNotEmpty(masterSchedulerConfigurations)){
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, masterSchedulerConfigurations);
        }else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT,
                    GngUtils.getInternalServerErrorList(String.format(ErrorCode.MASTER_CONFIG_NOT_FOUND,institutionId)));
        }
        return baseResponse;
    }


}
