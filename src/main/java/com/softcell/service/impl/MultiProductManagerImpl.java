package com.softcell.service.impl;


import com.softcell.constants.ActionName;
import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.elsearch.IndexingElasticSearchRepository;
import com.softcell.dao.mongodb.repository.elsearch.IndexingRepository;
import com.softcell.dao.mongodb.repository.multiproduct.MultiProductRepository;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.multiproduct.AssetCountResponse;
import com.softcell.gonogo.model.response.multiproduct.DealerSubventionWaivedOffResponse;
import com.softcell.gonogo.model.response.multiproduct.MultiProductError;
import com.softcell.gonogo.model.response.multiproduct.MultiProductResponse;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.service.MultiProductManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.*;

/**
 * @author yogeshb
 */
@Service
public class MultiProductManagerImpl implements MultiProductManager {

    @Autowired
    private MultiProductRepository multiProductRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private IndexingRepository indexingRepository;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private AuditHelper auditHelper;

    private Logger logger = LoggerFactory.getLogger(MultiProductManagerImpl.class);

    @Override
    public BaseResponse getProductCount(
            MultiProductRequest multiProductRequest) throws Exception {
        BaseResponse baseResponse;


        GoNoGoCroApplicationResponse parentApplication = multiProductRepository
                .getParentApplication(multiProductRequest.getRefID());

        if (null != parentApplication) {
            AssetCountResponse assetCountResponse = new AssetCountResponse();
            Set<String> refIDs = getAllRefIDAgainstCurrentID(parentApplication);


            int allowedProducts = GngUtils.getAllowedProduct(parentApplication);

            long addedProducts = getAddedProductsCount(parentApplication);

            boolean firstPostIPAFlag = isItFirstPostIPA(multiProductRequest
                    .getRefID());

            if (firstPostIPAFlag) {
                assetCountResponse.setProductNumber(1);
            } else {
                assetCountResponse
                        .setProductNumber((int) addedProducts + 1);
            }
            assetCountResponse.setUnUtilizedAmt(getUnUtilizedAmount(refIDs,
                    parentApplication));
            assetCountResponse.setProductAllowed(allowedProducts);
            assetCountResponse
                    .setProductRemaining((int) (allowedProducts - addedProducts));

            if (null != parentApplication.getCroDecisions() && !parentApplication.getCroDecisions().isEmpty()) {
                assetCountResponse.setApprovedAmount(parentApplication.getCroDecisions().get(0).getAmtApproved());
            }

            if ((addedProducts < allowedProducts)
                    && assetCountResponse.getUnUtilizedAmt() > 0) {
                /**
                 * Success Response
                 */
                assetCountResponse.setStatus(Constant.SUCCESS);
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, assetCountResponse);

            } else {
                /**
                 * Response with Error List
                 */

                Collection<Error> errors = new ArrayList<>();
                errors.add(Error.builder()
                        .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                        .message(ErrorCode.PRODUCT_ALLOWED_FAILED)
                        .level(Error.SEVERITY.LOW.name())
                        .build());

                baseResponse = GngUtils.getBaseResponse(HttpStatus.PRECONDITION_FAILED, errors);

            }
        } else {
            /**
             * Response With No content and error List
             */
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());

        }
        return baseResponse;

    }

    /**
     * @param refIDs
     * @param parentApplication
     * @return
     */
    private double getUnUtilizedAmount(Set<String> refIDs,
                                       GoNoGoCroApplicationResponse parentApplication) {
        double totalNetFundingAmount = 0;


        for (String refID : refIDs) {
            totalNetFundingAmount = totalNetFundingAmount
                    + getNetFundingAmountFromPostIPA(refID);
        }

        double approvedAmt = 0;
        if (null != parentApplication.getCroDecisions() && !parentApplication.getCroDecisions().isEmpty()) {
            approvedAmt = parentApplication.getCroDecisions().get(0)
                    .getAmtApproved();
        }


        return approvedAmt - totalNetFundingAmount;
    }

    @Override
    public BaseResponse addNewProduct(
            MultiProductRequest multiProductRequest) throws Exception {
        BaseResponse baseResponse = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog = auditHelper.createActivityLog(null, multiProductRequest.getHeader());

        activityLog.setAction(ActionName.MULTIPRODUCT.toString());
        activityLog.setRefId(multiProductRequest.getRefID());
        activityLog.setStage(GNGWorkflowConstant.APRV.toFaceValue());
        activityLog.setInstitutionId(multiProductRequest.getHeader().getInstitutionId());

        GoNoGoCroApplicationResponse parentApplication = multiProductRepository
                .getParentApplication(multiProductRequest.getRefID());

        MultiProductResponse multiProductResponse = null;
        if (null != parentApplication) {
            /**
             * Need to check stage against this application(PD_DE).
             */
            boolean firstPostIPAFlag = isItFirstPostIPA(multiProductRequest
                    .getRefID());

            if (firstPostIPAFlag) {

                multiProductResponse = new MultiProductResponse();

                multiProductResponse.setUnUtilizedAmt(parentApplication
                        .getCroDecisions().get(0).getAmtApproved());

                multiProductResponse.setProductNumber(1);

                multiProductResponse.setStatus(Constant.SUCCESS);
                activityLog.setStatus(Status.SUCCESS.toString());
                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, multiProductResponse);

            } else {
                Set<String> refIDs = getAllRefIDAgainstCurrentID(parentApplication);

                multiProductResponse = checkNewProductEligibility(
                        multiProductRequest, parentApplication, refIDs);

                if (StringUtils.equals(Constant.SUCCESS,
                        multiProductResponse.getStatus())) {

                    String newRefID;

                    do {
                        newRefID = createNewReferenceIDForNewProduct(parentApplication);
                    }
                    while (StringUtils.isBlank(newRefID));

                    parentApplication = copy(parentApplication, newRefID, refIDs);

                    try {
                        indexingRepository = new IndexingElasticSearchRepository();
                        indexingRepository.indexNotification(parentApplication);
                    } catch (Exception exception) {
                        logger.error("Some thing went wrong in update application in elastic search.");
                    }

                    multiProductResponse.setRefID(parentApplication.getGngRefId());

                    multiProductResponse.setStatus(Constant.SUCCESS);
                    activityLog.setStatus(Status.SUCCESS.toString());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, multiProductResponse);
                } else {
                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.ASSET_CATEGORY_FAILED)
                            .level(Error.SEVERITY.LOW.name())
                            .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                            .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                            .build());
                    activityLog.setStatus(Status.FAIL.toString());
                    activityLog.setCustomMsg(ErrorCode.ASSET_CATEGORY_FAILED);
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.PRECONDITION_FAILED, errors);
                }
            }
        } else {
            /**
             * Response With No content and error List
             */
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);
        return baseResponse;
    }

    /**
     * @param refID
     * @return
     */
    private boolean isItFirstPostIPA(String refID) {
        boolean stageFlag = multiProductRepository.checkApplicationStage(refID);
        boolean postIPAFlag = multiProductRepository.getPostIPAExistance(refID);
        if (stageFlag && postIPAFlag) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param parentApplication
     * @param newRefID
     * @param refIDs
     * @return
     */
    private GoNoGoCroApplicationResponse copy(
            GoNoGoCroApplicationResponse parentApplication, String newRefID,
            Set<String> refIDs) {
        double unUtilizedAmount = getUnUtilizedAmount(refIDs, parentApplication);
        double approvedAmount = parentApplication.getCroDecisions().get(0)
                .getAmtApproved();
        parentApplication.getCroDecisions().get(0)
                .setUnUtilizedAmt(unUtilizedAmount);
        parentApplication.getCroDecisions().get(0)
                .setUtilizedAmt(approvedAmount - unUtilizedAmount);

        Date dateTime = new Date();
        parentApplication.setDateTime(dateTime);
        parentApplication.getApplicationRequest().getHeader()
                .setDateTime(dateTime);
        parentApplication
                .setRootID((null == parentApplication.getRootID()) ? parentApplication
                        .getGngRefId() : parentApplication.getRootID());
        parentApplication.setParentID(parentApplication.getGngRefId());
        parentApplication.setGngRefId(newRefID);
        parentApplication.getApplicationRequest().setRefID(newRefID);
        parentApplication.getApplicationRequest().setCurrentStageId(
                GNGWorkflowConstant.APRV.toFaceValue());
        parentApplication.setProductSequenceNumber(parentApplication
                .getProductSequenceNumber() + 1);
        return multiProductRepository.cloneApplication(parentApplication);
    }

    /**
     * Create new application reference ID for additional product check whether
     * the new reference id is not exist in DB, else create another.
     *
     * @param parentApplication
     */
    private String createNewReferenceIDForNewProduct(
            GoNoGoCroApplicationResponse parentApplication) {
        if (null != parentApplication) {
            String newRefID = parentApplication.getApplicationRequest()
                    .getHeader().getDealerId()
                    + applicationRepository.getSequence(parentApplication
                            .getApplicationRequest().getHeader().getDealerId(),
                    parentApplication.getApplicationRequest()
                            .getHeader().getInstitutionId());
            if (!multiProductRepository.checkRefernceIDExistance(newRefID)) {
                return newRefID;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * BRE will return flag in Eligibility Entity, as count. which is different
     * from system app, varies customer to customer. if above count is
     * greater than one or more then allow to user select second product
     * otherwise return message, 'You are not eligible for additional credit'.
     *
     * @param multiProductRequest
     * @param parentApplication
     * @param refIDs
     * @return
     */
    private MultiProductResponse checkNewProductEligibility(
            MultiProductRequest multiProductRequest,
            GoNoGoCroApplicationResponse parentApplication, Set<String> refIDs) {

        MultiProductResponse multiProductResponse = new MultiProductResponse();

        /**
         * Here we need to check SO cannot capture the same asset category when
         * adding second or third product.
         *
         * Eg: If the first asset selected is TV then irrespective of the brand
         * another asset category of TV should not be allowed to be captured.
         */
        boolean verifyAssetCategoryFlag = verifyAssetCategory(
                multiProductRequest, parentApplication, refIDs);
        if (!verifyAssetCategoryFlag) {
            MultiProductError multiProductException = new MultiProductError();
            multiProductException.setErrorCode("ASSET_NOT_ALLOWED");
            multiProductException
                    .setErrorDescription("Same Asset Category not allowed with other manufacturer.");
            multiProductResponse.setError(multiProductException);
            multiProductResponse.setStatus(Constant.FAILED);
            return multiProductResponse;
        }

        /**
         * Here we need to check the balance amount available or not.
         */
        multiProductResponse = verifyUnutilizedAmount(parentApplication, refIDs);
        if (StringUtils.equals(Constant.FAILED,
                multiProductResponse.getStatus())) {
            return multiProductResponse;
        }

        return multiProductResponse;

    }

    /**
     * @param parentApplication
     * @return
     */
    private long getAddedProductsCount(GoNoGoCroApplicationResponse parentApplication) {

        if (StringUtils.isBlank(parentApplication.getRootID())) {
            return 1;
        }
        return multiProductRepository.getProductCount(parentApplication
                .getRootID());
    }

    /**
     * Here we need to check SO cannot capture the same asset category when
     * adding second or third product.
     * <p/>
     * Eg: If the first asset selected is TV then irrespective of the brand
     * another asset category of TV should not be allowed to be captured.
     *
     * @param parentApplication
     * @param refIDs
     */
    private boolean verifyAssetCategory(
            MultiProductRequest multiProductRequest,
            GoNoGoCroApplicationResponse parentApplication, Set<String> refIDs) {

        String newAssetCategory = multiProductRequest.getPostIPA()
                .getAssetDetails().get(0).getAssetCtg();

        List<String> assetCategoryList = getParentAssetCategories(refIDs);

        if (StringUtils.isNotBlank(newAssetCategory)
                && !assetCategoryList.contains(newAssetCategory)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param refIDs
     * @return
     */
    private List<String> getParentAssetCategories(Set<String> refIDs) {
        List<String> assetCategories = new ArrayList<String>();
        for (String refID : refIDs) {
            String assetCategory = getAssetCategoryFromPostIPA(refID);
            if (StringUtils.isNotBlank(assetCategory)) {
                assetCategories.add(assetCategory);
            }
        }
        return assetCategories;
    }

    /**
     * @param parentApplication
     * @return
     */
    private Set<String> getAllRefIDAgainstCurrentID(
            GoNoGoCroApplicationResponse parentApplication) {
        Set<String> refenceIDs = new HashSet<String>();

        if (StringUtils.isBlank(parentApplication.getRootID())) {
            refenceIDs.add(parentApplication.getGngRefId());
        } else {
            Set<String> refIDs = multiProductRepository
                    .getAllRefenceIDAgainstRootApplication(parentApplication
                            .getRootID());
            refenceIDs.addAll(refIDs);
        }
        return refenceIDs;
    }

    /**
     * @param refID
     * @return
     */
    private String getAssetCategoryFromPostIPA(String refID) {

        PostIpaRequest postIPA = multiProductRepository.getPostIPA(refID);
        if (null != postIPA && null != postIPA.getPostIPA()
                && postIPA.getPostIPA().getAssetDetails().size() > 0) {
            return postIPA.getPostIPA().getAssetDetails().get(0).getAssetCtg();
        }
        return null;
    }

    /**
     * Here we need to check the balance amount available or not.
     *
     * @param parentApplication
     * @param refIDs
     */
    private MultiProductResponse verifyUnutilizedAmount(
            GoNoGoCroApplicationResponse parentApplication, Set<String> refIDs) {
        MultiProductResponse multiProductResponse = new MultiProductResponse();

        double totalNetFundingAmount = 0;
        for (String refID : refIDs) {
            totalNetFundingAmount = totalNetFundingAmount
                    + getNetFundingAmountFromPostIPA(refID);
        }
        double approvedAmt = parentApplication.getCroDecisions().get(0)
                .getAmtApproved();

        double unUtilizedAmount = approvedAmt - totalNetFundingAmount;

        if (unUtilizedAmount < 0) {
            multiProductResponse.setUnUtilizedAmt(unUtilizedAmount);
            multiProductResponse.setStatus(Constant.FAILED);
            MultiProductError error = new MultiProductError();
            error.setErrorCode("UNUTILIZED_AMOUNT_NOT_ELIGIBILITY");
            error.setErrorDescription("UnutilizedAmount is not available to add new product.");
            multiProductResponse.setError(error);
            return multiProductResponse;
        } else {
            multiProductResponse
                    .setUnUtilizedAmt((totalNetFundingAmount == 0.0) ? approvedAmt
                            : unUtilizedAmount);
            multiProductResponse.setStatus(Constant.SUCCESS);
            return multiProductResponse;
        }
    }

    /**
     * @param refID
     * @return
     */
    private double getNetFundingAmountFromPostIPA(String refID) {
        PostIpaRequest postIpaRequest = multiProductRepository
                .getPostIPA(refID);
        if (null != postIpaRequest) {
            return postIpaRequest.getPostIPA().getNetFundingAmount();
        }
        return 0;
    }

    @Override
    public BaseResponse isDealerSubventionWaivedOff(
            Header header) {
        DealerSubventionWaivedOffResponse dealerSubventionWaivedOffResponse = new DealerSubventionWaivedOffResponse();
        dealerSubventionWaivedOffResponse
                .setDealerSubventionWaivedOff(multiProductRepository
                        .checkDealerSubventionWaivedOff(header));

        return GngUtils.getBaseResponse(HttpStatus.OK, dealerSubventionWaivedOffResponse);

    }
}
