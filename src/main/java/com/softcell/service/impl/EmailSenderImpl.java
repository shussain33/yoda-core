package com.softcell.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.dao.mongodb.repository.workflow.WorkFlowMongoRepository;
import com.softcell.dao.mongodb.repository.workflow.WorkFlowRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.OtpVerificationData;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.MailRequest;

import com.softcell.gonogo.model.email.*;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.sms.SmsServiceReqResLog;
import com.softcell.gonogo.service.factory.EmailTemplateBuilder;
import com.softcell.gonogo.service.factory.impl.EmailTemplateBuilderImpl;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.RandomUniqueIdGenerate;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.EmailSender;
import com.softcell.service.PlEmailSendManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmailSenderImpl implements EmailSender {
    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    private static final Logger logger = LoggerFactory.getLogger(EmailSenderImpl.class);

    @Autowired
    private WorkFlowRepository workFlowRepository;


    @Autowired
    private EmailTemplateBuilder emailTemplateBuilder;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    private String mailContent;

    public EmailSenderImpl() {
        if(null==workFlowRepository){
            workFlowRepository = new WorkFlowMongoRepository(MongoConfig.getMongoTemplate());
        }
        if(null == emailTemplateBuilder){
            emailTemplateBuilder = new EmailTemplateBuilderImpl();
        }
    }

    @Override
    public boolean sendCustomerAckEmail(GoNoGoCustomerApplication goNoGoCustomerApplication,
                                        String[] to,String[] cc, String subjectLine, String templateName) {

        EmailTemplateDetails emailTemplateDetails = new EmailTemplateDetails();

        emailTemplateDetails.setInstitutionId(goNoGoCustomerApplication
                .getApplicationRequest().getHeader().getInstitutionId());

        emailTemplateDetails.setRefId(goNoGoCustomerApplication.getGngRefId());

        emailTemplateDetails.setLoanAmount(goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getApplication()
                .getLoanAmount());

        emailTemplateDetails.setProductType(goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getApplication()
                .getLoanType());

        emailTemplateDetails.setTemplate(templateName);

        String template = emailTemplateBuilder.buildEmailTemplate(emailTemplateDetails);

        emailTemplateDetails.setTemplate(template);

        emailTemplateDetails.setCc(cc);

        emailTemplateDetails.setTo(to);

        emailTemplateDetails.setSubject(subjectLine);

        if (null != template && (null != to || null != cc)) {

            MailRequest mailContent = getMailRequest(emailTemplateDetails);

            PlEmailSendManager plEmailSendManager = new PlEmailSendManagerImpl();

            if (plEmailSendManager.sendEmailToClient(mailContent)) {

                emailTemplateDetails.setSent(true);

                logger.warn("Mail  has been sent.");

            } else {

                emailTemplateDetails.setSent(false);

                logger.warn("Mail not has been sent.");
            }
        } else {

            emailTemplateDetails.setSent(false);

            logger.warn("Mail not has been sent.");

        }


        if (workFlowRepository.saveMailLog(emailTemplateDetails)) {
            logger.info("Send Mail Logged.");
            return true;
        }

        return false;
    }

    @Override
    public BaseResponse sendVerificationMail(EmailRequest emailRequest) {
        logger.info("Inside sendVerificationMail()");
        BaseResponse response = null;
        String otp = RandomUniqueIdGenerate.generateUniqueId(6).toUpperCase();
        SmsServiceReqResLog smsServiceReqResLog = null;
        String institutionId = emailRequest.getHeader().getInstitutionId();
        if(Institute.isInstitute(institutionId,Institute.SBFC)){
            otp = RandomUniqueIdGenerate.generateNumericOTP(6);
        }

        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setAction("VERIFY_EMAIL");
        activityLogs.setRefId(emailRequest.getRefId());
        activityLogs.setInstitutionId(emailRequest.getHeader().getInstitutionId());
        activityLogs.setUserName(emailRequest.getHeader().getLoggedInUserId());
        activityLogs.setCustomMsg("verification email");

        WFJobCommDomain emailconfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                emailRequest.getHeader().getInstitutionId(), UrlType.GUPSHUP.toValue());

        if (null == emailconfig) {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLogs.setStatus(GNGWorkflowConstant.FAILED.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        List<SmsServiceReqResLog> smsServiceLogList = applicationRepository.fetchSmsServiceLog(emailRequest.getIdentifier());

        if(CollectionUtils.isNotEmpty(smsServiceLogList) && !checkMaxTry(institutionId, smsServiceLogList)){

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.EMAIL_OTP_LIMIT_EXCEEDED)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .level(Error.SEVERITY.HIGH.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.FORBIDDEN, errors);
        }else{

            smsServiceReqResLog = createSmsServiceLog(emailRequest, smsServiceLogList);
        }

        Map<String,String> postParmeters=getRequestParams(emailconfig,emailRequest,otp);
        SendMailRequest mailRequest = SendMailRequest.builder().requestParams(postParmeters).build();
        logger.info("Request : " , mailRequest);
        String url = Arrays.asList(emailconfig.getBaseUrl(), emailconfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        logger.info("URL",url);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            EmailResponse emailResponse = (EmailResponse) TransportUtils.postJsonRequest(mailRequest, url, EmailResponse.class);
            smsServiceReqResLog.setOrgRes(JsonUtil.ObjectToString(emailResponse));
            emailResponse.setOtp(otp);
            if(Cache.isSentOtpDisable(emailRequest.getHeader().getInstitutionId())){
                removeOtpFromResponse(institutionId,emailResponse);
            }
            emailResponse.setIdentifier(smsServiceReqResLog.getIdentifier());
            response = GngUtils.getBaseResponse(HttpStatus.OK,emailResponse);
            activityLogs.setStatus(GNGWorkflowConstant.SUCCESS.name());

            smsServiceReqResLog.setAction(url);
            externalAPILogRepository.saveSmsServiceReqResLog(smsServiceReqResLog);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occured while sending mail to customer");
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from EmailSender in thread %s",
                Thread.currentThread().getName()));
        applicationEventPublisher.publishEvent(activityLogs);
        return response;
    }

    private boolean checkMaxTry(String institutionId, List<SmsServiceReqResLog> smsServiceLogList){
        //as of now, we are hardcoding the opt resend value
        if(smsServiceLogList.size() >= 3)
            return false;

        return true;
    }

    private SmsServiceReqResLog createSmsServiceLog(EmailRequest emailRequest, List<SmsServiceReqResLog> smsServiceLogList){
        SmsServiceReqResLog smsServiceReqResLog = null;

        if(CollectionUtils.isNotEmpty(smsServiceLogList)){

            smsServiceReqResLog = SmsServiceReqResLog.builder()
                    .identifier(emailRequest.getIdentifier())
                    .institutionId(emailRequest.getHeader().getInstitutionId())
                    .emailIds(emailRequest.getEmailId()).build();

        }else{
            smsServiceReqResLog = SmsServiceReqResLog.builder()
                    .identifier(UUID.randomUUID().toString())
                    .institutionId(emailRequest.getHeader().getInstitutionId())
                    .emailIds(emailRequest.getEmailId()).build();
        }

        return smsServiceReqResLog;
    }

    private void removeOtpFromResponse(String institutionId, EmailResponse emailResponse) {
        if(StringUtils.isNotEmpty(emailResponse.getOtp())){
            //as of now we are hardcoding the otp retry value
            OtpVerificationData otpVerificationData = OtpVerificationData.builder()
                    .id(UUID.randomUUID().toString())
                    .institutionId(institutionId)
                    .otp(emailResponse.getOtp())
                    .retry(3) //as of now, we are hardcoding the opt resend value
                    .purpose("MAIL")
                    .build();
            applicationRepository.saveVerificationOtp(otpVerificationData);
            emailResponse.setOtp(null);
            emailResponse.setUuid(otpVerificationData.getId());
        }
    }

    private MailRequest getMailRequest(EmailTemplateDetails emailTemplateDetails) {

        return MailRequest.builder()
                .refID(emailTemplateDetails.getRefId())
                .institutionId(emailTemplateDetails.getInstitutionId())
                .to(emailTemplateDetails.getTo())
                .cc(emailTemplateDetails.getCc())
                .subject(emailTemplateDetails.getSubject())
                .content(emailTemplateDetails.getTemplate())
                .htmlEnabled(true)
                .build();
    }

    public String getFormattedMailContent(String... args) {
        String formattedSmsText = String.format(this.mailContent, args);
        return formattedSmsText;
    }

    public Map<String,String> getRequestParams(WFJobCommDomain emailconfig,EmailRequest emailRequest,String otp)
    {

        logger.info("OTP for email verification is:",otp);
        List<HeaderKey> keyValue = emailconfig.getKeys();
        Map<String, String> postParameters = new HashMap<>();
        for (HeaderKey key : keyValue) {
            if (key.getKeyName().equalsIgnoreCase(PostParameterKeys.CONTENT)) {
                mailContent = key.getKeyValue();
                //TODO save otp against mailId.
                postParameters.put(key.getKeyName(), getFormattedMailContent(otp, emailRequest.getRefId()));
            } else {
                postParameters.put(key.getKeyName(), key.getKeyValue());
            }
        }
        postParameters.put(PostParameterKeys.RECIPIENTS, emailRequest.getEmailId());
        return postParameters;
    }

    @Override
    public BaseResponse sendMail(GenericMailRequest emailRequest) {
        logger.info("Inside sendMail()");
        BaseResponse response = null;
        MailRequest mailRequest = new MailRequest();
        GoNoGoCustomerApplication goNoGoCustomerApplication =null;
        String action = emailRequest.getAction();
        String institutionId = emailRequest.getHeader().getInstitutionId();
        String refId = emailRequest.getRefID();
        // setting institutionId in mail request
        mailRequest.setInstitutionId(emailRequest.getHeader().getInstitutionId());
        String otp = GngUtils.generateOtp();
        WFJobCommDomain emailconfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                emailRequest.getHeader().getInstitutionId(), UrlType.GENERIC_MAIL.toValue());
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setAction(EndPointReferrer.SEND_MAIL);
        activityLogs.setRefId(emailRequest.getRefID());
        activityLogs.setInstitutionId(emailRequest.getHeader().getInstitutionId());
        activityLogs.setCustomMsg("verification email");

        if (null == emailconfig) {
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            activityLogs.setStatus(GNGWorkflowConstant.FAILED.name());
            return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        //getting template Data from templateDetails
        TemplateDetails templateDetails = null;

        templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteIdAndTemplateName(
                emailRequest.getHeader().getInstitutionId(), emailRequest.getHeader().getProduct().toProductName(),action);

        if(templateDetails != null){

            if(Institute.isInstitute(emailRequest.getHeader().getInstitutionId(),Institute.AMBIT)){
                if(templateDetails.getTemplateContent() != null) {
                    this.mailContent = templateDetails.getTemplateContent();
                }
                String emailContent = getFormattedMailContent(otp, emailRequest.getRefID());

                mailRequest.setContent(emailContent);
                mailRequest.setTo(emailRequest.getTo());
                mailRequest.setHtmlEnabled(true);
                mailRequest.setRefID(refId);
                if(templateDetails.getMailSubject() != null) {
                    String subject = templateDetails.getMailSubject();
                    mailRequest.setSubject(subject);
                } else{
                    mailRequest.setSubject("Mail with No Subject");
                }
            }
        }else{
            mailRequest.setSubject("No Subject Found");
            mailRequest.setContent("No Content Found for respective" + emailRequest.getHeader().getInstitutionId());
        }

        try {
            logger.info("Request : {}" , JsonUtil.ObjectToString(mailRequest));
        } catch (JsonProcessingException e) {
            ExceptionUtils.getStackTrace(e);
        }
        String url = Arrays.asList(emailconfig.getBaseUrl(), emailconfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        logger.info("URL {}",url);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            MessagingResponse emailResponse = (MessagingResponse) TransportUtils.postJsonRequest(mailRequest, url, MessagingResponse.class);
            emailResponse.setOtp(otp);
            response = GngUtils.getBaseResponse(HttpStatus.OK,emailResponse);
            activityLogs.setStatus(GNGWorkflowConstant.SUCCESS.name());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occured while sending mail to customer");
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from EmailSender in thread %s",
                Thread.currentThread().getName()));
        applicationEventPublisher.publishEvent(activityLogs);
        return response;
    }

}
