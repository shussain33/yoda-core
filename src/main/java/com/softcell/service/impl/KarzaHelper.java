package com.softcell.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yogesh on 19/7/18.
 */
public class KarzaHelper {

    public static final String PAN ="PAN";
    public static final String GST ="GST";
    public static final String GSTAUTH ="GSTAUTH";
    public static final String TAN ="TAN";
    public static final String TANAUTH ="TANAUTH";
    public static final String PASSPORT ="PASSPORT";
    public static final String VOTERID ="VOTER-ID";
    public static final String DRIVINGLICENSE ="DRIVING-LICENSE";


    public static final String AADHAR_REQUEST_TYPE = "aadhar";
    public static final String PAN_REQUEST_TYPE = "pan";
    public static final String VOTER_REQUEST_TYPE = "voter";
    public static final String PASSPORT_REQUEST_TYPE ="passport";
    public static final String GST_REQUEST_TYPE ="gst";
    public static final String DL_REQUEST_TYPE ="dl";
    public static final String GST_DETAILED_REQUEST_TYPE ="gstdetailed";
    public static final String TAN_AUTH_REQUEST_TYPE ="tan";
    public static final String TAN_DETAIL_REQUEST_TYPE ="tandetailed";
    public static final String COMPANY_SEARCH_REQUEST_TYPE = "compsearch";
    public static final String COMPANY_AND_LLP_CIN_REQUEST_TYPE = "cinlookup";
    public static final String COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE ="mca";
    public static final String UAN_REQUEST_TYPE ="uam";
    public static final String NREGA_REQUEST_TYPE ="mnrega";
    public static final String IEC_REQUEST_TYPE ="iec";
    public static final String IEC_DETAILED_REQUEST_TYPE ="iecdetailed";

    public static final String MCA_SIGNATORIES_REQUEST_TYPE ="mca-signatories";
    public static final String GST_SEARCH_REQUEST_TYPE ="gst-search";
    public static final String GSP_GSTIN_AUTH_REQUEST_TYPE ="gst-verification";
    public static final String GSP_GST_RETURN_FILLING_REQUEST_TYPE ="gst-return-status";
    public static final String SHOP_ESTABLISHMENT_REQUEST_TYPE ="shop";
    public static final String FSSAI_LICENSE_AUTH_REQUEST_TYPE ="fssai";
    public static final String FDA_LICENSE_AUTH_REQUEST_TYPE ="fda";
    public static final String CA_MEMBERSHIP_AUTH_REQUEST_TYPE ="icai";
    public static final String ICSI_MEMBERSHIP_REQUEST_TYPE ="icsi";
    public static final String ICWAI_MEMBERSHIP_REQUEST_TYPE ="icwaim";
    public static final String ICWAI_FIRM_REQUEST_TYPE ="icwaif";
    public static final String MCI_MEMBERSHIP_REQUEST_TYPE ="mci";
    public static final String PNG_AUTH_REQUEST_TYPE ="png";
    public static final String ELECTRICITY_BILL_AUTH_REQUEST_TYPE ="elec";
    public static final String TELEPHONE_BILL_AUTH_REQUEST_TYPE ="tele";
    public static final String MOBILE_OTP_REQUEST_TYPE ="mobile-otp";
    public static final String MOBILE_DETAILS_REQUEST_TYPE ="mobile-verify";
    public static final String LPG_ID_REQUEST_TYPE ="lpg";
    public static final String EPF_OTP_REQUEST_TYPE ="epf-get-otp";
    public static final String EPF_PASSBOOK_REQUEST_TYPE ="epf-get-passbook";
    public static final String EPF_UAN_LOOKUP_REQUEST_TYPE ="uan-lookup";
    public static final String EMPLOYER_LOOKUP_REQUEST_TYPE ="membership-lookup";
    public static final String ESIC_ID_REQUEST_TYPE ="esic";
    public static final String FORM_16_REQUEST_TYPE ="tds";
    public static final String FORM_16_QUATERLY_REQUEST_TYPE ="tdsq";
    public static final String ITR_REQUEST_TYPE ="itr";
    public static final String ADDRESS_MATCHING_REQUEST_TYPE ="address";
    public static final String EMAIL_REQUEST_TYPE ="email";
    public static final String EMAIL_VERIFICATION_REQUEST_TYPE ="email-verification";
    public static final String NAME_SIMILARITY_REQUEST_TYPE ="name";
    public static final String IFSC_CODE_REQUEST_TYPE ="ifsc";
    public static final String BANK_ACCOUNT_VERIFICATION_REQUEST_TYPE ="bankacc";
    public static final String HSN_CODE_REQUEST_TYPE ="dgft";
    public static final String WEBSITE_DOMAIN_REQUEST_TYPE ="whois";
    public static final String VEHICLE_RC_AUTH_REQUEST_TYPE ="rc";
    public static final String VEHICLE_RC_SEARCH_REQUEST_TYPE ="rcsearch";

    public static final String GST_SEARCH_BY_PAN_REQUEST_TYPE ="gst-search-by-pan";
    public static final String GST_TRANS_API_REQUEST_TYPE ="gst-trrn";
    public static final String GST_TRRN_OTP_REQUEST_TYPE ="gst-trrn-otp";
    public static final String GST_TRRN_VERIFY_OTP_REQUEST_TYPE ="gst-trrn-otp-verify";
    public static final String GST_CREDENTIAL_LINK_GENERATION_REQUEST_TYPE ="gst-trrn-credlink";
    public static final String GST_CREDENTIAL_LINK_SEND_REQUEST_TYPE ="gst-trrn-credlink-send";

    public static final String ENTITY_SEARCH_REQUEST_TYPE ="corp-search";
    public static final String DETAILED_PROFILE_REQUEST_TYPE ="corp-profile";
    public static final String GST_FILING_STATUS_REQUEST_TYPE ="filingstatus";
    public static final String GST_PROFILE_REQUEST_TYPE ="gst-profile";
    public static final String IBBI_REQUEST_TYPE ="ibbi";
    public static final String NCLT_REQUEST_TYPE ="nclt";
    public static final String BIFR_CHECK_REQUEST_TYPE ="bifr";
    public static final String DRT_CHECK_REQUEST_TYPE ="drt";
    public static final String SUIT_FILED_SEARCH_REQUEST_TYPE ="suits";
    public static final String LITIGATIONS_SEARCH_REQUEST_TYPE ="litigations-search";
    public static final String LITIGATIONS_DETAILS_DISTRICT_COURTS_REQUEST_TYPE ="distdetail";
    public static final String PAN_STATUS_CHECK = "pan-authentication";


    public static final List<String> individualKYCSeq = new ArrayList<String>() {{ add(PAN); add(VOTERID); add(DRIVINGLICENSE);}};
    public static final List<String> nonIndividualKYCSeq = new ArrayList<String>() {{ add(PAN); add(GST); add(TAN);}};
    /**
     * Status Codes
     */
    public static final String KARZA_STATUS_101="101";
    public static final String KARZA_STATUS_102="102";
    public static final String KARZA_STATUS_103="103";
    public static final String KARZA_STATUS_104="104";
    public static final String KARZA_STATUS_105="105";

    public static final Map<String, String> KARZA_STATUS_MESSAGES = new HashMap<>();

    static {

        KARZA_STATUS_MESSAGES.put(KARZA_STATUS_101,"Valid Authentication");
        KARZA_STATUS_MESSAGES.put(KARZA_STATUS_102,"Invalid ID number or combination of inputs");
        KARZA_STATUS_MESSAGES.put(KARZA_STATUS_103,"No records found for the given ID or combination of inputs");
        KARZA_STATUS_MESSAGES.put(KARZA_STATUS_104,"Max retries exceeded");
        KARZA_STATUS_MESSAGES.put(KARZA_STATUS_105,"Missing Consent");

    }

    // Karza API call Status
    public static final String VERIFIED = "verified";
    public static final String NOT_VERIFIED = "not verified";


}
