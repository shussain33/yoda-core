package com.softcell.service;


import com.softcell.constants.AuditType;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.response.ApplicationResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.workflow.AuditDataRequest;
import com.softcell.workflow.ReInitiateApplicationRequest;
import com.softcell.workflow.WorkFlowRestartConfiguration;

import java.util.List;

/**
 * @author vinodk
 *         <p/>
 *         This interface will be implemented by classes who needs to retrieve audit related details.
 */
public interface AuditDataManager {

    /**
     * @param refID         refId
     * @param institutionId id of the institution.
     * @param auditType     type of the audit data @AuditType.
     * @return @List<@ApplicationRequest>
     */
    List<ApplicationRequest> getAppRequestFromAuditData(String refID,
                                                        String institutionId, AuditType auditType) throws Exception;

    /**
     * @param auditDataRequest @AuditDataRequest request to get reappraisal application.
     * @return List of applications came for reappraisal.
     */
   BaseResponse getReappraisalApplications(AuditDataRequest auditDataRequest) throws Exception;

    /**
     * @param workFlowRestartConfiguration
     * @return
     */
    BaseResponse getApplicationData(WorkFlowRestartConfiguration workFlowRestartConfiguration) throws Exception;

    /**
     * @param auditDataRequest
     * @return
     */
    BaseResponse getBreAuditData(AuditDataRequest auditDataRequest) throws Exception;

    /**
     * @param gonogoRefId
     * @param institutionId
     * @return
     */
    ApplicationResponse checkStatus(String gonogoRefId, String institutionId) throws Exception;

    /**
     * @param reInitiateApplicationRequest
     * @return
     */
    BaseResponse reInitiateApplication(ReInitiateApplicationRequest reInitiateApplicationRequest) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    BaseResponse doReAppraiseApplication(ApplicationRequest applicationRequest) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return
     */
    BaseResponse getReAppraiseStatus(CheckApplicationStatus checkApplicationStatus) throws Exception;


    BaseResponse getQuickDataEntryStatus(AuditDataRequest auditDataRequest) throws Exception;
}
