package com.softcell.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.gonogo.model.core.CancelDoNotRaisedApplicationRequest;
import com.softcell.gonogo.model.core.salesforcedetails.SalesForceLog;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.ModifyAppStageAndStatusRequest;
import com.softcell.gonogo.model.sms.SmsServReqResLogRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * Created by yogeshb on 10/2/17.
 */
@Component
public interface AdminManager {

    /**
     * @param refID
     * @return
     */
    BaseResponse getPanLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    BaseResponse getAadharLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    BaseResponse getMbLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    BaseResponse getMbLogWithCoApplicant(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    BaseResponse getScoringLog(String refID) throws Exception;

    /**
     * @param refID
     * @return
     */
    BaseResponse getMainLog(String refID) throws Exception;


    /**
     * @param refID
     * @return
     */
    BaseResponse getSingleBucketData(String refID) throws Exception;

    /**
     * @return
     */
    BaseResponse getLogData() throws Exception;

    /**
     * @param loginRequest
     * @return GenericResponse
     */
    BaseResponse updateStageAndStatus(LoginRequest loginRequest) throws Exception;

    /**
     * @param loginRequest
     * @return
     */
    BaseResponse doCacheRefresh(LoginRequest loginRequest) throws Exception;

    /**
     * @param json
     * @return
     */
    BaseResponse getCsvFromJson(List<OtpLog> json) throws JsonProcessingException;

    /**
     * @param loginRequest
     * @return
     */
    BaseResponse updateDealerName(LoginRequest loginRequest) throws Exception;

    /**
     * @param salesForceLog
     * @return
     * @throws Exception
     */

    BaseResponse getSalesForceLog(SalesForceLog salesForceLog) throws Exception;

    /**
     * @param adminLogRequest
     * @return
     */
    BaseResponse getRawResponse(AdminLogRequest adminLogRequest);

    List<GetUserProfileCallLog> getCreditVidyaResponse(CreditVidyaLogRequest creditVidyaLogRequest);

    List<SaathiCallLog> getSaathiResponse(SaathiLogRequest saathiLogRequest);

    BaseResponse getTkycLog(TKycLogRequest tKycLogRequest);

    /**
     *
     * @param cancelDoNotRaisedApplicationRequest
     * @return
     */
    BaseResponse cancelDoNotRaisedApplication(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception;

    /**
     *
     * @param cancelDoNotRaisedApplicationRequest
     * @return
     */
    BaseResponse getDoNotRaisedApplicationCountForCancel(CancelDoNotRaisedApplicationRequest cancelDoNotRaisedApplicationRequest) throws Exception;

    BaseResponse getTcLosLog(TcLosLogRequest tcLosLogRequest);

    public BaseResponse getSmsServiceReqResLog(SmsServReqResLogRequest smsServReqResLogRequest);

    /**
     *
     * @param aadharNo
     * @param institutionId
     * @return
     */
    BaseResponse getAadharLogByAadharNoAndInstId(String aadharNo, String institutionId);

    /**
     *
     * @param stageAndStatusRequest
     * @return
     */
    BaseResponse modifyAppStageAndStatus(ModifyAppStageAndStatusRequest stageAndStatusRequest) throws Exception;

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse getApplicationStatusAndStage(String refId, String institutionId) throws Exception;

    /*
        * A File is providing decision, remarks and the refIds to be changed to
        * Here is file format
        * -------------
        * decision => Approved / Declined / Queue / OnHold/ Cancelled / Disbursed / LMS-Initiated
        * remark => This wll be populated in CRO Justification
        * refId1
        * refId2
        * ...
        * refIdn
        * -------------
        * */
    BaseResponse bulkDeclineCases(File uploadedFile, String institutionId)  throws Exception;
}
