package com.softcell.service;

import com.softcell.gonogo.model.request.core.Header;

public interface ApplicationHealthManager {

    public Object getDBStat() throws Exception;

    public Object getCollectionNames() throws Exception;

    public Object getServerStatus() throws Exception;

    public Object gethostInfo() throws Exception;

    public Object getExternalServicesStatus(Header header);

}
