package com.softcell.service;

import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ssg0268 on 9/11/19.
 */
public interface InsuranceManager {

     BaseResponse saveInsuranceData(InsuranceRequest insuranceRequest, HttpServletRequest httpRequest);

    BaseResponse getInsuranceData(InsuranceRequest insuranceRequest, HttpServletRequest httpRequest);
}
