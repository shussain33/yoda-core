package com.softcell.service;

import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.SchemeMetadataRequest;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.util.List;


public interface MetaDataStoreManager {

    /**
     *
     * @param schemeMetadataRequest
     * @return
     */
    List<SchemeMasterData> getFilteredSchemes(SchemeMetadataRequest schemeMetadataRequest);

    /**
     *
     * @param instId
     * @return
     */
    BaseResponse getBankDetails(String instId);

    /**
     *
     * @param instId
     * @param queryString
     * @return
     */
    BaseResponse getCarSurrogateMaster(String instId, String queryString);

    /**
     *
     * @param instId
     * @param queryString
     * @return
     */
    BaseResponse getEmployerMasterDetails(String instId, String queryString);

    /**
     *
     * @param instId
     * @param queryString
     * @return
     */
    BaseResponse getPinCodeDetails(String instId, String queryString);

    /**
     *
     * @param instId
     * @param category
     * @return
     */
    BaseResponse getAssetMaster(String instId, String category);

    /**
     *
     * @param request
     * @return
     */
    BaseResponse getModelVariantManufacturer(ModelVariantMasterRequest request);

    /**
     *
     * @param modelVariantMasterRequest
     * @return
     */
    BaseResponse getReferenceDetails(ModelVariantMasterRequest modelVariantMasterRequest);

    /**
     *
     * @return
     */
    BaseResponse getBankNames(String institutionId);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @return
     */
    BaseResponse getStates(String institutionId, String bank);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @return
     */
    BaseResponse getDistrict(String institutionId, String bank, String state);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @param district
     * @return
     */
    BaseResponse getBranches(String institutionId, String bank, String state, String district);

    /**
     *
     *
     * @param institutionId
     * @param bank
     * @param state
     * @param district
     * @param branch
     * @return
     */
    BaseResponse getBankMasterDetails(String institutionId, String bank, String state, String district, String branch);

    /**
     *
     *
     * @param institutionId
     * @param ifsc
     * @return
     */
    BaseResponse getBankMasterDetailsByIfscCode(String institutionId, String ifsc);

    BaseResponse getBankMasterDetailsByMicrCode(String institutionId, String micr);

    /**
     *
     * @param institutionId
     * @param bankName
     * @return
     *
     * @Depricated User GetBankNameFromBankMaster
     */
    BaseResponse getBankNamesFromLosMaster(String institutionId, String bankName);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse getAccountType(String institutionId);


    /**
     *
     * @param institutionId
     * @param bankName
     * @return
     */
    BaseResponse getBankNamesFromBankMaster(String institutionId, String bankName);

    BaseResponse getCity(String institutionId, String cityQuery);

    BaseResponse getPincodeMaster(String instId);

    BaseResponse getEmployerMaster(String instId);

    BaseResponse getCityMaster(String institutionId);
}
