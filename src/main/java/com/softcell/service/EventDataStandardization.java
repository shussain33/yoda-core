package com.softcell.service;

import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.logger.ApplicationCaseHistory;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author kishorp
 *         It will define to enrich the data points, which was log by AOP event by system.
 */
public interface EventDataStandardization {
    /**
     * @param institutionId Institution ID used for slice data by institution.
     * @param refId         This will used for application ID.
     * @param products      Product to filter the event.
     * @return Application Case history with Stage and Step Calculation.
     */
    public ApplicationCaseHistory getApplicationCaseHistory(String institutionId, String refId, Set<String> products, List<?> events);

    /**
     * @param institutionId Institution ID used for slice data by institution.
     * @param refId         This will used for application ID.
     * @return Application Case history with Stage and Step Calculation.
     */
    public ApplicationCaseHistory getApplicationCaseHistory(String institutionId, String refId, List<?> events);

    /**
     * @param applicationTrackings
     * @return
     * @throws IOException
     */
    byte[] getApplicationHistoryReport(List<ApplicationTracking> applicationTrackings)
            throws IOException;
}
