package com.softcell.service;

import com.softcell.gonogo.model.request.LOSDetailsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.reporting.request.DailyDisbursalRequest;

/**
 * Created by yogeshb on 24/5/17.
 */
public interface ExternalExposeServiceManager {

    /**
     *
     * @param lOSDetailsRequest
     * @return
     */
    BaseResponse updateOpsData(LOSDetailsRequest lOSDetailsRequest);


    byte[] getDailyDisbursal(DailyDisbursalRequest dailyDisbursalRequest) throws Exception;
}
