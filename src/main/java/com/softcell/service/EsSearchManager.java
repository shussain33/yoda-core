package com.softcell.service;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.queuemanager.search.request.ESQueueRequest;

/**
 * @author kishorp
 */
public interface EsSearchManager {
    public BaseResponse croQueue(ESQueueRequest esQueueRequest);

    public BaseResponse updateDatabaseIndex(String startDate, String endDate, String instituionId);
}
