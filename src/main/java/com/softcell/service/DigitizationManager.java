package com.softcell.service;

import com.softcell.gonogo.model.request.digitization.DigitizationApplicationReportRequest;
import com.softcell.gonogo.model.request.digitization.DownloadDigitizationFormRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * @author yogeshb
 */
public interface DigitizationManager {

    /**
     * @param institutionId
     * @param productId
     * @param refID
     * @return
     */
    public BaseResponse getApplicationForm(String institutionId, String productId,
                                           String refID) throws Exception;

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    public BaseResponse getAgreementForm(String institutionId, String productId,
                                         String refID) throws Exception;

    /**
     *
     * @param institutionId
     * @param productId
     * @param refID
     * @return
     * @throws Exception
     */
    public BaseResponse getDeliveryOrder(String institutionId, String productId,
                                         String refID ,String fileOperationType) throws Exception;

    /**
     *
     * @param downloadDigitizationFormRequest
     * @return
     */
    BaseResponse downloadDigitizationForm(DownloadDigitizationFormRequest downloadDigitizationFormRequest) throws Exception;

    /**
     *
     * @param institutionId
     * @param productId
     * @param refID
     * @return
     */
    BaseResponse downloadAchMandateForm(String institutionId, String productId,
                                        String refID)throws Exception;

    BaseResponse downloadSanctionLetter(DigitizationApplicationReportRequest request);
}
