package com.softcell.service;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;

/**
 * @author yogeshb
 */
public interface CreditCardManager {

    public BaseResponse checkCreditCard(
            CreditCardSurrogateRequest creditCardSurrogateRequest);

}
