package com.softcell.service;

import com.softcell.gonogo.model.configuration.admin.HierarchyMaster;
import com.softcell.gonogo.model.configuration.admin.WorkflowMaster;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 *  * Created by amit on 28/5/18.
 */
@Component
public interface AppConfigurationManager {

    public BaseResponse synchUsers(AdminConfigRequest adminConfigRequest, HttpServletRequest httpRequest);

    BaseResponse synchUser(SynchUserRequest synchUserRequest);

    List<LoginServiceResponse> getUsersDetailsFromUAM(String institutionId, HttpServletRequest httpRequest);

    public BaseResponse getUsersHierarchy(AdminConfigRequest adminConfigRequest);

    public BaseResponse getInstitutionConfiguration(AdminConfigRequest adminConfigRequest);

    BaseResponse getIntimationConfiguration(AdminConfigRequest adminConfigRequest);

    public BaseResponse saveProductModuleConfig(InstitutionModuleRequest institutionModuleRequest, String configType);

    public BaseResponse deleteProductModuleConfig(InstitutionModuleRequest institutionModuleRequest, String configType);

    BaseResponse saveIntimationConfig(IntimationConfigRequest intimationConfigRequest, String configType);

    BaseResponse deleteIntimationConfig(IntimationConfigRequest intimationConfigRequest, String configType);

    BaseResponse generateBranchGroups(GenerateGroupRequest generateGroupRequest, boolean isSynchCall);

    BaseResponse createWorkflow(WorkflowRequest workflowRequest);

    WorkflowMaster getActiveWorkFlow(String institutionId, String product);

    //template
    BaseResponse getTemplateDetails(String instituteId);

    BaseResponse genrateTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest);

    BaseResponse save(TemplateRequest templateRequest, HttpServletRequest httpRequest);

    BaseResponse updateTemplateInfo(TemplateRequest templateRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse createTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse deleteTemplate(TemplateRequest templateRequest, HttpServletRequest httpRequest);

    BaseResponse getAllTemplateDetails(String instituteId);

    BaseResponse intimateUser(IntimationRequest intimationReequest);

    List<HierarchyMaster> getActiveHierarchyFlow(String instituteId, String product);
}
