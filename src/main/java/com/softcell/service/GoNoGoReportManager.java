package com.softcell.service;

import com.softcell.gonogo.model.reports.request.GoNoGoReportRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface GoNoGoReportManager {

    BaseResponse downloadReportsDump(GoNoGoReportRequest goNoGoReportRequest);

    BaseResponse getAllReportsName(GoNoGoReportRequest goNoGoReportRequest);
}
