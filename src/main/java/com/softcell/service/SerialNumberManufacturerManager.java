package com.softcell.service;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface SerialNumberManufacturerManager {
    /**
     * this method will check vendor is valid or not if valid then validate its
     * serial number otherwise return "vendor not valid message" to the user.
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    public BaseResponse getVendorResponse(
            SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


}
