package com.softcell.service;

import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.DocImageFIleGetRequest;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.response.Document;
import com.softcell.gonogo.model.response.KycImages;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author yogeshb
 */
public interface DocumentStoreManager {
    /**
     * This service save image in grid database.
     *
     * @param fileUploadRequest
     * @return
     */
    BaseResponse saveImageToDB(FileUploadRequest fileUploadRequest) throws Exception;

    /**
     * @param file
     * @param fileUploadRequest
     * @return
     */
    BaseResponse saveImageWithMetadataToDB(MultipartFile file,
                                           FileUploadRequest fileUploadRequest) throws Exception;

    /**
     * This service update status of Document image.
     *
     * @param fileUploadRequest
     * @return
     */
    BaseResponse updateImageStatus(
            FileUploadRequest fileUploadRequest) throws Exception;

    /**
     * @param docImageFIleGetRequest
     * @return
     */
    byte[] getDocumentImage(DocImageFIleGetRequest docImageFIleGetRequest) throws Exception;

    /**
     * @param docImageFIleGetRequest
     * @return
     */
    KycImages getDocumentImages(
            DocImageFIleGetRequest docImageFIleGetRequest) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return
     */
    BaseResponse getDocumentImageDetails(
            CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param getImageRequest
     * @return
     */
    BaseResponse getDocumentImageByRefId(GetFileRequest getImageRequest) throws Exception;

    /**
     * This service pull Document image and convert it into base64 String.
     *
     * @param getImageRequest
     * @return
     */
    BaseResponse getDocumentBase64ImageByRefId(GetFileRequest getImageRequest) throws Exception;

    /**
     * @param getImageRequest
     * @return
     */
    BaseResponse getPdfDocumentByRefId(GetFileRequest getImageRequest) throws Exception;

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     * @throws Exception
     */
    BaseResponse getDeliveryOrderPdf(String refId, String institutionId) throws Exception;
    /**
     * This service pull cibil pdf from db and give response to client.
     *
     * @param getFileRequest
     * @return
     */
    BaseResponse getCibilPdfReport(GetFileRequest getFileRequest) throws Exception;


    /**
     * /**
     * This service pull cibil pdf from gonogocustomer and multibureaucoapplicant response
     * for applicant and if isCoapplicant flag is set true
     * it will send coapplicant pdf report too
     *
     * @param cibilPdfRequest
     * @return
     */
    List<Document> getCibilPdfReportWithCoapplicant(GetFileRequest cibilPdfRequest) throws Exception;


    /**
     * This service save image in grid database.
     *
     * @param fileUploadRequest
     * @return
     */
    BaseResponse saveImage(FileUploadRequest fileUploadRequest) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return
     */
    BaseResponse getDocumentImageDetailsByApplicant(
            CheckApplicationStatus checkApplicationStatus);

    /**
     * @param checkApplicationStatus
     * @return
     */
    boolean validateRequest(CheckApplicationStatus checkApplicationStatus);

    /**
     * This will return co-applicant cibil pdf
     *
     * @param cibilPdfRequest
     * @return
     */
    BaseResponse getCibilPdfReportForCoapplicant(GetFileRequest cibilPdfRequest);

    /**
     * @param getImageRequest
     * @return
     */
    BaseResponse sendMailToCustomer(GetFileRequest getImageRequest) throws Exception;

    /**
     *
     * @param getImageRequest
     * @return
     */
    BaseResponse sendDigitizeMailToCustomer(GetFileRequest getImageRequest) throws Exception;

    /**
     *
     * @param getFileRequest
     * @param bureau
     * @return
     */
    BaseResponse getBureauPdfReport(GetFileRequest getFileRequest, String bureau);

    BaseResponse getDocument(GetFileRequest getFileRequest);

    BaseResponse saveDocumentToDB(FileUploadRequest FileUploadRequest, MultipartFile file);

    BaseResponse getApplicationDocument(GetFileRequest getFileRequest);

    BaseResponse getLoanAppDocument(FileUploadRequest fileUploadRequest);

    BaseResponse getDocumentImageDetailsByRefId(CheckApplicationStatus checkApplicationStatus);

    BaseResponse deleteDocument(FileUploadRequest fileUploadRequest);
}
