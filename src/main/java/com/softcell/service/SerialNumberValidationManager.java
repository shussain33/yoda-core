package com.softcell.service;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.GetRollbackDetailsRequest;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorResultRequest;

/**
 * @author mahesh
 */
public interface SerialNumberValidationManager {

    /**
     * This function will validate the sales confirmation Record by sending
     * eight parameter i.e.referenceID ,serialNumber ,saleDate ,SKUCode ,vendor
     * ,salesTypes ,ProductType and PartnerCode and CEauthHeader in which
     * contain userName and password 1) web services will check Render Type if
     * it is not 'H' then it will send false response to the user. 2) web
     * services will also check serial length is less than 14 or not and it is
     * samsung serial number or not and serial number already sold out. 3) web
     * services also check model code match with serial number ,if match then
     * send response "Invalid Model Code". 4) it will also check serial number
     * already approved or not , if approved the send a approved message. if all
     * above condition are true ,it will send true response to user.
     *
     * @param serialsaleconfirmationrequest
     * @return GenericResponse in which contain the string message i.e ERROR
     * ,SUCCESS from services and Http Status.
     */
    BaseResponse serialSaleConfirmation(
            SerialSaleConfirmationRequest serialsaleconfirmationrequest) throws Exception;

    /**
     * @param serialNumberApplicableCheckRequest
     * @return return true response if serial number applicable
     */
    BaseResponse checkSerialNumberFlag(
            SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest);

    /**
     * This service will return the updated serial number details to the user
     * against that reference Id.
     *
     * @param referenceId
     * @return
     */
    BaseResponse getUpdatedSRNumberDetails(
            String referenceId);

    /**
     *
     * @param referenceId
     * @return
     */
    BaseResponse getSerialNumberDetails(String referenceId);

    /**
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateSony(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateIntex(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validatePanasonic(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateApple(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param rollbackRequest
     * @return
     */
    BaseResponse doRollback(RollbackRequest rollbackRequest) throws Exception;

    /**
     *
     * @param getRollbackDetailsRequest
     * @return
     */
    BaseResponse getRollbackServiceRequestDetails(GetRollbackDetailsRequest getRollbackDetailsRequest);


    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateGioneeImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateVideoconSansuiKelvinatorKentstar(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param disbursementRequest
     * @return
     */
    BaseResponse doDisbursement(DisbursementRequest disbursementRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateKent(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateOppoImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    BaseResponse validateGoogle(SerialSaleConfirmationRequest serialSaleConfirmationRequest) throws Exception;


    /**
     *
     * @param serialNumberApplicableVendorResultRequest
     * @return
     */
    BaseResponse saveSerialNumberApplicableResponse(SerialNumberApplicableVendorResultRequest serialNumberApplicableVendorResultRequest) throws Exception;


}

