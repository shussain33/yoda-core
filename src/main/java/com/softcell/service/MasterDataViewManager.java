package com.softcell.service;


import com.softcell.gonogo.model.request.SchemeMetadataRequest;
import com.softcell.gonogo.model.request.master.DeviationMasterRequest;
import com.softcell.gonogo.model.request.master.GetAllElecServProvRequest;
import com.softcell.gonogo.model.request.master.HierarchyMasterRequest;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.DeleteCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.InsertCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.SourcingDetailsRequest;
import com.softcell.gonogo.model.request.master.commongeneralmaster.UpdateCommonGeneralMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.GetSchemeZipReportRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.*;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.io.IOException;
import java.util.List;


/**
 * @author mahesh
 */
public interface MasterDataViewManager {

    /**
     *
     * @param
     * @return
     */
    public BaseResponse viewAssetModelMasterDetails(String institutionId,String  product,int limit,int skip);

    /**
     * This method will return all the scheme id's from scheme master
     * collection.
     *
     * @return List of scheme id's
     */
    public BaseResponse getAllSchemeIds(SchemeMasterRequest schemeMasterRequest);

    /**
     * This method will return the scheme date mapping details against schemeId.
     *
     * @param schemeDateMappingDetailsRequest
     * @return SchemeDetailsDateMapping object
     */
    public BaseResponse getSchemeDateMappingDeatils(
            GetSchemeDateMappingDetailsRequest schemeDateMappingDetailsRequest) throws Exception;

    /**
     * This method will return the pin code master against the institution Id, limit and skip.
     *
     * @param institutionId
     * @param limit
     * @param skip
     * @return pincode master List.
     */
    public BaseResponse viewPinCodeMasterDetails(String institutionId,int limit,int skip);

    /**
     * this method will return the employer master against the institution Id.
     *
     * @param institutionId
     * @return employer master list.
     */
    public BaseResponse viewEmployerMasterDetails(String institutionId,int limit,int skip);

    /**
     * This method return the Dealer Branch master list against institution id, limit and skip.
     *
     * @param institutionId
     * @param limit
     * @param skip
     * @return
     */
    public BaseResponse viewDealerBranchMasterDetails(String institutionId,String product, int limit, int skip);

    /**
     * This Method will return the DealerEmailMaster Against the Institution Id.
     *
     * @param institutionId
     * @return Dealer Email Master.
     */
    public BaseResponse viewDealerEmailMasterDetails(String institutionId,int limit,int skip);

    /**
     * This method will return List of DealerCity mapping Master against the
     * institution Id.
     *
     * @param institutionId
     * @return List of Dealer City Mapping Master.
     */
    public BaseResponse viewSchemeModelDealerCityMappingMaster(
            String institutionId,int limit,int skip);

    /**
     * This method will validate the asset cost provided by the user against the
     * model No.
     *
     * @param assetCostValidationRequest
     * @return asset cost valid or not
     */
    public BaseResponse validateAssetCost(
            AssetCostValidationRequest assetCostValidationRequest);

    /**
     * This method will update the scheme Date mapping details against schemeID.
     *
     * @param updateSchemeDetailsRequest
     * @return updateSchemeDetailsResponse ,scheme updated or not
     */
    public BaseResponse updateSchemeDateMappingDetails(
            UpdateSchemeDateMappingDetailsRequest updateSchemeDetailsRequest) throws Exception;

    /**
     * This method will return the Manufacture against the ccid(manufactureId).
     *
     * @param getSchemeInformationRequest
     * @return Set of Manufacture
     */
    public BaseResponse getManufacturerAgainstCcid(
            GetSchemeInformationRequest getSchemeInformationRequest) throws Exception;

    /**
     * This method will return the all states .
     *
     * @param institutionId
     * @return Set of States.
     */
    public BaseResponse getAllState(String institutionId) throws Exception;

    /**
     * This method will return all the City against the State.
     *
     * @param getAllCityAgainstStateRequest
     * @return
     */
    public BaseResponse getAllCityAgainstState(
            GetAllCityAgainstStateRequest getAllCityAgainstStateRequest);

    /**
     * This method will update the Scheme Dealer Mapping Details against the
     * Scheme Id.
     *
     * @param updateSchemeDealerMappingDetails
     * @return response(scheme updated or not.)
     */
    public BaseResponse updateSchemeDealerMappingDetails(
            SchemeDealerMappingDetails updateSchemeDealerMappingDetails) throws Exception;

    /**
     * This method will return the scheme dealer mapping details against the
     * Scheme Id.
     *
     * @param getSchemeDealerMappingRequest
     * @return
     */
    public BaseResponse getSchemeDealerMappingDetails(
            GetSchemeDealerMappingRequest getSchemeDealerMappingRequest) throws Exception;

    /**
     * This method will return the all asset category against the manufacturer.
     *
     * @param assetCtgrAgainstMnfcRequest
     * @return
     */
    public BaseResponse getAllAssetCategoryAgainstManfctr(
            GetAllAssetCtgrAgainstMnfcRequest assetCtgrAgainstMnfcRequest);

    /**
     * This method will return the all model no against the asset category and
     * manufacturer.
     *
     * @param getAllModelAgainstAsstCtgRequest
     * @return
     */
    public BaseResponse getAllModelAgainstAsstCtgMnfc(
            GetAllModelAgainstAsstCtgRequest getAllModelAgainstAsstCtgRequest);

    /**
     * This method will return the all dealer against the City.
     *
     * @param getAllDealerAgainstCityRequest
     * @return
     */
    public BaseResponse getAllDealerAgainstCity(
            GetAllDealerAgainstCityRequest getAllDealerAgainstCityRequest);

    /**
     * This method will return Whole filtered scheme against the scheme Id.
     *
     * @param schemeMetadataRequest
     * @return list of scheme master data.
     */
    public BaseResponse getFilteredSchemes(
            SchemeMetadataRequest schemeMetadataRequest) throws Exception;


    /**
     * This method will return the Asset Make against the manufacture and the
     * asset category.
     *
     * @param getAllAssetMakeAgainstMnfCtgRequest .
     * @return set of asset make.
     */
    public BaseResponse getAssetMakeAgainstMnfcAndCtg(
            GetAllAssetMakeAgainstMnfCtgRequest getAllAssetMakeAgainstMnfCtgRequest) throws Exception;

    /**
     * This method gives the filtered scheme i.e General Scheme ,Vanilla Scheme
     * ,All scheme , scheme against the manufacturer .filtered the scheme
     * according to information provided by user.
     *
     * @param getFilteredSchemeRequest
     * @return set of schemeInformation.
     */
    public BaseResponse getConditionalScheme(
            GetConditionalSchemeRequest getFilteredSchemeRequest);

    /**
     * @param getAllManufacturerRequest
     * @return
     */
    public BaseResponse getAllManufacturer(
            GetAllManufacturerRequest getAllManufacturerRequest);

    /**
     * @param getAllApplicableSchemeRequest
     * @return
     */
    public List<ApplicableSchemeDetails> getAllApplicableScheme(
            GetAllApplicableSchemeRequest getAllApplicableSchemeRequest);

    /**
     * @param schemeApproveDeclineRequest
     * @return
     */
    public BaseResponse approveDeclineSchemeStatus(
            SchemeApproveDeclineRequest schemeApproveDeclineRequest) throws Exception;

    /**
     * @return
     * @throws Exception
     */
    public byte [] getCsvZipReport( GetSchemeZipReportRequest getSchemeZipReportRequest) throws IOException;

    /**
     *
     * @param deleteCommonGeneralMasterRequest
     * @return
     * @throws Exception
     */
    BaseResponse deleteCommonGeneralParameterMaster(DeleteCommonGeneralMasterRequest deleteCommonGeneralMasterRequest) throws Exception;

    /**
     *
     * @param updateCommonGeneralMasterRequest
     * @return
     * @throws Exception
     */
    BaseResponse updateCommonGeneralParameterMaster(UpdateCommonGeneralMasterRequest updateCommonGeneralMasterRequest) throws Exception;

    /**
     *
     * @param insertCommonGeneralMasterRequest
     * @return
     */
    BaseResponse insertCommonGeneralParameterMaster(InsertCommonGeneralMasterRequest insertCommonGeneralMasterRequest);

    /**
     *
     * @param institutionId
     * @param limit
     * @param skip
     * @return
     */
    BaseResponse getCommonGeneralMasterRecord(String institutionId, Integer limit, Integer skip);

    /**
     *
     * @param sourcingDetailsRequest
     * @return
     */
    BaseResponse getSourcingDetails(SourcingDetailsRequest sourcingDetailsRequest);

    BaseResponse getAllElecServProviders(GetAllElecServProvRequest getAllElecServProvRequest);

    BaseResponse getAllDeviationMasters(DeviationMasterRequest deviationMasterRequest);

    BaseResponse getAllHierarchyMasters(HierarchyMasterRequest hierarchyMasterRequest);

    BaseResponse getRoiSchemeMasterData(HierarchyMasterRequest hierarchyMasterRequest);
}
