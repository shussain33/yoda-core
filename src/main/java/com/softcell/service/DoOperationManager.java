package com.softcell.service;

import com.softcell.gonogo.model.request.dooperation.DeliveryOrderOperationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by dv-stl-07 on 30/8/17.
 */
public interface DoOperationManager {

    /**
     * @param deliveryOrderOperationRequest
     * @return
     */
    BaseResponse cancelDeliveryOrder(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception;

    /**
     * @param deliveryOrderOperationRequest
     * @return
     */
    BaseResponse restoreDeliveryOrder(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception;


    /**
     * @param deliveryOrderOperationRequest
     * @return
     */
    BaseResponse getDeliveryOrderStatus(DeliveryOrderOperationRequest deliveryOrderOperationRequest) throws Exception;

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse getDoOperationLog(String refId, String institutionId);
}
