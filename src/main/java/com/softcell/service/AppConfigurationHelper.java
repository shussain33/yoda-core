package com.softcell.service;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationMongoRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.admin.*;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.masters.HierarchyUser;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMaster;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.masters.RoleWiseUsers;
import com.softcell.gonogo.model.request.GenerateGroupRequest;
import com.softcell.gonogo.model.request.IntimationConfigRequest;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.request.WorkflowRequest;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.model.security.v2.Product;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.rest.utils.EndPointReferrer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.workflow.executors.intimation.IntimationExecutor;
import io.netty.util.internal.StringUtil;

import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by amit on 31/5/18.
 */
@Component
public class AppConfigurationHelper {

    private static final Logger logger = LoggerFactory.getLogger(AppConfigurationHelper.class);

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Autowired
    AppConfigurationManager appConfigurationManager;

    @Autowired
    private LookupService lookupService;

    public List<OrganizationalHierarchyMasterV2> createHierarchyMasterData(
            Map<Integer, Map<String, Map<Branch, Map<String, List<LoginServiceResponse>>>>> resultMap, Map<String, Set<String>> productWiseRoleMap) {

        List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List = new ArrayList<>();
        OrganizationalHierarchyMasterV2 masterObj;

        for (Map.Entry<Integer, Map<String, Map<Branch, Map<String, List<LoginServiceResponse>>>>> instData : resultMap.entrySet()) {
            String institutionId = instData.getKey().toString();
            for (Map.Entry<String, Map<Branch, Map<String, List<LoginServiceResponse>>>> productData : instData.getValue().entrySet()) {
                String product = productData.getKey();
                Set<String> uniqueRoles = new HashSet<>();
                if (product != null)
                    productWiseRoleMap.put(product, uniqueRoles);
                for (Map.Entry<Branch, Map<String, List<LoginServiceResponse>>> branchData : productData.getValue().entrySet()) {
                    Branch branch = branchData.getKey();
                    masterObj = new OrganizationalHierarchyMasterV2();
                    masterObj.setInstitutionId(institutionId);
                    masterObj.setProduct(product);
                    masterObj.setChannel("Branch");
                    masterObj.setBranch(branch);

                    List<RoleWiseUsers> roleWiseUsersList = new ArrayList<>();
                    RoleWiseUsers roleWiseUsers;

                    for (Map.Entry<String, List<LoginServiceResponse>> userRoleData : branchData.getValue().entrySet()) {
                        String userRole = userRoleData.getKey();
                        uniqueRoles.add(userRole);
                        Set<HierarchyUser> usersList = new HashSet<>();
                        for (LoginServiceResponse userResponse : userRoleData.getValue()) {
                            try {
                                HierarchyUser user = createHierarchyUser(userRole, userResponse);
                                usersList.add(user);
                            }catch (Exception e){/*Handled Null pointer exception*/}
                        }
                        roleWiseUsers = new RoleWiseUsers();
                        roleWiseUsers.setRole(userRole);
                        roleWiseUsers.setUsers(new ArrayList(usersList));
                        roleWiseUsersList.add(roleWiseUsers);
                    }
                    masterObj.setRoleWiseUsersList(roleWiseUsersList);
                    hierarchyMasterV2List.add(masterObj);
                }
            }
        }
        return hierarchyMasterV2List;
    }

    private HierarchyUser createHierarchyUser(String userRole, LoginServiceResponse userResponse) {
        HierarchyUser user = new HierarchyUser();
        user.setUserId(userResponse.getLoginId());
        user.setUserName(userResponse.getFirstName() + " " + userResponse.getLastName());
        user.setRole(userRole);
        user.setEmailId(userResponse.getEmail());
        user.setContactNo(userResponse.getMobile());
        if (CollectionUtils.isNotEmpty(userResponse.getDealers())) {
            user.setDealerId(userResponse.getDealers().iterator().next().getDealerId());
            user.setDealerIds(userResponse.getDealers().stream().filter(obj -> StringUtils.equalsIgnoreCase(obj.getVerificationType(), userRole))
                    .map(obj -> obj.getDealerId()).collect(Collectors.toSet()));
        }
        if(null != userResponse.getActive()) user.setActive(userResponse.getActive());
        else user.setActive(StringUtils.equalsIgnoreCase(userResponse.getUserStatus(), "ACTIVE"));
        return user;
    }

    public Map<Integer, Map<String, Map<Branch, Map<String, List<LoginServiceResponse>>>>>
    calculateOrganisationalHierarchyForUsers(List<LoginServiceResponse> usersResponse) {
        Map<Integer, Map<String, Map<Branch, Map<String, List<LoginServiceResponse>>>>> hierarchyData = new HashMap<>();
        Map<String, Branch> branchNameObjMap = new HashMap<>();
        usersResponse.forEach(user -> {
            user.getBranches().forEach(branch -> {
                if (branch != null)
                    branchNameObjMap.put(branch.getBranchName().replaceAll("\\s+", ""), branch);
            });
        });

        for (LoginServiceResponse userData : usersResponse) {
            Integer institutionId = userData.getInstitutionId();

            if (!hierarchyData.containsKey(institutionId)) {
                hierarchyData.put(institutionId, new HashMap<>());
            }
            for (Product product : userData.getProduct()) {
                String productName = product.getProductName();
                if (!hierarchyData.get(institutionId).containsKey(productName)) {
                    hierarchyData.get(institutionId).put(productName, new HashMap<>());
                }
                if (userData.getHierarchy() != null &&
                        StringUtils.equalsIgnoreCase(userData.getHierarchy().getHierarchyLevel(), "Branch")) {
                    for (String branchName : userData.getHierarchy().getHierarchyValue()) {
                        Branch branch = branchNameObjMap.get(branchName.replaceAll("\\s+", ""));
                        if (branch != null) {
                            if (!hierarchyData.get(institutionId).get(productName).containsKey(branch)) {
                                hierarchyData.get(institutionId).get(productName).put(branch, new HashMap<>());
                            }
                            Iterator roleIterator = userData.getRoles().iterator();
                            while (roleIterator.hasNext()) {
                                String role = (String) roleIterator.next();
                                if (!hierarchyData.get(institutionId).get(productName).get(branch).containsKey(role)) {
                                    hierarchyData.get(institutionId).get(productName).get(branch).put(role, new ArrayList<>());
                                }
                                hierarchyData.get(institutionId).get(productName).get(branch).get(role).add(userData);
                            }
                        }
                    }
                }
            }
        }
        return hierarchyData;
    }

    public void addProductWiseRolesInConfig(Map<String, Set<String>> productWiseRoleMap, String institutionId) {
        InstitutionConfig institutionConfig =
                appConfigurationRepository.fetchInstitutionConfiguration(institutionId);

        if (institutionConfig == null) {
            institutionConfig = new InstitutionConfig();
            institutionConfig.setInstitutionId(institutionId);
        }
        List<String> products = (institutionConfig.getProducts() != null) ? institutionConfig.getProducts() : new ArrayList<>();
        for (Map.Entry<String, Set<String>> entry : productWiseRoleMap.entrySet()) {
            String productName = entry.getKey();
            List<String> roles = new ArrayList<>(entry.getValue());
            if (products.contains(productName)) {
                institutionConfig.getProductConfigs().forEach(productConfig -> {
                    if (StringUtils.equals(productName, productConfig.getProductName())) {
                        productConfig.setRoles(roles);
                    }
                });
            } else {
                institutionConfig.getProducts().add(productName);
                ProductConfig productConfig = new ProductConfig();
                productConfig.setProductName(productName);
                productConfig.setRoles(roles);
                List<ProductConfig> productConfigList = institutionConfig.getProductConfigs();
                if (productConfigList == null)
                    productConfigList = new ArrayList<>();
                productConfigList.add(productConfig);
                institutionConfig.setProductConfigs(productConfigList);
            }
        }
        appConfigurationRepository.save(institutionConfig);
    }

    public void filterAndUpdateModuleConfig(InstitutionConfig institutionConfig, ProductConfig productConfig, String configType) {
        String productName = productConfig.getProductName();
        if (institutionConfig.getProducts() != null && institutionConfig.getProducts().contains(productName)) {
            List<ProductConfig> productConfigList = new ArrayList<>();
            for (ProductConfig tempObj : institutionConfig.getProductConfigs()) {
                if (productName.equalsIgnoreCase(tempObj.getProductName())) {
                    if (StringUtils.equals(configType, EndPointReferrer.SAVE_MODULE_CONFIG)) {
                        tempObj.setModuleConfigList(productConfig.getModuleConfigList());
                    }
                    if (StringUtils.equals(configType, EndPointReferrer.SAVE_VALUEMASTER_CONFIG)) {
                        tempObj.setScreenValueMasterConfigs(productConfig.getScreenValueMasterConfigs());
                    }
                    if (StringUtils.equals(configType, EndPointReferrer.SAVE_STAGES_CONFIG)) {
                        tempObj.setStageConfigList(productConfig.getStageConfigList());
                    }
                    if (StringUtils.equals(configType, EndPointReferrer.SAVE_ROLE_CONFIG)) {
                        tempObj.setRoleConfigsList(productConfig.getRoleConfigsList());
                        Cache.initRoleEnabledStagMap();
                    }
                }
                productConfigList.add(tempObj);
            }
            institutionConfig.setProductConfigs(productConfigList);
        } else {
            List<String> products = (institutionConfig.getProducts() != null) ? institutionConfig.getProducts() : new ArrayList<>();
            products.add(productName);
            institutionConfig.setProducts(products);
            List<ProductConfig> productConfigList = (institutionConfig.getProductConfigs() != null) ?
                    institutionConfig.getProductConfigs() : new ArrayList<>();
            productConfigList.add(productConfig);
            institutionConfig.setProductConfigs(productConfigList);
        }
    }

    public void mapIntimationToInstitution(InstitutionConfig institutionConfig, String institutionId){
        logger.debug("started intimation-config mapping for instituteId {}", institutionId);
        List<IntimationConfiguration> intimationConfigurations = appConfigurationRepository.fetchIntimitionConfiguration(institutionId);
        if(CollectionUtils.isNotEmpty(intimationConfigurations)) {
            logger.debug("Found Intimation Configuration for {} institutionId", institutionId);
            institutionConfig.getProductConfigs().forEach(productConfig -> {
                intimationConfigurations.forEach(intimationConfiguration -> {
                    if (StringUtils.equalsIgnoreCase(productConfig.getProductName(), intimationConfiguration.getProduct()))
                        productConfig.setIntimationConfig(intimationConfiguration.getIntimationConfig());
                });
            });
        }
        logger.debug("ended intimation-config mapping for instituteId {}", institutionId);
    }


    public IntimationConfiguration filterAndUpdateIntimationConfig(IntimationConfiguration intimationConfiguration,
                                                                   IntimationConfigRequest intimationConfigRequest, String configType) {
        String productName = intimationConfigRequest.getProductName();
        if (intimationConfiguration != null) {

            IntimationConfig intimationConfig = (intimationConfiguration.getIntimationConfig() != null) ? intimationConfiguration.getIntimationConfig() :
                    new IntimationConfig();
            if (configType.equals(EndPointReferrer.SAVE_INTIMATION_CONFIG)) {
                ActionGroup actionGroup = intimationConfigRequest.getConfig();
                List<ActionGroup> actionGroupList = null;
                if (intimationConfig.getActionGroupList() != null) {
                    actionGroupList = intimationConfig.getActionGroupList().stream().filter(tempActioGroup ->
                            (tempActioGroup.getActionName() != null) &&
                                    !StringUtils.equalsIgnoreCase(tempActioGroup.getActionName(), actionGroup.getActionName()))
                            .collect(Collectors.toList());
                }
                if (actionGroupList == null) {
                    actionGroupList = new ArrayList<>();
                }
                actionGroupList.add(actionGroup);
                actionGroupList.sort(Comparator.comparing(ActionGroup::getActionName));
                intimationConfig.setActionGroupList(actionGroupList);
            } else if (configType.equals(EndPointReferrer.SAVE_INTIMATION_GROUP_CONFIG)) {
                IntimationGroup group = intimationConfigRequest.getGroup();
                if (intimationConfig.getGroups() == null) {
                    intimationConfig.setGroups(new ArrayList<>());
                    intimationConfig.getGroups().add(group);
                } else {
                    List<IntimationGroup> groups = intimationConfig.getGroups().stream().filter(grp -> !StringUtils.equals(
                            grp.getGroupName(), group.getGroupName())).collect(Collectors.toList());
                    groups.add(group);
                    groups.sort(Comparator.comparing(IntimationGroup::getGroupName));
                    intimationConfig.setGroups(groups);
                }
            }
            intimationConfiguration.setIntimationConfig(intimationConfig);

        } else {
            intimationConfiguration = new IntimationConfiguration();
            intimationConfiguration.setProduct(productName);
            intimationConfiguration.setIntimationConfig(new IntimationConfig());
            intimationConfiguration.getIntimationConfig().setActionGroupList(new ArrayList<>());
            intimationConfiguration.getIntimationConfig().getActionGroupList().add(intimationConfigRequest.getConfig());
            intimationConfiguration.getIntimationConfig().setGroups(new ArrayList<>());
            intimationConfiguration.getIntimationConfig().getGroups().add(intimationConfigRequest.getGroup());
        }
        return intimationConfiguration;
    }

    public void updateUserInOrganisationalHierarchy(String institutionId, LoginServiceResponse userResponse) {

        if (CollectionUtils.isNotEmpty(userResponse.getRoles())) {
            List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List = appConfigurationRepository
                    .fetchHierarchyMaster(institutionId);
            String userId = userResponse.getLoginId();
            removeUpdatedUserFromExistingHierarchy(userId, organizationalHierarchyMasterV2List);
            for (String userRole : userResponse.getRoles()) {
                HierarchyUser user = createHierarchyUser(userRole, userResponse);
                updateHierarchyDetailsWithUpdatedUser(user, userResponse, organizationalHierarchyMasterV2List);
                //Cache.initInstitutionWiseUserIdNameMap();
                //updateUserInIntimationGroups(institutionId, userResponse, user, organizationalHierarchyMasterV2List);
            }
            appConfigurationRepository.saveHierarchyMaster(userResponse.getInstitutionId().toString(),
                    organizationalHierarchyMasterV2List);
        }
    }

    private void updateUserInIntimationGroups(String institutionId, LoginServiceResponse userResponse, HierarchyUser user,
                                              List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List,  Map<String, LoginServiceResponse> userIdUserMap) {
        List<IntimationConfiguration> intimationConfigurations = appConfigurationRepository.fetchIntimitionConfiguration(institutionId);
        try {
            if (CollectionUtils.isNotEmpty(intimationConfigurations)) {
                intimationConfigurations.forEach(config -> {
                    if (config.getIntimationConfig() != null && config.getIntimationConfig().getGroups() != null) {
                        config.getIntimationConfig().getGroups().forEach(group -> {List<String> userIds = null;
                            if(CollectionUtils.isEmpty(group.getUsers())) {
                                List<LoginServiceResponse> users = group.getUsersList().stream().filter(tempUser -> !StringUtils.equalsIgnoreCase(
                                        tempUser.getLoginId(), userResponse.getLoginId()
                                )).collect(Collectors.toList());
                                userIds = users.stream().map(LoginServiceResponse::getLoginId).collect(Collectors.toList());
                            } else {
                                List<HierarchyUser> users = group.getUsers().stream().filter(tempUser -> !StringUtils.equalsIgnoreCase(
                                        tempUser.getUserId(), userResponse.getLoginId()
                                )).collect(Collectors.toList());
                                userIds = users.stream().map(HierarchyUser::getUserId).collect(Collectors.toList());
                            }
                            group.setUsers(null);
                            setUserReferenceInGroups(userIdUserMap, group, userIds);
                        });
                    }
                });
                if (user.isActive()) {
                    List<String> groupsToUpdate = getAllGroupNamesUserToAdd(institutionId, user, userResponse, organizationalHierarchyMasterV2List);
                    intimationConfigurations.forEach(config -> {
                        if (config.getIntimationConfig() != null && config.getIntimationConfig().getGroups() != null) {
                            config.getIntimationConfig().getGroups().forEach(group -> {
                                if (groupsToUpdate.contains(group.getGroupName())) {
                                    //group.getUsers().add(user);
                                    group.getUsersList().add(userIdUserMap.get(user.getUserId()));
                                }
                            });
                        }
                    });
                    appConfigurationRepository.save(intimationConfigurations);
                }
            }
        } catch (Exception e) {
            logger.debug("error while updating single user" + user.toString());
            //Update intimation groups on user update
            GenerateGroupRequest generateGroupRequest = new GenerateGroupRequest();
            generateGroupRequest.setHeader(new Header());
            generateGroupRequest.getHeader().setInstitutionId(institutionId);
            for (Product product : userResponse.getProduct()) {
                generateGroupRequest.setProduct(product.getProductName());
                appConfigurationManager.generateBranchGroups(generateGroupRequest, true);
            }
        }
    }

    private void setUserReferenceInGroups(Map<String, LoginServiceResponse> userIdUserMap, IntimationGroup group, List<String> userIds) {
        List<LoginServiceResponse> usersList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(userIds)) {
            for (String id : userIds) {
                usersList.add(userIdUserMap.get(id));
            }
        }
        group.setUsersList(usersList);
    }


    private List<String> getAllGroupNamesUserToAdd(String institutionId, HierarchyUser user, LoginServiceResponse userResponse,
                                                   List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List) {
        List<String> groupsToUpdate = new ArrayList<>();
        groupsToUpdate.add(user.getRole());
        // Load BranchName and Branch Object data into cache (Temporary fix to bypass cache loading on startup)
        //Cache.initBranchNameObjectMap();
        if (userResponse.getHierarchy() != null &&
                CollectionUtils.isNotEmpty(userResponse.getHierarchy().getHierarchyValue())) {
            Map<String, Branch> branchNameObjectMap = fetchBranchNameObjectMap(institutionId, userResponse);
            for (String branchName : userResponse.getHierarchy().getHierarchyValue()) {
                //Branch wise
                groupsToUpdate.add(branchName);
                //Branch_Role wise
                groupsToUpdate.add(branchName + "_" + user.getRole());
                /* if (Cache.BRANCH_NAME_OBJECT_MAP.get(institutionId) != null &&
                      Cache.BRANCH_NAME_OBJECT_MAP.get(institutionId).get(product) != null &&
                      Cache.BRANCH_NAME_OBJECT_MAP.get(institutionId).get(product).get(branchName) != null) {*/
                if (branchNameObjectMap.containsKey(branchName)) {
                    //Branch branch = Cache.BRANCH_NAME_OBJECT_MAP.get(institutionId).get(product).get(branchName);
                    Branch branch = branchNameObjectMap.get(branchName);
                    //Zone wise
                    groupsToUpdate.add(branch.getZone());
                    //Zone_Role wise
                    groupsToUpdate.add(branch.getZone() + "_" + user.getRole());
                }
            }
            if (userResponse.getProduct() != null) {
                //product wise
                for (Product product : userResponse.getProduct()) {
                    groupsToUpdate.add(product.getProductName() +
                            "(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")");
                }
            }
        }
        if (userResponse.getProduct() != null) {
            for (Product product : userResponse.getProduct()) {
                groupsToUpdate.add(product.getProductName() + "(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")");
            }
        }
        return groupsToUpdate;
    }

    private Map<String, Branch> fetchBranchNameObjectMap(String institutionId, LoginServiceResponse userResponse) {
        Map<String, Branch> branchNameObjectMap = new HashMap<>();
        List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterList =
                appConfigurationRepository.fetchHierarchyMaster(institutionId,
                        userResponse.getHierarchy().getHierarchyValue());
        organizationalHierarchyMasterList.forEach(master -> {
            Branch branch = master.getBranch();
            if (branch != null) {
                branchNameObjectMap.put(branch.getBranchName(), branch);
            }
        });
        return branchNameObjectMap;
    }

    private void updateHierarchyDetailsWithUpdatedUser(HierarchyUser user, LoginServiceResponse userResponse,
                                                       List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List) {
        List<String> productNames = new ArrayList<>();
        List<String> branchNames = new ArrayList<>();
        String userRole = user.getRole();

        for (Product product : userResponse.getProduct()) {
            productNames.add(product.getProductName());
        }
        if (userResponse.getHierarchy() != null &&
                StringUtils.equalsIgnoreCase(userResponse.getHierarchy().getHierarchyLevel(), "Branch")) {
            for (String branch : userResponse.getHierarchy().getHierarchyValue()) {
                branchNames.add(branch.replaceAll("\\s+", ""));
            }
        }

        organizationalHierarchyMasterV2List.forEach(masterRecord -> {
            if (productNames.contains(masterRecord.getProduct()) && masterRecord.getBranch().getBranchName() != null &&
                    branchNames.contains(masterRecord.getBranch().getBranchName().replaceAll("\\s+", ""))) {
                boolean newRole = true;
                for (RoleWiseUsers roleWiseUsers : masterRecord.getRoleWiseUsersList()) {
                    if (userRole.equals(roleWiseUsers.getRole())) {
                        roleWiseUsers.getUsers().add(user);
                        newRole = false;
                    }
                }
                if (newRole) {
                    RoleWiseUsers roleWiseUsers = new RoleWiseUsers();
                    roleWiseUsers.setRole(userRole);
                    List<HierarchyUser> list = new ArrayList<>();
                    list.add(user);
                    roleWiseUsers.setUsers(list);
                }
            }
        });
    }

    private void removeUpdatedUserFromExistingHierarchy(String userId, List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List) {
        for (OrganizationalHierarchyMasterV2 hierarchyMasterV2 : organizationalHierarchyMasterV2List) {
            List<RoleWiseUsers> roleWiseUsersList = hierarchyMasterV2.getRoleWiseUsersList();
            for (RoleWiseUsers roleUserGroup : roleWiseUsersList) {
                List<HierarchyUser> tempUsers = roleUserGroup.getUsers().stream()
                        .filter(user -> !StringUtils.equals(user.getUserId(), userId)).collect(Collectors.toList());
                roleUserGroup.setUsers(tempUsers);
            }
        }
    }

    public void createApplicationSpecificGroup(Set<String> groupTypeValues) {
        groupTypeValues.add(IntimationConstants.GROUP_TYPE_CUSTOMER);
        groupTypeValues.add(IntimationConstants.GROUP_TYPE_APPLICANT);
        groupTypeValues.add(IntimationConstants.GROUP_TYPE_CO_APPLICANTS);
    }

    public void createBranchWiseGroups(Set<String> groupTypeValues) {
        groupTypeValues.add(IntimationConstants.BRANCH);
    }

    public void createZoneWiseGroups(Set<String> groupTypeValues) {
        groupTypeValues.add(IntimationConstants.ZONE);
    }

    private Map<String, List<HierarchyUser>> getZoneWiseUsers(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        Map<String, List<HierarchyUser>> zoneWiseUsersMap = new HashMap<>();
        hierarchyMasterV2List.forEach(masterRecord -> {
            String zoneName = masterRecord.getBranch().getZone();
            if (!StringUtil.isNullOrEmpty(zoneName)) {
                if (!zoneWiseUsersMap.containsKey(zoneName))
                    zoneWiseUsersMap.put(zoneName, new ArrayList<>());
                masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                    zoneWiseUsersMap.get(zoneName).addAll(roleUserObj.getUsers());
                });
            }
        });
        return zoneWiseUsersMap;
    }

    public void createZoneRoleWiseGroups(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List, Set<String> groupTypeValues) {
        Map<String, Map<String, List<HierarchyUser>>> zoneWiseUsersList = getZoneRoleWiseUsers(hierarchyMasterV2List);
        zoneWiseUsersList.forEach((zone, map) -> {
            map.forEach((role, users) -> {
                groupTypeValues.add(IntimationConstants.ZONE + "_" + role);
            });
        });
    }

    private Map<String, Map<String, List<HierarchyUser>>> getZoneRoleWiseUsers(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        Map<String, Map<String, List<HierarchyUser>>> zoneRoleWiseUsersMap = new HashMap<>();
        hierarchyMasterV2List.forEach(masterRecord -> {
            String zoneName = masterRecord.getBranch().getZone();
            if (!StringUtil.isNullOrEmpty(zoneName)) {
                if (!zoneRoleWiseUsersMap.containsKey(zoneName))
                    zoneRoleWiseUsersMap.put(zoneName, new HashMap<>());
                masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                    String role = roleUserObj.getRole();
                    if (!zoneRoleWiseUsersMap.get(zoneName).containsKey(role))
                        zoneRoleWiseUsersMap.get(zoneName).put(role, new ArrayList<>());
                    zoneRoleWiseUsersMap.get(zoneName).get(role).addAll(roleUserObj.getUsers());
                });
            }
        });
        return zoneRoleWiseUsersMap;
    }

    public void createRoleWiseGroups(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List, Set<String> groupTypeValues) {
        Map<String, List<HierarchyUser>> roleWiseUsersMap = new HashMap<>();
        hierarchyMasterV2List.forEach(masterRecord -> {
            masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                String role = roleUserObj.getRole();
                if (!roleWiseUsersMap.containsKey(role)) {
                    roleWiseUsersMap.put(role, new ArrayList<>());
                }
                roleWiseUsersMap.get(role).addAll(roleUserObj.getUsers());
            });
        });
        roleWiseUsersMap.forEach((role, usersList) -> {
            groupTypeValues.add(role);
        });
    }

    private List<String> filterAndCalculateUniqueUsers(IntimationGroup intimationGroup, List<HierarchyUser> usersList) {
        List<String> userIds = new ArrayList<>();
        List<HierarchyUser> users = new ArrayList<>();
        usersList.forEach(user -> {
            if (!userIds.contains(user.getUserId())) {
                users.add(user);
                userIds.add(user.getUserId());
            }
        });
        Set<String> finalUserIds = new HashSet<>();
        List<String> finalUserIdList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(intimationGroup.getUsers())) {
            intimationGroup.getUsers().forEach(user -> {
                if (user.getUserId() == null) {
                    users.add(user);
                }
            });
        }
        if(CollectionUtils.isNotEmpty(users))
            finalUserIdList.addAll(users.stream().map(HierarchyUser::getUserId).collect(Collectors.toList()));
        //else {
        if(CollectionUtils.isNotEmpty(intimationGroup.getUsersList())){
            List<LoginServiceResponse> list = new ArrayList<>();
            intimationGroup.getUsersList().forEach(user -> {
                if (null != user && user.getLoginId() == null) {
                    list.add(user);
                }
            });
            finalUserIdList.addAll(list.stream().map(LoginServiceResponse::getLoginId).collect(Collectors.toList()));
        }
        finalUserIdList.forEach(finalUser -> {
            if(StringUtils.isNotEmpty(finalUser))
                finalUserIds.add(finalUser);
        });
        return new ArrayList<>(finalUserIds);
    }

    public void createBranchRoleWiseGroups(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List, Set<String> groupTypeValues) {
        hierarchyMasterV2List.forEach(masterRecord -> {
            masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                groupTypeValues.add(IntimationConstants.BRANCH + "_" + roleUserObj.getRole());
            });
        });
    }

    public void createProductWiseGroups(String product, Set<String> groupTypeValues) {
        groupTypeValues.add(product + "(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")");
    }

    public void createRegionRoleWiseGroups(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List, Set<String> groupTypeValues) {
        Map<String, Map<String, List<HierarchyUser>>> regionWiseUsersList = getRegionRoleWiseUsers(hierarchyMasterV2List);
        regionWiseUsersList.forEach((region, map) -> {
            map.forEach((role, users) -> {
                groupTypeValues.add(IntimationConstants.REGION + "_" + role);
            });
        });
    }

    private Map<String, Map<String, List<HierarchyUser>>> getRegionRoleWiseUsers(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        Map<String, Map<String, List<HierarchyUser>>> regionRoleWiseUsersMap = new HashMap<>();
        hierarchyMasterV2List.forEach(masterRecord -> {
            String regionName = masterRecord.getBranch().getRegion();
            if (!StringUtil.isNullOrEmpty(regionName)) {
                if (!regionRoleWiseUsersMap.containsKey(regionName))
                    regionRoleWiseUsersMap.put(regionName, new HashMap<>());
                masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                    String role = roleUserObj.getRole();
                    if (!regionRoleWiseUsersMap.get(regionName).containsKey(role))
                        regionRoleWiseUsersMap.get(regionName).put(role, new ArrayList<>());
                    regionRoleWiseUsersMap.get(regionName).get(role).addAll(roleUserObj.getUsers());
                });
            }
        });
        return regionRoleWiseUsersMap;
    }

    public void createLocationRoleWiseGroups(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List, Set<String> groupTypeValues) {
        Map<String, Map<String, List<HierarchyUser>>> locationWiseUsersList = getLocationRoleWiseUsers(hierarchyMasterV2List);
        locationWiseUsersList.forEach((location, map) -> {
            map.forEach((role, users) -> {
                groupTypeValues.add(IntimationConstants.LOCATION + "_" + role);
            });
        });
    }

    private Map<String, Map<String, List<HierarchyUser>>> getLocationRoleWiseUsers(List<OrganizationalHierarchyMasterV2> hierarchyMasterV2List) {
        Map<String, Map<String, List<HierarchyUser>>> locationRoleWiseUsersMap = new HashMap<>();
        hierarchyMasterV2List.forEach(masterRecord -> {
            String location = masterRecord.getBranch().getLocation();
            if (!StringUtil.isNullOrEmpty(location)) {
                if (!locationRoleWiseUsersMap.containsKey(location))
                    locationRoleWiseUsersMap.put(location, new HashMap<>());
                masterRecord.getRoleWiseUsersList().forEach(roleUserObj -> {
                    String role = roleUserObj.getRole();
                    if (!locationRoleWiseUsersMap.get(location).containsKey(role))
                        locationRoleWiseUsersMap.get(location).put(role, new ArrayList<>());
                    locationRoleWiseUsersMap.get(location).get(role).addAll(roleUserObj.getUsers());
                });
            }
        });
        return locationRoleWiseUsersMap;
    }

    public InstitutionConfig createOrUpdateWorkflowRequest(WorkflowRequest workflowRequest,
                                                           InstitutionConfig institutionConfig) {
        String institutionId = workflowRequest.getHeader().getInstitutionId();
        String productName = workflowRequest.getHeader().getProduct().name();
        WorkflowMaster workflowMaster = workflowRequest.getWorkflowMaster();

        if (institutionConfig == null) {
            institutionConfig = new InstitutionConfig();
            institutionConfig.setInstitutionId(institutionId);
            institutionConfig.setProducts(new ArrayList<>());
            institutionConfig.setProductConfigs(new ArrayList<>());
        }
        if (!institutionConfig.getProducts().contains(productName)) {
            institutionConfig.getProducts().add(productName);
            ProductConfig productConfig = new ProductConfig();
            productConfig.setProductName(productName);
            productConfig.setWorkflowMasterList(new ArrayList<>());
            productConfig.getWorkflowMasterList().add(workflowMaster);
            institutionConfig.getProductConfigs().add(productConfig);
        } else {
            for (ProductConfig productConfig : institutionConfig.getProductConfigs()) {
                if (StringUtils.equalsIgnoreCase(productName, productConfig.getProductName())) {
                    if (CollectionUtils.isEmpty(productConfig.getWorkflowMasterList())) {
                        productConfig.setWorkflowMasterList(new ArrayList<>());
                        productConfig.getWorkflowMasterList().add(workflowMaster);
                    } else {
                        List<WorkflowMaster> list = productConfig.getWorkflowMasterList().stream().
                                filter(master -> !StringUtils.equals(master.getWorkflowName(),
                                        workflowMaster.getWorkflowName())).collect(Collectors.toList());
                        list.add(workflowMaster);
                        productConfig.setWorkflowMasterList(list);
                    }
                }
            }
        }
        return institutionConfig;
    }

    public WorkflowNode getWorkflowNode(String nodeValue, String nodeType, String event, WorkflowMaster workflowMaster) {
        if (ConfigurationConstants.WF_NODE_TYPE_SCREEN.equals(nodeType)) {
            event = ConfigurationConstants.WF_EVENT_SAVE;
        } else if (ConfigurationConstants.WF_NODE_TYPE_STAGE.equals(nodeType)) {
            event = ConfigurationConstants.WF_EVENT_SUBMIT;
        }
        WorkflowNode node = null;
        node = getWorkflowNode(nodeValue, nodeType, event, node, workflowMaster.getNodes());
        return node;
    }

    private WorkflowNode getWorkflowNode(String nodeValue, String nodeType, String event, WorkflowNode node, List<WorkflowNode> nodeList) {
        for (WorkflowNode workflowNode : nodeList) {
            if (StringUtils.equalsIgnoreCase(event, workflowNode.getEvent())
                    && ConfigurationConstants.WF_NODE_TYPE_STAGE.equals(nodeType)
                    && (workflowNode.getCurrentStages() != null && workflowNode.getCurrentStages().contains(nodeValue))) {
                node = workflowNode;
                return node;
            } else if (StringUtils.equalsIgnoreCase(event, workflowNode.getEvent())
                    && ConfigurationConstants.WF_NODE_TYPE_SCREEN.equals(nodeType)
                    && StringUtils.equals(nodeValue, workflowNode.getValue())) {
                node = workflowNode;
                return node;
            } else {
                if (workflowNode.getSubWorkflow() != null) {
                    node = getWorkflowNode(nodeValue, nodeType, event, node, workflowNode.getSubWorkflow().getNodes());
                    if (node != null)
                        return node;
                }
            }
        }
        return node;
    }

    public String getNextStage(WorkflowNode node, String currentStage, String status, String role, String flowType) {
        String stage = null;
        if (CollectionUtils.isEmpty(node.getNodeCondition())) {
            stage = node.getNextStage();
        } else {
            boolean result = false;
            for (NodeCondition condition : node.getNodeCondition()) {
                result = evaluateExpressions(condition.getConditionNodeList(), currentStage, status, role, flowType);
                if (result) {
                    stage = condition.getTrueValue();
                    break;
                } else {
                    if (condition.getFalseValue() != null)
                        stage = condition.getFalseValue();
                }
            }
        }
        return stage;
    }

    private boolean evaluateExpressions(List<ConditionNode> conditionExpression, String currentStage, String status, String role,
                                        String flowType) {
        boolean result = true;
        String operator = null;
        for (ConditionNode entry : conditionExpression) {
            if (StringUtils.equals(ConfigurationConstants.TYPE_COND, entry.getType())) {
                result = isaBoolean(currentStage, status, role, result, entry, operator, flowType);
            } else if (StringUtils.equals(ConfigurationConstants.TYPE_EXP, entry.getType())) {
                operator = (String) entry.getOperator();
            }
        }
        return result;
    }

    private boolean isaBoolean(String currentStage, String status, String role, boolean result, ConditionNode conditionNode,
                               String operator, String flowType) {
        if (operator == null) {
            return result && checkCond(conditionNode, currentStage, status, role, flowType);
        } else if (StringUtils.equals(operator, ConfigurationConstants.ConditionExpression.AND.name())) {
            return result && checkCond(conditionNode, currentStage, status, role, flowType);
        } else {
            return result || checkCond(conditionNode, currentStage, status, role, flowType);
        }
    }

    private boolean checkCond(ConditionNode conditionNode, String stage, String status, String role, String flowType) {
        boolean flag = false;
        String appConditionKey = conditionNode.getAppConditionKey();
        String condOperator = conditionNode.getOperator();
        String condValue = conditionNode.getCondValue();

        if (StringUtils.equalsIgnoreCase(appConditionKey, ConfigurationConstants.COND_TYPE_ROLE)) {
            flag = StringUtils.equalsIgnoreCase(condValue, role);
        } else if (StringUtils.equalsIgnoreCase(appConditionKey, ConfigurationConstants.COND_TYPE_STAGE)) {
            flag = StringUtils.equalsIgnoreCase(condValue, stage);
        } else if (StringUtils.equalsIgnoreCase(appConditionKey, ConfigurationConstants.COND_TYPE_STATUS)) {
            flag = StringUtils.equalsIgnoreCase(condValue, status);
        } else if (StringUtils.equalsIgnoreCase(appConditionKey, ConfigurationConstants.COND_TYPE_FLOW_TYPE)) {
            flag = StringUtils.equalsIgnoreCase(condValue, flowType);
        }
        if ((StringUtils.equalsIgnoreCase(condOperator, ConfigurationConstants.ConditionExpression.equals.name()))) {
            return flag;
        } else {
            return !flag;
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void intimateUsers(String refId, String action) {
        executeIntimation(refId, action, new ArrayList<>(), null, null, null);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void intimateUsers(String refId, String action, String verificationType,
                              Map<String, Set<String>> verificationStatusDealersMap) {
        logger.debug("inside intimateUsers(): 1 for refId: {}",refId);
        GoNoGoCustomerApplication goNoGoCustomerApplication = (GoNoGoCustomerApplication)
                appConfigurationRepository.fetch(GoNoGoCustomerApplication.class, refId);
        String agencyName = "";
        Set<String> dealerIds = null;
        if (CollectionUtils.isNotEmpty(verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED))) {
            logger.debug("inside intimateUsers(): 2");
            dealerIds = new HashSet<>();
            for (String str : verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED)) {
                String[] splitData = str.split("##");
                dealerIds.add(splitData[0]);
                if (splitData.length > 1)
                    agencyName = splitData[1];
            }
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, dealerIds);
        }
        if (CollectionUtils.isNotEmpty(verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED))) {
            logger.debug("inside intimateUsers(): 3");
            dealerIds = new HashSet<>();
            for (String str : verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED)) {
                String[] splitData = str.split("##");
                dealerIds.add(splitData[0]);
                if (splitData.length > 1)
                    agencyName = splitData[1];
            }
            verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, dealerIds);
        }

        List<HierarchyUser> otherUsers = getActionBasesUsers(refId, goNoGoCustomerApplication.getApplicationRequest().getHeader(),
                action, verificationType, verificationStatusDealersMap, goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2());
        executeIntimation(refId, action, otherUsers, goNoGoCustomerApplication, verificationType, agencyName);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void intimateUsers(String refId, String action, List<String> userIds) {
        GoNoGoCustomerApplication goNoGoCustomerApplication = (GoNoGoCustomerApplication)
                appConfigurationRepository.fetch(GoNoGoCustomerApplication.class, refId);
        List<HierarchyUser> otherUsers = getOtherUsersBasedOnIds(userIds, goNoGoCustomerApplication.getApplicationRequest().getHeader());
        executeIntimation(refId, action, otherUsers, goNoGoCustomerApplication, null, null);
    }

    private List<HierarchyUser> getOtherUsersBasedOnIds(List<String> userIds, Header header) {
        List<OrganizationalHierarchyMasterV2> list = appConfigurationRepository.fetchHierarchyMaster(
                header.getInstitutionId(), header.getProduct().name(), null);
        List<HierarchyUser> users = new ArrayList<>();
        for (OrganizationalHierarchyMasterV2 masterObj : list) {
            for (RoleWiseUsers roleUsers : masterObj.getRoleWiseUsersList()) {
                for (HierarchyUser user : roleUsers.getUsers()) {
                    if (userIds.contains(user.getUserId()) && !users.contains(user))
                        users.add(user);
                }
            }
        }
        return users;
    }

    private void executeIntimation(String refId, String action, List<HierarchyUser> otherUsers,
                                   GoNoGoCustomerApplication goNoGoCustomerApplication, String verificationType, String agencyName) {
        if (goNoGoCustomerApplication == null)
            goNoGoCustomerApplication = (GoNoGoCustomerApplication) appConfigurationRepository.fetch(GoNoGoCustomerApplication.class, refId);
        //Cache.initInstitutionProductWiseIntimationMap();
        /*
            Removed cached code
            Map<String, IntimationConfig> productIntimationConfigMap = Cache.INSTITUTION_PRODUCT_WISE_INTIMATION_MAP.get(
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
            if (productIntimationConfigMap != null && !productIntimationConfigMap.isEmpty()) {
                IntimationConfig intimationConfig = productIntimationConfigMap.get(
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().name());
        */
        logger.debug("inside executeIntimation(): 1");
        String productName = goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().name();
        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        if (lookupService.checkActionsAccess( institutionId
                , goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId()
                , ActionName.INTIMATION_SERVICE)) {
            logger.debug("inside executeIntimation(): 2");
            IntimationConfiguration intimationConfiguration = appConfigurationRepository.fetchIntimitionConfiguration(institutionId, productName);
            if (intimationConfiguration != null) {
                IntimationConfig intimationConfig = intimationConfiguration.getIntimationConfig();
                if (intimationConfig != null) {
                    IntimationExecutor executor = new IntimationExecutor();
                    executor.process(goNoGoCustomerApplication, intimationConfig, action, otherUsers, verificationType, agencyName);
                    executor.start();
                }
            }
        }
    }

    private List<HierarchyUser> getActionBasesUsers(String refId, Header header, String action, String verificationType,
                                                    Map<String, Set<String>> verificationStatusDealersMap, Branch branchV2) {
        if (StringUtils.equalsIgnoreCase(action, IntimationConstants.VERIFICATION_STATUS_VERIFIED))
            return new ArrayList<>();

        Set<String> delearIds = new HashSet<>();
        List<HierarchyUser> users = new ArrayList<>();
        String branchCode = branchV2.getBranchId().toString();

        delearIds.addAll(verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_TRIGGERED));
        delearIds.addAll(verificationStatusDealersMap.get(IntimationConstants.VERIFICATION_STATUS_VERIFIED));
        logger.debug("DealerIds found {}", delearIds.toString());
        if (CollectionUtils.isNotEmpty(delearIds)) {
            String role = null;
            switch (verificationType) {
                case IntimationConstants.VER_TYPE_VALUATION:
                    role = Roles.Role.PROPERTY_VERIFIER.name();
                    break;
                case IntimationConstants.VER_TYPE_LEGAL:
                    role = Roles.Role.LEGAL_VERIFIER.name();
                    break;
                case EndPointReferrer.OFFICE_VERIFICATION:
                    role = Roles.Role.OFC_VERIFIER.name();
                    break;
                case EndPointReferrer.RESIDENCE_VERIFICATION:
                    role = Roles.Role.RESI_VERIFIER.name();
                    break;
                case EndPointReferrer.BANK_VERIFICATION:
                    role = Roles.Role.BANK_VERIFIER.name();
                    break;
                case EndPointReferrer.ITR_VERIFICATION:
                    role = Roles.Role.ITR_VERIFIER.name();
                    break;
            }
            if (role != null) {
                List<OrganizationalHierarchyMasterV2> list = appConfigurationRepository.fetchHierarchyMaster(
                        header.getInstitutionId(), header.getProduct().name(), role);
                for (OrganizationalHierarchyMasterV2 masterObj : list) {
                    if (StringUtils.equalsIgnoreCase(branchCode, masterObj.getBranch().getBranchId().toString())) {
                        for (RoleWiseUsers roleUsers : masterObj.getRoleWiseUsersList()) {
                            if (StringUtils.equalsIgnoreCase(roleUsers.getRole(), role)) {
                                roleUsers.getUsers().forEach(user -> {
                                    if(CollectionUtils.isNotEmpty(user.getDealerIds())) {
                                        delearIds.forEach(dealerId -> {
                                            if (user.getDealerIds().contains(dealerId))
                                                users.add(user);
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
            }
            logger.debug("Total Users found {} for role {} aginst these dealerIds {}", users.size(), role, delearIds.toString());
            return users;
        }
        return null;
    }

    public TemplateDetails generateTemplateUsingApplicationData(String institutionId, String productName, String templateKey){
        if (appConfigurationRepository == null) {
            appConfigurationRepository = new AppConfigurationMongoRepository();
        }
        return appConfigurationRepository.findTemplateDetailsByInstituteIdAndTemplateKey( institutionId, productName, templateKey );
    }

    public TemplateDetails generateTemplateUsingApplicationData(String refId, String institutionId,
                                                                String productName, String templateName, String loggedInUserRole, String verificationType, String agencyName) {
        Map<String, String> clientInfo = null;
        if (appConfigurationRepository == null) {
            appConfigurationRepository = new AppConfigurationMongoRepository();
        }
        TemplateDetails templateDetails = appConfigurationRepository.findTemplateDetailsByInstituteIdAndTemplateName(
                institutionId, productName, templateName);
        if (templateDetails != null) {
            // create pattern to find application fields for template content
            Pattern pattern = Pattern.compile("\\{\\{([^}]+)\\}\\}");
            String content = templateDetails.getTemplateContent();
            Matcher matcher = pattern.matcher(content);

            // Get application fields configured in template
            List<String> fields = new ArrayList<>();
            while (matcher.find()) {
                String key = matcher.group(1);
                fields.add(key);
            }
            //get application data by fields
            try {
                clientInfo = appConfigurationRepository.getTemplateConfigrationData(refId, fields, institutionId, verificationType);
            } catch (Exception e) {
                logger.error("{}",e.getStackTrace());
            }
            //Replace application values in template
            if (clientInfo != null) {
                logger.debug(clientInfo.toString());
                if (StringUtils.isNotEmpty(agencyName))
                    clientInfo.put(TemplateConstants.VERIFICATION_AGENCY, agencyName);
                for (Map.Entry<String, String> tempMap : clientInfo.entrySet()) {
                    content = content.replaceAll(tempMap.getKey(), (StringUtils.isNotEmpty(tempMap.getValue()) ?
                            tempMap.getValue() : tempMap.getKey()));
                }
                content = content.replaceAll("\\{\\{", " ");
                content = content.replaceAll("\\}\\}", " ");
                content = content.replaceAll("&nbsp;", "");
                content = content.replaceAll("\\<[^>]*>", "");
                templateDetails.setTemplateContent(content);
            }
            templateDetails.setConfigDetailsList(clientInfo);
        }
        return templateDetails;
    }

    public String setValueForNull(String value) {
        return value == null ? Constant.BLANK : value;
    }

    public String getCompleteAddress(String addressLine1, String addressLine2, String landMark) {
        if (addressLine1 == null) {
            addressLine1 = "";
        } else if (addressLine2 == null) {
            addressLine2 = "";
        } else if (landMark == null) {
            landMark = "";
        }
        return addressLine1 + " " + addressLine2 + " " + landMark;
    }


    public List<String> getProductActions() {
        List<String> actions = new ArrayList<>();
        actions.add(IntimationConstants.REGISTRATION);
        actions.add(IntimationConstants.DEMOGRAPHIC_DETAILS);
        actions.add(IntimationConstants.MCP_DETAILS);
        actions.add(IntimationConstants.IMD_DETAILS);
        actions.add(IntimationConstants.COLLATERALS_DETAILS);
        actions.add(IntimationConstants.PERSONAL_DETAILS);
        actions.add(IntimationConstants.SALES_SUBMIT);
        actions.add(IntimationConstants.CPA_SUBMIT);
        actions.add(IntimationConstants.CREDIT_SUBMIT);
        actions.add(IntimationConstants.BOPS_SUBMIT);
        actions.add(IntimationConstants.HOPS_SUBMIT);
        actions.add(IntimationConstants.CREDIT_DECISION);
        actions.add(IntimationConstants.VERIFICATION_TRIGGER);
        actions.add(IntimationConstants.VERIFICATION_DONE);
        actions.add(IntimationConstants.CASE_RECOMMENDATION);
        actions.add(IntimationConstants.PROPERTY_VALUATION);
        actions.sort(String::compareTo);
        return actions;
    }

    public String getActionBasesOnRole(String institutionId, String product, String role, String stage, boolean isCredit) {
        // Write dynamic role wise action picking code
        if (role.equals(Roles.Role.FOS.name())) {
            return IntimationConstants.SALES_SUBMIT;
        } else if (role.equals(Roles.Role.CPA.name())) {
            return IntimationConstants.CPA_SUBMIT;
        } else if (role.equals(Roles.Role.CREDIT.name()) || role.equals(Roles.Role.RCM.name()) || role.equals(Roles.Role.ZCM.name())
                || role.equals(Roles.Role.NCM.name()) || role.equals(Roles.Role.CRO.name()) || isCredit) {
            return IntimationConstants.CREDIT_SUBMIT;
        } else if (role.equals(Roles.Role.BOPS.name())) {
            return IntimationConstants.BOPS_SUBMIT;
        } else if (role.equals(Roles.Role.HOPS.name())) {
            return IntimationConstants.HOPS_SUBMIT;
        } else {
            return "SUBMIT_" + stage.toUpperCase();
        }
    }


    public WorkflowMaster getActiveWorkFlow(String institutionId, String product) {
        InstitutionConfig institutionConfig =
                appConfigurationRepository.fetchInstitutionConfiguration(institutionId);

        WorkflowMaster workflowMaster = null;
        if (institutionConfig != null && CollectionUtils.isNotEmpty(institutionConfig.getProducts())) {
            for (ProductConfig productConfig : institutionConfig.getProductConfigs()) {
                if (StringUtils.equals(productConfig.getProductName(), product)) {
                    if (CollectionUtils.isNotEmpty(productConfig.getWorkflowMasterList())) {
                        for (WorkflowMaster workflow : productConfig.getWorkflowMasterList()) {
                            if (workflow.isActive()) {
                                workflowMaster = workflow;
                                break;
                            }
                        }
                        ;
                    }
                }
            }
            ;
        }
        return workflowMaster;
    }

    public String getFirstStageName(String institutionId, String product) {
        WorkflowMaster workflowMaster = getActiveWorkFlow(institutionId,
                product);
        String stageName = null;
        if (workflowMaster != null) {
            for (WorkflowNode node : workflowMaster.getNodes()) {
                if (StringUtils.equals(node.getNodeId(), ConfigurationConstants.WF_START_NODE)) {
                    stageName = node.getValue();
                }
            }
            if (stageName == null) {
                for (WorkflowNode node : workflowMaster.getNodes()) {
                    if (StringUtils.equals(node.getNodeId(), "Node1")) {
                        stageName = node.getValue();
                    }
                }
            }
        }
        return stageName == null ? "DE" : stageName;
    }

    public List<HierarchyMaster> getActiveHierarchyFlow(String instituteId, String product) {
        InstitutionConfig institutionConfig =
                appConfigurationRepository.fetchInstitutionConfiguration(instituteId);

        List<HierarchyMaster> hierarchyMaster = new ArrayList<>();
        if (institutionConfig != null && CollectionUtils.isNotEmpty(institutionConfig.getProducts())) {
            for (ProductConfig productConfig : institutionConfig.getProductConfigs()) {
                if (StringUtils.equals(productConfig.getProductName(), product)) {
                    if (CollectionUtils.isNotEmpty(productConfig.getHierarchyMasterList())) {
                        for (HierarchyMaster hierarchyFlow : productConfig.getHierarchyMasterList()) {
                            if (hierarchyFlow.isActive()) {
                                hierarchyMaster.add(hierarchyFlow);
                            }
                        }
                        ;
                    }
                }
            }
            ;
        }
        return hierarchyMaster;
    }


    public String getCompleteAddress(CustomerAddress customerAddress) {
        String address = null;
        if(customerAddress.getLandMark() != null) {
            address = customerAddress.getAddressLine1().
                    concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getAddressLine2())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getLandMark())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getCity())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getState())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(Long.toString(customerAddress.getPin()));
        }else{
            address = customerAddress.getAddressLine1().
                    concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getAddressLine2())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getCity())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(customerAddress.getState())
                    .concat(FieldSeparator.SPACE_STR)
                    .concat(Long.toString(customerAddress.getPin()));
        }
        return address;
    }

    public static String getRequestStringValue(Object obj, String key, Class<?> cls){
        Object valueObj = null;
        Class<?> innerClass = null;
        String value = null;
        ParameterizedType pType = null;
        String[] tokens = key.split("\\."); //applicantComponentResponseList
        try {
            innerClass = cls;
            valueObj = obj;
            for(int i=0; i < tokens.length; i++){
                String token = tokens[i];
                java.lang.reflect.Field field = innerClass.getDeclaredField(token);
                field.setAccessible(true);
                Type type = field.getGenericType();
                if(type instanceof ParameterizedType)
                {
                    pType = (ParameterizedType) type;
                    innerClass = (Class<?>) pType.getActualTypeArguments()[0];
                    valueObj = field.get(valueObj);
                    if(valueObj instanceof List) {
                        List<?> list = (List<?>) valueObj;
                        if (list.size() > 0) {
                            value = getFieldValues(i, tokens, innerClass, list);
                            return value;
                        }
                    }
                }
                else{
                    valueObj = field.get(valueObj);
                    innerClass = field.getType();
                }
            }
        } catch (Exception e) {
            return value = "";
        }
        if(valueObj == null){
            valueObj = (Object)"";
        }
        return valueObj.toString();
    }

    public static String getFieldType(String key, Class<?> cls){
        Class<?> innerClass = null;
        String[] tokens = key.split("\\.");
        try {
            innerClass = cls;
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                java.lang.reflect.Field field = innerClass.getDeclaredField(token);
                field.setAccessible(true);
                innerClass = field.getType();
            }
        }catch(Exception e){
            return "default";
        }
        if(innerClass.isPrimitive())
            return innerClass.toString();
        else
            return "default";
    }

    private static String getFieldValues(int index, String[] tokens, Class<?> iClass, List<?> list) {
        String value = null;
        Object classObj = null;
        Class<?> innerClass = null;
        try {
            for (int i = 0; i < list.size(); i++) {
                classObj = list.get(i);
                innerClass = iClass;
                for (int j = index+1 ; j < tokens.length; j++) {
                    String token = tokens[j];
                    java.lang.reflect.Field field = innerClass.getDeclaredField(token);
                    field.setAccessible(true);
                    Type type = field.getGenericType();
                    if(type instanceof ParameterizedType){
                        ParameterizedType pType = (ParameterizedType) type;
                        innerClass = (Class<?>) pType.getActualTypeArguments()[0];
                        classObj = field.get(classObj);
                        if(classObj instanceof List){
                            List<?> ilist = (List<?>) classObj;
                            if(ilist.size() > 0){
                                value = getFieldValues(j,tokens,innerClass,ilist);
                                return value;
                            }
                        }
                    }else {
                        classObj = field.get(classObj);
                        innerClass = field.getType();
                    }
                }
                if(value == null){
                    value = classObj.toString();
                }else{
                    value = value + "^" +classObj.toString();
                }
            }
        }catch (Exception e) {
            return value;
        }
        return value;
    }


}
