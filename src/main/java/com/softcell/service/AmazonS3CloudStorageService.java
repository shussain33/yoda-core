package com.softcell.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse.Status;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadInputStreamRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * This class helps to upload the objects and files to amazon S3 cloud by giving
 * the required credential details like accessKey and secretKey.
 *
 * @author vinodk
 */
public class AmazonS3CloudStorageService {

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3CloudStorageService.class);

    private String accessKey;
    private String secretKey;


    /**
     * @param accessKey accessKey of AWS account.
     * @param secretKey secret key of AWS account.
     */
    public AmazonS3CloudStorageService(String accessKey, String secretKey) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    /**
     * @return the accessKey
     */
    public String getAccessKey() {
        return accessKey;
    }

    /**
     * @return the secretKey
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * This method stores the file into given AWS S3 bucket.
     *
     * @param bucketName     AWS S3 bucket name in which file will be stored
     * @param srcFilePath    path of the file which is to be stored on AWS
     * @param targetFileName name which will be given to the file which is getting stored
     *                       on AWS S3.
     */
    public AmazonS3UploadFileResponse uploadFile(String bucketName,
                                                 String srcFilePath, String targetFileName) {

        AmazonS3UploadFileResponse response = null;
        File file = null;
        boolean errorPresent = false;
        if (StringUtils.isBlank(srcFilePath)) {
            response = new AmazonS3UploadFileResponse();
            /*
             * If file path is null or empty set the status to invalid file path
			 * and return the response.
			 */
            response.setStatus(Status.INVALID_FILE_PATH);
            return response;
        }

        if (StringUtils.isBlank(targetFileName)) {
            response = new AmazonS3UploadFileResponse();
			/*
			 * If target file name is null or empty set the status to invalid
			 * target name and return the response.
			 */
            response.setStatus(Status.INVALID_TARGET_FILE_NAME);
            return response;
        }

        if (StringUtils.isBlank(bucketName)) {
            response = new AmazonS3UploadFileResponse();
			/*
			 * If bucket name id empty or null set the status to invalid bucket
			 * name and return the response.
			 */
            response.setStatus(Status.INVALID_BUCKET_NAME);
            return response;
        }
		/*
		 * Create file object from the given file path.
		 */
        file = new File(srcFilePath);
		/*
		 * Upload file to amazon S3.
		 */
        return uploadFileToS3(bucketName, file, targetFileName);
    }

    /**
     * @param bucketName     AWS S3 bucket name in which file will be stored
     * @param file           file which will be stored to AWS S3.
     * @param targetFileName name which will be given to the file which is getting stored
     *                       on AWS S3.
     */
    public AmazonS3UploadFileResponse uploadFile(String bucketName, File file,
                                                 String targetFileName) {
        return uploadFileToS3(bucketName, file, targetFileName);
    }

    /**
     * @param amazonS3UploadInputStreamRequest
     * @return
     */
    public AmazonS3UploadFileResponse uploadFile(
            AmazonS3UploadInputStreamRequest amazonS3UploadInputStreamRequest) {

        AmazonS3UploadFileResponse response = new AmazonS3UploadFileResponse();

		/*
		 * Create aws credentials from the provided access key and secret key
		 */
        AWSCredentials awsCredentials = new BasicAWSCredentials(this.accessKey,
                this.secretKey);
		/*
		 * Create S3 client which will send the upload request to S3 server.
		 */
        AmazonS3 s3Client = new AmazonS3Client(awsCredentials);
		/*
		 * New PutObjectRequest using amazonS3UploadInputStreamRequest, this
		 * request will be sent to S3 server.
		 */
        PutObjectRequest putObjectRequest = new PutObjectRequest(
                amazonS3UploadInputStreamRequest.getBucketName(),
                amazonS3UploadInputStreamRequest.getTargetFileName(),
                amazonS3UploadInputStreamRequest.getInputStream(),
                amazonS3UploadInputStreamRequest.getObjectMetaData());
		/*
		 * Upload file to S3 using putObject method of S3 client.
		 */
        try {
            PutObjectResult result = s3Client.putObject(putObjectRequest);
			/*
			 * Set s3 file URL in response. 
			 */
            response.setS3Url(s3Client.getUrl(
                    amazonS3UploadInputStreamRequest.getBucketName(),
                    amazonS3UploadInputStreamRequest.getTargetFileName())
                    .toString());
            response.setStatus(Status.SUCCESS);
        } catch (AmazonServiceException e) {
			/*
			 * Loggable the exception occurred while uploading the file to amazon
			 */
            logger.error("Error Code:-" + e.getErrorCode() + " Error Message:-"
                    + e.getErrorMessage(), e.getCause());
            response.setStatus(Status.FAILED);
            response.setErrorMessage(e.getErrorMessage());
        }
        return response;
    }

    /**
     * @param bucketName     AWS S3 bucket name in which file will be stored
     * @param file           file which will be stored to AWS S3.
     * @param targetFileName name which will be given to the file which is getting stored
     *                       on AWS S3.
     */
    private AmazonS3UploadFileResponse uploadFileToS3(String bucketName,
                                                      File file, String targetFileName) {
        AmazonS3UploadFileResponse response = new AmazonS3UploadFileResponse();

		/*
		 * Create aws credentials from the provided access key and secret key
		 */
        AWSCredentials awsCredentials = new BasicAWSCredentials(this.accessKey,
                this.secretKey);
		/*
		 * Create S3 client which will send the upload request to S3 server.
		 */
        AmazonS3 s3Client = new AmazonS3Client(awsCredentials);
		/*
		 * New PutObjectRequest using file and bucket name this request will be
		 * sent to S3 server.
		 */
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                targetFileName, file);
		/*
		 * Upload file to S3 using putObject method of S3 client.
		 */
        try {
            PutObjectResult result = s3Client.putObject(putObjectRequest);
            response.setS3Url(s3Client.getUrl(bucketName, targetFileName)
                    .toString());
            response.setStatus(Status.SUCCESS);
        } catch (AmazonServiceException e) {
			/*
			 * Loggable the exception occurred while uploading the file to amazon
			 */
            logger.error("Error Code:-" + e.getErrorCode() + " Error Message:-"
                    + e.getErrorMessage(), e.getCause());
            response.setStatus(Status.FAILED);
            response.setErrorMessage(e.getErrorMessage());
        }
        return response;
    }

}
