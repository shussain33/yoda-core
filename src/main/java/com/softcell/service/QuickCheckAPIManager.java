package com.softcell.service;

import com.softcell.gonogo.model.quickcheck.EMandateCallBackRequest;
import com.softcell.gonogo.model.quickcheck.QuickCheckAPIRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by ssg0302 on 27/8/19.
 */
public interface QuickCheckAPIManager {

    BaseResponse createEMandate(QuickCheckAPIRequest quickCheckAPIRequest) throws Exception;

    BaseResponse processEMandate(QuickCheckAPIRequest quickCheckAPIRequest) throws Exception;

    BaseResponse callBackEMandate(EMandateCallBackRequest eMandateCallBackRequest) throws Exception;

    BaseResponse getEMandateStatus(QuickCheckAPIRequest quickCheckAPIRequest) throws Exception;

    BaseResponse fetchEMandateDetails(QuickCheckAPIRequest quickCheckAPIRequest);

    BaseResponse resendLink(QuickCheckAPIRequest quickCheckAPIRequest);
}
