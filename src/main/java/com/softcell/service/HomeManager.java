package com.softcell.service;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.ChangePasswordRequest;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.LogoutRequest;
import com.softcell.gonogo.model.security.v2.LogoutBaseResponse;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author yogeshb
 */
public interface HomeManager {
    /**
     * @param loginRequest
     * @param httpRequest
     * @return This service returns User module Response on valid authentication.
     */
    BaseResponse login(
            LoginRequest loginRequest, HttpServletRequest httpRequest) throws Exception;

    /**
     * @param logoutRequest
     * @param httpRequest
     * @return Logout status
     */
    BaseResponse logout(LogoutRequest logoutRequest,
                           HttpServletRequest httpRequest) throws Exception;

    /**
     * This will connect to UM module to change password for requested user
     *
     * @param chngPassRqst
     * @param httpRequest
     * @return
     */
    BaseResponse changePassword(
            ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    /**
     * This will connect to UM module to change password for requested user
     *
     * @param chngPassRqst
     * @param httpRequest
     * @return
     */
    BaseResponse resetPassword(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;


    /**
     * The login service is version 2 of uam.
     * @param loginRequest
     * @param httpRequest
     * @return
     * @throws Exception
     */
    BaseResponse loginV2(LoginRequest loginRequest, HttpServletRequest httpRequest) throws Exception;

    /**
     * The change password service is version 2 of uam.
     * @param chngPassRqst
     * @param httpRequest
     * @return
     * @throws Exception
     */
    BaseResponse changePasswordV2(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    /**
     * The reset password service is version 2 of uam.
     * @param chngPassRqst
     * @param httpRequest
     * @return
     * @throws Exception
     */
    BaseResponse resetPasswordV2(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    BaseResponse changeUserPassword(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    BaseResponse loginV3(LoginRequest loginRequest, HttpServletRequest httpRequest) throws IOException;

    BaseResponse resetPasswordV3(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    com.softcell.gonogo.model.security.v2.LoginBaseResponse getLoginDetails(LoginRequest loginRequest) throws IOException;

    BaseResponse forgotPassword(ChangePasswordRequest chngPassRqst, HttpServletRequest httpRequest) throws Exception;

    LogoutBaseResponse logoutUserWithoutAuthToken(LogoutRequest logoutRequest) throws Exception;
}
