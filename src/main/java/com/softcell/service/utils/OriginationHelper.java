package com.softcell.service.utils;

import com.softcell.constants.ApplicantType;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Institute;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.ThirdPartyApiLog;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.coorgination.icici.*;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.EmployerMaster;
import com.softcell.gonogo.model.masters.PinCodeMaster;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.OriginationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.dmz.DMZRequest;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.thirdparty.utils.ThirdPartyFieldConstant;
import com.softcell.utils.GngCalculationUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ssg0268 on 29/5/19.
 */
@Component
public class OriginationHelper {

    private static final Logger logger = LoggerFactory.getLogger(OriginationHelper.class);

    // Map of fieldType vs map( gngValue, 3partyValue)
    private Map<String, Map<String, String>> fieldMasterMap;

    private Map<String, Map<String, Map<String, String>>> fieldMap;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    MasterRepository masterRepository;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    public static final String APPROVED_AMT = "Approved Amount";
    public static final String ADVANCE_EMI = "Advance EMI";
    public static final String PF = "PF%";
    public static final String INSURANCE = "Insurance";
    public static final String CREDIT_VIDYA = "Credit Vidya";
    public static final String CERSAI_FEES_WITH_GST = "Cersai Fees With GST";
    public static final String PRE_EMI = "Pre-EMI";
    public static final String GST_CHARGES = "GST and Charges";
    public static final String IMD_AMOUNT = "IMD Amount";
    public static final String IMD_AMOUNT2 = "IMD 2 amount with GST";
    public static final String NET_DISBURSMENT_AMOUNT = "Net Disbursment Amount";

    public static final String BLANK = "";
    public static final String ZERO = "0";
    public static final String Country = "INDIA";
    public static final String CountryCode = "1";


    public  void populateMisReleatedData(String refId, String institutionId){
        logger.info("Inside populateOriginationHelper with refid {} " ,refId);
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication =  applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            CoOrigination origination  = applicationRepository.fetchCoOriginationDetails(refId
                    , institutionId);
            populateOriginationMisData(origination, goNoGoCustomerApplication, refId, institutionId);
            applicationRepository.saveCoOriginationDetails(refId,institutionId, origination);
        } catch (Exception e) {
            logger.error("{} stackTrace {} *****************  detailMessage {}", refId,e.getStackTrace(),e.getMessage());
        }

    }

    private void populateOriginationMisData(CoOrigination origination, GoNoGoCustomerApplication goNoGoCustomerApplication, String refId, String institutionId) {
        logger.info("Inside populateoriginationMisData with refid {} " ,refId);

        MisReleatedData misReleatedData = null ;
        getFieldMasterMap(ThirdPartyFieldConstant.CO_ORIGINATION);
        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
        List<Collateral> collateralList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getCollateral();
        CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId, institutionId);
        if(origination == null){
            origination = new CoOrigination();
            origination.setRefId(refId);
            origination.setInstitutionId(institutionId);
        }
        misReleatedData = origination.getMisReleatedData();
        LoanAgreement loanAgreement = null;
        PropertyFileData propertyFileData = null;
        if(origination.getMisReleatedData() == null){
            misReleatedData = new MisReleatedData();
            loanAgreement = new LoanAgreement();
            propertyFileData = new PropertyFileData();
            misReleatedData.setPropertyFileData(propertyFileData);
            misReleatedData.setLoanAgreement(loanAgreement);
            origination.setMisReleatedData(misReleatedData);
        }else{
            loanAgreement = misReleatedData.getLoanAgreement();
            propertyFileData = misReleatedData.getPropertyFileData();
        }
        misReleatedData.setRefId("SBFC-" + refId);
        loanAgreement.setIndCorpFlag(getValue(ThirdPartyFieldConstant.APPLICANT_TYPE,applicant.getApplicantType(),fieldMasterMap));
        if( ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType())) {
            loanAgreement.setMaritialStatus(GngUtils.setValueNA(applicant.getMaritalStatus()));
            loanAgreement.setGender(GngUtils.setValueNA(applicant.getGender()));
            loanAgreement.setQualification(GngUtils.setValueNA(applicant.getEducation()));
            loanAgreement.setDob(
                    GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                    (GngDateUtil.ddMMyyyy,
                                            applicant.getDateOfBirth()),
                            GngDateUtil.ddMMyyyy_WITH_SLASH)

                   );
            loanAgreement.setProfession(getValue(ThirdPartyFieldConstant.EmploymentType,applicant.getEmployment().get(0).getEmploymentType(),fieldMasterMap));
        }else{
            if(CollectionUtils.isNotEmpty(applicant.getEmployment())){
                if(StringUtils.isNotEmpty(applicant.getEmployment().get(0).getDateOfJoining())){
                    loanAgreement.setDob(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                            (GngDateUtil.ddMMyyyy,
                                                    applicant.getEmployment().get(0).getDateOfJoining()),
                                    GngDateUtil.ddMMyyyy_WITH_SLASH));
                }

            }
            loanAgreement.setMaritialStatus(GngUtils.setValueNA(applicant.getMaritalStatus()));
            loanAgreement.setGender(GngUtils.setValueNA(applicant.getGender()));
            loanAgreement.setQualification(GngUtils.setValueNA(applicant.getEducation()));
        }
        loanAgreement.setConstId(applicant.getEmployment().get(0).getEmploymentType().toUpperCase());
        if(CollectionUtils.isNotEmpty(collateralList)){
            if(applicant.getApplicantName().equals(collateralList.get(0).getOwnerNames().get(0))){
                propertyFileData.setSoleOwnerFlag("Y");
            }else{
                propertyFileData.setSoleOwnerFlag("N");
            }
        }
        if(StringUtils.isNotEmpty(applicant.getApplicantType())){
            loanAgreement.setIndCorpFlag(getValue(ThirdPartyFieldConstant.APPLICANT_TYPE,applicant.getApplicantType(),fieldMasterMap));
        }
        loanAgreement.setBranchCode(GngUtils.setValueForBlank(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getHubBranchName()));
        loanAgreement.setCustFname(GngUtils.setValueForBlank(applicant.getApplicantName().getFirstName()));
        loanAgreement.setCustMname(GngUtils.setValueForBlank(applicant.getApplicantName().getMiddleName()));
        loanAgreement.setCustLname(GngUtils.setValueForBlank(applicant.getApplicantName().getLastName()));

        if(CollectionUtils.isNotEmpty(applicant.getAddress())){
            for(CustomerAddress address :applicant.getAddress()){
                if(StringUtils.equalsIgnoreCase(address.getAddressType() , EndPointReferrer.ADDRESS_TYPE_RESIDENCE)
                        ||(!ApplicantType.isIndividual(applicant.getApplicantType()) &&
                        StringUtils.equalsIgnoreCase(address.getAddressType(),EndPointReferrer.ADDRESS_TYPE_OFFICE))
                        ){
                    loanAgreement.setAdd1(address.getAddressLine1());
                    loanAgreement.setAdd2(address.getAddressLine2());
                    loanAgreement.setAdd3(address.getLandMark());
                    loanAgreement.setCity(address.getCity());
                    loanAgreement.setState(address.getState());
                    loanAgreement.setZipCode(String.valueOf(address.getPin()));
                }
            }
        }
        loanAgreement.setApplicationDate(
                GngDateUtil.changeDateFormat(goNoGoCustomerApplication.getCompleted().get(EndPointReferrer.STEP_REGISTRATION).getCompletedOn(),
                GngDateUtil.ddMMyyyy_WITH_SLASH));
        populateCamDetailsData(misReleatedData, camDetails.getSummary());
        populateValuationDetailData(misReleatedData, goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication());
        populateLoanChargesData(misReleatedData, goNoGoCustomerApplication);
        populateEligibiltyData(misReleatedData,refId, institutionId);

        Valuation valuation = applicationRepository.fetchValuationDetails(refId, institutionId);
        if(valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())){
            propertyFileData.setMarketValueTRP1(valuation.getValuationDetailsList().get(0).getValuationOutput().getCurrentMarketValue());
            propertyFileData.setMarketValueTRP2(valuation.getValuationDetailsList().get(0).getValuationOutput().getCurrentMarketValue());
            propertyFileData.setBuiltUpArea(valuation.getValuationDetailsList().get(0).getValuationOutput().getBuiltUpArea());
            propertyFileData.setLandCost(valuation.getValuationDetailsList().get(0).getValuationOutput().getValueToConsider());
            propertyFileData.setBuildDate(GngDateUtil.changeDateFormat(valuation.getValuationDetailsList().get(0).getValuationOutput().getPropertyValuationDate(),
                    GngDateUtil.DD_MMM_YY));
            propertyFileData.setAppraisalDate(GngDateUtil.changeDateFormat(valuation.getValuationDetailsList().get(0).getValuationOutput().getPropertyValuationDate(),
                    GngDateUtil.DD_MMM_YY));
            misReleatedData.getLoanAgreement().setAppraisalValue(valuation.getValuationDetailsList().get(0).getValuationOutput().getValueToConsider());
        }
        if(CollectionUtils.isNotEmpty(origination.getCoOriginationDetailsList()) &&
                origination.getCoOriginationDetailsList().get(1) != null   ){
            //misReleatedData.getLoanAgreement().setAdvamceEmi(origination.getCoOriginationDetailsList().get(1).getEmi());
            loanAgreement.setAmtFinanced(String.valueOf(origination.getCoOriginationDetailsList().get(1).getAppLoan()));
        }
    }

    private void populateEligibiltyData(MisReleatedData misReleatedData, String refId, String institutionId) {
        try{
            EligibilityDetails eligibilty = applicationRepository.fetchEligibilityDetails(refId, institutionId);
            if(eligibilty !=  null){
              misReleatedData.getPropertyFileData().setLtv(Math.round(eligibilty.getFoirDetail().getLtv()));
              misReleatedData.getPropertyFileData().setLtvMarketValue( Math.round(eligibilty.getFoirDetail().getLtv()));
            }
        } catch(Exception ex){
            logger.error("{} populateEligibiltyData", ex.getStackTrace());
        }
    }

    private void populateLoanChargesData(MisReleatedData misReleatedData, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(goNoGoCustomerApplication.getGngRefId(),
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        if(loanCharges.getEmiStartDate() != null){
            misReleatedData.getLoanAgreement().setEmiStartDate(GngDateUtil.changeDateFormat(loanCharges.getEmiStartDate(),GngDateUtil.ddMMyyyy_WITH_SLASH)) ;
            misReleatedData.getLoanAgreement().setUploadDisbursalDate(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(),GngDateUtil.ddMMyyyy_WITH_SLASH));
            misReleatedData.getLoanAgreement().setIntStartDate(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(),GngDateUtil.ddMMyyyy_WITH_SLASH));
            misReleatedData.getLoanAgreement().setEffrate(loanCharges.getInterestDetails().getDisbInterestRate());
        }
        misReleatedData.getLoanAgreement().setTenure(loanCharges.getTenureDetails().getTenureMonths());

    }

    private void populateValuationDetailData(MisReleatedData misReleatedData, Application application) {
        if(CollectionUtils.isNotEmpty(application.getCollateral())){
            misReleatedData.getPropertyFileData().setCollateralState(application.getCollateral().get(0).getAddress().getState());
            misReleatedData.getPropertyFileData().setCollateralDesc(application.getCollateral().get(0).getAddress().getCity());
            misReleatedData.getPropertyFileData().setCity(application.getCollateral().get(0).getAddress().getCity());
            misReleatedData.getPropertyFileData().setStreet(application.getCollateral().get(0).getAddress().getAddressLine1());
            misReleatedData.getPropertyFileData().setLocality(application.getCollateral().get(0).getAddress().getAddressLine2());
            misReleatedData.getPropertyFileData().setNoOfProperty(String.valueOf(application.getCollateral().size()));
            misReleatedData.getPropertyFileData().setHouseNo(application.getCollateral().get(0).getAddress().getAddressLine1());
            misReleatedData.getPropertyFileData().setPropertyType(application.getCollateral().get(0).getType());

            misReleatedData.getLoanAgreement().setPropertyAddr1(application.getCollateral().get(0).getAddress().getAddressLine1());
            misReleatedData.getLoanAgreement().setPropertyAddr2(application.getCollateral().get(0).getAddress().getAddressLine2());
            misReleatedData.getLoanAgreement().setPropertyAddr3(application.getCollateral().get(0).getAddress().getLandMark());
            misReleatedData.getLoanAgreement().setPropertyState(application.getCollateral().get(0).getAddress().getState());
            misReleatedData.getLoanAgreement().setPropertyZip(String.valueOf(application.getCollateral().get(0).getAddress().getPin()));
        }
    }

    private void populateCamDetailsData(MisReleatedData misReleatedData, CamSummary summary) {
        LoanAgreement loanAgreement = null;
        if(misReleatedData == null){
            misReleatedData = new MisReleatedData();
            loanAgreement =new LoanAgreement();
            misReleatedData.setLoanAgreement(loanAgreement);
        }
        loanAgreement = misReleatedData.getLoanAgreement();
        if(summary != null){
            loanAgreement.setFreq(getValue(ThirdPartyFieldConstant.FREQUENCY,summary.getInstallmentType(),fieldMasterMap));
            misReleatedData.getLoanAgreement().setPlsCategory(getValue(ThirdPartyFieldConstant.PSLSUBCLASSIFICATION,summary.getPslSubClassification(),fieldMasterMap));
        }
    }


    private Map<String, Map<String, String>> getFieldMasterMap(String value) {
        // fieldMasterMap
        if(null != fieldMap && null != fieldMap.get(value))
            fieldMasterMap = fieldMap.get(value);
        else {
            fieldMap = masterRepository.getThirdPartyAPI(Institute.SBFC, value);
            if(null != fieldMap && null != fieldMap.get(value)) fieldMasterMap = fieldMap.get(value);
        }

        return fieldMasterMap;
    }

    public static String getValue(String fieldTypeKey, String gngValue, Map<String, Map<String, String>> map){
        String value = "";
        if( map.get(fieldTypeKey) != null ){
            Map<String, String> master = map.get(fieldTypeKey);
            if(null!=master && StringUtils.isNotEmpty(master.get(gngValue))) value = master.get(gngValue);
        }
        return value;
    }

    public CoOrigination calculationHelper(LoanCharges loanCharges,CoOrigination origination){
        List<DropdownMaster> response = masterRepository.fetchDropDownMaster(origination.getInstitutionId(), "Loan_CHARGES_MASTER",EndPointReferrer.ONE, Product.LAP.toProductName());
        origination = calculateOriginationValue(loanCharges,response, origination);
        return origination;
    }

    public CoOrigination setOriginationChargesData(OriginationRequest originationRequest, CoOrigination origination) {
      LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(originationRequest.getRefID(),
              originationRequest.getHeader().getInstitutionId());
      //populate origination charges according to master fetch from database
      if(loanCharges != null && CollectionUtils.isNotEmpty(origination.getCoOriginationDetailsList())){
          origination  = calculationHelper(loanCharges, origination);
      }
       applicationRepository.saveCoOriginationDetails(originationRequest.getRefID(),originationRequest.getHeader().getInstitutionId(), origination);
      return origination;
    }
    public  CoOrigination calculateOriginationValue(LoanCharges loanCharges, List<DropdownMaster> dropdownMaster, CoOrigination origination){
        CoOriginationDetails nbfcValue = new CoOriginationDetails();
        CoOriginationDetails partnerBankValue = new CoOriginationDetails();
        CoOriginationDetails totalBankValue = new CoOriginationDetails();
        List<CoOriginationDetails> coOriginationDetailsList = new ArrayList<CoOriginationDetails>();

        GoNoGoCustomerApplication gngApp = null;
        if(loanCharges != null && StringUtils.isNotEmpty(loanCharges.getRefId())){
            String refId = loanCharges.getRefId();
            try {
                gngApp = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            }catch (Exception e){
                logger.error("Failed to fetch getGoNoGoCustomerApplicationByRefId {}",e.getStackTrace());
            }
        }

        String  sbfc = Institute.SBFC.name();
        String partnerBank = "PartnerBank";
        nbfcValue = origination.getCoOriginationDetailsList().get(0);
        partnerBankValue = origination.getCoOriginationDetailsList().get(1);
        totalBankValue = origination.getCoOriginationDetailsList().get(2);

        logger.debug("LOAN CHARGES: {}",loanCharges);

        if(CollectionUtils.isNotEmpty(dropdownMaster)){
            if(CollectionUtils.isNotEmpty(dropdownMaster.get(0).getDropDownValueDetailsList())){
                for(HashMap<String, String> chargesMaster : dropdownMaster.get(0).getDropDownValueDetailsList()){
                    if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), CERSAI_FEES_WITH_GST)){
                        nbfcValue.setCersaiFeesWithGST(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getCersaiFeeWithGST(),sbfc));
                        partnerBankValue.setCersaiFeesWithGST(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getCersaiFeeWithGST(),partnerBank));

                        totalBankValue.setCersaiFeesWithGST(loanCharges.getCersaiFeeWithGST());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), ADVANCE_EMI)){
                        nbfcValue.setEmi(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getAdvanceEMI(),sbfc));
                        partnerBankValue.setEmi(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getAdvanceEMI(),partnerBank));

                        totalBankValue.setEmi(loanCharges.getAdvanceEMI());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), APPROVED_AMT)){
                        nbfcValue.setAppLoan(GngCalculationUtils.calculateApprovedAMt(origination.getRoiMasterdata().getInternalPercentage(),
                                loanCharges.getLoanDetails().getFinalDisbAmt(),sbfc));
                        partnerBankValue.setAppLoan(GngCalculationUtils.calculateApprovedAMt(origination.getRoiMasterdata().getExternalPercentage(),
                                loanCharges.getLoanDetails().getFinalDisbAmt(),partnerBank));

                        totalBankValue.setAppLoan(loanCharges.getLoanDetails().getFinalDisbAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), PF)){
                        nbfcValue.setPF(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getProFeesAmt(),sbfc));
                        partnerBankValue.setPF(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getProFeesAmt(),partnerBank));

                        totalBankValue.setPF(loanCharges.getProFeesAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), INSURANCE)){
                        nbfcValue.setInsurance(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getInsuranceAmt(),sbfc));
                        partnerBankValue.setInsurance(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getInsuranceAmt(),partnerBank));

                        totalBankValue.setInsurance(loanCharges.getInsuranceAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), CREDIT_VIDYA)){
                        nbfcValue.setCreditVidya(GngCalculationUtils.calculateCharges(chargesMaster,
                                Double.parseDouble(GngUtils.setValueForZero(loanCharges.getCreditVidyaCharge())),sbfc));
                        partnerBankValue.setCreditVidya(GngCalculationUtils.calculateCharges(chargesMaster,
                                Double.parseDouble(GngUtils.setValueForZero(loanCharges.getCreditVidyaCharge())),partnerBank));

                        totalBankValue.setCreditVidya(Double.parseDouble(GngUtils.setValueForZero(loanCharges.getCreditVidyaCharge())));
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), PRE_EMI)){
                        nbfcValue.setPreEmi(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getPreEMIDeductAmt(),sbfc));
                        partnerBankValue.setPreEmi(GngCalculationUtils.calculateCharges(chargesMaster,
                                loanCharges.getPreEMIDeductAmt(),partnerBank));

                        totalBankValue.setPreEmi(loanCharges.getPreEMIDeductAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), GST_CHARGES)){
                        nbfcValue.setGstCharges(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getGstAmt(),sbfc));
                        partnerBankValue.setGstCharges(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getGstAmt(),partnerBank));

                        totalBankValue.setGstCharges(loanCharges.getGstAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), IMD_AMOUNT)){
                        nbfcValue.setImdAmount(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getImdCollectedAmt(),sbfc));
                        partnerBankValue.setImdAmount(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getImdCollectedAmt(),partnerBank));

                        totalBankValue.setImdAmount(loanCharges.getImdCollectedAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), IMD_AMOUNT2)){
                        nbfcValue.setImd2gstAmt(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getImd2gstAmt(),sbfc));
                        partnerBankValue.setImd2gstAmt(GngCalculationUtils.calculateCharges(chargesMaster, loanCharges.getImd2gstAmt(),partnerBank));

                        totalBankValue.setImd2gstAmt(loanCharges.getImd2gstAmt());
                    }else if(StringUtils.equalsIgnoreCase(chargesMaster.get("ChargeName"), NET_DISBURSMENT_AMOUNT)){
                        nbfcValue.setNetDisbural(GngCalculationUtils.calculateTotalCharges(nbfcValue, totalBankValue));
                        partnerBankValue.setNetDisbural(GngCalculationUtils.calculateTotalCharges(partnerBankValue, totalBankValue));

                        totalBankValue.setNetDisbural(nbfcValue.getNetDisbural() + partnerBankValue.getNetDisbural());
                        if(gngApp != null && gngApp.getApplicationRequest() != null && gngApp.getApplicationRequest().getRequest() != null &&
                                gngApp.getApplicationRequest().getRequest().getApplication() != null){
                            String topUpType = gngApp.getApplicationRequest().getRequest().getApplication().getTopupType();
                            if(StringUtils.isNotEmpty(topUpType) && StringUtils.equalsIgnoreCase(topUpType,"Top Up Gross")){
                                totalBankValue.setNetDisbural(totalBankValue.getNetDisbural() - loanCharges.getLoanDetails().getExistingLoanAmt());
                            }
                        }
                    }
                }
            }
        }
        logger.debug("NBFC: {}",nbfcValue);
        logger.debug("PARTNER BANK: {}",partnerBankValue);
        logger.debug("TOTAL: {}",totalBankValue);
        if(CollectionUtils.isNotEmpty(coOriginationDetailsList)){
            origination.setCoOriginationDetailsList(coOriginationDetailsList);
        }
        return origination;
    }

    //to auto populate changes After value Loan Charges save
    public void saveOriginationForUpdateValues(String refId, Header header, LoanCharges loanCharges) {
        logger.debug("{} refId : origination populate according to Master check",refId);
        CoOrigination origination  = applicationRepository.fetchCoOriginationDetails(refId
                , header.getInstitutionId());
        if(origination != null && CollectionUtils.isNotEmpty(origination.getCoOriginationDetailsList()) ){
            logger.debug("{} refId : origination populate according to Master",refId);
            //map roi value as in loan charges
            origination.getRoiMasterdata().setROI(loanCharges.getInterestDetails().getDisbInterestRate());
            if(origination.isPopulate()){
                origination.setPopulate(false);
                calculationHelper(loanCharges,origination);
            }
            applicationRepository.saveCoOriginationDetails(refId,header.getInstitutionId(), origination);
        }
    }

    public CorporateAPIRequest getCoorginationDetails(GoNoGoCustomerApplication goNoGoCustomerApplication){
        logger.info("Started getCoorginationDetails method for refId {}", goNoGoCustomerApplication.getGngRefId());
        String refId  = goNoGoCustomerApplication.getGngRefId();
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        Request request  = applicationRequest.getRequest();
        Application application = request.getApplication();
        Applicant applicant = request.getApplicant();
        Header header = applicationRequest.getHeader();
        Branch branch = applicationRequest.getAppMetaData().getBranchV2();
        String institutionId = header.getInstitutionId();
        String product = header.getProduct().name();
        getFieldMasterMap(ThirdPartyFieldConstant.ICICI_CORPORATE_API);

        EligibilityDetails eligibilityDetails = applicationRepository.fetchEligibilityDetail(refId, institutionId);
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
        CorporateAPIRequest corporateAPIRequest = new CorporateAPIRequest();

        //Sourcing Details
        IdsgApplicationSource idsgApplicationSource = populateApplicationSource(new IdsgApplicationSource(), refId, header);
        if(StringUtils.isNotEmpty(application.getIciciFormNumber())) idsgApplicationSource.setSourceApplicationNumber(application.getIciciFormNumber());
        if(null != applicant.getProfessionIncomeDetails() && StringUtils.isNotEmpty(applicant.getProfessionIncomeDetails().getLoanScheme())) {

            String scheme = applicant.getProfessionIncomeDetails().getLoanScheme();
            String collateralUsage = "", occupationType="";
            if(CollectionUtils.isNotEmpty(application.getCollateral())){
                collateralUsage = application.getCollateral().get(0).getUsage();
                occupationType = application.getCollateral().get(0).getOccupation();
            }
            if(StringUtils.isNotEmpty(scheme) && StringUtils.isNotEmpty(occupationType)) {
                SchemeDetails schemeDetails = applicationRepository.fetchSchemeDetails(institutionId, product, scheme, collateralUsage, occupationType);
                if(null != schemeDetails){
                    idsgApplicationSource.setPromotion(schemeDetails.getPromotionId());
                    idsgApplicationSource.setScheme(schemeDetails.getSchemeId());
                }
            }
        }
        String contactPerson = "";
        if(null != branch){
            SourcingDetails sourcingDetails = applicationRepository.fetchSourcingDetails(institutionId, product, branch.getBranchName(), branch.getLocation());
            if(null != sourcingDetails){
                contactPerson = StringUtils.isEmpty(sourcingDetails.getRmEmpId()) ? BLANK : sourcingDetails.getRmEmpId();
                idsgApplicationSource.setLoanCroEmpId(contactPerson);//RM EMP ID
                idsgApplicationSource.setLoanBsmEmpId(contactPerson);//RM EMP ID
                idsgApplicationSource.setLoanProcessHopNameDesc(StringUtils.isEmpty(sourcingDetails.getIDisburseProcessShop()) ? BLANK : sourcingDetails.getIDisburseProcessShop()); //I-disburse spoke location
                idsgApplicationSource.setLoanLocation(StringUtils.isEmpty(sourcingDetails.getApsLocation()) ? BLANK : sourcingDetails.getApsLocation()); // APS Location
                idsgApplicationSource.setProcessShopName(StringUtils.isEmpty(sourcingDetails.getApsLocation()) ? BLANK : sourcingDetails.getApsLocation()); // APS Location
                idsgApplicationSource.setLoanCreditManagerId(StringUtils.isEmpty(sourcingDetails.getRcmEmpid()) ? BLANK : sourcingDetails.getRcmEmpid());// RCM EMP ID
            }
        }
        corporateAPIRequest.setIdsgApplicationSource(idsgApplicationSource);

        //Applicant Details
        IdsgApplicantDetails idsgApplicantDetails = populateApplicantDetails(new IdsgApplicantDetails(), applicant, request, institutionId, false);
        idsgApplicantDetails.getIdsgNidvApplicantDetails().setContactPerson(contactPerson);
        corporateAPIRequest.setIdsgApplicantDetails(idsgApplicantDetails);

        //Loan Details
        IdsgLoanDetails idsgLoanDetails = populateLoanDetails( new IdsgLoanDetails(),loanCharges, eligibilityDetails, applicationRequest);
        corporateAPIRequest.setIdsgLoanDetails(idsgLoanDetails);

        //Co-Applicant Details
        List<IdsgApplicantDetails>  idsgCoApplicantDetailsList= new ArrayList<>();
        if(CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())){
            for(Applicant coApplicant :  applicationRequest.getRequest().getCoApplicant()){
                IdsgApplicantDetails idsgCoApplicantDetails = populateApplicantDetails(new IdsgApplicantDetails(), coApplicant, request, institutionId, true);
                idsgCoApplicantDetails.getIdsgNidvApplicantDetails().setContactPerson(contactPerson);
                idsgCoApplicantDetailsList.add(idsgCoApplicantDetails);
            }
        }
        corporateAPIRequest.setIdsgCoApplicantDetails(idsgCoApplicantDetailsList);

        //Investment Details
        IdsgInvestmentDetails idsgInvestmentDetails = populateInvestmentDetails();
        List<IdsgInvestmentDetails> idsgInvestmentDetailsList = new ArrayList<>();
        idsgInvestmentDetailsList.add(idsgInvestmentDetails);
        corporateAPIRequest.setIdsgInvestmentdetail(idsgInvestmentDetailsList);

        //Liability Details
        IdsgLiabilityDetails idsgLiabilityDetails = populateLiabilityDetails();
        List<IdsgLiabilityDetails> idsgLiabilityDetailsList = new ArrayList<>();
        idsgLiabilityDetailsList.add(idsgLiabilityDetails);
        corporateAPIRequest.setLiabilityDetails(idsgLiabilityDetailsList);

        logger.info("Ended getCoorginationDetails method for refId {}", goNoGoCustomerApplication.getGngRefId());
        return corporateAPIRequest;
    }

    private IdsgApplicationSource populateApplicationSource(IdsgApplicationSource idsgApplicationSource, String refId, Header header) {
        logger.info("Populating application source for refId {}", refId);
        idsgApplicationSource.setSourceApplicationNumber(refId);
        idsgApplicationSource.setChannelCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "channelCode", fieldMasterMap));
        idsgApplicationSource.setDsa(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "dsa", fieldMasterMap));
        idsgApplicationSource.setProduct(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "product", fieldMasterMap));
        idsgApplicationSource.setProductCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "productCode", fieldMasterMap));
        idsgApplicationSource.setPromoCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "promoCode", fieldMasterMap));
        idsgApplicationSource.setPromotion(BLANK);
        idsgApplicationSource.setProcessShopName(BLANK);//Aps Location
        idsgApplicationSource.setRelationshipManager(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "relationshipManager", fieldMasterMap));
        idsgApplicationSource.setScheme(BLANK);
        idsgApplicationSource.setSolId(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "solId", fieldMasterMap));
        idsgApplicationSource.setSpreadid(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "spreadid", fieldMasterMap));
        idsgApplicationSource.setSource(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "source", fieldMasterMap));
        idsgApplicationSource.setSourceType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "sourceType", fieldMasterMap));
        idsgApplicationSource.setSurrogateCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "surrogateCode", fieldMasterMap));
        idsgApplicationSource.setLopReferenceCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "lopReferenceCode", fieldMasterMap));
        idsgApplicationSource.setDstCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "dstCode", fieldMasterMap));
        idsgApplicationSource.setSourcingSystem(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "sourcingSystem", fieldMasterMap));
        idsgApplicationSource.setHlSourceNumber(refId);
        idsgApplicationSource.setLoanCroEmpId(BLANK);//RM EMP ID
        idsgApplicationSource.setLoanCentralCpc(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "loanCentralCpc", fieldMasterMap));
        idsgApplicationSource.setLoanBsmEmpId(BLANK);//RM EMP ID
        idsgApplicationSource.setLoanProcessHopNameDesc(BLANK); //I-disburse spoke location
        idsgApplicationSource.setLoanBrokerId(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "loanBrokerId", fieldMasterMap));
        idsgApplicationSource.setLoanLocation(BLANK); // APS Location
        idsgApplicationSource.setLoanMailTo(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "loanMailTo", fieldMasterMap));
        idsgApplicationSource.setLoanCreditManagerId(BLANK);// RCM EMP ID
        idsgApplicationSource.setLoanVendorNotification(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "loanVendorNotification", fieldMasterMap));
        return idsgApplicationSource;
    }

    private IdsgApplicantDetails populateApplicantDetails(IdsgApplicantDetails idsgApplicantDetails, Applicant applicant, Request request, String institutionId, boolean isCoApplicant) {
        logger.info("Populating Applicant Details");
        //Applicant Details
        idsgApplicantDetails.setFirstName(StringUtils.isEmpty(applicant.getApplicantName().getFirstName()) ? BLANK: applicant.getApplicantName().getFirstName());
        idsgApplicantDetails.setMiddleName(StringUtils.isEmpty(applicant.getApplicantName().getMiddleName()) ? BLANK: applicant.getApplicantName().getMiddleName() );
        idsgApplicantDetails.setLastName(StringUtils.isEmpty(applicant.getApplicantName().getLastName()) ? BLANK: applicant.getApplicantName().getLastName());
        idsgApplicantDetails.setFatherName(StringUtils.isEmpty(applicant.getFatherName().getFullName()) ? BLANK: applicant.getFatherName().getFullName());
        idsgApplicantDetails.setMaritalStatus(getValue(ThirdPartyFieldConstant.MARATIAL_STATUS,applicant.getMaritalStatus(),fieldMasterMap));
        idsgApplicantDetails.setMaritalStatusCode(getValue(ThirdPartyFieldConstant.MARATIAL_STATUS,applicant.getMaritalStatus(),fieldMasterMap));
        idsgApplicantDetails.setNationality(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nationality",fieldMasterMap));
        idsgApplicantDetails.setGender(StringUtils.isEmpty(applicant.getGender()) ? BLANK: applicant.getGender());
        idsgApplicantDetails.setGenderCode(getValue(ThirdPartyFieldConstant.GENDER,applicant.getGender(),fieldMasterMap));

        idsgApplicantDetails.setSalutation(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "salutation",fieldMasterMap));
        idsgApplicantDetails.setSalutationCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "salutationCode",fieldMasterMap));
        String personalPhone = applicant.getPhone().stream()
                .filter(phone -> StringUtils.equalsIgnoreCase(phone.getPhoneType(), GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue()))
                .map(Phone::getPhoneNumber).findAny().orElse(BLANK);
        idsgApplicantDetails.setMobile_number(personalPhone);

        String personalEmail = applicant.getEmail().stream()
                .filter(email -> StringUtils.equalsIgnoreCase(email.getEmailType(), GNGWorkflowConstant.PERSONAL.toFaceValue()) && StringUtils.isNotEmpty(email.getEmailAddress()))
                .map(Email::getEmailAddress).findAny().orElse(BLANK);
        idsgApplicantDetails.setEmail(personalEmail);

        idsgApplicantDetails.setNoOfDependent((Integer.toString(applicant.getNoOfDependents())) == null ? ZERO : Integer.toString(applicant.getNoOfDependents()));
        idsgApplicantDetails.setQualification(getValue(
                ThirdPartyFieldConstant.EDUCATION,applicant.getEducation(),
                fieldMasterMap));
        idsgApplicantDetails.setCategory(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "category",fieldMasterMap));
        idsgApplicantDetails.setCategoryCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "categoryCode",fieldMasterMap));
        CustomerAddress residenceAddress = null;
        if(ApplicantType.isIndividual(applicant.getApplicantType())) {
            residenceAddress = applicant.getAddress().stream()
                    .filter(customerAddress -> StringUtils.equalsIgnoreCase(customerAddress.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                    .findAny().orElse(new CustomerAddress());
        }else{
            residenceAddress = applicant.getAddress().stream()
                    .filter(customerAddress -> StringUtils.equalsIgnoreCase(customerAddress.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue()))
                    .findAny().orElse(new CustomerAddress());
        }

        idsgApplicantDetails.setResidentType(getValue(ThirdPartyFieldConstant.RESIDENCE_TYPE, residenceAddress.getResidenceAddressType(), fieldMasterMap));///Residence Type : Other - OTHR/Parental Rented - WPR/ Owned-OWNED/Co-Provided - COMPANY ///CustomerAddress.getresidenceAddressType;
        idsgApplicantDetails.setResidenceStatus(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "residenceStatus",fieldMasterMap)); ///applicant.getResidenceStatus();
        idsgApplicantDetails.setPanNo(applicant.getKyc().stream().filter(kyc -> StringUtils.equalsIgnoreCase(kyc.getKycRequestType(), GNGWorkflowConstant.PAN.toFaceValue()))
                .map(Kyc::getKycNumber).findAny().orElse(BLANK));
        idsgApplicantDetails.setSpecialCatagorycustomer(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "specialCatagorycustomer",fieldMasterMap));
        idsgApplicantDetails.setSpouseWorking(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "spouseWorking",fieldMasterMap));

        int rmonths = 0, ryears = 0;
        if(residenceAddress.getMonthAtAddress() > 0){
            int rm = residenceAddress.getMonthAtAddress();
            ryears = rm / 12;
            rmonths = rm % 12;
        }
        idsgApplicantDetails.setMonthAtcurrentresident(Integer.toString(rmonths));
        idsgApplicantDetails.setYearAtCurrentResident(Integer.toString(ryears));

        if(!isCoApplicant) {
            if(CollectionUtils.isNotEmpty(request.getCoApplicant())) {
                idsgApplicantDetails.setRelationshipWithApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "relationShipWithCoApplicant",fieldMasterMap));
                idsgApplicantDetails.setReasonoOfCoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "reasonoOfCoApplicantA",fieldMasterMap));
                idsgApplicantDetails.setCoApplicantType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "coApplicantTypeA",fieldMasterMap));
            }else{
                idsgApplicantDetails.setRelationshipWithApplicant(BLANK);
                idsgApplicantDetails.setReasonoOfCoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "reasonoOfCoApplicantB",fieldMasterMap));
                idsgApplicantDetails.setCoApplicantType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "coApplicantTypeB",fieldMasterMap));
            }
        }else{
            idsgApplicantDetails.setRelationshipWithApplicant(StringUtils.isEmpty(((CoApplicant)applicant).getRelationWithApplicant()) ? BLANK: ((CoApplicant)applicant).getRelationWithApplicant());
            idsgApplicantDetails.setReasonoOfCoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "reasonoOfCoApplicantC",fieldMasterMap));
            idsgApplicantDetails.setCoApplicantType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "coApplicantTypeC",fieldMasterMap));
        }
        idsgApplicantDetails.setReligion(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "religion",fieldMasterMap));

        //Address Details
        List<com.softcell.gonogo.model.coorgination.icici.AddressDetails> addressDetailsList = new ArrayList<>();
        for(CustomerAddress address: applicant.getAddress()){
            com.softcell.gonogo.model.coorgination.icici.AddressDetails addressDetails = new com.softcell.gonogo.model.coorgination.icici.AddressDetails();

            addressDetails.setAddressType(getValue(ThirdPartyFieldConstant.ADDRESS_TYPE, address.getAddressType(), fieldMasterMap)); //RESIDENCE - residentialAddress / PERMANENT - permanentAddress/ OFFICE - officeAddress //address.getAddressType()
            addressDetails.setAddressLine1(StringUtils.isEmpty(address.getAddressLine1()) ? BLANK: address.getAddressLine1());
            addressDetails.setAddressLine2(StringUtils.isEmpty(address.getAddressLine2()) ? BLANK: address.getAddressLine2());
            addressDetails.setAddressLine3(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "addressLine3",fieldMasterMap));

            if(null != Long.toString(address.getPin())) {
                PinCodeMaster pinCodeMaster = applicationRepository.fetchPinCodeDetails(institutionId, Long.toString(residenceAddress.getPin()));
                if (null == pinCodeMaster) pinCodeMaster = new PinCodeMaster();
                addressDetails.setCity(StringUtils.isEmpty(pinCodeMaster.getCity()) ?
                        StringUtils.isEmpty(address.getCity()) ? BLANK : address.getCity() : pinCodeMaster.getCity());////MASTERRRRR
                addressDetails.setCityCode(StringUtils.isEmpty(pinCodeMaster.getCityCode()) ? BLANK : pinCodeMaster.getCityCode());
                addressDetails.setState(StringUtils.isEmpty(pinCodeMaster.getState()) ?
                        StringUtils.isEmpty(address.getState()) ? BLANK : address.getState() : pinCodeMaster.getState());///MASTERRRRR
                addressDetails.setStateCode(StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? BLANK : pinCodeMaster.getStateCode());
                addressDetails.setCountry(StringUtils.isEmpty(pinCodeMaster.getCountry()) ?
                        StringUtils.isEmpty(address.getCountry()) ? Country : address.getCountry() : pinCodeMaster.getCountry());/////MASTERRRRR
                addressDetails.setCountryCode(StringUtils.isEmpty(pinCodeMaster.getCountryCode()) ? CountryCode : pinCodeMaster.getCountryCode());
                addressDetails.setZipcode(StringUtils.isEmpty(pinCodeMaster.getZipCode()) ?
                        null == Long.toString(address.getPin()) ? BLANK : String.valueOf(address.getPin()) : pinCodeMaster.getZipCode());////MASTERRRRR
            }else{
                addressDetails.setCity(BLANK);
                addressDetails.setCityCode(BLANK);
                addressDetails.setState(BLANK);
                addressDetails.setStateCode(BLANK);
                addressDetails.setCountry(BLANK);
                addressDetails.setCountryCode(BLANK);
                addressDetails.setZipcode(BLANK);
            }
            addressDetails.setMailingAddress(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "mailingAddress",fieldMasterMap));
            List<PhoneNumber> phoneNumbers = new ArrayList<>();
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setPhoneType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "phoneType",fieldMasterMap));
            if(StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                phoneNumber.setPhoneNo(personalPhone);
            else
                phoneNumber.setPhoneNo(StringUtils.isEmpty(address.getLandLine()) ? BLANK: address.getLandLine());
            phoneNumber.setStdCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "stdCode",fieldMasterMap));
            phoneNumbers.add(phoneNumber);
            addressDetails.setPhoneNumber(phoneNumbers);
            addressDetailsList.add(addressDetails);
        }
        idsgApplicantDetails.setAddressDetailsList(addressDetailsList);

        Employment employment = applicant.getEmployment().stream()
                .findFirst().orElse(new Employment());
        IdsgOccupationInfo idsgOccupationInfo = new IdsgOccupationInfo();
        idsgOccupationInfo.setOccupationType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "occupationType",fieldMasterMap));//employment.getEmploymentType()
        idsgOccupationInfo.setOccupationTypeCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "occupationTypeCode",fieldMasterMap));
        idsgOccupationInfo.setOccupationTypeOther(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "occupationTypeOther",fieldMasterMap));
        idsgOccupationInfo.setIndustry(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "industry",fieldMasterMap));///employment.getIndustryType()
        idsgOccupationInfo.setAgeOfRetirement(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "ageOfRetirement",fieldMasterMap));
        idsgOccupationInfo.setCompanyType(StringUtils.isEmpty(employment.getEmployerType())?
                getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "companyType", fieldMasterMap)
                : getValue(ThirdPartyFieldConstant.COMPANY_TYPE, employment.getEmployerType(), fieldMasterMap));///Public Limited - 100020 / Private Limited - 100023 / Partnership Firm - 100007 / Proprietorship Firm - 100006

        if(StringUtils.isNotEmpty(employment.getEmploymentName())){
            EmployerMaster employerMaster = applicationRepository.fetchEmployerDetails(institutionId, employment.getEmploymentName());
            if(null != employerMaster && StringUtils.isNotEmpty(employerMaster.getIciciEmpCode())){
                idsgOccupationInfo.setCompanyName(employerMaster.getIciciEmpCode());
            }else
                idsgOccupationInfo.setCompanyName(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "companyName", fieldMasterMap));
        }else
            idsgOccupationInfo.setCompanyName(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "companyName", fieldMasterMap));

        idsgOccupationInfo.setCompanyNature(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "companyNature", fieldMasterMap));
        idsgOccupationInfo.setDesignation(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "designation", fieldMasterMap));///////MASTERRRR
        idsgOccupationInfo.setIncomeConsidered(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "incomeConsidered", fieldMasterMap));
        idsgOccupationInfo.setGrossAnnualIncome(String.valueOf(employment.getGrossSalary()) == null ?
                getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "grossAnnualIncome", fieldMasterMap) : String.valueOf(employment.getGrossSalary()));

        idsgOccupationInfo.setMonthsInJob(Integer.toString(0));
        idsgOccupationInfo.setNetMonthlyIncome(String.valueOf(employment.getMonthlySalary()) == null ?
                getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "netMonthlyIncome", fieldMasterMap) : String.valueOf(employment.getMonthlySalary()));
        idsgOccupationInfo.setProfession(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "profession", fieldMasterMap));///MASTERRRRR
        idsgOccupationInfo.setYearsInJob(Integer.toString(employment.getTotalYearsOfExperience()));
        idsgOccupationInfo.setCreditCardflag(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "creditCardflag", fieldMasterMap));
        idsgApplicantDetails.setIdsgOccupationInfo(idsgOccupationInfo);

        //Fetching Pin Code Master Details
        PinCodeMaster pinCodeMaster = null;
        pinCodeMaster = applicationRepository.fetchPinCodeDetails(institutionId, Long.toString(residenceAddress.getPin()));
        if(null == pinCodeMaster) pinCodeMaster = new PinCodeMaster();

        //Nidv Applicant Details
        IdsgNidvApplicantDetails idsgNidvApplicantDetails = new IdsgNidvApplicantDetails();
        if(!isCoApplicant && ApplicantType.isIndividual(applicant.getApplicantType())) {
            idsgNidvApplicantDetails.setNidvCorporateFlag(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nidvCorporateFlagA", fieldMasterMap));//MASTERRRR
        } else if (!isCoApplicant && !ApplicantType.isIndividual(applicant.getApplicantType())) {
            idsgNidvApplicantDetails.setNidvCorporateFlag(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nidvCorporateFlagB", fieldMasterMap));//MASTERRRR
        } else {
            idsgNidvApplicantDetails.setNidvCorporateFlag(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nidvCorporateFlagC", fieldMasterMap));//MASTERRRR
        }

        if(isCoApplicant && ApplicantType.isIndividual(applicant.getApplicantType())){
            idsgNidvApplicantDetails.setCorporateFlagcoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "corporateFlagcoApplicantA", fieldMasterMap));
        }else if(isCoApplicant && !ApplicantType.isIndividual(applicant.getApplicantType())){
            idsgNidvApplicantDetails.setCorporateFlagcoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "corporateFlagcoApplicantB", fieldMasterMap));
        }else {
            idsgNidvApplicantDetails.setCorporateFlagcoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "corporateFlagcoApplicantC", fieldMasterMap));
        }

        idsgNidvApplicantDetails.setApplicantType(StringUtils.isEmpty(applicant.getApplicantType())? BLANK: applicant.getApplicantType());
        if(!isCoApplicant && CollectionUtils.isNotEmpty(request.getCoApplicant()))
            idsgNidvApplicantDetails.setReasonForCoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "reasonoOfCoApplicantA", fieldMasterMap));
        else
            idsgNidvApplicantDetails.setReasonForCoApplicant(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "reasonoOfCoApplicantB", fieldMasterMap));

        idsgNidvApplicantDetails.setExistingCustomer(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "existingCustomer", fieldMasterMap));//MASTERRRR
        idsgNidvApplicantDetails.setNidvCompanyName(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nidvCompanyName", fieldMasterMap));
        idsgNidvApplicantDetails.setRegion(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "region", fieldMasterMap));
        idsgNidvApplicantDetails.setConstitution(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "constitution", fieldMasterMap));
        idsgNidvApplicantDetails.setGroupp(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "groupp", fieldMasterMap));
        idsgNidvApplicantDetails.setNidvCategory(StringUtils.isEmpty(employment.getEmploymentCategory())? BLANK: employment.getEmploymentCategory());
        idsgNidvApplicantDetails.setNidvIndustry(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nidvIndustry", fieldMasterMap));

        if(ApplicantType.isIndividual(applicant.getApplicantType())) {
            idsgApplicantDetails.setDateOfBirth(StringUtils.isEmpty(applicant.getDateOfBirth())? BLANK : GngDateUtil.changeDateFormat(GngDateUtil.getDate(applicant.getDateOfBirth()).toDate(), "dd/MM/yyyy"));
            idsgNidvApplicantDetails.setIncorporationDate(StringUtils.isEmpty(applicant.getDateOfBirth())?
                    BLANK
                    : GngDateUtil.changeDateFormat(GngDateUtil.getDate(applicant.getDateOfBirth()).toDate(), "dd/MM/yyyy"));
        }else{
            idsgApplicantDetails.setDateOfBirth(StringUtils.isEmpty(employment.getDateOfJoining())? BLANK : GngDateUtil.changeDateFormat(GngDateUtil.getDate(employment.getDateOfJoining()).toDate(), "dd/MM/yyyy"));
            idsgNidvApplicantDetails.setIncorporationDate(StringUtils.isEmpty(employment.getDateOfJoining())?
                    BLANK
                    : GngDateUtil.changeDateFormat(GngDateUtil.getDate(employment.getDateOfJoining()).toDate(), "dd/MM/yyyy"));
        }

        idsgNidvApplicantDetails.setContactPerson(BLANK);//RM EMP ID
        idsgNidvApplicantDetails.setNidvDesignation(StringUtils.isEmpty(employment.getDesignation())? BLANK: employment.getDesignation());
        idsgNidvApplicantDetails.setEntity(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "entity", fieldMasterMap));//MASTERRRRR
        idsgNidvApplicantDetails.setNidvPanNumber(applicant.getKyc().stream().filter(kyc -> StringUtils.equalsIgnoreCase(kyc.getKycRequestType(), GNGWorkflowConstant.PAN.toFaceValue()))
                .map(Kyc::getKycNumber).findAny().orElse(BLANK));
        idsgNidvApplicantDetails.setFormNumber(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "formNumber", fieldMasterMap));
        idsgNidvApplicantDetails.setGstRegistered(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "gstRegistered", fieldMasterMap));
        idsgNidvApplicantDetails.setGstRegistratnNo(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "gstRegistratnNo", fieldMasterMap));
        idsgNidvApplicantDetails.setTurnOver(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "turnOver", fieldMasterMap));
        idsgNidvApplicantDetails.setRiskMonitoring(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "riskMonitoring", fieldMasterMap));
        idsgNidvApplicantDetails.setNri(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "nri", fieldMasterMap));
        idsgNidvApplicantDetails.setEmploymentType(getValue(ThirdPartyFieldConstant.EMPLOYMENT_TYPE, employment.getEmploymentType(), fieldMasterMap));
        idsgNidvApplicantDetails.setProvidentFundNumber(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "providentFundNumber", fieldMasterMap));
        idsgNidvApplicantDetails.setNoOfEmployeesInCompany(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "noOfEmployeesInCompany", fieldMasterMap));
        idsgNidvApplicantDetails.setSalaryType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "salaryType", fieldMasterMap));
        idsgNidvApplicantDetails.setPaymentMode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "paymentMode", fieldMasterMap));
        idsgNidvApplicantDetails.setGrossBlockValueInRuppes(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "grossBlockValueInRuppes", fieldMasterMap));
        idsgNidvApplicantDetails.setLandOtherItemValuesInRuppes(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "landOtherItemValuesInRuppes", fieldMasterMap));
        idsgNidvApplicantDetails.setPreviousCompanyName(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "previousCompanyName", fieldMasterMap));
        idsgNidvApplicantDetails.setOfficeOwnerShip(StringUtils.isEmpty(employment.getOwnerShip())? BLANK: employment.getOwnerShip());
        idsgNidvApplicantDetails.setShopRegistrationNo(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "shopRegistrationNo", fieldMasterMap));
        idsgNidvApplicantDetails.setWeakerSectnDescript(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "weakerSectnDescript", fieldMasterMap));

        idsgNidvApplicantDetails.setAddressType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "addressType", fieldMasterMap));
        idsgNidvApplicantDetails.setAddressLine1(StringUtils.isEmpty(residenceAddress.getAddressLine1())? BLANK: residenceAddress.getAddressLine1());
        idsgNidvApplicantDetails.setAddressLine2(StringUtils.isEmpty(residenceAddress.getAddressLine2())? BLANK: residenceAddress.getAddressLine2());
        idsgNidvApplicantDetails.setCountry(StringUtils.isEmpty(pinCodeMaster.getCountry()) ?
                StringUtils.isEmpty(residenceAddress.getCountry()) ? Country: residenceAddress.getCountry() : pinCodeMaster.getCountry());//MASTERRRR
        idsgNidvApplicantDetails.setCountryCode(StringUtils.isEmpty(pinCodeMaster.getCountryCode()) ? CountryCode : pinCodeMaster.getCountryCode());
        idsgNidvApplicantDetails.setState(StringUtils.isEmpty(pinCodeMaster.getState()) ?
                StringUtils.isEmpty(residenceAddress.getState()) ? BLANK: residenceAddress.getState() : pinCodeMaster.getState());//MASTERRRR
        idsgNidvApplicantDetails.setStateCode(StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? BLANK : pinCodeMaster.getStateCode());
        idsgNidvApplicantDetails.setCity(StringUtils.isEmpty(pinCodeMaster.getCity()) ?
                StringUtils.isEmpty(residenceAddress.getCity()) ? BLANK: residenceAddress.getCity() : pinCodeMaster.getCity());///MASTERRR
        idsgNidvApplicantDetails.setCityCode(StringUtils.isEmpty(pinCodeMaster.getCityCode()) ? BLANK : pinCodeMaster.getCityCode());
        idsgNidvApplicantDetails.setDistrictId(StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? BLANK : pinCodeMaster.getStateCode());////MASTERRR
        idsgNidvApplicantDetails.setZipcode(StringUtils.isEmpty(pinCodeMaster.getZipCode()) ?
                null == Long.toString(residenceAddress.getPin())? BLANK: String.valueOf(residenceAddress.getPin()) : pinCodeMaster.getZipCode());///MASTERRRR

        idsgNidvApplicantDetails.setPhoneNo(personalPhone);
        idsgNidvApplicantDetails.setStdCode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "stdCode", fieldMasterMap));
        idsgNidvApplicantDetails.setMailingAddress(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "mailingAddress", fieldMasterMap));///MASTERRR
        idsgApplicantDetails.setIdsgNidvApplicantDetails(idsgNidvApplicantDetails);

        return idsgApplicantDetails;
    }


    private IdsgLoanDetails populateLoanDetails(IdsgLoanDetails idsgLoanDetails, LoanCharges loanCharges,
                                                EligibilityDetails eligibilityDetails, ApplicationRequest applicationRequest) {
        logger.info("Populating Loan Details");
        idsgLoanDetails.setLoanAmountRequested(String.valueOf(loanCharges.getLoanDetails().getFinalLoanApprovedAmt()));
        idsgLoanDetails.setDisburseTo(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "disburseTo", fieldMasterMap));
        idsgLoanDetails.setEffectiveRate(String.valueOf(loanCharges.getInterestDetails().getInterestRate()));
        idsgLoanDetails.setFlatRate(String.valueOf(loanCharges.getInterestDetails().getInterestRate()));// Flate Rate????
        idsgLoanDetails.setInstalmentMode(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "instalmentMode", fieldMasterMap));//camSummary.getInstallmentType()
        idsgLoanDetails.setInstalmentPlan(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "instalmentPlan", fieldMasterMap));
        if(eligibilityDetails != null){
            idsgLoanDetails.setLoanEmi(String.valueOf(eligibilityDetails.getEmi()));
            idsgLoanDetails.setLtv(String.valueOf(eligibilityDetails.getCombinedLTV()));
        }
        idsgLoanDetails.setNoOfIntalments(String.valueOf(loanCharges.getTenureDetails().getDisbTenureMonths()));
        idsgLoanDetails.setProcessingFee(String.valueOf(loanCharges.getTotalFees()));
        idsgLoanDetails.setPurposeOfLoan(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "purposeOfLoan", fieldMasterMap));
        idsgLoanDetails.setRateEmiFlag(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "rateEmiFlag", fieldMasterMap));
        idsgLoanDetails.setTenure(String.valueOf(loanCharges.getTenureDetails().getDisbTenureMonths()));
        return idsgLoanDetails;
    }

    private IdsgInvestmentDetails populateInvestmentDetails(){
        logger.info("Populating Investment Details");
        return new IdsgInvestmentDetails().builder()
                .investmentType(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "investmentType", fieldMasterMap))
                .investmentName(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "investmentName", fieldMasterMap))
                .amount(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "amount", fieldMasterMap))
                .build();
    }

    private IdsgLiabilityDetails populateLiabilityDetails(){
        logger.info("Populating Liability Details");
        return new IdsgLiabilityDetails().builder()
                .liabilityConsidered(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "liabilityConsidered", fieldMasterMap))
                .sourceDescription(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "sourceDescription", fieldMasterMap))
                .balanceAmount(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "balanceAmount", fieldMasterMap))
                .emiAmount(getValue(ThirdPartyFieldConstant.DEFAULT_VALUES, "emiAmount", fieldMasterMap))
                .build();
    }

    public void pushCorporateAPIDetails(String refId) throws Exception {
        logger.info("Inside pushCorporateAPIDetails");
        try{
            GoNoGoCustomerApplication goNoGoCustomerApplication =  applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            Header header = goNoGoCustomerApplication.getApplicationRequest().getHeader();

            String institutionId = header.getInstitutionId();
            Product product = header.getProduct();
            String urlType = UrlType.ICICI_CORPORATE_API.toValue();

            WFJobCommDomain wfJobCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, product.toString(), urlType);
            if (wfJobCommDomain == null) {
                logger.error("No Configurations Found against this institutionID {} and Product {} for urlType {}", institutionId, product, urlType);
            }else{

                CorporateAPIRequest corporateAPIRequest = getCoorginationDetails(goNoGoCustomerApplication);

                CoorginationAPIRequest coorginationAPIRequest = new CoorginationAPIRequest();
                coorginationAPIRequest.setRefId(refId);
                coorginationAPIRequest.setHeader(header);
                coorginationAPIRequest.setType(urlType);
                coorginationAPIRequest.setCorporateAPIRequest(corporateAPIRequest);

                ThirdPartyApiLog apiLog = new ThirdPartyApiLog();
                apiLog.setRefId(refId);
                apiLog.setInstitutionId(institutionId);
                apiLog.setRequestType(urlType);
                apiLog.setRequestString(JsonUtil.ObjectToString(coorginationAPIRequest));
                apiLog.setRequest(coorginationAPIRequest);

                Map<String, String> headerMap = new HashMap<>();
                if (CollectionUtils.isNotEmpty(wfJobCommDomain.getKeys())) {
                    for (HeaderKey key : wfJobCommDomain.getKeys()) {
                        headerMap.put(key.getKeyName(), key.getKeyValue());
                    }
                }

                DMZRequest connectorRequest = new DMZRequest();
                connectorRequest.setRequestType(urlType);
                connectorRequest.setRequestURL(headerMap.get("URL"));
                connectorRequest.setRequestString(JsonUtil.ObjectToString(corporateAPIRequest));

                String url = wfJobCommDomain.getBaseUrl();
                logger.debug("{} CorporateAPIrequest {} for url {}", coorginationAPIRequest.getRefId(), JsonUtil.ObjectToString(corporateAPIRequest), url);
                String responseString = new HttpTransportationService().postData(url, JsonUtil.ObjectToString(connectorRequest), null, MediaType.APPLICATION_JSON_VALUE);

                logger.debug("{} response {}", coorginationAPIRequest.getRefId(), responseString);
                DMZResponse dmzResponse = JsonUtil.StringToObject(responseString, DMZResponse.class);
                try {
                    CorporateAPIResponse response = JsonUtil.StringToObject(dmzResponse.getResponseString(), CorporateAPIResponse.class);
                    apiLog.setResponse(response);
                    apiLog.setResponseString(JsonUtil.ObjectToString(response));
                }catch(Exception e){
                    logger.error("Error occured while Converting response String {}", e.getStackTrace());
                    apiLog.setResponse(dmzResponse);
                    apiLog.setResponseString(JsonUtil.ObjectToString(dmzResponse));
                }
                externalAPILogRepository.saveIciciCallLog(apiLog);

            }
        }catch(Exception e){
            logger.error("{} stackTrace {} *****************  detailMessage {}", refId,e.getStackTrace(),e.getMessage());
        }
    }
}
