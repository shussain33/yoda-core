package com.softcell.service.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.gonogo.service.AuthServiceCaller;
import com.softcell.gonogo.service.impl.AuthServiceCallerImpl;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * It is transport layer which will convert the service request into json , xml format
 * and give response in object format which can be converted into desired response
 * Created by bhuvneshk on 10/2/17.
 */
public class TransportUtils {

    private static final Logger logger = LoggerFactory.getLogger(TransportUtils.class);

    /**
     * It takes Service json request which convert it into request json , will hit the desired dmz service
     * response will be converted into Class provide in parameter
     * @param jsonRequest
     * @param url
     * @param name
     * @return
     * @throws Exception
     */
    public static Object postAuthRequest(Object jsonRequest, String url, Class<?> name) throws Exception {

        logger.debug("postAuthRequest started for url {} with input values {} ", url);

        try {

            if (StringUtils.isBlank(url)) {

                logger.error("postAuthRequest config not setup for service fetching response {}" , name.getSimpleName());

                throw new Exception(String.format("postAuthRequest  config not setup for service fetching ", name.getSimpleName()));
            }

            AuthServiceCaller authServiceCaller = new AuthServiceCallerImpl();

            String inputJson = JsonUtil.ObjectToString(jsonRequest);

            logger.debug("postAuthRequest calling authService for authentication");

            String response = authServiceCaller.authService(inputJson, url);

            Object responseObject = JsonUtil.StringToObject(response, name);

            logger.debug("postAuthRequest  received success response from auth service");

            return responseObject;

        } catch (JsonProcessingException e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());

        } catch (Exception e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


    public static Object postJsonRequest(Object jsonRequest, String url, Class<?> name) throws Exception {

        logger.debug("postJsonRequest  started for url {}",url);

        try {

            if (StringUtils.isBlank(url)) {
                logger.error("postJsonRequest config not setup for service fetching response for {}", name.getSimpleName());
                throw new Exception(String.format("postJsonRequest config not setup for service fetching response for {} ",name.getSimpleName()));
            }

            AuthServiceCaller authServiceCaller = new AuthServiceCallerImpl();

            String inputJson = JsonUtil.ObjectToString(jsonRequest);
            logger.debug("jsonRequest for mifin {}", inputJson);
            logger.debug(" postJsonRequest calling  authentication service ");

            String response = authServiceCaller.postJsonRequest(inputJson ,url);
            logger.debug("postJsonRequest receive success response {}", response);

            Object responseObject = null;
            if(StringUtils.isNotBlank(response)){
                responseObject = JsonUtil.StringToObject(response, name);
            }
            return responseObject;

        } catch (JsonProcessingException e) {

            logger.error("postJsonRequest: JsonProcessingException: {}",e.getMessage());
            e.printStackTrace();

            throw new Exception(e.getMessage());

        } catch (Exception e) {

            logger.error("postJsonRequest: Generic Exception: {}",e.getMessage());
            e.printStackTrace();

            throw new Exception(e.getMessage());
        }
    }

    public static String postJsonRequest(Object jsonRequest, String url) throws Exception {

        logger.debug("postJsonRequest  started for url {} with input request {} ",url);
        try {
            if (StringUtils.isBlank(url)) {
                logger.error("Url is blank {}", url);
                throw new Exception(String.format("Url is blank"));
            }

            AuthServiceCaller authServiceCaller = new AuthServiceCallerImpl();
            String inputJson = JsonUtil.ObjectToString(jsonRequest);
            logger.debug(" postJsonRequest calling  authentication service ");
            String response = authServiceCaller.postJsonRequest(inputJson ,url);
            logger.debug("postJsonRequest receive success response {}", response);

            return response;
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }
}
