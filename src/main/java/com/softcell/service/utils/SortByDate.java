/**
 * bhuvneshk3:52:49 PM  Copyright Softcell Technolgy
 **/
package com.softcell.service.utils;

import com.softcell.gonogo.model.dashboard.DashboardDetails;

import java.util.Comparator;
import java.util.Date;

/**
 * @author bhuvneshk
 *
 */
public class SortByDate implements Comparator<DashboardDetails> {


    @Override
    public int compare(DashboardDetails o1, DashboardDetails o2) {

        Date date1 = o1.getDate();
        Date date2 = o2.getDate();
        if (date1 == null || date2 == null) {
            return -1;
        } else if (date1.compareTo(date2) == 0) {
            return 0;
        } else if (date1.compareTo(date2) > 0) {
            return -1;
        } else if (date1.compareTo(date2) < 0) {
            return 1;
        }

        return 0;
    }

}
