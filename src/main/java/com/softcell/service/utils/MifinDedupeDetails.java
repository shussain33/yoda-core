package com.softcell.service.utils;

import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.ListOfDocs;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.ListOfDocsRequest;
import com.softcell.gonogo.model.request.core.Request;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MifinDedupeDetails {

    DisbursementMemo disbursementMemo;//collection

    CamDetails camDetails ;//collection done
    Eligibility eligibility;//collection done
    LoanCharges loanCharges;
    ListOfDocs listOfDocs;
    ListOfDocsRequest listOfDocsRequest ;//collection
    Valuation valuation ;//Collection done
    Repayment repayment ;//collection done
    ApplicationRequest applicationRequest ;
    VerificationDetails verificationDetails;
    Request request;
    LegalVerification legalVerification ;

}
