package com.softcell.service.utils;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.Stages;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.ThirdPartyCall;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.LoanDetails;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.FoirDetail;
import com.softcell.gonogo.model.core.eligibility.IncomeDetail;
import com.softcell.gonogo.model.core.valuation.*;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerificationInput;
import com.softcell.gonogo.model.core.verification.PropertyVerificationOutput;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.DropdownMasterRequest;
import com.softcell.gonogo.model.mifin.Asset;

import com.softcell.gonogo.model.mifin.IMD.IMDDetails;
import com.softcell.gonogo.model.mifin.IMD.IMDMiFinRequest;
import com.softcell.gonogo.model.mifin.topup.*;
import com.softcell.gonogo.model.mifin.topup.AssetDetails;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.ListOfDocsRequest;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.request.core.TopUpInfo;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.ExternalApiManager;
import com.softcell.service.impl.ExternalApiManagerImpl;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by yogesh on 24/4/18.
 */
@Component
public class

MiFinHelper {
    public static final Map<String, String> LOANPURPOSE = new HashMap<>();
    public static final Map<String, String> FREQUENCY = new HashMap<>();
    public static final Map<String, String> PROPERTY_USAGE = new HashMap<>();
    public static final Map<String, String> SCHEME = new HashMap<>();
    public static final Map<String, String> LOAN_TOPUP_TYPE = new HashMap<>();
    public static final Map<String, String> BANK_ACCT_TYPE = new HashMap<>();
    public static final Map<String, String> CUSTOMER_TYPE = new HashMap<>();
    public static final Map<String, String> QUALIFICATION = new HashMap<>();
    public static final Map<String, String> OCCUPATION_TYPE = new HashMap<>();
    public static final Map<String, String> OCCUPANCY_STATUS = new HashMap<>();
    public static final Map<String, String> PAYMENT_MODE = new HashMap<>();
    public static final Map<String, String> CUSTOMER_CATEGORY = new HashMap<>();
    public static final Map<String, String> ADDRESS_TYPE = new HashMap<>();
    public static final Map<String, String> GENDER_TYPE = new HashMap<>();
    public static final Map<String, String> MARITAL_STATUS = new HashMap<>();
    public static final Map<String, String> OCCUPANCY_MASTER_STATUS = new HashMap<>();
    public static final Map<String, String> INTEREST_TYPE = new HashMap<>();
    public static final Map<String, String> REPAYMENT_MODE = new HashMap<>();
    public static final Map<String, String> OWNERSHIP_TYPE = new HashMap<>();

    //    public static final String PROPERTY_TYPE_FREEHOLD = "Freehold";
    public static final String APPLICANT = "APPLICANT";
    public static final String COAPPLICANT = "COAPPLICANT";
    public static final String OTHERS = "OTHERS";
    public static final String STATUS_SUCCESS = "S";
    public static final String STATUS_FAILED = "F";
    public static final String PROPERTY_TYPE_FREEHOLD = "Freehold";
    public static final String VALUATOR_AGENCY_CODE ="1110";

    public static final String CHARGE_FEES_AMT_CERSAI = "CERSAI FEE";
    public static final String CHARGE_FEES_AMT_PROCESSING = "Loan Processing Fee";
    public static final String CHARGE_FEES_AMT_ADVANCE_EMI = "Advance EMI";
    public static final String CHARGE_FEES_PRE_EMI = "Pre Emi";
    public static final String CHARGE_FEES_AMT_INSURANCE = "Insurance Premium"; // insurance amount:
    public static final String CHARGE_FEES_AMT_IMD = "Imd"; // IMD Collected Amount
    public static final String CHARGE_FEES_AMT_IMD2 = "Imd2";
    public static final String CHARGE_FEES_AMT_CREDITVIDYA = "CreditVidya"; // Credit Vidya Amount (includes applcants and coApps charges)
    public static final String CHARGE_FEES_AMT_EXISTINAMT = "Existing Loan Closure"; // Existing Emi amount
    public static final String ADDITIONAL_CHARGES = "ADDITIONAL CHARGES"; //Additional Charges



    public static final String VERIFICATION_TYPE_RESIDENCE = "CPV Residence";
    public static final String VERIFICATION_TYPE_OFFICE = "CPV Office";
    public static final String VERIFICATION_TYPE_LEGAL = "Legal";
    public static final String VERIFICATION_TYPE_BANK_STMT = "Bank Statement Verification";
    public static final String VERIFICATION_TYPE_ITR = "ITR verification";

    public static final String VERIFICATION_POINT_RESIDENCE = "Residence";
    public static final String VERIFICATION_POINT_OFFICE = "Office";
    public static final String VERIFICATION_POINT_RESI_CUM_OFC = "Residence cum office";

    public static final String VERIFICATION_STATUS_COMPLETED = "Completed";
    public static final String VERIFICATION_STATUS_WAIVED = "Waived";
    public static final String VERIFICATION_STATUS_PENDING = "Pending";

    public static final String VERIFICATION_TARGET_APPLICANT = "Applicant";
    public static final String VERIFICATION_TARGET_COAPPLICANT = "CoApplicant";

    public static final String AADHAAR = "AADHAAR";

    public static final String TWO_DECIMALS = "#.##";

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MiFinHelper.class);


    public static final String ORGANIZATION_TYPE_GOVT = "GOVERNMENT";
    public static final String ORGANIZATION_TYPE_PSU = "PSU";
    public static final String LOAN_STATUS = "Live";
    public static  final String DEFAULT = "DEFAULT";
    static {
        // Address details > address type ( Sheet Address_Type )
        ADDRESS_TYPE.put(LosTvsAddressTypeEnum.RESIDENCE.name(), "Residence Address");
        ADDRESS_TYPE.put(LosTvsAddressTypeEnum.OFFICE.name(), "Office Address");
        ADDRESS_TYPE.put(LosTvsAddressTypeEnum.PERMANENT.name(), "Permanent Address");
        //ADDRESS_TYPE.put(LosTvsAddressTypeEnum.name(),"Residence cum office");

        // Basid Info > purpose of loan ( Sheet Purpose_of_Loan )
        LOANPURPOSE.put("Business Expansion", "Business Needs");
        LOANPURPOSE.put("Working Capital", "Working Capital");
        LOANPURPOSE.put("Machinery Purchase", "PURCHASE OF PROFESSIONAL EQUIPMENT");
        LOANPURPOSE.put("Marriage", "MARRIAGE");
        LOANPURPOSE.put("Wedding", "MARRIAGE");
        LOANPURPOSE.put("Asset Acquisition", "Asset Acquisition");
        LOANPURPOSE.put("Education", "EDUCATION");
        LOANPURPOSE.put("Travel", "HOLIDAY/ TRAVEL");
        LOANPURPOSE.put("Home Renovation", "HOME IMPROVEMENT");
        LOANPURPOSE.put("Debt Consolidation", "Debt Consolidation");
        LOANPURPOSE.put("Purchase", "Purchase");
        LOANPURPOSE.put("Medical", "MEDICAL");
        LOANPURPOSE.put("Other", "Others");

        // Credit sanction > frequency ( Sheet FREQUENCY )
        FREQUENCY.put("Monthly", "Monthly");
        FREQUENCY.put("Yearly", "Annually");
        FREQUENCY.put("half-yearly", "HalfYearly");
        FREQUENCY.put("Quaterly", "Quartely");

        // Asset details > property usage ( Sheet PROPERTY_USAGE )
        PROPERTY_USAGE.put("Commercial", LosTvsAddressTypeEnum.COMMERCIAL.name());
        PROPERTY_USAGE.put("Residential", LosTvsAddressTypeEnum.RESIDENTIAL.name());

        // Credit sanction > scheme
        SCHEME.put("Normal", "ME NEW CUSTOMER");
        SCHEME.put("BT", "ME BT");
        SCHEME.put("btTopup", "ME BT TOPUP");
        SCHEME.put("BT Topup", "ME BT TOPUP");
        SCHEME.put("Top Up Parallel", "ME TOP UP");
        SCHEME.put("Top Up Gross", "ME TOP UP");
        SCHEME.put("Secured BL BT", "SBL_BT");
        SCHEME.put("Secured BL", "S_BL");
        SCHEME.put("Secured BL BT Top-up", "SBL_BT_Top-up");
        SCHEME.put("Prof Loan - Doctor", "Prof Loan - Doctor");
        SCHEME.put("Prof Loan - CA", "Prof Loan - CA");
        SCHEME.put("SALARIED LAP BT TOPUP", "SALARIED LAP BT TOPUP");
        SCHEME.put("SALARIED LAP BT", "SALARIED LAP BT");
        SCHEME.put("SALARIED LAP", "SALARIED LAP");
        SCHEME.put("HL", "HL");
        SCHEME.put("HL BT", "HL BT");
        SCHEME.put("HL BT Top up", "HL BT Top up");
        SCHEME.put("Micro loans", "Micro loans");
        SCHEME.put("Micro loans BT", "Micro loans BT");
        SCHEME.put("Micro loans BT Top up", "Micro loans BT Top up");

        /// bASIC INFO > LOAN TOPUP TYPE
        LOAN_TOPUP_TYPE.put("Top Up Parallel", "P");
        LOAN_TOPUP_TYPE.put("Top Up Gross", "G");
        LOAN_TOPUP_TYPE.put("BT Topup", "P");

        CUSTOMER_CATEGORY.put(ApplicantType.NORMAL.value(), "New Customer");
        CUSTOMER_CATEGORY.put(ApplicantType.EXISTING.value(), "Existing Customer");
        // if scheme is topup then topup
        /*CUSTOMER_CATEGORY.put("", "Top-up");
        CUSTOMER_CATEGORY.put("", "Repeated");
        CUSTOMER_CATEGORY.put("", "Renewal");
*/
        // Bank details > account type
        //BANK_ACCT_TYPE.put("Current account", "Current Account");
        BANK_ACCT_TYPE.put("Saving", "Saving Account");
        BANK_ACCT_TYPE.put("Saving", "Saving Account");
        BANK_ACCT_TYPE.put("Current", "Current Account");
        BANK_ACCT_TYPE.put("CC", "CC Account");
        //we found cash credit for some cases.
        BANK_ACCT_TYPE.put("Cash Credit", "CC Account");
        BANK_ACCT_TYPE.put("OD", "OD Account");
        BANK_ACCT_TYPE.put("OverDraft", "OD Account");

        // Customer details > customer type
        CUSTOMER_TYPE.put(APPLICANT, "Applicant");
        CUSTOMER_TYPE.put(COAPPLICANT, "Co-Applicant");

        // Customer details > qualification
        QUALIFICATION.put("UnderGraduate", "Under Graduation");
        QUALIFICATION.put("Graduate", "Graduation");
        QUALIFICATION.put("PostGraduate", "Post-Graduation");

        // Customer details > occupation
        OCCUPATION_TYPE.put("Salaried", "Pvt Salaried");
        OCCUPATION_TYPE.put("Self-Employed Professional", "Self Employed Professional");
        OCCUPATION_TYPE.put("Self-Employed Non-Professional", "Self Employed Non Proff");
        OCCUPATION_TYPE.put(ApplicantType.CORPORATE.value(), "Self Employed Professional");

        OCCUPANCY_STATUS.put("Self-occupied", "SELF-OCCUPIED");
        OCCUPANCY_STATUS.put("Rented", "RENTED");
        OCCUPANCY_STATUS.put("Vacant", "VACANT");
        OCCUPANCY_STATUS.put("Mixed Use", "Mixed Use");
        OCCUPANCY_STATUS.put("Owned", "SELF-OCCUPIED");

        // Disbursal details > instrument type
        PAYMENT_MODE.put("Cash", "Cash");
        PAYMENT_MODE.put("Cheque", "Cheque");
        PAYMENT_MODE.put("PDC", "Cheque");
        PAYMENT_MODE.put("DD", "Demand Draft");
        PAYMENT_MODE.put("Fund Transfer", "Direct Transfer");
        PAYMENT_MODE.put("NEFT", "NEFT");
        PAYMENT_MODE.put("RTGS", "RTGS");
        PAYMENT_MODE.put("IMPS", "IMPS");
        PAYMENT_MODE.put("INPS", "IMPS"); // For wrong value already present
        PAYMENT_MODE.put("NACH", "Net Banking");
        PAYMENT_MODE.put("Online Transfer", "IMPS");

        //GENDER
        GENDER_TYPE.put("Male", "Male");
        GENDER_TYPE.put("Female", "Female");
        GENDER_TYPE.put("Transgender", "TRANSGENDER");

        //MARITAL_STATUS
        MARITAL_STATUS.put("Married", "Married");
        MARITAL_STATUS.put("Single", "Single");
        MARITAL_STATUS.put("Widow", "WIDOW");
        MARITAL_STATUS.put("Divorced", "DIVORCED");

        //OCCUPANCY_MASTER_STATUS
        OCCUPANCY_MASTER_STATUS.put("1000000001", "Rented");
        OCCUPANCY_MASTER_STATUS.put("1000000002", "Self-Occupied");
        OCCUPANCY_MASTER_STATUS.put("1000000003", "Vacant");
        OCCUPANCY_MASTER_STATUS.put("Rented", "1000000001");
        OCCUPANCY_MASTER_STATUS.put("Self-Occupied", "1000000002");
        OCCUPANCY_MASTER_STATUS.put("Vacant", "1000000003");

        INTEREST_TYPE.put("Floating", "FLOATING");
        INTEREST_TYPE.put("Fixed", "FIXED");

        //RepaymentMode
        REPAYMENT_MODE.put("Cash" , "Cash");
        REPAYMENT_MODE.put("Cheque" , "Cheque");
        REPAYMENT_MODE.put("DD" , "Cheque");
        REPAYMENT_MODE.put("PDC", "Cheque" );
        REPAYMENT_MODE.put("Fund Transfer" ,"ECS");
        REPAYMENT_MODE.put("NEFT" ,"ECS");
        REPAYMENT_MODE.put("RTGS" ,"ECS");
        REPAYMENT_MODE.put("IMPS" ,"ECS");
        REPAYMENT_MODE.put("NACH" ,"ECS");
        REPAYMENT_MODE.put("ACH" ,"ACH");

        //OWNERSHIP_TYPE
        OWNERSHIP_TYPE.put("Owned" ,"Owned");
        OWNERSHIP_TYPE.put("Others" ,"Other");
        OWNERSHIP_TYPE.put("CoProvided" ,"CoProvided");
        OWNERSHIP_TYPE.put("Rented" ,"Rented");
        OWNERSHIP_TYPE.put("CoProvided" ,"CoProvided");

    }

    public static final  Comparator<LoanSummaryDetail> sortByProspectCode = (LoanSummaryDetail o1, LoanSummaryDetail o2)
            -> o1.getPROSPECTCODE().compareTo(o2.getPROSPECTCODE());

    public final static String getCustomerEntityType(String applicantType) {
        String entityType = ApplicantType.CORPORATE.value();
        if (ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicantType)) {
            entityType = ApplicantType.INDIVIDUAL.value();
        }
        return entityType;
    }

    // ***********   TOPUP related constants *********

    public static final String EXISTINGLOANDETAILS = "Existing_Loan_detail";
    public static final String MIFIN_DEDUPE_DETAIL = "mifin dedupe detail";
    public static final String MIFINLOANDETAIL = "mifin-loan-detail";
    public static final String MIFINLMSDETAIL = "mifin-lms-detail";
    public static final String MIFINEMICALDETAIL = "mifin-cal-detail";

    public static final String TOPUP_PARALLEL = "Top Up Parallel";
    public static final String TOPUP_GROSS = "Top Up Gross";
    public static final String Normal = "Normal";
    public static final String LOAN_TYPE_A = "A";
    public static final String LOAN_TYPE_B = "B";

    public static final String AGENCY_CODE = "1004";
    public static final String INSTALLMENT_TYPE = "Monthly";
    public static final String USAGE = "Commercial";
    public static final String OCCUPANCYSTATUS = "Rented";
    public static final String VALUATIONSTATUS = "PENDING";
    public static final String BANK_ACCT_Type = "Saving";
    public static final String VERIFICATION_RESULT_POSITIVE = "Positive";
    public static final String VERIFICATION_RESULT_NEGATIVE = "Negative";
    public static final String BLANK_VALUE = "";
    public static final String PAN = "PAN";
    public static final String PERSONAL_MOBILE = "PERSONAL_MOBILE";
    public static final String PERSONAL = "PERSONAL";
    public static final String INDIVIDUAL = "Individual";
    // ***********   END TOPUP related constants *********
    public static final String MIFINEMICALCULATOR = "emiCalculator";

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    private ExternalApiManager externalApiManager;

    @Autowired
    private ExternalApiManagerImpl externalApiManagerImpl;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private MasterRepository masterRepository;

    public void checkAndGetMifinDedupeDetail(ApplicationRequest applicationRequest) {
        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(
                    applicationRequest.getRefID());
            Applicant applicant = null;
            List<CoApplicant> coApplicantList = null;
            if (applicationRequest.getRequest().getApplicant() != null) {
                boolean sameApplicantDtls = compareApplicant(applicationRequest.getRequest().getApplicant(),
                        goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant());
                if (!sameApplicantDtls) {
                    applicant = applicationRequest.getRequest().getApplicant();
                }
            }
            if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
                List<String> changedCoApplicantIds = compareCoApplicant(goNoGoCustomerApplication.getApplicationRequest()
                        .getRequest().getCoApplicant(), applicationRequest.getRequest().getCoApplicant());
                if (CollectionUtils.isNotEmpty(changedCoApplicantIds)) {
                    coApplicantList = new ArrayList<>();
                    for (CoApplicant coAppObj : applicationRequest.getRequest().getCoApplicant()) {
                        if (changedCoApplicantIds.contains(coAppObj.getApplicantId())) {
                            coApplicantList.add(coAppObj);
                        }
                    }
                }
            }
            if (applicant != null || CollectionUtils.isNotEmpty(coApplicantList)) {
                externalApiManager.sendDataToMifinDedupe(applicationRequest, applicant, coApplicantList);
            }

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
    }

    public List<String> compareCoApplicant(List<CoApplicant> coApplicantList, List<CoApplicant> coApplicantDatabaseList) {
        List<String> distinctApplicantId = new ArrayList<>();
        for (CoApplicant coApplicant : coApplicantList) {
            boolean isPresent = false;
            boolean notExist = true;
            for (CoApplicant coapplicantdb : coApplicantDatabaseList) {
                if (StringUtils.equalsIgnoreCase(coApplicant.getApplicantId(), coapplicantdb.getApplicantId())) {
                    isPresent = coApplicant.equalsCoApplicant(coapplicantdb);
                    notExist = false;
                }
            }
            if (notExist || !isPresent) {
                distinctApplicantId.add(coApplicant.getApplicantId());
            }
        }
        return distinctApplicantId;
    }

    public boolean compareApplicant(Applicant applicant, Applicant applicantDatabas) {
        return applicant.equalsApplicant(applicantDatabas);
    }

    public void fetchAllObjectsForRefId(String refId, String institutionId, MifinDedupeDetails mifinDedupeDetails, ApplicationRequest applicationRequest) throws Exception {

        mifinDedupeDetails.setApplicationRequest(applicationRequest);

        CamDetails camDetails = applicationRepository.fetchCamSummaryByRefId(refId, institutionId);
        if (camDetails == null) {
            camDetails = new CamDetails();
            camDetails.setRefId(refId);
            camDetails.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setCamDetails(camDetails);
/*
        Eligibility eligibility = applicationRepository.fetchEligibilityDetails(refId, institutionId);
        if (eligibility == null) {
            eligibility = new Eligibility();
            eligibility.setRefId(refId);
            eligibility.setInstitutionId(institutionId);
        }*/
        //mifinDedupeDetails.setEligibility(eligibility);

      /*  LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
        if (loanCharges != null) {
            loanCharges = new LoanCharges();
            loanCharges.setRefId(refId);
            loanCharges.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setLoanCharges(loanCharges);*/

        DisbursementMemo disbursementMemo = applicationRepository.fetchDMDetails(refId, institutionId);
        if (disbursementMemo == null) {
            disbursementMemo = new DisbursementMemo();
            disbursementMemo.setRefId(refId);
            disbursementMemo.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setDisbursementMemo(disbursementMemo);

        Repayment repayment = applicationRepository.fetchRepaymentDetails(refId, institutionId);
        if (repayment == null) {
            repayment = new Repayment();
            repayment.setRefId(refId);
            repayment.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setRepayment(repayment);

        ListOfDocs listOfDocs = applicationRepository.fetchListOfDocs(refId, institutionId);
        if (listOfDocs == null) {
            listOfDocs = new ListOfDocs();
            listOfDocs.setRefId(refId);
            listOfDocs.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setListOfDocs(listOfDocs);

        Valuation valuation = applicationRepository.fetchValuationDetails(refId, institutionId);
        if (valuation == null) {
            valuation = new Valuation();
            valuation.setRefId(refId);
            valuation.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setValuation(valuation);

       /* VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId , institutionId);
        if (verificationDetails == null){
            verificationDetails = new VerificationDetails();
            verificationDetails.setRefId(refId);
            verificationDetails.setInstitutionId(institutionId);
        }
        mifinDedupeDetails.setVerificationDetails(verificationDetails);*/

    }

    private static void populateDataInMifinDedupeDetails(DisbursementMemo disbursementMemo, MifinDedupeDetails mifinDedupeDetails, CamDetails camDetails,
                                                         Eligibility eligibility, Repayment repayment, Valuation valuation, ApplicationRequest applicationRequest,
                                                         ListOfDocsRequest listOfDocsRequest, VerificationDetails verificationDetails, LoanCharges loanCharges,
                                                         LegalVerification legalVerification) {
        mifinDedupeDetails.setApplicationRequest(applicationRequest);
        mifinDedupeDetails.setCamDetails(camDetails);
        mifinDedupeDetails.setDisbursementMemo(disbursementMemo);
       // mifinDedupeDetails.setEligibility(eligibility);
        mifinDedupeDetails.setListOfDocsRequest(listOfDocsRequest);
        mifinDedupeDetails.setRepayment(repayment);
        mifinDedupeDetails.setValuation(valuation);
        mifinDedupeDetails.setLoanCharges(loanCharges);
        mifinDedupeDetails.setLegalVerification(legalVerification);
        //mifinDedupeDetails.setVerificationDetails(verificationDetails);
    }

    //Mapping AppMetaData
    private static void populateAppMetaDataInAppRequest(LoanSummary loanSummary, ApplicationRequest applicationRequest) {
        if (applicationRequest.getAppMetaData() != null) {
            if (applicationRequest.getAppMetaData().getBranchV2() != null) {
                if (StringUtils.isNotEmpty(loanSummary.getBranch().trim())) {
                    applicationRequest.getAppMetaData().getBranchV2().setBranchName(loanSummary.getBranch());
                }
                if (StringUtils.isNotEmpty(loanSummary.getRelationShipManager().trim())) {
                    applicationRequest.getAppMetaData().getBranchV2().setBranchManagerName(loanSummary.getRelationShipManager());
                }
            } else {
                Branch branchV2 = new Branch();
                if (StringUtils.isNotEmpty(loanSummary.getBranch().trim())) {
                    branchV2.setBranchName(loanSummary.getBranch());
                }
                if (StringUtils.isNotEmpty(loanSummary.getRelationShipManager().trim())) {
                    branchV2.setBranchManagerName(loanSummary.getRelationShipManager());
                }
                applicationRequest.getAppMetaData().setBranchV2(branchV2);
            }
        } else {
            AppMetaData appMetaData = new AppMetaData();
            Branch branchV2 = new Branch();
            if (StringUtils.isNotEmpty(loanSummary.getBranch().trim())) {
                branchV2.setBranchName(loanSummary.getBranch());
            }
            if (StringUtils.isNotEmpty(loanSummary.getRelationShipManager())) {
                branchV2.setBranchManagerName(loanSummary.getRelationShipManager());
            }
            appMetaData.setBranchV2(branchV2);
            applicationRequest.setAppMetaData(appMetaData);
        }
    }


    //Mapping List of Docs
    private static ListOfDocsRequest populateDataInDocRequest(DateFormat format, LoanSummary loanSummary, List<DocDetail> docDetailList, ApplicationRequest applicationRequest, MifinDedupeDetails mifinDedupeDetails) throws ParseException {

        ListOfDocsRequest listOfDocsRequest = new ListOfDocsRequest();
        ListOfDocs listOfDocs = new ListOfDocs();
        listOfDocs.setInstitutionId(applicationRequest.getInstitutionId());
        listOfDocs.setRefId(applicationRequest.getRefID());

        listOfDocsRequest.setRefId(applicationRequest.getRefID());
        listOfDocsRequest.setHeader(applicationRequest.getHeader());
        listOfDocs.setDocDetails(docDetailList);
        listOfDocsRequest.setListOfDocs(listOfDocs);
        return listOfDocsRequest;
    }

    //PropertyValuation
    private static Valuation populateDataInValuation(DateFormat format, LoanSummary loanSummary,
                                                     MifinDedupeDetails mifinDedupeDetails, List<String> successRecords, int i, Application application) throws ParseException {
        logger.info("Populating data in Valuation");
        Valuation valuation = mifinDedupeDetails.getValuation();
        boolean valuationAdded = false;
        Collateral colletral = application.getCollateral().get(i);
        try {
            List<ValuationDetails> valuationDetailsList = (valuation.getValuationDetailsList() != null) ?
                    valuation.getValuationDetailsList() : new ArrayList<>();
            ValuationDetails valuationDetails = null;
            logger.info("before if value of i{}", i);
            if (CollectionUtils.isNotEmpty(valuationDetailsList) && (valuationDetailsList.size() > i)) {
                logger.info("in if value of i{}", i);
                valuationDetails = valuationDetailsList.get(i);
            } else {
                valuationDetails = new ValuationDetails();
                valuationAdded = true;
            }
            ValuationOutput valuationOutput = (valuationDetails.getValuationOutput() != null) ?
                    valuationDetails.getValuationOutput() : new ValuationOutput();
            //TODO @kislay or @Samruddhi remove comments from code and hardcoding
                /*if(StringUtils.isNotEmpty(loanSummary.getPROPERTYUSAGE().trim())){
                    valuationOutput.setPropertyUsage(loanSummary.getPROPERTYUSAGE());
                }*/
            valuationOutput.setPropertyUsage(OCCUPANCYSTATUS);

               /*if(StringUtils.isNotEmpty(loanSummary.getPROPERTYSTATUS().trim())){
                   valuationOutput.setPropertyOccupancy(loanSummary.getPROPERTYSTATUS());
               }*/
            valuationOutput.setPropertyOccupancy(OCCUPANCYSTATUS);
            valuationOutput.setCurrentPropertyOwner(new Name());
            valuationOutput.setPropertyAddress(new CustomerAddress());
            valuationOutput.setInspectionAddress(new CustomerAddress());
            valuationOutput.setPropertyWithinMC(false);
            valuationOutput.setDemarcation(false);
            if (StringUtils.isNotEmpty(loanSummary.getPRORESIDUALAGE().trim())) {
                valuationOutput.setResidualAge(loanSummary.getPRORESIDUALAGE());
            }
            if (StringUtils.isNotEmpty(loanSummary.getEVALUATIONDATE().trim())) {
                //    valuationOutput.setPropertyValuationDate(format.parse(loanSummary.getEVALUATIONDATE()));
            }

            //TODO @kislay & samruddhi remove below comments and hardcoding
                /*
                if(StringUtils.isNotEmpty(loanSummary.getVALUATORNAME().trim())){
                    valuationDetails.setAgencyCode(loanSummary.getVALUATORNAME());
                }*/
            valuationDetails.setAgencyCode(AGENCY_CODE);
            if (StringUtils.isNotEmpty(loanSummary.getVerificationStatus().trim())) {
                valuationDetails.setStatus(loanSummary.getVerificationStatus());
            }
            valuationDetails.setValuationOutput(valuationOutput);
            if (valuationDetails.getValuationInput() != null) {
                valuationDetails.setValuationInput(valuationDetails.getValuationInput());
            } else {
                logger.info("in valuationInput");
                valuationDetails.setCollateralId(colletral.getCollateralId());
                valuationDetails.setValuationInput(new ValuationInput());
                valuationDetails.getValuationInput().setPropertyAddress(colletral.getAddress());
                valuationDetails.getValuationInput().setPropertyOwnerNames(colletral.getOwnerNames());
                valuationDetails.getValuationInput().setPropertyType(colletral.getType());
                valuationDetails.getValuationInput().setApplicantName(mifinDedupeDetails.getApplicationRequest().getRequest().getApplicant().getApplicantName());
            }
            //For testing purpose

            //TODO remove hardcoding. It has been already mapped
            valuationDetails.setStatus(VALUATIONSTATUS);

                /*
                valuationDetails.setSubmissionTime(new Date());
                valuationDetails.setVerificationTime(new Date());*/
            if (valuationAdded) {
                valuationDetailsList.add(valuationDetails);
            }

            valuation.setValuationDetailsList(valuationDetailsList);
            logger.info("Valuation:{}", valuation);
            successRecords.add("Valuation");
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return valuation;
    }

    //PropertyVerification
    private static VerificationDetails populateDataInPropertyVerification(LoanSummary loanSummary, MifinDedupeDetails mifinDedupeDetails) {
        VerificationDetails verificationDetails = mifinDedupeDetails.getVerificationDetails() != null ? mifinDedupeDetails.getVerificationDetails() : new VerificationDetails();
        List<PropertyVerification> propertyVerificationList = verificationDetails.getResidenceVerification() != null ? verificationDetails.getResidenceVerification() : new ArrayList<>();
        PropertyVerification propertyVerification1 = null;
        PropertyVerificationInput propertyVerificationInput = null;
        PropertyVerificationOutput propertyVerificationOutput = null;
        int i = propertyVerificationList.size();
        if (propertyVerificationList.size() > 0) {
            propertyVerification1 = propertyVerificationList.get(0);
            if (propertyVerification1.getInput() != null) {
                propertyVerificationInput = propertyVerification1.getInput();
            } else {
                propertyVerificationInput = new PropertyVerificationInput();
            }
            if (propertyVerification1.getOutput() != null) {
                propertyVerificationOutput = propertyVerification1.getOutput();
            } else {
                propertyVerificationOutput = new PropertyVerificationOutput();
            }
            propertyVerificationList.remove(0);
        } else {
            propertyVerification1 = new PropertyVerification();
            propertyVerificationInput = new PropertyVerificationInput();
            propertyVerificationOutput = new PropertyVerificationOutput();
        }
        if (StringUtils.isNotEmpty(loanSummary.getVALUATORNAME().trim())) {
            propertyVerificationInput.setVerificationAgency(loanSummary.getVALUATORNAME());
        }
        if (StringUtils.isNotEmpty(loanSummary.getVerificationStatus().trim())) {
            propertyVerificationOutput.setStatus(loanSummary.getVerificationStatus());
        }
        propertyVerification1.setInput(propertyVerificationInput);
        propertyVerification1.setOutput(propertyVerificationOutput);
        propertyVerificationList.add(propertyVerification1);
        verificationDetails.setResidenceVerification(propertyVerificationList);
        return verificationDetails;
    }


    //Mapping DMInput
    private static void populateDataInDmInput(DisbursementMemo disbursementMemo, DateFormat format, LoanSummary loanSummary,
                                              List<String> successRecords) throws ParseException {
        logger.info("Populating Data in DMInput");
        try {
            DMInput dmInput = new DMInput();
            List<DMInput> dmInputList = new ArrayList<>();
            if (StringUtils.isNotEmpty(loanSummary.getINSTRUMENTTYPE().trim())) {
                dmInput.setDmPaymentMode(loanSummary.getINSTRUMENTTYPE());
            }
            if (StringUtils.isNotEmpty(loanSummary.getINSTRUMENTNO()) &&
                    StringUtils.isNotEmpty(loanSummary.getINSTRUMENTNO().trim())) {
                dmInput.setInstrumentNo(loanSummary.getINSTRUMENTNO());
            }
            if (StringUtils.isNotEmpty(loanSummary.getINSTRUMENTDATE().trim())) {
                dmInput.setDmDate(format.parse(loanSummary.getINSTRUMENTDATE()));
            }
            if (StringUtils.isNotEmpty(loanSummary.getDISBURSALAMOUNT().trim())) {
                dmInput.setDmAmt(loanSummary.getDISBURSALAMOUNT());
            }

            dmInputList.add(dmInput);
            disbursementMemo.setDmInputList(dmInputList);
            logger.info("DisbursementMemo:{}", disbursementMemo);
            successRecords.add("Disbursal Memo - Input");
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
    }


    public MifinDedupeDetails fillDedupeData(TopUpDedupeResponse topUpDedupeResponse, ApplicationRequest applicationRequest,
                                             MifinDedupeDetails mifinDedupeDetails, List<String> successRecords,TopUpDedupeRequest topUpDedupeRequest) {

        logger.info("Inside fillDedupeDetails");
       /* DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);*/
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        List<String> miFinOldProspectCode =  new ArrayList<>();
        LegalVerification legalVerification = null;
        LoanSummaryDetail  loanSummaryDetails = null;
        List<AssetDetails> assetDetailsList = new ArrayList<AssetDetails>();
        AssetDetail assetDetails = new AssetDetail();
        try {
            if (CollectionUtils.isNotEmpty(topUpDedupeResponse.getLoanSummaryDeatilList())) {
                List<LoanSummaryDetail> loanSummaryList = sortPropectCode(topUpDedupeResponse.getLoanSummaryDeatilList());
                DisbursementMemo disbursementMemo = mifinDedupeDetails.getDisbursementMemo();
             //   List<LoanSummaryDetail> loanSummaryList =  (List<LoanSummaryDetail>)topUpDedupeResponse.getLoanSummaryDeatilList();
                LoanSummaryDetail loanSummary = loanSummaryList.get(0);
                        /*JsonUtil.MapToObject((Map<String, Object>) topUpDedupeResponse.getLoanSummaryDeatilList().get(0), LoanSummaryDetail.class);*/
                CamDetails camDetails = populateDataInCamDetailsData(disbursementMemo, loanSummary.getCreditSanction(), mifinDedupeDetails, successRecords, applicationRequest);
               Eligibility eligibility =  null;/* populateDataInEligibilityData(loanSummary, mifinDedupeDetails, successRecords);
                eligibility.setRefId(applicationRequest.getRefID());
                eligibility.setInstitutionId(applicationRequest.getHeader().getInstitutionId());*/
                LoanCharges loanCharges = null;
               // populateDataInLoanChargesData(disbursementMemo, format, loanSummary, mifinDedupeDetails, successRecords);
                Repayment repayment = populateDataInRepaymentData(loanSummary, mifinDedupeDetails, successRecords);
                List<DocDetail> docDetailList = populateDataInDocDetailData(mifinDedupeDetails, loanSummary.getDocumnet(), successRecords, topUpDedupeResponse, format);
                VerificationDetails verificationDetails = null;
                Valuation valuation = null;
                //populateDataInPropertyVerification(loanSummary , mifinDedupeDetails);
                Request request = applicationRequest.getRequest() != null ? applicationRequest.getRequest() : new Request();
                TopUpInfo topUpInfo = applicationRequest.getRequest().getTopupInfo() != null ? applicationRequest.getRequest().getTopupInfo():new TopUpInfo();
                for(LoanSummaryDetail loanSummaryDetail : loanSummaryList){
                    loanSummaryDetails = loanSummaryDetail;
                          //  JsonUtil.MapToObject((Map<String, Object>)loanSummaryDetail, LoanSummaryDetail.class);
                    miFinOldProspectCode.add(loanSummaryDetails.getPROSPECTCODE());
                    assetDetails = loanSummaryDetails.getAssetDetails();
                    topUpInfo.setMiFinOldCustomerCode(loanSummaryDetails.getApplicantCode());
                    List<AssetDetails> tempList = assetDetails.getAssetDetailsList();
                    for(AssetDetails assetDetails1: tempList){
                        assetDetailsList.add(assetDetails1);
                    }
                }
                topUpInfo.setMiFinOldProspectCode(miFinOldProspectCode);
                topUpInfo.setTotalExposure(topUpDedupeResponse.getTotalExposure());
                topUpInfo.setValueToBeConsider(topUpDedupeResponse.getTotalValueToBeConsider());
                //sum of principal outstanding amount whose dedupeFlag is true(needed at eligibility screen for sbfc)
                if(Institute.isInstitute(topUpDedupeRequest.getHeader().getInstitutionId(),Institute.SBFC) &&
                        StringUtils.equalsIgnoreCase(topUpDedupeRequest.getHeader().getProduct().name(),Product.LAP.name()) &&
                        StringUtils.equalsIgnoreCase(topUpDedupeRequest.getLoanDetailTypeValue(),"Top Up Gross")){
                    topUpGrossPosHelper(topUpInfo,topUpDedupeRequest);
                }
                request.setTopupInfo(topUpInfo);
                assetDetails.setAssetDetailsList(assetDetailsList);
                Application application = populateDataInApplicationData(topUpDedupeResponse, loanSummary, request, successRecords, assetDetails);


                if(assetDetails != null && CollectionUtils.isNotEmpty(assetDetails.getAssetDetailsList())){
                    List<ValuationDetails> valuationDetailsList = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(assetDetails.getAssetDetailsList())) {
                        for (int i = 0; i < assetDetails.getAssetDetailsList().size(); i++) {
                                        valuation = populateDataInValuationData(format, assetDetails.getAssetDetailsList().get(i), mifinDedupeDetails, successRecords, i, application, valuationDetailsList);
                                        logger.info(" in valuation {}", i);
                                        mifinDedupeDetails.setValuation(valuation);
                                    }
                    }
                    applicationRepository.updateCompletedInfo(applicationRequest.getRefID(),applicationRequest.getHeader().getInstitutionId(),
                            EndPointReferrer.SAVE_VALUATION_DETAILS, applicationRequest.getHeader().getLoggedInUserId(), applicationRequest.getHeader().getLoggedInUserRole());
                }

                legalVerification = populateDataInLegalVerification(assetDetails, applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
               //refer for popolate Applicant Data
                ListOfDocsRequest listOfDocsRequest = populateDataInAppRequestData(mifinDedupeDetails, format, loanSummary, docDetailList, application, applicationRequest, request);
                populateDataInDmInputData(disbursementMemo, format, loanSummary, successRecords);
                if(CollectionUtils.isNotEmpty(topUpInfo.getMiFinOldProspectCode())){
                    String prospectCode = String.join(FieldSeparator.COMMA_STR, topUpInfo.getMiFinOldProspectCode());
                    disbursementMemo.setLinkLoan(prospectCode);
                }

                populateDataInMifinDedupeDetails(disbursementMemo, mifinDedupeDetails, camDetails, eligibility,
                        repayment, valuation, applicationRequest, listOfDocsRequest, verificationDetails, loanCharges, legalVerification);
                logger.info("MifinDedupeDetails:{}", mifinDedupeDetails);
            }

        } catch (Exception e) {
            logger.error("{} Error occoured while populate data execution - {}", applicationRequest.getRefID(),e.getStackTrace());
        }
        return mifinDedupeDetails;
    }

    private void topUpGrossPosHelper(TopUpInfo topUpInfo,TopUpDedupeRequest topUpDedupeRequest){
        double posSum = 0;
        List<com.softcell.gonogo.model.mifin.topup.Applicant> applicantList = topUpDedupeRequest.getApplicantList();
        if(CollectionUtils.isNotEmpty(applicantList)){
            for (com.softcell.gonogo.model.mifin.topup.Applicant applicant:applicantList){
                List<ProspectDetails> prospectDetailsList = applicant.getProspectList();
                if(CollectionUtils.isNotEmpty(prospectDetailsList)){
                    for(ProspectDetails prospectDetails : prospectDetailsList){
                        if(prospectDetails.dedupeFlag){
                            posSum += Double.parseDouble(prospectDetails.getProncipalOutstanding());
                        }
                    }
                }
            }
        }
        topUpInfo.setPrincipalOutstandingTotal(posSum);
    }

    private Valuation populateDataInValuationData(DateFormat format, AssetDetails assetDetails, MifinDedupeDetails mifinDedupeDetails, List<String> successRecords, int i, Application application,  List<ValuationDetails> valuationDetailsList) throws IOException {
        List<ValuationDetail>  mifinValuationDetailsList = new ArrayList<>();
        Valuation valuation = new Valuation();

        try{
            if(assetDetails.getValuationStructure() != null){
                if(assetDetails.getValuationStructure() instanceof List<?>){
                    List<ValuationDetail>  mifinValuationDetailsList1 = (List<ValuationDetail>) assetDetails.getValuationStructure();
                    logger.debug("inside populateDataInValuationData:1 {}",mifinValuationDetailsList1);
                    for(Object obj : mifinValuationDetailsList1){
                        if(obj != null && !obj.equals("")){
                            ValuationDetail valuationDetail = JsonUtil.MapToObject((Map<String, Object>) obj, ValuationDetail.class);
                            mifinValuationDetailsList.add(valuationDetail);
                        }
                    }
                }else if(! (assetDetails.getValuationStructure() instanceof String)){
                    ValuationDetail valuationDetail = JsonUtil.MapToObject((Map<String, Object>) assetDetails.getValuationStructure(), ValuationDetail.class);
                    mifinValuationDetailsList.add(valuationDetail);
                }
            }
        }catch (Exception e){
            logger.error("Exception In populate Valuation during topUpInfo:1 {}",e.getMessage());
            e.printStackTrace();
        }

        if(CollectionUtils.isNotEmpty(mifinValuationDetailsList)){
            logger.debug("inside populateDataInValuationData:2 {}",mifinValuationDetailsList);
            for(ValuationDetail mifnValuationDetail: mifinValuationDetailsList){
                logger.info("Populating data in Valuation Data");

                boolean valuationAdded = false;
                try {
                    Collateral colletral = application.getCollateral().get(i);
                    ValuationDetails valuationDetails = null;
                    logger.info("before if value of i{}", i);

                    valuationDetails = new ValuationDetails();
                    valuationAdded = true;
                    valuationDetails.setStatus(ThirdPartyVerification.Status.VERIFIED.name());
                    //set Agency hard codde
                    valuationDetails.setAgencyCode(VALUATOR_AGENCY_CODE);
                    ValuationOutput valuationOutput =  new ValuationOutput();
                    valuationDetails.setCollateralId(colletral.getCollateralId());
                   // valuationDetails.setWaived(true);
                    if (StringUtils.isNotEmpty(mifnValuationDetail.getPropertyUsage())) {
                        valuationOutput.setPropertyUsage(mifnValuationDetail.getPropertyUsage());
                    }
                    if (StringUtils.isNotEmpty(mifnValuationDetail.getPropertyStatus())) {
                        valuationOutput.setPropertyOccupancy(MiFinHelper.OCCUPANCY_STATUS.get(mifnValuationDetail.getPropertyStatus()));
                    }
                    valuationOutput.setCurrentMarketValue(mifnValuationDetail.getMarketValue());
                    valuationOutput.setCurrentPropertyOwner(new Name());
                    valuationOutput.setPropertyAddress(new CustomerAddress());
                    valuationOutput.setInspectionAddress(new CustomerAddress());
                    valuationOutput.setPropertyWithinMC(false);
                    valuationOutput.setDemarcation(false);
           /* if (StringUtils.isNotEmpty(assetDetails.getProResidualAge())) {
                valuationOutput.setResidualAge(assetDetails.getProResidualAge());
            }*/
                    if (StringUtils.isNotEmpty(mifnValuationDetail.getEvaluationDate())) {
                        valuationOutput.setPropertyValuationDate(format.parse(mifnValuationDetail.getEvaluationDate()));
                    }
           /* if (StringUtils.isNotEmpty(assetDetails.getValuatorName())) {
                valuationDetails.setAgencyCode(assetDetails.getValuatorName());
                valuationDetails.setAgencyName(assetDetails.getValuatorName());
            }*/
                    valuationDetails.setValuationOutput(valuationOutput);
                    if (valuationDetails.getValuationInput() != null) {
                        valuationDetails.setValuationInput(valuationDetails.getValuationInput());
                    } else {
                        logger.info("in valuationInput");
                        valuationDetails.setValuationInput(new ValuationInput());
                        valuationDetails.getValuationInput().setPropertyAddress(colletral.getAddress());
                        valuationDetails.getValuationInput().setPropertyOwnerNames(colletral.getOwnerNames());
                        valuationDetails.getValuationInput().setPropertyType(colletral.getType());
                        valuationDetails.getValuationInput().setApplicantName(mifinDedupeDetails.getApplicationRequest().getRequest().getApplicant().getApplicantName());
                    }
               /*
                valuationDetails.setSubmissionTime(new Date());
                valuationDetails.setVerificationTime(new Date());*/
                    if (valuationAdded) {
                        valuationDetailsList.add(valuationDetails);
                    }

                    valuation.setValuationDetailsList(valuationDetailsList);
                    logger.info("ValuationData:{}", valuation);
                    successRecords.add("ValuationData");

                } catch (Exception e) {
                    logger.error("Exception In populate Valuation during topUpInfo:2 {}",e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        return valuation;
    }

    private void populateDataInDmInputData(DisbursementMemo disbursementMemo, DateFormat format, LoanSummaryDetail loanSummary, List<String> successRecords) {
        logger.info("Populating Data in DMInput");
        try {
            /*DMInput dmInput = new DMInput();
            List<DMInput> dmInputList = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList())) {
                if (StringUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentType())) {
                    dmInput.setDmPaymentMode(getKey(MiFinHelper.PAYMENT_MODE, loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentType()));
                }
                if (StringUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentNo()) &&
                        StringUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentNo().trim())) {
                    dmInput.setInstrumentNo(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentNo());
                }
                if (StringUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentDate())) {
                    dmInput.setDmDate(format.parse(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getInstrumentDate()));
                }
                if (StringUtils.isNotEmpty(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getDisbursalAmount())) {
                    dmInput.setDmAmt(loanSummary.getDisbursalDetail().getDisbursalDetailsList().get(0).getDisbursalAmount());
                }
            }

            dmInputList.add(dmInput);
            disbursementMemo.setDmInputList(dmInputList);*/
            disbursementMemo.setLinkLoan(loanSummary.getPROSPECTCODE());
            disbursementMemo.setCustomerId(loanSummary.getApplicantCode());
            logger.info("DisbursementMemo:{}", disbursementMemo);
            successRecords.add("Disbursal Memo - Input");
        } catch (Exception e) {
            logger.error("{} in dm details " , e.getStackTrace());
        }
    }

    private ListOfDocsRequest populateDataInAppRequestData(MifinDedupeDetails mifinDedupeDetails, DateFormat format, LoanSummaryDetail loanSummary, List<DocDetail> docDetailList, Application application, ApplicationRequest applicationRequest, Request request) {
        logger.info("Populating data in ApplicationRequest");
        /*applicationRequest.getRequest().getApplication().setChannelType(loanSummary.getBasicInfo().getSourcingChannelType());
        */
        ListOfDocsRequest listOfDocsRequest = populateDataInDocRequestData(format, loanSummary, docDetailList, applicationRequest, mifinDedupeDetails);
        request.setApplication(application);
        populateDataInApplicantData(loanSummary, request);
        applicationRequest.setRequest(request);

        logger.info("ApplicationRequest:{}", applicationRequest);
        logger.info("listOfDocRequest:{}", listOfDocsRequest);
        return listOfDocsRequest;
    }

    private void populateDataInApplicantData(LoanSummaryDetail loanSummary, Request request) {

        //comment will remove after conformation demographic and sourcing screen population
        Applicant applicant = (request.getApplicant() != null) ? request.getApplicant() : new Applicant();
        List<AddressDetails> customerAddrList = new ArrayList<AddressDetails>();


        //Mapping ProfessionalIncomeDetails
        ProfessionIncomeDetails professionIncomeDetails = (applicant.getProfessionIncomeDetails() != null) ? applicant.getProfessionIncomeDetails() : new ProfessionIncomeDetails();
        if (StringUtils.isNotEmpty(request.getApplication().getTopupType())) {
            professionIncomeDetails.setLoanScheme(request.getApplication().getTopupType());
        } else {
            professionIncomeDetails.setLoanScheme(loanSummary.getCreditSanction().getSchemeId());
        }
        applicant.setProfessionIncomeDetails(professionIncomeDetails);
        List<Employment> employmentList = new ArrayList<>();
        Employment employment = new Employment();
        List<CustomerDetails> customerDetailList = loanSummary.getCustomer().getCustomerDetailList();
        if (CollectionUtils.isNotEmpty(customerDetailList)) {
            if (StringUtils.isNotEmpty(customerDetailList.get(0).getNetIncome())) {
                employment.setMonthlySalary(Double.parseDouble(GngUtils.setValueForZero(customerDetailList.get(0).getNetIncome())));
                employment.setEmploymentType(getKey(OCCUPATION_TYPE, customerDetailList.get(0).getOccupationType()));
                employment.setTotalYearsOfExperience(Integer.parseInt(GngUtils.setValueForZero(customerDetailList.get(0).getWorkExperience())));
                if(CollectionUtils.isNotEmpty(customerDetailList.get(0).getAddressDetailList())){
                    List<AddressDetails> addressDetailList = customerDetailList.get(0).getAddressDetailList();
                    for(AddressDetails addressDetails : addressDetailList){
                        if(StringUtils.isNotEmpty(addressDetails.getAddressType())){
                          String type =  getKey(MiFinHelper.ADDRESS_TYPE,GngUtils.setValueForNull(addressDetails.getAddressType()));
                            if(StringUtils.equalsIgnoreCase(type , EndPointReferrer.ADDRESS_TYPE_OFFICE)) {
                                employment.setEmploymentName(GngUtils.setValueForNull(addressDetails.getNameOfCompany()));
                            }
                        }
                    }
                }
                if(StringUtils.equalsIgnoreCase(customerDetailList.get(0).getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                    employment.setDateOfJoining(GngUtils.setValueForNull(customerDetailList.get(0).getDob()));
                }
                employmentList.add(employment);
            }
        }


        //Mapping Customer Name
        Name name = applicant.getApplicantName() != null ? applicant.getApplicantName() : new Name();
        //Mapping Customer's Father Name
        Name fatherName = applicant.getFatherName() != null ? applicant.getFatherName() : new Name();
        //Mapping Customer's Spouse Name
        Name spouseName = applicant.getSpouseName() != null ? applicant.getSpouseName() : new Name();
        //Mappin MotherName For LAP,PL
        Name motherName = applicant.getMotherName() != null ? applicant.getMotherName() : new Name();
        //Mapping AddressDetails
        List<CustomerAddress> customerAddressList =  new ArrayList<>();
        List<LoanDetails> loanDetailsList =  new ArrayList<>();
        LoanDetails loanDetail = new LoanDetails();
        List<Kyc> kycList =  new ArrayList<>();

        List<Email> emailList = new ArrayList<Email>();
        List<CustomerAddress> custAddrList = new ArrayList<CustomerAddress>();
        List<Phone> phoneList =  new ArrayList<>();


        //For populate noOfBounce
        if(loanSummary.getLmsDetail() != null){
            loanDetail.setEmiBouncedCountInAYear(Integer.parseInt(loanSummary.getLmsDetail().getNoOfBounce()));
            loanDetailsList.add(loanDetail);
            applicant.setLoanDetails(loanDetailsList);
        }
        if (CollectionUtils.isNotEmpty(customerDetailList)) {
            if (customerDetailList.get(0) != null) {
                CustomerDetails customerDetails = customerDetailList.get(0);
                applicant.setOldCustomerCode(GngUtils.setValueForBlank(customerDetails.getCustomerCode()));
                name.setFirstName(GngUtils.setValueForNull(customerDetails.getFirstName()).toUpperCase());
                name.setMiddleName(GngUtils.setValueForNull(customerDetails.getMiddleName()).toUpperCase());
                name.setLastName(GngUtils.setValueForNull(customerDetails.getLastName()).toUpperCase());
                fatherName.setFirstName(GngUtils.setValueForNull(customerDetails.getFatherFname()).toUpperCase());
                fatherName.setMiddleName(GngUtils.setValueForNull(customerDetails.getFatherMname()).toUpperCase());
                fatherName.setLastName(GngUtils.setValueForNull(customerDetails.getFatherLname()).toUpperCase());
                spouseName.setFirstName(GngUtils.setValueForNull(customerDetails.getSpouseFirstname()).toUpperCase());
                spouseName.setMiddleName(GngUtils.setValueForNull(customerDetails.getSpouseMiddlename()).toUpperCase());
                spouseName.setLastName(GngUtils.setValueForNull(customerDetails.getSpouseLastname()).toUpperCase());
                motherName.setFirstName(GngUtils.setValueForNull(customerDetails.getMotherMaidenName()).toUpperCase());
                applicant.setMothersMaidenName(GngUtils.setValueForNull(customerDetails.getMotherMaidenName()).toUpperCase());
                applicant.setGender(GngUtils.setValueForBlank(customerDetails.getGender()));
                if (CollectionUtils.isNotEmpty(customerDetailList.get(0).getAddressDetailList())) {
                    customerAddrList = customerDetailList.get(0).getAddressDetailList();
                    for (AddressDetails addressDetails : customerAddrList) {
                        CustomerAddress customerAddress = new CustomerAddress();
                        Email email = new Email();
                        Phone phone = new Phone();
                        customerAddress.setAddressType(getKey(MiFinHelper.ADDRESS_TYPE, addressDetails.getAddressType()));
                        customerAddress.setResidenceAddressType(MiFinHelper.OWNERSHIP_TYPE.get(
                                GngUtils.setValueForNull(addressDetails.getOccupancyStatus())));
                        customerAddress.setAddressLine1(GngUtils.setValueForNull(addressDetails.getAdd1()));
                        customerAddress.setAddressLine2(GngUtils.setValueForNull(addressDetails.getAdd2()));
                        customerAddress.setCity(GngUtils.setValueForNull(addressDetails.getAddCity()));
                        customerAddress.setState(GngUtils.setValueForNull(addressDetails.getAddState()));
                        customerAddress.setDistrict(GngUtils.setValueForNull(addressDetails.getAddDist()));
                        customerAddress.setPin(Long.parseLong(GngUtils.setValueForZero(addressDetails.getAddPincode())));
                        customerAddress.setLandMark(GngUtils.setValueForNull(addressDetails.getAddLandmark()));
                        customerAddress.setTimeAtAddress(Integer.parseInt(GngUtils.setValueForZero(addressDetails.getAddYearsOfStay())));
                        if(StringUtils.equalsIgnoreCase(customerDetails.getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                            if(StringUtils.equalsIgnoreCase("Office Address",addressDetails.getAddressType())){
                                custAddrList.add(0,customerAddress);
                            }
                        }else{
                            custAddrList.add(customerAddress);
                        }
                        if(CollectionUtils.isNotEmpty(phoneList)){
                            phone.setPhoneNumber( phoneList.stream()
                                    .filter( obj -> !StringUtils.equalsIgnoreCase(obj.getPhoneNumber(),addressDetails.getAddMobile()))
                                    .findAny()
                                    .map(v -> addressDetails.getAddMobile())
                                    .orElse(""));
                            if(StringUtils.isNotEmpty(phone.getPhoneNumber())){
                                phone.setPhoneType((GNGWorkflowConstant.PERSONAL.toFaceValue()));
                                phoneList.add(phone);
                            }
                        }else{
                            phone.setPhoneType(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue());
                            phone.setPhoneNumber(addressDetails.getAddMobile());
                            phoneList.add(phone);
                        }

                        if(CollectionUtils.isNotEmpty(emailList)){
                            email.setEmailAddress( emailList.stream()
                                    .filter( obj -> !StringUtils.equalsIgnoreCase(obj.getEmailAddress(),addressDetails.getAddEmail()))
                                    .findAny()
                                    .map(v -> addressDetails.getAddEmail())
                                    .orElse(""));
                            if(StringUtils.isNotEmpty(email.getEmailAddress())){
                                email.setEmailType((GNGWorkflowConstant.WORK.toFaceValue()));
                                emailList.add(email);
                            }
                        }else{
                            email.setEmailType(GNGWorkflowConstant.PERSONAL.toFaceValue());
                            if(!StringUtils.equalsIgnoreCase(addressDetails.getAddEmail(),FieldSeparator.HYPHEN)){
                                email.setEmailAddress(addressDetails.getAddEmail());
                            }
                            emailList.add(email);
                        }

                    }
                }
                if (customerDetails.getAddharNo() != null) {
                    Kyc kyc = new Kyc();
                    kyc.setKycNumber(customerDetails.getAddharNo());
                    kyc.setKycName(GNGWorkflowConstant.AADHAAR.name());
                    kycList.add(kyc);
                }
                if (customerDetails.getPanNo() != null) {
                    Kyc kyc = new Kyc();
                    kyc.setKycNumber(customerDetails.getPanNo());
                    kyc.setKycName(KYC_TYPES.PAN.name());
                    kycList.add(kyc);
                }
                if(StringUtils.equalsIgnoreCase(customerDetails.getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                    applicant.setApplicantType(ApplicantType.PROPRIETORSHIP.value());
                }else{
                    applicant.setApplicantType(MiFinHelper.getCustomerEntityType(customerDetails.getCustomerEntityType()));
                    if (StringUtils.isNotEmpty(customerDetails.getDob())) {
                        applicant.setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                        (GngDateUtil.yyyy_MM_dd,
                                                customerDetails.getDob()),
                                GngDateUtil.ddMMyyyy));
                    } else {
                        applicant.setDateOfBirth(Constant.BLANK);

                    }
                }


                applicant.setEducation(getKey(MiFinHelper.QUALIFICATION, customerDetails.getQualification()));
                applicant.setNoOfDependents(Integer.parseInt(GngUtils.setValueForZero(customerDetails.getNoOfDependents())));
                //applicant.setTanNo(GngUtils.setValueForNull(customerDetails.getTanNo()));
                applicant.setGstNo(GngUtils.setValueForNull(customerDetails.getGstIn()));
                applicant.setMaritalStatus(getKey(MARITAL_STATUS,customerDetails.getMaritalStatus()));
            }

            applicant.setApplicantId("0");
            applicant.setApplicantName(name);
            applicant.setFatherName(fatherName);
            applicant.setSpouseName(spouseName);
            applicant.setMotherName(motherName);
            applicant.setAddress(custAddrList);
            applicant.setKyc(kycList);
            applicant.setPhone(phoneList);
            applicant.setEmail(emailList);
            applicant.setEmployment(employmentList);
        }
        List <CoApplicant> coApplicantList = new ArrayList<>();
        coApplicantList = populateCoApplicantData(coApplicantList ,customerDetailList);
        if(CollectionUtils.isNotEmpty(coApplicantList)){
            request.setCoApplicant(coApplicantList);
        }
        request.setApplicant(applicant);
        if(CollectionUtils.isNotEmpty(coApplicantList)){
            request.setCoApplicant(coApplicantList);
        }

    }


    private ListOfDocsRequest populateDataInDocRequestData(DateFormat format, LoanSummaryDetail loanSummary, List<DocDetail> docDetailList, ApplicationRequest applicationRequest, MifinDedupeDetails mifinDedupeDetails) {
        ListOfDocsRequest listOfDocsRequest = new ListOfDocsRequest();
        DocDetail docDetail = new DocDetail();
        ListOfDocs listOfDocs = new ListOfDocs();
        listOfDocs.setInstitutionId(applicationRequest.getInstitutionId());
        listOfDocs.setRefId(applicationRequest.getRefID());
        listOfDocsRequest.setRefId(applicationRequest.getRefID());
        listOfDocsRequest.setHeader(applicationRequest.getHeader());
        listOfDocs.setDocDetails(docDetailList);
        listOfDocsRequest.setListOfDocs(listOfDocs);
        return listOfDocsRequest;
    }

    private Application populateDataInApplicationData(TopUpDedupeResponse topUpDedupeResponse, LoanSummaryDetail loanSummary, Request request, List<String> successRecords, AssetDetail assetDetails ) {

        logger.info("Populating data in ApplicationData");
        Application application = (request.getApplication() != null) ? request.getApplication() : new Application();

        try {
            List<Collateral> collateralList = new ArrayList<>();
            collateralList = populateCollateralInApplicationData(assetDetails, collateralList);
             /* application.setLoanPurpose(loanSummary.getBasicInfo().getPurposeOfLoan());*/
            if (loanSummary.getCreditSanction() != null) {
                double loanAmount = StringUtils.isNotEmpty(loanSummary.getCreditSanction().getAppliedLoanAmount()) ? Double.parseDouble(loanSummary.getCreditSanction().getAppliedLoanAmount()) : 0;
                application.setInstallmentType(loanSummary.getCreditSanction().getSchemeId());
            }
            application.setTopupType(topUpDedupeResponse.getTopUpType());
            application.setCollateral(collateralList);
            logger.info("Application:{}", application);
            successRecords.add("Application DetailsData");
        } catch (Exception e) {
            logger.info("Exception in application {} ", e.getStackTrace());
        }
        return application;
    }

    private List<Collateral> populateCollateralInApplicationData(AssetDetail assetDetail, List<Collateral> collateralList) {
        Collateral collateral = null;
        try {

               if (assetDetail != null) {
                   List<AssetDetails> assetDetailsList = assetDetail.getAssetDetailsList();
                   for (int i = 0; i < assetDetailsList.size(); i++) {
                       boolean collateralAdded = false;
                       AssetDetails assetDetails = assetDetailsList.get(i);
                       collateral = new Collateral();
                       collateralAdded = true;

                       CustomerAddress collateralCustomerAddress = new CustomerAddress();
                       if (StringUtils.isNotEmpty(assetDetails.getPropertyAddress1())) {
                           collateralCustomerAddress.setAddressLine1(assetDetails.getPropertyAddress1());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getPropertyAddress2())) {
                           collateralCustomerAddress.setAddressLine2(assetDetails.getPropertyAddress2());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getLandmark()) && StringUtils.isNotEmpty(assetDetails.getLandmark().trim())) {
                           collateralCustomerAddress.setLandMark(assetDetails.getLandmark());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getCity())) {
                           collateralCustomerAddress.setCity(assetDetails.getCity());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getState())) {
                           collateralCustomerAddress.setState(assetDetails.getState());
                       }
                       long pin = StringUtils.isNotEmpty(assetDetails.getZipCode()) ? Long.parseLong(assetDetails.getZipCode()) : 0;
                       collateralCustomerAddress.setPin(pin);
                       collateral.setAddress(collateralCustomerAddress);

                       collateral.setCollateralId(Integer.toString(i));
                       logger.info("Colletral id {}", collateral.getCollateralId());
                       List<Name> nameList = new ArrayList<>();
                       Name name = new Name();
                       if (StringUtils.isNotEmpty(assetDetails.getOwnerName())) {
                           name.setFirstName(assetDetails.getOwnerName());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getOwnerMname())) {
                           name.setMiddleName(assetDetails.getOwnerMname());
                       }
                       if (StringUtils.isNotEmpty(assetDetails.getOwnerLname())) {
                           name.setLastName(assetDetails.getOwnerLname());
                       }
                       // TODO @Kislay or @Samruddhi remove hardcoding and use below specified if conditions to set PropertyType
                       if (CollectionUtils.isNotEmpty(assetDetails.getValuationDetailList())) {
                           collateral.setUsage(GngUtils.setValueForBlank(assetDetails.getValuationDetailList().get(0).getPropertyUsage()));
                       }
                       nameList.add(name);
                       collateral.setOwnerNames(nameList);
                       if (collateralAdded) {
                           collateralList.add(collateral);
                       }
                   }
               }
           } catch (Exception ex) {
               ex.printStackTrace();
           }
       return collateralList;

    }

    private List<DocDetail> populateDataInDocDetailData(MifinDedupeDetails mifinDedupeDetails, Documnet documnet, List<String> successRecords, TopUpDedupeResponse topUpDedupeResponse, DateFormat format) {
        logger.info("Populating data in DocDetailData");
        ListOfDocs listOfDocs = mifinDedupeDetails.getListOfDocs() != null ?
                mifinDedupeDetails.getListOfDocs() : new ListOfDocs();
        List<DocDetail> docDetailList = new ArrayList<>();
        DocDetail docDetail = null;
        try {

            if (documnet != null && CollectionUtils.isNotEmpty(documnet.getDocumnetDetialList())) {
                for (int i = 0; i < documnet.getDocumnetDetialList().size(); i++) {
                    boolean docDetailAdded = false;
                    if (i < docDetailList.size()) {
                        docDetail = docDetailList.get(i);
                    } else {
                        docDetail = new DocDetail();
                        docDetailAdded = true;
                    }
                    if (StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentType()) &&
                            StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentType().trim())) {
                        docDetail.setDocType(documnet.getDocumnetDetialList().get(i).getDocumentType().trim());
                    }

                    if (StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentName()) &&
                            StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentName().trim())) {
                        docDetail.setDesc(documnet.getDocumnetDetialList().get(i).getDocumentName().trim());
                    }
                    if (StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getReceivedDate()) &&
                            StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getReceivedDate().trim())) {
                        docDetail.setDocDate(format.parse(documnet.getDocumnetDetialList().get(i).getReceivedDate().trim()));
                    }
                    if (StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getStatus()) &&
                            StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getStatus().trim())) {
                        docDetail.setOpsDocStatus(documnet.getDocumnetDetialList().get(i).getStatus().trim());
                    }
                    List<Remark> remarkList =  new ArrayList<>();
                    Remark remark = null;
                    boolean remarkAdded = false;
                    if (CollectionUtils.isNotEmpty(remarkList)) {
                        remark = remarkList.get(0);

                    } else {
                        remark = new Remark();
                        remarkAdded = true;
                    }
                    if (StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentDescription()) &&
                            StringUtils.isNotEmpty(documnet.getDocumnetDetialList().get(i).getDocumentDescription().trim())) {
                        //remark.setComment(topUpDedupeResponse.getLoanSummaryList().get(i).getDocument_Remarks());
                        remark.setComment(documnet.getDocumnetDetialList().get(i).getDocumentDescription());
                    }
                    if (remarkAdded) {
                        remarkList.add(remark);
                    }
                    docDetail.setRemarks(remarkList);
                    if (docDetailAdded) {
                        docDetailList.add(docDetail);
                    }
                }
            }
            logger.info("DocDetail List:{}", docDetailList);
            successRecords.add("Document DetailsData");
        } catch (Exception ex) {
            logger.error("while populating Document Details ", ex.getStackTrace());
        }
        return docDetailList;
    }


    private Repayment populateDataInRepaymentData(LoanSummaryDetail loanSummary, MifinDedupeDetails mifinDedupeDetails, List<String> successRecords) {

        logger.info("Populating data in Repayment");
        Repayment repayment = mifinDedupeDetails.getRepayment();
        try {
            List<RepaymentDetails> repaymentDetailsList = new ArrayList<>();
            RepaymentDetails repaymentDetails = null;
            if (CollectionUtils.isNotEmpty(repaymentDetailsList)) {
                repaymentDetails = repaymentDetailsList.get(0);
                repaymentDetailsList.remove(0);
            } else {
                repaymentDetails = new RepaymentDetails();
            }
            if (CollectionUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList())) {
                if (StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getIfscCode()) &&
                        StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getIfscCode().trim())) {
                    repaymentDetails.setIfscCode(loanSummary.getBankDetail().getBankDetailList().get(0).getIfscCode());
                }
                if (StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getMicr()) &&
                        StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getMicr().trim())) {
                    repaymentDetails.setmICRCode(loanSummary.getBankDetail().getBankDetailList().get(0).getMicr());
                }
                if (StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getCustomerAcNumber()) &&
                        StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getCustomerAcNumber().trim())) {
                    repaymentDetails.setAccountNumber(loanSummary.getBankDetail().getBankDetailList().get(0).getCustomerAcNumber());
                }
                if (StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getAccountType())
                        && StringUtils.isNotEmpty(loanSummary.getBankDetail().getBankDetailList().get(0).getAccountType().trim())) {
                    repaymentDetails.setAccountType(getKey(MiFinHelper.BANK_ACCT_TYPE, loanSummary.getBankDetail().getBankDetailList().get(0).getAccountType()));
                }
            }
            repaymentDetailsList.add(repaymentDetails);
            repayment.setRepaymentDetailsList(repaymentDetailsList);
            logger.info("Repayment:{}", repayment);
            successRecords.add("Repayment Details");
        } catch (Exception e) {
            logger.error("{} while populating repayment in methode " , e.getStackTrace());
        }
        return repayment;
    }

    private LoanCharges populateDataInLoanChargesData(DisbursementMemo disbursementMemo, DateFormat format, LoanSummaryDetail loanSummary, MifinDedupeDetails mifinDedupeDetails, List<String> successRecords) {
        logger.info("Populating data in LoanChargesData");
        LoanCharges loanCharges = new LoanCharges();
        try {

            TenureDetails tenureDetails =  new TenureDetails();
            if (loanSummary.getCreditSanction() != null) {
                if (StringUtils.isNotEmpty(loanSummary.getCreditSanction().getTenor()) &&
                        StringUtils.isNotEmpty(loanSummary.getCreditSanction().getTenor().trim())) {
                    tenureDetails.setDisbTenureMonths(Integer.parseInt(loanSummary.getCreditSanction().getTenor()));
                }
                loanCharges.setTenureDetails(tenureDetails);
                InterestDetails interestDetails = (loanCharges.getInterestDetails() != null) ?
                        loanCharges.getInterestDetails() : new InterestDetails();
                if (StringUtils.isNotEmpty(loanSummary.getCreditSanction().getRoi()) &&
                        StringUtils.isNotEmpty(loanSummary.getCreditSanction().getRoi().trim())) {
                    interestDetails.setDisbInterestRate(Double.parseDouble(loanSummary.getCreditSanction().getRoi()));
                }
                loanCharges.setInterestDetails(interestDetails);
                if (CollectionUtils.isNotEmpty(loanSummary.getCharges().getChargeDetailsList())) {
                    double gstAmount = StringUtils.isNotEmpty(loanSummary.getCharges().getChargeDetailsList().get(0).getGstOnCharge().trim()) ? Double.parseDouble(loanSummary.getCharges().getChargeDetailsList().get(0).getGstOnCharge()) : 0;
                    loanCharges.setGstAmt(gstAmount);
                }
                if (StringUtils.isNotEmpty(loanSummary.getCreditSanction().getInterestType()) &&
                        StringUtils.isNotEmpty(loanSummary.getCreditSanction().getInterestType().trim())) {
                    loanCharges.setTypeOfROI(MiFinHelper.INTEREST_TYPE.get(loanSummary.getCreditSanction().getInterestType()));
                }
                if (StringUtils.isNotEmpty(loanSummary.getCreditSanction().getEmiStartDate()) &&
                        StringUtils.isNotEmpty(loanSummary.getCreditSanction().getEmiStartDate().trim())) {
                    loanCharges.setEmiStartDate(format.parse(loanSummary.getCreditSanction().getEmiStartDate()));
                }

                if (StringUtils.isNotEmpty(loanSummary.getCreditSanction().getDisbursalDate()) &&
                        StringUtils.isNotEmpty(loanSummary.getCreditSanction().getDisbursalDate().trim())) {
                    loanCharges.setDisbursedDate(format.parse(loanSummary.getCreditSanction().getDisbursalDate()));
                }
            }
            LoanAmtDetails loanAmtDetails = new LoanAmtDetails();
            Remark remark = new Remark();
            loanAmtDetails.setRemarks(remark);

            List<com.softcell.gonogo.model.ops.LoanDetails> loanDetailsList = (loanCharges.getLoanChargesDetails() != null) ?
                    loanCharges.getLoanChargesDetails() : new ArrayList<>();
            com.softcell.gonogo.model.ops.LoanDetails loanDetails = null;

            boolean loanDetailsAdded = false;
            if (CollectionUtils.isNotEmpty(loanDetailsList)) {
                loanDetails = loanDetailsList.get(0);
            } else {
                loanDetails = new com.softcell.gonogo.model.ops.LoanDetails();
                loanDetailsAdded = true;
            }
            if (CollectionUtils.isNotEmpty(loanSummary.getCharges().getChargeDetailsList())) {
                List<ChargeDetails> chargeDetailsList = loanSummary.getCharges().getChargeDetailsList();
                if(CollectionUtils.isNotEmpty(chargeDetailsList)){
                    for(ChargeDetails chargeDetail : chargeDetailsList){
                        if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_CERSAI,chargeDetail.getChargeId())){
                            loanCharges.setCersaiFees(Double.parseDouble(chargeDetail.getChargeAmount()));
                        }else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_ADVANCE_EMI,chargeDetail.getChargeId())){
                            loanCharges.setAdvanceEMI(Double.parseDouble(chargeDetail.getChargeAmount()));
                        }else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_IMD,chargeDetail.getChargeId())){
                            loanCharges.setImdCollectedAmt(Double.parseDouble(chargeDetail.getChargeAmount()));
                        }else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_IMD2,chargeDetail.getChargeId())){
                            loanCharges.setImdCollectedAmt(Double.parseDouble(chargeDetail.getChargeAmount()));
                        } else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_PROCESSING,chargeDetail.getChargeId())){
                            loanCharges.setProFeesAmt(Double.parseDouble(chargeDetail.getChargeAmount()));
                        }else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_CREDITVIDYA,chargeDetail.getChargeId())){
                            loanCharges.setCreditVidyaCharge(chargeDetail.getChargeAmount());
                        }else if(StringUtils.isNotEmpty(chargeDetail.getChargeAmount()) &&
                                StringUtils.equalsIgnoreCase(MiFinHelper.CHARGE_FEES_AMT_CREDITVIDYA,chargeDetail.getChargeId())){
                            loanCharges.setCreditVidyaCharge(chargeDetail.getChargeAmount());
                        }
                        loanCharges.setGstAmt(Double.parseDouble(chargeDetail.getGstOnCharge()));

                    }
                }
                if (StringUtils.isNotEmpty(loanSummary.getCharges().getChargeDetailsList().get(0).getChargeId())) {
                    loanDetails.setChargeName(loanSummary.getCharges().getChargeDetailsList().get(0).getChargeId());
                }
                double chargeAmount = StringUtils.isNotEmpty(loanSummary.getCharges().getChargeDetailsList().get(0).getChargeAmount()) ? Double.parseDouble(loanSummary.getCharges().getChargeDetailsList().get(0).getChargeAmount()) : 0;
                loanDetails.setChargeAmount(chargeAmount);
            }
            if (loanDetailsAdded) {
                loanDetailsList.add(loanDetails);
            }

            loanCharges.setLoanDetails(loanAmtDetails);
            loanCharges.setLoanChargesDetails(loanDetailsList);
            disbursementMemo.setLoanCharges(loanCharges);
            logger.info("Loan Charges:{}", loanCharges);
            successRecords.add("Loan Charges Data");
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return loanCharges;
    }

    private Eligibility populateDataInEligibilityData(LoanSummaryDetail loanSummary, MifinDedupeDetails mifinDedupeDetails, List<String> successRecords) {

        logger.info("Populating data in EligibilityData");
        Eligibility eligibility = new Eligibility(); //mifinDedupeDetails.getEligibility();
        try {
            List<EligibilityDetails> eligibilityList =  new ArrayList<>();
            EligibilityDetails eligibilityDetails = null;
            boolean eligibilityAdded = false;
            if (CollectionUtils.isNotEmpty(eligibilityList)) {
                eligibilityDetails = eligibilityList.get(0);
            } else {
                eligibilityDetails = new EligibilityDetails();
                eligibilityAdded = true;
            }
            FoirDetail foirDetail = (eligibilityDetails.getFoirDetail() != null) ? eligibilityDetails.getFoirDetail() : new FoirDetail();
            if (loanSummary.getCreditSanction() != null) {
                double aprvAmount = StringUtils.isNotEmpty(loanSummary.getCreditSanction().getSanctionedAmount()) ? Double.parseDouble(loanSummary.getCreditSanction().getSanctionedAmount()) : 0;
                eligibility.setAprvLoan(aprvAmount);
                // eligibilityDetails.setAprvLoan(aprvAmount);
                double foir = StringUtils.isNotEmpty(loanSummary.getCreditSanction().getDbr()) ? Double.parseDouble(loanSummary.getCreditSanction().getDbr()) : 0;
                eligibility.setFoir(foir);
                if (CollectionUtils.isNotEmpty(loanSummary.getCustomer().getCustomerDetailList())) {
                    double grossIncome = StringUtils.isNotEmpty(loanSummary.getCustomer().getCustomerDetailList().get(0).getGrosIncome()) ? Double.parseDouble(loanSummary.getCustomer().getCustomerDetailList().get(0).getGrosIncome()) : 0;
                    eligibility.setGrossIncome(grossIncome);
                }
                //eligibilityDetails.setFoir(foir);
                double ltv = StringUtils.isNotEmpty(loanSummary.getCreditSanction().getLtv()) ? Double.parseDouble(loanSummary.getCreditSanction().getLtv()) : 0;
                foirDetail.setLtv(ltv);
                eligibilityDetails.setFoirDetail(foirDetail);
            }

            if (eligibilityAdded) {
                eligibilityList.add(eligibilityDetails);
            }
            eligibility.setEligibilityListDetails(eligibilityList);
            eligibility.setFoirDetail(foirDetail);

            if (CollectionUtils.isEmpty(eligibility.getEligibilityListDetails())) {
                List<IncomeDetail> incomeDetailList = new ArrayList<IncomeDetail>();
                incomeDetailList.add(new IncomeDetail());
                eligibility.setIncomeDetailList(incomeDetailList);
            }

            logger.info("Eligibility :{}", eligibility);
            successRecords.add("Eligibilty Details Data");
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return eligibility;
    }

    private CamDetails populateDataInCamDetailsData(DisbursementMemo disbursementMemo, CreditSanction creditSanction, MifinDedupeDetails mifinDedupeDetails, List<String> successRecords, ApplicationRequest applicationRequest) {
        logger.info("populating data in Cam Details Data");
        CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID(), applicationRequest.getInstitutionId(), EndPointReferrer.CAM_SUMMARY);
        try {
            CamSummary camSummary = camDetails.getSummary();

            if (creditSanction != null) {
                if (StringUtils.isNotEmpty(creditSanction.getSanctionApprovedBy()) && StringUtils.isNotEmpty(creditSanction.getSanctionApprovedBy().trim())) {
                    camSummary.setFinalLoanApprovedBy(creditSanction.getSanctionApprovedBy());
                }
                if (StringUtils.isNotEmpty(creditSanction.getFrequency())
                        && StringUtils.isNotEmpty(creditSanction.getFrequency().trim())) {
                    camSummary.setInstallmentType(getKey(MiFinHelper.FREQUENCY, creditSanction.getFrequency()));
                }
            }
            camDetails.setSummary(camSummary);
            disbursementMemo.setCamSummary(camSummary);
            logger.info("Cam Summary:{}", camSummary);
            successRecords.add("Cam Details Data");
        } catch (Exception e) {
            logger.error("populating camSummary Data {} ", e.getStackTrace());
        }
        return camDetails;
    }
    public List<CoApplicant> populateCoApplicantData(List<CoApplicant> coApplicantList, List<CustomerDetails> customerDetailList) {
        if (CollectionUtils.isNotEmpty(customerDetailList)) {
            //Mapping AddressDetails
            for (int i= 1; i<customerDetailList.size(); i++) {
                List<AddressDetails> customerAddrList = new ArrayList<AddressDetails>();
                List<Kyc> kycList =  new ArrayList<>();
                List<LoanDetails> loanDetailsList =  new ArrayList<>();

                LoanDetails loanDetail = new LoanDetails();
                loanDetailsList.add(loanDetail);
                List<Email> emailList = new ArrayList<Email>();
                List<CustomerAddress> custAddrList = new ArrayList<CustomerAddress>();
                List<Phone> phoneList =  new ArrayList<>();
                //Mapping Customer Name
                Name name =  new Name();
                //Mapping Customer's Father Name
                Name fatherName =  new Name();
                //Mapping Customer's Spouse Name
                Name spouseName = new Name();
                //Mappin MotherName For LAP,PL
                Name motherName =  new Name();
                CoApplicant coApplicant = new CoApplicant();
                coApplicant.setApplicantId(String.valueOf(i));
                coApplicant.setLoanDetails(loanDetailsList);
                CustomerDetails customerDetails = customerDetailList.get(i);
                coApplicant.setOldCustomerCode(GngUtils.setValueForBlank(customerDetails.getCustomerCode()));
                name.setFirstName(GngUtils.setValueForNull(customerDetails.getFirstName()).toUpperCase());
                name.setMiddleName(GngUtils.setValueForNull(customerDetails.getMiddleName()).toUpperCase());
                name.setLastName(GngUtils.setValueForNull(customerDetails.getLastName()).toUpperCase());
                fatherName.setFirstName(GngUtils.setValueForNull(customerDetails.getFatherFname()).toUpperCase());
                fatherName.setMiddleName(GngUtils.setValueForNull(customerDetails.getFatherMname()).toUpperCase());
                fatherName.setLastName(GngUtils.setValueForNull(customerDetails.getFatherLname()).toUpperCase());
                spouseName.setFirstName(GngUtils.setValueForNull(customerDetails.getSpouseFirstname()).toUpperCase());
                spouseName.setMiddleName(GngUtils.setValueForNull(customerDetails.getSpouseMiddlename()).toUpperCase());
                spouseName.setLastName(GngUtils.setValueForNull(customerDetails.getSpouseLastname()).toUpperCase());
                motherName.setFirstName(GngUtils.setValueForNull(customerDetails.getMotherMaidenName()).toUpperCase());
                coApplicant.setMothersMaidenName(GngUtils.setValueForNull(customerDetails.getMotherMaidenName()).toUpperCase());
                List<Employment> employmentList = new ArrayList<>();
                Employment employment =  new Employment();
                employment.setEmploymentType(getKey(OCCUPATION_TYPE,customerDetails.getOccupationType()));

                if (CollectionUtils.isNotEmpty(customerDetailList.get(i).getAddressDetailList())) {
                    customerAddrList = customerDetailList.get(i).getAddressDetailList();
                    for (AddressDetails addressDetails : customerAddrList) {
                        CustomerAddress customerAddress = new CustomerAddress();
                        Email email = new Email();
                        Phone phone = new Phone();
                        customerAddress.setAddressType(getKey(MiFinHelper.ADDRESS_TYPE, addressDetails.getAddressType()));
                        customerAddress.setResidenceAddressType(MiFinHelper.OWNERSHIP_TYPE.get(
                                GngUtils.setValueForNull(addressDetails.getOccupancyStatus())));
                        customerAddress.setAddressLine1(GngUtils.setValueForNull(addressDetails.getAdd1()));
                        customerAddress.setAddressLine2(GngUtils.setValueForNull(addressDetails.getAdd2()));
                        customerAddress.setCity(GngUtils.setValueForNull(addressDetails.getAddCity()));
                        customerAddress.setState(GngUtils.setValueForNull(addressDetails.getAddState()));
                        customerAddress.setDistrict(GngUtils.setValueForNull(addressDetails.getAddDist()));
                        customerAddress.setPin(Long.parseLong(addressDetails.getAddPincode()));
                        if(CollectionUtils.isNotEmpty(phoneList)){
                            phone.setPhoneNumber( phoneList.stream()
                                    .filter( obj -> !StringUtils.equalsIgnoreCase(obj.getPhoneNumber(),addressDetails.getAddMobile()))
                                    .findAny()
                                    .map(v -> addressDetails.getAddMobile())
                                    .orElse(""));
                            if(StringUtils.isNotEmpty(phone.getPhoneNumber())){
                                phone.setPhoneType((GNGWorkflowConstant.PERSONAL.toFaceValue()));
                                phoneList.add(phone);
                            }
                        }else{
                            phone.setPhoneType(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue());
                            phone.setPhoneNumber(addressDetails.getAddMobile());
                            phoneList.add(phone);
                        }

                        if(CollectionUtils.isNotEmpty(emailList)){
                            email.setEmailAddress( emailList.stream()
                                    .filter( obj -> !StringUtils.equalsIgnoreCase(obj.getEmailAddress(),addressDetails.getAddEmail()))
                                    .findAny()
                                    .map(v -> addressDetails.getAddEmail())
                                    .orElse(""));
                            if(StringUtils.isNotEmpty(email.getEmailAddress())){
                                email.setEmailType((GNGWorkflowConstant.WORK.toFaceValue()));
                                emailList.add(email);
                            }
                        }else{
                            email.setEmailType(GNGWorkflowConstant.PERSONAL.toFaceValue());
                            if(!StringUtils.equalsIgnoreCase(addressDetails.getAddEmail(),FieldSeparator.HYPHEN)){
                                email.setEmailAddress(addressDetails.getAddEmail());
                            }
                            emailList.add(email);
                        }
                        customerAddress.setTimeAtAddress(Integer.parseInt(GngUtils.setValueForZero(addressDetails.getAddYearsOfStay())));
                        if(StringUtils.equalsIgnoreCase(customerDetails.getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                            if(StringUtils.equalsIgnoreCase("Office Address",addressDetails.getAddressType())){
                                custAddrList.add(0,customerAddress);
                            }
                        }else{
                            custAddrList.add(customerAddress);
                        }

                    }
                }
                if (customerDetails.getAddharNo() != null) {
                    Kyc kyc = new Kyc();
                    kyc.setKycNumber(customerDetails.getAddharNo());
                    kyc.setKycName(GNGWorkflowConstant.AADHAAR.name());
                    kycList.add(kyc);
                }
                if (customerDetails.getPanNo() != null) {
                    Kyc kyc = new Kyc();
                    kyc.setKycNumber(customerDetails.getPanNo());
                    kyc.setKycName(KYC_TYPES.PAN.name());
                    kycList.add(kyc);

                }
                if(StringUtils.equalsIgnoreCase(customerDetails.getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                    coApplicant.setApplicantType(ApplicantType.PROPRIETORSHIP.value());
                    employment.setDateOfJoining(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                    (GngDateUtil.yyyy_MM_dd,
                                            customerDetails.getDob()),
                            GngDateUtil.ddMMyyyy));
                }else{
                    coApplicant.setApplicantType(MiFinHelper.getCustomerEntityType(customerDetails.getCustomerEntityType()));
                    if (StringUtils.isNotEmpty(customerDetails.getDob())) {
                        coApplicant.setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                        (GngDateUtil.yyyy_MM_dd,
                                                customerDetails.getDob()),
                                GngDateUtil.ddMMyyyy));
                    } else {
                        coApplicant.setDateOfBirth(Constant.BLANK);

                    }
                }
                coApplicant.setThirdPartyCall(new ThirdPartyCall());
                coApplicant.setDateOfBirth(customerDetails.getDob());
                employmentList.add(employment);

                coApplicant.setEducation(getKey(MiFinHelper.QUALIFICATION, customerDetails.getQualification()));
                coApplicant.setNoOfDependents(Integer.parseInt(GngUtils.setValueForZero(customerDetails.getNoOfDependents())));
                if(StringUtils.equalsIgnoreCase(customerDetails.getCustomerEntityType(), ApplicantType.CORPORATE.name())){
                    coApplicant.setApplicantType(ApplicantType.PROPRIETORSHIP.value());
                }else{
                    coApplicant.setApplicantType(MiFinHelper.getCustomerEntityType(customerDetails.getCustomerEntityType()));
                }
                //coApplicant.setTanNo(GngUtils.setValueForNull(customerDetails.getTanNo()));
                coApplicant.setGstNo(GngUtils.setValueForNull(customerDetails.getGstIn()));
                coApplicant.setGender(customerDetails.getGender());
                coApplicant.setMaritalStatus(getKey(MARITAL_STATUS,customerDetails.getMaritalStatus()));
                coApplicant.setApplicantName(name);
                coApplicant.setFatherName(fatherName);
                coApplicant.setSpouseName(spouseName);
                coApplicant.setMotherName(motherName);
                coApplicant.setAddress(custAddrList);
                coApplicant.setKyc(kycList);
                coApplicant.setPhone(phoneList);
                coApplicant.setEmail(emailList);
                coApplicant.setEmployment(employmentList);
                coApplicantList.add(coApplicant);
            }
        }
        return coApplicantList ;
    }

    public String getKey(Map<String, String> map, String value) {
        String key = null;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                key = entry.getKey().toString();
                break;
            }
        }
        return key;

    }

    public TopUpDedupeResponse getTempResponse() {
        String response = "{\n" +
                "  \"RESPONSE\" :{\n" +
                "    \"STATUS\": \"S\",\n" +
                "    \"MESSAGE\": \"Data processed successfully\",\n" +
                "      \"APPLICANTLIST\":   [{\n" +
                "        \"CUSTOMERCODE\":\"AP00000001\",\n" +
                "      \"PROSPECTLIST\":[{\n" +
                "       \"PROSPECTCODE\":\"PR00000001\",\n" +
                "       \"PRODUCTID\":\"1000000116\",\n" +
                "       \"SCHEMEID\":\"1000000169\",\n" +
                "       \"LOANAMOUNT\":\"5000\",\n" +
                "       \"DISBURSALDATE\":\"01-OCT-2018\",\n" +
                "       \"PRINCIPAL_OUTSTANDING\":\"4500\",\n" +
                "       \"EMI_OUTSTANDING\":\"500\",\n" +
                "       \"FORECLOSURE_INTEREST\":\"45\"\n" +
                "   },\n" +
                "     {\n" +
                "     \"PROSPECTCODE\":\"PR00000002\",\n" +
                "     \"PRODUCTID\":\"1000000116\",\n" +
                "     \"SCHEMEID\":\"10000001170\",\n" +
                "     \"LOANAMOUNT\":\"6000\",\n" +
                "     \"DISBURSALDATE\":\"07-OCT-2018\",\n" +
                "     \"PRINCIPAL_OUTSTANDING\":\"5500\",\n" +
                "     \"EMI_OUTSTANDING\":\"500\",\n" +
                "     \"FORECLOSURE_INTEREST\":\"50\"\n" +
                "     }]\n" +
                "},\n" +
                "{\n" +
                "   \"CUSTOMERCODE\":\"AP00000002\",\n" +
                "   \"PROSPECTLIST\":\n" +
                " [{\n" +
                "   \"PROSPECTCODE\":\"PR00000001\",\n" +
                "   \"PRODUCTID\":\"1000000116\",\n" +
                "   \"SCHEMEID\":\"1000000169\",\n" +
                "   \"LOANAMOUNT\":\"5000\",\n" +
                "   \"DISBURSALDATE\":\"01-OCT-2018\",\n" +
                "   \"PRINCIPAL_OUTSTANDING\":\"4500\",\n" +
                "   \"EMI_OUTSTANDING\":\"500\",\n" +
                "   \"FORECLOSURE_INTEREST\":\"45\"\n" +
                "   },\n" +
                "   {\n" +
                "   \"PROSPECTCODE\":\"PR00000002\",\n" +
                "   \"PRODUCTID\":\"1000000116\",\n" +
                "   \"SCHEMEID\":\"10000001170\",\n" +
                "   \"LOANAMOUNT\":\"6000\",\n" +
                "   \"DISBURSALDATE\":\"07-OCT-2018\",\n" +
                "   \"PRINCIPAL_OUTSTANDING\":\"5500\",\n" +
                "   \"EMI_OUTSTANDING\":\"500\",\n" +
                "   \"FORECLOSURE_INTEREST\":\"50\"\n" +
                "   }]\n" +
                "}\n" +
                "]\n" +
                "}\n" +
                "\n" +
                "}";

        MifinReciveResponse mifinReciveResponse = new MifinReciveResponse();

        mifinReciveResponse.setResponse(response);
        MifinTopUpResponse mifinTopUpresponse = null;
        try {
            mifinTopUpresponse = JsonUtil.StringToObject(mifinReciveResponse.getResponse(), MifinTopUpResponse.class);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return mifinTopUpresponse.getResponseMifin();
    }

    public TopUpDedupeResponse getTempResponseForLoandetail() {
        String response = "{\n" +
                "  \"RESPONSE\": {\n" +
                "    \"MESSAGE\": \"QUERY EXECUTED SUCCESSFULLY\",\n" +
                "    \"STATUS\": \"S\",\n" +
                "    \"LOANSUMMARY\": [\n" +
                "      {\n" +
                "        \"PROSPECTCODE\": \"PR00000001\",\n" +
                "        \"APPLICANTCODE\": \"\",\n" +
                "        \"BASICINFO\": {\n" +
                "          \"BRANCH\": \"Rohini\",\n" +
                "          \"RELATIONSHIP_MANAGER\": \"Amar\",\n" +
                "          \"PURPOSE_OF_LOAN\": \"HOME IMPROVEMENT\",\n" +
                "          \"SOURCING_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"SOURCING_CHANNEL_NAME\": \"SBFC\",\n" +
                "          \"APPFORMNO\": \"40210200454656\",\n" +
                "          \"FULLFILLMENT_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"FULFILLMENT_CHANNEL_NAME\": \"SBFC\"\n" +
                "        },\n" +
                "        \"CREDITSANCTION\": {\n" +
                "          \"SANCTIONAPPROVEDBY\": \"\",\n" +
                "          \"PRODUCTID\": \"SBL\",\n" +
                "          \"SCHEMEID\": \"PL\",\n" +
                "          \"APPLIED_LOAN_AMOUNT\": \"500000\",\n" +
                "          \"SANCTIONED_AMOUNT\": \"250000\",\n" +
                "          \"TENOR\": \"48\",\n" +
                "          \"ROI\": \"18.0\",\n" +
                "          \"INSTALLMENTTYPE\": \"EMI\",\n" +
                "          \"FREQUENCY\": \"Annually\",\n" +
                "          \"INTERESTTYPE\": \"FIXED\",\n" +
                "          \"EMI_START_DATE\": \"2019-01-05\",\n" +
                "          \"DISBURSAL_DATE\": \"2018-12-07\",\n" +
                "          \"ADVANCE_EMI_FLAG\": \"N\",\n" +
                "          \"NO_ADVANCE_INSTALMENT\": \"0\",\n" +
                "          \"LTV\": \"1\",\n" +
                "          \"DBR\": \"60.0\",\n" +
                "          \"REPAYMENTMODE\": \"CASH\"\n" +
                "        },\n" +
                "        \"CHARGES\": {\n" +
                "          \"CHARGE_DETAILS\": [\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CERSAI FEE\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Advance EMI\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Pre Emi\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"ICICI PRUDENTIAL LIFE INSURANCE\",\n" +
                "              \"CHARGE_AMOUNT\": \"4047\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"4047\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Imd\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Loan Processing Fee\",\n" +
                "              \"CHARGE_AMOUNT\": \"6250\",\n" +
                "              \"GST_ON_CHARGE\": \"1125\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"7375\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CreditVidya\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"VERIFICATION\": {\n" +
                "          \"VARIFICATION_DETAIL\": null\n" +
                "        },\n" +
                "        \"DOCUMENTS\": {\n" +
                "          \"DOCUMENT_DETAILS\": [\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Salary Slip2\",\n" +
                "              \"DOCUMENTNAME\": \"Salary Slip\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Salary Slip\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-11-27\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"ASSETDETAIL\": {\n" +
                "          \"ASSET_DETAILS\": [\n" +
                "            {\n" +
                "              \"PROPERTYTYPE\": \"Freehold\",\n" +
                "              \"PROPERTYADDRESS1\": \"HVHSVSVS\",\n" +
                "              \"PROPERTYADDRESS2\": \"SNDBSN\",\n" +
                "              \"PROPERTYADDRESS3\": \"TEST\",\n" +
                "              \"LANDMARK\": \"L-TESTDS\",\n" +
                "              \"LOCALITY\": \"LL-TEST\",\n" +
                "              \"CITY\": \"MUMBAI\",\n" +
                "              \"STATE\": \"MAHARASHTRA\",\n" +
                "              \"ZIPCODE\": \"400001\",\n" +
                "              \"OWNER_FNAME\": \"O-TEST\",\n" +
                "              \"OWNER_MNAME\": \"O-MTEST\",\n" +
                "              \"OWNER_LNAME\": \"O-LTEST\",\n" +
                "              \"VALUATORNAME\": \"TEST V\",\n" +
                "              \"PROPERTY_USAGE\": \"TEST U\",\n" +
                "              \"PROPERTY_STATUS\": \"TEST S\",\n" +
                "              \"PROPERTY_AGE\": \"10\",\n" +
                "              \"PRO_RESIDUALAGE\": \"10\",\n" +
                "              \"MARKET_VALUE\": \"1000000\",\n" +
                "              \"REPLACEMENT_COST\": \"10\",\n" +
                "              \"EVALUATION_DATE\": \"2018-03-31\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"BANKDETAILS\": {\n" +
                "          \"BANKDETAIL\": [\n" +
                "            {\n" +
                "              \"MICR\": null,\n" +
                "              \"IFSC_CODE\": \"psib0000040\",\n" +
                "              \"BANK_ID\": \"punjab and sind bank\",\n" +
                "              \"BANK_BRANCH_ID\": \"DELHI\",\n" +
                "              \"BANK_BRANCH_CITY_ID\": \"DELHI\",\n" +
                "              \"CUSTOMER_AC_NUMBER\": \"00401000011615\",\n" +
                "              \"BENEFICERY_NAME\": \"CHANDAN SINGH\",\n" +
                "              \"BANKTYPE\": \"\",\n" +
                "              \"ACCOUNT_TYPE\": \"Saving Account\",\n" +
                "              \"NOOFYEAR\": \"5\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"DISBURSALDETAIL\": {\n" +
                "          \"DISBURSALLIST\": [\n" +
                "            {\n" +
                "              \"BANK_ID\": \"HDFC BANK\",\n" +
                "              \"BANK_BRANCH_ID\": \"\",\n" +
                "              \"INSTRUMENTTYPE\": \"RTGS\",\n" +
                "              \"INSTRUMENTNO\": \"0\",\n" +
                "              \"INSTRUMENTDATE\": \"2018-12-07\",\n" +
                "              \"BOPS\": null,\n" +
                "              \"HOPS\": \"SAGAR JADHAV\",\n" +
                "              \"DISBURSAL_AMOUNT\": \"238578\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"CUSTOMER\": {\n" +
                "          \"CUSTOMERDETAILS\": [\n" +
                "            {\n" +
                "              \"CUSTOMERCODE\": \"\",\n" +
                "              \"CUSTOMER_ENTITY_TYPE\": \"INDIVIDUAL\",\n" +
                "              \"CUSTOMER_TYPE\": \"Applicant\",\n" +
                "              \"FNAME\": \"CHANDAN\",\n" +
                "              \"MNAME\": \"\",\n" +
                "              \"LNAME\": \"SINGH\",\n" +
                "              \"FATHER_FNAME\": \"\",\n" +
                "              \"FATHER_MNAME\": \"\",\n" +
                "              \"FATHER_LNAME\": \"\",\n" +
                "              \"SPOUSE_FNAME\": \"VIMAL\",\n" +
                "              \"SPOUSE_MNAME\": \"\",\n" +
                "              \"SPOUSE_LNAME\": \"KANDARI\",\n" +
                "              \"MOTHER_MAIDEN_NAME\": \"\",\n" +
                "              \"DOB\": \"1976-02-02\",\n" +
                "              \"GENDER\": \"Male\",\n" +
                "              \"MARITAL_STATUS\": \"Married\",\n" +
                "              \"CATEGORY\": \"New Customer\",\n" +
                "              \"QUALIFICATION\": \"\",\n" +
                "              \"NO_OF_DEPENDENTS\": \"0\",\n" +
                "              \"OCCUPATION_TYPE\": \"Pvt Salaried\",\n" +
                "              \"TANNO\": \"\",\n" +
                "              \"GROSS_INCOME\": \"37964\",\n" +
                "              \"NET_INCOME\": \"37964\",\n" +
                "              \"WORK_EXPERIENCE\": \"\",\n" +
                "              \"PASSPORT_NO\": \"\",\n" +
                "              \"PASSPORT_EXP_DATE\": \"\",\n" +
                "              \"AADHAR_NO\": \"\",\n" +
                "              \"PAN_NO\": \"BBKPS2459D\",\n" +
                "              \"VOTER_ID\": \"\",\n" +
                "              \"DRIVING_LICENSE\": \"\",\n" +
                "              \"DRIVING_LICENSE_EXP_DATE\": \"\",\n" +
                "              \"RATION_CARD\": \"\",\n" +
                "              \"BANK_PASSBOOK\": \"\",\n" +
                "              \"INDUSTRYTYPE\": \"\",\n" +
                "              \"PROFESSION\": \"\",\n" +
                "              \"GSTIN\": \"\",\n" +
                "              \"EMPLOYMENTTYPE\": \"Salaried\",\n" +
                "              \"CIBIL_SCORE\": \"1009\",\n" +
                "              \"CURRENT_EMPOLYER\": \"qtc\",\n" +
                "              \"ENTITY_OTHER\": \"FO9879\",\n" +
                "              \"Industry_Code\": \"test001\",\n" +
                "              \"Organization_Type\": \"GOVERNMENT / PSU\",\n" +
                "              \"PROFESSION_Code\": \"Emp001\",\n" +
                "              \"RELATION\": \"FATHER\",\n" +
                "              \"ADDRESS_DETAIL\": [\n" +
                "                {\n" +
                "                    \"ADDRESS_TYPE\": \"Residence Address\",\n" +
                "                    \"MAILING_ADDRESS\": \"Y\",\n" +
                "                    \"NAMEOFCOMPANY\": \"\",\n" +
                "                    \"ADD_1\": \"Ugf-2, 622 &  623/5\",\n" +
                "                    \"ADD_2\": \"Mehrauli, hauz khas south delhI\",\n" +
                "                    \"ADD_3\": \"\",\n" +
                "                    \"ADD_CITY\": \"DELHI\",\n" +
                "                    \"ADD_DIST\": \"\",\n" +
                "                    \"ADD_LANDMARK\": \"Ram mandir\",\n" +
                "                    \"ADD_STATE\": \"DELHI\",\n" +
                "                    \"ADD_PINCODE\": \"110030\",\n" +
                "                    \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                    \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                    \"ADD_PHONE\": \"9810338495\",\n" +
                "                    \"ADD_MOBILE\": \"9810338495\",\n" +
                "                    \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                    \"OCCUPANCY_STATUS\": \"OTHERS\",\n" +
                "                    \"ADD_MONTH_OF_STAY\": \"8\",\n" +
                "                    \"QUALIFICATION\": \"Graduation\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Permanent Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"H no 266\",\n" +
                "                  \"ADD_2\": \"Plot no 40\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Garwal colony mehrauli\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110030\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Office Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"C 105\",\n" +
                "                  \"ADD_2\": \"Ph 1\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Narayana inds arear\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110028\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"0\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                }\n" +
                "              ]\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"LMSDETAIL\": {\n" +
                "          \"EMI\": 6150,\n" +
                "          \"FORECLOSURE_INTEREST\": 9924.66,\n" +
                "          \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "          \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "          \"OTHER_CHARGES\": 0\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"PROSPECTID\": \"PR00000002\",\n" +
                "        \"APPLICANTID\": \"\",\n" +
                "        \"BASICINFO\": {\n" +
                "          \"BRANCH\": \"Rohini\",\n" +
                "          \"RELATIONSHIP_MANAGER\": \"Amar\",\n" +
                "          \"PURPOSE_OF_LOAN\": \"HOME IMPROVEMENT\",\n" +
                "          \"SOURCING_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"SOURCING_CHANNEL_NAME\": \"SBFC\",\n" +
                "          \"APPFORMNO\": \"40210200454656\",\n" +
                "          \"FULLFILLMENT_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"FULFILLMENT_CHANNEL_NAME\": \"SBFC\"\n" +
                "        },\n" +
                "        \"CREDITSANCTION\": {\n" +
                "          \"SANCTIONAPPROVEDBY\": \"\",\n" +
                "          \"PRODUCTID\": \"SBL\",\n" +
                "          \"SCHEMEID\": \"PL\",\n" +
                "          \"APPLIED_LOAN_AMOUNT\": \"500000\",\n" +
                "          \"SANCTIONED_AMOUNT\": \"250000\",\n" +
                "          \"TENOR\": \"48\",\n" +
                "          \"ROI\": \"18.0\",\n" +
                "          \"INSTALLMENTTYPE\": \"EMI\",\n" +
                "          \"FREQUENCY\": \"Annually\",\n" +
                "          \"INTERESTTYPE\": \"FIXED\",\n" +
                "          \"EMI_START_DATE\": \"2019-01-05\",\n" +
                "          \"DISBURSAL_DATE\": \"2018-12-07\",\n" +
                "          \"ADVANCE_EMI_FLAG\": \"N\",\n" +
                "          \"NO_ADVANCE_INSTALMENT\": \"0\",\n" +
                "          \"LTV\": \"1\",\n" +
                "          \"DBR\": \"60.0\",\n" +
                "          \"REPAYMENTMODE\": \"CASH\"\n" +
                "        },\n" +
                "        \"CHARGES\": {\n" +
                "          \"CHARGE_DETAILS\": [\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CERSAI FEE\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Advance EMI\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Pre Emi\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"ICICI PRUDENTIAL LIFE INSURANCE\",\n" +
                "              \"CHARGE_AMOUNT\": \"4047\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"4047\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Imd\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Loan Processing Fee\",\n" +
                "              \"CHARGE_AMOUNT\": \"6250\",\n" +
                "              \"GST_ON_CHARGE\": \"1125\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"7375\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CreditVidya\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"VERIFICATION\": {\n" +
                "          \"VARIFICATION_DETAIL\": null\n" +
                "        },\n" +
                "        \"DOCUMENTS\": {\n" +
                "          \"DOCUMENT_DETAILS\": [\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Salary Slip2\",\n" +
                "              \"DOCUMENTNAME\": \"Salary Slip\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Salary Slip\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-11-27\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"ASSETDETAIL\": {\n" +
                "          \"ASSET_DETAILS\": [\n" +
                "            {\n" +
                "              \"PROPERTYTYPE\": \"Freehold\",\n" +
                "              \"PROPERTYADDRESS1\": \"HVHSVSVS\",\n" +
                "              \"PROPERTYADDRESS2\": \"SNDBSN\",\n" +
                "              \"PROPERTYADDRESS3\": \"TEST\",\n" +
                "              \"LANDMARK\": \"L-TESTDS\",\n" +
                "              \"LOCALITY\": \"LL-TEST\",\n" +
                "              \"CITY\": \"MUMBAI\",\n" +
                "              \"STATE\": \"MAHARASHTRA\",\n" +
                "              \"ZIPCODE\": \"400001\",\n" +
                "              \"OWNER_FNAME\": \"O-TEST\",\n" +
                "              \"OWNER_MNAME\": \"O-MTEST\",\n" +
                "              \"OWNER_LNAME\": \"O-LTEST\",\n" +
                "              \"VALUATORNAME\": \"TEST V\",\n" +
                "              \"PROPERTY_USAGE\": \"TEST U\",\n" +
                "              \"PROPERTY_STATUS\": \"TEST S\",\n" +
                "              \"PROPERTY_AGE\": \"10\",\n" +
                "              \"PRO_RESIDUALAGE\": \"10\",\n" +
                "              \"MARKET_VALUE\": \"1000000\",\n" +
                "              \"REPLACEMENT_COST\": \"10\",\n" +
                "              \"EVALUATION_DATE\": \"2018-03-31\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"BANKDETAILS\": {\n" +
                "          \"BANKDETAIL\": [\n" +
                "            {\n" +
                "              \"MICR\": null,\n" +
                "              \"IFSC_CODE\": \"psib0000040\",\n" +
                "              \"BANK_ID\": \"punjab and sind bank\",\n" +
                "              \"BANK_BRANCH_ID\": \"DELHI\",\n" +
                "              \"BANK_BRANCH_CITY_ID\": \"DELHI\",\n" +
                "              \"CUSTOMER_AC_NUMBER\": \"00401000011615\",\n" +
                "              \"BENEFICERY_NAME\": \"CHANDAN SINGH\",\n" +
                "              \"BANKTYPE\": \"\",\n" +
                "              \"ACCOUNT_TYPE\": \"Saving Account\",\n" +
                "              \"NOOFYEAR\": \"5\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"DISBURSALDETAIL\": {\n" +
                "          \"DISBURSALLIST\": [\n" +
                "            {\n" +
                "              \"BANK_ID\": \"HDFC BANK\",\n" +
                "              \"BANK_BRANCH_ID\": \"\",\n" +
                "              \"INSTRUMENTTYPE\": \"RTGS\",\n" +
                "              \"INSTRUMENTNO\": \"0\",\n" +
                "              \"INSTRUMENTDATE\": \"2018-12-07\",\n" +
                "              \"BOPS\": null,\n" +
                "              \"HOPS\": \"SAGAR JADHAV\",\n" +
                "              \"DISBURSAL_AMOUNT\": \"238578\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"CUSTOMER\": {\n" +
                "          \"CUSTOMERDETAILS\": [\n" +
                "            {\n" +
                "              \"CUSTOMERCODE\": \"\",\n" +
                "              \"CUSTOMER_ENTITY_TYPE\": \"INDIVIDUAL\",\n" +
                "              \"CUSTOMER_TYPE\": \"Applicant\",\n" +
                "              \"FNAME\": \"CHANDAN\",\n" +
                "              \"MNAME\": \"\",\n" +
                "              \"LNAME\": \"SINGH\",\n" +
                "              \"FATHER_FNAME\": \"\",\n" +
                "              \"FATHER_MNAME\": \"\",\n" +
                "              \"FATHER_LNAME\": \"\",\n" +
                "              \"SPOUSE_FNAME\": \"VIMAL\",\n" +
                "              \"SPOUSE_MNAME\": \"\",\n" +
                "              \"SPOUSE_LNAME\": \"KANDARI\",\n" +
                "              \"MOTHER_MAIDEN_NAME\": \"\",\n" +
                "              \"DOB\": \"1976-02-02\",\n" +
                "              \"GENDER\": \"Male\",\n" +
                "              \"MARITAL_STATUS\": \"Married\",\n" +
                "              \"CATEGORY\": \"New Customer\",\n" +
                "              \"QUALIFICATION\": \"\",\n" +
                "              \"NO_OF_DEPENDENTS\": \"0\",\n" +
                "              \"OCCUPATION_TYPE\": \"Pvt Salaried\",\n" +
                "              \"TANNO\": \"\",\n" +
                "              \"GROSS_INCOME\": \"37964\",\n" +
                "              \"NET_INCOME\": \"37964\",\n" +
                "              \"WORK_EXPERIENCE\": \"\",\n" +
                "              \"PASSPORT_NO\": \"\",\n" +
                "              \"PASSPORT_EXP_DATE\": \"\",\n" +
                "              \"AADHAR_NO\": \"\",\n" +
                "              \"PAN_NO\": \"BBKPS2459D\",\n" +
                "              \"VOTER_ID\": \"\",\n" +
                "              \"DRIVING_LICENSE\": \"\",\n" +
                "              \"DRIVING_LICENSE_EXP_DATE\": \"\",\n" +
                "              \"RATION_CARD\": \"\",\n" +
                "              \"BANK_PASSBOOK\": \"\",\n" +
                "              \"INDUSTRYTYPE\": \"\",\n" +
                "              \"PROFESSION\": \"\",\n" +
                "              \"GSTIN\": \"\",\n" +
                "              \"EMPLOYMENTTYPE\": \"Salaried\",\n" +
                "              \"CIBIL_SCORE\": \"1009\",\n" +
                "              \"CURRENT_EMPOLYER\": \"qtc\",\n" +
                "              \"ENTITY_OTHER\": \"FO9879\",\n" +
                "              \"Industry_Code\": \"test001\",\n" +
                "              \"Organization_Type\": \"GOVERNMENT / PSU\",\n" +
                "              \"PROFESSION_Code\": \"Emp001\",\n" +
                "              \"RELATION\": \"FATHER\",\n" +
                "              \"ADDRESS_DETAIL\": [\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Residence Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"Y\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"Ugf-2, 622 &  623/5\",\n" +
                "                  \"ADD_2\": \"Mehrauli, hauz khas south delhI\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Ram mandir\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110030\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\",\n" +
                "                  \"ADD_MONTH_OF_STAY\": \"8\",\n" +
                "                  \"QUALIFICATION\": \"Graduation\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Permanent Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"H no 266\",\n" +
                "                  \"ADD_2\": \"Plot no 40\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Garwal colony mehrauli\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110030\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Office Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"C 105\",\n" +
                "                  \"ADD_2\": \"Ph 1\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Narayana inds arear\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110028\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"0\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                }\n" +
                "              ]\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"LMSDETAIL\": {\n" +
                "          \"EMI\": 6150,\n" +
                "          \"FORECLOSURE_INTEREST\": 9924.66,\n" +
                "          \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "          \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "          \"OTHER_CHARGES\": 0\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"PROSPECTID\": \"PR00000003\",\n" +
                "        \"APPLICANTID\": \"\",\n" +
                "        \"BASICINFO\": {\n" +
                "          \"BRANCH\": \"Rohini\",\n" +
                "          \"RELATIONSHIP_MANAGER\": \"Amar\",\n" +
                "          \"PURPOSE_OF_LOAN\": \"HOME IMPROVEMENT\",\n" +
                "          \"SOURCING_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"SOURCING_CHANNEL_NAME\": \"SBFC\",\n" +
                "          \"APPFORMNO\": \"40210200454656\",\n" +
                "          \"FULLFILLMENT_CHANNEL_TYPE\": \"Branch\",\n" +
                "          \"FULFILLMENT_CHANNEL_NAME\": \"SBFC\"\n" +
                "        },\n" +
                "        \"CREDITSANCTION\": {\n" +
                "          \"SANCTIONAPPROVEDBY\": \"\",\n" +
                "          \"PRODUCTID\": \"SBL\",\n" +
                "          \"SCHEMEID\": \"PL\",\n" +
                "          \"APPLIED_LOAN_AMOUNT\": \"500000\",\n" +
                "          \"SANCTIONED_AMOUNT\": \"250000\",\n" +
                "          \"TENOR\": \"48\",\n" +
                "          \"ROI\": \"18.0\",\n" +
                "          \"INSTALLMENTTYPE\": \"EMI\",\n" +
                "          \"FREQUENCY\": \"Annually\",\n" +
                "          \"INTERESTTYPE\": \"FIXED\",\n" +
                "          \"EMI_START_DATE\": \"2019-01-05\",\n" +
                "          \"DISBURSAL_DATE\": \"2018-12-07\",\n" +
                "          \"ADVANCE_EMI_FLAG\": \"N\",\n" +
                "          \"NO_ADVANCE_INSTALMENT\": \"0\",\n" +
                "          \"LTV\": \"1\",\n" +
                "          \"DBR\": \"60.0\",\n" +
                "          \"REPAYMENTMODE\": \"CASH\"\n" +
                "        },\n" +
                "        \"CHARGES\": {\n" +
                "          \"CHARGE_DETAILS\": [\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CERSAI FEE\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Advance EMI\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Pre Emi\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"ICICI PRUDENTIAL LIFE INSURANCE\",\n" +
                "              \"CHARGE_AMOUNT\": \"4047\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"4047\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Imd\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"Loan Processing Fee\",\n" +
                "              \"CHARGE_AMOUNT\": \"6250\",\n" +
                "              \"GST_ON_CHARGE\": \"1125\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"7375\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"CHARGEID\": \"CreditVidya\",\n" +
                "              \"CHARGE_AMOUNT\": \"0\",\n" +
                "              \"GST_ON_CHARGE\": \"0\",\n" +
                "              \"TOTAL_CHARGE_AMOUNT\": \"0\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"VERIFICATION\": {\n" +
                "          \"VARIFICATION_DETAIL\": null\n" +
                "        },\n" +
                "        \"DOCUMENTS\": {\n" +
                "          \"DOCUMENT_DETAILS\": [\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Salary Slip2\",\n" +
                "              \"DOCUMENTNAME\": \"Salary Slip\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Salary Slip\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-11-27\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"DOCUMENTTYPE\": \"Common Document\",\n" +
                "              \"DOCUMENTNAME\": \"Common Document\",\n" +
                "              \"DOCUMENTDESCRIPTION\": \"Common Document\",\n" +
                "              \"STATUS\": \"Received\",\n" +
                "              \"RECEIVEDDATE\": \"2018-12-06\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"ASSETDETAIL\": {\n" +
                "          \"ASSET_DETAILS\": [\n" +
                "            {\n" +
                "              \"PROPERTYTYPE\": \"Freehold\",\n" +
                "              \"PROPERTYADDRESS1\": \"HVHSVSVS\",\n" +
                "              \"PROPERTYADDRESS2\": \"SNDBSN\",\n" +
                "              \"PROPERTYADDRESS3\": \"TEST\",\n" +
                "              \"LANDMARK\": \"L-TESTDS\",\n" +
                "              \"LOCALITY\": \"LL-TEST\",\n" +
                "              \"CITY\": \"MUMBAI\",\n" +
                "              \"STATE\": \"MAHARASHTRA\",\n" +
                "              \"ZIPCODE\": \"400001\",\n" +
                "              \"OWNER_FNAME\": \"O-TEST\",\n" +
                "              \"OWNER_MNAME\": \"O-MTEST\",\n" +
                "              \"OWNER_LNAME\": \"O-LTEST\",\n" +
                "              \"VALUATORNAME\": \"TEST V\",\n" +
                "              \"PROPERTY_USAGE\": \"TEST U\",\n" +
                "              \"PROPERTY_STATUS\": \"TEST S\",\n" +
                "              \"PROPERTY_AGE\": \"10\",\n" +
                "              \"PRO_RESIDUALAGE\": \"10\",\n" +
                "              \"MARKET_VALUE\": \"1000000\",\n" +
                "              \"REPLACEMENT_COST\": \"10\",\n" +
                "              \"EVALUATION_DATE\": \"2018-03-31\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"BANKDETAILS\": {\n" +
                "          \"BANKDETAIL\": [\n" +
                "            {\n" +
                "              \"MICR\": null,\n" +
                "              \"IFSC_CODE\": \"psib0000040\",\n" +
                "              \"BANK_ID\": \"punjab and sind bank\",\n" +
                "              \"BANK_BRANCH_ID\": \"DELHI\",\n" +
                "              \"BANK_BRANCH_CITY_ID\": \"DELHI\",\n" +
                "              \"CUSTOMER_AC_NUMBER\": \"00401000011615\",\n" +
                "              \"BENEFICERY_NAME\": \"CHANDAN SINGH\",\n" +
                "              \"BANKTYPE\": \"\",\n" +
                "              \"ACCOUNT_TYPE\": \"Saving Account\",\n" +
                "              \"NOOFYEAR\": \"5\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"DISBURSALDETAIL\": {\n" +
                "          \"DISBURSALLIST\": [\n" +
                "            {\n" +
                "              \"BANK_ID\": \"HDFC BANK\",\n" +
                "              \"BANK_BRANCH_ID\": \"\",\n" +
                "              \"INSTRUMENTTYPE\": \"RTGS\",\n" +
                "              \"INSTRUMENTNO\": \"0\",\n" +
                "              \"INSTRUMENTDATE\": \"2018-12-07\",\n" +
                "              \"BOPS\": null,\n" +
                "              \"HOPS\": \"SAGAR JADHAV\",\n" +
                "              \"DISBURSAL_AMOUNT\": \"238578\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"CUSTOMER\": {\n" +
                "          \"CUSTOMERDETAILS\": [\n" +
                "            {\n" +
                "              \"CUSTOMERCODE\": \"\",\n" +
                "              \"CUSTOMER_ENTITY_TYPE\": \"INDIVIDUAL\",\n" +
                "              \"CUSTOMER_TYPE\": \"Applicant\",\n" +
                "              \"FNAME\": \"CHANDAN\",\n" +
                "              \"MNAME\": \"\",\n" +
                "              \"LNAME\": \"SINGH\",\n" +
                "              \"FATHER_FNAME\": \"\",\n" +
                "              \"FATHER_MNAME\": \"\",\n" +
                "              \"FATHER_LNAME\": \"\",\n" +
                "              \"SPOUSE_FNAME\": \"VIMAL\",\n" +
                "              \"SPOUSE_MNAME\": \"\",\n" +
                "              \"SPOUSE_LNAME\": \"KANDARI\",\n" +
                "              \"MOTHER_MAIDEN_NAME\": \"\",\n" +
                "              \"DOB\": \"1976-02-02\",\n" +
                "              \"GENDER\": \"Male\",\n" +
                "              \"MARITAL_STATUS\": \"Married\",\n" +
                "              \"CATEGORY\": \"New Customer\",\n" +
                "              \"QUALIFICATION\": \"\",\n" +
                "              \"NO_OF_DEPENDENTS\": \"0\",\n" +
                "              \"OCCUPATION_TYPE\": \"Pvt Salaried\",\n" +
                "              \"TANNO\": \"\",\n" +
                "              \"GROSS_INCOME\": \"37964\",\n" +
                "              \"NET_INCOME\": \"37964\",\n" +
                "              \"WORK_EXPERIENCE\": \"\",\n" +
                "              \"PASSPORT_NO\": \"\",\n" +
                "              \"PASSPORT_EXP_DATE\": \"\",\n" +
                "              \"AADHAR_NO\": \"\",\n" +
                "              \"PAN_NO\": \"BBKPS2459D\",\n" +
                "              \"VOTER_ID\": \"\",\n" +
                "              \"DRIVING_LICENSE\": \"\",\n" +
                "              \"DRIVING_LICENSE_EXP_DATE\": \"\",\n" +
                "              \"RATION_CARD\": \"\",\n" +
                "              \"BANK_PASSBOOK\": \"\",\n" +
                "              \"INDUSTRYTYPE\": \"\",\n" +
                "              \"PROFESSION\": \"\",\n" +
                "              \"GSTIN\": \"\",\n" +
                "              \"EMPLOYMENTTYPE\": \"Salaried\",\n" +
                "              \"CIBIL_SCORE\": \"1009\",\n" +
                "              \"CURRENT_EMPOLYER\": \"qtc\",\n" +
                "              \"ENTITY_OTHER\": \"FO9879\",\n" +
                "              \"Industry_Code\": \"test001\",\n" +
                "              \"Organization_Type\": \"GOVERNMENT / PSU\",\n" +
                "              \"PROFESSION_Code\": \"Emp001\",\n" +
                "              \"RELATION\": \"FATHER\",\n" +
                "              \"ADDRESS_DETAIL\": [\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Residence Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"Y\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"Ugf-2, 622 &  623/5\",\n" +
                "                  \"ADD_2\": \"Mehrauli, hauz khas south delhI\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Ram mandir\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110030\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\",\n" +
                "                  \"ADD_MONTH_OF_STAY\": \"8\",\n" +
                "                  \"QUALIFICATION\": \"Graduation\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Permanent Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"H no 266\",\n" +
                "                  \"ADD_2\": \"Plot no 40\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Garwal colony mehrauli\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110030\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"1\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"ADDRESS_TYPE\": \"Office Address\",\n" +
                "                  \"MAILING_ADDRESS\": \"N\",\n" +
                "                  \"NAMEOFCOMPANY\": \"\",\n" +
                "                  \"ADD_1\": \"C 105\",\n" +
                "                  \"ADD_2\": \"Ph 1\",\n" +
                "                  \"ADD_3\": \"\",\n" +
                "                  \"ADD_CITY\": \"DELHI\",\n" +
                "                  \"ADD_DIST\": \"\",\n" +
                "                  \"ADD_LANDMARK\": \"Narayana inds arear\",\n" +
                "                  \"ADD_STATE\": \"DELHI\",\n" +
                "                  \"ADD_PINCODE\": \"110028\",\n" +
                "                  \"ADD_YEAR_OF_STAY\": \"0\",\n" +
                "                  \"ADD_PHONE_STD_CODE\": \"\",\n" +
                "                  \"ADD_PHONE\": \"9810338495\",\n" +
                "                  \"ADD_MOBILE\": \"9810338495\",\n" +
                "                  \"ADD_EMAIL\": \"chandansingh.kandari@gmail.com\",\n" +
                "                  \"OCCUPANCY_STATUS\": \"OTHERS\"\n" +
                "                }\n" +
                "              ]\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        \"LMSDETAIL\": {\n" +
                "          \"EMI\": 6150,\n" +
                "          \"FORECLOSURE_INTEREST\": 9924.66,\n" +
                "          \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "          \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "          \"OTHER_CHARGES\": 0\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"LMSDETAIL\": [\n" +
                "      {\n" +
                "        \"EMI\": 6150,\n" +
                "        \"FORECLOSURE_INTEREST\": 9924.66,\n" +
                "        \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "        \"PROSPECTCODE\": \"PR00000001\",\n" +
                "        \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "        \"OTHER_CHARGES\": 0\n" +
                "      },\n" +
                "      {\n" +
                "        \"EMI\": 6639,\n" +
                "        \"FORECLOSURE_INTEREST\": 14446.03,\n" +
                "        \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "        \"PROSPECTCODE\": \"PR00000002\",\n" +
                "        \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "        \"OTHER_CHARGES\": 0\n" +
                "      },\n" +
                "      {\n" +
                "        \"EMI\": 13278,\n" +
                "        \"FORECLOSURE_INTEREST\": 29169.86,\n" +
                "        \"PRINCIPAL_OUTSTANDING\": 0,\n" +
                "        \"PROSPECTCODE\": \"PR00000003\",\n" +
                "        \"INTEREST_DUE_OUTSTANDING\": 0,\n" +
                "        \"OTHER_CHARGES\": 0\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        MifinReciveResponse mifinReciveResponse = new MifinReciveResponse();

        mifinReciveResponse.setResponse(response);
        MifinTopUpResponse mifinTopUpresponse = new MifinTopUpResponse();

        TopUpDedupeResponse topUpDedupeResponse = mifinTopUpresponse.getResponseMifin();
        try {
            mifinTopUpresponse = JsonUtil.StringToObject(mifinReciveResponse.getResponse(), MifinTopUpResponse.class);

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }

        topUpDedupeResponse = mifinTopUpresponse.getResponseMifin();


        List<LmsDetail> list = new ArrayList<LmsDetail>();
        List<LoanSummaryDetail> LoansummaryList = new ArrayList<LoanSummaryDetail>();
        // List<LoanSummary>LoansummaryList = new ArrayList<LoanSummary>();
        if (topUpDedupeResponse != null) {
            if (topUpDedupeResponse.getObject() != null && topUpDedupeResponse.getObject() instanceof List<?>) {
                list = (List<LmsDetail>) topUpDedupeResponse.getObject();
                topUpDedupeResponse.setLmsDetailsList(list);
                logger.info("lmsDetailList as object  {}", list);
            } else if (topUpDedupeResponse.getObject() != null) {
                try {
                    topUpDedupeResponse.setLmsDetail(JsonUtil.MapToObject((Map<String, Object>) topUpDedupeResponse.getObject(), LmsDetail.class));
                    list.add(topUpDedupeResponse.getLmsDetail());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (topUpDedupeResponse.getLoanSummaryObject() != null && topUpDedupeResponse.getLoanSummaryObject() instanceof List<?>) {
                LoansummaryList = (List<LoanSummaryDetail>) topUpDedupeResponse.getLoanSummaryObject();
                topUpDedupeResponse.setLoanSummaryDeatilList(LoansummaryList);
                     /*LoansummaryList = (List<LoanSummary>) topUpDedupeResponse.getLoanSummaryObject();
                     topUpDedupeResponse.setLoanSummaryList(LoansummaryList);*/
                logger.info("LoanSummaryDetail as object  {}", list);
            } else if (topUpDedupeResponse.getLoanSummaryObject() != null) {
                try {
                    topUpDedupeResponse.setLoanSummaryDeatil(JsonUtil.MapToObject((Map<String, Object>) topUpDedupeResponse.getLoanSummaryObject(), LoanSummaryDetail.class));
                    LoansummaryList.add(topUpDedupeResponse.getLoanSummaryDeatil());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        mifinTopUpresponse.setResponseMifin(topUpDedupeResponse);
        logger.info("Mifin Response {} ", mifinTopUpresponse.getResponseMifin());
        logger.info("mifinTopUpresponse {}", mifinTopUpresponse);
        logger.info("TopUpDedupeResponse {}", mifinTopUpresponse.getResponseMifin());
        return mifinTopUpresponse.getResponseMifin();
    }

    //Organization_Type  < Map to master (GOVERNMENT / PSU) < applicant.employment[0].employerType
    public static String getOrganizationType(String employerType) {
        String organizationType = ORGANIZATION_TYPE_PSU;
        if (StringUtils.containsIgnoreCase(employerType, "GOV")) {
            organizationType = ORGANIZATION_TYPE_GOVT;
        }
        return organizationType;
    }

    private LegalVerification populateDataInLegalVerification(AssetDetail assetDetail , String refID, String instId) {

        LegalVerification legalVerification = applicationRepository.fetchLegalVerificatioDetails(refID, instId);
        List<LegalVerificationDetails> legalVerfificationDetailList = new ArrayList<LegalVerificationDetails>();
        LegalVerificationDetails legalVerificationDetails = new LegalVerificationDetails();
        LegalVerificationOutput legalVerificationOutput = new LegalVerificationOutput();
        List<AssetDetails> assetDetailsList = assetDetail.getAssetDetailsList();
        if (assetDetail != null && CollectionUtils.isNotEmpty(assetDetail.getAssetDetailsList())) {
            assetDetailsList = assetDetail.getAssetDetailsList();
        }
        if (legalVerification == null || CollectionUtils.isNotEmpty(legalVerification.getLegalVerificationDetailsList())) {
            legalVerification = new LegalVerification();
            if (CollectionUtils.isNotEmpty(assetDetailsList)) {
                for(int i =0; i< assetDetailsList.size(); i++) {
                        legalVerificationDetails = new LegalVerificationDetails();
                        legalVerificationOutput.setTypeOfProp(GngUtils.setValueForNull(assetDetailsList.get(i).getPropertyType()));
                        legalVerificationDetails.setLegalVerificationOutput(legalVerificationOutput);
                        legalVerificationDetails.setLegalVerificationInput(new LegalVerificationInput());
                        legalVerificationDetails.setCollateralId(String.valueOf(i));
                        legalVerfificationDetailList.add(legalVerificationDetails);
                }
                legalVerification.setLegalVerificationDetailsList(legalVerfificationDetailList);
            }
        } else {
            if (CollectionUtils.isNotEmpty(assetDetailsList)) {
                for(int i =0; i< assetDetailsList.size(); i++)  {
                    legalVerificationDetails = new LegalVerificationDetails();
                    legalVerificationOutput.setTypeOfProp(assetDetailsList.get(i).getPropertyType());
                    legalVerificationDetails.setLegalVerificationOutput(legalVerificationOutput);
                    legalVerificationDetails.setLegalVerificationInput(new LegalVerificationInput());
                    legalVerificationDetails.setCollateralId(String.valueOf(i));
                    legalVerfificationDetailList.add(legalVerificationDetails);
                }
                legalVerification.setLegalVerificationDetailsList(legalVerfificationDetailList);
            }
        }
        return legalVerification;
    }

    public MifinReciveResponse getTempResponseForMifinDedupe() {
        String response = "{\"RESPONSE\":{\"STATUS\":\"S\",\"MESSAGE\":\"API AUTHENTICATION SUCCESS\",\"APPLICANTDETAILS\":{\"PAN\":\"\",\"POTENTIALMATCH_MSGSTATUS\":\"\",\"MNAME\":\"\",\"FNAME\":\"SAGAR\",\"EXACTMATCH_MSGSTATUS\":\"N\",\"PASSPORTNO\":\"\",\"DOB\":\"1978-05-04T00:00:00\",\"SCORE\":100,\"FATHERNAME\":\"BALBIR KUMAR GUPTA\",\"CUSTID\":\"AP00198856\",\"POTENTIAL_APPLICANTCODE\":\"\",\"LNAME\":\"\",\"MOTHERNAME\":\"\",\"DL\":\"\",\"VOTERIDCARDNO\":\"\",\"MOBILE\":\"\",\"ADDRESSDETAIL\":\"\",\"STRONG_MATCH\":\"Y\",\"POTENTIAL_MATCH\":\"Y\",\"MATCHED_APPLICANTCODE\":\"\",\"CUSTTYPE\":\"Applicant\",\"ADDRESSDETAIL\":[]},\"MATCHEDAPPLICANTS\":[{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"VENKATASUREKHA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Mar 26 2013  3:40PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"SATYANARAYANA  T\",\"LOGINID\":1000000142,\"MOTHERNAME\":\"THMMANA\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00019E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"V\",\"ACTUAL_CUSTID\":\"AP00003927\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"GURAM\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9963260664,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"D3\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00004802\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":71200},{\"SCHEME\":\"XGL E\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00048308\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":71200},{\"SCHEME\":\"GOLD 16\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00187432\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":71200},{\"SCHEME\":\"GOLD 16\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00189715\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":61200}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"23-15-1/10 TIRUMULASETTY  SATYANARAYANAPURAM\",\"PINCODE\":520011,\"CITY\":\"VIJAYAWADA\"},{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"23-15-1/10 TIRUMULASETTY  SATYANARAYANAPURAM\",\"PINCODE\":520011,\"CITY\":\"VIJAYAWADA\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"ARASE\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Mar 28 2013 10:16AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"JAVARAIA  H\",\"LOGINID\":1000000316,\"MOTHERNAME\":\"JAYAMMA\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00019E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"A\",\"ACTUAL_CUSTID\":\"AP00055205\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"GOWDA K G\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":7204002030,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00189970\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":30000}],\"ADDRESSDETAIL\":[{\"STATE\":\"KARNATAKA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"NO 37 3 RD CROSS SAMPIGE ROAD  MALLESHWARAM NEAR K C GENERAL HOSPITAL BACK GATE\",\"PINCODE\":560003,\"CITY\":\"BANGALORE\"},{\"STATE\":\"KARNATAKA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"NO 37 3 RD CROSS SAMPIGE ROAD  MALLESHWARAM NEAR K C GENERAL HOSPITAL BACK GATE\",\"PINCODE\":560003,\"CITY\":\"BANGALORE\"}],},{\"PAN\":\"ARXPK2190F\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SHAHIL\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Jul 13 2013  5:17PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"KAMSHUDDIN  KHAN\",\"LOGINID\":1000000311,\"MOTHERNAME\":\"SABRI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00022E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"A\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00031477\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"ALI KHAN\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9719503388,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00109533\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":40000},{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00141815\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":43000},{\"SCHEME\":\"SUPER SAVER 1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00213819\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":29500},{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00439476\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":26000},{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00446591\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":47000},{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00485043\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":26000},{\"SCHEME\":\"A4\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00494639\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":55000}],\"ADDRESSDETAIL\":[{\"STATE\":\"UTTARAKHAND\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"1 DDUN VILL. GORAKHPUR BADOWALA NEAR KHAN MARKET\",\"PINCODE\":248001,\"CITY\":\"DEHRADUN\"},{\"STATE\":\"UTTARAKHAND\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"1 DDUN VILL. GORAKHPUR BADOWALA NEAR KHAN MARKET\",\"PINCODE\":248001,\"CITY\":\"DEHRADUN\"},{\"STATE\":\"UTTARAKHAND\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"DDUN  DDUN TRANSPORT NAGAR NEAR DHARAM KANTA\",\"PINCODE\":248001,\"CITY\":\"DEHRADUN\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SAGGURTHI\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Nov 25 2013  6:14PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"SAGGURTHI MOHAN RAO\",\"LOGINID\":1000000142,\"MOTHERNAME\":\"MANGALAPATI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00024E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"J\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00072565\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"JOHN CHINNA RAO\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9866911946,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"SUPER SAVER 1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00235548\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":57500}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"36-5-29 WOOD PET THULLURU YACOB VEEDHI VENKATESWARA PURAM SIDDARTHA NAGAR\",\"PINCODE\":520010,\"CITY\":\"VIJAYAWADA\"},{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"36-5-29 WOOD PET THULLURU YACOB VEEDHI VENKATESWARA PURAM SIDDARTHA NAGAR\",\"PINCODE\":520010,\"CITY\":\"VIJAYAWADA\"}],},{\"PAN\":\"ABZPN8920C\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"PARAMASHIVAN\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Feb 26 2014 11:59AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"THANGANARI APPAN NADAR\",\"LOGINID\":1000000247,\"MOTHERNAME\":\"THANGATHI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00027E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"T\",\"FNAME_FIRSTCHAR\":\"P\",\"ACTUAL_CUSTID\":\"AP00031367\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"T NADAR\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9867272321,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 16\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00109087\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":74000},{\"SCHEME\":\"GOLD 16\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00118474\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":189000},{\"SCHEME\":\"SUPER SAVER 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00167003\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":100000},{\"SCHEME\":\"SUPER SAVER 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00168553\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":110000},{\"SCHEME\":\"SUPER SAVER 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00169274\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":135000},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00172063\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":200000},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00172071\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":84000},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00172081\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":192000,\"SANCTIONED_AMOUNT\":192000},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00179986\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":100000},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00179996\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":192000},{\"SCHEME\":\"B1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00241084\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":10000},{\"SCHEME\":\"A5\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00255744\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":50000}],\"ADDRESSDETAIL\":[{\"STATE\":\"MAHARASHTRA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"C 2 2 ROOM NO 4 SECTOR 16 VASHI  VASHI  SECTOR 16\",\"PINCODE\":400703,\"CITY\":\"NAVIMUMBAI\"}],},{\"PAN\":\"COLPS1159F\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SAGAR\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Apr 18 2014  4:40PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"BALBIR  KUMAR GUPTA\",\"LOGINID\":1000000327,\"MOTHERNAME\":\"SAVITA\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00029E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00084772\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":200,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":8130753635,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00270986\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":19047}],\"ADDRESSDETAIL\":[{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"C-56 UPPER G/F L-TYPE UTTAM NAGAR HASTAL VIHAR\",\"PINCODE\":110059,\"CITY\":\"NEW DELHI\"},{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"C-56 UPPER G/F L-TYPE UTTAM NAGAR HASTAL VIHAR\",\"PINCODE\":110059,\"CITY\":\"NEW DELHI\"},{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"C-56 UPPER G/F L-TYPE UTTAM NAGAR HASTAL VIHAR\",\"PINCODE\":110059,\"CITY\":\"NEW DELHI\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"MURALIDHARAN\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec  9 2014  1:21PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"RAMASAMI  R\",\"LOGINID\":1000000835,\"MOTHERNAME\":\"DHANUSHI\",\"DL\":\"F/TN/046/001719/2001\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00035E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"M\",\"ACTUAL_CUSTID\":\"AP00053026\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"R\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9941917691,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00183988\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":59700},{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00260716\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":30100},{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00261599\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":0},{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00264842\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":22100},{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00274236\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":30000},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00326580\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":42000},{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00327869\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":11500},{\"SCHEME\":\"A1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00328226\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":15000},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00419406\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":98500},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00419414\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":14500},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00442910\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":29000}],\"ADDRESSDETAIL\":[{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"NO TF21 BLOCK 6 LAKSHMI NAGAR 4TH STAGE ABILASH APARTMENTS NANGANALLUR ABILASH APARTMENTS\",\"PINCODE\":600061,\"CITY\":\"CHENNAI\"},{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"NO TF21 BLOCK 6 LAKSHMI NAGAR 4TH STAGE ABILASH APARTMENTS NANGANALLUR ABILASH APARTMENTS\",\"PINCODE\":600061,\"CITY\":\"CHENNAI\"},{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Residence cum office\",\"ADDRESS\":\"NO TF21 BLOCK 6 LAKSHMI NAGAR 4TH STAGE ABILASH APARTMENTS NANGANALLUR ABILASH APARTMENTS\",\"PINCODE\":600061,\"CITY\":\"CHENNAI\"},{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"NO TF21 BLOCK 6 LAKSHMI NAGAR 4TH STAGE ABILASH APARTMENTS NANGANALLUR ABILASH APARTMENTS\",\"PINCODE\":600061,\"CITY\":\"CHENNAI\"}],},{\"PAN\":\"APFPS6081M\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"PYYYYYYYYYY\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec 10 2014 11:03AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"PUTTAPPA  A\",\"LOGINID\":1000000551,\"MOTHERNAME\":\"MUNIMARAMMA\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00035E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"P\",\"ACTUAL_CUSTID\":\"AP00106393\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"SIVARAM\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":8015803145,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"ME NEW CUSTOMER\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00328422\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":100000,\"SANCTIONED_AMOUNT\":100000}],\"ADDRESSDETAIL\":[{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"NO 316 1 MGR NAGAR  MOOKANDAPALLI POST HOSUR TALUK\",\"PINCODE\":635126,\"CITY\":\"DHARMAPURI\"},{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"NO 316 1 MGR NAGAR  MOOKANDAPALLI POST HOSUR TALUK\",\"PINCODE\":635126,\"CITY\":\"DHARMAPURI\"},{\"STATE\":\"TAMIL NADU\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"NO 65A 2 VENUGOPAL SWAMY STREET   MOOKANDAPALLI\",\"PINCODE\":635126,\"CITY\":\"DHARMAPURI\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"CHANDANA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Jul 22 2015 12:18PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"CHANDANA MOHAN RAO\",\"LOGINID\":1000000700,\"APPLICANTLOANINFO\":\"\",\"MOTHERNAME\":\"PASUPULETI\",\"DL\":\"AP02020120055539\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00039E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"U\",\"FNAME_FIRSTCHAR\":\"C\",\"ACTUAL_CUSTID\":\"AP00125245\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"UMA MAHESWAR RAO\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9550805698,\"DL_SOURCECITY\":\"\",\"ADDRESSDETAIL\":{\"ADDRESSLIST\":[{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"H NO 11 3 142 REVATHI TAKIES ROAD  KHAMMAM\",\"PINCODE\":507001,\"CITY\":\"KHAMMAM\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"H NO 11 3 142 REVATHI TAKIES ROAD  KHAMMAM\",\"PINCODE\":507001,\"CITY\":\"KHAMMAM\"}]},\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[],\"ADDRESSDETAIL\":[{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"H NO 11 3 142 REVATHI TAKIES ROAD  KHAMMAM\",\"PINCODE\":507001,\"CITY\":\"KHAMMAM\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"H NO 11 3 142 REVATHI TAKIES ROAD  KHAMMAM\",\"PINCODE\":507001,\"CITY\":\"KHAMMAM\"}],},{\"PAN\":\"ARLPS8120K\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"ARVIND\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Oct  7 2015 10:31AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"RAM YAGYA SINGH\",\"LOGINID\":1000000949,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.0004E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"A\",\"ACTUAL_CUSTID\":\"AP00133052\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"SINGH\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9212203674,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"SE NEW CUSTOMER\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00402173\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":0}],\"ADDRESSDETAIL\":[{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"G 23 HARKESH NAGAR OKHLA INDL ESTATE BLOCK G\",\"PINCODE\":110020,\"CITY\":\"NEW DELHI\"},{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"G 23 HARKESH NAGAR OKHLA INDL ESTATE BLOCK G\",\"PINCODE\":110020,\"CITY\":\"NEW DELHI\"},{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"D 242 KARAN VIHAR GHAZIABAD KHORA COLONY\",\"PINCODE\":201010,\"CITY\":\"GHAZIABAD\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SANTOSH\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Nov 19 2015  1:13PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"GANPAT  GAYAKWAR\",\"LOGINID\":1000000440,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00041E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00140907\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"GANPAT\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9887434355,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"B1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00413012\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":60000},{\"SCHEME\":\"A3\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00462033\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":85000}],\"ADDRESSDETAIL\":[{\"STATE\":\"RAJASTHAN\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"3140 CHANDPOLE BAZAR BAGRU WALON KA RASTA JAIPUR JAIPUR\",\"PINCODE\":302002,\"CITY\":\"JAIPUR\"},{\"STATE\":\"MAHARASHTRA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"0 TEHSIL SANGLI VILLAGE BHIKANDI MAHARASTRA MAHARASTRA\",\"PINCODE\":415310,\"CITY\":\"SANGLI\"},{\"STATE\":\"RAJASTHAN\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"3140 CHANDPOLE BAZAR BAGRU WALON KA RASTA JAIPUR JAIPUR\",\"PINCODE\":302002,\"CITY\":\"JAIPUR\"}],},{\"PAN\":\"ABHPY6717A\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SIVA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec 16 2015  3:44AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"DASARADHA RAMAIAH IMMADI\",\"LOGINID\":1000000712,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00041E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"N\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00145645\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"NAGA MALLESWARARAO IMMADI\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9392792229,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"SCV New\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00420659\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":344000},{\"SCHEME\":\"SCV New\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00569235\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":344000}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"D NO 3 22  ETUKURU POST  GUNTUR DIST\",\"PINCODE\":522017,\"CITY\":\"GUNTUR\"},{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"D NO 3 22  ETUKURU POST  GUNTUR DIST\",\"PINCODE\":522017,\"CITY\":\"GUNTUR\"}],},{\"PAN\":\"ANYPC5503D\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"CHANDRASHEKAR\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec 26 2015  1:11PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"SIDDA  LINGAPPA\",\"LOGINID\":1000000350,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00041E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"C\",\"ACTUAL_CUSTID\":\"AP00147624\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"S\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9538188707,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"SCV New\",\"OS_OTHERCHARGES\":22650,\"PROSPECTCODE\":\"PR00423672\",\"OS_INTEREST\":1624,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":107080,\"SANCTIONED_AMOUNT\":367696}],\"ADDRESSDETAIL\":[{\"STATE\":\"KARNATAKA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"NO-82/3 5 TH CROSS 8 TH MAIN ROAD KTG COLLEGE MAIN ROAD NA SRIGHANDHA NAGAR SUNKADAKATTE 40 FEET ROAD\",\"PINCODE\":560091,\"CITY\":\"BANGALORE\"},{\"STATE\":\"KARNATAKA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"NO 999 6 TH CROSS GAJANANNA NAGAR NA HEGGANA HALLI PEENYA SECOND STAGE NA\",\"PINCODE\":560091,\"CITY\":\"BANGALORE\"},{\"STATE\":\"KARNATAKA\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"NO-82/3 5 TH CROSS 8 TH MAIN ROAD KTG COLLEGE MAIN ROAD NA SRIGHANDHA NAGAR SUNKADAKATTE 40 FEET ROAD\",\"PINCODE\":560091,\"CITY\":\"BANGALORE\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"ALETI\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Aug 23 2016 11:18AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"A BRAMMA RAO\",\"LOGINID\":1000001097,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00044E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"A\",\"ACTUAL_CUSTID\":\"AP00167482\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"NIRMALA\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":8008641931,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A4\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00492536\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":49500},{\"SCHEME\":\"N1\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00547870\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":11000},{\"SCHEME\":\"N3\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00567353\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":37000},{\"SCHEME\":\"K3\",\"OS_OTHERCHARGES\":12099,\"PROSPECTCODE\":\"PR00576505\",\"OS_INTEREST\":715,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":11000},{\"SCHEME\":\"N3\",\"OS_OTHERCHARGES\":1060,\"PROSPECTCODE\":\"PR00591850\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":43000,\"SANCTIONED_AMOUNT\":43000}],\"ADDRESSDETAIL\":[{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"3-158/1625 DANVAIGUDEM COLONY NA KHAMMAM SAIBABA TEMPLE\",\"PINCODE\":507003,\"CITY\":\"KHAMMAM\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"3-158/1625 DANVAIGUDEM COLONY NA KHAMMAM SAIBABA TEMPLE\",\"PINCODE\":507003,\"CITY\":\"KHAMMAM\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"3-158/1625 DANVAIGUDEM COLONY NA KHAMMAM SAIBABA TEMPLE\",\"PINCODE\":507003,\"CITY\":\"KHAMMAM\"}],},{\"PAN\":\"AAAAA4460K\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"JITENDRA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec 30 2016 12:00PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"KRISHANPAL  SINGH\",\"LOGINID\":1000000744,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00044E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"P\",\"FNAME_FIRSTCHAR\":\"J\",\"ACTUAL_CUSTID\":\"AP00170374\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"PAL SINGH\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9954320690,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A4\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00517729\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":25125}],\"ADDRESSDETAIL\":[{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"37A AP 32 NEAR ALKAPURI AGRA TEMPLE\",\"PINCODE\":282001,\"CITY\":\"AGRA\"},{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"0 FIROZABAD RAJPURA MILK DIARY NEAR\",\"PINCODE\":283203,\"CITY\":\"AGRA\"},{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"37A AP 32 NEAR ALKAPURI AGRA TEMPLE\",\"PINCODE\":282001,\"CITY\":\"AGRA\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"GURPREET\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Oct  1 2018  1:40PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"AJAIB   SINGH\",\"LOGINID\":1000001567,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.0005E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"G\",\"ACTUAL_CUSTID\":\"AP00189694\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"SINGH\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9041466059,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A3\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00614795\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":59530,\"SANCTIONED_AMOUNT\":59530},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00632107\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":26200,\"SANCTIONED_AMOUNT\":26200}],\"ADDRESSDETAIL\":[{\"STATE\":\"PUNJAB\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"H NO 96 VILL SEKHPURA  0 VILL SHEKHPURA  NEAR PUNJABI UNI  PATIALA NEAR GURDWARA SAHIB\",\"PINCODE\":147002,\"CITY\":\"PATIALA\"},{\"STATE\":\"PUNJAB\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"H NO 96 VILL SEKHPURA  0 VILL SHEKHPURA  NEAR PUNJABI UNI  PATIALA NEAR GURDWARA SAHIB\",\"PINCODE\":147002,\"CITY\":\"PATIALA\"},{\"STATE\":\"PUNJAB\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"H NO 96 VILL SEKHPURA  0 VILL SHEKHPURA  NEAR PUNJABI UNI  PATIALA NEAR GURDWARA SAHIB\",\"PINCODE\":147002,\"CITY\":\"PATIALA\"}],},{\"PAN\":\"AEJPN0823P\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"MRUNALINI\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Oct 11 2018  1:31PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"SAMEER  NEVASE\",\"LOGINID\":1000000275,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.0005E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"S\",\"FNAME_FIRSTCHAR\":\"M\",\"ACTUAL_CUSTID\":\"AP00190560\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"SAMEER NEVASE\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":7040944397,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00616584\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":140090},{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00619244\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":88000}],\"ADDRESSDETAIL\":[{\"STATE\":\"MAHARASHTRA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"F NO A6  PCMC CHOWK MAHALAXMI HEIGHTS BHOSRI NR PCMC BUS STOP\",\"PINCODE\":411039,\"CITY\":\"PUNE\"},{\"STATE\":\"MAHARASHTRA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"F NO A6  PCMC CHOWK MAHALAXMI HEIGHTS BHOSRI NR PCMC BUS STOP\",\"PINCODE\":411039,\"CITY\":\"PUNE\"},{\"STATE\":\"MAHARASHTRA\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"F NO A6  PCMC CHOWK MAHALAXMI HEIGHTS BHOSRI NR PCMC BUS STOP\",\"PINCODE\":411039,\"CITY\":\"PUNE\"}],},{\"PAN\":\"AOMPG5468C\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SAGAR\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"07-Jun-1989\",\"PRODUCT_DT\":\"Nov 29 2018  6:48PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"VINOD  GUPTA\",\"LOGINID\":1000001639,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00052E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00193141\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9874226169,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A6\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00625561\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":64000,\"SANCTIONED_AMOUNT\":64000}],\"ADDRESSDETAIL\":[{\"STATE\":\"WEST BENGAL\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"14B,D.C ROAD KHIDDIRPORE NA NA NEAR HOSSAIN SAHA PARK\",\"PINCODE\":700023,\"CITY\":\"KOLKATA\"},{\"STATE\":\"WEST BENGAL\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"14B,D.C ROAD KHIDDIRPORE   NEAR HOSSAIN SAHA PARK\",\"PINCODE\":700023,\"CITY\":\"KOLKATA\"},{\"STATE\":\"WEST BENGAL\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"52 BENI BANERJEE AVINEW DHAKURIA\",\"PINCODE\":700031,\"CITY\":\"KOLKATA\"}],},{\"PAN\":\"APKPB1433M\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"NARAYANA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Feb  9 2019 11:05AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"MADHU  BELLAMKONDA\",\"LOGINID\":1000001651,\"MOTHERNAME\":\"\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00056E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"N\",\"ACTUAL_CUSTID\":\"AP00198473\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"BELLAMKONDA\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9848668081,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00640104\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":351000,\"SANCTIONED_AMOUNT\":351000},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00640112\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":89050},{\"SCHEME\":\"A7\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00640125\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"DISBURSED\",\"OS_PRINCIPAL\":89050,\"SANCTIONED_AMOUNT\":89050}],\"ADDRESSDETAIL\":[{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"F 201 SIXTH PHASE PLOT HIG FOUR SEVENTY THREE KPHB COLONY MARUTHI BHAVAN APPARTMENTS\",\"PINCODE\":500085,\"CITY\":\"HYDERABAD\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"F 201 SIXTH PHASE PLOT HIG FOUR SEVENTY THREE KPHB COLONY MARUTHI BHAVAN APPARTMENTS\",\"PINCODE\":500085,\"CITY\":\"HYDERABAD\"},{\"STATE\":\"TELANGANA\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"F 201 SIXTH PHASE PLOT HIG FOUR SEVENTY THREE KPHB COLONY MARUTHI BHAVAN APPARTMENTS\",\"PINCODE\":500085,\"CITY\":\"HYDERABAD\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"MOHAMMED\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Jan 23 2012  6:34PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"ABDUL  JABBAR\",\"LOGINID\":1000000296,\"MOTHERNAME\":\"NOORJAHAN\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00005E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"M\",\"ACTUAL_CUSTID\":\"AP00016791\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"ZAHEER\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9703043223,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"B18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00049797\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":113000}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"9-4-4/E/2  NSF COLONY TOLICHOWKI IDIAL SCHOOL\",\"PINCODE\":500008,\"CITY\":\"HYDERABAD\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"RAMACHANDRIKA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Jun 12 2012  2:45PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"VEERAJU  LINGALA\",\"LOGINID\":1000000405,\"MOTHERNAME\":\"KOMMU\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00009E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"P\",\"FNAME_FIRSTCHAR\":\"R\",\"ACTUAL_CUSTID\":\"AP00026828\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"PRASAD  LINGALA\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9550785344,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD4\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00088446\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":110000}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"19-3-7 POLICE LINE STREET  BANKPETA BLOCK NO 1\",\"PINCODE\":533001,\"CITY\":\"KAKINADA\"},{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"19-3-7 POLICE LINE STREET  BANKPETA BLOCK NO 1\",\"PINCODE\":533001,\"CITY\":\"KAKINADA\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"SATYANARAYANA\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Jul  9 2012  5:23PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"MALLAIAH  T\",\"LOGINID\":1000000459,\"MOTHERNAME\":\"SHIVAMMA\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.0001E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"S\",\"ACTUAL_CUSTID\":\"AP00026620\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"T\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":8125321330,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD4\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00087561\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":120306},{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00098856\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":71000}],\"ADDRESSDETAIL\":[{\"STATE\":\"ANDHRA PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"2-72 HANUMA SAI NAGAR  UPPAL\",\"PINCODE\":500039,\"CITY\":\"HYDERABAD\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"DHIRENDER\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Sep 24 2012  4:01PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"KUMBH RAJ SINGH\",\"LOGINID\":1000000472,\"MOTHERNAME\":\"SHILA DEVI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00013E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"D\",\"ACTUAL_CUSTID\":\"AP00035664\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9815657655,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00126311\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":118000}],\"ADDRESSDETAIL\":[{\"STATE\":\"CHANDIGARH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"69  VILL KAJHERI CHANDIGARH\",\"PINCODE\":160047,\"CITY\":\"CHANDIGARH\"},{\"STATE\":\"CHANDIGARH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"69  VILL KAJHERI CHANDIGARH\",\"PINCODE\":160047,\"CITY\":\"CHANDIGARH\"},{\"STATE\":\"CHANDIGARH\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"69  VILL KAJHERI CHANDIGARH\",\"PINCODE\":160047,\"CITY\":\"CHANDIGARH\"}],},{\"PAN\":\"ASNPK7665C\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"MUKESH\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Dec 31 2012 12:41PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"HARIDWARI  LAL\",\"LOGINID\":1000000602,\"MOTHERNAME\":\"LAXMI DEVI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00016E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"M\",\"ACTUAL_CUSTID\":\"AP00029435\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9953225343,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 17\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00100280\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":66500},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00160317\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":70000}],\"ADDRESSDETAIL\":[{\"STATE\":\"DELHI\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"HNO 7WS BHAGAT SINGH GALI BABARPUR SHAHDARA  DELHI NEAR POST OFFICE\",\"PINCODE\":110032,\"CITY\":\"NEW DELHI\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"HARI\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Feb 15 2013  3:24PM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"GANESH  PRASAD\",\"LOGINID\":1000000559,\"MOTHERNAME\":\"SUBHADRA DEVI\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00018E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"H\",\"ACTUAL_CUSTID\":\"AP00044077\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"KRISHNA\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":9305087485,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00156484\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":7500},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00175468\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":9500}],\"ADDRESSDETAIL\":[{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"56 HARJENDAR NAGAR PURANA  DVITEEY\",\"PINCODE\":208007,\"CITY\":\"KANPUR\"},{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"56 HARJENDAR NAGAR PURANA  DVITEEY\",\"PINCODE\":208007,\"CITY\":\"KANPUR\"},{\"STATE\":\"UTTAR PRADESH\",\"ADDRESSTYPE\":\"Office Address\",\"ADDRESS\":\"1  HALSI ROAD\",\"PINCODE\":208001,\"CITY\":\"KANPUR\"}],},{\"PAN\":\"\",\"EXACTMATCH_OF_CUSTID\":\"\",\"FNAME\":\"RAJESH\",\"EMAIL_USER\":\"\",\"PASSPORTNO\":\"\",\"DOB\":\"04-May-1978\",\"PRODUCT_DT\":\"Mar 20 2013 11:45AM\",\"STRONGMATCH_OF_CUSTID\":\"\",\"FATHERNAME\":\"DESHRAJ  SHARMA\",\"LOGINID\":1000000127,\"MOTHERNAME\":\"VIJAY\",\"DL\":\"\",\"SOURCE\":\"NEWCUSTOMER\",\"VOTERIDCARDNO\":\"\",\"NEWCUST_CUSTID\":1.00019E9,\"EMAIL\":\"\",\"DESCR\":\"\",\"LNAME_FIRSTCHAR\":\"&#x20;\",\"FNAME_FIRSTCHAR\":\"R\",\"ACTUAL_CUSTID\":\"AP00032428\",\"CUSTT\":\"\",\"MNAME\":\"\",\"SCORE\":50,\"DESCRIPTION\":\"Description Here...\",\"PRODUCT\":\"PERSONAL LOAN\",\"LNAME\":\"SHARMA\",\"BRANCH\":\"\",\"REFERENCEDBY\":\"\",\"MOBILE\":7503262000,\"DL_SOURCECITY\":\"\",\"GENDER\":\"\",\"MNAME_FIRSTCHAR\":\"\",\"REFERENCETYPE\":\"\",\"REGION_CODE\":\"\",\"RCARDNUMBER\":\"\",\"MATCH_TYPE\":\"\",\"TITLE\":\"\",\"CUSTTYPE\":\"Applicant\",\"APPLICANTLOANINFO\":[{\"SCHEME\":\"GOLD 18\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00113287\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":15611},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00128050\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":0},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00128054\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":0},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00128063\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"REJECT\",\"OS_PRINCIPAL\":0},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00128159\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":10500},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00131555\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":12500},{\"SCHEME\":\"GOLD 20\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00171671\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":22500},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00182852\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":17500},{\"SCHEME\":\"GOLD2\",\"OS_OTHERCHARGES\":0,\"PROSPECTCODE\":\"PR00187422\",\"OS_INTEREST\":0,\"LOAN_STATUS\":\"FORCLOSED\",\"OS_PRINCIPAL\":0,\"SANCTIONED_AMOUNT\":12500}],\"ADDRESSDETAIL\":[{\"STATE\":\"HARYANA\",\"ADDRESSTYPE\":\"Residence Address\",\"ADDRESS\":\"2940 BLOCK A   SGM NAGAR\",\"PINCODE\":121001,\"CITY\":\"FARIDABAD\"},{\"STATE\":\"HARYANA\",\"ADDRESSTYPE\":\"Permanent Address\",\"ADDRESS\":\"2940 BLOCK A   SGM NAGAR\",\"PINCODE\":121001,\"CITY\":\"FARIDABAD\"}]}]}}\n";
        MifinReciveResponse mifinReciveResponse = new MifinReciveResponse();

        mifinReciveResponse.setResponse(response);
        MifinTopUpResponse mifinTopUpresponse = new MifinTopUpResponse();

        TopUpDedupeResponse topUpDedupeResponse = mifinTopUpresponse.getResponseMifin();
        try {
            mifinTopUpresponse = JsonUtil.StringToObject(mifinReciveResponse.getResponse(), MifinTopUpResponse.class);

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }

        topUpDedupeResponse = mifinTopUpresponse.getResponseMifin();
        mifinTopUpresponse.setResponseMifin(topUpDedupeResponse);
        logger.info("Mifin Response {} ", mifinTopUpresponse.getResponseMifin());
        logger.info("mifinTopUpresponse {}", mifinTopUpresponse);
        logger.info("TopUpDedupeResponse {}", mifinTopUpresponse.getResponseMifin());
        return mifinReciveResponse;
    }

    private List<LoanSummaryDetail>  sortPropectCode(List<LoanSummaryDetail> resLoanSummaryDeatilList)  {

        List<LoanSummaryDetail> loanSummaryDeatilList = new ArrayList<LoanSummaryDetail>();
        //templist.sort();
            ;
            try {
                List<LoanSummaryDetail> resloanSummaryDeatilList  = resLoanSummaryDeatilList;
                if(CollectionUtils.isNotEmpty(resloanSummaryDeatilList)){
                    for(Object  loanSummaryDetail1 : resloanSummaryDeatilList){
                        LoanSummaryDetail loanSummaryDetail = new LoanSummaryDetail();
                        loanSummaryDetail = JsonUtil.MapToObject((Map<String, Object>)loanSummaryDetail1, LoanSummaryDetail.class);
                        loanSummaryDeatilList.add(loanSummaryDetail);
                    }
                    if(resloanSummaryDeatilList.size() > 1){
                        loanSummaryDeatilList.sort(MiFinHelper.sortByProspectCode);
                        Collections.sort(loanSummaryDeatilList, Collections.reverseOrder());
                    }
                }
            } catch (IOException e) {
                logger.error("{}",e.getStackTrace());
            }


        logger.info("sorted list {} ",loanSummaryDeatilList );
        return loanSummaryDeatilList;
    }

    public void calTotalExposureAmt(TopUpDedupeResponse topUpDedupeResponse) {

        /*To add total Exposure value to show on ExistingLoan Screen*/
        double totalExposure = 0 ;

        if(CollectionUtils.isNotEmpty(topUpDedupeResponse.getApplicantlist())){
            List<com.softcell.gonogo.model.mifin.topup.Applicant> applicantList = topUpDedupeResponse.getApplicantlist();

            for(com.softcell.gonogo.model.mifin.topup.Applicant applicant : applicantList){
                List<ProspectDetails> prospectDetailsList =  applicant.getProspectList();
                for(ProspectDetails prospectDetails : prospectDetailsList){
                    if(StringUtils.equalsIgnoreCase(LOAN_STATUS, prospectDetails.getLoanStatus())){
                        double pricipalOs = Double.parseDouble(GngUtils.setValueForZero(prospectDetails.getProncipalOutstanding()));
                        totalExposure = totalExposure + pricipalOs;

                    }
                }
            }
        }
        topUpDedupeResponse.setTotalExposure(Math.round(totalExposure));
    }

    public void sortDisbursalDate(String refId, List<com.softcell.gonogo.model.mifin.topup.Applicant>  applicantList) {

        /*Sort disbursal date in ascending Order. Required for MOB calculation in case of topUp */
        TopUpInfo topUpInfo = null;

        try {
            GoNoGoCustomerApplication goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo() == null){
                topUpInfo = new TopUpInfo();
            }else{
                topUpInfo = goNoGoCustomerApplication.getApplicationRequest().getRequest().getTopupInfo();
            }
            List<String> date = new ArrayList<>();

            for (com.softcell.gonogo.model.mifin.topup.Applicant applicant : applicantList) {
                if (CollectionUtils.isNotEmpty(applicant.getProspectList())) {
                    List<ProspectDetails> prospectList = applicant.getProspectList();
                    if(CollectionUtils.isNotEmpty(prospectList)){
                        for(ProspectDetails prospectDetails : prospectList){
                            if(prospectDetails.dedupeFlag){
                                date.add(prospectDetails.getDisbursalDate());
                            }

                        }
                    }
                }
            }

            if(CollectionUtils.isNotEmpty(date)){
                    Collections.sort(date);
                    topUpInfo.setDisbursalDate(date.get(0));
            }

            goNoGoCustomerApplication.getApplicationRequest().getRequest().setTopupInfo(topUpInfo);
            applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());
        } catch (Exception e) {
           logger.error("{} exception occurn in mifin Api ",e.getStackTrace());
        }

    }

    public Asset getAssetDetails(ApplicationRequest applicationRequest, LegalVerification legalVerification, CamDetails camDetails) {
        Asset asset = new Asset();
        List<Collateral> collateralList = applicationRequest.getRequest().getApplication().getCollateral();
        Valuation valuation = getValuationDetails(applicationRequest);
        logger.debug("Setting assets ... Valuation fetched : {}", valuation == null ? false : true);
        List<com.softcell.gonogo.model.mifin.AssetDetails> assetDetailsList = new ArrayList<>();
        if (valuation != null && CollectionUtils.isNotEmpty(valuation.getValuationDetailsList()))
        {
            List<ValuationDetails> valuationDetailsList = valuation.getValuationDetailsList();
            if(CollectionUtils.isNotEmpty(collateralList)){
                for(Collateral  collateral : collateralList){
                    com.softcell.gonogo.model.mifin.AssetDetails assetDetails = new com.softcell.gonogo.model.mifin.AssetDetails();
                    List<com.softcell.gonogo.model.mifin.Valuation> valuationList = new ArrayList<com.softcell.gonogo.model.mifin.Valuation>();

                    assetDetails.setPropertyAdd1(collateral.getAddress().getAddressLine1());
                    assetDetails.setPropertyAdd2(collateral.getAddress().getAddressLine2());
                    assetDetails.setCity(collateral.getAddress().getCity());
                    assetDetails.setLandmark(collateral.getAddress().getLandMark());
                    assetDetails.setZipcode(String.valueOf(collateral.getAddress().getPin()));
                    assetDetails.setState(collateral.getAddress().getState());
                    assetDetails.setOwnerFName(collateral.getOwnerNames().get(0).getFirstName());
                    assetDetails.setOwnerMName(collateral.getOwnerNames().get(0).getMiddleName());
                    assetDetails.setOwnerLName(collateral.getOwnerNames().get(0).getLastName());
                    assetDetails.setPropertyAdd3(trim(Constant.BLANK, 50));
                    for(ValuationDetails valuationDetails : valuationDetailsList){
                        if(StringUtils.equalsIgnoreCase(valuationDetails.getCollateralId(), collateral.getCollateralId())){
                            com.softcell.gonogo.model.mifin.Valuation mifinValuatiuon= new com.softcell.gonogo.model.mifin.Valuation();

                            if(valuationDetails.getValuationOutput() != null && valuationDetails.getValuationOutput().getPropertyValuationDate() != null){
                                mifinValuatiuon.setEvaluationDate(GngUtils.setValueForNull(GngDateUtil.getYYYYMMDDFormat(
                                        valuationDetails.getValuationOutput().getPropertyValuationDate())));
                            }else{
                                mifinValuatiuon.setEvaluationDate(Constant.BLANK);
                            }

                            if(applicationRequest.getHeader().getProduct() == Product.LAP){
                                if(null != camDetails && null != camDetails.getSummary())
                                    mifinValuatiuon.setMarketValue(Integer.toString(camDetails.getSummary().getCollateralList().stream()
                                            .filter(obj -> StringUtils.equalsIgnoreCase(obj.getCollateralId(), valuationDetails.getCollateralId()))
                                            .map(obj -> obj.getValueToConsider())
                                            .findAny().orElse(0)));
                            }else {
                                mifinValuatiuon.setMarketValue(valuationDetails.getValuationOutput() != null && valuationDetails.getValuationOutput().getCurrentMarketValue() != null ?
                                        valuationDetails.getValuationOutput().getCurrentMarketValue() : Constant.BLANK);
                            }
                            mifinValuatiuon.setPropertyUsage(GngUtils.setValueForNull(MiFinHelper.PROPERTY_USAGE.get(collateral.getUsage())));
                            mifinValuatiuon.setPropertyStatus(GngUtils.setValueForNull(MiFinHelper.OCCUPANCY_STATUS.get(collateral.getOccupation())));
                            mifinValuatiuon.setReplacementCost(Constant.ZERO);
                            mifinValuatiuon.setValuatorName(GngUtils.setValueForNull(StringUtils.substring(StringUtils.trim(valuationDetails.getAgencyCode()), 0, 50)));
                            if(StringUtils.isEmpty(mifinValuatiuon.getValuatorName()) && applicationRequest.getRequest().getTopupInfo() != null){
                                mifinValuatiuon.setValuatorName(MiFinHelper.VALUATOR_AGENCY_CODE);
                            }
                            mifinValuatiuon.setProResidualAge(valuationDetails.getValuationOutput() != null && valuationDetails.getValuationOutput().getResidualAge() != null ?
                                    valuationDetails.getValuationOutput().getResidualAge() : Constant.ZERO);
                            mifinValuatiuon.setPropertyAge(Constant.ZERO);
                            valuationList.add(mifinValuatiuon);
                        }
                        assetDetails.setValuationDetailList(valuationList);
                    }
                    List<LegalVerificationDetails> legalVerificationDetailsList =  legalVerification.getLegalVerificationDetailsList();
                    for(LegalVerificationDetails  legalVerificationDetails :legalVerificationDetailsList){

                        if(StringUtils.equalsIgnoreCase(legalVerificationDetails.getCollateralId(), collateral.getCollateralId())){
                            assetDetails.setPropertyType(legalVerificationDetails.getLegalVerificationOutput().getTypeOfProp());
                        }
                    }
                    if (StringUtils.isEmpty(assetDetails.getPropertyType())) {
                        assetDetails.setPropertyType(MiFinHelper.PROPERTY_TYPE_FREEHOLD);
                    }
                    logger.debug(" refid  {} AssetDetails property type value {} ",applicationRequest.getRefID(), assetDetails.getPropertyType());
                    assetDetailsList.add(assetDetails);
                }
            }

        }
        asset.setAssetDetailsList(assetDetailsList);
        return asset;
    }

    private Valuation getValuationDetails(ApplicationRequest applicationRequest) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setRefId(applicationRequest.getRefID());
        valuationRequest.setHeader(applicationRequest.getHeader());
        return applicationRepository.fetchValuationDetails(valuationRequest);
    }

    private String trim(String value, int trimIndex) {
        return StringUtils.substring(StringUtils.trim(value), 0, trimIndex);
    }

    public void setChargeDetailsForOrigination(List<com.softcell.gonogo.model.mifin.ChargeDetails> chargeDetailsList) {
        if(CollectionUtils.isNotEmpty(chargeDetailsList)){
            Iterator itr = chargeDetailsList.iterator();
            com.softcell.gonogo.model.mifin.ChargeDetails chargeDetails = null;
            while(itr.hasNext()){
                chargeDetails = (com.softcell.gonogo.model.mifin.ChargeDetails)itr.next();
                chargeDetails.setChargeAppliedOn(Constant.BLOCKSTATUS);
            }
        }
    }

    public String setSchemeWiseMaster(String scheme, String originationScheme ) {
        if(StringUtils.isNotEmpty(originationScheme) &&
        StringUtils.isNotEmpty(scheme) ){
          return   scheme.concat(Constant.SPACE).concat(originationScheme);
        }
        return null;

    }

    public LoanCharges getTranchWiseLoanCharges(ApplicationRequest applicationRequest, List<LoanCharges> loanChargesList){
        LoanCharges loanCharges = null;
        try {
            String percentage = fetchPercentage(applicationRequest);
            if (StringUtils.isNotEmpty(percentage)) {
                loanCharges = loanChargesList.stream()
                        .filter(response -> Double.valueOf(response.getLoanPercentage()).intValue() == Double.valueOf(percentage).intValue())
                        .findAny().orElse(null);
            }
        }catch(Exception e){
            logger.error("{ } refId, Exception occurred while fetching Loan Charges for {} product. Exception {}",
                    applicationRequest.getRefID(), applicationRequest.getHeader().getProduct().name(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("loanCharges {}", loanCharges);
        return loanCharges;
    }

    public DMInput getTranchWiseDMInput(ApplicationRequest applicationRequest, List<DMInput> dmInputList){
        DMInput dmInput = null;
        try {
            String percentage = fetchPercentage(applicationRequest);
            if (StringUtils.isNotEmpty(percentage)) {
                dmInput = dmInputList.stream()
                        .filter(response -> Double.valueOf(response.getLoanPercentage()).intValue() == Double.valueOf(percentage).intValue())
                        .findAny().orElse(null);
            }
        }catch(Exception e){
            logger.error("{} refId, Exception occurred while fetching DMInput for {} product. Exception {}",
                    applicationRequest.getRefID(), applicationRequest.getHeader().getProduct().name(), ExceptionUtils.getStackTrace(e));
        }
        logger.debug("dmInput {}", dmInput);
        return dmInput;
    }

    private String fetchPercentage(ApplicationRequest applicationRequest){
        DropdownMasterRequest dropdownMasterRequest = new DropdownMasterRequest().builder()
                .header(applicationRequest.getHeader())
                .dropDownType("LOAN_PERCENTAGE")
                .queryType(EndPointReferrer.ONE)
                .build();
        List<DropdownMaster> dropDownMaster = masterRepository.fetchDropDownMaster(dropdownMasterRequest);
        String percentage = null;
        if(CollectionUtils.isNotEmpty(dropDownMaster)) {
            if (StringUtils.equalsIgnoreCase(applicationRequest.getCurrentStageId(), Stages.Stage.DE.name()))
                percentage = dropDownMaster.get(0).getDropDownValueDetailsList().get(0).get("sLoanPercentage_Stp");
            else if (Stages.isTranchStage(applicationRequest.getCurrentStageId()))
                percentage = dropDownMaster.get(0).getDropDownValueDetailsList().get(0).get("sLoanPercentage_Remain");
        }
        logger.debug("percentage {}", percentage);
        return percentage;
    }
    public IMDMiFinRequest builIMDRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, InitialMoneyDeposit imd2) {
        return IMDMiFinRequest.builder()
                .basicInfo(buildBasicinfo(goNoGoCustomerApplication, imd2))
                .build();
    }

    private com.softcell.gonogo.model.mifin.IMD.BasicInfo buildBasicinfo(GoNoGoCustomerApplication goNoGoCustomerApplication, InitialMoneyDeposit imd2) {
        return com.softcell.gonogo.model.mifin.IMD.BasicInfo.builder()
                .imd_DETAILS(buildImdDetails(goNoGoCustomerApplication, imd2))
                .build();
    }

    private List<IMDDetails> buildImdDetails(GoNoGoCustomerApplication goNoGoCustApp, InitialMoneyDeposit imd2) {
        List<IMDDetails> imdDetails = new ArrayList<>();
        Branch branchV2 = goNoGoCustApp.getApplicationRequest().getAppMetaData().getBranchV2();
        imdDetails.add(IMDDetails.builder()
                .appRefNo(goNoGoCustApp.getGngRefId())
                .modeOfPayment(imd2.getInstrumentType())
                .utr(!StringUtils.equalsIgnoreCase(imd2.getInstrumentType(),"Cash")
                        ? imd2.getInstrumentNumber() : goNoGoCustApp.getGngRefId())
                .imdAmt(Double.toString(imd2.getAmount()))
                .transactionDt(dateFormatHelper(new Date()))
                .clearanceDt(dateFormatHelper(imd2.getClearingDate()))
                .product("SBL")
                .branch(StringUtils.isNotEmpty(branchV2.getHubBranchName()) ? branchV2.getHubBranchName() : branchV2.getBranchName())
                .custAddress(buildAddress(goNoGoCustApp.getApplicationRequest().getRequest()))
                .build());
        return imdDetails;
    }

    private String buildTransactionDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String myDate = "2022/03/31 00:00:00";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy");
        Date date = null;
        try {
            date = sdf.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            return simpleDateFormat.format(date);
        } else {
            return simpleDateFormat.format(new Date());
        }
    }

    private String dateFormatHelper(Date dateToConvert){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy");
        return simpleDateFormat.format(dateToConvert);
    }

    private String buildAddress(Request request) {
        AtomicReference<String> addressTemp = new AtomicReference<>(Constant.BLANK);
        if (null != request.getApplicant() && CollectionUtils.isNotEmpty(request.getApplicant().getAddress())) {
            if (StringUtils.equalsIgnoreCase(ApplicantType.INDIVIDUAL.value(), request.getApplicant().getApplicantType())) {
                request.getApplicant().getAddress().stream().forEach(address -> {
                    if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.RESIDENCE.name(), address.getAddressType())) {
                        addressTemp.set(address.getAddressLine1() + ", " + address.getAddressLine2());
                    }
                });
            } else {
                request.getApplicant().getAddress().stream().forEach(address -> {
                    if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.OFFICE.name(), address.getAddressType())) {
                        addressTemp.set(address.getAddressLine1() + ", " + address.getAddressLine2());
                    }
                });
            }
        }
        return addressTemp.get();
    }

}