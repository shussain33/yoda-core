package com.softcell.service;

import com.softcell.constants.master.MasterDetails;
import com.softcell.gonogo.model.core.error.FileUploadStatus;

import java.io.File;
import java.io.IOException;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public interface GenericUpload {

    FileUploadStatus uploadPincode(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadBranch(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadDealerEmail(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadInsurancePremium(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadState(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadCityState(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadScheme(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadAssetModel(File file, MasterDetails masterName, String institutionId) throws IOException;

    FileUploadStatus uploadApiRoleAuthorisationMaster(File file,MasterDetails masterDetails,String institutionId,String sourceId) throws Exception;
}
