package com.softcell.service;

import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by mahesh  on 24/6/17.
 */
public interface DMSFeedManager {

    /**
     *
     * @param pushDMSDocumentsRequest
     * @return
     */
    BaseResponse pushDocumentsToDms(PushDMSDocumentsRequest pushDMSDocumentsRequest) throws Exception;

    /**
     *
     * @param refId
     * @return
     */
    BaseResponse getPushDocumentsInfo(String refId, String institutionId);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse getDMSErrorLog(String refId, String institutionId);

}
