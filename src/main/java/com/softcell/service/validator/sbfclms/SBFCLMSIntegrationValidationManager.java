package com.softcell.service.validator.sbfclms;

import com.softcell.constants.ApplicantType;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.lms.DisburseResponse;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * Created by kumar on 20/7/18.
 */
@Component
public class SBFCLMSIntegrationValidationManager {

    public DisburseResponse validateRquest(GoNoGoCustomerApplication goNoGoCustomerApplication, Map lmsMasterDataMap, LoanCharges loanCharges, DisbursementMemo disbursementMemo , CamDetails camDetails) {

        DisburseResponse disburseResponse = DisburseResponse.builder().build();

        // Check Applicant
        if (goNoGoCustomerApplication == null ||
                goNoGoCustomerApplication.getDateTime() == null ||
                goNoGoCustomerApplication.getApplicationRequest() == null ||
                goNoGoCustomerApplication.getApplicationRequest().getRequest() == null ||
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant() == null ||
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication() == null ||
                goNoGoCustomerApplication.getApplicationRequest().getAppMetaData() == null){

            disburseResponse = DisburseResponse.builder()
                    .error_description("Invalid Gonogo Customer Application.")
                    .build();
        } else {

            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

            // Check Kyc
            if (applicant.getKyc() != null){

                boolean invalidKyc = true;
                for (Kyc kycdoc: applicant.getKyc()) {

                    if (StringUtils.isNotBlank(kycdoc.getKycNumber()) &&
                            StringUtils.isNotBlank(kycdoc.getKycName()) &&
                            lmsMasterDataMap.containsKey(kycdoc.getKycName())){

                        invalidKyc = false;
                        break;
                    }
                }

                if (invalidKyc){

                    disburseResponse = DisburseResponse.builder()
                            .error_description("Invalid Customer Kyc Document.")
                            .build();
                }
            } else {

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Customer Kyc Document.")
                        .build();
            }

            // Check Address
            if (CollectionUtils.isEmpty(applicant.getAddress())){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Customer Address.")
                        .build();
            } else {

                boolean invalidAddress = true;
                for (CustomerAddress customerAddress: applicant.getAddress()) {

                    if (StringUtils.isNotBlank(customerAddress.getAddressType()) &&
                            StringUtils.isNotBlank(customerAddress.getAddressLine1()) &&
                            StringUtils.isNotBlank(customerAddress.getState()) &&
                            lmsMasterDataMap.containsKey(customerAddress.getAddressType()) &&
                            lmsMasterDataMap.containsKey(customerAddress.getState())){

                        invalidAddress = false;
                        break;
                    }
                }

                if (invalidAddress){

                    disburseResponse = DisburseResponse.builder()
                            .error_description("Customer Address is Null.")
                            .build();
                }
            }

            if (StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                    .getApplicant().getApplicantType(), ApplicantType.INDIVIDUAL.value())) {
                // Check Applicant Name
                if (applicant.getApplicantName() == null ||
                        StringUtils.isBlank(applicant.getApplicantName().getFirstName()) ||
                        StringUtils.isBlank(applicant.getApplicantName().getLastName())) {

                    disburseResponse = DisburseResponse.builder()
                            .error_description("Customer Name is Null.")
                            .build();
                }

                // Check Date of Birth
                if (StringUtils.isBlank(applicant.getDateOfBirth())){

                    disburseResponse = DisburseResponse.builder()
                            .error_description("Customer Date of Birth is Null.")
                            .build();
                }
            } else {
                if (applicant.getApplicantName() == null || StringUtils.isBlank(applicant.getApplicantName().getFirstName())) {
                    disburseResponse = DisburseResponse.builder()
                            .error_description("Customer Name is Null.")
                            .build();
                }

                if (applicant.getEmployment().get(0).getDateOfJoining() == null) {
                    disburseResponse = DisburseResponse.builder().error_description("Date Of Incorporation is null.").build();
                }
            }


            // Check Mobile No
            if (CollectionUtils.isEmpty(applicant.getPhone())){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Customer Phone No is Null.")
                        .build();

            } else {

                boolean invalidPhoneNumber = true;
                for (Phone phone:applicant.getPhone()) {

                    if (StringUtils.isNotBlank(phone.getPhoneNumber())){

                        invalidPhoneNumber = false;
                        break;
                    }
                }

                if (invalidPhoneNumber){

                    disburseResponse = DisburseResponse.builder()
                            .error_description("Customer Phone No is Null.")
                            .build();
                }
            }

            // Check Gender
            if (StringUtils.isBlank(applicant.getGender())){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Customer Gender is Null.")
                        .build();
            }

            // Check Date
            if (goNoGoCustomerApplication.getDateTime() == null){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Customer Application Date is Null.")
                        .build();
            }

        }

        // Check Loan Charges
        if (loanCharges == null){

            disburseResponse = DisburseResponse.builder()
                    .error_description("Invalid Loan Charges.")
                    .build();
        } else {
            //Check tenureMonths
            if (loanCharges.getTenureDetails() == null ||
                    loanCharges.getTenureDetails().getTenureMonths() < 1) {
                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Tenure Months")
                        .build();
            }


            // Check DisbTenureMonths
            if (loanCharges.getTenureDetails() == null ||
                    loanCharges.getTenureDetails().getDisbTenureMonths() < 1){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid DisbTenureMonths.")
                        .build();
            }

            // Check InterestRate
            if (loanCharges.getInterestDetails() == null ||
                    loanCharges.getInterestDetails().getInterestRate() <= 0){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid InterestRate.")
                        .build();
            }

            // Check Principal
            if (loanCharges.getLoanDetails() == null ||
                    loanCharges.getLoanDetails().getFinalDisbAmt() < 1){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Principal Amount.")
                        .build();
            }

            // Check DisbursedDate
            if (loanCharges.getDisbursedDate() == null){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid DisbursedDate.")
                        .build();
            }
        }

        //Check DisbursementMemo
        if (disbursementMemo == null ||
                disbursementMemo.getRepayment() == null ||
                CollectionUtils.isEmpty(disbursementMemo.getRepayment().getRepaymentDetailsList()) ||
                CollectionUtils.isEmpty(disbursementMemo.getDmInputList())){

            disburseResponse = DisburseResponse.builder()
                    .error_description("Invalid DMDetails.")
                    .build();
        } else {

            if (StringUtils.isBlank(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getAccountNumber())){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Account Number.")
                        .build();
            }

            if (StringUtils.isBlank(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getIfscCode())){

                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid IfscCode.")
                        .build();
            }
            if (StringUtils.isBlank(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getChqNumber())) {
                disburseResponse = DisburseResponse.builder()
                        .error_description("Invalid Check number")
                        .build();
            }
        }

        //Check camDetails
        if (camDetails == null || camDetails.getSummary() == null ||
                camDetails.getSummary().getLoginDate() == null ||
                camDetails.getSummary().getApprovalDate() == null) {
            disburseResponse = DisburseResponse.builder()
                    .error_description("Invalid camDetails")
                    .build();
        } else {
            //Check activationDate
            if (camDetails.getSummary().getApprovalDate() == null) {
                disburseResponse = DisburseResponse.builder()
                        .error_description("Client activation date is null.")
                        .build();
            }

            //Check submittedOnDate
            if (camDetails.getSummary().getLoginDate() == null) {
                disburseResponse = DisburseResponse.builder()
                        .error_description("Client submittedOnDate is null.")
                        .build();
            }
        }

        return disburseResponse;
    }
}