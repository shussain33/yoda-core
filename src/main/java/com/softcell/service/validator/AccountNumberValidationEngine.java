package com.softcell.service.validator;

import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * Created by sampat on 3/11/17.
 */

public interface AccountNumberValidationEngine {

    /**
     *
     * @param impsRequest
     * @return
     */
    Collection<Error> validationForIMPS(IMPSRequest impsRequest);

}
