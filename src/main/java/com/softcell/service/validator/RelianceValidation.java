package com.softcell.service.validator;

import com.softcell.gonogo.model.reliance.RelianceDigitalRequest;

/**
 * This is to validate reliance request
 *
 * @author bhuvneshk
 */
public class RelianceValidation {

    /**
     * This method
     *
     * @param request
     */
    public static void relianceLenghtValidation(RelianceDigitalRequest request) {

        if (request.getAdvanceEMI() != null) {
            //Integer //mandatory
        }
        if (request.getArea() != null) {
            //String
        }
        if (request.getAssetCategory() != null) {
            //String
        }
        if (request.getAssetQuantity() != null) {
            //String
        }
        if (request.getBrand() != null) {
            //String
        }
        if (request.getBuildingName() != null) {
            //String
        }
        if (request.getCity() != null) {
            //String
        }
        if (request.getCostOfProduct() != null) {
            //Numeric
        }
        if (request.getCreatedDate() != null) {
            //dd-mm-yyyy
        }
        if (request.getCustomerDownPayment() != null) {
            //mandatory
        }
        if (request.getCustomerName() != null) {
            //String
        }
        if (request.getDbdPercentage() != null) {
            //integer
        }
        if (request.getDealerCode() != null) {
            //numeric
        }
        if (request.getDealID() != null) {
            //alpaNumeric //mandatory
        }
        if (request.getDoNumber() != null) {
            //numeric
        }
        if (request.getDoStatus() != null) {
            //Default NEW
        }
        if (request.getEmailId() != null) {
            //abc@test.com
        }
        if (request.getFloorNo() != null) {
            //String
        }
        if (request.getGrossLoanAmount() != null) {
            //String
        }
        if (request.getGuid() != null) {
            //String
        }
        if (request.getHouseNo() != null) {
            //String
        }
        if (request.getLandmark() != null) {
            //Strign
        }
        if (request.getMarginMoney() != null) {
            //Numeric
        }
        if (request.getMfrSubvention() != null) {
            //numeric
        }
        if (request.getMobileNo() != null) {
            //max10digit
        }
        if (request.getMopID() != null) {
            //defaultval
        }
        if (request.getNetDisbursement() != null) {
            //mandatory
        }
        if (request.getNetLoanAmount() != null) {
            //Numeric Mandatory
        }
        if (request.getNumAdvTenure() != null) {
            //numerc
        }
        if (request.getOtherCharges() != null) {
            //mandatory
        }
        if (request.getPincode() != null) {
            //String
        }
        if (request.getPlotNo() != null) {
            //String
        }
        if (request.getProcessingFee() != null) {
            //Mandatory Numeric
        }
        if (request.getProduct() != null) {
            //String
        }
        if (request.getProductType() != null) {
            //String
        }
        if (request.getRefID() != null) {

        }
        if (request.getResiAddress1() != null) {
            //AphaNumeric 40char
        }
        if (request.getResiAddress2() != null) {

        }
        if (request.getResiAddress3() != null) {

        }
        if (request.getSchemeName() != null) {
            //varchar //mandatory
        }
        if (request.getSector() != null) {
            //String
        }
        if (request.getSocietyName() != null) {
            //String
        }
        if (request.getState() != null) {
            //String
        }
        if (request.getStreet() != null) {
            //String
        }
        if (request.getSubvention() != null) {
            //Numeric
        }
        if (request.getTenure() != null) {
            //Numeric
        }
        if (request.getWingNo() != null) {
            //String
        }


    }
}
