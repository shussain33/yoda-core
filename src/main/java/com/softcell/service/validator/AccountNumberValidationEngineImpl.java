package com.softcell.service.validator;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.response.core.Error;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by sampat on 3/11/17.
 */

@Service
public class AccountNumberValidationEngineImpl implements AccountNumberValidationEngine{

    @Override
    public Collection<Error> validationForIMPS(IMPSRequest impsRequest) {
        Collection<Error> errors = new ArrayList<>();


        if (StringUtils.isBlank(impsRequest.getAccountNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sAccountNumber")
                    .id("accountNumber")
                    .message(ErrorCode.BLANK_ACC_NO)
                    .build());
        }

        if (StringUtils.isBlank(impsRequest.getIFSCCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sIFSCCode")
                    .id("IFSCCode")
                    .message(ErrorCode.BLANK_IFSC)
                    .build());
        }

        if (StringUtils.isBlank(impsRequest.getAccountHolderFName())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sAccountHolderFName")
                    .id("accountHolderFName")
                    .message(ErrorCode.BLANK_HOLDER_NAME)
                    .build());
        }

        if (StringUtils.isBlank(impsRequest.getAccountHolderLName())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sAccountHolderLName")
                    .id("accountHolderLName")
                    .message(ErrorCode.BLANK_HOLDER_NAME)
                    .build());
        }

        return errors;
    }
}
