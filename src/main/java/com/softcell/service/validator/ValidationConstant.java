package com.softcell.service.validator;


import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.core.error.Error;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yogeshb
 */

/**
 * @author mahesh
 */
public class ValidationConstant {
    /**
     * <pre>
     * A ERROR_MAP will define error explanation, Error code is map key.
     * </pre>
     */
    public static Map<String, Error> ERROR_MAP = new HashMap<>();

    static {
        initErrorCache();
    }

    /**
     * Init all Configured Errors
     */
    public static void initErrorCache() {

        Error error = new Error();
        error.setId(ErrorCode.HEADER_BLANK);
        error.setFieldName("oHeader");
        error.setErrorCode(ErrorCode.HEADER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_APPLICATION_ID_BLANK);
        error.setFieldName("sAppID");
        error.setErrorCode(ErrorCode.HEADER_APPLICATION_ID_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sAppID is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_APPLICATION_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_APPLICATION_SOURCE_BLANK);
        error.setFieldName("sAppSource");
        error.setErrorCode(ErrorCode.HEADER_APPLICATION_SOURCE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sAppSource is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_APPLICATION_SOURCE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_INSTITUTION_ID_BLANK);
        error.setFieldName("sInstID");
        error.setErrorCode(ErrorCode.HEADER_INSTITUTION_ID_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sInstID is blank.");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_INSTITUTION_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_REQUEST_TYPE_BLANK);
        error.setFieldName("sReqType");
        error.setErrorCode(ErrorCode.HEADER_REQUEST_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sReqType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_REQUEST_TYPE_BLANK, error);

        /**
         * 6 HEADER_SOURCE_ID_BLANK MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_SOURCE_ID_BLANK);
        error.setFieldName("sSourceID");
        error.setErrorCode(ErrorCode.HEADER_SOURCE_ID_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sSourceID is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_SOURCE_ID_BLANK, error);

        /**
         * 8 APPLICATION_SOURCE MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_APPLICATION_SOURCE_INVALID);
        error.setFieldName("oHeader__sAppSource");
        error.setErrorCode(ErrorCode.HEADER_APPLICATION_SOURCE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sAppSource is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_APPLICATION_SOURCE_INVALID, error);

        /**
         * 9 INSTITUTION_ID MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_INSTITUTION_ID_INVALID);
        error.setFieldName(" sInstID");
        error.setErrorCode(ErrorCode.HEADER_INSTITUTION_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription(" oHeader__sInstID  is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_INSTITUTION_ID_INVALID, error);

        /**
         * 10 HEADER_REQUEST_TYPE_INVALID MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_REQUEST_TYPE_INVALID);
        error.setFieldName("sReqType");
        error.setErrorCode(ErrorCode.HEADER_REQUEST_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sReqType is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_REQUEST_TYPE_INVALID, error);

        /**
         * 11 HEADER_SOURCE_ID_INVALID MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_SOURCE_ID_INVALID);
        error.setFieldName("sSourceID");
        error.setErrorCode(ErrorCode.HEADER_SOURCE_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sSourceID is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_SOURCE_ID_INVALID, error);

        /**
         * 1 APPLICANT_ID MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ID_BLANK);
        error.setFieldName("sApplID");
        error.setErrorCode(ErrorCode.APPLICANT_ID_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sApplID is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_ID_INVALID);
        error.setFieldName("sApplID");
        error.setErrorCode(ErrorCode.APPLICANT_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sApplID is invalid.");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ID_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_NAME_BLANK);
        error.setFieldName("oApplName");
        error.setErrorCode(ErrorCode.APPLICANT_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__oApplName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_NAME_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_FIRST_NAME_BLANK);
        error.setFieldName("sFirstName");
        error.setErrorCode(ErrorCode.APPLICANT_FIRST_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__oApplName__sFirstName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_FIRST_NAME_BLANK, error);

        /**
         * 5 FIRST_NAME MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_FIRST_NAME_INVALID);
        error.setFieldName("sFirstName");
        error.setErrorCode(ErrorCode.APPLICANT_FIRST_NAME_INVALID);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__oApplName__sFirstName is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_FIRST_NAME_INVALID, error);

        /**
         * 6 LAST_NAME MANDATORY FIELD
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_LAST_NAME_BLANK);
        error.setFieldName("sLastName");
        error.setErrorCode(ErrorCode.APPLICANT_LAST_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__oApplName__sLastName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_LAST_NAME_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_LAST_NAME_INVALID);
        error.setFieldName("sLastName");
        error.setErrorCode(ErrorCode.APPLICANT_LAST_NAME_INVALID);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__oApplName__sLastName is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_LAST_NAME_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_GENDER_BLANK);
        error.setFieldName("sApplGndr");
        error.setErrorCode(ErrorCode.APPLICANT_GENDER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sApplGndr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_GENDER_BLANK, error);

        /**
         * GENDER INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_GENDER_INVALID);
        error.setFieldName("sApplGndr");
        error.setErrorCode(ErrorCode.APPLICANT_GENDER_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sApplGndr is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_GENDER_INVALID, error);

        /**
         * DATE_OF_BIRTH_BLANK
         */

        error = new Error();
        error.setId(ErrorCode.APPLICANT_DATE_OF_BIRTH_BLANK);
        error.setFieldName("sDob");
        error.setErrorCode(ErrorCode.APPLICANT_DATE_OF_BIRTH_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sDob is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_DATE_OF_BIRTH_BLANK, error);

        /**
         * DATE_OF_BIRTH_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_DATE_OF_BIRTH_INVALID);
        error.setFieldName("sDob");
        error.setErrorCode(ErrorCode.APPLICANT_DATE_OF_BIRTH_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__sDob  is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_DATE_OF_BIRTH_INVALID, error);

        /**
         * KYC_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_BLANK);
        error.setFieldName("aKycDocs");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_BLANK, error);

        /**
         * KYC_EXPIRY_DATE_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_BLANK);
        error.setFieldName("sExpiryDate");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sExpiryDate should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_BLANK, error);
        /**
         * KYC_ISSUE_DATE_BLANK
         *
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_ISSUE_DATE_BLANK);
        error.setFieldName("sIssueDate");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_ISSUE_DATE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sIssueDate should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_ISSUE_DATE_BLANK, error);

        /**
         * KYC_NAME_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_NAME_BLANK);
        error.setFieldName("sKycName");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycName should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_NAME_BLANK, error);
        /**
         * KYC_NUMBER_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_NUMBER_BLANK);
        error.setFieldName("sKycNumber");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_NUMBER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_NUMBER_BLANK, error);
        /**
         * KYC_NAME_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_NAME_INVALID);
        error.setFieldName("sKycName");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_NAME_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycName is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_NAME_INVALID, error);
        /**
         * PAN_NUMBER_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_PAN_NUMBER_INVALID);
        error.setFieldName("sKycNumber(Pan)");
        error.setErrorCode(ErrorCode.APPLICANT_PAN_NUMBER_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(Pan) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PAN_NUMBER_INVALID, error);
        /**
         * UID_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_UID_INVALID);
        error.setFieldName("sKycNumber(UID)");
        error.setErrorCode(ErrorCode.APPLICANT_UID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(UID) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_UID_INVALID, error);
        /**
         * VOTER_ID_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_VOTER_ID_INVALID);
        error.setFieldName("sKycNumber(VOTER_ID)");
        error.setErrorCode(ErrorCode.APPLICANT_VOTER_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(VOTER_ID) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_VOTER_ID_INVALID, error);
        /**
         * DRIVING_LICENSE_NUMBER_INVALID
         */

        error = new Error();
        error.setId(ErrorCode.APPLICANT_DRIVING_LICENSE_NUMBER_INVALID);
        error.setFieldName("sKycNumber(Driving_Licence)");
        error.setErrorCode(ErrorCode.APPLICANT_DRIVING_LICENSE_NUMBER_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(Driving_licence) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP
                .put(ErrorCode.APPLICANT_DRIVING_LICENSE_NUMBER_INVALID, error);
        /**
         * PASSPORT_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_PASSPORT_INVALID);
        error.setFieldName("sKycNumber(Passport)");
        error.setErrorCode(ErrorCode.APPLICANT_PASSPORT_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(Passport) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PASSPORT_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_RATION_CARD_INVALID);
        error.setFieldName("sKycNumber(Ration_card)");
        error.setErrorCode(ErrorCode.APPLICANT_RATION_CARD_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(Ration_Card) is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_RATION_CARD_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_BLANK);
        error.setFieldName("aAddr");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_BLANK, error);
        /**
         * APPLICANT_ADDRESS_LINE1_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_LINE1_BLANK);
        error.setFieldName("sLine1");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_LINE1_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sLine1 is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_LINE1_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_TYPE_BLANK);
        error.setFieldName("sAddrType");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sAddrType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_CITY_BLANK);
        error.setFieldName("sCity");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_CITY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sCity is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_CITY_BLANK, error);
        /**
         * APPLICANT_ADDRESS_COUNTRY_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_COUNTRY_BLANK);
        error.setFieldName("sCountry");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_COUNTRY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sCountry is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_COUNTRY_BLANK, error);
        /**
         * APPLICANT_ADDRESS_PIN_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_PIN_BLANK);
        error.setFieldName("iPinCode");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_PIN_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__iPinCode is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_PIN_BLANK, error);
        /**
         * APPLICANT_ADDRESS_STATE_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_STATE_BLANK);
        error.setFieldName("sState");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_STATE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sState is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_STATE_BLANK, error);
        /**
         * APPLICANT_TIME_AT_ADDRESS_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_MONTH_AT_ADDRESS_BLANK);
        error.setFieldName("iMonthAtAddr");
        error.setErrorCode(ErrorCode.APPLICANT_MONTH_AT_ADDRESS_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__iMonthAtAddr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_MONTH_AT_ADDRESS_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_STATE_INVALID);
        error.setFieldName("sState");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_STATE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sState is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_STATE_INVALID, error);
        /**
         * APPLICANT_ADDRESS_PIN_NUMBER_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_PIN_NUMBER_INVALID);
        error.setFieldName("iPinCode");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_PIN_NUMBER_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__iPinCode  is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_PIN_NUMBER_INVALID, error);
        /**
         * APPLICANT_ADDRESS_COUNTRY_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_COUNTRY_INVALID);
        error.setFieldName("sCountry");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_COUNTRY_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sCountry is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_COUNTRY_INVALID, error);
        /**
         * APPLICANT_ADDRESS_CITY_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_CITY_INVALID);
        error.setFieldName("sCity");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_CITY_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sCity is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_CITY_INVALID, error);
        /**
         * APPLICANT_ADDRESS_TYPE_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_ADDRESS_TYPE_INVALID);
        error.setFieldName("sAddrType");
        error.setErrorCode(ErrorCode.APPLICANT_ADDRESS_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aAddr__sAddrType is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_ADDRESS_TYPE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.REQUEST_HEADER_BLANK);
        error.setFieldName("oReq");
        error.setErrorCode(ErrorCode.REQUEST_HEADER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq Header Should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.REQUEST_HEADER_BLANK, error);
        /**
         * SUSPICIOUS_ACTIVITY BLANK
         */

        error = new Error();
        error.setId(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_BLANK);
        error.setFieldName("sSuspiciousActivity");
        error.setErrorCode(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__sSuspiciousActivity is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_BLANK, error);

        /**
         * SUSPICIOUS_ACTIVITY INVALID
         */

        error = new Error();
        error.setId(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_INVALID);
        error.setFieldName("sSuspiciousActivity");
        error.setErrorCode(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__sSuspiciousActivity is not valid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.REQUEST_SUSPICIOUS_ACTIVITY_INVALID, error);

        /**
         * APPLICATION_BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_BLANK);
        error.setFieldName("oApplication");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_BLANK);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplication Should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_BLANK, error);

        /**
         * APPLICATION_ID INVALID
         */
        error = new Error();
        error.setId(ErrorCode.HEADER_APPLICATION_ID_INVALID);
        error.setFieldName("sAppID");
        error.setErrorCode(ErrorCode.HEADER_APPLICATION_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oHeader__sAppID is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_APPLICATION_ID_INVALID, error);

        /**
         * APPLICATION_LOAN BLANK
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_BLANK);
        error.setFieldName("sLoanType");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__sLoanType should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_BLANK, error);

        /**
         * APPLICATION_LOAN INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_INVALID);
        error.setFieldName("sLoanType");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__sLoanType is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_INVALID, error);
        /**
         * APPLICATION_LOAN_APPLIED_FOR_INVALID
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_INVALID);
        error.setFieldName("sAppliedFor");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__sAppliedFor is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_INVALID,
                error);

        /**
         * APPLICATION_LOAN_APPLIED_FOR_BLANK
         *
         */
        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_BLANK);
        error.setFieldName("sAppliedFor");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__sAppliedFor should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_APPLIED_FOR_BLANK,
                error);
        /**
         * APPLICATION_LOAN_AMOUNT_BLANK
         */

        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_AMOUNT_BLANK);
        error.setFieldName("dLoanAmt");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_AMOUNT_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__dLoanAmt should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_AMOUNT_BLANK, error);

        /**
         * APPLICATION_LOAN_TENOR_BLANK
         */

        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_LOAN_TENOR_BLANK);
        error.setFieldName("iLoanTenor");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_LOAN_TENOR_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__iLoanTenor should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_LOAN_TENOR_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_APPLICATION_PROPERTY_BLANK);
        error.setFieldName("oProperty");
        error.setErrorCode(ErrorCode.APPLICANT_APPLICATION_PROPERTY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_APPLICATION_PROPERTY_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_LOCATION_INVALID);
        error.setFieldName("sProprtyLocation");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_LOCATION_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sProprtyLocation is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_LOCATION_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_LOCATION_BLANK);
        error.setFieldName("sProprtyLocation");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_LOCATION_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sProprtyLocation should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_LOCATION_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_NAME_INVALID);
        error.setFieldName("sProprtyName");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_NAME_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sProprtyName invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_NAME_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_NAME_BLANK);
        error.setFieldName("sProprtyName");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sProprtyName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_NAME_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_TYPE_INVALID);
        error.setFieldName("sPropertyType");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyType is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_TYPE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_TYPE_BLANK);
        error.setFieldName("sPropertyType");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_STATUS_INVALID);
        error.setFieldName("sPropertyStatus");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_STATUS_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyStatus is not valid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_STATUS_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_STATUS_BLANK);
        error.setFieldName("sPropertyStatus");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_STATUS_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyStatus is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_STATUS_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_ID_INVALID);
        error.setFieldName("sPropertyId");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_ID_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyId is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_ID_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_ID_BLANK);
        error.setFieldName("sPropertyId");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_ID_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyId is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_VALUE_BLANK);
        error.setFieldName("sPropertyValue");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_VALUE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyValue is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_VALUE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PROPERTY_VALUE_INVALID);
        error.setFieldName("sPropertyValue");
        error.setErrorCode(ErrorCode.APPLICANT_PROPERTY_VALUE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplication__oProperty_sPropertyValue is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PROPERTY_VALUE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_BLANK);
        error.setFieldName("aPhone");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aPhone should not blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_TYPE_INVALID);
        error.setFieldName("sPhoneType");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aPhone__sPhoneType is Invalid.");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_TYPE_INVALID, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_TYPE_BLANK);
        error.setFieldName("sPhoneType");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aPhone__sPhoneType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_NUMBER_BLANK);
        error.setFieldName("sPhoneNumber ");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_NUMBER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__aPhone__sPhoneNumber is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_NUMBER_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_AREA_CODE_BLANK);
        error.setFieldName("PHONE_AREACODE");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_AREA_CODE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aPhone__sAreaCode is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_AREA_CODE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_COUNTRY_CODE_BLANK);
        error.setFieldName("sCountryCode");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_COUNTRY_CODE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aPhone__sCountryCode is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_COUNTRY_CODE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_PHONE_NUMBER_INVALID);
        error.setFieldName("oReq__oApplicant__aPhone__sPhoneNumber ");
        error.setErrorCode(ErrorCode.APPLICANT_PHONE_NUMBER_INVALID);
        error.setLevel("L1");
        error.setErrorDescription(" oReq__oApplicant__aPhone__sPhoneNumber is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PHONE_NUMBER_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMAIL_ADDRESS_BLANK);
        error.setFieldName("sEmailAddr");
        error.setErrorCode(ErrorCode.APPLICANT_EMAIL_ADDRESS_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmail__sEmailAddr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMAIL_ADDRESS_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMAIL_TYPE_INVALID);
        error.setFieldName("sEmailType");
        error.setErrorCode(ErrorCode.APPLICANT_EMAIL_TYPE_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmail__sEmailType is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMAIL_TYPE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMAIL_TYPE_BLANK);
        error.setFieldName("sEmailType");
        error.setErrorCode(ErrorCode.APPLICANT_EMAIL_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmail__sEmailType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMAIL_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMAIL_ADDRESS_INVALID);
        error.setFieldName("sEmailAddr");
        error.setErrorCode(ErrorCode.APPLICANT_EMAIL_ADDRESS_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmail__sEmailAddr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMAIL_ADDRESS_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_BLANK);
        error.setFieldName("aEmpl");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_NAME_BLANK);
        error.setFieldName("aEmpl__sEmplName");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sEmplName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_NAME_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_BLANK);
        error.setFieldName("sEmplType");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sEmplType is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_MONTHLY_SALARY_BLANK);
        error.setFieldName("dmonthSal");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_MONTHLY_SALARY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__dmonthSal is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_MONTHLY_SALARY_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_GROSS_SALARY_BLANK);
        error.setFieldName("dGrossSal");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_GROSS_SALARY_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__dGrossSal is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_GROSS_SALARY_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_TIME_WITH_EMPLOYER_BLANK);
        error.setFieldName("iTmWithEmplr");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_TIME_WITH_EMPLOYER_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__iTmWithEmplr is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_TIME_WITH_EMPLOYER_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_BLANK);
        error.setFieldName("sDtJoin");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sDtJoin is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_BLANK);
        error.setFieldName("sDtLeave");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sDtLeave is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_LIST_BLANK);
        error.setFieldName("aLastMonthIncome");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_LIST_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__aLastMonthIncome is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(
                ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_LIST_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_INVALID);
        error.setFieldName("aLastMonthIncome");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__aLastMonthIncome___sMonthName is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_INVALID,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_BLANK);
        error.setFieldName("sMonthName");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__aLastMonthIncome__sMonthName is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_BLANK);
        error.setFieldName("sMonthIncome");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_BLANK);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__aLastMonthIncome__sMonthIncome is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_BLANK,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_INVALID);
        error.setFieldName("sDtLeave");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sDtLeave is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_INVALID,
                error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_INVALID);
        error.setFieldName("sDtJoin");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_INVALID);
        error.setLevel("L1");
        error.setErrorDescription("oReq__oApplicant__aEmpl__sDtJoin is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_DATE_OF_JOINING_INVALID,
                error);

        // error = new Error();
        // error.setId(ErrorCode.HEADER_DATE_TIME_BLANK);
        // error.setFieldName("DATE_TIME");
        // error.setLevel("L1");
        // error.setErrorCode(ErrorCode.HEADER_DATE_TIME_BLANK);
        // error.setErrorDescription(" DATE_TIME is Blank");
        // error.setCondition("REQUIRED");
        // error.setApplicationFor("GONOGO");
        // error.setErrorType("ERROR");
        // ERROR_MAP.put(ErrorCode.HEADER_DATE_TIME_BLANK, error);
        //
        // error = new Error();
        // error.setId(ErrorCode.HEADER_DATE_TIME_INVALID);
        // error.setFieldName("DATE_TIME");
        // error.setLevel("L1");
        // error.setErrorCode(ErrorCode.HEADER_DATE_TIME_INVALID);
        // error.setErrorDescription(" DATE_TIME is Invalid");
        // error.setCondition("REQUIRED");
        // error.setApplicationFor("GONOGO");
        // error.setErrorType("ERROR");
        // ERROR_MAP.put(ErrorCode.HEADER_DATE_TIME_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_DSA_ID_BLANK);
        error.setFieldName("sDsaId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_DSA_ID_BLANK);
        error.setErrorDescription("oHeader__sDsaId is Blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_DSA_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_DSA_ID_INVALID);
        error.setFieldName("sDsaId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_DSA_ID_INVALID);
        error.setErrorDescription("oHeader__sDsaId is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_DSA_ID_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_CRO_ID_BLANK);
        error.setFieldName("sCroId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_CRO_ID_BLANK);
        error.setErrorDescription("oHeader__sCroId is Blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_CRO_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_CRO_ID_INVALID);
        error.setFieldName("sCroId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_CRO_ID_INVALID);
        error.setErrorDescription("oHeader__sCroId is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_CRO_ID_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_DEALER_ID_BLANK);
        error.setFieldName("sDealerId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_DEALER_ID_BLANK);
        error.setErrorDescription("oHeader__sDealerId is Blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_DEALER_ID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.HEADER_DEALER_ID_INVALID);
        error.setFieldName("sDealerId");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.HEADER_DEALER_ID_INVALID);
        error.setErrorDescription("oHeader__sDealerId is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.HEADER_DEALER_ID_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_INVALID);
        error.setFieldName("sExpiryDate");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_INVALID);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sExpiryDate is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_EXPIRY_DATE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_KYC_ISSUE_DATE_INVALID);
        error.setFieldName("sIssueDate");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_KYC_ISSUE_DATE_INVALID);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sIssueDate is Invalid ");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_KYC_ISSUE_DATE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_MARITAL_STATUS_BLANK);
        error.setFieldName("sMarStat");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_MARITAL_STATUS_BLANK);
        error.setErrorDescription("oReq__oApplicant__sMarStat is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_MARITAL_STATUS_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_MARITAL_STATUS_INVALID);
        error.setFieldName("sMarStat");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_MARITAL_STATUS_INVALID);
        error.setErrorDescription("oReq__oApplicant__sMarStat is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_MARITAL_STATUS_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EDUCATION_BLANK);
        error.setFieldName("sEdu");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_EDUCATION_BLANK);
        error.setErrorDescription("oReq__oApplicant__sEdu is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EDUCATION_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EDUCATION_INVALID);
        error.setFieldName("sEdu");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_EDUCATION_INVALID);
        error.setErrorDescription("oReq__oApplicant__sEdu is invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EDUCATION_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_MONTH_AT_CITY_BLANK);
        error.setFieldName("iMonthAtCity");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_MONTH_AT_CITY_BLANK);
        error.setErrorDescription("oReq__oApplicant__aAddr__iMonthAtCity is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_MONTH_AT_CITY_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_BLANK);
        error.setFieldName("sAddrType");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_BLANK);
        error.setErrorDescription("oReq__oApplicant__aAddr__sAddrType is  blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_INVALID);
        error.setFieldName("sResAddrType");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_INVALID);
        error.setErrorDescription("oReq__oApplicant__aAddr__sResAddrType is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP
                .put(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_INVALID);
        error.setFieldName("sEmplType");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_INVALID);
        error.setErrorDescription("oReq__oApplicant__aEmpl__sEmplType Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_INVALID, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_BLANK);
        error.setFieldName("sConst");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_BLANK);
        error.setErrorDescription("oReq__oApplicant__aEmpl__sConst is Blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_INVALID);
        error.setFieldName("sConst");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_INVALID);
        error.setErrorDescription("oReq__oApplicant__aEmpl__sConst is Invalid");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_INVALID, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_DRIVINGLICENSE_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_DRIVINGLICENSE_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(DRIVING-LICENSE) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_DRIVINGLICENSE_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_RATIONCARD_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_RATIONCARD_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(RATION-CARD) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_RATIONCARD_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_VOTERID_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_VOTERID_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(VOTER-ID) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_VOTERID_BLANK, error);

        error = new Error();
        error.setId(ErrorCode.APPLICANT_AADHAAR_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_AADHAAR_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(AADHAAR) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_AADHAAR_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_PASSPORT_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_PASSPORT_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(PASSPORT) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PASSPORT_BLANK, error);


        error = new Error();
        error.setId(ErrorCode.APPLICANT_PAN_NUMBER_BLANK);
        error.setFieldName("sKycNumber");
        error.setLevel("L1");
        error.setErrorCode(ErrorCode.APPLICANT_PAN_NUMBER_BLANK);
        error.setErrorDescription("oReq__oApplicant__aKycDocs__sKycNumber(PAN) is blank");
        error.setCondition("REQUIRED");
        error.setApplicationFor("GONOGO");
        error.setErrorType("ERROR");
        ERROR_MAP.put(ErrorCode.APPLICANT_PAN_NUMBER_BLANK, error);


    }
}
