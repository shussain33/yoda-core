package com.softcell.service.validator;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.tkil.TKILRequest;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;

import java.util.Collection;

/**
 * Created by yogeshb on 14/3/17.
 */
public interface SerialNumberValidationEngine {
    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForCAV(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForMobile(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialsaleconfirmationrequest
     * @return
     */
    Collection<Error> validationForSamsung(SerialSaleConfirmationRequest serialsaleconfirmationrequest);

    /**
     *
     * @param serialsaleconfirmationrequest
     * @return
     */
    Collection<Error> validationForSamsungImie(SerialSaleConfirmationRequest serialsaleconfirmationrequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForIntex(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForAppleDPL(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForAppleCD(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    Collection<Error> validationForImeiAppleRollback(RollbackRequest rollbackRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    Collection<Error> validationForSerialNumberAppleRollback(RollbackRequest rollbackRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForGioneeImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForPanasonicSerial(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForPanasonicImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForVideocon(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    Collection<Error> validationForSerialVideoconRollback(RollbackRequest rollbackRequest);

    /**
     *
     * @param disbursementRequest
     * @return
     */
    Collection<Error> validationForSerialVideoconDisbursement(DisbursementRequest disbursementRequest);

    /**
     *
     * @param tkilRequest
     * @return
     */
    public Collection<Error> validationForTkil(TKILRequest tkilRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    public Collection<Error> validationForKent(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    public Collection<Error> validationForKentRollback(RollbackRequest rollbackRequest);

    /**
     *
     * @param disbursementRequest
     * @return
     */
    public Collection<Error> validationForKentDisburse(DisbursementRequest disbursementRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    public Collection<Error> validationForOppoImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForGoogleImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    public Collection<Error> validationForGoogleRollback(RollbackRequest rollbackRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> commonValidationForImeiAndModel(SerialSaleConfirmationRequest serialSaleConfirmationRequest);



}


