package com.softcell.service.validator;


import com.softcell.constants.ApplicantType;
import com.softcell.constants.ValidationPattern;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.error.Error;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author yogeshb This Class is use to validate Applicant application request.
 */
@Service
public class ApplicationValidationEngineImpl implements
        ApplicationValidationEngine {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationValidationEngineImpl.class);
    private static final String EMPTY_LOAN_ACCOUNT_NUMBER = "Provide loan account number.";
    private static final String INVALID_COAPPLICANT_TYPE = "Please provide atleast one individual co-applicant.";


    private List<Error> errorList = null;

    /**
     * VALIDATE APPLICATION REQUEST
     */
    @Override
    public BaseResponse validate(ApplicationRequest applicationRequest) {

        logger.debug("validate service is started");

        errorList = new ArrayList<Error>();

        /**
         * Error list of all validated fields.
         */
        // header
        if (applicationRequest.getHeader() != null) {

            validateHeader(applicationRequest.getHeader());

        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_BLANK);
            errorList.add(error);
        }

        // request
        if (applicationRequest.getRequest() != null) {

            validateRequest(applicationRequest.getRequest());

        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.REQUEST_HEADER_BLANK);
            errorList.add(error);
        }

        logger.debug("validate service is completed with errorList size {}", errorList.size());

        return GngUtils.getBaseResponse(HttpStatus.OK, errorList);
    }

    @Override
    public void validate(ApplicationRequest applicationRequest, String executionPoint) throws GoNoGoException {
        String insttId = applicationRequest.getHeader().getInstitutionId();
        switch (insttId) {
            case "4075": // SBFC
            case "4021": //SBFC prod instId
                validateForSBFC(applicationRequest, executionPoint);
                break;
            default:
                break;
        }
    }


    @Override
    public Collection<com.softcell.gonogo.model.response.core.Error> validateGstRequest(GstDetailsRequest gstDetailsRequest) {

        Collection<com.softcell.gonogo.model.response.core.Error> errors = new ArrayList<>();


        if (null != gstDetailsRequest.getGstDetails()
                && gstDetailsRequest.getGstDetails().isGstDetailsEntered()) {
            if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getEmailId()) || !GngUtils.isValid(ValidationPattern.EMAIL, gstDetailsRequest.getGstDetails().getEmailId())) {
                errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                        .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                        .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                        .fieldName("sEmailId")
                        .id("emailId")
                        .message(ErrorCode.BLANK_OR_INVALID_GST_EMAIL)
                        .build());
            }
            if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getGstNumber()) || !GngUtils.isValid(ValidationPattern.GST_NUMBER, gstDetailsRequest.getGstDetails().getGstNumber())) {
                errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                        .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                        .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                        .fieldName("sGstNumber")
                        .id("GstNumber")
                        .message(ErrorCode.BLANK_GST_NUMBER)
                        .build());
            }
            if (null != gstDetailsRequest.getGstDetails().getFromDate() && null != gstDetailsRequest.getGstDetails().getToDate()) {

                if (!GngDateUtil.isGreaterThanGstStartDate(gstDetailsRequest.getGstDetails().getFromDate())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("dtFrom")
                            .id("fromDate")
                            .message(ErrorCode.INVALID_FROM_DATE)
                            .build());
                }

                if (!GngDateUtil.isGreaterThanGstStartDate(gstDetailsRequest.getGstDetails().getToDate())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("dtTo")
                            .id("toDate")
                            .message(ErrorCode.INVALID_TO_DATE)
                            .build());
                }

                if (!GngDateUtil.isToDateGreaterThanFromDate(gstDetailsRequest.getGstDetails().getFromDate(), gstDetailsRequest.getGstDetails().getToDate())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("dtFrom and dtTo")
                            .id("fromDate and toDate")
                            .message(ErrorCode.WRONG_RANGE_BETWEEN_TO_DATE_AND_FROM_DATE)
                            .build());
                }

            } else {
                errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                        .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                        .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                        .fieldName("dtFrom or dtTo")
                        .id("fromDate or toDate")
                        .message(ErrorCode.BLANK_TO_DATE_OR_FROM_DATE)
                        .build());
            }

            if (null != gstDetailsRequest.getGstDetails().getAddress()) {
                if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getAddress().getAddressLine1())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("sAddressLine1")
                            .id("AddressLine1")
                            .message(ErrorCode.BLANK_ADDRESS_LINE1)
                            .build());
                }

                if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getAddress().getCity())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("sCity")
                            .id("City")
                            .message(ErrorCode.BLANK_ADDRESS_CITY)
                            .build());
                }
                if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getAddress().getState())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("sCity")
                            .id("City")
                            .message(ErrorCode.BLANK_ADDRESS_STATE)
                            .build());
                }
                if (StringUtils.isBlank(gstDetailsRequest.getGstDetails().getAddress().getCountry())) {
                    errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                            .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                            .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                            .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                            .fieldName("sCountry")
                            .id("Country")
                            .message(ErrorCode.BLANK_ADDRESS_COUNTRY)
                            .build());
                }
            } else {
                errors.add(com.softcell.gonogo.model.response.core.Error.builder()
                        .errorType(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toValue())
                        .errorCode(String.valueOf(com.softcell.gonogo.model.response.core.Error.ERROR_TYPE.BUSINESS.toCode()))
                        .level(com.softcell.gonogo.model.response.core.Error.SEVERITY.HIGH.name())
                        .fieldName("oAddress")
                        .id("address")
                        .message(ErrorCode.NULL_GST_ADDRESS)
                        .build());
            }
        }
        return errors;
    }

    /**
     * VALIDATE REQUEST
     *
     * @param request
     */
    private void validateRequest(Request request) {


        // validate applicant

        if (request.getApplicant() != null) {
            validateApplicant(request.getApplicant());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_HEADER_BLANK);
            errorList.add(error);
        }

        // validate application
        if (request.getApplication() != null) {
            validateApplication(request.getApplication());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_BLANK);
            errorList.add(error);

        }


    }


    /**
     * validate application
     *
     * @param application
     */
    private void validateApplication(Application application) {


        if (StringUtils.isNotBlank(application.getLoanType())) {
            validateLoanType(application.getLoanType());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_BLANK);
            errorList.add(error);
        }


        if (application.getLoanAmount() == 0l) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_LOAN_AMOUNT_BLANK);
            errorList.add(error);
        }

        if (application.getLoanTenor() == 0) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_LOAN_TENOR_BLANK);
            errorList.add(error);
        }

        if (application.getLoanType().equals("HOUSING LOAN")) {
            if (null != application.getProperty()) {
                applicationPropertyDetails(application.getProperty());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_APPLICATION_PROPERTY_BLANK);
                errorList.add(error);
            }
        }

    }


    /**
     * validate housing loan type
     *
     * @param loanType
     */
    private void validateLoanType(String loanType) {

        List<String> loanTypeList = new ArrayList<>();
        loanTypeList.add("CCBT");
        loanTypeList.add("PERSONAL LOAN");
        loanTypeList.add("Consumer Durables");
        if (!loanTypeList.contains(loanType)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_LOAN_TYPE_INVALID);
            errorList.add(error);
        }

    }

    /**
     * validate property details i.e.
     *
     * @param property
     */

    private void applicationPropertyDetails(Property property) {
        if (property != null) {
            if (StringUtils.isNotBlank(property.getLocation())) {
                if (StringUtils.length(property.getLocation()) > 100) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_LOCATION_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_LOCATION_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(property.getPropertyName())) {
                if (StringUtils.length(property.getPropertyName()) > 200) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_NAME_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_NAME_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(property.getPropertyType())) {
                if (!(StringUtils.equals("RESIDENTIAL",
                        property.getPropertyType()) || StringUtils.equals(
                        "COMMERCIAL", property.getPropertyType()))) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_TYPE_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_TYPE_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(property.getStatus())) {
                if (!(StringUtils.equals("SELF OCCUPIED", property.getStatus())
                        || StringUtils.equals("RENTED", property.getStatus()) || StringUtils
                        .equals("COMMERCIAL", property.getStatus()))) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_STATUS_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_STATUS_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(property.getPropertyId())) {
                if (StringUtils.length(property.getPropertyId()) > 20) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_ID_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_ID_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(property.getPropertyValue())) {
                if (StringUtils.length(property.getPropertyValue()) > 20) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PROPERTY_VALUE_INVALID);
                    errorList.add(error);
                }
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PROPERTY_VALUE_BLANK);
                errorList.add(error);
            }
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_APPLICATION_PROPERTY_BLANK);
            errorList.add(error);
        }

    }

    /**
     * VALIDATE ALL DETAILS OF APPLICANT.
     *
     * @param applicant
     */
    private void validateApplicant(Applicant applicant) {

        // VALIDATE APPLICANT ID
        if (StringUtils.isNotBlank(applicant.getApplicantId())) {
            validateApplicantid(applicant.getApplicantId());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ID_BLANK);
            errorList.add(error);
        }
        // MANDATORY
        // VALIDATE APPLICANT NAME
        if (null != applicant.getApplicantName()) {
            validateApplicantName(applicant.getApplicantName());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_NAME_BLANK);
            errorList.add(error);
        }
        // // VALIDATE APPLICANT FATHER NAME
        // if (applicant.getFatherName() != null) {
        // validateFatherName(applicant.getFatherName());
        // } else {
        // Error error =
        // ValidationConstant.ERROR_MAP.get(ErrorCode.FATHER_NAME_BLANK);
        // errorList.add(error);
        // }
        // VALIDATE APPLICANT SPOUSE NAME
        // if (applicant.getSpouseName() != null) {
        // validategetSpouseName(applicant.getSpouseName());
        // } else {
        // Error error =
        // ValidationConstant.ERROR_MAP.get(ErrorCode.SPOUSE_NAME_BLANK);
        // errorList.add(error);
        // }

        // VALIDATE APPLICANT RELIGION
        // if (StringUtils.isNotBlank(applicant.getReligion())) {
        // validateReligion(applicant.getReligion());
        // } else {
        // Error error = ValidationConstant.ERROR_MAP
        // .get(ErrorCode.RELIGION_BLANK);
        // errorList.add(error);
        // }

        /**
         * Marital Status validation
         */

        if (StringUtils.isNotBlank(applicant.getMaritalStatus())) {
            validateMaritalStatus(applicant.getMaritalStatus());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_MARITAL_STATUS_BLANK);
            errorList.add(error);
        }

        // VALIDATE APPLICANT GENDER(mandatory)
        if (StringUtils.isNotBlank(applicant.getGender())) {
            validateGender(applicant.getGender());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_GENDER_BLANK);
            errorList.add(error);

        }
        // VALIDATE APPLICANT DATE_OF_BIRTH(mandatory)
        if (StringUtils.isNotBlank(applicant.getDateOfBirth())) {
            validateDateOfBirth(applicant.getDateOfBirth());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_DATE_OF_BIRTH_BLANK);
            errorList.add(error);
        }

        if (StringUtils.isNotBlank(applicant.getEducation())) {
            validateEducation(applicant.getEducation());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EDUCATION_BLANK);
            errorList.add(error);
        }

        // MANDATORY
        // VALIDATE APPLICANT KYC DOCUMENT
        if (applicant.getKyc() != null) {
            validateKYC(applicant.getKyc());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_KYC_BLANK);
            errorList.add(error);
        }
        // MANDATORY
        // VALIDATE APPLICANT ADDRESS
        if (applicant.getAddress() != null) {
            validateAddress(applicant.getAddress());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_BLANK);
            errorList.add(error);
        }
        // MANDATORY
        // VALIDATE APPLICANT PHONE NUMBER
        if (applicant.getPhone() != null) {
            validatePhone(applicant.getPhone());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_PHONE_BLANK);
            errorList.add(error);
        }

        // VALIDATE APPLICANT EMAIL ADDRESS
        if (applicant.getEmail() != null) {
            validateEmail(applicant.getEmail());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMAIL_ADDRESS_BLANK);
            errorList.add(error);
        }

        // VALIDATE APPLICANT EMPLOYMENT
        if (applicant.getEmployment() != null) {
            validateEmployment(applicant.getEmployment());

        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMPLOYMENT_BLANK);
            errorList.add(error);
        }

    }

    private void validateEducation(String education) {
        List<String> educationList = new ArrayList<>();
        educationList.add("DOCTORATE");
        educationList.add("GRADUATE");
        educationList.add("POST-GRADUATE");
        educationList.add("PROFESSIONAL");
        educationList.add("UNDER GRADUATE");
        educationList.add("OTHERS");
        if (!educationList.contains(education)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EDUCATION_INVALID);
            errorList.add(error);
        }

    }

    private void validateMaritalStatus(String maritalStatus) {
        List<String> maritalStatusList = new ArrayList<>();
        maritalStatusList.add("Single");
        maritalStatusList.add("Married");

        if (!maritalStatusList.contains(maritalStatus)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_MARITAL_STATUS_INVALID);
            errorList.add(error);
        }

    }

    /**
     * Validate Applicant Employment.
     *
     * @param employmentList
     */
    private void validateEmployment(List<Employment> employmentList) {

        for (Employment employment : employmentList) {

            if (StringUtils.isNotBlank(employment.getEmploymentType())) {

                validateEmploymentType(employment.getEmploymentType());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_BLANK);
                errorList.add(error);
            }
            if (StringUtils.isNotBlank(employment.getEmploymentName())) {

            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMPLOYMENT_NAME_BLANK);
                errorList.add(error);
            }

            if (employment.getMonthlySalary() == 0.0d) {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMPLOYMENT_MONTHLY_SALARY_BLANK);
                errorList.add(error);
            }


            if (employment.getTimeWithEmployer() == 0) {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMPLOYMENT_TIME_WITH_EMPLOYER_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(employment.getConstitution())) {
                validateContitution(employment.getConstitution());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_BLANK);
                errorList.add(error);
            }


        }
    }

    private void validateContitution(String constitution) {
        List<String> contitutionList = new ArrayList<>();
        contitutionList.add("TRUST");
        contitutionList.add("SELF-EMPLOYED");
        contitutionList.add("SALARIED");
        contitutionList.add("PARTNERSHIP");
        contitutionList.add("PRIVATE LIMITED COMPANY");
        if (!contitutionList.contains(constitution)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMPLOYMENT_CONSTITUTION_INVALID);
            errorList.add(error);
        }

    }

    private void validateEmploymentType(String employmentType) {
        List<String> employmentTypeList = new ArrayList<>();
        employmentTypeList.add("SELF-EMPLOYED");
        employmentTypeList.add("PROFESSIONAL");
        employmentTypeList.add("RETIRED");
        employmentTypeList.add("SALARIED");
        employmentTypeList.add("OTHERS");
        if (!employmentTypeList.contains(employmentType)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMPLOYMENT_TYPE_INVALID);
            errorList.add(error);
        }
    }


    /**
     * Validate Email
     *
     * @param emailList
     */
    private void validateEmail(List<Email> emailList) {
        // emailList = new ArrayList<Email>();
        for (Email email : emailList) {

            if (StringUtils.isNotBlank(email.getEmailType())) {
                validateEmailType(email.getEmailType());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMAIL_TYPE_BLANK);
                errorList.add(error);
            }

            if (StringUtils.isNotBlank(email.getEmailAddress())) {
                validateEmailAddress(email.getEmailAddress());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_EMAIL_ADDRESS_BLANK);
                errorList.add(error);
            }
        }

    }

    /**
     * Validate Email Type
     *
     * @param emailType
     */
    private void validateEmailType(String emailType) {
        List<String> emailTypeList = new ArrayList<String>();
        emailTypeList.add("PERSONAL");
        emailTypeList.add("OFFICE");
        emailTypeList.add("WORK");
        emailTypeList.add("PERMANENT");

        if (!emailTypeList.contains(emailType)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMAIL_TYPE_INVALID);
            errorList.add(error);
        }

    }

    /**
     * Validate Email Address Pattern
     *
     * @param emailAddress
     */
    private void validateEmailAddress(String emailAddress) {
        Pattern pattern = Pattern.compile(ValidationPattern.EMAIL);
        Matcher matcher = pattern.matcher(emailAddress);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_EMAIL_ADDRESS_INVALID);
            errorList.add(error);
        }
    }

    /**
     * Validate Phone
     *
     * @param phoneList
     */
    private void validatePhone(List<Phone> phoneList) {

        for (Phone phone : phoneList) {

            if (StringUtils.isNotBlank(phone.getPhoneType())) {
                validatePhoneType(phone.getPhoneType());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PHONE_TYPE_BLANK);
                errorList.add(error);
            }
            if (phone.getPhoneType().equals("PERSONAL_MOBILE")
                    || phone.getPhoneType().equals("RESIDENCE_MOBILE")
                    || phone.getPhoneType().equals("OFFICE_MOBILE")) {
                if (StringUtils.isNotBlank(phone.getPhoneNumber())) {
                    validatePhoneNumber(phone.getPhoneNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PHONE_NUMBER_BLANK);
                    errorList.add(error);
                }
            }
            if (phone.getPhoneType().equals("PERSONAL_PHONE")
                    || phone.getPhoneType().equals("RESIDENCE_PHONE")
                    || phone.getPhoneType().equals("OFFICE_PHONE")) {
                if (StringUtils.isNotBlank(phone.getAreaCode())) {

                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PHONE_AREA_CODE_BLANK);
                    errorList.add(error);
                }
            }
            if (StringUtils.isNotBlank(phone.getCountryCode())) {

            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_PHONE_COUNTRY_CODE_BLANK);
                errorList.add(error);
            }
        }
    }

    /**
     * Validate Phone Number Pattern.
     *
     * @param phoneNumber
     */
    private void validatePhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.PHONE_NUMBER);
        Matcher matcher = pattern.matcher(String.valueOf(phoneNumber));

        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_PHONE_NUMBER_INVALID);
            errorList.add(error);
        }
    }

    /**
     * Validate Phone Type.
     *
     * @param phoneType
     */
    private void validatePhoneType(String phoneType) {
        List<String> phoneTypeList = new ArrayList<String>();
        phoneTypeList.add("PERSONAL_MOBILE");
        phoneTypeList.add("PERSONAL_PHONE");
        phoneTypeList.add("RESIDENCE_MOBILE");
        phoneTypeList.add("RESIDENCE_PHONE");
        phoneTypeList.add("OFFICE_PHONE");
        phoneTypeList.add("OFFICE_MOBILE");

        if (!phoneTypeList.contains(phoneType)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_PHONE_TYPE_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE CUSTOMER ADDRESS(MANDATORY)
     *
     * @param customerAddressList
     */
    private void validateAddress(List<CustomerAddress> customerAddressList) {

        for (CustomerAddress customerAddress : customerAddressList) {
            if (customerAddress != null) {
                if (StringUtils.isBlank(customerAddress.getAddressLine1())) {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_LINE1_BLANK);
                    errorList.add(error);
                }

                if (StringUtils.isNotBlank(customerAddress.getCity())) {
                    validateAddressCity(customerAddress.getCity());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_CITY_BLANK);
                    errorList.add(error);
                }

                if (StringUtils.isNotBlank(customerAddress.getCountry())) {
                    validateAddressCountry(customerAddress.getCountry());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_COUNTRY_BLANK);
                    errorList.add(error);
                }

                if (customerAddress.getPin() != 0) {
                    validateAddressPin(customerAddress.getPin());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_PIN_BLANK);
                    errorList.add(error);
                }

                if (StringUtils.isNotBlank(customerAddress.getState())) {
                    validateAddressState(customerAddress.getState());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_STATE_BLANK);
                    errorList.add(error);
                }
                if (StringUtils.equals("RESIDENCE",
                        customerAddress.getAddressType())
                        || StringUtils.equals("PERMANENT",
                        customerAddress.getAddressType())) {
                    if (customerAddress.getMonthAtAddress() == 0) {

                        Error error = ValidationConstant.ERROR_MAP
                                .get(ErrorCode.APPLICANT_MONTH_AT_ADDRESS_BLANK);
                        errorList.add(error);
                    }
                }
                if (StringUtils.equals("RESIDENCE",
                        customerAddress.getAddressType())
                        || StringUtils.equals("PERMANENT",
                        customerAddress.getAddressType())) {
                    if (customerAddress.getMonthAtCity() == 0) {

                        Error error = ValidationConstant.ERROR_MAP
                                .get(ErrorCode.APPLICANT_MONTH_AT_CITY_BLANK);
                        errorList.add(error);
                    }
                }

                if (StringUtils.isNotBlank(customerAddress.getAddressType())) {
                    validateAddressType(customerAddress.getAddressType());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_ADDRESS_TYPE_BLANK);
                    errorList.add(error);
                }

                if (StringUtils.isNotBlank(customerAddress
                        .getResidenceAddressType())) {
                    validateResidenceAddressType(
                            customerAddress.getResidenceAddressType(),
                            customerAddress.getAddressType());
                }

            }
        }

    }

    private void validateResidenceAddressType(String residenceAddressType,
                                              String addressType) {

        if ((StringUtils.equals("RESIDENCE", addressType) || StringUtils
                .equals("PERMANENT", addressType))
                && !(StringUtils.equals("OFFICE", addressType))) {
            if (StringUtils.isNotBlank(residenceAddressType)) {
                validateResidenceAddressTypeList(residenceAddressType);
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_BLANK);
                errorList.add(error);
            }
        }
    }

    private void validateResidenceAddressTypeList(String residenceAddressType) {
        List<String> residenceAddressTypeList = new ArrayList<String>();
        residenceAddressTypeList.add("OWNED-BUNGLOW");
        residenceAddressTypeList.add("OWNED-CHAWL");
        residenceAddressTypeList.add("OWNED-FLAT");
        residenceAddressTypeList.add("OWNED-PENTHOUSE");
        residenceAddressTypeList.add("OWNED-ROWHOUSE");
        residenceAddressTypeList.add("RENTED-BUNGLOW");
        residenceAddressTypeList.add("RENTED-CHAWL");
        residenceAddressTypeList.add("RENTED-FLAT");
        residenceAddressTypeList.add("RENTED-PENTHOUSE");
        residenceAddressTypeList.add("RENTED-BACHELOR ACCOMODATION");
        residenceAddressTypeList.add("COMPANY PROVIDED-FLAT");
        residenceAddressTypeList.add("PARENT OWNED-HOUSE");
        residenceAddressTypeList.add("PARENT OWNED-FLAT");
        residenceAddressTypeList.add("COMPANY PROVIDED-HOUSE");
        residenceAddressTypeList.add("RENTED-ROWHOUSE");

        if (!residenceAddressTypeList.contains(residenceAddressType)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_RESIDENCE_ADDRESS_TYPE_INVALID);
            errorList.add(error);
        }

    }

    // mandatory
    private void validateAddressState(String state) {
        Pattern pattern = Pattern.compile(ValidationPattern.STATE_CITY_COUNTRY);
        Matcher matcher = pattern.matcher(String.valueOf(state));
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_STATE_INVALID);
            errorList.add(error);
        }

    }

    // mandatory
    private void validateAddressPin(long pin) {
        Pattern pattern = Pattern.compile(ValidationPattern.PIN_CODE);
        Matcher matcher = pattern.matcher(String.valueOf(pin));
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_PIN_NUMBER_INVALID);
            errorList.add(error);
        }
    }

    // mandatory
    private void validateAddressCountry(String country) {
        Pattern pattern = Pattern.compile(ValidationPattern.STATE_CITY_COUNTRY);
        Matcher matcher = pattern.matcher(String.valueOf(country));
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_COUNTRY_INVALID);
            errorList.add(error);
        }
    }

    private void validateAddressCity(String city) {
        Pattern pattern = Pattern.compile(ValidationPattern.STATE_CITY_COUNTRY);
        Matcher matcher = pattern.matcher(String.valueOf(city));
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_CITY_INVALID);
            errorList.add(error);
        }
    }

    private void validateAddressType(String addressType) {
        List<String> addressTypeList = new ArrayList<>();
        addressTypeList.add("RESIDENCE");
        addressTypeList.add("OFFICE");
        addressTypeList.add("PERMANENT");

        if (!(addressTypeList.contains(addressType))) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ADDRESS_TYPE_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE KYC DOCUMENT(MANDATORY)
     *
     * @param kycList
     */
    private void validateKYC(List<Kyc> kycList) {

        for (Kyc kyc : kycList) {

            if (StringUtils.isNotBlank(kyc.getKycName())) {
                validateKycName(kyc.getKycName());
            } else {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_KYC_NAME_BLANK);
                errorList.add(error);
            }

            if (StringUtils.equals("PAN", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validatePanNumber(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PAN_NUMBER_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("PASSPORT", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validatePassportNumber(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_PASSPORT_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("VOTER-ID", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validateVoterId(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_VOTERID_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("RATION-CARD", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validateRationCard(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_RATIONCARD_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("DRIVING-LICENSE", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validateDrivingLicenseNumber(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_DRIVINGLICENSE_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("AADHAAR", kyc.getKycName())) {
                if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                    validateUIDNumber(kyc.getKycNumber());
                } else {
                    Error error = ValidationConstant.ERROR_MAP
                            .get(ErrorCode.APPLICANT_AADHAAR_BLANK);
                    errorList.add(error);
                }
            } else if (StringUtils.equals("APPLICANT-PHOTO", kyc.getKycName())) {

            } else if (StringUtils.equals("INCOME-PROOF1", kyc.getKycName())) {

            } else if (StringUtils.equals("INCOME-PROOF2", kyc.getKycName())) {

            } else if (StringUtils.equals("OTHER", kyc.getKycName())) {

            } else if (StringUtils.equals("APPLICATION_FORM", kyc.getKycName())) {

            } else if (StringUtils.equals("AGREEMENT", kyc.getKycName())) {

            } else if (StringUtils.equals("ACH", kyc.getKycName())) {

            } else if (StringUtils.equals("DISBURSEMENT", kyc.getKycName())) {

            }
        }

    }

    /**
     * VALIDATE KYC NAMES(MANDATORY)
     *
     * @param kycName
     */

    private void validateKycName(String kycName) {
        List<String> kycNameList = new ArrayList<>();
        kycNameList.add("APPLICANT-PHOTO");
        kycNameList.add("RATION-CARD");
        kycNameList.add("VOTER-ID");
        kycNameList.add("PAN");
        kycNameList.add("AADHAAR");
        kycNameList.add("PASSPORT");
        kycNameList.add("DRIVING-LICENSE");
        kycNameList.add("INCOME-PROOF1");
        kycNameList.add("INCOME-PROOF2");
        kycNameList.add("APPLICATION_FORM");
        kycNameList.add("AGREEMENT");
        kycNameList.add("ACH");
        kycNameList.add("DISBURSEMENT");
        kycNameList.add("OTHER");

        if (!kycNameList.contains(kycName)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_KYC_NAME_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE PAN NUMBER
     *
     * @param kycNumber
     */
    private void validatePanNumber(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.PAN);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_PAN_NUMBER_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE UID NUMBER
     *
     * @param kycNumber
     */
    private void validateUIDNumber(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.UID);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_UID_INVALID);
            errorList.add(error);
        }
    }

    private void validateVoterId(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.VOTER_ID);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_VOTER_ID_INVALID);
            errorList.add(error);
        }
    }

    private void validateDrivingLicenseNumber(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.DRIVING_LICENSE);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_DRIVING_LICENSE_NUMBER_INVALID);
            errorList.add(error);
        }
    }

    private void validatePassportNumber(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.PASSPORT);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_PASSPORT_INVALID);
            errorList.add(error);
        }
    }

    private void validateRationCard(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.RATION_CARD);
        Matcher matcher = pattern.matcher(kycNumber);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_RATION_CARD_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE APPLICANT DATE OF BIRTH(MANDATORY FIELD)
     *
     * @param dateOfBirth
     */
    private void validateDateOfBirth(String dateOfBirth) {
        Pattern pattern = Pattern.compile(ValidationPattern.DOB);
        Matcher matcher = pattern.matcher(dateOfBirth);
        // Check if pattern matches
        if (!(matcher.matches())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_DATE_OF_BIRTH_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE APPLICANT GENDER(MANDATORY)
     *
     * @param gender
     */
    private void validateGender(String gender) {
        if (StringUtils.isNotBlank(gender)) {
            if (!(StringUtils
                    .equalsIgnoreCase("MALE", StringUtils.trim(gender))
                    || StringUtils.equalsIgnoreCase("FEMALE",
                    StringUtils.trim(gender)) || StringUtils
                    .equalsIgnoreCase("TRANSGENDER",
                            StringUtils.trim(gender)))) {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_GENDER_INVALID);
                errorList.add(error);
            }
        }

    }


    /**
     * VALIDATE APPLICANT NAME(MANDATORY FIELD)
     *
     * @param applicantName
     */
    private void validateApplicantName(Name applicantName) {

        // Mandatory field
        if (StringUtils.isNotBlank(applicantName.getFirstName())) {
            Pattern pattern = Pattern.compile(ValidationPattern.FIRST_NAME);
            Matcher matcher = pattern.matcher(String.valueOf(applicantName
                    .getFirstName()));
            // Check if pattern matches
            if (!(matcher.matches())) {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_FIRST_NAME_INVALID);
                errorList.add(error);
            }
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_FIRST_NAME_BLANK);
            errorList.add(error);
        }

        // Mandatory field
        if (StringUtils.isNotBlank(applicantName.getLastName())) {
            Pattern pattern = Pattern.compile(ValidationPattern.LAST_NAME);
            Matcher matcher = pattern.matcher(String.valueOf(applicantName
                    .getLastName()));
            // Check if pattern matches
            if (!(matcher.matches())) {
                Error error = ValidationConstant.ERROR_MAP
                        .get(ErrorCode.APPLICANT_LAST_NAME_INVALID);
                errorList.add(error);
            }
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_LAST_NAME_BLANK);
            errorList.add(error);
        }


    }

    /**
     * VALIDATE APPLICANT_ID
     *
     * @param applicantId
     */
    private void validateApplicantid(String applicantId) {

        if (StringUtils.length(applicantId) > 20) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.APPLICANT_ID_INVALID);
            errorList.add(error);
        }
    }


    /**
     * VALIDATE HEADER CODE START HERE
     *
     * @param header
     */
    private void validateHeader(Header header) {


        logger.debug("validating header is started");
        /**
         /**
         * To check InstitutionID validation.
         */
        if (StringUtils.isNotBlank(header.getInstitutionId())) {

            validateInstitutionId(header.getInstitutionId());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_INSTITUTION_ID_BLANK);
            errorList.add(error);
        }

        /**
         * To check ApplicationSource validation.
         */
        if (StringUtils.isBlank(header.getApplicationSource())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_APPLICATION_SOURCE_BLANK);
            errorList.add(error);
        }
        /**
         * To check RequestType
         */
        if (StringUtils.isNotBlank(header.getRequestType())) {
            validateRequestType(header.getRequestType());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_REQUEST_TYPE_BLANK);
            errorList.add(error);
        }
        if (StringUtils.isBlank(header.getSourceId())) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_SOURCE_ID_BLANK);
            errorList.add(error);
        }


        /**
         * VAlidation for Dealer ID .
         */
        if (StringUtils.isNotBlank(header.getDealerId())) {
            validateDealerId(header.getDealerId());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_DEALER_ID_BLANK);
            errorList.add(error);
        }

        /**
         * Validation for DsaId .
         */
        if (StringUtils.isNotBlank(header.getDsaId())) {
            validateDsaId(header.getDsaId());
        } else {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_DSA_ID_BLANK);
            errorList.add(error);
        }

        logger.debug("validating header is completed");

    }


    private void validateDsaId(String dsaId) {

        if (StringUtils.length(dsaId) > 20) {

            logger.debug("inside ValidateDsaId dsaId leghth > 20");

            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_DSA_ID_INVALID);
            errorList.add(error);
        }

    }

    private void validateDealerId(String dealerId) {

        if (StringUtils.length(dealerId) > 20) {

            logger.debug("inside validateDealerId dealerId length > 20");

            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_DEALER_ID_INVALID);
            errorList.add(error);
        }

    }


    /**
     * VALIDATE REQUEST TYPE OF HEADER
     *
     * @param requestType
     */
    private void validateRequestType(String requestType) {
        if (!(StringUtils.equalsIgnoreCase("JSON", requestType))) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_REQUEST_TYPE_INVALID);
            errorList.add(error);
        }
    }

    /**
     * VALIDATE INSTITUTION_ID OF HEADER
     *
     * @param institutionId
     */
    private void validateInstitutionId(String institutionId) {
        /**
         * To check the Institutions which are available in Institution List.
         *
         * eg. 4019,4011 these are valid institutionID till date.
         *
         */
        List<String> institutionList = new ArrayList<String>();
        institutionList.add("4019");
        institutionList.add("4011");

        if (!institutionList.contains(institutionId)) {
            Error error = ValidationConstant.ERROR_MAP
                    .get(ErrorCode.HEADER_INSTITUTION_ID_INVALID);
            errorList.add(error);
        }

    }

    private void validateForSBFC(ApplicationRequest applicationRequest, String executionPoint) throws GoNoGoException {
        switch (executionPoint) {
            case EndPointReferrer.STEP_REGISTRATION:
                // Check whetehr existing loan available
                if (ApplicantType.get(applicationRequest.getApplicantType()) == ApplicantType.EXISTING
                        && StringUtils.isEmpty(applicationRequest.getRequest().getApplication().getExistingLoanAccNumber())) {
                    // Throw exception with result
                    throw new GoNoGoException(EMPTY_LOAN_ACCOUNT_NUMBER);
                }
                break;
            case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:
                // For CPA carry out this check
                if( Roles.Role.CPA == Roles.Role.valueOf(applicationRequest.getHeader().getLoggedInUserRole()) ) {

                    // Check if applicant is Non-Individual atleast One Individual co-applicant is mandetory.
                    if (ApplicantType.get(applicationRequest.getRequest().getApplicant().getApplicantType())
                                                                            == ApplicantType.NON_INDIVIDUAL) {
                        if (CollectionUtils.isEmpty(applicationRequest.getRequest().getCoApplicant())) {
                            // Throw exception with result
                            throw new GoNoGoException();
                        }

                        List<CoApplicant> coApplicantList =
                                applicationRequest.getRequest().getCoApplicant()
                                        .stream()
                                        .filter(coApplicant -> ApplicantType.get(coApplicant.getApplicantType())
                                                                            == ApplicantType.INDIVIDUAL)
                                        .collect(Collectors.toList());

                        if (CollectionUtils.isEmpty(coApplicantList)) {
                            throw new GoNoGoException(INVALID_COAPPLICANT_TYPE);
                        }
                    }
                }
                break;
            case EndPointReferrer.STEP_PROFESSION_INCOME_DETAILS: {
                // TODO
                break;
            }
        }
    }
}
