package com.softcell.service.validator;

import com.softcell.constants.ValidationPattern;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.tkil.TKILRequest;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by yogeshb on 14/3/17.
 */
@Service
public class SerialNumberValidationEngineImpl implements SerialNumberValidationEngine {

    @Override
    public Collection<Error> validationForCAV(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        String serialNumberLength = "must be 7";
        Collection<Error> errors = sonyCommonValidation(serialSaleConfirmationRequest);
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber()) || StringUtils.length(serialSaleConfirmationRequest.getSerialNumber()) != 7) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(String.format(ErrorCode.INVALID_SERIAL_NUMBER, serialNumberLength))
                    .build());
        }

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getProductCode()) || StringUtils.length(serialSaleConfirmationRequest.getProductCode()) != 8) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sProductCode")
                    .id("productCode")
                    .message(ErrorCode.INVALID_SERIAL_NUMBER_PRODUCT_CODE)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getStoreCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sStoreCode")
                    .id("storeCode")
                    .message(ErrorCode.INVALID_STORE_CODE)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForMobile(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = sonyCommonValidation(serialSaleConfirmationRequest);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.INVALID_MODEL_CODE)
                    .build());
        }

        return errors;

    }

    private Collection<Error> sonyCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getMaterialName())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sMaterialName")
                    .id("materialName")
                    .message(ErrorCode.INVALID_MATERIAL_NAME)
                    .build());
        }
        return errors;
    }


    private Collection<Error> panasonicCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getHeader().getDealerId())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDealerId")
                    .id("dealerId")
                    .message(ErrorCode.INVALID_DEALER_ID)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.INVALID_MODEL_CODE)
                    .build());
        }

        return errors;
    }


    @Override
    public Collection<Error> validationForSamsung(SerialSaleConfirmationRequest serialsaleconfirmationrequest) {

        Collection<Error> errors = lgSamsungCommonValidation(serialsaleconfirmationrequest);

        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSaleType()) || !GngUtils.isValid(ValidationPattern.SPECIAL_CHARACTER, serialsaleconfirmationrequest.getSaleType())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSaleType")
                    .id("saleType")
                    .message(ErrorCode.INVALID_SALE_TYPE)
                    .build());
        }

        if (StringUtils.isBlank(serialsaleconfirmationrequest.getPartnerCode()) || !GngUtils.isValid(ValidationPattern.SPECIAL_CHARACTER, serialsaleconfirmationrequest.getPartnerCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sPartnerCode")
                    .id("partnerCode")
                    .message(ErrorCode.INVALID_PARTNER_CODE)
                    .build());
        }
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getProductType()) || !GngUtils.isValid(ValidationPattern.SPECIAL_CHARACTER, serialsaleconfirmationrequest.getProductType())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sProductType")
                    .id("productType")
                    .message(ErrorCode.INVALID_PRODUCT_TYPE)
                    .build());
        }

        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSaleDate()) || !GngUtils.isValid(ValidationPattern.SPECIAL_CHARACTER, serialsaleconfirmationrequest.getSaleDate())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSaleDate")
                    .id("saleDate")
                    .message(ErrorCode.DATE_FORMAT_NOT_SUPPORTED)
                    .build());
        }

        return errors;

    }

    @Override
    public Collection<Error> validationForSamsungImie(SerialSaleConfirmationRequest serialsaleconfirmationrequest) {
        Collection<Error> errors = new ArrayList<>();
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.INVALID_SKUCODE)
                    .build());
        }
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getChannelCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sChannelCode")
                    .id("channelCode")
                    .message(ErrorCode.INVALID_CHANNEL_CODE)
                    .build());
        }
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSaleDate())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSaleDate")
                    .id("saleDate")
                    .message(ErrorCode.DATE_FORMAT_NOT_SUPPORTED)
                    .build());
        }
        if (StringUtils.isBlank(serialsaleconfirmationrequest.getHeader().getDealerId())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDealerId")
                    .id("dealerId")
                    .message(ErrorCode.INVALID_DEALER_ID)
                    .build());
        }


        return errors;
    }


    @Override
    public Collection<Error> validationForPanasonicSerial(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        String serialNumber = serialSaleConfirmationRequest.getSerialNumber();
        int length = StringUtils.length(serialNumber);

        Collection<Error> errors = panasonicCommonValidation(serialSaleConfirmationRequest);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber()) && (length < 8 || length > 18)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(String.format(ErrorCode.LENGHT_SERIAL_INBETWEEN, 8 , 18))
                    .build());
        }

        return errors;
    }

    @Override
    public Collection<Error> validationForPanasonicImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        String imeiNumber = serialSaleConfirmationRequest.getImeiNumber();
        int length = StringUtils.length(imeiNumber);

        Collection<Error> errors = panasonicCommonValidation(serialSaleConfirmationRequest);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && (length != 15)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH , 15))
                    .build());
        }

        return errors;

    }


    @Override
    public Collection<Error> validationForIntex(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();

        String serialNumber = serialSaleConfirmationRequest.getSerialNumber();
        int length = StringUtils.length(serialNumber);

        if (StringUtils.isBlank(serialNumber) || length > 30) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.SERIAL_NUMBER_BLANK_AND_NOT_VALID_FOR_INTEX)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForAppleDPL(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = getAppleCommonValidation(serialSaleConfirmationRequest);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }


        return errors;
    }

    @Override
    public Collection<Error> validationForAppleCD(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = getAppleCommonValidation(serialSaleConfirmationRequest);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }

        return errors;
    }

    @Override
    public Collection<Error> validationForImeiAppleRollback(RollbackRequest rollbackRequest) {
        Collection<Error> errors = new ArrayList<>();

        String imeiNumber = rollbackRequest.getImeiNumber();
        int length = StringUtils.length(imeiNumber);

        if (StringUtils.isBlank(imeiNumber) || length != 15) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.IMEI_NUMBER_EMPTY_OR_INVALID)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForSerialNumberAppleRollback(RollbackRequest rollbackRequest) {
        Collection<Error> errors = new ArrayList<>();

        String serialNumber = rollbackRequest.getSerialNumber();
        int length = StringUtils.length(serialNumber);

        if (StringUtils.isBlank(serialNumber) || length > 25) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.SERIAL_NUMBER_EMPTY_OR_INVALID)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForGioneeImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber()) || StringUtils.length(serialSaleConfirmationRequest.getImeiNumber()) >30) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.IMEI_NUMBER_EMPTY_OR_INVALID)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode()) || StringUtils.length(serialSaleConfirmationRequest.getSkuCode()) >20) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("modelCode/skuCode")
                    .message(ErrorCode.INVALID_MODEL_CODE)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getStoreCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sStoreCode")
                    .id("storeCode")
                    .message(ErrorCode.INVALID_RETAILER_CODE)
                    .build());
        }
        return errors;
    }

    private Collection<Error> getAppleCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getMpn())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sMpn")
                    .id("mpn")
                    .message(ErrorCode.BLANK_MPN)
                    .build());
        }

        if (serialSaleConfirmationRequest.getStoreAppleId() <= 0) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("lStoreAppleId")
                    .id("storeAppleId")
                    .message(ErrorCode.BLANK_STORE_APPLE_ID)
                    .build());
        }

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getScheme())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sScheme")
                    .id("scheme")
                    .message(ErrorCode.BLANK_SCHEME)
                    .build());
        }

        /*if (serialSaleConfirmationRequest.getTenure()<=0) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("iTenure")
                    .id("tenure")
                    .message(ErrorCode.BLANK_TENURE)
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getBankName())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sBankName")
                    .id("bankName")
                    .message(ErrorCode.BLANK_BANK_NAME)
                    .build());
        }*/

        return errors;

    }


    private Collection<Error> lgSamsungCommonValidation(SerialSaleConfirmationRequest serialsaleconfirmationrequest) {

        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSerialNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }

        if (StringUtils.isBlank(serialsaleconfirmationrequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.INVALID_SKUCODE)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForVideocon(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();

        String serialNumber = serialSaleConfirmationRequest.getSerialNumber();
        String skuCode = serialSaleConfirmationRequest.getSkuCode();
        int serialLength = StringUtils.length(serialNumber);
        final int slength=18;
        if (StringUtils.isBlank(serialNumber)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }
        if (StringUtils.isBlank(skuCode)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForSerialVideoconRollback(RollbackRequest rollbackRequest) {
        SerialSaleConfirmationRequest serialSaleConfirmationRequest = SerialSaleConfirmationRequest.builder()
        .skuCode(rollbackRequest.getSkuCode())
        .serialNumber(rollbackRequest.getSerialNumber())
        .build();
        return validationForVideocon(serialSaleConfirmationRequest);
    }

    @Override
    public Collection<Error> validationForSerialVideoconDisbursement(DisbursementRequest disbursementRequest) {
        SerialSaleConfirmationRequest serialSaleConfirmationRequest = SerialSaleConfirmationRequest.builder()
                .skuCode(disbursementRequest.getSkuCode())
                .serialNumber(disbursementRequest.getSerialNumber())
                .build();
        return validationForVideocon(serialSaleConfirmationRequest);
    }

    @Override
    public Collection<Error> validationForTkil(TKILRequest tkilRequest) {
        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(tkilRequest.getDoNumber()) || null==tkilRequest.getDoNumber()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDoNumber")
                    .id("doNumber")
                    .message(ErrorCode.INVALID_DO_NUMBER)
                    .build());
        }

        if (StringUtils.isBlank(tkilRequest.getDoDate()) || null==tkilRequest.getDoDate()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDoDate")
                    .id("doDate")
                    .message(ErrorCode.INVALID_SALE_DATE)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getStoreCode()) || null==tkilRequest.getStoreCode()) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sStoreCode")
                    .id("storeCode")
                    .message(ErrorCode.INVALID_STORE_CODE)
                    .build());
        }

        if (StringUtils.isBlank(tkilRequest.getCustomerName()) || null==tkilRequest.getCustomerName()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sCustomerName")
                    .id("customerName")
                    .message(ErrorCode.INVALID_NAME)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getProductBrand()) || null==tkilRequest.getProductBrand()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sProductBrand")
                    .id("productBrand")
                    .message(ErrorCode.INVALID_MAKE)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getProductCost()) || null==tkilRequest.getProductCost()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sProductCost")
                    .id("productCost")
                    .message(ErrorCode.INVALID_PRODUCT_COST)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getNetLoanAmount()) || null==tkilRequest.getNetLoanAmount()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sNetLoanAmount")
                    .id("netLoanAmount")
                    .message(ErrorCode.INVALID_NET_LOAN_AMOUNT)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getDisbursementAmount()) || null==tkilRequest.getDisbursementAmount()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDisbursementAmount")
                    .id("disbursementAmount")
                    .message(ErrorCode.INVALID_DISBURSEMENT_AMOUNT)
                    .build());
        }
        if (StringUtils.isBlank(tkilRequest.getProcessingFees()) || null==tkilRequest.getProcessingFees()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sProcessingFees")
                    .id("processingFees")
                    .message(ErrorCode.INVALID_PROCESSING_FEES)
                    .build());
        }

        if (StringUtils.isBlank(tkilRequest.getDealersubvention()) || null==tkilRequest.getDealersubvention()){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDealersubvention")
                    .id("dealersubvention")
                    .message(ErrorCode.INVALID_DEALER_SUBVENTION)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForKent(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return kentCommonValidation(serialSaleConfirmationRequest.getSerialNumber(),serialSaleConfirmationRequest.getSkuCode());
    }

    @Override
    public Collection<Error> validationForKentRollback(RollbackRequest rollbackRequest) {
        return kentCommonValidation(rollbackRequest.getSerialNumber(),rollbackRequest.getSkuCode());
    }

    @Override
    public Collection<Error> validationForKentDisburse(DisbursementRequest disbursementRequest) {
        return kentCommonValidation(disbursementRequest.getSerialNumber(),disbursementRequest.getSkuCode());
    }

    private Collection<Error> kentCommonValidation(String serialNumber, String skuCode) {

        Collection<Error> errors = new ArrayList<>();
        if (StringUtils.isBlank(serialNumber)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }
        if (StringUtils.isBlank(skuCode)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForOppoImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        String modelCode = serialSaleConfirmationRequest.getSkuCode();
        int length = StringUtils.length(modelCode);

        Collection<Error> errors = new ArrayList<>();
        final int  imeiNoLenght=15;

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && StringUtils.length(serialSaleConfirmationRequest.getImeiNumber()) != 15) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH,imeiNoLenght))
                    .build());
        }

        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getSkuCode()) && (length < 2 || length > 50)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("modelCode/skuCode")
                    .message(String.format(ErrorCode.LENGTH_MODEL_INBETWEEN, 2, 50))
                    .build());
        }

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getDealerId())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sOppoDealerId")
                    .id("oppoDealerId")
                    .message(ErrorCode.DEALER_CODE_EMPTY)
                    .build());
        }

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getChannelCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sChannelCode")
                    .id("channelCode")
                    .message(ErrorCode.CHANNEL_CODE_EMPTY)
                    .build());
        }

        return errors;
    }


    @Override
    public Collection<Error> validationForGoogleImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = googleCommonValidation(serialSaleConfirmationRequest.getImeiNumber());

        // 'skuCode' is consider as 'rdsCode' for Google/Redington.
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_RDS_CODE)
                    .build());
        }

        return errors;

    }

    @Override
    public Collection<Error> validationForGoogleRollback(RollbackRequest rollbackRequest) {
        return googleCommonValidation(rollbackRequest.getImeiNumber());
    }

    private Collection<Error> googleCommonValidation(String imeiNumber) {

        Collection<Error> errors = new ArrayList<>();

        int length = StringUtils.length(imeiNumber);

        if (StringUtils.isBlank(imeiNumber)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(imeiNumber) && (length != 15)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.LENGTH_IMEI_NUMBER, 15))
                    .build());
        }

        return errors;
    }

    @Override
    public Collection<Error> commonValidationForImeiAndModel(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        int length = StringUtils.length(serialSaleConfirmationRequest.getImeiNumber());

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && (length !=15)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH, 15))
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }

        return errors;
    }


}