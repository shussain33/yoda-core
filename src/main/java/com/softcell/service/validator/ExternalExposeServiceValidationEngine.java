package com.softcell.service.validator;

import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * Created by yogeshb on 24/5/17.
 */
public interface ExternalExposeServiceValidationEngine {
    /**
     *
     * @param losDetails
     * @return
     */
    Collection<Error> validateLosUpdateRequest(LOSDetails losDetails);
}
