package com.softcell.service.validator;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.gonogo.model.response.core.Error;

import java.util.List;

public interface ConfigurationValidationEngine {
    public List<Error> validateTemplateConfigurationFieldsOnInsert(TemplateConfiguration templateConfiguration);

}
