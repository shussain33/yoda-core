/**
 *
 */
package com.softcell.service.validator;

import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * @author kishorp
 */
public interface ApplicationValidationEngine {

    /**
     * Validate Application Request Class
     *
     * @param applicationRequest
     * @return
     */
    BaseResponse validate(ApplicationRequest applicationRequest);

    void validate(ApplicationRequest applicationRequest, String executionPoint) throws GoNoGoException;

    /**
     *
     * @param gstDetailsRequest
     * @return
     */
    Collection<Error> validateGstRequest(GstDetailsRequest gstDetailsRequest);
}
