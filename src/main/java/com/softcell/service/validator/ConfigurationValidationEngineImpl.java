package com.softcell.service.validator;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.gonogo.model.response.core.Error;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConfigurationValidationEngineImpl implements ConfigurationValidationEngine {

    @Override
    public List<Error> validateTemplateConfigurationFieldsOnInsert(TemplateConfiguration templateConfiguration) {
        List<Error> errors = new ArrayList<>();
        if (null != templateConfiguration) {
            if (StringUtils.isBlank(templateConfiguration.getInstitutionId())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.INSTITUTION_ID));
            }
            if (StringUtils.isBlank(templateConfiguration.getInstitutionName())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.INSTITUTION_NAME));
            }
            if (StringUtils.isBlank(templateConfiguration.getLogoId())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.LOGO_ID));
            }
            if (StringUtils.isBlank(templateConfiguration.getProductId())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.PRODUCT_ID));
            }
            if (StringUtils.isBlank(templateConfiguration.getTemplateId())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.TEMPLATE_ID));
            }
            if (StringUtils.isBlank(templateConfiguration.getTemplatePath())) {
                errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.TEMPLATE_PATH));
            }
        } else {
            errors.add(TemplateConfigurationValidationConstant.ERROR_MAP.get(TemplateConfigurationValidationConstant.TEMPLATE_CONFIGURATION));
        }
        return errors;
    }

}
