package com.softcell.service.validator;

import com.softcell.gonogo.model.response.core.Error;

import java.util.HashMap;
import java.util.Map;

public class TemplateConfigurationValidationConstant {
    public static final String TEMPLATE_CONFIGURATION = "TEMPLATE_CONFIGURATION";
    public static final String INSTITUTION_ID = "INSTITUTION_ID";
    public static final String INSTITUTION_NAME = "INSTITUTION_NAME";
    public static final String LOGO_ID = "LOGO_ID";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String TEMPLATE_ID = "TEMPLATE_ID";
    public static final String TEMPLATE_PATH = "TEMPLATE_PATH";
    public static final Map<String, Error> ERROR_MAP = new HashMap<String, Error>();

    static {
        initErrorCache();
    }

    public static void initErrorCache() {
        Error error = null;
        error = new Error();
        error.setFieldName(INSTITUTION_ID);
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage(INSTITUTION_ID);
        error.setErrorType("ERROR");
        ERROR_MAP.put(INSTITUTION_ID, error);

        error = new Error();
        error.setFieldName(INSTITUTION_NAME);
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage(INSTITUTION_NAME);
        error.setErrorType("ERROR");
        ERROR_MAP.put(INSTITUTION_NAME, error);

        error = new Error();
        error.setFieldName("LOGO_ID");
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage("LOGO_ID");
        error.setErrorType("ERROR");
        ERROR_MAP.put(LOGO_ID, error);

        error = new Error();
        error.setFieldName(PRODUCT_ID);
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage(PRODUCT_ID);
        error.setErrorType("ERROR");
        ERROR_MAP.put(PRODUCT_ID, error);

        error = new Error();
        error.setFieldName(TEMPLATE_ID);
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage(TEMPLATE_ID);
        error.setErrorType("ERROR");
        ERROR_MAP.put(TEMPLATE_ID, error);

        error = new Error();
        error.setFieldName(TEMPLATE_PATH);
        error.setLevel(Level.L1.name());
        error.setId("");
        error.setErrorCode("");
        error.setMessage(TEMPLATE_PATH);
        error.setErrorType("ERROR");
        ERROR_MAP.put(TEMPLATE_PATH, error);

        error = new Error();
        error.setFieldName(TEMPLATE_CONFIGURATION);
        error.setLevel("L1");
        error.setId("");
        error.setErrorCode("");
        error.setMessage(TEMPLATE_CONFIGURATION);
        error.setErrorType("ERROR");
        ERROR_MAP.put(TEMPLATE_CONFIGURATION, error);
    }
}
