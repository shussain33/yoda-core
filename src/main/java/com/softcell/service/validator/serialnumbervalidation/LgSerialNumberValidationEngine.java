package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

public interface LgSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForLgSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForLgImeiNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


}
