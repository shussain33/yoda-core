package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.service.validator.serialnumbervalidation.NokiaSerialNumberValidationEngine;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

@Service
public class NokiaSerialNumberValidationEngineImpl implements NokiaSerialNumberValidationEngine {

    @Override
    public Collection<Error> validationForNokiaImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = NokiaCommonValidation(serialSaleConfirmationRequest);

        return errors;

    }

    @Override
    public Collection<Error> validationForNokiaImeiRollback(RollbackRequest rollbackRequest) {

        Collection<Error> errors = new ArrayList<>();

        int length = StringUtils.length(rollbackRequest.getImeiNumber());

        if (StringUtils.isBlank(rollbackRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(rollbackRequest.getImeiNumber()) && (length != 15)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH, 15))
                    .build());
        }
        if (StringUtils.isBlank(rollbackRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }

        return errors;

    }

    private Collection<Error> NokiaCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        int length = StringUtils.length(serialSaleConfirmationRequest.getImeiNumber());

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && (length !=15)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH, 15))
                    .build());
        }
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }

        return errors;
    }

}
