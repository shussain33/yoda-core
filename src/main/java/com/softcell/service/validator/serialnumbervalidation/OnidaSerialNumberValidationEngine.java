package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * Created by Ibrar on 04/12/17.
 */

public interface OnidaSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForOnidaSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest);
}
