package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;

import java.util.Collection;

public interface CarrierSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    public Collection<Error> validationForCarrier(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    public Collection<Error> validationForCarrierRollback(RollbackRequest rollbackRequest);
}
