package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.validator.serialnumbervalidation.OnidaSerialNumberValidationEngine;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Ibrar on 04/12/17.
 */

@Service
public class OnidaSerialNumberValidationEngineImpl implements OnidaSerialNumberValidationEngine{

    @Override
    public Collection<Error> validationForOnidaSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();
        int SerialNoLength=StringUtils.length(serialSaleConfirmationRequest.getSerialNumber());
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber()) && (SerialNoLength > 49)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(String.format(ErrorCode.INVALID_SERIAL_LENGTH, 49))
                    .build());
        }

        return errors;
    }

}
