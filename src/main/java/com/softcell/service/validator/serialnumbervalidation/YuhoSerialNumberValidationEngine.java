package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * Created by prasenjit wadmare on 19/11/17.
 */

public interface YuhoSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForYuhoImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


}
