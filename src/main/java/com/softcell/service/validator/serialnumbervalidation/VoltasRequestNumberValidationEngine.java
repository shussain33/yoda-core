package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 *  created by prasenjit wadmare on 18/12/2017
 */

public interface VoltasRequestNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForVoltas(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


}
