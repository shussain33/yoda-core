package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;

import java.util.Collection;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

public interface NokiaSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForNokiaImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    Collection<Error> validationForNokiaImeiRollback(RollbackRequest rollbackRequest);



}
