package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;

import java.util.Collection;

/**
 * @author prasenjit wadmare
 * @date 02/01/2018
 */

public interface LavaSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    Collection<Error> validationForLavaImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


}
