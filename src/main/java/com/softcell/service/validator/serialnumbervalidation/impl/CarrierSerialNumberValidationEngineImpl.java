package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.service.validator.serialnumbervalidation.CarrierSerialNumberValidationEngine;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class CarrierSerialNumberValidationEngineImpl implements CarrierSerialNumberValidationEngine {

    @Override
    public Collection<Error> validationForCarrier(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return carrierCommonValidation(serialSaleConfirmationRequest.getSerialNumber(),serialSaleConfirmationRequest.getMake());
    }

    @Override
    public Collection<Error> validationForCarrierRollback(RollbackRequest rollbackRequest) {
        return carrierCommonValidation(rollbackRequest.getSerialNumber(),rollbackRequest.getMake());
    }

    private Collection<Error> carrierCommonValidation(String serialNumber, String make) {

        Collection<Error> errors = new ArrayList<>();

        int serialNolength = StringUtils.length(serialNumber);
        int makelength = StringUtils.length(make);

        if (StringUtils.isBlank(serialNumber)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.BLANK_SERIAL_NUMBER)
                    .build());
        }
        if (StringUtils.isNotBlank(serialNumber) && (serialNolength >100)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(String.format(ErrorCode.SERIAL_NOT_GREATER, 100))
                    .build());
        }
        if (StringUtils.isBlank(make)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sMake")
                    .id("make")
                    .message(ErrorCode.BLANK_MAKE_CODE)
                    .build());
        }
        if ( StringUtils.isNotBlank(make) && (makelength > 50)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sMake")
                    .id("make")
                    .message(String.format(ErrorCode.MAKE_NOT_GREATER, 50))
                    .build());
        }

        return errors;
    }

}
