package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.service.validator.serialnumbervalidation.ComioSerialNumberValidationEngine;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by ibrar on 16/11/17.
 */
@Service
public class ComioSerialNumberValidationEngineImpl implements ComioSerialNumberValidationEngine {

    @Override
    public Collection<Error> validationForComioImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        Collection<Error> errors = new ArrayList<>();
        int imeiLength=StringUtils.length(serialSaleConfirmationRequest.getImeiNumber());

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && (imeiLength > 30)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.IMEI_NOT_GREATER, 30))
                    .build());
        }
        return errors;
    }

    @Override
    public Collection<Error> validationForComioRollback(RollbackRequest rollbackRequest) {
        Collection<Error> errors = new ArrayList<>();

        int imeiLength=StringUtils.length(rollbackRequest.getImeiNumber());
        int modelLength=StringUtils.length(rollbackRequest.getSkuCode());

        if (StringUtils.isBlank(rollbackRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }
        if ( StringUtils.isNotBlank(rollbackRequest.getImeiNumber()) && (imeiLength > 30)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.IMEI_NOT_GREATER, 30))
                    .build());
        }

        if (StringUtils.isBlank(rollbackRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.BLANK_MODEL_CODE)
                    .build());
        }

        if ( StringUtils.isNotBlank(rollbackRequest.getSkuCode()) && (modelLength > 20)) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(String.format(ErrorCode.MODEL_NOT_GREATER, 20))
                    .build());
        }
        return errors;
    }
}