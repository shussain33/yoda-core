package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.validator.SerialNumberValidationEngine;
import com.softcell.service.validator.serialnumbervalidation.LavaSerialNumberValidationEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author prasenjit wadmare
 * @date 02/01/2018
 */

@Service
public class LavaSerialNumberValidationEngineImpl implements LavaSerialNumberValidationEngine {

    @Autowired
    private SerialNumberValidationEngine serialNumberValidationEngine;

    @Override
    public Collection<Error> validationForLavaImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = LavaCommonValidation(serialSaleConfirmationRequest);

        return errors;

    }

    private Collection<Error> LavaCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        errors = serialNumberValidationEngine.commonValidationForImeiAndModel(serialSaleConfirmationRequest);

        return errors;
    }

}
