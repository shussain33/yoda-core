package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.validator.serialnumbervalidation.VoltasRequestNumberValidationEngine;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 *  created by prasenjit wadmare on 18/12/2017
 */

@Service
public class VoltasRequestNumberValidationEngineImpl implements VoltasRequestNumberValidationEngine {


    @Override
    public Collection<Error> validationForVoltas(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        // serviceRequestNumber  is used for Voltas.
        if (StringUtils.isBlank(serialSaleConfirmationRequest.getServiceRequestNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSRNumber")
                    .id("serviceRequestNumber")
                    .message(ErrorCode.BLANK_SERVICE_REQUEST_NUMBER)
                    .build());
        }

        return errors;
    }
}
