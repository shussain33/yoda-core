package com.softcell.service.validator.serialnumbervalidation.impl;

import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.service.validator.serialnumbervalidation.LgSerialNumberValidationEngine;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author prasenjit wadmare
 * @date 09 Jan 2018
 */

@Service
public class LgSerialNumberValidationEngineImpl implements LgSerialNumberValidationEngine {

    private static final Logger logger = LoggerFactory.getLogger(LgSerialNumberValidationEngineImpl.class);


    @Override
    public Collection<Error> validationForLgSerialNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = LgCommonValidation(serialSaleConfirmationRequest);

        String serialNumber = serialSaleConfirmationRequest.getSerialNumber();
        int length = StringUtils.length(serialNumber);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSerialNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                    .build());
        }

        if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getSerialNumber()) && length < 12 || length > 25) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSerialNumber")
                    .id("serialNumber")
                    .message(String.format(ErrorCode.LENGHT_SERIAL_INBETWEEN, 12 , 25))
                    .build());
        }

        return errors;

    }

    @Override
    public Collection<Error> validationForLgImeiNumber(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = LgCommonValidation(serialSaleConfirmationRequest);

        String serialNumber = serialSaleConfirmationRequest.getImeiNumber();
        int length = StringUtils.length(serialNumber);

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getImeiNumber())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(ErrorCode.INVALID_IMEI_NUMBER)
                    .build());
        }

        if (StringUtils.isNotBlank(serialSaleConfirmationRequest.getImeiNumber()) && length != 15) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sImeiNumber")
                    .id("imeiNumber")
                    .message(String.format(ErrorCode.INVALID_IMEI_LENGTH, 15))
                    .build());
        }

        return errors;
    }


    private Collection<Error> LgCommonValidation(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        Collection<Error> errors = new ArrayList<>();

        if (StringUtils.isBlank(serialSaleConfirmationRequest.getSkuCode())) {
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sSkuCode")
                    .id("skuCode")
                    .message(ErrorCode.INVALID_SKUCODE)
                    .build());
        }

        return errors;
    }

}
