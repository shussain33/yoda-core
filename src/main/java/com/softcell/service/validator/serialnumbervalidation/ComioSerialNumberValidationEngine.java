package com.softcell.service.validator.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;

import java.util.Collection;

/**
 * Created by ibrar on 16/11/17.
 */
public interface ComioSerialNumberValidationEngine {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    public Collection<Error> validationForComioImei(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    public Collection<Error> validationForComioRollback(RollbackRequest rollbackRequest);

}


