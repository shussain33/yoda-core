package com.softcell.service.validator;

import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.response.core.Error;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by yogeshb on 24/5/17.
 */
@Service
public class ExternalExposeServiceValidationEngineImpl implements ExternalExposeServiceValidationEngine {

    @Override
    public  Collection<Error>  validateLosUpdateRequest(LOSDetails losDetails) {

        Collection<Error> errors = new ArrayList<>();

        if(!StringUtils.equals(Status.YES.name(),losDetails.getDealerTieUpFlag()) &&
                !StringUtils.equals(Status.NO.name(),losDetails.getDealerTieUpFlag())){
            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sDealerTieUpFlag")
                    .id("DealerTieUpFlag")
                    .message(ErrorCode.INVALID_DEALER_TIEUP_FLAG)
                    .build());

        }

        if(StringUtils.equals(Status.NO.name(),losDetails.getDealerTieUpFlag())
                && StringUtils.isBlank(losDetails.getUtrNumber())){

            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sUtr")
                    .id("UtrNumber")
                    .message(ErrorCode.BLANK_UTR_NUMBER)
                    .build());

        }

        if(StringUtils.equals(Status.YES.name(),losDetails.getDealerTieUpFlag())
                && (StringUtils.isBlank(losDetails.getUtrNumber())
                && StringUtils.isBlank(losDetails.getDealerReferenceNumber()) )){

            errors.add(Error.builder()
                    .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                    .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                    .level(Error.SEVERITY.HIGH.name())
                    .fieldName("sUtr || sDealerRefNumber")
                    .id("UtrNumber || DealerRefNumber")
                    .message(ErrorCode.BLANK_UTR_NUMBER_OR_DEALER_REFERENCE)
                    .build());

        }
        return errors;
    }
}
