package com.softcell.service;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yogeshb
 */
public interface GradualApplicationManager {
    /**
     * @param applicationRequest
     * @param stepId
     * @param httpRequest
     * @return
     */
    public BaseResponse initApplicationLifeCycle(
            ApplicationRequest applicationRequest, String stepId,
            HttpServletRequest httpRequest) throws Exception;
}
