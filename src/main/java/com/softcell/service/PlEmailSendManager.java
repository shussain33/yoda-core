package com.softcell.service;

import com.softcell.gonogo.model.core.MailRequest;

/**
 * @author yogeshb
 */
public interface PlEmailSendManager {
    /**
     * @param attachementDOMailRequest
     * @return
     */
    public boolean sendEmailToClient(MailRequest attachementDOMailRequest);

}
