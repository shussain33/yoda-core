package com.softcell.service;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.util.List;


public interface LOSGoNoGoApplicationService {
	List<GoNoGoCustomerApplication> getAllgoNoGoCustomerApplicationslistRecord();

	GoNoGoCustomerApplication getGONOGOApplicationAsPerCaseId(String gngRefId);

	List<GoNoGoCustomerApplication> getsearchRecord(String date, String stage);

	PostIpaRequest getpostIpaRequestBysRefID(String sRefID);

	SerialNumberInfo getSerialNumberInfoBysRefID(String refID);

	InsurancePremiumDetails getinsurancePremiumDetailsBysRefID(String refID);

	ExtendedWarrantyDetails getextendedWarrantyDetailsBysRefID(String refID);

	SchemeMasterData  getschemeMasterDataBysRefID(String schemeID);

	FileUploadRequest getfileUploadRequestAsPerCaseId(String gngRefId);

	//InsertOrUpdateTvsRecordGroupFive populateTvsCdLosGroupTwo(InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupTwo, GoNoGoCustomerApplication gonogoCustomerApplicationTwo, DocumentListForGroupFive uploadFileDetails);

	DocumentListForGroupFive getUploadFileDetailsAsPerCaseId(String gngRefId);

	TvsGroupServiceStatus getTvsGroupServiceStatus(String gngRefI);

	public void saveTvsGroupServicStatus(TvsGroupServiceStatus groupService_Status);

	public void postAuditingInfo(AuditingInfo PostAuditingInfo);

	List<AuditingInfo> getAuditLogById(String _id);

	List<AuditingInfo> getCompleteAuditLog();

	BaseResponse invokeTVSIntiateService(LosTvsRequest losTvsRequest, TvsIntiateData tvsIntiateData) throws Exception;

	BaseResponse invokeTVSGroupOneService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneResponse) throws Exception;

	BaseResponse invokeTVSGroupTwoService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoResponse) throws Exception;

	BaseResponse invokeTVSGroupThreeService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordResponse) throws Exception;

	BaseResponse invokeTVSGroupFourService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupFour groupFourReturn) throws Exception;

	BaseResponse invokeTVSGroupFiveService(LosTvsRequest losTvsRequest, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveResponse) throws Exception;

    List<TVSCdLosGroupResponse> pullAndPopulateDataByOnlyCaseId(String caseId);

	BaseResponse pullAndTranformDataByCaseID(LosTvsRequest losTvsRequest) throws Exception;
}
