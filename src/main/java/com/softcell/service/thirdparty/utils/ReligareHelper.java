package com.softcell.service.thirdparty.utils;

import com.google.common.collect.Iterables;
import com.softcell.constants.Constant;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.KYC_TYPES;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.core.OptDetails;
import com.softcell.gonogo.model.insurance.religare.*;
import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.DropdownMasterRequest;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.utils.GngCalculationUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ssg0268 on 8/11/19.
 */
@Component
public class ReligareHelper {

    @Autowired
    private MasterRepository masterRepository;
    private static Logger logger = LoggerFactory.getLogger(ReligareHelper.class);

    public static String RELIGARE_LOAN_PROTECTION = "CARE LOAN PROTECTION";
    public static String COVER_TYPE_FAMILY_FLOATOVER = "FAMILYFLOATER";
    public static String COVER_TYEP_INDIVIDUAL = "INDIVIDUAL";
    public static  String THREE_LAKH_SCHEME = "3 LAKH";
    public static  String FIVE_LAKH_SCHEME = "5 LAKH";
    public static  String InsuranceProduct = "OPTION1";

    public CreatePolicySecure buildReligareRequest(InsuranceRequest insuranceRequest, Applicant applicant, InsurancePolicy insurancePolicy , LoanCharges loanCharges ) throws ParseException {

        CreatePolicySecure request = new CreatePolicySecure();
        List<PolicyAdditionalFieldsDOList> policyAdditionalFieldsDOLists= new ArrayList<PolicyAdditionalFieldsDOList>();
        IntPolicyDataIO intPolicyDataIO = new IntPolicyDataIO();
        Policy policy = new Policy();
        policy.setLoanAccNum(insuranceRequest.getRefId());
        if(CollectionUtils.isNotEmpty(insurancePolicy.getOptDetailsList())){
           policy.setCoverType(COVER_TYPE_FAMILY_FLOATOVER);
        }else{
            policy.setCoverType(COVER_TYEP_INDIVIDUAL);
        }
        policy.setTerm(3);
        policy.setIsPremiumCalculation("YES");
        policy.setBusinessTypeCd("NEWBUSINESS");
        policy.setBaseAgentId("20093583");
        policy.setBaseProductId("70001065");
        //for productionAgentId   20093583
        //for AgentId lower Env 22575376
        //add check according to option 1,2
        String ch = insurancePolicy.getScheme();
        if(StringUtils.equalsIgnoreCase(insurancePolicy.getProduct().trim(),InsuranceProduct)) {
            if (StringUtils.equalsIgnoreCase(THREE_LAKH_SCHEME, ch) || StringUtils.equalsIgnoreCase(ch, "CRITICAL ILLNESS")) {
                if(isFamilyFlatOver(policy.getCoverType())){
                    policy.setSumInsured("002");
                } else{
                    policy.setSumInsured("001");
                }
            } else if (StringUtils.equalsIgnoreCase(FIVE_LAKH_SCHEME, ch)) {
                if(isFamilyFlatOver(policy.getCoverType())){
                    policy.setSumInsured("004");
                } else{
                    policy.setSumInsured("003");
                }
            }

        }else{
            if (StringUtils.equalsIgnoreCase(THREE_LAKH_SCHEME, ch)) {
                if(isFamilyFlatOver(policy.getCoverType())){
                    policy.setSumInsured("006");
                } else{
                    policy.setSumInsured("005");
                }
            } else if (StringUtils.equalsIgnoreCase(FIVE_LAKH_SCHEME, ch)) {
                if(isFamilyFlatOver(policy.getCoverType())){
                    policy.setSumInsured("008");
                } else {
                    policy.setSumInsured("007");
                }
            }

        }

        PolicyAdditionalFieldsDOList policyAdditionalField = new PolicyAdditionalFieldsDOList();
        policyAdditionalField.setFieldAgree("YES");
        policyAdditionalField.setField10(insurancePolicy.getNomineeInfo().getName().getFirstName()+ ""+ insurancePolicy.getNomineeInfo().getName().getLastName());
        //relationship master to be shared
        policyAdditionalField.setField12("MOTH");
        policyAdditionalField.setFieldTc("YES");
        policyAdditionalFieldsDOLists.add(policyAdditionalField);
        policy.setPolicyAdditionalFieldsDOList(policyAdditionalFieldsDOLists);

        policy.setLoanTenure(insurancePolicy.getTenorRequested());
        policy.setLoanAmount(insurancePolicy.getProposedAmount());
        List<PartyDOList> partyDOList = new ArrayList<>();
        int cnt =0;
        while(cnt < 2){
            partyDOList = populatePartyDoList(partyDOList , applicant, cnt, insuranceRequest.getRefId(),null) ;
            cnt++;
        }
      //TODO teset after confirmation
      if(CollectionUtils.isNotEmpty(insurancePolicy.getOptDetailsList())){
            for(OptDetails optDetails : insurancePolicy.getOptDetailsList()){
                if(StringUtils.equalsIgnoreCase(optDetails.getRelationship(),"Husband") || StringUtils.equalsIgnoreCase(optDetails.getRelationship(),
                        "Wife")){
                    partyDOList = populatePartyDoList(partyDOList , applicant, cnt, insuranceRequest.getRefId(), optDetails) ;
                }
            }
        }
        policy.setPartyDOList(partyDOList);
        Date date = loanCharges.getDisbursedDate();
        DateFormat dateFormat = new SimpleDateFormat(GngDateUtil.ddMMyyyy_WITH_SLASH );
        String strDate = dateFormat.format(date);
        policy.setLoanDisbursalDt(strDate);
        policy.setLoanTenureUnitCd("MONTH");
        intPolicyDataIO.setPolicy(policy);
        request.setIntPolicyDataIO(intPolicyDataIO);
        return request;
    }

    private boolean isFamilyFlatOver(String coverType) {
        if(StringUtils.equalsIgnoreCase(coverType,COVER_TYPE_FAMILY_FLOATOVER)){
            return true;
        }else{
            return false;
        }
    }

    private List<PartyDOList> populatePartyDoList(List<PartyDOList> partyDOList , Applicant applicant , int cnt , String refId, OptDetails optDetails) throws ParseException {
        PartyDOList partyDO = new PartyDOList ();
        List <PartyAddressDOList> partyAddressDOLists = new ArrayList<>();
        List<PartyIdentityDOList> partyIdentityDOList = new ArrayList<PartyIdentityDOList>();
        PartyIdentityDOList partyIdentityDO =  new PartyIdentityDOList();
        List <PartyContactDOList> partyContactDOLists =  new ArrayList<PartyContactDOList>();
        List<PartyEmailDOList> partyEmailDOLists  =new ArrayList<PartyEmailDOList>();
        List<PartyEmploymentDOList> partyEmploymentDOLists = new ArrayList<PartyEmploymentDOList>();
        List<PartyQuestionDOList> partyQuestionDOLists = new ArrayList<PartyQuestionDOList>();
        PartyEmploymentDOList partyEmploymentDO = new PartyEmploymentDOList();

        //reid+appid
        partyDO.setGuid(refId+applicant.getApplicantId());

        if(optDetails != null){
            partyDO.setGuid(refId + FieldSeparator.HYPHEN +applicant.getApplicantId());
            if(StringUtils.isNotEmpty(optDetails.getName().getFirstName())){
                partyDO.setFirstName(optDetails.getName().getFirstName());
            }
            if(StringUtils.isNotEmpty(optDetails.getName().getLastName())){
                partyDO.setLastName(optDetails.getName().getLastName());
            }
            if(StringUtils.isNotEmpty(applicant.getGender())){
                //need to master for transgender
                if(StringUtils.equalsIgnoreCase(optDetails.getGender() , GNGWorkflowConstant.MALE.name())){
                    partyDO.setTitleCd("MR");
                    partyDO.setGenderCd("MALE");
                }
                else  if(StringUtils.equalsIgnoreCase(optDetails.getGender() ,  GNGWorkflowConstant.FEMALE.name())){
                    partyDO.setGenderCd("FEMALE");
                    partyDO.setTitleCd("MS");
                }
            }
        }else{
            if(StringUtils.isNotEmpty(applicant.getApplicantName().getFirstName())){
                partyDO.setFirstName(applicant.getApplicantName().getFirstName());
            }
            if(StringUtils.isNotEmpty(applicant.getApplicantName().getLastName())){
                partyDO.setLastName(applicant.getApplicantName().getFirstName());
            }
            if(StringUtils.isNotEmpty(applicant.getGender())){
                //need to master for transgender
                if(StringUtils.equalsIgnoreCase(applicant.getGender() , GNGWorkflowConstant.MALE.name())){
                    partyDO.setTitleCd("MR");
                    partyDO.setGenderCd("MALE");
                }
                else  if(StringUtils.equalsIgnoreCase(applicant.getGender() ,  GNGWorkflowConstant.FEMALE.name())){
                    partyDO.setGenderCd("FEMALE");
                    partyDO.setTitleCd("MS");
                }
            }
        }

        partyDO.setRelationCd("SELF");
        //two times Primary and proposer
        if(cnt == 0){
            partyDO.setRoleCd("PROPOSER");
        }
        if(cnt == 1 || optDetails != null){
            partyDO.setRoleCd("PRIMARY");
            if(optDetails != null){
                partyDO.setRelationCd("SPSE");
            }
            partyEmploymentDO.setOccupationCd("I001");
            partyEmploymentDOLists.add(partyEmploymentDO);
            partyDO.setPartyEmploymentDOList(partyEmploymentDOLists);
            PartyQuestionDOList  partyQuestionDO=  new  PartyQuestionDOList();
            partyQuestionDO.setQuestionCd(Constant.BLANK);
            partyQuestionDO.setQuestionSetCd(Constant.BLANK);
            partyQuestionDO.setResponse(Constant.BLANK);
            partyQuestionDOLists.add(partyQuestionDO);
            partyDO.setPartyQuestionDOList(partyQuestionDOLists);
        }

        partyIdentityDO.setIdentityTypeCd(KYC_TYPES.PAN.name());
        partyIdentityDOList.add(partyIdentityDO);
        partyDO.setPartyIdentityDOList(partyIdentityDOList);
        //relationship with proposer and primary is self
        partyAddressDOLists =  populateAddressList(partyAddressDOLists,applicant.getAddress());
        partyContactDOLists = populateContactList(partyContactDOLists, applicant.getPhone());
        partyEmailDOLists  = populateEmailList(partyEmailDOLists, applicant.getEmail());

        partyDO.setPartyAddressDOList(partyAddressDOLists);
        partyDO.setPartyContactDOList(partyContactDOLists);
        partyDO.setPartyEmailDOList(partyEmailDOLists);
        String dt = applicant.getDateOfBirth();
        DateFormat dateFormat = new SimpleDateFormat(GngDateUtil.ddMMyyyy);
        Date date = dateFormat.parse(dt);
        DateFormat formatter = new SimpleDateFormat(GngDateUtil.ddMMyyyy_WITH_SLASH );
        String dateStr = formatter.format(date);
        if(optDetails != null){
            dateStr  = GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                    (GngDateUtil.YyyyMmddTHhMmSs, optDetails.getDateOfBirth()), GngDateUtil.ddMMyyyy_WITH_SLASH);
        }
        partyDO.setBirthDt(dateStr);
        partyDOList.add(partyDO);
        return partyDOList;
    }

    private List<PartyEmailDOList> populateEmailList(List<PartyEmailDOList> partyEmailDOLists, List<Email> emailList) {

        for(Email email :emailList){
            PartyEmailDOList partyEmailDO =  new PartyEmailDOList();
            if(StringUtils.equalsIgnoreCase(email.getEmailType(),GNGWorkflowConstant.PERSONAL_EMAIL.name())) {
                partyEmailDO.setEmailTypeCd(GngUtils.setValueForNull(email.getEmailType()));
                partyEmailDO.setEmailAddress(GngUtils.setValueForNull(email.getEmailAddress()));
            }if(StringUtils.equalsIgnoreCase(email.getEmailType(),GNGWorkflowConstant.OFFICE_EMAIL.name())) {
                partyEmailDO.setEmailTypeCd(GngUtils.setValueForNull(email.getEmailType()));
                partyEmailDO.setEmailAddress(GngUtils.setValueForNull(email.getEmailAddress()));
            }
            if(partyEmailDO.getEmailAddress() == null){
                if(partyEmailDO != null) {
                    partyEmailDO.setEmailAddress("BLANKMAIL@WE.COM");
                    partyEmailDO.setEmailTypeCd("PERSONAL");
                }
            }
            partyEmailDOLists.add(partyEmailDO);
        }

        return partyEmailDOLists;
    }

    private List<PartyContactDOList> populateContactList(List<PartyContactDOList> partyContactDOLists, List<Phone> phoneList) {
        for (Phone phone  : phoneList){
            if(StringUtils.isNotEmpty(phone.getPhoneNumber())){
                PartyContactDOList partyContactDO= new PartyContactDOList();
                if(StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_MOBILE.name(),phone.getPhoneType())){
                    partyContactDO.setContactTypeCd("MOBILE");
                    if(StringUtils.isNotEmpty(phone.getPhoneNumber())){
                        partyContactDO.setContactNum(Long.parseLong(phone.getPhoneNumber()));
                        partyContactDO.setStdCode("+91");
                    }
                }

                partyContactDOLists.add(partyContactDO);
            }
        }
        return partyContactDOLists;
    }

    private List<PartyAddressDOList> populateAddressList(List<PartyAddressDOList> partyAddressDOLists, List<CustomerAddress> addressList) {

        for(CustomerAddress customerAddress : addressList){
            PartyAddressDOList partyAddressDO = new PartyAddressDOList();
            if(StringUtils.equalsIgnoreCase(customerAddress.getAddressType(),GNGWorkflowConstant.PERMANENT.name())) {
                partyAddressDO.setAddressTypeCd("PERMANENT");
                partyAddressDO.setAddressLine1Lang1(customerAddress.getAddressLine1());
                partyAddressDO.setAddressLine2Lang1(customerAddress.getAddressLine2());
                partyAddressDO.setAreaCd(customerAddress.getCity());
                partyAddressDO.setStateCd(customerAddress.getState());
                partyAddressDO.setPinCode(Long.toString(customerAddress.getPin()));
                partyAddressDO.setCityCd(customerAddress.getCity());
                partyAddressDOLists.add(partyAddressDO);
            }
            if(StringUtils.equalsIgnoreCase(customerAddress.getAddressType(), GNGWorkflowConstant.RESIDENCE.name())) {
                partyAddressDO.setAddressTypeCd("COMMUNICATION");
                partyAddressDO.setAddressLine1Lang1(customerAddress.getAddressLine1());
                partyAddressDO.setAddressLine2Lang1(customerAddress.getAddressLine2());
                partyAddressDO.setCityCd(customerAddress.getCity());
                partyAddressDO.setAreaCd(customerAddress.getCity());
                partyAddressDO.setStateCd(customerAddress.getState());
                partyAddressDO.setPinCode(Long.toString(customerAddress.getPin()));
                partyAddressDOLists.add(partyAddressDO);
            }
        }
        return partyAddressDOLists;
    }

    public void calculateReligarePremium(String institutionId, String product, String changedAmount, String changedTenor, Applicant applicant,InsurancePolicy policy) {
        try{

            double loanAmount  = Double.parseDouble(changedAmount);
            Double premium = 0.0;

            DropdownMasterRequest dropdownMaster = new DropdownMasterRequest();
            dropdownMaster.setHeader(new Header());
            dropdownMaster.getHeader().setInstitutionId(institutionId);
            dropdownMaster.setDropDownType("Care_Protection_Master");
            dropdownMaster.setQueryType(EndPointReferrer.ONE);
            List<DropdownMaster> response = masterRepository.fetchDropDownMaster(dropdownMaster);
            List<HashMap<String, String>> dropDownValueDetailsList = response.get(0).getDropDownValueDetailsList();
            if(CollectionUtils.isNotEmpty(dropDownValueDetailsList)){
                for(Map<String,String> dropdown : dropDownValueDetailsList){
                    if(StringUtils.containsIgnoreCase(dropdown.get("sProduct"),product)){
                        String a = dropdown.get("FROM");
                        String b = dropdown.get("TO");
                        int i = Integer.parseInt(a);
                        int j = Integer.parseInt(b);
                        if(applicant.getAge() >= i && applicant.getAge() <=j){
                            logger.debug("{}",applicant.getAge());
                            logger.info("dropDown {}",dropdown);
                            premium = calculatePremium(dropdown, loanAmount, changedTenor);
                            break;
                        }
                    }
                }
            }
            policy.setPremium(GngCalculationUtils.roundValue(premium,0));
        }catch (Exception ex){
            logger.error("{} exception" , ExceptionUtils.getStackTrace(ex)); ;
        }
    }

    private Double calculatePremium( Map criteria,   double loanAmount,  String tenurer) {
        String tenor  = (String) criteria.get("Tenor");
        String calAmountKey = null ;
        tenor = tenor.substring(1, tenor.length()-1);
        String[] keyValuePairs = tenor.split(",");
        List<String> keyList = new ArrayList<>();
        Map<String,String> map = new HashMap<>();
        int tenure = Integer.parseInt(tenurer);
        for(String pair : keyValuePairs) {
            pair = pair.replace("{","").trim();
            pair = pair.replace("}","").trim();
            pair = pair.replace("\"","");
            String[] entry = pair.split(":");
            if(entry.length == 2){
                map.put(entry[0].trim(), entry[1].trim());
            }else{
                map.put(entry[0].trim(), "0");
            }
            keyList.add(entry[0].trim());
        }
        Double premium = 0.0 ;
        logger.info("keyList {}",keyList);
        logger.info("map {}",map);
        for(int i =0; i < keyList.size();i++){
            if(tenure <= Integer.parseInt(keyList.get(0))){
                calAmountKey = keyList.get(0);
                break;
            }else if( i > 0 && tenure >= Integer.parseInt(keyList.get(i-1)) && tenure <=Integer.parseInt(keyList.get(i))){
                calAmountKey = keyList.get(i);
                break;
            }
        }
        if(StringUtils.isEmpty(calAmountKey)){
            calAmountKey = Iterables.getLast(keyList, null);
        }
        premium = (loanAmount/100000)* Integer.parseInt(map.get(calAmountKey));
        return premium;
    }


    private Double calculatePremium(List<Map<Object,Object>> tenorObj, double loanAmount, int tenure, Map tenorCriteria) {
        Double d = null ;
        if(tenure < 13 ){
            String s = (String) tenorObj.get(4).get("12");
            d = (loanAmount/100000)* Integer.parseInt(s);
        }else if(tenure > 12 &&tenure < 25){
            String s = (String) tenorObj.get(0).get("24");
            d = (loanAmount/100000)* Integer.parseInt(s);
        }else if(tenure > 24 && tenure < 37){
             String s = (String) tenorObj.get(1).get("36");
             d = (loanAmount/100000)* Integer.parseInt(s);
        }else if(tenure > 36 &&tenure < 49){
            String s = (String) tenorObj.get(2).get("48");
            d = (loanAmount/100000)* Integer.parseInt(s);
        }else // allow same calculation for year greater  then  48 months
            if(tenure > 48 ){
            String s = (String) tenorObj.get(3).get("60");
            d = (loanAmount/100000)* Integer.parseInt(s);
        }

        if( d != null){
            BigDecimal bd = new BigDecimal(d).setScale(2 , RoundingMode.HALF_UP);
            d = bd.doubleValue();
        }else{
           d = 0.0;
        }
        return d;
    }
}
