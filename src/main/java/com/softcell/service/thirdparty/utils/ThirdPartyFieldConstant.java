package com.softcell.service.thirdparty.utils;

/**
 * Created by ssg0268 on 10/6/19.
 */
public class ThirdPartyFieldConstant {

    public static final String CO_ORIGINATION = "CO_ORIGINATION";
    public static final String APPLICANT_TYPE = "APPLICANT_TYPE";
    public static final String FREQUENCY = "FREQUENCY";
    public static final String EmploymentType = "EmploymentType";
    public static final String PSLSUBCLASSIFICATION = "PSLSUBCLASSIFICATION";
    public static final String ICICI_CORPORATE_API = "ICICI_CORPORATE_API";
    public static final String DEFAULT_VALUES = "DEFAULT_VALUES";
    public static  final String EDUCATION = "EDUCATION";
    public static final String RESIDENCE = "RESIDENCE";
    public static final String RESIDENCE_TYPE = "RESIDENCE_TYPE";
    public static final String ADDRESS_TYPE = "ADDRESS_TYPE";
    public static final String COMPANY_TYPE = "COMPANY_TYPE";
    public static final String EMPLOYMENT_TYPE = "EMPLOYMENT_TYPE";
    public static final String MARATIAL_STATUS = "MARATIAL_STATUS";
    public static final String GENDER = "GENDER";

    public static final String MIFIN = "MIFIN";
    public static final String MIFIN_CREATE_LOAN = "MIFIN.new-loan" ;
    public static final String MIFIN_SAVE_APPLICANT_DETAILS = "MIFIN.save-applicant-details" ;
    public static final String MIFIN_SAVE_APPLICANT_ADDRESS = "MIFIN.save-applicant-address" ;
    public static final String MIFIN_SAVE_FINANCIAL_INFO = "MIFIN.save-financial-info";
    public static final String MIFIN_PAN_SEARCH = "MIFIN.pan-card-search";
    public static final String MIFIN_PROCESS_DEDUPE = "MIFIN.process-dedupe";
    public static final String MIFIN_UPDATE_DEDUPE_STATUS="MIFIN.update-dedupe-status";
    public static final String MIFIN_DISBURSAL_MAKER = "MIFIN.save-disbursal-maker";
    public static final String MIFIN_DELETE_APPLICANT = "MIFIN.delete-applicant";
    public static final String CO_APPLICANT ="CO-APPLICANT";
    public static final String MIFIN_LOAN_DETAILS = "MIFIN.update-loan-details";
    public static final String MIFIN_SAVE_VERIFICATION = "MIFIN.save-verification";
    public static final String MIFIN_SAVE_CR_DECISION="MIFIN.save-cr-decision";
    public static final String MIFIN_SAVE_PERSONAL_INSURANCE = "MIFIN.save-personal-insurance";
    public static final String MIFIN_SEARCH_EXISTING_APPLICANT="MIFIN.search-existing-applicant";

}
