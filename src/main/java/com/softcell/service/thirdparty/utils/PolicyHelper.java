package com.softcell.service.thirdparty.utils;

import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.KYC_TYPES;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.insurance.InsuranceCompanyDetails;
import com.softcell.gonogo.model.configuration.insurance.InsuranceScheme;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.insurance.*;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by archana on 11/12/18.
 */
@Component
public class PolicyHelper {

    private static Logger logger = LoggerFactory.getLogger(PolicyHelper.class);

    @Autowired
    IciciHelper iciciHelper;

    @Autowired
    @Qualifier("religareEvaluator")
    ReligarePolicyEvaluator religarePolicyEvaluator;

    public static final String COLLECTION_NAME = "religareCallLog";

    public InsurancePolicy populateInsuranceData(InsurancePolicy insurancePolicy, String companyName) {

        if(StringUtils.equalsIgnoreCase(companyName,IciciHelper.ICICI_COMPANY_NAME)){
            insurancePolicy.setInsuranceId("0");
            insurancePolicy.setPremiumHit(true);
            insurancePolicy.setCompany(companyName);
            insurancePolicy.setName("DEATH_BENIFIT + CI");
            insurancePolicy.setScheme("DIAMOND");
            insurancePolicy.setProduct("LIFE");
            insurancePolicy.setMasterCode("SBFCPL03");
        }else if(StringUtils.equalsIgnoreCase(companyName,ReligareHelper.RELIGARE_LOAN_PROTECTION)){
            insurancePolicy.setInsuranceId("0");
            insurancePolicy.setPremiumHit(true);
            insurancePolicy.setCompany(companyName);
            insurancePolicy.setName("NO_COPAY");
            insurancePolicy.setScheme("CRITICAL ILLNESS");
            insurancePolicy.setProduct("OPTION1");
        }
        NomineeInfo nomineeInfo = new NomineeInfo();
        Name name = new Name();
        Appointee appointee = new Appointee();
        appointee.setName(name);
        nomineeInfo.setAppointee(appointee);
        nomineeInfo.setName(name);
        nomineeInfo.setDateOfBirth(Constant.BLANK);
        insurancePolicy.setNomineeInfo(nomineeInfo);
        insurancePolicy.setOptDetailsList(new ArrayList<>());
        return insurancePolicy;
    }



    // Insurance companies
    public enum InsuranceCompany {
        // TODO move product and template names here
        ICICI("ICICI PRUDENTIAL LIFE INSURANCE"), RELIGARE("CARE HEALTH INSURANCE") ,RELIGARE_LOAN_PROTECTION("CARE LOAN PROTECTION"),PNB_Metlife("PNB Metlife"), TATA_AIG("TATA AIG GENERAL INSURANCE COMPANY LIMITED") ;

        public String companyName;
        InsuranceCompany(String strName){
            companyName = strName;
        }
        public static boolean isInsuranceCompany(String companyName, InsuranceCompany company){
            if ( StringUtils.equalsIgnoreCase(companyName, company.companyName)){
                return true;
            }
            return false;
        }
    }
    // Template names
    private static final String ICICI_LAP_POLICY = "ICICI_LAP_POLICY";
    private static final String RELIGARE_LAP_POLICY = "RELIGARE_LAP_POLICY";

    // Insttid - map( product - map(insurance company, template name))
    private Map<String, Map<String, Map<String, String>>> TEMPLATE_CONFIGURATION_MAP ;
    //= new HashMap<String, HashMap<String, HashMap<TemplateName, String>>>();

    //method is to set Policy Benefit null(in case insurance company is ICICI) for Insurance Master
    public static final void setPolicyBenefitNull(com.softcell.gonogo.model.configuration.insurance.InsurancePolicy insurancePolicy ,
                                                  InsuranceCompanyDetails insuranceCompanyDetails, Map.Entry<String,List<InsuranceScheme>> benefit_SchemeMap ){
        if( InsuranceCompany.isInsuranceCompany(insuranceCompanyDetails.getCompanyName(), InsuranceCompany.ICICI) ) {
            insurancePolicy.setBenefit(null);
        }else {
            insurancePolicy.setBenefit(benefit_SchemeMap.getKey());
        }
    }

    public Object buildInsuranceRequest(InsuranceRequest insuranceRequest , Applicant applicant,
                                        Date disbursalDate, InsurancePolicy policy, WFJobCommDomain connection){
        Object request = null;
        if( InsuranceCompany.isInsuranceCompany(policy.getCompany(), InsuranceCompany.ICICI)){
            request = iciciHelper.buildIciciInsuranceRequest(insuranceRequest, applicant, disbursalDate, policy, connection);
        }
        return request;
    }
    }
