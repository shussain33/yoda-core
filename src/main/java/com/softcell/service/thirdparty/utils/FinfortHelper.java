package com.softcell.service.thirdparty.utils;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.KYC_TYPES;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.verification.ItrVerification;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.ssl2.finfort.model.*;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by archana on 15/11/18.
 */
public class FinfortHelper {

    // TODO get agency code
    public static final String AGENCY_CODE = "";
    // ThTODO : get later from configuration
    private static String ACCOUNT = "SBCF-LOSBranch";
    private static String LENDER_BRANCH = "SBCF-LOS";
    private static String LENDER_DIVISION = "SBCF-LOS";
    private static String PRODUCT_ORDERED_IND = "Self Employed Credit Evaluation";
    private static String PRODUCT_ORDERED_CORP = "Credit Assessment Information Report";
    private static String DATA_PULL_ACTION_CHOSEN_IND = "CSR Assisted";
    private static String DATA_PULL_ACTION_CHOSEN_CORP = "Lender Assisted";
    private static String LANGUAGE_HINDI = "Hindi";

    public static Object buildInitiateRequest(String refID, String institutionId, String serviceId,
                                            ApplicationRequest applicationRequest, Applicant applicant){
        if( GngUtils.isIndividual(applicant) ){
            return buildIndividualApplicantRequest(applicationRequest, applicant);
        } else {
            return buildCorporateApplicantRequest(applicationRequest, applicant);
        }
    }

    private static Object buildIndividualApplicantRequest(ApplicationRequest applicationRequest, Applicant applicant) {
        IndividualFinfortRequest request = new IndividualFinfortRequest();
        request.setAccount(ACCOUNT);
        request.setLenderBranch(LENDER_BRANCH);
        request.setLenderDivision(LENDER_DIVISION);
        request.setProductOrdered(PRODUCT_ORDERED_IND);
        request.setDataPullOptionChosen(DATA_PULL_ACTION_CHOSEN_IND);

        request.setChannelPartnerName(GngUtils.convertNameToFullName(applicationRequest.getAppMetaData().getDsaName()));
        request.setChannelPartnerEmailId(applicationRequest.getAppMetaData().getDsaEmailId().getEmailAddress());
        request.setChannelPartnerMobile(applicationRequest.getAppMetaData().getPhone().getPhoneNumber());

        request.setLenderReferenceNumber(applicationRequest.getRefID());
        request.setBorrowerFirstName(applicant.getApplicantName().getFirstName());
        request.setBorrowerMiddleName(applicant.getApplicantName().getMiddleName());
        request.setBorrowerLastName(applicant.getApplicantName().getLastName());
        // dd/mm/yyyy
        if( StringUtils.isNotEmpty(applicant.getDateOfBirth() ) ){
            Date parsed = GngDateUtil.getDateFromSpecificFormat(GngDateUtil.ddMMyyyy,
                    applicant.getDateOfBirth());
            String dob = GngDateUtil.changeDateFormat(parsed, GngDateUtil.ddMMyyyy_WITH_SLASH);
            request.setDateOfBirth(GngUtils.setValueForNull(dob));
        }
        // KYC -PAN
        for (Kyc kyc : applicant.getKyc()) {
           if(StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.PAN.name())) {
               request.setBorrowerPAN(kyc.getKycNumber());
               break;
           }
        }
        // Email
        if(CollectionUtils.isNotEmpty(applicant.getEmail())) {
            request.setBorrowerEmail(applicant.getEmail().get(0).getEmailAddress());
            if(applicant.getEmail().size() > 1)
                request.setBorrowerAdditionalEmail(applicant.getEmail().get(1).getEmailAddress());
        } else {
            request.setBorrowerEmail("dummy.mail.id@gmail.com");
        }
        boolean notProvided = false;
        if(CollectionUtils.isNotEmpty(applicant.getPhone())){
            for(Phone phone : applicant.getPhone()){
                if(!notProvided && StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_MOBILE.name(), phone.getPhoneType())){
                    request.setBorrowerMobile(phone.getPhoneNumber());
                    notProvided = true;
                } else{
                    request.setBorrowerAdditionalMobile(phone.getPhoneNumber());
                }
            }
        }
        if(!notProvided)
            request.setBorrowerMobile("9999999999");
        request.setBorrowerlanguage(LANGUAGE_HINDI);
        //request.setDinNumber("01234567");
        // Address
        if(CollectionUtils.isNotEmpty(applicant.getAddress())){
            for(CustomerAddress address : applicant.getAddress()){
                if(StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.name())){
                    request.setBorrowerStreet(GngUtils.setValueForNull(address.getStreet()));
                    request.setCity(address.getCity());
                    request.setBorrowerState(address.getState());
                    request.setCountry(address.getCountry());
                    request.setBorrowerPinCode(String.valueOf(address.getPin()));
                }
            }
        }
        //request.setAdditionalInfo("");
        //request.setChannelPartnerName("");
        //request.setChannelPartnerEmailId("");
        //request.setChannelPartnerMobile("");
        //request.setSalesExecutiveName(applicationRequest.getAppMetaData().getDsaName().getFirstName());
        //request.setSalesExecutiveEmailId(applicationRequest.getAppMetaData().getDsaEmailId().getEmailAddress());
        //request.setSalesExecutiveMobile(applicationRequest.getAppMetaData().getPhone().getPhoneNumber());
        return request;
    }

    private static Object buildCorporateApplicantRequest(ApplicationRequest applicationRequest, Applicant applicant) {
        CorporateFinfortRequest request = new CorporateFinfortRequest();
        request.setAccount(ACCOUNT);
        request.setLenderBranch(LENDER_BRANCH);
        request.setLenderDivision(LENDER_DIVISION);
        request.setProductOrdered(PRODUCT_ORDERED_CORP);
        request.setDataPullOptionChosen(DATA_PULL_ACTION_CHOSEN_CORP);

        request.setChannelPartnerName(GngUtils.convertNameToFullName(applicationRequest.getAppMetaData().getDsaName()));
        request.setChannelPartnerEmailId(applicationRequest.getAppMetaData().getDsaEmailId().getEmailAddress());
        request.setChannelPartnerMobile(applicationRequest.getAppMetaData().getPhone().getPhoneNumber());

        request.setLenderReferenceNumber(applicationRequest.getRefID());
        request.setPartnershipOrCompanyName(applicant.getApplicantName().getFirstName());
        if(CollectionUtils.isNotEmpty(applicant.getEmployment())){
            request.setDateOfBirth(applicant.getEmployment().get(0).getDateOfJoining());
        }
        for (Kyc kyc : applicant.getKyc()) {
            if(StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.PAN.name())) {
                request.setBorrowerPAN(kyc.getKycNumber());
                break;
            }
        }
        if(CollectionUtils.isNotEmpty(applicant.getEmail())) {
            request.setBorrowerEmail(applicant.getEmail().get(0).getEmailAddress());
            if(applicant.getEmail().size() > 1)
                request.setBorrowerAdditionalEmail(applicant.getEmail().get(1).getEmailAddress());
        } else {
            request.setBorrowerEmail("dummy.mail.id@gmail.com");
        }
        boolean noFound = false;
        if(CollectionUtils.isNotEmpty(applicant.getPhone())){
            for(Phone phone : applicant.getPhone()){
                if(!noFound && StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_MOBILE.name(), phone.getPhoneType())){
                    request.setBorrowerMobile(phone.getPhoneNumber());
                    noFound = true;
                } else{
                    request.setBorrowerAdditionalMobile(phone.getPhoneNumber());
                }
            }
        }
        if(!noFound)
            request.setBorrowerMobile("9999999999");
        request.setBorrowerlanguage(LANGUAGE_HINDI);
        //request.setDinNumber("01234567");
        if(CollectionUtils.isNotEmpty(applicant.getAddress())){
            for(CustomerAddress address : applicant.getAddress()){
                if(StringUtils.equalsIgnoreCase(address.getAddressType(), GNGWorkflowConstant.RESIDENCE.name())){
                    request.setBorrowerStreet(address.getStreet());
                    request.setCity(address.getCity());
                    request.setBorrowerState(address.getState());
                    request.setCountry(address.getCountry());
                    request.setBorrowerPinCode(String.valueOf(address.getPin()));
                }
            }
        }
        //request.setAdditionalInfo("");
        //request.setChannelPartnerName("");
        //request.setChannelPartnerEmailId("");
        //request.setChannelPartnerMobile("");
        //request.setSalesExecutiveName(applicationRequest.getAppMetaData().getDsaName().getFirstName());
        //request.setSalesExecutiveEmailId(applicationRequest.getAppMetaData().getDsaEmailId().getEmailAddress());
        //request.setSalesExecutiveMobile(applicationRequest.getAppMetaData().getPhone().getPhoneNumber());
        //request.setCin_LLPIN("");
        return request;
    }

    public static void populateFinancialData(String refId, String institutionId, String applicantId,
                                             ApplicationRequest applicationRequest, CamDetails camDetails,
                                             VerificationDetails verificationDetails, Report finfortReport){
        if(CollectionUtils.isNotEmpty(finfortReport.getDetailedFinancials() ) ){
            Applicant applicant = null;
            if( StringUtils.equalsIgnoreCase(applicationRequest.getRequest().getApplicant().getApplicantId(),
                    applicantId) ) {
                applicant = applicationRequest.getRequest().getApplicant();
            } else {
                if( CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())){
                    List<CoApplicant> coApps = applicationRequest.getRequest().getCoApplicant()
                                                .stream()
                            .filter(coapp -> StringUtils.equalsIgnoreCase(coapp.getApplicantId(), applicantId))
                            .collect(Collectors.toList());
                    if( CollectionUtils.isNotEmpty(coApps))
                        applicant = coApps.get(0);
                }
            }
            for(DetailedFinancials financials : finfortReport.getDetailedFinancials()){
                fillPnLAndBalanceSheet(applicantId, camDetails, financials);
                if( applicant != null)
                    fillItrData( refId,  institutionId, applicantId, applicant, verificationDetails, financials.getItr());
            }
        }

    }

    private static void fillPnLAndBalanceSheet(String applicantId, CamDetails camDetails,  DetailedFinancials financials) {
        Pnl pnl = financials.getPnl();

        if( StringUtils.isNotEmpty( financials.getYear()) && pnl != null ) {

            // format : 2018-10-27

            String year = GngDateUtil.getYear(financials.getYear(), GngDateUtil.yyyy_MM_dd);
            // create list only if it is not available;
            // It is available next time since this code is getting called in iteration
            List<PLAndBSAnalysis> plAndBSAnalysisList = new ArrayList<>();
            PLAndBSAnalysis plAndBSAnalysis  = new PLAndBSAnalysis();
            plAndBSAnalysis.setApplicantId(applicantId);
            plAndBSAnalysis.setProfitAndLoss(new ProfitAndLoss());
            plAndBSAnalysis.setBalanceSheet(new BalanceSheet());
            List<String> yearList = new ArrayList<>();
            yearList.add(year);
            plAndBSAnalysis.getProfitAndLoss().setYearsList(yearList);
            plAndBSAnalysis.getBalanceSheet().setYearsList(yearList);
            plAndBSAnalysisList.add(plAndBSAnalysis);

            if( CollectionUtils.isEmpty(camDetails.getPlAndBSAnalysis())) {
                camDetails.setPlAndBSAnalysis(plAndBSAnalysisList);
            } else {
                plAndBSAnalysisList = camDetails.getPlAndBSAnalysis().stream()
                                        .filter(plbs -> StringUtils.equalsIgnoreCase(applicantId, plbs.getApplicantId()))
                                        .collect(Collectors.toList());
                if( CollectionUtils.isNotEmpty(plAndBSAnalysisList) ){
                    plAndBSAnalysis = plAndBSAnalysisList.get(0);
                    if( plAndBSAnalysis.getBalanceSheet() == null ){
                        plAndBSAnalysis.setBalanceSheet(new BalanceSheet());
                        plAndBSAnalysis.getBalanceSheet().setYearsList(yearList);
                    }
                    if( plAndBSAnalysis.getProfitAndLoss() == null ){
                        plAndBSAnalysis.setProfitAndLoss(new ProfitAndLoss());
                        plAndBSAnalysis.getProfitAndLoss().setYearsList(yearList);
                    }
                } else {
                    camDetails.getPlAndBSAnalysis().add(plAndBSAnalysis);
                }
            }
            // Set Pnl fileds
            fillPnLData(year, plAndBSAnalysis.getProfitAndLoss(), financials);
            // Set Balancesheet fields
            fillBalanceSheetData(year, plAndBSAnalysis.getBalanceSheet(), financials);
        }

    }

    private static void fillItrData(String refId, String institutionId, String applicantId, Applicant applicant,
                                    VerificationDetails verificationDetails, Itr itr) {
        List<ItrVerification> itrVerificationList;
        ItrVerification itrVerification;
        if( verificationDetails == null ) {
            verificationDetails = new VerificationDetails();
            verificationDetails.setRefId(refId);
            verificationDetails.setInstitutionId(institutionId);
            itrVerificationList = new ArrayList<>();
            verificationDetails.setItrVerification(itrVerificationList);
            itrVerification = new ItrVerification();
            itrVerificationList.add(itrVerification);
        } else {
            itrVerificationList = verificationDetails.getItrVerification();
            if( CollectionUtils.isEmpty(itrVerificationList) ) {
                itrVerificationList = new ArrayList<>();
                verificationDetails.setItrVerification(itrVerificationList);
                itrVerification = new ItrVerification();
                itrVerificationList.add(itrVerification);
            } else { // itrVerificationList is not empty
                // todo get teh proper one
                String pan = getPan(applicant);
                // PAN is not mandatory for co-applicant ; in that case need to employ other field to get the correct element
                if( StringUtils.isNotEmpty(pan)){
                    List<ItrVerification> panItr = itrVerificationList.stream()
                                            .filter(itrVer -> StringUtils.equalsIgnoreCase(itrVer.getPanNumber(), pan))
                                            .collect(Collectors.toList());
                    if( CollectionUtils.isNotEmpty(panItr)){
                        itrVerification = panItr.get(0);
                    } else {
                        itrVerification = new ItrVerification();
                        itrVerificationList.add(itrVerification);
                    }
                } else { // Pan not present with applicant. Nee to check using name
                    List<ItrVerification> panItr = itrVerificationList.stream()
                            .filter(itrVer -> StringUtils.contains(itr.getApplicantName(),
                                    GngUtils.convertNameToFullName(itrVer.getApplicantName()) ) )
                            .collect(Collectors.toList());
                    if( CollectionUtils.isNotEmpty(panItr) ){
                        itrVerification = panItr.get(0);
                    } else {
                        itrVerification = new ItrVerification();
                        itrVerificationList.add(itrVerification);
                    }
                }
            }
        }
        itrVerification.setApplicantId(applicantId);
        // Applicant Name
        itrVerification.setApplicantName(GngUtils.seggragateName(itr.getApplicantName()));
        // Assessment Year
        itrVerification.setAssessmentYear(itr.getAssessmentYear());
        // Ack No
        itrVerification.setAcknowledgementNumber(itr.getAckNo());
        // todo ITR Filing Year
        //itrVerification.setItrFillingDate(itr.getItrFilingYear());
        // Location
        itrVerification.setLocation(itr.getLocation());
        // PAN No.
        itrVerification.setPanNumber(itr.getPan());
        // Agency
        itrVerification.setAgencyCode(itr.getAgency());
        // Tax Paid
        itrVerification.setTaxPaidAmount(NumberUtils.toDouble(itr.getTaxPaid()));
        // Total Income/GTI
        itrVerification.setTotalIncomeOrGTI(Double.toString(itr.getTotalIncomeGTI()));
        // Status
        itrVerification.setStatus(itr.getStatus());
        // Final Status
        itrVerification.setFinalStatus(itr.getFinalStatus());
        // Total Income/GTI Amount
        itrVerification.setTotalIncomeOrGTIAmount(itr.getTotalincomeGTIAmount());

        // Set status as VERIFIED
        itrVerification.setStatus(ThirdPartyVerification.Status.VERIFIED.name());
    }

    private static String getPan(Applicant applicant) {
        String pan = null;
        List<Kyc> kycDocs = applicant.getKyc();
        if( CollectionUtils.isNotEmpty(kycDocs)){
            List<Kyc> kycPan = kycDocs.stream().filter(kyc-> StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.PAN.name()))
                    .collect(Collectors.toList());
            if( CollectionUtils.isNotEmpty(kycPan)){
                pan = kycPan.get(0).getKycNumber();
            }
        }
        return pan;
    }

    private static void fillPnLData(String year, ProfitAndLoss profitAndLoss, DetailedFinancials financials) {

        Pnl pnl = financials.getPnl();
        LineItems lineItems = pnl.getLineItems() ;
        StatementFieldDetails statementFieldDetails = new StatementFieldDetails();

        double value;
        // Sales
        value = lineItems.getNetRevenue();
        if( profitAndLoss.getSales() == null ) profitAndLoss.setSales(statementFieldDetails);
        if( CollectionUtils.isEmpty(profitAndLoss.getSales().getYears()) ) {
            statementFieldDetails.setYears(new ArrayList<>());
        }
        setStatementFieldDetails(year, statementFieldDetails, value);


        // COGS

        value = lineItems.getTotalCostOfMaterialsConsumed();
        if( profitAndLoss.getCostOfGoodsSold() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setCostOfGoodsSold(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Gross Profits (GP)
        value = lineItems.getGrossProfit();
        if( profitAndLoss.getGrossProfits() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setGrossProfits(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Admin Expenses
        value = lineItems.getAdminExpense();
        if( profitAndLoss.getAdminExpences() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setAdminExpences(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Selling & Mktg Expenses
        value = lineItems.getSellingMktgExpenses();
        if( profitAndLoss.getSellingAndMktgExpenses() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setSellingAndMktgExpenses(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Salary/ Employee benefit Expenses

        value = lineItems.getTotalEmployeeBenefitExpense();
        if( profitAndLoss.getSalaryOrEmployeeBenifitExpenses() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setSalaryOrEmployeeBenifitExpenses(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Total Operating Expense
        value = lineItems.getTotalOperatingExpense();
        if( profitAndLoss.getTotalOperatingExpense() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setTotalOperatingExpense(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Add back Partner's Salary
        //Add back Directors Salary
        //Add Partner's Interest

        //Operating Margins
        value = lineItems.getOperatingProfit();
        if( profitAndLoss.getOperatingMargin() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setOperatingMargin(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Other Income (non incidental to Business)

        value = lineItems.getOtherIncome();
        if( profitAndLoss.getOtherIncomeNonIncidentalToBusiness() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setOtherIncomeNonIncidentalToBusiness(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Profit before Depreciation, Interest & Tax (PBDIT)
        value = lineItems.getProfitBeforeDepreciationInterestTax();
        if( profitAndLoss.getPBDIT() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPBDIT(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //PBDIT (excluding Partner's salary & interest)

        value = lineItems.getPbditExcludingPartnerSalaryInterest();
        if( profitAndLoss.getPBDITexcludingPartnerSalaryAndInterest() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPBDITexcludingPartnerSalaryAndInterest(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Interest to FI and Banks

        value = lineItems.getInterestToFiAndBank();
        if( profitAndLoss.getInterestToFIandBanks() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setInterestToFIandBanks(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Interest to Private Parties/

        value = lineItems.getInterestToPrivateParties();
        if( profitAndLoss.getInterestToPvtParties() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setInterestToPvtParties(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Total Interest Paid

        value = lineItems.getInterest();
        if( profitAndLoss.getTotalInterestPaid() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setTotalInterestPaid(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Profit Before Depreciation & Tax (PBDT)

        value = lineItems.getProfitBeforeDepreciationInterestTax();
        if( profitAndLoss.getPBDT() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPBDT(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Depreciation

        value = lineItems.getDepreciation();
        if( profitAndLoss.getDepreciation() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setDepreciation(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Profit before Tax (PBT)

        value = lineItems.getProfitBeforeTax();
        if( profitAndLoss.getPBT() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPBT(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //         Tax paid

        value = lineItems.getIncomeTax();
        if( profitAndLoss.getTaxPaid() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setTaxPaid(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        // Profit After Tax (PAT)

        value = lineItems.getProfitAfterTaxPAT();
        if( profitAndLoss.getPAT() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPAT(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //Post tax Cash profits + sal. + partners int.

        value = lineItems.getPostTaxCashProfitsSalPartnerInterest();
        if( profitAndLoss.getPostTaxCashProfitsWithSalAndPartnerInterest() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setPostTaxCashProfitsWithSalAndPartnerInterest(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);

        //       Net Profit Ratio
        value = lineItems.getNetProfitRatio();
        if( profitAndLoss.getNetProfitRatio() == null ) {
            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            profitAndLoss.setNetProfitRatio(statementFieldDetails);
        }
        setStatementFieldDetails(year,statementFieldDetails,value);


    }

    private static void setStatementFieldDetails(String year, StatementFieldDetails statementFieldDetails, double value) {
        YearValue yearValue =  new YearValue();
        yearValue.setYear(year);
        yearValue.setValue(value);
        if( ! statementFieldDetails.getYears().contains(yearValue)) {
            statementFieldDetails.getYears().add(yearValue);
        } else {
            List<YearValue> yearValueList = statementFieldDetails.getYears();
            yearValueList.get(yearValueList.indexOf(yearValue)).setValue(value);
        }
    }


    private static void fillBalanceSheetData(String year, BalanceSheet balanceSheet, DetailedFinancials financials) {

        Liabilities liabilities = financials.getBs().getLiabilities();
        Assets assets = financials.getBs().getAssets();
        SubTotals subTotals = financials.getBs().getSubTotals();
        StatementFieldDetails statementFieldDetails = new StatementFieldDetails();
        double value;


        // Share Capital
        value = liabilities.getShareCapital();
        if (balanceSheet.getShareCapital() == null) {
            balanceSheet.setShareCapital(statementFieldDetails);
            statementFieldDetails.setYears(new ArrayList<>());
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Reserves and Surplus

        value = liabilities.getReservesAndSurplus();

        if (balanceSheet.getReservesAndSurplus() == null) {

            statementFieldDetails = new StatementFieldDetails();
            balanceSheet.setReservesAndSurplus(statementFieldDetails);
            statementFieldDetails.setYears(new ArrayList<>());
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Advances to Group and Co/Friends

        value = liabilities.getAdvancesToGroupCofriends();

        if (balanceSheet.getAdvancesToGroup() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setAdvancesToGroup(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

/*        //Net Fixed Assets

        value = liabilities.getFromFamilyAndFriendsNonInterestBearing();

        if (balanceSheet.getNetFixedAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setNetFixedAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);*/

        //Misc. Exp. Not written off

        value = liabilities.getDMiscExpNotWrittenOff();

        if (balanceSheet.getMiscExpNotWrittenOff() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setMiscExpNotWrittenOff(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Secured Debt (Term Loans)

        value = liabilities.getSecuredDebtTermLoans();

        if (balanceSheet.getSecuredDebt() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setSecuredDebt(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Cash Credit limit (CC Limit)

        value = liabilities.getCashCredit();

        if (balanceSheet.getCcLimit() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setCcLimit(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Unsecured Debt - Personal Loans

        value = liabilities.getDUnsecuredDebtPersonalLoans();

        if (balanceSheet.getUnsecuredDebtPersonal() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setUnsecuredDebtPersonal(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Unsecured Debt - others (including intt bearing loans from family & friends)s

        value = liabilities.getUnsecuredDebtOthersIncludingInttBearingLoansFromFamilyFriends();

        if (balanceSheet.getUnsecuredDebtOthers() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setUnsecuredDebtOthers(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Creditors

        value = liabilities.getTradePayables();

        if (balanceSheet.getCreditors() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setCreditors(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Other Current Liabilities

        value = liabilities.getOtherCurrentLiabilities();

        if (balanceSheet.getOtherCurrentLiabilities() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setOtherCurrentLiabilities(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Deferred Tax Assets / (Liab.)

        value = liabilities.getDeferredTaxLiabilitiesNet();

        if (balanceSheet.getDeferredTaxAssetsLiab() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDeferredTaxAssetsLiab(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);


        /*
            For Assets
         */

        // Gross Fixed Assets
        value = assets.getGrossFixedAssets();

        if (balanceSheet.getGrossFixedAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setGrossFixedAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Less : Depreciation
        value = assets.getDepreciation();

        if (balanceSheet.getDepreciation() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDepreciation(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Invesments
        value = assets.getInvestments();

        if (balanceSheet.getInvestments() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setInvestments(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Less : Depreciation
        value = assets.getDepreciation();

        if (balanceSheet.getDepreciation() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDepreciation(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Less : Current Assets
        value = assets.getCurrentAssets();

        if (balanceSheet.getCurrentAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setCurrentAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Inventories
        value = assets.getInventories();

        if (balanceSheet.getInventory() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setInventory(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Debtors
        value = assets.getTradeReceivablesDebtors();

        if (balanceSheet.getDebtors() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDebtors(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Exceeding six months
        value = assets.getExceedingSixMonths();

        if (balanceSheet.getDebtorsExceedsSixMonths() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDebtorsExceedsSixMonths(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Less than six months later
        value = assets.getLessThanSixMonthsLater();

        if (balanceSheet.getDebtorsLessthanSixMonths() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setDebtorsLessthanSixMonths(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);


        // Loans and Advances
        value = assets.getLoansAndAdvances();

        if (balanceSheet.getLoansAndAdvancess() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setLoansAndAdvancess(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Other genuine advances
        value = assets.getOtherGenuineAdvances();

        if (balanceSheet.getOtherGenuineAdvances() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setOtherGenuineAdvances(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Cash and Bank
        value = assets.getCashAndBankBalances();

        if (balanceSheet.getCashAndBank() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setCashAndBank(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Total Current Assets
        value = assets.getTotalCurrentAssets();

        if (balanceSheet.getTotalCurrentAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalCurrentAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Total Assets
        value = assets.getGivenAssetsTotal();

        if (balanceSheet.getTotalAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Provisions
        value = assets.getProvisions();

        if (balanceSheet.getProvisions() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setProvisions(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Residual Current Assets
        value = assets.getResidualCurrentAssets();

        if (balanceSheet.getResidualCurrentAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setResidualCurrentAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Any working capital limits which is not part of secured loans
        value = assets.getAnyWorkingCapitalLimtsWhichIsNotPartOfSecuredLoans();

        if (balanceSheet.getAnyWorkingCapitalLimits() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setAnyWorkingCapitalLimits(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        //Net Working Capital
        value = assets.getNetWorkingCapital();

        if (balanceSheet.getNetWorkingCapital() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setNetWorkingCapital(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Total Application of Funds
        value = assets.getTotalApplicationOfFunds();

        if (balanceSheet.getTotalApplicationOfFunds() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalApplicationOfFunds(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        /*
            Subtotals
         */

        // Total Equity Fund
        value = subTotals.getTotalEquity();

        if (balanceSheet.getTotalEquityFund() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalEquityFund(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Total Outside Debts
        value = subTotals.getTotalDebt();

        if (balanceSheet.getTotalOutsideDebts() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalOutsideDebts(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Total fund in the business
        value = subTotals.getTotalFundInTheBusiness();

        if (balanceSheet.getTotalFundInBusiness() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setTotalFundInBusiness(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Net Fixed Assets
        value = subTotals.getNetFixedAssets();

        if (balanceSheet.getNetFixedAssets() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setNetFixedAssets(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

        // Current Liabilities
        value = subTotals.getTotalCurrentLiabilities();

        if (balanceSheet.getCurrentLiabilities() == null) {

            statementFieldDetails = new StatementFieldDetails();
            statementFieldDetails.setYears(new ArrayList<>());
            balanceSheet.setCurrentLiabilities(statementFieldDetails);
        }
        setStatementFieldDetails(year, statementFieldDetails, value);

    }

    public FetchReportRequest buildFetchRequest(String ackId) {
        FetchReportRequest reportRequest = new FetchReportRequest();
        reportRequest.setAckId(ackId);
        return reportRequest;
    }
}
