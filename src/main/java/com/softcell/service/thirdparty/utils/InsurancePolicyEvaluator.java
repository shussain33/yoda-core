package com.softcell.service.thirdparty.utils;

import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.request.ApplicationRequest;

/**
 * Created by archana on 11/12/18.
 *
 * Offers methods to evaluates insurance related calculated fields based on the
 * information provided.
 */
public interface InsurancePolicyEvaluator {

    /* Statuses for user policy. */
    public static final String STATUS_REGISTERED ="REGISTERED";
    public static final String STATUS_INPROCESS ="INPROCESS";
    public static final String STATUS_DISPATCHED ="DISPATCHED";
    public static final String STATUS_FAILED = "FAILED";


    void setTemplateFile(String templateFile);

    String getTemplateFile();

    void evaluateBenefit(ApplicationRequest applicationRequest, InsurancePolicy policy);

    void evaluate(ApplicationRequest applicationRequest, Applicant applicant, InsurancePolicy policy) throws Exception;
}
