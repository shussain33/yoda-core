package com.softcell.service.thirdparty.utils;

import com.softcell.constants.*;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.insurance.icici.*;
import com.softcell.gonogo.model.insurance.icici.Product;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceData;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngNumUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by archana on 20/12/18.
 */
@Component
public class IciciHelper {

    public static final String COLLECTION_NAME = "iciciCallLog";
    public static final int MAX_HIT = 3;

    /*
        *  Response codes
            E00     If the data validation is successful then, E00 will be returned followed with response message according to the partner type (i.e. online/offline partner).
            E99     If any technical error is occurs in the application, response code of E99 will be returned.
            E01     If there is an issue in the encrypted JSON, E01 will be returned with the error message.
                    In case of landing page is Pre-Application Form, if any lead related data sent, contains invalid data, E01 with corresponding error message will be sent as response.
                    In case of landing page as Application form when the product information sent is invalid or when Login Assistant is down, the corresponding EBI message will be returned with the error code E01.
            E01, E02, E03, E99
                    genericErrorCodeList
            E04     preEbiValidationErrorCode
            E05     SDEValidationErrorCode
            E06     ebiValidationErrorCode
            E07     appFormValidationErrorCode
            E08     paymentValidationErrorCode
        */
    public static String SUCCESS_CODE = "E00";
    public static String ICICI_COMPANY_NAME = "ICICI PRUDENTIAL LIFE INSURANCE";
    public static String ICICI_LOMBARD_INSURANCE = "ICICI Lombard Insurance";

    /* Enums for ICICI MASTERS */
    public enum RequestField {
        ADVISOR_CODE("advisorCode"),
        SOURCE( "source"),
        SOURCE_KEY("sourceKey"),
        URL("URL");

        private final String fieldName;

        RequestField(String fieldName){
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return fieldName;
        }
    }

    /* Enums for ICICI MASTERS */
    public enum BenefitOption {
        GOLD("6"),
        SILVER( "0"),
        DIAMOND("5"),
        PLATINUM("3");

        private final String fieldName;

        BenefitOption(String fieldName){
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return fieldName;
        }

    }

    public static String getFieldName(String name){
        for(BenefitOption option : Arrays.asList(BenefitOption.values())){
            if(StringUtils.equalsIgnoreCase(option.name(), name)){
                return option.getFieldName();
            }
        }
        return null;
    }

    public enum Occupation {
        PROF, // Professional
        SPVT, // Salaried
        BSEM  // Self Employed
        ;
        /*
            Others	SPRO
            Student	STDN
            Housewife	    HSWF
            Agriculturist	AGRI
            Retired	        RETD
            ICICI Group Employee	IPRU
        */
    }

    public enum Gender {
        MALE("Male"), FEMALE("Female");

        private String genderValue;

        Gender(String gender){
            genderValue = gender;
        }
        public String getGenderValue(){
            return genderValue;
        }
    }

    public enum IncomeSource {
        SALARY("Salary"), BUSINESS("Business Income"),
        ASSETS_SALE("Sale of Assets"), INHERITENCE("Inheritence"), OTHER("Others");

        private String incomeSource;
        IncomeSource (String source){
            incomeSource = source;
        }

        public String incomeSource(){
            return incomeSource;
        }

    }
    private static final Map<String, String> STATE  = new HashMap<>();
    static {
        STATE.put("ANDAMAN AND NICOBAR", "1");
        STATE.put("GOA", "10");
        STATE.put("GUJARAT", "11");
        STATE.put("HARAYANA", "12");
        STATE.put("ANDHRA PRADESH", "2");
        STATE.put("HIMACHAL PRADESH", "21");
        STATE.put("JAMMU AND KASHMIR", "22");
        STATE.put("KARNATAKA", "23");
        STATE.put("KERALA", "24");
        STATE.put("LAKSHADWEEP", "25");
        STATE.put("MADHYA PRADESH", "26");
        STATE.put("MAHARASHTRA", "27");
        STATE.put("MANIPUR", "28");
        STATE.put("MEGHALAYA", "29");
        STATE.put("ARUNACHAL PRADESH", "3");
        STATE.put("MIZORAM", "30");
        STATE.put("NAGALAND", "31");
        STATE.put("ORISSA", "32");
        STATE.put("PONDICHERRY", "33");
        STATE.put("PUNJAB", "34");
        STATE.put("RAJASTHAN", "35");
        STATE.put("SIKKIM", "36");
        STATE.put("TAMIL NADU", "37");
        STATE.put("TRIPURA", "38");
        STATE.put("UTTAR PRADESH", "39");
        STATE.put("ASSAM", "4");
        STATE.put("WEST BENGAL", "40");
        STATE.put("JHARKHAND", "42");
        STATE.put("UTTARANCHAL", "43");
        STATE.put("CHHATTISGARH", "44");
        STATE.put("TELANGANA", "45");
        STATE.put("BIHAR", "5");
        STATE.put("CHANDIGARH", "6");
        STATE.put("DADRA AND NAGAR", "7");
        STATE.put("DAMAN AND DIU", "8");
        STATE.put("DELHI", "9");
    }
      /*-------- END ICICI Masters ------------*/

    public IciciRequest buildIciciInsuranceRequest(InsuranceRequest insuranceRequest, Applicant applicant,
                                                   Date disbarsalDate, InsurancePolicy policy, WFJobCommDomain connection){
        IciciRequest iciciRequest = new IciciRequest();
        List<HeaderKey> headerKeyList = connection.getKeys();
        Map<String, String> requestField= new HashMap<>();
        if(CollectionUtils.isNotEmpty(headerKeyList)){
            for(HeaderKey header : headerKeyList){
                requestField.put(header.getKeyName(),header.getKeyValue());
            }
        }
        HeaderKey tempKey = new HeaderKey();
        iciciRequest.advisorCode = requestField.get(RequestField.ADVISOR_CODE.fieldName);
        iciciRequest.source = requestField.get(RequestField.SOURCE.fieldName);
        iciciRequest.sourceKey = requestField.get(RequestField.SOURCE_KEY.fieldName);

        iciciRequest.salesDataReqd = GNGWorkflowConstant.YES.toFaceValue();
        iciciRequest.dependentFlag = GNGWorkflowConstant.NO.toFaceValue();
        // Set long time as transaction id
       iciciRequest.sourceTransactionId = insuranceRequest.getRefId() + "-" + applicant.getApplicantId();
        iciciRequest.sourceOfFund = getSourceOfFund(applicant);
        iciciRequest.sourceOfFundDesc = Constant.BLANK;
        iciciRequest.clientId = Constant.BLANK;
        iciciRequest.uidId = Constant.BLANK;
        iciciRequest.proposerInfos = getProposerInfo(insuranceRequest.getInsuranceData(),applicant, disbarsalDate);
        iciciRequest.buyersPinCode = iciciRequest.proposerInfos.comunctnAddress.pincode;
        iciciRequest.sellersPinCode = iciciRequest.buyersPinCode;
        //iciciRequest.healthDetails = getHealthDetails(applicationRequest, applicant);
        iciciRequest.productSelection = getProductSelection(insuranceRequest.getInsuranceData(), applicant, policy);
        iciciRequest.nomineeInfos = getNomineeInfo(policy.getNomineeInfo());
        iciciRequest.advisorSalesDetails = getAdvisorSalesDetails();
        iciciRequest.otpDetails = getOtpDetails();
        iciciRequest.paymentData = getPaymentData(policy);
        return iciciRequest;
    }

    private PaymentData getPaymentData(InsurancePolicy policy) {
        PaymentData paymentData = new PaymentData();
        paymentData.payAmount = GngNumUtil.doubleConversion(policy.getPremium(), "0");
        paymentData.payFinalPremiumAmnt = paymentData.payAmount;
        return paymentData;
    }

    private String getSourceOfFund(Applicant applicant) {
        String incomeSource = IncomeSource.OTHER.incomeSource();
        Employment employment = null;
        if(CollectionUtils.isNotEmpty(applicant.getEmployment())) {
            List<Employment> employmentList = applicant.getEmployment();
            employment = employmentList.get(0);
        }
        if( employment != null ) {
            if (StringUtils.equalsIgnoreCase(EmploymentType.SALARIED, employment.getEmploymentType())) {
                incomeSource = IncomeSource.SALARY.incomeSource();
            } else if (StringUtils.equalsIgnoreCase(EmploymentType.SENP, employment.getEmploymentType())
                    || StringUtils.equalsIgnoreCase(EmploymentType.SEP, employment.getEmploymentType())) {
                incomeSource = IncomeSource.BUSINESS.incomeSource();
            }
        }
        return incomeSource;
    }

    private ProposerInfos getProposerInfo(InsuranceData insuranceData, Applicant applicant, Date disbursalDate) {
        ProposerInfos proposer = new ProposerInfos();
        proposer.frstNm = applicant.getApplicantName().getFirstName().trim().toUpperCase();
        proposer.lstNm = applicant.getApplicantName().getLastName().trim().toUpperCase();
        // dd-MON-yyyy = 16-Dec-1991
        proposer.dob = GngDateUtil.changeDateFormat( GngDateUtil.getDate(applicant.getDateOfBirth()).toDate(),GngDateUtil.ddMMMyyyy_WITH_HYPHEN)  ;

        // Gender get from master
        proposer.gender = getGender(applicant.getGender());

        // TODO Transgender
        proposer.mobNo = Constant.BLANK;
        Phone phone = applicant.getPhone().stream()
                            .filter(ph -> StringUtils.equalsIgnoreCase(ph.getPhoneType(),
                                        GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue()))
                            .findFirst().orElse(null);
        if(phone != null) {
            proposer.mobNo = phone.getPhoneNumber();
        }
        // Communication address
        com.softcell.gonogo.model.address.Address address = applicant.getAddress().stream()
                            .filter(addr -> StringUtils.equalsIgnoreCase(addr.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                            .findFirst().orElse(null);
        if( address != null ){
            proposer.comunctnAddress = getAddress(address);
        }
        // Permanent address
        address = applicant.getAddress().stream()
                .filter(addr -> StringUtils.equalsIgnoreCase(addr.getAddressType(), GNGWorkflowConstant.PERMANENT.toFaceValue()))
                .findFirst().orElse(null);
        if( address != null ){
            proposer.prmntAddress = getAddress(address);
        }
        // email
        Email personalEmail = applicant.getEmail().stream()
                .filter(email -> StringUtils.equalsIgnoreCase(email.getEmailType(), GNGWorkflowConstant.PERSONAL.toFaceValue()))
                .findFirst().orElse(null);
        if( personalEmail != null)
            proposer.email = personalEmail.getEmailAddress();
        // occupation
        proposer.occ = getOccupation(applicant);
        // loan acct number
        proposer.lan = insuranceData.getRefId();
        // loan status = Main Applicant
        proposer.loanStatus = "Main Applicant";
        // pan
        Kyc pan = applicant.getKyc().stream()
                .filter(kyc -> StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.PAN.name()))
                .findFirst().orElse(null);
        if( pan != null)
            proposer.panNo = pan.getKycNumber();
        // aadhar
        Kyc aadhar = applicant.getKyc().stream()
                .filter(kyc -> StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.AADHAR.name()))
                .findFirst().orElse(null);
        if( aadhar != null ) {
            // adhar option selected
            proposer.aadhaarOptionSelected = "Aadhaar No";
            // adharno
            proposer.aadharCardNo = aadhar.getKycNumber();
        }

        // todo as Akshay dateOfCommencementOfLoan = MM/DD/YYYY | 01/24/2019
        proposer.dateOfCommencementOfLoan = GngDateUtil.changeDateFormat( disbursalDate,GngDateUtil.MMddyyyy_WITH_SLASH)  ;
        return proposer;
    }

    private String getGender(String gender) {
        String masterGender = Gender.MALE.getGenderValue();
        if( StringUtils.equalsIgnoreCase(gender, GNGWorkflowConstant.MALE.toFaceValue()) ) {
            masterGender =  Gender.MALE.getGenderValue();
        } else if( StringUtils.equalsIgnoreCase(gender, GNGWorkflowConstant.FEMALE.toFaceValue()) ){
            masterGender = Gender.FEMALE.getGenderValue();
        }
        return masterGender;
    }

    private String getOccupation(Applicant applicant) {
        List<Employment> employmentList = applicant.getEmployment();
        Employment employment =
        CollectionUtils.isNotEmpty(employmentList) ?  employmentList.get(0) : null;
        String occupation = "";
        if( employment != null ) {
            String employmentType = employment.getEmploymentType();
            if (StringUtils.equalsIgnoreCase(EmploymentType.SALARIED, employmentType)) {
                occupation = Occupation.SPVT.name();
            } else if (StringUtils.equalsIgnoreCase(EmploymentType.SEP, employmentType)) {
                occupation = Occupation.PROF.name();
            } else if (StringUtils.equalsIgnoreCase(EmploymentType.SENP, employmentType)) {
                occupation = Occupation.BSEM.name();
            }
        }
        return occupation;
    }

    private Address getAddress(com.softcell.gonogo.model.address.Address address) {
        Address address1 = new Address();
        address1.pincode = Long.toString(address.getPin());
        address1.state =  STATE.get(address.getState());
        address1.line1 = address.getAddressLine1();
        address1.city = address.getCity();
        address1.country = address.getCountry();
        return address1;
    }

    /* Hard coding the values */
    private OtpDetails getOtpDetails() {
        OtpDetails otpDetails = new OtpDetails();
        otpDetails.otpReqCreatedTime = "28-02-2017 05:59:05";
        otpDetails.otpReqUpdatedTime = "28-02-2017 06:00:38";
        otpDetails.otpReqOtpNo = "4515";
        otpDetails.otpReqStatus = "Verified";
        otpDetails.otpReqExpiryTimeInMin = "30";
        return otpDetails;
    }

    /* Hardcoding all the values */
    private AdvisorSalesDetails getAdvisorSalesDetails() {
        AdvisorSalesDetails advisorSalesDetails = new AdvisorSalesDetails();
        advisorSalesDetails.channelType = "BR";
        advisorSalesDetails.cusBankAccNo = "";
        advisorSalesDetails.bankName = "SBFC";
        advisorSalesDetails.needRiskProfile = "";
        advisorSalesDetails.csrLimCode = "5032212";
        advisorSalesDetails.cafosCode = "999999999";
        advisorSalesDetails.oppId = "";
        advisorSalesDetails.fscCode = "01286361";
        advisorSalesDetails.spCode = "";
        advisorSalesDetails.bankBrnch = "SBFC";
        advisorSalesDetails.source = "SBFC";
        advisorSalesDetails.lanNo = "";
        advisorSalesDetails.selectedTab = "broker_ci";
        advisorSalesDetails.subChannel = "";
        return  advisorSalesDetails;
    }

    /* Hardcoding the values */
    private NomineeInfos getNomineeInfo(com.softcell.gonogo.model.core.NomineeInfo nomineeInfo) {
        NomineeInfos nomineeInfos = new NomineeInfos();

        if( nomineeInfo.getAppointee() != null) {
            ApnteDtls apnteDtls = new ApnteDtls();
            apnteDtls.frstNm = nomineeInfo.getAppointee().getName().getFirstName();
            apnteDtls.relationship = nomineeInfo.getAppointee().getRelationship();
            apnteDtls.gender = nomineeInfo.getAppointee().getGender();
            // dd/mm/yyyy
            try {
                if( StringUtils.isNotEmpty(nomineeInfo.getAppointee().getDateOfBirth()))
                    apnteDtls.dob = GngDateUtil.changeDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(nomineeInfo.getAppointee().getDateOfBirth()),
                        GngDateUtil.ddMMyyyy_WITH_SLASH);
            } catch (Exception e){
                e.printStackTrace();
            }
            apnteDtls.lstNm = nomineeInfo.getAppointee().getName().getLastName();
            nomineeInfos.apnteDtls = apnteDtls;
        }
        // TODO : check w/ site whether empty appointee is ok.
        nomineeInfos.frstNm = nomineeInfo.getName().getFirstName();
        nomineeInfos.relationship = nomineeInfo.getRelationship();
        nomineeInfos.gender = getGender(nomineeInfo.getGender());
        // dd/mm/yyyy
        try {
            nomineeInfos.dob = GngDateUtil.changeDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(nomineeInfo.getDateOfBirth()),
                    GngDateUtil.ddMMyyyy_WITH_SLASH);
        } catch (Exception e){
            e.printStackTrace();
        }
        nomineeInfos.lstNm = nomineeInfo.getName().getLastName();

        return nomineeInfos;
    }

    private ProductSelection getProductSelection(InsuranceData insuranceData, Applicant applicant, InsurancePolicy policy) {
       /* Application application = applicationRequest.getRequest().getApplication();*/
        ProductSelection productSelection = new ProductSelection();
        if(getFieldName(policy.getScheme()) != null) {
            productSelection.benefitOption = BenefitOption.valueOf(policy.getScheme()).getFieldName();
            productSelection.masterCode = policy.getMasterCode();
        } else {
            productSelection.benefitOption = "0";
            productSelection.masterCode = "SBFCPL03";
        }
        productSelection.premiumPayingFrequency = "Yearly" ;
        // application.getLoanTenor() in months
        productSelection.policyTerm = policy.getTenorRequested(); //String.valueOf(Integer.parseInt(application.getTenorRequested())/12);
        productSelection.premiumPayingTerm = "1"; //String.valueOf(Integer.parseInt(application.getTenorRequested())/12);//productSelection.policyTerm;
        productSelection.modalPremium = ""; // todo from webservice PremiumSummary.ModalPremium
        // from calculator cell B27
        productSelection.sumAssured = (policy.getSumAssured() == null || policy.getSumAssured() == 0 )? policy.getProposedAmount(): policy.getSumAssured().toString();
        // from calculator cell B38 = policy .premium
        productSelection.totalPremium = (policy.getPremium() == null) ? "0" : Integer.valueOf(policy.getPremium().intValue()).toString();
        productSelection.productName = "Group Loan Secure";
        productSelection.productId = "GL1"; // Default
        productSelection.lifeCoverOption = "Life Plus"; // Default
        productSelection.premiumpaymentoption = "Single Pay";
        productSelection.coverageOption = "Reduced"; //Default value if Group Loan Secure
        //Todo ask confirmation to map valu earlier value
        if(policy.isException()){
            productSelection.loanAmount = GngNumUtil.doubleConversion(Double.parseDouble(insuranceData.getChangedLoanAmount()), "0");
            productSelection.loanTenure =  insuranceData.getChangedTenorAmount();
        }else{
            productSelection.loanAmount = GngNumUtil.doubleConversion(Double.parseDouble(policy.getProposedAmount()), "0");
            productSelection.loanTenure =  policy.getTenorRequested();
        }
        // from calculator cell B22
        productSelection.cIBenefit = (policy.getCiBenefit() == null) ? "0" : policy.getCiBenefit().toString();
        productSelection.aDHB = ""; // todo will received from quote API PremiumSummary.ADHB
        return productSelection;
    }

    private List<HealthDetail> getHealthDetails(ApplicationRequest applicationRequest, Applicant applicant) {
        List<HealthDetail> healthDetailList = new ArrayList<>();
        HealthDetail healthDetail = new HealthDetail();
        healthDetail.code = "HQ01";
        healthDetail.answer1 = "5";     // int feet
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ02";
        healthDetail.answer1 = "6";     // int inches
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ03";
        healthDetail.answer1 = "75"; // int weight in Kgs
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ05";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ06";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ07";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ09";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ125";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ144";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);


        healthDetail = new HealthDetail();
        healthDetail.code = "HQ165";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);


        healthDetail = new HealthDetail();
        healthDetail.code = "HQ166";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ167";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.YN_No;
        healthDetail.answer3 = Constant.YN_No;
        healthDetail.answer4 = Constant.YN_No;
        healthDetail.answer5 = Constant.YN_No;
        healthDetail.answer6 = Constant.YN_No;
        healthDetail.answer7 = Constant.YN_No;
        healthDetail.answer8 = Constant.YN_No;
        healthDetail.answer9 = Constant.YN_No;
        healthDetail.answer10 = Constant.YN_No;
        healthDetail.answer11 = Constant.YN_No;
        healthDetail.answer12 = Constant.YN_No;
        healthDetail.answer13 = Constant.YN_No;
        healthDetail.answer14 = Constant.YN_No;
        healthDetail.answer15 = Constant.YN_No;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ168";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ61";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ24";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ21";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        healthDetail = new HealthDetail();
        healthDetail.code = "HQ172";
        healthDetail.answer1 = Constant.YN_No;
        healthDetail.answer2 = Constant.BLANK;
        healthDetail.answer3 = Constant.BLANK;
        healthDetail.answer4 = Constant.BLANK;
        healthDetailList.add(healthDetail);

        return healthDetailList;
    }

    public IciciPremiumCalculationRequest buildIciciPremiumCalculationRequest(InsuranceRequest insuranceRequest,
                                    InsurancePolicy policy, String initiationPoint, String changedAmount, Applicant applicant){
        IciciPremiumCalculationRequest request = new IciciPremiumCalculationRequest();
      //  Applicant applicant = applicationRequest.getRequest().getApplicant();
        Name name = applicant.getApplicantName();
        String firstName = "";
        String lastName = "";
        if(name != null){
            if(name.getFirstName() != null)
                firstName = name.getFirstName();
            if(name.getLastName() != null)
                lastName = name.getLastName();
        }
        request.FirstName = firstName;
        request.LastName = lastName;
        try{
            String dobString = applicant.getDateOfBirth();
            //String start_dt = "2011-01-01";
            DateFormat formatter = new SimpleDateFormat("ddMMyyyy");
            Date dob = (Date)formatter.parse(dobString);
            DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            request.DateOfBirth = formater.format(dob);
        } catch (Exception e) {
            //TODO remove this hardcoded default value
            request.DateOfBirth = "1983-09-30";
        }
        request.Gender = applicant.getGender();

        //sending single if applicant is widow, for sbfc
        request.MaritalStatus = applicant.getMaritalStatus();
        if(Institute.isInstitute(insuranceRequest.getHeader().getInstitutionId(),Institute.SBFC) &&
                StringUtils.equalsIgnoreCase(applicant.getMaritalStatus(),"Widow")){
            request.MaritalStatus = "Single";
        }

        ProductDetails productDetails = new ProductDetails();
        Product product = new Product();

        product.ProductType = "TRADITIONAL";
        product.ProductName = "Group Loan Secure";
        product.ProductCode = "GL1";
        product.ModeOfPayment = "Yearly";
        product.ModalPremium = "0";
        product.AnnualPremium = "0";

//        Application application = applicationRequest.getRequest().getApplication();
        Integer loanAmount = 0;
        loanAmount = Integer.valueOf(changedAmount);
        product.DeathBenefit = loanAmount.toString();
        product.LoanAmount = loanAmount.toString();
        product.Term = policy.getTenorRequested();
        product.LoanTenure = policy.getTenorRequested();
        product.PremiumPaymentTerm = "1";
        product.PremiumPaymentOption = "Single Pay";
        product.CoverageOption = "Reduced";
        if(getFieldName(policy.getScheme()) != null) {
            product.BenefitOption = BenefitOption.valueOf(policy.getScheme()).getFieldName();
            product.MasterCode = policy.getMasterCode();
        } else {
            product.BenefitOption = "05";
            product.MasterCode = "SBFCPL03";
        }
        product.IPAddress = "0:0:0:0:0:0:0:1";
        productDetails.Product = product;
        request.ProductDetails = productDetails;
        return request;
    }
}
