package com.softcell.service.thirdparty.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.ScoringCommunication;
import com.softcell.config.ScoringConfiguration;
import com.softcell.constants.ActionName;
import com.softcell.constants.CustomHttpStatus;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.repository.UploadFileMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.ThirdPartyCallLog;
import com.softcell.gonogo.model.adroit.AdroitCallLog;
import com.softcell.gonogo.model.adroit.ImageList;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.LogoutRequest;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.security.v2.LoginBaseResponse;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.model.security.v2.LogoutBaseResponse;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.factory.impl.ScoringJsonBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.HomeManager;
import com.softcell.ssl2.finfort.model.FileUploadData;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.plexus.util.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ssg408 on 24/9/18.
 */
@Component
public class ThirdPartyIntegrationHelper {

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyIntegrationHelper.class);
    public static final String PROMOCODE = "promocode";
    public static final String CHANNEL = "channel";
    public static final String ONLINE = "Online";
    // Request types TODO Use enum
    public static final String VALUATION_INITIATE = "INITIATE";
    public static final String VALUATION_CHECK = "CHECK";
    public static final String VALUATION_GET_IMAGE = "GET-IMAGE";
    public static final String INITIATE = "INITIATE";
    public static final String INITIATE_INDIVIDUAL = "INITIATE-INDIVIDUAL";
    public static final String INITIATE_CORPORATE = "INITIATE-CORPORATE";

    public static final String FILE_LIST = "FILE-LIST";
    public static final String FETCH_REPORT = "FETCH-REPORT";
    public static final String SFTP_CONNECTION = "SFTP-CONNECTION";
    public static final String STATUS_REPORT = "STATUS-REPORT";
    public static final String PERFIOS_GET_FILE_DB_LOG = "PerfiosGetfileLog";

    public static final String MSG_DOWNLOADED = "DOWNLOADED";
    public static final String MSG_DOWNLOAD_ERROR = "MSG_DOWNLOAD_ERROR";

    public static final String FILE_COMMON_DOCUMENT = "Common Document";
    private static final String FINFORT_REMARK = "File from Finfort";

    private static final String FILE_TYPE_IMAGE = "image";
    private static final String FILE_TYPE_DOC = "doc";

    public static final String PERFIOS = "PERFIOS";

    @Autowired
    private HomeManager homeManager;

    @Autowired
    private UploadFileMongoRepository fileRepository;

    @Autowired
    private ExternalAPILogRepository  extApiRepository;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    public static final Map<String , String> PAISA_BAZAR = new HashMap<>();
    public static final Map<String , String> AGGREGATOR = new HashMap<>();
    public static final Map<String , String> MCPAPI = new HashMap<>();



    public enum ThirdParty {
        PAISA_BAZAR, ADROIT, FINFORT;

        public static ThirdParty getService(String serviceName) {
            return Stream.of(values())
                    .filter(service -> StringUtils.equalsIgnoreCase( serviceName, service.name()))
                    .findFirst().orElse(null);
        }

        public static boolean iService(String serviceName, ThirdParty service) {
            return  StringUtils.equalsIgnoreCase( serviceName, service.name()) ;
        }
    }

    static {
        //
        PAISA_BAZAR.put(GNGWorkflowConstant.APPROVED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Application ######## is processed successfully based on " +
                        "the information provided to us. Our executive will call you shortly. Please mail us at support@sbfc.in for any query");

        AGGREGATOR.put(GNGWorkflowConstant.DECLINED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Unfortunately we cannot proceed with your application at this moment. Your declared information does not meet our criteria." +
                        " Please mail us at support@sbfc.in for any query");

        AGGREGATOR.put(GNGWorkflowConstant.OTHER.toFaceValue(),"Hi applicantname,\n" +
                "Application could not be processed at this moment. We will update you regarding your application shortly. " +
                "Please mail us at support@sbfc.in for any query");
        AGGREGATOR.put(PROMOCODE, "Paisa Bazaar");
        AGGREGATOR.put(CHANNEL, "Online");


        AGGREGATOR.put(GNGWorkflowConstant.APPROVED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Application ######## is processed successfully based on " +
                        "the information provided to us. Our executive will call you shortly. Please mail us at support@sbfc.in for any query");

        AGGREGATOR.put(GNGWorkflowConstant.DECLINED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Unfortunately we cannot proceed with your application ########  at this moment. Your declared information does not meet our criteria." +
                        " Please mail us at support@sbfc.in for any query");

        AGGREGATOR.put(GNGWorkflowConstant.OTHER.toFaceValue(),"Hi applicantname,\n" +
                "Application could not be processed at this moment. We will update you regarding your application shortly. " +
                "Please mail us at support@sbfc.in for any query");
        AGGREGATOR.put(PROMOCODE, "Paisa Bazaar");
        AGGREGATOR.put(CHANNEL, "Online");

        AGGREGATOR.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(),"User authentication failed, please try again.");
        AGGREGATOR.put(PROMOCODE, "Paisa Bazaar");

        MCPAPI.put(GNGWorkflowConstant.AUTH_ERROR.toFaceValue(),"User authentication failed, please try again.");
        MCPAPI.put(GNGWorkflowConstant.APPROVED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Application ######## is processed successfully based on " +
                        "the information provided to us. Cibil hit result : #CIBRES#. Application is falling into #BT# bucket.  Our executive will call you shortly. Please mail us at support@sbfc.in for any query");
        MCPAPI.put(GNGWorkflowConstant.DECLINED.toFaceValue(),
                "Hi applicantName,\n" +
                        "Unfortunately we cannot proceed with your application at this moment. Your declared information does not meet our criteria." +
                        " Please mail us at support@sbfc.in for any query");
        MCPAPI.put(GNGWorkflowConstant.OTHER.toFaceValue(),
                "Hi applicantName,\n" +
                        "Unfortunately we cannot proceed with your application ######## at this moment. Cibil hit result : #CIBRES#. Application is falling into #BT# bucket. Your declared information does not meet our criteria." +
                        " Our executive will call you shortly. Please mail us at support@sbfc.in for any query");
    }

    public LoginBaseResponse setLoginDetails(ApplicationRequest applicationRequest, String username, String password){
        LoginRequest loginRequest = new LoginRequest();
        LoginBaseResponse loginResponse = null;
        try {
            loginRequest.setUserName(username);
            if(StringUtils.isNotEmpty(applicationRequest.getAggregatorData().getEncryptPassword())){
                loginRequest.setPassword(applicationRequest.getAggregatorData().getEncryptPassword());
            }else if(StringUtils.isNotEmpty(password)){
                loginRequest.setPassword(GngUtils.encodeToSHA1(password));
            }else{
                return null;
            }

            loginRequest.setInstituteName("SBFC");

            loginResponse = homeManager.getLoginDetails(loginRequest);
            LoginServiceResponse loginServiceResponse = null;
            if (loginResponse.getBody() != null && loginResponse.getBody().getLoginServiceResponse() != null) {
                loginServiceResponse = loginResponse.getBody().getLoginServiceResponse();
                if (loginServiceResponse != null) {
                    if (CollectionUtils.isNotEmpty(loginServiceResponse.getBranches())) {
                        Branch branch = loginServiceResponse.getBranches().iterator().next();
                        AppMetaData appMetaData = new AppMetaData();
                        appMetaData.setEmpId(applicationRequest.getHeader().getLoggedInUserId());
                        appMetaData.setBranchV2(branch);
                        applicationRequest.setAppMetaData(appMetaData);
                    }
                }
            }
        } catch (Exception e){
            logger.error("setLoginDetails: Error while getting login details {}",e.getMessage());
            e.printStackTrace();
        }
        return loginResponse;
    }

    public LogoutBaseResponse setLogoutDetails(String userId, String institutionId){
        logger.info("setLogoutDetails method started");
        LogoutBaseResponse logoutBaseResponse = null;
        try{
            LogoutRequest logoutRequest = new LogoutRequest();
            logoutRequest.setUserId(userId);
            logoutRequest.setInstitutionId(institutionId);

            logoutBaseResponse = homeManager.logoutUserWithoutAuthToken(logoutRequest);
            if(logoutBaseResponse != null && logoutBaseResponse.getStatus() != null
                    && logoutBaseResponse.getStatus().getStatusCode() == 200){
                logger.info("setLogoutDetails: {} successfully logged out from UAM",userId);
            }
        }catch (Exception e){
            logger.error("Error while setLogoutDetails: {}",e.getMessage());
            e.printStackTrace();
        }
        return logoutBaseResponse;
    }

    /**
     * Build valuation trigger request for agencies
     * @param refID
     * @param institutionId
     * @param serviceId         Adroit, ...
     * @param applicationRequest
     * @param valuation
     * @return          AdroitRequest, ...
     */
    public Object buildValuationTriggerRequest(String refID, String institutionId, String serviceId, ApplicationRequest applicationRequest, Valuation valuation) {
        Object request = null;
        if(StringUtils.equalsIgnoreCase(serviceId, ThirdParty.ADROIT.name()) ) {
            request = AdroitHelper.buildAdroitRequest( refID,  institutionId,  serviceId,  applicationRequest,  valuation);
        } else {
            logger.info("Service {} not handled for institute {}", serviceId, institutionId);
        }
        return request;
    }

    public List<AdroitCallLog> getAckIds(String agencyService, String refID, String institutionId, List<ValuationDetails> agencySubmittedList) {
        // get the ackIds from teh respective db collection
        List<AdroitCallLog>  valuationCallLogList = new ArrayList<>();

        // Get collateralIds
        List<String> collateralIds = agencySubmittedList.stream().map(val -> val.getCollateralId()).collect(Collectors.toList());
        valuationCallLogList = extApiRepository.fetchAdroitAckIds(refID, collateralIds, VALUATION_INITIATE);

        /* Since there could be multiple triggers for a collateral,
                we have to get only the latest*/
        AdroitCallLog logValue = null; List<AdroitCallLog> adroitCallLogList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(valuationCallLogList ) ) {
            for (ValuationDetails dtls : agencySubmittedList) {
                logValue = valuationCallLogList.stream().filter(log -> StringUtils.equalsIgnoreCase(log.getCollateralId(),
                        dtls.getCollateralId())).findFirst().orElse(null);
                if (logValue != null) {
                    adroitCallLogList.add(logValue);
                }
            }
        }
        return adroitCallLogList;
    }

    public Object buildValuationCheckRequest(String institutionId, String serviceId, ThirdPartyCallLog requestFromObject) {
        Object request = null;
        if(StringUtils.equalsIgnoreCase(serviceId, ThirdParty.ADROIT.name()) ) {
            request = AdroitHelper.buildCheckRequest( requestFromObject.getAcknowledgementId(), institutionId);
        } else {
            logger.info("Service {} not handled ", serviceId );
        }
        return request;
    }

    public Object buildValuationImageRequest(String imageId, String institutionId, String serviceId, ValuationDetails valuationDetails) {

        Object request = null;
        if(StringUtils.equalsIgnoreCase(serviceId, ThirdParty.ADROIT.name()) ) {
            request = AdroitHelper.buildImageRequest( imageId, valuationDetails.getAcknowledgmentId(), institutionId);
        } else {
            logger.info("Service {} not handled ", serviceId );
        }
        return request;

    }

    public void storeValuationImages(String refId, String institutionId, ValuationDetails valuationDetails, String filedata,
                                     String imageSource, String product, Object imageinfo) {
        /*
        Filedata is in format "data:image/png;base64,<imagedata>".
        So get the datatype i.e. 'image/png' and append the file extension to the
        Form file name : Valuation file name format is
            "sFileName": "Property Valuation collId " + collId + ' index ' + index,
            "sFileType": type == 'input' ? 'valuation-input' : 'valuation-output'
          */
        final String expectedFormat = "data:image/png;base64,<imagedata>";
        String fileId = "";
        String extension = "";
        String[] fileinfo = filedata.split(",");
        if( fileinfo.length == 2) {
            extension = fileinfo[0].split(";")[0].split(":")[1];
            filedata = fileinfo[1];
        } else if( fileinfo.length > 2){
            logger.error("{} collId {} image data recived in improper format ;expected format is {}",
                    valuationDetails.getRefId(), valuationDetails.getCollateralId(), expectedFormat);
            return;
        }

        if( imageinfo instanceof com.softcell.gonogo.model.adroit.ImageList){
            fileinfo =((ImageList) imageinfo).getImageName().split(".");
            if( fileinfo.length == 2) {
                extension = fileinfo[1];
            }
            fileId = ((ImageList)imageinfo).getImageId();
        }
        String fileName = new StringBuilder()
                .append("Property Valuation collId ").append( valuationDetails.getCollateralId())
                .append(" index ").append(valuationDetails.getIndex()).toString();
        String filetype = "valuation-output";

        logger.info("{} image filename = {}, extension = {}; datalength chars = {}", refId, fileName, extension, filedata.length() );

        FileUploadRequest metaData = new FileUploadRequest();


        FileHeader fileHeader = getFileHeader(institutionId, null, imageSource, Product.getProductFromName(product).toProductName());
        UploadFileDetails fileDetails = getUploadFileDetails(null, valuationDetails.getCollateralId(), fileName, filetype, extension, fileId);

        metaData.setFileHeader(fileHeader);
        metaData.setUploadFileDetails(fileDetails);
        metaData.setGonogoReferanceId(refId);
        try {
            byte[] imageByte = Base64.decodeBase64(filedata);
            InputStream inputStream = new ByteArrayInputStream(imageByte);

            logger.info("Saving in image store fileName {} , fileType {}...", fileName, filetype);
            String id = fileRepository.store(inputStream, fileName, filetype, metaData);
            logger.debug("Image {} {} complete with id {}", fileName, filetype, id);
        } catch (Exception e){
            //e.printStackTrace();
            logger.error("Error while saving {} Valuation image from {} (ackId {} )", valuationDetails.getRefId(),
                    imageSource, valuationDetails.getAcknowledgmentId());
        }
    }


    public void saveFileData(String refId, String institutionId, String applicantId, String product, String ackId,
                             WFJobCommDomain serviceConfig, Object fileInfo, String fileName, byte[] fileContent) {

        String fileId = fileName;
        if( ThirdParty.iService(serviceConfig.getServiceId(), ThirdParty.FINFORT)) {
            FileUploadData fileData = (FileUploadData) fileInfo;
            //fileId = fileName;
            String extension = fileData.getExtension();
            String filetype = fileData.getType();

            logger.info("{} filename = {}, extension = {};", refId, fileName, extension);

            FileUploadRequest metaData = new FileUploadRequest();
            FileHeader fileHeader = getFileHeader(institutionId, applicantId, ThirdParty.FINFORT.name(), product);

            UploadFileDetails fileDetails = getUploadFileDetails(applicantId, null, fileName, filetype, extension, fileId);
            fileDetails.setRemark(FINFORT_REMARK);

            metaData.setFileHeader(fileHeader);
            metaData.setUploadFileDetails(fileDetails);
            metaData.setGonogoReferanceId(refId);

            try {
                InputStream inputStream = new ByteArrayInputStream(fileContent);

                logger.info("Saving in imagestore fileName {} , fileType {}...", fileName, filetype);
                // filename = contentType = Common Document
                String id = fileRepository.store(inputStream, FILE_COMMON_DOCUMENT, FILE_COMMON_DOCUMENT, metaData);
                logger.debug("File {} {} complete with id {}", fileName, filetype, id);
            } catch (Exception e) {
                //e.printStackTrace();
                logger.error("RefId-ackid {} {} Error while saving Finfort file {}", refId, ackId, fileName);
            }
        } else {
            logger.info("File save not configured for {} ", serviceConfig.getServiceId());
        }
    }

    private UploadFileDetails getUploadFileDetails(String applicantId, String collaterId, String fileName, String filetype, String extension,
                                                   String fileId) {
        UploadFileDetails fileDetails = new UploadFileDetails();
        if( StringUtils.isNotEmpty(applicantId) ) fileDetails.setApplicantId(applicantId);
        if( StringUtils.isNotEmpty(collaterId)) fileDetails.setCollateralId(collaterId);
        // reason = filename
        fileDetails.setReason(fileName);
        fileDetails.setFileId(fileId);
        fileDetails.setFileName(FILE_COMMON_DOCUMENT);
        fileDetails.setFileType(FILE_COMMON_DOCUMENT);
        // extension = image/doc
        fileDetails.setFileExtension(getFiletype(extension));

        return fileDetails;
    }

    private String getFiletype(String extension) {
        String filetype = FILE_TYPE_DOC;
        if( StringUtils.endsWithIgnoreCase(extension,"jpg") || StringUtils.endsWithIgnoreCase(extension,"jpeg")
            || StringUtils.endsWithIgnoreCase(extension,"png"))
            filetype = FILE_TYPE_IMAGE;
        else filetype = extension.replace(".", "");

        return filetype;
    }

    private FileHeader getFileHeader(String institutionId, String applicantId, String source, String product) {
        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId(institutionId);
        if( StringUtils.isNotEmpty(applicantId)) fileHeader.setApplicantId(applicantId);
        fileHeader.setApplicationSource(source);
        fileHeader.setProduct(Product.getProductFromName(product));
        return fileHeader;
    }

    public boolean isRequestType(String toBeChecked, String checkWith){
        boolean requestCheck = false;
        if( StringUtils.equalsIgnoreCase(toBeChecked, checkWith)) requestCheck = true;
        return requestCheck;
    }

    public void setFileRepository(UploadFileMongoRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public LoginBaseResponse setLoginDetails(LoginDataRequest loginDataRequest, String username, String password){
        LoginRequest loginRequest = new LoginRequest();
        LoginBaseResponse loginResponse = null;
        try {
            loginRequest.setUserName(username);
            if(StringUtils.isNotEmpty(loginDataRequest.getAggregatorData().getEncryptPassword())){
                loginRequest.setPassword(loginDataRequest.getAggregatorData().getEncryptPassword());
            }else if(StringUtils.isNotEmpty(password)){
                loginRequest.setPassword(GngUtils.encodeToSHA1(password));
            }else{
                return null;
            }

            loginRequest.setInstituteName("SBFC");

            loginResponse = homeManager.getLoginDetails(loginRequest);
            LoginServiceResponse loginServiceResponse = null;
            if (loginResponse.getBody() != null && loginResponse.getBody().getLoginServiceResponse() != null) {
                loginServiceResponse = loginResponse.getBody().getLoginServiceResponse();
                if (loginServiceResponse != null) {
                    if (CollectionUtils.isNotEmpty(loginServiceResponse.getBranches())) {
                        Branch branch = loginServiceResponse.getBranches().stream().filter(obj -> StringUtils.equalsIgnoreCase(obj.getBranchName(), "default")).findAny().orElse(null);
                        AppMetaData appMetaData = new AppMetaData();
                        appMetaData.setEmpId(loginDataRequest.getHeader().getLoggedInUserId());
                        appMetaData.setBranchV2(branch);
                        loginDataRequest.setAppMetaData(appMetaData);
                    }
                }
            }
        } catch (Exception e){
            logger.error("Error while getting login details {}", ExceptionUtils.getStackTrace(e));
        }
        return loginResponse;
    }

    public BaseResponse generateLoanChargesRepaymentRequestAndCallSobre(String instituteId, GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception{
        BaseResponse baseResponse = null;

        ScoringCommunication scoringCommunication = ScoringConfiguration.getCreditial(instituteId);
        if (null != scoringCommunication){
            if(StringUtils.isEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getTenorRequested()))
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().setTenorRequested("0");
            String policyName = "LoanChargesAndRepayment";
            ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
            ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantScoringJSonVersion2(goNoGoCustomerApplication);
            scoringApplicantRequest.setPolicyName(policyName);
            scoringApplicantRequest.getApplication().setPolicyName(policyName);
            logger.debug("SOBRE objectMapper formed for {} as {}", goNoGoCustomerApplication.getGngRefId(), scoringApplicantRequest);

            ObjectMapper objectMapper = new ObjectMapper();

            String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
            valueJson = valueJson.replaceAll("sApplID", "customerId");
            scoringApplicantRequest.setRequest(valueJson);

            //moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);

            String response = connectSobre(valueJson, scoringCommunication);
            response = response.replaceAll("customerId", "sApplID");
            logger.debug("{} SOBRE response received as {}", goNoGoCustomerApplication.getGngRefId(), response);

            //saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());

            ScoringApplicantResponse scoringApplicantResponse = null;
            if (StringUtils.isNotEmpty(response)) {
                scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
            }
            saveSobreRequestResponse(scoringApplicantRequest, scoringApplicantResponse);
            if(null != scoringApplicantResponse){
                try {
                    //Parent Object LoanCharges
                    LoanCharges loanCharges = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getLoanCharges();

                    //25% LoanCharges
                    LoanCharges loanCharges1 = new LoanCharges();
                    loanCharges1.setLoanPercentage(String.valueOf(((Double)((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$FINAL_APPLOAN_PERC_1")).intValue()));
                    loanCharges1.setTypeOfROI(((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$TYPE_OF_ROI")));
                    loanCharges1.setProFees(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PF_PERCENTAGE_FINAL")));
                    loanCharges1.setProFeesAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PF_AMOUNT_1")));
                    loanCharges1.setGst(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$GST_PERCENTAGE_FINAL")));
                    loanCharges1.setGstAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$GST_AMOUNT_1")));
                    //loanCharges1.setTotalFees(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TOTAL_PF_AMT_1")));
                    //loanCharges1.setProDeductAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TOTAL_PF_AMT_1")));
                    loanCharges1.setCersaiFees(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CERSAIFEE_AMT_1")));
                    loanCharges1.setCersaiFeeWithGST(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CERSAIFEE_AMOUNT_1")));
                    loanCharges1.setPreEMIDeductAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PREEMI_AMOUNT_1")));
                    //loanCharges1.setNetDisbAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("FINANCIAL_FIELDS$NET_DISB_AMOUNT_1")));
                    loanCharges1.setFinalEMIAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("FINANCIAL_FIELDS$EMI_AMOUNT_1")));
                    loanCharges1.setAdvanceEMI(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ADVANCE_EMI_MAIN_AMOUNT_1")));
                    loanCharges1.setAdvEMIWithGST(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ADVANCE_EMI_AMOUNT_1")));
                    loanCharges1.setCreditVidyaCharge(String.valueOf(((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CREDIT_VIDYA_CHARGE_AMOUNT_1")));
                    loanCharges1.setDueDate(((Double)((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DUE_DATE_FINAL")).intValue());
                    loanCharges1.setEmiStartDate(getEMIStartDate(loanCharges, loanCharges1.getDueDate()));
                    loanCharges1.setEmiEndDate(getEMIEndDate(loanCharges, loanCharges1.getEmiStartDate()));
                    loanCharges1.setDisbursedDate(loanCharges.getDisbursedDate());
                    loanCharges1.setInsuranceRequired(loanCharges.isInsuranceRequired());

                    //25% LoanAmt Details
                    LoanAmtDetails loanAmtDetails1 = new LoanAmtDetails();
                    loanAmtDetails1.setFinalLoanApprovedAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_FINAL")));
                    loanAmtDetails1.setApprovedLoanAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_FINAL")));
                    loanAmtDetails1.setFinalDisbAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_1")));
                    loanCharges1.setLoanDetails(loanAmtDetails1);

                    //25% InterestDetails
                    InterestDetails interestDetails1 = new InterestDetails();
                    interestDetails1.setInterestRate(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ROI_FINAL")));
                    interestDetails1.setDisbInterestRate(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ROI_FINAL")));
                    interestDetails1.setFoir(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DIGI_MAX_FOIR_PERC_FINAL")));
                    interestDetails1.setPfPercentage(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DIGI_PF_CHARGE_FINAL")));
                    interestDetails1.setFinalFoir(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DIGI_FINAL_FOIR_CAL")));
                    loanCharges1.setInterestDetails(interestDetails1);


                    //25% Insurance Details
                    List<InsurenceCat> insurenceCatList1 = new ArrayList<>();
                    InsurenceCat insurenceCat1 = new InsurenceCat();
                    insurenceCat1.setCompanyName((((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$INSURANCE_COMPANY_NAME"))).toUpperCase());
                    insurenceCat1.setAmount((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$INSURANCE_AMOUNT_1"));
                    insurenceCat1.setInsuranceCategory((((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$INSURANCE_CATEGORY"))));
                    insurenceCatList1.add(insurenceCat1);
                    loanCharges1.setInsurenceCatList(insurenceCatList1);

                    loanCharges1.setTotalFees(loanCharges1.getProFeesAmt() + loanCharges1.getGstAmt());
                    loanCharges1.setProDeductAmt(loanCharges1.getTotalFees());
                    loanCharges1.setNetDisbAmt(loanAmtDetails1.getFinalDisbAmt() - insurenceCat1.getAmount() - loanCharges1.getTotalFees() - loanCharges1.getCersaiFees() - loanCharges1.getPreEMIDeductAmt() - loanCharges1.getAdvEMIWithGST());
                    loanCharges1.setInsuranceAmt(insurenceCat1.getAmount());

                    //25%TenureDetails
                    TenureDetails tenureDetails1 = new TenureDetails();
                    tenureDetails1.setTenureMonths(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TENURE_FINAL")).intValue());
                    tenureDetails1.setDisbTenureMonths(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TENURE_FINAL")).intValue());
                    loanCharges1.setTenureDetails(tenureDetails1);

                    //75% LoanCharges
                    LoanCharges loanCharges2 = new LoanCharges();
                    loanCharges2.setLoanPercentage(String.valueOf(((Double)((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$FINAL_APPLOAN_PERC_2")).intValue()));
                    loanCharges2.setTypeOfROI(((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$TYPE_OF_ROI")));
                    loanCharges2.setProFees(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PF_PERCENTAGE_FINAL")));
                    loanCharges2.setProFeesAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PF_AMOUNT_2")));
                    loanCharges2.setGst(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$GST_PERCENTAGE_FINAL")));
                    loanCharges2.setGstAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$GST_AMOUNT_2")));
                    //loanCharges2.setTotalFees(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TOTAL_PF_AMT_2")));
                    //loanCharges2.setProDeductAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$TOTAL_PF_AMT_2")));
                    loanCharges2.setCersaiFees(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CERSAIFEE_AMT_2")));
                    loanCharges2.setCersaiFeeWithGST(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CERSAIFEE_AMOUNT_2")));
                    loanCharges2.setPreEMIDeductAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$PREEMI_AMOUNT_2")));
                    //loanCharges2.setNetDisbAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("FINANCIAL_FIELDS$NET_DISB_AMOUNT_2")));
                    loanCharges2.setFinalEMIAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("FINANCIAL_FIELDS$EMI_AMOUNT_2")));
                    loanCharges2.setAdvanceEMI(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ADVANCE_EMI_MAIN_AMOUNT_2")));
                    loanCharges2.setAdvEMIWithGST(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$ADVANCE_EMI_AMOUNT_2")));
                    loanCharges2.setCreditVidyaCharge(String.valueOf(((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$CREDIT_VIDYA_CHARGE_AMOUNT_2")));
                    loanCharges2.setDueDate(((Double)((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DUE_DATE_FINAL")).intValue());
                    loanCharges2.setInsuranceRequired(loanCharges.isInsuranceRequired());

                    //75% LoanAmt Details
                    LoanAmtDetails loanAmtDetails2 = new LoanAmtDetails();
                    loanAmtDetails2.setFinalLoanApprovedAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_FINAL")));
                    loanAmtDetails2.setApprovedLoanAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_FINAL")));
                    loanAmtDetails2.setFinalDisbAmt(Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$FINAL_APPLOAN_AMT_2")));
                    loanCharges2.setLoanDetails(loanAmtDetails2);
                    //75% InterestDetails
                    loanCharges2.setInterestDetails(interestDetails1);

                    //75% TenureDetails
                    loanCharges2.setTenureDetails(tenureDetails1);

                    //75% insurance details
                    List<InsurenceCat> insurenceCatList2 = new ArrayList<>();
                    InsurenceCat insurenceCat2 = new InsurenceCat();
                    insurenceCat2.setCompanyName((((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$INSURANCE_COMPANY_NAME"))).toUpperCase());
                    insurenceCat2.setAmount((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$INSURANCE_AMOUNT_2"));
                    insurenceCat2.setInsuranceCategory((((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$INSURANCE_CATEGORY"))));
                    insurenceCatList2.add(insurenceCat2);
                    loanCharges2.setInsurenceCatList(insurenceCatList2);

                    loanCharges2.setTotalFees(loanCharges2.getProFeesAmt() + loanCharges2.getGstAmt());
                    loanCharges2.setProDeductAmt(loanCharges2.getTotalFees());
                    loanCharges2.setNetDisbAmt(loanAmtDetails2.getFinalDisbAmt() - insurenceCat2.getAmount() - loanCharges2.getTotalFees() - loanCharges2.getCersaiFees() - loanCharges2.getPreEMIDeductAmt() - loanCharges2.getAdvEMIWithGST());
                    loanCharges2.setInsuranceAmt(insurenceCat2.getAmount());

                    List<LoanCharges> loanChargesList = new ArrayList<>();
                    loanChargesList.add(loanCharges1);
                    loanChargesList.add(loanCharges2);

                    loanCharges.setInstitutionId(instituteId);
                    loanCharges.setLoanChargesList(loanChargesList);
                    loanCharges.setFinalEMIAmt(Math.ceil((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$EMI_AMOUNT")));

                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, loanCharges);
                } catch (Exception e) {
                    logger.error("Error occured while reading ScoringApplicant Response {}", scoringApplicantResponse);
                    logger.error("Exception : {}", e.getStackTrace());
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, scoringApplicantResponse);
                }
            }
        }else
            baseResponse = GngUtils.getBaseResponse(HttpStatus.BAD_REQUEST, "Scoring not configured for institute");

        return baseResponse;
    }

    public boolean decisionToSaveLoginData(BaseResponse baseResponse){
        boolean doSave = false;
        Object response = baseResponse.getPayload().getT();
        if( response instanceof GoNoGoCustomerApplication) {
            GoNoGoCustomerApplication gngData = (GoNoGoCustomerApplication)response;
            if(null != gngData && StringUtils.isNotEmpty(gngData.getApplicationStatus())
                    && !StringUtils.equalsIgnoreCase(gngData.getApplicationStatus(),GNGWorkflowConstant.DECLINED.toFaceValue())){
                doSave = true;
            }
        }
        return doSave;
    }

    public BaseResponse generateRequestAndCallSobre(GoNoGoCustomerApplication goNoGoCustomerApplication, String policyName, boolean saveReqRes ) throws Exception{
        BaseResponse baseResponse;
        ScoringCommunication scoringCommunication;
        String instituteId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        if (instituteId == null) {
            logger.info("Institution id not provided !");
            throw new Exception("Institution id not provided !");
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instituteId);
            if (scoringCommunication == null) {
                logger.info("{} - Scoring not configured for institute {} ! Cannot execute scoring for {}", instituteId, goNoGoCustomerApplication.getGngRefId());
                throw new Exception("Scoring not configured for institute");
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
        ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantScoringJSonVersion2(goNoGoCustomerApplication);
        scoringApplicantRequest.setPolicyName(policyName);
        scoringApplicantRequest.getApplication().setPolicyName(policyName);
        logger.debug("SOBRE objectMapper formed for {} as {}", goNoGoCustomerApplication.getGngRefId(), scoringApplicantRequest);

        ObjectMapper objectMapper = new ObjectMapper();

        String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
        valueJson = valueJson.replaceAll("sApplID", "customerId");
        scoringApplicantRequest.setRequest(valueJson);
        if(saveReqRes)
            moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);

        String response = connectSobre(valueJson, scoringCommunication);
        response = response.replaceAll("customerId", "sApplID");
        logger.debug("{} SOBRE response received as {}", goNoGoCustomerApplication.getGngRefId(), response);

        if(saveReqRes)
            saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());

        ScoringApplicantResponse scoringApplicantResponse = null;
        if (StringUtils.isNotEmpty(response)) {
            scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
        }
        saveSobreRequestResponse(scoringApplicantRequest,scoringApplicantResponse);
        if (null != scoringApplicantResponse) {
            final String applicantId = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId();
            ScoringApplicantResponse primaryResponse = new ScoringApplicantResponse();
            primaryResponse.setSummary(scoringApplicantResponse.getSummary());
            primaryResponse.setApplicantResult(scoringApplicantResponse.getApplicantResult().stream().filter(result ->
                    StringUtils.equalsIgnoreCase(applicantId, result.getApplicantId()))
                    .collect(Collectors.toList()));
            ScoringResponse scoringResponse = new ScoringResponse();
            scoringResponse.setApplicantResponse(primaryResponse);
            if (null != scoringResponse.getApplicantResponse().getSummary()){
                goNoGoCustomerApplication.setApplicationStatus(scoringResponse.getApplicantResponse().getSummary().getApplicantDecesion());
            }
            goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResponse);
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoCustomerApplication);
        } else {
            logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
            String errorMsg = String.format("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(errorMsg)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.INVALID_REQUEST_CODE, errors);
        }
        return baseResponse;
    }

    private String connectSobre(String valueJson, ScoringCommunication scoringCommunication) throws IOException {
        String url = scoringCommunication.getScoringServiceUrl();
        StringBuilder endpoint = new StringBuilder(url) ;
        endpoint.append("/processApplication?")
                .append("INSTITUTION_ID=").append(scoringCommunication.getInstitutionId())
                .append("&USER_ID=").append(scoringCommunication.getUserId())
                .append("&PASSWORD=").append(scoringCommunication.getPassword());

        logger.debug("Sending SOBRE objectMapper for url {} is {}",endpoint.toString(), valueJson);
        String response = new HttpTransportationService().postRequest(endpoint.toString(), valueJson, MediaType.APPLICATION_JSON_VALUE);
        if(response != null) {
            response = response.replaceAll("customerId", "sApplID");
        }
        return response;
    }

    private void saveSobreRequestResponse(ScoringApplicantRequest scoringApplicantRequest, ScoringApplicantResponse scoringApplicantResponse){
        try {
            ScoringCallLog callLog = new ScoringCallLog();
            callLog.setRefId(scoringApplicantRequest.getHeader().getApplicationId());
            callLog.setStep(scoringApplicantRequest.getPolicyName());
            callLog.setInstitutionId(scoringApplicantRequest.getHeader().getInstitutionId());
            callLog.setCallDate(new Date());
            callLog.setRequestString(JsonUtil.ObjectToString(scoringApplicantRequest));
            callLog.setRequest(scoringApplicantRequest);

            if (scoringApplicantResponse != null) {
                callLog.setResponseString(JsonUtil.ObjectToString(scoringApplicantResponse));
                callLog.setResponse(scoringApplicantResponse);
            }
            callLog.setRequestType(EndPointReferrer.GET_SCORING_LOG);
            extApiRepository.saveSobreCallLog(callLog);
        }catch(Exception e){
            logger.error("Exception occured while saving sobreLogs {}", e.getStackTrace());
        }
    }

    protected void saveRawResponse(String rawResponse, String refId) {
        logger.debug("Saving data for {}", ActionName.SOBRE_RESPONSE_SAVE);
        RawResponseLog rawResponseLog = RawResponseLog.builder().rawResponse(rawResponse)
                .refId(refId)
                .responseType(ActionName.SOBRE_RESPONSE_SAVE).build();

        extApiRepository.saveRawResonseLog(rawResponseLog);
    }

    public int getBrokenPeriodDays(Date disbursedDate){ //20/5/2020
        int brokenPeriodDays = 0;
        int dueDateStart;
        int dueDateEnd;
        int iDueDate = 2; // hardCoded value 2

        dueDateStart = iDueDate + 1;
        dueDateEnd = iDueDate - 1;

        int emiApplicable = 0; // days in month calculated for emi.
        if(disbursedDate != null) {
            int day = disbursedDate.getDate(); // 20
            int daysInMonth = new Date(disbursedDate.getYear(), disbursedDate.getMonth() + 1, 0).getDate(); // days in month (ex. 30 31)  // 31
            if (day >= dueDateStart) {                                      // 20>18  // yes
                emiApplicable = daysInMonth - day + 1;                       //  31-20+1 == 12
                brokenPeriodDays = emiApplicable + dueDateEnd;               // 12+ 1 = 13 noOfBrokenDay
            } else if (day <= dueDateEnd) {
                brokenPeriodDays = dueDateEnd - day + 1;
            }
        }
        logger.debug("Broken Period Days: {}", brokenPeriodDays );
        return brokenPeriodDays;
    }

    private Date getEMIStartDate(LoanCharges loanCharges, int high){
        Date dEMIStartDt = null;
        int day = loanCharges.getDisbursedDate().getDate();
        if (day <= high && day >= 1) {
            dEMIStartDt = new Date(loanCharges.getDisbursedDate().getYear(), loanCharges.getDisbursedDate().getMonth() + 1, high);
        } else {
            dEMIStartDt = new Date(loanCharges.getDisbursedDate().getYear(), loanCharges.getDisbursedDate().getMonth() + 2, high);
        }
        logger.debug("EMI Start Date: {}", dEMIStartDt );
        return dEMIStartDt;
    }

    private Date getEMIEndDate(LoanCharges loanCharges, Date dEMIStartDate){
        Date dEMIEndDate = new Date(dEMIStartDate.getYear(), dEMIStartDate.getMonth()+1, dEMIStartDate.getDate());
        dEMIEndDate.setMonth(dEMIEndDate.getMonth() + (loanCharges.getTenureDetails().getDisbTenureMonths() - 1) );
        logger.debug("EMI Start Date: {}", dEMIEndDate );
        return dEMIEndDate;
    }
}
