package com.softcell.service.thirdparty.utils;

import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.request.ApplicationRequest;
import org.springframework.stereotype.Component;

/**
 * Created by ssg0268 on 8/11/19.
 */
@Component(value = "religareEvaluator")
public class ReligarePolicyEvaluator implements InsurancePolicyEvaluator {
    private String templatePath;

    @Override
    public void setTemplateFile(String templateFile) {
        this.templatePath = templateFile;
    }

    @Override
    public String getTemplateFile() {
        return templatePath;
    }

    @Override
    public void evaluateBenefit(ApplicationRequest applicationRequest, InsurancePolicy policy) {

    }

    @Override
    public void evaluate(ApplicationRequest applicationRequest, Applicant applicant, InsurancePolicy policy) {

    }
}
