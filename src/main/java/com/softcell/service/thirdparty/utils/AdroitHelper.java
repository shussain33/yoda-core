package com.softcell.service.thirdparty.utils;

import com.softcell.constants.Constant;
import com.softcell.constants.Institute;
import com.softcell.dao.mongodb.repository.AdminLogMongoRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.adroit.*;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.valuation.ValuationInput;
import com.softcell.gonogo.model.core.valuation.ValuationOutput;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by archana on 11/10/18.
 */
public class AdroitHelper {

    // These are the keys of which values are to be replaced from application
    public static final String HEADER_APPLICATIONID = "sApplicationId";
    private static final Logger logger = LoggerFactory.getLogger(AdroitHelper.class);


    private static Set<String> headerKeys = new HashSet();

    public static final String SUCCESS = "Success";
    // InstituteId vs Adroit agenct code
    private static Map<String, String> agentCodeMap = new HashMap<>();

    // InstituteId vs clientId
    private static Map<String, String> clientIdMap = new HashMap<>();
    private static Map<String, String> propertyTypeMap = new HashMap<>();

    static {
        headerKeys.add(HEADER_APPLICATIONID);
        agentCodeMap.put("4075", "sbfc");  // Dev
        agentCodeMap.put("4021", "sbfc"); // Prod

        clientIdMap.put("4075", "32"); // DEV
        clientIdMap.put("4021", "32"); // PROD

        propertyTypeMap.put("Owned", "Owned");
        propertyTypeMap.put("Mixed use", "Mixed use");
        propertyTypeMap.put("Shop", "Shop");
        propertyTypeMap.put("Owned", "Owned");
        propertyTypeMap.put("Mixed use", "Mixed use");
        propertyTypeMap.put("Shop", "Shop");

    }
    public static Set<String> getHeaderKeys() {
        return headerKeys;
    }

    // -------------------------------------------------------------

    public static Object buildAdroitRequest(String refID, String institutionId, String serviceId,
                                            ApplicationRequest applicationRequest, Valuation valuation) {
        Branch branch = applicationRequest.getAppMetaData().getBranchV2();
        ValuationInput input = valuation.getValuationDetailsList().get(0).getValuationInput();
        /* NOTE : there should be always collateral Id present;
                But some backdated data may not have it ( not populated from UI )
                So to make it failsafe , check for null
        */
        String collateralId = GngUtils.setValueForNull(valuation.getValuationDetailsList().get(0).getCollateralId());
        String area = getArea(collateralId, applicationRequest);
        int branchId = getBranchId(institutionId, branch.getId());
        AdroitRequest adroitRequest = AdroitRequest.builder()
                // Branch related data
                    .branchId(branchId).branchName(branch.getBranchName()).branchPersonName(branch.getBranchManagerName())
                // Applicant name
                    .applicantName(GngUtils.convertNameToFullName(input.getApplicantName()))
                // Address fields
                /*  Property_No + Building_No + Street_Sector  = Address line 1
                    District = Address line 2
                */
                    .propertyAddress1(getAddressLine1(input.getPropertyAddress()))
                    .propertyAddress2(GngUtils.setValueForNull(input.getPropertyAddress().getTaluka()))
                    .city(input.getPropertyAddress().getCity())
                    .location(GngUtils.setValueForNull(input.getPropertyAddress().getLocality()))
                    .pincode(Long.toString(input.getPropertyAddress().getPin()))
                    .state(input.getPropertyAddress().getState())
                // Contact Person Name
                    .contactPersonName(GngUtils.convertNameToFullName(input.getContactPersonName()))
                // Contact person mobile
                    .contactPersonMob1(GngUtils.setValueForNull(input.getContactNumber()))
                // contact person mobile 2
                    .contactPersonMob2(Constant.BLANK)
                // case type = product
                    .typeOfCase(applicationRequest.getHeader().getProduct().toProductName())
                // Area
                    .area(area)
                // Collateral id
                    .collateralId(collateralId)
                // agent code = "sbfc" TODO get from db
                    .agentCode(agentCodeMap.get(institutionId))
                // client code = "32" TODO get from DB
                    .clientId(clientIdMap.get(institutionId))
                // owner name
                    .ownerName(getOwnerName(input))
                // property type - Resi, ...
                    //.propertyType(propertyTypeMap.get(input.getPropertyType()))
                    .propertyType(GngUtils.setValueForNull(input.getPropertyType()))
                // doc provided
                    .documentProvided(GngUtils.booleanToYesNO(input.isPropertyDocsprovided()))
                    .deedProvided(GngUtils.booleanToYesNO(input.isLatestDeedCopyProvided()))
                    .mapProvided(GngUtils.booleanToYesNO(input.isApprovedMapProvided()))
               // docs
                    .documents(Constant.BLANK)
                    .documentName(Constant.BLANK)
                    .documentExt(Constant.BLANK)
                    .build();

        return adroitRequest;
    }

    private static String getOwnerName(ValuationInput input) {
        String ownerName = Constant.BLANK;
        if( CollectionUtils.isNotEmpty(input.getPropertyOwnerNames()) ){
            ownerName = GngUtils.convertNameToFullName(input.getPropertyOwnerNames().get(0));
        }
        return ownerName;
    }

    private static String getArea(String collateralId, ApplicationRequest applicationRequest) {
        String area = Constant.ZERO;
        if(StringUtils.isNotBlank(collateralId)) {
            List<String> tmpList= applicationRequest.getRequest().getApplication().getCollateral().stream()
                    .filter(collateral -> StringUtils.equalsIgnoreCase(collateralId, collateral.getCollateralId()))
                    .map(collateral -> collateral.getPropertyArea())
                    .collect(Collectors.toList());
            if(CollectionUtils.isNotEmpty(tmpList)){
                area = tmpList.get(0);
            }
        }
        if( StringUtils.isEmpty(area)) area = Constant.ZERO;
        return area;
    }

    private static String getAddressLine1(CustomerAddress propertyAddress) {
        /*  Property_No + Building_No + Street_Sector  = Address line 1
                    District = Address line 2
                */
        StringBuilder sb = new StringBuilder();
        sb.append(GngUtils.setValueForNull(propertyAddress.getFlatNo()))
                .append(GngUtils.setValueForNull(propertyAddress.getAddressLine1()))
                .append(GngUtils.setValueForNull(propertyAddress.getAddressLine2()))
                .append(GngUtils.setValueForNull(propertyAddress.getStreet()));
        return sb.toString();
    }

    private static int getBranchId(String institutionId, String branchid) {
        if(Institute.isInstitute(institutionId, Institute.SBFC) ){
            // SBFC has branchID as institutionId_branchId
            String[] splitBranchId = branchid.split("_");
            try {
                return Integer.parseInt(splitBranchId[1]);
            } catch (NumberFormatException ne){
                logger.error("{}",ne.getStackTrace());
                return 0;
            }
        }
        return 0;
    }

    public static Object buildCheckRequest(String acknowledgementId, String institutionId) {
        CheckRequest checkRequest = new CheckRequest();
        checkRequest.setAcknowledgementId(acknowledgementId);
        if( StringUtils.isNotEmpty(institutionId)){
            checkRequest.setInstitutionId(institutionId);
        }
        return checkRequest;
    }

    public static Object buildImageRequest(String imageId, String acknowledgementId, String institutionId) {
        ImageRequest imageRequest = new ImageRequest();
        imageRequest.setImageId(imageId);
        imageRequest.setAcknowledgementId(acknowledgementId);
        if( StringUtils.isNotEmpty(institutionId)){
            imageRequest.setInstitutionId(institutionId);
        }
        return imageRequest;
    }

    public static void populateValuationOutput(ValuationDetails valuationDetails, AdroitVerificationResponse verificationResponse) {
        Payload payload = verificationResponse.getPayload();
        ValuationOutput output = valuationDetails.getValuationOutput();
        if (output == null ){
            output = new ValuationOutput();
            valuationDetails.setValuationOutput(output);
        }
        setPropertyOwner(output, payload.getCurrentPropertyOwner());
        setPropertyAddress( output, payload.getPropertyAddress());
        setInspectionAddress(output, payload.getInspectionAddress());
        output.setPropertyWithinMC(payload.isPropertyWithinMC());
        output.setDemarcation(payload.isDemarcation());
        output.setBranchdistance(payload.getDistanceFromBranch());
        output.setPlanSanctioned(payload.isPlanSanctioned());
        output.setBuildingSanctioned(payload.isBuildingSanctioned());
        output.setPropertyUsage(payload.getPropertyUsage());
        output.setPropertyNature(payload.getPropertyNature());
        output.setPropertyOccupancy(payload.getPropertyOccupancy());
        output.setNoOfTenants(payload.getNoOfTenants());
        output.setLocalityType(payload.getLocalityType());
        output.setPropertyCondition(payload.getPropertyCondition());
        output.setResidualAge(payload.getResidualAge());
        output.setLandArea(payload.getLandArea());
        output.setLandRate(payload.getLandRate());
        output.setBuiltUpArea(payload.getBuiltUpArea());
        output.setPermissibleArea(payload.getPermissibleArea());
        output.setAdoptedArea(payload.getAdoptedArea());
        output.setConstructionRate(payload.getConstructionRate());
        output.setGovtApprovedRate(payload.getGovtApprovedRate());
        output.setCurrentMarketValue(payload.getCurrentMarketValue());
        output.setRemarks(payload.getRemarks());
        //payload.isAvailabilityOfTransport()
        //payload.getClassOfLocality()
        //payload.getBoundariesMatching()
        //setAccDetails()
        //payload.getApprovedUsageOfProp()
        //payload.getActualUsageOfProp()
        //payload.getTypeOfStructure()
        //payload.getNoOfFloors()
        //payload.getLocationOfPlot()
        //payload.getPropertyConnections()
        //payload.getAmenitiesTransportProximity()
        //payload.getDevOfSurroundingArea()
        //setApprovalDetails
        //payload.getTotalBuiltUpArea());
        //payload.getDeviationBylaw()
        //payload.getIsMcdDemolition()
        //payload.getQualityOfConstruction()
        //payload.getAgeOfProperty()
        //payload.getMaintenanceOfProperty()
        //payload.getProjectedLife()
        //payload.getBuildingHeight()
        //setConstructionDetails()

        output.setLandRate(payload.getLandApplicableRate());
        //payload.getLandShareValue()
        //payload.getConstructionApplicableRate());
        //payload.getConstructionValue());
        //payload.getFairMarketValue()
        //payload.getRentedValue()
        //payload.getForcedSaleValue();
        //payload.getInsurableValue();
        //payload.getPropAreaOfConstruction());
        //payload.getPropRateOfConstruction());
        //payload.getAddrLandmark()
        //payload.getDistanceFromCity()
        //payload.setAddrZone();
        //payload.getCurrentMarketRate());
        //payload.getAuthorizedPerson()
        output.setImageList(payload.getImageList());
        valuationDetails.setStatus (ThirdPartyVerification.Status.VERIFIED.name());
    }

    private static void setPropertyOwner(ValuationOutput output, CurrentPropertyOwner currentPropertyOwner) {
        Name name = new Name();
        output.setCurrentPropertyOwner(name);
        name.setFirstName(GngUtils.setValueForNull(currentPropertyOwner.firstName));
        name.setMiddleName(GngUtils.setValueForNull(currentPropertyOwner.middleName));
        name.setLastName(GngUtils.setValueForNull(currentPropertyOwner.lastName));
        name.setPrefix(GngUtils.setValueForNull(currentPropertyOwner.prefix));
        name.setSuffix(GngUtils.setValueForNull(currentPropertyOwner.suffix));
    }
    private static void setPropertyAddress(ValuationOutput output, PropertyAddress propertyAddress) {
        CustomerAddress address = new CustomerAddress();
        output.setPropertyAddress(address);

        address.setAddressLine1(GngUtils.setValueForNull(propertyAddress.getLine1()));
        address.setAddressLine2(GngUtils.setValueForNull(propertyAddress.getLine2()));
        address.setCity(GngUtils.setValueForNull(propertyAddress.getCity()));
        address.setPin(propertyAddress.getPinCode());
        address.setState(GngUtils.setValueForNull(propertyAddress.getState()));
        address.setCountry(GngUtils.setValueForNull(propertyAddress.getCountry()));
        address.setVillage(GngUtils.setValueForNull(propertyAddress.getVillage()));
        address.setDistrict(GngUtils.setValueForNull(propertyAddress.getDistrict()));
        address.setLandMark(GngUtils.setValueForNull(propertyAddress.getLandMark()));
        address.setAccommodation(GngUtils.setValueForNull(propertyAddress.getAccm()));
        address.setTimeAtAddress(propertyAddress.getTimeAtAddr());
        address.setAddressType(GngUtils.setValueForNull(propertyAddress.getAddrType()));
        address.setResidenceAddressType(GngUtils.setValueForNull(propertyAddress.getResAddrType()));
        address.setOfficeType(GngUtils.setValueForNull(propertyAddress.getOfficeType()));
        address.setMonthAtCity(propertyAddress.getMonthAtCity());
        address.setMonthAtAddress(propertyAddress.getMonthAtAddr());
        address.setRentAmount(propertyAddress.getRentAmt());
        address.setFlatNo(GngUtils.setValueForNull(propertyAddress.getFlatNo()));
        address.setStreet(GngUtils.setValueForNull(propertyAddress.getStreet()));
        address.setLocality(GngUtils.setValueForNull(propertyAddress.getLocality()));
        address.setYearAtCity(propertyAddress.getYearAtCity());
        address.setNegativeArea(GngUtils.setValueForNull(propertyAddress.getNegativeArea()));
        address.setNegativeAreaReason(GngUtils.setValueForNull(propertyAddress.getNegativeAreaReason()));
        address.setLatitude(propertyAddress.getLatitude());
        address.setLongitude(propertyAddress.getLongitude());
    }
    private static void setInspectionAddress(ValuationOutput output, InspectionAddress propertyAddress) {
        CustomerAddress address = new CustomerAddress();
        output.setInspectionAddress(address);

        address.setAddressLine1(GngUtils.setValueForNull(propertyAddress.getLine1()));
        address.setAddressLine2(GngUtils.setValueForNull(propertyAddress.getLine2()));
        address.setCity(GngUtils.setValueForNull(propertyAddress.getCity()));
        address.setPin(propertyAddress.getPinCode());
        address.setState(GngUtils.setValueForNull(propertyAddress.getState()));
        address.setCountry(GngUtils.setValueForNull(propertyAddress.getCountry()));
        address.setVillage(GngUtils.setValueForNull(propertyAddress.getVillage()));
        address.setDistrict(GngUtils.setValueForNull(propertyAddress.getDistrict()));
        address.setLandMark(GngUtils.setValueForNull(propertyAddress.getLandMark()));
        address.setAccommodation(GngUtils.setValueForNull(propertyAddress.getAccm()));
        address.setTimeAtAddress(propertyAddress.getTimeAtAddr());
        address.setAddressType(GngUtils.setValueForNull(propertyAddress.getAddrType()));
        address.setResidenceAddressType(GngUtils.setValueForNull(propertyAddress.getResAddrType()));
        address.setOfficeType(GngUtils.setValueForNull(propertyAddress.getOfficeType()));
        address.setMonthAtCity(propertyAddress.getMonthAtCity());
        address.setMonthAtAddress(propertyAddress.getMonthAtAddr());
        address.setRentAmount(propertyAddress.getRentAmt());
        address.setFlatNo(GngUtils.setValueForNull(propertyAddress.getFlatNo()));
        address.setStreet(GngUtils.setValueForNull(propertyAddress.getStreet()));
        address.setLocality(GngUtils.setValueForNull(propertyAddress.getLocality()));
        address.setYearAtCity(propertyAddress.getYearAtCity());
        address.setNegativeArea(GngUtils.setValueForNull(propertyAddress.getNegativeArea()));
        address.setNegativeAreaReason(GngUtils.setValueForNull(propertyAddress.getNegativeAreaReason()));
        address.setLatitude(propertyAddress.getLatitude());
        address.setLongitude(propertyAddress.getLongitude());
    }


}
