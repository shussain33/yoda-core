package com.softcell.service;

import com.softcell.gonogo.model.core.DedupeRemarkRequest;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.UpdateCamRequest;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.request.DeviationRequest;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.ops.CustomerCreditRequest;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.ssl.perfios.PerfiosRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

/**
 * @author yogeshb
 */
public interface DataEntryManager {
    /**
     * @param goNoGoCustomerApplication
     * @param stepId
     * @param httpRequest
     * @return
     */
    BaseResponse save(
            ApplicationRequest goNoGoCustomerApplication, String stepId,
            HttpServletRequest httpRequest) ;

    BaseResponse saveCamDetails(
            CamDetailsRequest camDetailsRequest, String camType, boolean isGetCamApi, HttpServletRequest httpRequest);

    BaseResponse saveVerificationDetails(
            VerificationRequest goNoGoCustomerApplication, String verificationType,
            HttpServletRequest httpRequest) ;

    BaseResponse getVerificationDetails(VerificationRequest verificationRequest);

    BaseResponse submit(ApplicationRequest applicationRequest, String stepId, HttpServletRequest httpRequest);

    void updateDM(ApplicationRequest applicationRequest);

    BaseResponse savePropertyVisitDetails(PropertyVisitRequest propertyVisitRequest, HttpServletRequest httpRequest);

    BaseResponse getPropertyVisitDetails(PropertyVisitRequest propertyVisitRequest);

    BaseResponse savePersonalDiscussion(ApplicationRequest applicationRequest, HttpServletRequest httpRequest);

    BaseResponse getRepaymentDetails(RepaymentRequest repaymentRequest);

    BaseResponse saveRepaymentDetails(RepaymentRequest repaymentRequest, HttpServletRequest httpRequest);

    BaseResponse saveLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest, HttpServletRequest httpRequest);

    BaseResponse getLegalVerificationDetails(LegalVerificationRequest legalVerificationRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse saveCustomerCreditDocs(CustomerCreditRequest customerCreditRequest, HttpServletRequest httpRequest);

    BaseResponse getCustomerCreditDocs(CustomerCreditRequest customerCreditRequest, HttpServletRequest httpRequest);

    Object generateAndSetRefId(ApplicationRequest applicationRequest);

    BaseResponse saveListOfDocs(ListOfDocsRequest listOfDocsRequest, HttpServletRequest httpRequest);

    BaseResponse getListOfDocs(ListOfDocsRequest listOfDocsRequest, HttpServletRequest httpRequest);

    BaseResponse saveEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest);

    BaseResponse getEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest);


    BaseResponse saveLoanChargesDetails(LoanChargesRequest loanChargesRequest, HttpServletRequest httpRequest);

    BaseResponse getLoanChargesDetails(LoanChargesRequest loanChargesRequest, HttpServletRequest httpRequest);

    BaseResponse triggerVerification(VerificationRequest verificationRequest, HttpServletRequest httpRequest);

    BaseResponse saveValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest);

    BaseResponse deleteValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest);

    BaseResponse getValuationDetails(ValuationRequest valuationRequest, HttpServletRequest httpRequest);

    boolean updateStageId(ApplicationRequest applicationRequest, String applicationStatus) throws Exception;

    BaseResponse changeApplicationLockStatus(ApplicationLockRequest applicationLockRequest, HttpServletRequest httpRequest);

    BaseResponse saveDeviation(DeviationRequest deviationRequest);

    BaseResponse approveAllDeviation(DeviationRequest deviationRequest);

    BaseResponse getDeviation(DeviationRequest deviationRequest);

    BaseResponse getCamDetailsByCamType(CamDetailsRequest camDetailsRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse updateCamDetails(UpdateCamRequest updateCamRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse saveDMDetails(DMRequest dmRequest, HttpServletRequest httpRequest);

    BaseResponse getDMDetails(DMRequest dmRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse saveSanctionConditions(SanctionConditionRequest sanctionConditionRequest, HttpServletRequest httpRequest);

    BaseResponse getSanctionConditions(SanctionConditionRequest sanctionConditionRequest, HttpServletRequest httpRequest);

    BaseResponse saveDedupeRemarks(DedupeRemarkRequest dedupeRemarkRequest);
    BaseResponse getDedupeRemarks(DedupeRemarkRequest dedupeRemarkRequest);

    BaseResponse getDocumentByFileName(DocumentRequest documentRequest, HttpServletRequest httpRequest);

    BaseResponse updateCompletedInfoGonogo(UpdateGNGRequest updateGNGRequest, HttpServletRequest httpRequest) throws Exception;

    void externalAPICall(GoNoGoCustomerApplication goNoGoCustomerApplication, ApplicationRequest applicationRequest);

    BaseResponse verifyApproval(ApprovalVerificationRequest amountVerificationRequest);

    BaseResponse deleteCollateral(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception;

    @Deprecated
    BaseResponse deleteCoApplicant(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse deleteCoApplicantData(DeleteCoApplicantRequest request, HttpServletRequest httpRequest);

    BaseResponse savePerfiosData(PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getPerfiosData(PerfiosRequest perfiosRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getMifinLogData(PerfiosRequest mifinRequest, HttpServletRequest httpRequest);

    BaseResponse getCoApplicantCibilScore(String institutionId , List<String> refIds);

    List<String> migrateCibilDataForApplications(List<GoNoGoCustomerApplication> goNoGoCustomerApplication);

    BaseResponse migrateCoApplicantPLBSData(String institutionId , List<String> refIds);

    BaseResponse saveOriginationDetails(OriginationRequest originationRequest, HttpServletRequest httpRequest);

    BaseResponse fetchOriginationDetails(OriginationRequest originationRequest, HttpServletRequest httpRequest);

    BaseResponse migrateApplicationBranches(DataMigrationRequest dataMigrationRequest, HttpServletRequest httpRequest);

    BaseResponse verifyStageSubmition(StageSubmitVerificationRequest submitVerificationRequest) ;

    BaseResponse calculatePremiumAmount(InsuranceRequest insuranceRequest) throws Exception;

    BaseResponse insuranceDataPush(String refID, Header header) throws Exception;

    BaseResponse populateRepaymentDetails(CamDetailsRequest camDetailsRequest);

    BaseResponse saveLoginData(
            LoginDataRequest loginDataRequest,
            HttpServletRequest httpRequest) throws Exception;

    ApplicationRequest mappingApplicationRequest(LoginDataRequest loginDataRequest) throws Exception;

    public Collection<Error> validateLoginDataRequest(LoginDataRequest loginDataRequest, String functionName);

    BaseResponse persistRepayment(BankingDetailsRequest bankingDetailsRequest);

    void updateDM(ApplicationRequest applicationRequest, Repayment repayment);

    BaseResponse saveOCRData(OCRRequest ocrRequest) throws Exception;

    BaseResponse submitOCRData(OCRRequest ocrRequest) throws Exception;

    BaseResponse resetOCRData(OCRRequest ocrRequest) throws Exception;

    BaseResponse getOCRData(OCRRequest ocrRequest) throws Exception;

    BaseResponse resetApplicationStatus(LoginDataRequest loginDataRequest);

    BaseResponse saveDigiPLEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest);

    BaseResponse getDigiPLEligibilityDetails(EligibilityRequest eligibilityRequest, HttpServletRequest httpRequest);

    BaseResponse saveMismatchDetails(MismatchRequest MismatchRequest, HttpServletRequest httpRequest);

    BaseResponse getMismatchDetails(MismatchRequest MismatchRequest, HttpServletRequest httpRequest);

    BaseResponse updateKycDetails(KycUpdateRequest kycUpdateRequest);

    BaseResponse getKycDetails(KycUpdateRequest kycUpdateRequest);

    BaseResponse getApplicationBucket(LoginDataRequest loginDataRequest);

    BaseResponse fetchCRMDedupe(LoginDataRequest loginDataRequest);

    BaseResponse getActivityLogsDetails(UserActivityRequest userActivityRequest) throws Exception;
}
