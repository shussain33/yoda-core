package com.softcell.service.servicecoderegistration.impl;

import com.softcell.constants.CustomHttpStatus;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.serialnumbervalidation.voltas.ServiceCodeRegResponse;
import com.softcell.gonogo.servicecoderegistration.ServiceCodeRegistrationRequest;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.servicecoderegistration.ServiceCodeRegistrationManager;
import com.softcell.utils.GngUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * created by prasenjit wadmare 0n 21/12/2017
 */

@Service
public class ServiceCodeRegistrationManagerImpl implements ServiceCodeRegistrationManager {

    private static final Logger logger = LoggerFactory.getLogger(ServiceCodeRegistrationManagerImpl.class);

    @Autowired
    private WorkFlowCommunicationManagerImpl workFlowCommunicationManager;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Override
    public BaseResponse getVoltasHelpDeskNumber(ServiceCodeRegistrationRequest serviceCodeRegistrationRequest) throws Exception {

        BaseResponse baseResponse = null;
        Collection<Error> errors;
        ServiceCodeRegResponse serviceCodeRegResponse = new ServiceCodeRegResponse();

        //getting Voltas config from WFJobCommDomain
        WFJobCommDomain voltasConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                serviceCodeRegistrationRequest.getHeader().getInstitutionId(), UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.toValue());

        if (null != voltasConfig) {

            serviceCodeRegResponse.setHelpDeskNumber(voltasConfig.getHelpDeskNumber());

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, serviceCodeRegResponse);

        } else {
            //configuration not found.
            logger.warn("Configuration not found for {}", UrlType.VOLTAS_SERVICE_CODE_REGISTRATION.name());
            errors = GngUtils.getConfigurationNotFoundErrorList();

            baseResponse = GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
        }

        return baseResponse;
    }


}
