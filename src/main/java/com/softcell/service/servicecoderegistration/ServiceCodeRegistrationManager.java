package com.softcell.service.servicecoderegistration;

import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.servicecoderegistration.ServiceCodeRegistrationRequest;

/**
 * created by prasenjit wadmare 0n 21/12/2017
 */

public interface ServiceCodeRegistrationManager {

    /**
     *
     * @param serviceCodeRegistrationRequest
     * @return
     */
    BaseResponse getVoltasHelpDeskNumber(ServiceCodeRegistrationRequest serviceCodeRegistrationRequest) throws Exception;

}
