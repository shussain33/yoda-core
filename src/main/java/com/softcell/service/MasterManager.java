package com.softcell.service;

import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.masters.DropdownMasterRequest;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.master.DealerRankingRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;


/**
 * @author yogeshb
 */
public interface MasterManager {

    /**
     * @param query
     * @return
     */
    BaseResponse getAssetModelMasterListByTextSearch(
            String query);

    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getManufacturerList(
            ApplicationMetadataRequest metadataRequest);

    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getManufacturerDetailsList(ApplicationMetadataRequest metadataRequest);

    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getCategoryList(
            ApplicationMetadataRequest metadataRequest);

    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getMakeList(ApplicationMetadataRequest metadataRequest);


    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getAssetModelMasterList(ApplicationMetadataRequest metadataRequest);

    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getModelNumberList(
            ApplicationMetadataRequest metadataRequest);

    /**
     *
     * @param dealerRankingRequest
     * @return
     */
    BaseResponse getDealerRanking(
            DealerRankingRequest dealerRankingRequest);


    /**
     *
     * @param metadataRequest
     * @return
     */
    BaseResponse getListOfModelsWithOtherDetails(ApplicationMetadataRequest metadataRequest);

    BaseResponse saveDropDownMaster(DropdownMaster dropdownMaster);

    BaseResponse getDropDownMaster(DropdownMasterRequest dropdownMaster);
}
