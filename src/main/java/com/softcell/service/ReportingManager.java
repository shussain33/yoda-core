package com.softcell.service;


import com.softcell.config.reporting.ReportingConfigurations;
import com.softcell.gonogo.exceptions.ReportConfigNotInDb;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.SerialNumberInfoLogRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.reporting.domains.CreditReportRequest;
import com.softcell.reporting.domains.FirstLastLoginReportRequest;
import com.softcell.reporting.domains.OtpReportRequest;
import com.softcell.reporting.domains.SalesReportRequest;
import com.softcell.reporting.request.LosUtrUpdateReportRequest;
import com.softcell.reporting.request.ReportRequest;
import com.softcell.reporting.request.ReportSearchRequest;

import java.util.Date;

/**
 * @author kishor
 */
public interface ReportingManager {

    /**
     * @param reportRequest All fields are mandatory.
     * @return All system fields which are available in GoNoGo system.
     */
    BaseResponse getReportDimensions(ReportRequest reportRequest) throws ReportConfigNotInDb;

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return will return data for particular type of report.
     */
    BaseResponse getCustomReports(
            ReportingConfigurations reportingConfigurations);

    /**
     * @param reportSearchRequest ReportSearchRequest  is use to find report name
     *                            by user query.
     * @return Custom Report Configuration.
     */
    BaseResponse getReportNames(ReportSearchRequest reportSearchRequest);

    /**
     * @param reportRequest All fields are mandatory.
     * @return All system fields which are available in GoNoGo system.
     */
    BaseResponse getReportConfiguration(ReportRequest reportRequest);

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return CSV FILE..
     * @throws Exception
     */
    BaseResponse getCsvReport(ReportingConfigurations reportingConfigurations) throws Exception;

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return will return data for particular type of report.
     */
    BaseResponse saveCustomReports(
            ReportingConfigurations reportingConfigurations);

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return will return data for particular type of report.
     */
    BaseResponse saveDefaultReports(
            ReportingConfigurations reportingConfigurations);

    /**
     * @param reportingConfigurations The Reporting app is used to filter the result set
     *                                of data. Only configured data set will be return, through this
     *                                method. Institution Id, product type,pagination, flat report
     *                                app are mandatory .All other field will used for to
     *                                minimized result set.
     * @return will return data for particular type of report.
     */
    BaseResponse previewCustomReports(
            ReportingConfigurations reportingConfigurations);

    /**
     * @param startDate
     * @param endDate
     * @return
     */
    byte[] getChannelWiseReport(Date startDate, Date endDate, String product, String institutionId) throws Exception;


    /**
     * method to generate sales report based on sales report request
     *
     * @param reportRequest
     * @return
     * @throws Exception
     */
    BaseResponse generateSalesIncentiveReport(SalesReportRequest reportRequest) throws Exception;


    /**
     * @param reportRequest
     * @return
     * @throws Exception
     */
    byte[] generateZippedSalesIncentiveReport(SalesReportRequest reportRequest) throws Exception;

    /**
     * @param reportRequest
     * @return
     * @throws Exception
     */
    byte[] generateZippedSalesReport(SalesReportRequest reportRequest) throws Exception;


    /**
     * @return
     * @throws Exception
     */
    BaseResponse generateOtpReport(OtpReportRequest otpReportRequest) throws Exception;


    /**
     * @param otpReportRequest
     * @return
     * @throws Exception
     */
    byte[] generateZippedOtpReport(OtpReportRequest otpReportRequest) throws Exception;

    /**
     * @return
     * @throws Exception
     */
    BaseResponse generateFirstLastLoginReport(FirstLastLoginReportRequest firstLastLoginReportRequest) throws Exception;

    /**
     * @param firstLastLoginReportRequest
     * @return
     * @throws Exception
     */
    byte[] generateZippedFirstLastLoginReport(FirstLastLoginReportRequest firstLastLoginReportRequest) throws Exception;


    /**
     * @param creditReportRequest
     * @return
     * @throws Exception
     */
    byte[] generateZippedCreditReport(CreditReportRequest creditReportRequest) throws Exception;


    /**
     * @return
     */
    BaseResponse createTreeViewForAssetModelReport();

    /**
     * @return
     */
    BaseResponse getSerialLog();

    /**
     * @param refID
     * @param vendor
     * @return
     */
    BaseResponse getSerialByIDLog(String refID, String vendor) throws Exception;

    /**
     *
     *
     * @param institutionId
     * @param vendor
     * @param startDate
     * @param endDate
     * @return
     */
    byte[] getSerialNumberLogZipReportByVendor(String institutionId, String vendor, Date startDate, Date endDate);


    /**
     * @param dateStartFrom
     * @param endDate
     * @param product
     * @param institutionId
     * @return
     */
    byte[] getDeviceReport(Date dateStartFrom, Date endDate, String product,
                                 String institutionId) throws Exception;

    /**
     * @param refID
     * @param vendor
     * @return
     */
    BaseResponse getSerialNumberRequestLog(String refID, String vendor);

    /**
     * @return
     */
    BaseResponse getSerialNumberRequestLog();

    /**
     * @param refID
     * @return
     */
    BaseResponse getMailLogReport(String refID);

    /**
     * @return
     */
    BaseResponse getMailLogReport();

    /**
     *
     * @param losUtrUpdateReportRequest
     * @return
     */
    byte[] getLosUtrUpdateReport(LosUtrUpdateReportRequest losUtrUpdateReportRequest) throws SystemException;

    /**
     *
     * @param serialNumberInfoLogRequest
     * @return
     * @throws SystemException
     */
    byte[] getSerialNumberRollBackReportByVendor(SerialNumberInfoLogRequest serialNumberInfoLogRequest) throws SystemException;

    /**
     *
     * @param aadhaarLogRequest
     * @return
     */
    BaseResponse getAadhaarLogReport(AadhaarLogRequest aadhaarLogRequest);

    /**
     *
     * @param dmzRequest
     * @return
     */
    BaseResponse getMbPushLogs(DmzRequest dmzRequest);

    /**
     *
     * @param refId
     * @return
     */
    BaseResponse getTvrLogs(String refId);

    /**
     *
     *
     * @param institutionId
     * @param product
     * @param startDate
     * @param endDate
     * @return
     */
    byte[] getAccountNumberLogZipReport(String institutionId, String product, Date startDate, Date endDate);

    /**
     *
     *
     * @param institutionId
     * @param product
     * @param startDate
     * @param endDate
     * @return
     */
    byte[] getSerialNumberApplicableVendorLogZipReport(String institutionId, Date startDate, Date endDate);

    /**
     *
     * @param refId
     * @return
     */
    BaseResponse getSBFCLMSLogByRefId(String refId);

    /**
     *
     * @param instId
     * @param startDate
     * @param endDate
     * @return
     */
    byte[] getSBFCLMSLogZipReport(String instId, Date startDate, Date endDate);
}
