package com.softcell.service;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.dmz.DmzServiceCheckRequest;

/**
 * This service is written
 *
 * @author bhuvneshk
 */
public interface DMZConnector {

    /**
     * This connects to reliance digital service
     * Parameter required is gonogoRefId
     *
     * @param dmzRequest
     * @return
     */
    BaseResponse connectRelianceDigitalService(DmzRequest dmzRequest) throws Exception;

    /**
     * This connects to dealer
     * Parameter required is gonogoRefId
     *
     * @param dmzRequest
     * @return
     */
    BaseResponse connectDealer(DmzRequest dmzRequest) throws Exception;

    /**
     * Method will connect to dmz connector and
     * it will post gng data to los
     *
     * @param dmzRequest
     * @return
     * @throws Exception
     */
    BaseResponse postGNGDataToLos(DmzRequest dmzRequest) throws Exception;

    /**
     * Method will connect to dmz connector and
     * it will post gng data to los
     *
     * @param dmzRequest
     * @return
     */
    BaseResponse postMBDataToLos(DmzRequest dmzRequest) throws  Exception;

    /**
     * This method will be used to check if service has already used
     * @param dmzServiceCheckReq
     * @return
     */
    BaseResponse getServiceStatus(DmzServiceCheckRequest dmzServiceCheckReq) throws Exception;

    /**
     *  @param referenceId
     * @param institutionId
     * @param goNoGoCustomerApplication
     */
    boolean pushTvr(String referenceId, String institutionId, GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     *  @param dmzRequest
     *
     */
    BaseResponse pushTkil(DmzRequest dmzRequest) throws Exception;

    /**
     *  @param refID
     *
     */
    BaseResponse getTkilLogs(String refID) throws Exception;

}
