package com.softcell.service;

import com.softcell.gonogo.model.request.AppDisbursalRequest;
import com.softcell.gonogo.model.request.GenSapFileRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by anupamad on 10/8/17.
 */
public interface DisbursementManager {
    BaseResponse disburseLoan(AppDisbursalRequest appDisbursalRequest) throws Exception;
    BaseResponse genSapFile(GenSapFileRequest genSapFileRequest) throws Exception;
}
