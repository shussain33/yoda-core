package com.softcell.service;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

public interface MultiProductManager {
    /**
     * @param multiProductRequest
     * @return
     */
    BaseResponse getProductCount(MultiProductRequest multiProductRequest) throws Exception;

    /**
     * @param multiProductRequest
     * @return
     */
    BaseResponse addNewProduct(MultiProductRequest multiProductRequest) throws Exception;

    /**
     * @param header
     * @return
     */
    BaseResponse isDealerSubventionWaivedOff(Header header);
}
