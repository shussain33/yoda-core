package com.softcell.service;

import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.TopUpDedupeRequest;
import com.softcell.gonogo.model.request.AdminLogRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.mifin.topup.TopUpDedupeResponse;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HEAD;
import java.util.List;
import java.text.ParseException;

public interface MifinManager {


    //MIFIN 1st api Applicant basic info call
    BaseResponse getTopupDedupeResponse(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest);

    BaseResponse getMifinApplicantData(String refId, HttpServletRequest httpRequest);

    //MIFIN 2nd api dedupe INFO
    //BaseResponse sendDatatoMifinDedupe(String refId, HttpServletRequest httpRequest) throws Exception;

    BaseResponse sendDatatoMifinDedupe(TopUpDedupeRequest request, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getMifinDedupeDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest);


    BaseResponse sendMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest,HttpServletRequest httpRequest) throws Exception;

    BaseResponse getMiFinLoanDetail(String refId, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getDedupe(ApplicationRequest applicationRequest, Applicant applicant,
                           List<CoApplicant> coApplicantList, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getDedupeData(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception;

    BaseResponse getMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest, HttpServletRequest httpRequest) throws ParseException;

    BaseResponse populateMifinDedupeData(TopUpDedupeResponse topUpDedupeResponse,String refId , String institutionId, TopUpDedupeRequest topUpDedupeRequest) throws ParseException;

    BaseResponse getMifinLmsDetail(TopUpDedupeRequest topUpDedupeRequest,HttpServletRequest httpRequest) throws Exception;

    BaseResponse sendEmiCalRequest(AdminLogRequest adminLogRequest, HttpServletRequest httpRequest);
}