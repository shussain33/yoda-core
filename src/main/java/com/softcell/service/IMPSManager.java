package com.softcell.service;

import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by sampat on 3/11/17.
 */

public interface IMPSManager {

     /**
      * @param impsRequest
      * @return
      */
    BaseResponse validateAccountNumber(IMPSRequest impsRequest) throws Exception;

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    BaseResponse getAccountValidationAttempt(String institutionId, String refID);

    /**
     * @param institutionId
     * @param refID
     * @return
     */
    BaseResponse getValidatedBanks(String institutionId, String refID);

    /**
     * @param institutionId
     * @return
     */
    BaseResponse checkAccountNumberValidationFlag(String institutionId);

    /**
     * @param bankingDetails
     * @param institutionId
     * @param refID
     */
    BaseResponse updateBankingDetailsImps(BankingDetails bankingDetails, String institutionId, String refID);

}
