package com.softcell.service;

import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.request.loyaltycard.LoyaltyCardStatusRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * Created by mahesh on 12/12/17.
 */
public interface LoyaltyCardManager {
    /**
     *
     * @param loyaltyCardStatusRequest
     * @return
     */
    BaseResponse checkLoyaltyCardStatus(LoyaltyCardStatusRequest loyaltyCardStatusRequest);

    /**
     *
     * @param loyaltyCardDetails
     * @return
     */
    BaseResponse updateLoyaltyCardDetails(LoyaltyCardDetails loyaltyCardDetails);

    /**
     *
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse fetchLoyaltyCardDetails(String refId, String institutionId);

    /**
     *
     * @param dealerId
     * @param institutionId
     * @return
     */
    BaseResponse getApplicableLoyaltyCardProviders(String dealerId, String institutionId) throws Exception;

}
