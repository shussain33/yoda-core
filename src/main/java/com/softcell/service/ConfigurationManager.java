package com.softcell.service;

import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.serialnumbervalidation.ValidationVendorsConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.dms.MasterMappingConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.los.LosChargeConfig;
import com.softcell.gonogo.model.response.GenericResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.VersionRequest;
import com.softcell.gonogo.model.security.Versions;

import java.util.List;

/**
 * @author yogeshb
 */
public interface ConfigurationManager {

    /**
     * @param versionRequest
     * @return
     */
    GenericResponse setVersion(VersionRequest versionRequest);

    /**
     * @param versionRequest
     * @return
     */
    GenericResponse removeVersion(VersionRequest versionRequest);

    /**
     * @return
     */
    List<Versions> getVersionConguration();

    /**
     * @param institutionId
     * @param version
     * @return
     */
    boolean isValidVersion(String institutionId, String version);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    BaseResponse addInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    BaseResponse removeInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    BaseResponse updateInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param institutionProductConfiguration
     * @return
     */
    List<InstitutionProductConfiguration> getInstitutionProductConfig(
            InstitutionProductConfiguration institutionProductConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse addTemplateConfiguration(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse getTemplateConfiguration(
            TemplateConfiguration templateConfiguration);

    /**
     * @param institutionId
     * @param productId
     * @param templateName
     * @return
     */
    TemplateConfiguration getTemplateConfiguration(String institutionId,
                                                   String productId, String templateName);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse updateTemplatePath(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse updateTemplateLogo(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse enableTemplate(
            TemplateConfiguration templateConfiguration);

    /**
     * @param templateConfiguration
     * @return
     */
    BaseResponse disableTemplate(
            TemplateConfiguration templateConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse addEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse updateEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse disableEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse enableEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse getAllEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse addAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     * @param emailConfiguration
     * @return
     */
    BaseResponse deleteAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     *
     * @param validationVendorsConfiguration
     * @return
     */
    BaseResponse addValidationVendors(ValidationVendorsConfiguration validationVendorsConfiguration);

    /**
     *
     * @return
     */
    BaseResponse getValidationVendors(String institutionId);

    /**
     *
     * @param validationVendorsConfiguration
     * @return
     */
    BaseResponse deleteValidationVendors(ValidationVendorsConfiguration validationVendorsConfiguration);

    /**
     *
     * @return
     */
    BaseResponse getCacheData();

    /**
     *
     * @return
     */
    BaseResponse dropCache();

    /**
     *
     * @param key
     * @return
     */
    BaseResponse dropCache(String key);

    /**
     *
     * @param emailConfiguration
     * @return
     */
    BaseResponse addSecondaryAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     *
     * @param emailConfiguration
     * @return
     */
    BaseResponse deleteSecondaryAttachmentsInEmailConfiguration(EmailConfiguration emailConfiguration);

    /**
     *
     * @param loyaltyCardConfiguration
     * @return
     */
    BaseResponse addLoyaltyCardConfig(LoyaltyCardConfiguration loyaltyCardConfiguration);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse findLoyaltyCardConfig(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse deleteLoyaltyCardConfig(String institutionId ,String productId,LoyaltyCardType loyaltyCardType);

    /**
     *
     * @param loyaltyCardConfiguration
     * @return
     */
    BaseResponse updateLoyaltyCardConfig(LoyaltyCardConfiguration loyaltyCardConfiguration);

    /**
     *
     * @param dmsFolderConfigurationList
     * @return
     */
    BaseResponse addDmsFolderConfiguration( List<DmsFolderConfiguration> dmsFolderConfigurationList);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse getDmsFolderConfiguration(String institutionId);

    /**
     *
     * @param dmsFolderConfiguration
     * @return
     */
    BaseResponse updateDmsFolderConfiguration(DmsFolderConfiguration dmsFolderConfiguration);

    /**
     *
     * @param institutionId
     * @param indexName
     * @return
     */
    BaseResponse deleteDmsFolderConfiguration(String institutionId, String indexName);

    /**
     * Update the sms templates configuration
     * @param smsTemplateConfiguration
     * @return
     */
    BaseResponse updateSmsTemplateConfig(SmsTemplateConfiguration smsTemplateConfiguration);

    /**
     * add SmsTemplateConfiguration
     * @param smsTemplateConfiguration
     * @return
     */
    BaseResponse addSmsTemplateConfig(SmsTemplateConfiguration smsTemplateConfiguration);

    /**
     * get SmsTemplateConfiguration
     *
     * @param institutionId
     * @return
     */
    BaseResponse getSmsTemplateConfig(String institutionId);

    /**
     * delete SmsTemplateConfiguration
     *
     * @param institutionId
     * @param productId
     * @return
     */
    BaseResponse deleteSmsTemplateConfig(String institutionId, String productId, String smsType);


    /**
     *
     * @param masterMappingConfigurationList
     * @return
     */
    BaseResponse addMasterMappingConfiguration( List<MasterMappingConfiguration> masterMappingConfigurationList);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse getLosChargeConfiguration(String institutionId);

    /**
     *
     * @param losChargeConfig
     * @return
     */
    BaseResponse createLosChargeConfig(LosChargeConfig losChargeConfig);

    /**
     *
     * @param caseCancellationJobConfig
     * @return
     */
    BaseResponse addCaseCancellationConfig(CaseCancellationJobConfig caseCancellationJobConfig);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse getCaseCancellationConfig(String institutionId);

    /**
     *
     * @param institutionId
     * @return
     */
    BaseResponse deleteCaseCancellationConfig(String institutionId);
    /**
     * @param institutionID
     * @return
     */
    BaseResponse getIMPSDomain(String institutionID);

    /**
     * @param impsConfigDomain
     * @return
     */
    BaseResponse addIMPSConfiguration(IMPSConfigDomain impsConfigDomain);

    /**
     * @param impsConfigDomain
     * @return
     */
    BaseResponse deleteIMPSDomain(IMPSConfigDomain impsConfigDomain);

    BaseResponse createQueueGroupConfiguration();

    BaseResponse fetchMasterSchedulerConfig(String institutionId);

    BaseResponse insertMasterSchedulerConfig(MasterSchedulerConfiguration masterSchedulerConfiguration, String userName, String reason, String masterType);
}