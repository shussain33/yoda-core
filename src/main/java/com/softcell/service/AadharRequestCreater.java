package com.softcell.service;


import com.softcell.gonogo.model.core.kyc.request.aadhar.*;
import com.softcell.utils.aadharpidblock.HelperClassAdhar;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class AadharRequestCreater {

	private static final Logger logger = LoggerFactory.getLogger(AadharRequestCreater.class);
	private String timeStamp;
	 HelperClassAdhar prepareAUAData = null;
	 /*static AuthAUADataCreator auaDataCreator = null;
	 static{
		 auaDataCreator = new AuthAUADataCreator(new Encrypter(ReaderJob.commLinksProp.getProperty("ENC_PID_CER_PATH")),false);
	 }*/


	private final String requestInitializationString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	private String tid;
	public String aadharXmlRequest(AadhaarRequestXML aadharRequest, String timeStamp)throws IOException{
			 String aadharXmlRequest_ = aadharXmlRequest_(aadharRequest,timeStamp);
			 return aadharXmlRequest_;
	}

	private String aadharXmlRequest_(AadhaarRequestXML aadharRequest,String timeStamp)throws IOException{
		this.timeStamp = timeStamp;
		AadhaarRequestXML aadharRequestDomain = aadharRequest;
		try {
		/*String dateFormat="ddMMyyhhmmss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);*/

			if (null != aadharRequestDomain) {
				StringBuffer request = new StringBuffer(requestInitializationString);

				String authVer = aadharRequestDomain.getVer();

				if (StringUtils.equalsIgnoreCase(authVer, "2.0")) {
					request.append("<Auth xmlns=\"http://www.uidai.gov.in/authentication/uid-auth-request/2.0\"");
				} else {
					request.append("<Auth xmlns=\"http://www.uidai.gov.in/authentication/uid-auth-request/1.0\"");
				}
				request.append(nodeWithAttrCreator(" uid", aadharRequestDomain.getUid()));

				String tid = aadharRequestDomain.getTid();

				if (StringUtils.equalsIgnoreCase(authVer, "2.0")) {
					request.append(nodeWithAttrCreator("rc", "Y"));
					if (StringUtils.equalsIgnoreCase(aadharRequestDomain.getUses().getBio(), "y")) {
						tid = "registered";
					} else {
						tid = "";
					}
				}


				request.append(nodeWithBlankAttrCreator("tid", tid));
				request.append(nodeWithAttrCreator("ac", aadharRequestDomain.getAc()));
				request.append(nodeWithAttrCreator("sa", aadharRequestDomain.getSa()));
				request.append(nodeWithAttrCreator("ver", aadharRequestDomain.getVer()));
				request.append(nodeWithAttrCreator("txn", aadharRequestDomain.getTxn()));
				request.append(nodeWithAttrCreator("lk", aadharRequestDomain.getLk()));
				request.append(">");


				if (aadharRequestDomain.getTkn() != null && !StringUtils.equalsIgnoreCase(authVer, "2.0")) {
					if (StringUtils.isNotBlank(aadharRequestDomain.getTkn().getType())
							&& StringUtils.isNotBlank(aadharRequestDomain.getTkn().getValue())) {
						request.append("<Tkn type=\"" + aadharRequestDomain.getTkn().getType() + "\" value=\"" + aadharRequestDomain.getTkn().getValue() + "\"/>");
					}

				} else {
//				request.append("<Tkn/>");

				}

				if (aadharRequestDomain.getUses() != null) {
					if (StringUtils.isNotBlank(aadharRequestDomain.getUses().getPi())
							&& StringUtils.isNotBlank(aadharRequestDomain.getUses().getPa())
							&& StringUtils.isNotBlank(aadharRequestDomain.getUses().getPfa())
							&& StringUtils.isNotBlank(aadharRequestDomain.getUses().getPin())
							&& StringUtils.isNotBlank(aadharRequestDomain.getUses().getOtp())) {

						if (StringUtils.isNotBlank(aadharRequestDomain.getUses().getBio())) {

							if (StringUtils.endsWithIgnoreCase(aadharRequestDomain.getUses().getBio(), "y")) {
								if (StringUtils.isNotBlank(aadharRequestDomain.getUses().getBt()))
									request.append("<Uses pi=\"" + aadharRequestDomain.getUses().getPi() + "\" pa=\"" + aadharRequestDomain.getUses().getPa() + "\" pfa=\"" + aadharRequestDomain.getUses().getPfa() + "\" bio=\"" + aadharRequestDomain.getUses().getBio() + "\" bt=\"" + aadharRequestDomain.getUses().getBt() + "\" pin=\"" + aadharRequestDomain.getUses().getPin() + "\" otp=\"" + aadharRequestDomain.getUses().getOtp() + "\"/>");
								else
									throw new Exception("BT IS BLENK WHILE BIO IS Y");
							} else if (StringUtils.endsWithIgnoreCase(aadharRequestDomain.getUses().getBio(), "n")) {
								if (aadharRequestDomain.getTkn() != null && !StringUtils.equalsIgnoreCase(authVer, "2.0")) {
									request.append("<Uses pi=\"" + aadharRequestDomain.getUses().getPi() + "\" pa=\"" + aadharRequestDomain.getUses().getPa() + "\" pfa=\"" + aadharRequestDomain.getUses().getPfa() + "\" bio=\"" + aadharRequestDomain.getUses().getBio() + "\" pin=\"" + aadharRequestDomain.getUses().getPin() + "\" otp=\"" + aadharRequestDomain.getUses().getOtp() + "\"/>");
								} else {
									request.append("<Uses pi=\"" + aadharRequestDomain.getUses().getPi() + "\" pa=\"" + aadharRequestDomain.getUses().getPa() + "\" pfa=\"" + aadharRequestDomain.getUses().getPfa() + "\" bio=\"" + aadharRequestDomain.getUses().getBio() + "\" bt=\"\" pin=\"" + aadharRequestDomain.getUses().getPin() + "\" otp=\"" + aadharRequestDomain.getUses().getOtp() + "\"/>");

								}
							}
						}

					}
				} else {

				}
				//meta start
				MetadataForDeviceAndTxn meta = aadharRequestDomain.getMeta();
				if (meta != null) {
					request.append("<Meta");
					request.append(nodeWithAttrCreator(" udc", meta.getUdc()));
					if (!StringUtils.equalsIgnoreCase(authVer, "2.0")) {
						request.append(nodeWithAttrCreator("fdc", meta.getFdc()));
						request.append(nodeWithAttrCreator("idc", meta.getIdc()));
					}
					if (StringUtils.equalsIgnoreCase(authVer, "2.0")
							&& StringUtils.equalsIgnoreCase(aadharRequestDomain.getUses().getBio(), "y")) {
						request.append(nodeWithAttrCreator("dpId", meta.getDpId()));
						request.append(nodeWithAttrCreator("rdsId", meta.getRdsId()));
						request.append(nodeWithAttrCreator("rdsVer", meta.getRdsVer()));
						request.append(nodeWithAttrCreator("dc", meta.getDc()));
						request.append(nodeWithAttrCreator("mi", meta.getMi()));
						request.append(nodeWithAttrCreator("mc", meta.getMc()));

					} else if (StringUtils.equalsIgnoreCase(authVer, "2.0")) {
						request.append(nodeWithBlankAttrCreator("dpId", ""));
						request.append(nodeWithBlankAttrCreator("rdsId", ""));
						request.append(nodeWithBlankAttrCreator("rdsVer", ""));
						request.append(nodeWithBlankAttrCreator("dc", ""));
						request.append(nodeWithBlankAttrCreator("mi", ""));
						request.append(nodeWithBlankAttrCreator("mc", ""));

					}

					if (StringUtils.equalsIgnoreCase(authVer, "1.6")) {
						request.append(nodeWithAttrCreator("pip", meta.getPip()));
						request.append(nodeWithAttrCreator("lot", meta.getLot()));
						request.append(nodeWithAttrCreator("lov", meta.getLov()));
					}

					request.append("/>");

				} else {
//				request.append("<Meta/>");
				}

				if (aadharRequestDomain.getData() != null) {
//				request.append("<Data type=\""+aadharRequestDomain.getData().getType()+"\">");
//				request.append("<Data>");
					PersonalIdData pid = aadharRequestDomain.getData().getPid();
					if (StringUtils.equalsIgnoreCase(authVer, "2.0")) {

						if (pid != null) {

							pid.setVer("2.0");
						}
					}
					StringBuffer createPidXml = null;
					if (StringUtils.isBlank(pid.getEncPidBlock())) {
						logger.error("Pid->EncPidBlock is empty while creating Auth XML");
					/*	createPidXml = createPidXml(pid);
						prepareAUAData = auaDataCreator.prepareAUAData(null, null, new String(createPidXml));
					*/}


//				System.out.println("createPidXml : "+createPidXml);
//				prepareAUAData = auaDataCreator.prepareAUAData(null, null, new String(createPidXml));

				}

				if (aadharRequestDomain.getSkey() != null) {
					if (StringUtils.isNotBlank(aadharRequestDomain.getSkey().getCi())
							&& StringUtils.isNotBlank(aadharRequestDomain.getSkey().getSkeyValue())) {
						//request.append("<Skey ci=\""+new String(prepareAUAData.getCertificateIdentifier())+ "\" ki=\""+new String(prepareAUAData.getSessionKeyDetails().getKeyIdentifier())+ "\">" + "</Skey>");
						request.append("<Skey ci=\"" + aadharRequestDomain.getSkey().getCi() + "\">" + aadharRequestDomain.getSkey().getSkeyValue() + "</Skey>");
					}

				} else {
					logger.error("Aadhaar request-> Session key is null while creating Auth XML");
					/*request.append("<Skey ci=\"" + prepareAUAData.getCertificateIdentifier() + "\">" + new String(Base64.encode(prepareAUAData.getSessionKeyDetails().getSkeyValue())) + "</Skey>");*/
				}

				request.append("<Data ");
				request.append(nodeWithAttrCreator("type", "X"));
				request.append(">");

				//new for direct pid block
				PersonalIdData pid = aadharRequestDomain.getData().getPid();
				if (StringUtils.isNotBlank(pid.getEncPidBlock())) {
					request.append(pid.getEncPidBlock());
				} else {
					//end direct pid
					logger.debug("prepareAUAData.getEncXMLPIDData()   ========>  "+prepareAUAData.getEncXMLPIDData());
					request.append(new String(Base64.encode(prepareAUAData.getEncXMLPIDData())));

					//new for direct pid block
				}
				//end direct pid

				request.append("</Data>");

				//start
				if (StringUtils.isNotBlank(aadharRequestDomain.getHmac())) {
					request.append("<Hmac>" + aadharRequestDomain.getHmac() + "</Hmac>");
				} else {
					if (StringUtils.isNotBlank(prepareAUAData.getEncryptedHmacBytes().toString())) {
						logger.debug("prepareAUAData.getEncryptedHmacBytes() =======>>>>>>>> "+prepareAUAData.getEncryptedHmacBytes());
						request.append("<Hmac>" + new String(Base64.encode(prepareAUAData.getEncryptedHmacBytes())) + "</Hmac>");
					} else {
						request.append("<Hmac/>");
					}
				}

				//end
			
			
			  /* if(StringUtils.isNotBlank(prepareAUAData.getEncryptedHmacBytes().toString()))
					{
				       request.append("<Hmac>"+new String(Base64.encode(prepareAUAData.getEncryptedHmacBytes()))+"</Hmac>");
					}
			   else{
				   request.append("<Hmac/>");
				}*/

				request.append("</Auth>");

				String finalTemp = new String(request);

				logger.debug("final Auth XML -> "+finalTemp);
				return finalTemp;
			} else {
				logger.error("Aadhaar request XMl is null while creating Auth XML");
				return "failure";
			}
		}
		catch (Exception e) {
			logger.error("Exception occurred while creating Auth XML with error message: {}",e.getMessage());
			return "failure";
		}

	}



		private static StringBuffer nodeWithAttrCreator(String attrName , String attrValue){
			StringBuffer buffer = new StringBuffer();
		
				if(StringUtils.isNotBlank(attrValue)){
					if(StringUtils.isNotBlank(attrName)){
						buffer.append(attrName+"=\""+attrValue+"\" ");
						return buffer;
					}
				}else {
					buffer.append("");
	//				buffer.append("</"+attrName+">");
				}
			return buffer;
		}
		
		private static StringBuffer nodeWithBlankAttrCreator(String attrName , String attrValue){
			StringBuffer buffer = new StringBuffer();
		
				if(StringUtils.isNotBlank(attrName)){
					buffer.append(attrName+"=\""+attrValue+"\" ");
					return buffer;
				}else {
					buffer.append("");
	//				buffer.append("</"+attrName+">");
				}
			return buffer;
		}
		public StringBuffer createPidXml(PersonalIdData pid) throws Exception{
			
			StringBuffer request = new StringBuffer();
			if(pid != null)
			{
				request.append("<Pid");
				if(StringUtils.isNotBlank(pid.getVer())){
						if(timeStamp == null)
							request.append(" ts=\""+new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss").format(new Date()) + "\" ver=\""+pid.getVer()+"\" wadh=\""+pid.getWadh()+"");
						else{
								
								request.append(" ts=\""+timeStamp + "\" ver=\""+pid.getVer()+"\" wadh=\""+pid.getWadh()+"\"");
						}
					}
				request.append(">");
				DemoGraphicData demo = pid.getDemo();
				if(demo != null )
				{
					Boolean demoFlag = true;
					if(StringUtils.isNotBlank(demo.getLang()) || demo.getPa()!=null || demo.getPfa()!=null ||demo.getPi()!=null)
					{
					
					if(demo.getPi() != null)
					{
						if(StringUtils.isNotBlank(demo.getPi().getMatchStrategy())
								|| StringUtils.isNotBlank(demo.getPi().getAge())
								|| StringUtils.isNotBlank(demo.getPi().getDateOfBirth())
								|| StringUtils.isNotBlank(demo.getPi().getDateOfBirthType())
								|| StringUtils.isNotBlank(demo.getPi().getEmail())
								|| StringUtils.isNotBlank(demo.getPi().getGender())
								|| StringUtils.isNotBlank(demo.getPi().getLmv())
								|| StringUtils.isNotBlank(demo.getPi().getLname())
								|| StringUtils.isNotBlank(demo.getPi().getMatchValue())
								|| StringUtils.isNotBlank(demo.getPi().getName())
								|| StringUtils.isNotBlank(demo.getPi().getPhone()))
						{
							if(demoFlag)
							{
								request.append("<Demo");
								request.append(nodeWithAttrCreator(" lang", demo.getLang()));
								request.append(">");
								demoFlag = false;
							}	
						request.append("<Pi ");
						request.append(nodeWithAttrCreator("ms", demo.getPi().getMatchStrategy()));
						request.append(nodeWithAttrCreator("mv", demo.getPi().getMatchValue()));
						request.append(nodeWithAttrCreator("name", demo.getPi().getName()));
						request.append(nodeWithAttrCreator("lname", demo.getPi().getLname()));
						request.append(nodeWithAttrCreator("lmv", demo.getPi().getLmv()));
						request.append(nodeWithAttrCreator("gender", demo.getPi().getGender()));
						request.append(nodeWithAttrCreator("dob", demo.getPi().getDateOfBirth()));
						request.append(nodeWithAttrCreator("dobt", demo.getPi().getDateOfBirthType()));
						request.append(nodeWithAttrCreator("age", demo.getPi().getAge()));
						request.append(nodeWithAttrCreator("phone", demo.getPi().getPhone()));
						request.append(nodeWithAttrCreator("email", demo.getPi().getEmail()));
						request.append("/>");
						}
					}
					if(demo.getPa() != null)
					{	//country not set TODO
						if(StringUtils.isNotBlank(demo.getPa().getCareOf())
								|| StringUtils.isNotBlank(demo.getPa().getDist())
								|| StringUtils.isNotBlank(demo.getPa().getHouseNo())
								|| StringUtils.isNotBlank(demo.getPa().getLandMark())
								|| StringUtils.isNotBlank(demo.getPa().getLocation())
								|| StringUtils.isNotBlank(demo.getPa().getMatchStrategy())
								|| StringUtils.isNotBlank(demo.getPa().getPostalCode())
								|| StringUtils.isNotBlank(demo.getPa().getPostOffice())
								|| StringUtils.isNotBlank(demo.getPa().getState())
								|| StringUtils.isNotBlank(demo.getPa().getCountry())
								|| StringUtils.isNotBlank(demo.getPa().getStreet())
								|| StringUtils.isNotBlank(demo.getPa().getSubDist())
								|| StringUtils.isNotBlank(demo.getPa().getVillageTownCity()))
						{
							if(demoFlag)
							{
								request.append("<Demo ");
								request.append(nodeWithAttrCreator("lang", demo.getLang()));
								request.append(">");
								demoFlag = false;
							}		
							
						request.append("<Pa ");
						request.append(nodeWithAttrCreator("ms", demo.getPa().getMatchStrategy()));
						request.append(nodeWithAttrCreator("co", demo.getPa().getCareOf()));
						request.append(nodeWithAttrCreator("house", demo.getPa().getHouseNo()));
						request.append(nodeWithAttrCreator("street", demo.getPa().getStreet()));
						request.append(nodeWithAttrCreator("lm", demo.getPa().getLandMark()));
						request.append(nodeWithAttrCreator("loc", demo.getPa().getLocation()));
						request.append(nodeWithAttrCreator("vtc", demo.getPa().getVillageTownCity()));
						request.append(nodeWithAttrCreator("subdist", demo.getPa().getSubDist()));
						request.append(nodeWithAttrCreator("dist", demo.getPa().getDist()));
						request.append(nodeWithAttrCreator("state", demo.getPa().getState()));
						request.append(nodeWithAttrCreator("country", demo.getPa().getCountry()));
						request.append(nodeWithAttrCreator("pc", demo.getPa().getPostalCode()));
						request.append(nodeWithAttrCreator("po", demo.getPa().getPostOffice()));
						request.append("/>");
						
						}
					}
					if(demo.getPfa() != null)
					{
						if(StringUtils.isNotBlank(demo.getPfa().getAddressValue())
								|| StringUtils.isNotBlank(demo.getPfa().getLav())
								|| StringUtils.isNotBlank(demo.getPfa().getLmv())
								|| StringUtils.isNotBlank(demo.getPfa().getMatchStrategy())
								|| StringUtils.isNotBlank(demo.getPfa().getMatchValue()))
						{
							if(demoFlag)
							{
								request.append("<Demo ");
								request.append(nodeWithAttrCreator("lang", demo.getLang()));
								request.append(">");
								demoFlag = false;
							}		
						request.append("<Pfa ");
						request.append(nodeWithAttrCreator("ms", demo.getPfa().getMatchStrategy()));
						request.append(nodeWithAttrCreator("mv", demo.getPfa().getMatchValue()));
						request.append(nodeWithAttrCreator("av", demo.getPfa().getAddressValue()));
						request.append(nodeWithAttrCreator("lav", demo.getPfa().getLav()));
						request.append(nodeWithAttrCreator("lmv", demo.getPfa().getLmv()));
						request.append("/>");
						}
					}
					if(!demoFlag)
					request.append("</Demo>");
					}
				
				}
				else
				{
//					request.append("<Demo/>");
				}
				//change
				
				if(pid.getBios() != null){
					
					request.append("<Bios ");
					if(pid.getBios().getBio() != null){
						List<BioMetricRecord> bioList = pid.getBios().getBio();
						if(bioList != null && bioList.size()>0){
							if(bioList.size()<=10 ){
								for(BioMetricRecord bio : bioList){
									request.append(nodeWithBlankAttrCreator("dih", bio.getType()));
									request.append(">");
									request.append("<Bio ");
									request.append(nodeWithAttrCreator("type", bio.getType()));
									request.append(nodeWithAttrCreator("posh", bio.getPosh()));
									
									if(StringUtils.equalsIgnoreCase(tid, "registered"))
									{
										request.append(nodeWithAttrCreator("bs", bio.getBs()));
									}
										
									request.append(">");
									request.append(bio.getValue());
									request.append("</Bio>");
								}
							}else{
								throw new Exception("No Of Bio Elements Must Not Be Greater Than 10");
							}
							
						}
					}
					
					request.append("</Bios>");
					
				}
				//end
				else
				{
				//	request.append("<Bios/>");
				}
				if(pid.getPv() != null)
				{
					if(StringUtils.isNotBlank(pid.getPv().getOtp()) 
							|| StringUtils.isNotBlank(pid.getPv().getPin()))
					{
						request.append("<Pv ");
						request.append(nodeWithAttrCreator("otp", pid.getPv().getOtp()));
						request.append(nodeWithAttrCreator("pin", pid.getPv().getPin()));
						request.append("/>");
					}
				}
				else
				{
				//	request.append("<Pv/>");
				}
				request.append("</Pid>");
			}
			else
			{
	     		request.append("<Pid/>");
			}
			System.out.println("********************************************* pid xml"+request.toString());
			return request;
			
		}
		public String createPidXmlJaxb(PersonalIdData pid){
			StringWriter pidXML = new StringWriter();

			try {
				JAXBContext.newInstance(PersonalIdData.class).createMarshaller().marshal(pid, pidXML);
			} catch (JAXBException e) {
				logger.error("{}",e.getStackTrace());
			}

//			System.out.println(pidXML.toString());

			return pidXML.toString();
		}
		
}
