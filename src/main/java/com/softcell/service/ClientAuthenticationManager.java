package com.softcell.service;

import com.softcell.gonogo.model.core.kyc.request.aadhar.ClientAadhaarRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.request.emudra.ESignRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.security.GetOtpRequest;

/**
 * @author yogeshb
 */
public interface ClientAuthenticationManager {
    /**
     * @param getOtpRequest
     * @return
     */
    BaseResponse sendOtp(GetOtpRequest getOtpRequest) throws Exception;

    /**
     * @param aadharOtpRequest
     * @return
     */
    BaseResponse getOtpByAadhar(
            ClientAadhaarRequest aadharOtpRequest) throws Exception;


    /**
     * @param aadharOtpWithOtherDeatilsRequest
     * @return
     */
    BaseResponse verifyOtpByAadharAndGetEkycDeatils(
            ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception;

    /**
     * @param aadharEkycwithBioRequest
     * @return
     */
    BaseResponse verifyBioByAadharAndGetEkycDetails(
            ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception;


    /**
     * @param aadharEkycwithBioRequest
     * @return
     */
    BaseResponse verifyBioByAadharAndGetEkycDetailsV2_2(
            ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception;

    /**
     * @param clientAadhaarRequest
     * @return
     */
    BaseResponse verifyIrisByAadharAndGetEkycDeatils(
            ClientAadhaarRequest clientAadhaarRequest);

    /**
     * @param smsRequest
     * @return
     */
    BaseResponse sendSms(SmsRequest smsRequest);

    /**
     * @param aadharOtpRequest
     * @return
     * @throws Exception
     */
    BaseResponse getOtpByAadharV2_1(
            ClientAadhaarRequest aadharOtpRequest) throws Exception;

    BaseResponse getOtpByAadharV2_2(
            ClientAadhaarRequest aadharOtpRequest) throws Exception;

    /**
     * @param aadharOtpWithOtherDeatilsRequest
     * @return
     */
    BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_1(
            ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception;

    BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_1_1(
            ClientAadhaarRequest aadharOtpWithOtherDeatilsRequest) throws Exception;

    /**
     * @param clientAadhaarRequest
     * @return
     */
    BaseResponse verifyIrisByAadharAndGetEkycDeatilsV2_1(ClientAadhaarRequest clientAadhaarRequest);

    BaseResponse verifyIrisByAadharAndGetEkycDeatilsV2_2(ClientAadhaarRequest clientAadhaarRequest) throws Exception;


    /* Send SMS to Personal Phone
     * @param applicationRequest
     * @param smsMessage
     * @return
     */
    BaseResponse sendSMS(ApplicationRequest applicationRequest, String smsMessage);

    /**
     *
     * @param eSignRequest
     * @return
     */
    BaseResponse doESign(ESignRequest eSignRequest) throws Exception;

    BaseResponse doEMandate(ESignRequest eSignRequest) throws Exception;

    BaseResponse doESignAndEMandate(ESignRequest eSignRequest) throws Exception;

    BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_2(ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception;

    BaseResponse verifyOtpByAadharAndGetEkycDeatilsV2_2_NEW(ClientAadhaarRequest aadharOtpWithOtherDetailsRequest) throws Exception;

    /**
     *
     * @param aadharOtpRequest
     * @return
     * @throws Exception
     */
    BaseResponse getOtpByAadharServiceVersionDecider(ClientAadhaarRequest aadharOtpRequest) throws Exception;

    /**
     *
     * @param clientAadhaarRequest
     * @return
     */
    BaseResponse verifyOtpByAadharServiceVersionDecider(ClientAadhaarRequest clientAadhaarRequest) throws Exception;

    /**
     *
     * @param clientAadhaarRequest
     * @return
     */
    BaseResponse verifyOtpByAadharServiceVersionDeciderV2_1_1(ClientAadhaarRequest clientAadhaarRequest) throws Exception;


    /**
     *
     * @param clientAadhaarRequest
     * @return
     * @throws Exception
     */
    BaseResponse verifyIrisByAadharServiceVersionDecider(ClientAadhaarRequest clientAadhaarRequest) throws Exception;

    /**
     *
     * @param aadharEkycwithBioRequest
     * @return
     * @throws Exception
     */
    BaseResponse verifyBioByAadharServiceVersionDecider(ClientAadhaarRequest aadharEkycwithBioRequest) throws Exception;

    BaseResponse verifyOtp(SmsRequest smsRequest);
}
