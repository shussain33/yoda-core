package com.softcell.service;

import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.InitialMoneyDeposit;
import com.softcell.gonogo.model.insurance.icici.IciciPremiumCalculationResponse;
import com.softcell.gonogo.model.lms.LmsResponse;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.mifin.MiFinResponse;
import com.softcell.gonogo.model.mifin.crm.CrmResponse;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.core.TopUpDedupeRequest;
import com.softcell.gonogo.model.mifin.topup.TopUpDedupeResponse;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.response.CreditVidyaResponse;
import com.softcell.gonogo.model.saathi.SaathiResponse;

import java.util.List;

/**
 * Created by archana on 4/10/17.
 */
public interface ExternalApiManager {
    SaathiResponse pushDataToSaathi(ApplicationRequest applicationRequest) throws Exception;

    MiFinResponse pushDataToMiFin(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception;

    MiFinResponse pushDataToMiFin(String refId) throws Exception;

    CreditVidyaResponse callToCreditVidya(String refId,String instId);

    //1st MIFIN applicANTdetails
    TopUpDedupeResponse sendDataToMIFINApplicantDetail(TopUpDedupeRequest topUpDedupeRequest, String refId);

 //according to ref id
    //MFIN 2nd API MIFIN_DEDUPE
   List <TopUpDedupeResponse> sendDataToMifinDedupe(ApplicationRequest applicationRequest, Applicant applicant,
                                                    List<CoApplicant> coApplicantList);

    List<TopUpDedupeResponse> sendMiFINLoanDetail(TopUpDedupeRequest topUpDedupeRequest) ;

   IciciPremiumCalculationResponse calculatePremiumICICICall(String refId, String institution,
                                                             InsuranceRequest insuranceRequest, String initiationPoint, String changedAmount, Applicant applicant);

    void pushPolicy(InsuranceRequest insuranceRequest, LoanCharges loanCharges);

    MiFinCallLog evaluateMifinDedup(ApplicationRequest applicationRequest);

    CrmResponse fetchCRMDedupeUsingHeaders(LoginDataRequest loginDataRequest);
    /**
     * Saving Gonogo Application into Mifin
     * @param applicationRequest
     * @return
     */
    void callToMiFin(ApplicationRequest applicationRequest, String loggedInUserRole, String loggedInUserId) throws Exception;

    /**
     * Saving Gonogo Application into Mifin
     * HOPS submits DISB case to miFin.DISB is last stage of the application.
     * Stage change to DISB and Update application status to Disbursed if All Apis gets successfull response
     * Otherwise reverting to previous stage if Mifin push failed or if Prospect code is not generated.
     * @param goNoGoCustomerApplication,loggedInUserRole,loggedInUserId
     * @return
     */
    void callToMiFinSaveData(GoNoGoCustomerApplication goNoGoCustomerApplication, String loggedInUserRole, String loggedInUserId) throws Exception;
    /**
     * Delete Applicant in Mifin
     * @param applicationRequest
     * @return
     */
    void callToMiFinDeleteApplicant(ApplicationRequest applicationRequest , List<String> coApplicantIds, String loggedInUserRole, String loggedInUserId) throws Exception;

    void pushIMDDataToMifin(GoNoGoCustomerApplication goNoGoCustomerApplication, InitialMoneyDeposit imd2) throws Exception;
}
