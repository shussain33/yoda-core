package com.softcell.service;

import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AmbitMifinLog;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.UpdateDedupeRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch.MiFinPanSearchBaseResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.util.List;

public interface AmbitMifinManager {

    /**
     * Pan Search
     * @param applicationRequest,applicant
     *@return
     */
    MiFinPanSearchBaseResponse panSearch(ApplicationRequest applicationRequest,Applicant applicant, String userId) throws Exception;

    /**
     * Pan search and Existing Applicant Search
     * @param applicationRequest
     * @return
     */
    BaseResponse panSearchAndSearchExistingApplicant(ApplicationRequest applicationRequest)throws Exception;

    /**
     * Creating new loan for Mifin
     * @param applicationRequest
     * @return
     */
    void createLoan(ApplicationRequest applicationRequest, String userId)throws Exception;

    /**
     * Saving Applicant Details
     * @param applicationRequest
     * @return
     */
    void saveApplicantDetails(ApplicationRequest applicationRequest, String userId)throws Exception;

    /**
     * Process Dedupe
     * @param applicationRequest
     * @return
     */
    void processDedupe(ApplicationRequest applicationRequest, String userId)throws Exception;

    /**
     * Update Process Dedupe
     * @param updateDedupeRequest
     * @return
     */
    BaseResponse updateProcessDedupe(UpdateDedupeRequest updateDedupeRequest) throws Exception;

    /**
     * Search Existing Applicant
     * @param applicationRequest,applicant
     * @return
     */
    BaseResponse searchExistingApplicantSearch(ApplicationRequest applicationRequest, Applicant applicant,MiFinPanSearchBaseResponse miFinPanSearchBaseResponse,String userId) throws Exception;

    /**
     * get Ambit Mifin Logs
     * @param ambitMifinLog
     * @return
     */
    BaseResponse getMifinAmbitLogs(AmbitMifinLog ambitMifinLog) throws Exception;

    /**
     * Saving Applicant Address
     * @param applicationRequest
     * @return
     */
    BaseResponse saveApplicantAddress(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Saving CoApplicant Address
     * @param applicationRequest
     * @return
     */
    BaseResponse saveCoApplicantAddress(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Delete Applicant Details
     * @param applicationRequest
     * @param coApplicantIds
     * @return
     */
    void deleteMifinApplicant(ApplicationRequest applicationRequest, List<String> coApplicantIds, String userId)throws Exception;

    /**
     * Update loan details
     * @param applicationRequest
     * @return
     */
    boolean updateLoanDetail(ApplicationRequest applicationRequest, String userId) throws Exception;


    /**
     * Save Verification
     * @param applicationRequest
     * @return
     */
    boolean saveVerification(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Save CR Decision
     * @param applicationRequest
     * @return
     */
    boolean crDecision(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Update Save Personal Insurance
     * @param applicationRequest
     */
    boolean savePersonalInsurance(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Save Disbursal Maker Details
     * @param applicationRequest
     * @return
     */
    boolean saveDisbursalMaker(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * Fetch UserId from UAM
     * @param header,loggedInUserRole,loggedInUserId)
     */
    String fetchUserId(Header header, String loggedInUserRole, String loggedInUserId);

    /**
     * Saving Financial Info Details
     * @param applicationRequest
     * @return
     */
    boolean saveFinancialInfo(ApplicationRequest applicationRequest, String userId)throws Exception;
    /**
     * Saving Financial and Fetch Dedupe
     * @param applicationRequest
     * @return
     */
    BaseResponse saveFinancialAndFetchDedupe(ApplicationRequest applicationRequest)throws Exception;

    /**
     * Fetch UserId from SME_ADMIN master
     * @param header, loggedInUserId
     * @return
     */
    String getUser(Header header, String loggedInUserId);

}