package com.softcell.service;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.metadata.MetadataEntity;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.rest.domains.MetadataDictRequest;

/**
 * Created by prateek on 10/3/17.
 */
public interface MetaDataDictionaryService {

    /**
     *
     * @param institutionId
     * @param product
     * @param type
     * @return
     * @throws SystemException
     */
    BaseResponse findTypeValues(final String institutionId, final String product , final String type) throws SystemException;

    /**
     *
     * @param institutionId
     * @param product
     * @return
     * @throws SystemException
     */
    BaseResponse findByInstitutionIdNProduct(String institutionId ,String product) throws SystemException;

    /**
     *
     * @param institutionId
     * @return
     * @throws SystemException
     */
    BaseResponse findByInstitutionId(String institutionId ) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    BaseResponse createTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @return
     * @throws SystemException
     */
    BaseResponse replaceTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @return  metadataEntity {@link MetadataEntity}
     * @throws SystemException
     */
    BaseResponse renameTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    BaseResponse deleteType(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    void factoryDefaultTypeValues(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param metadataDictRequest
     * @throws SystemException
     */
    void disableTypeValue(MetadataDictRequest metadataDictRequest) throws SystemException;

    /**
     *
     * @param type
     * @return
     */
    BaseResponse findByType(String type);
}
