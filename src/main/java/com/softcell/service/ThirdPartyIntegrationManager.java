package com.softcell.service;

import com.softcell.gonogo.model.coorgination.icici.CoorginationAPIRequest;
import com.softcell.gonogo.model.core.request.ThirdPartyRequest;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.finbit.FinbitCallbackRequest;
import com.softcell.gonogo.model.finbit.FinbitRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.LoanChargesRepaymentRequest;
import com.softcell.gonogo.model.request.LoginDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.ssl.perfios.GenerateFileRequest;
import com.softcell.ssl2.finfort.model.FileUploadRequest;
import com.softcell.ssl2.finfort.model.FinfortStatusRequest;
import com.softcell.ssl2.perfios.PerfiosDataRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ssg408 on 24/9/18.
 */
public interface ThirdPartyIntegrationManager {
    BaseResponse saveData(
            ApplicationRequest goNoGoCustomerApplication, String stepId,
            HttpServletRequest httpRequest) throws Exception;

    BaseResponse initiateFinfort(ApplicationRequest applicationRequest, HttpServletRequest httpRequest);

    BaseResponse processFinfortStatus(FinfortStatusRequest statusRequest, HttpServletRequest httpRequest);

    BaseResponse processFileList(FileUploadRequest fileUploadRequest, HttpServletRequest httpRequest);

    BaseResponse updatePerfiosData(PerfiosDataRequest perfiosDataRequest, HttpServletRequest httpRequest);

    BaseResponse getFinbitDetails(FinbitRequest finbitRequest, HttpServletRequest httpServletRequest) throws  Exception;

    BaseResponse saveFinbitData(FinbitRequest finbitRequest, HttpServletRequest httpRequest);

    BaseResponse saveFinbitDataTemp(FinbitRequest finbitRequest, HttpServletRequest httpRequest);

    BaseResponse getFinbitData(FinbitRequest finbitRequest, HttpServletRequest httpRequest);

    BaseResponse finbitCallback(FinbitCallbackRequest finbitCallbackRequest, HttpServletRequest httpServletRequest);

    BaseResponse getFinbitMonthlySummary(FinbitRequest finbitRequest, HttpServletRequest httpServletRequest);

    BaseResponse getCoorginationDetails(CoorginationAPIRequest coorginationAPIRequest, HttpServletRequest httpServletRequest) throws Exception;

    BaseResponse saveMcpData(LoginDataRequest loginDataRequest, String stepId, HttpServletRequest httpRequest) throws Exception;

    BaseResponse  saveLoanChargesAndRepaymentDetails(LoanChargesRepaymentRequest loanChargesRepaymentRequest, HttpServletRequest httpRequest);

    BaseResponse getScoringCallLog(ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest);

    BaseResponse getScoringCallDetails(ThirdPartyRequest thirdPartyRequest, HttpServletRequest httpRequest);

    BaseResponse generatePerfiosFile(GenerateFileRequest generateFileRequest, HttpServletRequest httpRequest);

    BaseResponse saveFaceMatchData(FaceMatchRequest faceMatchRequest);
}
