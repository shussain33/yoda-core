package com.softcell.service;

import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;


/**
 * @author prateek
 *         interface for AnalyticsManager
 */
public interface AnalyticsManager {


    BaseResponse getLoginStatusGraphDataOld(StackRequest stackRequest)
            throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getLoginStatusGraphData(StackRequest stackRequest)
            throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getLoginStatusTableData(StackRequest stackRequest)
            throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getLoginStatusTableDataForDtc(StackRequest stackRequest)
            throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse generateLoginByStagesReport(
            StackRequest stackRequest) throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse generateAssetManufacturerReport(
            StackRequest stackRequest) throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getLoginByTopDealer(StackRequest stackRequest)
            throws Exception;

    /**
     * @return
     * @throws Exception
     */
    BaseResponse getDsaReport() throws Exception;

    /**
     * @param stackRequest
     * @return
     */
    BaseResponse getStackScoreData(StackRequest stackRequest) throws Exception;

    /**
     * @return
     * @throws Exception
     */
    BaseResponse getRoleBasedDistinctBranches(StackRequest stackRequest) throws Exception;


    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getRoleBasedOnDistinctDealers(StackRequest stackRequest) throws Exception;


    BaseResponse getRoleBasedDistinctStages(StackRequest stackRequest) throws Exception;

    /**
     * @param stackRequest
     * @return
     * @throws Exception
     */
    BaseResponse getRoleBasedOnDistinctDSA(StackRequest stackRequest) throws Exception;
}
