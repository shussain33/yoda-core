package com.softcell.service;

import com.softcell.gonogo.model.request.core.UpdateDataRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ssg0268 on 4/9/19.
 */
public interface UpdateManager {
    BaseResponse updateData(UpdateDataRequest updateDataRequest, String stepId, HttpServletRequest httpRequest);
}
