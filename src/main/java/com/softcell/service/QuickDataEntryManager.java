package com.softcell.service;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by archana on 27/7/17.
 */
public interface QuickDataEntryManager {

    /**
     * Get LPG Details.
     * @param applicationRequest
     * @return
     * @throws Exception
     */
    BaseResponse submitQuickData(ApplicationRequest applicationRequest,
                                        String stepId, HttpServletRequest httpRequest) throws Exception;
}
