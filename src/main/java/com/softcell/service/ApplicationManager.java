package com.softcell.service;


import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.creditvidya.CreditVidyaDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyPremiumRequest;
import com.softcell.gonogo.model.request.gst.GstDetailsRequest;
import com.softcell.gonogo.model.request.insurance.GetInsurancePremiumRequest;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;


/**
 * @author yogeshb
 */
public interface ApplicationManager {
    /**
     * @param applicationRequest
     * @return
     */
    BaseResponse submitApplication(ApplicationRequest applicationRequest) throws Exception;

    /**
     * @param refId
     * @return
     */
    BaseResponse appExists(final String refId);

    /**
     * @param checkApplicationStatus
     * @return
     */
    BaseResponse getStatus(CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param postIpaRequest
     * @param httpRequest
     * @return
     */
    BaseResponse setpostIpaPdf(PostIpaRequest postIpaRequest, HttpServletRequest httpRequest) throws Exception;

    /**
     * @param postIpaRequest
     * @return
     */
    BaseResponse getpostIpaPdf(PostIpaRequest postIpaRequest) throws Exception;

    /**
     * @param dashboardRequest
     * @return
     */
    BaseResponse getDashBoardData(DashboardRequest dashboardRequest) throws Exception;

    /**
     * @param postIpaRequest
     * @return
     */
    BaseResponse setPostIpaStage(PostIpaRequest postIpaRequest) throws Exception;

    /**
     * @param partialSaveRequest
     * @return
     */
    BaseResponse getPartialSaveRequest(PartialSaveRequest partialSaveRequest);

    /**
     * @param fileHandoverDetails
     * @return Acknowledgement
     */
    BaseResponse setFileHandOverDetails(FileHandoverDetails fileHandoverDetails) throws Exception;


    /**
     * @param fileHandoverDetailsRequest
     * @return FileHandoverDetails
     */
    BaseResponse getFileHandoverDetails(
            FileHandoverDetailsRequest fileHandoverDetailsRequest) throws Exception;

    /**
     * This method is used to get negative area against the pinCode entered by user.
     *
     * @param negativeAreaFundingRequest
     * @return
     */
    BaseResponse getNegativeAreaFundingDetails(NegativeAreaFundingRequest negativeAreaFundingRequest) throws Exception;

    /**
     * @param checkApplicationStatus
     * @return
     * @throws Exception
     */
    BaseResponse getApplicationStatus(CheckApplicationStatus checkApplicationStatus) throws Exception;

    /**
     * @param updateReferencesRequest
     * @return
     */
    BaseResponse updateApplicantReferences(UpdateReferencesRequest updateReferencesRequest);

    /**
     * @param bankingDetailsRequest
     * @return
     */
    BaseResponse updateBankingDetails(BankingDetailsRequest bankingDetailsRequest);

    /**
     * @param extendedWarrantyPremiumRequest
     * @return
     */
    BaseResponse getExtendedWarrantyPremium(ExtendedWarrantyPremiumRequest extendedWarrantyPremiumRequest);

    /**
     * @param extendedWarrantyDetails
     * @return
     */
    BaseResponse updateExtendedWarrantyDetails(ExtendedWarrantyDetails extendedWarrantyDetails);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse fetchExtendedWarrantyDetails(String refId, String institutionId);

    /**
     * @param getInsurancePremiumRequest
     * @return
     */
    BaseResponse getInsurancePremiumAgainstAssetCategory(GetInsurancePremiumRequest getInsurancePremiumRequest);

    /**
     * @param insurancePremiumDetails
     * @return
     */
    BaseResponse updateInsurancePremiumDetails(InsurancePremiumDetails insurancePremiumDetails);

    /**
     * @param creditVidyaDetails
     * @return
     */
    BaseResponse updateCreditVidyaDetails(CreditVidyaDetails creditVidyaDetails);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse fetchInsuranceDetailsByRefId(String refId, String institutionId);

    /**
     * @param refId
     * @param institutionId
     * @return
     */
    BaseResponse fetchCreditVidyaDetailsByRefId(String refId, String institutionId);

    /**
     * @param updateRevisedEmiRequest
     * @return
     */
    BaseResponse updateRevisedEmiAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest);

    /**
     * @param gstDetailsRequest
     * @return
     */
    BaseResponse updateGstDetails(GstDetailsRequest gstDetailsRequest);

    /**
     * @param postIpaRequest
     * @param httpRequest
     * @return
     */
    BaseResponse savePostIpaRequest(PostIpaRequest postIpaRequest, HttpServletRequest httpRequest) throws Exception;

    /**
     * @param postIpaRequest
     * @return
     */
    BaseResponse setPostIpaStageAndStoreDO(PostIpaRequest postIpaRequest) throws Exception;

    /**
     * @param customerFinancialProfileRequest
     * @return
     */
    BaseResponse submitCustomerFinancialProfile(final CustomerFinancialProfileRequest customerFinancialProfileRequest);

    /**
     * @param goNoGoCroApplicationResponse
     * @return
     * @throws Exception
     */
    BaseResponse generateRefNum(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse) throws Exception;

    BaseResponse initializePostIpa(SchemeMetadataRequest schemeMetadataRequest) throws Exception;

    BaseResponse getInsurancePremium(GetInsurancePremiumRequest getInsurancePremiumRequest);

    BaseResponse getInsurancePremiumAgainstProvider(GetInsurancePremiumRequest getInsurancePremiumRequest);

    BaseResponse updateAddonServiceRevisedAmount(UpdateRevisedEmiRequest updateRevisedEmiRequest);

    /**
     *
     * @param geoLimitRequest
     * @return
     */
    BaseResponse checkPinCodeGeographicalLimit(GeoLimitRequest geoLimitRequest);
}
