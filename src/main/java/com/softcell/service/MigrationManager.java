package com.softcell.service;

import com.softcell.gonogo.model.request.MigrationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * Created by amit on 11/3/19.
 */
@Component
public interface MigrationManager {

    BaseResponse updateRegistrationInfo(MigrationRequest migrationRequest);

    BaseResponse updateSourcingChannel(MigrationRequest migrationRequest);

    BaseResponse updateBranchMangerName(MigrationRequest migrationRequest, HttpServletRequest httpRequest);

    BaseResponse bulkDsaIdCases(File uploadedFile, String institutionId);
    BaseResponse bulkDsaIdCasesCount(File uploadedFile, String institutionId);

    BaseResponse allocateInfoChange(File uploadedFile, String institutionId);

    BaseResponse changeInBankingDetail(String institutionId, boolean hit);
}
