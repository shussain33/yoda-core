package com.softcell.service;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

import java.util.List;

/**
 * Created by prateek on 13/2/17.
 */
public interface WorkFlowCommunicationManager {

    BaseResponse getWfJobComm(String institutionId) throws SystemException;

    WFJobCommDomain getWfCommDomainJobByType(final String institutionId , final String type );

    WFJobCommDomain getWfCommDomainJobByType(final String institutionId ,final String product, final String type );

    WFJobCommDomain getWfCommDomainJob(final String institutionId , final String product, final String serviceId, final String type  );

    WFJobCommDomain getConfigFromIsEnabledFlag(final String institutionId , final String type );

    List<WFJobCommDomain> getServices(String institutionId, String type, String product);

    WFJobCommDomain getWfCommDomainJobByMemberId(String institutionId, String product, String memberId, String type);

    void saveWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException;

    void deleteWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException;

    void updateWfJobComm(WFJobCommDomain wfJobCommDomain) throws SystemException;

    WFJobCommDomain getConfigForProduct(String institutionId, String type, String product);
}
