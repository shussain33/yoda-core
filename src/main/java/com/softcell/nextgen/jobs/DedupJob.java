package com.softcell.nextgen.jobs;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.DedupeDetails;
import com.softcell.gonogo.model.core.DedupeMatch;
import com.softcell.gonogo.model.core.DedupeMatchDetail;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.Module;
import com.softcell.nextgen.constants.WfJobResultType;
import com.softcell.nextgen.constants.WfJobTypeConst;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by prateek on 13/2/17.
 */
@Component
public class DedupJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(DedupJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    private Map<String, Object> resultMap = new HashedMap();

    private boolean isCoApplicant;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    public ModuleRequestRepository getModuleRequestRepository() {
        return moduleRequestRepository;
    }

    public void setModuleRequestRepository(ModuleRequestRepository moduleRequestRepository) {
        this.moduleRequestRepository = moduleRequestRepository;
    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug("Executing Dedup Job ");

        Set<DedupeDetails> dedupeResultSet = null;
        ModuleOutcome internalDedupeModuleResult = null;
        try {

            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

            if (null != applicationRequest) {
                FinalApplicationMongoRepository finalApplication = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
                FinalApplicationRepository finalApplicationRepository = (FinalApplicationRepository) ApplicationProxy.getProxy(finalApplication, FinalApplicationRepository.class);

                dedupeResultSet = finalApplicationRepository.deDupeCustomer(applicationRequest);

                if (null != dedupeResultSet && dedupeResultSet.contains(goNoGoCustomerApplication.getGngRefId())) {
                    dedupeResultSet.remove(goNoGoCustomerApplication.getGngRefId());
                }

                DedupeMatchDetail matchDetail = null;
                List<DedupeMatchDetail> matchDetailList = new ArrayList();
                for(DedupeDetails dedupeDetails : dedupeResultSet){
                    // create matchdetail obj
                    matchDetail = new DedupeMatchDetail();
                    // set dedupedeatil in matchdetail obj
                    matchDetail.setDedupeDetails(dedupeDetails);
                    // add matchdetail obj in the list
                    matchDetailList.add(matchDetail);
               }

                // Create DedupeMatch and set the list.
                DedupeMatch dedupeMatch = new DedupeMatch();
                dedupeMatch.setRefId(goNoGoCustomerApplication.getGngRefId());
                dedupeMatch.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                dedupeMatch.setDedupeMatchDetails(matchDetailList);
                finalApplicationRepository.saveDedupeInfo(dedupeMatch);

                // Set deduped  ids into gonogoApplication
                Set<String> dedupedRefIdList = dedupeResultSet.stream().map(dedupe -> dedupe.getRefId())
                                                                                    .collect(Collectors.toSet());
                int size = dedupeResultSet.size();
                //Dedupe Found,No Hit, No Match
                internalDedupeModuleResult = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                        .fieldValue(String.valueOf(size))
                        .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                        .message(size > 0 ? "Dedupe Found" : "No Match")
                        .build();
                goNoGoCustomerApplication.setDedupedApplications(dedupedRefIdList);
                goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.COMPLETE.toString());
                goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
                logger.debug("after updating status of application in dedup {}", goNoGoCustomerApplication);
                resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.COMPLETED);
            } else {
                //Dedupe Found,No Hit, No Match
                internalDedupeModuleResult = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                        .fieldValue(String.valueOf(0))
                        .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                        .message("No Hit")
                        .build();

                goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
                goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.ERROR.name());
                resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);
            }
        } catch (Exception e) {
            //Dedupe Found,No Hit, No Match
            internalDedupeModuleResult = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                    .fieldValue(String.valueOf(0))
                    .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                    .message("No Hit")
                    .build();

            goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
            goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.ERROR.name());
            logger.error("error occurred while processing de-dup job with probable cause [{}] ", e.getMessage());
            resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);
        }

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        resultMap.put(Module.JOB_NAME.name(), WfJobTypeConst.DEDUP_PROCESSOR.getValue());
        resultMap.put(Module.DEDUPE_STATUS.name(), !CollectionUtils.isEmpty(dedupeResultSet));
        JobResult jobResult = new JobResult();
        jobResult.setResult(resultMap);

        return jobResult;
    }
}
