package com.softcell.nextgen.jobs;

import com.softcell.constants.FieldSeparator;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.CroUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.aggregators.WfResultAggregator;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by prateek on 19/2/17.
 */
@Component
public class ScoringJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(ScoringJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Boolean isCoApplicant;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    @Autowired
    private ScoringJsonBuilder scoringJsonBuilder;

    @Autowired
    private WfResultAggregator wfResultAggregator;

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {
    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug("Scoring job called");

        // if re-initiate count is > 0 do not process scoring job
        if (goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return null;
        }

        ModuleOutcome moduleOutcome = new ModuleOutcome();

        goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

        moduleOutcome.setOrder(ScoringDisplayName.APPLICATION_SCORE_ORDERE);

        moduleOutcome.setFieldName(ScoringDisplayName.APPLICATION_SCORE);

        goNoGoCustomerApplication.getIntrimStatus().setScoringModuleResult(moduleOutcome);

        goNoGoCustomerApplication.getIntrimStatus().setAppsStatus(Status.VERIFIED.toString());

        ScoringResponse scoringResponse = callScoring();

        if (null != scoringResponse) {

            logger.info("Scoring job getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());

            if (null != scoringResponse.getScoreData() && null != scoringResponse.getScoreData().getScoreValue()) {

                String scoreValue = scoringResponse.getScoreData().getScoreValue();

                moduleOutcome.setFieldValue(scoreValue);

                moduleOutcome.setMessage(scoringResponse.getStatus());

                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());


            } else {

                moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);

                moduleOutcome.setMessage(scoringResponse.getStatus());

                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            }
        } else {

            logger.warn("Scoring job not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());

            moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);

            moduleOutcome.setMessage("Scoring Down");

            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

        }

        goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResponse);

        try {

            queueApplication();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        /**
         * Partial Scoring response save in db.
         */

        moduleRequestRepository
                .partialApplicationDbSave(goNoGoCustomerApplication);


        // result job result
        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }

    private void queueApplication() {

        if (0 == goNoGoCustomerApplication.getReInitiateCount()) {

            wfResultAggregator.setQueue(goNoGoCustomerApplication);

            return;

        } else {

            List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);

            goNoGoCustomerApplication.setCroDecisions(croDecisions);
        }
    }

    private ScoringResponse callScoring() throws Exception {

        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        ScoringApplicationRequest scoringApplicationRequest = scoringJsonBuilder.buildScoringJSon(goNoGoCustomerApplication);

        if (isCoApplicant) {

            List<ScoringApplicationRequest> coApplicationRequests = scoringJsonBuilder.buildScoringJsonForCoApplicant(goNoGoCustomerApplication);

            scoringApplicationRequest.setCoApplicationRequests(coApplicationRequests);
        }

        scoringApplicationRequest.setGngRefId(goNoGoCustomerApplication.getGngRefId());

        // save generated request for scoring
        if (scoringApplicationRequest != null) {

            moduleRequestRepository.saveScoringRequest(scoringApplicationRequest);

        }

        WFJobCommDomain scoringCommunication = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, WfJobTypeConst.SCORING_JOB.getValue());

        String url = StringUtils.join(Arrays.asList(scoringCommunication.getBaseUrl(), scoringCommunication.getEndpoint()), FieldSeparator.BLANK);

        try {

            String valueJson = JsonUtil.ObjectToString(scoringApplicationRequest);


            String endpoint = url + "/CalculateScoreSync?" + "INSTITUTION_ID=" +
                    scoringCommunication.getInstitutionId() + "&USER_ID=" + scoringCommunication.getMemberId() +
                    "&PASSWORD=" + scoringCommunication.getPassword();

            StopWatch stopWatch = new StopWatch();

            stopWatch.start();

            String response = httpTransportationService.postRequest(endpoint, valueJson, MediaType.APPLICATION_JSON_VALUE);

            stopWatch.stop();

            logger.debug(String.format(" Time taken by SOBRE api for reference no [%s] is %s", goNoGoCustomerApplication.getGngRefId(),
                    stopWatch.getTotalTimeMillis()));

            if (null == response) {
                logger.warn("not able to get scoring response so terminating this job ");
                return null;
            }

            ScoringResponse scoringResponse = JsonUtil.StringToObject(response, ScoringResponse.class);

            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

            return scoringResponse;

        } catch (Exception e) {

            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

            logger.error("{}",e.getStackTrace());

            logger.error("error occurred while calling sobre service endpoint [{}] ", url);

            return null;

        }
    }
}
