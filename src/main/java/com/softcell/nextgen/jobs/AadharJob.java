package com.softcell.nextgen.jobs;

import com.softcell.constants.Constant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.AadharServiceCaller;
import com.softcell.gonogo.service.factory.AadharEntityBuilder;
import com.softcell.nextgen.JobResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by prateek on 19/2/17.
 */
@Component
public class AadharJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(AadharJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private boolean isCoApplicant;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    @Autowired
    private AadharEntityBuilder aadharEntityBuilder;

    @Autowired
    private AadharServiceCaller aadharServiceCaller;


    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug(" Aadhaar job initialization  start.. ");


        ModuleOutcome applicationAadharResult = new ModuleOutcome();

        applicationAadharResult.setFieldName(ScoringDisplayName.AADHAR_RESULT);

        applicationAadharResult.setOrder(ScoringDisplayName.AADHAR_RESULT_ORDER);

        goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(applicationAadharResult);

        AadharMainResponse aadharMainResponse = null;

        try{

            aadharMainResponse = callAadharService();

        } catch (Exception e){
            logger.error("{}",e.getStackTrace());
            logger.error("error occurred while invoking aadhaar service with probable cause {}  ", e.getMessage());
        }

        if (null != aadharMainResponse) {

            goNoGoCustomerApplication.getApplicantComponentResponse().setAdharServiceResponse(aadharMainResponse);

            if (!StringUtils.equals(Constant.ERROR,aadharMainResponse.getTxnStatus())) {

                goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(Status.VERIFIED.name());

                applicationAadharResult.setFieldValue(aadharMainResponse.getKycResponse().getAadharResponseDetails().getAadharResponse().getResult());

                applicationAadharResult.setMessage(aadharMainResponse.getKycResponse().getAadharResponseDetails().getUidaiStatus());

                goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(applicationAadharResult);

            } else {

                goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(Status.COMPLETE.name());

                applicationAadharResult.setFieldValue("No Response");

                applicationAadharResult.setMessage(Status.NO_RESPONSE.name());

                goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(applicationAadharResult);

            }

        } else {

            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(Status.COMPLETE.name());

            applicationAadharResult.setMessage(Status.NO_RESPONSE.name());

            applicationAadharResult.setFieldValue("No Response");

            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(Status.COMPLETE.name());

            goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(applicationAadharResult);
        }

        /**
         * Partial Pan response save in db.
         */
        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;

    }

    /**
     *
     * @return
     * @throws Exception
     */
    private AadharMainResponse callAadharService() throws Exception {

        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

        if (applicationRequest != null) {

            AadhaarRequest aadhaarRequest = aadharEntityBuilder.buildAuthenticationRequest(applicationRequest);

            if (null != aadhaarRequest && null != aadhaarRequest.getKycRequest().getAadharHolderDetails().getAadharNumber()) {

                moduleRequestRepository.saveAadharRequest(aadhaarRequest);

                logger.debug(" Aadhaar request build and saved in database " );

                return aadharServiceCaller.callAadharEkycService(aadhaarRequest,applicationRequest.getHeader().getInstitutionId());

            } else {
                logger.error("Aadhaar request build failed");
                return null;
            }

        } else {
            return null;
        }
    }


}
