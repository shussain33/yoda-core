package com.softcell.nextgen.jobs;

import com.softcell.constants.FieldSeparator;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.PanServiceResponse;
import com.softcell.gonogo.model.core.kyc.response.pan.PanInfo;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponseDetails;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.PanEntityBuilder;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.executors.multibureau.AddressMatching;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by prateek on 20/2/17.
 */

@Component
public class PanVerificationJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(PanVerificationJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    @Autowired
    private PanEntityBuilder panEntityBuilder;

    @Autowired
    private HttpTransportationService httpTransportationService;

    private Boolean isCoApplicant;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }


    @Override
    public JobResult call() throws Exception {

        logger.debug("Pan cam Job Started");


        ModuleOutcome moduleOutcome = new ModuleOutcome();

        moduleOutcome.setFieldName(ScoringDisplayName.PAN_RESULT);

        moduleOutcome.setOrder(ScoringDisplayName.PAN_RESULT_ORDER);

        goNoGoCustomerApplication.getIntrimStatus().setPanModuleResult(moduleOutcome);

        goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

        PanResponse applicantPanResponse = callPanService();

        goNoGoCustomerApplication.getApplicantComponentResponse().setPanServiceResponse(applicantPanResponse);


        if (isCoApplicant) {
            callCoApplicantPanService();
        }

        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.IN_PROCESS.toString());


        try {

            if (applicantPanResponse != null &&
                    null != applicantPanResponse.getKycResponse() &&
                    null != applicantPanResponse.getKycResponse().getPanResponseDetails()) {

                logger.trace(" PanVerificationJob get data as {}  from pan service successfully!! ", applicantPanResponse);

                PanResponseDetails panResponseDetails = applicantPanResponse.getKycResponse().getPanResponseDetails();

                if (!panResponseDetails.getNsdlStatus().equals(Status.ERROR.toString())) {

                    PanInfo panInfo = panResponseDetails.getPanInfo();

                    if (null != panInfo && panInfo.getPanStatus().equals("E")) {


                        String firstLastName = GngUtils.stringToFirstMidlleLastName(Arrays.asList(panInfo.getFirstName(), panInfo.getLastName()));

                        String panNameFullName = GngUtils.stringToFirstMidlleLastName(Arrays.asList(panInfo.getFirstName(), panInfo.getMiddleName(), panInfo.getLastName()));

                        AddressMatching addressMatching = new AddressMatching();

                        float matchResult = addressMatching.fuzzyScore(panNameFullName, firstLastName);

                        moduleOutcome.setMessage(Status.EXIST.toString());

                        moduleOutcome.setFieldValue(panNameFullName);

                        moduleOutcome.setNameScore(AddressMatching.df.format(matchResult));

                        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());

                    } else if (panResponseDetails.getPanInfo().getPanStatus().equalsIgnoreCase("F")) {

                        moduleOutcome.setMessage(Status.FAKE_PAN.toString());

                        moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);

                        moduleOutcome.setNameScore(FieldSeparator.BLANK);

                        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());

                    } else {

                        moduleOutcome.setMessage(Status.NOT_FOUND.toString());

                        moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);

                        moduleOutcome.setNameScore(FieldSeparator.BLANK);

                        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());
                    }

                } else {

                    logger.warn("PanVerificationExecutor getting pan response with error.");

                    moduleOutcome.setFieldValue(Status.ERROR.toString());

                    moduleOutcome.setMessage(applicantPanResponse.getKycResponse().getPanResponseDetails().getErrorList().get(0).getDescription());

                    goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());

                }
            } else {

                logger.warn(" panVerificationJob not getting pan response.");

                moduleOutcome.setFieldValue(Status.NO_RESPONSE.toString());

                moduleOutcome.setMessage("No Response");

                goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());
            }
        } catch (Exception ex) {

            moduleOutcome.setFieldValue(Status.NO_RESPONSE.toString());

            moduleOutcome.setMessage(Status.ERROR.toString());

            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.toString());

            logger.error(" error occurred while doing pan cam with probable cause {} ", ex.getMessage());
        }


        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

        logger.debug(" panVerificationJob saved pan response in DB completed ");


        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }

    private PanResponse callPanService() {

        PanResponse panResponse = null;

        PanRequest panRequest = panEntityBuilder.buildPanRequest(goNoGoCustomerApplication);

        if (null != panRequest) {

            panRequest.setGngRefId(goNoGoCustomerApplication.getGngRefId());

            moduleRequestRepository.savePanRequest(panRequest);

            panResponse = panService(panRequest);

        }else{
            logger.trace("generated pan request {} ", panRequest);
            logger.debug("We are not able to create your pan request as data send by u don't have a valid pan number or it may be blank");
        }
        return panResponse;

    }


    private PanResponse panService(PanRequest panRequest) {


        PanResponse panResponse = null;
        String url = null;
        try {

            String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

            WFJobCommDomain wfCommDomainJobByType = workFlowCommunicationManager
                    .getWfCommDomainJobByType(institutionId, WfJobTypeConst.PAN_VERIFICATION_JOB.getValue());


            final String panJsonRequest = JsonUtil.ObjectToString(panRequest);

            url = StringUtils.join(new Object[]{wfCommDomainJobByType.getBaseUrl(),wfCommDomainJobByType.getEndpoint()},FieldSeparator.BLANK );

            HashMap<String, String> param = new HashMap<String, String>() {{
                put("INSTITUTION_ID", wfCommDomainJobByType.getInstitutionId());
                put("AGGREGATOR_ID", wfCommDomainJobByType.getAggregatorId());
                put("MEMBER_ID", wfCommDomainJobByType.getMemberId());
                put("PASSWORD", wfCommDomainJobByType.getPassword());
                put("inputJson_", panJsonRequest);
            }};

            logger.debug(" calling pan service on  endpoint  {} with params {} " , url ,param );

            String tempResponse = httpTransportationService.postRequest(url, param);

            if (StringUtils.isNotBlank(tempResponse)) {

                panResponse = JsonUtil.StringToObject(tempResponse, PanResponse.class);

            }

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception Occurred while send request to pan service with probable  cause as {}", e.getMessage());
            throw new SystemException(String.format("error occurred while getting pan response from pan service having endpoint as [%s] ", url));
        }
        return panResponse;
    }

    private void callCoApplicantPanService() {

        // fetch communication domain for pan
        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        WFJobCommDomain panCommDomain = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, WfJobTypeConst.PAN_VERIFICATION_JOB.getValue());

        List<CoApplicant> applicants = goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getCoApplicant();

        if (panCommDomain == null && (applicants == null || applicants.isEmpty())) {

            logger.debug(" pan  communication domain is not registered for institutionId {} please contact your institution admin ", institutionId);

            return;
        }


        PanResponse panResponse;

        PanRequest panRequest;

        int count = 0;

        PanServiceResponse panServiceResponse;

        for (CoApplicant applicant : applicants) {

            panRequest = panEntityBuilder.buildPanRequest(goNoGoCustomerApplication);

            if (null != panRequest) {

                moduleRequestRepository.savePanRequest(panRequest);

                panResponse = panService(panRequest);

                if (null != panResponse) {

                    goNoGoCustomerApplication.getApplicantComponentResponseList().get(count).setPanServiceResponse(panResponse);

                    panServiceResponse = new PanServiceResponse();

                    panServiceResponse.setRefId(goNoGoCustomerApplication.getGngRefId());

                    panServiceResponse.setCoApplicantId(applicant.getApplicantId());

                    panServiceResponse.setPanResponse(panResponse);

                    moduleRequestRepository.savePanResponse(panServiceResponse);

                }
            }
            count++;
        }

    }
}
