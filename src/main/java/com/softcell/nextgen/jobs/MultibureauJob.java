package com.softcell.nextgen.jobs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.pickup.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.MultiBureauRequestEntityBuilder;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 19/2/17.
 */
@Component
public class MultibureauJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(MultibureauJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Boolean isCoApplicant;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private MultiBureauRequestEntityBuilder requestBuilder;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    @Autowired
    private HttpTransportationService httpTransportationService;


    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {
    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug(" Multi bureau job execution started");


        RequestJsonDomain requestJsonDomain = requestBuilder.buildInitialRequest(goNoGoCustomerApplication);

        if (null != requestJsonDomain && null != requestJsonDomain.getRequest()) {


            requestJsonDomain.setGngRefId(goNoGoCustomerApplication.getGngRefId());


            moduleRequestRepository.saveBureauRequest(requestJsonDomain);

            ResponseMultiJsonDomain responseMultiJsonDomain = callMultiBureauService(requestJsonDomain);


            goNoGoCustomerApplication.getApplicantComponentResponse().setMultiBureauJsonRespose(responseMultiJsonDomain);

            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.COMPLETE.name());

            updateMbStatus(responseMultiJsonDomain);

        } else {

            ModuleOutcome moduleOutcome = new ModuleOutcome();

            moduleOutcome.setFieldName(ScoringDisplayName.CIBIL_SCORE);

            moduleOutcome.setFieldValue("NO RESPONSE");

            moduleOutcome.setMessage("No Score");

            moduleOutcome.setOrder(ScoringDisplayName.CIBIL_SCORE_ORDER);

            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.name());
        }


        if (isCoApplicant) {
            callCoApplicantMBService();
        }


        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }


    private ResponseMultiJsonDomain callMultiBureauService(@Valid RequestJsonDomain requestJsonDomain) throws JsonProcessingException {

        ResponseMultiJsonDomain multiJsonDomain = null;

        String acknowledgementRequest = JsonUtil.ObjectToString(requestJsonDomain);

        AcknowledgmentDomain acknowledgmentDomain = null;

        logger.info(" multi bureau job  calling MultiBureau endpoint  with request for  referenceId [{}]", goNoGoCustomerApplication.getGngRefId());

        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        WFJobCommDomain wfMultiBureauJob = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, WfJobTypeConst.MULTIBUREAU_JOB.getValue());

        String acknowledgementResponse = sendToService(acknowledgementRequest, institutionId);

        if (StringUtils.isNotBlank(acknowledgementResponse)) {

            try {

                logger.info(" MultiBureau job about to get ack response from  service for {} referenceId.", goNoGoCustomerApplication.getGngRefId());

                acknowledgmentDomain = JsonUtil.StringToObject(acknowledgementResponse, AcknowledgmentDomain.class);

                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.IN_PROCESS.name());

            } catch (IOException e) {

                logger.error("Exception occurred while converting acknowledgement response to domain with cause {}", e.getMessage());

                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.name());

            }

        } else {

            logger.error("multi bureau job  not getting ack response from  service for  referenceId {}", goNoGoCustomerApplication.getGngRefId());

        }

        if (acknowledgmentDomain != null) {

            if (Status.SUCCESS.name().equalsIgnoreCase(acknowledgmentDomain.getStatus())) {

                IssueJsonDomain issueJsonDomain = requestBuilder.buildIssueRequest(acknowledgmentDomain);

                String issueJson = JsonUtil.ObjectToString(issueJsonDomain);

                if (StringUtils.isNotBlank(issueJson)) {

                    int i = 0;

                    do {

                        try {

                            String responseJson = sendToService(issueJson, institutionId);

                            if (i > 0) {

                                logger.debug("Hey Ya !! its {} times , take some nap while invoking multi bureau service again , this service really sucks !! ", i + 1);

                                TimeUnit.SECONDS.sleep(i);
                            }

                            if (responseJson != null) {

                                logger.info(" issue response received from multi bureau service  for referenceId {} ",
                                        goNoGoCustomerApplication.getGngRefId());

                                try {

                                    multiJsonDomain = JsonUtil.StringToObject(responseJson, ResponseMultiJsonDomain.class);


                                    if (null != multiJsonDomain && !Status.IN_PROCESS.name().equalsIgnoreCase(multiJsonDomain.getStatus())
                                            && multiJsonDomain.getInprocessList() == null) {
                                        return multiJsonDomain;
                                    }

                                } catch (IOException e) {
                                    logger.error("error occurred while converting final response string  to respective domain with cause {} ",
                                            e.getMessage());

                                    goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.name());

                                }
                            } else {
                                logger.warn("multi bureau job  not getting final response from service for referenceId {} ",
                                        goNoGoCustomerApplication.getGngRefId());
                            }


                        } catch (Exception e) {

                            logger.error(" error occurred while getting final response from multi bureau ", e.getMessage());

                            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.name());

                        }

                        i++;

                    } while (null == multiJsonDomain && i < wfMultiBureauJob.getNoOfRetry());


                }
            } else {

                multiJsonDomain = new ResponseMultiJsonDomain();

                multiJsonDomain.setStatus("REJECTED");

                List<WarningAndError> errorList = acknowledgmentDomain.getErrors();

                if (errorList != null && !errorList.isEmpty()) {
                    multiJsonDomain.setErrors(errorList);
                }
            }

        }
        return multiJsonDomain;
    }

    private String sendToService(String requestJson, String institutionId) {

        WFJobCommDomain wfMultiBureauJob = workFlowCommunicationManager.getWfCommDomainJobByType(institutionId, WfJobTypeConst.MULTIBUREAU_JOB.getValue());

        String url = StringUtils.join(Arrays.asList(wfMultiBureauJob.getBaseUrl(), wfMultiBureauJob.getEndpoint()), FieldSeparator.BLANK) ;

        Map<String, String> postMap = new HashMap<String, String>() {{
            put("INSTITUTION_ID", wfMultiBureauJob.getInstitutionId());
            put("AGGREGATOR_ID", wfMultiBureauJob.getAggregatorId());
            put("MEMBER_ID", wfMultiBureauJob.getMemberId());
            put("PASSWORD", wfMultiBureauJob.getPassword());
            put("inputJson_", requestJson);
        }};

        String tempResponse = null;
        try {

            tempResponse = httpTransportationService.postRequest(url, postMap);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("error occurred while sending request to service with endpoint as {} having cause {} ", url, e.getMessage());
        }

        return tempResponse;


    }

    private void updateMbStatus(ResponseMultiJsonDomain applicantMultiBureauResponse) {

        ModuleOutcome moduleOutcome = new ModuleOutcome();

        moduleOutcome.setFieldName(ScoringDisplayName.CIBIL_SCORE);

        moduleOutcome.setOrder(ScoringDisplayName.CIBIL_SCORE_ORDER);


        if (applicantMultiBureauResponse != null) {

            List<Finished> finishedList = applicantMultiBureauResponse.getFinishedList();

            if (null != finishedList && !finishedList.isEmpty()) {

                for (Finished finished : finishedList) {

                    if (GNGWorkflowConstant.CIBIL.toFaceValue().equalsIgnoreCase(finished.getBureau())) {
                        /**
                         * set status of cibil response
                         */
                        moduleOutcome.setMessage(finished.getStatus());

                        CibilResponse cibilResponse = null;

                        try {

                            String s = JsonUtil.ObjectToString(finished.getResponseJsonObject());

                            cibilResponse = JsonUtil.StringToObject(s, CibilResponse.class);

                        } catch (Exception e) {
                            e.printStackTrace();
                            logger.error("error  Object to cibil Response object conversion with probable cause {} ", e.getMessage());
                        }


                        if (null != cibilResponse && null != cibilResponse.getScoreList() && !cibilResponse.getScoreList().isEmpty()) {

                            moduleOutcome.setFieldValue(cibilResponse.getScoreList().get(0).getScore());

                        } else {

                            moduleOutcome.setFieldValue("-");
                        }
                    }
                }
            } else {
                moduleOutcome.setFieldValue("-");
                moduleOutcome.setMessage(Status.ERROR.name());
            }

        } else {
            moduleOutcome.setMessage(Status.ERROR.name());
            moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);
        }

        goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);

        goNoGoCustomerApplication.getIntrimStatus().setCibilScore(Status.COMPLETE.name());
    }

    private void callCoApplicantMBService() throws SystemException {

        List<CoApplicant> applicants = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();

        RequestJsonDomain requestJsonDomain;

        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

        String gngId = goNoGoCustomerApplication.getGngRefId();

        List<ComponentResponse> applicantComponentResponseList = new ArrayList<ComponentResponse>();


        for (CoApplicant applicant : applicants) {

            requestJsonDomain = requestBuilder.buildCoApplicantInitialRequest(applicationRequest, applicant);

            if (null != requestJsonDomain) {

                moduleRequestRepository.saveBureauRequest(requestJsonDomain);

                ResponseMultiJsonDomain coApplicantMultiBureauResponse = null;
                try {

                    coApplicantMultiBureauResponse = callMultiBureauService(requestJsonDomain);

                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    logger.error("error occurred while calling multi bureau service for co-applicant ");
                    throw new SystemException(" error occurred while calling multi bureau service ");
                }

                if (coApplicantMultiBureauResponse != null) {

                    MultiBureauCoApplicantResponse coApplicantResponse = new MultiBureauCoApplicantResponse();

                    coApplicantResponse.setApplicantRefId(gngId);

                    coApplicantResponse.setCoApplicantRefId(applicant.getApplicantId());

                    coApplicantResponse.setMultiBureauJsonRespose(coApplicantMultiBureauResponse);

                    moduleRequestRepository.saveCoApplicantMBResponse(coApplicantResponse);

                    ComponentResponse componentResponse = new ComponentResponse();

                    componentResponse.setReferenceId(gngId);

                    componentResponse.setApplicantId(applicant.getApplicantId());

                    componentResponse.setMultiBureauJsonRespose(coApplicantMultiBureauResponse);

                    applicantComponentResponseList.add(componentResponse);
                }

            }
        }
        goNoGoCustomerApplication.setApplicantComponentResponseList(applicantComponentResponseList);
    }
}
