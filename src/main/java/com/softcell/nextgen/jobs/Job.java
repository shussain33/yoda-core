package com.softcell.nextgen.jobs;

import com.softcell.nextgen.JobResult;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.concurrent.Callable;

/**
 * Created by prateek on 19/2/17.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface Job extends Callable<JobResult> {

    void prerequisite(Object obj, Collection<Job> subJobs);

    default  void prerequisite(Object obj,Boolean isCoApplicant){}

    default void prerequisite(Collection<Job> obj){}

}
