package com.softcell.nextgen.jobs;

import com.softcell.constants.FieldSeparator;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.posidex.PosidexRequestLog;
import com.softcell.gonogo.model.posidex.DedupeEnquiryRequest;
import com.softcell.gonogo.model.posidex.DedupeEnquiryResponse;
import com.softcell.gonogo.model.posidex.GetEnquiryRequest;
import com.softcell.gonogo.model.response.PosidexResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.PosidexRequestBuilder;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.Module;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.constants.WfJobResultType;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.utils.TransportUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by yogeshb on 29/8/17.
 */
public class PosidexJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(PosidexJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Boolean isCoApplicant;

    @Autowired
    private PosidexRequestBuilder posidexRequestBuilder;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    private Map<String, Object> resultMap = new HashedMap();

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        WFJobCommDomain posidexConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), UrlType.POSIDEX_DO_ENQUIRY.toValue());

        WFJobCommDomain posidexGetEnquiryStatusConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), UrlType.POSIDEX_GET_ENQUIRY_STATUS.toValue());

        DedupeEnquiryRequest dedupeEnquiryRequest = posidexRequestBuilder.buildInitialRequest(goNoGoCustomerApplication, posidexConfig);

        PosidexResponse posidexResponse = new PosidexResponse();

        DedupeEnquiryResponse dedupeEnquiryResponse;

        DedupeEnquiryResponse posidexGetStatusResponse;

        boolean posidexDedupFlag = false;

        int size = 0;

        ModuleOutcome posidexModuleResult = null;

        if (null != posidexConfig && null != posidexGetEnquiryStatusConfig && null != dedupeEnquiryRequest) {

            PosidexRequestLog posidexRequestLog = new PosidexRequestLog();

            posidexRequestLog.setDate(new Date());

            posidexRequestLog.setRefId(goNoGoCustomerApplication.getGngRefId());

            posidexRequestLog.setDedupeDoEnquiryRequest(dedupeEnquiryRequest);

            String posidexFirstServiceUrl = Arrays.asList(posidexConfig.getBaseUrl(), posidexConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

            try {

                dedupeEnquiryResponse = (DedupeEnquiryResponse) TransportUtils.postJsonRequest(dedupeEnquiryRequest, posidexFirstServiceUrl, DedupeEnquiryResponse.class);

                if (null != dedupeEnquiryResponse) {

                    posidexResponse.setDedupeDoEnquiryResponse(dedupeEnquiryResponse);

                    GetEnquiryRequest getEnquiryRequest = posidexRequestBuilder.buildIssueRequest(dedupeEnquiryRequest, posidexGetEnquiryStatusConfig);

                    posidexRequestLog.setGetEnquiryStatusRequest(getEnquiryRequest);

                    String posidexSecondServiceUrl = Arrays.asList(posidexGetEnquiryStatusConfig.getBaseUrl(), posidexGetEnquiryStatusConfig.getEndpoint())
                            .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

                    int retryCount = 0;
                    String statusCode = null;
                    String statusMessage = null;

                    do {

                        //Slip for 5 second
                        Thread.sleep(5000);

                        posidexGetStatusResponse = (DedupeEnquiryResponse) TransportUtils
                                .postJsonRequest(getEnquiryRequest, posidexSecondServiceUrl, DedupeEnquiryResponse.class);

                        if (null != posidexGetStatusResponse) {
                            statusCode = posidexGetStatusResponse.getStatusCode();
                            statusMessage = posidexGetStatusResponse.getStatusMessage();
                        }


                        if (retryCount != 0) {
                            LOGGER.debug("Posidex retry attempt ::->> [{}]", retryCount);
                        }

                        retryCount++;
                    }
                    while (retryCount <= posidexGetEnquiryStatusConfig.getNoOfRetry()
                            && StringUtils.equals(Status.ER.name(), statusCode)
                            && StringUtils.equals("Enquiry is in progress, please try after sometime",
                            statusMessage));

                    if (null != posidexGetStatusResponse) {

                        if (!CollectionUtils.isEmpty(posidexGetStatusResponse.getMatchDetails())) {

                            posidexDedupFlag = true;
                            size = posidexGetStatusResponse.getMatchDetails().size();
                        }

                        posidexResponse.setDedupeEnquiryStatusResponse(posidexGetStatusResponse);

                        goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.COMPLETE.name());

                        //Dedupe Found,No Hit, No Match
                        posidexModuleResult = ModuleOutcome.builder()
                                .fieldName(ScoringDisplayName.POSIDEX_DEDUPE_STATUS)
                                .fieldValue(String.valueOf(size))
                                .order(ScoringDisplayName.POSIDEX_RESULT_ORDER)
                                .message(posidexDedupFlag ? "Dedupe Found" : "No Match")
                                .build();

                        goNoGoCustomerApplication.getIntrimStatus().setPosidexModuleResult(posidexModuleResult);

                        resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.COMPLETED);

                    } else {
                        //Dedupe Found,No Hit, No Match
                        posidexModuleResult = ModuleOutcome.builder()
                                .fieldName(ScoringDisplayName.POSIDEX_DEDUPE_STATUS)
                                .fieldValue(String.valueOf(size))
                                .order(ScoringDisplayName.POSIDEX_RESULT_ORDER)
                                .message("No Hit")
                                .build();
                        goNoGoCustomerApplication.getIntrimStatus().setPosidexModuleResult(posidexModuleResult);

                        goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());

                        resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);

                    }

                } else {

                    //Dedupe Found,No Hit, No Match
                    posidexModuleResult = ModuleOutcome.builder()
                            .fieldName(ScoringDisplayName.POSIDEX_DEDUPE_STATUS)
                            .fieldValue(String.valueOf(size))
                            .order(ScoringDisplayName.POSIDEX_RESULT_ORDER)
                            .message("No Hit")
                            .build();

                    goNoGoCustomerApplication.getIntrimStatus().setPosidexModuleResult(posidexModuleResult);


                    goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());

                    resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);

                }

            } catch (Exception e) {

                //Dedupe Found,No Hit, No Match
                posidexModuleResult = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.POSIDEX_DEDUPE_STATUS)
                        .fieldValue(String.valueOf(size))
                        .order(ScoringDisplayName.POSIDEX_RESULT_ORDER)
                        .message("No Hit")
                        .build();

                goNoGoCustomerApplication.getIntrimStatus().setPosidexModuleResult(posidexModuleResult);


                goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());

                LOGGER.error("Error occurred while executing posidex api with probable cause: [{}]", e.getMessage());

                resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);

            }

            goNoGoCustomerApplication.getApplicantComponentResponse().setPosidexResponse(posidexResponse);

            /* Partial Posidex response save in db.  */
            moduleRequestRepository.savePosidexRequest(posidexRequestLog);

        } else {
            //Dedupe Found,No Hit, No Match
            posidexModuleResult = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.POSIDEX_DEDUPE_STATUS)
                    .fieldValue(String.valueOf(size))
                    .order(ScoringDisplayName.POSIDEX_RESULT_ORDER)
                    .message("No Hit")
                    .build();

            goNoGoCustomerApplication.getIntrimStatus().setPosidexModuleResult(posidexModuleResult);

            goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());

            resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);
        }

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        resultMap.put(Module.DEDUPE_STATUS.name(), posidexDedupFlag);
        resultMap.put(Module.JOB_NAME.name(), WfJobTypeConst.POSIDEX_DEDUPE_EXECUTOR.getValue());
        JobResult jobResult = new JobResult();
        jobResult.setResult(resultMap);
        return jobResult;
    }
}
