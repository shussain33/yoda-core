package com.softcell.nextgen.jobs;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.Module;
import com.softcell.nextgen.constants.WfJobResultType;
import com.softcell.nextgen.constants.WfJobTypeConst;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by prateek on 13/2/17.
 */
@Component
public class DedupJobV2 implements Job {

    private static final Logger logger = LoggerFactory.getLogger(DedupJob.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private ModuleRequestRepository moduleRequestRepository;

    private Map<String, Object> resultMap = new HashedMap();

    private DedupeMatch dedupeMatch;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    public void prerequisite(Object obj) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug("Executing Dedup Job ");

        Set<DedupeDetails> dedupeResultSet = null;
        ModuleOutcome internalDedupeModuleResult = null;
        String applicantId = null;
        if(null==moduleRequestRepository)
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

        try {

            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

            if (null != applicationRequest) {
                FinalApplicationMongoRepository finalApplication = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
                FinalApplicationRepository finalApplicationRepository = (FinalApplicationRepository) ApplicationProxy.getProxy(finalApplication, FinalApplicationRepository.class);

                dedupeResultSet = finalApplicationRepository.deDupeCustomerV2(applicationRequest);

                if (null != dedupeResultSet && dedupeResultSet.contains(goNoGoCustomerApplication.getGngRefId())) {
                    dedupeResultSet.remove(goNoGoCustomerApplication.getGngRefId());
                }

                Applicant applicant = applicationRequest.getRequest().getApplicant();
                if (null != applicant) {
                    applicantId = applicant.getApplicantId();
                }

                DedupeMatchDetail matchDetail = null;
                List<DedupeMatchDetail> matchDetailList = new ArrayList();
                for(DedupeDetails dedupeDetails : dedupeResultSet){
                    // create matchdetail obj
                    matchDetail = new DedupeMatchDetail();
                    // set dedupedeatil in matchdetail obj
                    matchDetail.setDedupeDetails(dedupeDetails);
                    //set applicantId for MatchDetial obj
                    matchDetail.setApplicantId(applicantId);
                    // add matchdetail obj in the list
                    matchDetailList.add(matchDetail);
                }

                dedupeMatch = new DedupeMatch();
                dedupeMatch.setRefId(goNoGoCustomerApplication.getGngRefId());
                dedupeMatch.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

                if(CollectionUtils.isNotEmpty(matchDetailList)){
                    // Create DedupeMatch and set the list.
                    dedupeMatch.setDedupeMatchDetails(matchDetailList);
                }else{
                    dedupeMatch.setDedupeMatchDetails(new ArrayList<>());
                }

                //if dedupe match details found, updating to system.
                if(CollectionUtils.isNotEmpty(dedupeMatch.getDedupeMatchDetails())){
                    finalApplicationRepository.saveDedupeInfo(dedupeMatch);
                }

                // Set deduped  ids into gonogoApplication
                Set<String> dedupedRefIdList = dedupeResultSet.stream().map(dedupe -> dedupe.getRefId())
                        .collect(Collectors.toSet());
                int size = dedupeResultSet.size();
                //Dedupe Found,No Hit, No Match
                internalDedupeModuleResult = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                        .fieldValue(String.valueOf(size))
                        .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                        .message(size > 0 ? "Dedupe Found" : "No Match")
                        .build();
                goNoGoCustomerApplication.setDedupedApplications(dedupedRefIdList);
                goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.COMPLETE.toString());
                goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
                logger.debug("after updating status of application in dedup {}", goNoGoCustomerApplication);
                resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.COMPLETED);
            } else {
                //Dedupe Found,No Hit, No Match
                internalDedupeModuleResult = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                        .fieldValue(String.valueOf(0))
                        .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                        .message("No Hit")
                        .build();

                goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
                goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.ERROR.name());
                resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);
            }
        } catch (Exception e) {
            //Dedupe Found,No Hit, No Match
            internalDedupeModuleResult = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.INTERNAL_DEDUPE_STATUS)
                    .fieldValue(String.valueOf(0))
                    .order(ScoringDisplayName.INTERNAL_DEDUPE_ORDER)
                    .message("No Hit")
                    .build();

            goNoGoCustomerApplication.getIntrimStatus().setInternalDedupeResult(internalDedupeModuleResult);
            goNoGoCustomerApplication.getIntrimStatus().setDedupe(Status.ERROR.name());
            logger.error("error occurred while processing de-dup job with probable cause [{}] ", e.getMessage());
            resultMap.put(Module.JOB_STATUS.name(), WfJobResultType.ERROR);
        }

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        resultMap.put(Module.JOB_NAME.name(), WfJobTypeConst.DEDUP_PROCESSOR.getValue());
        resultMap.put(Module.DEDUPE_STATUS.name(), !CollectionUtils.isEmpty(dedupeResultSet));
        JobResult jobResult = new JobResult();
        jobResult.setResult(resultMap);

        return jobResult;
    }

    public DedupeMatch getDedupeMatch() {
        return dedupeMatch;
    }
}
