package com.softcell.nextgen.jobs;

import com.softcell.constants.*;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilRespAccount;
import com.softcell.gonogo.model.core.request.scoring.CibilRespAddress;
import com.softcell.gonogo.model.core.request.scoring.CibilRespEnquiry;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.model.scoring.AddressScoringResult;
import com.softcell.nextgen.JobResult;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.executors.multibureau.AddressMatching;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by prateek on 23/2/17.
 */
@Component
public class VerificationScoringJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(VerificationScoringJob.class);


    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Boolean isCoApplicant;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {
    }

    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug(" execution of cam scoring started... ");

        goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.NOT_AUTHORIZED.name());

        ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                .fieldValue(Status.NOT_AUTHORIZED.name())
                .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                .build();

        ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                .fieldValue(Status.NOT_AUTHORIZED.name())
                .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                .build();

        goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

        goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

        goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

        goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

        getScore();

        goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.COMPLETE.name());

        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }

    /**
     * method to attach scoring result based on business defined rules
     * and result as {@link com.softcell.constants.ResponseConstants}
     * @return  result as [MATCH , NO_MATCH]
     */
    private String getScore() throws IOException {

        ResponseMultiJsonDomain responseMultiJsonDomain = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose();

        if (responseMultiJsonDomain == null) {

            ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                    .fieldValue(FieldSeparator.BLANK)
                    .message(ResponseConstants.NOT_MATCH)
                    .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                    .build();

            ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                    .fieldValue(FieldSeparator.BLANK)
                    .message(ResponseConstants.NOT_MATCH)
                    .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                    .build();


            goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

            goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

            return ResponseConstants.NOT_MATCH;

        } else if (Status.COMPLETED.name().equalsIgnoreCase(responseMultiJsonDomain.getStatus())) {

            goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.VERIFIED.name());

            List<Finished> finishedList = responseMultiJsonDomain.getFinishedList();

            if (finishedList != null) {

                for (Finished finished : finishedList) {

                    if (GNGWorkflowConstant.CIBIL.toFaceValue().equalsIgnoreCase(finished.getBureau())) {

                        String s = JsonUtil.ObjectToString(finished.getResponseJsonObject());

                        CibilResponse cibilResponse = JsonUtil.StringToObject(s, CibilResponse.class);

                        int numberOfEnquiries = 0;

                        List<CibilRespEnquiry> enquiriesList = cibilResponse.getEnquiryList();

                        if (enquiriesList != null && !enquiriesList.isEmpty()) {

                            numberOfEnquiries = enquiriesList.size();
                        }

                        List<CibilRespAccount> accountList = cibilResponse.getAccountList();

                        if (accountList != null && !accountList.isEmpty()) {

                            numberOfEnquiries = numberOfEnquiries + accountList.size();
                        }

                        List<CibilRespAddress> cibilRespAddressList = cibilResponse.getAddressList();

                        List<CustomerAddress> addressList = goNoGoCustomerApplication
                                .getApplicationRequest().getRequest()
                                .getApplicant().getAddress();

                        AddressMatching addressMatching = new AddressMatching();

                        List<AddressScoringResult> addressScoringResultList = new ArrayList<>();

                        DateTime cibilProcessDate = null;

                        if (null != cibilResponse.getHeader()   && null != cibilResponse.getHeader().getDateProceed()) {

                            try {

                                cibilProcessDate = GngDateUtil.getDate(cibilResponse.getHeader().getDateProceed());

                            } catch (Exception e) {

                                logger.error("{}",e.getStackTrace());

                                logger.error("error occurred while parsing processed date from cibil response with probable cause [{}] ", e.getMessage());

                            }

                        }


                        AddressScoringResult addressScoringResult;

                        if ((cibilRespAddressList != null && !cibilRespAddressList.isEmpty()) && numberOfEnquiries > 0) {

                            for (CibilRespAddress cibilRespAddress : cibilRespAddressList) {

                                String addressFromCibil = GngUtils.getNonNormalizedAddress(cibilRespAddress);

                                for (CustomerAddress applicantAdress : addressList) {

                                        if (null != cibilRespAddress.getPinCode() && cibilRespAddress.getPinCode().trim().equalsIgnoreCase(applicantAdress.getPin() + FieldSeparator.BLANK)) {


                                            String applicantAddress = GngUtils.getNonNormalizedAddress(applicantAdress);

                                            float addressScore = addressMatching.fuzzyScore(addressFromCibil,applicantAddress);

                                            addressScoringResult = new AddressScoringResult();

                                            addressScoringResult.setAddType(applicantAdress.getAddressType());

                                            addressScoringResult.setGngRefNo(goNoGoCustomerApplication.getApplicationRequest().getRefID());

                                            addressScoringResult.setAdd1(addressFromCibil.toString());

                                            addressScoringResult.setAdd2(applicantAddress.toString());

                                            addressScoringResult.setScore(addressScore);

                                            /**
                                             * Cibil  Address Stability Months
                                             */
                                            if (cibilRespAddress.getDateReported() != null && cibilProcessDate != null) {

                                                try {

                                                    DateTime addressReportedDate = GngDateUtil.getDate(cibilRespAddress.getDateReported());

                                                    Months monthDiff = Months.monthsBetween(addressReportedDate, cibilProcessDate);

                                                    addressScoringResult.setCibilAddSubdateToDateDays(monthDiff.getMonths());

                                                } catch (Exception exception) {
                                                    exception.printStackTrace();
                                                }

                                            }

                                            addressScoringResultList.add(addressScoringResult);

                                        }
                                }
                            }

                            double officeAddress = 0.0;
                            double residentialAddress = 0;
                            int officeStabilityOfAddress = 0;
                            int residenceStabilityOfAddress = 0;

                            /**
                             *   get address having maximum score and cibil days difference in days. if two score are same.
                             */
                            if (!CollectionUtils.isEmpty(addressScoringResultList)) {

                                AddressScoringResult addressOfficeToScore = null;
                                List<AddressScoringResult> officeAddrList = addressScoringResultList.parallelStream()
                                        .filter(offAddr ->
                                                StringUtils.equals(offAddr.getAddType(),
                                                        GNGWorkflowConstant.OFFICE.toFaceValue()))
                                        .collect(Collectors.toList());


                                if (!CollectionUtils.isEmpty(officeAddrList)) {
                                    //Get the latest address score which will be last record.
                                    addressOfficeToScore = addressMatching.getScore(
                                            officeAddrList.get(officeAddrList.size() - 1));
                                    if (addressOfficeToScore != null) {
                                        officeAddress = addressOfficeToScore.getScore();
                                        officeStabilityOfAddress = addressOfficeToScore
                                                .getCibilAddSubdateToDateDays();
                                    }
                                }

                                List<AddressScoringResult> residentialAddrList = addressScoringResultList.parallelStream()
                                        .filter(offAddr ->
                                                StringUtils.equals(offAddr.getAddType(),
                                                        GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                                        .collect(Collectors.toList());

                                if (!CollectionUtils.isEmpty(residentialAddrList)) {
                                    addressOfficeToScore = addressMatching.getScore(
                                            residentialAddrList.get(residentialAddrList.size() - 1));
                                    if (addressOfficeToScore != null) {
                                        residentialAddress = addressOfficeToScore
                                                .getScore();
                                        residenceStabilityOfAddress = addressOfficeToScore
                                                .getCibilAddSubdateToDateDays();
                                    }
                                }
                            }

                            ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                                    .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                                    .fieldValue(CustomFormat.DF.format(residentialAddress))
                                    .message((Math.max(residentialAddress, officeAddress) > 70) ? ResponseConstants.MATCH : ResponseConstants.NOT_MATCH)
                                    .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                                    .addStability(residenceStabilityOfAddress)
                                    .build();

                            ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                                    .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                                    .fieldValue(CustomFormat.DF.format(officeAddress))
                                    .message((Math.max(residentialAddress, officeAddress) > 70) ? ResponseConstants.MATCH : ResponseConstants.NOT_MATCH)
                                    .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                                    .addStability(officeStabilityOfAddress)
                                    .build();

                            goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

                            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

                            goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

                            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

                            goNoGoCustomerApplication.getApplicantComponentResponse().setAddressScoringResult(addressScoringResultList);


                            if (Math.max(residentialAddress, officeAddress) > 70) {
                                return ResponseConstants.MATCH;
                            }

                        } else {

                            ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                                    .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                                    .fieldValue(FieldSeparator.BLANK)
                                    .message(ResponseConstants.NOT_MATCH)
                                    .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                                    .addStability(-1)
                                    .build();

                            ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                                    .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                                    .fieldValue(FieldSeparator.BLANK)
                                    .message(ResponseConstants.NOT_MATCH)
                                    .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                                    .addStability(-1)
                                    .build();

                            goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

                            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

                            goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

                            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

                        }

                    }
                }
            } else {

                /**
                 * Verification  scoring handle
                 */

                ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                        .fieldValue(FieldSeparator.BLANK)
                        .message(ResponseConstants.NOT_MATCH)
                        .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                        .addStability(-1)
                        .build();

                ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                        .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                        .fieldValue(FieldSeparator.BLANK)
                        .message(ResponseConstants.NOT_MATCH)
                        .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                        .addStability(-1)
                        .build();

                goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

                goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

                goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

                goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

                goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.CIBIL_NO_HIT.name());

                goNoGoCustomerApplication.getIntrimStatus().setCibilScore(Status.COMPLETE.name());

            }

        } else {

            ModuleOutcome residenceModuleOutcome = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS)
                    .fieldValue(FieldSeparator.BLANK)
                    .message(ResponseConstants.NOT_MATCH)
                    .order(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER)
                    .addStability(-1)
                    .build();

            ModuleOutcome officeModuleOutcome = ModuleOutcome.builder()
                    .fieldName(ScoringDisplayName.OFFICE_ADDRESS)
                    .fieldValue(FieldSeparator.BLANK)
                    .message(ResponseConstants.NOT_MATCH)
                    .order(ScoringDisplayName.OFFICE_ADDRESS_ORDER)
                    .addStability(-1)
                    .build();


            goNoGoCustomerApplication.getApplScoreVector().add(residenceModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(residenceModuleOutcome);

            goNoGoCustomerApplication.getApplScoreVector().add(officeModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(officeModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.COMPLETE.name());
        }

        return ResponseConstants.NOT_MATCH;
    }
}
