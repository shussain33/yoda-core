package com.softcell.nextgen.jobs;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilRespScore;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ntc.NTCGnGStatus;
import com.softcell.gonogo.model.ntc.NTCResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.service.NTCService;
import com.softcell.nextgen.JobResult;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by prateek on 19/2/17.
 */
@Component
public class NtcJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(NtcJob.class);

    @Autowired
    private NTCService ntcService;

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Boolean isCoApplicant;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }


    @Override
    public void prerequisite(Object obj, Boolean isCoApplicant) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) obj;
        this.isCoApplicant = isCoApplicant;
    }


    @Override
    public JobResult call() throws Exception {

        logger.debug(" Ntc Job processing started ");

        InterimStatus interimStatus = goNoGoCustomerApplication.getIntrimStatus();

        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();


        /**
         * If this service is not allowed for particular products then skip next flow.
         */
        if (checkExclusiveProductFlag()) {

            interimStatus.setNtcStatus(Status.NOT_APPLICABLE.name());

        }


        Integer cibilScore = getCibilScore();

        logger.info("Inside NTC calculated CIBIL SCORE 1: " + cibilScore);

        try {

            if (cibilScore != null && cibilScore == -1) {

                NTCResponse ntcResponse = callNTCService(institutionId);

                if (null != goNoGoCustomerApplication.getApplicationRequest()  && null != ntcResponse) {

                    goNoGoCustomerApplication.getApplicantComponentResponse().setNtcResponse(ntcResponse);

                    setNtcGngStatus(ntcResponse, interimStatus);
                }
            }

            interimStatus.setNtcStatus(Status.COMPLETE.name());

        } catch (Exception e) {

            interimStatus.setNtcStatus(Status.ERROR.name());

            e.printStackTrace();

            logger.error("error occurred while processing ntc job with probable cause [{}] ", e.getMessage());
        }


        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }

    private void setNtcGngStatus(NTCResponse ntcResponse, InterimStatus interimStatus) {
        if (interimStatus != null) {
            NTCGnGStatus ntcGngStatus = populatGNGStatus(ntcResponse);
            interimStatus.setNtcGngStatus(ntcGngStatus);
        }
    }

    private NTCGnGStatus populatGNGStatus(NTCResponse ntcResponse) {

        NTCGnGStatus ntcGnGStatus = new NTCGnGStatus();

        ntcGnGStatus.setStatus(ntcResponse.getStatus());
        ntcGnGStatus.setScore(ntcResponse.getNtcScore());
        ntcGnGStatus.setBand(ntcResponse.getNtcScoreBand());

        if (ntcResponse.getErrors() != null && ntcResponse.getErrors().size() > 0) {
            ntcGnGStatus.setMessage(ntcResponse.getErrors().get(0).getDescription());
        }
        return ntcGnGStatus;
    }


    /**
     * Call NTC service only if cibil score is less then zero
     *
     * @throws Exception
     */
    private NTCResponse callNTCService(String institutionId) throws Exception{

        NTCResponse ntcResponse = ntcService.callNTC(goNoGoCustomerApplication);
        return ntcResponse;
    }

    /**
     * It is use to get the cibil score form multi bureau response
     *
     * @return
     */
    private Integer getCibilScore() throws IOException {

        Integer scoreInt = null;
        ComponentResponse applicantComponentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();

        if (null != applicantComponentResponse  && null != applicantComponentResponse.getMultiBureauJsonRespose()) {

            ResponseMultiJsonDomain multiBureauResponse = goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getMultiBureauJsonRespose();

            List<Finished> finishedList = multiBureauResponse.getFinishedList();

            if (null != finishedList && !finishedList.isEmpty()) {

                for (Finished finished : finishedList) {

                    if (finished.getBureau().equals(GNGWorkflowConstant.CIBIL.name())) {


                        String s = JsonUtil.ObjectToString(finished.getResponseJsonObject());

                        CibilResponse cibilResponse = JsonUtil.StringToObject(s, CibilResponse.class);

                        List<CibilRespScore> cibilScoreList = cibilResponse.getScoreList();

                        if (cibilScoreList != null && !cibilScoreList.isEmpty()) {

                            CibilRespScore cibilScore = cibilScoreList.get(0);

                            if (cibilScore != null) {

                                String score = cibilScore.getScore();

                                if (StringUtils.isNotBlank(score)) {

                                    scoreInt = Integer.parseInt(getFormattedScore(score));

                                    return scoreInt;

                                }
                            }

                        }
                        break;
                    }
                }
            }
        }

        return scoreInt;
    }



    /**
     *
     * @param score
     * @return
     */
    private static String getFormattedScore(String score) {
        try {
            if (null != score && score.length() == 5) {
                if (score.charAt(3) == '-') {
                    return "-1";
                } else if (Character.getNumericValue(score.charAt(2)) > 0) {
                    return score.substring(2, score.length());
                } else {
                    if (StringUtils.equalsIgnoreCase(score, "00000")) {
                        return "0";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("error occurred while formatting score string in ntc job with probable cause [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * @return
     */
    private boolean checkExclusiveProductFlag() {

        ApplicationRequest application = goNoGoCustomerApplication.getApplicationRequest();

        if (application != null) {

            Request request = application.getRequest();

            if (request.getApplication() != null) {

                String loanType = request.getApplication().getLoanType();

                return Cache.NTC_EXCLUDED_PRODUCT_SET.contains(loanType);

            }

        }

        return false;
    }
}
