package com.softcell.nextgen.constants;

/**
 * Created by prateek on 22/2/17.
 */
public enum  WfJobResultType {
    COMPLETED,
    SUCCESS,
    ERROR,
    IN_PROCESS
}
