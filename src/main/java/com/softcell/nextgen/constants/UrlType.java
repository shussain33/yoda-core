package com.softcell.nextgen.constants;

/**
 * Created by yogeshb on 10/3/17.
 */
public enum UrlType {

    TOP_UP_DEDUPE("TOP_UP_DEDUPE"),
    CRM_DEDUPE("CRM_DEDUPE"),
    AADHAAR_OTP("aadharOtp"),
    AADHAAR_EKYC("aadharKyc"),
    SONY_CAV("sonyCav"),
    SONY_MOBILE("sonyMobile"),
    INTEX_SERIAL_NUMBER("intexSerialNumber"),
    PANASONIC_SERIAL_NUMBER("panasonicSerialNumber"),
    PANASONIC_IMEI("panasonic-imei"),
    VIDEOCON_SERIAL_NUMBER("videoconSerialNumber"),
    VIDEOCON_ROLLBACK("videocon_rollback"),
    APPLE("apple"),
    APPLE_ROLLBACK("apple_rollback"),
    SAMSUNG("samsung"),
    SAMSUNG_IMEI("samsung-imei"),
    LG_SERIAL("lg"),
    LG_IMEI("lg-imei"),
    GIONEE_IMEI("gionee-imei"),
    DMS_ADD_FOLDER("dms_add_folder"),
    DMS_POST_DOCUMENT("dms_post_document"),
    DMS_CONNECT_DIS("dms_connect_dis"),
    DMS_SEARCH_FOLDER("dms_search_folder"),
    TOTAL_KYC("total_kyc"),
    TOTAL_KYC_KDL("total_kyc_kdl"),
    TOTAL_KYC_KDL_V2("total_kyc_kdl_v2"),
    TOTAL_KYC_KLPG("total_kyc_klpg"),
    TOTAL_KYC_KLPG_V2("total_kyc_klpg_v2"),
    TOTAL_KYC_KELEC("total_kyc_kelec"),
    TOTAL_KYC_KELEC_V2("total_kyc_kelec_v2"),
    TOTAL_KYC_KVOTERID("total_kyc_kvoterid"),
    TOTAL_KYC_KVOTERID_V2("total_kyc_kvoterid_v2"),
    TOTAL_KYC_GSTAUTH("total_kyc_gstauth"),
    TOTAL_KYC_GSTIDENTITY("total_kyc_gstidentity"),
    TOTAL_KYC_PANAUTH("total_kyc_panauth"),
    TVR("tvr"),
    TKIL("tkil"),
    POSIDEX_DO_ENQUIRY("posidexDoEnquiry"),
    POSIDEX_GET_ENQUIRY_STATUS("posidexGetEnquiryStatus"),
    HDB_SOLICITED_QUICK_DATA("hdb_solicited_quick_data"),
    HDB_EXISTING_QUICK_DATA("hdb_existing_quick_data"),
    EMUDRA_ESIGN("emudra_esign"),
    SFTP_FILE("sftp_file"),
    CREDIT_VIDYA_GET_USER_PROFILE("get-user-profile"),
    TVS_CD_GET_STATUS("getCDStatus"),
    TVS_CD_INSERT_CD("insertCDCase"),
    KENT("kent"),
    OPPO_IMEI("oppo-imei"),
    GOOGLE_IMEI("google-imei"),
    GOOGLE_IMEI_ROLLBACK("google-imei-rollback"),
    TVS_SAP_ENTRY("tc_los_tvs_sap"),
    YUHO_IMEI("yuho-imei"),
    COMIO_IMEI("comio-imei"),
    COMIO_IMEI_ROLLBACK("comio-imei-rollback"),
    NOKIA_IMEI("nokia_imei"),
    NOKIA_IMEI_ROLLBACK("nokia_imei_rollback"),
    ONIDA_SERIAL_NUMBER("onida-serial-number"),
    VOLTAS_SERVICE_CODE_REGISTRATION("voltas_service_code_registration"),
    IMPS("imps"),
    LOSTVS_INITIATE("lostvs_initiate"),
    LOSTVS_GROUP_ONE("lostvs_group_one"),
    LOSTVS_GROUP_TWO("lostvs_group_two"),
    LOSTVS_GROUP_THREE("lostvs_group_three"),
    LOSTVS_GROUP_FOUR("lostvs_group_four"),
    LOSTVS_GROUP_FIVE("lostvs_group_five"),
    LAVA_IMEI("lava_imei"),
    CARRIER_SERIAL_NUMBER("carrier-serial-number"),
    CARRIER_SERIAL_ROLLBACK("carrier-serial-rollback"),

    GUPSHUP("gupshup"),
    MIFIN("mifin"),
    DIGI_MIFIN("digi_mifin"),
    SEND_BACK("send-back"),
    SEND_NOTIFICATION("send-notification"),
    KARZA("karza"),
    QUICKCHECK("quickcheck"),
    PERFIOS_GENERATE_FILE("PERFIOS_GENERATE_FILE"),

    LMS("lms"),

    // For SBFC-LMS
    RETRIEVE_CLIENT_BY_IDENTIFIER("retrieve-client-by-identifier"),
    CLIENT_CREATION("client-creation"),
    CREATE_LOAN("create-loan-api"),
    APPROVE("approve"),
    DISBURSE("disburse"),

	CREDIT_VIDYA("creditVidya"),
    FINFORT("FINFORT"),

    INSURANCE("INSURANCE"),

    INSURANCE_PREMIUM_CALCULATION("INSURANCE_PREMIUM_CALCULATION") ,

    RELIGARE("RELIGARE"),

    GENERIC_MAIL("generic_mail"),
    FETCH_FINBIT_DATA("fetch_finbit_data"),
    FINBIT_MONTHLY_SUMMARY("finbit_monthly_summary"),
    ICICI_CORPORATE_API("icici-corporate-api"),
    MIFINPUSHIMD("mifin_push_imd");

    private final String stringValue;

    private UrlType(final String s) {
        stringValue = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toValue() {
        return stringValue;
    }
}
