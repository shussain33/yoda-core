package com.softcell.nextgen.constants;

/**
 * Created by yogeshb on 4/9/17.
 */
public enum Module {
    DEDUPE_STATUS,
    JOB_STATUS,
    JOB_NAME
}
