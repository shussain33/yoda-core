package com.softcell.nextgen.constants;

/**
 * Created by prateek on 14/2/17.
 */
public enum WfJobTypeConst {

    MULTIBUREAU_PROCESSOR("multiBureauProcessor", "01"),
    MULTIBUREAU_JOB("multiBureauExecutor", "02"),

    NTC_PROCESSOR("ntcProcessor", "03"),
    NTC_EXECUTOR("ntcExecutor", "04"),
    DEDUP_PROCESSOR("dedupeProcessor", "05"),

    NEGATIVE_DEDUP("NegativeDedupe", "06"),
    DEDUP_EXECUTOR("DedupeExecuter", "07"),

    SCORING_PROCESSOR("scoringProcessor", "08"),

    SCORING_JOB("scoringExecutor", "09"),
    VERIFICATION_SCORING_JOB("verificationScoring", "10"),

    PAN_VERIFICATION_JOB("panVarificationExecuter", "11"),
    KYC_PROCESSOR("kycProcessor", "12"),
    SALESFORCE_PROCESSOR("salesforceProcessor", "13"),
    SALESFORCE_EXECUTOR("salesforceExecutor", "14"),

    AMAZON_PROCESSOR("amazonS3Processor", "15"),
    AMAZON_EXECUTOR("amazonS3ServiceExecutor", "16"),
    AADHAR_EXECUTOR("aadharExecutor", "17"),
    POSIDEX_DEDUPE_EXECUTOR("posidexExecutor", "18");

    private final String value;
    private final String code;

    private WfJobTypeConst(String value, String code) {
        this.value = value;
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

}

