package com.softcell.nextgen.domain;


import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.HeaderKey;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by prateek on 13/2/17.
 */
@Document(collection = "wf_communication_settings")
public class WFJobCommDomain extends AuditEntity {

    @Id
    private String id;

    @NotNull
    @Field("institutionId")
    private String institutionId;

    @NotNull
    @Field("productName")
    private String productName;

    @Field("aggregatorId")
    private String aggregatorId;

    @Field("memberId")
    private String memberId;

    @Field("password")
    private String password;

    @Field("licenseKey")
    private String licenseKey;

    @NotNull
    @Field("baseUrl")
    private String baseUrl;

    @NotNull
    @Field("endpoint")
    private String endpoint;

    @NotNull
    @Field("noOfRetry")
    private Integer noOfRetry;

    @Field("serviceId")
    private String serviceId;

    @Field("isEnabled")
    private Boolean isEnabled;

    @Field("updatedDt")
    private Date updatedDt;

    @Field("insertDt")
    private Date insertDt;

    @NotNull
    @Field("type")
    private String type;

    @Field("keys")
    private List<HeaderKey> keys;

    @Field("helpDeskNumber")
    private String helpDeskNumber;

    @Field("sessionType")
    private String sessionType;

    @Field("url")
    private String url;

    @Field("username")
    private String username;

    @Field("userRole")
    private String userRole;

    @Field("userEmail")
    private String userEmail;


    public WFJobCommDomain() {
    }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAggregatorId() {
        return aggregatorId;
    }

    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public Integer getNoOfRetry() {
        return noOfRetry;
    }

    public void setNoOfRetry(Integer noOfRetry) {
        this.noOfRetry = noOfRetry;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Date getInsertDt() {
        return insertDt;
    }

    public void setInsertDt(Date insertDt) {
        this.insertDt = insertDt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public List<HeaderKey> getKeys() {
        return keys;
    }

    public void setKeys(List<HeaderKey> keys) {
        this.keys = keys;
    }

    public String getHelpDeskNumber() {
        return helpDeskNumber;
    }

    public void setHelpDeskNumber(String helpDeskNumber) {
        this.helpDeskNumber = helpDeskNumber;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("WFJobCommDomain{");
        sb.append("id='").append(id).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", productName='").append(productName).append('\'');
        sb.append(", aggregatorId='").append(aggregatorId).append('\'');
        sb.append(", memberId='").append(memberId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", licenseKey='").append(licenseKey).append('\'');
        sb.append(", baseUrl='").append(baseUrl).append('\'');
        sb.append(", endpoint='").append(endpoint).append('\'');
        sb.append(", noOfRetry=").append(noOfRetry);
        sb.append(", serviceId='").append(serviceId).append('\'');
        sb.append(", isEnabled=").append(isEnabled);
        sb.append(", updatedDt=").append(updatedDt);
        sb.append(", insertDt=").append(insertDt);
        sb.append(", type='").append(type).append('\'');
        sb.append(", keys=").append(keys);
        sb.append(", helpDeskNumber='").append(helpDeskNumber).append('\'');
        sb.append(", sessionType='").append(sessionType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WFJobCommDomain that = (WFJobCommDomain) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (aggregatorId != null ? !aggregatorId.equals(that.aggregatorId) : that.aggregatorId != null) return false;
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (licenseKey != null ? !licenseKey.equals(that.licenseKey) : that.licenseKey != null) return false;
        if (baseUrl != null ? !baseUrl.equals(that.baseUrl) : that.baseUrl != null) return false;
        if (endpoint != null ? !endpoint.equals(that.endpoint) : that.endpoint != null) return false;
        if (noOfRetry != null ? !noOfRetry.equals(that.noOfRetry) : that.noOfRetry != null) return false;
        if (serviceId != null ? !serviceId.equals(that.serviceId) : that.serviceId != null) return false;
        if (isEnabled != null ? !isEnabled.equals(that.isEnabled) : that.isEnabled != null) return false;
        if (updatedDt != null ? !updatedDt.equals(that.updatedDt) : that.updatedDt != null) return false;
        if (insertDt != null ? !insertDt.equals(that.insertDt) : that.insertDt != null) return false;
        if (helpDeskNumber != null ? !helpDeskNumber.equals(that.helpDeskNumber) : that.helpDeskNumber != null) return false;
        if (sessionType != null ? !sessionType.equals(that.sessionType) : that.sessionType != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (aggregatorId != null ? aggregatorId.hashCode() : 0);
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (licenseKey != null ? licenseKey.hashCode() : 0);
        result = 31 * result + (baseUrl != null ? baseUrl.hashCode() : 0);
        result = 31 * result + (endpoint != null ? endpoint.hashCode() : 0);
        result = 31 * result + (noOfRetry != null ? noOfRetry.hashCode() : 0);
        result = 31 * result + (serviceId != null ? serviceId.hashCode() : 0);
        result = 31 * result + (isEnabled != null ? isEnabled.hashCode() : 0);
        result = 31 * result + (updatedDt != null ? updatedDt.hashCode() : 0);
        result = 31 * result + (insertDt != null ? insertDt.hashCode() : 0);
        result = 31 * result + (helpDeskNumber != null ? helpDeskNumber.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (sessionType != null ? sessionType.hashCode() : 0);
        return result;
    }

    public static class Builder {
        private WFJobCommDomain wfJobCommDomain = new WFJobCommDomain();

        public WFJobCommDomain build() {
            return this.wfJobCommDomain;
        }

        public Builder institutionId(String institutionId) {
            this.wfJobCommDomain.setInstitutionId(institutionId);
            return this;
        }

        public Builder productName(String productName) {
            this.wfJobCommDomain.setProductName(productName);
            return this;
        }

        public Builder aggregratorId(String aggregatorId) {
            this.wfJobCommDomain.setAggregatorId(aggregatorId);
            return this;
        }

        public Builder memberId(String memberId) {
            this.wfJobCommDomain.setMemberId(memberId);
            return this;
        }

        public Builder password(String password) {
            this.wfJobCommDomain.setPassword(password);
            return this;
        }

        public Builder baseUrl(String baseUrl) {
            this.wfJobCommDomain.setBaseUrl(baseUrl);
            return this;
        }

        public Builder endpoint(String endpoint) {
            this.wfJobCommDomain.setEndpoint(endpoint);
            return this;
        }

        public Builder noOfRetry(Integer noOfRetry) {
            this.wfJobCommDomain.setNoOfRetry(noOfRetry);
            return this;
        }

        public Builder serviceId(String serviceId) {
            this.wfJobCommDomain.setServiceId(serviceId);
            return this;
        }

        public Builder isEnabled(Boolean isEnabled) {
            this.wfJobCommDomain.setEnabled(isEnabled);
            return this;
        }

        public Builder updatedDt(Date updatedDt) {
            this.wfJobCommDomain.setUpdatedDt(updatedDt);
            return this;
        }

        public Builder insertDt(Date insertDt) {
            this.wfJobCommDomain.setInsertDt(insertDt);
            return this;
        }

        public Builder type(String type){
            this.wfJobCommDomain.setType(type);
            return this;
        }

        public Builder licenseKey(String licenseKey){
            this.wfJobCommDomain.licenseKey = licenseKey;
            return this;
        }

        public Builder helpDeskNumber(String helpDeskNumber){
            this.wfJobCommDomain.helpDeskNumber = helpDeskNumber;
            return this;
        }

        public Builder sessionType(String sessionType){
            this.wfJobCommDomain.sessionType = sessionType;
            return this;
        }


    }
}
