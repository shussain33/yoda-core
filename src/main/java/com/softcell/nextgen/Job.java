package com.softcell.nextgen;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 13/2/17.
 */
public abstract class Job implements Callable<JobResult>{



    @Override
    public JobResult call() throws Exception {

        TimeUnit.SECONDS.sleep(5);

        return new JobResult();
    }

}
