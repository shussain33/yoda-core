package com.softcell.nextgen;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by prateek on 13/2/17.
 */
public class JobResult {

    private Map<String, Object> result = new ConcurrentHashMap<>();

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobResult)) return false;
        JobResult jobResult = (JobResult) o;
        return Objects.equals(result, jobResult.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("result", result)
                .toString();
    }
}
