package com.softcell.nextgen.processors;

import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by prateek on 23/2/17.
 */
@Component
public class ScoringProcessor implements Job {

    private static final Logger logger = LoggerFactory.getLogger(ScoringProcessor.class);


    private Collection<Job> subJobs;

    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {
    }


    @Override
    public void prerequisite(Collection<Job> subJobs) {
        this.subJobs = subJobs;
    }

    @Override
    public JobResult call() throws Exception {


        logger.debug("scoring processor started with sub job count {} ", subJobs.size());

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        try{

            subJobs.parallelStream().forEach(job -> executorService.submit(job));

        }catch (Exception e){
            logger.error("{}",e.getStackTrace());
            logger.error("");
        } finally {

            logger.debug(" checking for scoring executor service, if it is completed with processing  result [{}]",
                    executorService.isTerminated());

            if(!executorService.isTerminated()){
                executorService.shutdown();
            }
        }

        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }
}
