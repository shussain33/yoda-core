package com.softcell.nextgen.processors;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by prateek on 20/2/17.
 */
@Component
public class KycProcessor implements Job {

    private static final Logger logger = LoggerFactory.getLogger(KycProcessor.class);

    private Collection<Job> subJob;


    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    @Override
    public void prerequisite(Collection<Job> subJob) {
        this.subJob = subJob;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug("Kyc processor initialization started with sub job count {} ", subJob.size());

        ExecutorService executorService = Executors.newSingleThreadExecutor();


        try{

            subJob.parallelStream().forEach(job -> executorService.submit(job));


        }catch (Exception e){

            logger.error("error occurred while starting kycProcessor sub jobs with probable cause {}  ", e.getMessage());

            throw new SystemException("error occurred while starting kycProcessor sub jobs with probable cause  "+ e.getMessage());

        }finally {

            if(!executorService.isTerminated()){

                logger.warn("shutting down KycProcessor executor service as all job finished ");

                executorService.shutdown();
            }

        }

        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;

    }
}
