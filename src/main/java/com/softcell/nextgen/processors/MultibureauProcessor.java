package com.softcell.nextgen.processors;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by prateek on 20/2/17.
 */
@Component
public class MultibureauProcessor implements Job {

    private static final Logger logger = LoggerFactory.getLogger(MultibureauProcessor.class);


    private Collection<Job> subJobs;


    @Override
    public void prerequisite(Object obj, Collection<Job> subJobs) {

    }

    @Override
    public void prerequisite(Collection<Job> subJobs) {
        this.subJobs = subJobs;
    }

    @Override
    public JobResult call() throws Exception {

        logger.debug(" Multi bureau job processor execution started with child job count {} ", subJobs.size());


        ExecutorService executorService = Executors.newFixedThreadPool(5);

        try {

            executorService.invokeAll(subJobs);

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("error occurred while processing multi bureau sub jobs with probable cause {} ", e.getMessage());

            throw new SystemException("error occurred while processing multi bureau sub jobs with probable cause "+ e.getMessage());

        }finally {

            if(!executorService.isShutdown()){

                logger.warn("shutting down multi bureau Processor executor service as all job finished ");

                executorService.shutdown();

            }

        }

        JobResult jobResult = new JobResult();

        jobResult.setResult(new HashMap<>());

        return jobResult;
    }
}
