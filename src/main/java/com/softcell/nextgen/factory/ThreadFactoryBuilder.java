package com.softcell.nextgen.factory;

import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Thread.UncaughtExceptionHandler;

/**
 * this is a customized thread factory that will help u do following things
 * 1 . To have custom thread names
 * 2 . To choose between thread types
 * 3 . To choose Thread Priority
 * 4 . To handle uncaught exceptions
 * <p>
 * USAGES:
 */
 /*_
 *  ThreadFactory customFactory =  new ThreadFactoryBuilder().
 *              setNamePrefix().
 *              setDeamon().
 *              setPriority().
 *              setUncaughtExceptionHandler(new UncaughtExceptionHandler(){
 *                  @Override
 *                  public void uncaughtException(Thread t, Throwable e){
 *                        System.err.println(String.format("Thread %s threw exception - %s",t.getName(), e.getMessage()));
 *                  }
 *               }).build();
 *
 *
 * Created by prateek on 16/2/17.
 */
public class ThreadFactoryBuilder {

    private String namePrefix = null;

    private boolean daemon = false;

    private int priority = Thread.NORM_PRIORITY;

    private ThreadFactory backingThreadFactory = null;

    private UncaughtExceptionHandler uncaughtExceptionHandler = null;

    private static ThreadFactory build(ThreadFactoryBuilder builder) {
        final String namePrefix = builder.namePrefix;
        final Boolean daemon = builder.daemon;
        final Integer priority = builder.priority;
        final UncaughtExceptionHandler uncaughtExceptionHandler = builder.uncaughtExceptionHandler;
        final ThreadFactory backingThreadFactory = (builder.backingThreadFactory != null) ? builder.backingThreadFactory
                : Executors.defaultThreadFactory();

        final AtomicLong count = new AtomicLong(0);

        return runnable -> {
            Thread thread = backingThreadFactory.newThread(runnable);
            if (namePrefix != null) {
                thread.setName(namePrefix + "-" + count.getAndIncrement());
            }
            if (daemon != null) {
                thread.setDaemon(daemon);
            }
            if (priority != null) {
                thread.setPriority(priority);
            }
            if (uncaughtExceptionHandler != null) {
                thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
            }
            return thread;
        };
    }

    public ThreadFactoryBuilder setNamePrefix(String namePrefix) {
        if (StringUtils.isBlank(namePrefix)) {
            throw new NullPointerException();
        }
        this.namePrefix = namePrefix;
        return this;
    }

    public ThreadFactoryBuilder setDeamon(Boolean deamon) {
        this.daemon =  deamon;
        return this;
    }

    public ThreadFactoryBuilder setPriority(int priority) {
        if (priority < Thread.MIN_PRIORITY) {
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be >= %s", priority,
                    Thread.MIN_PRIORITY));
        }

        if (priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be <= %s", priority,
                    Thread.MAX_PRIORITY));
        }

        this.priority = priority;
        return this;
    }

    public ThreadFactoryBuilder setUncaughtExceptionHandler(
            UncaughtExceptionHandler uncaughtExceptionHandler) {
        if (null == uncaughtExceptionHandler) {
            throw new NullPointerException(
                    "UncaughtExceptionHandler cannot be null");
        }
        this.uncaughtExceptionHandler = uncaughtExceptionHandler;
        return this;
    }

    public ThreadFactoryBuilder setThreadFactory(
            ThreadFactory backingThreadFactory) {
        if (null == uncaughtExceptionHandler) {
            throw new NullPointerException(
                    "BackingThreadFactory cannot be null");
        }
        this.backingThreadFactory = backingThreadFactory;
        return this;
    }

    public ThreadFactory build() {
        return build(this);
    }


}
