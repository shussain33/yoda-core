package com.softcell.nextgen.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.jobs.*;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by prateek on 26/2/17.
 */
@org.springframework.stereotype.Component
public class JobFactory {


    private static final Logger logger = LoggerFactory.getLogger(JobFactory.class);


    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    public List<Job> getRegisteredMultibureauSubJobs(Map<String, ModuleSetting> moduleSettingMap,
                                                     GoNoGoCustomerApplication goNoGoCustomerApplication) {

        // @formatter:off
        List<Job> subJobs = moduleSettingMap.entrySet()
                .parallelStream()
                .filter(module -> WfJobTypeConst.MULTIBUREAU_JOB.getValue().equalsIgnoreCase(module.getKey()))
                .map(module -> {
                    Job multibureauJob = new MultibureauJob();
                    autowireCapableBeanFactory.autowireBean(multibureauJob);
                    multibureauJob.prerequisite(goNoGoCustomerApplication, module.getValue().isCoApplicant());
                    return multibureauJob;
                }).collect(Collectors.toList());

        // @formatter:on

        return subJobs;
    }


    public List<Job> getRegisteredKycSubJobs(Map<String, ModuleSetting> moduleSettingMap,
                                             GoNoGoCustomerApplication goNoGoCustomerApplication) {

        // @formatter:off

        List<Job> kycSubJobs = moduleSettingMap.entrySet()
                .parallelStream()
                .filter(module ->
                                WfJobTypeConst.PAN_VERIFICATION_JOB.getValue().equalsIgnoreCase(module.getKey()) ||
                                        WfJobTypeConst.AADHAR_EXECUTOR.getValue().equalsIgnoreCase(module.getKey())
                ).map(module -> {
                    Job job = null;

                    if (WfJobTypeConst.PAN_VERIFICATION_JOB.getValue().equalsIgnoreCase(module.getKey())) {
                        Job panVerificationJob = new PanVerificationJob();
                        autowireCapableBeanFactory.autowireBean(panVerificationJob);
                        panVerificationJob.prerequisite(goNoGoCustomerApplication, module.getValue().isCoApplicant());
                        job = panVerificationJob;
                    } else if (WfJobTypeConst.AADHAR_EXECUTOR.getValue().equalsIgnoreCase(module.getKey())) {
                        Job aadhaarJob = new AadharJob();
                        autowireCapableBeanFactory.autowireBean(aadhaarJob);
                        aadhaarJob.prerequisite(goNoGoCustomerApplication, module.getValue().isCoApplicant());
                        job = aadhaarJob;
                    } else {
                        logger.debug(" moduleId {}  is not registered for ComponentId {} ", module.getKey(), "kycProcessor");
                    }
                    return job;
                }).collect(Collectors.toList());

        // @formatter:on

        return kycSubJobs;
    }

    public List<Job> getRegisteredScoringSubJob(Map<String, ModuleSetting> moduleSettingMap,
                                                GoNoGoCustomerApplication goNoGoCustomerApplication) {

        // @formatter:off


        List<Job> subJobs = moduleSettingMap.entrySet()
                .parallelStream()
                .filter(module -> WfJobTypeConst.VERIFICATION_SCORING_JOB.getValue().equalsIgnoreCase(module.getKey()) ||
                        WfJobTypeConst.SCORING_JOB.getValue().equalsIgnoreCase(module.getKey()))
                .map(module -> {
                    Job job = null;
                    if(WfJobTypeConst.VERIFICATION_SCORING_JOB.getValue().equalsIgnoreCase(module.getKey())){
                        Job verificationJob = new VerificationScoringJob();
                        autowireCapableBeanFactory.autowireBean(verificationJob);
                        verificationJob.prerequisite(goNoGoCustomerApplication, module.getValue().isCoApplicant());
                        job = verificationJob;
                    }else if(WfJobTypeConst.SCORING_JOB.getValue().equalsIgnoreCase(module.getKey())){
                        Job scoringJob = new ScoringJob();
                        autowireCapableBeanFactory.autowireBean(scoringJob);
                        scoringJob.prerequisite(goNoGoCustomerApplication, module.getValue().isCoApplicant());
                        job = scoringJob;
                    }else{
                        logger.debug(" module {} is not registered with ScoringProcessor  ", module.getKey());
                    }
                    return job;

        }).collect(Collectors.toList());

        // @formatter:on

        return subJobs;
    }
}
