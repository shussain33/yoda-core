package com.softcell.nextgen.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

/**
 * A helper class for running runnable Instances (Jobs) to run as a callable Instances
 * in executors
 * <p>
 * Created by prateek on 16/2/17.
 */
public class ThreadFactoryHelper {


    /**
     * method to convert collection {@see java.util.Collection} of runnable {@see java.lang.Runnable}
     * to collection {@see java.util.Collection} of callable {@see java.util.Callable}
     *
     * @param runnable
     * @return
     */
    public Collection<Callable<Void>> runnableToCallable(Collection<Runnable> runnable) {

        Collection<Callable<Void>> callables = new ArrayList<>();

        if (!runnable.isEmpty()) {

            for (Runnable runable : runnable) {
                callables.add(toCallable(runable));
            }

        }

        return callables;
    }

    private Callable<Void> toCallable(Runnable runnable) {
        return () -> {
            runnable.run();
            return null;
        };
    }

}
