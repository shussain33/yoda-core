package com.softcell.nextgen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/*_
 * This class servers as a monitoring utility for aadhar processor
 * it tells u following things
 * 1. poolSize
 * 2. corePoolSize
 * 3. active task count
 * 4. completed task count
 * 5. total holding task count
 * 6. boolean flag to get shutdown status of executor
 * 7. boolean flag representing whether executor pool is terminated or not
 */

/**
 * Created by prateek on 17/2/17.
 */
@Component
@Scope("prototype")
public class ExecutorPerformanceAnalyst implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorPerformanceAnalyst.class);

    private ThreadPoolExecutor executor;

    private int seconds;

    private boolean run = true;

    public ExecutorPerformanceAnalyst(ThreadPoolExecutor executor, int delay) {
        this.executor = executor;
        this.seconds = delay;
    }

    public void shutdown() {
        this.run = false;
    }

    @Override
    public void run() {

        while (this.run) {

            logger.debug(String.format("[monitor] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s , isTerminated : %s",
                    this.executor.getPoolSize(),
                    this.executor.getCorePoolSize(),
                    this.executor.getActiveCount(),
                    this.executor.getCompletedTaskCount(),
                    this.executor.getTaskCount(),
                    this.executor.isShutdown(),
                    this.executor.isTerminated()));

            try {
                TimeUnit.SECONDS.sleep(this.seconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
