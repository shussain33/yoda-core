package com.softcell.nextgen.manager;


import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.factory.ThreadFactoryBuilder;
import com.softcell.nextgen.jobs.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * Created by prateek on 12/2/17.
 */
@Service(value = "nextGenWorkFlowManager" )
public class NextGenWorkFlowManager {

    private static final Logger logger = LoggerFactory.getLogger(NextGenWorkFlowManager.class);

    private ExecutorService executorService;

    private List<Job> jobList;

    private int corePoolSize = 2;

    private int maxPoolSize = 4;

    private int keepAliveTime = 5000;

    public void addJobs(List<? extends Job>  jobs) {

        logger.debug("adding jobs {} to manager ", Optional.ofNullable(jobs.size()));
        jobList = new ArrayList<>();
        jobList.addAll(jobs);
    }

    public Boolean shutDownJobsIfCompleted(){

        logger.debug("shutting down executor as task finishes ");

        if(!executorService.isShutdown()){
            executorService.shutdown();
        }

        return true;

    }


    public List<Future<JobResult>> processJob() {

        logger.debug("Total job count for executor {}", jobList.size());


        ThreadFactory customThreadFactory =  new ThreadFactoryBuilder().
                              setNamePrefix("GNG-WORKFLOW").
                              setDeamon(false).
                              setPriority(Thread.NORM_PRIORITY).
                              setUncaughtExceptionHandler((t, e) -> System.err.println(String.format("Thread %s threw exception - %s",t.getName(), e.getMessage()))).build();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                keepAliveTime,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingDeque<>(),
                customThreadFactory,
                new RejectExecutionHandler());

        executorService = threadPoolExecutor;

        List<Future<JobResult>> futures = null;

        try {

            logger.debug("starting processing of added jobs ");

            futures = executorService.invokeAll(jobList);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {

            executorService.shutdown();

            try {

                executorService.awaitTermination(20000, TimeUnit.MILLISECONDS);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return futures;

    }

    public static class RejectExecutionHandler implements RejectedExecutionHandler {

        private static final Logger logger = LoggerFactory.getLogger(RejectExecutionHandler.class);

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            logger.debug("{} is rejected.", r.toString());
        }
    }
}
