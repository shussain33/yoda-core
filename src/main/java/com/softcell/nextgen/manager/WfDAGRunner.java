package com.softcell.nextgen.manager;

import com.softcell.config.ComponentConfiguration;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.dao.mongodb.repository.ComponentConfigurationRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.factory.JobFactory;
import com.softcell.nextgen.factory.ThreadFactoryBuilder;
import com.softcell.nextgen.jobs.DedupJob;
import com.softcell.nextgen.jobs.Job;
import com.softcell.nextgen.jobs.NtcJob;
import com.softcell.nextgen.processors.KycProcessor;
import com.softcell.nextgen.processors.MultibureauProcessor;
import com.softcell.nextgen.processors.ScoringProcessor;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.ComponentSetting;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by prateek on 19/2/17.
 */
@Service
public class WfDAGRunner {

    private static final Logger logger = LoggerFactory.getLogger(WfDAGRunner.class);

    @Autowired
    private NextGenWorkFlowManager nextGenWorkFlowManager;

    private int corePoolSize = 4;

    private int maxPoolSize = 8;

    private int keepAliveTime = 5000;


    @Autowired
    private ComponentConfigurationRepository componentConfigurationRepository;

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Autowired
    private JobFactory jobFactory;

    @Async
    public BaseResponse buildDag(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        List<Future<JobResult>> futures;

        ThreadFactory customThreadFactory = new ThreadFactoryBuilder().
                setNamePrefix("GNG-WORKFLOW").
                setDeamon(false).
                setPriority(Thread.NORM_PRIORITY).
                setUncaughtExceptionHandler((t, e) -> System.err.println(String.format("Thread %s threw exception - %s", t.getName(), e.getMessage()))).build();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                keepAliveTime,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingDeque<>(),
                customThreadFactory,
                new NextGenWorkFlowManager.RejectExecutionHandler());

        String institutionId =  goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        List<ComponentConfiguration> componentConfigurations = componentConfigurationRepository
                .getSystemComponentConfiguration(ComponentConfigurationType.BASIC,institutionId);


        ComponentSetting componentSetting = componentConfigurations.get(0).getComponentSettings();

        Map<Integer, List<Component>> componentMap = componentSetting.getComponentMap();

        List<Job> jobs = new ArrayList<>();

        for (Map.Entry<Integer, List<Component>> integerListEntry : componentMap.entrySet()) {

            List<Component> value = integerListEntry.getValue();

            for (Component component : value) {

                String componentId = component.getComponentId();

                if (StringUtils.equalsIgnoreCase(componentId, WfJobTypeConst.DEDUP_PROCESSOR.getValue())) {

                    DedupJob dedupJob = new DedupJob();

                    autowireCapableBeanFactory.autowireBean(dedupJob);

                    dedupJob.prerequisite(goNoGoCustomerApplication, false);

                    jobs.add(dedupJob);

                } else if (StringUtils.equalsIgnoreCase(componentId, WfJobTypeConst.MULTIBUREAU_PROCESSOR.getValue())) {

                    Map<String, ModuleSetting> moduleSettingMap = component.getModuleSettingMap();

                    List<Job> registeredMultibureauSubJobs = jobFactory.getRegisteredMultibureauSubJobs(moduleSettingMap, goNoGoCustomerApplication);

                    if (null != registeredMultibureauSubJobs && !registeredMultibureauSubJobs.isEmpty()) {

                        Job multibureauProcessor = new MultibureauProcessor();

                        autowireCapableBeanFactory.autowireBean(multibureauProcessor);

                        multibureauProcessor.prerequisite(registeredMultibureauSubJobs);

                        jobs.add(multibureauProcessor);


                    }

                } else if (StringUtils.equalsIgnoreCase(componentId, WfJobTypeConst.KYC_PROCESSOR.getValue())) {

                    Map<String, ModuleSetting> moduleSettingMap = component.getModuleSettingMap();

                    List<Job> registeredKycSubJobs = jobFactory.getRegisteredKycSubJobs(moduleSettingMap, goNoGoCustomerApplication);

                    if (null != registeredKycSubJobs && !registeredKycSubJobs.isEmpty()) {

                        KycProcessor kycJobProcessor = new KycProcessor();

                        autowireCapableBeanFactory.autowireBean(kycJobProcessor);

                        kycJobProcessor.prerequisite(registeredKycSubJobs);

                        jobs.add(kycJobProcessor);

                    }

                } else if (StringUtils.equalsIgnoreCase(componentId, WfJobTypeConst.NTC_PROCESSOR.getValue())) {

                    NtcJob ntcJob = new NtcJob();

                    autowireCapableBeanFactory.autowireBean(ntcJob);

                    ntcJob.prerequisite(goNoGoCustomerApplication, false);

                    jobs.add(ntcJob);

                } else if (StringUtils.equalsIgnoreCase(componentId, WfJobTypeConst.SCORING_PROCESSOR.getValue())) {

                    Map<String, ModuleSetting> moduleSettingMap = component.getModuleSettingMap();

                    List<Job> registeredScoringSubJob = jobFactory.getRegisteredScoringSubJob(moduleSettingMap, goNoGoCustomerApplication);

                    if (null != registeredScoringSubJob && !registeredScoringSubJob.isEmpty()) {

                        ScoringProcessor scoringProcessor = new ScoringProcessor();

                        autowireCapableBeanFactory.autowireBean(scoringProcessor);

                        scoringProcessor.prerequisite(registeredScoringSubJob);

                        jobs.add(scoringProcessor);


                    }

                }

            }
        }

        nextGenWorkFlowManager.addJobs(jobs);

        futures = nextGenWorkFlowManager.processJob();

        try {

            nextGenWorkFlowManager.shutDownJobsIfCompleted();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occurred while closing executor with probable cause [{}] ", e.getMessage());
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, futures);

    }


}
