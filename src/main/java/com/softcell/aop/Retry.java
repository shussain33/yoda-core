package com.softcell.aop;

import java.lang.annotation.*;

/**
 * Created by prateek on 27/2/17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Retry {

    /**
     * Declare exception types the retry will be issue on
     *
     * @return exception type causing a retry
     */
    Class<? extends Exception>[] on();


    /**
     * no of retry attempts
     * @return retry attempts
     */
    int times() default 1;


    /**
     * Fail if the current thread is enlisted in a running transaction.
     *
     * @return fail in case of a running transaction
     */
    boolean failInTransaction() default true;



}
