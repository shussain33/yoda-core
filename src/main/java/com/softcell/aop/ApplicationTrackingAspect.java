package com.softcell.aop;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.ApplicationTrackingMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationTrackingRepository;
import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.service.factory.ApplicationTrackingBuilder;
import com.softcell.gonogo.service.factory.impl.ApplicationTrackingBuilderImpl;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * This class is to aspect application tracking like information of application
 * like DSA , institution , dealer application status , application date
 *
 * @author bhuvneshk
 */
@Aspect
public class ApplicationTrackingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationTrackingAspect.class);

    private ApplicationTracking applicationTracking;


    private ApplicationTrackingBuilder builder = new ApplicationTrackingBuilderImpl();

    private ApplicationTrackingRepository repo = new ApplicationTrackingMongoRepository();


    /**
     * This is to keep log of ApplicationRequest before it hit the
     * initApplicationLifeCycle
     */

    @Pointcut("execution(* *.run()) ||"
            + "execution( * com.softcell.rest.controllers.GradualApplicationController.saveApplication(..)) ||"
            + "execution( * com.softcell.rest.controllers.CroController.croApproval(..)) ||"
            + "execution( * com.softcell.rest.controllers.CroController.updateInvoiceDetails(..)) ||"
            + "execution( * com.softcell.rest.controllers.CroController.resetQueue(..)) ||"
            + "execution( * com.softcell.rest.controllers.CroController.updateLosDetails(..)) ||"
            + "execution( * com.softcell.rest.controllers.SerialNumberValidationController.serialNumberValidation(..)) ||"
            + "execution( * com.softcell.rest.controllers.CroController.croOnHold(..)) ||"
            + "execution( * com.softcell.workflow.aggregators.WfResultAggregatorImpl.setQueue(..)) ||"
            + "execution( * com.softcell.rest.controllers.ApplicationController.setPostIpaStage(..)) ||"
            + "execution( * com.softcell.rest.controllers.ApplicationController.setPostIpaPdf(..))")
    public void appTrackingPointCut() {
    }

    @Around("appTrackingPointCut()")
    public Object aroundApplicationTracking(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Object target = joinPoint.getTarget();
        Date startDt = new Date();
        String methodName = joinPoint.getSignature().getName();
        Object result = joinPoint.proceed(args);
        applicationTracking = new ApplicationTracking();
        applicationTracking.setStartDate(startDt);
        applicationTracking.setClassName(target.getClass().getName());
        applicationTracking.setMethodName(methodName);

        args = joinPoint.getArgs();
        target = joinPoint.getTarget();
        buildApplicationTrackingObj(args, target, methodName);
        repo.saveApplicationTrackingObj(applicationTracking);
        applicationTracking = null;
        return result;
    }


    /**
     * this method will intercept all the exceptions that are thrown by all the classes and respected method
     * from packages  com.softcell.workflow.component..* || com.softcell.gonogo..* || com.softcell.gonogo..* || com.softcell.workflow..*
     *
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = "appTrackingPointCut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) throws Exception {
        if (applicationTracking == null) {
            applicationTracking = new ApplicationTracking();
        }
        ApplicationTracking.TrackingException exception = applicationTracking.new TrackingException();
        if (e.getCause() != null) {
            exception.setCause(e.getCause().toString());
        }
        exception.setDevMessage(e.getMessage());
        applicationTracking.setException(exception);
        applicationTracking.setStepId("401");
        repo.saveApplicationTrackingObj(applicationTracking);
    }

    /**
     * Build application tracking object based on the object where point cut
     * meets like From ApplicationRequest or GoNoGoCustomerApplication
     *
     * @param args
     * @param target
     * @param methodName
     */
    private void buildApplicationTrackingObj(Object[] args, Object target, String methodName) {
        ApplicationRequest applicationRequest = null;
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        ModuleSetting moduleSetting = null;
        String status = null;
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ApplicationRequest) {
                applicationRequest = (ApplicationRequest) args[i];
            } else if (args[i] instanceof GoNoGoCustomerApplication) {
                goNoGoCustomerApplication = (GoNoGoCustomerApplication) args[i];
            } else if (args[i] instanceof ModuleSetting) {
                moduleSetting = (ModuleSetting) args[i];
            } else if (args[i] instanceof CroApprovalRequest) {
                CroApprovalRequest request = (CroApprovalRequest) args[i];
                status = setCroApprovalRequest(applicationTracking, request);
            } else if (args[i] instanceof PostIpaRequest) {
                PostIpaRequest request = (PostIpaRequest) args[i];
                status = setPostIpaRequest(request, methodName);
            } else if (args[i] instanceof InvoiceDetailsRequest) {
                InvoiceDetailsRequest request = (InvoiceDetailsRequest) args[i];
                status = setInvoiceDetailsRequest(request);
            } else if (args[i] instanceof LOSDetailsRequest) {
                LOSDetailsRequest request = (LOSDetailsRequest) args[i];
                status = setLOSDetailsRequest(request);
            } else if (args[i] instanceof SerialSaleConfirmationRequest) {
                SerialSaleConfirmationRequest request = (SerialSaleConfirmationRequest) args[i];
                status = setSerialSaleConfirmationRequest(request);
            }

        }

        MetaAction action = null;
        if (target instanceof MetaAction) {
            action = (MetaAction) target;
            goNoGoCustomerApplication = action.getGoNoGoCustomerApplication();
            moduleSetting = action.getModuleSetting();
        } else if (target instanceof SerialNumberResponse) {
            SerialNumberResponse response = (SerialNumberResponse) target;
            status = response.getStatus();
        }

        if (goNoGoCustomerApplication != null & moduleSetting != null) {
            builder.buildAppTracking(goNoGoCustomerApplication, moduleSetting, applicationTracking);
        } else if (goNoGoCustomerApplication != null) {
            builder.buildAppTracking(goNoGoCustomerApplication, applicationTracking);
        } else if (applicationRequest != null) {
            builder.buildAppTracking(applicationRequest, applicationTracking);
        } else if (status != null) {
            builder.buildAppTracking(status, applicationTracking);
        }

        goNoGoCustomerApplication = null;
        moduleSetting = null;
        applicationRequest = null;
    }

    private String setInvoiceDetailsRequest(InvoiceDetailsRequest request) {
        applicationTracking.setInstitutionId(request.getHeader().getInstitutionId());
        String status = "Invoice Generated";
        applicationTracking.setGngRefId(request.getRefID());
        applicationTracking.setStepId("303");
        return status;
    }

    private String setSerialSaleConfirmationRequest(SerialSaleConfirmationRequest request) {
        applicationTracking.setInstitutionId(request.getHeader().getInstitutionId());
        applicationTracking.setStepId("302");
        String status = "Serial Number Validation";
        applicationTracking.setGngRefId(request.getReferenceID());
        return status;

    }

    private String setLOSDetailsRequest(LOSDetailsRequest request) {
        String status = null;
        if (null != request && request.getlOSDetails() != null) {
            applicationTracking.setInstitutionId(request.getHeader().getInstitutionId());
            status = request.getlOSDetails().getStatus();
            if (StringUtils.equalsIgnoreCase(status, GNGWorkflowConstant.DO.toFaceValue())) {
                status = "LOS Delivery Order";
                applicationTracking.setStepId("301");

            } else {
                ApplicationTrackingBuilderImpl.setStepId(applicationTracking,
                        request.getlOSDetails().getStatus());
            }
            applicationTracking.setGngRefId(request.getRefID());
        }
        return status;
    }

    private String setPostIpaRequest(PostIpaRequest request, String methodName) {
        String status = null;
        if (null != request) {
            applicationTracking.setInstitutionId(request.getHeader().getInstitutionId());
        }
        if (StringUtils.equals("setPostIpaPdf", methodName)) {
            status = "DO Generated";
            applicationTracking.setStepId("301");
        } else if (StringUtils.equals("setPostIpaStage", methodName)) {
            status = GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue();
            applicationTracking.setStepId("302");
        }
        applicationTracking.setGngRefId(request.getRefID());
        return status;
    }

    /**
     * @param applicationTracking
     * @param request
     * @return
     */
    private String setCroApprovalRequest(ApplicationTracking applicationTracking,
                                         CroApprovalRequest request) {
        String status = request.getApplicationStatus();
        applicationTracking.setInstitutionId(request.getHeader().getInstitutionId());
        if (GNGWorkflowConstant.APRV.toFaceValue().equalsIgnoreCase(status) || GNGWorkflowConstant.APPROVED.toFaceValue().equalsIgnoreCase(status)) {
            status = GNGWorkflowConstant.APRV.toFaceValue();
            applicationTracking.setStepId("203");
        } else if (GNGWorkflowConstant.DCLN.toFaceValue().equalsIgnoreCase(status) || GNGWorkflowConstant.DECLINED.toFaceValue().equalsIgnoreCase(status)) {
            applicationTracking.setStepId("202");
            status = GNGWorkflowConstant.DCLN.toFaceValue();
        } else {
            applicationTracking.setStepId("203");
        }
        applicationTracking.setGngRefId(request.getReferenceId());
        return status;
    }

}
