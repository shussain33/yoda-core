package com.softcell.aop;

import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

public class ApplicationProxy {


    public static Object getProxy(Object o, Class c) {
        AspectJProxyFactory factory = new AspectJProxyFactory(o);
        factory.addAspect(ApplicationTrackingAspect.class);
        factory.setInterfaces(c);
        return factory.getProxy();
    }
}
