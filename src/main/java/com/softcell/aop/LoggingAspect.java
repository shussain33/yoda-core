package com.softcell.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;


/**
 * Aspect for logging execution of service and repository Spring components.
 */
@Aspect
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);


    /**
     * this is pointcut method which is entry to aop logging
     * and we can do configuring at package label so here we handling top notch project packages
     * by creating a package label pointcut
     */
    @Pointcut("within(com.softcell.workflow.component..*) || within(com.softcell.gonogo..*) || within(com.softcell.reporting..*) || within(com.softcell.workflow..*))")
    public void loggingPointcut() {
    }


    /**
     * this method will intercept all the exceptions that are thrown by all the classes and respected method
     * from packages  com.softcell.workflow.component..* || com.softcell.gonogo..* || com.softcell.gonogo..* || com.softcell.workflow..*
     *
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = "loggingPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        logger.error("Exception in {}.{}() with cause = {} and exception {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), e.getCause(), e);
    }

    /**
     * This method is used for injecting around advice
     * before method call and after returning from method to intercept all method and
     * write log for before and afterr advice
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("loggingPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        logger.debug("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        try {
            Object result = joinPoint.proceed();
            logger.trace("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), result);
            return result;
        } catch (IllegalArgumentException e) {
            logger.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
                    joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
            throw e;
        }
    }
}
