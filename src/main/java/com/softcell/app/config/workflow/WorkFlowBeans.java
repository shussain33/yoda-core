package com.softcell.app.config.workflow;

import com.softcell.nextgen.jobs.AadharJob;
import com.softcell.nextgen.jobs.DedupJob;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by prateek on 19/2/17.
 */
@Configuration
public class WorkFlowBeans {

    @Bean
    public AadharJob getAadharJob(){
        return new AadharJob();
    }

    @Bean
    public DedupJob getDedupJob(){
        return new DedupJob();
    }


}
