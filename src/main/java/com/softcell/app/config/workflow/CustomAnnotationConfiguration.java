package com.softcell.app.config.workflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by prateek on 4/3/17.
 */
@Configuration
@ComponentScan(basePackages = {"com.softcell.dao.mongodb.generic.*"})
public class CustomAnnotationConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(CustomAnnotationConfiguration.class);
}
