package com.softcell.app.config.workflow;

import com.softcell.app.config.web.ProfileConsts;
import com.softcell.config.*;
import com.softcell.gonogo.model.multibureau.pickup.MbCommunicationDomain;
import com.softcell.queuemanager.config.EsConnectionConfig;
import com.softcell.queuemanager.search.SearchESRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by prateek on 8/2/17.
 */
@Configuration
@Profile(ProfileConsts.GNG_DEV)
public class WorkflowConfigurationDev {

    @Bean
    public SearchESRepository urlConfiguration2() {
        return new SearchESRepository();
    }

    @Bean
    public URLConfiguration urlConfiguration() {

        URLConfiguration urlConfiguration = new URLConfiguration();

        urlConfiguration.setMbCommunicationDomainConfig(getMbCommunicationDomainConfig());

        urlConfiguration.setPanCommunicationDomainConfig(getPanCommDomain());

        urlConfiguration.setScoringCommunicationConfig(getScoringCommConfig());

        urlConfiguration.setOtpConfiguration(getOtpConfiguration());

        urlConfiguration.setEmailServiceConfiguration(getEmailConfig());

        urlConfiguration.setEmailServiceConfigurationForPlAndCcbt(getEmailPlCcbtConfig());

        urlConfiguration.setSalesForceConfiguration(getSalesForceConfig());

        urlConfiguration.setDealerEmailMapping(new HashMap<String, String>() {{
            put("4019", "kparkhe@softcell.com");
        }});

        urlConfiguration.setAadharConfiguration(getAadharConfigDomain());

        urlConfiguration.setAuthenticationConfiguration(getAuthConfig());

        urlConfiguration.setEsConnectionConfig(getEsConfig());

        urlConfiguration.setNtcConfiguration(getNtcConfig());

        urlConfiguration.setAmazonS3Configuration(getAmazonConfig());

        urlConfiguration.setCreditCardSurrogateConfig(getCreditCardConfiguration());

        urlConfiguration.setRelianceConfiguration(getRelianceConfig());

        urlConfiguration.setLosConfiguration(getLosConfig());

        urlConfiguration.setSmsServiceConfiguration(getSmsConfig());

        return urlConfiguration;
    }

    @Bean
    public EmailServiceConfiguration getSoftcellEmail() {

        return EmailServiceConfiguration.builder().
                emailUrl("http://products.softcell.in/SoftcellEmailService/send-pdf-multiple-mail").
                baseUrl("http://products.softcell.in/SoftcellEmailService/").
                connectionTimeout(1000).build();

    }


    private Map<String, MbCommunicationDomain> getMbCommunicationDomainConfig() {
        return new HashMap<String, MbCommunicationDomain>() {{
            put("4019", getMbCommDevConfig());
            put("4011", getDmiMbConfig());
            put("3994", getHdfcMbConfig());
            put("4045", getDemoMbConfig());
        }};
    }

    private MbCommunicationDomain getMbCommDevConfig() {

        return MbCommunicationDomain.builder().
                institutionId("4019").
                mbId("test").
                aggregatorId("553").
                memberId("hdbs_cpu_gonogo@hdbs.com").
                password("UCojQlpaNmw").
                mbUrl("http://ua1.multibureau.in/CODExS/saas/saasRequest.action").
                baseUrl("http://ua1.multibureau.in/CODExS").build();

    }

    private MbCommunicationDomain getDmiMbConfig() {

        return MbCommunicationDomain.builder().
                institutionId("4019").
                mbId("test").
                aggregatorId("553").
                memberId("hdbs_cpu_gonogo@hdbs.com").
                password("UCojQlpaNmw").
                mbUrl("http://ua1.multibureau.in/CODExS/saas/saasRequest.action").
                baseUrl("http://ua1.multibureau.in/CODExS").build();
    }

    private MbCommunicationDomain getHdfcMbConfig() {

        return MbCommunicationDomain.builder().
                institutionId("3994").
                mbId("test").
                aggregatorId("553").
                memberId("cpu@hdfc.com").
                password("NyZLJDgzciQ=").
                mbUrl("http://ua1.multibureau.in/CODExS/saas/saasRequest.action").
                baseUrl("http://ua1.multibureau.in/CODExS").build();
    }


    private MbCommunicationDomain getDemoMbConfig() {

        return MbCommunicationDomain.builder().
                institutionId("4045").
                mbId("test").
                aggregatorId("576").
                memberId("cpu_user@softcell.com").
                password("dyNHazI0akU=").
                mbUrl("http://ua1.multibureau.in/CODExS/saas/saasRequest.action").
                baseUrl("http://ua1.multibureau.in/CODExS").build();
    }


    private Map<String, PanCommunicationDomain> getPanCommDomain() {
        return new HashMap<String, PanCommunicationDomain>() {{
            put("4019", getPanConfig());
            put("4045", getPanConfig());
        }};
    }

    private PanCommunicationDomain getPanConfig() {
        return PanCommunicationDomain.builder().
                panUrl("http://202.154.165.80:8080/sokyc/kyc/kycPanRequest.action").
                institutionId("4019").
                aggregatorId("553").
                memberId("hdbs_cpu_gonogo@hdbs.com").
                password("UCojQlpaNmw=").
                baseUrl("http://202.154.165.80:8080/sokyc/").build();
    }


    private Map<String, ScoringCommunication> getScoringCommConfig() {
        return new HashMap<String, ScoringCommunication>() {{
            put("4015", getHdbfsScoringCommConfig());
            put("4045", getDemoScoringConfig());
        }};
    }

    private ScoringCommunication getHdbfsScoringCommConfig() {
        return ScoringCommunication.builder().
                institutionId("4019").
                userId("test").
                password("test").
                scoringServiceUrl("http://gng.softcell.in/AppScoringV2Git/api/ScoringV3/").
                baseUrl("http://gng.softcell.in/AppScoringV2Git/").build();
    }

    private ScoringCommunication getDemoScoringConfig() {

        return ScoringCommunication.builder().
                institutionId("4045").
                userId("test").
                password("test").
                scoringServiceUrl("http://gng.softcell.in/AppScoringV2Git/api/ScoringV3/").
                baseUrl("http://gng.softcell.in/AppScoringV2Git/").build();

    }

    private Map<String, OtpConfiguration> getOtpConfiguration() {
        return new HashMap<String, OtpConfiguration>() {{
            put("4019", getHdbfsOtpDomain());
            put("4045", getDemoOtpDomain());
        }};
    }

    private OtpConfiguration getHdbfsOtpDomain() {
        return OtpConfiguration.builder().
                otpServiceURL("http://products.softcell.in/EmailAndSMSService/otp").
                baseUrl("http://products.softcell.in/EmailAndSMSService/").
                connectionTimeout(10000).build();
    }

    private OtpConfiguration getDemoOtpDomain() {
        return OtpConfiguration.builder().
                otpServiceURL("http://products.softcell.in/EmailAndSMSService2/otp").
                baseUrl("http://products.softcell.in/EmailAndSMSService2/").
                connectionTimeout(10000).build();
    }

    private Map<String, EmailServiceConfiguration> getEmailConfig() {
        return new HashMap<String, EmailServiceConfiguration>() {{
            put("4019", getEmailServiceConfig());
        }};
    }

    private EmailServiceConfiguration getEmailServiceConfig() {

        return EmailServiceConfiguration.builder().
                emailUrl("http://products.softcell.in/EmailAndSMSService/send-digitize-mail").
                baseUrl("http://products.softcell.in/EmailAndSMSService/").
                connectionTimeout(1000).build();

    }

    private Map<String, EmailServiceConfiguration> getEmailPlCcbtConfig() {
        return new HashMap<String, EmailServiceConfiguration>() {{
            put("4019", getPlCcbtEmailConfig());
        }};
    }

    private EmailServiceConfiguration getPlCcbtEmailConfig() {
        return EmailServiceConfiguration.builder().
                emailUrl("http://products.softcell.in/EmailAndSMSService/pl-salary-mail").
                baseUrl("http://products.softcell.in/EmailAndSMSService/").
                connectionTimeout(1000).build();
    }

    private Map<String, SalesForceConfiguration> getSalesForceConfig() {
        return new HashMap<String, SalesForceConfiguration>() {{
            put("4019", getSaleForceDmiDomain());
        }};
    }

    private SalesForceConfiguration getSaleForceDmiDomain() {
        return SalesForceConfiguration.builder().
                userName("dmi.gonogo@softcell.com").
                userPassword("dmigonogo1").
                keyStorePass("pass123").build();
    }

    private Map<String, AadharConfiguration> getAadharConfigDomain() {
        return new HashMap<String, AadharConfiguration>() {{
            put("4019", getAdharConfigDomain());
            put("4045", getAdharConfigDomain());
        }};
    }

    private AadharConfiguration getAdharConfigDomain() {

        return AadharConfiguration.builder().
                institutionId("4019").
                aggregatorId("553").
                password("UCojQlpaNmw=").
                memberId("hdbs_cpu_gonogo@hdbs.com").
                aadharOtpServiceUrl("http://202.154.165.85:90/SoftcellEKYC/kyc/kycOtpRequest.action").
                aadharEkyServiceUrl("http://202.154.165.85:90/SoftcellEKYC/kyc/kycV2AadharRequest.action").
                baseUrl("http://202.154.165.85:90/SoftcellEKYC/").
                licenseKey("MLeV-pjOfS5SwVgSeAr_yExbSHkmaXsMoYgshbxm9HR4Pzvw7MUUw7M").
                pdfReportFlag("N").
                auaCode("STGHDBFS01").
                subAuaCode("STGHDBFS01").
                connectionTimeout(10000).
                nameValidation(true).
                dobValidation(true).
                genderValidation(false).
                ageValidation(false).
                emailValidation(false).
                phoneValidation(false)
                .authApiVersion("2.0").build();
    }

    private Map<String, AuthenticationConfiguration> getAuthConfig() {
        return new HashMap<String, AuthenticationConfiguration>() {{
            put("AUTHENTICATION", getAuthConfigDomain());
        }};
    }

    private AuthenticationConfiguration getAuthConfigDomain() {
        return AuthenticationConfiguration.builder().
                url("http://202.154.165.32:80/Service/authenticate.action").
                baseUrl("http://202.154.165.32:80/Service/").
                changePassUrl("http://202.154.165.32:80/UM/content/changePasswordAction").
                resetPassUrl("http://202.154.165.32:80/UM/content/resetPasswordAction").build();
    }

    private Map<String, EsConnectionConfig> getEsConfig() {
        return new HashMap<String, EsConnectionConfig>() {{
            put("DEFAULT", getEsConfigDomain());
        }};
    }

    private EsConnectionConfig getEsConfigDomain() {

        EsConnectionConfig esConnectionConfig = new EsConnectionConfig();
        esConnectionConfig.setNodeIpAddresses(new String[]{"localhost"});
        esConnectionConfig.setIpAddress("localhost");
        esConnectionConfig.setHttpPort(9200);
        esConnectionConfig.setTcpPort(9300);
        esConnectionConfig.setClusterName("gonogo");
        esConnectionConfig.setUserName("test");
        esConnectionConfig.setPassword("test");
        esConnectionConfig.setType("type");

        return esConnectionConfig;

    }

    private Map<String, NTCConfiguration> getNtcConfig() {
        return new HashMap<String, NTCConfiguration>() {{
            put("4019", getNtcDomain());
        }};
    }

    private NTCConfiguration getNtcDomain() {
        return NTCConfiguration.builder().
                ntcUrl("http://ua1.multibureau.in/CODExS/saas/ntcRequest.action").
                baseUrl("http://ua1.multibureau.in/CODExS/").
                institutionId("4019").
                aggregatorId("553").
                password("UCojQlpaNmw=").
                memberId("hdbs_cpu_gonogo@hdbs.com").build();
    }

    private Map<String, AmazonS3Configuration> getAmazonConfig() {
        return new HashMap<String, AmazonS3Configuration>() {{
            put("4011", getAmazonDmiDomain());
        }};
    }

    private AmazonS3Configuration getAmazonDmiDomain() {
        return AmazonS3Configuration.builder().
                accessKey("AKIAJGPHX5KEGDFXQ23A").
                secretKey("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").
                bucketName("dmigoog").build();
    }

    private Map<String, CreditCardSurrogateConfig> getCreditCardConfiguration() {
        return new HashMap<String, CreditCardSurrogateConfig>() {{
            put("4019", getCreditCardSurrogateDomain());
        }};
    }

    private CreditCardSurrogateConfig getCreditCardSurrogateDomain() {
        return CreditCardSurrogateConfig.builder().
                url("https://hdbapp.hdbfs.com/hdbapp/bin/cc_bin.php").
                baseUrl("https://hdbapp.hdbfs.com/hdbapp/").
                action("get_bin_type").
                target("cc_Bin").
                dedupe(false).build();
    }

    private Map<String, RelianceConfiguration> getRelianceConfig() {
        return new HashMap<String, RelianceConfiguration>() {{
            put("4019", getRelianceConfigDomain());
        }};
    }

    private RelianceConfiguration getRelianceConfigDomain() {
        return RelianceConfiguration.builder().
                url("http://gng.softcell.in/Connector/connect/reliance-digital/").
                baseUrl("http://gng.softcell.in/Connector/").build();

    }

    private Map<String, LosConfiguration> getLosConfig() {
        return new HashMap<String, LosConfiguration>() {{
            put("4019", getLosConfigDomain());
            put("4011", getLosConfigDomain());
        }};
    }

    private LosConfiguration getLosConfigDomain() {
        return LosConfiguration.builder().
                gngPostUrl("http://gng.softcell.in/Connector/losconnect/los-gng-feed").
                mbPostUrl("http://gng.softcell.in/Connector/losconnect/los-mb-feed").
                baseUrl("http://gng.softcell.in/Connector/").build();
    }

    private Map<String, SmsServiceConfiguration> getSmsConfig() {
        return new HashMap<String, SmsServiceConfiguration>() {{
            put("4011", getSmsServiceConfigDomain());
        }};
    }

    private SmsServiceConfiguration getSmsServiceConfigDomain() {
        return SmsServiceConfiguration.builder().
                url("http://localhost:8080/SMSService/send-sms").
                baseUrl("http://localhost:8080/SMSService").build();
    }
}
