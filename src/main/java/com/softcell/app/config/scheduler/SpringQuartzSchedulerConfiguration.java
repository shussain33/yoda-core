package com.softcell.app.config.scheduler;

import com.softcell.app.config.web.ProfileConsts;
import com.softcell.gonogo.scheduler.RunCreditReportJob;
import com.softcell.gonogo.scheduler.RunDailyTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by prateek on 5/2/17.
 */
@Configuration
@ComponentScan("com.softcell.gonogo.scheduler")
@Profile(ProfileConsts.GNG_PROD)
public class SpringQuartzSchedulerConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(SpringQuartzSchedulerConfiguration.class);

    @Autowired private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        logger.info(" Configuring spring quartz job ");
    }

    @Bean
    public JobDetailFactoryBean jobDetail() {

        JobDetailFactoryBean factory = new JobDetailFactoryBean();

        factory.setJobClass(RunCreditReportJob.class);

        Map<String, Object> map = new java.util.HashMap<>();

        map.put("runDailyTask", new RunDailyTask());

        factory.setJobDataAsMap(map);

        factory.setDurability(true);

        factory.setGroup("mygroup");

        factory.setName("dailyCreditReportJob");

        return factory;
    }

    @Bean
    public CronTriggerFactoryBean cronTriggerFactoryBean() {

        CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();

        stFactory.setJobDetail(jobDetail().getObject());

        stFactory.setName("daily12AMTrigger");

        stFactory.setGroup("reporting-group");

        logger.debug("Configuring cron trigger to fire ");

        stFactory.setCronExpression("0 59 23 24 12 ? 2013/2");

        return stFactory;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();

        logger.debug("Configuring Job factory");

        jobFactory.setApplicationContext(applicationContext);

        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {

        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();

        scheduler.setConfigLocation(new ClassPathResource("quartz.properties"));

        scheduler.setJobFactory(springBeanJobFactory());

        logger.debug("Setting then scheduler up ");

        scheduler.setTriggers(cronTriggerFactoryBean().getObject());

        return scheduler;
    }

}
