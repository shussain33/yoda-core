package com.softcell.app.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.util.Arrays;

/**
 * Created by prateek on 9/2/17.
 */

@Configuration
@EnableCaching
public class CacheConfiguration extends CachingConfigurerSupport {

    private static final Logger logger = LoggerFactory.getLogger(CacheConfiguration.class);

    @Bean
    @Override
    public CacheManager cacheManager() {
        logger.debug("Instantiating a cache for application ");
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(new ConcurrentMapCache("default")));
        return cacheManager;
    }

    @PreDestroy
    public void destroy() {
        logger.info("Closing Cache Manager");
    }

}
