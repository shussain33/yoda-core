package com.softcell.app.config.web;

/**
 * Created by prateek on 6/2/17.
 */
public interface ProfileConsts {

    String GNG_SWAGGER = "swagger";
    String GNG_DEV = "dev";
    String GNG_UAT = "uat";
    String GNG_PRE_PROD = "preprod";
    String GNG_PROD = "prod";
    String GNG_SIT = "sit";

}
