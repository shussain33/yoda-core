package com.softcell.app.config.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


/**
 * Created by prateek on 6/2/17.
 */
@Configuration
@ComponentScan(basePackages = {"com.softcell.gonogo.service"})
public class PropertiesSourcesConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesSourcesConfiguration.class);

    private static final Resource DEV_PROPERTIES = new ClassPathResource("gonogo-dev.properties");

    private static final Resource SIT_PROPERTIES = new ClassPathResource("gonogo-sit.properties");

    private static final Resource UAT_PROPERTIES = new ClassPathResource("gonogo-uat.properties");

    private static final Resource PREPROD_PROPERTIES = new ClassPathResource("gonogo-preprod.properties");

    private static final Resource PROD_PROPERTIES = new ClassPathResource("gonogo-prod.properties");


    @Bean
    @Profile(ProfileConsts.GNG_DEV)
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

        logger.info("preparing properties file for application configured with profile {} ", ProfileConsts.GNG_DEV);

        PropertySourcesPlaceholderConfigurer property = getProperty();

        property.setLocation(DEV_PROPERTIES);

        property.setIgnoreUnresolvablePlaceholders(true);

        return property;
    }

    @Bean
    @Profile(ProfileConsts.GNG_UAT)
    public static PropertySourcesPlaceholderConfigurer uatProperties() {

        logger.info("preparing properties file for application configured with profile {} ", ProfileConsts.GNG_UAT);

        PropertySourcesPlaceholderConfigurer property = getProperty();
        property.setLocation(UAT_PROPERTIES);
        property.setIgnoreUnresolvablePlaceholders(true);
        return property;
    }

    @Bean
    @Profile(ProfileConsts.GNG_PRE_PROD)
    public static PropertySourcesPlaceholderConfigurer preprodProperties() {

        logger.info("preparing properties file for application configured with profile {} ", ProfileConsts.GNG_PRE_PROD);

        PropertySourcesPlaceholderConfigurer property = getProperty();
        property.setLocation(PREPROD_PROPERTIES);
        property.setIgnoreUnresolvablePlaceholders(true);
        return property;
    }

    @Bean
    @Profile(ProfileConsts.GNG_PROD)
    public static PropertySourcesPlaceholderConfigurer prodProperties() {

        logger.info("preparing properties file for application configured with profile {} ", ProfileConsts.GNG_PROD);

        PropertySourcesPlaceholderConfigurer property = getProperty();
        property.setLocation(PROD_PROPERTIES);
        property.setIgnoreUnresolvablePlaceholders(true);
        return property;
    }

    @Bean
    @Profile(ProfileConsts.GNG_SIT)
    public static PropertySourcesPlaceholderConfigurer sitProperties() {

        logger.info("preparing properties file for application configured with profile {} ", ProfileConsts.GNG_SIT);

        PropertySourcesPlaceholderConfigurer property = getProperty();
        property.setLocation(SIT_PROPERTIES);
        property.setIgnoreUnresolvablePlaceholders(true);
        return property;
    }

    private static PropertySourcesPlaceholderConfigurer getProperty() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertySourcesPlaceholderConfigurer;
    }


}
