package com.softcell.app.config.web;

import com.codahale.metrics.servlets.AdminServlet;
import com.softcell.rest.filter.LoggingFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.IOException;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class GoNoGoInitializer implements WebApplicationInitializer {

    private static final Logger logger = LoggerFactory.getLogger(GoNoGoInitializer.class);

    @Override
    public void onStartup(ServletContext container) throws ServletException {

        String activeProfiles;

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        try {

            final Attributes manifestAttrs = new Manifest(container.getResourceAsStream("/META-INF/MANIFEST.MF")).getMainAttributes();
            activeProfiles = manifestAttrs.getValue("spring-profiles-active");

        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }

        if (StringUtils.isBlank(activeProfiles)) {
            throw new IllegalStateException("No runtime profiles available, can't boot up the app.");
        }

        logger.info("GoNoGo web application initialization has begun.");

        rootContext.register(WebConfig.class);

        rootContext.setDisplayName("GoNoGo");


        rootContext.setServletContext(container);

        container.addListener(new ContextLoaderListener(rootContext));


        addLoggingFilter(container);

        addEncodingFilter(container);

        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, activeProfiles);

        addMetricsDispatcher(container, rootContext);

        addServiceDispatcherServlet(container, rootContext);

        logger.info("Web application fully configured");
    }

    private void addMetricsDispatcher(ServletContext container, AnnotationConfigWebApplicationContext rootContext){

        ServletRegistration.Dynamic metricsServlet = container.addServlet("metrics", new AdminServlet());

        metricsServlet.addMapping("/metrics/admin/*");

    }

    private void addServiceDispatcherServlet(ServletContext container, AnnotationConfigWebApplicationContext rootContext) {
        final String SERVICES_MAPPING = "/";

        DispatcherServlet dispatcherServlet = new DispatcherServlet(rootContext);

        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);

        ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(rootContext));

        dispatcher.setLoadOnStartup(1);

        dispatcher.setAsyncSupported(true);

        dispatcher.setLoadOnStartup(1);

        Set<String> mappingConflicts = dispatcher.addMapping(SERVICES_MAPPING);

        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                logger.error("Mapping conflict: " + s);
            }
            throw new IllegalStateException("'ServicesDispatcher' could not be mapped to '" + SERVICES_MAPPING + "'");
        }
    }

    private void addEncodingFilter(ServletContext container) {
        FilterRegistration.Dynamic fr = container.addFilter("encodingFilter", new CharacterEncodingFilter());
        fr.setInitParameter("encoding", "UTF-8");
        fr.setInitParameter("forceEncoding", "true");
        fr.addMappingForUrlPatterns(null, true, "/*");
    }



    private void addLoggingFilter(ServletContext container) {

        FilterRegistration.Dynamic fr = container.addFilter("loggingFilter", new LoggingFilter());
        fr.addMappingForUrlPatterns(null, true, "/*");
    }
}
