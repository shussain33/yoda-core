package com.softcell.app.config.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by prateek on 3/2/17.
 */
@EnableAsync
@Configuration
@ComponentScan(basePackages = {
        "com.softcell.service.*",
        "com.softcell.reporting.*",
        "com.softcell.gonogo",
        "com.softcell.gonogo.*",
        "com.softcell.gonogo.utils.*",
        "com.softcell.workflow",
        "com.softcell.nextgen.*",
        "com.softcell.utils.*"

})
@EnableSpringConfigured
public class ServiceConfig {

    private static final Logger logger = LoggerFactory.getLogger(ServiceConfig.class);
}
