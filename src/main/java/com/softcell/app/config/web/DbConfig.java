package com.softcell.app.config.web;

import com.mongodb.*;
import com.softcell.dao.mongodb.config.MongoAuditAware;
import com.softcell.rest.utils.JSR310DateConverters.DateToZonedDateTimeConverter;
import com.softcell.rest.utils.JSR310DateConverters.ZonedDateTimeToDateConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.event.LoggingEventListener;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by prateek on 3/2/17.
 */
@Configuration
@EnableMongoRepositories(basePackages = {"com.softcell.dao.mongodb"})
@ComponentScan(basePackages = {"com.softcell.dao.mongodb.*"})
@EnableMongoAuditing
public class DbConfig extends AbstractMongoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(DbConfig.class);

    @Value("${gonogo.db.name}")
    private String dbName;

    @Value("${gonogo.db.host}")
    private String dbHost;

    @Value("${gonogo.db.port}")
    private int dbPort;

    @Value("${gonogo.db.totalConnection}")
    private int connectionPerHost;

    @Value("${gonogo.db.gridFs.uri}")
    private String gridFsUri;

    @Value("${gonogo.db.client.uri}")
    private String mongoUri;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    protected Collection<String> getMappingBasePackages() {
        return Arrays.asList("com.softcell.gonogo", "com.softcell.nextgen.*","com.softcell.metrics.*","com.softcell.config.*");
    }

    @Override
    public Mongo mongo() throws Exception {

        logger.debug("Initializing mongo db with URI {}", mongoUri);

        ServerAddress serverAddress = new ServerAddress(dbHost, dbPort);

        MongoClientOptions options = MongoClientOptions.builder()
                .connectTimeout(1000).
                connectionsPerHost(connectionPerHost).
                socketKeepAlive(true).
                writeConcern(WriteConcern.ACKNOWLEDGED).
                build();

        MongoClientURI mongoClientURI = new MongoClientURI(mongoUri);
        Mongo mongo = new MongoClient(mongoClientURI);

        logger.debug("Initialization of mongo db client completed successfully using URI {}!!",mongoUri);

        return mongo;
    }

    @Primary
    @Bean
    public MongoTemplate mongoTemplate() throws Exception {


        logger.debug("Initializing mongo template with dbName [{}]", dbName);

        MongoClientURI mongoClientURI = new MongoClientURI(mongoUri);

        MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongoClientURI);

        MappingMongoConverter mappingMongoConverter = new MappingMongoConverter(new DefaultDbRefResolver
                (mongoDbFactory), new
                MongoMappingContext());

        mappingMongoConverter.setCustomConversions(customConversions());

        mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));

        mappingMongoConverter.afterPropertiesSet();

        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, mappingMongoConverter);

        mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);

        logger.debug("Initialization of mongo template completed successfully with URI {}",mongoUri);

        return mongoTemplate;
    }

    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {

        MongoClientURI mongoClientURI = new MongoClientURI(gridFsUri);

        MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongoClientURI);

        MappingMongoConverter mappingMongoConverter = new MappingMongoConverter(new DefaultDbRefResolver
                (mongoDbFactory), new
                MongoMappingContext());

        GridFsTemplate gridFsTemplate = new GridFsTemplate(mongoDbFactory, mappingMongoConverter);

        logger.debug("Initialization of mongo grid fs template completed successfully with URI {}",gridFsUri);

        return gridFsTemplate;
    }

    @Bean(name = "mongoSecondaryBean")
    public MongoTemplate mongoTemplateSecondaryPreference() throws Exception {

        logger.debug("Initializing mongo secondary template with dbName [{}]", dbName);
        try {
            MongoClientURI mongoClientURI = new MongoClientURI(mongoUri);

            MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongoClientURI);

            MappingMongoConverter mappingMongoConverter = new MappingMongoConverter(new DefaultDbRefResolver
                    (mongoDbFactory), new
                    MongoMappingContext());

            mappingMongoConverter.setCustomConversions(customConversions());

            mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));

            mappingMongoConverter.afterPropertiesSet();

            MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, mappingMongoConverter);

            mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);

            mongoTemplate.setReadPreference(ReadPreference.secondaryPreferred());

            logger.debug("Initialization of mongo template completed successfully with URI {}", mongoUri);

            return mongoTemplate;
        } catch (Exception ex) {
            logger.error("Exception occurred while transaction DB connectivity with URI : {}", mongoUri);
            throw ex;
        }
    }

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public CustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(DateToZonedDateTimeConverter.INSTANCE);
        converters.add(ZonedDateTimeToDateConverter.INSTANCE);
        return new CustomConversions(converters);
    }

    @Override
    public MongoDbFactory mongoDbFactory() throws Exception {
        return super.mongoDbFactory();
    }

    @Bean(name = "mongoAudit")
    public AuditorAware<String> mongoAuditProvider() {
        return new MongoAuditAware();
    }


    @Bean
    public LoggingEventListener mappingEventsListener() {
        return new LoggingEventListener();
    }




}
