package com.softcell.app.config.logging;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggerContextListener;
import ch.qos.logback.core.spi.ContextAwareBase;
import net.logstash.logback.appender.LogstashSocketAppender;
import net.logstash.logback.stacktrace.ShortenedThrowableConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Created by prateek on 7/2/17.
 */
@Configuration
public class LoggingConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(LoggingConfiguration.class);


    private LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();


    @Value("${gonogo.logstash.isenable}")
    private Boolean isLogstashenabled;

    @Value("${gonogo.app.version}")
    private String appVersion;

    @Value("${gonogo.logstash.host}")
    private String logstashHost;

    @Value("${gonogo.logstash.port}")
    private Integer logstashPort;

    @Value("${gonogo.logstash.queuecapacity}")
    private Integer logstashQueueCapacity;

    @PostConstruct
    public void init() {

        logger.debug("conditional logstash app as its value retrieved as {} ", isLogstashenabled);

        if (isLogstashenabled) {

            addLogstashAppender(context);

            // Add context listener
            LogbackLoggerContextListener logbackLoggerContextListener = new LogbackLoggerContextListener();
            logbackLoggerContextListener.setContext(context);
            context.addListener(logbackLoggerContextListener);

        }


    }

    public void addLogstashAppender(LoggerContext loggerContext) {

        logger.info("Initializing Logstash logging  ");

        LogstashSocketAppender logstashSocketAppender = new LogstashSocketAppender();
        logstashSocketAppender.setName("LOGSTASH");
        logstashSocketAppender.setContext(context);

        String customField = "{\"appName\" : \"Gonogo-core \", \"appVersion\":\"appVersion\"}";

        // Set the Logstash appender config from gonogo properties
        logstashSocketAppender.setSyslogHost(logstashHost);
        logstashSocketAppender.setPort(logstashPort);
        logstashSocketAppender.setCustomFields(customField);

        // Limit the maximum length of the forwarded stacktrace so that it won't exceed the 8KB UDP limit of logstash
        ShortenedThrowableConverter throwableConverter = new ShortenedThrowableConverter();
        throwableConverter.setMaxLength(7500);
        throwableConverter.setRootCauseFirst(true);
        logstashSocketAppender.setThrowableConverter(throwableConverter);

        logstashSocketAppender.start();

        // wrap the appender in an Async appender for performance
        AsyncAppender asyncLogstashAppender = new AsyncAppender();
        asyncLogstashAppender.setContext(context);
        asyncLogstashAppender.setName("ASYNC_LOGSTASH");
        asyncLogstashAppender.setQueueSize(logstashQueueCapacity);
        asyncLogstashAppender.addAppender(logstashSocketAppender);
        asyncLogstashAppender.start();

        context.getLogger("ROOT").addAppender(asyncLogstashAppender);

    }

    /**
     * Logback app is achieved by app file i.e gonogo.{env}.properties and API.
     * When app file change is detected, the app is reset.
     * This listener ensures that the programmatic app is also re-applied after reset.
     */
    class LogbackLoggerContextListener extends ContextAwareBase implements LoggerContextListener {

        @Override
        public boolean isResetResistant() {
            return true;
        }

        @Override
        public void onStart(LoggerContext loggerContext) {
            addLogstashAppender(loggerContext);
        }

        @Override
        public void onReset(LoggerContext loggerContext) {

        }

        @Override
        public void onStop(LoggerContext loggerContext) {

        }

        @Override
        public void onLevelChange(ch.qos.logback.classic.Logger logger, Level level) {

        }
    }


}
