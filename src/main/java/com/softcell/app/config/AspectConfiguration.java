package com.softcell.app.config;

import com.softcell.aop.ApplicationTrackingAspect;
import com.softcell.aop.LoggingAspect;
import com.softcell.aop.OptimisticConcurrencyControlAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by prateek on 21/2/17.
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = {"com.softcell.aop"})
public class AspectConfiguration {

    @Bean
    public LoggingAspect loggingAspect(){
        return new LoggingAspect();
    }

    @Bean
    public ApplicationTrackingAspect applicationTrackingAspect(){
        return new ApplicationTrackingAspect();
    }

    @Bean
    public OptimisticConcurrencyControlAspect optimisticConcurrencyControlAspect(){
        return new OptimisticConcurrencyControlAspect();
    }


}
