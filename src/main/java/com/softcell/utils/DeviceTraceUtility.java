package com.softcell.utils;

import com.softcell.gonogo.model.core.BrowserDetails;
import com.softcell.gonogo.model.core.DeviceInfo;
import com.softcell.gonogo.model.core.OSDetails;
import com.softcell.gonogo.model.request.core.Header;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yogeshb
 */
@Component
public class DeviceTraceUtility {

    private static final Logger logger = LoggerFactory.getLogger(DeviceTraceUtility.class);

    /**
     * @param header
     * @param httpRequest
     * @return
     */
    public void populateDeviceInfo(Header header, HttpServletRequest httpRequest) {

        logger.debug("populateDeviceInfo util started");

        String ipAddress = httpRequest.getRemoteAddr();

        String remoteIPAddress = httpRequest.getHeader("X-Forwarded-For");

        String userAgentString = httpRequest.getHeader("User-Agent");

        UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);

        String browserName = null;
        String browserVer = null;
        String osName = null;

        if (null != userAgent.getBrowser()) {

            browserName = userAgent.getBrowser().getName();

        }

        if (null != userAgent.getBrowserVersion()) {

            browserVer = userAgent.getBrowserVersion().getVersion();

        }

        OperatingSystem os = userAgent.getOperatingSystem();

        if (null != os) {

            osName = os.getName();
        }

        if (null != header.getDeviceInfo()) {

            header.getDeviceInfo().setIpAddress((!StringUtils.isEmpty(remoteIPAddress) ? remoteIPAddress : ipAddress));

            /**
             * update browser details
             */
            BrowserDetails browserDetails = new BrowserDetails();
            browserDetails.setBrowserName(browserName);
            browserDetails.setVersion(browserVer);
            header.getDeviceInfo().setBrowserDetails(browserDetails);

            /**
             * update os details
             */
            if (null != header.getDeviceInfo().getOsDetails()) {

                if (StringUtils.isBlank(header.getDeviceInfo().getOsDetails().getOsName())) {

                    header.getDeviceInfo().getOsDetails().setOsName(osName);

                }

            } else {
                OSDetails osDetails = new OSDetails();
                osDetails.setOsName(osName);
                header.getDeviceInfo().setOsDetails(osDetails);
            }

        } else {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.setIpAddress(ipAddress);
            header.setDeviceInfo(deviceInfo);
            /**
             * update browser details
             */
            BrowserDetails browserDetails = new BrowserDetails();
            browserDetails.setBrowserName(browserName);
            browserDetails.setVersion(browserVer);
            header.getDeviceInfo().setBrowserDetails(browserDetails);

            /**
             * update os details
             */
            OSDetails osDetails = new OSDetails();
            osDetails.setOsName(osName);
            header.getDeviceInfo().setOsDetails(osDetails);
        }

        logger.debug("populateDeviceInfo util completed");

    }

}
