package com.softcell.utils;

import com.itextpdf.text.pdf.PdfReader;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.dms.PushDMSDocumentsResponse;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.nextgen.constants.UrlType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by mahesh on 24/6/17.
 */
public class DMSUtils {

    private static final Logger logger = LoggerFactory.getLogger(DMSUtils.class);


    private static Map<String, String> documentExtensionMap = new HashMap() {
        {

            put("application/pdf", "pdf");
            put("jpg", "jpg");
            put("JPG", "jpg");
        }
    };



    public static String getDocumentNameWithExtension(String gonogoDocExtension, String documentName) {


        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(documentName);
        stringBuilder.append(".");
        stringBuilder.append(documentExtensionMap.get(gonogoDocExtension));

        return stringBuilder.toString();

    }

    public static String getDocumentExtension(String gonogoDocExtension) {

        return documentExtensionMap.get(gonogoDocExtension);
    }

    public static BaseResponse getDMSNoDocumentFoundResponse() {

        PushDMSDocumentsResponse pushDMSDocumentsResponse = new PushDMSDocumentsResponse();

        pushDMSDocumentsResponse.setResponseMsg("Documents not found for this application to push DMS");
        pushDMSDocumentsResponse.setStatus(Status.ERROR.name());

        return GngUtils.getBaseResponse(HttpStatus.OK, pushDMSDocumentsResponse);
    }

    public static BaseResponse getDMSPushDocumentsResponse(DMSPushDocumentInformation dmsPushDocumentInformation) {

        PushDMSDocumentsResponse pushDMSDocumentsResponse = new PushDMSDocumentsResponse();

        if(null !=dmsPushDocumentInformation){
            pushDMSDocumentsResponse.setDmsPushDocumentInformation(dmsPushDocumentInformation);
            pushDMSDocumentsResponse.setResponseMsg("Documents successfully push to DMS");
            pushDMSDocumentsResponse.setStatus(Status.SUCCESS.name());
        }else{
            pushDMSDocumentsResponse.setResponseMsg("Problem occur during dms data push");
            pushDMSDocumentsResponse.setStatus(Status.ERROR.name());
        }

        return GngUtils.getBaseResponse(HttpStatus.OK, pushDMSDocumentsResponse);

    }

    public static ImagesDetails getCibilReportDetails() {

        ImagesDetails imagesDetails = new ImagesDetails();

        imagesDetails.setImageId(GNGWorkflowConstant.CIBIL.toFaceValue());
        imagesDetails.setImageName(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue());
        imagesDetails.setImageType("application/pdf");

        return imagesDetails;
    }

    public static List<ImagesDetails> addCibilReportObjectId(List<ImagesDetails> documentsForDmsList) {
        if (!CollectionUtils.isEmpty(documentsForDmsList)) {
            documentsForDmsList.add(DMSUtils.getCibilReportDetails());
        } else {
            documentsForDmsList = new ArrayList<>();
            documentsForDmsList.add(DMSUtils.getCibilReportDetails());
        }

        return documentsForDmsList;
    }

    public static Set<String> getFolderNameSet(String parentFolderName) {

        Set<String> folderNameSet = new LinkedHashSet<>();

        folderNameSet.add(parentFolderName);
        folderNameSet.add(GNGWorkflowConstant.SOURCING.toFaceValue());
        folderNameSet.add(GNGWorkflowConstant.UNDERWRITING.toFaceValue());
        folderNameSet.add(GNGWorkflowConstant.DISBURSAL.toFaceValue());

        return folderNameSet;

    }


    public static String getFolderNameAccordingToImageType(String imageType) {

        if (Cache.DISBURSAL_IMAGES_SET.contains(imageType) || StringUtils.startsWith(imageType, "DO_")) {
            return GNGWorkflowConstant.DISBURSAL.toFaceValue();

        } else if (Cache.UNDERWRITER_IMAGES_SET.contains(imageType)) {
            return GNGWorkflowConstant.UNDERWRITING.toFaceValue();
        } else {
            return GNGWorkflowConstant.SOURCING.toFaceValue();
        }
    }

    public static List<String> getUrlTypeForDMS() {

        List<String> urlTypeList = new ArrayList<>();

        urlTypeList.add(UrlType.DMS_ADD_FOLDER.toValue());
        urlTypeList.add(UrlType.DMS_CONNECT_DIS.toValue());
        urlTypeList.add(UrlType.DMS_POST_DOCUMENT.toValue());
        urlTypeList.add(UrlType.DMS_SEARCH_FOLDER.toValue());

        return urlTypeList;

    }

    public static List<String> getRepeatedAllowedDocument() {
        List<String> classifiedDocList = new ArrayList<>();
        classifiedDocList.add(GNGWorkflowConstant.BANK_STATEMENT.toFaceValue());
        classifiedDocList.add(GNGWorkflowConstant.DEBIT_CARD.toFaceValue());
        return classifiedDocList;
    }

    public static int getDocumentSize(String base64Code) {

        int length = 0;

        if (StringUtils.isNotBlank(base64Code)) {
            length = base64Code.length();
            return (int) Math.ceil((length * 6) / 8 - 2);
        }
        return length;

    }

    public static long getFolderIdAgainstFolderName(String folderName) {

        Map<String, Long> folderInfoMap = new HashMap<>();

        folderInfoMap.put(Status.BUSINESS.name(), (long) 123);

        return folderInfoMap.get(folderName);


    }

    public static InvoiceDetailsRequest buildAgreementApplicationFormRequest(String refId, String institutionId, Product product) {

        InvoiceDetailsRequest invoiceDetailsRequest = new InvoiceDetailsRequest();

        Header header = new Header();

        header.setInstitutionId(institutionId);
        header.setProduct(product);
        invoiceDetailsRequest.setHeader(header);
        invoiceDetailsRequest.setRefID(refId);

        return invoiceDetailsRequest;
    }

    /**
     * This method is used to convert  base64Code to no of pages in the pdf.
     *
     * @param fileType
     * @param base64Code
     * @param fileName
     * @return
     */
    public static int getNoOfPagesInDocument(String fileType, String base64Code, String fileName) {

        int numberOfPages = 1;
        try {

            if (StringUtils.equals(fileType, "application/pdf")) {

                byte[] bytes = Base64.decodeBase64(base64Code);

                InputStream inputStream = new ByteArrayInputStream(bytes);
                PdfReader pdfReader = new PdfReader(inputStream);

                numberOfPages = pdfReader.getNumberOfPages();
            }

        } catch (Exception e) {
            logger.error("problem occur at the time calculating no of pages in the pdf document {}", fileName);

        }

        return numberOfPages;
    }


    public static String getKycDocument(GoNoGoCroApplicationResponse goNoGoCustomerApplication, String typeOfKycDocument) {


        if (null != goNoGoCustomerApplication.getApplicationRequest() && null != goNoGoCustomerApplication.getApplicationRequest().getRequest()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()) {

            List<Kyc> kycs = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc();
            for (Kyc kyc : kycs) {
                if (StringUtils.equalsIgnoreCase(typeOfKycDocument,
                        kyc.getKycName())) {
                    return kyc.getKycNumber();
                }
            }

        }
        return null;
    }

    public static long getBusinessFolderId(List<DmsFolderConfiguration> dmsFolderConfigurations, String folderName) {

        for (DmsFolderConfiguration dmsFolderConfiguration : dmsFolderConfigurations) {
            if (StringUtils.equals(dmsFolderConfiguration.getIndexName(), folderName)) {
                return dmsFolderConfiguration.getIndexId();
            }
        }
        return 0;
    }

    public static BaseResponse getDMSErrorResponse(String errorMassage ) {

        PushDMSDocumentsResponse pushDMSDocumentsResponse = new PushDMSDocumentsResponse();

        pushDMSDocumentsResponse.setStatus(Status.ERROR.name());
        pushDMSDocumentsResponse.setResponseMsg(errorMassage);

        return GngUtils.getBaseResponse(HttpStatus.OK, pushDMSDocumentsResponse);
    }

    public static String getProductNameForDms(String productName) {

        if (StringUtils.equals(productName, Product.CDL.name())) {
            return "CD";//CONSUMER_DURABLES_LOAN
        } else if (StringUtils.equals(productName, Product.CEL.name())) {
            return "CE"; //CONSTRUCTION_EQUIPMENT_LOAN
        } else {
            return productName;
        }

    }

}
