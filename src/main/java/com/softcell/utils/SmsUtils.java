package com.softcell.utils;

import com.softcell.gonogo.model.response.SmsResponse;
import com.softcell.gonogo.model.response.smsservice.Error;
import com.softcell.gonogo.model.response.smsservice.SmsStatus;

/**
 * @author vinodk
 */
public class SmsUtils {

    /**
     * @param errorMsg
     * @return
     */
    public static SmsResponse getSmsErrorResponse(String errorMsg,
                                                  SmsStatus[] smsStatus) {
        SmsResponse smsResponse = new SmsResponse();
        Error error = new Error();
        error.setErrorMsg(errorMsg);
        smsResponse.setSuccess(false);
        smsResponse.setSmsStatus(smsStatus);
        smsResponse.setErrors(new Error[]{error});
        return smsResponse;
    }

    /**
     * @param otpMsg
     * @param smsStatus
     * @return
     */
    public static SmsResponse getSmsSuccessResponse(String otpMsg,
                                                    SmsStatus[] smsStatus) {
        SmsResponse smsResponse = new SmsResponse();
        smsResponse.setSuccess(true);
        smsResponse.setErrors(null);
        smsResponse.setOtp(otpMsg);
        smsResponse.setSmsStatus(smsStatus);
        return smsResponse;
    }
}
