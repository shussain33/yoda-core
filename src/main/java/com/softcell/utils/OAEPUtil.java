package com.softcell.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.OAEPParameterSpec;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.interfaces.RSAKey;
import java.security.spec.AlgorithmParameterSpec;

public final class OAEPUtil  {

   
    private ORSAPadding padding; 
    // private String oaepHashAlgorithm = "SHA-256";

    public OAEPUtil() {
      }
      // initialize this cipher
    public void init(int n,AlgorithmParameterSpec params)
            throws InvalidKeyException, InvalidAlgorithmParameterException {
            /*OAEPParameterSpec myParams;
            if (params != null) {
                if (!(params instanceof OAEPParameterSpec)) {
                    throw new InvalidAlgorithmParameterException
                        ("Wrong Parameters for OAEP Padding");
                }
                myParams = (OAEPParameterSpec) params;
            } else {
                myParams = new OAEPParameterSpec("SHA-256", "MGF1",
                    MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
            }*/
            padding = ORSAPadding.getInstance(ORSAPadding.PAD_OAEP_MGF1,n,null,(OAEPParameterSpec)params);
           //      buffer = new byte[n];
            
        
    }
    /*public void init(int n) throws InvalidKeyException, InvalidAlgorithmParameterException{
        OAEPParameterSpec myParams;
             myParams = new OAEPParameterSpec("SHA-256", "MGF1",
                MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
         padding = RSAPadding.getInstance(RSAPadding.PAD_OAEP_MGF1,n,null,myParams);
    
    }*/

    

    
    // internal doFinal() method. Here we perform the actual RSA operation
    public byte[] unpad(byte[] data) throws BadPaddingException,
            IllegalBlockSizeException {
    	System.out.println(data.length);
         return padding.unpad(data);
    	//return padding.unpad(Hex.hex2byte(Hex.hexString(data)));
          
    }
   /* public byte[] unpad2(byte[] data) throws BadPaddingException,
    IllegalBlockSizeException {
 //return padding.unpad(Hex.hex2byte("00"+Hex.hexString(data)));
return padding.unpad(Hex.hex2byte("00"+Hex.hexString(data)));
  
}*/

    public static int getByteLength(BigInteger b) {
    	              int n = b.bitLength();
    	              return (n + 7) >> 3;
    	          }
    	      
    	          /**
    	           * Return the number of bytes required to store the modulus of this
    	           * RSA key.
    	           */
    	          public static int getByteLength(RSAKey key) {
    	              return getByteLength(key.getModulus());
    	          }
}
