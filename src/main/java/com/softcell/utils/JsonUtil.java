package com.softcell.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

/**
 * Created by prateek on 17/2/17.
 */
public class JsonUtil {


    private static ObjectMapper mapper = new ObjectMapper();

    public static byte[] ObjectToJsonBytes(Object object) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static <T> T StringToObject(String jsonString, Class<?> type) throws IOException {
        return (T)mapper.readValue(jsonString, type);

    }

    public static String ObjectToString(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public static <T> T MapToObject(Map<String, Object> map, Class<?> type) throws IOException {
        // convert map to JSON string
        String json = mapper.writeValueAsString(map);
        return StringToObject(json, type);
    }

    public static <T> T MapToObjectList(Map<String, Object> map, Class<?> type) throws IOException {
        // convert map to JSON string
        String json = mapper.writeValueAsString(map);

        return StringToObject(json, type);



    }

}
