/**
 * yogeshb9:55:09 pm  Copyright Softcell Technolgy
 **/
package com.softcell.utils;

import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.logger.EmailRequestRegistry;
import com.softcell.gonogo.model.request.GetFileRequest;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;


/**
 * @author yogeshb
 */
public class EmailRequestDeduped {

    public static boolean isInprocess(GetFileRequest getFileRequest) {
        EmailRequestRegistry emailRequestRegistry = new EmailRequestRegistry();
        StringBuffer deDupeKey = new StringBuffer();
        if (StringUtils.isNotBlank(getFileRequest.getImageFileID())) {
            deDupeKey.append(getFileRequest.getImageFileID());
        }
        if (StringUtils.isNotBlank(getFileRequest.getRefID())) {
            deDupeKey.append(getFileRequest.getRefID());
        }
        emailRequestRegistry.setDedupeKey(deDupeKey.toString());
        if (Cache.EMAIL_REGISTRY.containsKey(deDupeKey.toString())) {
            EmailRequestRegistry oldEmailRequestRegistry = Cache.EMAIL_REGISTRY.get(deDupeKey.toString());
            if (60 < GngDateUtil.getDateSecond(oldEmailRequestRegistry.getRequestTime(), new DateTime())) {
                Cache.EMAIL_REGISTRY
                        .put(deDupeKey.toString(), emailRequestRegistry);
                return false;
            }
            return true;
        } else {
            Cache.EMAIL_REGISTRY
                    .put(deDupeKey.toString(), emailRequestRegistry);
        }
        return false;
    }

    /**
     * @param getFileRequest
     * @return
     */

    public static boolean removeEmailRegistry(GetFileRequest getFileRequest) {
        StringBuffer deDupeKey = new StringBuffer();
        if (StringUtils.isNotBlank(getFileRequest.getImageFileID())) {
            deDupeKey.append(getFileRequest.getImageFileID());
        }
        if (StringUtils.isNotBlank(getFileRequest.getRefID())) {
            deDupeKey.append(getFileRequest.getRefID());
        }
        Cache.EMAIL_REGISTRY
                .remove(deDupeKey.toString());
        return false;
    }
}
