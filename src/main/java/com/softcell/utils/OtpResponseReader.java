package com.softcell.utils;

import com.softcell.constants.UIDAIERROR_METADATA;
import com.softcell.gonogo.ModuleManager;
import com.softcell.gonogo.model.core.kyc.response.Errors;
import com.softcell.gonogo.model.core.kyc.response.aadhar.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class OtpResponseReader {
	private static Logger logger = LoggerFactory.getLogger(OtpResponseReader.class);

	public XMLOtpResponse parseOtpResponse(String response) {
		return parseOtpResponse_(response);
	}

	private XMLOtpResponse parseOtpResponse_(String response) {
			        
					JAXBContext jaxbContext;
					javax.xml.bind.Unmarshaller jaxbUnMarshaller;
		XMLOtpResponse responseDomain = null;
			        StringReader stringReader = new StringReader(response);
			
			try {
				jaxbContext = JAXBContext.newInstance(XMLOtpResponse.class);
				jaxbUnMarshaller = jaxbContext.createUnmarshaller();
				responseDomain = (XMLOtpResponse) jaxbUnMarshaller.unmarshal(stringReader);
				return responseDomain;
			} catch (JAXBException e) {
				logger.error("{}",e.getStackTrace());
				return null;
			}
			catch (Exception e) {
				logger.error("{}",e.getStackTrace());
				return null;
			}
			
		}

	public AsaServiceErrorResponse parseAsaErrorResponse_(String response) {

		JAXBContext jaxbContext;
		javax.xml.bind.Unmarshaller jaxbUnMarshaller;
		AsaServiceErrorResponse responseDomain = null;
		StringReader stringReader = new StringReader(response);

		try {
			jaxbContext = JAXBContext.newInstance(AsaServiceErrorResponse.class);
			jaxbUnMarshaller = jaxbContext.createUnmarshaller();
			responseDomain = (AsaServiceErrorResponse) jaxbUnMarshaller.unmarshal(stringReader);
			return responseDomain;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			logger.error("{}",e.getStackTrace());
		}
		return responseDomain;
	}

	public KycResponse getOtpResponseDetailsObject(XMLOtpResponse otpResponse){
		KycResponse otpResponseDetailsMap = new KycResponse();
		OtpResponseDetails tempJsonMap = new OtpResponseDetails();
		try{
			if(otpResponse != null){
				if(StringUtils.endsWithIgnoreCase(otpResponse.getRet(), "y")){
					tempJsonMap.setUidaiStatus("SUCCESS");
					tempJsonMap.setOtpResponse(getOtpResponse(otpResponse));

				}else if(StringUtils.endsWithIgnoreCase(otpResponse.getRet(), "n")){
					tempJsonMap.setUidaiStatus("ERROR");
					List<Errors> errors = new ArrayList<Errors>();
					tempJsonMap.setErrors(getError(errors , otpResponse.getErr(), UIDAIERROR_METADATA.otpErrorMap.get(otpResponse.getErr())));
					tempJsonMap.setOtpResponse(getOtpResponse(otpResponse));
				}
			}
			otpResponseDetailsMap.setOtpResponseDetails(tempJsonMap);

		} catch (Exception e) {
			logger.error("{}",e.getStackTrace());
		}
		return otpResponseDetailsMap;

	}

	public OtpResponse getOtpResponse(XMLOtpResponse otpResponse){
		OtpResponse innerTempMap = new OtpResponse();


		innerTempMap.setResult(otpResponse.getRet());
		innerTempMap.setCode(otpResponse.getCode());
		innerTempMap.setTransactionIdentifier( otpResponse.getTxn());
		innerTempMap.setTs( otpResponse.getTs());
		if(StringUtils.endsWithIgnoreCase(otpResponse.getRet(), "n"))
			innerTempMap.setError(otpResponse.getErr());
		//innerTempMap.put("signature", aadharResponse.getSignature());

		return innerTempMap;

	}

	public List<Errors> getError(List<Errors> errors, String code, String desc) {
		try
		{
			Errors errorDomain = new Errors();
			errorDomain.setCode(code);
			errorDomain.setDescription(desc);
			errors.add(errorDomain);
		}catch(Exception e){
			logger.error("{}",e.getStackTrace());
		}
		return errors;
	}
	
	
	
	

}
