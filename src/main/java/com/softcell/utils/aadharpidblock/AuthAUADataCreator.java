/*******************************************************************************
 * DISCLAIMER: The sample code or utility or tool described herein
 *    is provided on an "as is" basis, without warranty of any kind.
 *    UIDAI does not warrant or guarantee the individual success
 *    developers may have in implementing the sample code on their
 *    environment.
 *
 *    UIDAI does not warrant, guarantee or make any representations
 *    of any kind with respect to the sample code and does not make
 *    any representations or warranties regarding the use, results
 *    of use, accuracy, timeliness or completeness of any data or
 *    information relating to the sample code. UIDAI disclaims all
 *    warranties, express or implied, and in particular, disclaims
 *    all warranties of merchantability, fitness for a particular
 *    purpose, and warranties related to the code, or any service
 *    or software related thereto.
 *
 *    UIDAI is not responsible for and shall not be liable directly
 *    or indirectly for any direct, indirect damages or costs of any
 *    type arising out of use or any action taken by you or others
 *    related to the sample code.
 *
 *    THIS IS NOT A SUPPORTED SOFTWARE.
 ******************************************************************************/
package com.softcell.utils.aadharpidblock;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.*;


/**
 * This class provides a method to collate all the data that needs to sent from Auth Client to AUA server.
 *
 * @author UIDAI
 */
public class AuthAUADataCreator {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthAUADataCreator.class);

    private static final int AES_256_KEY_SIZE = 32;
    private static final String RANDOM_ALGORITH_NAME = "SHA1PRNG";
    private Encrypter encrypter;
    private HashGenerator hashGenerator;
    private Map<String, SynchronizedKey> skeyMap = new HashMap<String, SynchronizedKey>();
    private SecureRandom secureSeedGenerator;
    private boolean useSSK = false;


    /**
     * Constructor
     *
     * @param encrypter                For encryption of Pid
     * @param useSynchronizedSesionKey Flag indicating whether synchronized sesssion key should be used.
     */
    public AuthAUADataCreator(Encrypter encrypter, boolean useSynchronizedSesionKey) {
        this.hashGenerator = new HashGenerator();
        this.encrypter = encrypter;
        this.useSSK = useSynchronizedSesionKey;

        try {
            this.secureSeedGenerator = SecureRandom.getInstance(RANDOM_ALGORITH_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Creates an instance of {@link } that represents all the data that an Auth client
     * will send to AUA server.
     *
     * @param pid
     * @return
     */
    public HelperClassAdhar prepareAUAData(Object pid) {
        try {

            byte[] pidXmlBytes = pid.toString().getBytes();

            byte[] sessionKey = null;

            byte[] newRandom = new byte[20];

            SynchronizedKey synchronizedKey = null;

            byte[] encryptedSessionKey = null;

            SessionKeyDetails sessionKeyDetails = null;

            if (useSSK) {
                synchronizedKey = this.skeyMap.get("x");

                LOGGER.info("synchronizedKey :{}", synchronizedKey);

                if (synchronizedKey == null) {//|| synchronizedKey.getSeedCreationDate().getTime()  - System.currentTimeMillis() > expiryTime) {
                    synchronizedKey = new SynchronizedKey(this.encrypter.generateSessionKey(), UUID.randomUUID().toString(), new Date());
                    this.skeyMap.put("x", synchronizedKey);

                    sessionKey = synchronizedKey.getSeedSkey();
                    encryptedSessionKey = this.encrypter.encryptUsingPublicKey(sessionKey);
                    sessionKeyDetails = SessionKeyDetails.createSkeyToInitializeSynchronizedKey(synchronizedKey.getKeyIdentifier(), encryptedSessionKey);
                } else {
                    byte[] seed = secureSeedGenerator.generateSeed(20);

                    SecureRandom random = SecureRandom.getInstance(RANDOM_ALGORITH_NAME);
                    random.setSeed(seed);
                    random.nextBytes(newRandom);

                    sessionKey = Arrays.copyOf(this.encrypter.encryptUsingSessionKey(synchronizedKey.getSeedSkey(), newRandom), AES_256_KEY_SIZE);
                    encryptedSessionKey = newRandom;

                    sessionKeyDetails = SessionKeyDetails.createSkeyToUsePreviouslyGeneratedSynchronizedKey(synchronizedKey.getKeyIdentifier(), encryptedSessionKey);
                }
            } else {
                sessionKey = this.encrypter.generateSessionKey();
                encryptedSessionKey = this.encrypter.encryptUsingPublicKey(sessionKey);
                sessionKeyDetails = SessionKeyDetails.createNormalSkey(encryptedSessionKey);

            }

            byte[] encXMLPIDData = this.encrypter.encryptUsingSessionKey(sessionKey, pidXmlBytes);
            byte[] hmac = this.hashGenerator.generateSha256Hash(pidXmlBytes);
            byte[] encryptedHmacBytes = this.encrypter.encryptUsingSessionKey(sessionKey, hmac);

            String certificateIdentifier = this.encrypter.getCertificateIdentifier();

            return new HelperClassAdhar(encXMLPIDData, encryptedHmacBytes, certificateIdentifier, sessionKeyDetails);

        } catch (Exception e) {
            LOGGER.error("Error occurred while encryption of pid block with probable cause ", e.getMessage());
            throw new RuntimeException(e);
        }

    }

    public static class SynchronizedKey {
        byte[] seedSkey;
        String keyIdentifier;
        Date seedCreationDate;

        public SynchronizedKey(byte[] seedSkey, String keyIdentifier, Date seedCreationDate) {
            super();
            this.seedSkey = seedSkey;
            this.keyIdentifier = keyIdentifier;
            this.seedCreationDate = seedCreationDate;
        }

        public String getKeyIdentifier() {
            return keyIdentifier;
        }

        public Date getSeedCreationDate() {
            return seedCreationDate;
        }

        public byte[] getSeedSkey() {
            return seedSkey;
        }
    }

}
