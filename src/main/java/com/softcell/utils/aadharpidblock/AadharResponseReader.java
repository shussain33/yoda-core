package com.softcell.utils.aadharpidblock;

import com.softcell.constants.UIDAIERROR_METADATA;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.kyc.response.Errors;
import com.softcell.gonogo.model.core.kyc.response.aadhar.*;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class AadharResponseReader  {


	/**
	 * @author Deepak Bhargava
	 * @throws
	 *
	 *
	 */
	public static void main(String[] args) {
		try {
			String content = FileUtils.readFileToString(new File("D:\\EKYAC\\uiadaiKEYCsource code\\uidai-kyc-client-1.0-src\\uidai-auth-client\\src\\main\\resources\\resp.xml"));
			AadharResponseReader resp = new AadharResponseReader();
			//AuthResponse aadharResponseReader = resp.aadharResponseReader(content);
			KycResp kycResponseReader = resp.kycResponseReader(content);
			System.out.println("response   "+kycResponseReader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public AuthResponse aadharResponseReader(String response) throws SystemException, ParseException {
		 return(aadharResponseReader_(response));
	}

	private AuthResponse aadharResponseReader_(String response) throws SystemException,ParseException {
        
		System.out.println("In AADHAR ResponseReader!!!");
		
		AuthResponse adharResponse = new AuthResponse();
		
		byte[] byteArray = response.getBytes();
		try{
			
			
			VTDGen vgen = new VTDGen();
			if(byteArray.length > 0){
				vgen.setDoc(byteArray);
			} else{
				System.out.println("XML DOCUMENT IS BLANK PLEASE PROVIDE VALID XML FILE..");
				throw new SystemException("XML DOCUMENT IS BLANK PLEASE PROVIDE VALID XML FILE ");
			}
			
	    	vgen.parse(true);
		    VTDNav nav = vgen.getNav();
		    if(nav.matchElement("AuthRes"))
		    {
		    	adharResponse.setRet(nav.toString(nav.getAttrVal("ret")));
		    	adharResponse.setCode(nav.toString(nav.getAttrVal("code")));
		    	adharResponse.setTxn(nav.toString(nav.getAttrVal("txn")));
		    	adharResponse.setErr(nav.toString(nav.getAttrVal("err")));
		    	adharResponse.setInfo(nav.toString(nav.getAttrVal("info")));
		    	adharResponse.setTs(nav.toString(nav.getAttrVal("ts")));
		    	
		    	if(nav.toElement(VTDNav.FC, "Signature"))
		    	{
		    		Signature sign = new Signature();
		    	//	sign.setXmlns(nav.toString(nav.getAttrVal("xmlns")));
		    		if(nav.toElement(VTDNav.FC, "SignedInfo"))
		    		{
		    			SignedInfo sinfo = new SignedInfo();
		    			if(nav.toElement(VTDNav.FC, "CanonicalizationMethod"))
		    			{
		    				CanonicalizationMethod canMethod = new CanonicalizationMethod();
		    				canMethod.setAlgo(nav.toString(nav.getAttrVal("Algorithm")));
		    				do{
		    					if(nav.matchElement("SignatureMethod"))
		    					{
		    						SignatureMethod signMethod = new SignatureMethod();
		    						signMethod.setAlgo(nav.toString(nav.getAttrVal("Algorithm")));
		    						sinfo.setSignMethod(signMethod);
		    					}
		    					if(nav.matchElement("Reference"))
		    					{
		    						Reference ref = new Reference();
		    						ref.setUri(nav.toString(nav.getAttrVal("URI")));
		    						if(nav.toElement(VTDNav.FC, "Transforms"))
		    						{
		    							Transforms tfs = new Transforms();
		    							if(nav.toElement(VTDNav.FC, "Transform"))
		    							{
		    								Transform tf = new Transform();
		    								tf.setAlgo(nav.toString(nav.getAttrVal("Algorithm")));
		    								tfs.setTransForm(tf);
		    								nav.toElement(VTDNav.P);
		    							}
		    							
		    							do{
		    								if(nav.matchElement("DigestMethod"))
		    								{
		    									DigestMethod dMethod = new DigestMethod();
		    									dMethod.setAlgo(nav.toString(nav.getAttrVal("Algorithm")));
		    									ref.setDigestedMethod(dMethod);
		    								}
		    								if(nav.matchElement("DigestValue") && checkElementText(nav))
		    								{
		    									ref.setDigestedvalue(StringUtils.trim(nav.toString(nav.getText())));
		    								}
		    								
		    							}while(nav.toElement(nav.NS));
		    							nav.toElement(nav.P);
		    						}
		    						sinfo.setReference(ref);
		    					}
		    					
		    				}while(nav.toElement(nav.NS));
		    				sinfo.setCanMethod(canMethod);
		    			}
		    			sign.setSignedInfo(sinfo);
		    			nav.toElement(nav.P);
		    		}
		    		
		    		if(nav.toElement(VTDNav.NS, "SignatureValue"))
		    		{
		    			sign.setSignValue(nav.toString(nav.getText()));
		    		}
		    		
		    		adharResponse.setSignature(sign);
		    	}
		    }
		
				return adharResponse;
			}catch (ParseException e) {
		    	e.printStackTrace();
		    	throw new SystemException("XML String Not Valid");
			}catch (NavException e) {
				e.printStackTrace();
				throw new SystemException(e.getMessage());
			}catch(Exception e){
				e.printStackTrace();
				throw new SystemException("Unknown Exception Occurs While Parsing PAN Response");
			}
	}
	//using JAXB
	public AuthResponse aadharResponse(String response) throws SystemException, ParseException {
		 return(aadharResponse_(response));
	}
	private AuthResponse aadharResponse_(String response) {
		        
				JAXBContext jaxbContext;
				Unmarshaller jaxbUnMarshaller;
				AuthResponse responseDomain = null;
		        StringReader stringReader = new StringReader(response);
		
		try {
			jaxbContext = JAXBContext.newInstance(AuthResponse.class);
			jaxbUnMarshaller = jaxbContext.createUnmarshaller();
			responseDomain = (AuthResponse) jaxbUnMarshaller.unmarshal(stringReader);
			return responseDomain;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseDomain;
	}
	//parse asa error response
	public AsaServiceErrorResponse parseAsaErrorResponse(String response) throws SystemException, ParseException {
		 return(parseAsaErrorResponse_(response));
	}
	private AsaServiceErrorResponse parseAsaErrorResponse_(String response) {
		        
				JAXBContext jaxbContext;
				Unmarshaller jaxbUnMarshaller;
				AsaServiceErrorResponse responseDomain = null;
		        StringReader stringReader = new StringReader(response);
		
		try {
			jaxbContext = JAXBContext.newInstance(AsaServiceErrorResponse.class);
			jaxbUnMarshaller = jaxbContext.createUnmarshaller();
			responseDomain = (AsaServiceErrorResponse) jaxbUnMarshaller.unmarshal(stringReader);
			return responseDomain;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseDomain;
	}
	//parse kyc response Resp element
	public  Resp kycRespElementReader(String response) {
		return kycRespElementReader_(response);
	}

	private  Resp kycRespElementReader_(String response) {
			        
					JAXBContext jaxbContext;
					Unmarshaller jaxbUnMarshaller;
					Resp responseDomain = null;
			        StringReader stringReader = new StringReader(response);
			
			try {
				jaxbContext = JAXBContext.newInstance(Resp.class);
				jaxbUnMarshaller = jaxbContext.createUnmarshaller();
				responseDomain = (Resp) jaxbUnMarshaller.unmarshal(stringReader);
				return responseDomain;
			} catch (JAXBException e) {
				e.printStackTrace();
				return null;
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
	}
	public  KycResp kycResponseReader(String response) {
		return kycResponseReader_(response);
	}

	private  KycResp kycResponseReader_(String response) {
			        
					JAXBContext jaxbContext;
					Unmarshaller jaxbUnMarshaller;
					KycResp responseDomain = null;
			        StringReader stringReader = new StringReader(response);
			
			try {
				/*jaxbContext = JAXBContext.newInstance(KycResp.class);
				jaxbUnMarshaller = jaxbContext.createUnmarshaller();
				responseDomain = (KycResp) jaxbUnMarshaller.unmarshal(stringReader);*/
				
				AadharResponseReader obj = new AadharResponseReader();
				responseDomain=(KycResp)obj.parseKycResponse(KycResp.class, response);
				return responseDomain;
			} catch (JAXBException e) {
				e.printStackTrace();
				return null;
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
	}

	private boolean checkElementText(VTDNav vnav) {
		int text = vnav.getText(); 
		if(text!=-1){
			return true;
		}else{
			return false;
		}
	}
	
	public static Object parseKycResponse(Class clazz, String xmlToParse)
			throws JAXBException {
		// Create an XMLReader to use with our filter
		try {
			// Prepare JAXB objects
			JAXBContext jc = JAXBContext.newInstance(clazz);
			Unmarshaller u = jc.createUnmarshaller();

			XMLReader reader;
			reader = XMLReaderFactory.createXMLReader();

			// Create the filter (to add namespace) and set the xmlReader as its
			// parent.
			NamespaceFilter inFilter = new NamespaceFilter(
					"http://www.uidai.gov.in/kyc/uid-kyc-response/1.0", false);
			inFilter.setParent(reader);

			// Prepare the input, in this case a java.io.File (output)
			InputSource is = new InputSource(new StringReader(xmlToParse));

			// Create a SAXSource specifying the filter
			SAXSource source = new SAXSource(inFilter, is);

			// Do unmarshalling
			Object res = u.unmarshal(source, clazz).getValue();
			return res;
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}



	public KycResponse getKycResponseDetailsObject(KycResp resp){
		KycResponse kycResponse=new KycResponse();
		KycResponseDetails kycResponseDetails=new KycResponseDetails();
		try{
			if(StringUtils.equalsIgnoreCase(resp.getRet(), "y")){
				kycResponseDetails.setUidaiStatus("SUCCESS");
				kycResponseDetails.setKycResponse(getKycResponse(resp));

			}else if(StringUtils.equalsIgnoreCase(resp.getRet(), "n")){
				//Map<String, Object> aadharErrorMap = new LinkedHashMap<String, Object>();
				List<Errors> errors = new ArrayList<Errors>();
				kycResponseDetails.setUidaiStatus("ERROR");
				kycResponseDetails.setErrors(getError(errors, resp.getErr(), UIDAIERROR_METADATA.kycErrorMap.get(StringUtils.trim(resp.getErr()))));
				//to add errors that occurs in auth response xml that is present in kyc response
				AuthResponse aadharResponse=null;
				AadharResponseReader parseResponse = new AadharResponseReader();
				if(StringUtils.isNotBlank(resp.getRar())){
					aadharResponse = parseResponse.aadharResponse(new String(Base64.decode(resp.getRar())));
					kycResponseDetails.setErrors(getError(errors, aadharResponse.getErr(), UIDAIERROR_METADATA.aadharError.get(aadharResponse.getErr())));
				}
				//end of auth resonse error addition

				kycResponseDetails.setKycResponse(getKycResponse(resp));
			}
			kycResponse.setKycResponseDetails(kycResponseDetails);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return kycResponse;

	}

	public KycResponseInner getKycResponse(KycResp response){
		KycResponseInner kycResponseInner = new KycResponseInner();
		if(response != null){
			kycResponseInner.setResult(response.getRet());
			kycResponseInner.setCode(response.getCode());
			kycResponseInner.setTransactionIdentifier(response.getTxn());
			kycResponseInner.setTs(response.getTs());
			if(StringUtils.equalsIgnoreCase(response.getRet(), "y")){
				if(response.getUidData() != null){
					kycResponseInner.setPht(response.getUidData().getPht());
					kycResponseInner.setUid(response.getUidData().getUid());
					kycResponseInner.setAuthResponseXml(response.getRar());
					kycResponseInner.setProofOfIdentity(getPoiMap(response));
					kycResponseInner.setAddressSegment(getPoaMap(response));
					if(response.getUidData().getPrn()!=null){
						if(StringUtils.isNotBlank(response.getUidData().getPrn().getLetterValue())){
							kycResponseInner.setPdfDocument(response.getUidData().getPrn().getLetterValue());
						}
					}
				}

			}else if(StringUtils.equalsIgnoreCase(response.getRet(), "n")){
				kycResponseInner.setError(response.getErr());
				kycResponseInner.setAuthResponseXml(response.getRar());
			}

		}

		return kycResponseInner;

	}

	public ProofOfIdentity getPoiMap(KycResp response){
		ProofOfIdentity poiMap = new ProofOfIdentity();
		if(response.getUidData().getPoi() != null){
			poiMap.setName(response.getUidData().getPoi().getName());
			poiMap.setDob(response.getUidData().getPoi().getDob());
			poiMap.setGender(response.getUidData().getPoi().getGender());
			poiMap.setPhone(response.getUidData().getPoi().getPhone());
			poiMap.setEmail(response.getUidData().getPoi().getEmail());
		}
		return poiMap;
	}
	public AddressSegment getPoaMap(KycResp response){
		AddressSegment poaMap = new AddressSegment();
		if(response.getUidData().getPoa() != null){
			poaMap.setCo(response.getUidData().getPoa().getCo());
			poaMap.setHouse(response.getUidData().getPoa().getHouse());
			poaMap.setStreet(response.getUidData().getPoa().getStreet());
			poaMap.setLm(response.getUidData().getPoa().getLm());
			poaMap.setLoc(response.getUidData().getPoa().getLoc());
			poaMap.setVtc(response.getUidData().getPoa().getVtc());
			poaMap.setSubdist(response.getUidData().getPoa().getSubdist());
			poaMap.setDist(response.getUidData().getPoa().getDist());
			poaMap.setState(response.getUidData().getPoa().getState());
			poaMap.setPc(response.getUidData().getPoa().getPc());
			poaMap.setPo(response.getUidData().getPoa().getPo());
			if(StringUtils.isNotBlank(response.getUidData().getPoa().getCountry())){
				poaMap.setCountry(response.getUidData().getPoa().getCountry());
			}
		}
		return poaMap;
	}

	public List<Errors> getError(List<Errors> errors, String code, String desc) {
		try
		{
			Errors errorDomain = new Errors();
			errorDomain.setCode(code);
			errorDomain.setDescription(desc);
			errors.add(errorDomain);
		}catch(Exception e){
			e.printStackTrace();
		}
		return errors;
	}
	


}
