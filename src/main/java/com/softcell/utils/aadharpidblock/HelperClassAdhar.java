package com.softcell.utils.aadharpidblock;

import java.util.Arrays;

public class HelperClassAdhar {

	byte[] encXMLPIDData;
	byte[] encryptedHmacBytes;
	byte[] sKey;
	String certificateIdentifier;
	SessionKeyDetails sessionKeyDetails;

	public HelperClassAdhar(byte[] encXMLPIDData, byte[] encryptedHmacBytes, String certificateIdentifier,
							SessionKeyDetails sessionKeyDetails) {
		super();
		this.encXMLPIDData = encXMLPIDData;
		this.encryptedHmacBytes = encryptedHmacBytes;
		this.certificateIdentifier = certificateIdentifier;
		this.sessionKeyDetails = sessionKeyDetails;
	}

	public HelperClassAdhar(byte[] encXMLPIDData, byte[] encryptedHmacBytes, String certificateIdentifier,
							byte[] sKey) {
		super();
		this.encXMLPIDData = encXMLPIDData;
		this.encryptedHmacBytes = encryptedHmacBytes;
		this.certificateIdentifier = certificateIdentifier;
		this.sKey = sKey;
	}

	public byte[] getEncXMLPIDData() {
		return encXMLPIDData;
	}

	public void setEncXMLPIDData(byte[] encXMLPIDData) {
		this.encXMLPIDData = encXMLPIDData;
	}

	public byte[] getEncryptedHmacBytes() {
		return encryptedHmacBytes;
	}

	public void setEncryptedHmacBytes(byte[] encryptedHmacBytes) {
		this.encryptedHmacBytes = encryptedHmacBytes;
	}

	public String getCertificateIdentifier() {
		return certificateIdentifier;
	}

	public void setCertificateIdentifier(String certificateIdentifier) {
		this.certificateIdentifier = certificateIdentifier;
	}

	public SessionKeyDetails getSessionKeyDetails() {
		return sessionKeyDetails;
	}

	public void setSessionKeyDetails(SessionKeyDetails sessionKeyDetails) {
		this.sessionKeyDetails = sessionKeyDetails;
	}

    public byte[] getsKey() {
        return sKey;
    }

    public void setsKey(byte[] sKey) {
        this.sKey = sKey;
    }

    @Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("HelperClassAdhar{");
		sb.append("encXMLPIDData=").append(Arrays.toString(encXMLPIDData));
		sb.append(", encryptedHmacBytes=").append(Arrays.toString(encryptedHmacBytes));
		sb.append(", certificateIdentifier='").append(certificateIdentifier).append('\'');
		sb.append(", sessionKeyDetails=").append(sessionKeyDetails);
		sb.append(", skey").append(sKey);
		sb.append('}');
		return sb.toString();
	}
}
