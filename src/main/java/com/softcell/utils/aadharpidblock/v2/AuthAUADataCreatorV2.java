package com.softcell.utils.aadharpidblock.v2;

import com.softcell.utils.aadharpidblock.AuthAUADataCreator;
import com.softcell.utils.aadharpidblock.HelperClassAdhar;
import com.softcell.utils.aadharpidblock.SessionKeyDetails;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by bhuvneshk on 24/8/17.
 */
public class AuthAUADataCreatorV2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthAUADataCreatorV2.class);

    private static final String JCE_PROVIDER = "BC";

    // AES Key size - in bits
    public static final int AES_KEY_SIZE_BITS = 256;

    // IV length - last 96 bits of ISO format timestamp
    public static final int IV_SIZE_BITS = 96;

    // Additional authentication data - last 128 bits of ISO format timestamp
    public static final int AAD_SIZE_BITS = 128;

    // Authentication tag length - in bits
    public static final int AUTH_TAG_SIZE_BITS = 128;

    /**
     * Hashing Algorithm Used for encryption and decryption
     */
    private String algorithm = "SHA-256";

    private  String SECURITY_PROVIDER = "BC";

    /**
     * Default Size of the HMAC/Hash Value in bytes
     */
    private int HMAC_SIZE = 32;

    private Date certExpiryDate;

    private static final String ASYMMETRIC_ALGO = "RSA/ECB/PKCS1Padding";

    private static final String CERTIFICATE_TYPE = "X.509";

    public HelperClassAdhar prepareAUADataV2(Object pid) {
        try {

            LOGGER.info("inside prepareAUADataV2");
            byte[] inputData = pid.toString().getBytes();

            byte[] sessionKey = generateSessionKey();
            byte[] encryptedSessionKey = encryptUsingPublicKey(sessionKey);

            AuthAUADataCreator.SynchronizedKey synchronizedKey = new AuthAUADataCreator.SynchronizedKey(sessionKey, UUID.randomUUID().toString(), new Date());

            SessionKeyDetails sessionKeyDetails = SessionKeyDetails.createSkeyToUsePreviouslyGeneratedSynchronizedKey(synchronizedKey.getKeyIdentifier(), encryptedSessionKey);


            LOGGER.info("encryptedSessionKey generated : {}",encryptedSessionKey);

            String ts = getCurrentISOTimeInUTF8();
            byte[] cipherTextWithTS = encrypt(inputData, sessionKey, ts);

            LOGGER.info("ts generated : {}",ts);


            byte[] srcHash = generateHash(inputData);
            byte[] iv = generateIv(ts);
            byte[] aad = generateAad(ts);

            byte[] encryptedHmacBytes = encryptDecryptUsingSessionKey(true , sessionKey, iv, aad, srcHash);

            String certificateIdentifier = getCertificateIdentifier();

            LOGGER.info("certificateIdentifier : {}",certificateIdentifier);

            return new HelperClassAdhar(cipherTextWithTS, encryptedHmacBytes, certificateIdentifier, sessionKeyDetails);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();

            LOGGER.error("Error occurred while encryption of pid block v2 with probable cause ",e.getMessage());

            throw new RuntimeException(e);
        } catch (InvalidCipherTextException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public byte[] encryptUsingPublicKey(byte[] data) throws IOException, GeneralSecurityException {
        // encrypt the session key with the public key
        Cipher pkCipher = Cipher.getInstance(ASYMMETRIC_ALGO, JCE_PROVIDER);
        CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE, JCE_PROVIDER);
        FileInputStream fileInputStream = new FileInputStream(new ClassPathResource("uidai_auth_encryp_certificate.cer").getFile());
        X509Certificate cert = (X509Certificate) certFactory.generateCertificate(fileInputStream);
        PublicKey publicKey = cert.getPublicKey();
        pkCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encSessionKey = pkCipher.doFinal(data);
        certExpiryDate = cert.getNotAfter();
        return encSessionKey;
    }

    public String getCertificateIdentifier() {
        SimpleDateFormat ciDateFormat = new SimpleDateFormat("yyyyMMdd");
        ciDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String certificateIdentifier = ciDateFormat.format(this.certExpiryDate);
        return certificateIdentifier;
    }

    /**
     * Returns the 256 bit hash value of the message
     *
     * @param message
     *            full plain text
     *
     * @return hash value
     *             I/O errors
     */
    public byte[] generateHash(byte[] message) throws Exception {
        byte[] hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm, SECURITY_PROVIDER);
            digest.reset();
            HMAC_SIZE = digest.getDigestLength();
            hash = digest.digest(message);
        } catch (GeneralSecurityException e) {
            throw new Exception(
                    "SHA-256 Hashing algorithm not available");
        }
        return hash;
    }

    /**
     * Encrypts given data using a generated session and used ts as for all other needs.
     * @param inputData - data to encrypt
     * @param sessionKey  - Session key
     * @param ts - timestamp as per the PID
     * @return encrypted data
     * @throws IllegalStateException
     * @throws InvalidCipherTextException
     * @throws Exception
     */
    public byte[] encrypt(byte[] inputData, byte[] sessionKey, String ts) throws IllegalStateException, InvalidCipherTextException, Exception {
        byte[] iv = this.generateIv(ts);
        byte[] aad = this.generateAad(ts);
        byte[] cipherText = this.encryptDecryptUsingSessionKey(true, sessionKey, iv, aad, inputData);
        byte[] tsInBytes = ts.getBytes("UTF-8");
        byte [] packedCipherData = new byte[cipherText.length + tsInBytes.length];
        System.arraycopy(tsInBytes, 0, packedCipherData, 0, tsInBytes.length);
        System.arraycopy(cipherText, 0, packedCipherData, tsInBytes.length, cipherText.length);
        return packedCipherData;
    }

    /**
     * Generate IV using timestamp
     * @param ts - timestamp string
     * @return 12 bytes array
     * @throws UnsupportedEncodingException
     */
    private byte[] generateIv(String ts) throws UnsupportedEncodingException {
        return getLastBits(ts, IV_SIZE_BITS / 8);
    }


    /**
     * Generate AAD using timestamp
     * @param ts - timestamp string
     * @return 16 bytes array
     * @throws UnsupportedEncodingException
     */
    private byte[] generateAad(String ts) throws UnsupportedEncodingException {
        return getLastBits(ts, AAD_SIZE_BITS / 8);
    }

    /**
     * Fetch specified last bits from String
     * @param ts - timestamp string
     * @param bits - no of bits to fetch
     * @return byte array of specified length
     * @throws UnsupportedEncodingException
     */
    private byte[] getLastBits(String ts, int bits) throws UnsupportedEncodingException {
        byte[] tsInBytes = ts.getBytes("UTF-8");
        return Arrays.copyOfRange(tsInBytes, tsInBytes.length - bits, tsInBytes.length);
    }

    /**
     * Encrypts given data using session key, iv, aad
     * @param cipherOperation - true for encrypt, false otherwise
     * @param skey	- Session key
     * @param iv  	- initialization vector or nonce
     * @param aad 	- additional authenticated data
     * @param data 	- data to encrypt
     * @return encrypted data
     * @throws IllegalStateException
     * @throws InvalidCipherTextException
     */
    private byte[] encryptDecryptUsingSessionKey(boolean cipherOperation, byte[] skey, byte[] iv, byte[] aad,
                                                 byte[] data) throws IllegalStateException, InvalidCipherTextException {

        AEADParameters aeadParam = new AEADParameters(new KeyParameter(skey), AUTH_TAG_SIZE_BITS, iv, aad);
        GCMBlockCipher gcmb = new GCMBlockCipher(new AESEngine());

        gcmb.init(cipherOperation, aeadParam);
        int outputSize = gcmb.getOutputSize(data.length);
        byte[] result = new byte[outputSize];
        int processLen = gcmb.processBytes(data, 0, data.length, result, 0);
        gcmb.doFinal(result, processLen);

        return result;
    }

    /**
     * Convert byte array to hex string
     * @param bytes - input bytes
     * @return - hex string
     */
    private static String byteArrayToHexString(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            result.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return result.toString();
    }

    /**
     * Creates a AES key that can be used as session key (skey)
     * @return session key byte array
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    private byte[] generateSessionKey() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyGenerator kgen = KeyGenerator.getInstance("AES", JCE_PROVIDER);
        kgen.init(AES_KEY_SIZE_BITS);
        SecretKey key = kgen.generateKey();
        byte[] symmKey = key.getEncoded();
        return symmKey;
    }

    /**
     * Get current ISO time
     * @return current time in String
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    private String getCurrentISOTimeInUTF8() {
        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss");
        String timeNow = df.format(new Date());
        return timeNow;
    }
}
