/**
 * yogeshb10:36:10 am  Copyright Softcell Technolgy
 **/
package com.softcell.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CharMatcher;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.constants.master.MasterDetails;
import com.softcell.gonogo.Buckets;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.cache.redis.ClusterRedisProperties;
import com.softcell.gonogo.cache.redis.StandaloneRedisProperties;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.request.scoring.CibilRespAddress;
import com.softcell.gonogo.model.kyc.response.ErrorDesc;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.surrogate.Surrogate;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.apache.commons.collections.*;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import com.softcell.gonogo.model.MasterMappingDetails;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author yogeshb
 */
public class GngUtils {

    private static final Logger logger = LoggerFactory.getLogger(GngUtils.class);

    private static final String GNG_CORE_RESOURCES = "GNG_CORE_RESOURCES";

    public static final List<String> verifiedSchemeList = new ArrayList<String>(){{ add("Secured BL BT"); add("Secured BL");  add("Secured BL BT Top-up");}};

    private static final CharSequence SPECIAL_CHAR = "<([{\\^-=$!|]})?*+.>";
    private static final Map<String, String> stateMap = new HashedMap() {{
        put("01", "JAMMU & KASHMIR");
        put("02", "HIMACHAL PRADESH");
        put("03", "PUNJAB");
        put("04", "CHANDIGARH");
        put("05", "UTTARANCHAL");
        put("06", "HARYANA");
        put("07", "DELHI");
        put("08", "RAJASTHAN");
        put("09", "UTTAR PRADESH");
        put("10", "BIHAR");
        put("11", "SIKKIM");
        put("12", "ARUNACHAL PRADESH");
        put("13", "NAGALAND");
        put("14", "MANIPUR");
        put("15", "MIZORAM");
        put("16", "TRIPURA");
        put("17", "MEGHALAYA");
        put("18", "ASSAM");
        put("19", "WEST BENGAL");
        put("20", "JHARKHAND");
        put("21", "ORISSA");
        put("22", "CHHATTISGARH");
        put("23", "MADHYA PRADESH");
        put("24", "GUJARAT");
        put("25", "DAMAN & DIU");
        put("26", "DADRA & NAGAR HAVELI");
        put("27", "MAHARASHTRA");
        put("28", "ANDHRA PRADESH");
        put("29", "KARNATAKA");
        put("30", "GOA");
        put("31", "LAKSHADWEEP");
        put("32", "KERALA");
        put("33", "TAMIL NADU");
        put("34", "PONDICHERRY");
        put("35", "ANDAMAN & NICOBAR ISLANDS");
        put("36", "TELANGANA");
        put("99", "APO ADDRESS");
    }};




    public static String generateOtp() {
        /**
         * generate a 5 digit integer 1000 <10000
         */
        int randomPIN = (int) (Math.random() * 9000) + 10000;
        return "" + randomPIN;
    }

    /**
     * Converting Double to String
     *
     * @paramvalue
     * @return
     */
    public static long generateUniqueId() {
        /**
         * generate a 12 digit id
         */
        long number = 0l;
        Random rand = new Random();
        number = (rand.nextInt(1000000)+1000000000l) * (rand.nextInt(900)+100);
        return number;
    }

    public static String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
    public static String getString(Double value) {
        String string = null;
        if (value != null) {
            string = new BigDecimal(value).toPlainString();
        } else {
            return "0";
        }
        return string;
    }

    /**
     * Converting Integer to String
     *
     * @param value
     * @return
     */
    public static String getString(Integer value) {
        String string = null;
        if (value != null) {
            string = value.toString();
        } else {
            return "0";
        }
        return string;
    }

    /**
     * Converting Long to String
     *
     * @param value
     * @return
     */
    public static String getString(Long value) {
        String string = null;
        if (value != null) {
            string = value.toString();
        } else {
            return "0";
        }
        return string;
    }

    public static String getDecimalValue(float f) {
        BigDecimal bd = new BigDecimal(Float.toString(f));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        return bd.toString();
    }

    public static boolean checkSecurityKey(HttpHeaders httpHeaders) {
        String ticketNumber = httpHeaders.getFirst("token-key");
        if (ticketNumber != null)
            if (ticketNumber.equals("95957453469767522788")) {
                return true;
            }
        return false;
    }


    public static String getOsType() {
        return System.getProperty("os.name");
    }

    /**
     * @return
     */
    public static String getReSourcePath() {

        String enviornmentPath = System.getenv(GNG_CORE_RESOURCES);

        return enviornmentPath;

    }

    public static Set<String> getDealerList(String dealerInclusionId) {
        String[] dealerInclusionArray = StringUtils.splitPreserveAllTokens(dealerInclusionId, FieldSeparator.DOLLER);
        Set<String> dealerIdSet = null;
        if (dealerInclusionArray.length > 0) {
            dealerIdSet = new HashSet<>();
            for (String dealerId : dealerInclusionArray) {
                if (StringUtils.isNotBlank(dealerId)) {
                    dealerIdSet.add(dealerId);
                }
            }
        }
        return dealerIdSet;
    }

    public static Object deepClone(Object object) {

        Object obj = null;
        ObjectInputStream ois = null;
        ByteArrayOutputStream baos;
        ObjectOutputStream oos;
        ByteArrayInputStream bais;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            bais = new ByteArrayInputStream(baos.toByteArray());
            ois = new ObjectInputStream(bais);

            oos.writeObject(object);

            obj = ois.readObject();

        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred while deep cloning the object with probable cause [{}] ", e.getMessage());
        } finally {
            if (null != ois) {
                try {
                    ois.close();
                } catch (IOException e) {

                }
            }
        }

        return obj;

    }

    /**
     * This method will return kyc name based on type of kyc
     *
     * @param kycConstant
     * @param kycList
     * @return
     */
    public static String getKYC(final String kycConstant, final List<Kyc> kycList, final String validationPattern) {

        if (!CollectionUtils.isEmpty(kycList)) {

            Pattern pattern = Pattern.compile(validationPattern);

            for (Kyc kyc : kycList) {

                if (kycConstant.equalsIgnoreCase(kyc.getKycName())) {

                    if (null != validationPattern && null != kyc.getKycNumber()) {

                        if (pattern.matcher(kyc.getKycNumber()).matches()) {

                            return kyc.getKycNumber();

                        } else {

                            return null;

                        }


                    } else {

                        return kyc.getKycNumber();
                    }
                }
            }
        } else {
            logger.warn(" kyc list  is null/blank ");
        }

        return null;
    }

    public static Optional<Phone> getPhone(String phoneConstant, List<Phone> phoneList) {

        if (!CollectionUtils.isEmpty(phoneList)) {

            return phoneList.parallelStream().filter(ph -> phoneConstant.equalsIgnoreCase(ph.getPhoneType())).findFirst();

        }

        return Optional.empty();
    }

    public static String getPhoneNumber(String phoneConstant, List<Phone> phoneList) {

        if (phoneList != null && phoneList.size() > 0) {
            for (Phone phone : phoneList) {
                if (phoneConstant.equalsIgnoreCase(phone.getPhoneType())) {
                    return phone.getPhoneNumber();
                }
            }
        }
        return null;
    }

    public static Email getEmail(String emailConstant, List<Email> emailList) {

        if (emailList != null && emailList.size() > 0) {
            for (Email email : emailList) {
                if (emailConstant.equalsIgnoreCase(email.getEmailType())) {
                    return email;
                }
            }
        }
        return null;
    }

    public static String getEmailId(String emailConstant, List<Email> emailList) {

        if (emailList != null && emailList.size() > 0) {
            for (Email email : emailList) {
                if (emailConstant.equalsIgnoreCase(email.getEmailType())) {
                    return email.getEmailAddress();
                }
            }
        }
        return null;
    }

    public static Kyc getKYCObject(String kycConstant, List<Kyc> kycList) {

        if (kycList != null && kycList.size() > 0) {
            for (Kyc kyc : kycList) {
                if (kycConstant.equalsIgnoreCase(kyc.getKycName())) {
                    return kyc;
                }
            }
        }
        return null;
    }

    public static String getKYCNumber(String kycConstant, List<Kyc> kycList) {

        String kycNumber = null;

        if (kycList != null && kycList.size() > 0) {
            for (Kyc kyc : kycList) {
                if (StringUtils.equalsIgnoreCase(kycConstant, kyc.getKycName())) {
                    kycNumber = kyc.getKycNumber();
                }

            }
        }
        return kycNumber;
    }

    public static CustomerAddress getAddressObject(String addressConst, List<CustomerAddress> addressList) {

        if (addressList != null && addressList.size() > 0) {
            for (CustomerAddress address : addressList) {
                if (addressConst.equalsIgnoreCase(address.getAddressType())) {
                    return address;
                }
            }
        }
        return null;
    }

    /**
     * This returns concatenate name string for Name object
     *
     * @param name
     * @return
     */
    public static String convertNameToFullName(Name name) {

        String result = null;

        if (null != name) {
            result = Arrays.asList(name.getFirstName(), name.getMiddleName(), name.getLastName())
                    .parallelStream()
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.joining(FieldSeparator.SPACE_STR));
        }

        return result;
    }

    public static String stringToFirstMidlleLastName(List<String> fmlName) {
        String result = null;

        if (!CollectionUtils.isEmpty(fmlName)) {

            result = fmlName.parallelStream().filter(StringUtils::isNotBlank).collect(Collectors.joining(FieldSeparator.SPACE_STR));

        }
        return result;
    }

    public static String getGenderInAadhaarRequestFormat(String gender) {

        if (StringUtils.equalsIgnoreCase(ReqResHelperConstants.Male.name(), gender)) {
            return ReqResHelperConstants.M.name();
        } else if (StringUtils.equalsIgnoreCase(ReqResHelperConstants.Female.name(),
                gender)) {
            return ReqResHelperConstants.F.name();
        } else {
            return null;
        }

    }

    public static String getNonNormalizedAddress(final Object obj) {

        StringBuffer result = new StringBuffer();

        if (null != obj && (obj instanceof CibilRespAddress || obj instanceof CustomerAddress)) {

            CibilRespAddress address = (CibilRespAddress) obj;

            if (null != address) {

                String state = stateMap.get(address.getStateCode());

                if (null == state && stateMap.values().contains(address.getStateCode())) {
                    state = address.getStateCode();
                }

                String join = StringUtils.join(new Object[]{
                        address.getAddressLine1(),
                        address.getAddressLine2(),
                        address.getAddressLine3(),
                        address.getAddressLine4(),
                        state
                }, FieldSeparator.SPACE);

                result.append(join);

            }

        }

        return result.toString();

    }

    public static BaseResponse getBaseResponse(HttpStatus httpStatus, Object buzResponse) {
        if (null == buzResponse)
            buzResponse = Collections.emptyMap();

        return BaseResponse.builder()
                .payload(new Payload<>(buzResponse))
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .build();
    }

    public static BaseResponse getBaseResponse(HttpStatus httpStatus, Collection<com.softcell.gonogo.model.response.core.Error> errors) {
        return BaseResponse.builder()
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .errors(errors)
                .build();
    }

    public static BaseResponse getBaseResponse(HttpStatus httpStatus, Object obj, Collection<Error> errors) {
        return BaseResponse.builder()
                .payload(new Payload<>(obj))
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .errors(errors)
                .build();
    }


    public static BaseResponse getBaseResponse(HttpStatus httpStatus) {
        return BaseResponse.builder()
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .build();
    }

    public static Collection<Error> getNoContentErrorList() {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.NO_DATA_FOUND)
                .errorCode(String.valueOf(Error.ERROR_TYPE.DATABASE.toCode()))
                .errorType(Error.ERROR_TYPE.DATABASE.toValue())
                .level(Error.SEVERITY.LOW.name())
                .build());
        return errors;
    }

    public static Collection<Error> getNoContentErrorList(String type) {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.NO_DATA+type)
                .errorCode(String.valueOf(Error.ERROR_TYPE.DATABASE.toCode()))
                .errorType(Error.ERROR_TYPE.DATABASE.toValue())
                .level(Error.SEVERITY.LOW.name())
                .build());
        return errors;
    }

    public static Collection<Error> getConfigurationNotFoundErrorList() {

        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());
        return errors;
    }

    public static BaseResponse getNonAuthoritativeInformationErrorList() {

        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.NON_AUTHORITATIVE_INFORMATION)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());

        return GngUtils.getBaseResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, errors);

    }


    public static Collection<Error> getCouldNotProcessErrorList() {
        final Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.PROCESSING_FAILED)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.HIGH.name())
                .build());
        return errors;
    }

    public static int getAllowedProduct(GoNoGoCroApplicationResponse parentApplication) {
        Object derivedFields = null;
        if (null != parentApplication.getApplicantComponentResponse() &&
                null != parentApplication.getApplicantComponentResponse().getScoringServiceResponse() &&
                null != parentApplication.getApplicantComponentResponse().getScoringServiceResponse().getDerivedFields()) {
            derivedFields = parentApplication
                    .getApplicantComponentResponse()
                    .getScoringServiceResponse().getDerivedFields();
        }

        int allowedProducts = 1;
        if (derivedFields instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> derivedFieldsMap = (Map<String, Object>) derivedFields;
            if (derivedFieldsMap
                    .get("CUSTOM_FIELDS$MULTI_PRODUCT") instanceof Integer) {
                allowedProducts = (int) derivedFieldsMap
                        .get("CUSTOM_FIELDS$MULTI_PRODUCT");
            }
        }
        return allowedProducts;
    }

    public static BaseResponse getBaseResponse(CustomHttpStatus httpStatus, Collection<Error> errors) {

        return BaseResponse.builder()
                .status(com.softcell.gonogo.model.response.core.Status.builder()
                        .statusCode(httpStatus.value()).statusValue(httpStatus.name()).build())
                .errors(errors)
                .build();
    }

    public static <T> T convertBaseResponsePayload(BaseResponse baseResponse, Class<T> clazz) {
        try {
            if (baseResponse != null && baseResponse.getPayload() != null && baseResponse.getPayload().getT() != null) {

                return clazz.cast(baseResponse.getPayload().getT());

            }

            return null;
        } catch (ClassCastException e) {
            logger.error("{}",e.getStackTrace());
            return null;
        }
    }

    public static String convertIntexResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.INTEX_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.INTEX_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.INTEX_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.INTEX_CODE_THREE;
            }
            break;

            default: {
                message = FieldSeparator.BLANK;
            }
            break;
        }
        return message;
    }

    public static String convertSetToString(Set<String> valueSet) {
        String result = "";
        if (null != valueSet)
            result = valueSet.parallelStream().filter(StringUtils::isNotBlank).collect(Collectors.joining(FieldSeparator.COMMA_STR));

        return result;
    }

    public static Collection<String> convertToCollectionOfString(Collection<Product> products) {
        Collection<String> productList = Collections.emptyList();
        if (!CollectionUtils.isEmpty(products)) {
            productList = products.parallelStream()
                    .map(product -> product.name())
                    .collect(Collectors.toList());
        }
        return productList;
    }

    /**
     * Regex validation use this method
     *
     * @param patternString
     * @param value
     * @return
     */
    public static boolean isValid(String patternString, String value) {


        if (StringUtils.isNotBlank(patternString) && StringUtils.isNotBlank(value)) {

            Pattern pattern = Pattern.compile(patternString);

            return pattern.matcher(value).matches();

        } else {
            return false;
        }

    }

    public static Collection<String> getEnumValueList(Class<? extends Enum<?>> e) {
        return Arrays.asList(Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new));
    }

    public static BaseResponse checkBlankResponseForSchemeMaster(List<SchemeMasterData> withFilterSchemeData) {

        if (null != withFilterSchemeData && !withFilterSchemeData.isEmpty()) {

            return GngUtils.getBaseResponse(HttpStatus.OK, withFilterSchemeData);
        } else {

            return GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
    }

    public static Set<String> getApplicationStagesForUtrUpload() {
        Set<String> stagesForUtrUpload = new TreeSet<>();

        stagesForUtrUpload.add(GNGWorkflowConstant.POST_DECISION_DATA_ENTRY.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.DO.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.SRNV.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.IMEIV.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.INV_GNR.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.LOS_APRV.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.LOS_DISB.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.LOS_QDE.toFaceValue());
        stagesForUtrUpload.add(GNGWorkflowConstant.LOS_BDE.toFaceValue());

        return stagesForUtrUpload;
    }


    public static String getCurrentStageBasedOnApplicationStatus(String applicationStatus) {

        if (GNGWorkflowConstant.QUEUED.toFaceValue().equalsIgnoreCase(applicationStatus)) {

            return GNGWorkflowConstant.CR_Q.toFaceValue();

        } else if (GNGWorkflowConstant.ON_HOLD.toFaceValue().equalsIgnoreCase(applicationStatus)) {

            return GNGWorkflowConstant.CR_H.toFaceValue();

        } else if (StringUtils.equalsIgnoreCase(applicationStatus, GNGWorkflowConstant.DECLINED.toFaceValue())) {

            return GNGWorkflowConstant.DCLN.toFaceValue();

        } else if (GNGWorkflowConstant.APPROVED.toFaceValue().equalsIgnoreCase(applicationStatus)) {

            return GNGWorkflowConstant.APRV.toFaceValue();

        } else if (GNGWorkflowConstant.REPROCESSING.toFaceValue().equalsIgnoreCase(applicationStatus)) {

            return GNGWorkflowConstant.CR_Q.toFaceValue();

        } else {

            logger.warn(" stageID [{}] provided is not registered  in system ", applicationStatus);
        }

        return null;
    }

    public static String getCurrentStageBasedOnApplicationBucket(String bucket){

        if(StringUtils.isNotEmpty(bucket))
            bucket = bucket.toUpperCase();
        else
            return null;

        String changedStage = null;
        if(Buckets.isFosBucket(bucket))
            changedStage = GNGWorkflowConstant.DE.name();
        else if(Buckets.isCreditBucket(bucket))
            changedStage = GNGWorkflowConstant.CRDT.name();

        return changedStage;
    }

    public static String getManufaturer(List<AssetDetails> assetDetails) {
        String manufacturer = null;
        if (null != assetDetails && !assetDetails.isEmpty()) {
            for (AssetDetails assetDetail : assetDetails) {
                manufacturer = assetDetail.getAssetMake();
            }
        }
        return manufacturer;
    }

    /**
     * Returns the manufacturer code of the asset.
     * @param assetDetails
     * @return
     */
    public static String getManufaturerCode(List<AssetDetails> assetDetails) {
        String manufacturer = null;
        if (null != assetDetails && !assetDetails.isEmpty()) {
            for (AssetDetails assetDetail : assetDetails) {
                manufacturer = assetDetail.getAssetMake();
            }
        }
        return manufacturer;
    }

    /**
     * This method will return Error list regarding to change application status.
     *
     * @return
     */
    public static Collection<Error> getChangeApplicationStatusErrorList() {

        Collection<Error> errors = new ArrayList<>();

        errors.add(Error.builder()
                .message(ErrorCode.UNABLE_TO_CHANGE_APPLICATION_STATUS)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());
        return errors;
    }

    /**
     * @param applicationRequestRecords
     * @return
     */
    public static List<GoNoGoCustomerApplication> getGoNoGoApplicationWithDECases(List<ApplicationRequest> applicationRequestRecords) {

        List<GoNoGoCustomerApplication> goNoGoCustomerApplicationsDECases = new ArrayList<>();
        for (ApplicationRequest applicationRequest : applicationRequestRecords) {

            GoNoGoCustomerApplication goNoGoCustomerApplication = new GoNoGoCustomerApplication();
            goNoGoCustomerApplication.setApplicationRequest(applicationRequest);
            goNoGoCustomerApplication.setGngRefId(applicationRequest.getRefID());
            goNoGoCustomerApplicationsDECases.add(goNoGoCustomerApplication);
        }

        return goNoGoCustomerApplicationsDECases;
    }

    public static String convertSamsungImeiResponseCodeToMessage(String responseCode) {
        String message = null;
        switch (responseCode) {
            case "0": {
                message = ResponseConstants.SAMSUNG_CODE_ZERO;
            }
            break;
            case "-1": {
                message = ResponseConstants.SAMSUNG_CODE_MINUS_ONE;
            }
            break;

            case "1": {
                message = ResponseConstants.SAMSUNG_CODE_ONE;
            }
            break;

            case "2": {
                message = ResponseConstants.SAMSUNG_CODE_TWO;
            }
            break;

            case "3": {
                message = ResponseConstants.SAMSUNG_CODE_THREE;
            }
            break;

            case "4": {
                message = ResponseConstants.SAMSUNG_CODE_FOUR;
            }
            break;

            case "5": {
                message = ResponseConstants.SAMSUNG_CODE_FIVE;
            }
            break;

            case "6": {
                message = ResponseConstants.SAMSUNG_CODE_SIX;
            }
            break;

            default: {
                message = FieldSeparator.BLANK;
            }
            break;
        }
        return message;
    }

    public static Collection<Error> getDealerMissMatchErrorList(String submissionDealerId, String postIpaDealerId) {

        Collection<Error> errors = new ArrayList<>();

        errors.add(Error.builder()
                .message(String.format(ErrorCode.DEALER_MISMATCH, submissionDealerId, postIpaDealerId))
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());
        return errors;
    }

    public static String getSchemeForAppleIMEI(PostIPA postIPA) {
        int minTenure = postIPA.getAdvanceEMITenor();
        int maxTenure = postIPA.getTenor();
        return new StringBuilder()
                .append(maxTenure)
                .append(FieldSeparator.PIPE)
                .append(minTenure).toString();
    }

    public static List<Character> getCharacterList(String stringData) {
        List<Character> list = new ArrayList<Character>();
        for (char c : (stringData.toCharArray())) {
            list.add(c);
        }
        return list;
    }

    /**
     * <<Sourcessytem>>- 3 chars +
     * <<Product code 8 chars>> +
     * <<Branch code of 4 chars>> +
     * << currnet date> - in ddmmyy format +
     * '.' +
     * <<running seq no  of 8 chars>>
     *
     * @param applicationRequest
     * @param header
     * @return
     */
    public static String getMbFileName(ApplicationRequest applicationRequest, Header header, long sequenceId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GNG");

        if (Product.CDL == header.getProduct()) {
            stringBuilder.append("000000CD");
        } else if (Product.DPL == header.getProduct()) {
            stringBuilder.append("000000DP");
        }

        if (null != applicationRequest && null != applicationRequest.getAppMetaData() && null != applicationRequest.getAppMetaData().getBranch()) {
            if (StringUtils.isNotBlank(applicationRequest.getAppMetaData().getBranch().getBranchCode())) {
                stringBuilder.append(getFourCharacterBranchCode(applicationRequest.getAppMetaData().getBranch().getBranchCode()));
            } else {
                stringBuilder.append("0000");
            }
        } else {
            stringBuilder.append("0000");
        }

        stringBuilder.append(GngDateUtil.getDDMMYYYYFormat(new Date()));
        stringBuilder.append('.');
        stringBuilder.append(getModifiedSequence(sequenceId));

        return stringBuilder.toString();
    }

    private static String getModifiedSequence(long sequenceId) {
        String convertedSequenceNumber = String.valueOf(sequenceId);
        int actualLength = StringUtils.length(convertedSequenceNumber);
        int lenOfAdditionZero = 8 - actualLength;
        if (lenOfAdditionZero >= 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int branchCodeIndex = 0; branchCodeIndex < lenOfAdditionZero; branchCodeIndex++) {
                stringBuilder.append("0");
            }
            stringBuilder.append(convertedSequenceNumber);
            return StringUtils.right(stringBuilder.toString(), 8);
        } else {
            return StringUtils.right(convertedSequenceNumber, 8);
        }
    }

    private static String getFourCharacterBranchCode(String branchCode) {
        int actualLength = StringUtils.length(branchCode);
        int lenOfAdditionZero = 4 - actualLength;
        if (lenOfAdditionZero >= 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int branchCodeIndex = 0; branchCodeIndex < lenOfAdditionZero; branchCodeIndex++) {
                stringBuilder.append("0");
            }
            stringBuilder.append(branchCode);
            return StringUtils.right(stringBuilder.toString(), 4);
        } else {
            return StringUtils.right(branchCode, 4);
        }
    }

    public static String getMake(List<AssetDetails> assetDetails) {
        String make = null;
        if (!CollectionUtils.isEmpty(assetDetails)) {
            for (AssetDetails assetDetail : assetDetails) {
                make = assetDetail.getAssetModelMake();
            }
        }
        return make;
    }

    public static String getDealerName(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        if (null != goNoGoCustomerApplication.getApplicationRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication() &&
                !CollectionUtils.isEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getAsset())) {

            return goNoGoCustomerApplication.getApplicationRequest()
                    .getRequest().getApplication().getAsset().get(0).getDlrName();

        }
        return null;

    }

    public static String getModelNo(PostIPA postIPA) {

        if (null != postIPA && !CollectionUtils.isEmpty(postIPA.getAssetDetails()) &&
                null != postIPA.getAssetDetails().get(0).getModelNo()) {

            return postIPA.getAssetDetails().get(0).getModelNo();

        }
        return null;

    }

    public static Set<String> getDedupeRefIdsSet(Set<DedupeDetails> dedupedApplications) {

        Set<String> refIdSet = new TreeSet<>();

        for (DedupeDetails dedupeDetails : dedupedApplications) {

            if (null != dedupeDetails.getRefId()) {
                refIdSet.add(dedupeDetails.getRefId());

            }
        }
        return refIdSet;
    }

    public static String getCibilScore(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        if (null != goNoGoCustomerApplication.getIntrimStatus() &&
                null != goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult() &&
                null != goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult().getFieldValue()) {

            return goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult().getFieldValue();
        }
        return null;
    }

    public static String formDeliveryOrderName(String gngRefId) {

        StringBuilder stringBuilder = new StringBuilder();

        if (StringUtils.isNotBlank(gngRefId)) {

            stringBuilder.append("DO_").append(gngRefId)
                    .append("_");

        }
        return stringBuilder.toString();
    }

    public static FileHeader getFileHeader(Header header) {

        FileHeader fileHeader = new FileHeader();

        fileHeader.setInstitutionId(header.getInstitutionId());
        fileHeader.setApplicationId(header.getApplicationId());
        fileHeader.setApplicationSource(header.getApplicationSource());
        fileHeader.setCroId(header.getCroId());
        fileHeader.setDateTime(header.getDateTime());
        fileHeader.setDsaId(header.getDsaId());
        fileHeader.setDealerId(header.getDealerId());
        fileHeader.setProduct(header.getProduct());
        fileHeader.setRequestType(header.getRequestType());
        fileHeader.setDeviceInfo(header.getDeviceInfo());

        return fileHeader;
    }

    /***
     *
     * This method will be return substring of range (0, subStringLength) of provided string s
     * based on only if string size is greater subStringLength
     * @param s  String need to be substring
     * @param subStringLength max length of substring
     * @return substring of provided string if string length greater then subStringLength it will return substring from 0 to subStringLength
     * */

    public static String getTruncatedString(String s, int subStringLength) {

        if (StringUtils.isEmpty(s))
            return StringUtils.EMPTY;

        return s.length() > subStringLength ? StringUtils.substring(s, 0, subStringLength) : s;
    }

    public static String sanitizeString(String queryString) {
        if (StringUtils.isNotBlank(queryString)) {
            return CharMatcher.anyOf(SPECIAL_CHAR).removeFrom(queryString);
        } else {
            return FieldSeparator.BLANK;
        }
    }

    public static String convertToPosidexContactCode(String addressType) {
        String contactCode = null;
        switch (addressType) {
            case "PERSONAL_MOBILE": {
                contactCode = "01";
            }
            break;
            case "RESIDENCE_PHONE": {
                contactCode = "02";
            }
            break;
            case "OFFICE_PHONE": {
                contactCode = "03";
            }
            break;
        }
        return contactCode;
    }

    public static String convertToPosidexAddressCode(String addressType) {
        String addressCode = null;
        switch (addressType) {
            case "OFFICE": {
                addressCode = "OF";
            }
            break;
            case "PERMANENT": {
                addressCode = "PR";
            }
            break;
            case "RESIDENCE": {
                addressCode = "RS";
            }
            break;
        }
        return addressCode;
    }

    public static String getPosidexTranformGender(String gender) {
        String posidexGender = null;
        switch (gender){
            case "Male":{
                posidexGender = GNGWorkflowConstant.M.toFaceValue();
            }
            break;
            case "Female":{
                posidexGender = GNGWorkflowConstant.F.toFaceValue();
            }
            break;
        }
        return posidexGender;
    }

    public static String convertVideoconResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.VIDEOCON_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.VIDEOCON_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.VIDEOCON_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.VIDEOCON_CODE_THREE;
            }
            break;

            case 4: {
                message = ResponseConstants.VIDEOCON_CODE_FOUR;
            }
            break;

            case 5: {
                message = ResponseConstants.VIDEOCON_CODE_FIVE;
            }
            break;

            case 6: {
                message = ResponseConstants.VIDEOCON_CODE_SIX;
            }
            break;

            default: {
                message = FieldSeparator.BLANK;
            }
            break;
        }
        return message;
    }

    public static String convertTKILResponseCodeToMessage(boolean responseCode){

        String message = null;
        if (responseCode)
            message = ResponseConstants.TKIL_TRUE;
        else
            message = ResponseConstants.TKIL_FALSE;

        return message;
    }

    public static boolean checkDeliveryOrderRestoreStatus(String deliveryOrderStatus) {

        if (StringUtils.equals(deliveryOrderStatus, DoStatusEnum.MAILED.name())
                || StringUtils.equals(deliveryOrderStatus, DoStatusEnum.MAIL_IN_PROGRESS.name())
                || StringUtils.equals(deliveryOrderStatus, DoStatusEnum.MAIL_FAILED.name())
                || StringUtils.equals(deliveryOrderStatus, DoStatusEnum.NOT_CREATED.name())
                || StringUtils.equals(deliveryOrderStatus, DoStatusEnum.CREATED.name())) {
            return true;

        }
        return false;

    }

    public static String decodeBase64EncodedString(String reqStr) {

        byte[] fileByte = org.apache.commons.codec.binary.Base64.decodeBase64(reqStr);
        return new String(fileByte);
    }

    public static String getSurrogateName(Surrogate surrogate) {

        String surrogateName = null;

        if(surrogate != null){

            if(surrogate.getSelectedSurrogate() != null){

                surrogateName = surrogate.getSelectedSurrogate().getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getBankSurragates())){

                surrogateName = SurrogateType.BANK_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getCarSurrogate())){

                surrogateName = SurrogateType.CAR_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getCreditCardSurrogate())){

                surrogateName = SurrogateType.CREDIT_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getDebitCardSurrogate())){

                surrogateName = SurrogateType.DEBIT_CARD_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getHouseSurrogate())){

                surrogateName = SurrogateType.HOUSE_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getOldMobilePhoneSurrogates())){

                surrogateName = SurrogateType.OLD_MOBILE_PHONE_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getPostPaidBillSurrogates())){

                surrogateName = SurrogateType.POST_PAID_BILL_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getSalariedSurrogate())){

                surrogateName = SurrogateType.SALARIED_SURROGATE.getLosSurrogate();

            }else if(!CollectionUtils.isEmpty(surrogate.getTraderSurrogate())){

                surrogateName = SurrogateType.BUSINESS_SURROGATE.getLosSurrogate();

            }
        }

        return surrogateName;

    }

    public static Collection<Error> getInvalidProductErrorList() {

        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.INVALID_PRODUCT)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.LOW.name())
                .build());
        return errors;
    }

    public static Collection<Error> getInvalidSerialOrIMEIErrorList() {

        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER)
                .errorCode(String.valueOf(Error.ERROR_TYPE.BUSINESS.toCode()))
                .errorType(Error.ERROR_TYPE.BUSINESS.toValue())
                .level(Error.SEVERITY.HIGH.name())
                .build());
        return errors;
    }

    public static String convertPanasonicMessageForIMEI(String responseMessage) {
        String result = null;
        final String serial = "serial";
        final String IMEI = "IMEI";

        if ( StringUtils.containsIgnoreCase(responseMessage,serial)) {
            //found
            result = responseMessage.toLowerCase().replaceAll( serial, IMEI );

        }else{
            //not found
            result = responseMessage;

        }

        return result;
    }
    /*
    * Generate UUID
    * */
    public static final String generateUUID()
    {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "");
    }

    public static String getBureauResponse(Object bureauResponseObject) {
        String bureauResponseString = "";
        if (null != bureauResponseObject) {
            if (bureauResponseObject instanceof LinkedHashMap) {
                try {
                    bureauResponseString = new ObjectMapper().writeValueAsString(bureauResponseObject);
                } catch (JsonProcessingException e) {
                    logger.error("{}",e.getStackTrace());
                    logger.error("Error converting bureau response object to string while sending to Salesforce cause");
                }
            } else {
                //it is json string with escape character.
                bureauResponseString = bureauResponseObject.toString().replace("\\", "");
            }
        }
        return bureauResponseString;
    }

    public static String convertGoogleResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.GOOGLE_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.GOOGLE_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.GOOGLE_CODE_TWO;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertGoogleRollbackResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.GOOGLE_ROLLBACK_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.GOOGLE_ROLLBACK_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.GOOGLE_ROLLBACK_CODE_TWO;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertOppoResponseCodeToMessage(int responseCode) {
        String message=null;
        switch(responseCode) {
            case 0: {
                message=ResponseConstants.OPPO_CODE_ZERO;
            }
            break;

            default: {
                message=FieldSeparator.BLANK;
            }
            break;
        }
        return  message;
    }

    public static String convertComioResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.COMIO_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.COMIO_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.COMIO_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.COMIO_CODE_THREE;
            }
            break;

            case 4: {
                message = ResponseConstants.COMIO_CODE_FOUR;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertComioRollbackResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_THREE;
            }
            break;

            case 4: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_FOUR;
            }
            break;

            case 5: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_FIVE;
            }
            break;

            case 6: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_SIX;
            }
            break;

            case 7: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_SEVEN;
            }
            break;

            case 8: {
                message = ResponseConstants.COMIO_ROLLBACK_CODE_EIGHT;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }


    public static String convertOnidaResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.ONIDA_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.ONIDA_CODE_ONE;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static LosMbData getLosMbData(ApplicationRequest applicationRequest, long mbPushSequenceNumber) {

        String mbFileName = getMbFileName(applicationRequest, applicationRequest.getHeader(), mbPushSequenceNumber);

        LosMbData losMbData = new LosMbData().builder()
                .losMbFileName(mbFileName)
                .refId(applicationRequest.getRefID())
                .build();

        return losMbData;
    }

    public static String convertNokiaResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.NOKIA_IMEI_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.NOKIA_IMEI_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.NOKIA_IMEI_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.NOKIA_IMEI_CODE_THREE;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertNokiaRollbackResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case 0: {
                message = ResponseConstants.NOKIA_ROLLBACK_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.NOKIA_ROLLBACK_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.NOKIA_ROLLBACK_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.NOKIA_ROLLBACK_CODE_THREE;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertCarrierResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case -1: {
                message = ResponseConstants.CARRIER_CODE_MINUS_ONE;
            }
            break;

            case 0: {
                message = ResponseConstants.CARRIER_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.CARRIER_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.CARRIER_CODE_TWO;
            }
            break;

            case 3: {
                message = ResponseConstants.CARRIER_CODE_THREE;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }

    public static String convertCarrierRollbackResponseCodeToMessage(int responseCode) {
        String message = null;
        switch (responseCode) {
            case -1: {
                message = ResponseConstants.CARRIER_ROLLBACK_CODE_MINUS_ONE;
            }

            case 0: {
                message = ResponseConstants.CARRIER_ROLLBACK_CODE_ZERO;
            }
            break;

            case 1: {
                message = ResponseConstants.CARRIER_ROLLBACK_CODE_ONE;
            }
            break;

            case 2: {
                message = ResponseConstants.CARRIER_ROLLBACK_CODE_TWO;
            }
            break;

            default: {
                message = ResponseConstants.INVALID_RESPONSE_CODE;
            }
            break;
        }
        return message;
    }


    public static Collection<Error> getThirdPartyFailureErrorList() {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                .level(Error.SEVERITY.HIGH.name())
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .build());
        return errors;
    }

    public static String convertImageToBase64Code(String imagePath) throws Exception{

        String base64Code = null;
        if (StringUtils.isNotBlank(imagePath)) {
            byte[] tickMarkByte = Files.readAllBytes(new ClassPathResource(imagePath).getFile().toPath());
            if (null != tickMarkByte) {
                base64Code = new String(Base64.encodeBase64(tickMarkByte), "UTF-8");
            }
        }
        return base64Code;
    }

    public static String trimString(String str)
    {
        str = str.substring(0, Math.min(str.length(), 25));
        return str;
    }

    public static Name convertFirstMiddleLastToName(String fName, String mName, String lName) {
        Name result = new Name();
            result.setFirstName(fName);
            result.setMiddleName(mName);
            result.setLastName(lName);
        return result;
    }

    public static String getSchemeForLg(PostIPA postIPA) {
        int minTenure = postIPA.getAdvanceEMITenor();
        int maxTenure = postIPA.getTenor();
        return new StringBuilder()
                .append(maxTenure)
                .append(FieldSeparator.CROSS)
                .append(minTenure).toString();
    }

    public static String convertLgMessageForIMEI(String responseMessage) {
        String result = null;
        final String serial = "serial";
        final String IMEI = "IMEI";

        if ( StringUtils.containsIgnoreCase(responseMessage,serial)) {
            //found
            result = responseMessage.toLowerCase().replaceAll( serial, IMEI );

        }else{
            //not found
            result = responseMessage;

        }

        return result;
    }
    public static String convertLavaResponseMessage(LavaResponse lavaResponse) {
        String message = null;
        switch (lavaResponse.getErrorCode()) {
            case -1: {
                message = ResponseConstants.INVALID_IMEI;
            }
            break;
            case 101: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROONE;
            }
            break;
            case 102: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROTWO;
            }
            break;
            case 103: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROTHREE;
            }
            break;
            case 104: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROFOUR;
            }
            break;
            case 105: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROFIVE;
            }
            break;
            case 106: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROSIX;
            }
            break;
            case 108: {
                message = ResponseConstants.LAVA_IMEI_CODE_ONEZEROEIGHT;
            }
            break;
            default: {
                message = lavaResponse.getMessage();
            }
            break;
        }
        return message;
    }

    public static ErrorDesc getOtherResponse(Map<Integer, String> queryResObject) {
        String statusCode = null;
        String statusMessage = null;

        for (Integer httpStatus : queryResObject.keySet()) {
            statusCode = String.valueOf(httpStatus);
            statusMessage = queryResObject.get(httpStatus);
            break;
        }

        return ErrorDesc.builder()
                .message(statusMessage)
                .type(statusCode)
                .build();
    }

    public static boolean isIndividual(Applicant applicant) {
        return isIndividual(applicant.getApplicantType());
    }

    public static boolean isIndividual(String applicantType) {
        if (ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicantType)) {
            return true;
        } else {
            return false;
        }
    }
    public static String encodeToSHA1(String string){
        String encoded;
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder();
        int length = 6;
        String salt = "";
        encoded = shaPasswordEncoder.encodePassword(string, salt);
        return encoded;
    }

    public static String getPhoneNoWithCountryCode(Phone phone) {
        StringBuffer str = new StringBuffer(Constant.BLANK);
        if (StringUtils.isNotEmpty(phone.getCountryCode())) {
            str = str.append(phone.getCountryCode()).append("-");
        }
        if (StringUtils.isNotEmpty(phone.getAreaCode())) {
            str = str.append(phone.getAreaCode()).append("-");
        }
        if (StringUtils.isNotEmpty(phone.getPhoneNumber())) {
            str = str.append(phone.getPhoneNumber());
        }
        return str.toString();
    }

    public static String setValueNA(String value)
    {
        return StringUtils.isEmpty(value)? Constant.NA : value;
    }

    public static String setValueForNull(String value)
    {
        return value == null ? Constant.BLANK : value;
    }

    public static String booleanToYesNO(boolean flag) {
        String yesNo = Constant.YN_No;
        if( flag ) yesNo = Constant.YN_Yes;
        return yesNo;
    }
    public static Name seggragateName(String nameString){
        Name name = new Name();
        if(StringUtils.isNotEmpty(nameString)){
            List<String> nameElements = Arrays.asList(StringUtils.split(nameString));
            if( nameElements.size() == 1){
                name.setFirstName(nameElements.get(0));
            } else if( nameElements.size() == 2){
                name.setFirstName(nameElements.get(0));
                name.setLastName(nameElements.get(1));
            } else if( nameElements.size() == 3){
                name.setFirstName(nameElements.get(0));
                name.setMiddleName(nameElements.get(1));
                name.setLastName(nameElements.get(2));
            } else {
                name.setFirstName(nameElements.get(0));
                name.setLastName(nameElements.get(nameElements.size()-1));

                /*int aa = nameElements.size()-1;
                nameElements.remove(aa);
                nameElements.remove(0);*/
                StringBuilder sb = new StringBuilder();
                for ( int i = 1; i< nameElements.size() - 1; i++) {

                    sb.append(nameElements.get(i)).append(FieldSeparator.SPACE_STR);
                }
                sb.trimToSize();
                name.setMiddleName(sb.toString());
            }
        }
        return name;
    }
    /* The below Utility method is used to get the requested DB String values
     * as map collection from the specified Object and Class type.
     */
    public static HashMap<String, String> getRequestedQueryValues(Object obj, List<String> keyList, Class<?> cls){
        HashMap<String, String> map = new HashMap();
        for(String key: keyList){
            String value = GngUtils.getRequestStringValue(obj, key, cls);
            map.put(key, value);
        }
        return map;
    }

    public static String getRequestStringValue(Object obj, String key, Class<?> cls){
        Object valueObj = null;
        Class innerClass = null;
        String value = null;
        String[] tokens = key.split("\\.");
        try {
            innerClass = cls;
            valueObj = obj;
            for(int i=0; i < tokens.length; i++){
                String token = tokens[i];
                Field field = innerClass.getDeclaredField(token);
                field.setAccessible(true);
                Type type = field.getGenericType();
                if(type instanceof ParameterizedType){
                    ParameterizedType pType = (ParameterizedType) type;
                    innerClass = (Class<?>) pType.getActualTypeArguments()[0];
                    valueObj = field.get(valueObj);
                    if(valueObj instanceof List){
                        List<?> list = (List<?>) valueObj;
                        if(list.size() > 0){
                            value = getFieldValues(i,tokens,innerClass,list);
                            return value;
                        }
                    }
                }else {
                    valueObj = field.get(valueObj);
                    innerClass = field.getType();
                }
            }
        } catch (Exception e) {
            return value;
        }
        return valueObj.toString();
    }

    private static String getFieldValues(int index, String[] tokens, Class<?> iClass, List<?> list) {
        String value = null;
        Object classObj = null;
        Class<?> innerClass = null;
        try {
            for (int i = 0; i < list.size(); i++) {
                classObj = list.get(i);
                innerClass = iClass;
                for (int j = index + 1; j < tokens.length; j++) {
                    String token = tokens[j];
                    Field field = innerClass.getDeclaredField(token);
                    field.setAccessible(true);
                    Type type = field.getGenericType();
                    if(type instanceof ParameterizedType){
                        ParameterizedType pType = (ParameterizedType) type;
                        innerClass = (Class<?>) pType.getActualTypeArguments()[0];
                        classObj = field.get(classObj);
                        if(classObj instanceof List){
                            List<?> ilist = (List<?>) classObj;
                            if(ilist.size() > 0){
                                value = getFieldValues(j,tokens,innerClass,ilist);
                                return value;
                            }
                        }
                    }else {
                        classObj = field.get(classObj);
                        innerClass = field.getType();
                    }
                }
                if(value == null){
                    value = classObj.toString();
                }else{
                    value = value + ","+classObj.toString();
                }
            }
        }catch (Exception e) {
            return value;
        }
        return value;
    }

    public static String getUserName(String institutionId, String userId){
        String userName = null;
        if(Cache.INSTITUTION_WISE_USER_ID_NAME_MAP.containsKey(institutionId)) {
            if(Cache.INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).containsKey(userId)){
                userName = Cache.INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).get(userId);
            }
        }
        return userName;
    }
    public static String getCibilScore(ResponseMultiJsonDomain multiBureauJsonRespose) {
        String cibilScore = null;
        Object obj = multiBureauJsonRespose.getMergedResponse();
        if(obj != null && ((LinkedHashMap) obj).get("bureauFeeds") != null &&
                CollectionUtils.isNotEmpty((List)(((LinkedHashMap)((LinkedHashMap) obj).get("bureauFeeds")).get("cibilScore")))) {
            cibilScore = (String)((LinkedHashMap)((List)((LinkedHashMap)((LinkedHashMap) obj).get("bureauFeeds"))
                    .get("cibilScore")).get(0)).get("score");
        }
        return cibilScore ;
    }

    public static String getCibilScore(OutputAckDomain mbCorpJsonResponse ) {
        String cibilScore = null;
        Object obj = mbCorpJsonResponse.getMergedResponse();
        if(obj != null && ((LinkedHashMap) obj).get("bureauFeeds") != null &&
                CollectionUtils.isNotEmpty((List)(((LinkedHashMap)((LinkedHashMap) obj).get("bureauFeeds")).get("cibilScore")))) {
            cibilScore = (String)((LinkedHashMap)((List)((LinkedHashMap)((LinkedHashMap) obj).get("bureauFeeds"))
                    .get("cibilScore")).get(0)).get("score");
        }
        return cibilScore ;
    }

    public static String setValueForZero(String value)
    {
        return value == null ? Constant.ZERO : value;
    }


    public static String setValueForBlank(String value)
    {
        return value == null ? Constant.BLANK : value;
    }

    public static Collection<Error> getInternalServerErrorList(String message) {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(message)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());

        return errors;

    }

    public static String getValueForKey(List<HeaderKey> headerKeyList, String keyName){
        String value = null;
        /*HeaderKey tempKey = new HeaderKey();
        tempKey.setKeyName(keyName);
        int index = headerKeyList.indexOf(tempKey);
        if( index != -1)
            value = headerKeyList.get(index).getKeyValue();*/
        for (HeaderKey headerKeyValue : headerKeyList) {
            if (headerKeyValue.getKeyName().equals(keyName))
                value = headerKeyValue.getKeyValue();
        }
        return value;
    }

    public void mapToCsv(Map<String, List<String>> info, File csvFile) {
        StringBuilder sb = new StringBuilder();
        sb.append(info.keySet().stream().map(Object::toString).collect(Collectors.joining(",")))
                .append("\n");

        int rowCount = ((List<String>)info.values().iterator().next()).size();
        for( int r = 0; r< rowCount; r++) {
            final int rr = r;
            /*Data may contain comm so we have to enclose String in quotes */
            sb.append(info.values().stream()
                                    .map(mapValueList ->  "\"" + mapValueList.get(rr) + "\"")
                                    .collect(Collectors.joining(",")))
                    .append("\n");
        }
        try {
            FileUtils.writeStringToFile(csvFile, sb.toString());
        } catch (IOException e) {
            logger.error("{}",e.getStackTrace());
        }
    }

    public static final  Comparator<Applicant> sortByApplicantId = (Applicant o1, Applicant o2)
            -> o1.getApplicantId().compareTo(o2.getApplicantId());

    public static Double setValueZero(Double value) {
        return  value == null ? 0 : value;
    }


    public static Collection<Error> getMasterRestrictionErrorList(String message) {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(message)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(MsgConstants.BUSINESS_ERROR)
                .level(Error.SEVERITY.CRITICAL.name())
                .build());

        return errors;
    }

    public static boolean masterNameValidation(String masterName) {
        boolean status = false;
        try {

            List<MasterMappingDetails> values = Arrays.asList(MasterMappingDetails.values());
            Optional<MasterMappingDetails> first = values.stream().filter(masterMappingDetails -> masterMappingDetails.getMasterName().contains(masterName)).findFirst();
            if (first.isPresent()) {
                status = true;
            }
        }catch (Exception e){
            logger.error("Exception occured while validating master Name and exception is:{}",e);
        }
        return status;
    }

    public static String getApiName(HttpServletRequest request) {
        String apiName = request.getRequestURI();
        Map object = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(object != null && object.size() > 0){
            for(int i = 0 ; i < object.size(); i++){
                apiName = StringUtils.substringBeforeLast(apiName, FieldSeparator.FORWARD_SLASH);
            }
        }
        apiName = StringUtils.substringAfterLast(apiName, FieldSeparator.FORWARD_SLASH);
        return apiName;
    }

    public static Collection<Error> getThirdPartyException() {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.EXTERNAL_CALL_FAILURE)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.HIGH.name())
                .build());
        return errors;
    }

    public static BaseResponse getBaseResponse(CustomHttpStatus httpStatus, String errorMessage) {

        Collection<Error> errors = getErrorList(errorMessage);
        return BaseResponse.builder()
                .status(
                        com.softcell.gonogo.model.response.core.Status.builder()
                                .statusCode(httpStatus.value())
                                .statusValue(httpStatus.name()).build())
                .errors(errors)
                .build();
    }

    private static Collection<Error> getErrorList(String errorMessage) {
        Error error = Error.builder()
                .message(errorMessage)
                .errorCode("ER001")
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build();
        Collection<Error> errors = new ArrayList<>();
        errors.add(error);
        return errors;
    }

    public static HashMap<String, Object> getApplicationPropertyDetails(){
        HashMap<String,Object> property=new HashMap<>();
        try {
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                    "workFlowBean.xml");
            Environment environment = applicationContext.getEnvironment();
            if(ArrayUtils.isNotEmpty(environment.getActiveProfiles())){
                String currentProfile = environment.getActiveProfiles()[0];
                Resource resource = new ClassPathResource("/gonogo-"+ currentProfile +".properties");
                logger.info("current active profile resources file name {}",resource.getFile().getName());
                Properties props = PropertiesLoaderUtils.loadProperties(resource);
                setRedisProperties(props, property);
            }
        }catch (Exception e){
            logger.error("Exception occured while fetching properties from property file:{}",e);
        }
        return property;
    }


    private static void setRedisProperties(Properties props, HashMap<String, Object> property) {
        ClusterRedisProperties clusterRedisProperties = new ClusterRedisProperties();
        StandaloneRedisProperties standaloneRedisProperties = new StandaloneRedisProperties();

        clusterRedisProperties.setActive(Boolean.parseBoolean(props.getProperty("redis.cluster.active")));
        clusterRedisProperties.setAdaptiveRefreshTriggersTimeout(Integer.parseInt(props.getProperty("redis.cluster.adaptiveRefreshTriggersTimeout")));
        clusterRedisProperties.setDefaultCacheExpiry(Long.valueOf(props.getProperty("redis.cluster.cache-expire")));
        clusterRedisProperties.setNodes(props.getProperty("redis.cluster.nodes"));

        standaloneRedisProperties.setActive(Boolean.parseBoolean(props.getProperty("redis.enabled")));
        standaloneRedisProperties.setAdaptiveRefreshTriggersTimeout(Integer.parseInt(props.getProperty("redis.adaptiveRefreshTriggersTimeout")));
        standaloneRedisProperties.setDefaultCacheExpiry(Long.valueOf(props.getProperty("redis.cache-expire")));
        standaloneRedisProperties.setRedisHost(props.getProperty("redis.host"));
        standaloneRedisProperties.setRedisPort(Integer.parseInt(props.getProperty("redis.port")));

        property.put(Constant.STANDALONE_SERVER,standaloneRedisProperties);
        property.put(Constant.CLUSTER_SERVER,clusterRedisProperties);
    }
}
