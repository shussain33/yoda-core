package com.softcell.utils;

import com.softcell.constants.ApplicantType;
import com.softcell.constants.CustomFormat;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.scoring.response.Eligibility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CroUtils {

    private static final Logger logger = LoggerFactory.getLogger(CroUtils.class);

    public static List<CroDecision> getCroDecision(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        List<CroDecision> croDecisions = new ArrayList<>();

        try {

            Eligibility eligibilityResponse = goNoGoCustomerApplication.getApplicantComponentResponse()
                    .getScoringServiceResponse().getEligibilityResponse();


            if(eligibilityResponse != null) {

                CroDecision croDecision = new CroDecision();

                croDecision.setEligibleAmt(eligibilityResponse.getEligibilityAmount());

                croDecision.setTenor(Integer.parseInt(CustomFormat.DECIMAL_FORMATE_0
                        .format(eligibilityResponse.getMaxTenor())));

                croDecision.setDownPayment(eligibilityResponse.getDp());

                // Calculation to find approved amount.
                if (goNoGoCustomerApplication.getApplicationRequest().getRequest() != null) {

                    // check added for qde Existing customer to set approved amount in cro decision.
                    if (StringUtils.equals(goNoGoCustomerApplication.getApplicationRequest().getApplicantType(), ApplicantType.EXISTING.value())) {
                        if (goNoGoCustomerApplication
                                .getApplicationRequest().getRequest().getApplication()
                                .getPreApprovedAmount() > eligibilityResponse.getEligibilityAmount()) {
                            croDecision.setAmtApproved(goNoGoCustomerApplication
                                    .getApplicationRequest().getRequest()
                                    .getApplication().getPreApprovedAmount());
                        } else {
                            croDecision.setAmtApproved(eligibilityResponse.getEligibilityAmount());
                        }
                    } else if (eligibilityResponse.getEligibilityAmount() > goNoGoCustomerApplication
                            .getApplicationRequest().getRequest().getApplication()
                            .getLoanAmount()) {
                        croDecision.setAmtApproved(goNoGoCustomerApplication
                                .getApplicationRequest().getRequest()
                                .getApplication().getLoanAmount());
                    } else {
                        croDecision.setAmtApproved(eligibilityResponse
                                .getEligibilityAmount());
                    }
                }
                croDecisions.add(croDecision);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
        }

        return croDecisions;
    }
}
