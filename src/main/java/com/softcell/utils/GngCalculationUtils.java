package com.softcell.utils;

import com.softcell.constants.Constant;
import com.softcell.gonogo.model.core.CoOriginationDetails;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

/**
 * Created by ssg0268 on 19/9/19.
 */
public class GngCalculationUtils {
    public static double calculateCharges(HashMap<String, String> chargesMaster, double charges, String key) {
        double numerator = 0 ;
        double denominator = 100;
        double result = 0;
        numerator = Double.parseDouble(chargesMaster.get(key));
        result = (charges * numerator)/denominator;
        result = roundValue(result,2);
        return result;
    }

    public static double roundValue(double result, int roundUpto) {
        BigDecimal bd = new BigDecimal(result).setScale(roundUpto , RoundingMode.HALF_DOWN);
        result = bd.doubleValue();
        return result;
    }

    public static double calculateTotalCharges(CoOriginationDetails originationdetail, CoOriginationDetails originationDetails) {

        double valueToBeDeducted  = originationdetail.getEmi() + originationdetail.getPF()
                + originationdetail.getInsurance() + originationdetail.getCreditVidya() + originationdetail.getCersaiFeesWithGST()
                + originationdetail.getPreEmi() + originationdetail.getGstCharges() + originationdetail.getImd2gstAmt();
        double totalCharges = originationdetail.getAppLoan() - (valueToBeDeducted);
        return roundValue(totalCharges,0);
    }

    public static double additionForDouble(double ...value) {
        double sum = 0 ;
        for(double field : value){
            sum = sum + field ;
        }
        return sum;
    }

    public static double calculateApprovedAMt(double chargesMaster, double charges, String sbfc) {
        double numerator = 0 ;
        double denominator = 100;
        double result = 0;
        numerator = chargesMaster;
        result = (charges * numerator)/denominator;
        result = roundValue(result,2);
        return result;
    }
    public static String setAmount(double amount){
        BigDecimal bigDecimal = BigDecimal.valueOf(amount);

        return bigDecimal.toPlainString() != null ? Long.toString( Math.round(bigDecimal.doubleValue()) ): Constant.ZERO;
    }
}
