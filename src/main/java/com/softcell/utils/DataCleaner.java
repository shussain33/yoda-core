package com.softcell.utils;

import com.softcell.constants.FieldSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.regex.Pattern;

/**
 * @author kishorp
 *         This class is to cleanInvisible the data. There are different layer to cleanInvisible the data as per the data entity category.
 *         For Name, address, phone general comments or message.
 */
public class DataCleaner {

    private static final Logger logger = LoggerFactory.getLogger(DataCleaner.class);

    private static final Pattern REGEX_PATTERN = Pattern.compile("[^\\p{ASCII}]+");


    /**
     * @param inputData The input character sequence, which need to cleanInvisible.
     *                  The method will allow the only latter and numbers.
     *                  Spaces are allowed.
     * @return
     */
    public static String cleanInvisible(String inputData) {

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();


        String s = null != inputData ?  REGEX_PATTERN.matcher(inputData).replaceAll(FieldSeparator.SPACE_STR) : inputData;

        stopWatch.stop();

        logger.trace(" time taken while cleaning string for invisible character is [{}] ms", stopWatch.getTotalTimeMillis());

        return s;

    }

}
