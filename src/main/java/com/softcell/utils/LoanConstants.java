package com.softcell.utils;

/**
 * Created by archana on 14/12/18.
 */
public interface LoanConstants {
    public static final String LOAN_TOPUP_PARALLEL = "Top Up Parallel";
    public static final String LOAN_TOPUP_GROSS = "Top Up Gross";
    public  static final String LOAN_BT_TOPUP = "BT Topup";
}
