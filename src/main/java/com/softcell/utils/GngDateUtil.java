/**
 * kishorp10:53:57 AM  Copyright Softcell Technolgy
 **/
package com.softcell.utils;

import com.softcell.constants.DateUnit;
import com.softcell.constants.ReportConstant;
import com.softcell.gonogo.model.casehistory.DataUnit;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.reporting.domains.Format;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;


/**
 * @author kishorp
 */
public class GngDateUtil {
    public static final String ddMMyyyy = "ddMMyyyy";
    public static final String ddMMyyyy_WITH_SLASH  = "dd/MM/yyyy";
    public static final String ddMMyyyy_WITH_HYPHEN  = "dd-MM-yyyy";
    public static final String ddMMMyyyy_WITH_SPACE = "dd MMM yyyy";
    public static final String ddMMMyyyy_WITH_HYPHEN = "dd-MMM-yyyy";
    public static final String DD_MMM_YY = "dd MMM YY"; // No formatter yet
    // "22/09/2017 12:28 PM"
    public static final String dd_MM_yyyy_hh_mm_a = "dd/MM/yyyy hh:mm a";
    public static final String dd_MM_yyyy_hh_mm_ss = "dd_MM_yyyy_HH_mm_ss";

    public static final String MMddyyyy_WITH_SLASH = "MM/dd/yyyy";

    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String yyyyMMdd_PATTERN = "yyyyMMdd";
    public static final String YyyyMmddHhMmSs = "yyyy-MM-dd HH:mm:ss";
    public static final String   YyyyMmddTHhMmSs="yyyy-MM-dd'T'HH:mm:ss";

    public static final String yyyy = "yyyy";

    private static ArrayList<Integer> firstQtr = new ArrayList<Integer>(){{ add(4); add(5); add(6);}};
    private static ArrayList<Integer> secondQtr = new ArrayList<Integer>(){{ add(7); add(8); add(9);}};
    private static ArrayList<Integer> thirdQtr = new ArrayList<Integer>(){{ add(10); add(11); add(12);}};

    public static DateTimeFormatter timeStamp = DateTimeFormat
            .forPattern("HH:mm:ss");
    public static DateTimeFormatter tvrTimeStamp = DateTimeFormat
            .forPattern("HHmmss");
    public static DateTimeFormatter timeStamp24H = DateTimeFormat
            .forPattern("hh:mm a");
    private static Logger logger = LoggerFactory.getLogger(GngDateUtil.class);


    private static DateTimeFormatter cibilDateFormat = DateTimeFormat.forPattern(ddMMyyyy);

// ddMmyyyyFormat
    private static DateTimeFormatter ddMMyyyySeparatedBySlashFormat = DateTimeFormat.forPattern(ddMMyyyy_WITH_SLASH);
    private static DateTimeFormatter ddMmYyyy = DateTimeFormat.forPattern(ddMMyyyy_WITH_HYPHEN);
    private static DateTimeFormatter ddMMMYyyy = DateTimeFormat.forPattern(ddMMMyyyy_WITH_SPACE);
    private static DateTimeFormatter ddMmYyyyHhMmSs = DateTimeFormat.forPattern(dd_MM_yyyy_hh_mm_ss);
    private static DateTimeFormatter ddMmYyyyHhMmUnderSqure = DateTimeFormat
            .forPattern("dd_MM_yyyy_HH_mm");
    private static DateTimeFormatter ddMmYyyyHhMmSsSss = DateTimeFormat
            .forPattern("dd-MM-yyyy HH:mm:ss:SSSS");
    private static DateTimeFormatter digitizationFormDateFormat = DateTimeFormat
            .forPattern("ddMMyy");
    private static DateTimeFormatter digitizationFormDateddMMYYYY = DateTimeFormat
            .forPattern("ddMMyyyy");


    private static DateTimeFormatter mmDdYyyySlash = DateTimeFormat
            .forPattern(MMddyyyy_WITH_SLASH);

    private static DateTimeFormatter hhmmss = DateTimeFormat
            .forPattern("HH:mm:ss");

    private static DateTimeFormatter aadharDateFormat = DateTimeFormat
            .forPattern(yyyy_MM_dd);
    private static DateTimeFormatter YyyyMmddHhMmSsFormatter = DateTimeFormat
            .forPattern(YyyyMmddHhMmSs);

    private static DateTimeFormatter pidAadharDateFormat = DateTimeFormat
            .forPattern("yyyy-MM-dd'T'HH:mm:ss");

    private static DateTimeFormatter dateTimeFormat = DateTimeFormat
            .forPattern(yyyy_MM_dd);

    private static DateTimeFormatter samsungImeiSaleDate = DateTimeFormat
            .forPattern("yyyyMMddHHmmss");

    private static DateTimeFormatter aadhaarRequest = DateTimeFormat
            .forPattern("ddMMyyyy HH:mm:ss");

    private static DateTimeFormatter yyyyMMdd = DateTimeFormat.forPattern("yyyyMMdd");

    public static Map<String, DateTimeFormatter> dateStringFormatterMap = new HashMap<>();

    static {
        dateStringFormatterMap.put(ddMMyyyy, cibilDateFormat);
        dateStringFormatterMap.put(ddMMyyyy_WITH_SLASH, ddMMyyyySeparatedBySlashFormat);
        dateStringFormatterMap.put(ddMMyyyy_WITH_HYPHEN, ddMmYyyy);
        dateStringFormatterMap.put(ddMMMyyyy_WITH_SPACE, ddMMMYyyy);
        dateStringFormatterMap.put(dd_MM_yyyy_hh_mm_ss, ddMmYyyyHhMmSs);

        dateStringFormatterMap.put(MMddyyyy_WITH_SLASH, mmDdYyyySlash);

        dateStringFormatterMap.put(yyyy_MM_dd, aadharDateFormat);
        dateStringFormatterMap.put(YyyyMmddHhMmSs, YyyyMmddHhMmSsFormatter);
    }

    /**
     * @param dateTime
     * @return
     */
    public static String getHHmmssCollon(DateTime dateTime) {
        return hhmmss.print(dateTime);
    }


    public static DateTime convertStringToFormatedDate(String dateString, String format) {

        return DateTime.parse(dateString, DateTimeFormat.forPattern(format)).withHourOfDay(00);

    }

    public static String changeDateFormat(Format format, String date) {
        DateTimeFormatter fromFormat = DateTimeFormat.forPattern(format
                .getFrom());
        DateTimeFormatter toFormat = DateTimeFormat.forPattern(format.getTo());
        return toFormat.print(fromFormat.parseDateTime(date));
    }

    /**
     * @param timeStamp
     * @return
     */
    public static String toAadharTimeStamp(Date timeStamp) {
        return pidAadharDateFormat.print(timeStamp.getTime());
    }

    public static String toddMMyyyyTimeStamp(Date timeStamp) {
        return cibilDateFormat.print(timeStamp.getTime());
    }


    /**
     * @param date
     * @return
     */
    public static DateTime getDate(String date) {
        return cibilDateFormat.parseDateTime(date);
    }

    public static String getGioneeDate(DateTime dateTime) {
        return dateStringFormatterMap.get(YyyyMmddHhMmSs).print(dateTime);
    }

    public static String getyyyyMMdd(DateTime dateTime) {
        return dateTimeFormat.print(dateTime);
    }

    /**
     * @param dateTime
     * @return
     */
    public static String getDdMmYyyy(DateTime dateTime) {
        return ddMmYyyy.print(dateTime);
    }

    public static String getDdMmYyyyFormatDate(DateTime dateTime) {
        return dateStringFormatterMap.get( ddMMyyyy_WITH_SLASH).print(dateTime);
    }




    public static String getDdMmYyyyHhMmSsDateFormat(DateTime dateTime) {
        return ddMmYyyyHhMmSs.print(dateTime);
    }

    /**
     * @param dateTime
     * @return Month, date and year separator by slash
     */
    public static String getMmDdYyyySlash(DateTime dateTime) {
        return mmDdYyyySlash.print(dateTime);
    }


    /**
     * @param dateTime
     * @return Month, date and year separator by slash
     */
    public static String getddMMMYyyy(DateTime dateTime) {
        return ddMMMYyyy.print(dateTime);
    }

    /**
     * @param dateTime
     * @return Month, date and year separator by slash
     */
    public static String getTimeStamp(DateTime dateTime) {
        return timeStamp.print(dateTime);
    }

    /**
     * @param dateTime
     * @return Month, date and year separator by slash
     */
    public static String getTimeStamp12H(DateTime dateTime, DateTimeFormatter pattern) {
        return pattern.print(dateTime);
    }

    /**
     * @param dateTime
     * @return Month, date and year separator by underSqure
     */
    public static String getMmDdYyyyUnderSqure(DateTime dateTime) {
        return ddMmYyyyHhMmUnderSqure.print(dateTime);
    }

    /**
     * @param dateTime
     * @return
     */
    public static String getDdMmYyyyHhMmSsSss(DateTime dateTime) {
        return ddMmYyyyHhMmSsSss.print(dateTime);
    }

    /**
     * @param dob
     * @return
     */
    public static int getAge(String dob) {
        try {
            DateTime dateTime = cibilDateFormat.parseDateTime(dob);
            LocalDate birthdate = new LocalDate(dateTime.getYear(),
                    dateTime.getMonthOfYear(), dateTime.getDayOfMonth()); // Birth
            // date
            LocalDate now = new LocalDate();
            Period period = new Period(birthdate, now,
                    PeriodType.yearMonthDay());
            return period.getYears();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0;
        }

    }

    /**
     * @param longDate
     * @return
     */
    public static String getFormattedDate(Date longDate) {
        return getDdMmYyyy(new DateTime(longDate));
    }

    /**
     * @param date
     * @return
     */
    public static String transformDateToAadharFormat(String date) {
        try {
            DateTime gonogoDob = cibilDateFormat.parseDateTime(StringUtils
                    .trim(date));
            return aadharDateFormat.print(gonogoDob);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }

    }

    /**
     * @param validFrom
     * @param validTo
     * @return
     */
    public static boolean isWithinRange(String validFrom, String validTo) {
        try {
            DateTime dateValidFrom = cibilDateFormat.parseDateTime(StringUtils
                    .trim(validFrom));
            DateTime dateValidTo = cibilDateFormat.parseDateTime(StringUtils
                    .trim(validTo));
            Date currentDate = new Date();
            return currentDate.after(dateValidFrom.toDate())
                    && currentDate.before(dateValidTo.toDate());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return false;
        }

    }

    /**
     * @param excluded
     * @return
     */
    public static boolean isDateMatch(String excluded) {
        try {
            DateTime excludedDate = cibilDateFormat.parseDateTime(StringUtils
                    .trim(excluded));
            DateTime currentDate = new DateTime();
            return currentDate.withTimeAtStartOfDay().isEqual(
                    excludedDate.withTimeAtStartOfDay());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return false;
        }

    }

    /**
     * @param startDate Initial date.
     * @param endDate   end date to find deference in seconds.
     * @return Deference in seconds between two date.
     */
    public static int getDateSecond(DateTime startDate, DateTime endDate) {
        long second = startDate.getMillis() - endDate.getMillis();
        return (int) Math.abs(second / 1000);
    }

    /**
     * @param reportingModuleConfiguration This is Mongodb ORM class which holds details of Reports which
     *                                     is generated by scheduler, It is use for create runtime Query
     *                                     to fetch data as per details provided by database.
     * @return from and to date for report generation query.
     */
    public static Date[] getFromToDate(
            ReportingModuleConfiguration reportingModuleConfiguration) {
        Date fromDate = new DateTime().minusDays(1).toDate();
        Date toDate = new Date();
        Date[] fromToDate = new Date[2];

        if (StringUtils.isBlank(reportingModuleConfiguration.getReportCycle())) {
            if (reportingModuleConfiguration.getInterval() != 0) {
                fromDate = new DateTime().minusDays(
                        reportingModuleConfiguration.getInterval()).toDate();
            } else {
                if (StringUtils.isNotBlank(reportingModuleConfiguration
                        .getStartDate())
                        && StringUtils.isNotBlank(reportingModuleConfiguration
                        .getEndDate())) {
                    fromDate = GngDateUtil.getDate(
                            reportingModuleConfiguration.getStartDate()).withHourOfDay(00)
                            .withMinuteOfHour(00).withSecondOfMinute(00).toDate();
                    toDate = GngDateUtil.getDate(
                            reportingModuleConfiguration.getEndDate()).withHourOfDay(23)
                            .withMinuteOfHour(59).withSecondOfMinute(59).toDate();
                }
            }
        } else {
            if (StringUtils.equalsIgnoreCase(ReportConstant.MTD.toString(),
                    reportingModuleConfiguration.getReportCycle())) {
                fromDate = new DateTime().withDayOfMonth(1).withHourOfDay(00)
                        .withMinuteOfHour(00).withSecondOfMinute(00).toDate();
            }

            if (StringUtils.equalsIgnoreCase(ReportConstant.YTD.toString(),
                    reportingModuleConfiguration.getReportCycle())) {
                fromDate = new DateTime().withDayOfYear(1).withHourOfDay(00)
                        .withMinuteOfHour(00).withSecondOfMinute(00).toDate();
            }

            if (StringUtils.equalsIgnoreCase(ReportConstant.WTD.toString(),
                    reportingModuleConfiguration.getReportCycle())) {
                fromDate = new DateTime().withDayOfWeek(1).withHourOfDay(00)
                        .withMinuteOfHour(00).withSecondOfMinute(00).toDate();
            }

        }
        fromToDate[0] = fromDate;
        fromToDate[1] = toDate;
        return fromToDate;
    }

    // FIXME need to fix date ISO conversion to UTC while query compilation
    public static Date getCurrentUTCDate() {
        Date date = null;
        Calendar cal = Calendar.getInstance();
        String DATE_FORMAT = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String s = sdf.format(cal.getTime());
        try {
            date = sdf.parse(s);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return date;
    }

    public static List<String> getTimeSeries(String start, String end) {

        DateTime startDate = new DateTime(start);
        DateTime endDate = new DateTime(end);
        List<String> timeseries = new ArrayList<String>();

        while (startDate.isBefore(endDate)
                && !endDate.toLocalDate().isEqual(startDate.toLocalDate())) {

            if (timeseries.isEmpty()) {
                timeseries.add(dateTimeFormat.print(startDate));
                continue;
            }
            startDate = startDate.plusDays(1);

            timeseries.add(dateTimeFormat.print(startDate));
        }
        return timeseries;
    }

    public static String readableTime(long millis) {
        if (millis < 1000) {
            return millis + " ms";
        }

        if (millis < 60000) {
            return (millis / 1000) + " s";
        }

        if (millis < (60000 * 60)) {
            return (millis / (1000 * 60)) + " m";
        }

        if (millis < (60000 * 60 * 24)) {
            return (millis / (1000 * 60 * 24)) + " h";
        }

        if (millis < (60000 * 60 * 24 * 12)) {
            return (millis / (1000 * 60 * 24 * 12)) + " d";
        }

        return millis + " ms";

    }

    public static DataUnit getTimeDataUnit(long millis) {
        DataUnit dataUnit = new DataUnit();
        if (millis < 1000) {
            dataUnit.setValue(millis + "");
            dataUnit.setUnit(DateUnit.UNIT.msecs);
            return dataUnit;
        }

        if (millis < 60000) {
            dataUnit.setValue((TimeUnit.MILLISECONDS.toSeconds(millis)) + "");
            dataUnit.setUnit(DateUnit.UNIT.secs);
            return dataUnit;
        }

        if (millis < 3600000) {
            dataUnit.setValue(TimeUnit.MILLISECONDS.toMinutes(millis) + "");
            dataUnit.setUnit(DateUnit.UNIT.mins);
            return dataUnit;
        }

        if (millis < 86400000) {
            dataUnit.setValue(TimeUnit.MILLISECONDS.toHours(millis) + "");
            dataUnit.setUnit(DateUnit.UNIT.hrs);
            return dataUnit;
        }

        if (millis > 86400000) {
            dataUnit.setValue(TimeUnit.MILLISECONDS.toDays(millis) + "");
            dataUnit.setUnit(DateUnit.UNIT.days);
            return dataUnit;
        }
        dataUnit.setValue(millis + "");
        return dataUnit;

    }


    public static Map<String, Integer> getTimeSeriesMap(String start, String end) {

        Map<String, Integer> map = new LinkedHashMap<String, Integer>();

        List<String> timeSeries = GngDateUtil.getTimeSeries(start, end);

        for (String string : timeSeries) {
            map.put(string, 0);
        }

        return map;

    }

    /**
     * @param dateTime
     * @return
     */
    public static String getDDMMYYFormat(Date dateTime) {
        return digitizationFormDateFormat.print(dateTime.getTime());
    }

    public static String getDDMMYYYYFormat(Date dateTime) {
        return digitizationFormDateddMMYYYY.print(dateTime.getTime());
    }

    public static String getYYYYMMDDFormat(Date dateTime) {
        return dateTimeFormat.print(dateTime.getTime());
    }

    public static XMLGregorianCalendar getGregorianDate(String dateS) {
        XMLGregorianCalendar dateGeorgian = null;
        if (dateS != null) {

            String DATE_FORMAT = "ddMMyyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            GregorianCalendar c = new GregorianCalendar();
            try {
                Date date = sdf.parse(dateS);
                c.setTime(date);
                dateGeorgian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                if (dateGeorgian != null) {
                    dateGeorgian.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                }
            } catch (DatatypeConfigurationException e) {
                // TODO Auto-generated catch block
                logger.error("{}",e.getStackTrace());
            } catch (ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return dateGeorgian;
    }

    public static XMLGregorianCalendar getGregorianDate(Date dateS) {
        XMLGregorianCalendar dateGeorgian = null;
        if (dateS != null) {
            GregorianCalendar c = new GregorianCalendar();
            try {
                c.setTime(dateS);
                dateGeorgian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                if (dateGeorgian != null) {
                    dateGeorgian.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                }
            } catch (DatatypeConfigurationException e) {
                logger.error("{}",e.getStackTrace());
            }
        }
        return dateGeorgian;
    }

    public static XMLGregorianCalendar getPanVerificationDate(String dateS) {
        XMLGregorianCalendar dateGeorgian = null;
        Date date = null;
        String DATE_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        GregorianCalendar c = new GregorianCalendar();
        try {
            date = sdf.parse(dateS);
            c.setTime(date);
            dateGeorgian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            if (dateGeorgian != null) {
                dateGeorgian.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            }
        } catch (ParseException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return dateGeorgian;

    }

    /**
     * @param hour
     * @return
     */
    public static boolean checkScheduleTime(int hour) {
        DateTime dateTime = new DateTime();
        if (dateTime.getHourOfDay() > hour) {
            return true;
        }
        return false;

    }

    public static String getFormattedDate(Date date, String format) {

        logger.warn("<<  getFormattedDate() : [ date : {},  format : {} ]", date, format);

        SimpleDateFormat sdf = null;

        try {
            if (StringUtils.isNotBlank(format) && date != null) {
                sdf = new SimpleDateFormat(format);

                String format2 = sdf.format(date);
                logger.warn(">> getFormattedDate() : [ return : {}]", format2);
                return format2;

            }

            return null;
        } catch (Exception e) {
            logger.error("Cannot format the date, format string is - {}", format);
            return null;
        }
    }

    public static boolean isDateWithinRange(Date validFrom, Date validTo) {
        Date currentDate = new Date();
        if (getZeroTimeDate(currentDate).compareTo(getZeroTimeDate(validFrom)) != -1
                && getZeroTimeDate(currentDate).compareTo(
                getZeroTimeDate(validTo)) != 1) {

            return true;
        } else {
            return false;
        }
    }

    public static boolean isDateMatch(Date otherDate) {

        Date currentDate = new Date();

        if (getZeroTimeDate(otherDate).compareTo(getZeroTimeDate(currentDate)) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static Date getZeroTimeDate(Date tempDate) {
        Date zeroTimeDate;
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(tempDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        zeroTimeDate = calendar.getTime();

        return zeroTimeDate;
    }

    public static String convertDateSetToString(Set<Date> setOfValues) {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        if (null != setOfValues && !setOfValues.isEmpty()) {
            for (Date value : setOfValues) {
                stringBuilder.append(prefix);
                prefix = ",";
                stringBuilder.append(changeDateFormat(value, "dd/MM/yyyy"));
            }
        }
        return stringBuilder.toString();
    }

    public static String changeDateFormat(Date date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date.getTime());
    }

    public static String toSaleDate(long time) {
        return samsungImeiSaleDate.print(time);
    }


    public static String toAadhaarRequest(long time) {
        return aadhaarRequest.print(time);
    }



    public static Date getDateFromSpecificFormat(String format, String dateInStringFormat) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            date = formatter.parse(dateInStringFormat);

        } catch (ParseException e) {
            logger.error("{}",e.getStackTrace());
        }
        return date;
    }

    public static boolean isToDateGreaterThanFromDate(Date fromDate, Date toDate) {
        int flag = getZeroTimeDate(fromDate).compareTo(getZeroTimeDate(toDate));
        if (flag == -1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGreaterThanGstStartDate(Date date) {
        Date GST_START_DATE = ddMmYyyy.parseDateTime(StringUtils
                .trim("01-07-2017")).toDate();
        int flag = getZeroTimeDate(GST_START_DATE).compareTo(getZeroTimeDate(date));
        if (flag == -1 || flag == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String getDateInYyyyMMddFormat(String dateOfBirth) {
        try {
            DateTime dateTime = cibilDateFormat.parseDateTime(dateOfBirth);
            return yyyyMMdd.print(dateTime);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "0";
        }
    }

    public static String getDateInYyyyMMddFormat(Date dateTime) {
        String date = null;
        try {
            date = yyyyMMdd.print(dateTime.getTime());

        } catch (Exception e) {
            //ignore
        }
        return date;
    }

    public static String getTimeStamp(Date dateTime) {
        return tvrTimeStamp.print(dateTime.getTime());
    }

    public static String transformDateToPosidexFormat(String dateOfBirth) {
        Date dateTime = getDateFromSpecificFormat("ddMMyyyy", dateOfBirth);
        String date = null;
        if (null != dateTime) {
            date = ddMMyyyySeparatedBySlashFormat.print(dateTime.getTime());
        }
        return date;
    }

    public static Date[] getSpecificToFromDate(DateTime startDate, DateTime endDate) {

        Date[] fromToDate = new Date[2];

        Date formDate = startDate.withHourOfDay(00)
                .withMinuteOfHour(00).withSecondOfMinute(00).toDate();

        Date toDate = endDate.withHourOfDay(23)
                .withMinuteOfHour(59).withSecondOfMinute(59).toDate();

        fromToDate[0] = formDate;
        fromToDate[1] = toDate;

        return fromToDate;
    }

    /**
     * Converts date string in "2017-09-27T11:10:12.843Z" format to any format provided
     * @param date
     * @param requiredFormat
     * @return
     */
    public static String changeDateformatWithtimezoneOffset(String date, String requiredFormat){
        DateTime converted = ISODateTimeFormat.dateTime().parseDateTime(date);
        //changeDateFormat(converted, requiredFormat);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern(requiredFormat);
        return dtfOut.print(converted);
    }

    public static Date addYearsToDate(Date date,int year)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, year);
        return c.getTime();
    }

    public static Date getZeroTimeFromDate(Date fromDate) {
        Date formattedFrmDt = new DateTime(fromDate).withHourOfDay(00)
                .withMinuteOfHour(00).withSecondOfMinute(00).toDate();

        return formattedFrmDt;
    }
    public static Date getEndTimeToDate(Date toDate) {
        Date formattedToDt = new DateTime(toDate).withHourOfDay(23)
                .withMinuteOfHour(59).withSecondOfMinute(59).toDate();

        return formattedToDt;
    }

    public static String convertDateIntoStringDateFormat(Date date)
    {
        String new_dateForDownPayment = "";
        try {
            final String OLD_FORMAT_For_DOWNPAYMENT = "dd/MM/yyyy";

            SimpleDateFormat newDateFormatFordownPayment = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
            Date dateForDownPaymentDetails = newDateFormatFordownPayment
                    .parse(newDateFormatFordownPayment.format(date));
            newDateFormatFordownPayment.applyPattern(OLD_FORMAT_For_DOWNPAYMENT);
            new_dateForDownPayment = newDateFormatFordownPayment.format(dateForDownPaymentDetails);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        return new_dateForDownPayment;
    }

    public static String getYear(String dateString, String dateFormat)  {

        DateTimeFormatter formatter = dateStringFormatterMap.get(dateFormat);
        if( formatter == null){
            formatter = DateTimeFormat.forPattern(dateFormat);
        }
        return Integer.toString(formatter.parseDateTime(dateString).getYear());
    }

    public static String convertDateIntoStringFormat(Date date)
    {
        String new_dateForDownPayment = "";
        try {
            final String OLD_FORMAT_For_DOWNPAYMENT = "ddMMyyyy";

            SimpleDateFormat newDateFormatFordownPayment = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
            Date dateForDownPaymentDetails = newDateFormatFordownPayment
                    .parse(newDateFormatFordownPayment.format(date));
            newDateFormatFordownPayment.applyPattern(OLD_FORMAT_For_DOWNPAYMENT);
            new_dateForDownPayment = newDateFormatFordownPayment.format(dateForDownPaymentDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new_dateForDownPayment;
    }

    public static int getDifferenceDays(Date d1, Date d2) {
        int daysdiff = 0;
        long diff = d2.getTime() - d1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    public static String getDateIntoWords(Date date){
        String dateIntoWords = "";
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int day = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);
            dateIntoWords = GngNumUtil.convertToWords(day) + " " + new DateFormatSymbols().getMonths()[month-1] + ", " + GngNumUtil.convertToWords(year);
        }catch(Exception e){
            logger.error("Error occurred while getDateIntoWords with probable cause [{}]", e.getMessage());
        }
        return dateIntoWords;
    }

    public static String toDateStringSlashSeprated(long time) {
        String dateString = null;
        try {
            Date date = new Date(time);
            DateTime dateTime = new DateTime(date);
            dateString = ddMMyyyySeparatedBySlashFormat.print(dateTime);
        } catch (Exception e) {
            logger.error("Error occurred while toDateStringWithSlash with probable cause [{}]", e.getMessage());
        }
        return dateString;
    }


    public static boolean checkScheduleTime(String beginTime, String endTime) {
        ZoneId zone = ZoneId.of( "Asia/Kolkata" ) ;
        java.time.LocalTime now = java.time.LocalTime.now(zone);  // Explicitly specify the desired/expected time zone.
        /* LocalTime fromTime = LocalTime.parse( beginTime);
       LocalTime toTime = LocalTime.parse( endTime);
        if(now.isAfter(fromTime) && now.isBefore(toTime))
            return true;
        else
            return false;*/
        String[] startMinSec = beginTime.split(":");
        String[] endMinSec = endTime.split(":");
        int hour = now.getHour();
        int minute = now.getMinute();
        String currentSystemTime;
        String currentHours;
        String currentMinutes;
        if(hour<10){
            currentHours="0"+hour;
        }else{
            currentHours=String.valueOf(hour);
        }
        if(minute<10){
            currentMinutes="0"+minute;
        }else{
            currentMinutes=String.valueOf(minute);
        }
        currentSystemTime=(currentHours + ":" + currentMinutes);
        java.time.LocalTime time = java.time.LocalTime.parse(currentSystemTime);
        int startHrs = Integer.parseInt(startMinSec[0]);
        int startMin = Integer.parseInt(startMinSec[1]);
        int endHrs = Integer.parseInt(endMinSec[0]);
        int endMin = Integer.parseInt(endMinSec[1]);
        return isTimeInBetween(time, java.time.LocalTime.of(startHrs, startMin), java.time.LocalTime.of(endHrs, endMin));
    }

    private static boolean isTimeInBetween(java.time.LocalTime candidate, java.time.LocalTime start, java.time.LocalTime end) {
        return !candidate.isBefore(start) && !candidate.isAfter(end);

    }

    public static boolean isBetween(java.time.LocalTime candidate, java.time.LocalTime start, java.time.LocalTime end) {
        return candidate.isBefore(start) && !candidate.isAfter(end);
    }


    public static boolean compareTime(String beginTime, String endTime){
        boolean status = false;
        ZoneId zone = ZoneId.of( "Asia/Kolkata" ) ;
        java.time.LocalTime now = java.time.LocalTime.now(zone);  // Explicitly specify the desired/expected time zone.
        /* LocalTime fromTime = LocalTime.parse( beginTime);
       LocalTime toTime = LocalTime.parse( endTime);
        if(now.isAfter(fromTime) && now.isBefore(toTime))
            return true;
        else
            return false;*/
        String[] startMinSec = beginTime.split(":");
        String[] endMinSec = endTime.split(":");
        int hour = now.getHour();
        int minute = now.getMinute();
        String currentSystemTime;
        String currentHours;
        String currentMinutes;
        if(hour<10){
            currentHours="0"+hour;
        }else{
            currentHours=String.valueOf(hour);
        }
        if(minute<10){
            currentMinutes="0"+minute;
        }else{
            currentMinutes=String.valueOf(minute);
        }
        currentSystemTime=(currentHours + ":" + currentMinutes);
        java.time.LocalTime time = java.time.LocalTime.parse(currentSystemTime);
        int startHrs = Integer.parseInt(startMinSec[0]);
        int startMin = Integer.parseInt(startMinSec[1]);
        int endHrs = Integer.parseInt(endMinSec[0]);
        int endMin = Integer.parseInt(endMinSec[1]);
        java.time.LocalTime startTime = java.time.LocalTime.of(startHrs, startMin);
        java.time.LocalTime end = java.time.LocalTime.of(endHrs, endMin);
        if (time.isBefore(startTime)){
            status = startTime.isBefore(end) ? true : false;
        }
        return status;
    }


    public static java.time.LocalDateTime getDateByToTime(String endTime) {
        ZoneId zone = ZoneId.of( "Asia/Kolkata" ) ;
        java.time.LocalTime now = java.time.LocalTime.now(zone);  // Explicitly specify the desired/expected time zone.
        /* LocalTime fromTime = LocalTime.parse( beginTime);
       LocalTime toTime = LocalTime.parse( endTime);
        if(now.isAfter(fromTime) && now.isBefore(toTime))
            return true;
        else
           return false;*/

        String[] endMinSec = endTime.split(":");
        int endHrs = Integer.parseInt(endMinSec[0]);
        int endMin = Integer.parseInt(endMinSec[1]);
        java.time.LocalTime end = java.time.LocalTime.of(endHrs, endMin);
        java.time.LocalTime time = end.plusMinutes(5);
        java.time.LocalDateTime localDateTime = java.time.LocalDateTime.of(java.time.LocalDate.now(), time);
        return localDateTime;
    }

    public static String getQuaterFromDate(Date date){
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;

        if (firstQtr.contains(month)){
            return "QUARTER1";
        } else if (secondQtr.contains(month)){
            return "QUARTER2";
        } else if (thirdQtr.contains(month)){
            return "QUARTER3";
        } else {
            return "QUARTER4";
        }
    }
}
