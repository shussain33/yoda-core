package com.softcell.utils;



public class DigitallySignRequest {
		
	public  static String signRequestString(String request,boolean includeKeyInfo) throws Exception{
//		char[] pwd= "public".toCharArray();
		DigitalSigner digitalSignator = new DigitalSigner("/tmp/ANKUR HANDA.pfx", /*ReaderJob.kycConfigProp.getProperty("digiSignKeystorePwd")*/"Cs1234##".toCharArray(), "le-1fdbb9b3-5988-41b4-a49c-af902bf0b906");
		String signXML = digitalSignator.signXML(request, true, false);
		return signXML;
		/*digiSignKeyAlias=le-04f345ec-ca97-461a-8d18-961ea1e61b01
				digiSignKeystorePwd=Cs1234##*/
	}

	public  static String signRequestStringWithHsm(String request,boolean includeKeyInfo) throws Exception{
//		char[] pwd= "public".toCharArray();
		DigitalSigner digitalSignator = new DigitalSigner(3, "".toCharArray());
		String signXML = digitalSignator.signXML(request, true, true);
		return signXML;
		/*digiSignKeyAlias=le-04f345ec-ca97-461a-8d18-961ea1e61b01
				digiSignKeystorePwd=Cs1234##*/
	}

}
