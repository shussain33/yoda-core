package com.softcell.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by prateek on 18/3/17.
 */
public class GnGFileUtils {
    private static Logger logger = LoggerFactory.getLogger(GnGFileUtils.class);
    public static byte[] compressBytes(String fileName, byte[] input){

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipfile = new ZipOutputStream(bos);
        ZipEntry zipentry = new ZipEntry(fileName);
        try {

            zipfile.putNextEntry(zipentry);

            zipfile.write(input);

            zipfile.close();

        } catch(IOException e) {
            logger.error("{}",e.getStackTrace());
        }

        return bos.toByteArray();
    }

}
