/**
 * ChmbaseERespType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ChmbaseERespType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ChmbaseERespType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ChmbaseERespType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for DATE_OF_REQUEST
     */
    protected java.lang.String localDATE_OF_REQUEST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_REQUESTTracker = false;

    /**
     * field for PREPARED_FOR
     */
    protected java.lang.String localPREPARED_FOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FORTracker = false;

    /**
     * field for PREPARED_FOR_ID
     */
    protected java.lang.String localPREPARED_FOR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FOR_IDTracker = false;

    /**
     * field for DATE_OF_ISSUE
     */
    protected java.lang.String localDATE_OF_ISSUE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_ISSUETracker = false;

    /**
     * field for REPORT_ID
     */
    protected java.lang.String localREPORT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORT_IDTracker = false;

    /**
     * field for BATCH_ID
     */
    protected java.lang.String localBATCH_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBATCH_IDTracker = false;

    /**
     * field for STATUS
     */
    protected java.lang.String localSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUSTracker = false;

    /**
     * field for ERROR
     */
    protected java.lang.String localERROR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERRORTracker = false;

    /**
     * field for NAME
     */
    protected java.lang.String localNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAMETracker = false;

    /**
     * field for AKA
     */
    protected java.lang.String localAKA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAKATracker = false;

    /**
     * field for SPOUSE
     */
    protected java.lang.String localSPOUSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSPOUSETracker = false;

    /**
     * field for FATHER
     */
    protected java.lang.String localFATHER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFATHERTracker = false;

    /**
     * field for MOTHER
     */
    protected java.lang.String localMOTHER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOTHERTracker = false;

    /**
     * field for DOB
     */
    protected java.lang.String localDOB;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOBTracker = false;

    /**
     * field for AGE
     */
    protected java.lang.String localAGE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGETracker = false;

    /**
     * field for AGE_AS_ON
     */
    protected java.lang.String localAGE_AS_ON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_AS_ONTracker = false;

    /**
     * field for RATION_CARD
     */
    protected java.lang.String localRATION_CARD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARDTracker = false;

    /**
     * field for PASSPORT
     */
    protected java.lang.String localPASSPORT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORTTracker = false;

    /**
     * field for VOTER_ID
     */
    protected java.lang.String localVOTER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_IDTracker = false;

    /**
     * field for DRIVING_LICENSE_NO
     */
    protected java.lang.String localDRIVING_LICENSE_NO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVING_LICENSE_NOTracker = false;

    /**
     * field for PAN
     */
    protected java.lang.String localPAN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPANTracker = false;

    /**
     * field for GENDER
     */
    protected java.lang.String localGENDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDERTracker = false;

    /**
     * field for OWNERSHIP
     */
    protected java.lang.String localOWNERSHIP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIPTracker = false;

    /**
     * field for ADDRESS_1
     */
    protected java.lang.String localADDRESS_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_1Tracker = false;

    /**
     * field for ADDRESS_2
     */
    protected java.lang.String localADDRESS_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_2Tracker = false;

    /**
     * field for ADDRESS_3
     */
    protected java.lang.String localADDRESS_3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_3Tracker = false;

    /**
     * field for PHONE_1
     */
    protected java.lang.String localPHONE_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_1Tracker = false;

    /**
     * field for PHONE_2
     */
    protected java.lang.String localPHONE_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_2Tracker = false;

    /**
     * field for PHONE_3
     */
    protected java.lang.String localPHONE_3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_3Tracker = false;

    /**
     * field for EMAIL_1
     */
    protected java.lang.String localEMAIL_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_1Tracker = false;

    /**
     * field for EMAIL_2
     */
    protected java.lang.String localEMAIL_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_2Tracker = false;

    /**
     * field for BRANCH
     */
    protected java.lang.String localBRANCH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBRANCHTracker = false;

    /**
     * field for KENDRA
     */
    protected java.lang.String localKENDRA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKENDRATracker = false;

    /**
     * field for MBR_ID
     */
    protected java.lang.String localMBR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMBR_IDTracker = false;

    /**
     * field for LOS_APP_ID
     */
    protected java.lang.String localLOS_APP_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOS_APP_IDTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYPTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP_DESC
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP_DESC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYP_DESCTracker = false;

    /**
     * field for CREDIT_INQUIRY_STAGE
     */
    protected java.lang.String localCREDIT_INQUIRY_STAGE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_INQUIRY_STAGETracker = false;

    /**
     * field for CREDT_RPT_ID
     */
    protected java.lang.String localCREDT_RPT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_IDTracker = false;

    /**
     * field for CREDT_REQ_TYP
     */
    protected java.lang.String localCREDT_REQ_TYP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_REQ_TYPTracker = false;

    /**
     * field for CREDT_RPT_TRN_DT_TM
     */
    protected java.lang.String localCREDT_RPT_TRN_DT_TM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_TRN_DT_TMTracker = false;

    /**
     * field for AC_OPEN_DT
     */
    protected java.lang.String localAC_OPEN_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAC_OPEN_DTTracker = false;

    /**
     * field for LOAN_AMOUNT
     */
    protected java.lang.String localLOAN_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOAN_AMOUNTTracker = false;

    /**
     * field for ENTITY_ID
     */
    protected java.lang.String localENTITY_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENTITY_IDTracker = false;

    /**
     * field for RESPONSE_TYPE
     */
    protected java.lang.String localRESPONSE_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESPONSE_TYPETracker = false;

    /**
     * field for ACK_CODE
     */
    protected java.lang.String localACK_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACK_CODETracker = false;

    /**
     * field for ACK_DESCRIPTION
     */
    protected java.lang.String localACK_DESCRIPTION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACK_DESCRIPTIONTracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isDATE_OF_REQUESTSpecified() {
        return localDATE_OF_REQUESTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_REQUEST() {
        return localDATE_OF_REQUEST;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_REQUEST
     */
    public void setDATE_OF_REQUEST(java.lang.String param) {
        localDATE_OF_REQUESTTracker = param != null;

        this.localDATE_OF_REQUEST = param;
    }

    public boolean isPREPARED_FORSpecified() {
        return localPREPARED_FORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR() {
        return localPREPARED_FOR;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR
     */
    public void setPREPARED_FOR(java.lang.String param) {
        localPREPARED_FORTracker = param != null;

        this.localPREPARED_FOR = param;
    }

    public boolean isPREPARED_FOR_IDSpecified() {
        return localPREPARED_FOR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR_ID() {
        return localPREPARED_FOR_ID;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR_ID
     */
    public void setPREPARED_FOR_ID(java.lang.String param) {
        localPREPARED_FOR_IDTracker = param != null;

        this.localPREPARED_FOR_ID = param;
    }

    public boolean isDATE_OF_ISSUESpecified() {
        return localDATE_OF_ISSUETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_ISSUE() {
        return localDATE_OF_ISSUE;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_ISSUE
     */
    public void setDATE_OF_ISSUE(java.lang.String param) {
        localDATE_OF_ISSUETracker = param != null;

        this.localDATE_OF_ISSUE = param;
    }

    public boolean isREPORT_IDSpecified() {
        return localREPORT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORT_ID() {
        return localREPORT_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORT_ID
     */
    public void setREPORT_ID(java.lang.String param) {
        localREPORT_IDTracker = param != null;

        this.localREPORT_ID = param;
    }

    public boolean isBATCH_IDSpecified() {
        return localBATCH_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBATCH_ID() {
        return localBATCH_ID;
    }

    /**
     * Auto generated setter method
     * @param param BATCH_ID
     */
    public void setBATCH_ID(java.lang.String param) {
        localBATCH_IDTracker = param != null;

        this.localBATCH_ID = param;
    }

    public boolean isSTATUSSpecified() {
        return localSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS() {
        return localSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param STATUS
     */
    public void setSTATUS(java.lang.String param) {
        localSTATUSTracker = param != null;

        this.localSTATUS = param;
    }

    public boolean isERRORSpecified() {
        return localERRORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERROR() {
        return localERROR;
    }

    /**
     * Auto generated setter method
     * @param param ERROR
     */
    public void setERROR(java.lang.String param) {
        localERRORTracker = param != null;

        this.localERROR = param;
    }

    public boolean isNAMESpecified() {
        return localNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME() {
        return localNAME;
    }

    /**
     * Auto generated setter method
     * @param param NAME
     */
    public void setNAME(java.lang.String param) {
        localNAMETracker = param != null;

        this.localNAME = param;
    }

    public boolean isAKASpecified() {
        return localAKATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAKA() {
        return localAKA;
    }

    /**
     * Auto generated setter method
     * @param param AKA
     */
    public void setAKA(java.lang.String param) {
        localAKATracker = param != null;

        this.localAKA = param;
    }

    public boolean isSPOUSESpecified() {
        return localSPOUSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSPOUSE() {
        return localSPOUSE;
    }

    /**
     * Auto generated setter method
     * @param param SPOUSE
     */
    public void setSPOUSE(java.lang.String param) {
        localSPOUSETracker = param != null;

        this.localSPOUSE = param;
    }

    public boolean isFATHERSpecified() {
        return localFATHERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFATHER() {
        return localFATHER;
    }

    /**
     * Auto generated setter method
     * @param param FATHER
     */
    public void setFATHER(java.lang.String param) {
        localFATHERTracker = param != null;

        this.localFATHER = param;
    }

    public boolean isMOTHERSpecified() {
        return localMOTHERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOTHER() {
        return localMOTHER;
    }

    /**
     * Auto generated setter method
     * @param param MOTHER
     */
    public void setMOTHER(java.lang.String param) {
        localMOTHERTracker = param != null;

        this.localMOTHER = param;
    }

    public boolean isDOBSpecified() {
        return localDOBTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB() {
        return localDOB;
    }

    /**
     * Auto generated setter method
     * @param param DOB
     */
    public void setDOB(java.lang.String param) {
        localDOBTracker = param != null;

        this.localDOB = param;
    }

    public boolean isAGESpecified() {
        return localAGETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE() {
        return localAGE;
    }

    /**
     * Auto generated setter method
     * @param param AGE
     */
    public void setAGE(java.lang.String param) {
        localAGETracker = param != null;

        this.localAGE = param;
    }

    public boolean isAGE_AS_ONSpecified() {
        return localAGE_AS_ONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_AS_ON() {
        return localAGE_AS_ON;
    }

    /**
     * Auto generated setter method
     * @param param AGE_AS_ON
     */
    public void setAGE_AS_ON(java.lang.String param) {
        localAGE_AS_ONTracker = param != null;

        this.localAGE_AS_ON = param;
    }

    public boolean isRATION_CARDSpecified() {
        return localRATION_CARDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD() {
        return localRATION_CARD;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD
     */
    public void setRATION_CARD(java.lang.String param) {
        localRATION_CARDTracker = param != null;

        this.localRATION_CARD = param;
    }

    public boolean isPASSPORTSpecified() {
        return localPASSPORTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT() {
        return localPASSPORT;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT
     */
    public void setPASSPORT(java.lang.String param) {
        localPASSPORTTracker = param != null;

        this.localPASSPORT = param;
    }

    public boolean isVOTER_IDSpecified() {
        return localVOTER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID() {
        return localVOTER_ID;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID
     */
    public void setVOTER_ID(java.lang.String param) {
        localVOTER_IDTracker = param != null;

        this.localVOTER_ID = param;
    }

    public boolean isDRIVING_LICENSE_NOSpecified() {
        return localDRIVING_LICENSE_NOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVING_LICENSE_NO() {
        return localDRIVING_LICENSE_NO;
    }

    /**
     * Auto generated setter method
     * @param param DRIVING_LICENSE_NO
     */
    public void setDRIVING_LICENSE_NO(java.lang.String param) {
        localDRIVING_LICENSE_NOTracker = param != null;

        this.localDRIVING_LICENSE_NO = param;
    }

    public boolean isPANSpecified() {
        return localPANTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN() {
        return localPAN;
    }

    /**
     * Auto generated setter method
     * @param param PAN
     */
    public void setPAN(java.lang.String param) {
        localPANTracker = param != null;

        this.localPAN = param;
    }

    public boolean isGENDERSpecified() {
        return localGENDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER() {
        return localGENDER;
    }

    /**
     * Auto generated setter method
     * @param param GENDER
     */
    public void setGENDER(java.lang.String param) {
        localGENDERTracker = param != null;

        this.localGENDER = param;
    }

    public boolean isOWNERSHIPSpecified() {
        return localOWNERSHIPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIP() {
        return localOWNERSHIP;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIP
     */
    public void setOWNERSHIP(java.lang.String param) {
        localOWNERSHIPTracker = param != null;

        this.localOWNERSHIP = param;
    }

    public boolean isADDRESS_1Specified() {
        return localADDRESS_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_1() {
        return localADDRESS_1;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_1
     */
    public void setADDRESS_1(java.lang.String param) {
        localADDRESS_1Tracker = param != null;

        this.localADDRESS_1 = param;
    }

    public boolean isADDRESS_2Specified() {
        return localADDRESS_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_2() {
        return localADDRESS_2;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_2
     */
    public void setADDRESS_2(java.lang.String param) {
        localADDRESS_2Tracker = param != null;

        this.localADDRESS_2 = param;
    }

    public boolean isADDRESS_3Specified() {
        return localADDRESS_3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_3() {
        return localADDRESS_3;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_3
     */
    public void setADDRESS_3(java.lang.String param) {
        localADDRESS_3Tracker = param != null;

        this.localADDRESS_3 = param;
    }

    public boolean isPHONE_1Specified() {
        return localPHONE_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_1() {
        return localPHONE_1;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_1
     */
    public void setPHONE_1(java.lang.String param) {
        localPHONE_1Tracker = param != null;

        this.localPHONE_1 = param;
    }

    public boolean isPHONE_2Specified() {
        return localPHONE_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_2() {
        return localPHONE_2;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_2
     */
    public void setPHONE_2(java.lang.String param) {
        localPHONE_2Tracker = param != null;

        this.localPHONE_2 = param;
    }

    public boolean isPHONE_3Specified() {
        return localPHONE_3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_3() {
        return localPHONE_3;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_3
     */
    public void setPHONE_3(java.lang.String param) {
        localPHONE_3Tracker = param != null;

        this.localPHONE_3 = param;
    }

    public boolean isEMAIL_1Specified() {
        return localEMAIL_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_1() {
        return localEMAIL_1;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_1
     */
    public void setEMAIL_1(java.lang.String param) {
        localEMAIL_1Tracker = param != null;

        this.localEMAIL_1 = param;
    }

    public boolean isEMAIL_2Specified() {
        return localEMAIL_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_2() {
        return localEMAIL_2;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_2
     */
    public void setEMAIL_2(java.lang.String param) {
        localEMAIL_2Tracker = param != null;

        this.localEMAIL_2 = param;
    }

    public boolean isBRANCHSpecified() {
        return localBRANCHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBRANCH() {
        return localBRANCH;
    }

    /**
     * Auto generated setter method
     * @param param BRANCH
     */
    public void setBRANCH(java.lang.String param) {
        localBRANCHTracker = param != null;

        this.localBRANCH = param;
    }

    public boolean isKENDRASpecified() {
        return localKENDRATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getKENDRA() {
        return localKENDRA;
    }

    /**
     * Auto generated setter method
     * @param param KENDRA
     */
    public void setKENDRA(java.lang.String param) {
        localKENDRATracker = param != null;

        this.localKENDRA = param;
    }

    public boolean isMBR_IDSpecified() {
        return localMBR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMBR_ID() {
        return localMBR_ID;
    }

    /**
     * Auto generated setter method
     * @param param MBR_ID
     */
    public void setMBR_ID(java.lang.String param) {
        localMBR_IDTracker = param != null;

        this.localMBR_ID = param;
    }

    public boolean isLOS_APP_IDSpecified() {
        return localLOS_APP_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOS_APP_ID() {
        return localLOS_APP_ID;
    }

    /**
     * Auto generated setter method
     * @param param LOS_APP_ID
     */
    public void setLOS_APP_ID(java.lang.String param) {
        localLOS_APP_IDTracker = param != null;

        this.localLOS_APP_ID = param;
    }

    public boolean isCREDT_INQ_PURPS_TYPSpecified() {
        return localCREDT_INQ_PURPS_TYPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP() {
        return localCREDT_INQ_PURPS_TYP;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP
     */
    public void setCREDT_INQ_PURPS_TYP(java.lang.String param) {
        localCREDT_INQ_PURPS_TYPTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP = param;
    }

    public boolean isCREDT_INQ_PURPS_TYP_DESCSpecified() {
        return localCREDT_INQ_PURPS_TYP_DESCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP_DESC() {
        return localCREDT_INQ_PURPS_TYP_DESC;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP_DESC
     */
    public void setCREDT_INQ_PURPS_TYP_DESC(java.lang.String param) {
        localCREDT_INQ_PURPS_TYP_DESCTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP_DESC = param;
    }

    public boolean isCREDIT_INQUIRY_STAGESpecified() {
        return localCREDIT_INQUIRY_STAGETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_INQUIRY_STAGE() {
        return localCREDIT_INQUIRY_STAGE;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_INQUIRY_STAGE
     */
    public void setCREDIT_INQUIRY_STAGE(java.lang.String param) {
        localCREDIT_INQUIRY_STAGETracker = param != null;

        this.localCREDIT_INQUIRY_STAGE = param;
    }

    public boolean isCREDT_RPT_IDSpecified() {
        return localCREDT_RPT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_ID() {
        return localCREDT_RPT_ID;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_ID
     */
    public void setCREDT_RPT_ID(java.lang.String param) {
        localCREDT_RPT_IDTracker = param != null;

        this.localCREDT_RPT_ID = param;
    }

    public boolean isCREDT_REQ_TYPSpecified() {
        return localCREDT_REQ_TYPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_REQ_TYP() {
        return localCREDT_REQ_TYP;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_REQ_TYP
     */
    public void setCREDT_REQ_TYP(java.lang.String param) {
        localCREDT_REQ_TYPTracker = param != null;

        this.localCREDT_REQ_TYP = param;
    }

    public boolean isCREDT_RPT_TRN_DT_TMSpecified() {
        return localCREDT_RPT_TRN_DT_TMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_TRN_DT_TM() {
        return localCREDT_RPT_TRN_DT_TM;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_TRN_DT_TM
     */
    public void setCREDT_RPT_TRN_DT_TM(java.lang.String param) {
        localCREDT_RPT_TRN_DT_TMTracker = param != null;

        this.localCREDT_RPT_TRN_DT_TM = param;
    }

    public boolean isAC_OPEN_DTSpecified() {
        return localAC_OPEN_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAC_OPEN_DT() {
        return localAC_OPEN_DT;
    }

    /**
     * Auto generated setter method
     * @param param AC_OPEN_DT
     */
    public void setAC_OPEN_DT(java.lang.String param) {
        localAC_OPEN_DTTracker = param != null;

        this.localAC_OPEN_DT = param;
    }

    public boolean isLOAN_AMOUNTSpecified() {
        return localLOAN_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOAN_AMOUNT() {
        return localLOAN_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param LOAN_AMOUNT
     */
    public void setLOAN_AMOUNT(java.lang.String param) {
        localLOAN_AMOUNTTracker = param != null;

        this.localLOAN_AMOUNT = param;
    }

    public boolean isENTITY_IDSpecified() {
        return localENTITY_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENTITY_ID() {
        return localENTITY_ID;
    }

    /**
     * Auto generated setter method
     * @param param ENTITY_ID
     */
    public void setENTITY_ID(java.lang.String param) {
        localENTITY_IDTracker = param != null;

        this.localENTITY_ID = param;
    }

    public boolean isRESPONSE_TYPESpecified() {
        return localRESPONSE_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESPONSE_TYPE() {
        return localRESPONSE_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param RESPONSE_TYPE
     */
    public void setRESPONSE_TYPE(java.lang.String param) {
        localRESPONSE_TYPETracker = param != null;

        this.localRESPONSE_TYPE = param;
    }

    public boolean isACK_CODESpecified() {
        return localACK_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACK_CODE() {
        return localACK_CODE;
    }

    /**
     * Auto generated setter method
     * @param param ACK_CODE
     */
    public void setACK_CODE(java.lang.String param) {
        localACK_CODETracker = param != null;

        this.localACK_CODE = param;
    }

    public boolean isACK_DESCRIPTIONSpecified() {
        return localACK_DESCRIPTIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACK_DESCRIPTION() {
        return localACK_DESCRIPTION;
    }

    /**
     * Auto generated setter method
     * @param param ACK_DESCRIPTION
     */
    public void setACK_DESCRIPTION(java.lang.String param) {
        localACK_DESCRIPTIONTracker = param != null;

        this.localACK_DESCRIPTION = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ChmbaseERespType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ChmbaseERespType", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_REQUESTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_REQUEST", xmlWriter);

            if (localDATE_OF_REQUEST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_REQUEST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_REQUEST);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR", xmlWriter);

            if (localPREPARED_FOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FOR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR_ID", xmlWriter);

            if (localPREPARED_FOR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_ISSUETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_ISSUE", xmlWriter);

            if (localDATE_OF_ISSUE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_ISSUE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_ISSUE);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORT_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORT_ID", xmlWriter);

            if (localREPORT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localBATCH_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BATCH_ID", xmlWriter);

            if (localBATCH_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BATCH_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBATCH_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS", xmlWriter);

            if (localSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localERRORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERROR", xmlWriter);

            if (localERROR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERROR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERROR);
            }

            xmlWriter.writeEndElement();
        }

        if (localNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "NAME", xmlWriter);

            if (localNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localAKATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AKA", xmlWriter);

            if (localAKA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AKA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAKA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSPOUSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SPOUSE", xmlWriter);

            if (localSPOUSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SPOUSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSPOUSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localFATHERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FATHER", xmlWriter);

            if (localFATHER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FATHER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFATHER);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOTHERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MOTHER", xmlWriter);

            if (localMOTHER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOTHER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOTHER);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOBTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB", xmlWriter);

            if (localDOB == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGETracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE", xmlWriter);

            if (localAGE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_AS_ONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_AS_ON", xmlWriter);

            if (localAGE_AS_ON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_AS_ON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_AS_ON);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD", xmlWriter);

            if (localRATION_CARD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT", xmlWriter);

            if (localPASSPORT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID", xmlWriter);

            if (localVOTER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVING_LICENSE_NOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVING_LICENSE_NO", xmlWriter);

            if (localDRIVING_LICENSE_NO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVING_LICENSE_NO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVING_LICENSE_NO);
            }

            xmlWriter.writeEndElement();
        }

        if (localPANTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN", xmlWriter);

            if (localPAN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER", xmlWriter);

            if (localGENDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIP", xmlWriter);

            if (localOWNERSHIP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIP);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_1", xmlWriter);

            if (localADDRESS_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_2", xmlWriter);

            if (localADDRESS_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_3", xmlWriter);

            if (localADDRESS_3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_3);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_1", xmlWriter);

            if (localPHONE_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_2", xmlWriter);

            if (localPHONE_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_3", xmlWriter);

            if (localPHONE_3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_3);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_1", xmlWriter);

            if (localEMAIL_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_2", xmlWriter);

            if (localEMAIL_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localBRANCHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BRANCH", xmlWriter);

            if (localBRANCH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BRANCH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBRANCH);
            }

            xmlWriter.writeEndElement();
        }

        if (localKENDRATracker) {
            namespace = "";
            writeStartElement(null, namespace, "KENDRA", xmlWriter);

            if (localKENDRA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "KENDRA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localKENDRA);
            }

            xmlWriter.writeEndElement();
        }

        if (localMBR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MBR_ID", xmlWriter);

            if (localMBR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MBR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMBR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOS_APP_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOS_APP_ID", xmlWriter);

            if (localLOS_APP_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOS_APP_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOS_APP_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP", xmlWriter);

            if (localCREDT_INQ_PURPS_TYP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYP_DESCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP_DESC",
                xmlWriter);

            if (localCREDT_INQ_PURPS_TYP_DESC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP_DESC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP_DESC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_INQUIRY_STAGETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_INQUIRY_STAGE", xmlWriter);

            if (localCREDIT_INQUIRY_STAGE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_INQUIRY_STAGE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_INQUIRY_STAGE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_ID", xmlWriter);

            if (localCREDT_RPT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_REQ_TYPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_REQ_TYP", xmlWriter);

            if (localCREDT_REQ_TYP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_REQ_TYP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_REQ_TYP);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_TRN_DT_TMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_TRN_DT_TM", xmlWriter);

            if (localCREDT_RPT_TRN_DT_TM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_TRN_DT_TM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_TRN_DT_TM);
            }

            xmlWriter.writeEndElement();
        }

        if (localAC_OPEN_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AC_OPEN_DT", xmlWriter);

            if (localAC_OPEN_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AC_OPEN_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAC_OPEN_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOAN_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOAN_AMOUNT", xmlWriter);

            if (localLOAN_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOAN_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOAN_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localENTITY_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENTITY_ID", xmlWriter);

            if (localENTITY_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENTITY_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENTITY_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESPONSE_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESPONSE_TYPE", xmlWriter);

            if (localRESPONSE_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESPONSE_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESPONSE_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACK_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACK_CODE", xmlWriter);

            if (localACK_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACK_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACK_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACK_DESCRIPTIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACK_DESCRIPTION", xmlWriter);

            if (localACK_DESCRIPTION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACK_DESCRIPTION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACK_DESCRIPTION);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ChmbaseERespType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ChmbaseERespType object = new ChmbaseERespType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ChmbaseERespType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ChmbaseERespType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_REQUEST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_REQUEST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_ISSUE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_ISSUE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORT_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BATCH_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BATCH_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BATCH_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBATCH_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERROR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERROR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERROR" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERROR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AKA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AKA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AKA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAKA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SPOUSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SPOUSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SPOUSE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSPOUSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FATHER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FATHER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FATHER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFATHER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MOTHER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOTHER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOTHER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOTHER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_AS_ON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_AS_ON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_AS_ON" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_AS_ON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATION_CARD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATION_CARD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTER_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DRIVING_LICENSE_NO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DRIVING_LICENSE_NO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVING_LICENSE_NO" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVING_LICENSE_NO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNERSHIP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNERSHIP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIP" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_1" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_2" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_3" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_1" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_2" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_3" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_1" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_2" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BRANCH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BRANCH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BRANCH" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBRANCH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "KENDRA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "KENDRA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "KENDRA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setKENDRA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MBR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MBR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MBR_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMBR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOS_APP_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOS_APP_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOS_APP_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOS_APP_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_INQ_PURPS_TYP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_INQ_PURPS_TYP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP_DESC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP_DESC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDIT_INQUIRY_STAGE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDIT_INQUIRY_STAGE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_INQUIRY_STAGE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_INQUIRY_STAGE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_REQ_TYP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_REQ_TYP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_RPT_TRN_DT_TM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_RPT_TRN_DT_TM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_TRN_DT_TM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_TRN_DT_TM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AC_OPEN_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AC_OPEN_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AC_OPEN_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAC_OPEN_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOAN_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOAN_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENTITY_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENTITY_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENTITY_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENTITY_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RESPONSE_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RESPONSE_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESPONSE_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESPONSE_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACK_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACK_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACK_CODE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACK_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACK_DESCRIPTION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACK_DESCRIPTION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACK_DESCRIPTION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACK_DESCRIPTION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
