/**
 * ChmAorSRespType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ChmAorSRespType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ChmAorSRespType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ChmAorSRespType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for DATE_OF_REQUEST
     */
    protected java.lang.String localDATE_OF_REQUEST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_REQUESTTracker = false;

    /**
     * field for PREPARED_FOR
     */
    protected java.lang.String localPREPARED_FOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FORTracker = false;

    /**
     * field for PREPARED_FOR_ID
     */
    protected java.lang.String localPREPARED_FOR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FOR_IDTracker = false;

    /**
     * field for DATE_OF_ISSUE
     */
    protected java.lang.String localDATE_OF_ISSUE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_ISSUETracker = false;

    /**
     * field for REPORT_ID
     */
    protected java.lang.String localREPORT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORT_IDTracker = false;

    /**
     * field for NAME_IQ
     */
    protected java.lang.String localNAME_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAME_IQTracker = false;

    /**
     * field for SPOUSE_IQ
     */
    protected java.lang.String localSPOUSE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSPOUSE_IQTracker = false;

    /**
     * field for FATHER_IQ
     */
    protected java.lang.String localFATHER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFATHER_IQTracker = false;

    /**
     * field for MOTHER_IQ
     */
    protected java.lang.String localMOTHER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOTHER_IQTracker = false;

    /**
     * field for DOB_IQ
     */
    protected java.lang.String localDOB_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOB_IQTracker = false;

    /**
     * field for AGE_IQ
     */
    protected java.lang.String localAGE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_IQTracker = false;

    /**
     * field for AGE_AS_ON_IQ
     */
    protected java.lang.String localAGE_AS_ON_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_AS_ON_IQTracker = false;

    /**
     * field for GENDER_IQ
     */
    protected java.lang.String localGENDER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDER_IQTracker = false;

    /**
     * field for PHONE_1_IQ
     */
    protected java.lang.String localPHONE_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_1_IQTracker = false;

    /**
     * field for PHONE_2_IQ
     */
    protected java.lang.String localPHONE_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_2_IQTracker = false;

    /**
     * field for PHONE_3_IQ
     */
    protected java.lang.String localPHONE_3_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_3_IQTracker = false;

    /**
     * field for ADDRESS_1_IQ
     */
    protected java.lang.String localADDRESS_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_1_IQTracker = false;

    /**
     * field for ADDRESS_2_IQ
     */
    protected java.lang.String localADDRESS_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_2_IQTracker = false;

    /**
     * field for REL_TYP_1_IQ
     */
    protected java.lang.String localREL_TYP_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_TYP_1_IQTracker = false;

    /**
     * field for REL_NM_1_IQ
     */
    protected java.lang.String localREL_NM_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_NM_1_IQTracker = false;

    /**
     * field for REL_TYP_2_IQ
     */
    protected java.lang.String localREL_TYP_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_TYP_2_IQTracker = false;

    /**
     * field for REL_NM_2_IQ
     */
    protected java.lang.String localREL_NM_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_NM_2_IQTracker = false;

    /**
     * field for REL_TYP_3_IQ
     */
    protected java.lang.String localREL_TYP_3_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_TYP_3_IQTracker = false;

    /**
     * field for REL_NM_3_IQ
     */
    protected java.lang.String localREL_NM_3_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_NM_3_IQTracker = false;

    /**
     * field for REL_TYP_4_IQ
     */
    protected java.lang.String localREL_TYP_4_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_TYP_4_IQTracker = false;

    /**
     * field for REL_NM_4_IQ
     */
    protected java.lang.String localREL_NM_4_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREL_NM_4_IQTracker = false;

    /**
     * field for ID_TYPE_1_IQ
     */
    protected java.lang.String localID_TYPE_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_TYPE_1_IQTracker = false;

    /**
     * field for ID_VALUE_1_IQ
     */
    protected java.lang.String localID_VALUE_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_VALUE_1_IQTracker = false;

    /**
     * field for ID_TYPE_2_IQ
     */
    protected java.lang.String localID_TYPE_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_TYPE_2_IQTracker = false;

    /**
     * field for ID_VALUE_2_IQ
     */
    protected java.lang.String localID_VALUE_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_VALUE_2_IQTracker = false;

    /**
     * field for RATION_CARD_IQ
     */
    protected java.lang.String localRATION_CARD_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_IQTracker = false;

    /**
     * field for VOTERS_ID_IQ
     */
    protected java.lang.String localVOTERS_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTERS_ID_IQTracker = false;

    /**
     * field for DRIVING_LICENCE_NO_IQ
     */
    protected java.lang.String localDRIVING_LICENCE_NO_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVING_LICENCE_NO_IQTracker = false;

    /**
     * field for PAN_IQ
     */
    protected java.lang.String localPAN_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_IQTracker = false;

    /**
     * field for PASSPORT_IQ
     */
    protected java.lang.String localPASSPORT_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_IQTracker = false;

    /**
     * field for OTHER_ID_IQ
     */
    protected java.lang.String localOTHER_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOTHER_ID_IQTracker = false;

    /**
     * field for BRANCH_IQ
     */
    protected java.lang.String localBRANCH_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBRANCH_IQTracker = false;

    /**
     * field for KENDRA_IQ
     */
    protected java.lang.String localKENDRA_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKENDRA_IQTracker = false;

    /**
     * field for MBR_ID_IQ
     */
    protected java.lang.String localMBR_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMBR_ID_IQTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYPTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP_DESC
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP_DESC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYP_DESCTracker = false;

    /**
     * field for CREDIT_INQUIRY_STAGE
     */
    protected java.lang.String localCREDIT_INQUIRY_STAGE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_INQUIRY_STAGETracker = false;

    /**
     * field for CREDT_RPT_ID
     */
    protected java.lang.String localCREDT_RPT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_IDTracker = false;

    /**
     * field for CREDT_REQ_TYP
     */
    protected java.lang.String localCREDT_REQ_TYP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_REQ_TYPTracker = false;

    /**
     * field for CREDT_RPT_TRN_DT_TM
     */
    protected java.lang.String localCREDT_RPT_TRN_DT_TM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_TRN_DT_TMTracker = false;

    /**
     * field for AC_OPEN_DT
     */
    protected java.lang.String localAC_OPEN_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAC_OPEN_DTTracker = false;

    /**
     * field for LOAN_AMOUNT
     */
    protected java.lang.String localLOAN_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOAN_AMOUNTTracker = false;

    /**
     * field for ENTITY_ID
     */
    protected java.lang.String localENTITY_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENTITY_IDTracker = false;

    /**
     * field for IND_STATUS_PR
     */
    protected java.lang.String localIND_STATUS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_STATUS_PRTracker = false;

    /**
     * field for IND_NO_OF_DEFAULT_ACCOUNTS_PR
     */
    protected java.lang.String localIND_NO_OF_DEFAULT_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_DEFAULT_ACCOUNTS_PRTracker = false;

    /**
     * field for IND_TOTAL_RESPONSES_PR
     */
    protected java.lang.String localIND_TOTAL_RESPONSES_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TOTAL_RESPONSES_PRTracker = false;

    /**
     * field for IND_NO_OF_CLOSED_ACCOUNTS_PR
     */
    protected java.lang.String localIND_NO_OF_CLOSED_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_CLOSED_ACCOUNTS_PRTracker = false;

    /**
     * field for IND_NO_OF_ACTIVE_ACCOUNTS_PR
     */
    protected java.lang.String localIND_NO_OF_ACTIVE_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_ACTIVE_ACCOUNTS_PRTracker = false;

    /**
     * field for IND_NO_OF_OTHER_MFIS_PR
     */
    protected java.lang.String localIND_NO_OF_OTHER_MFIS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_OTHER_MFIS_PRTracker = false;

    /**
     * field for IND_OWN_MFI_INDECATOR_PR
     */
    protected java.lang.String localIND_OWN_MFI_INDECATOR_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_OWN_MFI_INDECATOR_PRTracker = false;

    /**
     * field for IND_TTL_OWN_DISBURSD_AMT_PR
     */
    protected java.lang.String localIND_TTL_OWN_DISBURSD_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_DISBURSD_AMT_PRTracker = false;

    /**
     * field for IND_TTL_OTHER_DISBURSD_AMT_PR
     */
    protected java.lang.String localIND_TTL_OTHER_DISBURSD_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_DISBURSD_AMT_PRTracker = false;

    /**
     * field for IND_TTL_OWN_CURRENT_BAL_PR
     */
    protected java.lang.String localIND_TTL_OWN_CURRENT_BAL_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_CURRENT_BAL_PRTracker = false;

    /**
     * field for IND_TTL_OTHER_CURRENT_BAL_PR
     */
    protected java.lang.String localIND_TTL_OTHER_CURRENT_BAL_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_CURRENT_BAL_PRTracker = false;

    /**
     * field for IND_TTL_OWN_INSTLMNT_AMT_PR
     */
    protected java.lang.String localIND_TTL_OWN_INSTLMNT_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_INSTLMNT_AMT_PRTracker = false;

    /**
     * field for IND_TTL_OTHER_INSTLMNT_AMT_PR
     */
    protected java.lang.String localIND_TTL_OTHER_INSTLMNT_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_INSTLMNT_AMT_PRTracker = false;

    /**
     * field for IND_MAX_WORST_DELEQUENCY_PR
     */
    protected java.lang.String localIND_MAX_WORST_DELEQUENCY_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MAX_WORST_DELEQUENCY_PRTracker = false;

    /**
     * field for IND_ERRORS_PR
     */
    protected java.lang.String localIND_ERRORS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ERRORS_PRTracker = false;

    /**
     * field for IND_STATUS_SE
     */
    protected java.lang.String localIND_STATUS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_STATUS_SETracker = false;

    /**
     * field for IND_NO_OF_DEFAULT_ACCOUNTS_SE
     */
    protected java.lang.String localIND_NO_OF_DEFAULT_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_DEFAULT_ACCOUNTS_SETracker = false;

    /**
     * field for IND_TOTAL_RESPONSES_SE
     */
    protected java.lang.String localIND_TOTAL_RESPONSES_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TOTAL_RESPONSES_SETracker = false;

    /**
     * field for IND_NO_OF_CLOSED_ACCOUNTS_SE
     */
    protected java.lang.String localIND_NO_OF_CLOSED_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_CLOSED_ACCOUNTS_SETracker = false;

    /**
     * field for IND_NO_OF_ACTIVE_ACCOUNTS_SE
     */
    protected java.lang.String localIND_NO_OF_ACTIVE_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_ACTIVE_ACCOUNTS_SETracker = false;

    /**
     * field for IND_NO_OF_OTHER_MFIS_SE
     */
    protected java.lang.String localIND_NO_OF_OTHER_MFIS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_OTHER_MFIS_SETracker = false;

    /**
     * field for IND_OWN_MFI_INDECATOR_SE
     */
    protected java.lang.String localIND_OWN_MFI_INDECATOR_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_OWN_MFI_INDECATOR_SETracker = false;

    /**
     * field for IND_TTL_OWN_DISBURSD_AMT_SE
     */
    protected java.lang.String localIND_TTL_OWN_DISBURSD_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_DISBURSD_AMT_SETracker = false;

    /**
     * field for IND_TTL_OTHER_DISBURSD_AMT_SE
     */
    protected java.lang.String localIND_TTL_OTHER_DISBURSD_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_DISBURSD_AMT_SETracker = false;

    /**
     * field for IND_TTL_OWN_CURRENT_BAL_SE
     */
    protected java.lang.String localIND_TTL_OWN_CURRENT_BAL_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_CURRENT_BAL_SETracker = false;

    /**
     * field for IND_TTL_OTHER_CURRENT_BAL_SE
     */
    protected java.lang.String localIND_TTL_OTHER_CURRENT_BAL_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_CURRENT_BAL_SETracker = false;

    /**
     * field for IND_TTL_OWN_INSTLMNT_AMT_SE
     */
    protected java.lang.String localIND_TTL_OWN_INSTLMNT_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_INSTLMNT_AMT_SETracker = false;

    /**
     * field for IND_TTL_OTHER_INSTLMNT_AMT_SE
     */
    protected java.lang.String localIND_TTL_OTHER_INSTLMNT_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_INSTLMNT_AMT_SETracker = false;

    /**
     * field for IND_MAX_WORST_DELEQUENCY_SE
     */
    protected java.lang.String localIND_MAX_WORST_DELEQUENCY_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MAX_WORST_DELEQUENCY_SETracker = false;

    /**
     * field for IND_ERRORS_SE
     */
    protected java.lang.String localIND_ERRORS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ERRORS_SETracker = false;

    /**
     * field for IND_STATUS_SM
     */
    protected java.lang.String localIND_STATUS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_STATUS_SMTracker = false;

    /**
     * field for IND_NO_OF_DEFAULT_ACCOUNTS_SM
     */
    protected java.lang.String localIND_NO_OF_DEFAULT_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_DEFAULT_ACCOUNTS_SMTracker = false;

    /**
     * field for IND_TOTAL_RESPONSES_SM
     */
    protected java.lang.String localIND_TOTAL_RESPONSES_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TOTAL_RESPONSES_SMTracker = false;

    /**
     * field for IND_NO_OF_CLOSED_ACCOUNTS_SM
     */
    protected java.lang.String localIND_NO_OF_CLOSED_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_CLOSED_ACCOUNTS_SMTracker = false;

    /**
     * field for IND_NO_OF_ACTIVE_ACCOUNTS_SM
     */
    protected java.lang.String localIND_NO_OF_ACTIVE_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_ACTIVE_ACCOUNTS_SMTracker = false;

    /**
     * field for IND_NO_OF_OTHER_MFIS_SM
     */
    protected java.lang.String localIND_NO_OF_OTHER_MFIS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_OTHER_MFIS_SMTracker = false;

    /**
     * field for IND_OWN_MFI_INDECATOR_SM
     */
    protected java.lang.String localIND_OWN_MFI_INDECATOR_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_OWN_MFI_INDECATOR_SMTracker = false;

    /**
     * field for IND_TTL_OWN_DISBURSD_AMT_SM
     */
    protected java.lang.String localIND_TTL_OWN_DISBURSD_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_DISBURSD_AMT_SMTracker = false;

    /**
     * field for IND_TTL_OTHER_DISBURSD_AMT_SM
     */
    protected java.lang.String localIND_TTL_OTHER_DISBURSD_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_DISBURSD_AMT_SMTracker = false;

    /**
     * field for IND_TTL_OWN_CURRENT_BAL_SM
     */
    protected java.lang.String localIND_TTL_OWN_CURRENT_BAL_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_CURRENT_BAL_SMTracker = false;

    /**
     * field for IND_TTL_OTHER_CURRENT_BAL_SM
     */
    protected java.lang.String localIND_TTL_OTHER_CURRENT_BAL_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_CURRENT_BAL_SMTracker = false;

    /**
     * field for IND_TTL_OWN_INSTLMNT_AMT_SM
     */
    protected java.lang.String localIND_TTL_OWN_INSTLMNT_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OWN_INSTLMNT_AMT_SMTracker = false;

    /**
     * field for IND_TTL_OTHER_INSTLMNT_AMT_SM
     */
    protected java.lang.String localIND_TTL_OTHER_INSTLMNT_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_TTL_OTHER_INSTLMNT_AMT_SMTracker = false;

    /**
     * field for IND_MAX_WORST_DELEQUENCY_SM
     */
    protected java.lang.String localIND_MAX_WORST_DELEQUENCY_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MAX_WORST_DELEQUENCY_SMTracker = false;

    /**
     * field for IND_ERRORS_SM
     */
    protected java.lang.String localIND_ERRORS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ERRORS_SMTracker = false;

    /**
     * field for IND_MATCHED_TYPE
     */
    protected java.lang.String localIND_MATCHED_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MATCHED_TYPETracker = false;

    /**
     * field for IND_MFI
     */
    protected java.lang.String localIND_MFI;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MFITracker = false;

    /**
     * field for IND_MFI_ID
     */
    protected java.lang.String localIND_MFI_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_MFI_IDTracker = false;

    /**
     * field for IND_BRANCH
     */
    protected java.lang.String localIND_BRANCH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_BRANCHTracker = false;

    /**
     * field for IND_KENDRA
     */
    protected java.lang.String localIND_KENDRA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_KENDRATracker = false;

    /**
     * field for IND_NAME
     */
    protected java.lang.String localIND_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NAMETracker = false;

    /**
     * field for IND_SPOUSE
     */
    protected java.lang.String localIND_SPOUSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_SPOUSETracker = false;

    /**
     * field for IND_FATHER
     */
    protected java.lang.String localIND_FATHER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_FATHERTracker = false;

    /**
     * field for IND_CNSMR_MBR_ID
     */
    protected java.lang.String localIND_CNSMR_MBR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_CNSMR_MBR_IDTracker = false;

    /**
     * field for IND_DOB
     */
    protected java.lang.String localIND_DOB;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_DOBTracker = false;

    /**
     * field for IND_AGE
     */
    protected java.lang.String localIND_AGE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_AGETracker = false;

    /**
     * field for IND_AGE_AS_ON
     */
    protected java.lang.String localIND_AGE_AS_ON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_AGE_AS_ONTracker = false;

    /**
     * field for IND_PHONE
     */
    protected java.lang.String localIND_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_PHONETracker = false;

    /**
     * field for IND_ADDRESS_1
     */
    protected java.lang.String localIND_ADDRESS_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ADDRESS_1Tracker = false;

    /**
     * field for IND_ADDRESS_2
     */
    protected java.lang.String localIND_ADDRESS_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ADDRESS_2Tracker = false;

    /**
     * field for IND_REL_TYP_1
     */
    protected java.lang.String localIND_REL_TYP_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_REL_TYP_1Tracker = false;

    /**
     * field for IND_REL_NM_1
     */
    protected java.lang.String localIND_REL_NM_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_REL_NM_1Tracker = false;

    /**
     * field for IND_REL_TYP_2
     */
    protected java.lang.String localIND_REL_TYP_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_REL_TYP_2Tracker = false;

    /**
     * field for IND_REL_NM_2
     */
    protected java.lang.String localIND_REL_NM_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_REL_NM_2Tracker = false;

    /**
     * field for IND_ID_TYP_1
     */
    protected java.lang.String localIND_ID_TYP_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ID_TYP_1Tracker = false;

    /**
     * field for IND_ID_VALUE_1
     */
    protected java.lang.String localIND_ID_VALUE_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ID_VALUE_1Tracker = false;

    /**
     * field for IND_ID_TYP_2
     */
    protected java.lang.String localIND_ID_TYP_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ID_TYP_2Tracker = false;

    /**
     * field for IND_ID_VALUE_2
     */
    protected java.lang.String localIND_ID_VALUE_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ID_VALUE_2Tracker = false;

    /**
     * field for IND_GROUP_ID
     */
    protected java.lang.String localIND_GROUP_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_GROUP_IDTracker = false;

    /**
     * field for IND_GROUP_CREATION_DATE
     */
    protected java.lang.String localIND_GROUP_CREATION_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_GROUP_CREATION_DATETracker = false;

    /**
     * field for IND_INSERT_DATE
     */
    protected java.lang.String localIND_INSERT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_INSERT_DATETracker = false;

    /**
     * field for IND_ACCT_TYPE
     */
    protected java.lang.String localIND_ACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ACCT_TYPETracker = false;

    /**
     * field for IND_FREQ
     */
    protected java.lang.String localIND_FREQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_FREQTracker = false;

    /**
     * field for IND_STATUS
     */
    protected java.lang.String localIND_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_STATUSTracker = false;

    /**
     * field for IND_ACCT_NUMBER
     */
    protected java.lang.String localIND_ACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_ACCT_NUMBERTracker = false;

    /**
     * field for IND_DISBURSED_AMT
     */
    protected java.lang.String localIND_DISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_DISBURSED_AMTTracker = false;

    /**
     * field for IND_CURRENT_BAL
     */
    protected java.lang.String localIND_CURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_CURRENT_BALTracker = false;

    /**
     * field for IND_INSTALLMENT_AMT
     */
    protected java.lang.String localIND_INSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_INSTALLMENT_AMTTracker = false;

    /**
     * field for IND_OVERDUE_AMT
     */
    protected java.lang.String localIND_OVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_OVERDUE_AMTTracker = false;

    /**
     * field for IND_WRITE_OFF_AMT
     */
    protected java.lang.String localIND_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_WRITE_OFF_AMTTracker = false;

    /**
     * field for IND_DISBURSED_DT
     */
    protected java.lang.String localIND_DISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_DISBURSED_DTTracker = false;

    /**
     * field for IND_CLOSED_DT
     */
    protected java.lang.String localIND_CLOSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_CLOSED_DTTracker = false;

    /**
     * field for IND_RECENT_DELINQ_DT
     */
    protected java.lang.String localIND_RECENT_DELINQ_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_RECENT_DELINQ_DTTracker = false;

    /**
     * field for IND_DPD
     */
    protected java.lang.String localIND_DPD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_DPDTracker = false;

    /**
     * field for IND_INQ_CNT
     */
    protected java.lang.String localIND_INQ_CNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_INQ_CNTTracker = false;

    /**
     * field for IND_INFO_AS_ON
     */
    protected java.lang.String localIND_INFO_AS_ON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_INFO_AS_ONTracker = false;

    /**
     * field for IND_WORST_DELEQUENCY_AMOUNT
     */
    protected java.lang.String localIND_WORST_DELEQUENCY_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_WORST_DELEQUENCY_AMOUNTTracker = false;

    /**
     * field for IND_PAYMENT_HISTORY
     */
    protected java.lang.String localIND_PAYMENT_HISTORY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_PAYMENT_HISTORYTracker = false;

    /**
     * field for IND_IS_ACTIVE_BORROWER
     */
    protected java.lang.String localIND_IS_ACTIVE_BORROWER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_IS_ACTIVE_BORROWERTracker = false;

    /**
     * field for IND_NO_OF_BORROWERS
     */
    protected java.lang.String localIND_NO_OF_BORROWERS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_NO_OF_BORROWERSTracker = false;

    /**
     * field for IND_COMMENT_RES
     */
    protected java.lang.String localIND_COMMENT_RES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_COMMENT_RESTracker = false;

    /**
     * field for GRP_STATUS_PR
     */
    protected java.lang.String localGRP_STATUS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_STATUS_PRTracker = false;

    /**
     * field for GRP_NO_OF_DEFAULT_ACCOUNTS_PR
     */
    protected java.lang.String localGRP_NO_OF_DEFAULT_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_DEFAULT_ACCOUNTS_PRTracker = false;

    /**
     * field for GRP_TOTAL_RESPONSES_PR
     */
    protected java.lang.String localGRP_TOTAL_RESPONSES_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TOTAL_RESPONSES_PRTracker = false;

    /**
     * field for GRP_NO_OF_CLOSED_ACCOUNTS_PR
     */
    protected java.lang.String localGRP_NO_OF_CLOSED_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_CLOSED_ACCOUNTS_PRTracker = false;

    /**
     * field for GRP_NO_OF_ACTIVE_ACCOUNTS_PR
     */
    protected java.lang.String localGRP_NO_OF_ACTIVE_ACCOUNTS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_ACTIVE_ACCOUNTS_PRTracker = false;

    /**
     * field for GRP_NO_OF_OTHER_MFIS_PR
     */
    protected java.lang.String localGRP_NO_OF_OTHER_MFIS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_OTHER_MFIS_PRTracker = false;

    /**
     * field for GRP_OWN_MFI_INDECATOR_PR
     */
    protected java.lang.String localGRP_OWN_MFI_INDECATOR_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_OWN_MFI_INDECATOR_PRTracker = false;

    /**
     * field for GRP_TTL_OWN_DISBURSD_AMT_PR
     */
    protected java.lang.String localGRP_TTL_OWN_DISBURSD_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_DISBURSD_AMT_PRTracker = false;

    /**
     * field for GRP_TTL_OTHER_DISBURSD_AMT_PR
     */
    protected java.lang.String localGRP_TTL_OTHER_DISBURSD_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_DISBURSD_AMT_PRTracker = false;

    /**
     * field for GRP_TTL_OWN_CURRENT_BAL_PR
     */
    protected java.lang.String localGRP_TTL_OWN_CURRENT_BAL_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_CURRENT_BAL_PRTracker = false;

    /**
     * field for GRP_TTL_OTHER_CURRENT_BAL_PR
     */
    protected java.lang.String localGRP_TTL_OTHER_CURRENT_BAL_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_CURRENT_BAL_PRTracker = false;

    /**
     * field for GRP_TTL_OWN_INSTLMNT_AMT_PR
     */
    protected java.lang.String localGRP_TTL_OWN_INSTLMNT_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_INSTLMNT_AMT_PRTracker = false;

    /**
     * field for GRP_TTL_OTHER_INSTLMNT_AMT_PR
     */
    protected java.lang.String localGRP_TTL_OTHER_INSTLMNT_AMT_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_INSTLMNT_AMT_PRTracker = false;

    /**
     * field for GRP_MAX_WORST_DELEQUENCY_PR
     */
    protected java.lang.String localGRP_MAX_WORST_DELEQUENCY_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MAX_WORST_DELEQUENCY_PRTracker = false;

    /**
     * field for GRP_ERRORS_PR
     */
    protected java.lang.String localGRP_ERRORS_PR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ERRORS_PRTracker = false;

    /**
     * field for GRP_STATUS_SE
     */
    protected java.lang.String localGRP_STATUS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_STATUS_SETracker = false;

    /**
     * field for GRP_NO_OF_DEFAULT_ACCOUNTS_SE
     */
    protected java.lang.String localGRP_NO_OF_DEFAULT_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_DEFAULT_ACCOUNTS_SETracker = false;

    /**
     * field for GRP_TOTAL_RESPONSES_SE
     */
    protected java.lang.String localGRP_TOTAL_RESPONSES_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TOTAL_RESPONSES_SETracker = false;

    /**
     * field for GRP_NO_OF_CLOSED_ACCOUNTS_SE
     */
    protected java.lang.String localGRP_NO_OF_CLOSED_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_CLOSED_ACCOUNTS_SETracker = false;

    /**
     * field for GRP_NO_OF_ACTIVE_ACCOUNTS_SE
     */
    protected java.lang.String localGRP_NO_OF_ACTIVE_ACCOUNTS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_ACTIVE_ACCOUNTS_SETracker = false;

    /**
     * field for GRP_NO_OF_OTHER_MFIS_SE
     */
    protected java.lang.String localGRP_NO_OF_OTHER_MFIS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_OTHER_MFIS_SETracker = false;

    /**
     * field for GRP_OWN_MFI_INDECATOR_SE
     */
    protected java.lang.String localGRP_OWN_MFI_INDECATOR_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_OWN_MFI_INDECATOR_SETracker = false;

    /**
     * field for GRP_TTL_OWN_DISBURSD_AMT_SE
     */
    protected java.lang.String localGRP_TTL_OWN_DISBURSD_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_DISBURSD_AMT_SETracker = false;

    /**
     * field for GRP_TTL_OTHER_DISBURSD_AMT_SE
     */
    protected java.lang.String localGRP_TTL_OTHER_DISBURSD_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_DISBURSD_AMT_SETracker = false;

    /**
     * field for GRP_TTL_OWN_CURRENT_BAL_SE
     */
    protected java.lang.String localGRP_TTL_OWN_CURRENT_BAL_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_CURRENT_BAL_SETracker = false;

    /**
     * field for GRP_TTL_OTHER_CURRENT_BAL_SE
     */
    protected java.lang.String localGRP_TTL_OTHER_CURRENT_BAL_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_CURRENT_BAL_SETracker = false;

    /**
     * field for GRP_TTL_OWN_INSTLMNT_AMT_SE
     */
    protected java.lang.String localGRP_TTL_OWN_INSTLMNT_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_INSTLMNT_AMT_SETracker = false;

    /**
     * field for GRP_TTL_OTHER_INSTLMNT_AMT_SE
     */
    protected java.lang.String localGRP_TTL_OTHER_INSTLMNT_AMT_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_INSTLMNT_AMT_SETracker = false;

    /**
     * field for GRP_MAX_WORST_DELEQUENCY_SE
     */
    protected java.lang.String localGRP_MAX_WORST_DELEQUENCY_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MAX_WORST_DELEQUENCY_SETracker = false;

    /**
     * field for GRP_ERRORS_SE
     */
    protected java.lang.String localGRP_ERRORS_SE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ERRORS_SETracker = false;

    /**
     * field for GRP_STATUS_SM
     */
    protected java.lang.String localGRP_STATUS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_STATUS_SMTracker = false;

    /**
     * field for GRP_NO_OF_DEFAULT_ACCOUNTS_SM
     */
    protected java.lang.String localGRP_NO_OF_DEFAULT_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_DEFAULT_ACCOUNTS_SMTracker = false;

    /**
     * field for GRP_TOTAL_RESPONSES_SM
     */
    protected java.lang.String localGRP_TOTAL_RESPONSES_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TOTAL_RESPONSES_SMTracker = false;

    /**
     * field for GRP_NO_OF_CLOSED_ACCOUNTS_SM
     */
    protected java.lang.String localGRP_NO_OF_CLOSED_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_CLOSED_ACCOUNTS_SMTracker = false;

    /**
     * field for GRP_NO_OF_ACTIVE_ACCOUNTS_SM
     */
    protected java.lang.String localGRP_NO_OF_ACTIVE_ACCOUNTS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_ACTIVE_ACCOUNTS_SMTracker = false;

    /**
     * field for GRP_NO_OF_OTHER_MFIS_SM
     */
    protected java.lang.String localGRP_NO_OF_OTHER_MFIS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_OTHER_MFIS_SMTracker = false;

    /**
     * field for GRP_OWN_MFI_INDECATOR_SM
     */
    protected java.lang.String localGRP_OWN_MFI_INDECATOR_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_OWN_MFI_INDECATOR_SMTracker = false;

    /**
     * field for GRP_TTL_OWN_DISBURSD_AMT_SM
     */
    protected java.lang.String localGRP_TTL_OWN_DISBURSD_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_DISBURSD_AMT_SMTracker = false;

    /**
     * field for GRP_TTL_OTHER_DISBURSD_AMT_SM
     */
    protected java.lang.String localGRP_TTL_OTHER_DISBURSD_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_DISBURSD_AMT_SMTracker = false;

    /**
     * field for GRP_TTL_OWN_CURRENT_BAL_SM
     */
    protected java.lang.String localGRP_TTL_OWN_CURRENT_BAL_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_CURRENT_BAL_SMTracker = false;

    /**
     * field for GRP_TTL_OTHER_CURRENT_BAL_SM
     */
    protected java.lang.String localGRP_TTL_OTHER_CURRENT_BAL_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_CURRENT_BAL_SMTracker = false;

    /**
     * field for GRP_TTL_OWN_INSTLMNT_AMT_SM
     */
    protected java.lang.String localGRP_TTL_OWN_INSTLMNT_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OWN_INSTLMNT_AMT_SMTracker = false;

    /**
     * field for GRP_TTL_OTHER_INSTLMNT_AMT_SM
     */
    protected java.lang.String localGRP_TTL_OTHER_INSTLMNT_AMT_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_TTL_OTHER_INSTLMNT_AMT_SMTracker = false;

    /**
     * field for GRP_MAX_WORST_DELEQUENCY_SM
     */
    protected java.lang.String localGRP_MAX_WORST_DELEQUENCY_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MAX_WORST_DELEQUENCY_SMTracker = false;

    /**
     * field for GRP_ERRORS_SM
     */
    protected java.lang.String localGRP_ERRORS_SM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ERRORS_SMTracker = false;

    /**
     * field for GRP_MATCHED_TYPE
     */
    protected java.lang.String localGRP_MATCHED_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MATCHED_TYPETracker = false;

    /**
     * field for GRP_MFI
     */
    protected java.lang.String localGRP_MFI;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MFITracker = false;

    /**
     * field for GRP_MFI_ID
     */
    protected java.lang.String localGRP_MFI_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_MFI_IDTracker = false;

    /**
     * field for GRP_BRANCH
     */
    protected java.lang.String localGRP_BRANCH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_BRANCHTracker = false;

    /**
     * field for GRP_KENDRA
     */
    protected java.lang.String localGRP_KENDRA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_KENDRATracker = false;

    /**
     * field for GRP_NAME
     */
    protected java.lang.String localGRP_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NAMETracker = false;

    /**
     * field for GRP_SPOUSE
     */
    protected java.lang.String localGRP_SPOUSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_SPOUSETracker = false;

    /**
     * field for GRP_FATHER
     */
    protected java.lang.String localGRP_FATHER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_FATHERTracker = false;

    /**
     * field for GRP_CNSMR_MBR_ID
     */
    protected java.lang.String localGRP_CNSMR_MBR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_CNSMR_MBR_IDTracker = false;

    /**
     * field for GRP_DOB
     */
    protected java.lang.String localGRP_DOB;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_DOBTracker = false;

    /**
     * field for GRP_AGE
     */
    protected java.lang.String localGRP_AGE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_AGETracker = false;

    /**
     * field for GRP_AGE_AS_ON
     */
    protected java.lang.String localGRP_AGE_AS_ON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_AGE_AS_ONTracker = false;

    /**
     * field for GRP_PHONE
     */
    protected java.lang.String localGRP_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_PHONETracker = false;

    /**
     * field for GRP_ADDRESS_1
     */
    protected java.lang.String localGRP_ADDRESS_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ADDRESS_1Tracker = false;

    /**
     * field for GRP_ADDRESS_2
     */
    protected java.lang.String localGRP_ADDRESS_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ADDRESS_2Tracker = false;

    /**
     * field for GRP_REL_TYP_1
     */
    protected java.lang.String localGRP_REL_TYP_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_REL_TYP_1Tracker = false;

    /**
     * field for GRP_REL_NM_1
     */
    protected java.lang.String localGRP_REL_NM_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_REL_NM_1Tracker = false;

    /**
     * field for GRP_REL_TYP_2
     */
    protected java.lang.String localGRP_REL_TYP_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_REL_TYP_2Tracker = false;

    /**
     * field for GRP_REL_NM_2
     */
    protected java.lang.String localGRP_REL_NM_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_REL_NM_2Tracker = false;

    /**
     * field for GRP_ID_TYP_1
     */
    protected java.lang.String localGRP_ID_TYP_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ID_TYP_1Tracker = false;

    /**
     * field for GRP_ID_VALUE_1
     */
    protected java.lang.String localGRP_ID_VALUE_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ID_VALUE_1Tracker = false;

    /**
     * field for GRP_ID_TYP_2
     */
    protected java.lang.String localGRP_ID_TYP_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ID_TYP_2Tracker = false;

    /**
     * field for GRP_ID_VALUE_2
     */
    protected java.lang.String localGRP_ID_VALUE_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ID_VALUE_2Tracker = false;

    /**
     * field for GRP_GROUP_ID
     */
    protected java.lang.String localGRP_GROUP_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_GROUP_IDTracker = false;

    /**
     * field for GRP_GROUP_CREATION_DATE
     */
    protected java.lang.String localGRP_GROUP_CREATION_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_GROUP_CREATION_DATETracker = false;

    /**
     * field for GRP_INSERT_DATE
     */
    protected java.lang.String localGRP_INSERT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_INSERT_DATETracker = false;

    /**
     * field for GRP_ACCT_TYPE
     */
    protected java.lang.String localGRP_ACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ACCT_TYPETracker = false;

    /**
     * field for GRP_FREQ
     */
    protected java.lang.String localGRP_FREQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_FREQTracker = false;

    /**
     * field for GRP_STATUS
     */
    protected java.lang.String localGRP_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_STATUSTracker = false;

    /**
     * field for GRP_ACCT_NUMBER
     */
    protected java.lang.String localGRP_ACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_ACCT_NUMBERTracker = false;

    /**
     * field for GRP_DISBURSED_AMT
     */
    protected java.lang.String localGRP_DISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_DISBURSED_AMTTracker = false;

    /**
     * field for GRP_CURRENT_BAL
     */
    protected java.lang.String localGRP_CURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_CURRENT_BALTracker = false;

    /**
     * field for GRP_INSTALLMENT_AMT
     */
    protected java.lang.String localGRP_INSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_INSTALLMENT_AMTTracker = false;

    /**
     * field for GRP_OVERDUE_AMT
     */
    protected java.lang.String localGRP_OVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_OVERDUE_AMTTracker = false;

    /**
     * field for GRP_WRITE_OFF_AMT
     */
    protected java.lang.String localGRP_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_WRITE_OFF_AMTTracker = false;

    /**
     * field for GRP_DISBURSED_DT
     */
    protected java.lang.String localGRP_DISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_DISBURSED_DTTracker = false;

    /**
     * field for GRP_CLOSED_DT
     */
    protected java.lang.String localGRP_CLOSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_CLOSED_DTTracker = false;

    /**
     * field for GRP_RECENT_DELINQ_DT
     */
    protected java.lang.String localGRP_RECENT_DELINQ_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_RECENT_DELINQ_DTTracker = false;

    /**
     * field for GRP_DPD
     */
    protected java.lang.String localGRP_DPD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_DPDTracker = false;

    /**
     * field for GRP_INQ_CNT
     */
    protected java.lang.String localGRP_INQ_CNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_INQ_CNTTracker = false;

    /**
     * field for GRP_INFO_AS_ON
     */
    protected java.lang.String localGRP_INFO_AS_ON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_INFO_AS_ONTracker = false;

    /**
     * field for GRP_WORST_DELEQUENCY_AMOUNT
     */
    protected java.lang.String localGRP_WORST_DELEQUENCY_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_WORST_DELEQUENCY_AMOUNTTracker = false;

    /**
     * field for GRP_PAYMENT_HISTORY
     */
    protected java.lang.String localGRP_PAYMENT_HISTORY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_PAYMENT_HISTORYTracker = false;

    /**
     * field for GRP_IS_ACTIVE_BORROWER
     */
    protected java.lang.String localGRP_IS_ACTIVE_BORROWER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_IS_ACTIVE_BORROWERTracker = false;

    /**
     * field for GRP_NO_OF_BORROWERS
     */
    protected java.lang.String localGRP_NO_OF_BORROWERS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_NO_OF_BORROWERSTracker = false;

    /**
     * field for GRP_COMMENT_RES
     */
    protected java.lang.String localGRP_COMMENT_RES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGRP_COMMENT_RESTracker = false;

    /**
     * field for MEMBER_NAME
     */
    protected java.lang.String localMEMBER_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_NAMETracker = false;

    /**
     * field for INQUIRY_DATE
     */
    protected java.lang.String localINQUIRY_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINQUIRY_DATETracker = false;

    /**
     * field for PURPOSE
     */
    protected java.lang.String localPURPOSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPURPOSETracker = false;

    /**
     * field for AMOUNT
     */
    protected java.lang.String localAMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNTTracker = false;

    /**
     * field for IND_REMARK
     */
    protected java.lang.String localIND_REMARK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIND_REMARKTracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isDATE_OF_REQUESTSpecified() {
        return localDATE_OF_REQUESTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_REQUEST() {
        return localDATE_OF_REQUEST;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_REQUEST
     */
    public void setDATE_OF_REQUEST(java.lang.String param) {
        localDATE_OF_REQUESTTracker = param != null;

        this.localDATE_OF_REQUEST = param;
    }

    public boolean isPREPARED_FORSpecified() {
        return localPREPARED_FORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR() {
        return localPREPARED_FOR;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR
     */
    public void setPREPARED_FOR(java.lang.String param) {
        localPREPARED_FORTracker = param != null;

        this.localPREPARED_FOR = param;
    }

    public boolean isPREPARED_FOR_IDSpecified() {
        return localPREPARED_FOR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR_ID() {
        return localPREPARED_FOR_ID;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR_ID
     */
    public void setPREPARED_FOR_ID(java.lang.String param) {
        localPREPARED_FOR_IDTracker = param != null;

        this.localPREPARED_FOR_ID = param;
    }

    public boolean isDATE_OF_ISSUESpecified() {
        return localDATE_OF_ISSUETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_ISSUE() {
        return localDATE_OF_ISSUE;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_ISSUE
     */
    public void setDATE_OF_ISSUE(java.lang.String param) {
        localDATE_OF_ISSUETracker = param != null;

        this.localDATE_OF_ISSUE = param;
    }

    public boolean isREPORT_IDSpecified() {
        return localREPORT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORT_ID() {
        return localREPORT_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORT_ID
     */
    public void setREPORT_ID(java.lang.String param) {
        localREPORT_IDTracker = param != null;

        this.localREPORT_ID = param;
    }

    public boolean isNAME_IQSpecified() {
        return localNAME_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME_IQ() {
        return localNAME_IQ;
    }

    /**
     * Auto generated setter method
     * @param param NAME_IQ
     */
    public void setNAME_IQ(java.lang.String param) {
        localNAME_IQTracker = param != null;

        this.localNAME_IQ = param;
    }

    public boolean isSPOUSE_IQSpecified() {
        return localSPOUSE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSPOUSE_IQ() {
        return localSPOUSE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param SPOUSE_IQ
     */
    public void setSPOUSE_IQ(java.lang.String param) {
        localSPOUSE_IQTracker = param != null;

        this.localSPOUSE_IQ = param;
    }

    public boolean isFATHER_IQSpecified() {
        return localFATHER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFATHER_IQ() {
        return localFATHER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param FATHER_IQ
     */
    public void setFATHER_IQ(java.lang.String param) {
        localFATHER_IQTracker = param != null;

        this.localFATHER_IQ = param;
    }

    public boolean isMOTHER_IQSpecified() {
        return localMOTHER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOTHER_IQ() {
        return localMOTHER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param MOTHER_IQ
     */
    public void setMOTHER_IQ(java.lang.String param) {
        localMOTHER_IQTracker = param != null;

        this.localMOTHER_IQ = param;
    }

    public boolean isDOB_IQSpecified() {
        return localDOB_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB_IQ() {
        return localDOB_IQ;
    }

    /**
     * Auto generated setter method
     * @param param DOB_IQ
     */
    public void setDOB_IQ(java.lang.String param) {
        localDOB_IQTracker = param != null;

        this.localDOB_IQ = param;
    }

    public boolean isAGE_IQSpecified() {
        return localAGE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_IQ() {
        return localAGE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AGE_IQ
     */
    public void setAGE_IQ(java.lang.String param) {
        localAGE_IQTracker = param != null;

        this.localAGE_IQ = param;
    }

    public boolean isAGE_AS_ON_IQSpecified() {
        return localAGE_AS_ON_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_AS_ON_IQ() {
        return localAGE_AS_ON_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AGE_AS_ON_IQ
     */
    public void setAGE_AS_ON_IQ(java.lang.String param) {
        localAGE_AS_ON_IQTracker = param != null;

        this.localAGE_AS_ON_IQ = param;
    }

    public boolean isGENDER_IQSpecified() {
        return localGENDER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER_IQ() {
        return localGENDER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param GENDER_IQ
     */
    public void setGENDER_IQ(java.lang.String param) {
        localGENDER_IQTracker = param != null;

        this.localGENDER_IQ = param;
    }

    public boolean isPHONE_1_IQSpecified() {
        return localPHONE_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_1_IQ() {
        return localPHONE_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_1_IQ
     */
    public void setPHONE_1_IQ(java.lang.String param) {
        localPHONE_1_IQTracker = param != null;

        this.localPHONE_1_IQ = param;
    }

    public boolean isPHONE_2_IQSpecified() {
        return localPHONE_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_2_IQ() {
        return localPHONE_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_2_IQ
     */
    public void setPHONE_2_IQ(java.lang.String param) {
        localPHONE_2_IQTracker = param != null;

        this.localPHONE_2_IQ = param;
    }

    public boolean isPHONE_3_IQSpecified() {
        return localPHONE_3_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_3_IQ() {
        return localPHONE_3_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_3_IQ
     */
    public void setPHONE_3_IQ(java.lang.String param) {
        localPHONE_3_IQTracker = param != null;

        this.localPHONE_3_IQ = param;
    }

    public boolean isADDRESS_1_IQSpecified() {
        return localADDRESS_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_1_IQ() {
        return localADDRESS_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_1_IQ
     */
    public void setADDRESS_1_IQ(java.lang.String param) {
        localADDRESS_1_IQTracker = param != null;

        this.localADDRESS_1_IQ = param;
    }

    public boolean isADDRESS_2_IQSpecified() {
        return localADDRESS_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_2_IQ() {
        return localADDRESS_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_2_IQ
     */
    public void setADDRESS_2_IQ(java.lang.String param) {
        localADDRESS_2_IQTracker = param != null;

        this.localADDRESS_2_IQ = param;
    }

    public boolean isREL_TYP_1_IQSpecified() {
        return localREL_TYP_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_TYP_1_IQ() {
        return localREL_TYP_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_TYP_1_IQ
     */
    public void setREL_TYP_1_IQ(java.lang.String param) {
        localREL_TYP_1_IQTracker = param != null;

        this.localREL_TYP_1_IQ = param;
    }

    public boolean isREL_NM_1_IQSpecified() {
        return localREL_NM_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_NM_1_IQ() {
        return localREL_NM_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_NM_1_IQ
     */
    public void setREL_NM_1_IQ(java.lang.String param) {
        localREL_NM_1_IQTracker = param != null;

        this.localREL_NM_1_IQ = param;
    }

    public boolean isREL_TYP_2_IQSpecified() {
        return localREL_TYP_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_TYP_2_IQ() {
        return localREL_TYP_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_TYP_2_IQ
     */
    public void setREL_TYP_2_IQ(java.lang.String param) {
        localREL_TYP_2_IQTracker = param != null;

        this.localREL_TYP_2_IQ = param;
    }

    public boolean isREL_NM_2_IQSpecified() {
        return localREL_NM_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_NM_2_IQ() {
        return localREL_NM_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_NM_2_IQ
     */
    public void setREL_NM_2_IQ(java.lang.String param) {
        localREL_NM_2_IQTracker = param != null;

        this.localREL_NM_2_IQ = param;
    }

    public boolean isREL_TYP_3_IQSpecified() {
        return localREL_TYP_3_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_TYP_3_IQ() {
        return localREL_TYP_3_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_TYP_3_IQ
     */
    public void setREL_TYP_3_IQ(java.lang.String param) {
        localREL_TYP_3_IQTracker = param != null;

        this.localREL_TYP_3_IQ = param;
    }

    public boolean isREL_NM_3_IQSpecified() {
        return localREL_NM_3_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_NM_3_IQ() {
        return localREL_NM_3_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_NM_3_IQ
     */
    public void setREL_NM_3_IQ(java.lang.String param) {
        localREL_NM_3_IQTracker = param != null;

        this.localREL_NM_3_IQ = param;
    }

    public boolean isREL_TYP_4_IQSpecified() {
        return localREL_TYP_4_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_TYP_4_IQ() {
        return localREL_TYP_4_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_TYP_4_IQ
     */
    public void setREL_TYP_4_IQ(java.lang.String param) {
        localREL_TYP_4_IQTracker = param != null;

        this.localREL_TYP_4_IQ = param;
    }

    public boolean isREL_NM_4_IQSpecified() {
        return localREL_NM_4_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREL_NM_4_IQ() {
        return localREL_NM_4_IQ;
    }

    /**
     * Auto generated setter method
     * @param param REL_NM_4_IQ
     */
    public void setREL_NM_4_IQ(java.lang.String param) {
        localREL_NM_4_IQTracker = param != null;

        this.localREL_NM_4_IQ = param;
    }

    public boolean isID_TYPE_1_IQSpecified() {
        return localID_TYPE_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_TYPE_1_IQ() {
        return localID_TYPE_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ID_TYPE_1_IQ
     */
    public void setID_TYPE_1_IQ(java.lang.String param) {
        localID_TYPE_1_IQTracker = param != null;

        this.localID_TYPE_1_IQ = param;
    }

    public boolean isID_VALUE_1_IQSpecified() {
        return localID_VALUE_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_VALUE_1_IQ() {
        return localID_VALUE_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ID_VALUE_1_IQ
     */
    public void setID_VALUE_1_IQ(java.lang.String param) {
        localID_VALUE_1_IQTracker = param != null;

        this.localID_VALUE_1_IQ = param;
    }

    public boolean isID_TYPE_2_IQSpecified() {
        return localID_TYPE_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_TYPE_2_IQ() {
        return localID_TYPE_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ID_TYPE_2_IQ
     */
    public void setID_TYPE_2_IQ(java.lang.String param) {
        localID_TYPE_2_IQTracker = param != null;

        this.localID_TYPE_2_IQ = param;
    }

    public boolean isID_VALUE_2_IQSpecified() {
        return localID_VALUE_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_VALUE_2_IQ() {
        return localID_VALUE_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ID_VALUE_2_IQ
     */
    public void setID_VALUE_2_IQ(java.lang.String param) {
        localID_VALUE_2_IQTracker = param != null;

        this.localID_VALUE_2_IQ = param;
    }

    public boolean isRATION_CARD_IQSpecified() {
        return localRATION_CARD_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_IQ() {
        return localRATION_CARD_IQ;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_IQ
     */
    public void setRATION_CARD_IQ(java.lang.String param) {
        localRATION_CARD_IQTracker = param != null;

        this.localRATION_CARD_IQ = param;
    }

    public boolean isVOTERS_ID_IQSpecified() {
        return localVOTERS_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTERS_ID_IQ() {
        return localVOTERS_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param VOTERS_ID_IQ
     */
    public void setVOTERS_ID_IQ(java.lang.String param) {
        localVOTERS_ID_IQTracker = param != null;

        this.localVOTERS_ID_IQ = param;
    }

    public boolean isDRIVING_LICENCE_NO_IQSpecified() {
        return localDRIVING_LICENCE_NO_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVING_LICENCE_NO_IQ() {
        return localDRIVING_LICENCE_NO_IQ;
    }

    /**
     * Auto generated setter method
     * @param param DRIVING_LICENCE_NO_IQ
     */
    public void setDRIVING_LICENCE_NO_IQ(java.lang.String param) {
        localDRIVING_LICENCE_NO_IQTracker = param != null;

        this.localDRIVING_LICENCE_NO_IQ = param;
    }

    public boolean isPAN_IQSpecified() {
        return localPAN_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_IQ() {
        return localPAN_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PAN_IQ
     */
    public void setPAN_IQ(java.lang.String param) {
        localPAN_IQTracker = param != null;

        this.localPAN_IQ = param;
    }

    public boolean isPASSPORT_IQSpecified() {
        return localPASSPORT_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_IQ() {
        return localPASSPORT_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_IQ
     */
    public void setPASSPORT_IQ(java.lang.String param) {
        localPASSPORT_IQTracker = param != null;

        this.localPASSPORT_IQ = param;
    }

    public boolean isOTHER_ID_IQSpecified() {
        return localOTHER_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOTHER_ID_IQ() {
        return localOTHER_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param OTHER_ID_IQ
     */
    public void setOTHER_ID_IQ(java.lang.String param) {
        localOTHER_ID_IQTracker = param != null;

        this.localOTHER_ID_IQ = param;
    }

    public boolean isBRANCH_IQSpecified() {
        return localBRANCH_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBRANCH_IQ() {
        return localBRANCH_IQ;
    }

    /**
     * Auto generated setter method
     * @param param BRANCH_IQ
     */
    public void setBRANCH_IQ(java.lang.String param) {
        localBRANCH_IQTracker = param != null;

        this.localBRANCH_IQ = param;
    }

    public boolean isKENDRA_IQSpecified() {
        return localKENDRA_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getKENDRA_IQ() {
        return localKENDRA_IQ;
    }

    /**
     * Auto generated setter method
     * @param param KENDRA_IQ
     */
    public void setKENDRA_IQ(java.lang.String param) {
        localKENDRA_IQTracker = param != null;

        this.localKENDRA_IQ = param;
    }

    public boolean isMBR_ID_IQSpecified() {
        return localMBR_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMBR_ID_IQ() {
        return localMBR_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param MBR_ID_IQ
     */
    public void setMBR_ID_IQ(java.lang.String param) {
        localMBR_ID_IQTracker = param != null;

        this.localMBR_ID_IQ = param;
    }

    public boolean isCREDT_INQ_PURPS_TYPSpecified() {
        return localCREDT_INQ_PURPS_TYPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP() {
        return localCREDT_INQ_PURPS_TYP;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP
     */
    public void setCREDT_INQ_PURPS_TYP(java.lang.String param) {
        localCREDT_INQ_PURPS_TYPTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP = param;
    }

    public boolean isCREDT_INQ_PURPS_TYP_DESCSpecified() {
        return localCREDT_INQ_PURPS_TYP_DESCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP_DESC() {
        return localCREDT_INQ_PURPS_TYP_DESC;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP_DESC
     */
    public void setCREDT_INQ_PURPS_TYP_DESC(java.lang.String param) {
        localCREDT_INQ_PURPS_TYP_DESCTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP_DESC = param;
    }

    public boolean isCREDIT_INQUIRY_STAGESpecified() {
        return localCREDIT_INQUIRY_STAGETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_INQUIRY_STAGE() {
        return localCREDIT_INQUIRY_STAGE;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_INQUIRY_STAGE
     */
    public void setCREDIT_INQUIRY_STAGE(java.lang.String param) {
        localCREDIT_INQUIRY_STAGETracker = param != null;

        this.localCREDIT_INQUIRY_STAGE = param;
    }

    public boolean isCREDT_RPT_IDSpecified() {
        return localCREDT_RPT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_ID() {
        return localCREDT_RPT_ID;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_ID
     */
    public void setCREDT_RPT_ID(java.lang.String param) {
        localCREDT_RPT_IDTracker = param != null;

        this.localCREDT_RPT_ID = param;
    }

    public boolean isCREDT_REQ_TYPSpecified() {
        return localCREDT_REQ_TYPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_REQ_TYP() {
        return localCREDT_REQ_TYP;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_REQ_TYP
     */
    public void setCREDT_REQ_TYP(java.lang.String param) {
        localCREDT_REQ_TYPTracker = param != null;

        this.localCREDT_REQ_TYP = param;
    }

    public boolean isCREDT_RPT_TRN_DT_TMSpecified() {
        return localCREDT_RPT_TRN_DT_TMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_TRN_DT_TM() {
        return localCREDT_RPT_TRN_DT_TM;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_TRN_DT_TM
     */
    public void setCREDT_RPT_TRN_DT_TM(java.lang.String param) {
        localCREDT_RPT_TRN_DT_TMTracker = param != null;

        this.localCREDT_RPT_TRN_DT_TM = param;
    }

    public boolean isAC_OPEN_DTSpecified() {
        return localAC_OPEN_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAC_OPEN_DT() {
        return localAC_OPEN_DT;
    }

    /**
     * Auto generated setter method
     * @param param AC_OPEN_DT
     */
    public void setAC_OPEN_DT(java.lang.String param) {
        localAC_OPEN_DTTracker = param != null;

        this.localAC_OPEN_DT = param;
    }

    public boolean isLOAN_AMOUNTSpecified() {
        return localLOAN_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOAN_AMOUNT() {
        return localLOAN_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param LOAN_AMOUNT
     */
    public void setLOAN_AMOUNT(java.lang.String param) {
        localLOAN_AMOUNTTracker = param != null;

        this.localLOAN_AMOUNT = param;
    }

    public boolean isENTITY_IDSpecified() {
        return localENTITY_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENTITY_ID() {
        return localENTITY_ID;
    }

    /**
     * Auto generated setter method
     * @param param ENTITY_ID
     */
    public void setENTITY_ID(java.lang.String param) {
        localENTITY_IDTracker = param != null;

        this.localENTITY_ID = param;
    }

    public boolean isIND_STATUS_PRSpecified() {
        return localIND_STATUS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_STATUS_PR() {
        return localIND_STATUS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_STATUS_PR
     */
    public void setIND_STATUS_PR(java.lang.String param) {
        localIND_STATUS_PRTracker = param != null;

        this.localIND_STATUS_PR = param;
    }

    public boolean isIND_NO_OF_DEFAULT_ACCOUNTS_PRSpecified() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_DEFAULT_ACCOUNTS_PR() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_DEFAULT_ACCOUNTS_PR
     */
    public void setIND_NO_OF_DEFAULT_ACCOUNTS_PR(java.lang.String param) {
        localIND_NO_OF_DEFAULT_ACCOUNTS_PRTracker = param != null;

        this.localIND_NO_OF_DEFAULT_ACCOUNTS_PR = param;
    }

    public boolean isIND_TOTAL_RESPONSES_PRSpecified() {
        return localIND_TOTAL_RESPONSES_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TOTAL_RESPONSES_PR() {
        return localIND_TOTAL_RESPONSES_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TOTAL_RESPONSES_PR
     */
    public void setIND_TOTAL_RESPONSES_PR(java.lang.String param) {
        localIND_TOTAL_RESPONSES_PRTracker = param != null;

        this.localIND_TOTAL_RESPONSES_PR = param;
    }

    public boolean isIND_NO_OF_CLOSED_ACCOUNTS_PRSpecified() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_CLOSED_ACCOUNTS_PR() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_CLOSED_ACCOUNTS_PR
     */
    public void setIND_NO_OF_CLOSED_ACCOUNTS_PR(java.lang.String param) {
        localIND_NO_OF_CLOSED_ACCOUNTS_PRTracker = param != null;

        this.localIND_NO_OF_CLOSED_ACCOUNTS_PR = param;
    }

    public boolean isIND_NO_OF_ACTIVE_ACCOUNTS_PRSpecified() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_ACTIVE_ACCOUNTS_PR() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_ACTIVE_ACCOUNTS_PR
     */
    public void setIND_NO_OF_ACTIVE_ACCOUNTS_PR(java.lang.String param) {
        localIND_NO_OF_ACTIVE_ACCOUNTS_PRTracker = param != null;

        this.localIND_NO_OF_ACTIVE_ACCOUNTS_PR = param;
    }

    public boolean isIND_NO_OF_OTHER_MFIS_PRSpecified() {
        return localIND_NO_OF_OTHER_MFIS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_OTHER_MFIS_PR() {
        return localIND_NO_OF_OTHER_MFIS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_OTHER_MFIS_PR
     */
    public void setIND_NO_OF_OTHER_MFIS_PR(java.lang.String param) {
        localIND_NO_OF_OTHER_MFIS_PRTracker = param != null;

        this.localIND_NO_OF_OTHER_MFIS_PR = param;
    }

    public boolean isIND_OWN_MFI_INDECATOR_PRSpecified() {
        return localIND_OWN_MFI_INDECATOR_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_OWN_MFI_INDECATOR_PR() {
        return localIND_OWN_MFI_INDECATOR_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_OWN_MFI_INDECATOR_PR
     */
    public void setIND_OWN_MFI_INDECATOR_PR(java.lang.String param) {
        localIND_OWN_MFI_INDECATOR_PRTracker = param != null;

        this.localIND_OWN_MFI_INDECATOR_PR = param;
    }

    public boolean isIND_TTL_OWN_DISBURSD_AMT_PRSpecified() {
        return localIND_TTL_OWN_DISBURSD_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_DISBURSD_AMT_PR() {
        return localIND_TTL_OWN_DISBURSD_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_DISBURSD_AMT_PR
     */
    public void setIND_TTL_OWN_DISBURSD_AMT_PR(java.lang.String param) {
        localIND_TTL_OWN_DISBURSD_AMT_PRTracker = param != null;

        this.localIND_TTL_OWN_DISBURSD_AMT_PR = param;
    }

    public boolean isIND_TTL_OTHER_DISBURSD_AMT_PRSpecified() {
        return localIND_TTL_OTHER_DISBURSD_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_DISBURSD_AMT_PR() {
        return localIND_TTL_OTHER_DISBURSD_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_DISBURSD_AMT_PR
     */
    public void setIND_TTL_OTHER_DISBURSD_AMT_PR(java.lang.String param) {
        localIND_TTL_OTHER_DISBURSD_AMT_PRTracker = param != null;

        this.localIND_TTL_OTHER_DISBURSD_AMT_PR = param;
    }

    public boolean isIND_TTL_OWN_CURRENT_BAL_PRSpecified() {
        return localIND_TTL_OWN_CURRENT_BAL_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_CURRENT_BAL_PR() {
        return localIND_TTL_OWN_CURRENT_BAL_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_CURRENT_BAL_PR
     */
    public void setIND_TTL_OWN_CURRENT_BAL_PR(java.lang.String param) {
        localIND_TTL_OWN_CURRENT_BAL_PRTracker = param != null;

        this.localIND_TTL_OWN_CURRENT_BAL_PR = param;
    }

    public boolean isIND_TTL_OTHER_CURRENT_BAL_PRSpecified() {
        return localIND_TTL_OTHER_CURRENT_BAL_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_CURRENT_BAL_PR() {
        return localIND_TTL_OTHER_CURRENT_BAL_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_CURRENT_BAL_PR
     */
    public void setIND_TTL_OTHER_CURRENT_BAL_PR(java.lang.String param) {
        localIND_TTL_OTHER_CURRENT_BAL_PRTracker = param != null;

        this.localIND_TTL_OTHER_CURRENT_BAL_PR = param;
    }

    public boolean isIND_TTL_OWN_INSTLMNT_AMT_PRSpecified() {
        return localIND_TTL_OWN_INSTLMNT_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_INSTLMNT_AMT_PR() {
        return localIND_TTL_OWN_INSTLMNT_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_INSTLMNT_AMT_PR
     */
    public void setIND_TTL_OWN_INSTLMNT_AMT_PR(java.lang.String param) {
        localIND_TTL_OWN_INSTLMNT_AMT_PRTracker = param != null;

        this.localIND_TTL_OWN_INSTLMNT_AMT_PR = param;
    }

    public boolean isIND_TTL_OTHER_INSTLMNT_AMT_PRSpecified() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_INSTLMNT_AMT_PR() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_INSTLMNT_AMT_PR
     */
    public void setIND_TTL_OTHER_INSTLMNT_AMT_PR(java.lang.String param) {
        localIND_TTL_OTHER_INSTLMNT_AMT_PRTracker = param != null;

        this.localIND_TTL_OTHER_INSTLMNT_AMT_PR = param;
    }

    public boolean isIND_MAX_WORST_DELEQUENCY_PRSpecified() {
        return localIND_MAX_WORST_DELEQUENCY_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MAX_WORST_DELEQUENCY_PR() {
        return localIND_MAX_WORST_DELEQUENCY_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_MAX_WORST_DELEQUENCY_PR
     */
    public void setIND_MAX_WORST_DELEQUENCY_PR(java.lang.String param) {
        localIND_MAX_WORST_DELEQUENCY_PRTracker = param != null;

        this.localIND_MAX_WORST_DELEQUENCY_PR = param;
    }

    public boolean isIND_ERRORS_PRSpecified() {
        return localIND_ERRORS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ERRORS_PR() {
        return localIND_ERRORS_PR;
    }

    /**
     * Auto generated setter method
     * @param param IND_ERRORS_PR
     */
    public void setIND_ERRORS_PR(java.lang.String param) {
        localIND_ERRORS_PRTracker = param != null;

        this.localIND_ERRORS_PR = param;
    }

    public boolean isIND_STATUS_SESpecified() {
        return localIND_STATUS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_STATUS_SE() {
        return localIND_STATUS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_STATUS_SE
     */
    public void setIND_STATUS_SE(java.lang.String param) {
        localIND_STATUS_SETracker = param != null;

        this.localIND_STATUS_SE = param;
    }

    public boolean isIND_NO_OF_DEFAULT_ACCOUNTS_SESpecified() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_DEFAULT_ACCOUNTS_SE() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_DEFAULT_ACCOUNTS_SE
     */
    public void setIND_NO_OF_DEFAULT_ACCOUNTS_SE(java.lang.String param) {
        localIND_NO_OF_DEFAULT_ACCOUNTS_SETracker = param != null;

        this.localIND_NO_OF_DEFAULT_ACCOUNTS_SE = param;
    }

    public boolean isIND_TOTAL_RESPONSES_SESpecified() {
        return localIND_TOTAL_RESPONSES_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TOTAL_RESPONSES_SE() {
        return localIND_TOTAL_RESPONSES_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TOTAL_RESPONSES_SE
     */
    public void setIND_TOTAL_RESPONSES_SE(java.lang.String param) {
        localIND_TOTAL_RESPONSES_SETracker = param != null;

        this.localIND_TOTAL_RESPONSES_SE = param;
    }

    public boolean isIND_NO_OF_CLOSED_ACCOUNTS_SESpecified() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_CLOSED_ACCOUNTS_SE() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_CLOSED_ACCOUNTS_SE
     */
    public void setIND_NO_OF_CLOSED_ACCOUNTS_SE(java.lang.String param) {
        localIND_NO_OF_CLOSED_ACCOUNTS_SETracker = param != null;

        this.localIND_NO_OF_CLOSED_ACCOUNTS_SE = param;
    }

    public boolean isIND_NO_OF_ACTIVE_ACCOUNTS_SESpecified() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_ACTIVE_ACCOUNTS_SE() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_ACTIVE_ACCOUNTS_SE
     */
    public void setIND_NO_OF_ACTIVE_ACCOUNTS_SE(java.lang.String param) {
        localIND_NO_OF_ACTIVE_ACCOUNTS_SETracker = param != null;

        this.localIND_NO_OF_ACTIVE_ACCOUNTS_SE = param;
    }

    public boolean isIND_NO_OF_OTHER_MFIS_SESpecified() {
        return localIND_NO_OF_OTHER_MFIS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_OTHER_MFIS_SE() {
        return localIND_NO_OF_OTHER_MFIS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_OTHER_MFIS_SE
     */
    public void setIND_NO_OF_OTHER_MFIS_SE(java.lang.String param) {
        localIND_NO_OF_OTHER_MFIS_SETracker = param != null;

        this.localIND_NO_OF_OTHER_MFIS_SE = param;
    }

    public boolean isIND_OWN_MFI_INDECATOR_SESpecified() {
        return localIND_OWN_MFI_INDECATOR_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_OWN_MFI_INDECATOR_SE() {
        return localIND_OWN_MFI_INDECATOR_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_OWN_MFI_INDECATOR_SE
     */
    public void setIND_OWN_MFI_INDECATOR_SE(java.lang.String param) {
        localIND_OWN_MFI_INDECATOR_SETracker = param != null;

        this.localIND_OWN_MFI_INDECATOR_SE = param;
    }

    public boolean isIND_TTL_OWN_DISBURSD_AMT_SESpecified() {
        return localIND_TTL_OWN_DISBURSD_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_DISBURSD_AMT_SE() {
        return localIND_TTL_OWN_DISBURSD_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_DISBURSD_AMT_SE
     */
    public void setIND_TTL_OWN_DISBURSD_AMT_SE(java.lang.String param) {
        localIND_TTL_OWN_DISBURSD_AMT_SETracker = param != null;

        this.localIND_TTL_OWN_DISBURSD_AMT_SE = param;
    }

    public boolean isIND_TTL_OTHER_DISBURSD_AMT_SESpecified() {
        return localIND_TTL_OTHER_DISBURSD_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_DISBURSD_AMT_SE() {
        return localIND_TTL_OTHER_DISBURSD_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_DISBURSD_AMT_SE
     */
    public void setIND_TTL_OTHER_DISBURSD_AMT_SE(java.lang.String param) {
        localIND_TTL_OTHER_DISBURSD_AMT_SETracker = param != null;

        this.localIND_TTL_OTHER_DISBURSD_AMT_SE = param;
    }

    public boolean isIND_TTL_OWN_CURRENT_BAL_SESpecified() {
        return localIND_TTL_OWN_CURRENT_BAL_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_CURRENT_BAL_SE() {
        return localIND_TTL_OWN_CURRENT_BAL_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_CURRENT_BAL_SE
     */
    public void setIND_TTL_OWN_CURRENT_BAL_SE(java.lang.String param) {
        localIND_TTL_OWN_CURRENT_BAL_SETracker = param != null;

        this.localIND_TTL_OWN_CURRENT_BAL_SE = param;
    }

    public boolean isIND_TTL_OTHER_CURRENT_BAL_SESpecified() {
        return localIND_TTL_OTHER_CURRENT_BAL_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_CURRENT_BAL_SE() {
        return localIND_TTL_OTHER_CURRENT_BAL_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_CURRENT_BAL_SE
     */
    public void setIND_TTL_OTHER_CURRENT_BAL_SE(java.lang.String param) {
        localIND_TTL_OTHER_CURRENT_BAL_SETracker = param != null;

        this.localIND_TTL_OTHER_CURRENT_BAL_SE = param;
    }

    public boolean isIND_TTL_OWN_INSTLMNT_AMT_SESpecified() {
        return localIND_TTL_OWN_INSTLMNT_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_INSTLMNT_AMT_SE() {
        return localIND_TTL_OWN_INSTLMNT_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_INSTLMNT_AMT_SE
     */
    public void setIND_TTL_OWN_INSTLMNT_AMT_SE(java.lang.String param) {
        localIND_TTL_OWN_INSTLMNT_AMT_SETracker = param != null;

        this.localIND_TTL_OWN_INSTLMNT_AMT_SE = param;
    }

    public boolean isIND_TTL_OTHER_INSTLMNT_AMT_SESpecified() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_INSTLMNT_AMT_SE() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_INSTLMNT_AMT_SE
     */
    public void setIND_TTL_OTHER_INSTLMNT_AMT_SE(java.lang.String param) {
        localIND_TTL_OTHER_INSTLMNT_AMT_SETracker = param != null;

        this.localIND_TTL_OTHER_INSTLMNT_AMT_SE = param;
    }

    public boolean isIND_MAX_WORST_DELEQUENCY_SESpecified() {
        return localIND_MAX_WORST_DELEQUENCY_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MAX_WORST_DELEQUENCY_SE() {
        return localIND_MAX_WORST_DELEQUENCY_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_MAX_WORST_DELEQUENCY_SE
     */
    public void setIND_MAX_WORST_DELEQUENCY_SE(java.lang.String param) {
        localIND_MAX_WORST_DELEQUENCY_SETracker = param != null;

        this.localIND_MAX_WORST_DELEQUENCY_SE = param;
    }

    public boolean isIND_ERRORS_SESpecified() {
        return localIND_ERRORS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ERRORS_SE() {
        return localIND_ERRORS_SE;
    }

    /**
     * Auto generated setter method
     * @param param IND_ERRORS_SE
     */
    public void setIND_ERRORS_SE(java.lang.String param) {
        localIND_ERRORS_SETracker = param != null;

        this.localIND_ERRORS_SE = param;
    }

    public boolean isIND_STATUS_SMSpecified() {
        return localIND_STATUS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_STATUS_SM() {
        return localIND_STATUS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_STATUS_SM
     */
    public void setIND_STATUS_SM(java.lang.String param) {
        localIND_STATUS_SMTracker = param != null;

        this.localIND_STATUS_SM = param;
    }

    public boolean isIND_NO_OF_DEFAULT_ACCOUNTS_SMSpecified() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_DEFAULT_ACCOUNTS_SM() {
        return localIND_NO_OF_DEFAULT_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_DEFAULT_ACCOUNTS_SM
     */
    public void setIND_NO_OF_DEFAULT_ACCOUNTS_SM(java.lang.String param) {
        localIND_NO_OF_DEFAULT_ACCOUNTS_SMTracker = param != null;

        this.localIND_NO_OF_DEFAULT_ACCOUNTS_SM = param;
    }

    public boolean isIND_TOTAL_RESPONSES_SMSpecified() {
        return localIND_TOTAL_RESPONSES_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TOTAL_RESPONSES_SM() {
        return localIND_TOTAL_RESPONSES_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TOTAL_RESPONSES_SM
     */
    public void setIND_TOTAL_RESPONSES_SM(java.lang.String param) {
        localIND_TOTAL_RESPONSES_SMTracker = param != null;

        this.localIND_TOTAL_RESPONSES_SM = param;
    }

    public boolean isIND_NO_OF_CLOSED_ACCOUNTS_SMSpecified() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_CLOSED_ACCOUNTS_SM() {
        return localIND_NO_OF_CLOSED_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_CLOSED_ACCOUNTS_SM
     */
    public void setIND_NO_OF_CLOSED_ACCOUNTS_SM(java.lang.String param) {
        localIND_NO_OF_CLOSED_ACCOUNTS_SMTracker = param != null;

        this.localIND_NO_OF_CLOSED_ACCOUNTS_SM = param;
    }

    public boolean isIND_NO_OF_ACTIVE_ACCOUNTS_SMSpecified() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_ACTIVE_ACCOUNTS_SM() {
        return localIND_NO_OF_ACTIVE_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_ACTIVE_ACCOUNTS_SM
     */
    public void setIND_NO_OF_ACTIVE_ACCOUNTS_SM(java.lang.String param) {
        localIND_NO_OF_ACTIVE_ACCOUNTS_SMTracker = param != null;

        this.localIND_NO_OF_ACTIVE_ACCOUNTS_SM = param;
    }

    public boolean isIND_NO_OF_OTHER_MFIS_SMSpecified() {
        return localIND_NO_OF_OTHER_MFIS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_OTHER_MFIS_SM() {
        return localIND_NO_OF_OTHER_MFIS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_OTHER_MFIS_SM
     */
    public void setIND_NO_OF_OTHER_MFIS_SM(java.lang.String param) {
        localIND_NO_OF_OTHER_MFIS_SMTracker = param != null;

        this.localIND_NO_OF_OTHER_MFIS_SM = param;
    }

    public boolean isIND_OWN_MFI_INDECATOR_SMSpecified() {
        return localIND_OWN_MFI_INDECATOR_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_OWN_MFI_INDECATOR_SM() {
        return localIND_OWN_MFI_INDECATOR_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_OWN_MFI_INDECATOR_SM
     */
    public void setIND_OWN_MFI_INDECATOR_SM(java.lang.String param) {
        localIND_OWN_MFI_INDECATOR_SMTracker = param != null;

        this.localIND_OWN_MFI_INDECATOR_SM = param;
    }

    public boolean isIND_TTL_OWN_DISBURSD_AMT_SMSpecified() {
        return localIND_TTL_OWN_DISBURSD_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_DISBURSD_AMT_SM() {
        return localIND_TTL_OWN_DISBURSD_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_DISBURSD_AMT_SM
     */
    public void setIND_TTL_OWN_DISBURSD_AMT_SM(java.lang.String param) {
        localIND_TTL_OWN_DISBURSD_AMT_SMTracker = param != null;

        this.localIND_TTL_OWN_DISBURSD_AMT_SM = param;
    }

    public boolean isIND_TTL_OTHER_DISBURSD_AMT_SMSpecified() {
        return localIND_TTL_OTHER_DISBURSD_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_DISBURSD_AMT_SM() {
        return localIND_TTL_OTHER_DISBURSD_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_DISBURSD_AMT_SM
     */
    public void setIND_TTL_OTHER_DISBURSD_AMT_SM(java.lang.String param) {
        localIND_TTL_OTHER_DISBURSD_AMT_SMTracker = param != null;

        this.localIND_TTL_OTHER_DISBURSD_AMT_SM = param;
    }

    public boolean isIND_TTL_OWN_CURRENT_BAL_SMSpecified() {
        return localIND_TTL_OWN_CURRENT_BAL_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_CURRENT_BAL_SM() {
        return localIND_TTL_OWN_CURRENT_BAL_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_CURRENT_BAL_SM
     */
    public void setIND_TTL_OWN_CURRENT_BAL_SM(java.lang.String param) {
        localIND_TTL_OWN_CURRENT_BAL_SMTracker = param != null;

        this.localIND_TTL_OWN_CURRENT_BAL_SM = param;
    }

    public boolean isIND_TTL_OTHER_CURRENT_BAL_SMSpecified() {
        return localIND_TTL_OTHER_CURRENT_BAL_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_CURRENT_BAL_SM() {
        return localIND_TTL_OTHER_CURRENT_BAL_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_CURRENT_BAL_SM
     */
    public void setIND_TTL_OTHER_CURRENT_BAL_SM(java.lang.String param) {
        localIND_TTL_OTHER_CURRENT_BAL_SMTracker = param != null;

        this.localIND_TTL_OTHER_CURRENT_BAL_SM = param;
    }

    public boolean isIND_TTL_OWN_INSTLMNT_AMT_SMSpecified() {
        return localIND_TTL_OWN_INSTLMNT_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OWN_INSTLMNT_AMT_SM() {
        return localIND_TTL_OWN_INSTLMNT_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OWN_INSTLMNT_AMT_SM
     */
    public void setIND_TTL_OWN_INSTLMNT_AMT_SM(java.lang.String param) {
        localIND_TTL_OWN_INSTLMNT_AMT_SMTracker = param != null;

        this.localIND_TTL_OWN_INSTLMNT_AMT_SM = param;
    }

    public boolean isIND_TTL_OTHER_INSTLMNT_AMT_SMSpecified() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_TTL_OTHER_INSTLMNT_AMT_SM() {
        return localIND_TTL_OTHER_INSTLMNT_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_TTL_OTHER_INSTLMNT_AMT_SM
     */
    public void setIND_TTL_OTHER_INSTLMNT_AMT_SM(java.lang.String param) {
        localIND_TTL_OTHER_INSTLMNT_AMT_SMTracker = param != null;

        this.localIND_TTL_OTHER_INSTLMNT_AMT_SM = param;
    }

    public boolean isIND_MAX_WORST_DELEQUENCY_SMSpecified() {
        return localIND_MAX_WORST_DELEQUENCY_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MAX_WORST_DELEQUENCY_SM() {
        return localIND_MAX_WORST_DELEQUENCY_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_MAX_WORST_DELEQUENCY_SM
     */
    public void setIND_MAX_WORST_DELEQUENCY_SM(java.lang.String param) {
        localIND_MAX_WORST_DELEQUENCY_SMTracker = param != null;

        this.localIND_MAX_WORST_DELEQUENCY_SM = param;
    }

    public boolean isIND_ERRORS_SMSpecified() {
        return localIND_ERRORS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ERRORS_SM() {
        return localIND_ERRORS_SM;
    }

    /**
     * Auto generated setter method
     * @param param IND_ERRORS_SM
     */
    public void setIND_ERRORS_SM(java.lang.String param) {
        localIND_ERRORS_SMTracker = param != null;

        this.localIND_ERRORS_SM = param;
    }

    public boolean isIND_MATCHED_TYPESpecified() {
        return localIND_MATCHED_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MATCHED_TYPE() {
        return localIND_MATCHED_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param IND_MATCHED_TYPE
     */
    public void setIND_MATCHED_TYPE(java.lang.String param) {
        localIND_MATCHED_TYPETracker = param != null;

        this.localIND_MATCHED_TYPE = param;
    }

    public boolean isIND_MFISpecified() {
        return localIND_MFITracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MFI() {
        return localIND_MFI;
    }

    /**
     * Auto generated setter method
     * @param param IND_MFI
     */
    public void setIND_MFI(java.lang.String param) {
        localIND_MFITracker = param != null;

        this.localIND_MFI = param;
    }

    public boolean isIND_MFI_IDSpecified() {
        return localIND_MFI_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_MFI_ID() {
        return localIND_MFI_ID;
    }

    /**
     * Auto generated setter method
     * @param param IND_MFI_ID
     */
    public void setIND_MFI_ID(java.lang.String param) {
        localIND_MFI_IDTracker = param != null;

        this.localIND_MFI_ID = param;
    }

    public boolean isIND_BRANCHSpecified() {
        return localIND_BRANCHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_BRANCH() {
        return localIND_BRANCH;
    }

    /**
     * Auto generated setter method
     * @param param IND_BRANCH
     */
    public void setIND_BRANCH(java.lang.String param) {
        localIND_BRANCHTracker = param != null;

        this.localIND_BRANCH = param;
    }

    public boolean isIND_KENDRASpecified() {
        return localIND_KENDRATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_KENDRA() {
        return localIND_KENDRA;
    }

    /**
     * Auto generated setter method
     * @param param IND_KENDRA
     */
    public void setIND_KENDRA(java.lang.String param) {
        localIND_KENDRATracker = param != null;

        this.localIND_KENDRA = param;
    }

    public boolean isIND_NAMESpecified() {
        return localIND_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NAME() {
        return localIND_NAME;
    }

    /**
     * Auto generated setter method
     * @param param IND_NAME
     */
    public void setIND_NAME(java.lang.String param) {
        localIND_NAMETracker = param != null;

        this.localIND_NAME = param;
    }

    public boolean isIND_SPOUSESpecified() {
        return localIND_SPOUSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_SPOUSE() {
        return localIND_SPOUSE;
    }

    /**
     * Auto generated setter method
     * @param param IND_SPOUSE
     */
    public void setIND_SPOUSE(java.lang.String param) {
        localIND_SPOUSETracker = param != null;

        this.localIND_SPOUSE = param;
    }

    public boolean isIND_FATHERSpecified() {
        return localIND_FATHERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_FATHER() {
        return localIND_FATHER;
    }

    /**
     * Auto generated setter method
     * @param param IND_FATHER
     */
    public void setIND_FATHER(java.lang.String param) {
        localIND_FATHERTracker = param != null;

        this.localIND_FATHER = param;
    }

    public boolean isIND_CNSMR_MBR_IDSpecified() {
        return localIND_CNSMR_MBR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_CNSMR_MBR_ID() {
        return localIND_CNSMR_MBR_ID;
    }

    /**
     * Auto generated setter method
     * @param param IND_CNSMR_MBR_ID
     */
    public void setIND_CNSMR_MBR_ID(java.lang.String param) {
        localIND_CNSMR_MBR_IDTracker = param != null;

        this.localIND_CNSMR_MBR_ID = param;
    }

    public boolean isIND_DOBSpecified() {
        return localIND_DOBTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_DOB() {
        return localIND_DOB;
    }

    /**
     * Auto generated setter method
     * @param param IND_DOB
     */
    public void setIND_DOB(java.lang.String param) {
        localIND_DOBTracker = param != null;

        this.localIND_DOB = param;
    }

    public boolean isIND_AGESpecified() {
        return localIND_AGETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_AGE() {
        return localIND_AGE;
    }

    /**
     * Auto generated setter method
     * @param param IND_AGE
     */
    public void setIND_AGE(java.lang.String param) {
        localIND_AGETracker = param != null;

        this.localIND_AGE = param;
    }

    public boolean isIND_AGE_AS_ONSpecified() {
        return localIND_AGE_AS_ONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_AGE_AS_ON() {
        return localIND_AGE_AS_ON;
    }

    /**
     * Auto generated setter method
     * @param param IND_AGE_AS_ON
     */
    public void setIND_AGE_AS_ON(java.lang.String param) {
        localIND_AGE_AS_ONTracker = param != null;

        this.localIND_AGE_AS_ON = param;
    }

    public boolean isIND_PHONESpecified() {
        return localIND_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_PHONE() {
        return localIND_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param IND_PHONE
     */
    public void setIND_PHONE(java.lang.String param) {
        localIND_PHONETracker = param != null;

        this.localIND_PHONE = param;
    }

    public boolean isIND_ADDRESS_1Specified() {
        return localIND_ADDRESS_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ADDRESS_1() {
        return localIND_ADDRESS_1;
    }

    /**
     * Auto generated setter method
     * @param param IND_ADDRESS_1
     */
    public void setIND_ADDRESS_1(java.lang.String param) {
        localIND_ADDRESS_1Tracker = param != null;

        this.localIND_ADDRESS_1 = param;
    }

    public boolean isIND_ADDRESS_2Specified() {
        return localIND_ADDRESS_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ADDRESS_2() {
        return localIND_ADDRESS_2;
    }

    /**
     * Auto generated setter method
     * @param param IND_ADDRESS_2
     */
    public void setIND_ADDRESS_2(java.lang.String param) {
        localIND_ADDRESS_2Tracker = param != null;

        this.localIND_ADDRESS_2 = param;
    }

    public boolean isIND_REL_TYP_1Specified() {
        return localIND_REL_TYP_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_REL_TYP_1() {
        return localIND_REL_TYP_1;
    }

    /**
     * Auto generated setter method
     * @param param IND_REL_TYP_1
     */
    public void setIND_REL_TYP_1(java.lang.String param) {
        localIND_REL_TYP_1Tracker = param != null;

        this.localIND_REL_TYP_1 = param;
    }

    public boolean isIND_REL_NM_1Specified() {
        return localIND_REL_NM_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_REL_NM_1() {
        return localIND_REL_NM_1;
    }

    /**
     * Auto generated setter method
     * @param param IND_REL_NM_1
     */
    public void setIND_REL_NM_1(java.lang.String param) {
        localIND_REL_NM_1Tracker = param != null;

        this.localIND_REL_NM_1 = param;
    }

    public boolean isIND_REL_TYP_2Specified() {
        return localIND_REL_TYP_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_REL_TYP_2() {
        return localIND_REL_TYP_2;
    }

    /**
     * Auto generated setter method
     * @param param IND_REL_TYP_2
     */
    public void setIND_REL_TYP_2(java.lang.String param) {
        localIND_REL_TYP_2Tracker = param != null;

        this.localIND_REL_TYP_2 = param;
    }

    public boolean isIND_REL_NM_2Specified() {
        return localIND_REL_NM_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_REL_NM_2() {
        return localIND_REL_NM_2;
    }

    /**
     * Auto generated setter method
     * @param param IND_REL_NM_2
     */
    public void setIND_REL_NM_2(java.lang.String param) {
        localIND_REL_NM_2Tracker = param != null;

        this.localIND_REL_NM_2 = param;
    }

    public boolean isIND_ID_TYP_1Specified() {
        return localIND_ID_TYP_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ID_TYP_1() {
        return localIND_ID_TYP_1;
    }

    /**
     * Auto generated setter method
     * @param param IND_ID_TYP_1
     */
    public void setIND_ID_TYP_1(java.lang.String param) {
        localIND_ID_TYP_1Tracker = param != null;

        this.localIND_ID_TYP_1 = param;
    }

    public boolean isIND_ID_VALUE_1Specified() {
        return localIND_ID_VALUE_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ID_VALUE_1() {
        return localIND_ID_VALUE_1;
    }

    /**
     * Auto generated setter method
     * @param param IND_ID_VALUE_1
     */
    public void setIND_ID_VALUE_1(java.lang.String param) {
        localIND_ID_VALUE_1Tracker = param != null;

        this.localIND_ID_VALUE_1 = param;
    }

    public boolean isIND_ID_TYP_2Specified() {
        return localIND_ID_TYP_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ID_TYP_2() {
        return localIND_ID_TYP_2;
    }

    /**
     * Auto generated setter method
     * @param param IND_ID_TYP_2
     */
    public void setIND_ID_TYP_2(java.lang.String param) {
        localIND_ID_TYP_2Tracker = param != null;

        this.localIND_ID_TYP_2 = param;
    }

    public boolean isIND_ID_VALUE_2Specified() {
        return localIND_ID_VALUE_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ID_VALUE_2() {
        return localIND_ID_VALUE_2;
    }

    /**
     * Auto generated setter method
     * @param param IND_ID_VALUE_2
     */
    public void setIND_ID_VALUE_2(java.lang.String param) {
        localIND_ID_VALUE_2Tracker = param != null;

        this.localIND_ID_VALUE_2 = param;
    }

    public boolean isIND_GROUP_IDSpecified() {
        return localIND_GROUP_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_GROUP_ID() {
        return localIND_GROUP_ID;
    }

    /**
     * Auto generated setter method
     * @param param IND_GROUP_ID
     */
    public void setIND_GROUP_ID(java.lang.String param) {
        localIND_GROUP_IDTracker = param != null;

        this.localIND_GROUP_ID = param;
    }

    public boolean isIND_GROUP_CREATION_DATESpecified() {
        return localIND_GROUP_CREATION_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_GROUP_CREATION_DATE() {
        return localIND_GROUP_CREATION_DATE;
    }

    /**
     * Auto generated setter method
     * @param param IND_GROUP_CREATION_DATE
     */
    public void setIND_GROUP_CREATION_DATE(java.lang.String param) {
        localIND_GROUP_CREATION_DATETracker = param != null;

        this.localIND_GROUP_CREATION_DATE = param;
    }

    public boolean isIND_INSERT_DATESpecified() {
        return localIND_INSERT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_INSERT_DATE() {
        return localIND_INSERT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param IND_INSERT_DATE
     */
    public void setIND_INSERT_DATE(java.lang.String param) {
        localIND_INSERT_DATETracker = param != null;

        this.localIND_INSERT_DATE = param;
    }

    public boolean isIND_ACCT_TYPESpecified() {
        return localIND_ACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ACCT_TYPE() {
        return localIND_ACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param IND_ACCT_TYPE
     */
    public void setIND_ACCT_TYPE(java.lang.String param) {
        localIND_ACCT_TYPETracker = param != null;

        this.localIND_ACCT_TYPE = param;
    }

    public boolean isIND_FREQSpecified() {
        return localIND_FREQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_FREQ() {
        return localIND_FREQ;
    }

    /**
     * Auto generated setter method
     * @param param IND_FREQ
     */
    public void setIND_FREQ(java.lang.String param) {
        localIND_FREQTracker = param != null;

        this.localIND_FREQ = param;
    }

    public boolean isIND_STATUSSpecified() {
        return localIND_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_STATUS() {
        return localIND_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param IND_STATUS
     */
    public void setIND_STATUS(java.lang.String param) {
        localIND_STATUSTracker = param != null;

        this.localIND_STATUS = param;
    }

    public boolean isIND_ACCT_NUMBERSpecified() {
        return localIND_ACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_ACCT_NUMBER() {
        return localIND_ACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param IND_ACCT_NUMBER
     */
    public void setIND_ACCT_NUMBER(java.lang.String param) {
        localIND_ACCT_NUMBERTracker = param != null;

        this.localIND_ACCT_NUMBER = param;
    }

    public boolean isIND_DISBURSED_AMTSpecified() {
        return localIND_DISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_DISBURSED_AMT() {
        return localIND_DISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param IND_DISBURSED_AMT
     */
    public void setIND_DISBURSED_AMT(java.lang.String param) {
        localIND_DISBURSED_AMTTracker = param != null;

        this.localIND_DISBURSED_AMT = param;
    }

    public boolean isIND_CURRENT_BALSpecified() {
        return localIND_CURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_CURRENT_BAL() {
        return localIND_CURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param IND_CURRENT_BAL
     */
    public void setIND_CURRENT_BAL(java.lang.String param) {
        localIND_CURRENT_BALTracker = param != null;

        this.localIND_CURRENT_BAL = param;
    }

    public boolean isIND_INSTALLMENT_AMTSpecified() {
        return localIND_INSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_INSTALLMENT_AMT() {
        return localIND_INSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param IND_INSTALLMENT_AMT
     */
    public void setIND_INSTALLMENT_AMT(java.lang.String param) {
        localIND_INSTALLMENT_AMTTracker = param != null;

        this.localIND_INSTALLMENT_AMT = param;
    }

    public boolean isIND_OVERDUE_AMTSpecified() {
        return localIND_OVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_OVERDUE_AMT() {
        return localIND_OVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param IND_OVERDUE_AMT
     */
    public void setIND_OVERDUE_AMT(java.lang.String param) {
        localIND_OVERDUE_AMTTracker = param != null;

        this.localIND_OVERDUE_AMT = param;
    }

    public boolean isIND_WRITE_OFF_AMTSpecified() {
        return localIND_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_WRITE_OFF_AMT() {
        return localIND_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param IND_WRITE_OFF_AMT
     */
    public void setIND_WRITE_OFF_AMT(java.lang.String param) {
        localIND_WRITE_OFF_AMTTracker = param != null;

        this.localIND_WRITE_OFF_AMT = param;
    }

    public boolean isIND_DISBURSED_DTSpecified() {
        return localIND_DISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_DISBURSED_DT() {
        return localIND_DISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param IND_DISBURSED_DT
     */
    public void setIND_DISBURSED_DT(java.lang.String param) {
        localIND_DISBURSED_DTTracker = param != null;

        this.localIND_DISBURSED_DT = param;
    }

    public boolean isIND_CLOSED_DTSpecified() {
        return localIND_CLOSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_CLOSED_DT() {
        return localIND_CLOSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param IND_CLOSED_DT
     */
    public void setIND_CLOSED_DT(java.lang.String param) {
        localIND_CLOSED_DTTracker = param != null;

        this.localIND_CLOSED_DT = param;
    }

    public boolean isIND_RECENT_DELINQ_DTSpecified() {
        return localIND_RECENT_DELINQ_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_RECENT_DELINQ_DT() {
        return localIND_RECENT_DELINQ_DT;
    }

    /**
     * Auto generated setter method
     * @param param IND_RECENT_DELINQ_DT
     */
    public void setIND_RECENT_DELINQ_DT(java.lang.String param) {
        localIND_RECENT_DELINQ_DTTracker = param != null;

        this.localIND_RECENT_DELINQ_DT = param;
    }

    public boolean isIND_DPDSpecified() {
        return localIND_DPDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_DPD() {
        return localIND_DPD;
    }

    /**
     * Auto generated setter method
     * @param param IND_DPD
     */
    public void setIND_DPD(java.lang.String param) {
        localIND_DPDTracker = param != null;

        this.localIND_DPD = param;
    }

    public boolean isIND_INQ_CNTSpecified() {
        return localIND_INQ_CNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_INQ_CNT() {
        return localIND_INQ_CNT;
    }

    /**
     * Auto generated setter method
     * @param param IND_INQ_CNT
     */
    public void setIND_INQ_CNT(java.lang.String param) {
        localIND_INQ_CNTTracker = param != null;

        this.localIND_INQ_CNT = param;
    }

    public boolean isIND_INFO_AS_ONSpecified() {
        return localIND_INFO_AS_ONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_INFO_AS_ON() {
        return localIND_INFO_AS_ON;
    }

    /**
     * Auto generated setter method
     * @param param IND_INFO_AS_ON
     */
    public void setIND_INFO_AS_ON(java.lang.String param) {
        localIND_INFO_AS_ONTracker = param != null;

        this.localIND_INFO_AS_ON = param;
    }

    public boolean isIND_WORST_DELEQUENCY_AMOUNTSpecified() {
        return localIND_WORST_DELEQUENCY_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_WORST_DELEQUENCY_AMOUNT() {
        return localIND_WORST_DELEQUENCY_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param IND_WORST_DELEQUENCY_AMOUNT
     */
    public void setIND_WORST_DELEQUENCY_AMOUNT(java.lang.String param) {
        localIND_WORST_DELEQUENCY_AMOUNTTracker = param != null;

        this.localIND_WORST_DELEQUENCY_AMOUNT = param;
    }

    public boolean isIND_PAYMENT_HISTORYSpecified() {
        return localIND_PAYMENT_HISTORYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_PAYMENT_HISTORY() {
        return localIND_PAYMENT_HISTORY;
    }

    /**
     * Auto generated setter method
     * @param param IND_PAYMENT_HISTORY
     */
    public void setIND_PAYMENT_HISTORY(java.lang.String param) {
        localIND_PAYMENT_HISTORYTracker = param != null;

        this.localIND_PAYMENT_HISTORY = param;
    }

    public boolean isIND_IS_ACTIVE_BORROWERSpecified() {
        return localIND_IS_ACTIVE_BORROWERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_IS_ACTIVE_BORROWER() {
        return localIND_IS_ACTIVE_BORROWER;
    }

    /**
     * Auto generated setter method
     * @param param IND_IS_ACTIVE_BORROWER
     */
    public void setIND_IS_ACTIVE_BORROWER(java.lang.String param) {
        localIND_IS_ACTIVE_BORROWERTracker = param != null;

        this.localIND_IS_ACTIVE_BORROWER = param;
    }

    public boolean isIND_NO_OF_BORROWERSSpecified() {
        return localIND_NO_OF_BORROWERSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_NO_OF_BORROWERS() {
        return localIND_NO_OF_BORROWERS;
    }

    /**
     * Auto generated setter method
     * @param param IND_NO_OF_BORROWERS
     */
    public void setIND_NO_OF_BORROWERS(java.lang.String param) {
        localIND_NO_OF_BORROWERSTracker = param != null;

        this.localIND_NO_OF_BORROWERS = param;
    }

    public boolean isIND_COMMENT_RESSpecified() {
        return localIND_COMMENT_RESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_COMMENT_RES() {
        return localIND_COMMENT_RES;
    }

    /**
     * Auto generated setter method
     * @param param IND_COMMENT_RES
     */
    public void setIND_COMMENT_RES(java.lang.String param) {
        localIND_COMMENT_RESTracker = param != null;

        this.localIND_COMMENT_RES = param;
    }

    public boolean isGRP_STATUS_PRSpecified() {
        return localGRP_STATUS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_STATUS_PR() {
        return localGRP_STATUS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_STATUS_PR
     */
    public void setGRP_STATUS_PR(java.lang.String param) {
        localGRP_STATUS_PRTracker = param != null;

        this.localGRP_STATUS_PR = param;
    }

    public boolean isGRP_NO_OF_DEFAULT_ACCOUNTS_PRSpecified() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_DEFAULT_ACCOUNTS_PR() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_DEFAULT_ACCOUNTS_PR
     */
    public void setGRP_NO_OF_DEFAULT_ACCOUNTS_PR(java.lang.String param) {
        localGRP_NO_OF_DEFAULT_ACCOUNTS_PRTracker = param != null;

        this.localGRP_NO_OF_DEFAULT_ACCOUNTS_PR = param;
    }

    public boolean isGRP_TOTAL_RESPONSES_PRSpecified() {
        return localGRP_TOTAL_RESPONSES_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TOTAL_RESPONSES_PR() {
        return localGRP_TOTAL_RESPONSES_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TOTAL_RESPONSES_PR
     */
    public void setGRP_TOTAL_RESPONSES_PR(java.lang.String param) {
        localGRP_TOTAL_RESPONSES_PRTracker = param != null;

        this.localGRP_TOTAL_RESPONSES_PR = param;
    }

    public boolean isGRP_NO_OF_CLOSED_ACCOUNTS_PRSpecified() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_CLOSED_ACCOUNTS_PR() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_CLOSED_ACCOUNTS_PR
     */
    public void setGRP_NO_OF_CLOSED_ACCOUNTS_PR(java.lang.String param) {
        localGRP_NO_OF_CLOSED_ACCOUNTS_PRTracker = param != null;

        this.localGRP_NO_OF_CLOSED_ACCOUNTS_PR = param;
    }

    public boolean isGRP_NO_OF_ACTIVE_ACCOUNTS_PRSpecified() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_ACTIVE_ACCOUNTS_PR() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_ACTIVE_ACCOUNTS_PR
     */
    public void setGRP_NO_OF_ACTIVE_ACCOUNTS_PR(java.lang.String param) {
        localGRP_NO_OF_ACTIVE_ACCOUNTS_PRTracker = param != null;

        this.localGRP_NO_OF_ACTIVE_ACCOUNTS_PR = param;
    }

    public boolean isGRP_NO_OF_OTHER_MFIS_PRSpecified() {
        return localGRP_NO_OF_OTHER_MFIS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_OTHER_MFIS_PR() {
        return localGRP_NO_OF_OTHER_MFIS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_OTHER_MFIS_PR
     */
    public void setGRP_NO_OF_OTHER_MFIS_PR(java.lang.String param) {
        localGRP_NO_OF_OTHER_MFIS_PRTracker = param != null;

        this.localGRP_NO_OF_OTHER_MFIS_PR = param;
    }

    public boolean isGRP_OWN_MFI_INDECATOR_PRSpecified() {
        return localGRP_OWN_MFI_INDECATOR_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_OWN_MFI_INDECATOR_PR() {
        return localGRP_OWN_MFI_INDECATOR_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_OWN_MFI_INDECATOR_PR
     */
    public void setGRP_OWN_MFI_INDECATOR_PR(java.lang.String param) {
        localGRP_OWN_MFI_INDECATOR_PRTracker = param != null;

        this.localGRP_OWN_MFI_INDECATOR_PR = param;
    }

    public boolean isGRP_TTL_OWN_DISBURSD_AMT_PRSpecified() {
        return localGRP_TTL_OWN_DISBURSD_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_DISBURSD_AMT_PR() {
        return localGRP_TTL_OWN_DISBURSD_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_DISBURSD_AMT_PR
     */
    public void setGRP_TTL_OWN_DISBURSD_AMT_PR(java.lang.String param) {
        localGRP_TTL_OWN_DISBURSD_AMT_PRTracker = param != null;

        this.localGRP_TTL_OWN_DISBURSD_AMT_PR = param;
    }

    public boolean isGRP_TTL_OTHER_DISBURSD_AMT_PRSpecified() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_DISBURSD_AMT_PR() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_DISBURSD_AMT_PR
     */
    public void setGRP_TTL_OTHER_DISBURSD_AMT_PR(java.lang.String param) {
        localGRP_TTL_OTHER_DISBURSD_AMT_PRTracker = param != null;

        this.localGRP_TTL_OTHER_DISBURSD_AMT_PR = param;
    }

    public boolean isGRP_TTL_OWN_CURRENT_BAL_PRSpecified() {
        return localGRP_TTL_OWN_CURRENT_BAL_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_CURRENT_BAL_PR() {
        return localGRP_TTL_OWN_CURRENT_BAL_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_CURRENT_BAL_PR
     */
    public void setGRP_TTL_OWN_CURRENT_BAL_PR(java.lang.String param) {
        localGRP_TTL_OWN_CURRENT_BAL_PRTracker = param != null;

        this.localGRP_TTL_OWN_CURRENT_BAL_PR = param;
    }

    public boolean isGRP_TTL_OTHER_CURRENT_BAL_PRSpecified() {
        return localGRP_TTL_OTHER_CURRENT_BAL_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_CURRENT_BAL_PR() {
        return localGRP_TTL_OTHER_CURRENT_BAL_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_CURRENT_BAL_PR
     */
    public void setGRP_TTL_OTHER_CURRENT_BAL_PR(java.lang.String param) {
        localGRP_TTL_OTHER_CURRENT_BAL_PRTracker = param != null;

        this.localGRP_TTL_OTHER_CURRENT_BAL_PR = param;
    }

    public boolean isGRP_TTL_OWN_INSTLMNT_AMT_PRSpecified() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_INSTLMNT_AMT_PR() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_INSTLMNT_AMT_PR
     */
    public void setGRP_TTL_OWN_INSTLMNT_AMT_PR(java.lang.String param) {
        localGRP_TTL_OWN_INSTLMNT_AMT_PRTracker = param != null;

        this.localGRP_TTL_OWN_INSTLMNT_AMT_PR = param;
    }

    public boolean isGRP_TTL_OTHER_INSTLMNT_AMT_PRSpecified() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_INSTLMNT_AMT_PR() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_INSTLMNT_AMT_PR
     */
    public void setGRP_TTL_OTHER_INSTLMNT_AMT_PR(java.lang.String param) {
        localGRP_TTL_OTHER_INSTLMNT_AMT_PRTracker = param != null;

        this.localGRP_TTL_OTHER_INSTLMNT_AMT_PR = param;
    }

    public boolean isGRP_MAX_WORST_DELEQUENCY_PRSpecified() {
        return localGRP_MAX_WORST_DELEQUENCY_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MAX_WORST_DELEQUENCY_PR() {
        return localGRP_MAX_WORST_DELEQUENCY_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MAX_WORST_DELEQUENCY_PR
     */
    public void setGRP_MAX_WORST_DELEQUENCY_PR(java.lang.String param) {
        localGRP_MAX_WORST_DELEQUENCY_PRTracker = param != null;

        this.localGRP_MAX_WORST_DELEQUENCY_PR = param;
    }

    public boolean isGRP_ERRORS_PRSpecified() {
        return localGRP_ERRORS_PRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ERRORS_PR() {
        return localGRP_ERRORS_PR;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ERRORS_PR
     */
    public void setGRP_ERRORS_PR(java.lang.String param) {
        localGRP_ERRORS_PRTracker = param != null;

        this.localGRP_ERRORS_PR = param;
    }

    public boolean isGRP_STATUS_SESpecified() {
        return localGRP_STATUS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_STATUS_SE() {
        return localGRP_STATUS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_STATUS_SE
     */
    public void setGRP_STATUS_SE(java.lang.String param) {
        localGRP_STATUS_SETracker = param != null;

        this.localGRP_STATUS_SE = param;
    }

    public boolean isGRP_NO_OF_DEFAULT_ACCOUNTS_SESpecified() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_DEFAULT_ACCOUNTS_SE() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_DEFAULT_ACCOUNTS_SE
     */
    public void setGRP_NO_OF_DEFAULT_ACCOUNTS_SE(java.lang.String param) {
        localGRP_NO_OF_DEFAULT_ACCOUNTS_SETracker = param != null;

        this.localGRP_NO_OF_DEFAULT_ACCOUNTS_SE = param;
    }

    public boolean isGRP_TOTAL_RESPONSES_SESpecified() {
        return localGRP_TOTAL_RESPONSES_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TOTAL_RESPONSES_SE() {
        return localGRP_TOTAL_RESPONSES_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TOTAL_RESPONSES_SE
     */
    public void setGRP_TOTAL_RESPONSES_SE(java.lang.String param) {
        localGRP_TOTAL_RESPONSES_SETracker = param != null;

        this.localGRP_TOTAL_RESPONSES_SE = param;
    }

    public boolean isGRP_NO_OF_CLOSED_ACCOUNTS_SESpecified() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_CLOSED_ACCOUNTS_SE() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_CLOSED_ACCOUNTS_SE
     */
    public void setGRP_NO_OF_CLOSED_ACCOUNTS_SE(java.lang.String param) {
        localGRP_NO_OF_CLOSED_ACCOUNTS_SETracker = param != null;

        this.localGRP_NO_OF_CLOSED_ACCOUNTS_SE = param;
    }

    public boolean isGRP_NO_OF_ACTIVE_ACCOUNTS_SESpecified() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_ACTIVE_ACCOUNTS_SE() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_ACTIVE_ACCOUNTS_SE
     */
    public void setGRP_NO_OF_ACTIVE_ACCOUNTS_SE(java.lang.String param) {
        localGRP_NO_OF_ACTIVE_ACCOUNTS_SETracker = param != null;

        this.localGRP_NO_OF_ACTIVE_ACCOUNTS_SE = param;
    }

    public boolean isGRP_NO_OF_OTHER_MFIS_SESpecified() {
        return localGRP_NO_OF_OTHER_MFIS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_OTHER_MFIS_SE() {
        return localGRP_NO_OF_OTHER_MFIS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_OTHER_MFIS_SE
     */
    public void setGRP_NO_OF_OTHER_MFIS_SE(java.lang.String param) {
        localGRP_NO_OF_OTHER_MFIS_SETracker = param != null;

        this.localGRP_NO_OF_OTHER_MFIS_SE = param;
    }

    public boolean isGRP_OWN_MFI_INDECATOR_SESpecified() {
        return localGRP_OWN_MFI_INDECATOR_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_OWN_MFI_INDECATOR_SE() {
        return localGRP_OWN_MFI_INDECATOR_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_OWN_MFI_INDECATOR_SE
     */
    public void setGRP_OWN_MFI_INDECATOR_SE(java.lang.String param) {
        localGRP_OWN_MFI_INDECATOR_SETracker = param != null;

        this.localGRP_OWN_MFI_INDECATOR_SE = param;
    }

    public boolean isGRP_TTL_OWN_DISBURSD_AMT_SESpecified() {
        return localGRP_TTL_OWN_DISBURSD_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_DISBURSD_AMT_SE() {
        return localGRP_TTL_OWN_DISBURSD_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_DISBURSD_AMT_SE
     */
    public void setGRP_TTL_OWN_DISBURSD_AMT_SE(java.lang.String param) {
        localGRP_TTL_OWN_DISBURSD_AMT_SETracker = param != null;

        this.localGRP_TTL_OWN_DISBURSD_AMT_SE = param;
    }

    public boolean isGRP_TTL_OTHER_DISBURSD_AMT_SESpecified() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_DISBURSD_AMT_SE() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_DISBURSD_AMT_SE
     */
    public void setGRP_TTL_OTHER_DISBURSD_AMT_SE(java.lang.String param) {
        localGRP_TTL_OTHER_DISBURSD_AMT_SETracker = param != null;

        this.localGRP_TTL_OTHER_DISBURSD_AMT_SE = param;
    }

    public boolean isGRP_TTL_OWN_CURRENT_BAL_SESpecified() {
        return localGRP_TTL_OWN_CURRENT_BAL_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_CURRENT_BAL_SE() {
        return localGRP_TTL_OWN_CURRENT_BAL_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_CURRENT_BAL_SE
     */
    public void setGRP_TTL_OWN_CURRENT_BAL_SE(java.lang.String param) {
        localGRP_TTL_OWN_CURRENT_BAL_SETracker = param != null;

        this.localGRP_TTL_OWN_CURRENT_BAL_SE = param;
    }

    public boolean isGRP_TTL_OTHER_CURRENT_BAL_SESpecified() {
        return localGRP_TTL_OTHER_CURRENT_BAL_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_CURRENT_BAL_SE() {
        return localGRP_TTL_OTHER_CURRENT_BAL_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_CURRENT_BAL_SE
     */
    public void setGRP_TTL_OTHER_CURRENT_BAL_SE(java.lang.String param) {
        localGRP_TTL_OTHER_CURRENT_BAL_SETracker = param != null;

        this.localGRP_TTL_OTHER_CURRENT_BAL_SE = param;
    }

    public boolean isGRP_TTL_OWN_INSTLMNT_AMT_SESpecified() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_INSTLMNT_AMT_SE() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_INSTLMNT_AMT_SE
     */
    public void setGRP_TTL_OWN_INSTLMNT_AMT_SE(java.lang.String param) {
        localGRP_TTL_OWN_INSTLMNT_AMT_SETracker = param != null;

        this.localGRP_TTL_OWN_INSTLMNT_AMT_SE = param;
    }

    public boolean isGRP_TTL_OTHER_INSTLMNT_AMT_SESpecified() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_INSTLMNT_AMT_SE() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_INSTLMNT_AMT_SE
     */
    public void setGRP_TTL_OTHER_INSTLMNT_AMT_SE(java.lang.String param) {
        localGRP_TTL_OTHER_INSTLMNT_AMT_SETracker = param != null;

        this.localGRP_TTL_OTHER_INSTLMNT_AMT_SE = param;
    }

    public boolean isGRP_MAX_WORST_DELEQUENCY_SESpecified() {
        return localGRP_MAX_WORST_DELEQUENCY_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MAX_WORST_DELEQUENCY_SE() {
        return localGRP_MAX_WORST_DELEQUENCY_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MAX_WORST_DELEQUENCY_SE
     */
    public void setGRP_MAX_WORST_DELEQUENCY_SE(java.lang.String param) {
        localGRP_MAX_WORST_DELEQUENCY_SETracker = param != null;

        this.localGRP_MAX_WORST_DELEQUENCY_SE = param;
    }

    public boolean isGRP_ERRORS_SESpecified() {
        return localGRP_ERRORS_SETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ERRORS_SE() {
        return localGRP_ERRORS_SE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ERRORS_SE
     */
    public void setGRP_ERRORS_SE(java.lang.String param) {
        localGRP_ERRORS_SETracker = param != null;

        this.localGRP_ERRORS_SE = param;
    }

    public boolean isGRP_STATUS_SMSpecified() {
        return localGRP_STATUS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_STATUS_SM() {
        return localGRP_STATUS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_STATUS_SM
     */
    public void setGRP_STATUS_SM(java.lang.String param) {
        localGRP_STATUS_SMTracker = param != null;

        this.localGRP_STATUS_SM = param;
    }

    public boolean isGRP_NO_OF_DEFAULT_ACCOUNTS_SMSpecified() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_DEFAULT_ACCOUNTS_SM() {
        return localGRP_NO_OF_DEFAULT_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_DEFAULT_ACCOUNTS_SM
     */
    public void setGRP_NO_OF_DEFAULT_ACCOUNTS_SM(java.lang.String param) {
        localGRP_NO_OF_DEFAULT_ACCOUNTS_SMTracker = param != null;

        this.localGRP_NO_OF_DEFAULT_ACCOUNTS_SM = param;
    }

    public boolean isGRP_TOTAL_RESPONSES_SMSpecified() {
        return localGRP_TOTAL_RESPONSES_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TOTAL_RESPONSES_SM() {
        return localGRP_TOTAL_RESPONSES_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TOTAL_RESPONSES_SM
     */
    public void setGRP_TOTAL_RESPONSES_SM(java.lang.String param) {
        localGRP_TOTAL_RESPONSES_SMTracker = param != null;

        this.localGRP_TOTAL_RESPONSES_SM = param;
    }

    public boolean isGRP_NO_OF_CLOSED_ACCOUNTS_SMSpecified() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_CLOSED_ACCOUNTS_SM() {
        return localGRP_NO_OF_CLOSED_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_CLOSED_ACCOUNTS_SM
     */
    public void setGRP_NO_OF_CLOSED_ACCOUNTS_SM(java.lang.String param) {
        localGRP_NO_OF_CLOSED_ACCOUNTS_SMTracker = param != null;

        this.localGRP_NO_OF_CLOSED_ACCOUNTS_SM = param;
    }

    public boolean isGRP_NO_OF_ACTIVE_ACCOUNTS_SMSpecified() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_ACTIVE_ACCOUNTS_SM() {
        return localGRP_NO_OF_ACTIVE_ACCOUNTS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_ACTIVE_ACCOUNTS_SM
     */
    public void setGRP_NO_OF_ACTIVE_ACCOUNTS_SM(java.lang.String param) {
        localGRP_NO_OF_ACTIVE_ACCOUNTS_SMTracker = param != null;

        this.localGRP_NO_OF_ACTIVE_ACCOUNTS_SM = param;
    }

    public boolean isGRP_NO_OF_OTHER_MFIS_SMSpecified() {
        return localGRP_NO_OF_OTHER_MFIS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_OTHER_MFIS_SM() {
        return localGRP_NO_OF_OTHER_MFIS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_OTHER_MFIS_SM
     */
    public void setGRP_NO_OF_OTHER_MFIS_SM(java.lang.String param) {
        localGRP_NO_OF_OTHER_MFIS_SMTracker = param != null;

        this.localGRP_NO_OF_OTHER_MFIS_SM = param;
    }

    public boolean isGRP_OWN_MFI_INDECATOR_SMSpecified() {
        return localGRP_OWN_MFI_INDECATOR_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_OWN_MFI_INDECATOR_SM() {
        return localGRP_OWN_MFI_INDECATOR_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_OWN_MFI_INDECATOR_SM
     */
    public void setGRP_OWN_MFI_INDECATOR_SM(java.lang.String param) {
        localGRP_OWN_MFI_INDECATOR_SMTracker = param != null;

        this.localGRP_OWN_MFI_INDECATOR_SM = param;
    }

    public boolean isGRP_TTL_OWN_DISBURSD_AMT_SMSpecified() {
        return localGRP_TTL_OWN_DISBURSD_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_DISBURSD_AMT_SM() {
        return localGRP_TTL_OWN_DISBURSD_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_DISBURSD_AMT_SM
     */
    public void setGRP_TTL_OWN_DISBURSD_AMT_SM(java.lang.String param) {
        localGRP_TTL_OWN_DISBURSD_AMT_SMTracker = param != null;

        this.localGRP_TTL_OWN_DISBURSD_AMT_SM = param;
    }

    public boolean isGRP_TTL_OTHER_DISBURSD_AMT_SMSpecified() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_DISBURSD_AMT_SM() {
        return localGRP_TTL_OTHER_DISBURSD_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_DISBURSD_AMT_SM
     */
    public void setGRP_TTL_OTHER_DISBURSD_AMT_SM(java.lang.String param) {
        localGRP_TTL_OTHER_DISBURSD_AMT_SMTracker = param != null;

        this.localGRP_TTL_OTHER_DISBURSD_AMT_SM = param;
    }

    public boolean isGRP_TTL_OWN_CURRENT_BAL_SMSpecified() {
        return localGRP_TTL_OWN_CURRENT_BAL_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_CURRENT_BAL_SM() {
        return localGRP_TTL_OWN_CURRENT_BAL_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_CURRENT_BAL_SM
     */
    public void setGRP_TTL_OWN_CURRENT_BAL_SM(java.lang.String param) {
        localGRP_TTL_OWN_CURRENT_BAL_SMTracker = param != null;

        this.localGRP_TTL_OWN_CURRENT_BAL_SM = param;
    }

    public boolean isGRP_TTL_OTHER_CURRENT_BAL_SMSpecified() {
        return localGRP_TTL_OTHER_CURRENT_BAL_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_CURRENT_BAL_SM() {
        return localGRP_TTL_OTHER_CURRENT_BAL_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_CURRENT_BAL_SM
     */
    public void setGRP_TTL_OTHER_CURRENT_BAL_SM(java.lang.String param) {
        localGRP_TTL_OTHER_CURRENT_BAL_SMTracker = param != null;

        this.localGRP_TTL_OTHER_CURRENT_BAL_SM = param;
    }

    public boolean isGRP_TTL_OWN_INSTLMNT_AMT_SMSpecified() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OWN_INSTLMNT_AMT_SM() {
        return localGRP_TTL_OWN_INSTLMNT_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OWN_INSTLMNT_AMT_SM
     */
    public void setGRP_TTL_OWN_INSTLMNT_AMT_SM(java.lang.String param) {
        localGRP_TTL_OWN_INSTLMNT_AMT_SMTracker = param != null;

        this.localGRP_TTL_OWN_INSTLMNT_AMT_SM = param;
    }

    public boolean isGRP_TTL_OTHER_INSTLMNT_AMT_SMSpecified() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_TTL_OTHER_INSTLMNT_AMT_SM() {
        return localGRP_TTL_OTHER_INSTLMNT_AMT_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_TTL_OTHER_INSTLMNT_AMT_SM
     */
    public void setGRP_TTL_OTHER_INSTLMNT_AMT_SM(java.lang.String param) {
        localGRP_TTL_OTHER_INSTLMNT_AMT_SMTracker = param != null;

        this.localGRP_TTL_OTHER_INSTLMNT_AMT_SM = param;
    }

    public boolean isGRP_MAX_WORST_DELEQUENCY_SMSpecified() {
        return localGRP_MAX_WORST_DELEQUENCY_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MAX_WORST_DELEQUENCY_SM() {
        return localGRP_MAX_WORST_DELEQUENCY_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MAX_WORST_DELEQUENCY_SM
     */
    public void setGRP_MAX_WORST_DELEQUENCY_SM(java.lang.String param) {
        localGRP_MAX_WORST_DELEQUENCY_SMTracker = param != null;

        this.localGRP_MAX_WORST_DELEQUENCY_SM = param;
    }

    public boolean isGRP_ERRORS_SMSpecified() {
        return localGRP_ERRORS_SMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ERRORS_SM() {
        return localGRP_ERRORS_SM;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ERRORS_SM
     */
    public void setGRP_ERRORS_SM(java.lang.String param) {
        localGRP_ERRORS_SMTracker = param != null;

        this.localGRP_ERRORS_SM = param;
    }

    public boolean isGRP_MATCHED_TYPESpecified() {
        return localGRP_MATCHED_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MATCHED_TYPE() {
        return localGRP_MATCHED_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MATCHED_TYPE
     */
    public void setGRP_MATCHED_TYPE(java.lang.String param) {
        localGRP_MATCHED_TYPETracker = param != null;

        this.localGRP_MATCHED_TYPE = param;
    }

    public boolean isGRP_MFISpecified() {
        return localGRP_MFITracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MFI() {
        return localGRP_MFI;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MFI
     */
    public void setGRP_MFI(java.lang.String param) {
        localGRP_MFITracker = param != null;

        this.localGRP_MFI = param;
    }

    public boolean isGRP_MFI_IDSpecified() {
        return localGRP_MFI_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_MFI_ID() {
        return localGRP_MFI_ID;
    }

    /**
     * Auto generated setter method
     * @param param GRP_MFI_ID
     */
    public void setGRP_MFI_ID(java.lang.String param) {
        localGRP_MFI_IDTracker = param != null;

        this.localGRP_MFI_ID = param;
    }

    public boolean isGRP_BRANCHSpecified() {
        return localGRP_BRANCHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_BRANCH() {
        return localGRP_BRANCH;
    }

    /**
     * Auto generated setter method
     * @param param GRP_BRANCH
     */
    public void setGRP_BRANCH(java.lang.String param) {
        localGRP_BRANCHTracker = param != null;

        this.localGRP_BRANCH = param;
    }

    public boolean isGRP_KENDRASpecified() {
        return localGRP_KENDRATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_KENDRA() {
        return localGRP_KENDRA;
    }

    /**
     * Auto generated setter method
     * @param param GRP_KENDRA
     */
    public void setGRP_KENDRA(java.lang.String param) {
        localGRP_KENDRATracker = param != null;

        this.localGRP_KENDRA = param;
    }

    public boolean isGRP_NAMESpecified() {
        return localGRP_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NAME() {
        return localGRP_NAME;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NAME
     */
    public void setGRP_NAME(java.lang.String param) {
        localGRP_NAMETracker = param != null;

        this.localGRP_NAME = param;
    }

    public boolean isGRP_SPOUSESpecified() {
        return localGRP_SPOUSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_SPOUSE() {
        return localGRP_SPOUSE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_SPOUSE
     */
    public void setGRP_SPOUSE(java.lang.String param) {
        localGRP_SPOUSETracker = param != null;

        this.localGRP_SPOUSE = param;
    }

    public boolean isGRP_FATHERSpecified() {
        return localGRP_FATHERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_FATHER() {
        return localGRP_FATHER;
    }

    /**
     * Auto generated setter method
     * @param param GRP_FATHER
     */
    public void setGRP_FATHER(java.lang.String param) {
        localGRP_FATHERTracker = param != null;

        this.localGRP_FATHER = param;
    }

    public boolean isGRP_CNSMR_MBR_IDSpecified() {
        return localGRP_CNSMR_MBR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_CNSMR_MBR_ID() {
        return localGRP_CNSMR_MBR_ID;
    }

    /**
     * Auto generated setter method
     * @param param GRP_CNSMR_MBR_ID
     */
    public void setGRP_CNSMR_MBR_ID(java.lang.String param) {
        localGRP_CNSMR_MBR_IDTracker = param != null;

        this.localGRP_CNSMR_MBR_ID = param;
    }

    public boolean isGRP_DOBSpecified() {
        return localGRP_DOBTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_DOB() {
        return localGRP_DOB;
    }

    /**
     * Auto generated setter method
     * @param param GRP_DOB
     */
    public void setGRP_DOB(java.lang.String param) {
        localGRP_DOBTracker = param != null;

        this.localGRP_DOB = param;
    }

    public boolean isGRP_AGESpecified() {
        return localGRP_AGETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_AGE() {
        return localGRP_AGE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_AGE
     */
    public void setGRP_AGE(java.lang.String param) {
        localGRP_AGETracker = param != null;

        this.localGRP_AGE = param;
    }

    public boolean isGRP_AGE_AS_ONSpecified() {
        return localGRP_AGE_AS_ONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_AGE_AS_ON() {
        return localGRP_AGE_AS_ON;
    }

    /**
     * Auto generated setter method
     * @param param GRP_AGE_AS_ON
     */
    public void setGRP_AGE_AS_ON(java.lang.String param) {
        localGRP_AGE_AS_ONTracker = param != null;

        this.localGRP_AGE_AS_ON = param;
    }

    public boolean isGRP_PHONESpecified() {
        return localGRP_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_PHONE() {
        return localGRP_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_PHONE
     */
    public void setGRP_PHONE(java.lang.String param) {
        localGRP_PHONETracker = param != null;

        this.localGRP_PHONE = param;
    }

    public boolean isGRP_ADDRESS_1Specified() {
        return localGRP_ADDRESS_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ADDRESS_1() {
        return localGRP_ADDRESS_1;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ADDRESS_1
     */
    public void setGRP_ADDRESS_1(java.lang.String param) {
        localGRP_ADDRESS_1Tracker = param != null;

        this.localGRP_ADDRESS_1 = param;
    }

    public boolean isGRP_ADDRESS_2Specified() {
        return localGRP_ADDRESS_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ADDRESS_2() {
        return localGRP_ADDRESS_2;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ADDRESS_2
     */
    public void setGRP_ADDRESS_2(java.lang.String param) {
        localGRP_ADDRESS_2Tracker = param != null;

        this.localGRP_ADDRESS_2 = param;
    }

    public boolean isGRP_REL_TYP_1Specified() {
        return localGRP_REL_TYP_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_REL_TYP_1() {
        return localGRP_REL_TYP_1;
    }

    /**
     * Auto generated setter method
     * @param param GRP_REL_TYP_1
     */
    public void setGRP_REL_TYP_1(java.lang.String param) {
        localGRP_REL_TYP_1Tracker = param != null;

        this.localGRP_REL_TYP_1 = param;
    }

    public boolean isGRP_REL_NM_1Specified() {
        return localGRP_REL_NM_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_REL_NM_1() {
        return localGRP_REL_NM_1;
    }

    /**
     * Auto generated setter method
     * @param param GRP_REL_NM_1
     */
    public void setGRP_REL_NM_1(java.lang.String param) {
        localGRP_REL_NM_1Tracker = param != null;

        this.localGRP_REL_NM_1 = param;
    }

    public boolean isGRP_REL_TYP_2Specified() {
        return localGRP_REL_TYP_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_REL_TYP_2() {
        return localGRP_REL_TYP_2;
    }

    /**
     * Auto generated setter method
     * @param param GRP_REL_TYP_2
     */
    public void setGRP_REL_TYP_2(java.lang.String param) {
        localGRP_REL_TYP_2Tracker = param != null;

        this.localGRP_REL_TYP_2 = param;
    }

    public boolean isGRP_REL_NM_2Specified() {
        return localGRP_REL_NM_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_REL_NM_2() {
        return localGRP_REL_NM_2;
    }

    /**
     * Auto generated setter method
     * @param param GRP_REL_NM_2
     */
    public void setGRP_REL_NM_2(java.lang.String param) {
        localGRP_REL_NM_2Tracker = param != null;

        this.localGRP_REL_NM_2 = param;
    }

    public boolean isGRP_ID_TYP_1Specified() {
        return localGRP_ID_TYP_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ID_TYP_1() {
        return localGRP_ID_TYP_1;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ID_TYP_1
     */
    public void setGRP_ID_TYP_1(java.lang.String param) {
        localGRP_ID_TYP_1Tracker = param != null;

        this.localGRP_ID_TYP_1 = param;
    }

    public boolean isGRP_ID_VALUE_1Specified() {
        return localGRP_ID_VALUE_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ID_VALUE_1() {
        return localGRP_ID_VALUE_1;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ID_VALUE_1
     */
    public void setGRP_ID_VALUE_1(java.lang.String param) {
        localGRP_ID_VALUE_1Tracker = param != null;

        this.localGRP_ID_VALUE_1 = param;
    }

    public boolean isGRP_ID_TYP_2Specified() {
        return localGRP_ID_TYP_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ID_TYP_2() {
        return localGRP_ID_TYP_2;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ID_TYP_2
     */
    public void setGRP_ID_TYP_2(java.lang.String param) {
        localGRP_ID_TYP_2Tracker = param != null;

        this.localGRP_ID_TYP_2 = param;
    }

    public boolean isGRP_ID_VALUE_2Specified() {
        return localGRP_ID_VALUE_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ID_VALUE_2() {
        return localGRP_ID_VALUE_2;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ID_VALUE_2
     */
    public void setGRP_ID_VALUE_2(java.lang.String param) {
        localGRP_ID_VALUE_2Tracker = param != null;

        this.localGRP_ID_VALUE_2 = param;
    }

    public boolean isGRP_GROUP_IDSpecified() {
        return localGRP_GROUP_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_GROUP_ID() {
        return localGRP_GROUP_ID;
    }

    /**
     * Auto generated setter method
     * @param param GRP_GROUP_ID
     */
    public void setGRP_GROUP_ID(java.lang.String param) {
        localGRP_GROUP_IDTracker = param != null;

        this.localGRP_GROUP_ID = param;
    }

    public boolean isGRP_GROUP_CREATION_DATESpecified() {
        return localGRP_GROUP_CREATION_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_GROUP_CREATION_DATE() {
        return localGRP_GROUP_CREATION_DATE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_GROUP_CREATION_DATE
     */
    public void setGRP_GROUP_CREATION_DATE(java.lang.String param) {
        localGRP_GROUP_CREATION_DATETracker = param != null;

        this.localGRP_GROUP_CREATION_DATE = param;
    }

    public boolean isGRP_INSERT_DATESpecified() {
        return localGRP_INSERT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_INSERT_DATE() {
        return localGRP_INSERT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_INSERT_DATE
     */
    public void setGRP_INSERT_DATE(java.lang.String param) {
        localGRP_INSERT_DATETracker = param != null;

        this.localGRP_INSERT_DATE = param;
    }

    public boolean isGRP_ACCT_TYPESpecified() {
        return localGRP_ACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ACCT_TYPE() {
        return localGRP_ACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ACCT_TYPE
     */
    public void setGRP_ACCT_TYPE(java.lang.String param) {
        localGRP_ACCT_TYPETracker = param != null;

        this.localGRP_ACCT_TYPE = param;
    }

    public boolean isGRP_FREQSpecified() {
        return localGRP_FREQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_FREQ() {
        return localGRP_FREQ;
    }

    /**
     * Auto generated setter method
     * @param param GRP_FREQ
     */
    public void setGRP_FREQ(java.lang.String param) {
        localGRP_FREQTracker = param != null;

        this.localGRP_FREQ = param;
    }

    public boolean isGRP_STATUSSpecified() {
        return localGRP_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_STATUS() {
        return localGRP_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param GRP_STATUS
     */
    public void setGRP_STATUS(java.lang.String param) {
        localGRP_STATUSTracker = param != null;

        this.localGRP_STATUS = param;
    }

    public boolean isGRP_ACCT_NUMBERSpecified() {
        return localGRP_ACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_ACCT_NUMBER() {
        return localGRP_ACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param GRP_ACCT_NUMBER
     */
    public void setGRP_ACCT_NUMBER(java.lang.String param) {
        localGRP_ACCT_NUMBERTracker = param != null;

        this.localGRP_ACCT_NUMBER = param;
    }

    public boolean isGRP_DISBURSED_AMTSpecified() {
        return localGRP_DISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_DISBURSED_AMT() {
        return localGRP_DISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_DISBURSED_AMT
     */
    public void setGRP_DISBURSED_AMT(java.lang.String param) {
        localGRP_DISBURSED_AMTTracker = param != null;

        this.localGRP_DISBURSED_AMT = param;
    }

    public boolean isGRP_CURRENT_BALSpecified() {
        return localGRP_CURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_CURRENT_BAL() {
        return localGRP_CURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param GRP_CURRENT_BAL
     */
    public void setGRP_CURRENT_BAL(java.lang.String param) {
        localGRP_CURRENT_BALTracker = param != null;

        this.localGRP_CURRENT_BAL = param;
    }

    public boolean isGRP_INSTALLMENT_AMTSpecified() {
        return localGRP_INSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_INSTALLMENT_AMT() {
        return localGRP_INSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_INSTALLMENT_AMT
     */
    public void setGRP_INSTALLMENT_AMT(java.lang.String param) {
        localGRP_INSTALLMENT_AMTTracker = param != null;

        this.localGRP_INSTALLMENT_AMT = param;
    }

    public boolean isGRP_OVERDUE_AMTSpecified() {
        return localGRP_OVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_OVERDUE_AMT() {
        return localGRP_OVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_OVERDUE_AMT
     */
    public void setGRP_OVERDUE_AMT(java.lang.String param) {
        localGRP_OVERDUE_AMTTracker = param != null;

        this.localGRP_OVERDUE_AMT = param;
    }

    public boolean isGRP_WRITE_OFF_AMTSpecified() {
        return localGRP_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_WRITE_OFF_AMT() {
        return localGRP_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_WRITE_OFF_AMT
     */
    public void setGRP_WRITE_OFF_AMT(java.lang.String param) {
        localGRP_WRITE_OFF_AMTTracker = param != null;

        this.localGRP_WRITE_OFF_AMT = param;
    }

    public boolean isGRP_DISBURSED_DTSpecified() {
        return localGRP_DISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_DISBURSED_DT() {
        return localGRP_DISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_DISBURSED_DT
     */
    public void setGRP_DISBURSED_DT(java.lang.String param) {
        localGRP_DISBURSED_DTTracker = param != null;

        this.localGRP_DISBURSED_DT = param;
    }

    public boolean isGRP_CLOSED_DTSpecified() {
        return localGRP_CLOSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_CLOSED_DT() {
        return localGRP_CLOSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_CLOSED_DT
     */
    public void setGRP_CLOSED_DT(java.lang.String param) {
        localGRP_CLOSED_DTTracker = param != null;

        this.localGRP_CLOSED_DT = param;
    }

    public boolean isGRP_RECENT_DELINQ_DTSpecified() {
        return localGRP_RECENT_DELINQ_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_RECENT_DELINQ_DT() {
        return localGRP_RECENT_DELINQ_DT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_RECENT_DELINQ_DT
     */
    public void setGRP_RECENT_DELINQ_DT(java.lang.String param) {
        localGRP_RECENT_DELINQ_DTTracker = param != null;

        this.localGRP_RECENT_DELINQ_DT = param;
    }

    public boolean isGRP_DPDSpecified() {
        return localGRP_DPDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_DPD() {
        return localGRP_DPD;
    }

    /**
     * Auto generated setter method
     * @param param GRP_DPD
     */
    public void setGRP_DPD(java.lang.String param) {
        localGRP_DPDTracker = param != null;

        this.localGRP_DPD = param;
    }

    public boolean isGRP_INQ_CNTSpecified() {
        return localGRP_INQ_CNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_INQ_CNT() {
        return localGRP_INQ_CNT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_INQ_CNT
     */
    public void setGRP_INQ_CNT(java.lang.String param) {
        localGRP_INQ_CNTTracker = param != null;

        this.localGRP_INQ_CNT = param;
    }

    public boolean isGRP_INFO_AS_ONSpecified() {
        return localGRP_INFO_AS_ONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_INFO_AS_ON() {
        return localGRP_INFO_AS_ON;
    }

    /**
     * Auto generated setter method
     * @param param GRP_INFO_AS_ON
     */
    public void setGRP_INFO_AS_ON(java.lang.String param) {
        localGRP_INFO_AS_ONTracker = param != null;

        this.localGRP_INFO_AS_ON = param;
    }

    public boolean isGRP_WORST_DELEQUENCY_AMOUNTSpecified() {
        return localGRP_WORST_DELEQUENCY_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_WORST_DELEQUENCY_AMOUNT() {
        return localGRP_WORST_DELEQUENCY_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param GRP_WORST_DELEQUENCY_AMOUNT
     */
    public void setGRP_WORST_DELEQUENCY_AMOUNT(java.lang.String param) {
        localGRP_WORST_DELEQUENCY_AMOUNTTracker = param != null;

        this.localGRP_WORST_DELEQUENCY_AMOUNT = param;
    }

    public boolean isGRP_PAYMENT_HISTORYSpecified() {
        return localGRP_PAYMENT_HISTORYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_PAYMENT_HISTORY() {
        return localGRP_PAYMENT_HISTORY;
    }

    /**
     * Auto generated setter method
     * @param param GRP_PAYMENT_HISTORY
     */
    public void setGRP_PAYMENT_HISTORY(java.lang.String param) {
        localGRP_PAYMENT_HISTORYTracker = param != null;

        this.localGRP_PAYMENT_HISTORY = param;
    }

    public boolean isGRP_IS_ACTIVE_BORROWERSpecified() {
        return localGRP_IS_ACTIVE_BORROWERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_IS_ACTIVE_BORROWER() {
        return localGRP_IS_ACTIVE_BORROWER;
    }

    /**
     * Auto generated setter method
     * @param param GRP_IS_ACTIVE_BORROWER
     */
    public void setGRP_IS_ACTIVE_BORROWER(java.lang.String param) {
        localGRP_IS_ACTIVE_BORROWERTracker = param != null;

        this.localGRP_IS_ACTIVE_BORROWER = param;
    }

    public boolean isGRP_NO_OF_BORROWERSSpecified() {
        return localGRP_NO_OF_BORROWERSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_NO_OF_BORROWERS() {
        return localGRP_NO_OF_BORROWERS;
    }

    /**
     * Auto generated setter method
     * @param param GRP_NO_OF_BORROWERS
     */
    public void setGRP_NO_OF_BORROWERS(java.lang.String param) {
        localGRP_NO_OF_BORROWERSTracker = param != null;

        this.localGRP_NO_OF_BORROWERS = param;
    }

    public boolean isGRP_COMMENT_RESSpecified() {
        return localGRP_COMMENT_RESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGRP_COMMENT_RES() {
        return localGRP_COMMENT_RES;
    }

    /**
     * Auto generated setter method
     * @param param GRP_COMMENT_RES
     */
    public void setGRP_COMMENT_RES(java.lang.String param) {
        localGRP_COMMENT_RESTracker = param != null;

        this.localGRP_COMMENT_RES = param;
    }

    public boolean isMEMBER_NAMESpecified() {
        return localMEMBER_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_NAME() {
        return localMEMBER_NAME;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_NAME
     */
    public void setMEMBER_NAME(java.lang.String param) {
        localMEMBER_NAMETracker = param != null;

        this.localMEMBER_NAME = param;
    }

    public boolean isINQUIRY_DATESpecified() {
        return localINQUIRY_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINQUIRY_DATE() {
        return localINQUIRY_DATE;
    }

    /**
     * Auto generated setter method
     * @param param INQUIRY_DATE
     */
    public void setINQUIRY_DATE(java.lang.String param) {
        localINQUIRY_DATETracker = param != null;

        this.localINQUIRY_DATE = param;
    }

    public boolean isPURPOSESpecified() {
        return localPURPOSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPURPOSE() {
        return localPURPOSE;
    }

    /**
     * Auto generated setter method
     * @param param PURPOSE
     */
    public void setPURPOSE(java.lang.String param) {
        localPURPOSETracker = param != null;

        this.localPURPOSE = param;
    }

    public boolean isAMOUNTSpecified() {
        return localAMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNT() {
        return localAMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNT
     */
    public void setAMOUNT(java.lang.String param) {
        localAMOUNTTracker = param != null;

        this.localAMOUNT = param;
    }

    public boolean isIND_REMARKSpecified() {
        return localIND_REMARKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getIND_REMARK() {
        return localIND_REMARK;
    }

    /**
     * Auto generated setter method
     * @param param IND_REMARK
     */
    public void setIND_REMARK(java.lang.String param) {
        localIND_REMARKTracker = param != null;

        this.localIND_REMARK = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ChmAorSRespType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ChmAorSRespType", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_REQUESTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_REQUEST", xmlWriter);

            if (localDATE_OF_REQUEST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_REQUEST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_REQUEST);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR", xmlWriter);

            if (localPREPARED_FOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FOR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR_ID", xmlWriter);

            if (localPREPARED_FOR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_ISSUETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_ISSUE", xmlWriter);

            if (localDATE_OF_ISSUE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_ISSUE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_ISSUE);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORT_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORT_ID", xmlWriter);

            if (localREPORT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localNAME_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NAME_IQ", xmlWriter);

            if (localNAME_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localSPOUSE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SPOUSE_IQ", xmlWriter);

            if (localSPOUSE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SPOUSE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSPOUSE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localFATHER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FATHER_IQ", xmlWriter);

            if (localFATHER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FATHER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFATHER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOTHER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MOTHER_IQ", xmlWriter);

            if (localMOTHER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOTHER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOTHER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOB_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB_IQ", xmlWriter);

            if (localDOB_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_IQ", xmlWriter);

            if (localAGE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_AS_ON_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_AS_ON_IQ", xmlWriter);

            if (localAGE_AS_ON_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_AS_ON_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_AS_ON_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER_IQ", xmlWriter);

            if (localGENDER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_1_IQ", xmlWriter);

            if (localPHONE_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_2_IQ", xmlWriter);

            if (localPHONE_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_3_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_3_IQ", xmlWriter);

            if (localPHONE_3_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_3_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_3_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_1_IQ", xmlWriter);

            if (localADDRESS_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_2_IQ", xmlWriter);

            if (localADDRESS_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_TYP_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_TYP_1_IQ", xmlWriter);

            if (localREL_TYP_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_TYP_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_TYP_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_NM_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_NM_1_IQ", xmlWriter);

            if (localREL_NM_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_NM_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_NM_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_TYP_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_TYP_2_IQ", xmlWriter);

            if (localREL_TYP_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_TYP_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_TYP_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_NM_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_NM_2_IQ", xmlWriter);

            if (localREL_NM_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_NM_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_NM_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_TYP_3_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_TYP_3_IQ", xmlWriter);

            if (localREL_TYP_3_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_TYP_3_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_TYP_3_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_NM_3_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_NM_3_IQ", xmlWriter);

            if (localREL_NM_3_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_NM_3_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_NM_3_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_TYP_4_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_TYP_4_IQ", xmlWriter);

            if (localREL_TYP_4_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_TYP_4_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_TYP_4_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localREL_NM_4_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REL_NM_4_IQ", xmlWriter);

            if (localREL_NM_4_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REL_NM_4_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREL_NM_4_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_TYPE_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_TYPE_1_IQ", xmlWriter);

            if (localID_TYPE_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_TYPE_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_TYPE_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_VALUE_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_VALUE_1_IQ", xmlWriter);

            if (localID_VALUE_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_VALUE_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_VALUE_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_TYPE_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_TYPE_2_IQ", xmlWriter);

            if (localID_TYPE_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_TYPE_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_TYPE_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_VALUE_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_VALUE_2_IQ", xmlWriter);

            if (localID_VALUE_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_VALUE_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_VALUE_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_IQ", xmlWriter);

            if (localRATION_CARD_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTERS_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTERS_ID_IQ", xmlWriter);

            if (localVOTERS_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTERS_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTERS_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVING_LICENCE_NO_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVING_LICENCE_NO_IQ",
                xmlWriter);

            if (localDRIVING_LICENCE_NO_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVING_LICENCE_NO_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVING_LICENCE_NO_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_IQ", xmlWriter);

            if (localPAN_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_IQ", xmlWriter);

            if (localPASSPORT_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localOTHER_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OTHER_ID_IQ", xmlWriter);

            if (localOTHER_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OTHER_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOTHER_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localBRANCH_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BRANCH_IQ", xmlWriter);

            if (localBRANCH_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BRANCH_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBRANCH_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localKENDRA_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "KENDRA_IQ", xmlWriter);

            if (localKENDRA_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "KENDRA_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localKENDRA_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localMBR_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MBR_ID_IQ", xmlWriter);

            if (localMBR_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MBR_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMBR_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP", xmlWriter);

            if (localCREDT_INQ_PURPS_TYP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYP_DESCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP_DESC",
                xmlWriter);

            if (localCREDT_INQ_PURPS_TYP_DESC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP_DESC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP_DESC);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_INQUIRY_STAGETracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_INQUIRY_STAGE", xmlWriter);

            if (localCREDIT_INQUIRY_STAGE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_INQUIRY_STAGE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_INQUIRY_STAGE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_ID", xmlWriter);

            if (localCREDT_RPT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_REQ_TYPTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_REQ_TYP", xmlWriter);

            if (localCREDT_REQ_TYP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_REQ_TYP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_REQ_TYP);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_TRN_DT_TMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_TRN_DT_TM", xmlWriter);

            if (localCREDT_RPT_TRN_DT_TM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_TRN_DT_TM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_TRN_DT_TM);
            }

            xmlWriter.writeEndElement();
        }

        if (localAC_OPEN_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AC_OPEN_DT", xmlWriter);

            if (localAC_OPEN_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AC_OPEN_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAC_OPEN_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOAN_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOAN_AMOUNT", xmlWriter);

            if (localLOAN_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOAN_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOAN_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localENTITY_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENTITY_ID", xmlWriter);

            if (localENTITY_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENTITY_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENTITY_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_STATUS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_STATUS_PR", xmlWriter);

            if (localIND_STATUS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_STATUS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_STATUS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_DEFAULT_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_DEFAULT_ACCOUNTS_PR",
                xmlWriter);

            if (localIND_NO_OF_DEFAULT_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_DEFAULT_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_DEFAULT_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TOTAL_RESPONSES_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TOTAL_RESPONSES_PR",
                xmlWriter);

            if (localIND_TOTAL_RESPONSES_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TOTAL_RESPONSES_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TOTAL_RESPONSES_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_CLOSED_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_CLOSED_ACCOUNTS_PR",
                xmlWriter);

            if (localIND_NO_OF_CLOSED_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_CLOSED_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_CLOSED_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_ACTIVE_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_ACTIVE_ACCOUNTS_PR",
                xmlWriter);

            if (localIND_NO_OF_ACTIVE_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_ACTIVE_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_ACTIVE_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_OTHER_MFIS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_OTHER_MFIS_PR",
                xmlWriter);

            if (localIND_NO_OF_OTHER_MFIS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_OTHER_MFIS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_OTHER_MFIS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_OWN_MFI_INDECATOR_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_OWN_MFI_INDECATOR_PR",
                xmlWriter);

            if (localIND_OWN_MFI_INDECATOR_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_OWN_MFI_INDECATOR_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_OWN_MFI_INDECATOR_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_DISBURSD_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_DISBURSD_AMT_PR",
                xmlWriter);

            if (localIND_TTL_OWN_DISBURSD_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_DISBURSD_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_DISBURSD_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_DISBURSD_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_DISBURSD_AMT_PR",
                xmlWriter);

            if (localIND_TTL_OTHER_DISBURSD_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_DISBURSD_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_DISBURSD_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_CURRENT_BAL_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_CURRENT_BAL_PR",
                xmlWriter);

            if (localIND_TTL_OWN_CURRENT_BAL_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_CURRENT_BAL_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_CURRENT_BAL_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_CURRENT_BAL_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_CURRENT_BAL_PR",
                xmlWriter);

            if (localIND_TTL_OTHER_CURRENT_BAL_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_CURRENT_BAL_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_CURRENT_BAL_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_INSTLMNT_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_INSTLMNT_AMT_PR",
                xmlWriter);

            if (localIND_TTL_OWN_INSTLMNT_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_INSTLMNT_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_INSTLMNT_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_INSTLMNT_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_INSTLMNT_AMT_PR",
                xmlWriter);

            if (localIND_TTL_OTHER_INSTLMNT_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_INSTLMNT_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_INSTLMNT_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MAX_WORST_DELEQUENCY_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MAX_WORST_DELEQUENCY_PR",
                xmlWriter);

            if (localIND_MAX_WORST_DELEQUENCY_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MAX_WORST_DELEQUENCY_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MAX_WORST_DELEQUENCY_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ERRORS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ERRORS_PR", xmlWriter);

            if (localIND_ERRORS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ERRORS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ERRORS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_STATUS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_STATUS_SE", xmlWriter);

            if (localIND_STATUS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_STATUS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_STATUS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_DEFAULT_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_DEFAULT_ACCOUNTS_SE",
                xmlWriter);

            if (localIND_NO_OF_DEFAULT_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_DEFAULT_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_DEFAULT_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TOTAL_RESPONSES_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TOTAL_RESPONSES_SE",
                xmlWriter);

            if (localIND_TOTAL_RESPONSES_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TOTAL_RESPONSES_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TOTAL_RESPONSES_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_CLOSED_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_CLOSED_ACCOUNTS_SE",
                xmlWriter);

            if (localIND_NO_OF_CLOSED_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_CLOSED_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_CLOSED_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_ACTIVE_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_ACTIVE_ACCOUNTS_SE",
                xmlWriter);

            if (localIND_NO_OF_ACTIVE_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_ACTIVE_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_ACTIVE_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_OTHER_MFIS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_OTHER_MFIS_SE",
                xmlWriter);

            if (localIND_NO_OF_OTHER_MFIS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_OTHER_MFIS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_OTHER_MFIS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_OWN_MFI_INDECATOR_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_OWN_MFI_INDECATOR_SE",
                xmlWriter);

            if (localIND_OWN_MFI_INDECATOR_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_OWN_MFI_INDECATOR_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_OWN_MFI_INDECATOR_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_DISBURSD_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_DISBURSD_AMT_SE",
                xmlWriter);

            if (localIND_TTL_OWN_DISBURSD_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_DISBURSD_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_DISBURSD_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_DISBURSD_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_DISBURSD_AMT_SE",
                xmlWriter);

            if (localIND_TTL_OTHER_DISBURSD_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_DISBURSD_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_DISBURSD_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_CURRENT_BAL_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_CURRENT_BAL_SE",
                xmlWriter);

            if (localIND_TTL_OWN_CURRENT_BAL_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_CURRENT_BAL_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_CURRENT_BAL_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_CURRENT_BAL_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_CURRENT_BAL_SE",
                xmlWriter);

            if (localIND_TTL_OTHER_CURRENT_BAL_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_CURRENT_BAL_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_CURRENT_BAL_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_INSTLMNT_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_INSTLMNT_AMT_SE",
                xmlWriter);

            if (localIND_TTL_OWN_INSTLMNT_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_INSTLMNT_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_INSTLMNT_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_INSTLMNT_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_INSTLMNT_AMT_SE",
                xmlWriter);

            if (localIND_TTL_OTHER_INSTLMNT_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_INSTLMNT_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_INSTLMNT_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MAX_WORST_DELEQUENCY_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MAX_WORST_DELEQUENCY_SE",
                xmlWriter);

            if (localIND_MAX_WORST_DELEQUENCY_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MAX_WORST_DELEQUENCY_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MAX_WORST_DELEQUENCY_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ERRORS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ERRORS_SE", xmlWriter);

            if (localIND_ERRORS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ERRORS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ERRORS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_STATUS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_STATUS_SM", xmlWriter);

            if (localIND_STATUS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_STATUS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_STATUS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_DEFAULT_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_DEFAULT_ACCOUNTS_SM",
                xmlWriter);

            if (localIND_NO_OF_DEFAULT_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_DEFAULT_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_DEFAULT_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TOTAL_RESPONSES_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TOTAL_RESPONSES_SM",
                xmlWriter);

            if (localIND_TOTAL_RESPONSES_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TOTAL_RESPONSES_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TOTAL_RESPONSES_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_CLOSED_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_CLOSED_ACCOUNTS_SM",
                xmlWriter);

            if (localIND_NO_OF_CLOSED_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_CLOSED_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_CLOSED_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_ACTIVE_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_ACTIVE_ACCOUNTS_SM",
                xmlWriter);

            if (localIND_NO_OF_ACTIVE_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_ACTIVE_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_ACTIVE_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_OTHER_MFIS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_OTHER_MFIS_SM",
                xmlWriter);

            if (localIND_NO_OF_OTHER_MFIS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_OTHER_MFIS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_OTHER_MFIS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_OWN_MFI_INDECATOR_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_OWN_MFI_INDECATOR_SM",
                xmlWriter);

            if (localIND_OWN_MFI_INDECATOR_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_OWN_MFI_INDECATOR_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_OWN_MFI_INDECATOR_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_DISBURSD_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_DISBURSD_AMT_SM",
                xmlWriter);

            if (localIND_TTL_OWN_DISBURSD_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_DISBURSD_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_DISBURSD_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_DISBURSD_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_DISBURSD_AMT_SM",
                xmlWriter);

            if (localIND_TTL_OTHER_DISBURSD_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_DISBURSD_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_DISBURSD_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_CURRENT_BAL_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_CURRENT_BAL_SM",
                xmlWriter);

            if (localIND_TTL_OWN_CURRENT_BAL_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_CURRENT_BAL_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_CURRENT_BAL_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_CURRENT_BAL_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_CURRENT_BAL_SM",
                xmlWriter);

            if (localIND_TTL_OTHER_CURRENT_BAL_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_CURRENT_BAL_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_CURRENT_BAL_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OWN_INSTLMNT_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OWN_INSTLMNT_AMT_SM",
                xmlWriter);

            if (localIND_TTL_OWN_INSTLMNT_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OWN_INSTLMNT_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OWN_INSTLMNT_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_TTL_OTHER_INSTLMNT_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_TTL_OTHER_INSTLMNT_AMT_SM",
                xmlWriter);

            if (localIND_TTL_OTHER_INSTLMNT_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_TTL_OTHER_INSTLMNT_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_TTL_OTHER_INSTLMNT_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MAX_WORST_DELEQUENCY_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MAX_WORST_DELEQUENCY_SM",
                xmlWriter);

            if (localIND_MAX_WORST_DELEQUENCY_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MAX_WORST_DELEQUENCY_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MAX_WORST_DELEQUENCY_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ERRORS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ERRORS_SM", xmlWriter);

            if (localIND_ERRORS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ERRORS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ERRORS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MATCHED_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MATCHED_TYPE", xmlWriter);

            if (localIND_MATCHED_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MATCHED_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MATCHED_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MFITracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MFI", xmlWriter);

            if (localIND_MFI == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MFI cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MFI);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_MFI_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_MFI_ID", xmlWriter);

            if (localIND_MFI_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_MFI_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_MFI_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_BRANCHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_BRANCH", xmlWriter);

            if (localIND_BRANCH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_BRANCH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_BRANCH);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_KENDRATracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_KENDRA", xmlWriter);

            if (localIND_KENDRA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_KENDRA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_KENDRA);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NAME", xmlWriter);

            if (localIND_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_SPOUSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_SPOUSE", xmlWriter);

            if (localIND_SPOUSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_SPOUSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_SPOUSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_FATHERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_FATHER", xmlWriter);

            if (localIND_FATHER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_FATHER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_FATHER);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_CNSMR_MBR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_CNSMR_MBR_ID", xmlWriter);

            if (localIND_CNSMR_MBR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_CNSMR_MBR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_CNSMR_MBR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_DOBTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_DOB", xmlWriter);

            if (localIND_DOB == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_DOB cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_DOB);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_AGETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_AGE", xmlWriter);

            if (localIND_AGE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_AGE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_AGE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_AGE_AS_ONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_AGE_AS_ON", xmlWriter);

            if (localIND_AGE_AS_ON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_AGE_AS_ON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_AGE_AS_ON);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_PHONE", xmlWriter);

            if (localIND_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ADDRESS_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ADDRESS_1", xmlWriter);

            if (localIND_ADDRESS_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ADDRESS_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ADDRESS_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ADDRESS_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ADDRESS_2", xmlWriter);

            if (localIND_ADDRESS_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ADDRESS_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ADDRESS_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_REL_TYP_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_REL_TYP_1", xmlWriter);

            if (localIND_REL_TYP_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_REL_TYP_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_REL_TYP_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_REL_NM_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_REL_NM_1", xmlWriter);

            if (localIND_REL_NM_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_REL_NM_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_REL_NM_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_REL_TYP_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_REL_TYP_2", xmlWriter);

            if (localIND_REL_TYP_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_REL_TYP_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_REL_TYP_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_REL_NM_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_REL_NM_2", xmlWriter);

            if (localIND_REL_NM_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_REL_NM_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_REL_NM_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ID_TYP_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ID_TYP_1", xmlWriter);

            if (localIND_ID_TYP_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ID_TYP_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ID_TYP_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ID_VALUE_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ID_VALUE_1", xmlWriter);

            if (localIND_ID_VALUE_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ID_VALUE_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ID_VALUE_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ID_TYP_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ID_TYP_2", xmlWriter);

            if (localIND_ID_TYP_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ID_TYP_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ID_TYP_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ID_VALUE_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ID_VALUE_2", xmlWriter);

            if (localIND_ID_VALUE_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ID_VALUE_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ID_VALUE_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_GROUP_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_GROUP_ID", xmlWriter);

            if (localIND_GROUP_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_GROUP_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_GROUP_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_GROUP_CREATION_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_GROUP_CREATION_DATE",
                xmlWriter);

            if (localIND_GROUP_CREATION_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_GROUP_CREATION_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_GROUP_CREATION_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_INSERT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_INSERT_DATE", xmlWriter);

            if (localIND_INSERT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_INSERT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_INSERT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ACCT_TYPE", xmlWriter);

            if (localIND_ACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_FREQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_FREQ", xmlWriter);

            if (localIND_FREQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_FREQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_FREQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_STATUS", xmlWriter);

            if (localIND_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_ACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_ACCT_NUMBER", xmlWriter);

            if (localIND_ACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_ACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_DISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_DISBURSED_AMT", xmlWriter);

            if (localIND_DISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_DISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_CURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_CURRENT_BAL", xmlWriter);

            if (localIND_CURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_CURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_INSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_INSTALLMENT_AMT", xmlWriter);

            if (localIND_INSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_INSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_OVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_OVERDUE_AMT", xmlWriter);

            if (localIND_OVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_OVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_WRITE_OFF_AMT", xmlWriter);

            if (localIND_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_DISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_DISBURSED_DT", xmlWriter);

            if (localIND_DISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_DISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_CLOSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_CLOSED_DT", xmlWriter);

            if (localIND_CLOSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_CLOSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_CLOSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_RECENT_DELINQ_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_RECENT_DELINQ_DT", xmlWriter);

            if (localIND_RECENT_DELINQ_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_RECENT_DELINQ_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_RECENT_DELINQ_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_DPDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_DPD", xmlWriter);

            if (localIND_DPD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_DPD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_DPD);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_INQ_CNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_INQ_CNT", xmlWriter);

            if (localIND_INQ_CNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_INQ_CNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_INQ_CNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_INFO_AS_ONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_INFO_AS_ON", xmlWriter);

            if (localIND_INFO_AS_ON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_INFO_AS_ON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_INFO_AS_ON);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_WORST_DELEQUENCY_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_WORST_DELEQUENCY_AMOUNT",
                xmlWriter);

            if (localIND_WORST_DELEQUENCY_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_WORST_DELEQUENCY_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_WORST_DELEQUENCY_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_PAYMENT_HISTORYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_PAYMENT_HISTORY", xmlWriter);

            if (localIND_PAYMENT_HISTORY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_PAYMENT_HISTORY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_PAYMENT_HISTORY);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_IS_ACTIVE_BORROWERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_IS_ACTIVE_BORROWER",
                xmlWriter);

            if (localIND_IS_ACTIVE_BORROWER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_IS_ACTIVE_BORROWER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_IS_ACTIVE_BORROWER);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_NO_OF_BORROWERSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_NO_OF_BORROWERS", xmlWriter);

            if (localIND_NO_OF_BORROWERS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_NO_OF_BORROWERS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_NO_OF_BORROWERS);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_COMMENT_RESTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_COMMENT_RES", xmlWriter);

            if (localIND_COMMENT_RES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_COMMENT_RES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_COMMENT_RES);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_STATUS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_STATUS_PR", xmlWriter);

            if (localGRP_STATUS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_STATUS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_STATUS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_DEFAULT_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_DEFAULT_ACCOUNTS_PR",
                xmlWriter);

            if (localGRP_NO_OF_DEFAULT_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_DEFAULT_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_DEFAULT_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TOTAL_RESPONSES_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TOTAL_RESPONSES_PR",
                xmlWriter);

            if (localGRP_TOTAL_RESPONSES_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TOTAL_RESPONSES_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TOTAL_RESPONSES_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_CLOSED_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_CLOSED_ACCOUNTS_PR",
                xmlWriter);

            if (localGRP_NO_OF_CLOSED_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_CLOSED_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_CLOSED_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_ACTIVE_ACCOUNTS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_ACTIVE_ACCOUNTS_PR",
                xmlWriter);

            if (localGRP_NO_OF_ACTIVE_ACCOUNTS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_ACTIVE_ACCOUNTS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_ACTIVE_ACCOUNTS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_OTHER_MFIS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_OTHER_MFIS_PR",
                xmlWriter);

            if (localGRP_NO_OF_OTHER_MFIS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_OTHER_MFIS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_OTHER_MFIS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_OWN_MFI_INDECATOR_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_OWN_MFI_INDECATOR_PR",
                xmlWriter);

            if (localGRP_OWN_MFI_INDECATOR_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_OWN_MFI_INDECATOR_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_OWN_MFI_INDECATOR_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_DISBURSD_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_DISBURSD_AMT_PR",
                xmlWriter);

            if (localGRP_TTL_OWN_DISBURSD_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_DISBURSD_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_DISBURSD_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_DISBURSD_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_DISBURSD_AMT_PR",
                xmlWriter);

            if (localGRP_TTL_OTHER_DISBURSD_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_DISBURSD_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_DISBURSD_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_CURRENT_BAL_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_CURRENT_BAL_PR",
                xmlWriter);

            if (localGRP_TTL_OWN_CURRENT_BAL_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_CURRENT_BAL_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_CURRENT_BAL_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_CURRENT_BAL_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_CURRENT_BAL_PR",
                xmlWriter);

            if (localGRP_TTL_OTHER_CURRENT_BAL_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_CURRENT_BAL_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_CURRENT_BAL_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_INSTLMNT_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_INSTLMNT_AMT_PR",
                xmlWriter);

            if (localGRP_TTL_OWN_INSTLMNT_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_INSTLMNT_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_INSTLMNT_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_INSTLMNT_AMT_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_INSTLMNT_AMT_PR",
                xmlWriter);

            if (localGRP_TTL_OTHER_INSTLMNT_AMT_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_INSTLMNT_AMT_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_INSTLMNT_AMT_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MAX_WORST_DELEQUENCY_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MAX_WORST_DELEQUENCY_PR",
                xmlWriter);

            if (localGRP_MAX_WORST_DELEQUENCY_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MAX_WORST_DELEQUENCY_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MAX_WORST_DELEQUENCY_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ERRORS_PRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ERRORS_PR", xmlWriter);

            if (localGRP_ERRORS_PR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ERRORS_PR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ERRORS_PR);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_STATUS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_STATUS_SE", xmlWriter);

            if (localGRP_STATUS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_STATUS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_STATUS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_DEFAULT_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_DEFAULT_ACCOUNTS_SE",
                xmlWriter);

            if (localGRP_NO_OF_DEFAULT_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_DEFAULT_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_DEFAULT_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TOTAL_RESPONSES_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TOTAL_RESPONSES_SE",
                xmlWriter);

            if (localGRP_TOTAL_RESPONSES_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TOTAL_RESPONSES_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TOTAL_RESPONSES_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_CLOSED_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_CLOSED_ACCOUNTS_SE",
                xmlWriter);

            if (localGRP_NO_OF_CLOSED_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_CLOSED_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_CLOSED_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_ACTIVE_ACCOUNTS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_ACTIVE_ACCOUNTS_SE",
                xmlWriter);

            if (localGRP_NO_OF_ACTIVE_ACCOUNTS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_ACTIVE_ACCOUNTS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_ACTIVE_ACCOUNTS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_OTHER_MFIS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_OTHER_MFIS_SE",
                xmlWriter);

            if (localGRP_NO_OF_OTHER_MFIS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_OTHER_MFIS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_OTHER_MFIS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_OWN_MFI_INDECATOR_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_OWN_MFI_INDECATOR_SE",
                xmlWriter);

            if (localGRP_OWN_MFI_INDECATOR_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_OWN_MFI_INDECATOR_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_OWN_MFI_INDECATOR_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_DISBURSD_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_DISBURSD_AMT_SE",
                xmlWriter);

            if (localGRP_TTL_OWN_DISBURSD_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_DISBURSD_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_DISBURSD_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_DISBURSD_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_DISBURSD_AMT_SE",
                xmlWriter);

            if (localGRP_TTL_OTHER_DISBURSD_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_DISBURSD_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_DISBURSD_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_CURRENT_BAL_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_CURRENT_BAL_SE",
                xmlWriter);

            if (localGRP_TTL_OWN_CURRENT_BAL_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_CURRENT_BAL_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_CURRENT_BAL_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_CURRENT_BAL_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_CURRENT_BAL_SE",
                xmlWriter);

            if (localGRP_TTL_OTHER_CURRENT_BAL_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_CURRENT_BAL_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_CURRENT_BAL_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_INSTLMNT_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_INSTLMNT_AMT_SE",
                xmlWriter);

            if (localGRP_TTL_OWN_INSTLMNT_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_INSTLMNT_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_INSTLMNT_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_INSTLMNT_AMT_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_INSTLMNT_AMT_SE",
                xmlWriter);

            if (localGRP_TTL_OTHER_INSTLMNT_AMT_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_INSTLMNT_AMT_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_INSTLMNT_AMT_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MAX_WORST_DELEQUENCY_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MAX_WORST_DELEQUENCY_SE",
                xmlWriter);

            if (localGRP_MAX_WORST_DELEQUENCY_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MAX_WORST_DELEQUENCY_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MAX_WORST_DELEQUENCY_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ERRORS_SETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ERRORS_SE", xmlWriter);

            if (localGRP_ERRORS_SE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ERRORS_SE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ERRORS_SE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_STATUS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_STATUS_SM", xmlWriter);

            if (localGRP_STATUS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_STATUS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_STATUS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_DEFAULT_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_DEFAULT_ACCOUNTS_SM",
                xmlWriter);

            if (localGRP_NO_OF_DEFAULT_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_DEFAULT_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_DEFAULT_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TOTAL_RESPONSES_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TOTAL_RESPONSES_SM",
                xmlWriter);

            if (localGRP_TOTAL_RESPONSES_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TOTAL_RESPONSES_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TOTAL_RESPONSES_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_CLOSED_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_CLOSED_ACCOUNTS_SM",
                xmlWriter);

            if (localGRP_NO_OF_CLOSED_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_CLOSED_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_CLOSED_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_ACTIVE_ACCOUNTS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_ACTIVE_ACCOUNTS_SM",
                xmlWriter);

            if (localGRP_NO_OF_ACTIVE_ACCOUNTS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_ACTIVE_ACCOUNTS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_ACTIVE_ACCOUNTS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_OTHER_MFIS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_OTHER_MFIS_SM",
                xmlWriter);

            if (localGRP_NO_OF_OTHER_MFIS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_OTHER_MFIS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_OTHER_MFIS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_OWN_MFI_INDECATOR_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_OWN_MFI_INDECATOR_SM",
                xmlWriter);

            if (localGRP_OWN_MFI_INDECATOR_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_OWN_MFI_INDECATOR_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_OWN_MFI_INDECATOR_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_DISBURSD_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_DISBURSD_AMT_SM",
                xmlWriter);

            if (localGRP_TTL_OWN_DISBURSD_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_DISBURSD_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_DISBURSD_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_DISBURSD_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_DISBURSD_AMT_SM",
                xmlWriter);

            if (localGRP_TTL_OTHER_DISBURSD_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_DISBURSD_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_DISBURSD_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_CURRENT_BAL_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_CURRENT_BAL_SM",
                xmlWriter);

            if (localGRP_TTL_OWN_CURRENT_BAL_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_CURRENT_BAL_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_CURRENT_BAL_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_CURRENT_BAL_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_CURRENT_BAL_SM",
                xmlWriter);

            if (localGRP_TTL_OTHER_CURRENT_BAL_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_CURRENT_BAL_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_CURRENT_BAL_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OWN_INSTLMNT_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OWN_INSTLMNT_AMT_SM",
                xmlWriter);

            if (localGRP_TTL_OWN_INSTLMNT_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OWN_INSTLMNT_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OWN_INSTLMNT_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_TTL_OTHER_INSTLMNT_AMT_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_TTL_OTHER_INSTLMNT_AMT_SM",
                xmlWriter);

            if (localGRP_TTL_OTHER_INSTLMNT_AMT_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_TTL_OTHER_INSTLMNT_AMT_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_TTL_OTHER_INSTLMNT_AMT_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MAX_WORST_DELEQUENCY_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MAX_WORST_DELEQUENCY_SM",
                xmlWriter);

            if (localGRP_MAX_WORST_DELEQUENCY_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MAX_WORST_DELEQUENCY_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MAX_WORST_DELEQUENCY_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ERRORS_SMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ERRORS_SM", xmlWriter);

            if (localGRP_ERRORS_SM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ERRORS_SM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ERRORS_SM);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MATCHED_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MATCHED_TYPE", xmlWriter);

            if (localGRP_MATCHED_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MATCHED_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MATCHED_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MFITracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MFI", xmlWriter);

            if (localGRP_MFI == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MFI cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MFI);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_MFI_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_MFI_ID", xmlWriter);

            if (localGRP_MFI_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_MFI_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_MFI_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_BRANCHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_BRANCH", xmlWriter);

            if (localGRP_BRANCH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_BRANCH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_BRANCH);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_KENDRATracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_KENDRA", xmlWriter);

            if (localGRP_KENDRA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_KENDRA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_KENDRA);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NAME", xmlWriter);

            if (localGRP_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_SPOUSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_SPOUSE", xmlWriter);

            if (localGRP_SPOUSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_SPOUSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_SPOUSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_FATHERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_FATHER", xmlWriter);

            if (localGRP_FATHER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_FATHER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_FATHER);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_CNSMR_MBR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_CNSMR_MBR_ID", xmlWriter);

            if (localGRP_CNSMR_MBR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_CNSMR_MBR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_CNSMR_MBR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_DOBTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_DOB", xmlWriter);

            if (localGRP_DOB == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_DOB cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_DOB);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_AGETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_AGE", xmlWriter);

            if (localGRP_AGE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_AGE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_AGE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_AGE_AS_ONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_AGE_AS_ON", xmlWriter);

            if (localGRP_AGE_AS_ON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_AGE_AS_ON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_AGE_AS_ON);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_PHONE", xmlWriter);

            if (localGRP_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ADDRESS_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ADDRESS_1", xmlWriter);

            if (localGRP_ADDRESS_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ADDRESS_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ADDRESS_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ADDRESS_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ADDRESS_2", xmlWriter);

            if (localGRP_ADDRESS_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ADDRESS_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ADDRESS_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_REL_TYP_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_REL_TYP_1", xmlWriter);

            if (localGRP_REL_TYP_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_REL_TYP_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_REL_TYP_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_REL_NM_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_REL_NM_1", xmlWriter);

            if (localGRP_REL_NM_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_REL_NM_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_REL_NM_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_REL_TYP_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_REL_TYP_2", xmlWriter);

            if (localGRP_REL_TYP_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_REL_TYP_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_REL_TYP_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_REL_NM_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_REL_NM_2", xmlWriter);

            if (localGRP_REL_NM_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_REL_NM_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_REL_NM_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ID_TYP_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ID_TYP_1", xmlWriter);

            if (localGRP_ID_TYP_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ID_TYP_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ID_TYP_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ID_VALUE_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ID_VALUE_1", xmlWriter);

            if (localGRP_ID_VALUE_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ID_VALUE_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ID_VALUE_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ID_TYP_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ID_TYP_2", xmlWriter);

            if (localGRP_ID_TYP_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ID_TYP_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ID_TYP_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ID_VALUE_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ID_VALUE_2", xmlWriter);

            if (localGRP_ID_VALUE_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ID_VALUE_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ID_VALUE_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_GROUP_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_GROUP_ID", xmlWriter);

            if (localGRP_GROUP_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_GROUP_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_GROUP_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_GROUP_CREATION_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_GROUP_CREATION_DATE",
                xmlWriter);

            if (localGRP_GROUP_CREATION_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_GROUP_CREATION_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_GROUP_CREATION_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_INSERT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_INSERT_DATE", xmlWriter);

            if (localGRP_INSERT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_INSERT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_INSERT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ACCT_TYPE", xmlWriter);

            if (localGRP_ACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_FREQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_FREQ", xmlWriter);

            if (localGRP_FREQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_FREQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_FREQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_STATUS", xmlWriter);

            if (localGRP_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_ACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_ACCT_NUMBER", xmlWriter);

            if (localGRP_ACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_ACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_DISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_DISBURSED_AMT", xmlWriter);

            if (localGRP_DISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_DISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_CURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_CURRENT_BAL", xmlWriter);

            if (localGRP_CURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_CURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_INSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_INSTALLMENT_AMT", xmlWriter);

            if (localGRP_INSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_INSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_OVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_OVERDUE_AMT", xmlWriter);

            if (localGRP_OVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_OVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_WRITE_OFF_AMT", xmlWriter);

            if (localGRP_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_DISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_DISBURSED_DT", xmlWriter);

            if (localGRP_DISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_DISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_CLOSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_CLOSED_DT", xmlWriter);

            if (localGRP_CLOSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_CLOSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_CLOSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_RECENT_DELINQ_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_RECENT_DELINQ_DT", xmlWriter);

            if (localGRP_RECENT_DELINQ_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_RECENT_DELINQ_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_RECENT_DELINQ_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_DPDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_DPD", xmlWriter);

            if (localGRP_DPD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_DPD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_DPD);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_INQ_CNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_INQ_CNT", xmlWriter);

            if (localGRP_INQ_CNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_INQ_CNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_INQ_CNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_INFO_AS_ONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_INFO_AS_ON", xmlWriter);

            if (localGRP_INFO_AS_ON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_INFO_AS_ON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_INFO_AS_ON);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_WORST_DELEQUENCY_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_WORST_DELEQUENCY_AMOUNT",
                xmlWriter);

            if (localGRP_WORST_DELEQUENCY_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_WORST_DELEQUENCY_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_WORST_DELEQUENCY_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_PAYMENT_HISTORYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_PAYMENT_HISTORY", xmlWriter);

            if (localGRP_PAYMENT_HISTORY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_PAYMENT_HISTORY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_PAYMENT_HISTORY);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_IS_ACTIVE_BORROWERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_IS_ACTIVE_BORROWER",
                xmlWriter);

            if (localGRP_IS_ACTIVE_BORROWER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_IS_ACTIVE_BORROWER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_IS_ACTIVE_BORROWER);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_NO_OF_BORROWERSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_NO_OF_BORROWERS", xmlWriter);

            if (localGRP_NO_OF_BORROWERS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_NO_OF_BORROWERS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_NO_OF_BORROWERS);
            }

            xmlWriter.writeEndElement();
        }

        if (localGRP_COMMENT_RESTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GRP_COMMENT_RES", xmlWriter);

            if (localGRP_COMMENT_RES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GRP_COMMENT_RES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGRP_COMMENT_RES);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_NAME", xmlWriter);

            if (localMEMBER_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localINQUIRY_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "INQUIRY_DATE", xmlWriter);

            if (localINQUIRY_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INQUIRY_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINQUIRY_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPURPOSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PURPOSE", xmlWriter);

            if (localPURPOSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PURPOSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPURPOSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNT", xmlWriter);

            if (localAMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localIND_REMARKTracker) {
            namespace = "";
            writeStartElement(null, namespace, "IND_REMARK", xmlWriter);

            if (localIND_REMARK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "IND_REMARK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localIND_REMARK);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ChmAorSRespType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ChmAorSRespType object = new ChmAorSRespType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ChmAorSRespType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ChmAorSRespType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_REQUEST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_REQUEST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_ISSUE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_ISSUE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORT_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NAME_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SPOUSE_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SPOUSE_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SPOUSE_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSPOUSE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FATHER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FATHER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FATHER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFATHER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MOTHER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOTHER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOTHER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOTHER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_AS_ON_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_AS_ON_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_AS_ON_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_AS_ON_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_3_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_3_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_3_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_3_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_TYP_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_TYP_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_TYP_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_TYP_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_NM_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_NM_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_NM_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_NM_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_TYP_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_TYP_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_TYP_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_TYP_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_NM_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_NM_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_NM_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_NM_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_TYP_3_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_TYP_3_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_TYP_3_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_TYP_3_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_NM_3_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_NM_3_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_NM_3_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_NM_3_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_TYP_4_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_TYP_4_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_TYP_4_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_TYP_4_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REL_NM_4_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REL_NM_4_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REL_NM_4_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREL_NM_4_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_TYPE_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_TYPE_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_TYPE_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_TYPE_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_VALUE_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_VALUE_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_VALUE_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_VALUE_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_TYPE_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_TYPE_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_TYPE_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_TYPE_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_VALUE_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_VALUE_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_VALUE_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_VALUE_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATION_CARD_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATION_CARD_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTERS_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTERS_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTERS_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTERS_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENCE_NO_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENCE_NO_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVING_LICENCE_NO_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVING_LICENCE_NO_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OTHER_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OTHER_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OTHER_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOTHER_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BRANCH_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BRANCH_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BRANCH_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBRANCH_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "KENDRA_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "KENDRA_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "KENDRA_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setKENDRA_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MBR_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MBR_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MBR_ID_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMBR_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_INQ_PURPS_TYP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_INQ_PURPS_TYP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP_DESC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP_DESC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDIT_INQUIRY_STAGE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDIT_INQUIRY_STAGE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_INQUIRY_STAGE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_INQUIRY_STAGE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_REQ_TYP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_REQ_TYP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_RPT_TRN_DT_TM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_RPT_TRN_DT_TM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_TRN_DT_TM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_TRN_DT_TM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AC_OPEN_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AC_OPEN_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AC_OPEN_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAC_OPEN_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOAN_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOAN_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENTITY_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENTITY_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENTITY_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENTITY_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_STATUS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_STATUS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_STATUS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_STATUS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_DEFAULT_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_DEFAULT_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TOTAL_RESPONSES_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TOTAL_RESPONSES_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_CLOSED_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_CLOSED_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_ACTIVE_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_ACTIVE_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_OTHER_MFIS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_OTHER_MFIS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_OWN_MFI_INDECATOR_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_OWN_MFI_INDECATOR_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_DISBURSD_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_DISBURSD_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_DISBURSD_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_DISBURSD_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_CURRENT_BAL_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_CURRENT_BAL_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_CURRENT_BAL_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_CURRENT_BAL_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_INSTLMNT_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_INSTLMNT_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_INSTLMNT_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_INSTLMNT_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MAX_WORST_DELEQUENCY_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MAX_WORST_DELEQUENCY_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ERRORS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ERRORS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ERRORS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ERRORS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_STATUS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_STATUS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_STATUS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_STATUS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_DEFAULT_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_DEFAULT_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TOTAL_RESPONSES_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TOTAL_RESPONSES_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_CLOSED_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_CLOSED_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_ACTIVE_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_ACTIVE_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_OTHER_MFIS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_OTHER_MFIS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_OWN_MFI_INDECATOR_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_OWN_MFI_INDECATOR_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_DISBURSD_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_DISBURSD_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_DISBURSD_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_DISBURSD_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_CURRENT_BAL_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_CURRENT_BAL_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_CURRENT_BAL_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_CURRENT_BAL_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_INSTLMNT_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_INSTLMNT_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_INSTLMNT_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_INSTLMNT_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MAX_WORST_DELEQUENCY_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MAX_WORST_DELEQUENCY_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ERRORS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ERRORS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ERRORS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ERRORS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_STATUS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_STATUS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_STATUS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_STATUS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_DEFAULT_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_DEFAULT_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_DEFAULT_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TOTAL_RESPONSES_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TOTAL_RESPONSES_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TOTAL_RESPONSES_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_CLOSED_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_CLOSED_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_CLOSED_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_ACTIVE_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_ACTIVE_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_ACTIVE_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_NO_OF_OTHER_MFIS_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_OTHER_MFIS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_OTHER_MFIS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_OWN_MFI_INDECATOR_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_OWN_MFI_INDECATOR_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_OWN_MFI_INDECATOR_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_DISBURSD_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_DISBURSD_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_DISBURSD_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_DISBURSD_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_DISBURSD_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_DISBURSD_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_CURRENT_BAL_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_CURRENT_BAL_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_CURRENT_BAL_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_CURRENT_BAL_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_CURRENT_BAL_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_CURRENT_BAL_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OWN_INSTLMNT_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OWN_INSTLMNT_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OWN_INSTLMNT_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_TTL_OTHER_INSTLMNT_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_TTL_OTHER_INSTLMNT_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_TTL_OTHER_INSTLMNT_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_MAX_WORST_DELEQUENCY_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MAX_WORST_DELEQUENCY_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MAX_WORST_DELEQUENCY_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ERRORS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ERRORS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ERRORS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ERRORS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_MATCHED_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_MATCHED_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MATCHED_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MATCHED_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_MFI").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_MFI").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MFI" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MFI(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_MFI_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_MFI_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_MFI_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_MFI_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_BRANCH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_BRANCH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_BRANCH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_BRANCH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_KENDRA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_KENDRA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_KENDRA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_KENDRA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_SPOUSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_SPOUSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_SPOUSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_SPOUSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_FATHER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_FATHER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_FATHER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_FATHER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_CNSMR_MBR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_CNSMR_MBR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_CNSMR_MBR_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_CNSMR_MBR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_DOB").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_DOB").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_DOB" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_DOB(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_AGE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_AGE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_AGE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_AGE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_AGE_AS_ON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_AGE_AS_ON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_AGE_AS_ON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_AGE_AS_ON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_PHONE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ADDRESS_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ADDRESS_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ADDRESS_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ADDRESS_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ADDRESS_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ADDRESS_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ADDRESS_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ADDRESS_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_REL_TYP_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_REL_TYP_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_REL_TYP_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_REL_TYP_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_REL_NM_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_REL_NM_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_REL_NM_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_REL_NM_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_REL_TYP_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_REL_TYP_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_REL_TYP_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_REL_TYP_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_REL_NM_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_REL_NM_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_REL_NM_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_REL_NM_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ID_TYP_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ID_TYP_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ID_TYP_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ID_TYP_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ID_VALUE_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ID_VALUE_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ID_VALUE_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ID_VALUE_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ID_TYP_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ID_TYP_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ID_TYP_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ID_TYP_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ID_VALUE_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ID_VALUE_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ID_VALUE_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ID_VALUE_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_GROUP_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_GROUP_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_GROUP_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_GROUP_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_GROUP_CREATION_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_GROUP_CREATION_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_GROUP_CREATION_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_GROUP_CREATION_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_INSERT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_INSERT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_INSERT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_INSERT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ACCT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_FREQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_FREQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_FREQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_FREQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_ACCT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_ACCT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_ACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_DISBURSED_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_DISBURSED_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_DISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_CURRENT_BAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_CURRENT_BAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_CURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_INSTALLMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_INSTALLMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_INSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_OVERDUE_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_OVERDUE_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_OVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_DISBURSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_DISBURSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_DISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_CLOSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_CLOSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_CLOSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_CLOSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_RECENT_DELINQ_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_RECENT_DELINQ_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_RECENT_DELINQ_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_RECENT_DELINQ_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_DPD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_DPD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_DPD" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_DPD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_INQ_CNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_INQ_CNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_INQ_CNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_INQ_CNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_INFO_AS_ON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_INFO_AS_ON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_INFO_AS_ON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_INFO_AS_ON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_WORST_DELEQUENCY_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_WORST_DELEQUENCY_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_WORST_DELEQUENCY_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_WORST_DELEQUENCY_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_PAYMENT_HISTORY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_PAYMENT_HISTORY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_PAYMENT_HISTORY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_PAYMENT_HISTORY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "IND_IS_ACTIVE_BORROWER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "IND_IS_ACTIVE_BORROWER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_IS_ACTIVE_BORROWER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_IS_ACTIVE_BORROWER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_NO_OF_BORROWERS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_NO_OF_BORROWERS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_NO_OF_BORROWERS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_NO_OF_BORROWERS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_COMMENT_RES").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_COMMENT_RES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_COMMENT_RES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_COMMENT_RES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_STATUS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_STATUS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_STATUS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_STATUS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_DEFAULT_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_DEFAULT_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TOTAL_RESPONSES_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TOTAL_RESPONSES_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_CLOSED_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_CLOSED_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_ACTIVE_ACCOUNTS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_ACTIVE_ACCOUNTS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_OTHER_MFIS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_OTHER_MFIS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_PR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_PR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_OWN_MFI_INDECATOR_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_OWN_MFI_INDECATOR_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_DISBURSD_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_DISBURSD_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_DISBURSD_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_DISBURSD_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_CURRENT_BAL_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_CURRENT_BAL_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_CURRENT_BAL_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_CURRENT_BAL_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_INSTLMNT_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_INSTLMNT_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_INSTLMNT_AMT_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_INSTLMNT_AMT_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MAX_WORST_DELEQUENCY_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MAX_WORST_DELEQUENCY_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ERRORS_PR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ERRORS_PR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ERRORS_PR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ERRORS_PR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_STATUS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_STATUS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_STATUS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_STATUS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_DEFAULT_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_DEFAULT_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TOTAL_RESPONSES_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TOTAL_RESPONSES_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_CLOSED_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_CLOSED_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_ACTIVE_ACCOUNTS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_ACTIVE_ACCOUNTS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_OTHER_MFIS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_OTHER_MFIS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_SE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_SE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_OWN_MFI_INDECATOR_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_OWN_MFI_INDECATOR_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_DISBURSD_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_DISBURSD_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_DISBURSD_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_DISBURSD_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_CURRENT_BAL_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_CURRENT_BAL_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_CURRENT_BAL_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_CURRENT_BAL_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_INSTLMNT_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_INSTLMNT_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_INSTLMNT_AMT_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_INSTLMNT_AMT_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MAX_WORST_DELEQUENCY_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MAX_WORST_DELEQUENCY_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ERRORS_SE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ERRORS_SE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ERRORS_SE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ERRORS_SE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_STATUS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_STATUS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_STATUS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_STATUS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_DEFAULT_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_DEFAULT_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_DEFAULT_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TOTAL_RESPONSES_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TOTAL_RESPONSES_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TOTAL_RESPONSES_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_CLOSED_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_CLOSED_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_CLOSED_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_ACTIVE_ACCOUNTS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_ACTIVE_ACCOUNTS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_ACTIVE_ACCOUNTS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_NO_OF_OTHER_MFIS_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_OTHER_MFIS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_OTHER_MFIS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_SM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_OWN_MFI_INDECATOR_SM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_OWN_MFI_INDECATOR_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_OWN_MFI_INDECATOR_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_DISBURSD_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_DISBURSD_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_DISBURSD_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_DISBURSD_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_DISBURSD_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_DISBURSD_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_CURRENT_BAL_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_CURRENT_BAL_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_CURRENT_BAL_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_CURRENT_BAL_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_CURRENT_BAL_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_CURRENT_BAL_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OWN_INSTLMNT_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OWN_INSTLMNT_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OWN_INSTLMNT_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_TTL_OTHER_INSTLMNT_AMT_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_TTL_OTHER_INSTLMNT_AMT_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_TTL_OTHER_INSTLMNT_AMT_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_MAX_WORST_DELEQUENCY_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MAX_WORST_DELEQUENCY_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MAX_WORST_DELEQUENCY_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ERRORS_SM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ERRORS_SM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ERRORS_SM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ERRORS_SM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_MATCHED_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_MATCHED_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MATCHED_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MATCHED_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_MFI").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_MFI").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MFI" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MFI(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_MFI_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_MFI_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_MFI_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_MFI_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_BRANCH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_BRANCH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_BRANCH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_BRANCH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_KENDRA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_KENDRA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_KENDRA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_KENDRA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_SPOUSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_SPOUSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_SPOUSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_SPOUSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_FATHER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_FATHER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_FATHER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_FATHER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_CNSMR_MBR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_CNSMR_MBR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_CNSMR_MBR_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_CNSMR_MBR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_DOB").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_DOB").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_DOB" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_DOB(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_AGE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_AGE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_AGE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_AGE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_AGE_AS_ON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_AGE_AS_ON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_AGE_AS_ON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_AGE_AS_ON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_PHONE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ADDRESS_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ADDRESS_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ADDRESS_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ADDRESS_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ADDRESS_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ADDRESS_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ADDRESS_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ADDRESS_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_REL_TYP_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_REL_TYP_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_REL_TYP_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_REL_TYP_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_REL_NM_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_REL_NM_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_REL_NM_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_REL_NM_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_REL_TYP_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_REL_TYP_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_REL_TYP_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_REL_TYP_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_REL_NM_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_REL_NM_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_REL_NM_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_REL_NM_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ID_TYP_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ID_TYP_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ID_TYP_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ID_TYP_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ID_VALUE_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ID_VALUE_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ID_VALUE_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ID_VALUE_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ID_TYP_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ID_TYP_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ID_TYP_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ID_TYP_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ID_VALUE_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ID_VALUE_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ID_VALUE_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ID_VALUE_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_GROUP_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_GROUP_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_GROUP_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_GROUP_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_GROUP_CREATION_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_GROUP_CREATION_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_GROUP_CREATION_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_GROUP_CREATION_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_INSERT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_INSERT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_INSERT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_INSERT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ACCT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_FREQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_FREQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_FREQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_FREQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_ACCT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_ACCT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_ACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_DISBURSED_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_DISBURSED_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_DISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_CURRENT_BAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_CURRENT_BAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_CURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_INSTALLMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_INSTALLMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_INSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_OVERDUE_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_OVERDUE_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_OVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_DISBURSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_DISBURSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_DISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_CLOSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_CLOSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_CLOSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_CLOSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_RECENT_DELINQ_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_RECENT_DELINQ_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_RECENT_DELINQ_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_RECENT_DELINQ_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_DPD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_DPD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_DPD" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_DPD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_INQ_CNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_INQ_CNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_INQ_CNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_INQ_CNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_INFO_AS_ON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_INFO_AS_ON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_INFO_AS_ON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_INFO_AS_ON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_WORST_DELEQUENCY_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_WORST_DELEQUENCY_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_WORST_DELEQUENCY_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_WORST_DELEQUENCY_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_PAYMENT_HISTORY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_PAYMENT_HISTORY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_PAYMENT_HISTORY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_PAYMENT_HISTORY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "GRP_IS_ACTIVE_BORROWER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "GRP_IS_ACTIVE_BORROWER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_IS_ACTIVE_BORROWER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_IS_ACTIVE_BORROWER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_NO_OF_BORROWERS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_NO_OF_BORROWERS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_NO_OF_BORROWERS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_NO_OF_BORROWERS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GRP_COMMENT_RES").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GRP_COMMENT_RES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GRP_COMMENT_RES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGRP_COMMENT_RES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MEMBER_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MEMBER_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INQUIRY_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INQUIRY_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INQUIRY_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINQUIRY_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PURPOSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PURPOSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PURPOSE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPURPOSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "IND_REMARK").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "IND_REMARK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "IND_REMARK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setIND_REMARK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
