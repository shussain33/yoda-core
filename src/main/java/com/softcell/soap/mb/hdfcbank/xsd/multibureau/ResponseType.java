/**
 * ResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ResponseType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ResponseType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ResponseType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for HEADER
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType localHEADER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHEADERTracker = false;

    /**
     * field for ACKNOWLEDGEMENTID
     */
    protected java.lang.String localACKNOWLEDGEMENTID;

    /**
     * field for STATUS
     */
    protected java.lang.String localSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUSTracker = false;

    /**
     * field for FINISHED
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[] localFINISHED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFINISHEDTracker = false;

    /**
     * field for REJECT
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[] localREJECT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREJECTTracker = false;

    public boolean isHEADERSpecified() {
        return localHEADERTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType getHEADER() {
        return localHEADER;
    }

    /**
     * Auto generated setter method
     * @param param HEADER
     */
    public void setHEADER(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType param) {
        localHEADERTracker = param != null;

        this.localHEADER = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACKNOWLEDGEMENTID() {
        return localACKNOWLEDGEMENTID;
    }

    /**
     * Auto generated setter method
     * @param param ACKNOWLEDGEMENTID
     */
    public void setACKNOWLEDGEMENTID(java.lang.String param) {
        this.localACKNOWLEDGEMENTID = param;
    }

    public boolean isSTATUSSpecified() {
        return localSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS() {
        return localSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param STATUS
     */
    public void setSTATUS(java.lang.String param) {
        localSTATUSTracker = param != null;

        this.localSTATUS = param;
    }

    public boolean isFINISHEDSpecified() {
        return localFINISHEDTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[] getFINISHED() {
        return localFINISHED;
    }

    /**
     * validate the array for FINISHED
     */
    protected void validateFINISHED(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param FINISHED
     */
    public void setFINISHED(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[] param) {
        validateFINISHED(param);

        localFINISHEDTracker = param != null;

        this.localFINISHED = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType
     */
    public void addFINISHED(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType param) {
        if (localFINISHED == null) {
            localFINISHED = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[] {
                    
                };
        }

        //update the setting tracker
        localFINISHEDTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localFINISHED);
        list.add(param);
        this.localFINISHED = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[list.size()]);
    }

    public boolean isREJECTSpecified() {
        return localREJECTTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[] getREJECT() {
        return localREJECT;
    }

    /**
     * validate the array for REJECT
     */
    protected void validateREJECT(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param REJECT
     */
    public void setREJECT(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[] param) {
        validateREJECT(param);

        localREJECTTracker = param != null;

        this.localREJECT = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType
     */
    public void addREJECT(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType param) {
        if (localREJECT == null) {
            localREJECT = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[] {
                    
                };
        }

        //update the setting tracker
        localREJECTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localREJECT);
        list.add(param);
        this.localREJECT = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[list.size()]);
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ResponseType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ResponseType", xmlWriter);
            }
        }

        if (localHEADERTracker) {
            if (localHEADER == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "HEADER cannot be null!!");
            }

            localHEADER.serialize(new javax.xml.namespace.QName("", "HEADER"),
                xmlWriter);
        }

        namespace = "";
        writeStartElement(null, namespace, "ACKNOWLEDGEMENT-ID", xmlWriter);

        if (localACKNOWLEDGEMENTID == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "ACKNOWLEDGEMENT-ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localACKNOWLEDGEMENTID);
        }

        xmlWriter.writeEndElement();

        if (localSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS", xmlWriter);

            if (localSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localFINISHEDTracker) {
            if (localFINISHED != null) {
                for (int i = 0; i < localFINISHED.length; i++) {
                    if (localFINISHED[i] != null) {
                        localFINISHED[i].serialize(new javax.xml.namespace.QName(
                                "", "FINISHED"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "FINISHED cannot be null!!");
            }
        }

        if (localREJECTTracker) {
            if (localREJECT != null) {
                for (int i = 0; i < localREJECT.length; i++) {
                    if (localREJECT[i] != null) {
                        localREJECT[i].serialize(new javax.xml.namespace.QName(
                                "", "REJECT"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "REJECT cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ResponseType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ResponseType object = new ResponseType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ResponseType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ResponseType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list4 = new java.util.ArrayList();

                java.util.ArrayList list5 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "HEADER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "HEADER").equals(
                            reader.getName())) {
                    object.setHEADER(com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACKNOWLEDGEMENT-ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACKNOWLEDGEMENT-ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACKNOWLEDGEMENT-ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACKNOWLEDGEMENTID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FINISHED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FINISHED").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list4.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone4 = false;

                    while (!loopDone4) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone4 = true;
                        } else {
                            if (new javax.xml.namespace.QName("", "FINISHED").equals(
                                        reader.getName())) {
                                list4.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType.Factory.parse(
                                        reader));
                            } else {
                                loopDone4 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setFINISHED((com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType.class,
                            list4));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REJECT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REJECT").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone5 = false;

                    while (!loopDone5) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone5 = true;
                        } else {
                            if (new javax.xml.namespace.QName("", "REJECT").equals(
                                        reader.getName())) {
                                list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType.Factory.parse(
                                        reader));
                            } else {
                                loopDone5 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setREJECT((com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType.class,
                            list5));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
