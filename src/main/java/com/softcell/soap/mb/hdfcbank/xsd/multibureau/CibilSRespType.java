/**
 * CibilSRespType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  CibilSRespType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class CibilSRespType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = CibilSRespType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for DATE_PROCESSED
     */
    protected java.lang.String localDATE_PROCESSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_PROCESSEDTracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for SUBJECT_RETURN_CODE
     */
    protected java.lang.String localSUBJECT_RETURN_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBJECT_RETURN_CODETracker = false;

    /**
     * field for ENQUIRY_CONTROL_NUMBER
     */
    protected java.lang.String localENQUIRY_CONTROL_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_CONTROL_NUMBERTracker = false;

    /**
     * field for CONSUMER_NAME_FIELD1
     */
    protected java.lang.String localCONSUMER_NAME_FIELD1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUMER_NAME_FIELD1Tracker = false;

    /**
     * field for CONSUME_NAME_FIELD2
     */
    protected java.lang.String localCONSUME_NAME_FIELD2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUME_NAME_FIELD2Tracker = false;

    /**
     * field for CONSUMER_NAME_FIELD3
     */
    protected java.lang.String localCONSUMER_NAME_FIELD3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUMER_NAME_FIELD3Tracker = false;

    /**
     * field for CONSUMER_NAME_FIELD4
     */
    protected java.lang.String localCONSUMER_NAME_FIELD4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUMER_NAME_FIELD4Tracker = false;

    /**
     * field for CONSUMER_NAME_FIELD5
     */
    protected java.lang.String localCONSUMER_NAME_FIELD5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONSUMER_NAME_FIELD5Tracker = false;

    /**
     * field for DATE_OF_BIRTH
     */
    protected java.lang.String localDATE_OF_BIRTH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_BIRTHTracker = false;

    /**
     * field for GENDER
     */
    protected java.lang.String localGENDER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDERTracker = false;

    /**
     * field for DT_ERRCODE_PN
     */
    protected java.lang.String localDT_ERRCODE_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_ERRCODE_PNTracker = false;

    /**
     * field for ERR_SEG_TAG_PN
     */
    protected java.lang.String localERR_SEG_TAG_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_SEG_TAG_PNTracker = false;

    /**
     * field for ERR_CODE_PN
     */
    protected java.lang.String localERR_CODE_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_CODE_PNTracker = false;

    /**
     * field for ERROR_PN
     */
    protected java.lang.String localERROR_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERROR_PNTracker = false;

    /**
     * field for DT_ENTCIBILRECODE_PN
     */
    protected java.lang.String localDT_ENTCIBILRECODE_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_ENTCIBILRECODE_PNTracker = false;

    /**
     * field for CIBIL_REMARK_CODE_PN
     */
    protected java.lang.String localCIBIL_REMARK_CODE_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_REMARK_CODE_PNTracker = false;

    /**
     * field for DATE_DISP_REMARK_CODE_PN
     */
    protected java.lang.String localDATE_DISP_REMARK_CODE_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_DISP_REMARK_CODE_PNTracker = false;

    /**
     * field for ERR_DISP_REM_CODE1_PN
     */
    protected java.lang.String localERR_DISP_REM_CODE1_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REM_CODE1_PNTracker = false;

    /**
     * field for ERR_DISP_REM_CODE2_PN
     */
    protected java.lang.String localERR_DISP_REM_CODE2_PN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REM_CODE2_PNTracker = false;

    /**
     * field for ID_TYPE
     */
    protected java.lang.String localID_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_TYPETracker = false;

    /**
     * field for ID_NUMBER
     */
    protected java.lang.String localID_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localID_NUMBERTracker = false;

    /**
     * field for ISSUE_DATE
     */
    protected java.lang.String localISSUE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localISSUE_DATETracker = false;

    /**
     * field for EXPIRATION_DATE
     */
    protected java.lang.String localEXPIRATION_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPIRATION_DATETracker = false;

    /**
     * field for ENRICHED_THROUGH_ENQUIRY_ID
     */
    protected java.lang.String localENRICHED_THROUGH_ENQUIRY_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENRICHED_THROUGH_ENQUIRY_IDTracker = false;

    /**
     * field for TELEPHONE_TYPE
     */
    protected java.lang.String localTELEPHONE_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEPHONE_TYPETracker = false;

    /**
     * field for TELEPHONE_EXTENSION
     */
    protected java.lang.String localTELEPHONE_EXTENSION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEPHONE_EXTENSIONTracker = false;

    /**
     * field for TELEPHONE_NUMBER
     */
    protected java.lang.String localTELEPHONE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTELEPHONE_NUMBERTracker = false;

    /**
     * field for ENRICHED_THROUGH_ENQUIRY_PT
     */
    protected java.lang.String localENRICHED_THROUGH_ENQUIRY_PT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENRICHED_THROUGH_ENQUIRY_PTTracker = false;

    /**
     * field for EMAIL_ID
     */
    protected java.lang.String localEMAIL_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_IDTracker = false;

    /**
     * field for ACCOUNT_TYPE
     */
    protected java.lang.String localACCOUNT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_TYPETracker = false;

    /**
     * field for DATE_REPORTED_AND_CERTIFIED_EM
     */
    protected java.lang.String localDATE_REPORTED_AND_CERTIFIED_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_REPORTED_AND_CERTIFIED_EMTracker = false;

    /**
     * field for OCCUPATION_CODE_EM
     */
    protected java.lang.String localOCCUPATION_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOCCUPATION_CODE_EMTracker = false;

    /**
     * field for INCOME_EM
     */
    protected java.lang.String localINCOME_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCOME_EMTracker = false;

    /**
     * field for NET_GROSS_INDICATOR_EM
     */
    protected java.lang.String localNET_GROSS_INDICATOR_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNET_GROSS_INDICATOR_EMTracker = false;

    /**
     * field for MNTHLY_ANNUAL_INDICATOR_EM
     */
    protected java.lang.String localMNTHLY_ANNUAL_INDICATOR_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMNTHLY_ANNUAL_INDICATOR_EMTracker = false;

    /**
     * field for DATE_ENTRY_ERR_CODE_EM
     */
    protected java.lang.String localDATE_ENTRY_ERR_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_ENTRY_ERR_CODE_EMTracker = false;

    /**
     * field for ERR_CODE_EM
     */
    protected java.lang.String localERR_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_CODE_EMTracker = false;

    /**
     * field for DATE_CIBIL_ERR_CODE_EM
     */
    protected java.lang.String localDATE_CIBIL_ERR_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_CIBIL_ERR_CODE_EMTracker = false;

    /**
     * field for CIBIL_REMARK_CODE_EM
     */
    protected java.lang.String localCIBIL_REMARK_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_REMARK_CODE_EMTracker = false;

    /**
     * field for DT_DIS_REMARK_CODE_EM
     */
    protected java.lang.String localDT_DIS_REMARK_CODE_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_DIS_REMARK_CODE_EMTracker = false;

    /**
     * field for ERR_DISP_REMCODE1_EM
     */
    protected java.lang.String localERR_DISP_REMCODE1_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REMCODE1_EMTracker = false;

    /**
     * field for ERR_DISP_REMCODE2_EM
     */
    protected java.lang.String localERR_DISP_REMCODE2_EM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REMCODE2_EMTracker = false;

    /**
     * field for ACCOUNT_NUMBER_PI
     */
    protected java.lang.String localACCOUNT_NUMBER_PI;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_NUMBER_PITracker = false;

    /**
     * field for SCORE_NAME
     */
    protected java.lang.String localSCORE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_NAMETracker = false;

    /**
     * field for SCORE_CARD_NAME
     */
    protected java.lang.String localSCORE_CARD_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_CARD_NAMETracker = false;

    /**
     * field for SCORE_CARD_VERSION
     */
    protected java.lang.String localSCORE_CARD_VERSION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_CARD_VERSIONTracker = false;

    /**
     * field for SCORE_DATE
     */
    protected java.lang.String localSCORE_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORE_DATETracker = false;

    /**
     * field for SCORE
     */
    protected java.lang.String localSCORE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCORETracker = false;

    /**
     * field for EXCLUSION_CODES_1_TO_5
     */
    protected java.lang.String localEXCLUSION_CODES_1_TO_5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUSION_CODES_1_TO_5Tracker = false;

    /**
     * field for EXCLUSION_CODES_6_TO_10
     */
    protected java.lang.String localEXCLUSION_CODES_6_TO_10;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUSION_CODES_6_TO_10Tracker = false;

    /**
     * field for EXCLUSION_CODES_11_TO_15
     */
    protected java.lang.String localEXCLUSION_CODES_11_TO_15;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUSION_CODES_11_TO_15Tracker = false;

    /**
     * field for EXCLUSION_CODES_16_TO_20
     */
    protected java.lang.String localEXCLUSION_CODES_16_TO_20;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUSION_CODES_16_TO_20Tracker = false;

    /**
     * field for REASON_CODES_1_TO_5
     */
    protected java.lang.String localREASON_CODES_1_TO_5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_1_TO_5Tracker = false;

    /**
     * field for REASON_CODES_6_TO_10
     */
    protected java.lang.String localREASON_CODES_6_TO_10;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_6_TO_10Tracker = false;

    /**
     * field for REASON_CODES_11_TO_15
     */
    protected java.lang.String localREASON_CODES_11_TO_15;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_11_TO_15Tracker = false;

    /**
     * field for REASON_CODES_16_TO_20
     */
    protected java.lang.String localREASON_CODES_16_TO_20;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_16_TO_20Tracker = false;

    /**
     * field for REASON_CODES_21_TO_25
     */
    protected java.lang.String localREASON_CODES_21_TO_25;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_21_TO_25Tracker = false;

    /**
     * field for REASON_CODES_26_TO_30
     */
    protected java.lang.String localREASON_CODES_26_TO_30;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_26_TO_30Tracker = false;

    /**
     * field for REASON_CODES_31_TO_35
     */
    protected java.lang.String localREASON_CODES_31_TO_35;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_31_TO_35Tracker = false;

    /**
     * field for REASON_CODES_36_TO_40
     */
    protected java.lang.String localREASON_CODES_36_TO_40;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_36_TO_40Tracker = false;

    /**
     * field for REASON_CODES_41_TO_45
     */
    protected java.lang.String localREASON_CODES_41_TO_45;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_41_TO_45Tracker = false;

    /**
     * field for REASON_CODES_46_TO_50
     */
    protected java.lang.String localREASON_CODES_46_TO_50;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREASON_CODES_46_TO_50Tracker = false;

    /**
     * field for ERROR_CODE_SC
     */
    protected java.lang.String localERROR_CODE_SC;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERROR_CODE_SCTracker = false;

    /**
     * field for ADDRESS_LINE_1
     */
    protected java.lang.String localADDRESS_LINE_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_LINE_1Tracker = false;

    /**
     * field for ADDRESS_LINE_2
     */
    protected java.lang.String localADDRESS_LINE_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_LINE_2Tracker = false;

    /**
     * field for ADDRESS_LINE_3
     */
    protected java.lang.String localADDRESS_LINE_3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_LINE_3Tracker = false;

    /**
     * field for ADDRESS_LINE_4
     */
    protected java.lang.String localADDRESS_LINE_4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_LINE_4Tracker = false;

    /**
     * field for ADDRESS_LINE_5
     */
    protected java.lang.String localADDRESS_LINE_5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_LINE_5Tracker = false;

    /**
     * field for STATE_CODE
     */
    protected java.lang.String localSTATE_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATE_CODETracker = false;

    /**
     * field for STATE
     */
    protected java.lang.String localSTATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATETracker = false;

    /**
     * field for PINCODE
     */
    protected java.lang.String localPINCODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPINCODETracker = false;

    /**
     * field for ADDRESS_CATERGORY
     */
    protected java.lang.String localADDRESS_CATERGORY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_CATERGORYTracker = false;

    /**
     * field for RESIDENCE_CODE
     */
    protected java.lang.String localRESIDENCE_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRESIDENCE_CODETracker = false;

    /**
     * field for DATE_REPORTED_PA
     */
    protected java.lang.String localDATE_REPORTED_PA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_REPORTED_PATracker = false;

    /**
     * field for ENRICHED_THROUGH_ENQUIRY_PA
     */
    protected java.lang.String localENRICHED_THROUGH_ENQUIRY_PA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENRICHED_THROUGH_ENQUIRY_PATracker = false;

    /**
     * field for REPO_MEMSHORTNAME_TL
     */
    protected java.lang.String localREPO_MEMSHORTNAME_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPO_MEMSHORTNAME_TLTracker = false;

    /**
     * field for ACCOUNT_NUMBER_TL
     */
    protected java.lang.String localACCOUNT_NUMBER_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_NUMBER_TLTracker = false;

    /**
     * field for ACCOUNT_TYPE_TL
     */
    protected java.lang.String localACCOUNT_TYPE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_TYPE_TLTracker = false;

    /**
     * field for OWNERSHIP_INDICATOR_TL
     */
    protected java.lang.String localOWNERSHIP_INDICATOR_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIP_INDICATOR_TLTracker = false;

    /**
     * field for DATE_OPENED_DISBURSED_TL
     */
    protected java.lang.String localDATE_OPENED_DISBURSED_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OPENED_DISBURSED_TLTracker = false;

    /**
     * field for DATE_OF_LAST_PAYMENT_TL
     */
    protected java.lang.String localDATE_OF_LAST_PAYMENT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_LAST_PAYMENT_TLTracker = false;

    /**
     * field for DATE_CLOSED_TL
     */
    protected java.lang.String localDATE_CLOSED_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_CLOSED_TLTracker = false;

    /**
     * field for TL_DATE_REPORTED
     */
    protected java.lang.String localTL_DATE_REPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTL_DATE_REPORTEDTracker = false;

    /**
     * field for HIGH_CREDIT_SANCTIONED_AMOUNT
     */
    protected java.lang.String localHIGH_CREDIT_SANCTIONED_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHIGH_CREDIT_SANCTIONED_AMOUNTTracker = false;

    /**
     * field for CURRENT_BALANCE_TL
     */
    protected java.lang.String localCURRENT_BALANCE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENT_BALANCE_TLTracker = false;

    /**
     * field for AMOUNT_OVERDUE_TL
     */
    protected java.lang.String localAMOUNT_OVERDUE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAMOUNT_OVERDUE_TLTracker = false;

    /**
     * field for PAYMENT_HISTORY_1
     */
    protected java.lang.String localPAYMENT_HISTORY_1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENT_HISTORY_1Tracker = false;

    /**
     * field for PAYMENT_HISTORY_2
     */
    protected java.lang.String localPAYMENT_HISTORY_2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENT_HISTORY_2Tracker = false;

    /**
     * field for PAYMENT_HISTORY_START_DATE
     */
    protected java.lang.String localPAYMENT_HISTORY_START_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENT_HISTORY_START_DATETracker = false;

    /**
     * field for PAYMENT_HISTORY_END_DATE
     */
    protected java.lang.String localPAYMENT_HISTORY_END_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENT_HISTORY_END_DATETracker = false;

    /**
     * field for SUIT_FILED_STATUS_TL
     */
    protected java.lang.String localSUIT_FILED_STATUS_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUIT_FILED_STATUS_TLTracker = false;

    /**
     * field for WOF_SETTLED_STATUS_TL
     */
    protected java.lang.String localWOF_SETTLED_STATUS_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWOF_SETTLED_STATUS_TLTracker = false;

    /**
     * field for VALOFCOLLATERAL_TL
     */
    protected java.lang.String localVALOFCOLLATERAL_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVALOFCOLLATERAL_TLTracker = false;

    /**
     * field for TYPEOFCOLLATERAL_TL
     */
    protected java.lang.String localTYPEOFCOLLATERAL_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTYPEOFCOLLATERAL_TLTracker = false;

    /**
     * field for CREDITLIMIT_TL
     */
    protected java.lang.String localCREDITLIMIT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDITLIMIT_TLTracker = false;

    /**
     * field for CASHLIMIT_TL
     */
    protected java.lang.String localCASHLIMIT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCASHLIMIT_TLTracker = false;

    /**
     * field for RATEOFINTREST_TL
     */
    protected java.lang.String localRATEOFINTREST_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATEOFINTREST_TLTracker = false;

    /**
     * field for REPAY_TENURE
     */
    protected java.lang.String localREPAY_TENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPAY_TENURETracker = false;

    /**
     * field for EMI_AMOUNT_TL
     */
    protected java.lang.String localEMI_AMOUNT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMI_AMOUNT_TLTracker = false;

    /**
     * field for WOF_TOT_AMOUNT_TL
     */
    protected java.lang.String localWOF_TOT_AMOUNT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWOF_TOT_AMOUNT_TLTracker = false;

    /**
     * field for WOF_PRINCIPAL_TL
     */
    protected java.lang.String localWOF_PRINCIPAL_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWOF_PRINCIPAL_TLTracker = false;

    /**
     * field for SETTLEMENT_AMOUNT_TL
     */
    protected java.lang.String localSETTLEMENT_AMOUNT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSETTLEMENT_AMOUNT_TLTracker = false;

    /**
     * field for PAYMENT_FREQUENCY_TL
     */
    protected java.lang.String localPAYMENT_FREQUENCY_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAYMENT_FREQUENCY_TLTracker = false;

    /**
     * field for ACTUAL_PAYMENT_AMOUNT_TL
     */
    protected java.lang.String localACTUAL_PAYMENT_AMOUNT_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACTUAL_PAYMENT_AMOUNT_TLTracker = false;

    /**
     * field for DT_ENTERRCODE_TL
     */
    protected java.lang.String localDT_ENTERRCODE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_ENTERRCODE_TLTracker = false;

    /**
     * field for ERRCODE_TL
     */
    protected java.lang.String localERRCODE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERRCODE_TLTracker = false;

    /**
     * field for DT_ENTCIBIL_REMARK_CODE_TL
     */
    protected java.lang.String localDT_ENTCIBIL_REMARK_CODE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_ENTCIBIL_REMARK_CODE_TLTracker = false;

    /**
     * field for CIBIL_REMARKS_CODE_TL
     */
    protected java.lang.String localCIBIL_REMARKS_CODE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCIBIL_REMARKS_CODE_TLTracker = false;

    /**
     * field for DT_DISPUTE_CODE_TL
     */
    protected java.lang.String localDT_DISPUTE_CODE_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDT_DISPUTE_CODE_TLTracker = false;

    /**
     * field for ERR_DISP_REMCODE1_TL
     */
    protected java.lang.String localERR_DISP_REMCODE1_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REMCODE1_TLTracker = false;

    /**
     * field for ERR_DISP_REMCODE2_TL
     */
    protected java.lang.String localERR_DISP_REMCODE2_TL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERR_DISP_REMCODE2_TLTracker = false;

    /**
     * field for DATE_OF_ENQUIRY_IQ
     */
    protected java.lang.String localDATE_OF_ENQUIRY_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_ENQUIRY_IQTracker = false;

    /**
     * field for ENQ_MEMBER_SHORT_NAME_IQ
     */
    protected java.lang.String localENQ_MEMBER_SHORT_NAME_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQ_MEMBER_SHORT_NAME_IQTracker = false;

    /**
     * field for ENQUIRY_PURPOSE_IQ
     */
    protected java.lang.String localENQUIRY_PURPOSE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQUIRY_PURPOSE_IQTracker = false;

    /**
     * field for ENQIURY_AMOUNT_IQ
     */
    protected java.lang.String localENQIURY_AMOUNT_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENQIURY_AMOUNT_IQTracker = false;

    /**
     * field for DATE_OF_ENTRY_DR
     */
    protected java.lang.String localDATE_OF_ENTRY_DR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_ENTRY_DRTracker = false;

    /**
     * field for DISP_REM_LINE1
     */
    protected java.lang.String localDISP_REM_LINE1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE1Tracker = false;

    /**
     * field for DISP_REM_LINE2
     */
    protected java.lang.String localDISP_REM_LINE2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE2Tracker = false;

    /**
     * field for DISP_REM_LINE3
     */
    protected java.lang.String localDISP_REM_LINE3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE3Tracker = false;

    /**
     * field for DISP_REM_LINE4
     */
    protected java.lang.String localDISP_REM_LINE4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE4Tracker = false;

    /**
     * field for DISP_REM_LINE5
     */
    protected java.lang.String localDISP_REM_LINE5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE5Tracker = false;

    /**
     * field for DISP_REM_LINE6
     */
    protected java.lang.String localDISP_REM_LINE6;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISP_REM_LINE6Tracker = false;

    /**
     * field for OUTPUT_WRITE_FLAG
     */
    protected java.lang.String localOUTPUT_WRITE_FLAG;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_FLAGTracker = false;

    /**
     * field for OUTPUT_WRITE_TIME
     */
    protected java.lang.String localOUTPUT_WRITE_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_WRITE_TIMETracker = false;

    /**
     * field for OUTPUT_READ_TIME
     */
    protected java.lang.String localOUTPUT_READ_TIME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOUTPUT_READ_TIMETracker = false;

    /**
     * field for Respfiller1
     */
    protected java.lang.String localRespfiller1;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller1Tracker = false;

    /**
     * field for Respfiller2
     */
    protected java.lang.String localRespfiller2;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller2Tracker = false;

    /**
     * field for Respfiller3
     */
    protected java.lang.String localRespfiller3;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller3Tracker = false;

    /**
     * field for Respfiller4
     */
    protected java.lang.String localRespfiller4;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller4Tracker = false;

    /**
     * field for Respfiller5
     */
    protected java.lang.String localRespfiller5;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRespfiller5Tracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isDATE_PROCESSEDSpecified() {
        return localDATE_PROCESSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_PROCESSED() {
        return localDATE_PROCESSED;
    }

    /**
     * Auto generated setter method
     * @param param DATE_PROCESSED
     */
    public void setDATE_PROCESSED(java.lang.String param) {
        localDATE_PROCESSEDTracker = param != null;

        this.localDATE_PROCESSED = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isSUBJECT_RETURN_CODESpecified() {
        return localSUBJECT_RETURN_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBJECT_RETURN_CODE() {
        return localSUBJECT_RETURN_CODE;
    }

    /**
     * Auto generated setter method
     * @param param SUBJECT_RETURN_CODE
     */
    public void setSUBJECT_RETURN_CODE(java.lang.String param) {
        localSUBJECT_RETURN_CODETracker = param != null;

        this.localSUBJECT_RETURN_CODE = param;
    }

    public boolean isENQUIRY_CONTROL_NUMBERSpecified() {
        return localENQUIRY_CONTROL_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_CONTROL_NUMBER() {
        return localENQUIRY_CONTROL_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_CONTROL_NUMBER
     */
    public void setENQUIRY_CONTROL_NUMBER(java.lang.String param) {
        localENQUIRY_CONTROL_NUMBERTracker = param != null;

        this.localENQUIRY_CONTROL_NUMBER = param;
    }

    public boolean isCONSUMER_NAME_FIELD1Specified() {
        return localCONSUMER_NAME_FIELD1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUMER_NAME_FIELD1() {
        return localCONSUMER_NAME_FIELD1;
    }

    /**
     * Auto generated setter method
     * @param param CONSUMER_NAME_FIELD1
     */
    public void setCONSUMER_NAME_FIELD1(java.lang.String param) {
        localCONSUMER_NAME_FIELD1Tracker = param != null;

        this.localCONSUMER_NAME_FIELD1 = param;
    }

    public boolean isCONSUME_NAME_FIELD2Specified() {
        return localCONSUME_NAME_FIELD2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUME_NAME_FIELD2() {
        return localCONSUME_NAME_FIELD2;
    }

    /**
     * Auto generated setter method
     * @param param CONSUME_NAME_FIELD2
     */
    public void setCONSUME_NAME_FIELD2(java.lang.String param) {
        localCONSUME_NAME_FIELD2Tracker = param != null;

        this.localCONSUME_NAME_FIELD2 = param;
    }

    public boolean isCONSUMER_NAME_FIELD3Specified() {
        return localCONSUMER_NAME_FIELD3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUMER_NAME_FIELD3() {
        return localCONSUMER_NAME_FIELD3;
    }

    /**
     * Auto generated setter method
     * @param param CONSUMER_NAME_FIELD3
     */
    public void setCONSUMER_NAME_FIELD3(java.lang.String param) {
        localCONSUMER_NAME_FIELD3Tracker = param != null;

        this.localCONSUMER_NAME_FIELD3 = param;
    }

    public boolean isCONSUMER_NAME_FIELD4Specified() {
        return localCONSUMER_NAME_FIELD4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUMER_NAME_FIELD4() {
        return localCONSUMER_NAME_FIELD4;
    }

    /**
     * Auto generated setter method
     * @param param CONSUMER_NAME_FIELD4
     */
    public void setCONSUMER_NAME_FIELD4(java.lang.String param) {
        localCONSUMER_NAME_FIELD4Tracker = param != null;

        this.localCONSUMER_NAME_FIELD4 = param;
    }

    public boolean isCONSUMER_NAME_FIELD5Specified() {
        return localCONSUMER_NAME_FIELD5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONSUMER_NAME_FIELD5() {
        return localCONSUMER_NAME_FIELD5;
    }

    /**
     * Auto generated setter method
     * @param param CONSUMER_NAME_FIELD5
     */
    public void setCONSUMER_NAME_FIELD5(java.lang.String param) {
        localCONSUMER_NAME_FIELD5Tracker = param != null;

        this.localCONSUMER_NAME_FIELD5 = param;
    }

    public boolean isDATE_OF_BIRTHSpecified() {
        return localDATE_OF_BIRTHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_BIRTH() {
        return localDATE_OF_BIRTH;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_BIRTH
     */
    public void setDATE_OF_BIRTH(java.lang.String param) {
        localDATE_OF_BIRTHTracker = param != null;

        this.localDATE_OF_BIRTH = param;
    }

    public boolean isGENDERSpecified() {
        return localGENDERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER() {
        return localGENDER;
    }

    /**
     * Auto generated setter method
     * @param param GENDER
     */
    public void setGENDER(java.lang.String param) {
        localGENDERTracker = param != null;

        this.localGENDER = param;
    }

    public boolean isDT_ERRCODE_PNSpecified() {
        return localDT_ERRCODE_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_ERRCODE_PN() {
        return localDT_ERRCODE_PN;
    }

    /**
     * Auto generated setter method
     * @param param DT_ERRCODE_PN
     */
    public void setDT_ERRCODE_PN(java.lang.String param) {
        localDT_ERRCODE_PNTracker = param != null;

        this.localDT_ERRCODE_PN = param;
    }

    public boolean isERR_SEG_TAG_PNSpecified() {
        return localERR_SEG_TAG_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_SEG_TAG_PN() {
        return localERR_SEG_TAG_PN;
    }

    /**
     * Auto generated setter method
     * @param param ERR_SEG_TAG_PN
     */
    public void setERR_SEG_TAG_PN(java.lang.String param) {
        localERR_SEG_TAG_PNTracker = param != null;

        this.localERR_SEG_TAG_PN = param;
    }

    public boolean isERR_CODE_PNSpecified() {
        return localERR_CODE_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_CODE_PN() {
        return localERR_CODE_PN;
    }

    /**
     * Auto generated setter method
     * @param param ERR_CODE_PN
     */
    public void setERR_CODE_PN(java.lang.String param) {
        localERR_CODE_PNTracker = param != null;

        this.localERR_CODE_PN = param;
    }

    public boolean isERROR_PNSpecified() {
        return localERROR_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERROR_PN() {
        return localERROR_PN;
    }

    /**
     * Auto generated setter method
     * @param param ERROR_PN
     */
    public void setERROR_PN(java.lang.String param) {
        localERROR_PNTracker = param != null;

        this.localERROR_PN = param;
    }

    public boolean isDT_ENTCIBILRECODE_PNSpecified() {
        return localDT_ENTCIBILRECODE_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_ENTCIBILRECODE_PN() {
        return localDT_ENTCIBILRECODE_PN;
    }

    /**
     * Auto generated setter method
     * @param param DT_ENTCIBILRECODE_PN
     */
    public void setDT_ENTCIBILRECODE_PN(java.lang.String param) {
        localDT_ENTCIBILRECODE_PNTracker = param != null;

        this.localDT_ENTCIBILRECODE_PN = param;
    }

    public boolean isCIBIL_REMARK_CODE_PNSpecified() {
        return localCIBIL_REMARK_CODE_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCIBIL_REMARK_CODE_PN() {
        return localCIBIL_REMARK_CODE_PN;
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_REMARK_CODE_PN
     */
    public void setCIBIL_REMARK_CODE_PN(java.lang.String param) {
        localCIBIL_REMARK_CODE_PNTracker = param != null;

        this.localCIBIL_REMARK_CODE_PN = param;
    }

    public boolean isDATE_DISP_REMARK_CODE_PNSpecified() {
        return localDATE_DISP_REMARK_CODE_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_DISP_REMARK_CODE_PN() {
        return localDATE_DISP_REMARK_CODE_PN;
    }

    /**
     * Auto generated setter method
     * @param param DATE_DISP_REMARK_CODE_PN
     */
    public void setDATE_DISP_REMARK_CODE_PN(java.lang.String param) {
        localDATE_DISP_REMARK_CODE_PNTracker = param != null;

        this.localDATE_DISP_REMARK_CODE_PN = param;
    }

    public boolean isERR_DISP_REM_CODE1_PNSpecified() {
        return localERR_DISP_REM_CODE1_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REM_CODE1_PN() {
        return localERR_DISP_REM_CODE1_PN;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REM_CODE1_PN
     */
    public void setERR_DISP_REM_CODE1_PN(java.lang.String param) {
        localERR_DISP_REM_CODE1_PNTracker = param != null;

        this.localERR_DISP_REM_CODE1_PN = param;
    }

    public boolean isERR_DISP_REM_CODE2_PNSpecified() {
        return localERR_DISP_REM_CODE2_PNTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REM_CODE2_PN() {
        return localERR_DISP_REM_CODE2_PN;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REM_CODE2_PN
     */
    public void setERR_DISP_REM_CODE2_PN(java.lang.String param) {
        localERR_DISP_REM_CODE2_PNTracker = param != null;

        this.localERR_DISP_REM_CODE2_PN = param;
    }

    public boolean isID_TYPESpecified() {
        return localID_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_TYPE() {
        return localID_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param ID_TYPE
     */
    public void setID_TYPE(java.lang.String param) {
        localID_TYPETracker = param != null;

        this.localID_TYPE = param;
    }

    public boolean isID_NUMBERSpecified() {
        return localID_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getID_NUMBER() {
        return localID_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param ID_NUMBER
     */
    public void setID_NUMBER(java.lang.String param) {
        localID_NUMBERTracker = param != null;

        this.localID_NUMBER = param;
    }

    public boolean isISSUE_DATESpecified() {
        return localISSUE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getISSUE_DATE() {
        return localISSUE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param ISSUE_DATE
     */
    public void setISSUE_DATE(java.lang.String param) {
        localISSUE_DATETracker = param != null;

        this.localISSUE_DATE = param;
    }

    public boolean isEXPIRATION_DATESpecified() {
        return localEXPIRATION_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXPIRATION_DATE() {
        return localEXPIRATION_DATE;
    }

    /**
     * Auto generated setter method
     * @param param EXPIRATION_DATE
     */
    public void setEXPIRATION_DATE(java.lang.String param) {
        localEXPIRATION_DATETracker = param != null;

        this.localEXPIRATION_DATE = param;
    }

    public boolean isENRICHED_THROUGH_ENQUIRY_IDSpecified() {
        return localENRICHED_THROUGH_ENQUIRY_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENRICHED_THROUGH_ENQUIRY_ID() {
        return localENRICHED_THROUGH_ENQUIRY_ID;
    }

    /**
     * Auto generated setter method
     * @param param ENRICHED_THROUGH_ENQUIRY_ID
     */
    public void setENRICHED_THROUGH_ENQUIRY_ID(java.lang.String param) {
        localENRICHED_THROUGH_ENQUIRY_IDTracker = param != null;

        this.localENRICHED_THROUGH_ENQUIRY_ID = param;
    }

    public boolean isTELEPHONE_TYPESpecified() {
        return localTELEPHONE_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEPHONE_TYPE() {
        return localTELEPHONE_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param TELEPHONE_TYPE
     */
    public void setTELEPHONE_TYPE(java.lang.String param) {
        localTELEPHONE_TYPETracker = param != null;

        this.localTELEPHONE_TYPE = param;
    }

    public boolean isTELEPHONE_EXTENSIONSpecified() {
        return localTELEPHONE_EXTENSIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEPHONE_EXTENSION() {
        return localTELEPHONE_EXTENSION;
    }

    /**
     * Auto generated setter method
     * @param param TELEPHONE_EXTENSION
     */
    public void setTELEPHONE_EXTENSION(java.lang.String param) {
        localTELEPHONE_EXTENSIONTracker = param != null;

        this.localTELEPHONE_EXTENSION = param;
    }

    public boolean isTELEPHONE_NUMBERSpecified() {
        return localTELEPHONE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTELEPHONE_NUMBER() {
        return localTELEPHONE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param TELEPHONE_NUMBER
     */
    public void setTELEPHONE_NUMBER(java.lang.String param) {
        localTELEPHONE_NUMBERTracker = param != null;

        this.localTELEPHONE_NUMBER = param;
    }

    public boolean isENRICHED_THROUGH_ENQUIRY_PTSpecified() {
        return localENRICHED_THROUGH_ENQUIRY_PTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENRICHED_THROUGH_ENQUIRY_PT() {
        return localENRICHED_THROUGH_ENQUIRY_PT;
    }

    /**
     * Auto generated setter method
     * @param param ENRICHED_THROUGH_ENQUIRY_PT
     */
    public void setENRICHED_THROUGH_ENQUIRY_PT(java.lang.String param) {
        localENRICHED_THROUGH_ENQUIRY_PTTracker = param != null;

        this.localENRICHED_THROUGH_ENQUIRY_PT = param;
    }

    public boolean isEMAIL_IDSpecified() {
        return localEMAIL_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_ID() {
        return localEMAIL_ID;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_ID
     */
    public void setEMAIL_ID(java.lang.String param) {
        localEMAIL_IDTracker = param != null;

        this.localEMAIL_ID = param;
    }

    public boolean isACCOUNT_TYPESpecified() {
        return localACCOUNT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_TYPE() {
        return localACCOUNT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_TYPE
     */
    public void setACCOUNT_TYPE(java.lang.String param) {
        localACCOUNT_TYPETracker = param != null;

        this.localACCOUNT_TYPE = param;
    }

    public boolean isDATE_REPORTED_AND_CERTIFIED_EMSpecified() {
        return localDATE_REPORTED_AND_CERTIFIED_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_REPORTED_AND_CERTIFIED_EM() {
        return localDATE_REPORTED_AND_CERTIFIED_EM;
    }

    /**
     * Auto generated setter method
     * @param param DATE_REPORTED_AND_CERTIFIED_EM
     */
    public void setDATE_REPORTED_AND_CERTIFIED_EM(java.lang.String param) {
        localDATE_REPORTED_AND_CERTIFIED_EMTracker = param != null;

        this.localDATE_REPORTED_AND_CERTIFIED_EM = param;
    }

    public boolean isOCCUPATION_CODE_EMSpecified() {
        return localOCCUPATION_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOCCUPATION_CODE_EM() {
        return localOCCUPATION_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param OCCUPATION_CODE_EM
     */
    public void setOCCUPATION_CODE_EM(java.lang.String param) {
        localOCCUPATION_CODE_EMTracker = param != null;

        this.localOCCUPATION_CODE_EM = param;
    }

    public boolean isINCOME_EMSpecified() {
        return localINCOME_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCOME_EM() {
        return localINCOME_EM;
    }

    /**
     * Auto generated setter method
     * @param param INCOME_EM
     */
    public void setINCOME_EM(java.lang.String param) {
        localINCOME_EMTracker = param != null;

        this.localINCOME_EM = param;
    }

    public boolean isNET_GROSS_INDICATOR_EMSpecified() {
        return localNET_GROSS_INDICATOR_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNET_GROSS_INDICATOR_EM() {
        return localNET_GROSS_INDICATOR_EM;
    }

    /**
     * Auto generated setter method
     * @param param NET_GROSS_INDICATOR_EM
     */
    public void setNET_GROSS_INDICATOR_EM(java.lang.String param) {
        localNET_GROSS_INDICATOR_EMTracker = param != null;

        this.localNET_GROSS_INDICATOR_EM = param;
    }

    public boolean isMNTHLY_ANNUAL_INDICATOR_EMSpecified() {
        return localMNTHLY_ANNUAL_INDICATOR_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMNTHLY_ANNUAL_INDICATOR_EM() {
        return localMNTHLY_ANNUAL_INDICATOR_EM;
    }

    /**
     * Auto generated setter method
     * @param param MNTHLY_ANNUAL_INDICATOR_EM
     */
    public void setMNTHLY_ANNUAL_INDICATOR_EM(java.lang.String param) {
        localMNTHLY_ANNUAL_INDICATOR_EMTracker = param != null;

        this.localMNTHLY_ANNUAL_INDICATOR_EM = param;
    }

    public boolean isDATE_ENTRY_ERR_CODE_EMSpecified() {
        return localDATE_ENTRY_ERR_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_ENTRY_ERR_CODE_EM() {
        return localDATE_ENTRY_ERR_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param DATE_ENTRY_ERR_CODE_EM
     */
    public void setDATE_ENTRY_ERR_CODE_EM(java.lang.String param) {
        localDATE_ENTRY_ERR_CODE_EMTracker = param != null;

        this.localDATE_ENTRY_ERR_CODE_EM = param;
    }

    public boolean isERR_CODE_EMSpecified() {
        return localERR_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_CODE_EM() {
        return localERR_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param ERR_CODE_EM
     */
    public void setERR_CODE_EM(java.lang.String param) {
        localERR_CODE_EMTracker = param != null;

        this.localERR_CODE_EM = param;
    }

    public boolean isDATE_CIBIL_ERR_CODE_EMSpecified() {
        return localDATE_CIBIL_ERR_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_CIBIL_ERR_CODE_EM() {
        return localDATE_CIBIL_ERR_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param DATE_CIBIL_ERR_CODE_EM
     */
    public void setDATE_CIBIL_ERR_CODE_EM(java.lang.String param) {
        localDATE_CIBIL_ERR_CODE_EMTracker = param != null;

        this.localDATE_CIBIL_ERR_CODE_EM = param;
    }

    public boolean isCIBIL_REMARK_CODE_EMSpecified() {
        return localCIBIL_REMARK_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCIBIL_REMARK_CODE_EM() {
        return localCIBIL_REMARK_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_REMARK_CODE_EM
     */
    public void setCIBIL_REMARK_CODE_EM(java.lang.String param) {
        localCIBIL_REMARK_CODE_EMTracker = param != null;

        this.localCIBIL_REMARK_CODE_EM = param;
    }

    public boolean isDT_DIS_REMARK_CODE_EMSpecified() {
        return localDT_DIS_REMARK_CODE_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_DIS_REMARK_CODE_EM() {
        return localDT_DIS_REMARK_CODE_EM;
    }

    /**
     * Auto generated setter method
     * @param param DT_DIS_REMARK_CODE_EM
     */
    public void setDT_DIS_REMARK_CODE_EM(java.lang.String param) {
        localDT_DIS_REMARK_CODE_EMTracker = param != null;

        this.localDT_DIS_REMARK_CODE_EM = param;
    }

    public boolean isERR_DISP_REMCODE1_EMSpecified() {
        return localERR_DISP_REMCODE1_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REMCODE1_EM() {
        return localERR_DISP_REMCODE1_EM;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REMCODE1_EM
     */
    public void setERR_DISP_REMCODE1_EM(java.lang.String param) {
        localERR_DISP_REMCODE1_EMTracker = param != null;

        this.localERR_DISP_REMCODE1_EM = param;
    }

    public boolean isERR_DISP_REMCODE2_EMSpecified() {
        return localERR_DISP_REMCODE2_EMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REMCODE2_EM() {
        return localERR_DISP_REMCODE2_EM;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REMCODE2_EM
     */
    public void setERR_DISP_REMCODE2_EM(java.lang.String param) {
        localERR_DISP_REMCODE2_EMTracker = param != null;

        this.localERR_DISP_REMCODE2_EM = param;
    }

    public boolean isACCOUNT_NUMBER_PISpecified() {
        return localACCOUNT_NUMBER_PITracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_NUMBER_PI() {
        return localACCOUNT_NUMBER_PI;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_NUMBER_PI
     */
    public void setACCOUNT_NUMBER_PI(java.lang.String param) {
        localACCOUNT_NUMBER_PITracker = param != null;

        this.localACCOUNT_NUMBER_PI = param;
    }

    public boolean isSCORE_NAMESpecified() {
        return localSCORE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_NAME() {
        return localSCORE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_NAME
     */
    public void setSCORE_NAME(java.lang.String param) {
        localSCORE_NAMETracker = param != null;

        this.localSCORE_NAME = param;
    }

    public boolean isSCORE_CARD_NAMESpecified() {
        return localSCORE_CARD_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_CARD_NAME() {
        return localSCORE_CARD_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_CARD_NAME
     */
    public void setSCORE_CARD_NAME(java.lang.String param) {
        localSCORE_CARD_NAMETracker = param != null;

        this.localSCORE_CARD_NAME = param;
    }

    public boolean isSCORE_CARD_VERSIONSpecified() {
        return localSCORE_CARD_VERSIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_CARD_VERSION() {
        return localSCORE_CARD_VERSION;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_CARD_VERSION
     */
    public void setSCORE_CARD_VERSION(java.lang.String param) {
        localSCORE_CARD_VERSIONTracker = param != null;

        this.localSCORE_CARD_VERSION = param;
    }

    public boolean isSCORE_DATESpecified() {
        return localSCORE_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE_DATE() {
        return localSCORE_DATE;
    }

    /**
     * Auto generated setter method
     * @param param SCORE_DATE
     */
    public void setSCORE_DATE(java.lang.String param) {
        localSCORE_DATETracker = param != null;

        this.localSCORE_DATE = param;
    }

    public boolean isSCORESpecified() {
        return localSCORETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCORE() {
        return localSCORE;
    }

    /**
     * Auto generated setter method
     * @param param SCORE
     */
    public void setSCORE(java.lang.String param) {
        localSCORETracker = param != null;

        this.localSCORE = param;
    }

    public boolean isEXCLUSION_CODES_1_TO_5Specified() {
        return localEXCLUSION_CODES_1_TO_5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUSION_CODES_1_TO_5() {
        return localEXCLUSION_CODES_1_TO_5;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUSION_CODES_1_TO_5
     */
    public void setEXCLUSION_CODES_1_TO_5(java.lang.String param) {
        localEXCLUSION_CODES_1_TO_5Tracker = param != null;

        this.localEXCLUSION_CODES_1_TO_5 = param;
    }

    public boolean isEXCLUSION_CODES_6_TO_10Specified() {
        return localEXCLUSION_CODES_6_TO_10Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUSION_CODES_6_TO_10() {
        return localEXCLUSION_CODES_6_TO_10;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUSION_CODES_6_TO_10
     */
    public void setEXCLUSION_CODES_6_TO_10(java.lang.String param) {
        localEXCLUSION_CODES_6_TO_10Tracker = param != null;

        this.localEXCLUSION_CODES_6_TO_10 = param;
    }

    public boolean isEXCLUSION_CODES_11_TO_15Specified() {
        return localEXCLUSION_CODES_11_TO_15Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUSION_CODES_11_TO_15() {
        return localEXCLUSION_CODES_11_TO_15;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUSION_CODES_11_TO_15
     */
    public void setEXCLUSION_CODES_11_TO_15(java.lang.String param) {
        localEXCLUSION_CODES_11_TO_15Tracker = param != null;

        this.localEXCLUSION_CODES_11_TO_15 = param;
    }

    public boolean isEXCLUSION_CODES_16_TO_20Specified() {
        return localEXCLUSION_CODES_16_TO_20Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUSION_CODES_16_TO_20() {
        return localEXCLUSION_CODES_16_TO_20;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUSION_CODES_16_TO_20
     */
    public void setEXCLUSION_CODES_16_TO_20(java.lang.String param) {
        localEXCLUSION_CODES_16_TO_20Tracker = param != null;

        this.localEXCLUSION_CODES_16_TO_20 = param;
    }

    public boolean isREASON_CODES_1_TO_5Specified() {
        return localREASON_CODES_1_TO_5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_1_TO_5() {
        return localREASON_CODES_1_TO_5;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_1_TO_5
     */
    public void setREASON_CODES_1_TO_5(java.lang.String param) {
        localREASON_CODES_1_TO_5Tracker = param != null;

        this.localREASON_CODES_1_TO_5 = param;
    }

    public boolean isREASON_CODES_6_TO_10Specified() {
        return localREASON_CODES_6_TO_10Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_6_TO_10() {
        return localREASON_CODES_6_TO_10;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_6_TO_10
     */
    public void setREASON_CODES_6_TO_10(java.lang.String param) {
        localREASON_CODES_6_TO_10Tracker = param != null;

        this.localREASON_CODES_6_TO_10 = param;
    }

    public boolean isREASON_CODES_11_TO_15Specified() {
        return localREASON_CODES_11_TO_15Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_11_TO_15() {
        return localREASON_CODES_11_TO_15;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_11_TO_15
     */
    public void setREASON_CODES_11_TO_15(java.lang.String param) {
        localREASON_CODES_11_TO_15Tracker = param != null;

        this.localREASON_CODES_11_TO_15 = param;
    }

    public boolean isREASON_CODES_16_TO_20Specified() {
        return localREASON_CODES_16_TO_20Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_16_TO_20() {
        return localREASON_CODES_16_TO_20;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_16_TO_20
     */
    public void setREASON_CODES_16_TO_20(java.lang.String param) {
        localREASON_CODES_16_TO_20Tracker = param != null;

        this.localREASON_CODES_16_TO_20 = param;
    }

    public boolean isREASON_CODES_21_TO_25Specified() {
        return localREASON_CODES_21_TO_25Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_21_TO_25() {
        return localREASON_CODES_21_TO_25;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_21_TO_25
     */
    public void setREASON_CODES_21_TO_25(java.lang.String param) {
        localREASON_CODES_21_TO_25Tracker = param != null;

        this.localREASON_CODES_21_TO_25 = param;
    }

    public boolean isREASON_CODES_26_TO_30Specified() {
        return localREASON_CODES_26_TO_30Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_26_TO_30() {
        return localREASON_CODES_26_TO_30;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_26_TO_30
     */
    public void setREASON_CODES_26_TO_30(java.lang.String param) {
        localREASON_CODES_26_TO_30Tracker = param != null;

        this.localREASON_CODES_26_TO_30 = param;
    }

    public boolean isREASON_CODES_31_TO_35Specified() {
        return localREASON_CODES_31_TO_35Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_31_TO_35() {
        return localREASON_CODES_31_TO_35;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_31_TO_35
     */
    public void setREASON_CODES_31_TO_35(java.lang.String param) {
        localREASON_CODES_31_TO_35Tracker = param != null;

        this.localREASON_CODES_31_TO_35 = param;
    }

    public boolean isREASON_CODES_36_TO_40Specified() {
        return localREASON_CODES_36_TO_40Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_36_TO_40() {
        return localREASON_CODES_36_TO_40;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_36_TO_40
     */
    public void setREASON_CODES_36_TO_40(java.lang.String param) {
        localREASON_CODES_36_TO_40Tracker = param != null;

        this.localREASON_CODES_36_TO_40 = param;
    }

    public boolean isREASON_CODES_41_TO_45Specified() {
        return localREASON_CODES_41_TO_45Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_41_TO_45() {
        return localREASON_CODES_41_TO_45;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_41_TO_45
     */
    public void setREASON_CODES_41_TO_45(java.lang.String param) {
        localREASON_CODES_41_TO_45Tracker = param != null;

        this.localREASON_CODES_41_TO_45 = param;
    }

    public boolean isREASON_CODES_46_TO_50Specified() {
        return localREASON_CODES_46_TO_50Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREASON_CODES_46_TO_50() {
        return localREASON_CODES_46_TO_50;
    }

    /**
     * Auto generated setter method
     * @param param REASON_CODES_46_TO_50
     */
    public void setREASON_CODES_46_TO_50(java.lang.String param) {
        localREASON_CODES_46_TO_50Tracker = param != null;

        this.localREASON_CODES_46_TO_50 = param;
    }

    public boolean isERROR_CODE_SCSpecified() {
        return localERROR_CODE_SCTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERROR_CODE_SC() {
        return localERROR_CODE_SC;
    }

    /**
     * Auto generated setter method
     * @param param ERROR_CODE_SC
     */
    public void setERROR_CODE_SC(java.lang.String param) {
        localERROR_CODE_SCTracker = param != null;

        this.localERROR_CODE_SC = param;
    }

    public boolean isADDRESS_LINE_1Specified() {
        return localADDRESS_LINE_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_LINE_1() {
        return localADDRESS_LINE_1;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_LINE_1
     */
    public void setADDRESS_LINE_1(java.lang.String param) {
        localADDRESS_LINE_1Tracker = param != null;

        this.localADDRESS_LINE_1 = param;
    }

    public boolean isADDRESS_LINE_2Specified() {
        return localADDRESS_LINE_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_LINE_2() {
        return localADDRESS_LINE_2;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_LINE_2
     */
    public void setADDRESS_LINE_2(java.lang.String param) {
        localADDRESS_LINE_2Tracker = param != null;

        this.localADDRESS_LINE_2 = param;
    }

    public boolean isADDRESS_LINE_3Specified() {
        return localADDRESS_LINE_3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_LINE_3() {
        return localADDRESS_LINE_3;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_LINE_3
     */
    public void setADDRESS_LINE_3(java.lang.String param) {
        localADDRESS_LINE_3Tracker = param != null;

        this.localADDRESS_LINE_3 = param;
    }

    public boolean isADDRESS_LINE_4Specified() {
        return localADDRESS_LINE_4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_LINE_4() {
        return localADDRESS_LINE_4;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_LINE_4
     */
    public void setADDRESS_LINE_4(java.lang.String param) {
        localADDRESS_LINE_4Tracker = param != null;

        this.localADDRESS_LINE_4 = param;
    }

    public boolean isADDRESS_LINE_5Specified() {
        return localADDRESS_LINE_5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_LINE_5() {
        return localADDRESS_LINE_5;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_LINE_5
     */
    public void setADDRESS_LINE_5(java.lang.String param) {
        localADDRESS_LINE_5Tracker = param != null;

        this.localADDRESS_LINE_5 = param;
    }

    public boolean isSTATE_CODESpecified() {
        return localSTATE_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATE_CODE() {
        return localSTATE_CODE;
    }

    /**
     * Auto generated setter method
     * @param param STATE_CODE
     */
    public void setSTATE_CODE(java.lang.String param) {
        localSTATE_CODETracker = param != null;

        this.localSTATE_CODE = param;
    }

    public boolean isSTATESpecified() {
        return localSTATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATE() {
        return localSTATE;
    }

    /**
     * Auto generated setter method
     * @param param STATE
     */
    public void setSTATE(java.lang.String param) {
        localSTATETracker = param != null;

        this.localSTATE = param;
    }

    public boolean isPINCODESpecified() {
        return localPINCODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPINCODE() {
        return localPINCODE;
    }

    /**
     * Auto generated setter method
     * @param param PINCODE
     */
    public void setPINCODE(java.lang.String param) {
        localPINCODETracker = param != null;

        this.localPINCODE = param;
    }

    public boolean isADDRESS_CATERGORYSpecified() {
        return localADDRESS_CATERGORYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_CATERGORY() {
        return localADDRESS_CATERGORY;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_CATERGORY
     */
    public void setADDRESS_CATERGORY(java.lang.String param) {
        localADDRESS_CATERGORYTracker = param != null;

        this.localADDRESS_CATERGORY = param;
    }

    public boolean isRESIDENCE_CODESpecified() {
        return localRESIDENCE_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRESIDENCE_CODE() {
        return localRESIDENCE_CODE;
    }

    /**
     * Auto generated setter method
     * @param param RESIDENCE_CODE
     */
    public void setRESIDENCE_CODE(java.lang.String param) {
        localRESIDENCE_CODETracker = param != null;

        this.localRESIDENCE_CODE = param;
    }

    public boolean isDATE_REPORTED_PASpecified() {
        return localDATE_REPORTED_PATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_REPORTED_PA() {
        return localDATE_REPORTED_PA;
    }

    /**
     * Auto generated setter method
     * @param param DATE_REPORTED_PA
     */
    public void setDATE_REPORTED_PA(java.lang.String param) {
        localDATE_REPORTED_PATracker = param != null;

        this.localDATE_REPORTED_PA = param;
    }

    public boolean isENRICHED_THROUGH_ENQUIRY_PASpecified() {
        return localENRICHED_THROUGH_ENQUIRY_PATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENRICHED_THROUGH_ENQUIRY_PA() {
        return localENRICHED_THROUGH_ENQUIRY_PA;
    }

    /**
     * Auto generated setter method
     * @param param ENRICHED_THROUGH_ENQUIRY_PA
     */
    public void setENRICHED_THROUGH_ENQUIRY_PA(java.lang.String param) {
        localENRICHED_THROUGH_ENQUIRY_PATracker = param != null;

        this.localENRICHED_THROUGH_ENQUIRY_PA = param;
    }

    public boolean isREPO_MEMSHORTNAME_TLSpecified() {
        return localREPO_MEMSHORTNAME_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPO_MEMSHORTNAME_TL() {
        return localREPO_MEMSHORTNAME_TL;
    }

    /**
     * Auto generated setter method
     * @param param REPO_MEMSHORTNAME_TL
     */
    public void setREPO_MEMSHORTNAME_TL(java.lang.String param) {
        localREPO_MEMSHORTNAME_TLTracker = param != null;

        this.localREPO_MEMSHORTNAME_TL = param;
    }

    public boolean isACCOUNT_NUMBER_TLSpecified() {
        return localACCOUNT_NUMBER_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_NUMBER_TL() {
        return localACCOUNT_NUMBER_TL;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_NUMBER_TL
     */
    public void setACCOUNT_NUMBER_TL(java.lang.String param) {
        localACCOUNT_NUMBER_TLTracker = param != null;

        this.localACCOUNT_NUMBER_TL = param;
    }

    public boolean isACCOUNT_TYPE_TLSpecified() {
        return localACCOUNT_TYPE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_TYPE_TL() {
        return localACCOUNT_TYPE_TL;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_TYPE_TL
     */
    public void setACCOUNT_TYPE_TL(java.lang.String param) {
        localACCOUNT_TYPE_TLTracker = param != null;

        this.localACCOUNT_TYPE_TL = param;
    }

    public boolean isOWNERSHIP_INDICATOR_TLSpecified() {
        return localOWNERSHIP_INDICATOR_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIP_INDICATOR_TL() {
        return localOWNERSHIP_INDICATOR_TL;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIP_INDICATOR_TL
     */
    public void setOWNERSHIP_INDICATOR_TL(java.lang.String param) {
        localOWNERSHIP_INDICATOR_TLTracker = param != null;

        this.localOWNERSHIP_INDICATOR_TL = param;
    }

    public boolean isDATE_OPENED_DISBURSED_TLSpecified() {
        return localDATE_OPENED_DISBURSED_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OPENED_DISBURSED_TL() {
        return localDATE_OPENED_DISBURSED_TL;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OPENED_DISBURSED_TL
     */
    public void setDATE_OPENED_DISBURSED_TL(java.lang.String param) {
        localDATE_OPENED_DISBURSED_TLTracker = param != null;

        this.localDATE_OPENED_DISBURSED_TL = param;
    }

    public boolean isDATE_OF_LAST_PAYMENT_TLSpecified() {
        return localDATE_OF_LAST_PAYMENT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_LAST_PAYMENT_TL() {
        return localDATE_OF_LAST_PAYMENT_TL;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_LAST_PAYMENT_TL
     */
    public void setDATE_OF_LAST_PAYMENT_TL(java.lang.String param) {
        localDATE_OF_LAST_PAYMENT_TLTracker = param != null;

        this.localDATE_OF_LAST_PAYMENT_TL = param;
    }

    public boolean isDATE_CLOSED_TLSpecified() {
        return localDATE_CLOSED_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_CLOSED_TL() {
        return localDATE_CLOSED_TL;
    }

    /**
     * Auto generated setter method
     * @param param DATE_CLOSED_TL
     */
    public void setDATE_CLOSED_TL(java.lang.String param) {
        localDATE_CLOSED_TLTracker = param != null;

        this.localDATE_CLOSED_TL = param;
    }

    public boolean isTL_DATE_REPORTEDSpecified() {
        return localTL_DATE_REPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTL_DATE_REPORTED() {
        return localTL_DATE_REPORTED;
    }

    /**
     * Auto generated setter method
     * @param param TL_DATE_REPORTED
     */
    public void setTL_DATE_REPORTED(java.lang.String param) {
        localTL_DATE_REPORTEDTracker = param != null;

        this.localTL_DATE_REPORTED = param;
    }

    public boolean isHIGH_CREDIT_SANCTIONED_AMOUNTSpecified() {
        return localHIGH_CREDIT_SANCTIONED_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHIGH_CREDIT_SANCTIONED_AMOUNT() {
        return localHIGH_CREDIT_SANCTIONED_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param HIGH_CREDIT_SANCTIONED_AMOUNT
     */
    public void setHIGH_CREDIT_SANCTIONED_AMOUNT(java.lang.String param) {
        localHIGH_CREDIT_SANCTIONED_AMOUNTTracker = param != null;

        this.localHIGH_CREDIT_SANCTIONED_AMOUNT = param;
    }

    public boolean isCURRENT_BALANCE_TLSpecified() {
        return localCURRENT_BALANCE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENT_BALANCE_TL() {
        return localCURRENT_BALANCE_TL;
    }

    /**
     * Auto generated setter method
     * @param param CURRENT_BALANCE_TL
     */
    public void setCURRENT_BALANCE_TL(java.lang.String param) {
        localCURRENT_BALANCE_TLTracker = param != null;

        this.localCURRENT_BALANCE_TL = param;
    }

    public boolean isAMOUNT_OVERDUE_TLSpecified() {
        return localAMOUNT_OVERDUE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAMOUNT_OVERDUE_TL() {
        return localAMOUNT_OVERDUE_TL;
    }

    /**
     * Auto generated setter method
     * @param param AMOUNT_OVERDUE_TL
     */
    public void setAMOUNT_OVERDUE_TL(java.lang.String param) {
        localAMOUNT_OVERDUE_TLTracker = param != null;

        this.localAMOUNT_OVERDUE_TL = param;
    }

    public boolean isPAYMENT_HISTORY_1Specified() {
        return localPAYMENT_HISTORY_1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENT_HISTORY_1() {
        return localPAYMENT_HISTORY_1;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENT_HISTORY_1
     */
    public void setPAYMENT_HISTORY_1(java.lang.String param) {
        localPAYMENT_HISTORY_1Tracker = param != null;

        this.localPAYMENT_HISTORY_1 = param;
    }

    public boolean isPAYMENT_HISTORY_2Specified() {
        return localPAYMENT_HISTORY_2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENT_HISTORY_2() {
        return localPAYMENT_HISTORY_2;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENT_HISTORY_2
     */
    public void setPAYMENT_HISTORY_2(java.lang.String param) {
        localPAYMENT_HISTORY_2Tracker = param != null;

        this.localPAYMENT_HISTORY_2 = param;
    }

    public boolean isPAYMENT_HISTORY_START_DATESpecified() {
        return localPAYMENT_HISTORY_START_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENT_HISTORY_START_DATE() {
        return localPAYMENT_HISTORY_START_DATE;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENT_HISTORY_START_DATE
     */
    public void setPAYMENT_HISTORY_START_DATE(java.lang.String param) {
        localPAYMENT_HISTORY_START_DATETracker = param != null;

        this.localPAYMENT_HISTORY_START_DATE = param;
    }

    public boolean isPAYMENT_HISTORY_END_DATESpecified() {
        return localPAYMENT_HISTORY_END_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENT_HISTORY_END_DATE() {
        return localPAYMENT_HISTORY_END_DATE;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENT_HISTORY_END_DATE
     */
    public void setPAYMENT_HISTORY_END_DATE(java.lang.String param) {
        localPAYMENT_HISTORY_END_DATETracker = param != null;

        this.localPAYMENT_HISTORY_END_DATE = param;
    }

    public boolean isSUIT_FILED_STATUS_TLSpecified() {
        return localSUIT_FILED_STATUS_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUIT_FILED_STATUS_TL() {
        return localSUIT_FILED_STATUS_TL;
    }

    /**
     * Auto generated setter method
     * @param param SUIT_FILED_STATUS_TL
     */
    public void setSUIT_FILED_STATUS_TL(java.lang.String param) {
        localSUIT_FILED_STATUS_TLTracker = param != null;

        this.localSUIT_FILED_STATUS_TL = param;
    }

    public boolean isWOF_SETTLED_STATUS_TLSpecified() {
        return localWOF_SETTLED_STATUS_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWOF_SETTLED_STATUS_TL() {
        return localWOF_SETTLED_STATUS_TL;
    }

    /**
     * Auto generated setter method
     * @param param WOF_SETTLED_STATUS_TL
     */
    public void setWOF_SETTLED_STATUS_TL(java.lang.String param) {
        localWOF_SETTLED_STATUS_TLTracker = param != null;

        this.localWOF_SETTLED_STATUS_TL = param;
    }

    public boolean isVALOFCOLLATERAL_TLSpecified() {
        return localVALOFCOLLATERAL_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVALOFCOLLATERAL_TL() {
        return localVALOFCOLLATERAL_TL;
    }

    /**
     * Auto generated setter method
     * @param param VALOFCOLLATERAL_TL
     */
    public void setVALOFCOLLATERAL_TL(java.lang.String param) {
        localVALOFCOLLATERAL_TLTracker = param != null;

        this.localVALOFCOLLATERAL_TL = param;
    }

    public boolean isTYPEOFCOLLATERAL_TLSpecified() {
        return localTYPEOFCOLLATERAL_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTYPEOFCOLLATERAL_TL() {
        return localTYPEOFCOLLATERAL_TL;
    }

    /**
     * Auto generated setter method
     * @param param TYPEOFCOLLATERAL_TL
     */
    public void setTYPEOFCOLLATERAL_TL(java.lang.String param) {
        localTYPEOFCOLLATERAL_TLTracker = param != null;

        this.localTYPEOFCOLLATERAL_TL = param;
    }

    public boolean isCREDITLIMIT_TLSpecified() {
        return localCREDITLIMIT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDITLIMIT_TL() {
        return localCREDITLIMIT_TL;
    }

    /**
     * Auto generated setter method
     * @param param CREDITLIMIT_TL
     */
    public void setCREDITLIMIT_TL(java.lang.String param) {
        localCREDITLIMIT_TLTracker = param != null;

        this.localCREDITLIMIT_TL = param;
    }

    public boolean isCASHLIMIT_TLSpecified() {
        return localCASHLIMIT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCASHLIMIT_TL() {
        return localCASHLIMIT_TL;
    }

    /**
     * Auto generated setter method
     * @param param CASHLIMIT_TL
     */
    public void setCASHLIMIT_TL(java.lang.String param) {
        localCASHLIMIT_TLTracker = param != null;

        this.localCASHLIMIT_TL = param;
    }

    public boolean isRATEOFINTREST_TLSpecified() {
        return localRATEOFINTREST_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATEOFINTREST_TL() {
        return localRATEOFINTREST_TL;
    }

    /**
     * Auto generated setter method
     * @param param RATEOFINTREST_TL
     */
    public void setRATEOFINTREST_TL(java.lang.String param) {
        localRATEOFINTREST_TLTracker = param != null;

        this.localRATEOFINTREST_TL = param;
    }

    public boolean isREPAY_TENURESpecified() {
        return localREPAY_TENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPAY_TENURE() {
        return localREPAY_TENURE;
    }

    /**
     * Auto generated setter method
     * @param param REPAY_TENURE
     */
    public void setREPAY_TENURE(java.lang.String param) {
        localREPAY_TENURETracker = param != null;

        this.localREPAY_TENURE = param;
    }

    public boolean isEMI_AMOUNT_TLSpecified() {
        return localEMI_AMOUNT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMI_AMOUNT_TL() {
        return localEMI_AMOUNT_TL;
    }

    /**
     * Auto generated setter method
     * @param param EMI_AMOUNT_TL
     */
    public void setEMI_AMOUNT_TL(java.lang.String param) {
        localEMI_AMOUNT_TLTracker = param != null;

        this.localEMI_AMOUNT_TL = param;
    }

    public boolean isWOF_TOT_AMOUNT_TLSpecified() {
        return localWOF_TOT_AMOUNT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWOF_TOT_AMOUNT_TL() {
        return localWOF_TOT_AMOUNT_TL;
    }

    /**
     * Auto generated setter method
     * @param param WOF_TOT_AMOUNT_TL
     */
    public void setWOF_TOT_AMOUNT_TL(java.lang.String param) {
        localWOF_TOT_AMOUNT_TLTracker = param != null;

        this.localWOF_TOT_AMOUNT_TL = param;
    }

    public boolean isWOF_PRINCIPAL_TLSpecified() {
        return localWOF_PRINCIPAL_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWOF_PRINCIPAL_TL() {
        return localWOF_PRINCIPAL_TL;
    }

    /**
     * Auto generated setter method
     * @param param WOF_PRINCIPAL_TL
     */
    public void setWOF_PRINCIPAL_TL(java.lang.String param) {
        localWOF_PRINCIPAL_TLTracker = param != null;

        this.localWOF_PRINCIPAL_TL = param;
    }

    public boolean isSETTLEMENT_AMOUNT_TLSpecified() {
        return localSETTLEMENT_AMOUNT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSETTLEMENT_AMOUNT_TL() {
        return localSETTLEMENT_AMOUNT_TL;
    }

    /**
     * Auto generated setter method
     * @param param SETTLEMENT_AMOUNT_TL
     */
    public void setSETTLEMENT_AMOUNT_TL(java.lang.String param) {
        localSETTLEMENT_AMOUNT_TLTracker = param != null;

        this.localSETTLEMENT_AMOUNT_TL = param;
    }

    public boolean isPAYMENT_FREQUENCY_TLSpecified() {
        return localPAYMENT_FREQUENCY_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAYMENT_FREQUENCY_TL() {
        return localPAYMENT_FREQUENCY_TL;
    }

    /**
     * Auto generated setter method
     * @param param PAYMENT_FREQUENCY_TL
     */
    public void setPAYMENT_FREQUENCY_TL(java.lang.String param) {
        localPAYMENT_FREQUENCY_TLTracker = param != null;

        this.localPAYMENT_FREQUENCY_TL = param;
    }

    public boolean isACTUAL_PAYMENT_AMOUNT_TLSpecified() {
        return localACTUAL_PAYMENT_AMOUNT_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACTUAL_PAYMENT_AMOUNT_TL() {
        return localACTUAL_PAYMENT_AMOUNT_TL;
    }

    /**
     * Auto generated setter method
     * @param param ACTUAL_PAYMENT_AMOUNT_TL
     */
    public void setACTUAL_PAYMENT_AMOUNT_TL(java.lang.String param) {
        localACTUAL_PAYMENT_AMOUNT_TLTracker = param != null;

        this.localACTUAL_PAYMENT_AMOUNT_TL = param;
    }

    public boolean isDT_ENTERRCODE_TLSpecified() {
        return localDT_ENTERRCODE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_ENTERRCODE_TL() {
        return localDT_ENTERRCODE_TL;
    }

    /**
     * Auto generated setter method
     * @param param DT_ENTERRCODE_TL
     */
    public void setDT_ENTERRCODE_TL(java.lang.String param) {
        localDT_ENTERRCODE_TLTracker = param != null;

        this.localDT_ENTERRCODE_TL = param;
    }

    public boolean isERRCODE_TLSpecified() {
        return localERRCODE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERRCODE_TL() {
        return localERRCODE_TL;
    }

    /**
     * Auto generated setter method
     * @param param ERRCODE_TL
     */
    public void setERRCODE_TL(java.lang.String param) {
        localERRCODE_TLTracker = param != null;

        this.localERRCODE_TL = param;
    }

    public boolean isDT_ENTCIBIL_REMARK_CODE_TLSpecified() {
        return localDT_ENTCIBIL_REMARK_CODE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_ENTCIBIL_REMARK_CODE_TL() {
        return localDT_ENTCIBIL_REMARK_CODE_TL;
    }

    /**
     * Auto generated setter method
     * @param param DT_ENTCIBIL_REMARK_CODE_TL
     */
    public void setDT_ENTCIBIL_REMARK_CODE_TL(java.lang.String param) {
        localDT_ENTCIBIL_REMARK_CODE_TLTracker = param != null;

        this.localDT_ENTCIBIL_REMARK_CODE_TL = param;
    }

    public boolean isCIBIL_REMARKS_CODE_TLSpecified() {
        return localCIBIL_REMARKS_CODE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCIBIL_REMARKS_CODE_TL() {
        return localCIBIL_REMARKS_CODE_TL;
    }

    /**
     * Auto generated setter method
     * @param param CIBIL_REMARKS_CODE_TL
     */
    public void setCIBIL_REMARKS_CODE_TL(java.lang.String param) {
        localCIBIL_REMARKS_CODE_TLTracker = param != null;

        this.localCIBIL_REMARKS_CODE_TL = param;
    }

    public boolean isDT_DISPUTE_CODE_TLSpecified() {
        return localDT_DISPUTE_CODE_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDT_DISPUTE_CODE_TL() {
        return localDT_DISPUTE_CODE_TL;
    }

    /**
     * Auto generated setter method
     * @param param DT_DISPUTE_CODE_TL
     */
    public void setDT_DISPUTE_CODE_TL(java.lang.String param) {
        localDT_DISPUTE_CODE_TLTracker = param != null;

        this.localDT_DISPUTE_CODE_TL = param;
    }

    public boolean isERR_DISP_REMCODE1_TLSpecified() {
        return localERR_DISP_REMCODE1_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REMCODE1_TL() {
        return localERR_DISP_REMCODE1_TL;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REMCODE1_TL
     */
    public void setERR_DISP_REMCODE1_TL(java.lang.String param) {
        localERR_DISP_REMCODE1_TLTracker = param != null;

        this.localERR_DISP_REMCODE1_TL = param;
    }

    public boolean isERR_DISP_REMCODE2_TLSpecified() {
        return localERR_DISP_REMCODE2_TLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getERR_DISP_REMCODE2_TL() {
        return localERR_DISP_REMCODE2_TL;
    }

    /**
     * Auto generated setter method
     * @param param ERR_DISP_REMCODE2_TL
     */
    public void setERR_DISP_REMCODE2_TL(java.lang.String param) {
        localERR_DISP_REMCODE2_TLTracker = param != null;

        this.localERR_DISP_REMCODE2_TL = param;
    }

    public boolean isDATE_OF_ENQUIRY_IQSpecified() {
        return localDATE_OF_ENQUIRY_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_ENQUIRY_IQ() {
        return localDATE_OF_ENQUIRY_IQ;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_ENQUIRY_IQ
     */
    public void setDATE_OF_ENQUIRY_IQ(java.lang.String param) {
        localDATE_OF_ENQUIRY_IQTracker = param != null;

        this.localDATE_OF_ENQUIRY_IQ = param;
    }

    public boolean isENQ_MEMBER_SHORT_NAME_IQSpecified() {
        return localENQ_MEMBER_SHORT_NAME_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQ_MEMBER_SHORT_NAME_IQ() {
        return localENQ_MEMBER_SHORT_NAME_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQ_MEMBER_SHORT_NAME_IQ
     */
    public void setENQ_MEMBER_SHORT_NAME_IQ(java.lang.String param) {
        localENQ_MEMBER_SHORT_NAME_IQTracker = param != null;

        this.localENQ_MEMBER_SHORT_NAME_IQ = param;
    }

    public boolean isENQUIRY_PURPOSE_IQSpecified() {
        return localENQUIRY_PURPOSE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQUIRY_PURPOSE_IQ() {
        return localENQUIRY_PURPOSE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQUIRY_PURPOSE_IQ
     */
    public void setENQUIRY_PURPOSE_IQ(java.lang.String param) {
        localENQUIRY_PURPOSE_IQTracker = param != null;

        this.localENQUIRY_PURPOSE_IQ = param;
    }

    public boolean isENQIURY_AMOUNT_IQSpecified() {
        return localENQIURY_AMOUNT_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENQIURY_AMOUNT_IQ() {
        return localENQIURY_AMOUNT_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ENQIURY_AMOUNT_IQ
     */
    public void setENQIURY_AMOUNT_IQ(java.lang.String param) {
        localENQIURY_AMOUNT_IQTracker = param != null;

        this.localENQIURY_AMOUNT_IQ = param;
    }

    public boolean isDATE_OF_ENTRY_DRSpecified() {
        return localDATE_OF_ENTRY_DRTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_ENTRY_DR() {
        return localDATE_OF_ENTRY_DR;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_ENTRY_DR
     */
    public void setDATE_OF_ENTRY_DR(java.lang.String param) {
        localDATE_OF_ENTRY_DRTracker = param != null;

        this.localDATE_OF_ENTRY_DR = param;
    }

    public boolean isDISP_REM_LINE1Specified() {
        return localDISP_REM_LINE1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE1() {
        return localDISP_REM_LINE1;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE1
     */
    public void setDISP_REM_LINE1(java.lang.String param) {
        localDISP_REM_LINE1Tracker = param != null;

        this.localDISP_REM_LINE1 = param;
    }

    public boolean isDISP_REM_LINE2Specified() {
        return localDISP_REM_LINE2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE2() {
        return localDISP_REM_LINE2;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE2
     */
    public void setDISP_REM_LINE2(java.lang.String param) {
        localDISP_REM_LINE2Tracker = param != null;

        this.localDISP_REM_LINE2 = param;
    }

    public boolean isDISP_REM_LINE3Specified() {
        return localDISP_REM_LINE3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE3() {
        return localDISP_REM_LINE3;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE3
     */
    public void setDISP_REM_LINE3(java.lang.String param) {
        localDISP_REM_LINE3Tracker = param != null;

        this.localDISP_REM_LINE3 = param;
    }

    public boolean isDISP_REM_LINE4Specified() {
        return localDISP_REM_LINE4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE4() {
        return localDISP_REM_LINE4;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE4
     */
    public void setDISP_REM_LINE4(java.lang.String param) {
        localDISP_REM_LINE4Tracker = param != null;

        this.localDISP_REM_LINE4 = param;
    }

    public boolean isDISP_REM_LINE5Specified() {
        return localDISP_REM_LINE5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE5() {
        return localDISP_REM_LINE5;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE5
     */
    public void setDISP_REM_LINE5(java.lang.String param) {
        localDISP_REM_LINE5Tracker = param != null;

        this.localDISP_REM_LINE5 = param;
    }

    public boolean isDISP_REM_LINE6Specified() {
        return localDISP_REM_LINE6Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISP_REM_LINE6() {
        return localDISP_REM_LINE6;
    }

    /**
     * Auto generated setter method
     * @param param DISP_REM_LINE6
     */
    public void setDISP_REM_LINE6(java.lang.String param) {
        localDISP_REM_LINE6Tracker = param != null;

        this.localDISP_REM_LINE6 = param;
    }

    public boolean isOUTPUT_WRITE_FLAGSpecified() {
        return localOUTPUT_WRITE_FLAGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_FLAG() {
        return localOUTPUT_WRITE_FLAG;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_FLAG
     */
    public void setOUTPUT_WRITE_FLAG(java.lang.String param) {
        localOUTPUT_WRITE_FLAGTracker = param != null;

        this.localOUTPUT_WRITE_FLAG = param;
    }

    public boolean isOUTPUT_WRITE_TIMESpecified() {
        return localOUTPUT_WRITE_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_WRITE_TIME() {
        return localOUTPUT_WRITE_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_WRITE_TIME
     */
    public void setOUTPUT_WRITE_TIME(java.lang.String param) {
        localOUTPUT_WRITE_TIMETracker = param != null;

        this.localOUTPUT_WRITE_TIME = param;
    }

    public boolean isOUTPUT_READ_TIMESpecified() {
        return localOUTPUT_READ_TIMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOUTPUT_READ_TIME() {
        return localOUTPUT_READ_TIME;
    }

    /**
     * Auto generated setter method
     * @param param OUTPUT_READ_TIME
     */
    public void setOUTPUT_READ_TIME(java.lang.String param) {
        localOUTPUT_READ_TIMETracker = param != null;

        this.localOUTPUT_READ_TIME = param;
    }

    public boolean isRespfiller1Specified() {
        return localRespfiller1Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller1() {
        return localRespfiller1;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller1
     */
    public void setRespfiller1(java.lang.String param) {
        localRespfiller1Tracker = param != null;

        this.localRespfiller1 = param;
    }

    public boolean isRespfiller2Specified() {
        return localRespfiller2Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller2() {
        return localRespfiller2;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller2
     */
    public void setRespfiller2(java.lang.String param) {
        localRespfiller2Tracker = param != null;

        this.localRespfiller2 = param;
    }

    public boolean isRespfiller3Specified() {
        return localRespfiller3Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller3() {
        return localRespfiller3;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller3
     */
    public void setRespfiller3(java.lang.String param) {
        localRespfiller3Tracker = param != null;

        this.localRespfiller3 = param;
    }

    public boolean isRespfiller4Specified() {
        return localRespfiller4Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller4() {
        return localRespfiller4;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller4
     */
    public void setRespfiller4(java.lang.String param) {
        localRespfiller4Tracker = param != null;

        this.localRespfiller4 = param;
    }

    public boolean isRespfiller5Specified() {
        return localRespfiller5Tracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRespfiller5() {
        return localRespfiller5;
    }

    /**
     * Auto generated setter method
     * @param param Respfiller5
     */
    public void setRespfiller5(java.lang.String param) {
        localRespfiller5Tracker = param != null;

        this.localRespfiller5 = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":CibilSRespType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "CibilSRespType", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_PROCESSEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_PROCESSED", xmlWriter);

            if (localDATE_PROCESSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_PROCESSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_PROCESSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUBJECT_RETURN_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUBJECT_RETURN_CODE", xmlWriter);

            if (localSUBJECT_RETURN_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBJECT_RETURN_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBJECT_RETURN_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_CONTROL_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_CONTROL_NUMBER",
                xmlWriter);

            if (localENQUIRY_CONTROL_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_CONTROL_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_CONTROL_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUMER_NAME_FIELD1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUMER_NAME_FIELD1", xmlWriter);

            if (localCONSUMER_NAME_FIELD1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUMER_NAME_FIELD1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUMER_NAME_FIELD1);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUME_NAME_FIELD2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUME_NAME_FIELD2", xmlWriter);

            if (localCONSUME_NAME_FIELD2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUME_NAME_FIELD2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUME_NAME_FIELD2);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUMER_NAME_FIELD3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUMER_NAME_FIELD3", xmlWriter);

            if (localCONSUMER_NAME_FIELD3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUMER_NAME_FIELD3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUMER_NAME_FIELD3);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUMER_NAME_FIELD4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUMER_NAME_FIELD4", xmlWriter);

            if (localCONSUMER_NAME_FIELD4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUMER_NAME_FIELD4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUMER_NAME_FIELD4);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONSUMER_NAME_FIELD5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "CONSUMER_NAME_FIELD5", xmlWriter);

            if (localCONSUMER_NAME_FIELD5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONSUMER_NAME_FIELD5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONSUMER_NAME_FIELD5);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_BIRTHTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_BIRTH", xmlWriter);

            if (localDATE_OF_BIRTH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_BIRTH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_BIRTH);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER", xmlWriter);

            if (localGENDER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_ERRCODE_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_ERRCODE_PN", xmlWriter);

            if (localDT_ERRCODE_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_ERRCODE_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_ERRCODE_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_SEG_TAG_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_SEG_TAG_PN", xmlWriter);

            if (localERR_SEG_TAG_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_SEG_TAG_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_SEG_TAG_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_CODE_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_CODE_PN", xmlWriter);

            if (localERR_CODE_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_CODE_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_CODE_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localERROR_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERROR_PN", xmlWriter);

            if (localERROR_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERROR_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERROR_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_ENTCIBILRECODE_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_ENTCIBILRECODE_PN", xmlWriter);

            if (localDT_ENTCIBILRECODE_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_ENTCIBILRECODE_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_ENTCIBILRECODE_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localCIBIL_REMARK_CODE_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CIBIL_REMARK_CODE_PN", xmlWriter);

            if (localCIBIL_REMARK_CODE_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_REMARK_CODE_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCIBIL_REMARK_CODE_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_DISP_REMARK_CODE_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_DISP_REMARK_CODE_PN",
                xmlWriter);

            if (localDATE_DISP_REMARK_CODE_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_DISP_REMARK_CODE_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_DISP_REMARK_CODE_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REM_CODE1_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REM_CODE1_PN",
                xmlWriter);

            if (localERR_DISP_REM_CODE1_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REM_CODE1_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REM_CODE1_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REM_CODE2_PNTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REM_CODE2_PN",
                xmlWriter);

            if (localERR_DISP_REM_CODE2_PN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REM_CODE2_PN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REM_CODE2_PN);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_TYPE", xmlWriter);

            if (localID_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localID_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ID_NUMBER", xmlWriter);

            if (localID_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ID_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localID_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localISSUE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ISSUE_DATE", xmlWriter);

            if (localISSUE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ISSUE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localISSUE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXPIRATION_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXPIRATION_DATE", xmlWriter);

            if (localEXPIRATION_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPIRATION_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXPIRATION_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localENRICHED_THROUGH_ENQUIRY_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENRICHED_THROUGH_ENQUIRY_ID",
                xmlWriter);

            if (localENRICHED_THROUGH_ENQUIRY_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENRICHED_THROUGH_ENQUIRY_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENRICHED_THROUGH_ENQUIRY_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEPHONE_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEPHONE_TYPE", xmlWriter);

            if (localTELEPHONE_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEPHONE_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEPHONE_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEPHONE_EXTENSIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEPHONE_EXTENSION", xmlWriter);

            if (localTELEPHONE_EXTENSION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEPHONE_EXTENSION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEPHONE_EXTENSION);
            }

            xmlWriter.writeEndElement();
        }

        if (localTELEPHONE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TELEPHONE_NUMBER", xmlWriter);

            if (localTELEPHONE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TELEPHONE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTELEPHONE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localENRICHED_THROUGH_ENQUIRY_PTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENRICHED_THROUGH_ENQUIRY_PT",
                xmlWriter);

            if (localENRICHED_THROUGH_ENQUIRY_PT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENRICHED_THROUGH_ENQUIRY_PT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENRICHED_THROUGH_ENQUIRY_PT);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_ID", xmlWriter);

            if (localEMAIL_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_TYPE", xmlWriter);

            if (localACCOUNT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_REPORTED_AND_CERTIFIED_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace,
                "DATE_REPORTED_AND_CERTIFIED_EM", xmlWriter);

            if (localDATE_REPORTED_AND_CERTIFIED_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_REPORTED_AND_CERTIFIED_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_REPORTED_AND_CERTIFIED_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localOCCUPATION_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OCCUPATION_CODE_EM", xmlWriter);

            if (localOCCUPATION_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OCCUPATION_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOCCUPATION_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCOME_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INCOME_EM", xmlWriter);

            if (localINCOME_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCOME_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCOME_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localNET_GROSS_INDICATOR_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NET_GROSS_INDICATOR_EM",
                xmlWriter);

            if (localNET_GROSS_INDICATOR_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NET_GROSS_INDICATOR_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNET_GROSS_INDICATOR_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localMNTHLY_ANNUAL_INDICATOR_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MNTHLY_ANNUAL_INDICATOR_EM",
                xmlWriter);

            if (localMNTHLY_ANNUAL_INDICATOR_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MNTHLY_ANNUAL_INDICATOR_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMNTHLY_ANNUAL_INDICATOR_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_ENTRY_ERR_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_ENTRY_ERR_CODE_EM",
                xmlWriter);

            if (localDATE_ENTRY_ERR_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_ENTRY_ERR_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_ENTRY_ERR_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_CODE_EM", xmlWriter);

            if (localERR_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_CIBIL_ERR_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_CIBIL_ERR_CODE_EM",
                xmlWriter);

            if (localDATE_CIBIL_ERR_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_CIBIL_ERR_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_CIBIL_ERR_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localCIBIL_REMARK_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CIBIL_REMARK_CODE_EM", xmlWriter);

            if (localCIBIL_REMARK_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_REMARK_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCIBIL_REMARK_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_DIS_REMARK_CODE_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_DIS_REMARK_CODE_EM",
                xmlWriter);

            if (localDT_DIS_REMARK_CODE_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_DIS_REMARK_CODE_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_DIS_REMARK_CODE_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REMCODE1_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REMCODE1_EM", xmlWriter);

            if (localERR_DISP_REMCODE1_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REMCODE1_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REMCODE1_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REMCODE2_EMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REMCODE2_EM", xmlWriter);

            if (localERR_DISP_REMCODE2_EM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REMCODE2_EM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REMCODE2_EM);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_NUMBER_PITracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_NUMBER_PI", xmlWriter);

            if (localACCOUNT_NUMBER_PI == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_NUMBER_PI cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_NUMBER_PI);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_NAME", xmlWriter);

            if (localSCORE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_CARD_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_CARD_NAME", xmlWriter);

            if (localSCORE_CARD_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_CARD_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_CARD_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_CARD_VERSIONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_CARD_VERSION", xmlWriter);

            if (localSCORE_CARD_VERSION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_CARD_VERSION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_CARD_VERSION);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORE_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE_DATE", xmlWriter);

            if (localSCORE_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCORETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SCORE", xmlWriter);

            if (localSCORE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCORE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCORE);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUSION_CODES_1_TO_5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXCLUSION_CODES_1_TO_5",
                xmlWriter);

            if (localEXCLUSION_CODES_1_TO_5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUSION_CODES_1_TO_5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUSION_CODES_1_TO_5);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUSION_CODES_6_TO_10Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXCLUSION_CODES_6_TO_10",
                xmlWriter);

            if (localEXCLUSION_CODES_6_TO_10 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUSION_CODES_6_TO_10 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUSION_CODES_6_TO_10);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUSION_CODES_11_TO_15Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXCLUSION_CODES_11_TO_15",
                xmlWriter);

            if (localEXCLUSION_CODES_11_TO_15 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUSION_CODES_11_TO_15 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUSION_CODES_11_TO_15);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUSION_CODES_16_TO_20Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "EXCLUSION_CODES_16_TO_20",
                xmlWriter);

            if (localEXCLUSION_CODES_16_TO_20 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUSION_CODES_16_TO_20 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUSION_CODES_16_TO_20);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_1_TO_5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_1_TO_5", xmlWriter);

            if (localREASON_CODES_1_TO_5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_1_TO_5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_1_TO_5);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_6_TO_10Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_6_TO_10", xmlWriter);

            if (localREASON_CODES_6_TO_10 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_6_TO_10 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_6_TO_10);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_11_TO_15Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_11_TO_15",
                xmlWriter);

            if (localREASON_CODES_11_TO_15 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_11_TO_15 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_11_TO_15);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_16_TO_20Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_16_TO_20",
                xmlWriter);

            if (localREASON_CODES_16_TO_20 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_16_TO_20 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_16_TO_20);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_21_TO_25Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_21_TO_25",
                xmlWriter);

            if (localREASON_CODES_21_TO_25 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_21_TO_25 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_21_TO_25);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_26_TO_30Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_26_TO_30",
                xmlWriter);

            if (localREASON_CODES_26_TO_30 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_26_TO_30 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_26_TO_30);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_31_TO_35Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_31_TO_35",
                xmlWriter);

            if (localREASON_CODES_31_TO_35 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_31_TO_35 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_31_TO_35);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_36_TO_40Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_36_TO_40",
                xmlWriter);

            if (localREASON_CODES_36_TO_40 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_36_TO_40 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_36_TO_40);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_41_TO_45Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_41_TO_45",
                xmlWriter);

            if (localREASON_CODES_41_TO_45 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_41_TO_45 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_41_TO_45);
            }

            xmlWriter.writeEndElement();
        }

        if (localREASON_CODES_46_TO_50Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "REASON_CODES_46_TO_50",
                xmlWriter);

            if (localREASON_CODES_46_TO_50 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REASON_CODES_46_TO_50 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREASON_CODES_46_TO_50);
            }

            xmlWriter.writeEndElement();
        }

        if (localERROR_CODE_SCTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERROR_CODE_SC", xmlWriter);

            if (localERROR_CODE_SC == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERROR_CODE_SC cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERROR_CODE_SC);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_LINE_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_LINE_1", xmlWriter);

            if (localADDRESS_LINE_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_LINE_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_LINE_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_LINE_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_LINE_2", xmlWriter);

            if (localADDRESS_LINE_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_LINE_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_LINE_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_LINE_3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_LINE_3", xmlWriter);

            if (localADDRESS_LINE_3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_LINE_3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_LINE_3);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_LINE_4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_LINE_4", xmlWriter);

            if (localADDRESS_LINE_4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_LINE_4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_LINE_4);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_LINE_5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_LINE_5", xmlWriter);

            if (localADDRESS_LINE_5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_LINE_5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_LINE_5);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATE_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATE_CODE", xmlWriter);

            if (localSTATE_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATE_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATE_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATE", xmlWriter);

            if (localSTATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPINCODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PINCODE", xmlWriter);

            if (localPINCODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PINCODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPINCODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_CATERGORYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_CATERGORY", xmlWriter);

            if (localADDRESS_CATERGORY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_CATERGORY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_CATERGORY);
            }

            xmlWriter.writeEndElement();
        }

        if (localRESIDENCE_CODETracker) {
            namespace = "";
            writeStartElement(null, namespace, "RESIDENCE_CODE", xmlWriter);

            if (localRESIDENCE_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RESIDENCE_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRESIDENCE_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_REPORTED_PATracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_REPORTED_PA", xmlWriter);

            if (localDATE_REPORTED_PA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_REPORTED_PA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_REPORTED_PA);
            }

            xmlWriter.writeEndElement();
        }

        if (localENRICHED_THROUGH_ENQUIRY_PATracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENRICHED_THROUGH_ENQUIRY_PA",
                xmlWriter);

            if (localENRICHED_THROUGH_ENQUIRY_PA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENRICHED_THROUGH_ENQUIRY_PA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENRICHED_THROUGH_ENQUIRY_PA);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPO_MEMSHORTNAME_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPO_MEMSHORTNAME_TL", xmlWriter);

            if (localREPO_MEMSHORTNAME_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPO_MEMSHORTNAME_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPO_MEMSHORTNAME_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_NUMBER_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_NUMBER_TL", xmlWriter);

            if (localACCOUNT_NUMBER_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_NUMBER_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_NUMBER_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_TYPE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_TYPE_TL", xmlWriter);

            if (localACCOUNT_TYPE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_TYPE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_TYPE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIP_INDICATOR_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIP_INDICATOR_TL",
                xmlWriter);

            if (localOWNERSHIP_INDICATOR_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIP_INDICATOR_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIP_INDICATOR_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OPENED_DISBURSED_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OPENED_DISBURSED_TL",
                xmlWriter);

            if (localDATE_OPENED_DISBURSED_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OPENED_DISBURSED_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OPENED_DISBURSED_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_LAST_PAYMENT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_LAST_PAYMENT_TL",
                xmlWriter);

            if (localDATE_OF_LAST_PAYMENT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_LAST_PAYMENT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_LAST_PAYMENT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_CLOSED_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_CLOSED_TL", xmlWriter);

            if (localDATE_CLOSED_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_CLOSED_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_CLOSED_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTL_DATE_REPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TL_DATE_REPORTED", xmlWriter);

            if (localTL_DATE_REPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TL_DATE_REPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTL_DATE_REPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localHIGH_CREDIT_SANCTIONED_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "HIGH_CREDIT_SANCTIONED_AMOUNT",
                xmlWriter);

            if (localHIGH_CREDIT_SANCTIONED_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HIGH_CREDIT_SANCTIONED_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHIGH_CREDIT_SANCTIONED_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENT_BALANCE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENT_BALANCE_TL", xmlWriter);

            if (localCURRENT_BALANCE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENT_BALANCE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENT_BALANCE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localAMOUNT_OVERDUE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AMOUNT_OVERDUE_TL", xmlWriter);

            if (localAMOUNT_OVERDUE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AMOUNT_OVERDUE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAMOUNT_OVERDUE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENT_HISTORY_1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENT_HISTORY_1", xmlWriter);

            if (localPAYMENT_HISTORY_1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENT_HISTORY_1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENT_HISTORY_1);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENT_HISTORY_2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENT_HISTORY_2", xmlWriter);

            if (localPAYMENT_HISTORY_2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENT_HISTORY_2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENT_HISTORY_2);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENT_HISTORY_START_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENT_HISTORY_START_DATE",
                xmlWriter);

            if (localPAYMENT_HISTORY_START_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENT_HISTORY_START_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENT_HISTORY_START_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENT_HISTORY_END_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENT_HISTORY_END_DATE",
                xmlWriter);

            if (localPAYMENT_HISTORY_END_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENT_HISTORY_END_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENT_HISTORY_END_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUIT_FILED_STATUS_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUIT_FILED_STATUS_TL", xmlWriter);

            if (localSUIT_FILED_STATUS_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUIT_FILED_STATUS_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUIT_FILED_STATUS_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWOF_SETTLED_STATUS_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WOF_SETTLED_STATUS_TL",
                xmlWriter);

            if (localWOF_SETTLED_STATUS_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WOF_SETTLED_STATUS_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWOF_SETTLED_STATUS_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localVALOFCOLLATERAL_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VALOFCOLLATERAL_TL", xmlWriter);

            if (localVALOFCOLLATERAL_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VALOFCOLLATERAL_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVALOFCOLLATERAL_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localTYPEOFCOLLATERAL_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TYPEOFCOLLATERAL_TL", xmlWriter);

            if (localTYPEOFCOLLATERAL_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TYPEOFCOLLATERAL_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTYPEOFCOLLATERAL_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDITLIMIT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDITLIMIT_TL", xmlWriter);

            if (localCREDITLIMIT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDITLIMIT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDITLIMIT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCASHLIMIT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CASHLIMIT_TL", xmlWriter);

            if (localCASHLIMIT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CASHLIMIT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCASHLIMIT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATEOFINTREST_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATEOFINTREST_TL", xmlWriter);

            if (localRATEOFINTREST_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATEOFINTREST_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATEOFINTREST_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPAY_TENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPAY_TENURE", xmlWriter);

            if (localREPAY_TENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPAY_TENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPAY_TENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMI_AMOUNT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMI_AMOUNT_TL", xmlWriter);

            if (localEMI_AMOUNT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMI_AMOUNT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMI_AMOUNT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWOF_TOT_AMOUNT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WOF_TOT_AMOUNT_TL", xmlWriter);

            if (localWOF_TOT_AMOUNT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WOF_TOT_AMOUNT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWOF_TOT_AMOUNT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localWOF_PRINCIPAL_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WOF_PRINCIPAL_TL", xmlWriter);

            if (localWOF_PRINCIPAL_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WOF_PRINCIPAL_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWOF_PRINCIPAL_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSETTLEMENT_AMOUNT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SETTLEMENT_AMOUNT_TL", xmlWriter);

            if (localSETTLEMENT_AMOUNT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SETTLEMENT_AMOUNT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSETTLEMENT_AMOUNT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAYMENT_FREQUENCY_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAYMENT_FREQUENCY_TL", xmlWriter);

            if (localPAYMENT_FREQUENCY_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAYMENT_FREQUENCY_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAYMENT_FREQUENCY_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localACTUAL_PAYMENT_AMOUNT_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACTUAL_PAYMENT_AMOUNT_TL",
                xmlWriter);

            if (localACTUAL_PAYMENT_AMOUNT_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACTUAL_PAYMENT_AMOUNT_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACTUAL_PAYMENT_AMOUNT_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_ENTERRCODE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_ENTERRCODE_TL", xmlWriter);

            if (localDT_ENTERRCODE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_ENTERRCODE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_ENTERRCODE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localERRCODE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERRCODE_TL", xmlWriter);

            if (localERRCODE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERRCODE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERRCODE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_ENTCIBIL_REMARK_CODE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_ENTCIBIL_REMARK_CODE_TL",
                xmlWriter);

            if (localDT_ENTCIBIL_REMARK_CODE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_ENTCIBIL_REMARK_CODE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_ENTCIBIL_REMARK_CODE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCIBIL_REMARKS_CODE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CIBIL_REMARKS_CODE_TL",
                xmlWriter);

            if (localCIBIL_REMARKS_CODE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CIBIL_REMARKS_CODE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCIBIL_REMARKS_CODE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDT_DISPUTE_CODE_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DT_DISPUTE_CODE_TL", xmlWriter);

            if (localDT_DISPUTE_CODE_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DT_DISPUTE_CODE_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDT_DISPUTE_CODE_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REMCODE1_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REMCODE1_TL", xmlWriter);

            if (localERR_DISP_REMCODE1_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REMCODE1_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REMCODE1_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localERR_DISP_REMCODE2_TLTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ERR_DISP_REMCODE2_TL", xmlWriter);

            if (localERR_DISP_REMCODE2_TL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ERR_DISP_REMCODE2_TL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localERR_DISP_REMCODE2_TL);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_ENQUIRY_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_ENQUIRY_IQ", xmlWriter);

            if (localDATE_OF_ENQUIRY_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_ENQUIRY_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_ENQUIRY_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQ_MEMBER_SHORT_NAME_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQ_MEMBER_SHORT_NAME_IQ",
                xmlWriter);

            if (localENQ_MEMBER_SHORT_NAME_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQ_MEMBER_SHORT_NAME_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQ_MEMBER_SHORT_NAME_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQUIRY_PURPOSE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQUIRY_PURPOSE_IQ", xmlWriter);

            if (localENQUIRY_PURPOSE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQUIRY_PURPOSE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQUIRY_PURPOSE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENQIURY_AMOUNT_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENQIURY_AMOUNT_IQ", xmlWriter);

            if (localENQIURY_AMOUNT_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENQIURY_AMOUNT_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENQIURY_AMOUNT_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_ENTRY_DRTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_ENTRY_DR", xmlWriter);

            if (localDATE_OF_ENTRY_DR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_ENTRY_DR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_ENTRY_DR);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE1", xmlWriter);

            if (localDISP_REM_LINE1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE1);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE2", xmlWriter);

            if (localDISP_REM_LINE2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE2);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE3", xmlWriter);

            if (localDISP_REM_LINE3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE3);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE4", xmlWriter);

            if (localDISP_REM_LINE4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE4);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE5", xmlWriter);

            if (localDISP_REM_LINE5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE5);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISP_REM_LINE6Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISP_REM_LINE6", xmlWriter);

            if (localDISP_REM_LINE6 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISP_REM_LINE6 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISP_REM_LINE6);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_FLAGTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_FLAG", xmlWriter);

            if (localOUTPUT_WRITE_FLAG == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_FLAG cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_FLAG);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_WRITE_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_WRITE_TIME", xmlWriter);

            if (localOUTPUT_WRITE_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_WRITE_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_WRITE_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localOUTPUT_READ_TIMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "OUTPUT_READ_TIME", xmlWriter);

            if (localOUTPUT_READ_TIME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OUTPUT_READ_TIME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOUTPUT_READ_TIME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller1Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller1", xmlWriter);

            if (localRespfiller1 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller1 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller1);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller2Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller2", xmlWriter);

            if (localRespfiller2 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller2 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller2);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller3Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller3", xmlWriter);

            if (localRespfiller3 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller3 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller3);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller4Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller4", xmlWriter);

            if (localRespfiller4 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller4 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller4);
            }

            xmlWriter.writeEndElement();
        }

        if (localRespfiller5Tracker) {
            namespace = "";
            writeStartElement(null, namespace, "Respfiller5", xmlWriter);

            if (localRespfiller5 == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Respfiller5 cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRespfiller5);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static CibilSRespType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            CibilSRespType object = new CibilSRespType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"CibilSRespType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (CibilSRespType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_PROCESSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_PROCESSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_PROCESSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_PROCESSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUBJECT_RETURN_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBJECT_RETURN_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBJECT_RETURN_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBJECT_RETURN_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENQUIRY_CONTROL_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENQUIRY_CONTROL_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_CONTROL_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_CONTROL_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUMER_NAME_FIELD1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUMER_NAME_FIELD1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUME_NAME_FIELD2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUME_NAME_FIELD2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUME_NAME_FIELD2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUME_NAME_FIELD2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUMER_NAME_FIELD3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUMER_NAME_FIELD3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUMER_NAME_FIELD4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUMER_NAME_FIELD4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONSUMER_NAME_FIELD5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONSUMER_NAME_FIELD5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONSUMER_NAME_FIELD5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_BIRTH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_BIRTH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_BIRTH" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_BIRTH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DT_ERRCODE_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DT_ERRCODE_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_ERRCODE_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_ERRCODE_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_SEG_TAG_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_SEG_TAG_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_SEG_TAG_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_SEG_TAG_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_CODE_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_CODE_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_CODE_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_CODE_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERROR_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERROR_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERROR_PN" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERROR_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DT_ENTCIBILRECODE_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DT_ENTCIBILRECODE_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_ENTCIBILRECODE_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_ENTCIBILRECODE_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CIBIL_REMARK_CODE_PN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CIBIL_REMARK_CODE_PN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CIBIL_REMARK_CODE_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCIBIL_REMARK_CODE_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_DISP_REMARK_CODE_PN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_DISP_REMARK_CODE_PN").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_DISP_REMARK_CODE_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_DISP_REMARK_CODE_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ERR_DISP_REM_CODE1_PN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ERR_DISP_REM_CODE1_PN").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REM_CODE1_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REM_CODE1_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ERR_DISP_REM_CODE2_PN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ERR_DISP_REM_CODE2_PN").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REM_CODE2_PN" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REM_CODE2_PN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_TYPE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ID_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID_NUMBER" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ISSUE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ISSUE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ISSUE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setISSUE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EXPIRATION_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPIRATION_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXPIRATION_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXPIRATION_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENRICHED_THROUGH_ENQUIRY_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENRICHED_THROUGH_ENQUIRY_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEPHONE_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEPHONE_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEPHONE_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEPHONE_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEPHONE_EXTENSION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEPHONE_EXTENSION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEPHONE_EXTENSION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEPHONE_EXTENSION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TELEPHONE_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TELEPHONE_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TELEPHONE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTELEPHONE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_PT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_PT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENRICHED_THROUGH_ENQUIRY_PT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENRICHED_THROUGH_ENQUIRY_PT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_REPORTED_AND_CERTIFIED_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_REPORTED_AND_CERTIFIED_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_REPORTED_AND_CERTIFIED_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_REPORTED_AND_CERTIFIED_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OCCUPATION_CODE_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OCCUPATION_CODE_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OCCUPATION_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOCCUPATION_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INCOME_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCOME_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCOME_EM" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCOME_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NET_GROSS_INDICATOR_EM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NET_GROSS_INDICATOR_EM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NET_GROSS_INDICATOR_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNET_GROSS_INDICATOR_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MNTHLY_ANNUAL_INDICATOR_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MNTHLY_ANNUAL_INDICATOR_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MNTHLY_ANNUAL_INDICATOR_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMNTHLY_ANNUAL_INDICATOR_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_ENTRY_ERR_CODE_EM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_ENTRY_ERR_CODE_EM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_ENTRY_ERR_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_ENTRY_ERR_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_CODE_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_CODE_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_CIBIL_ERR_CODE_EM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_CIBIL_ERR_CODE_EM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_CIBIL_ERR_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_CIBIL_ERR_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CIBIL_REMARK_CODE_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CIBIL_REMARK_CODE_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CIBIL_REMARK_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCIBIL_REMARK_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DT_DIS_REMARK_CODE_EM").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DT_DIS_REMARK_CODE_EM").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_DIS_REMARK_CODE_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_DIS_REMARK_CODE_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE1_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE1_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REMCODE1_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REMCODE1_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE2_EM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE2_EM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REMCODE2_EM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REMCODE2_EM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_NUMBER_PI").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_NUMBER_PI").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_NUMBER_PI" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_NUMBER_PI(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_CARD_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_CARD_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_CARD_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_CARD_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_CARD_VERSION").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_CARD_VERSION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_CARD_VERSION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_CARD_VERSION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SCORE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCORE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCORE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCORE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_1_TO_5").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_1_TO_5").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUSION_CODES_1_TO_5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUSION_CODES_1_TO_5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_6_TO_10").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_6_TO_10").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUSION_CODES_6_TO_10" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUSION_CODES_6_TO_10(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_11_TO_15").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_11_TO_15").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUSION_CODES_11_TO_15" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUSION_CODES_11_TO_15(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_16_TO_20").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EXCLUSION_CODES_16_TO_20").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUSION_CODES_16_TO_20" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUSION_CODES_16_TO_20(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REASON_CODES_1_TO_5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REASON_CODES_1_TO_5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_1_TO_5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_1_TO_5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REASON_CODES_6_TO_10").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REASON_CODES_6_TO_10").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_6_TO_10" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_6_TO_10(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_11_TO_15").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_11_TO_15").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_11_TO_15" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_11_TO_15(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_16_TO_20").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_16_TO_20").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_16_TO_20" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_16_TO_20(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_21_TO_25").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_21_TO_25").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_21_TO_25" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_21_TO_25(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_26_TO_30").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_26_TO_30").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_26_TO_30" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_26_TO_30(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_31_TO_35").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_31_TO_35").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_31_TO_35" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_31_TO_35(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_36_TO_40").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_36_TO_40").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_36_TO_40" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_36_TO_40(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_41_TO_45").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_41_TO_45").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_41_TO_45" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_41_TO_45(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_46_TO_50").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REASON_CODES_46_TO_50").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REASON_CODES_46_TO_50" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREASON_CODES_46_TO_50(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERROR_CODE_SC").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERROR_CODE_SC").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERROR_CODE_SC" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERROR_CODE_SC(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_LINE_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_LINE_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_LINE_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_LINE_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_LINE_3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_LINE_3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_LINE_4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_LINE_4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_LINE_5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_LINE_5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_LINE_5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATE_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATE_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATE_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATE_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PINCODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PINCODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PINCODE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPINCODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_CATERGORY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_CATERGORY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_CATERGORY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_CATERGORY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RESIDENCE_CODE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RESIDENCE_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RESIDENCE_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRESIDENCE_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_REPORTED_PA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_REPORTED_PA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_REPORTED_PA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_REPORTED_PA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_PA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENRICHED_THROUGH_ENQUIRY_PA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENRICHED_THROUGH_ENQUIRY_PA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENRICHED_THROUGH_ENQUIRY_PA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPO_MEMSHORTNAME_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPO_MEMSHORTNAME_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPO_MEMSHORTNAME_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPO_MEMSHORTNAME_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_NUMBER_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_NUMBER_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_NUMBER_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_NUMBER_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_TYPE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_TYPE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_TYPE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_TYPE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "OWNERSHIP_INDICATOR_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OWNERSHIP_INDICATOR_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIP_INDICATOR_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIP_INDICATOR_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_OPENED_DISBURSED_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_OPENED_DISBURSED_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OPENED_DISBURSED_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OPENED_DISBURSED_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DATE_OF_LAST_PAYMENT_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DATE_OF_LAST_PAYMENT_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_LAST_PAYMENT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_LAST_PAYMENT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_CLOSED_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_CLOSED_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_CLOSED_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_CLOSED_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TL_DATE_REPORTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TL_DATE_REPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TL_DATE_REPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTL_DATE_REPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "HIGH_CREDIT_SANCTIONED_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "HIGH_CREDIT_SANCTIONED_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HIGH_CREDIT_SANCTIONED_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHIGH_CREDIT_SANCTIONED_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CURRENT_BALANCE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CURRENT_BALANCE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENT_BALANCE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENT_BALANCE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AMOUNT_OVERDUE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AMOUNT_OVERDUE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AMOUNT_OVERDUE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAMOUNT_OVERDUE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAYMENT_HISTORY_1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAYMENT_HISTORY_1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENT_HISTORY_1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENT_HISTORY_1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAYMENT_HISTORY_2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAYMENT_HISTORY_2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENT_HISTORY_2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENT_HISTORY_2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAYMENT_HISTORY_START_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAYMENT_HISTORY_START_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENT_HISTORY_START_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENT_HISTORY_START_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PAYMENT_HISTORY_END_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PAYMENT_HISTORY_END_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENT_HISTORY_END_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENT_HISTORY_END_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SUIT_FILED_STATUS_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUIT_FILED_STATUS_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUIT_FILED_STATUS_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUIT_FILED_STATUS_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WOF_SETTLED_STATUS_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WOF_SETTLED_STATUS_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WOF_SETTLED_STATUS_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWOF_SETTLED_STATUS_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VALOFCOLLATERAL_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VALOFCOLLATERAL_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VALOFCOLLATERAL_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVALOFCOLLATERAL_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TYPEOFCOLLATERAL_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPEOFCOLLATERAL_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TYPEOFCOLLATERAL_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTYPEOFCOLLATERAL_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDITLIMIT_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDITLIMIT_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDITLIMIT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDITLIMIT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CASHLIMIT_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CASHLIMIT_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CASHLIMIT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCASHLIMIT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATEOFINTREST_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATEOFINTREST_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATEOFINTREST_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATEOFINTREST_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPAY_TENURE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPAY_TENURE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPAY_TENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPAY_TENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMI_AMOUNT_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMI_AMOUNT_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMI_AMOUNT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMI_AMOUNT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WOF_TOT_AMOUNT_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WOF_TOT_AMOUNT_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WOF_TOT_AMOUNT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWOF_TOT_AMOUNT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WOF_PRINCIPAL_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WOF_PRINCIPAL_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WOF_PRINCIPAL_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWOF_PRINCIPAL_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SETTLEMENT_AMOUNT_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SETTLEMENT_AMOUNT_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SETTLEMENT_AMOUNT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSETTLEMENT_AMOUNT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAYMENT_FREQUENCY_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAYMENT_FREQUENCY_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAYMENT_FREQUENCY_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAYMENT_FREQUENCY_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ACTUAL_PAYMENT_AMOUNT_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ACTUAL_PAYMENT_AMOUNT_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACTUAL_PAYMENT_AMOUNT_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACTUAL_PAYMENT_AMOUNT_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DT_ENTERRCODE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DT_ENTERRCODE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_ENTERRCODE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_ENTERRCODE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERRCODE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERRCODE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERRCODE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERRCODE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DT_ENTCIBIL_REMARK_CODE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DT_ENTCIBIL_REMARK_CODE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_ENTCIBIL_REMARK_CODE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_ENTCIBIL_REMARK_CODE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CIBIL_REMARKS_CODE_TL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CIBIL_REMARKS_CODE_TL").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CIBIL_REMARKS_CODE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCIBIL_REMARKS_CODE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DT_DISPUTE_CODE_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DT_DISPUTE_CODE_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DT_DISPUTE_CODE_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDT_DISPUTE_CODE_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE1_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE1_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REMCODE1_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REMCODE1_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE2_TL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERR_DISP_REMCODE2_TL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ERR_DISP_REMCODE2_TL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setERR_DISP_REMCODE2_TL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_ENQUIRY_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_ENQUIRY_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_ENQUIRY_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_ENQUIRY_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ENQ_MEMBER_SHORT_NAME_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ENQ_MEMBER_SHORT_NAME_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQ_MEMBER_SHORT_NAME_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQ_MEMBER_SHORT_NAME_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQUIRY_PURPOSE_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQUIRY_PURPOSE_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQUIRY_PURPOSE_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQUIRY_PURPOSE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENQIURY_AMOUNT_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENQIURY_AMOUNT_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENQIURY_AMOUNT_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENQIURY_AMOUNT_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_ENTRY_DR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_ENTRY_DR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_ENTRY_DR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_ENTRY_DR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISP_REM_LINE6").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISP_REM_LINE6").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISP_REM_LINE6" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISP_REM_LINE6(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_FLAG").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_FLAG" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_FLAG(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_WRITE_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_WRITE_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_WRITE_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OUTPUT_READ_TIME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OUTPUT_READ_TIME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOUTPUT_READ_TIME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller1").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller1" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller2").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller2" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller3").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller3" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller3(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller4").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller4" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Respfiller5").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Respfiller5" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRespfiller5(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
