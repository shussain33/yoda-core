/**
 * RejectType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  RejectType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class RejectType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = RejectType
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for TRACKINGID
     */
    protected java.lang.String localTRACKINGID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTRACKINGIDTracker = false;

    /**
     * field for BUREAU
     */
    protected java.lang.String localBUREAU;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBUREAUTracker = false;

    /**
     * field for PRODUCT
     */
    protected java.lang.String localPRODUCT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRODUCTTracker = false;

    /**
     * field for STATUS
     */
    protected java.lang.String localSTATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUSTracker = false;

    /**
     * field for ERRORS
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[] localERRORS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localERRORSTracker = false;

    /**
     * field for WARNINGS
     * This was an Array!
     */
    protected com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[] localWARNINGS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWARNINGSTracker = false;

    public boolean isTRACKINGIDSpecified() {
        return localTRACKINGIDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTRACKINGID() {
        return localTRACKINGID;
    }

    /**
     * Auto generated setter method
     * @param param TRACKINGID
     */
    public void setTRACKINGID(java.lang.String param) {
        localTRACKINGIDTracker = param != null;

        this.localTRACKINGID = param;
    }

    public boolean isBUREAUSpecified() {
        return localBUREAUTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBUREAU() {
        return localBUREAU;
    }

    /**
     * Auto generated setter method
     * @param param BUREAU
     */
    public void setBUREAU(java.lang.String param) {
        localBUREAUTracker = param != null;

        this.localBUREAU = param;
    }

    public boolean isPRODUCTSpecified() {
        return localPRODUCTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRODUCT() {
        return localPRODUCT;
    }

    /**
     * Auto generated setter method
     * @param param PRODUCT
     */
    public void setPRODUCT(java.lang.String param) {
        localPRODUCTTracker = param != null;

        this.localPRODUCT = param;
    }

    public boolean isSTATUSSpecified() {
        return localSTATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS() {
        return localSTATUS;
    }

    /**
     * Auto generated setter method
     * @param param STATUS
     */
    public void setSTATUS(java.lang.String param) {
        localSTATUSTracker = param != null;

        this.localSTATUS = param;
    }

    public boolean isERRORSSpecified() {
        return localERRORSTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[] getERRORS() {
        return localERRORS;
    }

    /**
     * validate the array for ERRORS
     */
    protected void validateERRORS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param ERRORS
     */
    public void setERRORS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[] param) {
        validateERRORS(param);

        localERRORSTracker = param != null;

        this.localERRORS = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType
     */
    public void addERRORS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType param) {
        if (localERRORS == null) {
            localERRORS = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[] {
                    
                };
        }

        //update the setting tracker
        localERRORSTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localERRORS);
        list.add(param);
        this.localERRORS = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[list.size()]);
    }

    public boolean isWARNINGSSpecified() {
        return localWARNINGSTracker;
    }

    /**
     * Auto generated getter method
     * @return com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[]
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[] getWARNINGS() {
        return localWARNINGS;
    }

    /**
     * validate the array for WARNINGS
     */
    protected void validateWARNINGS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param WARNINGS
     */
    public void setWARNINGS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[] param) {
        validateWARNINGS(param);

        localWARNINGSTracker = param != null;

        this.localWARNINGS = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType
     */
    public void addWARNINGS(
        com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType param) {
        if (localWARNINGS == null) {
            localWARNINGS = new com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[] {
                    
                };
        }

        //update the setting tracker
        localWARNINGSTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localWARNINGS);
        list.add(param);
        this.localWARNINGS = (com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[]) list.toArray(new com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[list.size()]);
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":RejectType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "RejectType", xmlWriter);
            }
        }

        if (localTRACKINGIDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TRACKING-ID", xmlWriter);

            if (localTRACKINGID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TRACKING-ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTRACKINGID);
            }

            xmlWriter.writeEndElement();
        }

        if (localBUREAUTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BUREAU", xmlWriter);

            if (localBUREAU == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BUREAU cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBUREAU);
            }

            xmlWriter.writeEndElement();
        }

        if (localPRODUCTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PRODUCT", xmlWriter);

            if (localPRODUCT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRODUCT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRODUCT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS", xmlWriter);

            if (localSTATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localERRORSTracker) {
            if (localERRORS != null) {
                for (int i = 0; i < localERRORS.length; i++) {
                    if (localERRORS[i] != null) {
                        localERRORS[i].serialize(new javax.xml.namespace.QName(
                                "", "ERRORS"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "ERRORS cannot be null!!");
            }
        }

        if (localWARNINGSTracker) {
            if (localWARNINGS != null) {
                for (int i = 0; i < localWARNINGS.length; i++) {
                    if (localWARNINGS[i] != null) {
                        localWARNINGS[i].serialize(new javax.xml.namespace.QName(
                                "", "WARNINGS"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "WARNINGS cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static RejectType parse(javax.xml.stream.XMLStreamReader reader)
            throws java.lang.Exception {
            RejectType object = new RejectType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"RejectType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (RejectType) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list5 = new java.util.ArrayList();

                java.util.ArrayList list6 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TRACKING-ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TRACKING-ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TRACKING-ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTRACKINGID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BUREAU").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BUREAU").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BUREAU" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBUREAU(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PRODUCT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PRODUCT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRODUCT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRODUCT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ERRORS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ERRORS").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone5 = false;

                    while (!loopDone5) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone5 = true;
                        } else {
                            if (new javax.xml.namespace.QName("", "ERRORS").equals(
                                        reader.getName())) {
                                list5.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType.Factory.parse(
                                        reader));
                            } else {
                                loopDone5 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setERRORS((com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType.class,
                            list5));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WARNINGS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WARNINGS").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list6.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone6 = false;

                    while (!loopDone6) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone6 = true;
                        } else {
                            if (new javax.xml.namespace.QName("", "WARNINGS").equals(
                                        reader.getName())) {
                                list6.add(com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType.Factory.parse(
                                        reader));
                            } else {
                                loopDone6 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setWARNINGS((com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType.class,
                            list6));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
