/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ExtensionMapper class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExtensionMapper {
    public static java.lang.Object getTypeObject(
        java.lang.String namespaceURI, java.lang.String typeName,
        javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "EotHeaderType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EotHeaderType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "FinishedType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.FinishedType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "MBEoTRequestType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.MBEoTRequestType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "EquifaxSRespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxSRespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "MBIssueResponseAckType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.MBIssueResponseAckType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "RejectType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.RejectType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "AckHeaderType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.AckHeaderType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmbaseSRespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ErrorsType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ErrorsType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "CibilERespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "soaStandard".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaStandard.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "WarningsType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.WarningsType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "MBIssueResponseType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.MBIssueResponseType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ExperianSRespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ExperianSRespType1".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType1.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmAorSRespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorSRespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "soaFillers".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.SoaFillers.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ExperianSRespType2".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianSRespType2.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmAorERespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmAorERespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmbaseERespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseERespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "CibilSRespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "EquifaxERespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.EquifaxERespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ExperianERespType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExperianERespType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ResponseType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ResponseType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmbaseSRespType2".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType2.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "MBEoTAckType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.MBEoTAckType.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ChmbaseSRespType1".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ChmbaseSRespType1.Factory.parse(reader);
        }

        if ("multibureau.xsd.hdfcbank.mb.soap.softcell.com".equals(namespaceURI) &&
                "ResponseJSONType".equals(typeName)) {
            return com.softcell.soap.mb.hdfcbank.xsd.multibureau.ResponseJSONType.Factory.parse(reader);
        }

        throw new org.apache.axis2.databinding.ADBException("Unsupported type " +
            namespaceURI + " " + typeName);
    }
}
