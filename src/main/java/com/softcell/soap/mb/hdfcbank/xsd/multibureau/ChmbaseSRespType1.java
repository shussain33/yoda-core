/**
 * ChmbaseSRespType1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */
package com.softcell.soap.mb.hdfcbank.xsd.multibureau;


/**
 *  ChmbaseSRespType1 bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ChmbaseSRespType1 implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ChmbaseSRespType1
       Namespace URI = multibureau.xsd.hdfcbank.mb.soap.softcell.com
       Namespace Prefix = ns2
     */

    /**
     * field for SRNO
     */
    protected java.lang.String localSRNO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSRNOTracker = false;

    /**
     * field for MEMBER_REFERENCE_NUMBER
     */
    protected java.lang.String localMEMBER_REFERENCE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMEMBER_REFERENCE_NUMBERTracker = false;

    /**
     * field for SOA_SOURCE_NAME
     */
    protected java.lang.String localSOA_SOURCE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOA_SOURCE_NAMETracker = false;

    /**
     * field for DATE_OF_REQUEST
     */
    protected java.lang.String localDATE_OF_REQUEST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_REQUESTTracker = false;

    /**
     * field for PREPARED_FOR
     */
    protected java.lang.String localPREPARED_FOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FORTracker = false;

    /**
     * field for PREPARED_FOR_ID
     */
    protected java.lang.String localPREPARED_FOR_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPREPARED_FOR_IDTracker = false;

    /**
     * field for DATE_OF_ISSUE
     */
    protected java.lang.String localDATE_OF_ISSUE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_ISSUETracker = false;

    /**
     * field for REPORT_ID
     */
    protected java.lang.String localREPORT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORT_IDTracker = false;

    /**
     * field for BATCH_ID
     */
    protected java.lang.String localBATCH_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBATCH_IDTracker = false;

    /**
     * field for STATUS_IQ
     */
    protected java.lang.String localSTATUS_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSTATUS_IQTracker = false;

    /**
     * field for NAME_IQ
     */
    protected java.lang.String localNAME_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAME_IQTracker = false;

    /**
     * field for AKA_IQ
     */
    protected java.lang.String localAKA_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAKA_IQTracker = false;

    /**
     * field for SPOUSE_IQ
     */
    protected java.lang.String localSPOUSE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSPOUSE_IQTracker = false;

    /**
     * field for FATHER_IQ
     */
    protected java.lang.String localFATHER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFATHER_IQTracker = false;

    /**
     * field for MOTHER_IQ
     */
    protected java.lang.String localMOTHER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOTHER_IQTracker = false;

    /**
     * field for DOB_IQ
     */
    protected java.lang.String localDOB_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOB_IQTracker = false;

    /**
     * field for AGE_IQ
     */
    protected java.lang.String localAGE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_IQTracker = false;

    /**
     * field for AGE_AS_ON_IQ
     */
    protected java.lang.String localAGE_AS_ON_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAGE_AS_ON_IQTracker = false;

    /**
     * field for RATION_CARD_IQ
     */
    protected java.lang.String localRATION_CARD_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARD_IQTracker = false;

    /**
     * field for PASSPORT_IQ
     */
    protected java.lang.String localPASSPORT_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_IQTracker = false;

    /**
     * field for VOTERS_ID_IQ
     */
    protected java.lang.String localVOTERS_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTERS_ID_IQTracker = false;

    /**
     * field for DRIVING_LICENSE_NO_IQ
     */
    protected java.lang.String localDRIVING_LICENSE_NO_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVING_LICENSE_NO_IQTracker = false;

    /**
     * field for PAN_IQ
     */
    protected java.lang.String localPAN_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_IQTracker = false;

    /**
     * field for GENDER_IQ
     */
    protected java.lang.String localGENDER_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localGENDER_IQTracker = false;

    /**
     * field for OWNERSHIP_IQ
     */
    protected java.lang.String localOWNERSHIP_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIP_IQTracker = false;

    /**
     * field for ADDRESS_1_IQ
     */
    protected java.lang.String localADDRESS_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_1_IQTracker = false;

    /**
     * field for ADDRESS_2_IQ
     */
    protected java.lang.String localADDRESS_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_2_IQTracker = false;

    /**
     * field for ADDRESS_3_IQ
     */
    protected java.lang.String localADDRESS_3_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_3_IQTracker = false;

    /**
     * field for PHONE_1_IQ
     */
    protected java.lang.String localPHONE_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_1_IQTracker = false;

    /**
     * field for PHONE_2_IQ
     */
    protected java.lang.String localPHONE_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_2_IQTracker = false;

    /**
     * field for PHONE_3_IQ
     */
    protected java.lang.String localPHONE_3_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_3_IQTracker = false;

    /**
     * field for EMAIL_1_IQ
     */
    protected java.lang.String localEMAIL_1_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_1_IQTracker = false;

    /**
     * field for EMAIL_2_IQ
     */
    protected java.lang.String localEMAIL_2_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_2_IQTracker = false;

    /**
     * field for BRANCH_IQ
     */
    protected java.lang.String localBRANCH_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBRANCH_IQTracker = false;

    /**
     * field for KENDRA_IQ
     */
    protected java.lang.String localKENDRA_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKENDRA_IQTracker = false;

    /**
     * field for MBR_ID_IQ
     */
    protected java.lang.String localMBR_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMBR_ID_IQTracker = false;

    /**
     * field for LOS_APP_ID_IQ
     */
    protected java.lang.String localLOS_APP_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOS_APP_ID_IQTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP_IQ
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYP_IQTracker = false;

    /**
     * field for CREDT_INQ_PURPS_TYP_DESC_IQ
     */
    protected java.lang.String localCREDT_INQ_PURPS_TYP_DESC_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_INQ_PURPS_TYP_DESC_IQTracker = false;

    /**
     * field for CREDIT_INQUIRY_STAGE_IQ
     */
    protected java.lang.String localCREDIT_INQUIRY_STAGE_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_INQUIRY_STAGE_IQTracker = false;

    /**
     * field for CREDT_RPT_ID_IQ
     */
    protected java.lang.String localCREDT_RPT_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_ID_IQTracker = false;

    /**
     * field for CREDT_REQ_TYP_IQ
     */
    protected java.lang.String localCREDT_REQ_TYP_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_REQ_TYP_IQTracker = false;

    /**
     * field for CREDT_RPT_TRN_DT_TM_IQ
     */
    protected java.lang.String localCREDT_RPT_TRN_DT_TM_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDT_RPT_TRN_DT_TM_IQTracker = false;

    /**
     * field for AC_OPEN_DT_IQ
     */
    protected java.lang.String localAC_OPEN_DT_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAC_OPEN_DT_IQTracker = false;

    /**
     * field for LOAN_AMOUNT_IQ
     */
    protected java.lang.String localLOAN_AMOUNT_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLOAN_AMOUNT_IQTracker = false;

    /**
     * field for ENTITY_ID_IQ
     */
    protected java.lang.String localENTITY_ID_IQ;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENTITY_ID_IQTracker = false;

    /**
     * field for PR_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for PR_ACTIVE_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_ACTIVE_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_ACTIVE_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for PR_OVERDUE_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_OVERDUE_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_OVERDUE_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for PR_CURRENT_BALANCE
     */
    protected java.lang.String localPR_CURRENT_BALANCE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_CURRENT_BALANCETracker = false;

    /**
     * field for PR_SANCTIONED_AMOUNT
     */
    protected java.lang.String localPR_SANCTIONED_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_SANCTIONED_AMOUNTTracker = false;

    /**
     * field for PR_DISBURSED_AMOUNT
     */
    protected java.lang.String localPR_DISBURSED_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_DISBURSED_AMOUNTTracker = false;

    /**
     * field for PR_SECURED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_SECURED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_SECURED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for PR_UNSECURED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_UNSECURED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_UNSECURED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for PR_UNTAGGED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localPR_UNTAGGED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPR_UNTAGGED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_NUMBER_OF_ACCOUNTS
     */
    protected java.lang.String localSE_NUMBER_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_NUMBER_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_ACTIVE_NO_OF_ACCOUNTS
     */
    protected java.lang.String localSE_ACTIVE_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_ACTIVE_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_OVERDUE_NO_OF_ACCOUNTS
     */
    protected java.lang.String localSE_OVERDUE_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_OVERDUE_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_CURRENT_BALANCE
     */
    protected java.lang.String localSE_CURRENT_BALANCE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_CURRENT_BALANCETracker = false;

    /**
     * field for SE_SANCTIONED_AMOUNT
     */
    protected java.lang.String localSE_SANCTIONED_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_SANCTIONED_AMOUNTTracker = false;

    /**
     * field for SE_DISBURSED_AMOUNT
     */
    protected java.lang.String localSE_DISBURSED_AMOUNT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_DISBURSED_AMOUNTTracker = false;

    /**
     * field for SE_SECURED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localSE_SECURED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_SECURED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_UNSECURED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localSE_UNSECURED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_UNSECURED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for SE_UNTAGGED_NO_OF_ACCOUNTS
     */
    protected java.lang.String localSE_UNTAGGED_NO_OF_ACCOUNTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSE_UNTAGGED_NO_OF_ACCOUNTSTracker = false;

    /**
     * field for INQURIES_IN_LAST_SIX_MONTHS
     */
    protected java.lang.String localINQURIES_IN_LAST_SIX_MONTHS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINQURIES_IN_LAST_SIX_MONTHSTracker = false;

    /**
     * field for LENGTH_OF_CREDIT_HIS_YEAR
     */
    protected java.lang.String localLENGTH_OF_CREDIT_HIS_YEAR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLENGTH_OF_CREDIT_HIS_YEARTracker = false;

    /**
     * field for LENGTH_OF_CREDIT_HIS_MON
     */
    protected java.lang.String localLENGTH_OF_CREDIT_HIS_MON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLENGTH_OF_CREDIT_HIS_MONTracker = false;

    /**
     * field for AVG_ACC_AGE_YEAR
     */
    protected java.lang.String localAVG_ACC_AGE_YEAR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAVG_ACC_AGE_YEARTracker = false;

    /**
     * field for AVG_ACC_AGE_MON
     */
    protected java.lang.String localAVG_ACC_AGE_MON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAVG_ACC_AGE_MONTracker = false;

    /**
     * field for NEW_ACC_IN_LAST_SIX_MON
     */
    protected java.lang.String localNEW_ACC_IN_LAST_SIX_MON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNEW_ACC_IN_LAST_SIX_MONTracker = false;

    /**
     * field for NEW_DELINQ_ACC_LAST_SIX_MON
     */
    protected java.lang.String localNEW_DELINQ_ACC_LAST_SIX_MON;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNEW_DELINQ_ACC_LAST_SIX_MONTracker = false;

    /**
     * field for NAME
     */
    protected java.lang.String localNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAMETracker = false;

    /**
     * field for NAME_REPORTED_DATE
     */
    protected java.lang.String localNAME_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAME_REPORTED_DATETracker = false;

    /**
     * field for ADDRESS
     */
    protected java.lang.String localADDRESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESSTracker = false;

    /**
     * field for ADDRESS_REPORTED_DATE
     */
    protected java.lang.String localADDRESS_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADDRESS_REPORTED_DATETracker = false;

    /**
     * field for PAN_NO
     */
    protected java.lang.String localPAN_NO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_NOTracker = false;

    /**
     * field for PAN_NO_REPORTED_DATE
     */
    protected java.lang.String localPAN_NO_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPAN_NO_REPORTED_DATETracker = false;

    /**
     * field for DRIVING_LICENSE
     */
    protected java.lang.String localDRIVING_LICENSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVING_LICENSETracker = false;

    /**
     * field for DRIVING_LICENSE_REPORTED_DATE
     */
    protected java.lang.String localDRIVING_LICENSE_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDRIVING_LICENSE_REPORTED_DATETracker = false;

    /**
     * field for DOB
     */
    protected java.lang.String localDOB;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOBTracker = false;

    /**
     * field for DOB_REPORTED_DATE
     */
    protected java.lang.String localDOB_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOB_REPORTED_DATETracker = false;

    /**
     * field for VOTER_ID
     */
    protected java.lang.String localVOTER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_IDTracker = false;

    /**
     * field for VOTER_ID_REPORTED_DATE
     */
    protected java.lang.String localVOTER_ID_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVOTER_ID_REPORTED_DATETracker = false;

    /**
     * field for PASSPORT_NUMBER
     */
    protected java.lang.String localPASSPORT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_NUMBERTracker = false;

    /**
     * field for PASSPORT_REPORTED_DATE
     */
    protected java.lang.String localPASSPORT_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPASSPORT_REPORTED_DATETracker = false;

    /**
     * field for PHONE_NUMBER
     */
    protected java.lang.String localPHONE_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_NUMBERTracker = false;

    /**
     * field for PHONE_REPORTED_DATE
     */
    protected java.lang.String localPHONE_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPHONE_REPORTED_DATETracker = false;

    /**
     * field for RATION_CARD
     */
    protected java.lang.String localRATION_CARD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_CARDTracker = false;

    /**
     * field for RATION_REPORTED_DATE
     */
    protected java.lang.String localRATION_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRATION_REPORTED_DATETracker = false;

    /**
     * field for EMAIL_ID
     */
    protected java.lang.String localEMAIL_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_IDTracker = false;

    /**
     * field for EMAIL_ID_REPORTED_DATE
     */
    protected java.lang.String localEMAIL_ID_REPORTED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAIL_ID_REPORTED_DATETracker = false;

    /**
     * field for MATCHED_TYPE
     */
    protected java.lang.String localMATCHED_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMATCHED_TYPETracker = false;

    /**
     * field for ACCT_NUMBER
     */
    protected java.lang.String localACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCT_NUMBERTracker = false;

    /**
     * field for CREDIT_GUARANTOR
     */
    protected java.lang.String localCREDIT_GUARANTOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_GUARANTORTracker = false;

    /**
     * field for ACCT_TYPE
     */
    protected java.lang.String localACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCT_TYPETracker = false;

    /**
     * field for DATE_REPORTED
     */
    protected java.lang.String localDATE_REPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_REPORTEDTracker = false;

    /**
     * field for OWNERSHIP_IND
     */
    protected java.lang.String localOWNERSHIP_IND;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNERSHIP_INDTracker = false;

    /**
     * field for ACCOUNT_STATUS
     */
    protected java.lang.String localACCOUNT_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_STATUSTracker = false;

    /**
     * field for DISBURSED_AMT
     */
    protected java.lang.String localDISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISBURSED_AMTTracker = false;

    /**
     * field for DISBURSED_DT
     */
    protected java.lang.String localDISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDISBURSED_DTTracker = false;

    /**
     * field for LAST_PAYMENT_DATE
     */
    protected java.lang.String localLAST_PAYMENT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLAST_PAYMENT_DATETracker = false;

    /**
     * field for CLOSE_DT
     */
    protected java.lang.String localCLOSE_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCLOSE_DTTracker = false;

    /**
     * field for INSTALLMENT_AMT
     */
    protected java.lang.String localINSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINSTALLMENT_AMTTracker = false;

    /**
     * field for OVERDUE_AMT
     */
    protected java.lang.String localOVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOVERDUE_AMTTracker = false;

    /**
     * field for WRITE_OFF_AMT
     */
    protected java.lang.String localWRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITE_OFF_AMTTracker = false;

    /**
     * field for CURRENT_BAL
     */
    protected java.lang.String localCURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCURRENT_BALTracker = false;

    /**
     * field for CREDIT_LIMIT
     */
    protected java.lang.String localCREDIT_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREDIT_LIMITTracker = false;

    /**
     * field for ACCOUNT_REMARKS
     */
    protected java.lang.String localACCOUNT_REMARKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCOUNT_REMARKSTracker = false;

    /**
     * field for FREQUENCY
     */
    protected java.lang.String localFREQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFREQUENCYTracker = false;

    /**
     * field for SECURITY_STATUS
     */
    protected java.lang.String localSECURITY_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_STATUSTracker = false;

    /**
     * field for ORIGINAL_TERM
     */
    protected java.lang.String localORIGINAL_TERM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localORIGINAL_TERMTracker = false;

    /**
     * field for TERM_TO_MATURITY
     */
    protected java.lang.String localTERM_TO_MATURITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTERM_TO_MATURITYTracker = false;

    /**
     * field for ACCT_IN_DISPUTE
     */
    protected java.lang.String localACCT_IN_DISPUTE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACCT_IN_DISPUTETracker = false;

    /**
     * field for SETTLEMENT_AMT
     */
    protected java.lang.String localSETTLEMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSETTLEMENT_AMTTracker = false;

    /**
     * field for PRINCIPAL_WRITE_OFF_AMT
     */
    protected java.lang.String localPRINCIPAL_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRINCIPAL_WRITE_OFF_AMTTracker = false;

    /**
     * field for COMBINED_PAYMENT_HISTORY
     */
    protected java.lang.String localCOMBINED_PAYMENT_HISTORY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOMBINED_PAYMENT_HISTORYTracker = false;

    /**
     * field for REPAYMENT_TENURE
     */
    protected java.lang.String localREPAYMENT_TENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPAYMENT_TENURETracker = false;

    /**
     * field for INTEREST_RATE
     */
    protected java.lang.String localINTEREST_RATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINTEREST_RATETracker = false;

    /**
     * field for SUIT_FILED_WILFUL_DEFAULT
     */
    protected java.lang.String localSUIT_FILED_WILFUL_DEFAULT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUIT_FILED_WILFUL_DEFAULTTracker = false;

    /**
     * field for WRITTEN_OFF_SETTLED_STATUS
     */
    protected java.lang.String localWRITTEN_OFF_SETTLED_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWRITTEN_OFF_SETTLED_STATUSTracker = false;

    /**
     * field for CASH_LIMIT
     */
    protected java.lang.String localCASH_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCASH_LIMITTracker = false;

    /**
     * field for ACTUAL_PAYMENT
     */
    protected java.lang.String localACTUAL_PAYMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localACTUAL_PAYMENTTracker = false;

    /**
     * field for SECURITY_TYPE_LD
     */
    protected java.lang.String localSECURITY_TYPE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_TYPE_LDTracker = false;

    /**
     * field for OWNER_NAME_LD
     */
    protected java.lang.String localOWNER_NAME_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNER_NAME_LDTracker = false;

    /**
     * field for SECURITY_VALUE_LD
     */
    protected java.lang.String localSECURITY_VALUE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_VALUE_LDTracker = false;

    /**
     * field for DATE_OF_VALUE_LD
     */
    protected java.lang.String localDATE_OF_VALUE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_VALUE_LDTracker = false;

    /**
     * field for SECURITY_CHARGE_LD
     */
    protected java.lang.String localSECURITY_CHARGE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_CHARGE_LDTracker = false;

    /**
     * field for PROPERTY_ADDRESS_LD
     */
    protected java.lang.String localPROPERTY_ADDRESS_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROPERTY_ADDRESS_LDTracker = false;

    /**
     * field for AUTOMOBILE_TYPE_LD
     */
    protected java.lang.String localAUTOMOBILE_TYPE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAUTOMOBILE_TYPE_LDTracker = false;

    /**
     * field for YEAR_OF_MANUFACTURE_LD
     */
    protected java.lang.String localYEAR_OF_MANUFACTURE_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localYEAR_OF_MANUFACTURE_LDTracker = false;

    /**
     * field for REGISTRATION_NUMBER_LD
     */
    protected java.lang.String localREGISTRATION_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREGISTRATION_NUMBER_LDTracker = false;

    /**
     * field for ENGINE_NUMBER_LD
     */
    protected java.lang.String localENGINE_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENGINE_NUMBER_LDTracker = false;

    /**
     * field for CHASIS_NUMBER_LD
     */
    protected java.lang.String localCHASIS_NUMBER_LD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHASIS_NUMBER_LDTracker = false;

    /**
     * field for LINKED_ACCT_NUMBER
     */
    protected java.lang.String localLINKED_ACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACCT_NUMBERTracker = false;

    /**
     * field for LINKED_CREDIT_GUARANTOR
     */
    protected java.lang.String localLINKED_CREDIT_GUARANTOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_CREDIT_GUARANTORTracker = false;

    /**
     * field for LINKED_ACCT_TYPE
     */
    protected java.lang.String localLINKED_ACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACCT_TYPETracker = false;

    /**
     * field for LINKED_DATE_REPORTED
     */
    protected java.lang.String localLINKED_DATE_REPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_DATE_REPORTEDTracker = false;

    /**
     * field for LINKED_OWNERSHIP_IND
     */
    protected java.lang.String localLINKED_OWNERSHIP_IND;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_OWNERSHIP_INDTracker = false;

    /**
     * field for LINKED_ACCOUNT_STATUS
     */
    protected java.lang.String localLINKED_ACCOUNT_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACCOUNT_STATUSTracker = false;

    /**
     * field for LINKED_DISBURSED_AMT
     */
    protected java.lang.String localLINKED_DISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_DISBURSED_AMTTracker = false;

    /**
     * field for LINKED_DISBURSED_DT
     */
    protected java.lang.String localLINKED_DISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_DISBURSED_DTTracker = false;

    /**
     * field for LINKED_LAST_PAYMENT_DATE
     */
    protected java.lang.String localLINKED_LAST_PAYMENT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_LAST_PAYMENT_DATETracker = false;

    /**
     * field for LINKED_CLOSED_DATE
     */
    protected java.lang.String localLINKED_CLOSED_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_CLOSED_DATETracker = false;

    /**
     * field for LINKED_INSTALLMENT_AMT
     */
    protected java.lang.String localLINKED_INSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_INSTALLMENT_AMTTracker = false;

    /**
     * field for LINKED_OVERDUE_AMT
     */
    protected java.lang.String localLINKED_OVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_OVERDUE_AMTTracker = false;

    /**
     * field for LINKED_WRITE_OFF_AMT
     */
    protected java.lang.String localLINKED_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_WRITE_OFF_AMTTracker = false;

    /**
     * field for LINKED_CURRENT_BAL
     */
    protected java.lang.String localLINKED_CURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_CURRENT_BALTracker = false;

    /**
     * field for LINKED_CREDIT_LIMIT
     */
    protected java.lang.String localLINKED_CREDIT_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_CREDIT_LIMITTracker = false;

    /**
     * field for LINKED_ACCOUNT_REMARKS
     */
    protected java.lang.String localLINKED_ACCOUNT_REMARKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACCOUNT_REMARKSTracker = false;

    /**
     * field for LINKED_FREQUENCY
     */
    protected java.lang.String localLINKED_FREQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_FREQUENCYTracker = false;

    /**
     * field for LINKED_SECURITY_STATUS
     */
    protected java.lang.String localLINKED_SECURITY_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_SECURITY_STATUSTracker = false;

    /**
     * field for LINKED_ORIGINAL_TERM
     */
    protected java.lang.String localLINKED_ORIGINAL_TERM;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ORIGINAL_TERMTracker = false;

    /**
     * field for LINKED_TERM_TO_MATURITY
     */
    protected java.lang.String localLINKED_TERM_TO_MATURITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_TERM_TO_MATURITYTracker = false;

    /**
     * field for LINKED_ACCT_IN_DISPUTE
     */
    protected java.lang.String localLINKED_ACCT_IN_DISPUTE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACCT_IN_DISPUTETracker = false;

    /**
     * field for LINKED_SETTLEMENT_AMT
     */
    protected java.lang.String localLINKED_SETTLEMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_SETTLEMENT_AMTTracker = false;

    /**
     * field for LINKED_PRNPAL_WRITE_OFF_AMT
     */
    protected java.lang.String localLINKED_PRNPAL_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_PRNPAL_WRITE_OFF_AMTTracker = false;

    /**
     * field for LINKED_REPAYMENT_TENURE
     */
    protected java.lang.String localLINKED_REPAYMENT_TENURE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_REPAYMENT_TENURETracker = false;

    /**
     * field for LINKED_INTEREST_RATE
     */
    protected java.lang.String localLINKED_INTEREST_RATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_INTEREST_RATETracker = false;

    /**
     * field for LINKED_SUIT_F_WILFUL_DEFAULT
     */
    protected java.lang.String localLINKED_SUIT_F_WILFUL_DEFAULT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_SUIT_F_WILFUL_DEFAULTTracker = false;

    /**
     * field for LINKED_WRTN_OFF_SETTLD_STATUS
     */
    protected java.lang.String localLINKED_WRTN_OFF_SETTLD_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_WRTN_OFF_SETTLD_STATUSTracker = false;

    /**
     * field for LINKED_CASH_LIMIT
     */
    protected java.lang.String localLINKED_CASH_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_CASH_LIMITTracker = false;

    /**
     * field for LINKED_ACTUAL_PAYMENT
     */
    protected java.lang.String localLINKED_ACTUAL_PAYMENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINKED_ACTUAL_PAYMENTTracker = false;

    /**
     * field for SECURITY_TYPE_LA
     */
    protected java.lang.String localSECURITY_TYPE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_TYPE_LATracker = false;

    /**
     * field for OWNER_NAME_LA
     */
    protected java.lang.String localOWNER_NAME_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOWNER_NAME_LATracker = false;

    /**
     * field for SECURITY_VALUE_LA
     */
    protected java.lang.String localSECURITY_VALUE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_VALUE_LATracker = false;

    /**
     * field for DATE_OF_VALUE_LA
     */
    protected java.lang.String localDATE_OF_VALUE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_OF_VALUE_LATracker = false;

    /**
     * field for SECURITY_CHARGE_LA
     */
    protected java.lang.String localSECURITY_CHARGE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSECURITY_CHARGE_LATracker = false;

    /**
     * field for PROPERTY_ADDRESS_LA
     */
    protected java.lang.String localPROPERTY_ADDRESS_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROPERTY_ADDRESS_LATracker = false;

    /**
     * field for AUTOMOBILE_TYPE_LA
     */
    protected java.lang.String localAUTOMOBILE_TYPE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAUTOMOBILE_TYPE_LATracker = false;

    /**
     * field for YEAR_OF_MANUFACTURE_LA
     */
    protected java.lang.String localYEAR_OF_MANUFACTURE_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localYEAR_OF_MANUFACTURE_LATracker = false;

    /**
     * field for REGISTRATION_NUMBER_LA
     */
    protected java.lang.String localREGISTRATION_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREGISTRATION_NUMBER_LATracker = false;

    /**
     * field for ENGINE_NUMBER_LA
     */
    protected java.lang.String localENGINE_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENGINE_NUMBER_LATracker = false;

    /**
     * field for CHASIS_NUMBER_LA
     */
    protected java.lang.String localCHASIS_NUMBER_LA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCHASIS_NUMBER_LATracker = false;

    /**
     * field for SM_NAME
     */
    protected java.lang.String localSM_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_NAMETracker = false;

    /**
     * field for SM_ADDRESS
     */
    protected java.lang.String localSM_ADDRESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ADDRESSTracker = false;

    /**
     * field for SM_DOB
     */
    protected java.lang.String localSM_DOB;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DOBTracker = false;

    /**
     * field for SM_PHONE
     */
    protected java.lang.String localSM_PHONE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PHONETracker = false;

    /**
     * field for SM_PAN
     */
    protected java.lang.String localSM_PAN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PANTracker = false;

    /**
     * field for SM_PASSPORT
     */
    protected java.lang.String localSM_PASSPORT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_PASSPORTTracker = false;

    /**
     * field for SM_DRIVING_LICENSE
     */
    protected java.lang.String localSM_DRIVING_LICENSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DRIVING_LICENSETracker = false;

    /**
     * field for SM_VOTER_ID
     */
    protected java.lang.String localSM_VOTER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_VOTER_IDTracker = false;

    /**
     * field for SM_E_MAIL
     */
    protected java.lang.String localSM_E_MAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_E_MAILTracker = false;

    /**
     * field for SM_RATION_CARD
     */
    protected java.lang.String localSM_RATION_CARD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_RATION_CARDTracker = false;

    /**
     * field for SM_MATCHED_TYPE
     */
    protected java.lang.String localSM_MATCHED_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_MATCHED_TYPETracker = false;

    /**
     * field for SM_ACCT_NUMBER
     */
    protected java.lang.String localSM_ACCT_NUMBER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACCT_NUMBERTracker = false;

    /**
     * field for SM_CREDIT_GUARANTOR
     */
    protected java.lang.String localSM_CREDIT_GUARANTOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CREDIT_GUARANTORTracker = false;

    /**
     * field for SM_ACCT_TYPE
     */
    protected java.lang.String localSM_ACCT_TYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACCT_TYPETracker = false;

    /**
     * field for SM_DATE_REPORTED
     */
    protected java.lang.String localSM_DATE_REPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DATE_REPORTEDTracker = false;

    /**
     * field for SM_OWNERSHIP_IND
     */
    protected java.lang.String localSM_OWNERSHIP_IND;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_OWNERSHIP_INDTracker = false;

    /**
     * field for SM_ACCOUNT_STATUS
     */
    protected java.lang.String localSM_ACCOUNT_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACCOUNT_STATUSTracker = false;

    /**
     * field for SM_DISBURSED_AMT
     */
    protected java.lang.String localSM_DISBURSED_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DISBURSED_AMTTracker = false;

    /**
     * field for SM_DISBURSED_DT
     */
    protected java.lang.String localSM_DISBURSED_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_DISBURSED_DTTracker = false;

    /**
     * field for SM_LAST_PAYMENT_DATE
     */
    protected java.lang.String localSM_LAST_PAYMENT_DATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_LAST_PAYMENT_DATETracker = false;

    /**
     * field for SM_CLOSE_DT
     */
    protected java.lang.String localSM_CLOSE_DT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CLOSE_DTTracker = false;

    /**
     * field for SM_INSTALLMENT_AMT
     */
    protected java.lang.String localSM_INSTALLMENT_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_INSTALLMENT_AMTTracker = false;

    /**
     * field for SM_OVERDUE_AMT
     */
    protected java.lang.String localSM_OVERDUE_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_OVERDUE_AMTTracker = false;

    /**
     * field for SM_WRITE_OFF_AMT
     */
    protected java.lang.String localSM_WRITE_OFF_AMT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_WRITE_OFF_AMTTracker = false;

    /**
     * field for SM_CURRENT_BAL
     */
    protected java.lang.String localSM_CURRENT_BAL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CURRENT_BALTracker = false;

    /**
     * field for SM_CREDIT_LIMIT
     */
    protected java.lang.String localSM_CREDIT_LIMIT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_CREDIT_LIMITTracker = false;

    /**
     * field for SM_ACCOUNT_REMARKS
     */
    protected java.lang.String localSM_ACCOUNT_REMARKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_ACCOUNT_REMARKSTracker = false;

    /**
     * field for SM_FREQUENCY
     */
    protected java.lang.String localSM_FREQUENCY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSM_FREQUENCYTracker = false;

    public boolean isSRNOSpecified() {
        return localSRNOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSRNO() {
        return localSRNO;
    }

    /**
     * Auto generated setter method
     * @param param SRNO
     */
    public void setSRNO(java.lang.String param) {
        localSRNOTracker = param != null;

        this.localSRNO = param;
    }

    public boolean isMEMBER_REFERENCE_NUMBERSpecified() {
        return localMEMBER_REFERENCE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMEMBER_REFERENCE_NUMBER() {
        return localMEMBER_REFERENCE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param MEMBER_REFERENCE_NUMBER
     */
    public void setMEMBER_REFERENCE_NUMBER(java.lang.String param) {
        localMEMBER_REFERENCE_NUMBERTracker = param != null;

        this.localMEMBER_REFERENCE_NUMBER = param;
    }

    public boolean isSOA_SOURCE_NAMESpecified() {
        return localSOA_SOURCE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOA_SOURCE_NAME() {
        return localSOA_SOURCE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SOA_SOURCE_NAME
     */
    public void setSOA_SOURCE_NAME(java.lang.String param) {
        localSOA_SOURCE_NAMETracker = param != null;

        this.localSOA_SOURCE_NAME = param;
    }

    public boolean isDATE_OF_REQUESTSpecified() {
        return localDATE_OF_REQUESTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_REQUEST() {
        return localDATE_OF_REQUEST;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_REQUEST
     */
    public void setDATE_OF_REQUEST(java.lang.String param) {
        localDATE_OF_REQUESTTracker = param != null;

        this.localDATE_OF_REQUEST = param;
    }

    public boolean isPREPARED_FORSpecified() {
        return localPREPARED_FORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR() {
        return localPREPARED_FOR;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR
     */
    public void setPREPARED_FOR(java.lang.String param) {
        localPREPARED_FORTracker = param != null;

        this.localPREPARED_FOR = param;
    }

    public boolean isPREPARED_FOR_IDSpecified() {
        return localPREPARED_FOR_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPREPARED_FOR_ID() {
        return localPREPARED_FOR_ID;
    }

    /**
     * Auto generated setter method
     * @param param PREPARED_FOR_ID
     */
    public void setPREPARED_FOR_ID(java.lang.String param) {
        localPREPARED_FOR_IDTracker = param != null;

        this.localPREPARED_FOR_ID = param;
    }

    public boolean isDATE_OF_ISSUESpecified() {
        return localDATE_OF_ISSUETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_ISSUE() {
        return localDATE_OF_ISSUE;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_ISSUE
     */
    public void setDATE_OF_ISSUE(java.lang.String param) {
        localDATE_OF_ISSUETracker = param != null;

        this.localDATE_OF_ISSUE = param;
    }

    public boolean isREPORT_IDSpecified() {
        return localREPORT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPORT_ID() {
        return localREPORT_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORT_ID
     */
    public void setREPORT_ID(java.lang.String param) {
        localREPORT_IDTracker = param != null;

        this.localREPORT_ID = param;
    }

    public boolean isBATCH_IDSpecified() {
        return localBATCH_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBATCH_ID() {
        return localBATCH_ID;
    }

    /**
     * Auto generated setter method
     * @param param BATCH_ID
     */
    public void setBATCH_ID(java.lang.String param) {
        localBATCH_IDTracker = param != null;

        this.localBATCH_ID = param;
    }

    public boolean isSTATUS_IQSpecified() {
        return localSTATUS_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSTATUS_IQ() {
        return localSTATUS_IQ;
    }

    /**
     * Auto generated setter method
     * @param param STATUS_IQ
     */
    public void setSTATUS_IQ(java.lang.String param) {
        localSTATUS_IQTracker = param != null;

        this.localSTATUS_IQ = param;
    }

    public boolean isNAME_IQSpecified() {
        return localNAME_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME_IQ() {
        return localNAME_IQ;
    }

    /**
     * Auto generated setter method
     * @param param NAME_IQ
     */
    public void setNAME_IQ(java.lang.String param) {
        localNAME_IQTracker = param != null;

        this.localNAME_IQ = param;
    }

    public boolean isAKA_IQSpecified() {
        return localAKA_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAKA_IQ() {
        return localAKA_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AKA_IQ
     */
    public void setAKA_IQ(java.lang.String param) {
        localAKA_IQTracker = param != null;

        this.localAKA_IQ = param;
    }

    public boolean isSPOUSE_IQSpecified() {
        return localSPOUSE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSPOUSE_IQ() {
        return localSPOUSE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param SPOUSE_IQ
     */
    public void setSPOUSE_IQ(java.lang.String param) {
        localSPOUSE_IQTracker = param != null;

        this.localSPOUSE_IQ = param;
    }

    public boolean isFATHER_IQSpecified() {
        return localFATHER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFATHER_IQ() {
        return localFATHER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param FATHER_IQ
     */
    public void setFATHER_IQ(java.lang.String param) {
        localFATHER_IQTracker = param != null;

        this.localFATHER_IQ = param;
    }

    public boolean isMOTHER_IQSpecified() {
        return localMOTHER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOTHER_IQ() {
        return localMOTHER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param MOTHER_IQ
     */
    public void setMOTHER_IQ(java.lang.String param) {
        localMOTHER_IQTracker = param != null;

        this.localMOTHER_IQ = param;
    }

    public boolean isDOB_IQSpecified() {
        return localDOB_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB_IQ() {
        return localDOB_IQ;
    }

    /**
     * Auto generated setter method
     * @param param DOB_IQ
     */
    public void setDOB_IQ(java.lang.String param) {
        localDOB_IQTracker = param != null;

        this.localDOB_IQ = param;
    }

    public boolean isAGE_IQSpecified() {
        return localAGE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_IQ() {
        return localAGE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AGE_IQ
     */
    public void setAGE_IQ(java.lang.String param) {
        localAGE_IQTracker = param != null;

        this.localAGE_IQ = param;
    }

    public boolean isAGE_AS_ON_IQSpecified() {
        return localAGE_AS_ON_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAGE_AS_ON_IQ() {
        return localAGE_AS_ON_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AGE_AS_ON_IQ
     */
    public void setAGE_AS_ON_IQ(java.lang.String param) {
        localAGE_AS_ON_IQTracker = param != null;

        this.localAGE_AS_ON_IQ = param;
    }

    public boolean isRATION_CARD_IQSpecified() {
        return localRATION_CARD_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD_IQ() {
        return localRATION_CARD_IQ;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD_IQ
     */
    public void setRATION_CARD_IQ(java.lang.String param) {
        localRATION_CARD_IQTracker = param != null;

        this.localRATION_CARD_IQ = param;
    }

    public boolean isPASSPORT_IQSpecified() {
        return localPASSPORT_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_IQ() {
        return localPASSPORT_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_IQ
     */
    public void setPASSPORT_IQ(java.lang.String param) {
        localPASSPORT_IQTracker = param != null;

        this.localPASSPORT_IQ = param;
    }

    public boolean isVOTERS_ID_IQSpecified() {
        return localVOTERS_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTERS_ID_IQ() {
        return localVOTERS_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param VOTERS_ID_IQ
     */
    public void setVOTERS_ID_IQ(java.lang.String param) {
        localVOTERS_ID_IQTracker = param != null;

        this.localVOTERS_ID_IQ = param;
    }

    public boolean isDRIVING_LICENSE_NO_IQSpecified() {
        return localDRIVING_LICENSE_NO_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVING_LICENSE_NO_IQ() {
        return localDRIVING_LICENSE_NO_IQ;
    }

    /**
     * Auto generated setter method
     * @param param DRIVING_LICENSE_NO_IQ
     */
    public void setDRIVING_LICENSE_NO_IQ(java.lang.String param) {
        localDRIVING_LICENSE_NO_IQTracker = param != null;

        this.localDRIVING_LICENSE_NO_IQ = param;
    }

    public boolean isPAN_IQSpecified() {
        return localPAN_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_IQ() {
        return localPAN_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PAN_IQ
     */
    public void setPAN_IQ(java.lang.String param) {
        localPAN_IQTracker = param != null;

        this.localPAN_IQ = param;
    }

    public boolean isGENDER_IQSpecified() {
        return localGENDER_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getGENDER_IQ() {
        return localGENDER_IQ;
    }

    /**
     * Auto generated setter method
     * @param param GENDER_IQ
     */
    public void setGENDER_IQ(java.lang.String param) {
        localGENDER_IQTracker = param != null;

        this.localGENDER_IQ = param;
    }

    public boolean isOWNERSHIP_IQSpecified() {
        return localOWNERSHIP_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIP_IQ() {
        return localOWNERSHIP_IQ;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIP_IQ
     */
    public void setOWNERSHIP_IQ(java.lang.String param) {
        localOWNERSHIP_IQTracker = param != null;

        this.localOWNERSHIP_IQ = param;
    }

    public boolean isADDRESS_1_IQSpecified() {
        return localADDRESS_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_1_IQ() {
        return localADDRESS_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_1_IQ
     */
    public void setADDRESS_1_IQ(java.lang.String param) {
        localADDRESS_1_IQTracker = param != null;

        this.localADDRESS_1_IQ = param;
    }

    public boolean isADDRESS_2_IQSpecified() {
        return localADDRESS_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_2_IQ() {
        return localADDRESS_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_2_IQ
     */
    public void setADDRESS_2_IQ(java.lang.String param) {
        localADDRESS_2_IQTracker = param != null;

        this.localADDRESS_2_IQ = param;
    }

    public boolean isADDRESS_3_IQSpecified() {
        return localADDRESS_3_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_3_IQ() {
        return localADDRESS_3_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_3_IQ
     */
    public void setADDRESS_3_IQ(java.lang.String param) {
        localADDRESS_3_IQTracker = param != null;

        this.localADDRESS_3_IQ = param;
    }

    public boolean isPHONE_1_IQSpecified() {
        return localPHONE_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_1_IQ() {
        return localPHONE_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_1_IQ
     */
    public void setPHONE_1_IQ(java.lang.String param) {
        localPHONE_1_IQTracker = param != null;

        this.localPHONE_1_IQ = param;
    }

    public boolean isPHONE_2_IQSpecified() {
        return localPHONE_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_2_IQ() {
        return localPHONE_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_2_IQ
     */
    public void setPHONE_2_IQ(java.lang.String param) {
        localPHONE_2_IQTracker = param != null;

        this.localPHONE_2_IQ = param;
    }

    public boolean isPHONE_3_IQSpecified() {
        return localPHONE_3_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_3_IQ() {
        return localPHONE_3_IQ;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_3_IQ
     */
    public void setPHONE_3_IQ(java.lang.String param) {
        localPHONE_3_IQTracker = param != null;

        this.localPHONE_3_IQ = param;
    }

    public boolean isEMAIL_1_IQSpecified() {
        return localEMAIL_1_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_1_IQ() {
        return localEMAIL_1_IQ;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_1_IQ
     */
    public void setEMAIL_1_IQ(java.lang.String param) {
        localEMAIL_1_IQTracker = param != null;

        this.localEMAIL_1_IQ = param;
    }

    public boolean isEMAIL_2_IQSpecified() {
        return localEMAIL_2_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_2_IQ() {
        return localEMAIL_2_IQ;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_2_IQ
     */
    public void setEMAIL_2_IQ(java.lang.String param) {
        localEMAIL_2_IQTracker = param != null;

        this.localEMAIL_2_IQ = param;
    }

    public boolean isBRANCH_IQSpecified() {
        return localBRANCH_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getBRANCH_IQ() {
        return localBRANCH_IQ;
    }

    /**
     * Auto generated setter method
     * @param param BRANCH_IQ
     */
    public void setBRANCH_IQ(java.lang.String param) {
        localBRANCH_IQTracker = param != null;

        this.localBRANCH_IQ = param;
    }

    public boolean isKENDRA_IQSpecified() {
        return localKENDRA_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getKENDRA_IQ() {
        return localKENDRA_IQ;
    }

    /**
     * Auto generated setter method
     * @param param KENDRA_IQ
     */
    public void setKENDRA_IQ(java.lang.String param) {
        localKENDRA_IQTracker = param != null;

        this.localKENDRA_IQ = param;
    }

    public boolean isMBR_ID_IQSpecified() {
        return localMBR_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMBR_ID_IQ() {
        return localMBR_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param MBR_ID_IQ
     */
    public void setMBR_ID_IQ(java.lang.String param) {
        localMBR_ID_IQTracker = param != null;

        this.localMBR_ID_IQ = param;
    }

    public boolean isLOS_APP_ID_IQSpecified() {
        return localLOS_APP_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOS_APP_ID_IQ() {
        return localLOS_APP_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param LOS_APP_ID_IQ
     */
    public void setLOS_APP_ID_IQ(java.lang.String param) {
        localLOS_APP_ID_IQTracker = param != null;

        this.localLOS_APP_ID_IQ = param;
    }

    public boolean isCREDT_INQ_PURPS_TYP_IQSpecified() {
        return localCREDT_INQ_PURPS_TYP_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP_IQ() {
        return localCREDT_INQ_PURPS_TYP_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP_IQ
     */
    public void setCREDT_INQ_PURPS_TYP_IQ(java.lang.String param) {
        localCREDT_INQ_PURPS_TYP_IQTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP_IQ = param;
    }

    public boolean isCREDT_INQ_PURPS_TYP_DESC_IQSpecified() {
        return localCREDT_INQ_PURPS_TYP_DESC_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_INQ_PURPS_TYP_DESC_IQ() {
        return localCREDT_INQ_PURPS_TYP_DESC_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_INQ_PURPS_TYP_DESC_IQ
     */
    public void setCREDT_INQ_PURPS_TYP_DESC_IQ(java.lang.String param) {
        localCREDT_INQ_PURPS_TYP_DESC_IQTracker = param != null;

        this.localCREDT_INQ_PURPS_TYP_DESC_IQ = param;
    }

    public boolean isCREDIT_INQUIRY_STAGE_IQSpecified() {
        return localCREDIT_INQUIRY_STAGE_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_INQUIRY_STAGE_IQ() {
        return localCREDIT_INQUIRY_STAGE_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_INQUIRY_STAGE_IQ
     */
    public void setCREDIT_INQUIRY_STAGE_IQ(java.lang.String param) {
        localCREDIT_INQUIRY_STAGE_IQTracker = param != null;

        this.localCREDIT_INQUIRY_STAGE_IQ = param;
    }

    public boolean isCREDT_RPT_ID_IQSpecified() {
        return localCREDT_RPT_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_ID_IQ() {
        return localCREDT_RPT_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_ID_IQ
     */
    public void setCREDT_RPT_ID_IQ(java.lang.String param) {
        localCREDT_RPT_ID_IQTracker = param != null;

        this.localCREDT_RPT_ID_IQ = param;
    }

    public boolean isCREDT_REQ_TYP_IQSpecified() {
        return localCREDT_REQ_TYP_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_REQ_TYP_IQ() {
        return localCREDT_REQ_TYP_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_REQ_TYP_IQ
     */
    public void setCREDT_REQ_TYP_IQ(java.lang.String param) {
        localCREDT_REQ_TYP_IQTracker = param != null;

        this.localCREDT_REQ_TYP_IQ = param;
    }

    public boolean isCREDT_RPT_TRN_DT_TM_IQSpecified() {
        return localCREDT_RPT_TRN_DT_TM_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDT_RPT_TRN_DT_TM_IQ() {
        return localCREDT_RPT_TRN_DT_TM_IQ;
    }

    /**
     * Auto generated setter method
     * @param param CREDT_RPT_TRN_DT_TM_IQ
     */
    public void setCREDT_RPT_TRN_DT_TM_IQ(java.lang.String param) {
        localCREDT_RPT_TRN_DT_TM_IQTracker = param != null;

        this.localCREDT_RPT_TRN_DT_TM_IQ = param;
    }

    public boolean isAC_OPEN_DT_IQSpecified() {
        return localAC_OPEN_DT_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAC_OPEN_DT_IQ() {
        return localAC_OPEN_DT_IQ;
    }

    /**
     * Auto generated setter method
     * @param param AC_OPEN_DT_IQ
     */
    public void setAC_OPEN_DT_IQ(java.lang.String param) {
        localAC_OPEN_DT_IQTracker = param != null;

        this.localAC_OPEN_DT_IQ = param;
    }

    public boolean isLOAN_AMOUNT_IQSpecified() {
        return localLOAN_AMOUNT_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLOAN_AMOUNT_IQ() {
        return localLOAN_AMOUNT_IQ;
    }

    /**
     * Auto generated setter method
     * @param param LOAN_AMOUNT_IQ
     */
    public void setLOAN_AMOUNT_IQ(java.lang.String param) {
        localLOAN_AMOUNT_IQTracker = param != null;

        this.localLOAN_AMOUNT_IQ = param;
    }

    public boolean isENTITY_ID_IQSpecified() {
        return localENTITY_ID_IQTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENTITY_ID_IQ() {
        return localENTITY_ID_IQ;
    }

    /**
     * Auto generated setter method
     * @param param ENTITY_ID_IQ
     */
    public void setENTITY_ID_IQ(java.lang.String param) {
        localENTITY_ID_IQTracker = param != null;

        this.localENTITY_ID_IQ = param;
    }

    public boolean isPR_NO_OF_ACCOUNTSSpecified() {
        return localPR_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_NO_OF_ACCOUNTS() {
        return localPR_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_NO_OF_ACCOUNTS
     */
    public void setPR_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_NO_OF_ACCOUNTS = param;
    }

    public boolean isPR_ACTIVE_NO_OF_ACCOUNTSSpecified() {
        return localPR_ACTIVE_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_ACTIVE_NO_OF_ACCOUNTS() {
        return localPR_ACTIVE_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_ACTIVE_NO_OF_ACCOUNTS
     */
    public void setPR_ACTIVE_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_ACTIVE_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_ACTIVE_NO_OF_ACCOUNTS = param;
    }

    public boolean isPR_OVERDUE_NO_OF_ACCOUNTSSpecified() {
        return localPR_OVERDUE_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_OVERDUE_NO_OF_ACCOUNTS() {
        return localPR_OVERDUE_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_OVERDUE_NO_OF_ACCOUNTS
     */
    public void setPR_OVERDUE_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_OVERDUE_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_OVERDUE_NO_OF_ACCOUNTS = param;
    }

    public boolean isPR_CURRENT_BALANCESpecified() {
        return localPR_CURRENT_BALANCETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_CURRENT_BALANCE() {
        return localPR_CURRENT_BALANCE;
    }

    /**
     * Auto generated setter method
     * @param param PR_CURRENT_BALANCE
     */
    public void setPR_CURRENT_BALANCE(java.lang.String param) {
        localPR_CURRENT_BALANCETracker = param != null;

        this.localPR_CURRENT_BALANCE = param;
    }

    public boolean isPR_SANCTIONED_AMOUNTSpecified() {
        return localPR_SANCTIONED_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_SANCTIONED_AMOUNT() {
        return localPR_SANCTIONED_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param PR_SANCTIONED_AMOUNT
     */
    public void setPR_SANCTIONED_AMOUNT(java.lang.String param) {
        localPR_SANCTIONED_AMOUNTTracker = param != null;

        this.localPR_SANCTIONED_AMOUNT = param;
    }

    public boolean isPR_DISBURSED_AMOUNTSpecified() {
        return localPR_DISBURSED_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_DISBURSED_AMOUNT() {
        return localPR_DISBURSED_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param PR_DISBURSED_AMOUNT
     */
    public void setPR_DISBURSED_AMOUNT(java.lang.String param) {
        localPR_DISBURSED_AMOUNTTracker = param != null;

        this.localPR_DISBURSED_AMOUNT = param;
    }

    public boolean isPR_SECURED_NO_OF_ACCOUNTSSpecified() {
        return localPR_SECURED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_SECURED_NO_OF_ACCOUNTS() {
        return localPR_SECURED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_SECURED_NO_OF_ACCOUNTS
     */
    public void setPR_SECURED_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_SECURED_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_SECURED_NO_OF_ACCOUNTS = param;
    }

    public boolean isPR_UNSECURED_NO_OF_ACCOUNTSSpecified() {
        return localPR_UNSECURED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_UNSECURED_NO_OF_ACCOUNTS() {
        return localPR_UNSECURED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_UNSECURED_NO_OF_ACCOUNTS
     */
    public void setPR_UNSECURED_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_UNSECURED_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_UNSECURED_NO_OF_ACCOUNTS = param;
    }

    public boolean isPR_UNTAGGED_NO_OF_ACCOUNTSSpecified() {
        return localPR_UNTAGGED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPR_UNTAGGED_NO_OF_ACCOUNTS() {
        return localPR_UNTAGGED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param PR_UNTAGGED_NO_OF_ACCOUNTS
     */
    public void setPR_UNTAGGED_NO_OF_ACCOUNTS(java.lang.String param) {
        localPR_UNTAGGED_NO_OF_ACCOUNTSTracker = param != null;

        this.localPR_UNTAGGED_NO_OF_ACCOUNTS = param;
    }

    public boolean isSE_NUMBER_OF_ACCOUNTSSpecified() {
        return localSE_NUMBER_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_NUMBER_OF_ACCOUNTS() {
        return localSE_NUMBER_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_NUMBER_OF_ACCOUNTS
     */
    public void setSE_NUMBER_OF_ACCOUNTS(java.lang.String param) {
        localSE_NUMBER_OF_ACCOUNTSTracker = param != null;

        this.localSE_NUMBER_OF_ACCOUNTS = param;
    }

    public boolean isSE_ACTIVE_NO_OF_ACCOUNTSSpecified() {
        return localSE_ACTIVE_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_ACTIVE_NO_OF_ACCOUNTS() {
        return localSE_ACTIVE_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_ACTIVE_NO_OF_ACCOUNTS
     */
    public void setSE_ACTIVE_NO_OF_ACCOUNTS(java.lang.String param) {
        localSE_ACTIVE_NO_OF_ACCOUNTSTracker = param != null;

        this.localSE_ACTIVE_NO_OF_ACCOUNTS = param;
    }

    public boolean isSE_OVERDUE_NO_OF_ACCOUNTSSpecified() {
        return localSE_OVERDUE_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_OVERDUE_NO_OF_ACCOUNTS() {
        return localSE_OVERDUE_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_OVERDUE_NO_OF_ACCOUNTS
     */
    public void setSE_OVERDUE_NO_OF_ACCOUNTS(java.lang.String param) {
        localSE_OVERDUE_NO_OF_ACCOUNTSTracker = param != null;

        this.localSE_OVERDUE_NO_OF_ACCOUNTS = param;
    }

    public boolean isSE_CURRENT_BALANCESpecified() {
        return localSE_CURRENT_BALANCETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_CURRENT_BALANCE() {
        return localSE_CURRENT_BALANCE;
    }

    /**
     * Auto generated setter method
     * @param param SE_CURRENT_BALANCE
     */
    public void setSE_CURRENT_BALANCE(java.lang.String param) {
        localSE_CURRENT_BALANCETracker = param != null;

        this.localSE_CURRENT_BALANCE = param;
    }

    public boolean isSE_SANCTIONED_AMOUNTSpecified() {
        return localSE_SANCTIONED_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_SANCTIONED_AMOUNT() {
        return localSE_SANCTIONED_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param SE_SANCTIONED_AMOUNT
     */
    public void setSE_SANCTIONED_AMOUNT(java.lang.String param) {
        localSE_SANCTIONED_AMOUNTTracker = param != null;

        this.localSE_SANCTIONED_AMOUNT = param;
    }

    public boolean isSE_DISBURSED_AMOUNTSpecified() {
        return localSE_DISBURSED_AMOUNTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_DISBURSED_AMOUNT() {
        return localSE_DISBURSED_AMOUNT;
    }

    /**
     * Auto generated setter method
     * @param param SE_DISBURSED_AMOUNT
     */
    public void setSE_DISBURSED_AMOUNT(java.lang.String param) {
        localSE_DISBURSED_AMOUNTTracker = param != null;

        this.localSE_DISBURSED_AMOUNT = param;
    }

    public boolean isSE_SECURED_NO_OF_ACCOUNTSSpecified() {
        return localSE_SECURED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_SECURED_NO_OF_ACCOUNTS() {
        return localSE_SECURED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_SECURED_NO_OF_ACCOUNTS
     */
    public void setSE_SECURED_NO_OF_ACCOUNTS(java.lang.String param) {
        localSE_SECURED_NO_OF_ACCOUNTSTracker = param != null;

        this.localSE_SECURED_NO_OF_ACCOUNTS = param;
    }

    public boolean isSE_UNSECURED_NO_OF_ACCOUNTSSpecified() {
        return localSE_UNSECURED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_UNSECURED_NO_OF_ACCOUNTS() {
        return localSE_UNSECURED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_UNSECURED_NO_OF_ACCOUNTS
     */
    public void setSE_UNSECURED_NO_OF_ACCOUNTS(java.lang.String param) {
        localSE_UNSECURED_NO_OF_ACCOUNTSTracker = param != null;

        this.localSE_UNSECURED_NO_OF_ACCOUNTS = param;
    }

    public boolean isSE_UNTAGGED_NO_OF_ACCOUNTSSpecified() {
        return localSE_UNTAGGED_NO_OF_ACCOUNTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSE_UNTAGGED_NO_OF_ACCOUNTS() {
        return localSE_UNTAGGED_NO_OF_ACCOUNTS;
    }

    /**
     * Auto generated setter method
     * @param param SE_UNTAGGED_NO_OF_ACCOUNTS
     */
    public void setSE_UNTAGGED_NO_OF_ACCOUNTS(java.lang.String param) {
        localSE_UNTAGGED_NO_OF_ACCOUNTSTracker = param != null;

        this.localSE_UNTAGGED_NO_OF_ACCOUNTS = param;
    }

    public boolean isINQURIES_IN_LAST_SIX_MONTHSSpecified() {
        return localINQURIES_IN_LAST_SIX_MONTHSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINQURIES_IN_LAST_SIX_MONTHS() {
        return localINQURIES_IN_LAST_SIX_MONTHS;
    }

    /**
     * Auto generated setter method
     * @param param INQURIES_IN_LAST_SIX_MONTHS
     */
    public void setINQURIES_IN_LAST_SIX_MONTHS(java.lang.String param) {
        localINQURIES_IN_LAST_SIX_MONTHSTracker = param != null;

        this.localINQURIES_IN_LAST_SIX_MONTHS = param;
    }

    public boolean isLENGTH_OF_CREDIT_HIS_YEARSpecified() {
        return localLENGTH_OF_CREDIT_HIS_YEARTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLENGTH_OF_CREDIT_HIS_YEAR() {
        return localLENGTH_OF_CREDIT_HIS_YEAR;
    }

    /**
     * Auto generated setter method
     * @param param LENGTH_OF_CREDIT_HIS_YEAR
     */
    public void setLENGTH_OF_CREDIT_HIS_YEAR(java.lang.String param) {
        localLENGTH_OF_CREDIT_HIS_YEARTracker = param != null;

        this.localLENGTH_OF_CREDIT_HIS_YEAR = param;
    }

    public boolean isLENGTH_OF_CREDIT_HIS_MONSpecified() {
        return localLENGTH_OF_CREDIT_HIS_MONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLENGTH_OF_CREDIT_HIS_MON() {
        return localLENGTH_OF_CREDIT_HIS_MON;
    }

    /**
     * Auto generated setter method
     * @param param LENGTH_OF_CREDIT_HIS_MON
     */
    public void setLENGTH_OF_CREDIT_HIS_MON(java.lang.String param) {
        localLENGTH_OF_CREDIT_HIS_MONTracker = param != null;

        this.localLENGTH_OF_CREDIT_HIS_MON = param;
    }

    public boolean isAVG_ACC_AGE_YEARSpecified() {
        return localAVG_ACC_AGE_YEARTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAVG_ACC_AGE_YEAR() {
        return localAVG_ACC_AGE_YEAR;
    }

    /**
     * Auto generated setter method
     * @param param AVG_ACC_AGE_YEAR
     */
    public void setAVG_ACC_AGE_YEAR(java.lang.String param) {
        localAVG_ACC_AGE_YEARTracker = param != null;

        this.localAVG_ACC_AGE_YEAR = param;
    }

    public boolean isAVG_ACC_AGE_MONSpecified() {
        return localAVG_ACC_AGE_MONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAVG_ACC_AGE_MON() {
        return localAVG_ACC_AGE_MON;
    }

    /**
     * Auto generated setter method
     * @param param AVG_ACC_AGE_MON
     */
    public void setAVG_ACC_AGE_MON(java.lang.String param) {
        localAVG_ACC_AGE_MONTracker = param != null;

        this.localAVG_ACC_AGE_MON = param;
    }

    public boolean isNEW_ACC_IN_LAST_SIX_MONSpecified() {
        return localNEW_ACC_IN_LAST_SIX_MONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNEW_ACC_IN_LAST_SIX_MON() {
        return localNEW_ACC_IN_LAST_SIX_MON;
    }

    /**
     * Auto generated setter method
     * @param param NEW_ACC_IN_LAST_SIX_MON
     */
    public void setNEW_ACC_IN_LAST_SIX_MON(java.lang.String param) {
        localNEW_ACC_IN_LAST_SIX_MONTracker = param != null;

        this.localNEW_ACC_IN_LAST_SIX_MON = param;
    }

    public boolean isNEW_DELINQ_ACC_LAST_SIX_MONSpecified() {
        return localNEW_DELINQ_ACC_LAST_SIX_MONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNEW_DELINQ_ACC_LAST_SIX_MON() {
        return localNEW_DELINQ_ACC_LAST_SIX_MON;
    }

    /**
     * Auto generated setter method
     * @param param NEW_DELINQ_ACC_LAST_SIX_MON
     */
    public void setNEW_DELINQ_ACC_LAST_SIX_MON(java.lang.String param) {
        localNEW_DELINQ_ACC_LAST_SIX_MONTracker = param != null;

        this.localNEW_DELINQ_ACC_LAST_SIX_MON = param;
    }

    public boolean isNAMESpecified() {
        return localNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME() {
        return localNAME;
    }

    /**
     * Auto generated setter method
     * @param param NAME
     */
    public void setNAME(java.lang.String param) {
        localNAMETracker = param != null;

        this.localNAME = param;
    }

    public boolean isNAME_REPORTED_DATESpecified() {
        return localNAME_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME_REPORTED_DATE() {
        return localNAME_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param NAME_REPORTED_DATE
     */
    public void setNAME_REPORTED_DATE(java.lang.String param) {
        localNAME_REPORTED_DATETracker = param != null;

        this.localNAME_REPORTED_DATE = param;
    }

    public boolean isADDRESSSpecified() {
        return localADDRESSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS() {
        return localADDRESS;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS
     */
    public void setADDRESS(java.lang.String param) {
        localADDRESSTracker = param != null;

        this.localADDRESS = param;
    }

    public boolean isADDRESS_REPORTED_DATESpecified() {
        return localADDRESS_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADDRESS_REPORTED_DATE() {
        return localADDRESS_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param ADDRESS_REPORTED_DATE
     */
    public void setADDRESS_REPORTED_DATE(java.lang.String param) {
        localADDRESS_REPORTED_DATETracker = param != null;

        this.localADDRESS_REPORTED_DATE = param;
    }

    public boolean isPAN_NOSpecified() {
        return localPAN_NOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_NO() {
        return localPAN_NO;
    }

    /**
     * Auto generated setter method
     * @param param PAN_NO
     */
    public void setPAN_NO(java.lang.String param) {
        localPAN_NOTracker = param != null;

        this.localPAN_NO = param;
    }

    public boolean isPAN_NO_REPORTED_DATESpecified() {
        return localPAN_NO_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPAN_NO_REPORTED_DATE() {
        return localPAN_NO_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param PAN_NO_REPORTED_DATE
     */
    public void setPAN_NO_REPORTED_DATE(java.lang.String param) {
        localPAN_NO_REPORTED_DATETracker = param != null;

        this.localPAN_NO_REPORTED_DATE = param;
    }

    public boolean isDRIVING_LICENSESpecified() {
        return localDRIVING_LICENSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVING_LICENSE() {
        return localDRIVING_LICENSE;
    }

    /**
     * Auto generated setter method
     * @param param DRIVING_LICENSE
     */
    public void setDRIVING_LICENSE(java.lang.String param) {
        localDRIVING_LICENSETracker = param != null;

        this.localDRIVING_LICENSE = param;
    }

    public boolean isDRIVING_LICENSE_REPORTED_DATESpecified() {
        return localDRIVING_LICENSE_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDRIVING_LICENSE_REPORTED_DATE() {
        return localDRIVING_LICENSE_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param DRIVING_LICENSE_REPORTED_DATE
     */
    public void setDRIVING_LICENSE_REPORTED_DATE(java.lang.String param) {
        localDRIVING_LICENSE_REPORTED_DATETracker = param != null;

        this.localDRIVING_LICENSE_REPORTED_DATE = param;
    }

    public boolean isDOBSpecified() {
        return localDOBTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB() {
        return localDOB;
    }

    /**
     * Auto generated setter method
     * @param param DOB
     */
    public void setDOB(java.lang.String param) {
        localDOBTracker = param != null;

        this.localDOB = param;
    }

    public boolean isDOB_REPORTED_DATESpecified() {
        return localDOB_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDOB_REPORTED_DATE() {
        return localDOB_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param DOB_REPORTED_DATE
     */
    public void setDOB_REPORTED_DATE(java.lang.String param) {
        localDOB_REPORTED_DATETracker = param != null;

        this.localDOB_REPORTED_DATE = param;
    }

    public boolean isVOTER_IDSpecified() {
        return localVOTER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID() {
        return localVOTER_ID;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID
     */
    public void setVOTER_ID(java.lang.String param) {
        localVOTER_IDTracker = param != null;

        this.localVOTER_ID = param;
    }

    public boolean isVOTER_ID_REPORTED_DATESpecified() {
        return localVOTER_ID_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVOTER_ID_REPORTED_DATE() {
        return localVOTER_ID_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param VOTER_ID_REPORTED_DATE
     */
    public void setVOTER_ID_REPORTED_DATE(java.lang.String param) {
        localVOTER_ID_REPORTED_DATETracker = param != null;

        this.localVOTER_ID_REPORTED_DATE = param;
    }

    public boolean isPASSPORT_NUMBERSpecified() {
        return localPASSPORT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_NUMBER() {
        return localPASSPORT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_NUMBER
     */
    public void setPASSPORT_NUMBER(java.lang.String param) {
        localPASSPORT_NUMBERTracker = param != null;

        this.localPASSPORT_NUMBER = param;
    }

    public boolean isPASSPORT_REPORTED_DATESpecified() {
        return localPASSPORT_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPASSPORT_REPORTED_DATE() {
        return localPASSPORT_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param PASSPORT_REPORTED_DATE
     */
    public void setPASSPORT_REPORTED_DATE(java.lang.String param) {
        localPASSPORT_REPORTED_DATETracker = param != null;

        this.localPASSPORT_REPORTED_DATE = param;
    }

    public boolean isPHONE_NUMBERSpecified() {
        return localPHONE_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_NUMBER() {
        return localPHONE_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_NUMBER
     */
    public void setPHONE_NUMBER(java.lang.String param) {
        localPHONE_NUMBERTracker = param != null;

        this.localPHONE_NUMBER = param;
    }

    public boolean isPHONE_REPORTED_DATESpecified() {
        return localPHONE_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPHONE_REPORTED_DATE() {
        return localPHONE_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param PHONE_REPORTED_DATE
     */
    public void setPHONE_REPORTED_DATE(java.lang.String param) {
        localPHONE_REPORTED_DATETracker = param != null;

        this.localPHONE_REPORTED_DATE = param;
    }

    public boolean isRATION_CARDSpecified() {
        return localRATION_CARDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_CARD() {
        return localRATION_CARD;
    }

    /**
     * Auto generated setter method
     * @param param RATION_CARD
     */
    public void setRATION_CARD(java.lang.String param) {
        localRATION_CARDTracker = param != null;

        this.localRATION_CARD = param;
    }

    public boolean isRATION_REPORTED_DATESpecified() {
        return localRATION_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRATION_REPORTED_DATE() {
        return localRATION_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param RATION_REPORTED_DATE
     */
    public void setRATION_REPORTED_DATE(java.lang.String param) {
        localRATION_REPORTED_DATETracker = param != null;

        this.localRATION_REPORTED_DATE = param;
    }

    public boolean isEMAIL_IDSpecified() {
        return localEMAIL_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_ID() {
        return localEMAIL_ID;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_ID
     */
    public void setEMAIL_ID(java.lang.String param) {
        localEMAIL_IDTracker = param != null;

        this.localEMAIL_ID = param;
    }

    public boolean isEMAIL_ID_REPORTED_DATESpecified() {
        return localEMAIL_ID_REPORTED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL_ID_REPORTED_DATE() {
        return localEMAIL_ID_REPORTED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL_ID_REPORTED_DATE
     */
    public void setEMAIL_ID_REPORTED_DATE(java.lang.String param) {
        localEMAIL_ID_REPORTED_DATETracker = param != null;

        this.localEMAIL_ID_REPORTED_DATE = param;
    }

    public boolean isMATCHED_TYPESpecified() {
        return localMATCHED_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMATCHED_TYPE() {
        return localMATCHED_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param MATCHED_TYPE
     */
    public void setMATCHED_TYPE(java.lang.String param) {
        localMATCHED_TYPETracker = param != null;

        this.localMATCHED_TYPE = param;
    }

    public boolean isACCT_NUMBERSpecified() {
        return localACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCT_NUMBER() {
        return localACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param ACCT_NUMBER
     */
    public void setACCT_NUMBER(java.lang.String param) {
        localACCT_NUMBERTracker = param != null;

        this.localACCT_NUMBER = param;
    }

    public boolean isCREDIT_GUARANTORSpecified() {
        return localCREDIT_GUARANTORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_GUARANTOR() {
        return localCREDIT_GUARANTOR;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_GUARANTOR
     */
    public void setCREDIT_GUARANTOR(java.lang.String param) {
        localCREDIT_GUARANTORTracker = param != null;

        this.localCREDIT_GUARANTOR = param;
    }

    public boolean isACCT_TYPESpecified() {
        return localACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCT_TYPE() {
        return localACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param ACCT_TYPE
     */
    public void setACCT_TYPE(java.lang.String param) {
        localACCT_TYPETracker = param != null;

        this.localACCT_TYPE = param;
    }

    public boolean isDATE_REPORTEDSpecified() {
        return localDATE_REPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_REPORTED() {
        return localDATE_REPORTED;
    }

    /**
     * Auto generated setter method
     * @param param DATE_REPORTED
     */
    public void setDATE_REPORTED(java.lang.String param) {
        localDATE_REPORTEDTracker = param != null;

        this.localDATE_REPORTED = param;
    }

    public boolean isOWNERSHIP_INDSpecified() {
        return localOWNERSHIP_INDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNERSHIP_IND() {
        return localOWNERSHIP_IND;
    }

    /**
     * Auto generated setter method
     * @param param OWNERSHIP_IND
     */
    public void setOWNERSHIP_IND(java.lang.String param) {
        localOWNERSHIP_INDTracker = param != null;

        this.localOWNERSHIP_IND = param;
    }

    public boolean isACCOUNT_STATUSSpecified() {
        return localACCOUNT_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_STATUS() {
        return localACCOUNT_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_STATUS
     */
    public void setACCOUNT_STATUS(java.lang.String param) {
        localACCOUNT_STATUSTracker = param != null;

        this.localACCOUNT_STATUS = param;
    }

    public boolean isDISBURSED_AMTSpecified() {
        return localDISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISBURSED_AMT() {
        return localDISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param DISBURSED_AMT
     */
    public void setDISBURSED_AMT(java.lang.String param) {
        localDISBURSED_AMTTracker = param != null;

        this.localDISBURSED_AMT = param;
    }

    public boolean isDISBURSED_DTSpecified() {
        return localDISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDISBURSED_DT() {
        return localDISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param DISBURSED_DT
     */
    public void setDISBURSED_DT(java.lang.String param) {
        localDISBURSED_DTTracker = param != null;

        this.localDISBURSED_DT = param;
    }

    public boolean isLAST_PAYMENT_DATESpecified() {
        return localLAST_PAYMENT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLAST_PAYMENT_DATE() {
        return localLAST_PAYMENT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param LAST_PAYMENT_DATE
     */
    public void setLAST_PAYMENT_DATE(java.lang.String param) {
        localLAST_PAYMENT_DATETracker = param != null;

        this.localLAST_PAYMENT_DATE = param;
    }

    public boolean isCLOSE_DTSpecified() {
        return localCLOSE_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCLOSE_DT() {
        return localCLOSE_DT;
    }

    /**
     * Auto generated setter method
     * @param param CLOSE_DT
     */
    public void setCLOSE_DT(java.lang.String param) {
        localCLOSE_DTTracker = param != null;

        this.localCLOSE_DT = param;
    }

    public boolean isINSTALLMENT_AMTSpecified() {
        return localINSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINSTALLMENT_AMT() {
        return localINSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param INSTALLMENT_AMT
     */
    public void setINSTALLMENT_AMT(java.lang.String param) {
        localINSTALLMENT_AMTTracker = param != null;

        this.localINSTALLMENT_AMT = param;
    }

    public boolean isOVERDUE_AMTSpecified() {
        return localOVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOVERDUE_AMT() {
        return localOVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param OVERDUE_AMT
     */
    public void setOVERDUE_AMT(java.lang.String param) {
        localOVERDUE_AMTTracker = param != null;

        this.localOVERDUE_AMT = param;
    }

    public boolean isWRITE_OFF_AMTSpecified() {
        return localWRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITE_OFF_AMT() {
        return localWRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param WRITE_OFF_AMT
     */
    public void setWRITE_OFF_AMT(java.lang.String param) {
        localWRITE_OFF_AMTTracker = param != null;

        this.localWRITE_OFF_AMT = param;
    }

    public boolean isCURRENT_BALSpecified() {
        return localCURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCURRENT_BAL() {
        return localCURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param CURRENT_BAL
     */
    public void setCURRENT_BAL(java.lang.String param) {
        localCURRENT_BALTracker = param != null;

        this.localCURRENT_BAL = param;
    }

    public boolean isCREDIT_LIMITSpecified() {
        return localCREDIT_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCREDIT_LIMIT() {
        return localCREDIT_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param CREDIT_LIMIT
     */
    public void setCREDIT_LIMIT(java.lang.String param) {
        localCREDIT_LIMITTracker = param != null;

        this.localCREDIT_LIMIT = param;
    }

    public boolean isACCOUNT_REMARKSSpecified() {
        return localACCOUNT_REMARKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCOUNT_REMARKS() {
        return localACCOUNT_REMARKS;
    }

    /**
     * Auto generated setter method
     * @param param ACCOUNT_REMARKS
     */
    public void setACCOUNT_REMARKS(java.lang.String param) {
        localACCOUNT_REMARKSTracker = param != null;

        this.localACCOUNT_REMARKS = param;
    }

    public boolean isFREQUENCYSpecified() {
        return localFREQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFREQUENCY() {
        return localFREQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param FREQUENCY
     */
    public void setFREQUENCY(java.lang.String param) {
        localFREQUENCYTracker = param != null;

        this.localFREQUENCY = param;
    }

    public boolean isSECURITY_STATUSSpecified() {
        return localSECURITY_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_STATUS() {
        return localSECURITY_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_STATUS
     */
    public void setSECURITY_STATUS(java.lang.String param) {
        localSECURITY_STATUSTracker = param != null;

        this.localSECURITY_STATUS = param;
    }

    public boolean isORIGINAL_TERMSpecified() {
        return localORIGINAL_TERMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getORIGINAL_TERM() {
        return localORIGINAL_TERM;
    }

    /**
     * Auto generated setter method
     * @param param ORIGINAL_TERM
     */
    public void setORIGINAL_TERM(java.lang.String param) {
        localORIGINAL_TERMTracker = param != null;

        this.localORIGINAL_TERM = param;
    }

    public boolean isTERM_TO_MATURITYSpecified() {
        return localTERM_TO_MATURITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTERM_TO_MATURITY() {
        return localTERM_TO_MATURITY;
    }

    /**
     * Auto generated setter method
     * @param param TERM_TO_MATURITY
     */
    public void setTERM_TO_MATURITY(java.lang.String param) {
        localTERM_TO_MATURITYTracker = param != null;

        this.localTERM_TO_MATURITY = param;
    }

    public boolean isACCT_IN_DISPUTESpecified() {
        return localACCT_IN_DISPUTETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACCT_IN_DISPUTE() {
        return localACCT_IN_DISPUTE;
    }

    /**
     * Auto generated setter method
     * @param param ACCT_IN_DISPUTE
     */
    public void setACCT_IN_DISPUTE(java.lang.String param) {
        localACCT_IN_DISPUTETracker = param != null;

        this.localACCT_IN_DISPUTE = param;
    }

    public boolean isSETTLEMENT_AMTSpecified() {
        return localSETTLEMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSETTLEMENT_AMT() {
        return localSETTLEMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SETTLEMENT_AMT
     */
    public void setSETTLEMENT_AMT(java.lang.String param) {
        localSETTLEMENT_AMTTracker = param != null;

        this.localSETTLEMENT_AMT = param;
    }

    public boolean isPRINCIPAL_WRITE_OFF_AMTSpecified() {
        return localPRINCIPAL_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRINCIPAL_WRITE_OFF_AMT() {
        return localPRINCIPAL_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param PRINCIPAL_WRITE_OFF_AMT
     */
    public void setPRINCIPAL_WRITE_OFF_AMT(java.lang.String param) {
        localPRINCIPAL_WRITE_OFF_AMTTracker = param != null;

        this.localPRINCIPAL_WRITE_OFF_AMT = param;
    }

    public boolean isCOMBINED_PAYMENT_HISTORYSpecified() {
        return localCOMBINED_PAYMENT_HISTORYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOMBINED_PAYMENT_HISTORY() {
        return localCOMBINED_PAYMENT_HISTORY;
    }

    /**
     * Auto generated setter method
     * @param param COMBINED_PAYMENT_HISTORY
     */
    public void setCOMBINED_PAYMENT_HISTORY(java.lang.String param) {
        localCOMBINED_PAYMENT_HISTORYTracker = param != null;

        this.localCOMBINED_PAYMENT_HISTORY = param;
    }

    public boolean isREPAYMENT_TENURESpecified() {
        return localREPAYMENT_TENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPAYMENT_TENURE() {
        return localREPAYMENT_TENURE;
    }

    /**
     * Auto generated setter method
     * @param param REPAYMENT_TENURE
     */
    public void setREPAYMENT_TENURE(java.lang.String param) {
        localREPAYMENT_TENURETracker = param != null;

        this.localREPAYMENT_TENURE = param;
    }

    public boolean isINTEREST_RATESpecified() {
        return localINTEREST_RATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINTEREST_RATE() {
        return localINTEREST_RATE;
    }

    /**
     * Auto generated setter method
     * @param param INTEREST_RATE
     */
    public void setINTEREST_RATE(java.lang.String param) {
        localINTEREST_RATETracker = param != null;

        this.localINTEREST_RATE = param;
    }

    public boolean isSUIT_FILED_WILFUL_DEFAULTSpecified() {
        return localSUIT_FILED_WILFUL_DEFAULTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUIT_FILED_WILFUL_DEFAULT() {
        return localSUIT_FILED_WILFUL_DEFAULT;
    }

    /**
     * Auto generated setter method
     * @param param SUIT_FILED_WILFUL_DEFAULT
     */
    public void setSUIT_FILED_WILFUL_DEFAULT(java.lang.String param) {
        localSUIT_FILED_WILFUL_DEFAULTTracker = param != null;

        this.localSUIT_FILED_WILFUL_DEFAULT = param;
    }

    public boolean isWRITTEN_OFF_SETTLED_STATUSSpecified() {
        return localWRITTEN_OFF_SETTLED_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWRITTEN_OFF_SETTLED_STATUS() {
        return localWRITTEN_OFF_SETTLED_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param WRITTEN_OFF_SETTLED_STATUS
     */
    public void setWRITTEN_OFF_SETTLED_STATUS(java.lang.String param) {
        localWRITTEN_OFF_SETTLED_STATUSTracker = param != null;

        this.localWRITTEN_OFF_SETTLED_STATUS = param;
    }

    public boolean isCASH_LIMITSpecified() {
        return localCASH_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCASH_LIMIT() {
        return localCASH_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param CASH_LIMIT
     */
    public void setCASH_LIMIT(java.lang.String param) {
        localCASH_LIMITTracker = param != null;

        this.localCASH_LIMIT = param;
    }

    public boolean isACTUAL_PAYMENTSpecified() {
        return localACTUAL_PAYMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getACTUAL_PAYMENT() {
        return localACTUAL_PAYMENT;
    }

    /**
     * Auto generated setter method
     * @param param ACTUAL_PAYMENT
     */
    public void setACTUAL_PAYMENT(java.lang.String param) {
        localACTUAL_PAYMENTTracker = param != null;

        this.localACTUAL_PAYMENT = param;
    }

    public boolean isSECURITY_TYPE_LDSpecified() {
        return localSECURITY_TYPE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_TYPE_LD() {
        return localSECURITY_TYPE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_TYPE_LD
     */
    public void setSECURITY_TYPE_LD(java.lang.String param) {
        localSECURITY_TYPE_LDTracker = param != null;

        this.localSECURITY_TYPE_LD = param;
    }

    public boolean isOWNER_NAME_LDSpecified() {
        return localOWNER_NAME_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNER_NAME_LD() {
        return localOWNER_NAME_LD;
    }

    /**
     * Auto generated setter method
     * @param param OWNER_NAME_LD
     */
    public void setOWNER_NAME_LD(java.lang.String param) {
        localOWNER_NAME_LDTracker = param != null;

        this.localOWNER_NAME_LD = param;
    }

    public boolean isSECURITY_VALUE_LDSpecified() {
        return localSECURITY_VALUE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_VALUE_LD() {
        return localSECURITY_VALUE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_VALUE_LD
     */
    public void setSECURITY_VALUE_LD(java.lang.String param) {
        localSECURITY_VALUE_LDTracker = param != null;

        this.localSECURITY_VALUE_LD = param;
    }

    public boolean isDATE_OF_VALUE_LDSpecified() {
        return localDATE_OF_VALUE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_VALUE_LD() {
        return localDATE_OF_VALUE_LD;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_VALUE_LD
     */
    public void setDATE_OF_VALUE_LD(java.lang.String param) {
        localDATE_OF_VALUE_LDTracker = param != null;

        this.localDATE_OF_VALUE_LD = param;
    }

    public boolean isSECURITY_CHARGE_LDSpecified() {
        return localSECURITY_CHARGE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_CHARGE_LD() {
        return localSECURITY_CHARGE_LD;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_CHARGE_LD
     */
    public void setSECURITY_CHARGE_LD(java.lang.String param) {
        localSECURITY_CHARGE_LDTracker = param != null;

        this.localSECURITY_CHARGE_LD = param;
    }

    public boolean isPROPERTY_ADDRESS_LDSpecified() {
        return localPROPERTY_ADDRESS_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPROPERTY_ADDRESS_LD() {
        return localPROPERTY_ADDRESS_LD;
    }

    /**
     * Auto generated setter method
     * @param param PROPERTY_ADDRESS_LD
     */
    public void setPROPERTY_ADDRESS_LD(java.lang.String param) {
        localPROPERTY_ADDRESS_LDTracker = param != null;

        this.localPROPERTY_ADDRESS_LD = param;
    }

    public boolean isAUTOMOBILE_TYPE_LDSpecified() {
        return localAUTOMOBILE_TYPE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAUTOMOBILE_TYPE_LD() {
        return localAUTOMOBILE_TYPE_LD;
    }

    /**
     * Auto generated setter method
     * @param param AUTOMOBILE_TYPE_LD
     */
    public void setAUTOMOBILE_TYPE_LD(java.lang.String param) {
        localAUTOMOBILE_TYPE_LDTracker = param != null;

        this.localAUTOMOBILE_TYPE_LD = param;
    }

    public boolean isYEAR_OF_MANUFACTURE_LDSpecified() {
        return localYEAR_OF_MANUFACTURE_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getYEAR_OF_MANUFACTURE_LD() {
        return localYEAR_OF_MANUFACTURE_LD;
    }

    /**
     * Auto generated setter method
     * @param param YEAR_OF_MANUFACTURE_LD
     */
    public void setYEAR_OF_MANUFACTURE_LD(java.lang.String param) {
        localYEAR_OF_MANUFACTURE_LDTracker = param != null;

        this.localYEAR_OF_MANUFACTURE_LD = param;
    }

    public boolean isREGISTRATION_NUMBER_LDSpecified() {
        return localREGISTRATION_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREGISTRATION_NUMBER_LD() {
        return localREGISTRATION_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param REGISTRATION_NUMBER_LD
     */
    public void setREGISTRATION_NUMBER_LD(java.lang.String param) {
        localREGISTRATION_NUMBER_LDTracker = param != null;

        this.localREGISTRATION_NUMBER_LD = param;
    }

    public boolean isENGINE_NUMBER_LDSpecified() {
        return localENGINE_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENGINE_NUMBER_LD() {
        return localENGINE_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param ENGINE_NUMBER_LD
     */
    public void setENGINE_NUMBER_LD(java.lang.String param) {
        localENGINE_NUMBER_LDTracker = param != null;

        this.localENGINE_NUMBER_LD = param;
    }

    public boolean isCHASIS_NUMBER_LDSpecified() {
        return localCHASIS_NUMBER_LDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCHASIS_NUMBER_LD() {
        return localCHASIS_NUMBER_LD;
    }

    /**
     * Auto generated setter method
     * @param param CHASIS_NUMBER_LD
     */
    public void setCHASIS_NUMBER_LD(java.lang.String param) {
        localCHASIS_NUMBER_LDTracker = param != null;

        this.localCHASIS_NUMBER_LD = param;
    }

    public boolean isLINKED_ACCT_NUMBERSpecified() {
        return localLINKED_ACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACCT_NUMBER() {
        return localLINKED_ACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACCT_NUMBER
     */
    public void setLINKED_ACCT_NUMBER(java.lang.String param) {
        localLINKED_ACCT_NUMBERTracker = param != null;

        this.localLINKED_ACCT_NUMBER = param;
    }

    public boolean isLINKED_CREDIT_GUARANTORSpecified() {
        return localLINKED_CREDIT_GUARANTORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_CREDIT_GUARANTOR() {
        return localLINKED_CREDIT_GUARANTOR;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_CREDIT_GUARANTOR
     */
    public void setLINKED_CREDIT_GUARANTOR(java.lang.String param) {
        localLINKED_CREDIT_GUARANTORTracker = param != null;

        this.localLINKED_CREDIT_GUARANTOR = param;
    }

    public boolean isLINKED_ACCT_TYPESpecified() {
        return localLINKED_ACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACCT_TYPE() {
        return localLINKED_ACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACCT_TYPE
     */
    public void setLINKED_ACCT_TYPE(java.lang.String param) {
        localLINKED_ACCT_TYPETracker = param != null;

        this.localLINKED_ACCT_TYPE = param;
    }

    public boolean isLINKED_DATE_REPORTEDSpecified() {
        return localLINKED_DATE_REPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_DATE_REPORTED() {
        return localLINKED_DATE_REPORTED;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_DATE_REPORTED
     */
    public void setLINKED_DATE_REPORTED(java.lang.String param) {
        localLINKED_DATE_REPORTEDTracker = param != null;

        this.localLINKED_DATE_REPORTED = param;
    }

    public boolean isLINKED_OWNERSHIP_INDSpecified() {
        return localLINKED_OWNERSHIP_INDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_OWNERSHIP_IND() {
        return localLINKED_OWNERSHIP_IND;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_OWNERSHIP_IND
     */
    public void setLINKED_OWNERSHIP_IND(java.lang.String param) {
        localLINKED_OWNERSHIP_INDTracker = param != null;

        this.localLINKED_OWNERSHIP_IND = param;
    }

    public boolean isLINKED_ACCOUNT_STATUSSpecified() {
        return localLINKED_ACCOUNT_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACCOUNT_STATUS() {
        return localLINKED_ACCOUNT_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACCOUNT_STATUS
     */
    public void setLINKED_ACCOUNT_STATUS(java.lang.String param) {
        localLINKED_ACCOUNT_STATUSTracker = param != null;

        this.localLINKED_ACCOUNT_STATUS = param;
    }

    public boolean isLINKED_DISBURSED_AMTSpecified() {
        return localLINKED_DISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_DISBURSED_AMT() {
        return localLINKED_DISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_DISBURSED_AMT
     */
    public void setLINKED_DISBURSED_AMT(java.lang.String param) {
        localLINKED_DISBURSED_AMTTracker = param != null;

        this.localLINKED_DISBURSED_AMT = param;
    }

    public boolean isLINKED_DISBURSED_DTSpecified() {
        return localLINKED_DISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_DISBURSED_DT() {
        return localLINKED_DISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_DISBURSED_DT
     */
    public void setLINKED_DISBURSED_DT(java.lang.String param) {
        localLINKED_DISBURSED_DTTracker = param != null;

        this.localLINKED_DISBURSED_DT = param;
    }

    public boolean isLINKED_LAST_PAYMENT_DATESpecified() {
        return localLINKED_LAST_PAYMENT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_LAST_PAYMENT_DATE() {
        return localLINKED_LAST_PAYMENT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_LAST_PAYMENT_DATE
     */
    public void setLINKED_LAST_PAYMENT_DATE(java.lang.String param) {
        localLINKED_LAST_PAYMENT_DATETracker = param != null;

        this.localLINKED_LAST_PAYMENT_DATE = param;
    }

    public boolean isLINKED_CLOSED_DATESpecified() {
        return localLINKED_CLOSED_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_CLOSED_DATE() {
        return localLINKED_CLOSED_DATE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_CLOSED_DATE
     */
    public void setLINKED_CLOSED_DATE(java.lang.String param) {
        localLINKED_CLOSED_DATETracker = param != null;

        this.localLINKED_CLOSED_DATE = param;
    }

    public boolean isLINKED_INSTALLMENT_AMTSpecified() {
        return localLINKED_INSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_INSTALLMENT_AMT() {
        return localLINKED_INSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_INSTALLMENT_AMT
     */
    public void setLINKED_INSTALLMENT_AMT(java.lang.String param) {
        localLINKED_INSTALLMENT_AMTTracker = param != null;

        this.localLINKED_INSTALLMENT_AMT = param;
    }

    public boolean isLINKED_OVERDUE_AMTSpecified() {
        return localLINKED_OVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_OVERDUE_AMT() {
        return localLINKED_OVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_OVERDUE_AMT
     */
    public void setLINKED_OVERDUE_AMT(java.lang.String param) {
        localLINKED_OVERDUE_AMTTracker = param != null;

        this.localLINKED_OVERDUE_AMT = param;
    }

    public boolean isLINKED_WRITE_OFF_AMTSpecified() {
        return localLINKED_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_WRITE_OFF_AMT() {
        return localLINKED_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_WRITE_OFF_AMT
     */
    public void setLINKED_WRITE_OFF_AMT(java.lang.String param) {
        localLINKED_WRITE_OFF_AMTTracker = param != null;

        this.localLINKED_WRITE_OFF_AMT = param;
    }

    public boolean isLINKED_CURRENT_BALSpecified() {
        return localLINKED_CURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_CURRENT_BAL() {
        return localLINKED_CURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_CURRENT_BAL
     */
    public void setLINKED_CURRENT_BAL(java.lang.String param) {
        localLINKED_CURRENT_BALTracker = param != null;

        this.localLINKED_CURRENT_BAL = param;
    }

    public boolean isLINKED_CREDIT_LIMITSpecified() {
        return localLINKED_CREDIT_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_CREDIT_LIMIT() {
        return localLINKED_CREDIT_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_CREDIT_LIMIT
     */
    public void setLINKED_CREDIT_LIMIT(java.lang.String param) {
        localLINKED_CREDIT_LIMITTracker = param != null;

        this.localLINKED_CREDIT_LIMIT = param;
    }

    public boolean isLINKED_ACCOUNT_REMARKSSpecified() {
        return localLINKED_ACCOUNT_REMARKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACCOUNT_REMARKS() {
        return localLINKED_ACCOUNT_REMARKS;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACCOUNT_REMARKS
     */
    public void setLINKED_ACCOUNT_REMARKS(java.lang.String param) {
        localLINKED_ACCOUNT_REMARKSTracker = param != null;

        this.localLINKED_ACCOUNT_REMARKS = param;
    }

    public boolean isLINKED_FREQUENCYSpecified() {
        return localLINKED_FREQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_FREQUENCY() {
        return localLINKED_FREQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_FREQUENCY
     */
    public void setLINKED_FREQUENCY(java.lang.String param) {
        localLINKED_FREQUENCYTracker = param != null;

        this.localLINKED_FREQUENCY = param;
    }

    public boolean isLINKED_SECURITY_STATUSSpecified() {
        return localLINKED_SECURITY_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_SECURITY_STATUS() {
        return localLINKED_SECURITY_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_SECURITY_STATUS
     */
    public void setLINKED_SECURITY_STATUS(java.lang.String param) {
        localLINKED_SECURITY_STATUSTracker = param != null;

        this.localLINKED_SECURITY_STATUS = param;
    }

    public boolean isLINKED_ORIGINAL_TERMSpecified() {
        return localLINKED_ORIGINAL_TERMTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ORIGINAL_TERM() {
        return localLINKED_ORIGINAL_TERM;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ORIGINAL_TERM
     */
    public void setLINKED_ORIGINAL_TERM(java.lang.String param) {
        localLINKED_ORIGINAL_TERMTracker = param != null;

        this.localLINKED_ORIGINAL_TERM = param;
    }

    public boolean isLINKED_TERM_TO_MATURITYSpecified() {
        return localLINKED_TERM_TO_MATURITYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_TERM_TO_MATURITY() {
        return localLINKED_TERM_TO_MATURITY;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_TERM_TO_MATURITY
     */
    public void setLINKED_TERM_TO_MATURITY(java.lang.String param) {
        localLINKED_TERM_TO_MATURITYTracker = param != null;

        this.localLINKED_TERM_TO_MATURITY = param;
    }

    public boolean isLINKED_ACCT_IN_DISPUTESpecified() {
        return localLINKED_ACCT_IN_DISPUTETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACCT_IN_DISPUTE() {
        return localLINKED_ACCT_IN_DISPUTE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACCT_IN_DISPUTE
     */
    public void setLINKED_ACCT_IN_DISPUTE(java.lang.String param) {
        localLINKED_ACCT_IN_DISPUTETracker = param != null;

        this.localLINKED_ACCT_IN_DISPUTE = param;
    }

    public boolean isLINKED_SETTLEMENT_AMTSpecified() {
        return localLINKED_SETTLEMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_SETTLEMENT_AMT() {
        return localLINKED_SETTLEMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_SETTLEMENT_AMT
     */
    public void setLINKED_SETTLEMENT_AMT(java.lang.String param) {
        localLINKED_SETTLEMENT_AMTTracker = param != null;

        this.localLINKED_SETTLEMENT_AMT = param;
    }

    public boolean isLINKED_PRNPAL_WRITE_OFF_AMTSpecified() {
        return localLINKED_PRNPAL_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_PRNPAL_WRITE_OFF_AMT() {
        return localLINKED_PRNPAL_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_PRNPAL_WRITE_OFF_AMT
     */
    public void setLINKED_PRNPAL_WRITE_OFF_AMT(java.lang.String param) {
        localLINKED_PRNPAL_WRITE_OFF_AMTTracker = param != null;

        this.localLINKED_PRNPAL_WRITE_OFF_AMT = param;
    }

    public boolean isLINKED_REPAYMENT_TENURESpecified() {
        return localLINKED_REPAYMENT_TENURETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_REPAYMENT_TENURE() {
        return localLINKED_REPAYMENT_TENURE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_REPAYMENT_TENURE
     */
    public void setLINKED_REPAYMENT_TENURE(java.lang.String param) {
        localLINKED_REPAYMENT_TENURETracker = param != null;

        this.localLINKED_REPAYMENT_TENURE = param;
    }

    public boolean isLINKED_INTEREST_RATESpecified() {
        return localLINKED_INTEREST_RATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_INTEREST_RATE() {
        return localLINKED_INTEREST_RATE;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_INTEREST_RATE
     */
    public void setLINKED_INTEREST_RATE(java.lang.String param) {
        localLINKED_INTEREST_RATETracker = param != null;

        this.localLINKED_INTEREST_RATE = param;
    }

    public boolean isLINKED_SUIT_F_WILFUL_DEFAULTSpecified() {
        return localLINKED_SUIT_F_WILFUL_DEFAULTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_SUIT_F_WILFUL_DEFAULT() {
        return localLINKED_SUIT_F_WILFUL_DEFAULT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_SUIT_F_WILFUL_DEFAULT
     */
    public void setLINKED_SUIT_F_WILFUL_DEFAULT(java.lang.String param) {
        localLINKED_SUIT_F_WILFUL_DEFAULTTracker = param != null;

        this.localLINKED_SUIT_F_WILFUL_DEFAULT = param;
    }

    public boolean isLINKED_WRTN_OFF_SETTLD_STATUSSpecified() {
        return localLINKED_WRTN_OFF_SETTLD_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_WRTN_OFF_SETTLD_STATUS() {
        return localLINKED_WRTN_OFF_SETTLD_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_WRTN_OFF_SETTLD_STATUS
     */
    public void setLINKED_WRTN_OFF_SETTLD_STATUS(java.lang.String param) {
        localLINKED_WRTN_OFF_SETTLD_STATUSTracker = param != null;

        this.localLINKED_WRTN_OFF_SETTLD_STATUS = param;
    }

    public boolean isLINKED_CASH_LIMITSpecified() {
        return localLINKED_CASH_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_CASH_LIMIT() {
        return localLINKED_CASH_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_CASH_LIMIT
     */
    public void setLINKED_CASH_LIMIT(java.lang.String param) {
        localLINKED_CASH_LIMITTracker = param != null;

        this.localLINKED_CASH_LIMIT = param;
    }

    public boolean isLINKED_ACTUAL_PAYMENTSpecified() {
        return localLINKED_ACTUAL_PAYMENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINKED_ACTUAL_PAYMENT() {
        return localLINKED_ACTUAL_PAYMENT;
    }

    /**
     * Auto generated setter method
     * @param param LINKED_ACTUAL_PAYMENT
     */
    public void setLINKED_ACTUAL_PAYMENT(java.lang.String param) {
        localLINKED_ACTUAL_PAYMENTTracker = param != null;

        this.localLINKED_ACTUAL_PAYMENT = param;
    }

    public boolean isSECURITY_TYPE_LASpecified() {
        return localSECURITY_TYPE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_TYPE_LA() {
        return localSECURITY_TYPE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_TYPE_LA
     */
    public void setSECURITY_TYPE_LA(java.lang.String param) {
        localSECURITY_TYPE_LATracker = param != null;

        this.localSECURITY_TYPE_LA = param;
    }

    public boolean isOWNER_NAME_LASpecified() {
        return localOWNER_NAME_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOWNER_NAME_LA() {
        return localOWNER_NAME_LA;
    }

    /**
     * Auto generated setter method
     * @param param OWNER_NAME_LA
     */
    public void setOWNER_NAME_LA(java.lang.String param) {
        localOWNER_NAME_LATracker = param != null;

        this.localOWNER_NAME_LA = param;
    }

    public boolean isSECURITY_VALUE_LASpecified() {
        return localSECURITY_VALUE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_VALUE_LA() {
        return localSECURITY_VALUE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_VALUE_LA
     */
    public void setSECURITY_VALUE_LA(java.lang.String param) {
        localSECURITY_VALUE_LATracker = param != null;

        this.localSECURITY_VALUE_LA = param;
    }

    public boolean isDATE_OF_VALUE_LASpecified() {
        return localDATE_OF_VALUE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDATE_OF_VALUE_LA() {
        return localDATE_OF_VALUE_LA;
    }

    /**
     * Auto generated setter method
     * @param param DATE_OF_VALUE_LA
     */
    public void setDATE_OF_VALUE_LA(java.lang.String param) {
        localDATE_OF_VALUE_LATracker = param != null;

        this.localDATE_OF_VALUE_LA = param;
    }

    public boolean isSECURITY_CHARGE_LASpecified() {
        return localSECURITY_CHARGE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSECURITY_CHARGE_LA() {
        return localSECURITY_CHARGE_LA;
    }

    /**
     * Auto generated setter method
     * @param param SECURITY_CHARGE_LA
     */
    public void setSECURITY_CHARGE_LA(java.lang.String param) {
        localSECURITY_CHARGE_LATracker = param != null;

        this.localSECURITY_CHARGE_LA = param;
    }

    public boolean isPROPERTY_ADDRESS_LASpecified() {
        return localPROPERTY_ADDRESS_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPROPERTY_ADDRESS_LA() {
        return localPROPERTY_ADDRESS_LA;
    }

    /**
     * Auto generated setter method
     * @param param PROPERTY_ADDRESS_LA
     */
    public void setPROPERTY_ADDRESS_LA(java.lang.String param) {
        localPROPERTY_ADDRESS_LATracker = param != null;

        this.localPROPERTY_ADDRESS_LA = param;
    }

    public boolean isAUTOMOBILE_TYPE_LASpecified() {
        return localAUTOMOBILE_TYPE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAUTOMOBILE_TYPE_LA() {
        return localAUTOMOBILE_TYPE_LA;
    }

    /**
     * Auto generated setter method
     * @param param AUTOMOBILE_TYPE_LA
     */
    public void setAUTOMOBILE_TYPE_LA(java.lang.String param) {
        localAUTOMOBILE_TYPE_LATracker = param != null;

        this.localAUTOMOBILE_TYPE_LA = param;
    }

    public boolean isYEAR_OF_MANUFACTURE_LASpecified() {
        return localYEAR_OF_MANUFACTURE_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getYEAR_OF_MANUFACTURE_LA() {
        return localYEAR_OF_MANUFACTURE_LA;
    }

    /**
     * Auto generated setter method
     * @param param YEAR_OF_MANUFACTURE_LA
     */
    public void setYEAR_OF_MANUFACTURE_LA(java.lang.String param) {
        localYEAR_OF_MANUFACTURE_LATracker = param != null;

        this.localYEAR_OF_MANUFACTURE_LA = param;
    }

    public boolean isREGISTRATION_NUMBER_LASpecified() {
        return localREGISTRATION_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREGISTRATION_NUMBER_LA() {
        return localREGISTRATION_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param REGISTRATION_NUMBER_LA
     */
    public void setREGISTRATION_NUMBER_LA(java.lang.String param) {
        localREGISTRATION_NUMBER_LATracker = param != null;

        this.localREGISTRATION_NUMBER_LA = param;
    }

    public boolean isENGINE_NUMBER_LASpecified() {
        return localENGINE_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENGINE_NUMBER_LA() {
        return localENGINE_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param ENGINE_NUMBER_LA
     */
    public void setENGINE_NUMBER_LA(java.lang.String param) {
        localENGINE_NUMBER_LATracker = param != null;

        this.localENGINE_NUMBER_LA = param;
    }

    public boolean isCHASIS_NUMBER_LASpecified() {
        return localCHASIS_NUMBER_LATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCHASIS_NUMBER_LA() {
        return localCHASIS_NUMBER_LA;
    }

    /**
     * Auto generated setter method
     * @param param CHASIS_NUMBER_LA
     */
    public void setCHASIS_NUMBER_LA(java.lang.String param) {
        localCHASIS_NUMBER_LATracker = param != null;

        this.localCHASIS_NUMBER_LA = param;
    }

    public boolean isSM_NAMESpecified() {
        return localSM_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_NAME() {
        return localSM_NAME;
    }

    /**
     * Auto generated setter method
     * @param param SM_NAME
     */
    public void setSM_NAME(java.lang.String param) {
        localSM_NAMETracker = param != null;

        this.localSM_NAME = param;
    }

    public boolean isSM_ADDRESSSpecified() {
        return localSM_ADDRESSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ADDRESS() {
        return localSM_ADDRESS;
    }

    /**
     * Auto generated setter method
     * @param param SM_ADDRESS
     */
    public void setSM_ADDRESS(java.lang.String param) {
        localSM_ADDRESSTracker = param != null;

        this.localSM_ADDRESS = param;
    }

    public boolean isSM_DOBSpecified() {
        return localSM_DOBTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DOB() {
        return localSM_DOB;
    }

    /**
     * Auto generated setter method
     * @param param SM_DOB
     */
    public void setSM_DOB(java.lang.String param) {
        localSM_DOBTracker = param != null;

        this.localSM_DOB = param;
    }

    public boolean isSM_PHONESpecified() {
        return localSM_PHONETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PHONE() {
        return localSM_PHONE;
    }

    /**
     * Auto generated setter method
     * @param param SM_PHONE
     */
    public void setSM_PHONE(java.lang.String param) {
        localSM_PHONETracker = param != null;

        this.localSM_PHONE = param;
    }

    public boolean isSM_PANSpecified() {
        return localSM_PANTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PAN() {
        return localSM_PAN;
    }

    /**
     * Auto generated setter method
     * @param param SM_PAN
     */
    public void setSM_PAN(java.lang.String param) {
        localSM_PANTracker = param != null;

        this.localSM_PAN = param;
    }

    public boolean isSM_PASSPORTSpecified() {
        return localSM_PASSPORTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_PASSPORT() {
        return localSM_PASSPORT;
    }

    /**
     * Auto generated setter method
     * @param param SM_PASSPORT
     */
    public void setSM_PASSPORT(java.lang.String param) {
        localSM_PASSPORTTracker = param != null;

        this.localSM_PASSPORT = param;
    }

    public boolean isSM_DRIVING_LICENSESpecified() {
        return localSM_DRIVING_LICENSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DRIVING_LICENSE() {
        return localSM_DRIVING_LICENSE;
    }

    /**
     * Auto generated setter method
     * @param param SM_DRIVING_LICENSE
     */
    public void setSM_DRIVING_LICENSE(java.lang.String param) {
        localSM_DRIVING_LICENSETracker = param != null;

        this.localSM_DRIVING_LICENSE = param;
    }

    public boolean isSM_VOTER_IDSpecified() {
        return localSM_VOTER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_VOTER_ID() {
        return localSM_VOTER_ID;
    }

    /**
     * Auto generated setter method
     * @param param SM_VOTER_ID
     */
    public void setSM_VOTER_ID(java.lang.String param) {
        localSM_VOTER_IDTracker = param != null;

        this.localSM_VOTER_ID = param;
    }

    public boolean isSM_E_MAILSpecified() {
        return localSM_E_MAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_E_MAIL() {
        return localSM_E_MAIL;
    }

    /**
     * Auto generated setter method
     * @param param SM_E_MAIL
     */
    public void setSM_E_MAIL(java.lang.String param) {
        localSM_E_MAILTracker = param != null;

        this.localSM_E_MAIL = param;
    }

    public boolean isSM_RATION_CARDSpecified() {
        return localSM_RATION_CARDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_RATION_CARD() {
        return localSM_RATION_CARD;
    }

    /**
     * Auto generated setter method
     * @param param SM_RATION_CARD
     */
    public void setSM_RATION_CARD(java.lang.String param) {
        localSM_RATION_CARDTracker = param != null;

        this.localSM_RATION_CARD = param;
    }

    public boolean isSM_MATCHED_TYPESpecified() {
        return localSM_MATCHED_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_MATCHED_TYPE() {
        return localSM_MATCHED_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param SM_MATCHED_TYPE
     */
    public void setSM_MATCHED_TYPE(java.lang.String param) {
        localSM_MATCHED_TYPETracker = param != null;

        this.localSM_MATCHED_TYPE = param;
    }

    public boolean isSM_ACCT_NUMBERSpecified() {
        return localSM_ACCT_NUMBERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACCT_NUMBER() {
        return localSM_ACCT_NUMBER;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACCT_NUMBER
     */
    public void setSM_ACCT_NUMBER(java.lang.String param) {
        localSM_ACCT_NUMBERTracker = param != null;

        this.localSM_ACCT_NUMBER = param;
    }

    public boolean isSM_CREDIT_GUARANTORSpecified() {
        return localSM_CREDIT_GUARANTORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CREDIT_GUARANTOR() {
        return localSM_CREDIT_GUARANTOR;
    }

    /**
     * Auto generated setter method
     * @param param SM_CREDIT_GUARANTOR
     */
    public void setSM_CREDIT_GUARANTOR(java.lang.String param) {
        localSM_CREDIT_GUARANTORTracker = param != null;

        this.localSM_CREDIT_GUARANTOR = param;
    }

    public boolean isSM_ACCT_TYPESpecified() {
        return localSM_ACCT_TYPETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACCT_TYPE() {
        return localSM_ACCT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACCT_TYPE
     */
    public void setSM_ACCT_TYPE(java.lang.String param) {
        localSM_ACCT_TYPETracker = param != null;

        this.localSM_ACCT_TYPE = param;
    }

    public boolean isSM_DATE_REPORTEDSpecified() {
        return localSM_DATE_REPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DATE_REPORTED() {
        return localSM_DATE_REPORTED;
    }

    /**
     * Auto generated setter method
     * @param param SM_DATE_REPORTED
     */
    public void setSM_DATE_REPORTED(java.lang.String param) {
        localSM_DATE_REPORTEDTracker = param != null;

        this.localSM_DATE_REPORTED = param;
    }

    public boolean isSM_OWNERSHIP_INDSpecified() {
        return localSM_OWNERSHIP_INDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_OWNERSHIP_IND() {
        return localSM_OWNERSHIP_IND;
    }

    /**
     * Auto generated setter method
     * @param param SM_OWNERSHIP_IND
     */
    public void setSM_OWNERSHIP_IND(java.lang.String param) {
        localSM_OWNERSHIP_INDTracker = param != null;

        this.localSM_OWNERSHIP_IND = param;
    }

    public boolean isSM_ACCOUNT_STATUSSpecified() {
        return localSM_ACCOUNT_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACCOUNT_STATUS() {
        return localSM_ACCOUNT_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACCOUNT_STATUS
     */
    public void setSM_ACCOUNT_STATUS(java.lang.String param) {
        localSM_ACCOUNT_STATUSTracker = param != null;

        this.localSM_ACCOUNT_STATUS = param;
    }

    public boolean isSM_DISBURSED_AMTSpecified() {
        return localSM_DISBURSED_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DISBURSED_AMT() {
        return localSM_DISBURSED_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_DISBURSED_AMT
     */
    public void setSM_DISBURSED_AMT(java.lang.String param) {
        localSM_DISBURSED_AMTTracker = param != null;

        this.localSM_DISBURSED_AMT = param;
    }

    public boolean isSM_DISBURSED_DTSpecified() {
        return localSM_DISBURSED_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_DISBURSED_DT() {
        return localSM_DISBURSED_DT;
    }

    /**
     * Auto generated setter method
     * @param param SM_DISBURSED_DT
     */
    public void setSM_DISBURSED_DT(java.lang.String param) {
        localSM_DISBURSED_DTTracker = param != null;

        this.localSM_DISBURSED_DT = param;
    }

    public boolean isSM_LAST_PAYMENT_DATESpecified() {
        return localSM_LAST_PAYMENT_DATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_LAST_PAYMENT_DATE() {
        return localSM_LAST_PAYMENT_DATE;
    }

    /**
     * Auto generated setter method
     * @param param SM_LAST_PAYMENT_DATE
     */
    public void setSM_LAST_PAYMENT_DATE(java.lang.String param) {
        localSM_LAST_PAYMENT_DATETracker = param != null;

        this.localSM_LAST_PAYMENT_DATE = param;
    }

    public boolean isSM_CLOSE_DTSpecified() {
        return localSM_CLOSE_DTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CLOSE_DT() {
        return localSM_CLOSE_DT;
    }

    /**
     * Auto generated setter method
     * @param param SM_CLOSE_DT
     */
    public void setSM_CLOSE_DT(java.lang.String param) {
        localSM_CLOSE_DTTracker = param != null;

        this.localSM_CLOSE_DT = param;
    }

    public boolean isSM_INSTALLMENT_AMTSpecified() {
        return localSM_INSTALLMENT_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_INSTALLMENT_AMT() {
        return localSM_INSTALLMENT_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_INSTALLMENT_AMT
     */
    public void setSM_INSTALLMENT_AMT(java.lang.String param) {
        localSM_INSTALLMENT_AMTTracker = param != null;

        this.localSM_INSTALLMENT_AMT = param;
    }

    public boolean isSM_OVERDUE_AMTSpecified() {
        return localSM_OVERDUE_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_OVERDUE_AMT() {
        return localSM_OVERDUE_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_OVERDUE_AMT
     */
    public void setSM_OVERDUE_AMT(java.lang.String param) {
        localSM_OVERDUE_AMTTracker = param != null;

        this.localSM_OVERDUE_AMT = param;
    }

    public boolean isSM_WRITE_OFF_AMTSpecified() {
        return localSM_WRITE_OFF_AMTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_WRITE_OFF_AMT() {
        return localSM_WRITE_OFF_AMT;
    }

    /**
     * Auto generated setter method
     * @param param SM_WRITE_OFF_AMT
     */
    public void setSM_WRITE_OFF_AMT(java.lang.String param) {
        localSM_WRITE_OFF_AMTTracker = param != null;

        this.localSM_WRITE_OFF_AMT = param;
    }

    public boolean isSM_CURRENT_BALSpecified() {
        return localSM_CURRENT_BALTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CURRENT_BAL() {
        return localSM_CURRENT_BAL;
    }

    /**
     * Auto generated setter method
     * @param param SM_CURRENT_BAL
     */
    public void setSM_CURRENT_BAL(java.lang.String param) {
        localSM_CURRENT_BALTracker = param != null;

        this.localSM_CURRENT_BAL = param;
    }

    public boolean isSM_CREDIT_LIMITSpecified() {
        return localSM_CREDIT_LIMITTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_CREDIT_LIMIT() {
        return localSM_CREDIT_LIMIT;
    }

    /**
     * Auto generated setter method
     * @param param SM_CREDIT_LIMIT
     */
    public void setSM_CREDIT_LIMIT(java.lang.String param) {
        localSM_CREDIT_LIMITTracker = param != null;

        this.localSM_CREDIT_LIMIT = param;
    }

    public boolean isSM_ACCOUNT_REMARKSSpecified() {
        return localSM_ACCOUNT_REMARKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_ACCOUNT_REMARKS() {
        return localSM_ACCOUNT_REMARKS;
    }

    /**
     * Auto generated setter method
     * @param param SM_ACCOUNT_REMARKS
     */
    public void setSM_ACCOUNT_REMARKS(java.lang.String param) {
        localSM_ACCOUNT_REMARKSTracker = param != null;

        this.localSM_ACCOUNT_REMARKS = param;
    }

    public boolean isSM_FREQUENCYSpecified() {
        return localSM_FREQUENCYTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSM_FREQUENCY() {
        return localSM_FREQUENCY;
    }

    /**
     * Auto generated setter method
     * @param param SM_FREQUENCY
     */
    public void setSM_FREQUENCY(java.lang.String param) {
        localSM_FREQUENCYTracker = param != null;

        this.localSM_FREQUENCY = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "multibureau.xsd.hdfcbank.mb.soap.softcell.com");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ChmbaseSRespType1", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ChmbaseSRespType1", xmlWriter);
            }
        }

        if (localSRNOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SRNO", xmlWriter);

            if (localSRNO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SRNO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSRNO);
            }

            xmlWriter.writeEndElement();
        }

        if (localMEMBER_REFERENCE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MEMBER_REFERENCE_NUMBER",
                xmlWriter);

            if (localMEMBER_REFERENCE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MEMBER_REFERENCE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMEMBER_REFERENCE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOA_SOURCE_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SOA_SOURCE_NAME", xmlWriter);

            if (localSOA_SOURCE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOA_SOURCE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOA_SOURCE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_REQUESTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_REQUEST", xmlWriter);

            if (localDATE_OF_REQUEST == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_REQUEST cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_REQUEST);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR", xmlWriter);

            if (localPREPARED_FOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localPREPARED_FOR_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PREPARED_FOR_ID", xmlWriter);

            if (localPREPARED_FOR_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PREPARED_FOR_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPREPARED_FOR_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_ISSUETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_ISSUE", xmlWriter);

            if (localDATE_OF_ISSUE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_ISSUE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_ISSUE);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORT_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPORT_ID", xmlWriter);

            if (localREPORT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPORT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localBATCH_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BATCH_ID", xmlWriter);

            if (localBATCH_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BATCH_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBATCH_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localSTATUS_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "STATUS_IQ", xmlWriter);

            if (localSTATUS_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "STATUS_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSTATUS_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localNAME_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NAME_IQ", xmlWriter);

            if (localNAME_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAKA_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AKA_IQ", xmlWriter);

            if (localAKA_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AKA_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAKA_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localSPOUSE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SPOUSE_IQ", xmlWriter);

            if (localSPOUSE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SPOUSE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSPOUSE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localFATHER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FATHER_IQ", xmlWriter);

            if (localFATHER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FATHER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFATHER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOTHER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MOTHER_IQ", xmlWriter);

            if (localMOTHER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOTHER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOTHER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOB_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB_IQ", xmlWriter);

            if (localDOB_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_IQ", xmlWriter);

            if (localAGE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAGE_AS_ON_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AGE_AS_ON_IQ", xmlWriter);

            if (localAGE_AS_ON_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AGE_AS_ON_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAGE_AS_ON_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARD_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD_IQ", xmlWriter);

            if (localRATION_CARD_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_IQ", xmlWriter);

            if (localPASSPORT_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTERS_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTERS_ID_IQ", xmlWriter);

            if (localVOTERS_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTERS_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTERS_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVING_LICENSE_NO_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVING_LICENSE_NO_IQ",
                xmlWriter);

            if (localDRIVING_LICENSE_NO_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVING_LICENSE_NO_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVING_LICENSE_NO_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_IQ", xmlWriter);

            if (localPAN_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localGENDER_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "GENDER_IQ", xmlWriter);

            if (localGENDER_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "GENDER_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localGENDER_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIP_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIP_IQ", xmlWriter);

            if (localOWNERSHIP_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIP_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIP_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_1_IQ", xmlWriter);

            if (localADDRESS_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_2_IQ", xmlWriter);

            if (localADDRESS_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_3_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_3_IQ", xmlWriter);

            if (localADDRESS_3_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_3_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_3_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_1_IQ", xmlWriter);

            if (localPHONE_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_2_IQ", xmlWriter);

            if (localPHONE_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_3_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_3_IQ", xmlWriter);

            if (localPHONE_3_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_3_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_3_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_1_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_1_IQ", xmlWriter);

            if (localEMAIL_1_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_1_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_1_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_2_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_2_IQ", xmlWriter);

            if (localEMAIL_2_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_2_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_2_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localBRANCH_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "BRANCH_IQ", xmlWriter);

            if (localBRANCH_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "BRANCH_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localBRANCH_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localKENDRA_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "KENDRA_IQ", xmlWriter);

            if (localKENDRA_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "KENDRA_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localKENDRA_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localMBR_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "MBR_ID_IQ", xmlWriter);

            if (localMBR_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MBR_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMBR_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOS_APP_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOS_APP_ID_IQ", xmlWriter);

            if (localLOS_APP_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOS_APP_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOS_APP_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYP_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP_IQ",
                xmlWriter);

            if (localCREDT_INQ_PURPS_TYP_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_INQ_PURPS_TYP_DESC_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_INQ_PURPS_TYP_DESC_IQ",
                xmlWriter);

            if (localCREDT_INQ_PURPS_TYP_DESC_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_INQ_PURPS_TYP_DESC_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_INQ_PURPS_TYP_DESC_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_INQUIRY_STAGE_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_INQUIRY_STAGE_IQ",
                xmlWriter);

            if (localCREDIT_INQUIRY_STAGE_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_INQUIRY_STAGE_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_INQUIRY_STAGE_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_ID_IQ", xmlWriter);

            if (localCREDT_RPT_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_REQ_TYP_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_REQ_TYP_IQ", xmlWriter);

            if (localCREDT_REQ_TYP_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_REQ_TYP_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_REQ_TYP_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDT_RPT_TRN_DT_TM_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDT_RPT_TRN_DT_TM_IQ",
                xmlWriter);

            if (localCREDT_RPT_TRN_DT_TM_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDT_RPT_TRN_DT_TM_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDT_RPT_TRN_DT_TM_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localAC_OPEN_DT_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AC_OPEN_DT_IQ", xmlWriter);

            if (localAC_OPEN_DT_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AC_OPEN_DT_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAC_OPEN_DT_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localLOAN_AMOUNT_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LOAN_AMOUNT_IQ", xmlWriter);

            if (localLOAN_AMOUNT_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LOAN_AMOUNT_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLOAN_AMOUNT_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localENTITY_ID_IQTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENTITY_ID_IQ", xmlWriter);

            if (localENTITY_ID_IQ == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENTITY_ID_IQ cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENTITY_ID_IQ);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_NO_OF_ACCOUNTS", xmlWriter);

            if (localPR_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_ACTIVE_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_ACTIVE_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localPR_ACTIVE_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_ACTIVE_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_ACTIVE_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_OVERDUE_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_OVERDUE_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localPR_OVERDUE_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_OVERDUE_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_OVERDUE_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_CURRENT_BALANCETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_CURRENT_BALANCE", xmlWriter);

            if (localPR_CURRENT_BALANCE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_CURRENT_BALANCE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_CURRENT_BALANCE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_SANCTIONED_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_SANCTIONED_AMOUNT", xmlWriter);

            if (localPR_SANCTIONED_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_SANCTIONED_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_SANCTIONED_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_DISBURSED_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_DISBURSED_AMOUNT", xmlWriter);

            if (localPR_DISBURSED_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_DISBURSED_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_DISBURSED_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_SECURED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_SECURED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localPR_SECURED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_SECURED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_SECURED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_UNSECURED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_UNSECURED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localPR_UNSECURED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_UNSECURED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_UNSECURED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localPR_UNTAGGED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PR_UNTAGGED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localPR_UNTAGGED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PR_UNTAGGED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPR_UNTAGGED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_NUMBER_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_NUMBER_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_NUMBER_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_NUMBER_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_NUMBER_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_ACTIVE_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_ACTIVE_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_ACTIVE_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_ACTIVE_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_ACTIVE_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_OVERDUE_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_OVERDUE_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_OVERDUE_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_OVERDUE_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_OVERDUE_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_CURRENT_BALANCETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_CURRENT_BALANCE", xmlWriter);

            if (localSE_CURRENT_BALANCE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_CURRENT_BALANCE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_CURRENT_BALANCE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_SANCTIONED_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_SANCTIONED_AMOUNT", xmlWriter);

            if (localSE_SANCTIONED_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_SANCTIONED_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_SANCTIONED_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_DISBURSED_AMOUNTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_DISBURSED_AMOUNT", xmlWriter);

            if (localSE_DISBURSED_AMOUNT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_DISBURSED_AMOUNT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_DISBURSED_AMOUNT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_SECURED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_SECURED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_SECURED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_SECURED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_SECURED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_UNSECURED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_UNSECURED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_UNSECURED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_UNSECURED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_UNSECURED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSE_UNTAGGED_NO_OF_ACCOUNTSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SE_UNTAGGED_NO_OF_ACCOUNTS",
                xmlWriter);

            if (localSE_UNTAGGED_NO_OF_ACCOUNTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SE_UNTAGGED_NO_OF_ACCOUNTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSE_UNTAGGED_NO_OF_ACCOUNTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINQURIES_IN_LAST_SIX_MONTHSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INQURIES_IN_LAST_SIX_MONTHS",
                xmlWriter);

            if (localINQURIES_IN_LAST_SIX_MONTHS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INQURIES_IN_LAST_SIX_MONTHS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINQURIES_IN_LAST_SIX_MONTHS);
            }

            xmlWriter.writeEndElement();
        }

        if (localLENGTH_OF_CREDIT_HIS_YEARTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LENGTH_OF_CREDIT_HIS_YEAR",
                xmlWriter);

            if (localLENGTH_OF_CREDIT_HIS_YEAR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LENGTH_OF_CREDIT_HIS_YEAR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLENGTH_OF_CREDIT_HIS_YEAR);
            }

            xmlWriter.writeEndElement();
        }

        if (localLENGTH_OF_CREDIT_HIS_MONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LENGTH_OF_CREDIT_HIS_MON",
                xmlWriter);

            if (localLENGTH_OF_CREDIT_HIS_MON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LENGTH_OF_CREDIT_HIS_MON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLENGTH_OF_CREDIT_HIS_MON);
            }

            xmlWriter.writeEndElement();
        }

        if (localAVG_ACC_AGE_YEARTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AVG_ACC_AGE_YEAR", xmlWriter);

            if (localAVG_ACC_AGE_YEAR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AVG_ACC_AGE_YEAR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAVG_ACC_AGE_YEAR);
            }

            xmlWriter.writeEndElement();
        }

        if (localAVG_ACC_AGE_MONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AVG_ACC_AGE_MON", xmlWriter);

            if (localAVG_ACC_AGE_MON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AVG_ACC_AGE_MON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAVG_ACC_AGE_MON);
            }

            xmlWriter.writeEndElement();
        }

        if (localNEW_ACC_IN_LAST_SIX_MONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NEW_ACC_IN_LAST_SIX_MON",
                xmlWriter);

            if (localNEW_ACC_IN_LAST_SIX_MON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NEW_ACC_IN_LAST_SIX_MON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNEW_ACC_IN_LAST_SIX_MON);
            }

            xmlWriter.writeEndElement();
        }

        if (localNEW_DELINQ_ACC_LAST_SIX_MONTracker) {
            namespace = "";
            writeStartElement(null, namespace, "NEW_DELINQ_ACC_LAST_SIX_MON",
                xmlWriter);

            if (localNEW_DELINQ_ACC_LAST_SIX_MON == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NEW_DELINQ_ACC_LAST_SIX_MON cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNEW_DELINQ_ACC_LAST_SIX_MON);
            }

            xmlWriter.writeEndElement();
        }

        if (localNAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "NAME", xmlWriter);

            if (localNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localNAME_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "NAME_REPORTED_DATE", xmlWriter);

            if (localNAME_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS", xmlWriter);

            if (localADDRESS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS);
            }

            xmlWriter.writeEndElement();
        }

        if (localADDRESS_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ADDRESS_REPORTED_DATE",
                xmlWriter);

            if (localADDRESS_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADDRESS_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADDRESS_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_NOTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_NO", xmlWriter);

            if (localPAN_NO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_NO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_NO);
            }

            xmlWriter.writeEndElement();
        }

        if (localPAN_NO_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PAN_NO_REPORTED_DATE", xmlWriter);

            if (localPAN_NO_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PAN_NO_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPAN_NO_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVING_LICENSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVING_LICENSE", xmlWriter);

            if (localDRIVING_LICENSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVING_LICENSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVING_LICENSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDRIVING_LICENSE_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DRIVING_LICENSE_REPORTED_DATE",
                xmlWriter);

            if (localDRIVING_LICENSE_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DRIVING_LICENSE_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDRIVING_LICENSE_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOBTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB", xmlWriter);

            if (localDOB == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB);
            }

            xmlWriter.writeEndElement();
        }

        if (localDOB_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "DOB_REPORTED_DATE", xmlWriter);

            if (localDOB_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DOB_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDOB_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID", xmlWriter);

            if (localVOTER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localVOTER_ID_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "VOTER_ID_REPORTED_DATE",
                xmlWriter);

            if (localVOTER_ID_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VOTER_ID_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVOTER_ID_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_NUMBER", xmlWriter);

            if (localPASSPORT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localPASSPORT_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PASSPORT_REPORTED_DATE",
                xmlWriter);

            if (localPASSPORT_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PASSPORT_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPASSPORT_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_NUMBER", xmlWriter);

            if (localPHONE_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localPHONE_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "PHONE_REPORTED_DATE", xmlWriter);

            if (localPHONE_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PHONE_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPHONE_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_CARDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_CARD", xmlWriter);

            if (localRATION_CARD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_CARD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_CARD);
            }

            xmlWriter.writeEndElement();
        }

        if (localRATION_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "RATION_REPORTED_DATE", xmlWriter);

            if (localRATION_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RATION_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRATION_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_ID", xmlWriter);

            if (localEMAIL_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localEMAIL_ID_REPORTED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "EMAIL_ID_REPORTED_DATE",
                xmlWriter);

            if (localEMAIL_ID_REPORTED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL_ID_REPORTED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL_ID_REPORTED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localMATCHED_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "MATCHED_TYPE", xmlWriter);

            if (localMATCHED_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MATCHED_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMATCHED_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCT_NUMBER", xmlWriter);

            if (localACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_GUARANTORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_GUARANTOR", xmlWriter);

            if (localCREDIT_GUARANTOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_GUARANTOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_GUARANTOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCT_TYPE", xmlWriter);

            if (localACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_REPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_REPORTED", xmlWriter);

            if (localDATE_REPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_REPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_REPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNERSHIP_INDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNERSHIP_IND", xmlWriter);

            if (localOWNERSHIP_IND == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNERSHIP_IND cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNERSHIP_IND);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_STATUS", xmlWriter);

            if (localACCOUNT_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISBURSED_AMT", xmlWriter);

            if (localDISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localDISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DISBURSED_DT", xmlWriter);

            if (localDISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLAST_PAYMENT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LAST_PAYMENT_DATE", xmlWriter);

            if (localLAST_PAYMENT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LAST_PAYMENT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLAST_PAYMENT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCLOSE_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CLOSE_DT", xmlWriter);

            if (localCLOSE_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CLOSE_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCLOSE_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localINSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "INSTALLMENT_AMT", xmlWriter);

            if (localINSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localOVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OVERDUE_AMT", xmlWriter);

            if (localOVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITE_OFF_AMT", xmlWriter);

            if (localWRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CURRENT_BAL", xmlWriter);

            if (localCURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localCREDIT_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CREDIT_LIMIT", xmlWriter);

            if (localCREDIT_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CREDIT_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCREDIT_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCOUNT_REMARKSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCOUNT_REMARKS", xmlWriter);

            if (localACCOUNT_REMARKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCOUNT_REMARKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCOUNT_REMARKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localFREQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "FREQUENCY", xmlWriter);

            if (localFREQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FREQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFREQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_STATUS", xmlWriter);

            if (localSECURITY_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localORIGINAL_TERMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ORIGINAL_TERM", xmlWriter);

            if (localORIGINAL_TERM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ORIGINAL_TERM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localORIGINAL_TERM);
            }

            xmlWriter.writeEndElement();
        }

        if (localTERM_TO_MATURITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "TERM_TO_MATURITY", xmlWriter);

            if (localTERM_TO_MATURITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TERM_TO_MATURITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTERM_TO_MATURITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localACCT_IN_DISPUTETracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACCT_IN_DISPUTE", xmlWriter);

            if (localACCT_IN_DISPUTE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACCT_IN_DISPUTE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACCT_IN_DISPUTE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSETTLEMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SETTLEMENT_AMT", xmlWriter);

            if (localSETTLEMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SETTLEMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSETTLEMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localPRINCIPAL_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PRINCIPAL_WRITE_OFF_AMT",
                xmlWriter);

            if (localPRINCIPAL_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRINCIPAL_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRINCIPAL_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOMBINED_PAYMENT_HISTORYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "COMBINED_PAYMENT_HISTORY",
                xmlWriter);

            if (localCOMBINED_PAYMENT_HISTORY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "COMBINED_PAYMENT_HISTORY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCOMBINED_PAYMENT_HISTORY);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPAYMENT_TENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "REPAYMENT_TENURE", xmlWriter);

            if (localREPAYMENT_TENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPAYMENT_TENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPAYMENT_TENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localINTEREST_RATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "INTEREST_RATE", xmlWriter);

            if (localINTEREST_RATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INTEREST_RATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINTEREST_RATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUIT_FILED_WILFUL_DEFAULTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SUIT_FILED_WILFUL_DEFAULT",
                xmlWriter);

            if (localSUIT_FILED_WILFUL_DEFAULT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUIT_FILED_WILFUL_DEFAULT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUIT_FILED_WILFUL_DEFAULT);
            }

            xmlWriter.writeEndElement();
        }

        if (localWRITTEN_OFF_SETTLED_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "WRITTEN_OFF_SETTLED_STATUS",
                xmlWriter);

            if (localWRITTEN_OFF_SETTLED_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WRITTEN_OFF_SETTLED_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWRITTEN_OFF_SETTLED_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCASH_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CASH_LIMIT", xmlWriter);

            if (localCASH_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CASH_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCASH_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localACTUAL_PAYMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ACTUAL_PAYMENT", xmlWriter);

            if (localACTUAL_PAYMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ACTUAL_PAYMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localACTUAL_PAYMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_TYPE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_TYPE_LD", xmlWriter);

            if (localSECURITY_TYPE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_TYPE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_TYPE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNER_NAME_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNER_NAME_LD", xmlWriter);

            if (localOWNER_NAME_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNER_NAME_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNER_NAME_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_VALUE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_VALUE_LD", xmlWriter);

            if (localSECURITY_VALUE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_VALUE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_VALUE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_VALUE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_VALUE_LD", xmlWriter);

            if (localDATE_OF_VALUE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_VALUE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_VALUE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_CHARGE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_CHARGE_LD", xmlWriter);

            if (localSECURITY_CHARGE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_CHARGE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_CHARGE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localPROPERTY_ADDRESS_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "PROPERTY_ADDRESS_LD", xmlWriter);

            if (localPROPERTY_ADDRESS_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PROPERTY_ADDRESS_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPROPERTY_ADDRESS_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localAUTOMOBILE_TYPE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "AUTOMOBILE_TYPE_LD", xmlWriter);

            if (localAUTOMOBILE_TYPE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AUTOMOBILE_TYPE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAUTOMOBILE_TYPE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localYEAR_OF_MANUFACTURE_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "YEAR_OF_MANUFACTURE_LD",
                xmlWriter);

            if (localYEAR_OF_MANUFACTURE_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "YEAR_OF_MANUFACTURE_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localYEAR_OF_MANUFACTURE_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localREGISTRATION_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "REGISTRATION_NUMBER_LD",
                xmlWriter);

            if (localREGISTRATION_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REGISTRATION_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREGISTRATION_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localENGINE_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENGINE_NUMBER_LD", xmlWriter);

            if (localENGINE_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENGINE_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENGINE_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCHASIS_NUMBER_LDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "CHASIS_NUMBER_LD", xmlWriter);

            if (localCHASIS_NUMBER_LD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CHASIS_NUMBER_LD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCHASIS_NUMBER_LD);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACCT_NUMBER", xmlWriter);

            if (localLINKED_ACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_CREDIT_GUARANTORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_CREDIT_GUARANTOR",
                xmlWriter);

            if (localLINKED_CREDIT_GUARANTOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_CREDIT_GUARANTOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_CREDIT_GUARANTOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACCT_TYPE", xmlWriter);

            if (localLINKED_ACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_DATE_REPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_DATE_REPORTED", xmlWriter);

            if (localLINKED_DATE_REPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_DATE_REPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_DATE_REPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_OWNERSHIP_INDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_OWNERSHIP_IND", xmlWriter);

            if (localLINKED_OWNERSHIP_IND == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_OWNERSHIP_IND cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_OWNERSHIP_IND);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACCOUNT_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACCOUNT_STATUS",
                xmlWriter);

            if (localLINKED_ACCOUNT_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACCOUNT_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACCOUNT_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_DISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_DISBURSED_AMT", xmlWriter);

            if (localLINKED_DISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_DISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_DISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_DISBURSED_DT", xmlWriter);

            if (localLINKED_DISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_DISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_LAST_PAYMENT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_LAST_PAYMENT_DATE",
                xmlWriter);

            if (localLINKED_LAST_PAYMENT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_LAST_PAYMENT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_LAST_PAYMENT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_CLOSED_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_CLOSED_DATE", xmlWriter);

            if (localLINKED_CLOSED_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_CLOSED_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_CLOSED_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_INSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_INSTALLMENT_AMT",
                xmlWriter);

            if (localLINKED_INSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_INSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_OVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_OVERDUE_AMT", xmlWriter);

            if (localLINKED_OVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_OVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_WRITE_OFF_AMT", xmlWriter);

            if (localLINKED_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_CURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_CURRENT_BAL", xmlWriter);

            if (localLINKED_CURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_CURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_CREDIT_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_CREDIT_LIMIT", xmlWriter);

            if (localLINKED_CREDIT_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_CREDIT_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_CREDIT_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACCOUNT_REMARKSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACCOUNT_REMARKS",
                xmlWriter);

            if (localLINKED_ACCOUNT_REMARKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACCOUNT_REMARKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACCOUNT_REMARKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_FREQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_FREQUENCY", xmlWriter);

            if (localLINKED_FREQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_FREQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_FREQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_SECURITY_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_SECURITY_STATUS",
                xmlWriter);

            if (localLINKED_SECURITY_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_SECURITY_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_SECURITY_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ORIGINAL_TERMTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ORIGINAL_TERM", xmlWriter);

            if (localLINKED_ORIGINAL_TERM == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ORIGINAL_TERM cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ORIGINAL_TERM);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_TERM_TO_MATURITYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_TERM_TO_MATURITY",
                xmlWriter);

            if (localLINKED_TERM_TO_MATURITY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_TERM_TO_MATURITY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_TERM_TO_MATURITY);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACCT_IN_DISPUTETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACCT_IN_DISPUTE",
                xmlWriter);

            if (localLINKED_ACCT_IN_DISPUTE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACCT_IN_DISPUTE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACCT_IN_DISPUTE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_SETTLEMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_SETTLEMENT_AMT",
                xmlWriter);

            if (localLINKED_SETTLEMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_SETTLEMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_SETTLEMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_PRNPAL_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_PRNPAL_WRITE_OFF_AMT",
                xmlWriter);

            if (localLINKED_PRNPAL_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_PRNPAL_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_PRNPAL_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_REPAYMENT_TENURETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_REPAYMENT_TENURE",
                xmlWriter);

            if (localLINKED_REPAYMENT_TENURE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_REPAYMENT_TENURE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_REPAYMENT_TENURE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_INTEREST_RATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_INTEREST_RATE", xmlWriter);

            if (localLINKED_INTEREST_RATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_INTEREST_RATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_INTEREST_RATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_SUIT_F_WILFUL_DEFAULTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_SUIT_F_WILFUL_DEFAULT",
                xmlWriter);

            if (localLINKED_SUIT_F_WILFUL_DEFAULT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_SUIT_F_WILFUL_DEFAULT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_SUIT_F_WILFUL_DEFAULT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_WRTN_OFF_SETTLD_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_WRTN_OFF_SETTLD_STATUS",
                xmlWriter);

            if (localLINKED_WRTN_OFF_SETTLD_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_WRTN_OFF_SETTLD_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_WRTN_OFF_SETTLD_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_CASH_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_CASH_LIMIT", xmlWriter);

            if (localLINKED_CASH_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_CASH_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_CASH_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLINKED_ACTUAL_PAYMENTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "LINKED_ACTUAL_PAYMENT",
                xmlWriter);

            if (localLINKED_ACTUAL_PAYMENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINKED_ACTUAL_PAYMENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINKED_ACTUAL_PAYMENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_TYPE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_TYPE_LA", xmlWriter);

            if (localSECURITY_TYPE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_TYPE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_TYPE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localOWNER_NAME_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "OWNER_NAME_LA", xmlWriter);

            if (localOWNER_NAME_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OWNER_NAME_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOWNER_NAME_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_VALUE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_VALUE_LA", xmlWriter);

            if (localSECURITY_VALUE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_VALUE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_VALUE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_OF_VALUE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "DATE_OF_VALUE_LA", xmlWriter);

            if (localDATE_OF_VALUE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_OF_VALUE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localDATE_OF_VALUE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSECURITY_CHARGE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "SECURITY_CHARGE_LA", xmlWriter);

            if (localSECURITY_CHARGE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SECURITY_CHARGE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSECURITY_CHARGE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localPROPERTY_ADDRESS_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "PROPERTY_ADDRESS_LA", xmlWriter);

            if (localPROPERTY_ADDRESS_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PROPERTY_ADDRESS_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPROPERTY_ADDRESS_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localAUTOMOBILE_TYPE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "AUTOMOBILE_TYPE_LA", xmlWriter);

            if (localAUTOMOBILE_TYPE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AUTOMOBILE_TYPE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAUTOMOBILE_TYPE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localYEAR_OF_MANUFACTURE_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "YEAR_OF_MANUFACTURE_LA",
                xmlWriter);

            if (localYEAR_OF_MANUFACTURE_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "YEAR_OF_MANUFACTURE_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localYEAR_OF_MANUFACTURE_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localREGISTRATION_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "REGISTRATION_NUMBER_LA",
                xmlWriter);

            if (localREGISTRATION_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REGISTRATION_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREGISTRATION_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localENGINE_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "ENGINE_NUMBER_LA", xmlWriter);

            if (localENGINE_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENGINE_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENGINE_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localCHASIS_NUMBER_LATracker) {
            namespace = "";
            writeStartElement(null, namespace, "CHASIS_NUMBER_LA", xmlWriter);

            if (localCHASIS_NUMBER_LA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CHASIS_NUMBER_LA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCHASIS_NUMBER_LA);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_NAMETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_NAME", xmlWriter);

            if (localSM_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ADDRESSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ADDRESS", xmlWriter);

            if (localSM_ADDRESS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ADDRESS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ADDRESS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DOBTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DOB", xmlWriter);

            if (localSM_DOB == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DOB cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DOB);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PHONETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PHONE", xmlWriter);

            if (localSM_PHONE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PHONE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PHONE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PANTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PAN", xmlWriter);

            if (localSM_PAN == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PAN cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PAN);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_PASSPORTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_PASSPORT", xmlWriter);

            if (localSM_PASSPORT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_PASSPORT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_PASSPORT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DRIVING_LICENSETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DRIVING_LICENSE", xmlWriter);

            if (localSM_DRIVING_LICENSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DRIVING_LICENSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DRIVING_LICENSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_VOTER_IDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_VOTER_ID", xmlWriter);

            if (localSM_VOTER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_VOTER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_VOTER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_E_MAILTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_E_MAIL", xmlWriter);

            if (localSM_E_MAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_E_MAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_E_MAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_RATION_CARDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_RATION_CARD", xmlWriter);

            if (localSM_RATION_CARD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_RATION_CARD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_RATION_CARD);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_MATCHED_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_MATCHED_TYPE", xmlWriter);

            if (localSM_MATCHED_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_MATCHED_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_MATCHED_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACCT_NUMBERTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACCT_NUMBER", xmlWriter);

            if (localSM_ACCT_NUMBER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACCT_NUMBER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACCT_NUMBER);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CREDIT_GUARANTORTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CREDIT_GUARANTOR", xmlWriter);

            if (localSM_CREDIT_GUARANTOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CREDIT_GUARANTOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CREDIT_GUARANTOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACCT_TYPETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACCT_TYPE", xmlWriter);

            if (localSM_ACCT_TYPE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACCT_TYPE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACCT_TYPE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DATE_REPORTEDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DATE_REPORTED", xmlWriter);

            if (localSM_DATE_REPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DATE_REPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DATE_REPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_OWNERSHIP_INDTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_OWNERSHIP_IND", xmlWriter);

            if (localSM_OWNERSHIP_IND == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_OWNERSHIP_IND cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_OWNERSHIP_IND);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACCOUNT_STATUSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACCOUNT_STATUS", xmlWriter);

            if (localSM_ACCOUNT_STATUS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACCOUNT_STATUS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACCOUNT_STATUS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DISBURSED_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DISBURSED_AMT", xmlWriter);

            if (localSM_DISBURSED_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DISBURSED_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DISBURSED_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_DISBURSED_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_DISBURSED_DT", xmlWriter);

            if (localSM_DISBURSED_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_DISBURSED_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_DISBURSED_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_LAST_PAYMENT_DATETracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_LAST_PAYMENT_DATE", xmlWriter);

            if (localSM_LAST_PAYMENT_DATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_LAST_PAYMENT_DATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_LAST_PAYMENT_DATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CLOSE_DTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CLOSE_DT", xmlWriter);

            if (localSM_CLOSE_DT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CLOSE_DT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CLOSE_DT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_INSTALLMENT_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_INSTALLMENT_AMT", xmlWriter);

            if (localSM_INSTALLMENT_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_INSTALLMENT_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_INSTALLMENT_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_OVERDUE_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_OVERDUE_AMT", xmlWriter);

            if (localSM_OVERDUE_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_OVERDUE_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_OVERDUE_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_WRITE_OFF_AMTTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_WRITE_OFF_AMT", xmlWriter);

            if (localSM_WRITE_OFF_AMT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_WRITE_OFF_AMT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_WRITE_OFF_AMT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CURRENT_BALTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CURRENT_BAL", xmlWriter);

            if (localSM_CURRENT_BAL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CURRENT_BAL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CURRENT_BAL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_CREDIT_LIMITTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_CREDIT_LIMIT", xmlWriter);

            if (localSM_CREDIT_LIMIT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_CREDIT_LIMIT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_CREDIT_LIMIT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_ACCOUNT_REMARKSTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_ACCOUNT_REMARKS", xmlWriter);

            if (localSM_ACCOUNT_REMARKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_ACCOUNT_REMARKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_ACCOUNT_REMARKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSM_FREQUENCYTracker) {
            namespace = "";
            writeStartElement(null, namespace, "SM_FREQUENCY", xmlWriter);

            if (localSM_FREQUENCY == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SM_FREQUENCY cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSM_FREQUENCY);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("multibureau.xsd.hdfcbank.mb.soap.softcell.com")) {
            return "ns2";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ChmbaseSRespType1 parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ChmbaseSRespType1 object = new ChmbaseSRespType1();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ChmbaseSRespType1".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ChmbaseSRespType1) com.softcell.soap.mb.hdfcbank.xsd.multibureau.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SRNO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SRNO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSRNO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "MEMBER_REFERENCE_NUMBER").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MEMBER_REFERENCE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMEMBER_REFERENCE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOA_SOURCE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOA_SOURCE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOA_SOURCE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_REQUEST").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_REQUEST" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_REQUEST(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PREPARED_FOR_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PREPARED_FOR_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPREPARED_FOR_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_ISSUE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_ISSUE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_ISSUE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORT_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BATCH_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BATCH_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BATCH_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBATCH_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "STATUS_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "STATUS_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "STATUS_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSTATUS_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NAME_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AKA_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AKA_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AKA_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAKA_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SPOUSE_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SPOUSE_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SPOUSE_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSPOUSE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FATHER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FATHER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FATHER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFATHER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MOTHER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOTHER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOTHER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOTHER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AGE_AS_ON_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AGE_AS_ON_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AGE_AS_ON_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAGE_AS_ON_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATION_CARD_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATION_CARD_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTERS_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTERS_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTERS_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTERS_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENSE_NO_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENSE_NO_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVING_LICENSE_NO_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVING_LICENSE_NO_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "GENDER_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "GENDER_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "GENDER_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setGENDER_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNERSHIP_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNERSHIP_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIP_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIP_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS_3_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS_3_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_3_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_3_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_3_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_3_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_3_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_3_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_1_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_1_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_1_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_1_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_2_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_2_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_2_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_2_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "BRANCH_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "BRANCH_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "BRANCH_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setBRANCH_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "KENDRA_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "KENDRA_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "KENDRA_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setKENDRA_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MBR_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MBR_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MBR_ID_IQ" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMBR_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOS_APP_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOS_APP_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOS_APP_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOS_APP_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDT_INQ_PURPS_TYP_DESC_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_INQ_PURPS_TYP_DESC_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_INQ_PURPS_TYP_DESC_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDIT_INQUIRY_STAGE_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDIT_INQUIRY_STAGE_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_INQUIRY_STAGE_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_INQUIRY_STAGE_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_RPT_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDT_REQ_TYP_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_REQ_TYP_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_REQ_TYP_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "CREDT_RPT_TRN_DT_TM_IQ").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CREDT_RPT_TRN_DT_TM_IQ").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDT_RPT_TRN_DT_TM_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDT_RPT_TRN_DT_TM_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AC_OPEN_DT_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AC_OPEN_DT_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AC_OPEN_DT_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAC_OPEN_DT_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LOAN_AMOUNT_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LOAN_AMOUNT_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLOAN_AMOUNT_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENTITY_ID_IQ").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENTITY_ID_IQ").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENTITY_ID_IQ" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENTITY_ID_IQ(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PR_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PR_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PR_ACTIVE_NO_OF_ACCOUNTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PR_ACTIVE_NO_OF_ACCOUNTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_ACTIVE_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_ACTIVE_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PR_OVERDUE_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PR_OVERDUE_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_OVERDUE_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_OVERDUE_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PR_CURRENT_BALANCE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PR_CURRENT_BALANCE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_CURRENT_BALANCE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_CURRENT_BALANCE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PR_SANCTIONED_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PR_SANCTIONED_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_SANCTIONED_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_SANCTIONED_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PR_DISBURSED_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PR_DISBURSED_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_DISBURSED_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_DISBURSED_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PR_SECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PR_SECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_SECURED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_SECURED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PR_UNSECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PR_UNSECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_UNSECURED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_UNSECURED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PR_UNTAGGED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PR_UNTAGGED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PR_UNTAGGED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPR_UNTAGGED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_NUMBER_OF_ACCOUNTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_NUMBER_OF_ACCOUNTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_NUMBER_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_NUMBER_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_ACTIVE_NO_OF_ACCOUNTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_ACTIVE_NO_OF_ACCOUNTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_ACTIVE_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_ACTIVE_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_OVERDUE_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_OVERDUE_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_OVERDUE_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_OVERDUE_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SE_CURRENT_BALANCE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SE_CURRENT_BALANCE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_CURRENT_BALANCE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_CURRENT_BALANCE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SE_SANCTIONED_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SE_SANCTIONED_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_SANCTIONED_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_SANCTIONED_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SE_DISBURSED_AMOUNT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SE_DISBURSED_AMOUNT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_DISBURSED_AMOUNT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_DISBURSED_AMOUNT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_SECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_SECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_SECURED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_SECURED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_UNSECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_UNSECURED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_UNSECURED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_UNSECURED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SE_UNTAGGED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SE_UNTAGGED_NO_OF_ACCOUNTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SE_UNTAGGED_NO_OF_ACCOUNTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSE_UNTAGGED_NO_OF_ACCOUNTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "INQURIES_IN_LAST_SIX_MONTHS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INQURIES_IN_LAST_SIX_MONTHS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INQURIES_IN_LAST_SIX_MONTHS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINQURIES_IN_LAST_SIX_MONTHS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LENGTH_OF_CREDIT_HIS_YEAR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LENGTH_OF_CREDIT_HIS_YEAR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LENGTH_OF_CREDIT_HIS_YEAR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLENGTH_OF_CREDIT_HIS_YEAR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LENGTH_OF_CREDIT_HIS_MON").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LENGTH_OF_CREDIT_HIS_MON").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LENGTH_OF_CREDIT_HIS_MON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLENGTH_OF_CREDIT_HIS_MON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AVG_ACC_AGE_YEAR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AVG_ACC_AGE_YEAR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AVG_ACC_AGE_YEAR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAVG_ACC_AGE_YEAR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AVG_ACC_AGE_MON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AVG_ACC_AGE_MON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AVG_ACC_AGE_MON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAVG_ACC_AGE_MON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NEW_ACC_IN_LAST_SIX_MON").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NEW_ACC_IN_LAST_SIX_MON").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NEW_ACC_IN_LAST_SIX_MON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNEW_ACC_IN_LAST_SIX_MON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "NEW_DELINQ_ACC_LAST_SIX_MON").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NEW_DELINQ_ACC_LAST_SIX_MON").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NEW_DELINQ_ACC_LAST_SIX_MON" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNEW_DELINQ_ACC_LAST_SIX_MON(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "NAME_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ADDRESS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADDRESS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "ADDRESS_REPORTED_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "ADDRESS_REPORTED_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADDRESS_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADDRESS_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_NO").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_NO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_NO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_NO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PAN_NO_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PAN_NO_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PAN_NO_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPAN_NO_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DRIVING_LICENSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DRIVING_LICENSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVING_LICENSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVING_LICENSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENSE_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "DRIVING_LICENSE_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DRIVING_LICENSE_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDRIVING_LICENSE_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DOB_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOB_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DOB_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDOB_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "VOTER_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "VOTER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_REPORTED_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "VOTER_ID_REPORTED_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VOTER_ID_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVOTER_ID_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PASSPORT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PASSPORT_REPORTED_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PASSPORT_REPORTED_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PASSPORT_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPASSPORT_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PHONE_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PHONE_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PHONE_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPHONE_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATION_CARD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATION_CARD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_CARD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_CARD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "RATION_REPORTED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "RATION_REPORTED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RATION_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRATION_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "EMAIL_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "EMAIL_ID_REPORTED_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "EMAIL_ID_REPORTED_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL_ID_REPORTED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL_ID_REPORTED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "MATCHED_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MATCHED_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MATCHED_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMATCHED_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDIT_GUARANTOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDIT_GUARANTOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_GUARANTOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_GUARANTOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCT_TYPE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_REPORTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_REPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_REPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_REPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNERSHIP_IND").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNERSHIP_IND").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNERSHIP_IND" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNERSHIP_IND(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISBURSED_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISBURSED_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DISBURSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DISBURSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LAST_PAYMENT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LAST_PAYMENT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LAST_PAYMENT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLAST_PAYMENT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CLOSE_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CLOSE_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CLOSE_DT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCLOSE_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INSTALLMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INSTALLMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OVERDUE_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OVERDUE_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CURRENT_BAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CURRENT_BAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CREDIT_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREDIT_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CREDIT_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCREDIT_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCOUNT_REMARKS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCOUNT_REMARKS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCOUNT_REMARKS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCOUNT_REMARKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "FREQUENCY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FREQUENCY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FREQUENCY" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFREQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ORIGINAL_TERM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ORIGINAL_TERM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ORIGINAL_TERM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setORIGINAL_TERM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "TERM_TO_MATURITY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TERM_TO_MATURITY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TERM_TO_MATURITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTERM_TO_MATURITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACCT_IN_DISPUTE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACCT_IN_DISPUTE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACCT_IN_DISPUTE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACCT_IN_DISPUTE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SETTLEMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SETTLEMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SETTLEMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSETTLEMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "PRINCIPAL_WRITE_OFF_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PRINCIPAL_WRITE_OFF_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRINCIPAL_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRINCIPAL_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "COMBINED_PAYMENT_HISTORY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "COMBINED_PAYMENT_HISTORY").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COMBINED_PAYMENT_HISTORY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOMBINED_PAYMENT_HISTORY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "REPAYMENT_TENURE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPAYMENT_TENURE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPAYMENT_TENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPAYMENT_TENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "INTEREST_RATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "INTEREST_RATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INTEREST_RATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINTEREST_RATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "SUIT_FILED_WILFUL_DEFAULT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SUIT_FILED_WILFUL_DEFAULT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUIT_FILED_WILFUL_DEFAULT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUIT_FILED_WILFUL_DEFAULT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "WRITTEN_OFF_SETTLED_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "WRITTEN_OFF_SETTLED_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WRITTEN_OFF_SETTLED_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWRITTEN_OFF_SETTLED_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CASH_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CASH_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CASH_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCASH_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ACTUAL_PAYMENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ACTUAL_PAYMENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ACTUAL_PAYMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setACTUAL_PAYMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_TYPE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_TYPE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_TYPE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_TYPE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNER_NAME_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNER_NAME_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNER_NAME_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNER_NAME_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_VALUE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_VALUE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_VALUE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_VALUE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_VALUE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_VALUE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_VALUE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_VALUE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_CHARGE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_CHARGE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_CHARGE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_CHARGE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PROPERTY_ADDRESS_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROPERTY_ADDRESS_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PROPERTY_ADDRESS_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPROPERTY_ADDRESS_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AUTOMOBILE_TYPE_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AUTOMOBILE_TYPE_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AUTOMOBILE_TYPE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAUTOMOBILE_TYPE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "YEAR_OF_MANUFACTURE_LD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "YEAR_OF_MANUFACTURE_LD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "YEAR_OF_MANUFACTURE_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setYEAR_OF_MANUFACTURE_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REGISTRATION_NUMBER_LD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REGISTRATION_NUMBER_LD").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REGISTRATION_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREGISTRATION_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENGINE_NUMBER_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENGINE_NUMBER_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENGINE_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENGINE_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CHASIS_NUMBER_LD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CHASIS_NUMBER_LD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CHASIS_NUMBER_LD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCHASIS_NUMBER_LD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_ACCT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_ACCT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_CREDIT_GUARANTOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_CREDIT_GUARANTOR").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_CREDIT_GUARANTOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_CREDIT_GUARANTOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACCT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_DATE_REPORTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_DATE_REPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_DATE_REPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_DATE_REPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_OWNERSHIP_IND").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_OWNERSHIP_IND").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_OWNERSHIP_IND" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_OWNERSHIP_IND(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCOUNT_STATUS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCOUNT_STATUS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACCOUNT_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACCOUNT_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_DISBURSED_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_DISBURSED_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_DISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_DISBURSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_DISBURSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_DISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_LAST_PAYMENT_DATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_LAST_PAYMENT_DATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_LAST_PAYMENT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_LAST_PAYMENT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_CLOSED_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_CLOSED_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_CLOSED_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_CLOSED_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_INSTALLMENT_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_INSTALLMENT_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_INSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_OVERDUE_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_OVERDUE_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_OVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_CURRENT_BAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_CURRENT_BAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_CURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_CREDIT_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_CREDIT_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_CREDIT_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_CREDIT_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCOUNT_REMARKS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCOUNT_REMARKS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACCOUNT_REMARKS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACCOUNT_REMARKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_FREQUENCY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_FREQUENCY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_FREQUENCY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_FREQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_SECURITY_STATUS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_SECURITY_STATUS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_SECURITY_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_SECURITY_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_ORIGINAL_TERM").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_ORIGINAL_TERM").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ORIGINAL_TERM" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ORIGINAL_TERM(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_TERM_TO_MATURITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_TERM_TO_MATURITY").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_TERM_TO_MATURITY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_TERM_TO_MATURITY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCT_IN_DISPUTE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_ACCT_IN_DISPUTE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACCT_IN_DISPUTE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACCT_IN_DISPUTE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_SETTLEMENT_AMT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_SETTLEMENT_AMT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_SETTLEMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_SETTLEMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_PRNPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_PRNPAL_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_PRNPAL_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_PRNPAL_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_REPAYMENT_TENURE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_REPAYMENT_TENURE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_REPAYMENT_TENURE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_REPAYMENT_TENURE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_INTEREST_RATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_INTEREST_RATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_INTEREST_RATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_INTEREST_RATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_SUIT_F_WILFUL_DEFAULT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_SUIT_F_WILFUL_DEFAULT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_SUIT_F_WILFUL_DEFAULT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_SUIT_F_WILFUL_DEFAULT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_WRTN_OFF_SETTLD_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_WRTN_OFF_SETTLD_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_WRTN_OFF_SETTLD_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_WRTN_OFF_SETTLD_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "LINKED_CASH_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINKED_CASH_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_CASH_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_CASH_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "LINKED_ACTUAL_PAYMENT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "LINKED_ACTUAL_PAYMENT").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINKED_ACTUAL_PAYMENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINKED_ACTUAL_PAYMENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_TYPE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_TYPE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_TYPE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_TYPE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "OWNER_NAME_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OWNER_NAME_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OWNER_NAME_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOWNER_NAME_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_VALUE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_VALUE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_VALUE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_VALUE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "DATE_OF_VALUE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_OF_VALUE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATE_OF_VALUE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATE_OF_VALUE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SECURITY_CHARGE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SECURITY_CHARGE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SECURITY_CHARGE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSECURITY_CHARGE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "PROPERTY_ADDRESS_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROPERTY_ADDRESS_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PROPERTY_ADDRESS_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPROPERTY_ADDRESS_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "AUTOMOBILE_TYPE_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AUTOMOBILE_TYPE_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AUTOMOBILE_TYPE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAUTOMOBILE_TYPE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "YEAR_OF_MANUFACTURE_LA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "YEAR_OF_MANUFACTURE_LA").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "YEAR_OF_MANUFACTURE_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setYEAR_OF_MANUFACTURE_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("",
                            "REGISTRATION_NUMBER_LA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "REGISTRATION_NUMBER_LA").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REGISTRATION_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREGISTRATION_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "ENGINE_NUMBER_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENGINE_NUMBER_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENGINE_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENGINE_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "CHASIS_NUMBER_LA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CHASIS_NUMBER_LA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CHASIS_NUMBER_LA" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCHASIS_NUMBER_LA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_NAME").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ADDRESS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ADDRESS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ADDRESS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ADDRESS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DOB").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DOB").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DOB" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DOB(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_PHONE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_PHONE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PHONE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PHONE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_PAN").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_PAN").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PAN" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PAN(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_PASSPORT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_PASSPORT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_PASSPORT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_PASSPORT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DRIVING_LICENSE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DRIVING_LICENSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DRIVING_LICENSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DRIVING_LICENSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_VOTER_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_VOTER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_VOTER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_VOTER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_E_MAIL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_E_MAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_E_MAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_E_MAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_RATION_CARD").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_RATION_CARD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_RATION_CARD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_RATION_CARD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_MATCHED_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_MATCHED_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_MATCHED_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_MATCHED_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACCT_NUMBER").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACCT_NUMBER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACCT_NUMBER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACCT_NUMBER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CREDIT_GUARANTOR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CREDIT_GUARANTOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CREDIT_GUARANTOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CREDIT_GUARANTOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACCT_TYPE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACCT_TYPE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACCT_TYPE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACCT_TYPE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DATE_REPORTED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DATE_REPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DATE_REPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DATE_REPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_OWNERSHIP_IND").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_OWNERSHIP_IND").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_OWNERSHIP_IND" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_OWNERSHIP_IND(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACCOUNT_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACCOUNT_STATUS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACCOUNT_STATUS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACCOUNT_STATUS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DISBURSED_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DISBURSED_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DISBURSED_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DISBURSED_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_DISBURSED_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_DISBURSED_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_DISBURSED_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_DISBURSED_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_LAST_PAYMENT_DATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_LAST_PAYMENT_DATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_LAST_PAYMENT_DATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_LAST_PAYMENT_DATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CLOSE_DT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CLOSE_DT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CLOSE_DT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CLOSE_DT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_INSTALLMENT_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_INSTALLMENT_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_INSTALLMENT_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_INSTALLMENT_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_OVERDUE_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_OVERDUE_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_OVERDUE_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_OVERDUE_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_WRITE_OFF_AMT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_WRITE_OFF_AMT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_WRITE_OFF_AMT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_WRITE_OFF_AMT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CURRENT_BAL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CURRENT_BAL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CURRENT_BAL" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CURRENT_BAL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_CREDIT_LIMIT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_CREDIT_LIMIT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_CREDIT_LIMIT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_CREDIT_LIMIT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_ACCOUNT_REMARKS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_ACCOUNT_REMARKS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_ACCOUNT_REMARKS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_ACCOUNT_REMARKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName("", "SM_FREQUENCY").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SM_FREQUENCY").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SM_FREQUENCY" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSM_FREQUENCY(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
